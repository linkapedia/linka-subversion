#!/usr/bin/python
import os
import send_file
import sys
def send_directory(host,port,directory):
	count_files=0
	for dirnames,dirs,files in os.walk(directory):
		for file_name in files:
			print "sending file %s"%directory+file_name
			try:
				send_file.send_file(directory+file_name,host,port,False)
				count_files=count_files+1
			except Exception as e:				
				print e.message
	print "files sent number %d"%count_files
if __name__=='__main__':
	if(len(sys.argv)<4):
		print "please input all option example ./send_directory.py localhost 5000 ~/test_data/"
	else:
		send_directory(sys.argv[1],int(sys.argv[2]),sys.argv[3])
	

