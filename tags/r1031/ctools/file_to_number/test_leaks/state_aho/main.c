#include "../../state_aho.h"
int main(int argc,char **argv)
{
	memory_page_buffer *_buffer=NULL;
	word_poll *_word_poll=NULL;
	state_aho_cache *_state_aho_cache=load_state_aho("../../deploy/state.aho",&_buffer);
	int _word_size=2;
	if(_state_aho_cache==NULL)
	{
		printf("************ Error Building Treee ************\n");
		return -1;
	}
	printf("************ Creating Memory Poll  ************\n");
	_word_poll=create_word_poll(&_buffer);
	if(_word_poll==NULL)
	{
		printf("************ Error Make Poll************\n");
		return -1;
	}	
	_word_poll->words[0]=2430;
	_word_poll->words[1]=74;
	_word_poll->current_size=2;
	printf("************ Init Search ************\n");
	search_words(_state_aho_cache,_word_poll);
	printf("************ phrases found %d ************\n",_word_poll->current_size-_word_size);
	delete_state_aho_cache(_state_aho_cache);
	free_memory_page_buffer(_buffer);
	free(_word_poll);
	return 0;
}
