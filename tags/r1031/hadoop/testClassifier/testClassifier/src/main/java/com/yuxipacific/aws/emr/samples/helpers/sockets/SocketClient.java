package com.yuxipacific.aws.emr.samples.helpers.sockets;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ResourceBundle;

public class SocketClient {

    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("classifier/config");
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 5000;
    private static final byte PACKET_TITLE_TYPE = 1;
    private static final byte PACKET_DOCUMENT_TYPE = 2;
    private static final byte PACKET_END_TYPE = 3;

    private static Socket createSocket() throws Exception {
        Socket socket = new Socket();
        int port = Integer.parseInt(systemConfig.getString("classifier.socket.server.port"));
        String host = systemConfig.getString("classifier.socket.server.ip");
        if (host == null) {
            host = DEFAULT_HOST;
        }
        socket.connect(new InetSocketAddress(host, port));
        return socket;
    }

    private static void sendPacket(OutputStream outStream, int size, byte type, byte[] data) throws Exception {
        byte buffer[] = new byte[8];
        byte intdata[] = ByteBuffer.allocate(4).putInt(size).array();
        int index = 0;
        for (int i = intdata.length - 1; i > -1; i--) {
            buffer[index] = intdata[i];
            index++;
        }
        buffer[4] = type;
        buffer[5] = 'b';
        buffer[6] = 'i';
        buffer[7] = 0;
        
        outStream.write(buffer);
        outStream.flush();
        outStream.write(data);
        outStream.flush();
    }

    public static String sendAndReciveData(String message) throws Exception {
        if ((message == null) || (message.trim().isEmpty())) {
            System.err.println("No data provided to send to the socket.");
            return null;
        }
        String[] lines = null;
        OutputStream socketOutStream = null;
        InputStream in = null;
        Socket s = null;
        int size = 0;
        try {

            s = createSocket();
            socketOutStream = s.getOutputStream();
            in = s.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        lines = message.split("(\r\n|\n)");
        size = lines[0].getBytes().length;
        sendPacket(socketOutStream, size, PACKET_TITLE_TYPE, lines[0].getBytes());
        for (int i = 1; i < lines.length; i++) {
            if(lines[i].contains("ocatello music club"))
            {
                System.out.println(size);
            }
            
            size = lines[i].getBytes().length;
            sendPacket(socketOutStream, size, PACKET_DOCUMENT_TYPE, lines[i].getBytes());
        }
        sendPacket(socketOutStream, 0, PACKET_END_TYPE, new byte[]{0});
        //s.shutdownOutput();
        System.out.println("message sent...\n");
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] arr = new byte[1024];
       
        int b = 0;
        try {
            while ((b = in.read(arr)) != -1) {
                bao.write(arr, 0, b);
            }

            if (!s.isClosed()) {
     
                in.close();
                s.close();
            }
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }

        System.out.println("Socket Closed...");
        String resp = new String(bao.toByteArray(), "UTF-8");
        return resp;
    }
}