package com.yuxipacific.aws.emr.samples.helpers.tika;

import com.yuxipacific.aws.emr.samples.helpers.tika.exceptions.EmptyDocumentException;
import com.yuxipacific.aws.emr.samples.helpers.tika.exceptions.NoParseableDocumentException;
import java.io.File;
import java.io.FileNotFoundException;
import org.apache.tika.metadata.Metadata;

/**
 *
 * @author Bancolombia
 */
public class ParserProxy {

    public static String getTitleFromFile(File file) {

        if (file == null) {
            System.out.println("The file can not be null");
            return null;
        }

        if (!file.isFile() || !file.exists()) {
            System.out.println("the file {" + file.getAbsolutePath() + "} does not exist or is not a file");
            return null;
        }

        return FileParser.getMetadata(file).get(Metadata.TITLE);
    }

    public static String getTitleFromFile(byte[] file) {

        if (file == null) {
            System.out.println("the file content can not be null");
            return null;
        }
        return FileParser.getMetadata(file).get(Metadata.TITLE);
    }

    public static String parse(File file) throws NoParseableDocumentException, FileNotFoundException {
        String docContent = null;
        String reason = null;
        try {
            if (file != null && file.exists()) {
                docContent = FileParser.parse(file);
                if (docContent == null || docContent.isEmpty()) {
                    reason = "The file {" + file.getName() + "} does not contain relevant text to be classified.";
                    throw new EmptyDocumentException(reason);
                }
            } else {
                reason = "The file {" + file.getAbsolutePath() + "} was not found.";
                throw new FileNotFoundException(reason);
            }
        } catch (NoParseableDocumentException ex) {
            throw new NoParseableDocumentException(ex);
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException(reason);
        } catch (Exception ex) {
            System.out.println("Error while parsing the document" + ex.getMessage());
        }
        return docContent;
    }

    public static String parse(byte[] file) {
        String docContent = null;
        String reason = null;
        try {
            if (file != null) {
                docContent = FileParser.parse(file);
                if (docContent == null || docContent.isEmpty()) {
                    reason = "The file does not contain relevant text to be classified.";
                    throw new EmptyDocumentException(reason);
                }
            }
        } catch (Exception ex) {
            System.out.println("Error while parsing the document" + ex.getMessage());
        }
        return docContent;
    }
}
