package com.yuxipacific.testclassifier;

import com.yuxipacific.aws.emr.samples.helpers.EightyLegResults;
import com.yuxipacific.aws.emr.samples.helpers.reverb.SentenceExtractorProxy;
import com.yuxipacific.aws.emr.samples.helpers.sockets.SocketClient;
import com.yuxipacific.aws.emr.samples.helpers.tika.FileUtils;
import com.yuxipacific.aws.emr.samples.helpers.tika.ParserProxy;
import com.yuxipacific.watcher.DirWatcher;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        final String inputPath = args[0];
        final String outputPath = args[1];

        fileWatcher(inputPath, outputPath);

    }

    public static void fileWatcher(final String inputPath, final String outputPath) {
        File temp = new File(outputPath);
        final File processDir = new File(temp.getParentFile() + "/process");
        if (!processDir.exists()) {
            processDir.mkdir();
        }

        TimerTask task = new DirWatcher(inputPath) {
            protected void onChange(File file, String action) {



                // here we code the action on a change
                System.out.println("File " + file.getName() + " action: " + action);
                if ("add".equals(action)) {
                    File inputFile = new File(inputPath + "/" + file.getName());

                    try {
                        ByteArrayInputStream bai = new ByteArrayInputStream(FileUtils.readBytes(file));
                        try {
                            HashMap<String, byte[]> results = EightyLegResults.readFile(bai);
                            Set<String> keySet = results.keySet();
                            for (String _key : keySet) {
                                if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                                    String content = new String((byte[]) results.get(_key), "UTF-8");
                                    if ((content != null) && (!content.trim().isEmpty())) {

                                        String md5 = DigestUtils.md5Hex(_key.toString());
                                        write(content, processDir.getPath() + "/" + file.getName(), md5);

                                    }
                                } else {
                                    System.out.println("Exception: The content for the url: " + _key + " is null or empty.");
                                }
                            }

                        } catch (Exception ex) {
                            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }


                    inputFile.delete();
                    File processDirTemp = new File(processDir.getPath() + "/" + file.getName());
                    work(processDirTemp.getPath(), outputPath + "/" + file.getName());

                    processDirTemp.delete();
                }

            }
        };

        Timer timer = new Timer();
        timer.schedule(task, new Date(), 1000);
    }

    public static void work(String inputPath, String outputPath) {
        File folderOutput = new File(outputPath);
        if (!folderOutput.exists()) {
            folderOutput.mkdir();
        }

        File folder = new File(inputPath);
        File[] listOfFiles = folder.listFiles();
        byte[] bytes;
        int size;
        String title;
        String content;
        String sentences;
        String classifierInfoResponse;
        File dir;

        for (File file : listOfFiles) {
            if (file.isFile()) {
                size = (int) file.length();
                bytes = new byte[size];
                try {
                    bytes = read(file);
                    title = ParserProxy.getTitleFromFile(bytes);
                    content = ParserProxy.parse(bytes);
                    sentences = SentenceExtractorProxy.extractFrom(content, true);
                    if (sentences != null && !sentences.isEmpty()) {
                        classifierInfoResponse = SocketClient.sendAndReciveData(sentences);
                    } else {
                        classifierInfoResponse = null;
                    }

                    dir = new File(outputPath + "/" + file.getName());
                    dir.mkdirs();
                    if (sentences != null && !sentences.isEmpty()) {
                        write(sentences, dir.getAbsolutePath(), "sentences");
                    }
                    if (classifierInfoResponse != null && !classifierInfoResponse.isEmpty()) {
                        write(classifierInfoResponse, dir.getAbsolutePath(), "classifier");
                    }
                    copyfile(file.getAbsolutePath(), dir.getPath() + "/" + file.getName());
                    file.delete();

                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    public static byte[] read(File file) throws IOException {

        byte[] buffer = new byte[(int) file.length()];
        InputStream ios = null;
        try {
            ios = new FileInputStream(file);

            if (ios.read(buffer) == -1) {
                throw new IOException("EOF reached while trying to read the whole file");
            }
        } finally {
            try {
                if (ios != null) {
                    ios.close();
                }
            } catch (IOException e) {
            }
        }

        return buffer;
    }

    public static void write(String content, String path, String filename) {
        FileWriter fileWriter = null;
        try {
            File folder = new File(path);
            if (!folder.exists()) {
                folder.mkdir();
            }

            File newTextFile = new File(path + "/" + filename);
            fileWriter = new FileWriter(newTextFile);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
        } finally {
            try {
                fileWriter.close();
            } catch (IOException ex) {
            }
        }
    }

    private static void copyfile(String srFile, String dtFile) {
        try {
            File f1 = new File(srFile);
            File f2 = new File(dtFile);
            InputStream in = new FileInputStream(f1);

            //For Append the file.
//  OutputStream out = new FileOutputStream(f2,true);

            //For Overwrite the file.
            OutputStream out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            System.out.println("File copied.");

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
