package com.yuxipacific.aws.emr.samples.helpers.io;

import java.io.UnsupportedEncodingException;
import org.apache.hadoop.io.BytesWritable;

/**
 *
 * @author Xander Kno
 */
public class ImgBytesWritable extends BytesWritable {

    public ImgBytesWritable(byte[] toByteArray) {
        super(toByteArray);
    }

    @Override
    public String toString() {
        String content = null;
        try {
            content = new String(getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported Encoding Exception {" + e.getMessage() + "}");
        }
        return content;
    }
}
