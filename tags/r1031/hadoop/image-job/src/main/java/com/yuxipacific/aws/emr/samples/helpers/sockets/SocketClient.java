package com.yuxipacific.aws.emr.samples.helpers.sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ResourceBundle;

public class SocketClient {

    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("classifier/config");
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 5000;

    public static String sendAndReciveData(String message) throws IOException, UnknownHostException {
        if ((message == null) || (message.trim().isEmpty())) {
            System.err.println("No data provided to send to the socket.");
            return null;
        }
        Socket s = new Socket();
        String strPort = systemConfig.getString("classifier.socket.server.port");
        int port;
        try {
            port = Integer.parseInt(strPort);
        } catch (NumberFormatException e) {
            System.err.println("Port Config Not Found.");
            port = DEFAULT_PORT;
        }
        PrintWriter _out = null;
        BufferedReader _in = null;
        String host = systemConfig.getString("classifier.socket.server.ip");
        if ((host == null) || (host.trim().isEmpty())) {
            System.err.println("Hostname Config Not Found.");
            host = DEFAULT_HOST;
        }
        try {
            s.connect(new InetSocketAddress(host, port));
            System.out.println("*****Client*****");
            System.out.println("Connected");
            _out = new PrintWriter(s.getOutputStream(), true);
            _in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't Know about host: " + host);
            throw e;
        }

        _out.println(message);
        s.shutdownOutput();
        System.out.println("message sent...\n");
        StringBuilder response = new StringBuilder();
        String partialResult;
        while ((partialResult = _in.readLine()) != null) {
            response.append(partialResult);
            response.append("\n");
        }

        _out.close();
        _in.close();
        s.close();
        System.out.println("Socket Closed...");
        return response.toString();
    }
}