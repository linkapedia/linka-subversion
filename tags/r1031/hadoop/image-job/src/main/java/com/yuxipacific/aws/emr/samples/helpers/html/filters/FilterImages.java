/**
 * @author andres
 * @verion 0.1 BETA
 */
package com.yuxipacific.aws.emr.samples.helpers.html.filters;

public class FilterImages implements Cloneable {

    private double better_ratiox;
    private double better_ratioy;
    private int max_height;
    private int min_height;
    private int max_width;
    private int min_width;

    public FilterImages() {
        better_ratiox = 0;
        better_ratioy = 0;
        max_height = 0;
        min_height = 0;
        max_width = 0;
        min_width = 0;
    }

    public double getBetter_ratiox() {
        return better_ratiox;
    }

    public void setBetter_ratiox(int better_ratiox) {
        this.better_ratiox = better_ratiox;
    }

    public double getBetter_ratioy() {
        return better_ratioy;
    }

    public void setBetter_ratioy(int better_ratioy) {
        this.better_ratioy = better_ratioy;
    }

    public int getMax_height() {
        return max_height;
    }

    public void setMax_height(int max_height) {
        this.max_height = max_height;
    }

    public int getMin_height() {
        return min_height;
    }

    public void setMin_height(int min_height) {
        this.min_height = min_height;
    }

    public int getMax_width() {
        return max_width;
    }

    public void setMax_width(int max_width) {
        this.max_width = max_width;
    }

    public int getMin_width() {
        return min_width;
    }

    public void setMin_width(int min_width) {
        this.min_width = min_width;
    }
}
