/**
 * @author andres
 * @verion 0.1 BETA
 */
package com.yuxipacific.aws.emr.samples.helpers.io;

import com.sun.image.codec.jpeg.ImageFormatException;
import com.yuxipacific.aws.emr.samples.helpers.html.beans.ImageInfo;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Locale;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import org.apache.log4j.Logger;

public class UtilsTransform {

    private static final Logger log = Logger.getLogger(UtilsTransform.class);
    private static BufferedImage transparent = null;
    private static int BOXH = 200;
    private static int BOXW = 360;

    static {
        /*
         * URL url = UtilsTransform.class.getClassLoader().getResource(
         * "images/background.png"); File f; try { f = new File(url.toURI());
         * transparent = ImageIO.read(f); } catch (Exception e) {
         * LOG.error("Error getting transparent image"); }
         */
        try {
            InputStream is = UtilsTransform.class.getClassLoader()
                    .getResourceAsStream("images/background.png");
            transparent = ImageIO.read(is);
            if (transparent != null) {
                log.info("Get transparent Image ok!");
            } else {
                log.info("Not get transparent Image!");
            }
        } catch (Exception e) {
            log.error("Error getting transparent image");
        }
    }

    /**
     * method to set transparent one image
     * <p/>
     * @param image
     * @return
     */
    public static BufferedImage createLayerTransparent(BufferedImage image) {
        if (transparent == null) {
            log.info("The transparent image = null");
            return null;
        }
        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage imageComposite = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = imageComposite.getGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.drawImage(transparent, -6, -6, width + 10, height + 10, null);
        return imageComposite;
    }

    /**
     * return a resize image
     * <p/>
     * @param image
     * @param height
     * @param width
     * @param secureScale
     * @return
     */
    public static BufferedImage scaleImage(BufferedImage image, int height,
            int width, boolean correctScale) {
        if (image == null) {
            log.error("scaleImage() the image is null");
            return null;
        }
        BufferedImage imageR = null;

        int imgWidth = image.getWidth();
        int imgHeight = image.getHeight();
        // scale image if is possible
        if (correctScale) {
            if (imgWidth * height < imgHeight * width) {
                width = imgWidth * height / imgHeight;
            } else {
                height = imgHeight * width / imgWidth;
            }
        }
        // create new image with the new height and width
        imageR = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = imageR.createGraphics();
        try {
            // set parameters
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g.clearRect(0, 0, width, height);
            g.drawImage(image, 0, 0, width, height, null);
        } finally {
            g.dispose();
        }
        return imageR;
    }

    /**
     * paint image1 over image
     * <p/>
     * @param image
     * @param image1
     * @param height
     * @param width
     * @param posx
     * @param posy
     * @return
     */
    public static BufferedImage createCompositeImage(BufferedImage image,
            BufferedImage image1, int height, int width, int posx, int posy) {
        BufferedImage imageR = null;
        imageR = image;
        Graphics2D g = (Graphics2D) imageR.getGraphics();
        g.drawImage(image1, posx, posy, width, height, null);
        return imageR;
    }

    /**
     * paint image1 over image (paint complete layer image)
     * <p/>
     * @param image
     * @param image1
     * @return
     */
    public static BufferedImage createCompositeImage(BufferedImage image,
            BufferedImage image1) {
        if ((image == null) || (image1 == null)) {
            log.error("createComposite(BufferedImage, BufferedImage) Some images are null");
            return null;
        }
        return createCompositeImage(image, image1, image1.getHeight(),
                image1.getWidth(), 0, 0);
    }

    /**
     * create the image in a jpg format
     * <p/>
     * @param image
     * @param quality
     * @throws ImageFormatException
     * @throws IOException
     */
    public static BufferedImage convertToJPG(BufferedImage image, float quality) throws ImageFormatException, IOException {
        log.debug("UtilsTransform: convertToJPG(BufferedImage, float)");
        ByteArrayOutputStream bao = new ByteArrayOutputStream();

        // remove transparency
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
                image.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufferedImage.createGraphics();
        // Color.WHITE estes the background to white. You can use any other
        // color
        g.drawImage(image, 0, 0, bufferedImage.getWidth(),
                bufferedImage.getHeight(), Color.WHITE, null);

        ImageWriter writer = null;
        Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
        if (iter.hasNext()) {
            writer = (ImageWriter) iter.next();
        }
        ImageOutputStream ios = ImageIO.createImageOutputStream(bao);
        writer.setOutput(ios);
        ImageWriteParam iwparam = new JPEGImageWriteParam(Locale.getDefault());
        iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwparam.setCompressionQuality(quality);

        writer.write(null, new IIOImage(bufferedImage, null, null), iwparam);
        ios.flush();
        BufferedImage imageR = ImageIO.read(new ByteArrayInputStream(bao
                .toByteArray()));
        writer.dispose();
        ios.close();
        return imageR;
    }

    /**
     *
     * @param image
     * @return
     * @throws ImageFormatException
     * @throws IOException
     */
    public static BufferedImage convertToJPG(BufferedImage image)
            throws ImageFormatException, IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();

        // remove transparency
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
                image.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufferedImage.createGraphics();
        // Color.WHITE the background to white. You can use any other
        // color
        g.drawImage(image, 0, 0, bufferedImage.getWidth(),
                bufferedImage.getHeight(), Color.WHITE, null);

        ImageIO.write(bufferedImage, "jpg", bao);
        BufferedImage imageR = ImageIO.read(new ByteArrayInputStream(bao
                .toByteArray()));
        return imageR;
    }

    /**
     * create the image in a png format
     * <p/>
     * @param image
     * @throws IOException
     */
    public static BufferedImage convertToPNG(BufferedImage image)
            throws IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ImageIO.write(image, "png", bao);
        BufferedImage imageR = ImageIO.read(new ByteArrayInputStream(bao
                .toByteArray()));
        return imageR;
    }

    public ImageInfo addBackgroundToImage(ImageInfo imageInfo) {
        BufferedImage background = UtilsTransform.scaleImage(imageInfo.getImage(), BOXH, BOXW, false);
        background = UtilsTransform.createLayerTransparent(background);

        BufferedImage mini = null;
        if ((imageInfo.getImage().getHeight() <= BOXH) && (imageInfo.getImage().getWidth() <= BOXW)) {
            mini = imageInfo.getImage();
        } else {
            mini = UtilsTransform.scaleImage(imageInfo.getImage(), BOXH, BOXW, true);
        }
        background = UtilsTransform.createCompositeImage(background, mini, mini.getHeight(), mini.getWidth(), (BOXW - mini.getWidth()) / 2, (BOXH - mini.getHeight()) / 2);
        imageInfo.setImage(background);
        return imageInfo;
    }
}
