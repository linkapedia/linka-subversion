package com.yuxipacific.aws.emr.samples.helpers.reverb;

import com.yuxipacific.utils.StringUtils;
import edu.washington.cs.knowitall.extractor.ReVerbExtractor;
import edu.washington.cs.knowitall.extractor.conf.ConfidenceFunction;
import edu.washington.cs.knowitall.extractor.conf.ReVerbOpenNlpConfFunction;
import edu.washington.cs.knowitall.nlp.ChunkedSentence;
import edu.washington.cs.knowitall.nlp.ChunkedSentenceReader;
import edu.washington.cs.knowitall.nlp.extraction.ChunkedBinaryExtraction;
import edu.washington.cs.knowitall.util.DefaultObjects;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class SentenceExtractorProxy {

    private static final Logger log = Logger.getLogger(SentenceExtractorProxy.class);
    private static final double THRESHOLD_START = 0.6;

    /**
     * Method to handle the Sentence Extraction from a given text.
     * <p/>
     * @param content File content to extract the sentence from.
     * @return Sentences extracted from the given text.
     * @throws Exception
     */
    public static String extractFrom(String content, boolean isHTML) throws Exception {
        log.debug("extractFrom(String)");
        if (content == null || content.isEmpty()) {
            log.error("Cannot execute the extraction without content.");
            return null;
        }
        StringBuilder fileExtractedContent = new StringBuilder();
        double conf = 0.0;
        Iterable<ChunkedBinaryExtraction> extractions = null;
        Map<String, Double> sentences = new LinkedHashMap<String, Double>();
        try {
            InputStream in = new ByteArrayInputStream(content.getBytes("UTF-8"));
            InputStreamReader isr = new InputStreamReader(in);

            log.debug("Initializing NLP tools...");
            ChunkedSentenceReader sentReader = null;
            sentReader = DefaultObjects.getDefaultSentenceReader(isr, isHTML);
            log.debug("Done.");

            try {
                ReVerbExtractor reverb = new ReVerbExtractor();
                ConfidenceFunction confFunc = new ReVerbOpenNlpConfFunction();
                String sentString;
                for (ChunkedSentence sent : sentReader.getSentences()) {
                    sentString = sent.getTokensAsString();
                    extractions = reverb.extract(sent);
                    if (extractions != null && extractions.iterator() != null && extractions.iterator().hasNext()) {
                        for (ChunkedBinaryExtraction extr : extractions) {
                            conf = confFunc.getConf(extr);
                            if (conf < 0.0) {
                                conf = -1f;
                            }
                            if (conf > THRESHOLD_START && conf <= 1) {
                                log.debug("Sentence: " + sentString + "\t{Confidence: " + conf + "}");
                                log.debug("Fixing spaces.");
                                sentences.put(StringUtils.escapeString(sentString), conf);
                            }
                        }
                    }
                }
                //Remove duplicated
                for (String sentence : sentences.keySet()) {
                    fileExtractedContent.append(sentence).append("\n");
                }
            } catch (StringIndexOutOfBoundsException e) {
                log.error("An exception has ocurred: The end of the content has been reached.");
            }
        } catch (Exception e) {
            log.error("An exception has ocurred: ", e);
        }
        return fileExtractedContent.toString();
    }
}
