package com.linkapedia.mapreducejobs;

import com.linkapedia.mapreducejobs.mapper.Map;
import com.linkapedia.mapreducejobs.reducer.ReducerSentences;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 * Start application
 *
 */
public class SentencesJob {

    public static void main(String[] args)
            throws Exception {
        Configuration conf = new Configuration();
//        conf.setBoolean("mapred.task.profile", true);
//        conf.set("mapred.task.profile.params", "-agentlib:hprof=cpu=samples,"
//                + "heap=sites,depth=6,force=n,thread=y,verbose=n,file=%s");
//        conf.set("mapred.task.profile.maps", "0-2");
//        conf.set("mapred.task.profile.reduces", "0-3");

        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length != 3) {
            System.err.println("Usage: SentenceJob <in> <out>");
            System.exit(2);
        }
        Job job = new Job(conf, "SentenceJob");
        job.setJarByClass(SentencesJob.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(Map.class);
        job.setReducerClass(ReducerSentences.class);

        FileInputFormat.addInputPath(job, new Path(otherArgs[1]));
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[2]));
        job.waitForCompletion(true);
    }
}