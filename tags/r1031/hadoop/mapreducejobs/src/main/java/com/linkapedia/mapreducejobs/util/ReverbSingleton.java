/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import edu.washington.cs.knowitall.extractor.ReVerbExtractor;

/**
 *
 * @author andres
 */
public class ReverbSingleton {
    
    private static  ReVerbExtractor uniqueReverb;
    private ReverbSingleton(){};
    
    public static ReVerbExtractor getInstance(){
        
        if(uniqueReverb == null){
            uniqueReverb =  new ReVerbExtractor();
        }
        
        return uniqueReverb;
        
    }
    
}
