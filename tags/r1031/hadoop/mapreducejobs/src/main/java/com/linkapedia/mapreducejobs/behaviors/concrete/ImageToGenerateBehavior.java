package com.linkapedia.mapreducejobs.behaviors.concrete;

import com.linkapedia.mapreducejobs.behaviors.interfaces.IImageProcess;
import java.net.URL;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.filters.FilterImages;
import org.linkapedia.images.filters.FilterResources;
import org.linkapedia.images.filters.config.DefaultFilterImages;
import org.linkapedia.images.filters.config.DefaultFilterResources;
import org.linkapedia.images.resources.ResourceHtml;
import org.linkapedia.images.wrapper.PackImages;
import org.linkapedia.images.bo.ImageSelection;

/**
 *
 * @author andres
 */
public class ImageToGenerateBehavior implements IImageProcess {

    public ImageInfo generateImage(String key) {
        PackImages list = null;
        ImageInfo ii = null;
        FilterImages filter = new DefaultFilterImages();
        FilterResources pfWeb = new DefaultFilterResources();
        try {
            URL url = new URL(key);
            list = ResourceHtml.getHtmlImages(url, pfWeb);
        } catch (Exception e1) {
            System.out.println("Error getting images from url: " + key + " " + e1.getMessage());
        }

        try {
            ii = ImageSelection.getBetterImageFilter(list, filter);
        } catch (Exception e) {
            System.out.println("Error getting better image: " + e.getMessage());
        }
        return ii;
    }
}
