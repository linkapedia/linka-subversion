package com.linkapedia.mapreducejobs;

import com.linkapedia.mapreducejobs.mapper.Map;
import com.linkapedia.mapreducejobs.reducer.ReducerImages;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 *
 * @author andres
 */
public class ImagesJob {

    public static void main(String[] args) {
        Configuration conf = new Configuration();

        String[] otherArgs = null;
        try {
            otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        } catch (IOException ex) {
            System.err.println("Error parsing parameters: " + ex.getMessage());
        }
        if (otherArgs.length != 3) {
            System.err.println("Usage: ImageJob <in> <out>");
            System.exit(2);
        }
        Job job = null;
        try {
            job = new Job(conf, "ImageJob");
        } catch (IOException ex) {
            System.err.println("Error creating job: " + ex.getMessage());
            return;
        }
        job.setJarByClass(ImagesJob.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(BytesWritable.class);

        job.setMapperClass(Map.class);
        job.setReducerClass(ReducerImages.class);
        try {
            FileInputFormat.addInputPath(job, new Path(otherArgs[1]));
            FileOutputFormat.setOutputPath(job, new Path(otherArgs[2]));
        } catch (IOException ex) {
            System.err.println("Error FileInputFormat: " + ex.getMessage());
        }
        
        try {
            //MultipleOutputs.addNamedOutput(job, "seq", SequenceFileOutputFormat.class, NullWritable.class, ImgBytesWritable.class);

            job.waitForCompletion(true);
        } catch (IOException ex) {
            System.err.println("Error executing job: " + ex.getMessage());
        } catch (InterruptedException ex) {
            System.err.println("Error executing job: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.err.println("Error executing job: " + ex.getMessage());
        }
    }
}