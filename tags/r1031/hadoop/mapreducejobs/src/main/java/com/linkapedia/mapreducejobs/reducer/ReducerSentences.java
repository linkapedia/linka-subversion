package com.linkapedia.mapreducejobs.reducer;

import com.linkapedia.mapreducejobs.beans.ClassifierInfo;
import com.linkapedia.mapreducejobs.behaviors.concrete.SentenceRenditionBehavior;
import com.linkapedia.mapreducejobs.behaviors.concrete.SentenceExtractorBehavior;
import com.linkapedia.mapreducejobs.output.OutputTask;
import com.linkapedia.mapreducejobs.output.SentenceExtractionProcess;
import com.linkapedia.mapreducejobs.util.StoreUtil;
import java.io.IOException;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author andres
 */
public class ReducerSentences extends Reducer<Text, Text, NullWritable, Text> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        System.out.println("Inside the reducer.");
        OutputTask sentenceTask = new SentenceExtractionProcess();
        for (Text val : values) {
            ClassifierInfo classifierSetences = null;
            ClassifierInfo renditionSentences = null;
            String html = val.toString();
            try {
                sentenceTask.setSentencesBehavior(new SentenceExtractorBehavior());
                classifierSetences = sentenceTask.generateSentences(html);
                if (classifierSetences != null && (!"".equals(classifierSetences.getSentences()) && classifierSetences.getSentences() != null) && !"".equals(classifierSetences.getTitle()) && classifierSetences.getclassifierScores() != null) {
                    //StoreUtil.storeDynamoNodeDocumentTemp(classifierSetences.getclassifierScores(), key.toString(), classifierSetences.getTitle());
                    StoreUtil.storeDynamoNodeDocument(classifierSetences.getclassifierScores(), key.toString(), classifierSetences.getTitle());
                    //StoreUtil.storeDynamoDocumentNode(classifierSetences.getclassifierScores(), key.toString(), classifierSetences.getTitle());
                    StoreUtil.storeDynamoDoc_Node(classifierSetences.getclassifierScores(), key.toString(), classifierSetences.getTitle());
                    StoreUtil.storeSentencesS3(classifierSetences.getSentences(), key.toString());
                }
            } catch (Exception ex) {
                System.out.println("Error in the whole process...");
            }
            try {
                sentenceTask.setSentencesBehavior(new SentenceRenditionBehavior());
                renditionSentences = sentenceTask.generateSentences(html);
                if (renditionSentences != null && !"".equals(renditionSentences.getSentences()) && renditionSentences.getSentences() != null) {
                    StoreUtil.storeSentencesRenditionS3(renditionSentences.getSentences(), key.toString());
                }
            } catch (Exception ex) {
            }
        }
    }
}
