/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.local.test;

import com.linkapedia.mapreducejobs.beans.ClassifierInfo;
import com.linkapedia.mapreducejobs.output.OutputTask;
import com.linkapedia.mapreducejobs.output.SentenceExtractionProcess;
import com.linkapedia.mapreducejobs.util.BringFilesFromS3;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import org.jsoup.nodes.Document;
/**
 *
 * @author Sahar Ebadi
 */
public class TitleExtraction {
    public static void main(String args[]){
        System.out.println("Testing Behaviors...");
        getTitle();
    }
    public static void getTitle() {
        String inputPath = "c://titleextraction/classrun1.txt";
//            System.out.println(".txt file path: "+inputPath);
        BringFilesFromS3 bfs = new BringFilesFromS3();
        HashMap<String, byte[]> results;
        File file = new File(inputPath);
        String docsToGenerated = getContentFromFile(file);
        Document doc = null;
        OutputTask sentenceTask = new SentenceExtractionProcess();
        ClassifierInfo classifierSetences;
        
//            try {
//            doc = Jsoup.connect("http://www.w3schools.com/").get();
//            } catch (IOException ex) 
//                Logger.getLogger(TitleExtraction.class.getName()).log(Level.SEVERE, null, ex);
//            }
        //list of the name of 80leg files
        String[] legs = docsToGenerated.split("\n");
        // for each 80leg.zip file do the following
        for (int i = 0; i < legs.length; i++) {
            try {
                results = bfs.getZipContentFromS3(legs[i]);
                if (results != null) {
                    Set<String> keySet = results.keySet();
                    for (String _key : keySet) {
                        if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                            String content = new String((byte[]) results.get(_key), "UTF-8");
                            if ((content != null) && (!content.trim().isEmpty())) {
    //                            doc = Jsoup.connect(results.get(_key).toString()).get();// uses URL to get the HTML
                                System.out.println("__________________ "+_key+"________________________");
//                                    System.out.println("content: ");
//                                    System.out.println(content);
//                                sentenceTask.setSentencesBehavior(new sentenceAndTitleExtractorBehaviour());
                                classifierSetences = sentenceTask.generateSentences(content);
                                if (classifierSetences != null && (!"".equals(classifierSetences.getTitle()) && classifierSetences.getTitle() != null)) {
                                    System.out.println("Final title from test: "+classifierSetences.getTitle());
                                }else if(classifierSetences!=null ||!classifierSetences.equals("")){
                                    System.out.println("ERRORR! sentences are full");
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
        }
        }
//   System.out.println("empty ones:"+emptyCount+", totalDocs: "+totalDocs);

    }
    public static String getContentFromFile(File file) {
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        DataInputStream dis = null;
        StringBuilder sb = new StringBuilder();

        try {
            fis = new FileInputStream(file);

            // Here BufferedInputStream is added for fast reading.
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);

            // dis.available() returns 0 if the file does not have more lines.

            while (dis.available() != 0) {

                // this statement reads the line from the file and print it to
                // the console.
                //System.out.println(dis.readLine());
                sb.append(dis.readLine());
                sb.append("\n");
            }

            // dispose all the resources after using them.
            fis.close();
            bis.close();
            dis.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
