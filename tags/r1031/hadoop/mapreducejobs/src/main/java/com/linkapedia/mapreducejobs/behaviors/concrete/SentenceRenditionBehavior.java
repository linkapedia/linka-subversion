/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.behaviors.concrete;

import com.linkapedia.mapreducejobs.beans.ClassifierInfo;
import com.linkapedia.mapreducejobs.behaviors.interfaces.ISentencesProcess;
import com.linkapedia.mapreducejobs.util.ParserProxy;
import com.linkapedia.mapreducejobs.util.SentenceExtractorProxy;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andres
 */
public class SentenceRenditionBehavior implements ISentencesProcess {

    public ClassifierInfo generateSentences(String htmlString) {

        ClassifierInfo response = new ClassifierInfo();

        if ((!"".equals(htmlString)) && (htmlString != null)) {
            try {
                StringBuilder sb = new StringBuilder();
                byte[] htmlContent = htmlString.getBytes("UTF-8");
                //String content = ParserProxy.parse(htmlContent);

                sb.append("\n");
                sb.append(" Content: ");

                response.setTitle("Triples");
                System.out.println("Getting Sentences");
                try {
                    String sentenceRendition = SentenceExtractorProxy.extractRendition(htmlString);
                    if ((sentenceRendition != null) && (!sentenceRendition.trim().isEmpty())) {
                        response.setSentences(sentenceRendition);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
                }


            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return response;
    }
}
