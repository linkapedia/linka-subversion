package com.linkapedia.mapreducejobs;


import com.linkapedia.mapreduce.WholeFileInputFormat;
import com.linkapedia.mapreducejobs.mapper.EightyLegMap;
import com.linkapedia.mapreducejobs.reducer.ReducerSentences;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Start application
 *
 */
public class NewSentencesJob extends Configured implements Tool {
    
    @Override
    public int run(String[] args) throws Exception {
        if (args.length !=3) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n",
                    getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }
        Job job = new Job(getConf(), "Sentences job");
        job.setJarByClass(getClass());
        WholeFileInputFormat.addInputPath(job, new Path(args[1]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        job.setInputFormatClass(WholeFileInputFormat.class);
        job.setMapperClass(EightyLegMap.class);
        job.setReducerClass(ReducerSentences.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new NewSentencesJob(), args);
        System.exit(exitCode);
    }
}