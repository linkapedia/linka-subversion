package com.linkapedia.mapreducejobs.output;

import com.linkapedia.mapreducejobs.beans.ClassifierInfo;
import com.linkapedia.mapreducejobs.behaviors.interfaces.IImageProcess;
import com.linkapedia.mapreducejobs.behaviors.interfaces.ISentencesProcess;
import com.linkapedia.mapreducejobs.util.StoreUtil;
import org.linkapedia.images.beans.ImageInfo;


/**
 *
 * @author andres
 */
public abstract class OutputTask {

    IImageProcess imageProcess;
    ISentencesProcess sentenceProcess;

    protected boolean storeLocal(String infoToStore) {
        return StoreUtil.storeLocal(infoToStore);
    }

    public ClassifierInfo generateSentences(String htmlInput) {
        //call behavior method
        return sentenceProcess.generateSentences(htmlInput);


    }

    public ImageInfo generateImages(String key) {
        //call behavior method
        return imageProcess.generateImage(key);
    }

    public void setSentencesBehavior(ISentencesProcess sentenceProcess) {
        this.sentenceProcess = sentenceProcess;
    }

    public void setImagesBehavior(IImageProcess imageProcess) {
        this.imageProcess = imageProcess;
    }
}
