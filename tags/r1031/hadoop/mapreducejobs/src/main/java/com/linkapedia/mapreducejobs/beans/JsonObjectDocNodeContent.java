/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.beans;

import java.util.List;

/**
 *
 * @author andy
 */
public class JsonObjectDocNodeContent {
    private long timeStamp;
    private String url;
    private List<String> nodes;

    /**
     * @return the timeStamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp the timeStamp to set
     */
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the nodeIds
     */
    public List<String> getNodes() {
        return nodes;
    }

    /**
     * @param nodeIds the nodeIds to set
     */
    public void setNodes(List<String> nodeIds) {
        this.nodes = nodeIds;
    }
    
}
