echo after having moved program file tar files (e.g., /src/local/itsharvester and down) 
echo and webapp tar files 
echo emacs /etc/csh.cshrc
echo setenv PATH ${PATH}:/src/java/bin
echo setenv JAVA_HOME /usr/local/java
echo setenv CATALINA_HOME /usr/local/src/tomcathome
echo setenv JAVA_OPTS -Xmx384M


echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo @@@@@@@ START eg 104 AND 115 crawler setup @@@@@@@@@@@
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

echo emacs ~/.cshrc
alias cd1 'cd /usr/local/src/tomcathome/bin'
alias cd2 'cd /usr/local/src/tomcathome/webapps/itsharvester'
alias cd3 'cd /usr/local/src/ITSHarvester'
alias cd5 'cd /usr/local/src/tomcathome/logs'
alias cd6 'cd /src'
alias cd7 'cd /home/admin/src/ITSHarvesterClient/'
alias cd8 'cd /home/admin/fcrawl/documents'
alias tcat 'tail -n1000 -f /usr/local/src/tomcathome/logs/catalina.out'
alias tlog 'tail -n1000 -f /usr/local/src/ITSHarvester/logs/ITSLog.log'
alias txml 'tail -n1000 -f /temp/xml.txt'
alias ttlog 'tail -n1000 -f /usr/local/src/tomcathome/logs/localhost_log.2003-06-01.txt'
alias ecd 'emacs ~/.cshrc'
alias f104 ftp 207.103.213.104


echo log delete  rm -f /usr/local/src/tomcathome/logs/catalina.out
echo log delete  rm -f /usr/local/src/ITSHarvester/logs/ITSLog.log
echo log delete  rm -f /temp/xml.txt
echo log delete  rm -f /usr/local/src/tomcathome/logs/localhost_log.2003-06-05.txt



echo step 1 GET FILES TO TARGET MACHINE
  cp /src/j2sdk-1_4_1_03-linux-i586.bin /usr/local/src
  cp /src/jakarta-tomcat-4.1.24.zip /usr/local/src
  rm -rf /usr/local/src/jakarta-tomcat-4.1.24

echo step 2 INSTALL JAVA
  chmod +x /usr/local/src/j2sdk-1_4_1_03-linux-i586.bin
  cd /usr/local/src
  rm -rf /usr/local/src/j2sdk1.4.1_03
  ./j2sdk-1_4_1_03-linux-i586.bin

echo step 3 INSTALL TOMCAT
  cd /usr/local/src
  rm -rf /usr/local/src/jakarta-tomcat-4.1.24
  unzip /usr/local/src/jakarta-tomcat-4.1.24.zip

echo step 4 make java link
  cd /usr/local
  rm -f /usr/local/java
  ln -s /usr/local/src/j2sdk1.4.1_03 java

  cd /usr/local/bin
  rm -f /usr/local/bin/java
  ln -s /usr/local/java/bin/java java

echo step 5 make tomcathome link
  cd /usr/local/src
  rm -f /usr/local/src/tomcathome
  ln -s /usr/local/src/jakarta-tomcat-4.1.24 tomcathome


echo step 7 PF tar files

  rm -f /usr/local/src/PFITSHarvester.tar
  rm -f /usr/local/src/PFITSHarvesterClient.tar

  cp /src/PFITSHarvester.tar /usr/local/src
  cp /src/PFITSHarvesterClient.tar /usr/local/src

  rm -rf /usr/local/src/ITSHarvester
  rm -rf /usr/local/src/ITSHarvesterClient

  cd /usr/local/src
  tar -xvf PFITSHarvester.tar
  tar -xvf PFITSHarvesterClient.tar

        
echo step 8 WA tar files

  rm -f /usr/local/src/tomcathome/webapps/WAitsharvester.tar 
  rm -f /usr/local/src/tomcathome/webapps/WAitsharvesterclient.tar 

  cp /src/WAitsharvester.tar /usr/local/src/tomcathome/webapps
  cp /src/WAitsharvesterclient.tar /usr/local/src/tomcathome/webapps

  rm -rf /usr/local/src/tomcathome/webapps/itsharvester
  cd /usr/local/src/tomcathome/webapps
  tar -xvf WAitsharvester.tar

  rm -rf /usr/local/src/tomcathome/webapps/itsharvesterclient 
  cd /usr/local/src/tomcathome/webapps
  tar -xvf WAitsharvesterclient.tar


echo step 9 fix web.xml: 
  cd /usr/local/src/tomcathome/webapps/itsharvester/WEB-INF
  mv web.xml web-win.xml
  cp web-linux.xml web.xml

  cd /usr/local/src/tomcathome/webapps/itsharvesterclient/WEB-INF
  mv web.xml web-win.xml
  cp web-linux.xml web.xml

echo step 9.1  DELETE LOG FILES 
  rm /usr/local/src/tomcathome/logs/catalina.out
  rm /usr/local/src/ITSHarvester/logs/ITSLog.log
  rm /temp/xml.txt
  rm /usr/local/src/tomcathome/logs/localhost_log.2003-06-01.txt

echo step 9.2 INDRADEBUG FILES 

  rm /temp/IndraDebugMetaSearchVerbose.txt
  rm /temp/IndraXMLToFile.txt
  rm /temp/IndraXMLToFileArgs.txt
  rm /temp//temp/IndraControlStopMetasearchFeeder.txt
  rm /temp/IndraDebugLeaveClassifyTempFiles.txt

  touch /temp/IndraDebugMetaSearchVerbose.txtOFF
  touch /temp/IndraXMLToFile.txtOFF
  touch /temp/IndraXMLToFileArgs.txtOFF
  touch /temp//temp/IndraControlStopMetasearchFeeder.txtOFF
  touch /temp/IndraDebugLeaveClassifyTempFiles.txt

echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo @@@@@@@  END  eg 104 AND 115 crawler setup @@@@@@@@@@@
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
