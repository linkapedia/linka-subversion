
time /T
rem goto :step9_create_insaller_out_dirs

:step1 build itsapi
call build itsapi
echo "step1 build itsapi"
rem pause

if not exist ""C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar"" ECHO "missing "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar""
if not exist ""C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar"" pause


:step2 build itsclassifier
rem aaa call build itsclassifier
rem aaa echo "step1 build itsclassifier"
rem aaa rem pause

:step3 build javadoc 
call cd1
cd "C:\Documents and Settings\Indraweb\buildprocess\javadocrun"
call javadocs_gen.bat

xcopy "C:\Documents and Settings\Indraweb\buildprocess\javadocoutput" "C:\Program Files\ITS\javadoc" /s
copy "C:\Documents and Settings\Indraweb\buildprocess\javadocinput\index.html" "C:\Program Files\ITS\javadoc" 


:step9_create_insaller_out_dirs 

rmdir /S /Q     "C:\Program Files\install4j\iwprojects\Installer Build"
if exist "C:\Program Files\install4j\iwprojects\Installer Build" ECHO "ERROR removing C:\Program Files\install4j\iwprojects\Installer Build"
if exist "C:\Program Files\install4j\iwprojects\Installer Build" pause
mkdir "C:\Program Files\install4j\iwprojects\Installer Build"

:step10 create installer for cdmsserver
ss get "$/BuildProcesses/install4j/CDMSSERVER.install4j" -GL"C:\Program Files\install4j\iwprojects" -R  -O- -Yhkon,racer9
"C:\Program Files\install4j\bin\install4jc.exe" -v "C:\Program Files\install4j\iwprojects\CDMSSERVER.install4j"

:step11 create installer for cdmsedit 
ss get "$/BuildProcesses/install4j/CDMSEDIT.install4j" -GL"C:\Program Files\install4j\iwprojects" -R  -O- -Yhkon,racer9
"C:\Program Files\install4j\bin\install4jc.exe" -v "C:\Program Files\install4j\iwprojects\CDMSEDIT.install4j"
	
:step12 create installer for cdmsclassifier
rem aaa echo step12 create installer for cdmsclassifier
rem aaa ss get "$/BuildProcesses/install4j/CDMSCLASSIFIER.install4j" -GL"C:\Program Files\install4j\iwprojects" -R  -O- -Yhkon,racer9
rem aaa "C:\Program Files\install4j\bin\install4jc.exe" -v "C:\Program Files\install4j\iwprojects\CDMSCLASSIFIER.install4j"


call cd1

:step20 copycdms

copycdms

rem call build itsharvester
rem call build itsharvesterclient

time /T
echo COMPLETE 
pause
