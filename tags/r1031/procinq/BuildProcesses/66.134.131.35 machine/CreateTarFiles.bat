c:
echo off


rem cd "c:\program files"
rem set FOLDERTOTAR=c:\program files\ITS
rem if not exist %FOLDERTOTAR% echo "ERROR can't tar - %FOLDERTOTAR% missing"
rem if not exist %FOLDERTOTAR% echo goto endprocerror
rem tar -cvf .\ITS.tar ITS
rem move .\ITS.tar  "C:\Documents and Settings\Indraweb\buildprocess\PFtarFiles\PFITS.tar" 


rem ************** harvester
 
cd c:\program files
set FOLDERTOTAR=c:\program files\ITSHarvester
if not exist %FOLDERTOTAR% echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist %FOLDERTOTAR% echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf PFITSHarvester.tar ITSHarvester
copy .\PFITSHarvester.tar  "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\PFITSHarvester.tar" 


set FOLDERTOTAR=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsharvester
cd C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps
if not exist "%FOLDERTOTAR%" echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist "%FOLDERTOTAR%" echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf WAitsharvester.tar itsharvester
move .\WAitsharvester.tar "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\WAitsharvester.tar" 

rem ************** harvesterclient

cd c:\program files
set FOLDERTOTAR=c:\program files\ITSHarvesterClient
if not exist %FOLDERTOTAR% echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist %FOLDERTOTAR% echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf PFITSHarvesterClient.tar ITSHarvesterClient
copy .\PFITSHarvesterClient.tar  "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\PFITSHarvesterClient.tar" 


set FOLDERTOTAR=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsharvesterclient
cd C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps
if not exist "%FOLDERTOTAR%" echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist "%FOLDERTOTAR%" echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf WAitsharvesterclient.tar itsharvesterclient
move .\WAitsharvesterclient.tar "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\WAitsharvesterclient.tar" 

rem ************** ITS/itsapi

cd c:\program files
set FOLDERTOTAR=c:\program files\ITS
if not exist %FOLDERTOTAR% echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist %FOLDERTOTAR% echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf PFITS.tar ITS
copy .\PFITS.tar  "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\PFITS.tar" 

set FOLDERTOTAR=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi
cd C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps
if not exist "%FOLDERTOTAR%" echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist "%FOLDERTOTAR%" echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf WAitsapi.tar itsapi
move .\WAitsapi.tar "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\WAitsapi.tar" 

rem ** ITSAPI servlet ? 
set FOLDERTOTAR=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servletitsapi
cd C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps
if not exist "%FOLDERTOTAR%" echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist "%FOLDERTOTAR%" echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf WAservletitsapi.tar servletitsapi
move .\WAservletitsapi.tar "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\WAservletitsapi.tar" 



rem ************** get images for ROOT
set FOLDERTOTAR=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\ROOT\images
cd C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\ROOT
if not exist "%FOLDERTOTAR%" echo "ERROR can't tar - %FOLDERTOTAR% missing"
if not exist "%FOLDERTOTAR%" echo goto endprocerror
c:\utils\tar-1.12.msdos.exe -cvf images.tar images
move .\images.tar "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\images.tar" 




rem ************** last step - get tar files for xfer 
copy "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\*.tar"  e:\applications\temp
rem (app specific copy) copy "C:\Documents and Settings\Indraweb\buildprocess\TarFilesForLinux\WAItsharvester.tar"  e:\applications\temp


goto endproc

:endprocerror
echo error in proc

pause

:endproc

pause


