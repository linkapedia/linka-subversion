
rem this should be invoked such as : "build itsapi" - needs a build target
rem echo LAST CHANCE TO ABORT THIS BUILD - MAY BE SET TO STOP SERVLET CONTAINERS IF YOU CONTINUE
rem pause
echo off

set FASTBUILD=FALSE

if not "%FASTBUILD%"=="TRUE" goto NOFASTBUILDWARNING
echo DOING FAST BUILD
:NOFASTBUILDWARNING

if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING
echo DOING FULL BUILD
:NOFULLBUILDWARNING


time /T	
set ARG1=%1



rem ********* determine ITS home dir for this build

set BUILDPROCESS=C:\Documents and Settings\Indraweb\buildprocess


set WEBAPPSHOME=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps

if "%ARG1%"=="itsapi"             set SRVLTHM_SVL=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet%ARG1%
if "%ARG1%"=="itsharvesterclient" set SRVLTHM_SVL=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet%ARG1%
if "%ARG1%"=="itsclassifier"      set SRVLTHM_SVL=
if "%ARG1%"=="itsharvester"       set SRVLTHM_SVL=

if "%ARG1%"=="itsapi" set ITSHOME=c:\program files\ITS
if "%ARG1%"=="itsapi" set WEBAPPS_ITS_BUILDSPECIFIC=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi
if "%ARG1%"=="itsapi" goto STARTBUILD

if "%ARG1%"=="itsclassifier" 	set ITSHOME=c:\program files\ITSClassifier
if "%ARG1%"=="itsclassifier" 	set WEBAPPS_ITS_BUILDSPECIFIC=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsclassifier
if "%ARG1%"=="itsclassifier" 	goto STARTBUILD

if "%ARG1%"=="itsharvester" 	set ITSHOME=c:\program files\ITSHarvester
if "%ARG1%"=="itsharvester" 	set WEBAPPS_ITS_BUILDSPECIFIC=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsharvester
if "%ARG1%"=="itsharvester" 	goto STARTBUILD

if "%ARG1%"=="itsharvesterclient" 	set ITSHOME=c:\program Files\ITSHarvesterClient
if "%ARG1%"=="itsharvesterclient" 	set WEBAPPS_ITS_BUILDSPECIFIC=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsharvesterclient
if "%ARG1%"=="itsharvesterclient" 	goto STARTBUILD

echo NOTICE: FIRST argument must be (case included) itsapi OR itsclassifier OR itsharvester OR itsharvesterclient Stopping Build
goto ENDPROCERROR

:STARTBUILD
rem goto BUILD_STEP13.3
rem goto BUILD_STEP18.2
rem echo on
rem goto BUILD_STEPEIGHTEEN
rem goto BUILDJAVADOC
echo stop hk3


echo "ARG1 is %ARG1%"
echo "ITSHOME is [%ITSHOME%]"
echo "WEBAPPS_ITS_BUILDSPECIFIC is [%WEBAPPS_ITS_BUILDSPECIFIC%]"
echo "SRVLTHM_SVL is [%SRVLTHM_SVL%]"



rem ************ make sure can manipulate and write required dir's 





echo BUILD_STEP 1 DELETING OLD SERVLET CONTAINERS DIR 
rmdir /S /Q    "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes" ECHO "ERROR removing %WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes" goto ENDPROCERROR
mkdir "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"
rmdir /S /Q    "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib" ECHO "ERROR removing %WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib" goto ENDPROCERROR
mkdir "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib"

if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_NoDel1
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_NoDel1
  rmdir /S /Q    "%SRVLTHM_SVL%\WEB-INF\classes"
  if exist "%SRVLTHM_SVL%\WEB-INF\classes" ECHO "ERROR removing %SRVLTHM_SVL%\WEB-INF\classes"
  if exist "%SRVLTHM_SVL%\WEB-INF\classes" goto ENDPROCERROR
  mkdir "%SRVLTHM_SVL%\WEB-INF\classes"
  rmdir /S /Q    "%SRVLTHM_SVL%\WEB-INF\lib"
  if exist "%SRVLTHM_SVL%\WEB-INF\lib" ECHO "ERROR removing %SRVLTHM_SVL%\WEB-INF\lib"
  if exist "%SRVLTHM_SVL%\WEB-INF\lib" goto ENDPROCERROR
  mkdir "%SRVLTHM_SVL%\WEB-INF\lib"
:SKIPNOSERVLETUIDIR_NoDel1


echo NOTE: SERVLET CONTAINERS LIKE TOMCAT AND JRUN SHOULD BE SHUT DOWN BEFORE CONTINUING !!!  


cd "%BUILDPROCESS%"


echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ START BUILD
if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipLogAndSigFilesBackup

ECHO BUILD_STEP 2 BACKUP SIGNATURE FILES 
rmdir  /Q /S "C:\temp\corpussource"
if exist "C:\temp\corpussource" ECHO "ERROR removing C:\temp\corpussource"
if exist "C:\temp\corpussource" goto ENDPROCERROR
mkdir        "C:\temp\corpussource"
xcopy         "%ITSHOME%\corpussource\*.*" "C:\temp\corpussource" /s


ECHO BUILD_STEP 3 BACKUP LOG FILES 
rem save log files for later in build restore to dirs (NOT FOR PRODUCTION BUILD)
rmdir  /Q /S "C:\temp\logs"
if exist "C:\temp\logs" ECHO "ERROR removing C:\temp\logs"
if exist "C:\temp\logs" goto ENDPROCERROR
mkdir        "C:\temp\logs"
copy         "%ITSHOME%\logs\*.*" "C:\temp\logs\"
:NOFULLBUILDWARNING_skipLogAndSigFilesBackup

rem echo "ITSAPPHOME is [%ITSHOME%]"

ECHO BUILD_STEP 4 CREATE HOME DIR STRUCTURE "%ITSHOME%"
rem ITS HOME (subdirs in alphabetical order)
if exist "%ITSHOME%" rmdir  /Q /S "%ITSHOME%"
if exist "%ITSHOME%" ECHO "ERROR removing %ITSHOME%"
if exist "%ITSHOME%" goto ENDPROCERROR

mkdir        "%ITSHOME%"
mkdir        "%ITSHOME%\bin"
mkdir        "%ITSHOME%\classifyfiles"	
mkdir        "%ITSHOME%\temp"


ECHO BUILD_STEP 5 GET NON CLASS FILES (commonwords, ITSLogConfig_%ARG1%.txt) FROM SSAFE THRU BUILDPROCESS
rem **** root files thru build process folder (due to multiple target homes)
if exist "%BUILDPROCESS%\commonwords.txt" del /F /Q "%BUILDPROCESS%\commonwords.txt"
if exist "%BUILDPROCESS%\ITSLogConfig_%ARG1%.txt" del /F /Q "%BUILDPROCESS%\ITSLogConfig_%ARG1%*.txt"
ss get "$/ITSHome/commonwords.txt" -GL"%BUILDPROCESS%" -R  -O- -Yhkon,racer9
ss get "$/BuildProcesses/LogSpecs/ITSLogConfig_%ARG1%*.txt" -GL"%BUILDPROCESS%/LogSpecs"  -R  -O- -Yhkon,racer9
copy "%BUILDPROCESS%\commonwords.txt" "%ITSHOME%" /Y
copy "%BUILDPROCESS%\LogSpecs\ITSLogConfig_%ARG1%.txt" "%ITSHOME%\ITSLogConfig.txt" /Y
copy "%BUILDPROCESS%\LogSpecs\ITSLogConfig_%ARG1%-linux.txt" "%ITSHOME%\ITSLogConfig-linux.txt" /Y

rem BUILD SPECIFIC - its folder contents

rem not doing corpus perl programs as of 2004 07 
rem if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skip5.1
rem   ECHO BUILD_STEP 5.1 CORPUS-IMPORT thru build process folder
rem   rmdir  /Q /S "%BUILDPROCESS%\corpus-import"
rem   if exist "%BUILDPROCESS%\corpus-import" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\corpus-import"
rem   if exist "%BUILDPROCESS%\corpus-import" goto ENDPROCERROR
rem   mkdir        "%BUILDPROCESS%\corpus-import"
rem   ss get "$/corpus" -GL"%BUILDPROCESS%\corpus-import"  -R  -O- -Yhkon,racer9
rem   mkdir        "%ITSHOME%\corpus-import"
rem   xcopy "%BUILDPROCESS%\corpus-import\*.*" "%ITSHOME%\corpus-import" /Y /S /Q
rem :NOFULLBUILDWARNING_skip5.1

if not "%ARG1%"=="itsapi" goto SKIP_NONITSAPI_1

  ECHO BUILD_STEP 5.2 DOCUMENTATION thru build process folder
  rmdir  /Q /S "%BUILDPROCESS%\doc"
  mkdir        "%BUILDPROCESS%\doc"
  ss get "$/ITS Documentation/*.pdf" -GL"%BUILDPROCESS%\doc"  -R  -O- -Yhkon,racer9
  mkdir        "%ITSHOME%\doc"
  xcopy "%BUILDPROCESS%\doc\*.*" "%ITSHOME%\doc\" /Y /S /Q

  ECHO BUILD_STEP 5.3 PERL thru build process folder
  rmdir  /Q /S "%BUILDPROCESS%\PERL"
  if exist "%BUILDPROCESS%\PERL" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\PERL"
  if exist "%BUILDPROCESS%\PERL" goto ENDPROCERROR
  mkdir        "%BUILDPROCESS%\PERL"
  ss get "$/Perl" -GL"%BUILDPROCESS%\PERL"  -R  -O- -Yhkon,racer9
  mkdir        "%ITSHOME%\PERL"
  xcopy "%BUILDPROCESS%\PERL\*.*" "%ITSHOME%\PERL\" /Y /S /Q

  ECHO BUILD_STEP 5.4 ROCCH thru build process folder
  rem del /F /Q /S "%BUILDPROCESS%\ROCCH\*.*"
  rmdir  /Q /S "%BUILDPROCESS%\ROCCH"
  if exist "%BUILDPROCESS%\ROCCH" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\ROCCH"
  if exist "%BUILDPROCESS%\ROCCH" goto ENDPROCERROR
  mkdir        "%BUILDPROCESS%\ROCCH"
  ss get "$/ROCCH/Makefile" -GL"%BUILDPROCESS%\ROCCH"  -R  -O- -Yhkon,racer9
  ss get "$/ROCCH/Readme" -GL"%BUILDPROCESS%\ROCCH"  -R   -O- -Yhkon,racer9
  ss get "$/ROCCH/ROCCH" -GL"%BUILDPROCESS%\ROCCH"  -R   -O- -Yhkon,racer9
  mkdir        "%ITSHOME%\ROCCH"
  xcopy "%BUILDPROCESS%\ROCCH\*.*" "%ITSHOME%\ROCCH" /Y /S /Q

:SKIP_NONITSAPI_1


if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skip6_templates
  rem BUILD SPECIFIC - templates
  ECHO BUILD_STEP 6 GET TEMPLATES FROM SSAFE THRU BUILDPROCESS
  rem BUILD SPECIFIC - TEMPLATES 
  rem **** Templates thru build process folder
  rmdir  /Q /S "%BUILDPROCESS%\templates"
  if exist "%BUILDPROCESS%\templates" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\templates"
  if exist "%BUILDPROCESS%\templates" goto ENDPROCERROR
  mkdir        "%BUILDPROCESS%\templates"
  ss get "$/templates" -GL"%BUILDPROCESS%\templates"  -R   -O- -Yhkon,racer9
  mkdir        "%ITSHOME%\templates"


  rem **** get Templates/notification to build process folder also
  mkdir        "%BUILDPROCESS%\templates\notification"
  ss get "$/Java2/Notification/*.tpl"  -GL"%BUILDPROCESS%\templates\notification"  -R  -O- -Yhkon,racer9
  xcopy "%BUILDPROCESS%\templates\*.*" "%ITSHOME%\templates" /Y /S  /Q
:NOFULLBUILDWARNING_skip6_templates


if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skip6.1
  ECHO BUILD_STEP 6.1 word frequencies thru build process folder
  rmdir  /Q /S "%BUILDPROCESS%\WordFrequencies"
  mkdir        "%BUILDPROCESS%\WordFrequencies"
  ss get "$/ITSHome/WordFrequencies" -GL"%BUILDPROCESS%\WordFrequencies"  -R  -O- -Yhkon,racer9
  mkdir        "%ITSHOME%\WordFrequencies"
  xcopy "%BUILDPROCESS%\WordFrequencies\*.*" "%ITSHOME%\WordFrequencies" /Y   /Q
:NOFULLBUILDWARNING_skip6.1


rem **** create required folders for runtime
mkdir        "%ITSHOME%\CorpusSource"
mkdir        "%ITSHOME%\logs"



if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipJavaEnvironmentJars
  rem BUILD SPECIFIC - external jar files 
  ECHO BUILD_STEP 7 GET JAVA ENVIRONMENT JAR FILES FROM SSAFE THRU BUILDPROCESS
  rem *********************************** java environment (zip files)
  rmdir /Q /S "%BUILDPROCESS%\java environment"
  if exist "%BUILDPROCESS%\java environment" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\java environment"
  if exist "%BUILDPROCESS%\java environment" goto ENDPROCERROR
  mkdir       "%BUILDPROCESS%\java environment"


ECHO BUILD_STEP 7.1 CREATE FOLDER : WEBAPPS_ITS_BUILDSPECIFIC LIB 
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib" rmdir /Q /S    "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib" ECHO "ERROR removing "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib" goto ENDPROCERROR
mkdir          "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib"

if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_noservletlib
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_noservletlib
  ECHO BUILD_STEP 7.2 CREATE FOLDER : SERVLETHOME-servlet LIB 
  rmdir /Q /S    "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib"
  if exist "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib" ECHO "ERROR removing C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet\WEB-INF\lib"
  if exist "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib" goto ENDPROCERROR
  mkdir          "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib"
:SKIPNOSERVLETUIDIR_noservletlib

ECHO BUILD_STEP 7.3 ************** GET JAR FILES PER BUILD TYPE
rem BUILD SPECIFIC - which jar files needed 


ss get "$/Java2/java environment/gnu-regexp-1.1.4.jar" -GL"%BUILDPROCESS%\java environment" -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/ms.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/oreilly.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/log4j-1.2.8.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/runtime12.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/DbConnectionBroker1.0.11.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/classes12.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/jfreechart-0.9.17.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/jcommon-0.9.2.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/dom4j.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/jh.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/PDFBox.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/xercesImpl.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/alloy.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/jcs-SNAPSHOT.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/commons-logging.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/tm-extractors-0.4.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/junit/aspectjrt-1.1.1.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/junit/cactus-1.6.1.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/junit/commons-httpclient-2.0.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/junit/httpunit-1.5.4.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/junit/junit-3.8.1.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/jce1_2_1.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/jcifs-1.1.6.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/xtrim-api.jar" -GL"%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/lingpipe-2.3.0.jar" -GL "%BUILDPROCESS%\java environment"   -R  -O- -Yhkon,racer9


if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_nocopy
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_nocopy
  xcopy "%BUILDPROCESS%\java environment\*.*" "%SRVLTHM_SVL%\WEB-INF\lib" /Y /Q
:SKIPNOSERVLETUIDIR_nocopy

rem already-copied servlet app needs no db connections or CQL or ftp 
ss get "$/Java2/java environment/FTPEnterprise.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/sql4j.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9
ss get "$/Java2/java environment/OculusLayout.jar" -GL"%BUILDPROCESS%\java environment"  -R  -O- -Yhkon,racer9

xcopy "%BUILDPROCESS%\java environment\*.*" "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib\" /Y /Q
:NOFULLBUILDWARNING_skipJavaEnvironmentJars


:BUILD_STEP8

if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipImages
rem echo on

ECHO BUILD_STEP 8 GET IMAGES DIRECT TO ROOT\images
rem if "%ARG1%"=="itsharvester" goto SKIPIMAGES
if "%ARG1%"=="itsclassifier" goto SKIPIMAGES
  rem *********************************** images
  rmdir /S /Q    "%WEBAPPSHOME%\servlet%ARG1%\images"
  if exist "%WEBAPPSHOME%\servlet%ARG1%\images" ECHO "ERROR removing %WEBAPPSHOME%\servlet%ARG1%\images"
  if exist "%WEBAPPSHOME%\servlet%ARG1%\images" goto ENDPROCERROR
  mkdir          "%WEBAPPSHOME%\servlet%ARG1%\images"
  ss get "$/images" -GL"%WEBAPPSHOME%\servlet%ARG1%\images"  -R  -O- -Yhkon,racer9
:SKIPIMAGES
:NOFULLBUILDWARNING_skipImages
rem pause


rem ********************************** CLASSES FOLDERS 

ECHO BUILD_STEP 9 CREATE FOLDER : WEBAPPS_ITS_BUILDSPECIFIC CLASSES 
rmdir /Q /S    "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes" ECHO "ERROR removing "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes" goto ENDPROCERROR
mkdir          "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"


ECHO BUILD_STEP 10 GET JAVA FROM SSAFE ALL UNDER BUILDPROCESS


rem 1a 4 times below pause "DEBUG NOT REMOVING JAVA" 
rmdir /S /Q    "%BUILDPROCESS%\java"
if exist "%BUILDPROCESS%\java" ECHO "ERROR removing "%BUILDPROCESS%\java"
if exist "%BUILDPROCESS%\java" goto ENDPROCERROR
mkdir "%BUILDPROCESS%\java"

if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipjavaGetCQL
  ECHO BUILD_STEP 10.1 GET JAVA SOURCE - CQL
  rem if not "%ARG1%"=="itsapi" goto SKIPCQL
  rmdir /S /Q    "%BUILDPROCESS%\java\CQL"
  if exist "%BUILDPROCESS%\java\CQL" ECHO "ERROR removing C:\Documents and aSettings\Indraweb\buildprocess\java\CQL"
  if exist "%BUILDPROCESS%\java\CQL" goto ENDPROCERROR
  mkdir          "%BUILDPROCESS%\java\CQL"
  ss get "$/Java2/CQL" -GL"%BUILDPROCESS%\java\CQL"  -R   -O- -Yhkon,racer9
:SKIPCQL
:NOFULLBUILDWARNING_skipjavaGetCQL

ECHO BUILD_STEP 10.2 GET JAVA SOURCE - engine
rem rmdir /S /Q    "%BUILDPROCESS%\java\engine"
rem 1a 4 times below 
if exist "%BUILDPROCESS%\java\engine" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\java\engine"
if exist "%BUILDPROCESS%\java\engine" goto ENDPROCERROR
mkdir          "%BUILDPROCESS%\java\engine"
ss get "$/Java2/engine" -GL"%BUILDPROCESS%\java\engine"  -R   -O- -Yhkon,racer9

if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipjavaGetExtClassifiers
  ECHO BUILD_STEP 10.3 GET JAVA SOURCE - External Classifiers 
  rem rmdir /S /Q    "%BUILDPROCESS%\java\External Classifiers"
  mkdir          "%BUILDPROCESS%\java\External Classifiers"
  ss get "$/Java2/External Classifiers" -GL"%BUILDPROCESS%\java\External Classifiers"  -R   -O- -Yhkon,racer9
:NOFULLBUILDWARNING_skipjavaGetExtClassifiers


if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipjavaGet_GUIservershared
  ECHO BUILD_STEP 10.35 GET JAVA SOURCE - GUI SERVER SHARED
  rem rmdir /S /Q    "%BUILDPROCESS%\java\GUIServerShared"
  mkdir          "%BUILDPROCESS%\java\GUIServerShared"
  ss get "$/Java2/GUIServerShared" -GL"%BUILDPROCESS%\java\GUIServerShared"  -R   -O- -Yhkon,racer9
:NOFULLBUILDWARNING_skipjavaGet_GUIservershared



rem if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipjavaGetIndraDisplay
  rem if "%ARG1%"=="itsclassifier" goto SKIPIndraDisplay
  rem if "%ARG1%"=="itsharvester" goto SKIPIndraDisplay
    ECHO BUILD_STEP 10.36 *********************************** GET JAVA SOURCE - IndraDisplay
  if exist "%BUILDPROCESS%\java\IndraDisplay"  rmdir /S /Q    "%BUILDPROCESS%\java\IndraDisplay"
if exist "%BUILDPROCESS%\java\IndraDisplay" ECHO "ERROR rmdiring "%BUILDPROCESS%\java\IndraDisplay"
if exist "%BUILDPROCESS%\java\IndraDisplay" goto ENDPROCERROR
  mkdir          "%BUILDPROCESS%\java\IndraDisplay"
  ss get "$/Java2/IndraDisplay" -GL"%BUILDPROCESS%\java\IndraDisplay" -R  -O- -Yhkon,racer9

    ECHO BUILD_STEP 10.37 *********************************** GET JAVA SOURCE - ITSadmin
rem pause

  rmdir /S /Q    "%BUILDPROCESS%\ITSAdmin"
if exist "%BUILDPROCESS%\ITSAdmin" ECHO "ERROR rmdiring "%BUILDPROCESS%\ITSAdmin"
if exist "%BUILDPROCESS%\ITSAdmin" goto ENDPROCERROR
  mkdir          "%BUILDPROCESS%\ITSAdmin\src\com\iw"
  mkdir          "%BUILDPROCESS%\ITSAdmin\bin"

  ss get "$/Java2/ITSadmin/src/iw" -GL"%BUILDPROCESS%\ITSAdmin\src\com\iw" -R  -O- -Yhkon,racer9

    ECHO BUILD_STEP 10.38 *********************************** GET JAVA SOURCE - ITSadmin get guiservershared
  xcopy "C:\Documents and Settings\Indraweb\buildprocess\java\GUIServerShared\src\com\*.java" "C:\Documents and Settings\Indraweb\buildprocess\ITSAdmin\src\com" /s




  rem rmdir /S /Q    "%BUILDPROCESS%\java\IndraDisplayUtils"
  mkdir          "%BUILDPROCESS%\java\IndraDisplayUtils"
  ss get "$/Java2/IndraDisplayUtils" -GL"%BUILDPROCESS%\java\IndraDisplayUtils" -R  -O- -Yhkon,racer9
:SKIPIndraDisplay
rem :NOFULLBUILDWARNING_skipjavaGetIndraDisplay
  ECHO BUILD_STEP 10.39 marker 





 
rem if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipjavaGetMISCJAVA
  ECHO BUILD_STEP 10.4 GET JAVA SOURCE - IndraUtils
  rem rmdir /S /Q    "%BUILDPROCESS%\java\IndraUtils"
  mkdir          "%BUILDPROCESS%\java\IndraUtils"
  ss get "$/Java2/IndraUtils" -GL"%BUILDPROCESS%\java\IndraUtils" -R  -O- -Yhkon,racer9

  rem rmdir /S /Q    "%BUILDPROCESS%\java\NDMatchProject"
  ECHO BUILD_STEP 10.5 GET JAVA SOURCE - NDMatchProject
  mkdir          "%BUILDPROCESS%\java\NDMatchProject"
  ss get "$/Java2/NDMatchProject" -GL"%BUILDPROCESS%\java\NDMatchProject" -R  -O- -Yhkon,racer9

  ECHO BUILD_STEP 10.6 GET JAVA SOURCE - NDScoreProject
  rem rmdir /S /Q    "%BUILDPROCESS%\java\NDScoreProject"
rem 1a 2 below for not remove and get java pre compile for ndscore 
mkdir          "%BUILDPROCESS%\java\NDScoreProject"
ss get "$/Java2/NDScoreProject" -GL"%BUILDPROCESS%\java\NDScoreProject" -R  -O- -Yhkon,racer9

  ECHO BUILD_STEP 10.7 GET JAVA SOURCE - TS_API_PROJECT
  rem rmdir /S /Q    "%BUILDPROCESS%\java\TS_API_PROJECT"
  mkdir          "%BUILDPROCESS%\java\TS_API_PROJECT"
  mkdir          "%BUILDPROCESS%\java\TS_API_PROJECT\api"
  ss get "$/Java2/TS_API_PROJECT" -GL"%BUILDPROCESS%\java\TS_API_PROJECT" -R  -O- -Yhkon,racer9

  ECHO BUILD_STEP 10.8.3 delete and recreate %BUILDPROCESS%\classes dir
  rmdir /S /Q    "%BUILDPROCESS%\classes"
  if exist "%BUILDPROCESS%\classes" ECHO "ERROR removing %BUILDPROCESS%\classes"
  if exist "%BUILDPROCESS%\classes" goto ENDPROCERROR
  mkdir          "%BUILDPROCESS%\classes"
rem :NOFULLBUILDWARNING_skipjavaGetMISCJAVA

ECHO BUILD_STEP 11 !!!!!!!!!!!!! MASTER X00 JAVA FILES BUILD - BUILDPROCESS CLASSES
call ant buildjava 
IF ERRORLEVEL 1 pause




if "%FASTBUILD%"=="TRUE" goto SKIPSERVLETSIDECLASSESROOTCOPY
  ECHO BUILD_STEP 12 COPY primary CLASSES from buildprocess to under the WEBAPPS_ITS_BUILDSPECIFIC
  xcopy "%BUILDPROCESS%\classes"  "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"  /Y /S  /Q
:SKIPSERVLETSIDECLASSESROOTCOPY



rem create project jars 
  rmdir /S /Q    "C:\Documents and Settings\Indraweb\buildprocess\projectjars"
  if exist "C:\Documents and Settings\Indraweb\buildprocess\projectjars" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\projectjars"
  if exist "C:\Documents and Settings\Indraweb\buildprocess\projectjars" goto ENDPROCERROR
  mkdir "C:\Documents and Settings\Indraweb\buildprocess\projectjars"
  if not exist "C:\Documents and Settings\Indraweb\buildprocess\projectjars" ECHO "ERROR creating C:\Documents and Settings\Indraweb\buildprocess\projectjars"
  if not exist "C:\Documents and Settings\Indraweb\buildprocess\projectjars" goto ENDPROCERROR






if "%FASTBUILD%"=="TRUE" goto SKIPBINAPPLICATIONS_fcrawl
  ECHO BUILD_STEP 13 BUILD ITSHOME/BIN APPLICATIONS 
  if not "%ARG1%"=="itsapi" goto SKIPITSAPIBINApplications
  rem BUILD ITSHOME/BIN APPLICATIONS 
  ECHO BUILD_STEP 13.1 BUILD ITSHOME/BIN APPLICATIONS - FCRAWL
  rem 1) *********************************** fcrawl
  mkdir        "%ITSHOME%\bin\fcrawl"
  rmdir /S /Q    "%BUILDPROCESS%\fcrawl"
  if exist "%BUILDPROCESS%\fcrawl" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\fcrawl"
  if exist "%BUILDPROCESS%\fcrawl" goto ENDPROCERROR
  mkdir          "%BUILDPROCESS%\fcrawl\src"
  mkdir          "%BUILDPROCESS%\fcrawl\bin"
  ss get "$/Java2/fcrawl/*.java" -GL"%BUILDPROCESS%\fcrawl\src" -R   -O- -Yhkon,racer9
  ss get "$/Java2/IndraUtils/src/com/iw/repository/*.java" -GL"%BUILDPROCESS%\fcrawl\src" -R   -O- -Yhkon,racer9
  ss get "$/Java2/IndraUtils/src/com/iw/fcrawl/*.java" -GL"%BUILDPROCESS%\fcrawl\src" -R   -O- -Yhkon,racer9
  ss get "$/Java2/IndraUtils/src/com/iw/system/FNode.java" -GL"%BUILDPROCESS%\fcrawl\src" -R   -O- -Yhkon,racer9
  ss get "$/Java2/IndraUtils/src/com/iw/system/FNodeDocument.java" -GL"%BUILDPROCESS%\fcrawl\src" -R   -O- -Yhkon,racer9
  rem **** get and copy to bin the fcrawl bat file and sample fcrawl cfg file  
  ss get "$/ITSHome/bin/fcrawl/runFcrawl.bat" -GL"%BUILDPROCESS%\fcrawl" -R  -O- -Yhkon,racer9
  ss get "$/ITSHome/bin/fcrawl/fcrawl.cfg" -GL"%BUILDPROCESS%\fcrawl" -R  -O- -Yhkon,racer9
  copy "%BUILDPROCESS%\fcrawl\runFcrawl.bat" "%ITSHOME%\bin\fcrawl"
  copy "%BUILDPROCESS%\fcrawl\fcrawl.cfg" "%ITSHOME%\bin\fcrawl"
  rem ************** COMPILE FCRAWL JAVA PROJECT
  cd "%BUILDPROCESS%"
  call ant buildfcrawl
  IF ERRORLEVEL 1 pause
  cd "%BUILDPROCESS%\fcrawl\bin"
  jar -cvf "%ITSHOME%\bin\fcrawl\fcrawl.jar" *.class > c:\temp\temp.txt
:SKIPBINAPPLICATIONS_fcrawl






rem create guiservershared.jar and itslicense.jar
rem initially these are needed for notification specifically


  cd "C:\Documents and Settings\Indraweb\buildprocess\classes"
  jar -cvf   "C:\Documents and Settings\Indraweb\buildprocess\projectjars\ITSguiservershared.jar" com\iw\system 
  jar -uf   "C:\Documents and Settings\Indraweb\buildprocess\projectjars\ITSguiservershared.jar" com\iw\guiservershared
  jar -uf   "C:\Documents and Settings\Indraweb\buildprocess\projectjars\ITSguiservershared.jar" com\iw\tools
	
  cd "C:\Documents and Settings\Indraweb\buildprocess\classes"
  jar -cvf   "C:\Documents and Settings\Indraweb\buildprocess\projectjars\ITSLicense.jar" com\iw\license
	






if "%FASTBUILD%"=="TRUE" goto SKIPBINAPPLICATIONS_notification
  ECHO BUILD_STEP 13.2 BUILD ITSHOME/BIN APPLICATIONS - notification
  rem 2) *********************************** notification
  mkdir        "%ITSHOME%\bin\notification"
  rmdir /S /Q    "%BUILDPROCESS%\notification"
  if exist "%BUILDPROCESS%\notification" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\notification"
  if exist "%BUILDPROCESS%\notification" goto ENDPROCERROR
  mkdir          "%BUILDPROCESS%\notification\src"
  mkdir          "%BUILDPROCESS%\notification\bin"
  ss get "$/Java2/Notification/*.java" -GL"%BUILDPROCESS%\notification\src" -R   -O- -Yhkon,racer9
  rem **** get and copy to bin the notification bat file 
  ss get "$/Java2/Notification/runNotification.bat" -GL"%BUILDPROCESS%\notification" -R  -O- -Yhkon,racer9
  copy "%BUILDPROCESS%\notification\runNotification.bat" "%ITSHOME%\bin\notification"
  rem ************** COMPILE Notification JAVA PROJECT
  cd "%BUILDPROCESS%"
  call ant buildNotification
  IF ERRORLEVEL 1 pause
	
  rem create notification.jar file
  cd "%BUILDPROCESS%\notification\bin"
	jar -cvf "%ITSHOME%\bin\notification\notification.jar" . > c:\temp\temp.txt
  cd "C:\Documents and Settings\Indraweb\buildprocess\notification\bin"
	jar -cvf "%ITSHOME%\bin\notification\notification.jar" . 


  copy "%BUILDPROCESS%\java environment\dom4j.jar" "%ITSHOME%\bin\notification\dom4j.jar"
  copy "C:\Documents and Settings\Indraweb\buildprocess\projectjars\ITSguiservershared.jar" "%ITSHOME%\bin\notification\ITSguiservershared.jar"
  copy "C:\Documents and Settings\Indraweb\buildprocess\projectjars\ITSLicense.jar" "%ITSHOME%\bin\notification\ITSLicense.jar"


  rem cp C:\temp\notification.jar "%ITSHOME%\bin\notification\notification.jar"
  rem ********* notification templates 
  ss get "$/Java2/Notification/*.tpl" -GL"%BUILDPROCESS%\notification\src" -R   -O- -Yhkon,racer9
:SKIPBINAPPLICATIONS_notification



:BUILD_STEP13.3
if "%FASTBUILD%"=="TRUE" goto SKIPBINAPPLICATIONS_APIShell
  ECHO BUILD_STEP13.3 BUILD ITSHOME/BIN APPLICATIONS - APIShell
  rem 3) *********************************** APIShell
  rem NOTE:  CLASS FILES OF ITSAPI MUST EXIST AT THIS POINT TO ADD HASHREE TO THIS JAR
  rmdir /S /Q    "%BUILDPROCESS%\APIShell"
  if exist "%BUILDPROCESS%\APIShell" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\APIShell"
  if exist "%BUILDPROCESS%\APIShell" goto ENDPROCERROR
  mkdir          "%BUILDPROCESS%\APIShell\src"
  mkdir          "%BUILDPROCESS%\APIShell\bin"
  mkdir          "%BUILDPROCESS%\APIShell\bin\api\results"

  rmdir /S /Q    "%ITSHOME%\bin\APIShell"
  if exist "%ITSHOME%\bin\APIShell" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\APIShell"
  if exist "%ITSHOME%\bin\APIShell" goto ENDPROCERROR
  mkdir          "%ITSHOME%\bin\APIShell"

  ss get "$/Java2/APIShell/*.bat" -GL"%ITSHOME%\bin\APIShell" -R  -O- -Yhkon,racer9

  ss get "$/Java2/APIShell/*.java" -GL"%BUILDPROCESS%\APIShell\src" -R  -O- -Yhkon,racer9
  rem **** get and copy to bin the reindex bat file 
  cd "%BUILDPROCESS%"
  call ant APIShell
  IF ERRORLEVEL 1 pause
  cd "%BUILDPROCESS%\APIShell\bin"
  jar -cvf "%ITSHOME%\bin\APIShell\APIShell.jar" .   > c:\temp\temp.txt
:SKIPBINAPPLICATIONS_APIShell

rem pause


  ECHO BUILD_STEP 13.4 BUILD ITSHOME/BIN APPLICATIONS - itsadmin
  rem ************** COMPILE ITSADMIN PROJECT
  cd "%BUILDPROCESS%"
  set CLASSPATH=./classes
  call ant builditsadmin
  IF ERRORLEVEL 1 pause


  ECHO BUILD_STEP 13.4.1 GET ANCILLARY FILES (NON JAR) FROM SSAFE IMAGES, HELP ETC.
  ss get "$/HelpFiles/itsuserdocs/its.jar" -GL"%BUILDPROCESS%\itsadmin\bin" -R  -O- -Yhkon,racer9

  ECHO BUILD_STEP 13.4.2 
  mkdir "%ITSHOME%\bin\itsadmin"
  cd "%BUILDPROCESS%\itsadmin\bin"
  rem copy"%BUILDPROCESS%\itsadmin\bin"	
  rem hbkhbk rem 
rem   copy "%BUILDPROCESS%\java environment"\jh.jar .
rem   copy "%BUILDPROCESS%\java environment"\oculuslayout.jar .
rem   copy "%BUILDPROCESS%\java environment"\dom4j.jar .
rem   copy "%BUILDPROCESS%\java environment"\jcommon-0.9.2.jar .
rem   copy "%BUILDPROCESS%\java environment"\jfreechart-0.9.17.jar .

  ECHO BUILD_STEP 13.4.3

 jar -xvf "%BUILDPROCESS%/java environment"/jh.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/oculuslayout.jar  > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/dom4j.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/jcommon-0.9.2.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/jfreechart-0.9.17.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/alloy.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/log4j-1.2.8.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/PDFBox.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/tm-extractors-0.4.jar   > c:\temp\temp.txt
 jar -xvf "%BUILDPROCESS%/java environment"/xercesImpl.jar   > c:\temp\temp.txt


  ECHO BUILD_STEP 13.4.35
 jar -xvf "%BUILDPROCESS%/itsadmin\bin"/its.jar   > c:\temp\temp.txt
 rem seems copy not needed copy "%BUILDPROCESS%\itsadmin\bin"\its.jar "%ITSHOME%\bin\itsadmin\"
 del  "%BUILDPROCESS%/itsadmin\bin"/its.jar   


  ECHO BUILD_STEP 13.4.4 

  cd "%BUILDPROCESS%\itsadmin\bin"
del SUN_MICR.RSA /s
del SUN_MICR.SF  /s
   rem now build the cdms.jar file 

  rem cd "%BUILDPROCESS%"

  jar -cvfM "%ITSHOME%\bin\itsadmin\cdms.jar" . > c:\temp\temp.txt

  copy "%BUILDPROCESS%\itsadmin\bin"\its.jar "%ITSHOME%\bin\itsadmin\"

  ECHO BUILD_STEP 13.4.5 

  mkdir "%ITSHOME%\bin\itsadmin\ITSImages"
  ss get "$/Java2/ITSadmin/itsimages" -GL"%ITSHOME%\bin\itsadmin\itsimages" -R  -O- -Yhkon,racer9

  ECHO BUILD_STEP 13.4.51 



:SKIPITSAPIBINApplications


if not "%ARG1%"=="itsharvesterclient" goto SKIPITSHarvesterClientBIN
ECHO BUILD_STEP 14 BUILD ITSHARVESTCLIENT/BIN (NDFEEDER) MetasearchFeeder APPLICATION 
rem 2) *********************************** ndfeeder MetasearchFeeder
if exist "%BUILDPROCESS%\MetasearchFeeder" rmdir /S /Q    "%BUILDPROCESS%\MetasearchFeeder"
if exist "%BUILDPROCESS%\MetasearchFeeder" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\MetasearchFeeder"
if exist "%BUILDPROCESS%\MetasearchFeeder" goto ENDPROCERROR
mkdir          "%BUILDPROCESS%\MetasearchFeeder\src"
mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses"
mkdir          "%BUILDPROCESS%\MetasearchFeeder\bin"
ss get "$/Java2/MetasearchFeeder/src/*.java" -GL"%BUILDPROCESS%\MetasearchFeeder\src" -R   -O- -Yhkon,racer9
rem **** get and copy to bin the MetasearchFeeder files 
ss get "$/Java2/MetasearchFeeder/run.*" -GL"%BUILDPROCESS%\MetasearchFeeder"  -O- -Yhkon,racer9

rem ************** COMPILE MetasearchFeeder JAVA PROJECT
  cd "%BUILDPROCESS%"
  call ant buildMetasearchFeeder
  IF ERRORLEVEL 1 pause
  cd "%BUILDPROCESS%\MetasearchFeeder\bin"
  jar -cvf "%BUILDPROCESS%\MetasearchFeeder\MetasearchFeeder.jar" .   > c:\temp\temp.txt

  rem ## get missing classes for metasearchfeeder project  
  mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\iw\threadmgr"
  xcopy "%BUILDPROCESS%\classes\com\iw\threadmgr"  "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\iw\threadmgr"

  mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\database"
  xcopy "%BUILDPROCESS%\classes\com\indraweb\database"  "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\database"

  mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\execution"
  xcopy "%BUILDPROCESS%\classes\com\indraweb\execution"  "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\execution"

  mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\util"
  xcopy "%BUILDPROCESS%\classes\com\indraweb\util"  "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\util"

  mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\utils"
  xcopy "%BUILDPROCESS%\classes\com\indraweb\utils"  "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\indraweb\utils"  /Y /Q /S

  mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\api"
  xcopy "%BUILDPROCESS%\classes\api"  "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\api"

  mkdir          "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\api\util"
  xcopy "%BUILDPROCESS%\classes\api\util"  "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\api\util" /Y /S /Q

  cd "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses"
  jar -uvf "%BUILDPROCESS%\MetasearchFeeder\MetasearchFeeder.jar" .   > c:\temp\temp.txt
  rem ## buildprocess side metasearchfeeder files created - now copy to itshome/bin/msfeeder
  if exist "%ITSHOME%\bin\MetasearchFeeder" rmdir /S /Q "%ITSHOME%\bin\MetasearchFeeder"
  if exist "%ITSHOME%\bin\MetasearchFeeder" ECHO "ERROR removing %ITSHOME%\bin\MetasearchFeeder"
  if exist "%ITSHOME%\bin\MetasearchFeeder" goto ENDPROCERROR
  mkdir "%ITSHOME%\bin\MetasearchFeeder\"
  mkdir "%ITSHOME%\bin\MetasearchFeeder\com"
  rem ## seems not to want to run main out the jar file 
  xcopy "%BUILDPROCESS%\MetasearchFeeder\tempadditionalclasses\com\*.*" "%ITSHOME%\bin\MetasearchFeeder\com" /Y /S /Q


  copy "%BUILDPROCESS%\MetasearchFeeder\run.*" "%ITSHOME%\bin\MetasearchFeeder\" 
  copy "%BUILDPROCESS%\MetasearchFeeder\*.jar" "%ITSHOME%\bin\MetasearchFeeder\" 
:SKIPITSHarvesterClientBIN


:BUILD_STEP15.1

ECHO BUILD_STEP 15.1 ************** get ITS_X web.xml 
del /F /Q "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web.xml"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web.xml" echo "ERROR DELETING %WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web.xml"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web.xml" goto ENDPROCERROR
ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/webapps/itsapi/WEB-INF/web.xml" -GL"%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF" -R  -O- -Yhkon,racer9
if not exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web.xml" echo "ERROR GETTING %WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web.xml"
if not exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web.xml" goto ENDPROCERROR

del /F /Q "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web-linux.xml"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web-linux.xml" echo "ERROR DELETING %WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web-linux.xml"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\web-linux.xml" goto ENDPROCERROR
ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/webapps/%ARG1%/WEB-INF/web-linux.xml" -GL"%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF" -R  -O- -Yhkon,racer9

:BUILD_STEP15.2
if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_nowebxml
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_nowebxml

  ECHO BUILD_STEP 15.2 ************** get SERVLET web.xml and retarget web_xxx.xml to web.xml
  if exist "%WEBAPPSHOME%\servlet\WEB-INF\web.xml" del /F /Q "%WEBAPPSHOME%\servlet\WEB-INF\web.xml" 
  ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/UI/web.xml" -GL"%WEBAPPSHOME%\servlet%ARG1%\WEB-INF" -R  -Yhkon,racer9
  rem copy the SRVLTHM_SVL web_xxx.xml file to web.xml - recall that api parm is different in each UI server type
  move "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\web.xml" "%SRVLTHM_SVL%\WEB-INF\web.xml"



:SKIPNOSERVLETUIDIR_nowebxml

ECHO BUILD_STEP 16 ************** get server.xml file to conf dir
del /F /Q "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml"
if exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" echo "ERROR DELETING C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml"
if exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" goto ENDPROCERROR
ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/server.xml" -GL"C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf" -R  -O- -Yhkon,racer9
if not exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" echo "ERROR GETTING C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml"
if not exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" goto ENDPROCERROR


ECHO BUILD_STEP 17 ************** version [%ARG1%] create jar files for four back end servers and two UI servers 

   if exist "%BUILDPROCESS%\classesthisversion" rmdir /S /Q    "%BUILDPROCESS%\classesthisversion"
   if exist "%BUILDPROCESS%\classesthisversion" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\classesthisversion"
   if exist "%BUILDPROCESS%\classesthisversion" goto ENDPROC
   mkdir "%BUILDPROCESS%\classesthisversion"

ECHO BUILD_STEP 17.1 $$$$$$$$ create %ARG1%-svr.jar (back end) version 
rem $$ keep only classes from the master build that are relevant for this version 
rem itsapi - all but metasearcn
rem itsharvester - nothing but login, classify and metasearch and others required, no CQL, 
rem itsclassifier - itsharvester without the metasearch 
rem itsharvesterclient - itsharvester without the metasearch 

   rem *** ITSX-svr INCLUDE
   rem *** ts.class and api 
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-svr"
   rem @@ ts.class
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\ts.class" "%BUILDPROCESS%\classesthisversion\%ARG1%-svr" /Y /Q 
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-svr\api"
   rem @@ api dir 
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\api" "%BUILDPROCESS%\classesthisversion\%ARG1%-svr\api" /Y /Q /S

   rem *** com and sql4j 
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-svr\com"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\com" "%BUILDPROCESS%\classesthisversion\%ARG1%-svr\com" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-svr\sql4j"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\sql4j" "%BUILDPROCESS%\classesthisversion\%ARG1%-svr\sql4j" /Y /Q /S
   cd "%BUILDPROCESS%\classesthisversion\%ARG1%-svr"

   rem *** ITSX-svr EXCLUDE
   rem *** delete tsmetasearch content if not itsharvester
   if "%ARG1%"=="itsharvester" goto SKIPDeleteMSClasses
   if "%ARG1%"=="itsapi" goto SKIPDeleteMSClasses
     rem %%%delete tsmetasearch content if not itsharvester
     rem @@ delete api\tsmetasearchnode for all but harvesterclient
     rmdir /S /Q "%BUILDPROCESS%\classesthisversion\%ARG1%-svr\api\tsmetasearchnode"
   :SKIPDeleteMSClasses

   rem *** do jar for server side 	
   ECHO BUILD_STEP 17.1.1 start %ARG1%-svr.jar build
   jar -cvf "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib\%ARG1%-svr.jar"  .   > c:\temp\temp.txt


if "%ARG1%"=="itsharvester" echo Conditional - SKIP UI.jar for [%ARG1%] version 
if "%ARG1%"=="itsclassifier" echo Conditional - SKIP UI.jar for [%ARG1%] version
if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_noUIjar
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_noUIjar
ECHO BUILD_STEP 17.2 $$$$$$$$ create %ARG1%-ui (front end) version 

   rem *** ITSX-ui INCLUDE
   rem *** com and sql4j 
   rem $$ jar  itsapi UI jar file 
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\Main.class" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui" /Y /Q 
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\api"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\api\*.*" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\api" /Y /Q 
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\com"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\com" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\com" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Crawl"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\Crawl" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Crawl" /Y /Q  /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\HTML"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\HTML" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\HTML" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Idrac"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\Idrac" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Idrac" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Logging"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\Logging" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Logging" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\People"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\People" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\People" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\ROC"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\ROC" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\ROC" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Server"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\Server" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Server" /Y /Q /S
   mkdir "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Taxonomy"
   xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\Taxonomy" "%BUILDPROCESS%\classesthisversion\%ARG1%-ui\Taxonomy" /Y /Q /S

   rem *** ITSX-ui EXCLUDE


   cd "%BUILDPROCESS%\classesthisversion\%ARG1%-ui"
   ECHO BUILD_STEP 17.2.1 start %ARG1%-ui.jar build
   jar -cvf "%SRVLTHM_SVL%\WEB-INF\lib\%ARG1%-ui.jar" . > c:\temp\temp.txt

rem **** create the UI-specific copy from the specific to the generic webapp\servlet folder
rmdir /S /Q    "%WEBAPPSHOME%\servlet"
if exist "%WEBAPPSHOME%\servlet" ECHO "ERROR removing C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet"
if exist "%WEBAPPSHOME%\servlet" goto ENDPROC
mkdir "%WEBAPPSHOME%\servlet"
mkdir "%WEBAPPSHOME%\servlet\classes"
xcopy "%WEBAPPSHOME%\servlet%ARG1%" "%WEBAPPSHOME%\servlet" /Y /Q /S




:SKIPNOSERVLETUIDIR_noUIjar

rem rem $$ jar servlet server jar file 
rem mkdir "%BUILDPROCESS%\classesthisversion\servlet%ARG1%"
rem jar -cvf "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib\%ARG1%.jar"  "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes  > c:\temp\temp.txt

rem $$$$$$$$ itsharvesterclient version 
rem if exist "%BUILDPROCESS%\classesthisversion" rmdir /S /Q    "%BUILDPROCESS%\classesthisversion"
rem if exist "%BUILDPROCESS%\classesthisversion" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\classesthisversion"
rem if exist "%BUILDPROCESS%\classesthisversion" goto ENDPROC
rem mkdir "%BUILDPROCESS%\classesthisversion"
rem xcopy "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes\*.class" "%BUILDPROCESS%\classesthisversion" /Y /Q /S
rem jar -cvf "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib\%ARG1%.jar"  "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes  > c:\temp\temp.txt


cd "%BUILDPROCESS%\classesthisversion"

rem API SIDE 
rem if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib\%ARG1%.jar" del "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib\%ARG1%.jar" 
rem jar -cvf "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\lib\%ARG1%.jar"  "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes  > c:\temp\temp.txt
rem rem now delete what not needed from the 
rem rem UI SIDE 
rem if exist "%SRVLTHM_SVL%\WEB-INF\lib\%ARG1%UI.jar" del "%SRVLTHM_SVL%\WEB-INF\lib\%ARG1%UI.jar" 


:BUILD_STEPEIGHTEEN
ECHO BUILD_STEP 18 ************** DELETE ITSHOME CLASS FILES SINCE JARRED ALREADY
cd "%BUILDPROCESS%"
rmdir /Q /S    "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes" ECHO ERROR removing "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"
if exist "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes" goto ENDPROC
mkdir          "%WEBAPPS_ITS_BUILDSPECIFIC%\WEB-INF\classes"



:BUILD_STEP18.2
ECHO BUILD_STEP18.2 ************** create war files
rem echo on

  del /F /Q /S "%ITSHOME%\WARS\*.*"
  rmdir  /Q /S "%ITSHOME%\WARS"
  if exist "%ITSHOME%\WARS" ECHO "ERROR removing %ITSHOME%\WARS"
  if exist "%ITSHOME%\WARS" goto ENDPROCERROR
  mkdir        "%ITSHOME%\WARS"
c:
cd "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi"
jar -cvf "%ITSHOME%\WARS"\%ARG1%.war WEB-INF\*.xml WEB-INF\lib\classes12.jar WEB-INF\lib\jcs-SNAPSHOT.jar WEB-INF\lib\DbConnectionBroker1.0.11.jar WEB-INF\lib\dom4j.jar WEB-INF\lib\FTPEnterprise.jar WEB-INF\lib\gnu-regexp-1.1.4.jar WEB-INF\lib\itsapi-svr.jar WEB-INF\lib\log4j-1.2.8.jar WEB-INF\lib\ms.jar WEB-INF\lib\oreilly.jar WEB-INF\lib\PDFBox.jar WEB-INF\lib\runtime12.jar WEB-INF\lib\sql4j.jar WEB-INF\lib\xercesImpl.jar WEB-INF\lib\tm-extractors-0.4.jar WEB-INF\lib\junit\aspectjrt-1.1.1.jar WEB-INF\lib\junit\cactus-1.6.1.jar WEB-INF\lib\junit\commons-httpclient-2.0.jar WEB-INF\lib\junit\httpunit-1.5.4.jar WEB-INF\lib\junit\junit-3.8.1.jar WEB-INF\lib\jce1_2_1.jar WEB-INF\lib\jcifs-1.1.6.jar  WEB-INF\lib\xtrim-api.jar 
rem jar -cvf "%ITSHOME%\WARS"\%ARG1%.war WEB-INF


if "%ARG1%"=="itsclassifier"      goto NOUIWARNEEDED

c:
cd "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet"
jar -cvf "%ITSHOME%\WARS"\servlet.war WEB-INF\*.xml WEB-INF\lib\dom4j.jar WEB-INF\lib\gnu-regexp-1.1.4.jar WEB-INF\lib\itsapi-ui.jar WEB-INF\lib\log4j-1.2.8.jar WEB-INF\lib\ms.jar WEB-INF\lib\oreilly.jar WEB-INF\lib\PDFBox.jar WEB-INF\lib\runtime12.jar WEB-INF\lib\xercesImpl.jar images WEB-INF\lib\tm-extractors-0.4.jar WEB-INF\lib\junit\aspectjrt-1.1.1.jar WEB-INF\lib\junit\cactus-1.6.1.jar WEB-INF\lib\junit\commons-httpclient-2.0.jar WEB-INF\lib\junit\httpunit-1.5.4.jar WEB-INF\lib\junit\junit-3.8.1.jar
rem jar -cvf "%ITSHOME%\WARS"\servlet.war WEB-INF

:NOUIWARNEEDED

rem pause


:BUILD_STEPEIGHTEEN.13
ECHO BUILD_STEP 18.13 ************** GET AD exe and ldif files from sssafe to c drive for later copy to cdimage
  del /F /Q /S "%BUILDPROCESS%\ADirectory\*.*"
  rmdir  /Q /S "%BUILDPROCESS%\ADirectory"
  if exist "%BUILDPROCESS%\ADirectory" ECHO "ERROR removing %BUILDPROCESS%\ADirectory"
  if exist "%BUILDPROCESS%\ADirectory" goto ENDPROCERROR
  mkdir  "%BUILDPROCESS%\ADirectory"
ss get "$/BuildProcesses/Installers/ADexe/ad setup.exe" -GL"%BUILDPROCESS%\ADirectory"  -R  -O- -Yhkon,racer9
ss get "$/ADirectory/*.ldif" -GL"%BUILDPROCESS%\ADirectory"  -R  -O- -Yhkon,racer9

:BUILD_STEPEIGHTEEN.15
ECHO BUILD_STEP 18.15 ************** GET Open LDAP exe and ldif files from ssafe to c drive for later copy to cdimage
  del /F /Q /S "%BUILDPROCESS%\OpenLDAP\*.*"
  rmdir  /Q /S "%BUILDPROCESS%\OpenLDAP"
  if exist "%BUILDPROCESS%\OpenLDAP" ECHO "ERROR removing %BUILDPROCESS%\OpenLDAP"
  if exist "%BUILDPROCESS%\OpenLDAP" goto ENDPROCERROR
  mkdir  "%BUILDPROCESS%\OpenLDAP"
ss get "$/LDAP/OpenLDAP/*.*" -GL"%BUILDPROCESS%\OpenLDAP"  -R  -O- -Yhkon,racer9


:BUILD_STEPEIGHTEEN.2
ECHO BUILD_STEP 18.2 ************** GET LDAP BROWSER java program for win and linux 
  del /F /Q /S "%BUILDPROCESS%\Ldap\LDAP BROWSER\*.*"
  rmdir  /Q /S "%BUILDPROCESS%\Ldap\LDAP BROWSER"
  if exist "%BUILDPROCESS%\Ldap\LDAP BROWSER" ECHO "ERROR removing %BUILDPROCESS%\Ldap\LDAP BROWSER"
  if exist "%BUILDPROCESS%\Ldap\LDAP BROWSER" goto ENDPROCERROR
  mkdir  "%BUILDPROCESS%\LDAP\LDAP Browser"
ss get "$/LDAP/LDAP Browser/*.*" -GL"%BUILDPROCESS%\LDAP\LDAP Browser"  -R  -O- -Yhkon,racer9

  del /F /Q /S "%ITSHOME%\Ldap\LDAP BROWSER\*.*"
  rmdir  /Q /S "%ITSHOME%\Ldap\LDAP BROWSER"
  if exist "%ITSHOME%\Ldap\LDAP BROWSER" ECHO "ERROR removing %ITSHOME%\Ldap\LDAP BROWSER"
  if exist "%ITSHOME%\Ldap\LDAP BROWSER" goto ENDPROCERROR
mkdir "%ITSHOME%\LDAP\LDAP Browser"
xcopy "%BUILDPROCESS%\Ldap\LDAP BROWSER\*.*" "%ITSHOME%\LDAP\LDAP Browser" /Y /S /Q




ECHO BUILD_STEP 18.4  ****************** get png image files (installer?)
rem  1 root / ldap / open ldap - those 4 files 
rem  2 "$/BuildProcesses/Installers/ADexe/ad sestup.exe" file

rem images png filess


  del /F /Q /S "%BUILDPROCESS%\images\*.*"
  rmdir  /Q /S "%BUILDPROCESS%\images"
  if exist "%BUILDPROCESS%\images" ECHO "ERROR removing %BUILDPROCESS%\images"
  if exist "%BUILDPROCESS%\images" goto ENDPROCERROR
  mkdir  "%BUILDPROCESS%\images"
ss get "$/BuildProcesses/images/*.png" -GL"%BUILDPROCESS%/images" -R  -O- -Yhkon,racer9











if "%FASTBUILD%"=="TRUE" goto NOFULLBUILDWARNING_skipLogAndSigFilesRestore
rem ***********************************  (NOT FOR PRODUCTION BUILD)
ECHO BUILD_STEP 19 ************** RESTORE SIG FILES 
rem ***********************************  restore sig files (NOT FOR PRODUCTION BUILD)
xcopy         "C:\temp\corpussource\*.*" "%ITSHOME%\corpussource\" /s

ECHO BUILD_STEP 20 ************** RESTORE LOG FILES 
rem ***********************************  restore log files (NOT FOR PRODUCTION BUILD)
copy         "C:\temp\logs\*.*" "	%ITSHOME%\logs\"
:NOFULLBUILDWARNING_skipLogAndSigFilesRestore


ECHO BUILD_STEP 21 delete log and debug trigger files 
mkdir \temp\sav
move /Y \temp\Indra*.txt \temp\sav
move /Y \temp\PropOverride.txt \temp\sav



ECHO BUILD_STEP 21.5 get db install scripts
rmdir /Q /S    "%BUILDPROCESS%\database\installation" 
if exist "%BUILDPROCESS%\database\installation" ECHO ERROR removing "%BUILDPROCESS%\database\installation"
if exist "%BUILDPROCESS%\database\installation" goto ENDPROC
mkdir    "%BUILDPROCESS%\database\installation"
ss get "$/database/installation/*.*" -GL"%BUILDPROCESS%\database\installation"  -O- -Yhkon,racer9



:BUILDJAVADOC
ECHO BUILD_STEP 22 build javadoc

rem echo on
rmdir /Q /S    "%BUILDPROCESS%\javadocinput"
rem PAUSE

if exist "%BUILDPROCESS%\javadocinput" ECHO ERROR removing "%BUILDPROCESS%\javadocinput"
if exist "%BUILDPROCESS%\javadocinput" goto ENDPROC
mkdir    "%BUILDPROCESS%\javadocinput"

ss get "$/HelpFiles/javadocinput/*.*" -GL"%BUILDPROCESS%\javadocinput"  -R  -O- -Yhkon,racer9

rmdir /Q /S    "%ITSHOME%\javadoc"
if exist "%ITSHOME%\javadoc" ECHO ERROR removing "%ITSHOME%\javadoc"
if exist "%ITSHOME%\javadoc" goto ENDPROC
mkdir          "%ITSHOME%\javadoc"

//call %BUILDPROCESS%\

rem pause














echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BUILD COMPLETED 

goto endproc
:ENDPROCERROR
echo "BUILD ERROR : LOOK JUST ABOVE FOR ERRORS AND WARNINGS"
pause

:ENDPROC
cd "%BUILDPROCESS%"
echo "BUILD COMPLETE - No apparent errors : but LOOK ABOVE FOR ERRORS AND WARNINGS"
time /T

rem PAUSE
