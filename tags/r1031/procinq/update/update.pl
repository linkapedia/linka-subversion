#!/usr/bin/perl
#
# update.pl
# 
# Given a corpus id, this script will select all documents available
# for a corpus and verify that each of them exists.   This is not 
# done by downloading the document-- instead a head request is made
# to the web server and a check for a 404 error is made.

$| = 1; # Flush STDOUT

use Getopt::Long;
use DBI;
use LWP::Parallel::UserAgent;
use URI;

local $corpus = ""; local $fork = 3;
&GetOptions ("corpus=i" => \$corpus);

if ($corpus eq "") { 
    print "Error: you must supply a valid corpus with the -corpus tag in order to run this script.\n";
    exit(1);
}

# Get a persistent connection to the database.
local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9");

## Select all documents from the database for this corpus..

print "Getting document count from the database...\n";
local ($sth) = $dbh->prepare("select count(*) from nodedocument where nodeid in (select nodeid from node where corpusid = $corpus)");
$sth->execute;
my $count = $sth->fetchrow_array();
$sth->finish; undef $sth;

print "$count documents to process.\n\n";
print "Selecting documents from the database...\n";
local $sth = $dbh->prepare("select distinct(D.DocUrl) from NodeDocument N, Document D where N.DocumentId = D.DocumentId and N.NodeId IN (select NodeId from Node where CorpusId = $corpus)");
$sth->execute;

## Don't register more than 20k requests at a time, we'll exhause memory!
local ($loop) = 0;

while ($loop < 20000) {
    undef $ua;

    $ua = LWP::Parallel::UserAgent->new();
    $ua->redirect (1); # prevents automatic following of redirects
    $ua->max_hosts(10); # sets maximum number of locations accessed in paral
    $ua->max_req  (10); # sets maximum number of parallel requests per host
    $ua->timeout (10);
    local $type = "HEAD";    

    print "Documents selected.   Registering requests...\n";
    while (($loop < 20000) && ($docurl = $sth->fetchrow_array())) { 
	$loop++;
	my $request = HTTP::Request->new($type);
	my $url = URI->new($docurl);
	$request->url($url);
	$request->header('url' => $docurl); 
	$ua->register ($request);
    }
    if ($loop == 20000) { $loop = 0; }
    print "Running parallel user agent ...\n";
    my $entries = $ua->wait(30);
}
$sth->finish; 
$dbh->disconnect;
print "Done.\n";

1;

sub LWP::Parallel::UserAgent::on_return {
    my $self = shift;
    my ($request, $response, $entry) = @_;
    
    my ($url) = $response->request->header('url'); 
    $url =~ s/\'//gi;
    
    # Response: HTTP/1.1 200 OK
    # Connection: close
    # Date: Wed, 13 Jun 2001 17:11:41 GMT
    # Accept-Ranges: bytes
    # Server: Netscape-Enterprise/3.6 SP2
    # Content-Length: 94442
    # Content-Type: text/html
    # Last-Modified: Wed, 10 Jan 2001 06:04:31 GMT
    
    open (LOG, ">>maint.log");
    
    if ($response->code == 404) {
	
	my $sth = $dbh->prepare("delete from Document where DocUrl = '$url'");
	$sth->execute || warn "Could not delete $url -- $!\n"; 
	$sth->finish;
	print LOG localtime()." Deleting $url (".$response->code." File not found)\n";
	print localtime()." Deleting $url (".$response->code." File not found)\n";
	
    } elsif (($response->code == 302) || ($response->code == 301)) {
	
        my $sRes = $response->as_string;
        if ($sRes =~ /Location\:\ (.*)/i) { 
	    my $newurl = $1; $newurl =~ s/\'//gi;
	    my $sth = $dbh->prepare("delete from Document where DocUrl = '$newurl'");
	    $sth->execute || warn "Could not change $url -- $!\n"; 
	    $sth->finish;
	    $sth = $dbh->prepare("update Document set DocUrl = '$newurl' where DocUrl = '$url'");
	    $sth->execute || warn "Could not change $url -- $!\n"; 
	    $sth->finish;
	    print LOG localtime()." Updating database ($url moved to $newurl) (".$response->code.")\n"; 
	    print localtime()." Updating database ($url moved to $newurl) (".$response->code.")\n"; }
	else { 
	    print LOG "Having problems.   Look at: \n$sRes\n"; 
	    print "Having problems.   Look at: \n$sRes\n"; 
	} 
	
    } elsif ($response->code > 499) {
	
        my $sth = $dbh->prepare("update Document set TroubleStatus = 1 where DocUrl = '$url'");
        $sth->execute || warn "Could not delete $url -- $!\n";
        $sth->finish;
	print LOG localtime()." Response code ".$response->code." indicates server error.   Incremented trouble flag. ($url)\n";
	print localtime()." Response code ".$response->code.". indicates server error.   Incremented trouble flag. ($url)\n";
	
    } else {
	
	print LOG localtime().": ", $response->code, ", ", $response->message,
	" {",++$self->{+__PACKAGE__}{did}, "} ($url)\n";
	print localtime().": ", $response->code, ", ", $response->message,
	" {",++$self->{+__PACKAGE__}{did}, "} ($url)\n";
    }
    close(LOG);
}
$dbh->disconnect();

sub stop {
    $sth->finish;
    $dbh->disconnect();
    exit(1);
}

1;

