<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<META NAME="GENERATOR" CONTENT="Solutionsoft HelpBreeze JavaHelp Edition">
<TITLE>Classifcaton Explain Report</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF=../javahelp.css>
</HEAD>
<BODY BGCOLOR=#ffffff>
<H1>Classifcaton Explain Report</H1>
<P>After a document has been classified, you can choose to 
have any of the topic relationships explained to you in the form of a report.</P>
<P><IMG height=378 hspace=0 src="../Images/classify.GIF" width=675 border=0></P>
<P>This report will gather information from the 
classification engine as to which parts of the document caused the system to 
decide it should be included in a given topic.&nbsp;&nbsp;After selecting a 
Topic in the above window, press explain to view the report.</P>
<P>&nbsp;</P>
<P><IMG height=351 hspace=0 
src="../Images/explainreport1.GIF" width=730 border=0></P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P><IMG height=252 hspace=0 
src="../Images/explainreport2.GIF" width=733 border=0></P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P><IMG height=457 hspace=0 
src="../Images/explainreport3.GIF" width=736 border=0></P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P><IMG height=587 hspace=0 
src="../Images/explainreport4.GIF" width=736 border=0></P>
</BODY>
</HTML>