#!/usr/bin/perl
#
# Looksmart.pl
#
# Spider a series of web pages and build out a new corpus hierarchy.
#
# Parameters:
#    - name             The corpus name to be created
#    - start            Starting URL
#    - node             Starting node id
#    - regex            Pattern to identify links by
#    - depth            Maximum depth level to pursue off-site links (default: 2)

use LWP::Parallel::UserAgent;		# Spidering agent
use Getopt::Long; 		# Input arguments
use URI::URL;			# URL builder
require "NodeHiObject.pm"; 	# Object definition

local ($index) = 0;
local ($regex) = "eus317914";
local ($start) = "http://www.looksmart.com/eus1/eus317836/eus317914/r?l&";
local ($depth) = 1;
local ($not) = "skip";

# Input arguments
&GetOptions ("name=s" => \$name,
             "node=i" => \$node_index,
             "regex=s" => \$regex,
             "depth=i" => \$depth,
             "not=s" => \$not,
             "start=s" => \$start);

if (!defined($name)) { die "Sorry, you must use the -name command to choose the Corpus Name.\n"; }
if (!defined($start)) { die "Sorry, you must use the -start command to select the URL from which I will begin my crawl.\n"; }
if (!defined($regex)) { die "Sorry, you must use the -regex command to identify a common pattern in target URLs.\n"; }

local (%node_hash); local (%dup);

local $ua = new LWP::Parallel::UserAgent 'Indraweb/1.0', 'info@indraweb.com';
$ua->remember_failures (1);
$ua->in_order  (1);
$ua->timeout   (45);
$ua->redirect  (1);
$ua->max_req (25);
$ua->max_hosts (15);

&register_url($start, -1, 1, $name, 0);

local $entries = $ua->wait(20); 

# STEP 3: Process results
foreach (keys %$entries) {
    my $response = $entries->{$_}->response;
    
    if ($response->code != 200) {
	print($response->request->url . " error, " . $response->message."\n");
    }
}

sub register_url {
    my ($url, $pid, $niwp, $title, $depth) = @_;
    if ($dup{$url} == 1) { return 0; }
    #if ($dup{$url} == 1) { print "Duplicate: $url ($title)\n"; return; }

    $dup{$url} = 1;
    #print "Registering $url ($title)\n";

    my ($request) = HTTP::Request->new('GET', $url) || warn "Could not process $url - $!\n";
    $request->header('url' => $url,
		     'pid' => $pid,
		     'niwp' => $niwp,
		     'title' => $title,
		     'depth' => $depth);
    
    if ( my $result = $ua->register ($request,\&the_callback)) {
	die "Error! " .$result->error_as_HTML."\n";
	exit(0); } 

    return 1;
}

# Sort routine: Sort nodes numerically
# This callback will be invoked as requests complete!
# - Build node hierarchy object:
#   * Node Id (assign sequentially)
#   * Parent Id
#   * Node Index Within Parent
#   * Filename (assign by hierarchy)
sub the_callback {
    my ($content, $response, $protocol, $entry) = @_;
    
    if (length ($content) ) {
	# store content as it arrives
	$response->add_content($content);
        print " ";
    } elsif ((defined($response->content)) && (length($response->content))) {
	
	# Get stuff back out of header
	my ($url) = $response->request->header('url');
	my ($pid) = $response->request->header('pid');
	my ($niwp) = $response->request->header('niwp');
	my ($title) = $response->request->header('title');
	my ($mydepth) = $response->request->header('depth');

	print "Request received from $title ($url)\n";
	
	undef $data; local ($data) = $response->content;
	local ($child_niwp) = 1;
	local ($node);
	
	# Detect if the link is going off-site
	local ($site_url); if ($start =~ /http:\/\/(.*?)\//i) {
	    $site_url = $1; $site_url =~ s/www\.//i;
	}
	
	if ($url !~ /$site_url/i) { $mydepth++; }
	
	# Only instantiate a new object if this is indeed a new node in the hierarchy
	if ($mydepth == 0) {     
	    $index++;
	    
	    $node = NodeHiObject->new;
	    $node->{Nid} = $index;
	    $node->{Pid} = $pid;
	    $node->{URL} = $url;
	    $node->{NIWP} = $niwp;
	    $node->{Children} = "";
	    $node->{Title} = $title;
	    
	    # Build filename path
	    local $pwbn = $node_hash{$node->Pid};
	    if (defined($pwbn)) {
		my $niwp_string = sprintf ("%3.3d", $niwp);
		$node->{Filename} = $pwbn->Filename.$niwp_string."/";
	    } else { 
		$node->{Filename} = "001/";
	    }
	    undef $pwbn;
	    
	    # Set the parent node's children, if applicable
	    local $pwbn = $node_hash{$node->Pid};
	    if (defined($pwbn)) {
		if ($pwbn->Children eq "") {
		    $pwbn->{Children} = $node->Nid;
		} else {
		    foreach $listelement (map { split ',' } $pwbn->{Children}) {
			if ($listelement eq $node->Nid) { $has_children = 1; } }
		    if ($has_children == 0) {
			$pwbn->{Children} = $pwbn->Children . "," . $node->Nid;
		    }
		}}
	    $node_hash{$index} = $node;
	    $data =~ s/<TITLE>(.*)<\/TITLE>/<TITLE>$title<\/TITLE>/i;
	} else {
	    $node = $node_hash{$pid};
	}
	
	# Write HTML content to the filesystem
	print "Writing data to file: /tmp/".$node->Filename."000.html\n";
	mkdir ("/tmp/".$node->Filename);
	open (FILE, ">>"."/tmp/".$node->Filename."000.html"); print FILE $data; close(FILE);
	print "File content stored.\n";
	
	# Get the content from the user agent
	$data =~ s/\n//gi;
	print "Object defined.\n";
	
      LINKS:
	while (1) {
	    # Get URL ($1) and TITLE ($2)
	    undef $child_url; undef $child_title;
	    local $child_url; local $child_title; 
	    
	    $data =~ s/\s*href\s*=\s*\"*?([^>\" ]+)\"*?.*?>{1}?(.*?)(<\/a>{1}?)//io;
	    if (!$1 || !$2) { last LINKS; }
	    
	    # Build FULL URL
	    $child_url = &build_url($1, $url);
	    $child_title = $2;

	    # Now, check to ensure that this document qualifies
	    if ( # duplicates check
		 # throw out entries with the mailto tag
		 ($child_url !~ /mailto/i) &&
		 # entry must either match the regex pattern or be an off-site link
		 (($child_url =~ /$site_url/i) && ($child_url =~ /$regex/i)) &&
		 # title cannot be an image tag
		 ($child_title !~ /img/i) &&
		 # do not allow spidering of any images, sounds, PDF files, etc.
		 ($child_url !~ /\.jpg/i) &&
		 ($child_url !~ /$not/i) &&
		 ($child_url !~ /\.wav/i) &&
		 ($child_url !~ /\.bmp/i) &&
		 ($child_url !~ /\.pdf/i) &&
		 ($child_url !~ /\.gif/i) &&
		 ($child_url !~ /javascript/i)) {
		
		if (&register_url($child_url, $index, $child_niwp, $child_title, $mydepth)) {
		   # If it wasn't a duplicate, increment our node within parent counter
		   $child_niwp++; 
		}
	    } elsif (
		     # throw out entries with the mailto tag
		     ($child_url !~ /mailto/i) &&
		     ($child_url !~ /$site_url/i) &&
		     ($child_title !~ /img/i) &&
		     ($child_url !~ /\.jpg/i) &&
		     ($child_url !~ /looksmart/i) &&
		     ($child_url !~ /\.wav/i) &&
		     ($child_url !~ /\.bmp/i) &&
		     ($child_url !~ /\.pdf/i) &&
		     ($child_url !~ /\.gif/i) &&
		     ($mydepth < $depth) &&
		     ($child_url !~ /javascript/i)) {
		&register_url($child_url, $index, $child_niwp, $child_title, $mydepth);
	    }}}
}

sub by_parent {
   $a->Pid <=> $b->Pid || 0;
}

# Regenerate the entire URL
sub build_url {
    my ($partial, $model) = @_;
    my $url = new URI::URL($partial, $model);
    my $build = $url->abs->as_string;

    return $build;
}

1;

