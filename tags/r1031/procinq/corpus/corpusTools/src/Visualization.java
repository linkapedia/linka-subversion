import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class Visualization
{
    public static Hashtable nodes = new Hashtable();
    public static Hashtable nodesR = new Hashtable();
    public static Vector v = new Vector();

    // main function
    public static void main(String[] args) {
        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                        "jdbc:oracle:thin:@66.134.131.62:1521:medusa",
                        "sbooks", // ## fill in User here
                        "racer9" // ## fill in Password here
                );

            System.out.println("Step 1: Get all the nodes in the docanal table.");

            // step 1: get all the nodes in this corpus
            String query = "select nodeid from documentanalysis2";
            Statement stmt = null; ResultSet rs = null; int loop = 0;
            try {
                // execute query
                stmt = connection.createStatement();
                rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int nID = rs.getInt(1); loop++;

                    /* timing start
                    select genreid, count(*) from nodedocument where nodeid in (select nodeid from
                    node start with nodeid = 538300 connect by prior nodeid = parentid) group by genreid;
                    timing stop;
                    */
                    Statement stmt2 = null; ResultSet rs2 = null;
                    query = "select count(*) from nodedocument where nodeid in (select nodeid from "+
                            "node start with nodeid = "+nID+" connect by prior nodeid = parentid)"; // group by genreid";

                    try {
                        // execute second query
                        //System.out.println("*** DEBUG (using node: "+nID+") ***");

                        stmt2 = connection.createStatement();
                        rs2 = stmt2.executeQuery(query);

                        while (rs2.next()) {
                            System.out.println("update documentanalysis2 set countr = "+rs2.getInt(1)+" where "+
                                               "nodeid = "+nID+";"); // and genreid = "+rs2.getInt(1)+" ;");
                            System.out.println("/* finished "+loop+" of 27294 topics */");
                        }
                    } catch (java.sql.SQLException e) { System.out.println("warning: error processing query: "+query);
                        e.printStackTrace(System.err);
                    }
                    finally { rs2.close(); stmt2.close(); rs2 = null; stmt2 = null; }
                }
            } catch (java.sql.SQLException e) { System.out.println("fatal error processing query: "+query);
                e.printStackTrace(System.err);  throw e; }
            finally { rs.close(); stmt.close(); rs = null; stmt = null; }

            connection.close();
        } catch (Exception e) {
            e.printStackTrace(System.out); return;
        }
    }

    /*
    public static String insertNodeNoSrc(String top, Connection connection) throws java.sql.SQLException {
        nodeID++; long niwp = nodeID - 778635;
        top = top.replaceAll("&#215;", "x");
        top = top.replaceAll("'", "''");
        top = top.replaceAll("&", "&'||'");

        if (top.equals("")) { System.err.println("****** FATAL ERROR ****** TOP IS EMPTY"); }

        // parent =  772724, depth = 2, corpus = 82
        String query = "insert into node values ("+nodeID+", 82, '"+top+"', 100, 11292969, "+niwp+", "+
             "2, sysdate, sysdate, 1, null, sysdate, '"+top+"', "+nodeID+")";

        Statement stmt = null;

        try {
            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            stmt.close();
            stmt = null;
        }

        return nodeID+"";
    }

    private static String insertNode (String top, String path, Connection connection) throws Exception {
        path = "C:/DOCUME~1/indraweb/Desktop/janes/janes/" + path;

        FileInputStream fis = new FileInputStream(new File(path));
        BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
        String sData = new String(); StringBuffer sbContent = new StringBuffer();

        buf.readLine(); int save = 0;
        while ((sData = buf.readLine()) != null) {
            Pattern r = Pattern.compile("<!-- /TEXT -->", Pattern.CASE_INSENSITIVE);
            Matcher m = r.matcher(sData); if (m.find()) save = 2;

            if (save == 1) sbContent.append(sData);

            r = Pattern.compile("<!-- TEXT -->", Pattern.CASE_INSENSITIVE);
            m = r.matcher(sData); if ((m.find()) && (save != 2)) save = 1;
        }

        fis.close();

        String content = sbContent.toString().replaceAll("<(.*?)>", " ");
        content = content.replaceAll("&(.*?);", " ");

        System.out.println("   Found child: "+top);

        nodeID++; long niwp = nodeID - 778635;
        top = top.replaceAll("&#215;", "x");
        top = top.replaceAll("'", "''");
        top = top.replaceAll("&", "&'||'");

        if (top.equals("")) { System.err.println("****** FATAL ERROR ****** TOP IS EMPTY"); }

        // parent =  772724, depth = 2, corpus = 82
        String query = "insert into node values ("+nodeID+", 82, '"+top+"', "+content.length()+", "+parent+", "+niwp+", "+
             "3, sysdate, sysdate, 1, null, sysdate, '"+top+"', "+nodeID+")";

        Statement stmt = null;

        try {
            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            stmt.close();
            stmt = null;
        }

        content = content.replaceAll("&", "&'||'");
        content = content.replaceAll("\n\n", " ");
        content = content.replaceAll("^\n", "");

        FixBlanks.insertClobToNodeData(nodeID, content, connection);

        return nodeID+"";
    }
    */

    public static String capitalize(String str, char[] delimiters) {
        if (str == null || str.length() == 0) {
            return str;
        }
        int strLen = str.length();
        StringBuffer buffer = new StringBuffer(strLen);

        int delimitersLen = 0;
        if(delimiters != null) {
            delimitersLen = delimiters.length;
        }

        boolean capitalizeNext = true;
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);

            boolean isDelimiter = false;
            if(delimiters == null) {
                isDelimiter = Character.isWhitespace(ch);
            } else {
                for(int j=0; j < delimitersLen; j++) {
                    if(ch == delimiters[j]) {
                        isDelimiter = true;
                        break;
                    }
                }
            }

            if (isDelimiter) {
                buffer.append(ch);
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer.append(Character.toTitleCase(ch));
                capitalizeNext = false;
            } else {
                buffer.append(ch);
            }
        }
        return buffer.toString();
    }

    private static String stringHTMLtoText(String s) {
        HTMLtoTEXT parser = new HTMLtoTEXT();

        try { parser.parse(new StringReader(s)); }
        catch (Exception e) {
            //e.printStackTrace(System.out);
            String t = s.replaceAll("<(.*?)>", " ");
            return t;
        }
        return parser.getText();
    }

}
