import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class Worldbook2 {
    public static HashSet hs = new HashSet();
    public static Vector v = new Vector();

    // main function
    public static void main(String[] args) {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@66.134.131.62:1521:medusa",
                    "sbooks", // ## fill in User here
                    "racer9" // ## fill in Password here
            );

            System.out.println("Step 1: Load data file.");

            File f = new File("C:/IntelliJ2.5/data/worldbook-full.txt");
            FileInputStream fis = null;

            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            buf.readLine();
            while ((sData = buf.readLine()) != null) {
                if (!sData.startsWith("url:")) {
                    String[] topics = sData.split(" -> ");

                    for (int i = 0; i < topics.length; i++) {
                        topics[i] = topics[i].trim();
                        topics[i] = topics[i].replaceAll((char) 13 + "", "");

                        hs.add(topics[i].toLowerCase());
                    }
                }
            }

            fis.close();

            Statement stmt = null;
            ResultSet rs = null;

            String query = "select nodeid, nodetitle from node where corpusid = 49";

            try {
                // execute query
                stmt = connection.createStatement();
                rs = stmt.executeQuery(query);

                int loop = 0;

                while (rs.next()) {
                    if (!hs.contains(rs.getString(2).toLowerCase())) {
                        System.out.println(rs.getString(2)+" ("+rs.getInt(1)+")");
                    }
                }
            } catch (java.sql.SQLException e) {
                throw e;
            } finally {
                rs.close();
                stmt.close();
            }

        } catch (Exception e) {
            e.printStackTrace(System.err); return;
        }
    }
}
