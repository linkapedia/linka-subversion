import java.io.*;
import javax.swing.text.html.*;
import javax.swing.text.html.parser.*;

public class HTMLtoTEXT extends HTMLEditorKit.ParserCallback {

    StringBuffer s;

    public HTMLtoTEXT() {
    }

    public void parse(Reader in) throws IOException {
        s = new StringBuffer();
        ParserDelegator delegator = new ParserDelegator();
        delegator.parse(in, this, false);
    }

    public void handleText(char[] text, int pos) {
        s.append(" "+new String(text));
    }

    public String getText() {
        return s.toString();
    }
}
