import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class Janes
{
    public static long nodeID = 778635;
    public static int corpusID = 82;
    public static String parent = "";
    public static Hashtable nodes = new Hashtable();

    // main function
    public static void main(String[] args) {
        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        Connection connection = null;
        try {
            File f = new File("C:/DOCUME~1/indraweb/Desktop/janes/janes/jrewsec2.htm");

            FileInputStream fis = null;

            connection = DriverManager.getConnection(
                        "jdbc:oracle:thin:@192.168.0.223:1521:content",
                        "sbooks", // ## fill in User here
                        "racer9" // ## fill in Password here
                );

            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            buf.readLine();
            while ((sData = buf.readLine()) != null) {
                //align="left"><a href="#bposit3">ARMOURED ENGINEER VEHICLES</a>
                Pattern r = Pattern.compile("align=\"left\"><a(.*?)href=\"#(.*?)\">(.*?)</a>", Pattern.CASE_INSENSITIVE);
                Matcher m = r.matcher(sData);

                if (m.find()) {
                    String nodeTitle = m.group(3).toString().toLowerCase();
                    String ID = m.group(2).toString();

                    nodeTitle = capitalize(nodeTitle, null);

                    System.out.println("Found node: "+nodeTitle+" ("+ID+")");
                    String newID = insertNodeNoSrc(nodeTitle, connection);
                    nodes.put(ID, newID);
                }
            }

            buf.close(); fis.close();

            fis = new FileInputStream(f);
            buf = new BufferedReader(new InputStreamReader(fis));
            sData = new String();

            buf.readLine();
            while ((sData = buf.readLine()) != null) {
               //<a name="bposit1">AMPHIBIANS</a>
                Pattern r = Pattern.compile("<a name=\"(.*?)\">", Pattern.CASE_INSENSITIVE);
                Matcher m = r.matcher(sData);

                if (m.find()) {
                    String ID = m.group(1).toString();
                    if (nodes.containsKey(ID)) {
                        parent = (String) nodes.get(ID); System.out.println("Parent: "+parent);
                    }
                }

               // # <li><a href="jmvl2005/jmvl0845.htm" target=_top>Aquatrack tracked 8,000 kg amphibious vehicle</a></li>
               r = Pattern.compile("<li><a href=\"(.*?)\"(.*?)>(.*?)</a></li>", Pattern.CASE_INSENSITIVE);
               m = r.matcher(sData);

                if (m.find()) {
                    String Child = m.group(3).toString();
                    String ID = m.group(1).toString();

                    insertNode(Child, ID, connection);
                }
            }

            connection.close();
        } catch (Exception e) {
            e.printStackTrace(System.out); return;
        }
    }

    public static String insertNodeNoSrc(String top, Connection connection) throws java.sql.SQLException {
        nodeID++; long niwp = nodeID - 778635;
        top = top.replaceAll("&#215;", "x");
        top = top.replaceAll("'", "''");
        top = top.replaceAll("&", "&'||'");

        if (top.equals("")) { System.err.println("****** FATAL ERROR ****** TOP IS EMPTY"); }

        // parent =  772724, depth = 2, corpus = 82
        String query = "insert into node values ("+nodeID+", 82, '"+top+"', 100, 11292969, "+niwp+", "+
             "2, sysdate, sysdate, 1, null, sysdate, '"+top+"', "+nodeID+")";

        Statement stmt = null;

        try {
            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            stmt.close();
            stmt = null;
        }

        return nodeID+"";
    }

    private static String insertNode (String top, String path, Connection connection) throws Exception {
        path = "C:/DOCUME~1/indraweb/Desktop/janes/janes/" + path;

        FileInputStream fis = new FileInputStream(new File(path));
        BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
        String sData = new String(); StringBuffer sbContent = new StringBuffer();

        buf.readLine(); int save = 0;
        while ((sData = buf.readLine()) != null) {
            Pattern r = Pattern.compile("<!-- /TEXT -->", Pattern.CASE_INSENSITIVE);
            Matcher m = r.matcher(sData); if (m.find()) save = 2;

            if (save == 1) sbContent.append(sData);

            r = Pattern.compile("<!-- TEXT -->", Pattern.CASE_INSENSITIVE);
            m = r.matcher(sData); if ((m.find()) && (save != 2)) save = 1;
        }

        fis.close();

        String content = sbContent.toString().replaceAll("<(.*?)>", " ");
        content = content.replaceAll("&(.*?);", " ");

        System.out.println("   Found child: "+top);

        nodeID++; long niwp = nodeID - 778635;
        top = top.replaceAll("&#215;", "x");
        top = top.replaceAll("'", "''");
        top = top.replaceAll("&", "&'||'");

        if (top.equals("")) { System.err.println("****** FATAL ERROR ****** TOP IS EMPTY"); }

        // parent =  772724, depth = 2, corpus = 82
        String query = "insert into node values ("+nodeID+", 82, '"+top+"', "+content.length()+", "+parent+", "+niwp+", "+
             "3, sysdate, sysdate, 1, null, sysdate, '"+top+"', "+nodeID+")";

        Statement stmt = null;

        try {
            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            stmt.close();
            stmt = null;
        }

        content = content.replaceAll("&", "&'||'");
        content = content.replaceAll("\n\n", " ");
        content = content.replaceAll("^\n", "");

        FixBlanks.insertClobToNodeData(nodeID, content, connection);

        return nodeID+"";
    }

    public static String capitalize(String str, char[] delimiters) {
        if (str == null || str.length() == 0) {
            return str;
        }
        int strLen = str.length();
        StringBuffer buffer = new StringBuffer(strLen);

        int delimitersLen = 0;
        if(delimiters != null) {
            delimitersLen = delimiters.length;
        }

        boolean capitalizeNext = true;
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);

            boolean isDelimiter = false;
            if(delimiters == null) {
                isDelimiter = Character.isWhitespace(ch);
            } else {
                for(int j=0; j < delimitersLen; j++) {
                    if(ch == delimiters[j]) {
                        isDelimiter = true;
                        break;
                    }
                }
            }

            if (isDelimiter) {
                buffer.append(ch);
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer.append(Character.toTitleCase(ch));
                capitalizeNext = false;
            } else {
                buffer.append(ch);
            }
        }
        return buffer.toString();
    }

    private static String stringHTMLtoText(String s) {
        HTMLtoTEXT parser = new HTMLtoTEXT();

        try { parser.parse(new StringReader(s)); }
        catch (Exception e) {
            //e.printStackTrace(System.out);
            String t = s.replaceAll("<(.*?)>", " ");
            return t;
        }
        return parser.getText();
    }

}
