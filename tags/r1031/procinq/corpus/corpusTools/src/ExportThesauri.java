import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class ExportThesauri {
    private static String getThesName(String sTID, Connection dbc) throws Exception {
        String sSQL = "select thesaurusname from thesaurus where thesaurusid = " + sTID;
        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery(sSQL);
        String sThesName = null;
        if (rs.next())
            sThesName = rs.getString(1);
        rs.close();
        return sThesName;
    }

    // main function
    public static void main(String[] args) {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@66.134.131.62:1521:medusa",
                    "sbooks", // ## fill in User here
                    "racer9" // ## fill in Password here
            );
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return;
        }

        int iNumThesaurusUpdated = 0;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            String SQL = "select t.THESAURUSID, tw1.THESAURUSWORD, tw2.THESAURUSWORD " +
                    " from thesaurus t, thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2  " +
                    " where t.thesaurusID = tr.thesaurusID and tr.WORDANCHOR = tw1.WORDID and tr.WORDRELATED = tw2.WORDID " +
                    " order by THESAURUSID DESC, tw1.THESAURUSWORD";

            stmt = connection.createStatement();

            rs = stmt.executeQuery(SQL);
            String sTIDCurrent = null;
            PrintWriter file = null;

            String sFileNameClosing = null;
            String sFileNameOpening = null;

            String sPriorAnchorWord = "";
            String sTIDNew = null;
            int iloop = 0;
            while (rs.next()) {
                sTIDNew = rs.getString(1);
                String sW1 = rs.getString(2);
                String sW2 = rs.getString(3);
                String sLineOpenWord = null;
                String sLine2WordSyns = null;

                // HANDLE NEXT THESAURUS
                if (!sTIDNew.equals(sTIDCurrent)) {
                    iloop++;
                    System.out.println(" [" + iloop + "] ******** NEXT THESAURUS PRIOR [" + sTIDCurrent + "] NEW [" + sTIDNew + "] ");
                    // close file fom if this
                    if (file != null) {
                        String sThesNameCurent = getThesName(sTIDCurrent, connection);
                        sFileNameClosing = "c:/temp/Thes_" + sTIDCurrent + ".txt";
                        file.close();
                        System.out.println("complete thesaurus file create [" + sTIDCurrent + "]");

                        iNumThesaurusUpdated++;

                        file = null;
                    }

                    sFileNameOpening = "c:/temp/Thes_" + sTIDNew.replace(' ', '_') + ".txt";
                    // GET PERMISSION TO WRITE THES FILE EXPORT
                    Statement stmt2 = connection.createStatement();
                    ResultSet rs2 = stmt2.executeQuery("call dbms_java.grant_permission('PUBLIC','java.io.FilePermission', '" + sFileNameOpening + "', 'write,delete')");
                    rs2.close();
                    stmt2.close();

                    file = new PrintWriter(new BufferedWriter(new FileWriter(sFileNameOpening)));
                    System.out.println("started thesaurus file create [" + sFileNameOpening + "]");

                    sTIDCurrent = sTIDNew;
                }
                // HANDLE NEXT ANCHOR WORD
                if (sW1.equals(sPriorAnchorWord)) {
                    sLineOpenWord = "";
                } else {
                    sLineOpenWord = "\r\n" + sW1.trim() + "\r\n";
                    sPriorAnchorWord = sW1;
                }
                sLine2WordSyns = "   SYN " + sW2 + "\r\n";
                file.write(sLineOpenWord + sLine2WordSyns);
            }

            if (file != null) {
                file.close();
                System.out.println("complete thesaurus file create [" + sFileNameOpening + "]");

                iNumThesaurusUpdated++;
            }

        } catch (Exception e) {
            e.printStackTrace(System.err);
            return;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace(System.err);
                return;
            }
        }
    }
}
