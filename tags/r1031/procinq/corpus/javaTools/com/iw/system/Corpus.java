package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 24, 2003
 * Time: 1:35:47 PM
 */

/*
  <CORPUSID>10000002</CORPUSID>
  <CORPUS_NAME>Biological agents</CORPUS_NAME>
  <CORPUSDESC>null</CORPUSDESC>
  <ROCF1>0.0</ROCF1>
  <ROCF2>0.0</ROCF2>
  <ROCF3>0.0</ROCF3>
  <ROCC1>0.0</ROCC1>
  <ROCC2>0.0</ROCC2>
  <ROCC3>0.0</ROCC3>
  <CORPUSACTIVE>1</CORPUSACTIVE>
 */
import java.util.Vector;

public class Corpus implements Comparable {
    // User attributes
    private String CorpusID;
    private String CorpusName;
    private String CorpusDesc;
    private String CorpusActive;

    // constructor(s)
    public Corpus () { }
    public Corpus (HashTree ht) {
        CorpusID = (String) ht.get("CORPUSID");
        CorpusName = (String) ht.get("CORPUS_NAME");

        if (ht.containsKey("CORPUSDESC")) {
            CorpusDesc = (String) ht.get("CORPUSDESC"); }
        if (ht.containsKey("CORPUSACTIVE")) {
            CorpusActive = (String) ht.get("CORPUSACTIVE"); }
    }

    // accessor functions
    public String getID() { return CorpusID; }
    public String getName() { return CorpusName; }
    public String getDescription() { return CorpusDesc; }
    public String getActive() { return CorpusActive; }

    public void setID(String ID) { CorpusID = ID; }
    public void setName(String Name) { CorpusName = Name; }
    public void setDescription(String Description) { CorpusDesc = Description; }
    public void setActive(String Active) { CorpusActive = Active; }

    // generic set: used by SAX parser
    public void set(String Key, String Value) {
        if (Key.equals("ID")) { setID(Value); }
        else if (Key.equals("BASENAMESTRING")) { setName(Value); }
        else if (Key.equals("CORPUSDESC")) { setDescription(Value); }
        else if (Key.equals("CORPUSACTIVE")) { setActive(Value); }
    }

    public String toString() { return CorpusName; }
    public int compareTo(Object o) { return CorpusName.compareTo(o.toString()); }
}
