#!/usr/bin/perl

## corpusout.pl
#
# Given a corpus identifier and filename, output an XML version of the corpus 
#  using the Topic Maps XTM DTD

use DBI;
use Getopt::Long;

local ($corpus) = "";
local ($filename) = "corpusout.xtm";

&GetOptions ("corpus=i" => \$corpus,
	     "filename=s" => \$filename);

if ($corpus == "") { die "corpusout.pl -corpus <corpus> -filename <filename>\n"; }

open (FILE, ">$filename");
print FILE "<?xml version=\"1.0\"?>\n";

local ($dbh) = DBI->connect("dbi:Oracle:dallas", "sbooks", "racer9");

print "Selecting corpus information from the database..\n";
local ($q) = "select corpusid, corpus_name, corpusdesc, rocf1, rocf2, rocf3, rocc1, rocc2, rocc3 from corpus where corpusid = $corpus";
local ($sth) = $dbh->prepare($q) || &die_error ("Died during corpus selection: $q.\n");
$sth->execute || &die_error ("Died during corpus selection: $q.\n");
local ($cid, $cname, $cdesc, $r1, $r2, $r3, $r4, $r5, $r6) = $sth->fetchrow_array();
$sth->finish();

print "Printing corpus information to XML file..\n";
print FILE "<topicMap id=\"$cid\">\n";
print FILE "   <baseName> <baseNameString><![CDATA[$cname]]></baseNameString> </baseName>\n";
print FILE "   <occurrence>\n";
print FILE "      <resourceData id=\"CorpusDesc\">\n";
print FILE "      $cdesc\n";
print FILE "      </resourceData>\n";
print FILE "      <resourceData id=\"ROCF1\">$r1</resourceData>\n";
print FILE "      <resourceData id=\"ROCF2\">$r2</resourceData>\n";
print FILE "      <resourceData id=\"ROCF3\">$r3</resourceData>\n";
print FILE "      <resourceData id=\"ROCC1\">$r4</resourceData>\n";
print FILE "      <resourceData id=\"ROCC2\">$r5</resourceData>\n";
print FILE "      <resourceData id=\"ROCC3\">$r6</resourceData>\n";
print FILE "   </occurrence>\n";

print "Selecting node and signature information from the database..\n";
$q = "select nodeid, nodetitle, nodesize, parentid, nodeindexwithinparent, depthfromroot from node where corpusid = $cid order by nodeid asc";
$sth = $dbh->prepare($q) || &die_error ("Died during node selection: $q.\n");
$sth->execute || &die_error ("Died during node selection: $q.\n");

print "Printing node and signature information to FILE...\n";
while (($nid, $ntitle, $nsize, $pid, $niwp, $depth) = $sth->fetchrow_array()) {
    print FILE "\n   <topic id=\"$nid\">\n";
    print FILE "      <baseName> <baseNameString>$ntitle</baseNameString> </baseName>\n";
    print FILE "      <instanceOf>$cid</instanceOf>\n";
    print FILE "      <occurrence>\n";
    print FILE "         <resourceData id=\"NodeSize\">$nsize</resourceData>\n";
    print FILE "         <resourceData id=\"ParentId\">$pid</resourceData>\n";
    print FILE "         <resourceData id=\"NodeIndexWithinParent\">$niwp</resourceData>\n";
    print FILE "         <resourceData id=\"DepthFromRoot\">$depth</resourceData>\n";
    print FILE "         <resourceData id=\"Signatures\">";

    my ($sigsth) = $dbh->prepare("select signatureword, signatureoccurences from signature where nodeid = $nid") || &die_error("Died during signature selection: $q\n");
    $sigsth->execute || &die_error("Died during signature selection: $q\n");
    
    local ($signatures) = "";
    while (my($sigword, $sigocc) = $sigsth->fetchrow_array()) {
	$signatures = $signatures . "$sigword||$sigocc,";
    } chop($signatures); $sigsth->finish();
    
    print FILE "$signatures</resourceData>\n";
    print FILE "      </occurrence>\n";		       
    print FILE "   </topic>\n";
}

print FILE "</topicMap>\n";
$sth->finish();


sub die_error {
    my ($err_string) = $_[0];

    close(FILE);
    die $err_string;
}

1;
