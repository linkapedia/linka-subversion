// Worldbook
//
// Read the Worldbook index file in and create the taxonomy.

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class Worldbook {
    public static Hashtable htArguments = new Hashtable();

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI("192.168.0.223:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        File f = new File((String) htArguments.get("file"));
        if (!f.exists()) { System.err.println("File: "+(String) htArguments.get("file")+" does not exist."); return; }

        FileInputStream fis = new FileInputStream(f);
        BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
        String sData = new String();

        Corpus c = server.addCorpus("Worldbook (rebuilt)", "Worldbook");
        Node root = server.getCorpusRoot(c.getID());

        Hashtable ht = new Hashtable();
        ht.put("-1--"+root.get("NODETITLE"), root);

        /*
        htArguments.put("ParentID", n.get("PARENTID"));
        htArguments.put("NodeSize", n.get("NODESIZE"));
        htArguments.put("NodeDesc", n.get("NODEDESC"));
        htArguments.put("NodeTitle", n.get("NODETITLE"));
        htArguments.put("CorpusID", n.get("CORPUSID"));
        htArguments.put("LinkNodeID", n.get("NODEID"));
        htArguments.put("NodeID", n.get("NODEID"));

        if ((n.get("DEPTHFROMROOT") != null) && (!n.get("DEPTHFROMROOT").equals("")))
            htArguments.put("DepthFromRoot", n.get("DEPTHFROMROOT"));
        */

        buf.readLine(); String sLast = root.get("NODEID");
        while ((sData = buf.readLine()) != null) {
            String sParent = root.get("NODEID");

            if (sData.indexOf("->") != -1) {
                String topics[] = sData.split(" -> ");

                for (int i = 1; i < topics.length; i++) {
                    String sNodeTitle = topics[i].trim();
                    Node n = null;

                    // if the node exists, get it, otherwise, create it.
                    if (ht.containsKey(sParent+"--"+sNodeTitle)) {
                        n = (Node) ht.get(sParent+"--"+sNodeTitle);
                    } else {
                        n = new Node();
                        n.set("PARENTID", sParent);
                        n.set("NODESIZE", "50");
                        n.set("NODEDESC", sNodeTitle);
                        n.set("NODETITLE", sNodeTitle);
                        n.set("CORPUSID", c.getID());
                        n.set("DEPTHFROMROOT", i+"");
                        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

                        n = server.addNode(n);
                        ht.put(sParent+"--"+n.get("NODETITLE"), n);
                    }

                    sParent = n.get("NODEID");
                    sLast = n.get("NODEID");
                }
            } else { // node source has been selected
                Pattern p = Pattern.compile("id=(.*)", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(sData); boolean bResult = m.find();

                if (bResult) {
                    String sFname = m.group(1);
                    File f2 = new File((String) htArguments.get("data")+"/"+sFname+".txt.html");

                    if (f2.exists()) {
                        FileReader in = new FileReader(f2);

                        HTMLtoTEXT parser = new HTMLtoTEXT();
                        parser.parse(in);
                        in.close();

                        System.out.println("Setting node source... file: "+f2.getAbsolutePath()+" id: "+sLast);
                        server.setNodeSource(sLast, parser.getText());
                    } else {
                        System.err.println("Warning: "+f2.getAbsolutePath()+" does not exist.");
                    }
                }
            }
        }

        fis.close();
   }

   public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}