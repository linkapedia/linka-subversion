package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 24, 2003
 * Time: 1:35:47 PM
 */

/*
  <CORPUSID>10000002</CORPUSID>
  <CORPUS_NAME>Biological agents</CORPUS_NAME>
  <CORPUSDESC>null</CORPUSDESC>
  <ROCF1>0.0</ROCF1>
  <ROCF2>0.0</ROCF2>
  <ROCF3>0.0</ROCF3>
  <ROCC1>0.0</ROCC1>
  <ROCC2>0.0</ROCC2>
  <ROCC3>0.0</ROCC3>
  <CORPUSACTIVE>1</CORPUSACTIVE>
 */
import org.dom4j.Element;

import java.util.Vector;

public class Signature implements Comparable {
    // User attributes
    private String sSignatureWord;
    private double dSignatureWeight;
    private boolean ThesaurusTerm;

    // constructor(s)
    public Signature (String word, double dWeight) {
        sSignatureWord = word; dSignatureWeight = dWeight;
    }

    public Signature (HashTree ht) {
        sSignatureWord = (String) ht.get("WORD");
        dSignatureWeight = new Double((String) ht.get("WEIGHT")).doubleValue();
    }

    public Signature (Vector v) {
        sSignatureWord = (String) v.elementAt(0);
        dSignatureWeight = new Double ((String) v.elementAt(1)).doubleValue();
    }

    public Signature (Element elem) {
        sSignatureWord = elem.element("WORD").getText();
        dSignatureWeight = new Double(elem.element("WEIGHT").getText()).doubleValue();
    }

    // move this function into Signature object at some point please
    public Double getFrequency(Node n) {
        double size = new Double(n.get("NODESIZE")).doubleValue();

        if (size == 0) return new Double("0.0"); // if nodesize is 0, return empty

        return new Double(100.0 * (getWeight() / size));
    }

    // accessor functions
    public String getWord() { return sSignatureWord; }
    public double getWeight() { return dSignatureWeight ;}
    public boolean isSynonym() { return ThesaurusTerm; }

    public void setWord(String Word) { sSignatureWord = Word; }
    public void setWeight(double dWeight) { dSignatureWeight = dWeight; }
    public void setWeight(int iWeight) { dSignatureWeight = (double) iWeight; }
    public void setSynonym(boolean b) { ThesaurusTerm = b; }

    public String toString() { return sSignatureWord; }
    public int compareTo(Object o) { return sSignatureWord.compareTo(o.toString()); }
}
