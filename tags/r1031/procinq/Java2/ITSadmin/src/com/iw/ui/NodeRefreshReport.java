/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Feb 2, 2004
 * Time: 1:57:28 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.oculustech.layout.*;
import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.system.*;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.*;
import java.awt.event.*;

public class NodeRefreshReport extends TopicSearch implements ActionListener, ListSelectionListener, MouseListener {
    protected JButton removeButton;
    protected JButton refreshButton;

    public NodeRefreshReport (ITSAdministrator frame) throws Exception {
        super(frame);
        getMDIframe().setTitle("Topic Refresh Report");
    }

    public void buildTopicSearch(JInternalFrame MDIframe, OculusBox ob, boolean bQueryTextBox)
            throws Exception {
        JDesktopPane jdesktoppane = ITSframe.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        searchButton = new JButton("Search");
        searchButton.addActionListener(this);
        removeButton = new JButton("Remove");
        removeButton.addActionListener(this);
        refreshButton = new JButton("Refresh Now");
        refreshButton.addActionListener(this);
        clearButton = new JButton("Close");
        clearButton.addActionListener(this);

        removeButton.setEnabled(false);
        refreshButton.setEnabled(false);

        taxonomyLabel = new JLabel("Taxonomy:");
        taxonomyLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        Vector v = its.getCorpora();
        v.addElement("All");
        taxonomyDropDown = new JComboBox(v);
        taxonomyDropDown.setSelectedItem("All");

        resultsTable = new JSortTable();
        resultsTable.addMouseListener(this);

        queryTextArea = new JTextArea();

        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(new Space(5,5));
            layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
            {
                layout.add(new Space(5,5));
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestGrid(2,1);
                            {
                                layout.add(taxonomyLabel);
                                layout.add(taxonomyDropDown);
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.add(new JScrollPane(resultsTable));
                            layout.add(new Space(5,5));
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.add(searchButton);
                            layout.add(new Space(2,2));
                            layout.add(refreshButton);
                            layout.add(new Space(2,2));
                            layout.add(removeButton);
                            layout.add(new Space(2,2));
                            layout.add(clearButton);
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.add(new Space(5,5));

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    private void loadResults(JSortTable table) throws Exception {
        Vector vNodes = new Vector();
        Hashtable htCorpus = new Hashtable();

        if (taxonomyDropDown.getSelectedItem().equals("All")) {
            vNodes = its.getRefreshNodeList();
        } else {
            vNodes = its.getRefreshNodeList((Corpus) taxonomyDropDown.getSelectedItem());
        }

        if (vNodes.size() == 0) {
            DefaultSortTableModel dtm = new DefaultSortTableModel();
            table.setModel(dtm);
            return;
        }

        removeButton.setEnabled(true);
        refreshButton.setEnabled(true);

        DefaultSortTableModel dtm = new DefaultSortTableModel(vNodes.size(), 3);
        dtm.setEditable(false);
        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            dtm.setValueAt(n, i, 0);
            dtm.setValueAt(n.get("NODEDESC"), i, 1);

            if (!htCorpus.containsKey(n.get("CORPUSID"))) {
                htCorpus.put(n.get("CORPUSID"), its.getCorpora(n.get("CORPUSID")));
            }

            dtm.setValueAt((Corpus) htCorpus.get(n.get("CORPUSID")), i, 2);
        }
        Vector v = new Vector();
        v.add("Title");
        v.add("Definition");
        v.add("Taxonomy");
        dtm.setColumnIdentifiers(v);

        table.setModel(dtm);
    }
    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                Component glass = getGlass();

                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    if (e.getSource().equals(refreshButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row > -1) {
                            ITSTreeNode n = (ITSTreeNode) resultsTable.getModel().getValueAt(row, 0);
                            RefreshNodePopup rnp = new RefreshNodePopup(ITSframe, n);
                            rnp.setVisible(true);
                        }
                    }
                    if (e.getSource().equals(searchButton)) {
                        try {
                            loadResults(resultsTable);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    }
                    if (e.getSource().equals(removeButton)) {
                        int row = resultsTable.getSelectedRow();
                        try {
                            ITSTreeNode n = (ITSTreeNode) resultsTable.getModel().getValueAt(row, 0);
                            its.removeRefreshNode(n.get("NODEID"));

                            DefaultSortTableModel dstm = (DefaultSortTableModel) resultsTable.getModel();
                            dstm.removeRow(row);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    }
                    if (e.getSource().equals(clearButton)) {
                        getMDIframe().dispose();
                    }
                } catch (Exception e) {
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                getGlass().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                getGlass().setVisible(false);
            }
        };
        aWorker.start();
    }

}
