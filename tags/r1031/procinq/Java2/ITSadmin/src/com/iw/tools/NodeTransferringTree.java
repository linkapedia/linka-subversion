package com.iw.tools;

import com.iw.system.Document;
import com.iw.system.ITS;
import com.iw.system.ITSTreeNode;
import com.iw.system.NotAuthorized;
import com.iw.system.User;
import com.iw.ui.ITSAdministrator;
import com.iw.ui.PopupProgressBar;
import com.iw.license.IndraLicense;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.swing.event.TreeModelEvent;

/**
 * A JTree that supports clipboard operations and drag-and-drop of tree nodes. It maintains
 * its own local clipboard, and provides methods for clipboard operations. To use clipboard
 * features, a client needs to create its own gui (context menus, etc.) that invokes the
 * clipboard support methods. Drag and drop operations are automatic, needing no effort on
 * the part of the client code. Note: for now, this class assumes that the tree nodes are
 * all DefaultMutableTreeNodes, and that the model is a DefaultTreeModel. It also requires
 * that any user objects of the nodes implement
 * com.neusys.components.nodetransferringtree.PubliclyCloneable, because a clone of a node's
 * user object is made as part of the transfer process. This prevents having more than one
 * node's user object refer to the same content.
 */
public class NodeTransferringTree extends JTree implements Autoscroll {

    private DropTargetListener dropTargetListener = new
            TreeDropTargetListener();
    private DropTarget nodeDropTarget = new DropTarget(this,
            dropTargetListener);
    private boolean treeDragging = false;
    protected Point viewLocation;

    public ITS its = null;

    public NodeTransferringTree(DefaultTreeModel model) {
        super(model);
        new TreeDragSource();
        getNodeDropTarget().setDefaultActions(
                DnDConstants.ACTION_COPY_OR_MOVE);
    }

    public void setDragging(boolean b) { treeDragging = b; }

    public boolean isDragging() { return treeDragging; }
    public boolean isEditable() {
        TreePath path = getSelectionPath();
        if (path == null) return true;

        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();
        ITSTreeNode n = (ITSTreeNode) item.getUserObject();

        return setModifyPermission(n);
    }

    public boolean setModifyPermission(ITSTreeNode n) {
        User u = ITS.getUser();
        if (u.lockFunctions() && (!n.get("NODEID").equals(n.get("LINKNODEID")))) return false;
        return true;
    }

    public void scheduleNodeRefresh() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();
        ITSTreeNode n = (ITSTreeNode) item.getUserObject();

        try { its.addRefreshNode(n.get("NODEID")); }
        catch (Exception e) { throw e; }
    }

    public void alphabetizeNode() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        alphabetizeNode(item);
    }

    public void alphabetizeNode(DefaultMutableTreeNode item) throws Exception {
        its.alphabetizeChildren((ITSTreeNode) item.getUserObject());
        int index = getNodeIndex(item);

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    public void alphabetizeNodeRecursively() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        alphabetizeNodeRecursively(item);
    }

    public void alphabetizeNodeRecursively(DefaultMutableTreeNode item) throws Exception {
        its.alphabetizeChildren((ITSTreeNode) item.getUserObject(), true);
        int index = getNodeIndex(item);

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    public void capitalizeNode() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        capitalizeNode(item);
    }

    public void capitalizeNode(DefaultMutableTreeNode item) throws Exception {
        its.capitalizeChildren((ITSTreeNode) item.getUserObject());
        int index = getNodeIndex(item);

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    public void capitalizeNodeRecursively() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        capitalizeNodeRecursively(item);
    }

    public void capitalizeNodeRecursively(DefaultMutableTreeNode item) throws Exception {
        its.capitalizeChildren((ITSTreeNode) item.getUserObject(), true);
        int index = getNodeIndex(item);

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    public String editNodeStatusRecursively (int status) throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        String response = its.editNodeStatusRecurse((ITSTreeNode) item.getUserObject(), status);

        refreshNode(item);

        return response;
    }

    public void rollUpNodes(ITSAdministrator admin) throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        PopupProgressBar ppb = new PopupProgressBar(admin, 0, 100);
        ppb.show();

        its.rollUpNodes((ITSTreeNode) item.getUserObject(), 50, ppb);
        int index = getNodeIndex(item);
        ppb.dispose();

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    /* Strip Node */

    public void stripNodeWhitespace() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        stripNodeWhitespace(item);
    }

    public void stripNodeWhitespace(DefaultMutableTreeNode item) throws Exception {
        its.stripChildrenWhitespace((ITSTreeNode) item.getUserObject(), true);
        int index = getNodeIndex(item);

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    public void stripNode() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        stripNode(item);
    }

    public void stripNode(DefaultMutableTreeNode item) throws Exception {
        its.stripChildren((ITSTreeNode) item.getUserObject());
        int index = getNodeIndex(item);

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    public void stripNodeRecursively() throws Exception {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode item =
                (DefaultMutableTreeNode) path.getLastPathComponent();

        stripNodeRecursively(item);
    }

   /**
    *  (1) make terms in parentheses at end of topic name into must have and remove from topic
    * for example:
    *    original topic = American Institute of Architects (AIA)
    *    new topic = American Institute of Architects
    *    new must have = AIA
    **/
    public void stripParensAddMustHave(DefaultMutableTreeNode selectedNode, ITSTreeNode n) throws Exception {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) selectedNode.getParent();
        int index = getNodeIndex(selectedNode);
        String title = n.get("NODETITLE");

        Pattern r = Pattern.compile("\\((.*?)\\)", Pattern.CASE_INSENSITIVE);
        Matcher m = r.matcher(title);
        boolean bResult = m.find();
        if (bResult) {
            // step 1: create the must have term
            String musthave = (String) m.group(1).toString();
            try { its.addMusthave(n.get("NODEID"), musthave.toLowerCase().trim()); }
            catch (Exception e) {
                e.printStackTrace(System.err);
                throw new Exception("Could not create must have term '"+musthave+"'.  See error log for more details.");
            }

            // step 2: alter the node title
            try {
                title = title.replaceAll("\\("+musthave+"\\)", "").trim();
                n.set("NODETITLE", title);
                its.editNodeTitle(n);
                its.editNodeIndex(n.get("NODEID"), n.get("NODEINDEXWITHINPARENT"));

            } catch (Exception e) {
                e.printStackTrace(System.err);
                throw new Exception("Could not change node title to '"+title+"'.  See error log for more details.");
            }
            // step 3: refresh
            refreshNode(parent);
            if (index != -1) setSelectionRow(index);

            return;
        }
    }
    public void stripParensAddMustHaveRecurse(DefaultMutableTreeNode selectedNode, ITSTreeNode n) throws Exception {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) selectedNode.getParent();
        int index = getNodeIndex(selectedNode);

        try { its.parensToMustHaveRecursive(n);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            throw new Exception(e.getLocalizedMessage());
        }

        refreshNode(parent);
        //if (index != -1) setSelectionRow(index);

        return;
    }

    /**
     *   (2) separate topic name at commas, making all, except first, as must haves
     * for example:
     *    original topic = asbestos-cement board, asbestos-cement wallboard, asbestos sheeting
     *    new topic = asbestos-cement board
     *    new must haves = asbestos-cement wallboard & asbestos sheeting
     */
    public void stripCommasAddMustHave(DefaultMutableTreeNode selectedNode, ITSTreeNode n) throws Exception {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) selectedNode.getParent();
        int index = getNodeIndex(selectedNode);
        String title = n.get("NODETITLE");

        String[] titar = title.split(",");
        if (titar.length > 1) {
            // step 1: create the must have terms
            for (int i = 1; i < titar.length; i++) {
                try { its.addMusthave(n.get("NODEID"), titar[i].toLowerCase().trim());
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }
            }

            // step 2: alter the node title
            try {
                title = titar[0].trim();
                n.set("NODETITLE", title);
                its.editNodeTitle(n);
                its.editNodeIndex(n.get("NODEID"), n.get("NODEINDEXWITHINPARENT"));
            } catch (Exception e) {
                e.printStackTrace(System.err);
                throw new Exception("Could not change node title to '"+title+"'.  See error log for more details.");
            }
            // step 3: refresh
            refreshNode(parent);
            if (index != -1) setSelectionRow(index);

            return;
        }
    }
    public void stripCommasAddMustHaveRecurse(DefaultMutableTreeNode selectedNode, ITSTreeNode n) throws Exception {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) selectedNode.getParent();
        int index = getNodeIndex(selectedNode);

        try { its.commasToMustHaveRecursive(n);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            throw new Exception(e.getLocalizedMessage());
        }

        refreshNode(parent);
        //if (index != -1) setSelectionRow(index);

        return;
    }

    public void stripNodeRecursively(DefaultMutableTreeNode item) throws Exception {
        its.stripChildren((ITSTreeNode) item.getUserObject(), true);
        int index = getNodeIndex(item);

        refreshNode(item);
        if (index != -1) setSelectionRow(index);
    }

    /**
     * Copies the selected DefaultMutableTreeNode to this tree's clipboard.
     */
    public void copySelectedNode() {
        TreePath[] path = getSelectionPaths();
        //System.out.println("copySelectedNode: " + path);
        its.originalSelectionPath = path[0];

        if (path == null) return;

        // modified this to allow multi-select
        DefaultMutableTreeNode[] newNodes = new DefaultMutableTreeNode[path.length];
        for (int i = 0; i < newNodes.length; i++) {
            newNodes[i] = copyNode((DefaultMutableTreeNode) path[i].getLastPathComponent());
        }

        Transferable transferable = new TreeNodeSelection(newNodes);
        getClipboard().setContents(transferable, null);
    }

    /**
     * Cuts the selected DefaultMutableTreeNode to this tree's clipboard.
     */
    public void cutSelectedNode() {
        copySelectedNode();
        TreePath[] path = getSelectionPaths();

        DefaultMutableTreeNode[] cutNodes = new DefaultMutableTreeNode[path.length];
        for (int i = 0; i < path.length; i++) {
            cutNodes[i] = (DefaultMutableTreeNode) path[i].getLastPathComponent();
            ITSTreeNode n = (ITSTreeNode) cutNodes[i].getUserObject();
            n.setTree(this);
            cutNodes[i].setUserObject(n);
        }
        its.cutNode = cutNodes;
    }

    /**
     * Pastes the DefaultMutableTreeNode that is in this tree's clipboard into the next
     * child position under the selected DefaultMutableTreeNode.
     */
    public void pasteIntoSelectedNode(ITSAdministrator admin) { pasteIntoSelectedNode(admin, false); }
    public void pasteIntoSelectedNode(ITSAdministrator admin, boolean pasteAsLink) {
        TreePath path = getSelectionPath();
        //System.out.println("pasteSelectedNode: " + path);
        DefaultMutableTreeNode selectedNode =
                (DefaultMutableTreeNode) path.getLastPathComponent();
        Transferable transferable = getClipboard().getContents(this);

        if (transferable == null) {
            System.out.println("Nothing to paste.");
            return;
        }

        DefaultMutableTreeNode[] dmtns = null;

        try { dmtns = (DefaultMutableTreeNode[]) transferable.getTransferData(TreeNodeSelection.treeNodeFlavor); }
        catch (Exception e) { throw new IllegalStateException("Tried to paste unsupported or unavailable object into tree. "); }

        boolean bRemovedCutNodes = false;

        for (int i = 0; i < dmtns.length; i++) {
            DefaultMutableTreeNode newNode = null;
            newNode = dmtns[i];

            ITSTreeNode nNew = (ITSTreeNode) newNode.getUserObject();
            ITSTreeNode nInto = (ITSTreeNode) selectedNode.getUserObject();

            // *new* get the NIWP of the last node in the current hierarchy and add 1
            try {
                ITSTreeNode lastChild = (ITSTreeNode) ((DefaultMutableTreeNode) selectedNode.getLastChild()).getUserObject();
                nNew.set("NODEINDEXWITHINPARENT", (Integer.parseInt(lastChild.get("NODEINDEXWITHINPARENT"))+1)+"");
            } catch (Exception e) { System.err.println("Could not reset NIWP, using a value of 1 instead."); }

            TreePath stp = new TreePath(selectedNode.getPath());

            if (its.originalSelectionPath.isDescendant(stp)) {
                System.out.println("Cannot insert a topic into it's descendent.");
                return;
            }

            if (nInto.isLink()) {
                System.out.println("Linked topics may not contain descendents.");
                JOptionPane.showMessageDialog(
                        admin, "Linked topics may not contain descendents.", "Error", JOptionPane.NO_OPTION);
                return;
            }

            if ((its.cutNode != null) && (!pasteAsLink)) {
                if (nNew.get("PARENTID").equals("-1")) {
                    System.out.println("Cannot move the root node of a corpus elsewhere.");
                    return;
                }
                try {
                    ITSTreeNode n = its.moveNode(nNew.get("NODEID"), nInto.get("NODEID"));
                    newNode.setUserObject(n);
                    selectedNode.add(newNode);

                    int index = getNodeIndex(selectedNode);

                    if (!bRemovedCutNodes) {
                        for (int j = 0; j < its.cutNode.length; j++) {
                            DefaultMutableTreeNode cut = its.cutNode[j];
                            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) cut.getParent();
                            parent.remove(cut);

                            if (parent.getDepth() < selectedNode.getDepth()) {
                                index = getNodeIndex(parent);
                            }

                            if (nNew.getTree() != null) {
                                ((DefaultTreeModel) (nNew.getTree()).getModel()).nodeStructureChanged(parent);
                            } else {
                                ((DefaultTreeModel) getModel()).nodeStructureChanged(parent);
                            }
                            ((DefaultTreeModel) getModel()).nodeStructureChanged(selectedNode);

                            TreePath tp = (TreePath) this.getPathForRow(index);
                            //System.out.println("Expanding: " + tp);
                            if (index != -1) expandRow(index);
                        }
                        bRemovedCutNodes = true;
                    }

                } catch (Exception e) {
                    System.out.println("Move node failed.");
                    e.printStackTrace(System.out);
                } finally {
                    its.cutNode = null;
                }
            } else {
                nNew.set("PARENTID",nInto.get("NODEID"));

                try {
                    // on a COPY, add the node and ALL of his descendents
                    ITSTreeNode n = null;

                    if (pasteAsLink) {
                        if (nNew.isLink()) {
                            System.out.println("You may not link to another link.");
                            JOptionPane.showMessageDialog(
                                admin, "You may not link to another link.", "Error", JOptionPane.NO_OPTION);
                            return;
                        }
                        nNew.set("CORPUSID", nInto.get("CORPUSID"));
                        n = its.addNode(nNew);
                    } else {
                        PopupProgressBar ppb = new PopupProgressBar(admin, 0, 100);
                        ppb.show();
                        n = its.copyNode(nNew, ppb);
                        ppb.dispose();
                    }
                    newNode.setUserObject(n);

                    refreshNode(selectedNode);
                } catch (NotAuthorized na) {
                    JOptionPane.showMessageDialog(admin, "You may not create this link as it would violate data integrity.",
                            "Error", JOptionPane.NO_OPTION);
                    return;
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(admin, "Sorry, you may not paste a topic that has been deleted.",
                            "Error", JOptionPane.NO_OPTION);
                    e.printStackTrace(System.out);

                    return;
                }
            }

            //selectedNode.add(newNode);
            int index = getNodeIndex(selectedNode);

            ((DefaultTreeModel) getModel()).nodeStructureChanged(selectedNode);
            if (index != -1) expandRow(index);
        }
    }

    public void splitNode(ITSAdministrator admin, DefaultMutableTreeNode item) {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) item.getParent();
        DefaultMutableTreeNode newNode = null;

        Transferable transferable = getClipboard().getContents(this);

        try {
            DefaultMutableTreeNode[] dmtns;

            try { dmtns = (DefaultMutableTreeNode[]) transferable.getTransferData(TreeNodeSelection.treeNodeFlavor); }
            catch (Exception e) { throw new IllegalStateException("Tried to paste unsupported or unavailable object into tree. "); }

            newNode = dmtns[0];

        } catch (Exception e) {
            throw new IllegalStateException("Tried to paste unsupported or unavailable object into tree. ");
        }

        ITSTreeNode nOld = null;
        ITSTreeNode nNew = (ITSTreeNode) newNode.getUserObject();
        ITSTreeNode nInto = (ITSTreeNode) parent.getUserObject();

        TreePath stp = new TreePath(parent.getPath());

        if (nInto.isLink()) {
            System.out.println("Linked topics may not contain descendents.");
            JOptionPane.showMessageDialog(
                    admin, "Linked topics may not contain descendents.", "Error", JOptionPane.NO_OPTION);
            return;
        }

        nNew.set("PARENTID",nInto.get("NODEID"));

        try {
            nOld = its.getNodeSource((ITSTreeNode) item.getUserObject());

            // on a COPY, add the node and ALL of his descendents
            PopupProgressBar ppb = new PopupProgressBar(admin, 0, 100);
            ppb.show();

            ITSTreeNode n = its.copyNode(nNew, ppb);

            ppb.dispose();

            newNode.setUserObject(n);

            its.setNodeSource(n, nOld.get("NODESOURCE"));
            its.editNodeIndex(n.get("NODEID"), nOld.get("NODEINDEXWITHINPARENT"));

            refreshNode(parent);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(admin, "Sorry, you may not paste a topic that has been deleted.",
                "Error", JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);

            return;
        }

        //selectedNode.add(newNode);
        int index = getNodeIndex(parent);

        ((DefaultTreeModel) getModel()).nodeStructureChanged(parent);
        if (index != -1) expandRow(index);
    }

    public int getNodeIndex(DefaultMutableTreeNode item) {
        int row = 0;

        TreePath tp = new TreePath(item.getPath());
        TreePath compare = (TreePath) getPathForRow(row);

        while (!tp.equals(compare)) {
            row++; //System.out.println("Comparing: "+tp+" to "+compare+" ("+row+")"); row++;
            compare = (TreePath) getPathForRow(row);

            if (compare == null) {
                return -1;
            }
        }

        return row;
    }

    public DefaultMutableTreeNode getChildNode(DefaultMutableTreeNode item, ITSTreeNode n) {
        Enumeration eChildren = item.children();

        while (eChildren.hasMoreElements()) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) eChildren.nextElement();
            ITSTreeNode nodechild = (ITSTreeNode) child.getUserObject();

            if (nodechild.get("NODEID").equals(n.get("NODEID"))) {
                return child;
            }
        }

        return null;
    }

    // implemented to add scrolling when dragging an object
    protected Rectangle getTableRect() {
        JViewport jvp = (JViewport) (SwingUtilities.getAncestorOfClass(
                JViewport.class, this));
        return (jvp == null ? null : jvp.getViewRect());
    }

    public Insets getAutoscrollInsets() {
        Insets insets = new Insets(0, 0, 0, 0);
        Rectangle rect = getTableRect();
        if (rect != null) {
            insets.top = rect.y + 20;
            insets.left = rect.x + 20;
            insets.bottom = getHeight() - (rect.y + rect.height) + 20;
            insets.right = getWidth() - (rect.x + rect.width) + 20;
        }
        return insets;
    }

    protected void scheduleViewportUpdate() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JScrollBar scrollBar;
                Point p;
                synchronized (this) {
                    p = viewLocation;
                }
                JScrollPane jsp = (JScrollPane) (
                        SwingUtilities.getAncestorOfClass(
                                JScrollPane.class, NodeTransferringTree.this));
                scrollBar = jsp.getHorizontalScrollBar();
                scrollBar.setValue(p.x);
                scrollBar = jsp.getVerticalScrollBar();
                scrollBar.setValue(p.y);
            }
        });
    }

    public synchronized void autoscroll(Point p) {
        int offset;
        Insets insets = getAutoscrollInsets();
        Rectangle rect = getTableRect();
        JViewport jvp = (JViewport) (SwingUtilities.getAncestorOfClass(
                JViewport.class, this));
        if (jvp != null) {
            Point oldLocation = jvp.getViewPosition();
            if (p.y < insets.top) {
                offset = getScrollableUnitIncrement(rect,
                        SwingConstants.VERTICAL, -1);
                viewLocation = new Point(oldLocation.x,
                        oldLocation.y - offset);
            }
            if (p.x < insets.left) {
                offset = getScrollableUnitIncrement(rect,
                        SwingConstants.HORIZONTAL, -1);
                viewLocation = new Point(oldLocation.x - offset,
                        oldLocation.y);
            }
            if (p.y > getHeight() - insets.bottom) {
                offset = getScrollableUnitIncrement(rect,
                        SwingConstants.VERTICAL, 1);
                viewLocation = new Point(oldLocation.x,
                        oldLocation.y + offset);
            }
            if (p.x > getWidth() - insets.right) {
                offset = getScrollableUnitIncrement(rect,
                        SwingConstants.HORIZONTAL, 1);
                viewLocation = new Point(oldLocation.x + offset,
                        oldLocation.y);
            }

            if (!(oldLocation.equals(viewLocation))) {
                scheduleViewportUpdate();
            }
        }
    }


    /**
     * Deletes the currently selected DefaultMutableTreeNode. The selected node must not be
     * the root. If it is, an IllegalStateException will be thrown.
     */
    public int deleteSelectedNode() {
        TreePath path = getSelectionPath();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                path.getLastPathComponent();
        if (node == getModel().getRoot()) {
            throw new IllegalStateException("Tried to delete root node. ");
        }
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode)
                node.getParent();
        DefaultMutableTreeNode next = null;
        int index = -1;

        if (node.getNextSibling() != null)
            next = node.getNextSibling();
        else if (node.getPreviousSibling() != null)
            next = node.getPreviousSibling();
        else
            next = parent;

        parent.remove(node);
        ((DefaultTreeModel) getModel()).nodeStructureChanged(parent);

        index = getNodeIndex(next);

        return index;
    }

    private DefaultMutableTreeNode[] copyNodes(Object[] inNodes) {
        DefaultMutableTreeNode[] dmtns = new DefaultMutableTreeNode[inNodes.length];

        for (int i = 0; i < inNodes.length; i++) {
            DefaultMutableTreeNode dmtn = copyNode((DefaultMutableTreeNode) inNodes[i]);
            dmtns[i] = dmtn;
        }

        return dmtns;
    }

    private DefaultMutableTreeNode copyNode(DefaultMutableTreeNode inNode) {
        DefaultMutableTreeNode returnNode = new
                DefaultMutableTreeNode();
        if (!(inNode.getUserObject() instanceof PubliclyCloneable)) {
            throw new IllegalStateException("Object is not publicly cloneable");
        }
        PubliclyCloneable object = (PubliclyCloneable)
                inNode.getUserObject();
        if (object != null) {
            ITSTreeNode n = (ITSTreeNode) object.clone();
            n.setTree(this);
            returnNode.setUserObject(object);
        }

        Enumeration childNodes = inNode.children();
        while (childNodes.hasMoreElements()) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode)
                    childNodes.nextElement();
            returnNode.add(copyNode(child));
        }

        return returnNode;
    }

    public void expandNode(TreeNode
            node) {
        if (node == null) return;

        int n = node.getChildCount();

        for (int i = 0; i < n; ++i) {
            expandNode(node.getChildAt(i));
        }

        // only expandPath for leafs to save some cpu
        // cycles. Check 'n' and isLeaf() to defeat
        // broken ifLeaf() implementations.
        if (n == 0 || node.isLeaf()) {
            if (node.getChildCount() == 0) {
                node = node.getParent();
            }
            if (node != null) {
                DefaultTreeModel model = (DefaultTreeModel) getModel();
                expandPath(new TreePath(model.getPathToRoot(node)));
            }
        }
    }

    public Hashtable
            getExpandedPaths(DefaultMutableTreeNode
            root) {
        Hashtable ht = new Hashtable(); // hashtable to hold all expanded node identifiers

        if (root == null) {
            return ht;
        }
        TreePath tp = new TreePath(root.getPath());
        ITSTreeNode n = (ITSTreeNode) root.getUserObject();

        if (isExpanded(tp)) {
            ht.put(n.get("NODEID"), n);
        }

        Enumeration eC = root.children();
        while (eC.hasMoreElements()) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) eC.nextElement();
            Hashtable htChild = getExpandedPaths(child);

            ht.putAll(htChild);
        }
        return ht;
    }

    public void expandNodes(DefaultMutableTreeNode
            root, Hashtable
            ht) {
        if (root == null) {
            return;
        }

        ITSTreeNode n = (ITSTreeNode) root.getUserObject();

        if (ht.containsKey(n.get("NODEID"))) {
            //System.out.println("Expand Node: " + n.getTitle());
            TreePath tp = new TreePath(root.getPath());

            expandNode(root);
            expandPath(tp);
        }

        Enumeration eC = root.children();
        while (eC.hasMoreElements()) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) eC.nextElement();
            expandNodes(child, ht);
        }
    }

    public void refreshNode(DefaultMutableTreeNode
            item) throws Exception {
        ITSTreeNode n = (ITSTreeNode) item.getUserObject();

        item.removeAllChildren();
        createChildren(item, n.get("NODEID"));
    }

    public void createChildren(DefaultMutableTreeNode parent, String ID) throws Exception {
    	
        Vector vNodes = its.CQL("SELECT <NODE> WHERE PARENTID = " + ID + " ORDER BY NODEINDEXWITHINPARENT ASC", 1, 10000);
        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode(n, true);

            parent.add(dmtn);
        }
        expandNode(parent);
        if (vNodes.size() != 0) ((DefaultTreeModel) getModel()).nodeStructureChanged(parent);
        //Node n = (Node) parent.getUserObject();
        //this.setSelectionPath(new TreePath(parent.getPath()));
    }

    private Clipboard
            getClipboard() {
        return its.clipboard;
    }

    private DropTargetListener
            getDropTargetListener() {
        return dropTargetListener;
    }

    private DropTarget
            getNodeDropTarget() {
        return nodeDropTarget;
    }

    private class TreeDropTargetListener implements DropTargetListener {

        /**
         * Called if the user has modified
         * the current drop gesture.
         */
        public void dropActionChanged(DropTargetDragEvent e) {
            if (!isDragAcceptable(e)) {
                e.rejectDrag();
            }
        }

        /**
         * The drag operation has departed
         * the <code>DropTarget</code> without dropping.
         */
        public void dragExit(DropTargetEvent e) {
            setSelectionPath(getOriginalSelectionPath());
        }

        /**
         * The drag operation has terminated
         * with a drop on this <code>DropTarget</code>.
         */
        public void drop(DropTargetDropEvent e) {
            if (!isDropAcceptable(e)) {
                e.rejectDrop();
            } else {
                e.acceptDrop(e.getDropAction());
                treeDragging = false;

                Transferable transferable = e.getTransferable();
                DefaultMutableTreeNode node = null;
                try {
                    node = ((DefaultMutableTreeNode[]) transferable.getTransferData(TreeNodeSelection.treeNodeFlavor))[0];
                } catch (Exception ex) {
                    e.dropComplete(false);
                    ex.printStackTrace(System.err);
                    throw new IllegalStateException("Tried to drop unsupported or unavailable object onto tree.");
                }
                Point location = e.getLocation();
                TreePath path = getPathForLocation(location.x, location.y);
                DefaultMutableTreeNode parent = (DefaultMutableTreeNode) path.getLastPathComponent();

                ITSTreeNode nNew = (ITSTreeNode) node.getUserObject();
                ITSTreeNode nInto = (ITSTreeNode) parent.getUserObject();

                // if attempting to drop a topic onto itself, or inside itself, do not comply.
                TreePath tp = new TreePath(parent.getPath());
                //System.out.println("is "+getOriginalSelectionPath()+" a descendant of "+tp+" ?");

                if (!isDropAcceptable(tp, nNew, nInto)) { return; }

                // new! prompt when dragging items - but (even newer) only if prompts are set in preferences
                if (ITS.getUser().PromptForDragAndDrop()) {
                    int i = JOptionPane.showConfirmDialog(null, "Drop topic "+nNew.get("NODETITLE")+" into topic "+nInto.get("NODETITLE")+"? ",
                            "Confirm", JOptionPane.YES_NO_OPTION);
                    if (i != 0) return;
                }

                try {
                    ITSTreeNode n = its.moveNode(nNew.get("LINKNODEID"), nInto.get("LINKNODEID"));

                    node.setUserObject(n);
                    parent.add(node);
                } catch (Exception ex) {
                    System.out.println("Error: could not save drag and drop information to the database.");
                    return;
                }

                ((DefaultTreeModel) getModel()).nodeStructureChanged(parent);

                e.dropComplete(true);
            }
            setSelectionPath(null);
        }

        /**
         * Called when a drag operation has
         * encountered the <code>DropTarget</code>.
         */
        public void dragEnter(DropTargetDragEvent e) {
            if (!isDragAcceptable(e)) {
                e.rejectDrag();
            } else {
                if (getOriginalSelectionPath() == null) {
                    setOriginalSelectionPath(getSelectionPath());
                    clearSelection();
                }
            }
        }

        /**
         * Called when a drag operation is ongoing
         * on the <code>DropTarget</code>.
         */
        public void dragOver(DropTargetDragEvent e) {
            if (!isDragAcceptable(e)) {
                clearSelection();
            } else {
                Point location = e.getLocation();
                setSelectionPath(getPathForLocation(location.x,
                        location.y));
            }
        }

        private boolean isDragAcceptable(DropTargetDragEvent e) {
            boolean returnValue = false;
            if (e.getDropAction() == DnDConstants.ACTION_COPY ||
                    e.getDropAction() == DnDConstants.ACTION_COPY_OR_MOVE
                    ||
                    e.getDropAction() == DnDConstants.ACTION_MOVE) {
                DataFlavor[] dataFlavors = e.getCurrentDataFlavors();
                for (int i = 0; i < dataFlavors.length; ++i) {
                    if (dataFlavors[i].equals(
                            TreeNodeSelection.treeNodeFlavor)) {
                        returnValue = true;
                    }
                }
            }

            return returnValue;
        }

        private boolean isDropAcceptable(TreePath tp, ITSTreeNode nNew, ITSTreeNode nInto) {
            TreePath orig = getOriginalSelectionPath();
            if (nInto.isLink()) {
                System.out.println("Linked topics may not contain descendents.");
                JOptionPane.showMessageDialog(
                        null, "Linked topics may not contain descendents.", "Error", JOptionPane.NO_OPTION);
                return false;
            }
            if (nNew.get("PARENTID").equals("-1")) {
                System.out.println("The root topic of a corpus may not be positioned elsewhere.");
                JOptionPane.showMessageDialog(
                        null, "The root topic of a corpus may not be positioned elsewhere.",
                        "Error", JOptionPane.NO_OPTION);
                return false;
            }
            if (nNew.get("NODEID").equals(nInto.get("NODEID"))) {
                System.out.println("You may not insert a topic into itself.");
                JOptionPane.showMessageDialog(
                        null, "You may not insert a topic into itself.", "Error", JOptionPane.NO_OPTION);
                return false;
            }

            return true;
        }

        private boolean isDropAcceptable(DropTargetDropEvent e) {
            boolean returnValue = false;
            if (e.getDropAction() == DnDConstants.ACTION_COPY ||
                    e.getDropAction() == DnDConstants.ACTION_COPY_OR_MOVE
                    ||
                    e.getDropAction() == DnDConstants.ACTION_MOVE) {
                DataFlavor[] dataFlavors = e.getCurrentDataFlavors();
                for (int i = 0; i < dataFlavors.length; ++i) {
                    if (dataFlavors[i].equals(
                            TreeNodeSelection.treeNodeFlavor)) {
                        returnValue = true;
                    }
                }
            }
            return returnValue;
        }

        private void setOriginalSelectionPath(TreePath path) {
            its.originalSelectionPath = path;
        }

        private TreePath getOriginalSelectionPath() {
            return its.originalSelectionPath;
        }
    }

    private class TreeDragSource implements DragSourceListener,
            DragGestureListener {
        private DefaultMutableTreeNode dragNode = null;

        public TreeDragSource() {
            DragSource dragSource = DragSource.getDefaultDragSource();
            dragSource.createDefaultDragGestureRecognizer(
                    NodeTransferringTree.this,
                    DnDConstants.ACTION_COPY_OR_MOVE,
                    this);
        }

        /**
         * Called as the hotspot enters a platform dependent drop site.
         * This method is invoked when the following conditions are
         true:
         * <UL>
         * <LI>The logical cursor's hotspot initially intersects
         * a GUI <code>Component</code>'s  visible geometry.
         * <LI>That <code>Component</code> has an active
         * <code>DropTarget</code> associated with it.
         * <LI>The <code>DropTarget</code>'s registered
         * <code>DropTargetListener</code> dragEnter() method is invoked
         and
         * returns successfully.
         * <LI>The registered <code>DropTargetListener</code> invokes
         * the <code>DropTargetDragEvent</code>'s acceptDrag() method to

         * accept the drag based upon interrogation of the source's
         * potential drop action(s) and available data types
         * (<code>DataFlavor</code>s).
         * </UL>
         */
        public void dragEnter(DragSourceDragEvent e) {
            setDragging(true);
        }

        /**
         * This method is invoked to signify that the Drag and Drop
         * operation is complete. The getDropSuccess() method of
         * the <code>DragSourceDropEvent</code> can be used to
         * determine the termination state. The getDropAction() method
         * returns the operation that the <code>DropTarget</code>
         * selected (via the DropTargetDropEvent acceptDrop() parameter)

         * to apply to the Drop operation. Once this method is complete,
         the
         * current <code>DragSourceContext</code> and
         * associated resources become invalid.
         */
        public void dragDropEnd(DragSourceDropEvent e) {
            if (e.getDropSuccess()) {
                if (e.getDropAction() == DnDConstants.ACTION_MOVE) {
                    ((DefaultTreeModel) getModel()
                            ).removeNodeFromParent(getDragNode());
                }
            }
        }

        /**
         * Called as the hotspot moves over a platform dependent drop
         site.
         * This method is invoked when the following conditions
         * are true:
         *<UL>
         *<LI>The cursor's logical hotspot has moved but still
         * intersects the visible geometry of the <code>Component</code>

         * associated with the previous dragEnter() invocation.
         * <LI>That <code>Component</code> still has a
         * <code>DropTarget</code> associated with it.
         * <LI>That <code>DropTarget</code> is still active.
         * <LI>The <code>DropTarget</code>'s registered
         * <code>DropTargetListener</code> dragOver() method
         * is invoked and returns successfully.
         * <LI>The <code>DropTarget</code> does not reject
         * the drag via rejectDrag()
         * </UL>
         */
        public void dragOver(DragSourceDragEvent e) {
            setDragging(true);
        }

        /**
         * Called when the user has modified the drop gesture.
         * This method is invoked when the state of the input
         * device(s) that the user is interacting with changes.
         * Such devices are typically the mouse buttons or keyboard
         * modifiers that the user is interacting with.
         */
        public void dropActionChanged(DragSourceDragEvent e) {
        }

        /**
         * Called as the hotspot exits a platform dependent drop site.
         * This method is invoked when the following conditions
         * are true:
         * <UL>
         * <LI>The cursor's logical hotspot no longer
         * intersects the visible geometry of the <code>Component</code>

         * associated with the previous dragEnter() invocation.
         * </UL>
         * OR
         * <UL>
         * <LI>The <code>Component</code> that the logical cursor's
         hotspot
         * intersected that resulted in the previous dragEnter()
         invocation
         * no longer has an active <code>DropTarget</code> or
         * <code>DropTargetListener</code> associated with it.
         * </UL>
         * OR
         * <UL>
         * <LI> The current <code>DropTarget</code>'s
         * <code>DropTargetListener</code> has invoked rejectDrag()
         * since the last dragEnter() or dragOver() invocation.
         * </UL>
         */
        public void dragExit(DragSourceEvent e) {
            setDragging(false);
        }

        /**
         * A <code>DragGestureRecognizer</code> has detected
         * a platform-dependent drag initiating gesture and
         * is notifying this listener
         * in order for it to initiate the action for the user.
         */
        public void dragGestureRecognized(DragGestureEvent e) {
            Point location = e.getDragOrigin();
            TreePath path = getPathForLocation(location.x, location.y
            );
            if (path != null && isPathSelected(path)) {
                DefaultMutableTreeNode node =
                        (DefaultMutableTreeNode)
                        path.getLastPathComponent();
                DefaultMutableTreeNode copy = copyNode(node);
                Transferable transferable = new TreeNodeSelection(copy);
                e.startDrag(null, transferable, this);
                setDragNode(node);
            }
        }

        private void setDragNode(DefaultMutableTreeNode node) {
            dragNode = node;
        }

        private DefaultMutableTreeNode getDragNode() {
            return dragNode;
        }
    }
}

/**
 * Supports clipboard and drag-and-drop operations for
 DefaultMutableTreeNodes.
 */
class TreeNodeSelection implements Transferable {
    private DefaultMutableTreeNode[] nodes;

    public static final DataFlavor treeNodeFlavor =
            new DataFlavor(DefaultMutableTreeNode.class, "Tree Node");
    private static DataFlavor[] flavors = {treeNodeFlavor};

    public TreeNodeSelection(DefaultMutableTreeNode inNode) {
        nodes = new DefaultMutableTreeNode[1];
        nodes[0] = inNode;
    }

    public TreeNodeSelection(DefaultMutableTreeNode[] inNodes) {
        nodes = inNodes;
    }

    public DataFlavor[] getTransferDataFlavors() {
        return flavors;
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor.equals(treeNodeFlavor);
    }

    public synchronized Object getTransferData(DataFlavor flavor)
            throws UnsupportedFlavorException {
        if (flavor.equals(treeNodeFlavor)) {
            return nodes;
        }
        throw new UnsupportedFlavorException(flavor);
    }
}
