package com.iw.system;

public class Language {
    private String DisplayName;
    private String Code;

    public Language (String DisplayName, String Code) {
       this.DisplayName = DisplayName; this.Code = Code;
    }

    public String getName() { return DisplayName; }
    public String getCode() { return Code; }

    public String toString() { return DisplayName; }
}
