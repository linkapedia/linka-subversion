package com.iw.tools;

import javax.swing.*;
import java.text.*;
import java.util.*;

public class SortedList extends JList {
    /**
     * Constructs a SortedList instance with an empty model.
     * Uses a Collator for the default Comparator.
     */
    private Comparator comparator;

    public SortedList() {
        this(new DefaultListModel(), Collator.getInstance());
    }

    /**
     * Constructs a SortedList with the listData. The SortedList
     * data model is sorted only if listData is sorted.
     *
     * @param listData a Vector of elements to store in the list
     * @param compare a Comparator to allow for sorting
     */
    public SortedList(Vector listData, Comparator compare) {
        this(new DefaultListModel(), compare);
        DefaultListModel model = (DefaultListModel) this.getModel();
        int size = listData.size();

        for (int x = 0; x < size; ++x) {
            model.addElement(listData.elementAt(x));
        }
    }

    /**
     * Constructs a SortedList with the listData. The SortedList
     * data model is sorted only if listData is sorted.
     *
     * @param listData an array of Objects to store in the list
     * @param compare a Comparator to allow for sorting
     */
    public SortedList(Object[] listData, Comparator compare) {
        this(new DefaultListModel(), compare);
        DefaultListModel model = (DefaultListModel) this.getModel();
        int size = listData.length;
        for (int x = 0; x < size; ++x) {
            model.addElement(listData[x]);
        }
    }

    /**
     * Constructs a SortedList with the provided non-null
     * DefaultListModel and Comparator
     * The SortedList is sorted only if listData is sorted.
     *
     * @param listData a DefaultListModel to store in the list
     * @param compare a Comparator to allow for sorting
     */
    public SortedList(DefaultListModel listData, Comparator compare) {
        super(listData);
        this.comparator = compare;
    }

    public SortedList(Vector v) {
        super(v);
    }

    /**
     * Sorts the elements of the SortedList using the collation
     * rules of the default Locale.
     *
     * Although the List (Vector) creation and model update appear
     * to be a lot of overhead, no model data is actually moving. Only
     * model element references are being copied.
     */
    public void sort() throws Exception {
        ListModel lm = getModel();
        DefaultListModel dlm = new DefaultListModel();

        for (int i = 0; i < lm.getSize(); i++) {
            dlm.addElement(lm.getElementAt(i));
        }

        qs(dlm);
        setModel(dlm);
        revalidate();
    }

    // the following three static functions are used to sort NodeDocument vectors by score
    public static void qs(DefaultListModel model) {

        DefaultListModel lml = new DefaultListModel();                // Left and right sides
        DefaultListModel lmr = new DefaultListModel();
        Object el;

        if (model.getSize() < 2) return;                      // 0 or 1= sorted

        Object o = (Object) model.getElementAt(0);

        // Start at element 1
        for (int i = 1; i < model.getSize(); i++) {
            el = (Object) model.getElementAt(i);
            if (el.toString().compareTo(o.toString()) > 0) lmr.addElement(el);
            else lml.addElement(el);
        }

        qs(lml);                                        // Recursive call left
        qs(lmr);                                        //    "        "  right
        lml.addElement(model.getElementAt(0));

        addModel(model, lml, lmr);
    }

    // Add two vectors together, into a destination Vector
    private static void addModel(DefaultListModel dest, DefaultListModel left, DefaultListModel right) {

        int i;
        dest.removeAllElements();                     // reset destination

        for (i = 0; i < left.getSize(); i++) dest.addElement(left.getElementAt(i));
        for (i = 0; i < right.getSize(); i++) dest.addElement(right.getElementAt(i));
    }
}
