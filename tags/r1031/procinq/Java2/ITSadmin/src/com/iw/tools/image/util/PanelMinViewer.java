package com.iw.tools.image.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedHashMap;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class PanelMinViewer extends JPanel implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8064215954964630215L;
	private Image image;
	private IPaint painter;
	private LinkedHashMap<String, String> mapInfo;

	public PanelMinViewer(Image image, LinkedHashMap<String, String> mapInfo,
			IPaint painter) {
		this.mapInfo = mapInfo;
		this.image = image;
		this.painter = painter;
		this.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

		this.addMouseListener(this);
	}

	public void setIpaint(IPaint painter) {
		this.painter = painter;
	}

	public void setImage(Image image) {
		this.image = image;
		repaint();
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(g);
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		this.painter.onPaint(image, mapInfo);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
		StringBuilder sb = new StringBuilder();
		if (!mapInfo.isEmpty()) {
			sb.append("<html><center><h2 style=\"color:blue\">INFORMATION</h2></center><br/>");
			for (String value : mapInfo.keySet()) {
				sb.append("<b>");
				sb.append(value).append(": ");
				sb.append("</b>");
				sb.append(mapInfo.get(value));
				sb.append("<br/>");
			}
			this.setToolTipText(sb.toString());
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

	}

}
