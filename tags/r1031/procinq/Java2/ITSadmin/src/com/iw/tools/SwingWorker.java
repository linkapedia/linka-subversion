package com.iw.tools;

import java.awt.*;
import javax.swing.*;
import java.util.EventObject;
import java.util.Vector;
import java.io.File;

/**
 * This is a variant of the SwingWorker
 * It works in conjunction with the GlassPane class
 * to allow users to execute timeconsuming task on a separate thread
 * The GlassPane addition can prevent users from executing another SwingWorker task
 * while one SwingWorker task is already executing
 *
 * @author Yexin Chen
 */
public abstract class SwingWorker {
    private Thread thread;
    public EventObject e;
    private Component oldGlass;
    public File f = null;

    /**
     * Class to maintain reference to current worker thread
     * under separate synchronization control.
     */
    private static class ThreadVar {
        private Thread thread;

        ThreadVar(Thread t) {
            thread = t;
        }

        synchronized Thread get() {
            return thread;
        }

        synchronized void clear() {
            thread = null;
        }
    }

    private ThreadVar threadVar;

    private GlassPane glassPane;
    private java.awt.Component aComponent;

    /**
     * Start a thread that will call the <code>construct</code> method
     * and then exit.
     * @param aComponent a reference to the UI component that's directly using
     * SwingWorker
     */
    public SwingWorker(EventObject e, File f) {
        this.f = f; this.e = e;
        Initialize();
    }

    public SwingWorker(EventObject e) {
        this.e = e;
        Initialize();
    }

    public void Initialize() {
        Component aComponent = (Component) e.getSource();
        setAComponent(aComponent);

        final Runnable doFinished = new Runnable() {
            public void run() {
                finished();
            }
        };

        Runnable doConstruct = new Runnable() {
            public void run() {
                try {
                    construct();
                } finally {
                    threadVar.clear();
                }

                // Execute the doFinished runnable on the Swing dispatcher thread
                SwingUtilities.invokeLater(doFinished);
            }
        };

        // Group the new worker thread in the same group as the "spawner" thread
        Thread t = new Thread(Thread.currentThread().getThreadGroup(), doConstruct);
        threadVar = new ThreadVar(t);
    }

    /**
     * Activate the capabilities of glasspane
     *
     */
    private void activateGlassPane() {
        // Mount the glasspane on the component window
        Vector vPanes = GlassPane.mount(getAComponent(), true);
        if (vPanes == null) return;
        if (vPanes.size() > 1) {
            oldGlass = (Component) vPanes.elementAt(1);
        }

        GlassPane aPane = (GlassPane) vPanes.elementAt(0);

        // keep track of the glasspane as an instance variable
        if (aPane != null) setGlassPane(aPane);

        if (getGlassPane() != null) {
            // Start interception UI interactions
            aPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            getGlassPane().setVisible(true);
            getGlassPane().transferFocus();
        }
    }

    /**
     * Enable the glass pane (to disable unwanted UI manipulation), then spawn the non-UI logic on a separate thread.
     */
    private void construct() {
        try {
            activateGlassPane();
            doNonUILogic();
        } catch (RuntimeException e) {
        }
    }

    /**
     * Deactivate the glasspane
     *
     */
    private void deactivateGlassPane() {
        if (getGlassPane() != null) {
            getGlassPane().clearListeners(getGlassPane());

            getGlassPane().unmount(getAComponent(), oldGlass);

            // Stop UI interception
            getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            getGlassPane().setVisible(false);
        }

    }

    /**
     * This method will be implemented by the inner class of SwingWorker
     * It should only consist of the logic that's unrelated to UI
     *
     * @throws java.lang.RuntimeException thrown if there are any errors in the non-ui logic
     */
    protected abstract void doNonUILogic() throws RuntimeException;

    /**
     * This method will be implemented by the inner class of SwingWorker
     * It should only consist of the logic that's related to UI updating, after
     * the doNonUILogic() method is done.
     *
     * @throws java.lang.RuntimeException thrown if there are any problems executing the ui update logic
     */
    protected abstract void doUIUpdateLogic() throws RuntimeException;

    /**
     * Called on the event dispatching thread (not on the worker thread)
     * after the <code>construct</code> method has returned.
     */
    protected void finished() {
        try {
            deactivateGlassPane();
            doUIUpdateLogic();
        } catch (RuntimeException e) {
            // Do nothing, simply cleanup below
            System.out.println("SwingWorker error" + e);
            e.printStackTrace(System.out);
        } finally {
            // Allow original component to get the focus
            if (getAComponent() != null) {
                getAComponent().requestFocus();
            }
        }
    }

    /**
     * Getter method
     *
     * @return java.awt.Component
     */
    protected Component getAComponent() {
        return aComponent;
    }

    /**
     * Getter method
     *
     * @return GlassPane
     */
    protected GlassPane getGlassPane() {
        return glassPane;
    }

    /**
     * A new method that interrupts the worker thread.  Call this method
     * to force the worker to stop what it's doing.
     */
    public void interrupt() {
        Thread t = threadVar.get();
        if (t != null) {
            t.interrupt();
        }
        threadVar.clear();
    }

    /**
     * Setter method
     *
     * @param newAComponent java.awt.Component
     */
    protected void setAComponent(Component newAComponent) {
        aComponent = newAComponent;
    }

    /**
     * Setter method
     *
     * @param newGlassPane GlassPane
     */
    protected void setGlassPane(GlassPane newGlassPane) {
        glassPane = newGlassPane;
    }

    /**
     * Start the worker thread.
     */
    public void start() {
        Thread t = threadVar.get();
        if (t != null) {
            t.start();
        }
    }
}
