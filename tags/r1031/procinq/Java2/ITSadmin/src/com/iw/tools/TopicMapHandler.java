package com.iw.tools;

import java.io.IOException;
import java.util.Hashtable;

import org.xml.sax.*;

import com.iw.system.Node;
import com.iw.system.Corpus;
import com.iw.system.ITS;
import com.iw.ui.TopicMapImport;
import com.iw.ui.PopupProgressBar;
import com.iw.ui.ITSAdministrator;

public class TopicMapHandler
        implements EntityResolver, DTDHandler, ContentHandler, ErrorHandler {

    private Node n = null; private Corpus c = null; ITS its = null;
    private String attributeName = ""; private boolean wroteCorpus = false;
    private String characters = ""; private PopupProgressBar ppb;
    int size = 0; int numberNodes = 0;

    public TopicMapHandler (ITSAdministrator itsframe) {
        super();
        this.its = itsframe.its;
    }

    public void setPopup(PopupProgressBar ppb, int numberNodes) {
        this.ppb = ppb; this.numberNodes = numberNodes;
    }

    public void destroyPopup() { ppb.dispose(); }

    public PopupProgressBar getProgress() { return ppb; }

    public Hashtable getAttributeHash(Attributes attributes) {
        Hashtable ht = new Hashtable();
        for (int i = 0; i < attributes.getLength(); i++) {
            String q = attributes.getQName(i);
            ht.put(q, attributes.getValue(q));
            //System.out.println("   attribute ("+q+") values ("+attributes.getValue(q)+")");
        }
        return ht;
    }

    public void startElement(String uri, String localName,
                             String qName, Attributes attributes)
            throws SAXException {
        //System.out.println("start.. URI: "+uri+" localName: "+localName+" qName: "+qName);
        Hashtable htAtts = getAttributeHash(attributes);

        // this tag indicates either a CORPUS or a NODE is being instantiated.
        if (qName.toUpperCase().equals("TOPICMAP")) { c = new Corpus(); }
        else if (qName.toUpperCase().equals("TOPIC")) {
            // if a new node is being instantiated yet we have not written the corpus to DB, do that first
            if (!wroteCorpus) {
                System.out.println("** WRITING CORPUS "+c.getName()+" TO DB **"); wroteCorpus = true;
                try { its.addCorpusFromTopicMap(c); }
                catch (Exception e) {
                    e.printStackTrace(System.out);
                    throw new SAXException("API layer error - could not insert corpus "+c.getName());
                }
            }
            n = new Node();
        } else if (qName.toUpperCase().equals("RESOURCEDATA")) {
            attributeName = (String) htAtts.get("id");
            return;
        }

        if (htAtts.containsKey("id")) {
            if (n == null) {
                //System.out.println("set: corpus id = "+(String) htAtts.get("id"));
                c.setID((String) htAtts.get("id"));
            } else {
                //System.out.println("set: node id = "+(String) htAtts.get("id"));
                n.set("NODEID", (String) htAtts.get("id"));
            }
        }

        attributeName = qName.toUpperCase();
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        // write attribute
        if (characters.length() > 0) {
            if (n == null) {
                //System.out.println("set (corpus) "+attributeName+" = "+characters);
                c.set(attributeName.toUpperCase(), characters);
            } else {
                //System.out.println("set (node) "+attributeName+" = "+characters);
                n.set(attributeName.toUpperCase(), characters);
            }
            characters = "";
        }

        if (qName.toUpperCase().equals("TOPIC")) {
            size++; System.out.println("** WRITING NODE "+n.get("NODETITLE")+" TO DB **");
            try { its.addNodeFromTopicMap(n); }
            catch (Exception e) {
                e.printStackTrace(System.out);
                throw new SAXException("API layer error - could not insert node "+n.get("NODETITLE"));
            }

            if (ppb != null) { updateProgressBar(size); }
        }
        //System.out.println("end element: "+uri+" "+localName+" "+qName);
        // no op
    }

    public void updateProgressBar (int size) {
        float score = 100*((float) size/(float) numberNodes);
        System.out.println("updating progress bar with: "+(int) score);
        ppb.setProgress((int) score);
    }

    public void characters(char ch[], int start, int length)
            throws SAXException {
        String s = new String("");

        for (int i = start; i < start+length; i++) {
            s = s + ch[i];
        }
        characters = characters + s.trim();
    }

    public InputSource resolveEntity(String publicId, String systemId)
// 	throws IOException, SAXException
            throws SAXException {
        //System.out.println("Resolve entity: "+publicId+" "+systemId);
        return null;
    }

    public void notationDecl(String name, String publicId, String systemId)
            throws SAXException {
        //System.out.println("notation Dec1: "+name+" "+publicId+" "+systemId);
        // no op
    }

    public void unparsedEntityDecl(String name, String publicId,
                                   String systemId, String notationName)
            throws SAXException {
        //System.out.println("unparsed Entity Dec1: "+name+" "+publicId+" "+systemId+" "+notationName);
        // no op
    }

    public void setDocumentLocator(Locator locator) {
        //System.out.println("set document locator: "+locator.toString());
        // no op
    }

    public void startDocument()
            throws SAXException {
        //System.out.println("start document");
        // no op
    }

    public void endDocument()
            throws SAXException {
        //System.out.println("end document");
        // no op
    }

    public void startPrefixMapping(String prefix, String uri)
            throws SAXException {
        //System.out.println("start prefix mapping: "+prefix+" "+uri);
        // no op
    }

    public void endPrefixMapping(String prefix)
            throws SAXException {
        //System.out.println("end prefix mapping: "+prefix);

        // no op
    }

    public void ignorableWhitespace(char ch[], int start, int length)
            throws SAXException {
        // no op
    }

    public void processingInstruction(String target, String data)
            throws SAXException {
        //System.out.println("procesing instruction: "+target+" "+data);
        // no op
    }

    public void skippedEntity(String name)
            throws SAXException {
        //System.out.println("skipped entity: "+name);
        // no op
    }

    public void warning(SAXParseException e)
            throws SAXException {
        System.out.println("Warning! ");
        e.printStackTrace(System.out);
        // no op
    }

    public void error(SAXParseException e)
            throws SAXException {
        System.out.println("Error! ");
        e.printStackTrace(System.out);
        // no op
    }

    public void fatalError(SAXParseException e)
            throws SAXException {
        System.out.println("Fatal Error! ");
        e.printStackTrace(System.out);
        throw e;
    }
}
