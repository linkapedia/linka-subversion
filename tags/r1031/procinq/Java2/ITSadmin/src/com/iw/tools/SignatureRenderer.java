/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Feb 18, 2004
 * Time: 5:24:04 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.tools;

import org.jfree.chart.renderer.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.data.CategoryDataset;

import java.awt.geom.Rectangle2D;
import java.awt.*;

public class SignatureRenderer extends BarRenderer {
    int selectedIndex = -1;

    public SignatureRenderer(int index) { super(); selectedIndex = index; }
    public void drawItem(Graphics2D d, CategoryItemRendererState state, Rectangle2D d1, CategoryPlot plot, CategoryAxis axis, ValueAxis axis1, CategoryDataset dataset, int i, int i1) {
        if (i1 == selectedIndex) { setSeriesPaint(0, Color.ORANGE ); }
        else { setSeriesPaint(0, Color.BLUE); }
        super.drawItem(d, state, d1, plot, axis, axis1, dataset, i, i1);
    }
    public void setSelectedIndex(int index) { selectedIndex = index; }
}
