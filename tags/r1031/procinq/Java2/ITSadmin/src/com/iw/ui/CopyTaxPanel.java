/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Nov 6, 2003
 * Time: 1:40:38 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.oculustech.layout.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;

import com.iw.tools.SortedList;
import com.iw.tools.SwingWorker;

public class CopyTaxPanel extends JPanel implements ActionListener {
    public int iButtonPressed;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;

    // Instance attributes used in this example
    protected JInternalFrame MDIframe;
    protected JPanel openPanel;

    protected JLabel taxLabel;
    protected JTextField taxText;
    protected JLabel sigText;
    protected JCheckBox sigCheck;
    protected JLabel sourceLabel;
    protected JCheckBox sourceBox;
    protected JLabel docLabel;
    protected JCheckBox docCheck;
    protected JButton okButton;
    protected JButton cancelButton;

    private Corpus c = null;

    public CopyTaxPanel() throws Exception { }
    public CopyTaxPanel(ITSAdministrator frame, Corpus c) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        this.c = c;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Copy Taxonomy", false, true, false, true);
        MDIframe.setLocation(70, 70);
        MDIframe.setSize(505, 110);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        taxLabel = new JLabel("New Taxonomy:");
        taxLabel.setToolTipText("Please enter the name of the new taxonomy.");

        taxText = new JTextField("Copy of "+c.getName());
        taxText.setToolTipText("Please enter the name of the new taxonomy.");

        sigText = new JLabel("Copy Signatures:");
        sigText.setToolTipText("Check this box if you would like to duplicate signatures.");

        sigCheck = new JCheckBox();
        sigCheck.setSelected(true);
        sigCheck.setToolTipText("Check this box if you would like to duplicate signatures.");

        sourceLabel = new JLabel("Copy Source:");
        sourceLabel.setToolTipText("Check this box if you would like to duplicate topic source.");

        sourceBox = new JCheckBox();
        sourceBox.setSelected(true);
        sourceBox.setToolTipText("Check this box if you would like to duplicate topic source.");

        docLabel = new JLabel("Copy Documents:");
        docLabel.setToolTipText("Check this box if you would like to duplicate document relationships.");

        docCheck = new JCheckBox();
        docCheck.setSelected(true);
        docCheck.setToolTipText("Check this box if you would like to duplicate document relationships.");

        okButton = new JButton("OK");
        okButton.setSelected(true);
        okButton.setToolTipText("Copy this taxonomy now.");
        okButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setToolTipText("Cancel this operation.");
        cancelButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(taxLabel);
                    layout.add(taxText);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(sigText);
                    layout.add(sigCheck);
                    layout.addSpace(20);
                    layout.add(sourceLabel);
                    layout.add(sourceBox);
                    layout.addSpace(20);
                    layout.add(docLabel);
                    layout.add(docCheck);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(okButton);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(okButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == cancelButton) {
            MDIframe.dispose();
        }

        if (event.getSource() == okButton) {
            SwingWorker aWorker = new SwingWorker(event) {

                protected void doNonUILogic() {
                    ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    MDIframe.getGlassPane().setVisible(true);

                    PopupProgressBar ppb = new PopupProgressBar(ITSframe, 0, 100);
                    ppb.show();

                    c.setName(taxText.getText());

                    try { its.copyCorpus(c, sigCheck.isSelected(), sourceBox.isSelected(), docCheck.isSelected(), ppb); }
                    catch (Exception e) {
                        e.printStackTrace(System.out);
                        JOptionPane.showMessageDialog(ITSframe, "Failure! An error was encountered during corpus replication.",
                            "Error", JOptionPane.NO_OPTION);
                        return;
                    }

                    ppb.dispose();

                    JOptionPane.showMessageDialog(ITSframe, "The taxonomy that you selected have been copied successfully.",
                        "Information", JOptionPane.NO_OPTION);

                    MDIframe.dispose();
                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    MDIframe.getGlassPane().setVisible(false);
                }
            };
            aWorker.start();
        }
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }
}
