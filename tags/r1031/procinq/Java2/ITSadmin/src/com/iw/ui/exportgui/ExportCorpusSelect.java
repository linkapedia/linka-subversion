package com.iw.ui.exportgui;

import com.iw.ui.*;
import com.iw.system.ExportElement;
import com.iw.system.Corpus;
import com.iw.tools.SwingWorker;
import com.oculustech.layout.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;

public class ExportCorpusSelect extends OpenTaxonomy {
    private ExportElement ee = null;

    public ExportCorpusSelect() throws Exception { }
    public ExportCorpusSelect(ITSAdministrator frame, ExportElement ee) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        this.ee = ee;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Export Taxonomy", false, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(575, 280);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        // Create the data model for this example
        listData = frame.its.getCorpora();

        listbox = new JList(listData);
        listbox.setFixedCellWidth(440);
        // listbox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // MH Change
        
        listbox.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
       
        
        listbox.addKeyListener(this);
        listbox.setSelectedIndex(0);

        openButton = new JButton("Export");
        openButton.setMnemonic('e');
        openButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setMnemonic('c');
        cancelButton.addActionListener(this);

        scrollPane = new JScrollPane();
        scrollPane.getViewport().add(listbox);

        layout.addSpace(15);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(scrollPane, OculusLayout.NO_STRETCH, OculusLayout.WANT_STRETCHED);
            layout.addSpace(15);
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(openButton);
            layout.addSpace(2);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(5);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(openButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == openButton) {
            if (listbox.getSelectedValue() == null) return;
            //MDIframe.dispose();

            SwingWorker aWorker = new SwingWorker(event) {
                private File f = null;

                protected void doNonUILogic() {
                    ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    MDIframe.getGlassPane().setVisible(true);
                    
                    // start to go through multiselect
                    Object selected[] = listbox.getSelectedValues();
                    for (int i=selected.length - 1; i >=0;--i)
                    {
                        PopupProgressBar ppb = new PopupProgressBar(ITSframe, 0, 100);
                        ppb.show();
                        Corpus PassedCorpus  = (Corpus) selected[i];
                        
                        // rem - mh change PassedCorpus = (Corpus) listbox.getSelectedValue();
                        //PassedCorpus = (Corpus) listbox.getSelectedValues();
                                           
                                        
                        // mh CHANGE try { f = ee.exportTaxonomy(ITSframe.its, (Corpus) listbox.getSelectedValue(), ppb); }
                        try { f = ee.exportTaxonomy(ITSframe.its, PassedCorpus, ppb); }
                        catch (Exception e) {
                            JOptionPane.showMessageDialog(ITSframe, "Error exporting taxonomy, see log file for more details.",
                                    "Error", JOptionPane.NO_OPTION);
                            e.printStackTrace(System.err);
                        }

                        ppb.dispose();
                    	
                    }
                    
                    
                    
//                    PopupProgressBar ppb = new PopupProgressBar(ITSframe, 0, 100);
//                    ppb.show();
//                    Corpus PassedCorpus  = new Corpus();
                    // rem - mh change PassedCorpus = (Corpus) listbox.getSelectedValue();
                    //PassedCorpus = (Corpus) listbox.getSelectedValues();
                                       
                                    
                    // mh CHANGE try { f = ee.exportTaxonomy(ITSframe.its, (Corpus) listbox.getSelectedValue(), ppb); }
 //                   try { f = ee.exportTaxonomy(ITSframe.its, PassedCorpus, ppb); }
 //                   catch (Exception e) {
 //                       JOptionPane.showMessageDialog(ITSframe, "Error exporting taxonomy, see log file for more details.",
 //                               "Error", JOptionPane.NO_OPTION);
 //                       e.printStackTrace(System.err);
 //                   }

 //                   ppb.dispose();

                    if (f == null)
                        JOptionPane.showMessageDialog(ITSframe, "Error exporting taxonomy, see log file for more details.",
                                "Error", JOptionPane.NO_OPTION);
                    else
                        JOptionPane.showMessageDialog(ITSframe, "Taxonomy successfully exported to: "+f.getAbsolutePath(),
                                "Information", JOptionPane.NO_OPTION);
                }
                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();

                    ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    MDIframe.getGlassPane().setVisible(false);
                }
            };
            aWorker.start();
        }
        if (event.getSource() == cancelButton) MDIframe.dispose();

    }
}
