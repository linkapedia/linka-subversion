package com.iw.tools;

import java.util.ArrayList;
import java.util.List;

public class BingInfoBean {

	private String numErrors=" - ";
	private String numSuccess=" - ";
	private List<String> termsErrorsList = new ArrayList<String>();
	
	public String getNumErrors() {
		return numErrors;
	}
	public void setNumErrors(String numErrors) {
		this.numErrors = numErrors;
	}
	public String getNumSuccess() {
		return numSuccess;
	}
	public void setNumSuccess(String numSuccess) {
		this.numSuccess = numSuccess;
	}
	public List<String> getTermsErrorsList() {
		return termsErrorsList;
	}
	public void setTermsErrosList(List<String> termsErrorsList) {
		this.termsErrorsList = termsErrorsList;
	}
	
	
	
	
	
}
