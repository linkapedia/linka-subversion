package com.iw.ui;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.oculustech.layout.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.File;
import java.beans.*;

import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.ui.explain.*;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;

public class SignatureTestPanel extends JPanel implements PropertyChangeListener, ActionListener {
    public ITS its = null;
    public ITSAdministrator ITSframe = null;

    private boolean bDone = false;
    private Vector vNodeDocuments;
    private NodeDocument defaultND;

    private JInternalFrame MDIframe;
    private ITSTreeNode thisNode;
    private Vector signatures;
    private boolean post = false;

    protected JLabel topicLabel;
    protected JLabel topicLabel2;
    protected JProgressBar progressBar;
    protected RowColorJTable resultsTable;
    protected JButton explainButton;
    protected JButton closeButton;
    protected JButton postButton;

    public SignatureTestPanel(ITSAdministrator frame, ITSTreeNode n, Vector NodeDocuments,
                              Vector signatures) {
        its = frame.its; ITSframe = frame;
        vNodeDocuments = NodeDocuments;
        initializePanel(frame, n, NodeDocuments, signatures);
    }
    public SignatureTestPanel(ITSAdministrator frame, ITSTreeNode n, Vector NodeDocuments,
                              Vector signatures, boolean postResults) {
        its = frame.its; ITSframe = frame;
        vNodeDocuments = NodeDocuments;
        post = postResults;
        initializePanel(frame, n, NodeDocuments, signatures);
    }

    public void initializePanel(ITSAdministrator frame, ITSTreeNode n, Vector NodeDocuments, Vector signatures) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp; thisNode = n; this.signatures = signatures;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Signature Test Results: "+n.get("NODETITLE"), true, true, true, true);
        MDIframe.setLocation(80, 80);
        MDIframe.setSize(675, 580);
        MDIframe.setBackground(Color.lightGray);
        MDIframe.setFrameIcon(frame.iIndraweb);

        topicLabel = new JLabel("Topic Title:");
        topicLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));
        topicLabel2 = new JLabel(n.get("NODETITLE"));

        progressBar = new JProgressBar();
        progressBar.setToolTipText("Completion status of signature testing.");
        progressBar.setBorderPainted (true);
        progressBar.setStringPainted(true);

        resultsTable = new RowColorJTable(2, ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS.intValue(), (Object) new String(""));
        resultsTable.addCustomRenderer(3);

        explainButton = new JButton("Explain");
        explainButton.setToolTipText("Explain this result.");
        explainButton.addActionListener(this);

        postButton = new JButton("Post");
        postButton.setToolTipText("Post results of this classify to the database.");
        postButton.addActionListener(this);

        closeButton = new JButton("Close");
        closeButton.addActionListener(this);

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(topicLabel);
                    layout.add(topicLabel2);
                }
                layout.parent();
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.add(progressBar);
                        layout.add(new JScrollPane(resultsTable));
                    }
                    layout.parent();
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.add(explainButton);
                        layout.addSpace(10);
                        layout.add(postButton);
                        layout.addSpace(10);
                        layout.add(closeButton);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
            layout.addSpace(10);
        }
        layout.addSpace(10);
        layout.parent();

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(explainButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
        MDIframe.toFront();

        if (n.get("NODESIZE").equals("0")) {
            int i = JOptionPane.showConfirmDialog(MDIframe, "Warning: The size attribute of the topic "+n.get("NODETITLE")+
                " is currently set to 0.  Your documents will not classify properly.  Continue?",
                "Confirm", JOptionPane.YES_NO_OPTION);
            if (i != 0) { MDIframe.dispose(); return; }
        }

        MDIframe.addPropertyChangeListener(this);
        MDIframe.setClosable(false);
        MDIframe.removePropertyChangeListener(this);
        MDIframe.setClosable(true);
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void invokeExplain() throws Exception {
        int row = resultsTable.getSelectedRow();
        if ( row == -1 ) {
            JOptionPane.showMessageDialog(ITSframe, "A topic (row) must be selected.", "Information", JOptionPane.NO_OPTION);
            return;
        }

        Document d = (Document) resultsTable.getModel().getValueAt(row, 0);

        OculusBox ob = new OculusBox();

        // pop up explain frame is an internal MDI frame
        JInternalFrame jifExplainDisplay = new JInternalFrame("Explain Report for Node " + defaultND.get("NODEID") +
                " Doc " + d.get("DOCTITLE") + d.get("DOCUMENTID"), true, true, true, true);
        jifExplainDisplay.setLocation(60, 60);
        jifExplainDisplay.setSize(600, 500);  // hbk control
        //Dimension winsize = this.getSize(), screensize =
        //    Toolkit.getDefaultToolkit().getScreenSize();
        //setSize((int) (screensize.width / 1.5),  (int) (screensize.height / 1.25));
        jifExplainDisplay.setResizable(true);
        jifExplainDisplay.setBackground(Color.lightGray);
        jifExplainDisplay.setFrameIcon(ITSframe.iIndraweb);

        ExplainDisplayDataContainer explainDisplayDataContainer = null;

        /* ITSTreeNode n, int iDocIDSource,
            String sDocTitle, ITSAdministrator itsAdminFrame,
            Vector vSignatures, boolean bClassifyDocument)
            */

        try {
            explainDisplayDataContainer = its.getExplainData(
                                            thisNode,
                                            new Integer(d.get("DOCUMENTID")).intValue(),
                                            d.get("DOCTITLE"), ITSframe, signatures, ITS.getUser().modeClassifyDocument());
        } catch (EmptyCorpusToUseList e) {
            JOptionPane.showMessageDialog(ITSframe, "Taxonomy list not set.  Please configure your classification "+
                    "preferences and try your classification again.",
                    "Error", JOptionPane.NO_OPTION);
            Preferences p = new Preferences(ITSframe);
            p.show();

            return;
        }

        if ( explainDisplayDataContainer == null )
            return;

        ExplainDisplayPane explainDisplayPane = new ExplainDisplayPane(ITSframe, explainDisplayDataContainer, jifExplainDisplay);
        jifExplainDisplay.setContentPane(ob);
        jifExplainDisplay.show();
        jifExplainDisplay.getContentPane().add(explainDisplayPane);

        explainDisplayPane.setVisible(true);
        explainDisplayPane.show();

        //jIntFMDI.setMaximum(true );
        ITSframe.jdp.add(jifExplainDisplay);
        //jIntFMDI.setMaximum(true);
        jifExplainDisplay.show();
        jifExplainDisplay.setVisible(true);
        jifExplainDisplay.toFront();
    }

    // add old and new result to the result table
    public void addResult(NodeDocument ndBefore, NodeDocument ndAfter) {
        if (resultsTable.getRowCount() == 0) {
            final DefaultSortTableModel dtm = new DefaultSortTableModel(0, 4);
            Vector v = new Vector();
            v.add("Document Title");
            v.add("Gist");
            if (!post) v.add("Old Score");
            if (!post) { v.add("New Score"); } else { v.add("Score"); }
            dtm.setColumnIdentifiers(v);

            resultsTable.setModel(dtm);
        }
        defaultND = ndBefore;

        DefaultSortTableModel dtm = (DefaultSortTableModel) resultsTable.getModel();
        Vector v = new Vector();
        v.add(ndBefore.getDocProps());
        if (ndAfter == null) { v.add(ndBefore.get("DOCSUMMARY")); }
        else { v.add(ndAfter.get("DOCSUMMARY")); }
        if (!post) v.add(ndBefore.get("SCORE1"));
        if (ndAfter == null) {
            v.add("0.0"); }
        else { v.add(ndAfter.get("SCORE1")); }

        dtm.addRow(v);
        dtm.sortColumn(2, false);
    }

    // add old and new result to the result table
    public void addFailure(NodeDocument ndBefore) {
        if (resultsTable.getRowCount() == 0) {
            final DefaultSortTableModel dtm = new DefaultSortTableModel(0, 4);
            Vector v = new Vector();
            v.add("Document Title");
            v.add("Gist");
            if (!post) v.add("Old Score");
            if (!post) { v.add("New Score"); } else { v.add("Score"); }
            dtm.setColumnIdentifiers(v);

            resultsTable.setModel(dtm);
        }
        defaultND = ndBefore;

        DefaultSortTableModel dtm = (DefaultSortTableModel) resultsTable.getModel();
        Vector v = new Vector();
        v.add(ndBefore.getDocProps());

        v.add(ndBefore.get("DOCSUMMARY"));
        if (!post) v.add(ndBefore.get("SCORE1"));
        v.add("-1.0");

        dtm.addRow(v);
        dtm.sortColumn(2, false);
    }

    // foreach document:
    //  1 - classify the document against the new signature set
    //  2 - add the result to the table
    //  3 - update the progress bar
    public void propertyChange(PropertyChangeEvent event) {
        SwingWorker aWorker = new SwingWorker(event) {
            private boolean bSuccess = true;
            private String failMessage = "";

            protected void doNonUILogic() {
                try {
                    //System.out.println("Found "+vNodeDocuments.size()+" node documents.");
                    for (int i = 0; i < vNodeDocuments.size(); i++) {
                        NodeDocument nd = (NodeDocument) vNodeDocuments.elementAt(i);

                        // re-classify document
                        nd.set("NODESIZE", thisNode.get("NODESIZE"));
                        NodeDocument ndResult = null;

                        try {
                            ndResult = its.classify(nd, signatures);

                            // add old and new result to the result table
                            addResult(nd, ndResult);
                        } catch (EmptyCorpusToUseList e) {
                            failMessage = "Taxonomy list not set.  Please configure your classification "+
                                "preferences and try your classification again."; bSuccess = false; throw e;
                        } catch (ClassifyError e) {
                            System.out.println("Warning! Classification error!");
                            addFailure(nd);
                        } catch (Exception e) {
                            System.out.println("Classify had three failures, aborting!");
                            addFailure(nd);
                        }

                        // update the progress bar
                        int progress = (100*(i+1))/vNodeDocuments.size();
                        progressBar.setValue(progress);
                    }
                    // EDIT: undo signatures is no longer necessary
                    //its.saveSignatures(thisNode.getID(), undoSignatures);
                }
                catch (Exception e) {
                    e.printStackTrace(System.out); failMessage = e.getMessage(); bSuccess = false;
                } finally { bDone = true; }
            }
            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                MDIframe.getGlassPane().setVisible(false);

                if (!bSuccess) {
                    JOptionPane.showMessageDialog(ITSframe, "Could not test signatures for this topic: "+failMessage+".",
                        "Error", JOptionPane.NO_OPTION);
                    MDIframe.dispose();
                }
            }
        };
        aWorker.start();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(closeButton)) { MDIframe.dispose(); return; }
        if (e.getSource().equals(postButton)) {
            try {
                SwingWorker aWorker = new SwingWorker(e) {
                    boolean bSuccess = true;

                    protected void doNonUILogic() {
                        try {
                            int[] rows = resultsTable.getSelectedRows();
                            Vector nodeDocuments = new Vector();

                            for (int i = 0; i < rows.length; i++) {
                                Document d = (Document) resultsTable.getModel().getValueAt(rows[i], 0);
                                NodeDocument nd = new NodeDocument(d, thisNode);
                                nd.set("DOCSUMMARY", (String) resultsTable.getModel().getValueAt(rows[i], 1));
                                if (post) nd.set("SCORE1", (String) resultsTable.getModel().getValueAt(rows[i], 2));
                                else  nd.set("SCORE1", (String) resultsTable.getModel().getValueAt(rows[i], 3));

                                if (Double.parseDouble(nd.get("SCORE1")) > 0.0) its.addNodeDocument(nd);
                            }

                            if (rows.length == 0) {
                                JOptionPane.showMessageDialog(ITSframe, "Sorry, you must first select one or more documents.",
                                    "Information", JOptionPane.NO_OPTION);
                                bSuccess = false;
                                return;
                            }
                        }
                        catch (Exception e) { e.printStackTrace(System.out); }
                    }
                    protected void doUIUpdateLogic() {}

                    public void finished() {
                        super.finished();
                        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        getGlassPane().setVisible(false);

                        if (bSuccess) {
                            JOptionPane.showMessageDialog(ITSframe, "Document relationships added successfully.",
                                "Information", JOptionPane.NO_OPTION);
                            resultsTable.clearSelection();
                        }
                    }
                };
                aWorker.start();

            } catch (Exception ex) { ex.printStackTrace(System.out); }

        }
        if (e.getSource().equals(explainButton)) {
            try {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        try { invokeExplain(); }
                        catch (Exception e) { e.printStackTrace(System.out); }
                    }
                    protected void doUIUpdateLogic() {}

                    public void finished() {
                        super.finished();
                        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        getGlassPane().setVisible(false);
                    }
                };
                aWorker.start();

            } catch (Exception ex) { ex.printStackTrace(System.out); }
        }
    }
}
