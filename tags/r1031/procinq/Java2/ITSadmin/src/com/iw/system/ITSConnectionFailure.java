package com.iw.system;

public class ITSConnectionFailure extends Exception {
    public ITSConnectionFailure() { super(); }
    public ITSConnectionFailure(String message) { super(message); }
}
