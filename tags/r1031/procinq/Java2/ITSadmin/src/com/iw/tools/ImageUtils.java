package com.iw.tools;

import javax.swing.*;
import java.net.URL;
import java.awt.image.ImageProducer;
import java.awt.*;

public class ImageUtils {

    public static ImageIcon getImage(Component frame, String filename) {
        ImageIcon image = null;
        filename = "/" + filename;
        //path.
        //filename="/"+filename;
        URL url = frame.getClass().getResource(filename);
        try {
            image = new ImageIcon(frame.createImage((ImageProducer) url.getContent()));
        } catch (Exception e) {
            image = new ImageIcon(filename);
        }
        return image;
    }
}
