/*
 * GUI : Admin the server to connect bd sqlite
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
*/

package com.iw.tools.webdav.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.iw.tools.webdav.sqlite.SqlServerAdmin;




public class ServerAdmin extends JDialog implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8313921971957672580L;
	private JButton btnAdd,
			btnDelete,
			btnEdit,
			btnExplorar;
	private JTable table;
	private JPanel panel,
			panelButtons;
			
	private JScrollPane scrollPane;
	private JLabel lblHostName,
			lblUser,
			lblPassword,
			lblPath,
			lblName;
	private JTextField txtHostName,
			txtUser,
			txtPath,
			txtName;
	private JPasswordField txtPassword;
	private String[] headers ={"Name","HostName","User","Password","Path"};
	private DefaultTableModel modelTable;
	public static boolean cancel;	
	
	private String pass ;
	
	/**
	 * Constructor
	 */
	public ServerAdmin(){
		cancel=true;
		//Buttons 
		panelButtons = new JPanel();
		btnAdd = new JButton("Add");
		btnDelete = new JButton("Delete");
		btnEdit = new JButton("Update");
		btnExplorar = new JButton("Explore");
		panelButtons.add(btnAdd);
		panelButtons.add(btnDelete);
		panelButtons.add(btnEdit);
		panelButtons.add(btnExplorar);
		
		btnAdd.addActionListener(this);
		btnDelete.addActionListener(this);
		btnEdit.addActionListener(this);
		btnExplorar.addActionListener(this);
		//controls
        
        panel = new JPanel(new GridLayout(5, 2));
        lblName= new JLabel("Name: ");
		txtName= new JTextField(40);
        lblHostName= new JLabel("Hostname: ");
		lblUser=new JLabel("User: ");
		lblPassword=new JLabel("Password: ");
		lblPath=new JLabel("Path: ");
        txtHostName=new JTextField("http://",40);
		txtUser=new JTextField(40);
		txtPassword=new JPasswordField(40);
		//pass = new String(txtPassword.getPassword());
		txtPath=new JTextField("/",30);
		txtHostName.setToolTipText("Example: http://www.dav.dropdav.com/ (NOTE: finished with \"/\")");
		txtUser.setToolTipText("Especify the user if exists");
		txtPassword.setToolTipText("Especify the Password if exits");
		txtPath.setToolTipText("Path of folder");
		txtName.setToolTipText("Identity resource");
		panel.add(lblName);
		panel.add(txtName);
		panel.add(lblHostName);
		panel.add(txtHostName);
		panel.add(lblUser);
		panel.add(txtUser);
		panel.add(lblPassword);
		panel.add(txtPassword);
		panel.add(lblPath);
		panel.add(txtPath);
	
		panel.setBorder(BorderFactory.createEmptyBorder(10, 100, 10, 100));
		
		//table
		modelTable = new DefaultTableModel(headers,0){
			/**
			 * 
			 */
			private static final long serialVersionUID = -7901820575397587554L;

			public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
		};
		table = new JTable(modelTable);
		table.setSelectionMode(0);
		//event jtable
		table.addMouseListener(new MouseAdapter() 
		   {
		      public void mouseClicked(MouseEvent e) 
		      {
		         int fila = table.rowAtPoint(e.getPoint());
		         int columna = table.columnAtPoint(e.getPoint());
		         if ((fila > -1) && (columna > -1)){
		        	 columna=0;
		        	 txtName.setText(modelTable.getValueAt(fila,columna).toString());
		        	 txtHostName.setText(modelTable.getValueAt(fila,columna+1).toString());
			         txtUser.setText(modelTable.getValueAt(fila,columna+2).toString());
			         txtPassword.setText("");
			         txtPath.setText(modelTable.getValueAt(fila,columna+4).toString());
		         }
		             
		      }
		   });
		//////////////////////////////////////////////////////////////////////////
		scrollPane = new JScrollPane(table);
		
		scrollPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		scrollPane.setPreferredSize(new Dimension(500,150));
		scrollPane.revalidate();
		//load table info
		try {
			loadDates();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//add
		getContentPane().add(panelButtons,BorderLayout.NORTH);
		getContentPane().add(panel,BorderLayout.CENTER);
		getContentPane().add(scrollPane,BorderLayout.SOUTH);
		setTitle("Server Admin");
		getRootPane().setDefaultButton(btnExplorar);
		setSize(550, 350);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocation(300, 100);
		setModal(true);
		setVisible(true);
		
	}
	
	/**
	 * load date from sqlite to the JJable
	 * @throws SQLException
	 */
	public void loadDates() throws SQLException{
		while (modelTable.getRowCount ()> 0) { 
			 modelTable.removeRow(0); 
		}
		SqlServerAdmin ssa = new SqlServerAdmin();
		ResultSet res = ssa.consult();
		while (res.next())
		{
		   Object [] fila = new Object[5];

		   for (int i=0;i<5;i++)
		      fila[i] = res.getObject(i+1); 
		   modelTable.addRow(fila); 
		   
		}
		table.revalidate();
	}

	//events
	@Override
	public void actionPerformed(ActionEvent e) {
		pass = new String(txtPassword.getPassword());
		if(e.getSource().equals(btnAdd)){
			SqlServerAdmin ssa = new SqlServerAdmin();
			if(!validateFields())
				return;
			boolean bandera =ssa.insert(txtName.getText(),txtHostName.getText(), txtUser.getText(),txtPassword.getPassword().toString(), txtPath.getText());
			System.out.println(bandera);
			try {
				loadDates();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else if(e.getSource().equals(btnDelete)){
			SqlServerAdmin ssa = new SqlServerAdmin();
			if(txtName.getText().isEmpty()){
				JOptionPane.showMessageDialog(this, "Error, Name is emply");
				return;
			}
			boolean result=ssa.delete(txtName.getText());
			try {
				loadDates();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			txtHostName.setText("");
			txtName.setText("");
			txtPassword.setText("");
			txtPath.setText("");
			txtUser.setText("");
			System.out.println(result);
		}else if(e.getSource().equals(btnEdit)){
			SqlServerAdmin ssa = new SqlServerAdmin();
			if(!validateFields())
				return;
			boolean result = ssa.edit(txtName.getText(),txtHostName.getText(), txtUser.getText(), txtPassword.getPassword().toString(), txtPath.getText());
			System.out.println(result);
			try {
				loadDates();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}else if(e.getSource().equals(btnExplorar)){
			if(!validateFields())
				return;
			cancel=false;
			this.dispose();
		}
		
	}
	/**
	 * Validate fields to this class
	 * @return boolean: approved=true, discouraged=false
	 */
	private boolean validateFields(){
		if(txtName.getText().isEmpty()){
			JOptionPane.showMessageDialog(this, "Error, Name is emply");
			return false;
		}
		if(txtHostName.getText().isEmpty()){
			JOptionPane.showMessageDialog(this, "Error, HostName is emply");
			return false;
		}
		if(txtPath.getText().isEmpty()){
			JOptionPane.showMessageDialog(this, "Error, Path is emply");
			return false;
		}
		return true;
	}
	
	//methods to share fields with explorer
	public String getHostname(){
		if (txtHostName.getText().isEmpty()){
			return "";
		}else{
			return txtHostName.getText();
		}
	}
	public String getUser(){
		if (txtUser.getText().isEmpty()){
			return "";
		}else{
			return txtUser.getText();
		}
	}
	public String getPassword(){
		if (pass.isEmpty()){
			return "";
		}else{
			return pass;
		}
	}
	public String getPath(){
		if (txtPath.getText().isEmpty()){
			return "";
		}else{
			return txtPath.getText();
		}
	}
}
