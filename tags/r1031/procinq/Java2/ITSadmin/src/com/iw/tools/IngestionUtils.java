package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.regex.*;
import java.text.DecimalFormat;

import org.xml.sax.XMLReader;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.XMLReaderFactory;
import org.dom4j.io.SAXReader;

public class IngestionUtils {
    // create a corpus (without orthogonalization) given a folder
    //
    // each folder or file underneath this corpus is a topic.  Files will have node source, folders will not.

    public static Corpus createCorpusFromFilesystem(File f, ITS its) throws Exception {
        // create corpus from the folder specified
        Corpus c = its.addCorpus(readableFilename(f));
        ITSTreeNode root = (ITSTreeNode) ((Vector) its.CQL("SELECT <NODE> WHERE PARENTID = -1 AND CORPUSID = " + c.getID())).elementAt(0);

        createInfrastructure(f, root, its);

        return c;
    }

    public static Corpus createCorpusFromPipeSV(File sf, ITS its) throws Exception {
        // create corpus from the csv file specified
        //first column is level 1 in corpus
        //all remaining columns are signatures
        String filename = sf.getName();

        //Create the corpus and root node
        Corpus c = its.addCorpus(filename);
        Node parentNode = its.getCorpusRoot(c.getID());

        //Read CSV File and create nodes
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(sf), "UTF8"));

        //eventually this next variable can be user-input
        double weight = 1;

        String record = null;
        ITSTreeNode treeNode = null;

        try {
            while ((record = br.readLine()) != null) {
                //remove any trailing pipes
                while (record.endsWith("|")) {
                    record = record.substring(0, record.length() - 1);
                }

                //add node to corpus
                String[] line = record.split("\\|");
                String nodeName = line[0];

                ITSTreeNode n = createNode(parentNode.get("NODEID"), "100", "1", nodeName.trim(),
                        nodeName.trim(), parentNode.get("CORPUSID"));

                treeNode = its.addNode(n);
                if (treeNode != null) {
                    System.out.println("Adding node for: " + nodeName);

                    //determine number of signatures (all columns beyond first)
                    int numSigColumns = 0;
                    int foundIndex = 0;
                    for (int index = record.length() - 1; foundIndex > -1; numSigColumns++, index = foundIndex - 1) {
                        foundIndex = record.lastIndexOf("|", index);
                    }
                    numSigColumns = numSigColumns - 1;

                    //add signatures of other columns (empty columns are ignored)
                    String sigName;
                    Signature nodeSig = null;
                    Signatures vsig = new Signatures();
                    boolean sigsExist = false;

                    for (int i = 1; i <= numSigColumns; i++) {
                        sigName = line[i];
                        if (sigName.length() > 0) {
                            nodeSig = new Signature(sigName.trim(), weight);
                            vsig.add(nodeSig);
                            sigsExist = true;
                        }
                    }
                    if (sigsExist) {
                        its.saveSignatures(treeNode.get("NODEID"), vsig.getVecOfSignatureObjects(), false);
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("There was an error while importing pipe-delinmited file...sorry!");
            e.printStackTrace(System.err);
        }

        return c;
    }

    public static Corpus createCorpusFromPipeSV2(File sf, ITS its) throws Exception {
        // create corpus from the csv file specified
        //first column is level 1 in corpus
        //second column is level 2 in corpus
        //all remaining columns are signatures for level 2 node
        String filename = sf.getName();


        //Create the corpus and root node
        Corpus c = its.addCorpus(filename);
        Node rootNode = its.getCorpusRoot(c.getID());
        String rootNodeID = rootNode.get("NODEID");
        String corpusID = rootNode.get("CORPUSID");
        String parentNodeID = rootNodeID;

        //Read CSV File and create nodes
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(sf), "UTF-8"));


        //eventually this next variable can be user-input
        double weight = 1;

        String record;
        String currentLevel1NodeName = "";
        ITSTreeNode treeNode = null;
        ITSTreeNode n = null;

        try {
            while ((record = br.readLine()) != null) {

                //remove any trailing pipes
                while (record.endsWith("|")) {
                    record = record.substring(0, record.length() - 1);
                }

                System.out.println(record);

                //add node to corpus
                String[] line = record.split("\\|");
                String nodeName = line[0];

                if (nodeName.compareToIgnoreCase(currentLevel1NodeName) != 0) {
                    //add new level 1 node; it changed
                    n = createNode(rootNodeID, "100", "1", nodeName.trim(),
                            nodeName.trim(), corpusID);
                    treeNode = its.addNode(n);

                    System.out.println("Adding level 1 node for: " + nodeName);

                    currentLevel1NodeName = nodeName;
                    parentNodeID = treeNode.get("NODEID");
                }

                //add level 2 node
                nodeName = line[1];
                n = createNode(parentNodeID, "100", "2", nodeName.trim(),
                        nodeName.trim(), corpusID);
                treeNode = its.addNode(n);

                if (treeNode != null) {
                    System.out.println("Adding level 2 node for: " + nodeName);

                    //determine number of signatures (all columns beyond first)
                    int numSigColumns = 0;
                    int foundIndex = 0;
                    for (int index = record.length() - 1; foundIndex > -1; numSigColumns++, index = foundIndex - 1) {
                        foundIndex = record.lastIndexOf("|", index);
                    }
                    numSigColumns = numSigColumns - 1;

                    //add signatures of other columns (empty columns are ignored)
                    String sigName;
                    Signature nodeSig = null;
                    Signatures vsig = new Signatures();
                    boolean sigsExist = false;

                    for (int i = 2; i <= numSigColumns; i++) {
                        sigName = line[i];
                        if (sigName.length() > 0) {
                            nodeSig = new Signature(sigName.trim(), weight);
                            vsig.add(nodeSig);
                            sigsExist = true;
                        }
                    }
                    if (sigsExist) {
                        its.saveSignatures(treeNode.get("NODEID"), vsig.getVecOfSignatureObjects(), false);
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("There was an error while importing pipe-delimited file...sorry!");
            e.printStackTrace(System.err);
        }

        return c;
    }

    public static Corpus createCorpusFromCSV(File sf, ITS its) throws Exception {
        //Requires specific input format:
        //Each line contains full hierarchy of node names, plus, optionally, one of three keywords & value
        //Keywords: Signature, MustHave, Source
        //
        //Example file:
        //Test topic,Signature,SignatureOne
        //Test topic,Signature,SignatureTwo
        //Test topic,MustHave,MustHaveOne
        //Test topic,MustHave,MustHaveTwo
        //Test topic,SubtopicOne,Signature,SubOneSignatureOne
        //Test topic,SubtopicOne,MustHave,SubOneMustHaveOne
        //Test topic,SubtopicTwo,MustHave,SubTwoMustHaveOne
        //Test topic,SubtopicThree,Signature,SubThreeSignatureOne
        //Test topic,SubtopicFour,Signature,SubFourSignatureOne
        //Test topic,SubtopicFour,Signature,SubFourSignatureTwo
        //Test topic Two
        //Test topic Three,Source,"This is some source which has spaces and commas, and parentheses() and apostrophes', and that's all"
        //
        //****** LIMITATION: Currently only works with 128 or less nodes levels **************

        // create corpus from the csv file specified
        String filename = sf.getName();
        String sourceKeyword = "Source";
        String signatureKeyword = "Signature";
        String musthaveKeyword = "MustHave";

        //Create the corpus and root node
        Corpus c = its.addCorpus(filename);
        Node rootNode = its.getCorpusRoot(c.getID());

        String[][] existingLevelNodes = {{"ROOT", rootNode.get("NODEID")},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"},
            {"empty", "empty"}, {"empty", "empty"}, {"empty", "empty"}};
        String corpusID = rootNode.get("CORPUSID");

        //Read CSV File and create nodes
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(sf), "UTF-8"));


        //eventually this next variable can be user-input
        double weight = 1;

        String record;

        ITSTreeNode treeNode = null;
        ITSTreeNode treeNodeToBe = null;

        try {
            while ((record = br.readLine()) != null) {

                boolean bMetaDataFound = false;



                //remove any trailing commas
                record = record.trim();
                while (record.endsWith(",")) {
                    record = record.substring(0, record.length() - 1);
                }


                //add node(s) to corpus, if don't exist
                String line[] = record.split("\\,");
                String inputTerm = line[0];

                int i = 1;

                //create topic hierarchy
                while ((i <= line.length) && (inputTerm.compareToIgnoreCase(sourceKeyword) != 0) && (inputTerm.compareToIgnoreCase(musthaveKeyword) != 0) && (inputTerm.compareToIgnoreCase(signatureKeyword) != 0)) {
                    if (inputTerm.compareToIgnoreCase(existingLevelNodes[i][0]) != 0) {
                        //add new level node; it changed
                        treeNodeToBe = createNode(existingLevelNodes[i - 1][1], "100", i + "", inputTerm.trim(), inputTerm.trim(), corpusID);
                        treeNode = its.addNode(treeNodeToBe);

                        System.out.println("Adding level " + (i) + " node for: " + inputTerm);

                        //this node is now the current existing one for its level 
                        existingLevelNodes[i][0] = inputTerm;
                        existingLevelNodes[i][1] = treeNode.get("NODEID");
                    }

                    if (i < line.length) {
                        inputTerm = line[i];
                        bMetaDataFound = true;
                    } else {
                        bMetaDataFound = false;
                    }
                    i++;
                }

                //node hierarchy now exists; add any source/signature/musthave if provided
                if (bMetaDataFound) {
                    if (inputTerm.compareToIgnoreCase(signatureKeyword) == 0) {
                        //add signature to this node
                        inputTerm = line[i]; //next value is signature
                        Signature nodeSig = null;
                        Signatures vsig = new Signatures();

                        if (inputTerm.length() > 0) {
                            nodeSig = new Signature(inputTerm.trim(), weight);
                            vsig.add(nodeSig);
                            its.saveSignatures(existingLevelNodes[i - 1][1], vsig.getVecOfSignatureObjects(), false);
                        }
                    } else if (inputTerm.compareToIgnoreCase(musthaveKeyword) == 0) {
                        //add musthave to this node
                        inputTerm = line[i]; //next value is musthave

                        if (inputTerm.length() > 0) {
                            its.addMusthave(existingLevelNodes[i - 1][1], inputTerm.trim());
                        }
                    } else if (inputTerm.compareToIgnoreCase(sourceKeyword) == 0) {
                        //add source to this node

                        String inputNodeSource = null;
                        inputTerm = line[i]; //next value is source
                        if (inputTerm.length() > 0) {
                            inputNodeSource = inputTerm;
                        }

                        //check if there are any more source values to concatenate
                        //source that contains commas will break into separate vales until end of line
                        for (int j = i + 1; j < line.length; j++) {
                            inputTerm = line[j]; //next value is source

                            if (inputTerm.length() > 0) {
                                inputNodeSource += ", " + inputTerm;
                            }
                        }

                        //write source to node
                        treeNode = its.getNodeProps(existingLevelNodes[i - 1][1]);
                        its.setNodeSource(treeNode, inputNodeSource.trim());
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("There was an error while importing csv file...sorry!");
            e.printStackTrace(System.err);
        }

        return c;
    }

    public static Corpus createCorpusFromGAAPCSV(File sf, ITS its) throws Exception {
        //Requires VERY specific input format, that we built from the XBRL GAAP file:
        //Each line contains a 1st level topic and then a parent and a child topic.  
        //if the parent is not in our hash table, we add it as a topic
        //then we add the child (level 3 or greater topics) into to hash table
        //we keep going until we hit another parent that we need to add as a new topic,
        //and then we have to go back and add all the terms (level 3 and greatenewr) in the hash table as must haves 
        //of the most recently added topic
        //then we clear out the hash table and start again
        //
        //Example file:
        //Level 1 Topic, Level 2 Topic, Level 3 Topic
        //Level 1 Topic, Level 3 Topic, Level 4 Topic
        //Level 1 Topic, Level 4 Topic, Level 5 Topic
        //Level 1 Topic, Level 3 Topic, Level 4 (different) Topic
        //Level 1 Topic, Level 3 Topic, Level 4 (different) Topic

        // create corpus from the csv file specified
        String filename = sf.getName();

        //Create the corpus and root node
        Corpus c = its.addCorpus(filename);
        Node rootNode = its.getCorpusRoot(c.getID());
        String rootNodeID = rootNode.get("NODEID");
        String corpusID = rootNode.get("CORPUSID");

        //Read CSV File and create nodes
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(sf), "UTF8"));

        //table to hold Level 3 and greater topics which will be must haves to the level 2 parent topic
        Hashtable htMustHaves = new Hashtable();
        Hashtable htLevelOnes = new Hashtable();

        String record;
        ITSTreeNode treeNodeToBe = null;
        ITSTreeNode levelOneNode = null;
        ITSTreeNode levelTwoNode = null;
        ITSTreeNode levelThreeNode = null;
        ITSTreeNode levelFourNode = null;

        try {
            while ((record = br.readLine()) != null) {

                //remove any trailing commas
                while (record.endsWith(",")) {
                    record = record.substring(0, record.length() - 1);
                }

                System.out.println(record);
                //get column values and then add node(s) to corpus, if don't exist
                String line[] = record.split("\\,");
                String levelOneTerm = line[0];
                String parentTerm = line[1];
                String childTerm = line[2];

                //create Level 1 topic, if it doesn't already exist
                if (!htLevelOnes.containsKey(levelOneTerm.trim())) {
                    //Save off previous child values as must haves of last 4rd level topic
                    if (!htMustHaves.isEmpty()) {
                        Enumeration enumMustHaves = htMustHaves.keys();
                        while (enumMustHaves.hasMoreElements()) {
                            String sMustHave = (String) enumMustHaves.nextElement();
                            its.addMusthave(levelFourNode.get("NODEID"), sMustHave.trim());
                        }
                        System.out.println("*Added must haves for level 4*");

                        //clear out must haves table for next level 4 topic
                        htMustHaves.clear();
                    }

                    //add new level 1 node
                    treeNodeToBe = createNode(rootNodeID, "100", 1 + "", levelOneTerm.trim(), levelOneTerm.trim(), corpusID);
                    levelOneNode = its.addNode(treeNodeToBe);
                    htLevelOnes.put(levelOneTerm.trim(), "1");

                    System.out.println("*Added level 1: " + levelOneTerm.trim());

                    //automatically create new level two and three with other level one columns
                    treeNodeToBe = createNode(levelOneNode.get("NODEID"), "100", 1 + "", parentTerm.trim(), parentTerm.trim(), corpusID);
                    levelTwoNode = its.addNode(treeNodeToBe);
                    //htLevelTwos.put(parentTerm.trim(), "1");
                    System.out.println("*Added level 2: " + parentTerm.trim());

                    treeNodeToBe = createNode(levelTwoNode.get("NODEID"), "100", 1 + "", childTerm.trim(), childTerm.trim(), corpusID);
                    levelThreeNode = its.addNode(treeNodeToBe);
                    //htLevelThrees.put(childTerm.trim(), "1");
                    System.out.println("*Added level 3: " + childTerm.trim());
                } else {
                    if ((levelTwoNode != null) && (levelThreeNode != null)) //should never be empty
                    {
                        if (levelTwoNode.get("NODETITLE").equals(parentTerm.trim())) {
                            //Save off previous level 5 and greater values as must haves of last 4rd level topic
                            if (!htMustHaves.isEmpty() && levelFourNode != null) {
                                Enumeration enumMustHaves = htMustHaves.keys();
                                while (enumMustHaves.hasMoreElements()) {
                                    String sMustHave = (String) enumMustHaves.nextElement();
                                    its.addMusthave(levelFourNode.get("NODEID"), sMustHave.trim());
                                }
                                System.out.println("*Added must haves for level 4*");

                                //clear out must haves table for next level 4 topic
                                htMustHaves.clear();
                            }

                            //add a new level 3 child, parent term is back to level two
                            treeNodeToBe = createNode(levelTwoNode.get("NODEID"), "100", 1 + "", childTerm.trim(), childTerm.trim(), corpusID);
                            levelThreeNode = its.addNode(treeNodeToBe);
                            System.out.println("*Added level 3: " + childTerm.trim());
                        } else if (levelThreeNode.get("NODETITLE").equals(parentTerm.trim())) {
                            //Save off previous level 5 and greater values as must haves of last 4rd level topic
                            if (!htMustHaves.isEmpty() && levelFourNode != null) {
                                Enumeration enumMustHaves = htMustHaves.keys();
                                while (enumMustHaves.hasMoreElements()) {
                                    String sMustHave = (String) enumMustHaves.nextElement();
                                    its.addMusthave(levelFourNode.get("NODEID"), sMustHave.trim());
                                }
                                System.out.println("*Added must haves for level 4*");

                                //clear out must haves table for next level 4 topic
                                htMustHaves.clear();
                            }

                            //add a new level 4 child, parent term is back to level three
                            treeNodeToBe = createNode(levelThreeNode.get("NODEID"), "100", 1 + "", childTerm.trim(), childTerm.trim(), corpusID);
                            levelFourNode = its.addNode(treeNodeToBe);
                            htMustHaves.put(childTerm.trim(), "1");	//should be must have of itself
                            System.out.println("*Added level 4: " + childTerm.trim());
                        } else {
                            //parent is either a new level 5 or greater;  should go in as must haves
                            if (!htMustHaves.containsKey(parentTerm.trim())) {
                                htMustHaves.put(parentTerm.trim(), "1");
                            }

                            if (!htMustHaves.containsKey(childTerm.trim())) {
                                htMustHaves.put(childTerm.trim(), "1");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("There was an error while importing csv file...sorry!");
            e.printStackTrace(System.err);
        }

        return c;
    }

    public static Thesaurus createThesaurusFromCSV(File sf, String name, ITS its) throws Exception {
        // create corpus from the csv file specified
        String filename = sf.getName();

        // create a new thesaurus
        Thesaurus t = its.createThesaurus(name);

        //Read CSV File and create nodes
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(sf), "UTF8"));
        String record = "";
        try {
            while ((record = br.readLine()) != null) {
//                String[] values = record.split("\\|");
                String[] values = record.split("\\,");

                try {
                    String root = values[0];
                    String predicate = values[1];
                    String relationship = null;
                    if (values.length > 2) {
                        relationship = values[3];
                    }

                    if (!root.equalsIgnoreCase(predicate)) //do not add a synonym the same as the term
                    {
                        System.out.println("**Ingesting thesaurus topic: " + root + " with syn: " + predicate);

                        if ((values.length > 2) && (Integer.parseInt(relationship) > 0)) {
                            its.addRelationship(t.getID(), root, predicate, relationship);
                        } else {
                            its.addRelationship(t.getID(), root, predicate, "1");
                        }
                    }

                } catch (Exception e) {
                    // just skip this term if the entry is invalid
                }
            }
        } catch (Exception e) {
            System.err.println("There was an error while importing csv file...sorry!");
            e.printStackTrace(System.err);
        }

        System.out.println("Thesaurus created from CSV successfully!");
        return t;
    }

    public static Corpus createCorpusFromCNF(File f, ITS its) throws Exception {
        // create corpus from the folder specified
        Corpus c = its.addCorpusWithoutRoot(parseTitle(f));
        ITSTreeNode root = (ITSTreeNode) ((Vector) its.CQL("SELECT <NODE> WHERE PARENTID = -1 AND CORPUSID = " + c.getID())).elementAt(0);

        createInfrastructureFromCNF(f, root, its);

        return c;
    }

    // give the converted XML file, an ITS handle and a hashtable of directives, create a new corpus
    public static Corpus createCorpusFromXML(File f, ITS its, int size, Hashtable directives) throws Exception {
        return createCorpusFromXML(f, its, directives, size, null);
    }

    public static Corpus createCorpusFromXML(File f, ITS its, Hashtable directives, int size, PopupProgressBar ppb) throws Exception {
        Corpus c = its.addCorpus(readableFilename(f));
        ITSTreeNode n = (ITSTreeNode) ((Vector) its.CQL("SELECT <NODE> WHERE CORPUSID = " + c.getID())).elementAt(0);

        System.out.println("Creating corpus; " + readableFilename(f));

        Enumeration eTest = directives.keys();
        while (eTest.hasMoreElements()) {
            System.out.println("Key: " + (String) eTest.nextElement());
        }

        PDFimportHandlerPass2 handler = new PDFimportHandlerPass2(directives, c, n, its, size, ppb);
        SAXReader xmlReader = new SAXReader(false);

        xmlReader.setDefaultHandler(handler);

        InputSource ins = new InputSource(new FileInputStream(f));
        ins.setEncoding("UTF-8");

        try {
            org.dom4j.Document doc = xmlReader.read(ins);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            JOptionPane.showMessageDialog(null, "Fatal error: " + e.getLocalizedMessage(),
                    "Error", JOptionPane.NO_OPTION);
        }

        handler.closeOut();

        return c;
    }

    public static void importTranslatedMetadata(ITS server, PopupProgressBar ppb, File f, String lang) throws Exception {
        Hashtable htNodeMustHave = new Hashtable();
        Hashtable htNodeSignatures = new Hashtable();

        String str = "";
        // loop through each line of the file
        try {
            //BufferedReader in = new BufferedReader(new FileReader(f));
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f.getAbsolutePath()), "UTF8"));

            System.out.println("Processing file: " + f.getAbsolutePath());
            while ((str = in.readLine()) != null) {
                String[] arr = str.split("\\|\\|");

                if (arr.length == 4) {
                    if (arr[1].trim().equals("1")) {
                        // not sure what to do here
                    } else if (arr[1].trim().equals("2")) {
                        // add a must have term
                        Vector v = new Vector();
                        if (htNodeMustHave.containsKey(arr[0].trim())) {
                            v = (Vector) htNodeMustHave.get(arr[0].trim());
                        }

                        v.add(arr[2].trim());
                        htNodeMustHave.put(arr[0].trim(), v);

                    } else if (arr[1].trim().equals("3")) {
                        // add a signature term
                        Vector v = new Vector();
                        if (htNodeSignatures.containsKey(arr[0].trim())) {
                            v = (Vector) htNodeSignatures.get(arr[0].trim());
                        }
                        Signature s = new Signature(arr[2].trim(), new Double(arr[3].trim()).doubleValue());

                        v.add(s);
                        htNodeSignatures.put(arr[0].trim(), v);
                    }
                }
            }
            in.close();

            System.out.println("Done processing file: " + f.getAbsolutePath());

            int total = htNodeSignatures.size() * 2;
            DecimalFormat twoDigits = new DecimalFormat("0");
            int nodesProcessed = 0;

            System.out.println("total signatures: " + total);

            // Now run the API commands for each NODE, for each MUSTHAVE
            Enumeration eH = htNodeMustHave.keys();
            while (eH.hasMoreElements()) {
                String sNodeID = (String) eH.nextElement();
                String terms = "";
                Vector v = (Vector) htNodeMustHave.get(sNodeID);

                server.removeMusthave(sNodeID, null, lang);

                for (int i = 0; i < v.size(); i++) {
                    server.addMusthave(sNodeID, (String) v.elementAt(i), false, lang, false);
                }

                nodesProcessed++;

                if (total == 0) {
                    total = 1;
                }
                if (ppb != null) {
                    long perc = (nodesProcessed * 100) / total;
                    ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
                }

            }

            Enumeration eH2 = htNodeSignatures.keys();
            while (eH2.hasMoreElements()) {
                String sNodeID = (String) eH2.nextElement();
                Vector v = (Vector) htNodeSignatures.get(sNodeID);

                server.saveSignatures(sNodeID, v, true, lang);

                nodesProcessed++;

                if (ppb != null) {
                    long perc = (nodesProcessed * 100) / total;
                    ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
                }
            }

            ppb.setProgress(100);

        } catch (IOException e) {
            throw e;
        } catch (Exception ex) {
            System.err.println("Oops: " + str);
            throw ex;
        }
    }

    public static PDFimportHandlerPass1 scanConvertedXML(File f) throws Exception {
        PDFimportHandlerPass1 handler = new PDFimportHandlerPass1();
        SAXReader xmlReader = new SAXReader(false);

        xmlReader.setDefaultHandler(handler);

        InputSource ins = new InputSource(new FileInputStream(f));
        ins.setEncoding("UTF-8");

        org.dom4j.Document doc = xmlReader.read(ins);

        return handler;
    }

    // Recursive. Get each child and create that node.  If a child is a folder, call this function again.
    public static void createInfrastructure(File f, ITSTreeNode parent, ITS its) throws Exception {
        File[] children = f.listFiles();

        for (int i = 0; i < children.length; i++) {
            int depth = Integer.parseInt(parent.get("DEPTHFROMROOT"));
            ITSTreeNode n = createNode(parent.get("NODEID"), "100", (depth + 1) + "", readableFilename(children[i]),
                    readableFilename(children[i]), parent.get("CORPUSID"));
            n = its.addNode(n);

            // recurse if this is a folder, add node source if it is a file
            if (children[i].isDirectory()) {
                createInfrastructure(children[i], n, its);
            } else {
                try {
                    String s = getData(children[i]);
                    //System.out.println("Set document source: "+children[i].getAbsolutePath()+" source: "+s);
                    its.setNodeSource(n, s);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }
        }
    }

    // Recursive. Get each child and create that node.  If a child is a folder, call this function again.
    public static void createInfrastructureFromCNF(File f, ITSTreeNode parent, ITS its) throws Exception {
        File[] children = f.listFiles();

        int depth = Integer.parseInt(parent.get("DEPTHFROMROOT"));
        String nodeTitle = parseTitle(f);

        ITSTreeNode n = parent;
        if (!parent.get("PARENTID").equals("-1")) {
            n = createNode(parent.get("NODEID"), "100", (depth + 1) + "", nodeTitle, nodeTitle, parent.get("CORPUSID"));
            n = its.addNode(n);
        } else {
            parent.set("PARENTID", "-2");
        }

        try {
            String s = getCNFData(f);
            // no longer used if (s.length() > 200)
            its.setNodeSource(n, s);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        for (int i = 0; i < children.length; i++) {
            // in CNF, a folder does not designate a topic, only an 001.html file does.
            if (children[i].isDirectory()) {
                createInfrastructureFromCNF(children[i], n, its);
            }
        }
    }

    // Given a directory in CNF, grab the 000.html file from underneath and parse out the title
    public static String parseTitle(File f) throws Exception {
        File CNFfile = new File(f.getAbsolutePath() + "/000.html");
        boolean b = false;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(CNFfile);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            Pattern r = Pattern.compile("<TITLE>(.*)</TITLE>", Pattern.CASE_INSENSITIVE);
            Pattern r2 = Pattern.compile("<TITLE>", Pattern.CASE_INSENSITIVE);
            Matcher m = null;
            Matcher m2 = null;
            while ((sData = buf.readLine()) != null) {
                if (b) {
                    return sData;
                }

                m = r.matcher(sData);
                m2 = r2.matcher(sData);
                if (m.find()) {
                    return (String) m.group(1).toString();
                }
                if (m2.find()) {
                    b = true;
                }
            }

            System.err.println("Warning: No topic title found for " + CNFfile.getAbsolutePath());

            return "Untitled Topic";
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
            fis = null;
        }
    }

    // get a stream of text from a file.  if this document is not .txt, filter it first.
    public static String getData(File f) throws Exception {
        boolean bUsingTempFile = false;
        File tempFile = f;

        // if the file is of type text, it needs an HTML wrapper before being posted
        if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
            bUsingTempFile = true;
            try {
                tempFile = ITS.PDFtoTXT(f);
            } catch (Exception e) {
                e.printStackTrace(System.out);
                return "";
            }
        } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
            bUsingTempFile = true;
            try {
                tempFile = ITS.MSWORDtoTXT(f);
            } catch (Exception e) {
                e.printStackTrace(System.out);
                return "";
            }
        } else if ((f.getAbsolutePath().toLowerCase().endsWith(".html"))
                || (f.getAbsolutePath().toLowerCase().endsWith(".htm"))) {
            bUsingTempFile = true;
            try {
                tempFile = ITS.HTMLtoTXT(f);
            } catch (Exception e) {
                e.printStackTrace(System.out);
                return "";
            }
        } else {
            tempFile = f;
            bUsingTempFile = false;
        }

        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(tempFile);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData + "\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (fis != null) {
                fis.close();
                fis.close();
            }
        }

        if (bUsingTempFile) {
            tempFile.delete();
        }

        return sOut;
    }

    // get a stream of text from a file.  if this document is not .txt, filter it first.
    public static String getCNFData(File f) throws Exception {
        boolean bUsingTempFile = false;
        File tempFile = new File(f.getAbsolutePath() + "/000.html");

        // if the file is of type text, it needs an HTML wrapper before being posted
        try {
            tempFile = ITS.HTMLtoTXT(tempFile);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return "";
        }

        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(tempFile);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData + "\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (fis != null) {
                fis.close();
                fis.close();
            }
        }

        return sOut;
    }

    public static ITSTreeNode createNode(String ParentID, String NodeSize, String DepthFromRoot,
            String NodeDesc, String NodeTitle, String CorpusID) {
        ITSTreeNode n = new ITSTreeNode();

        n.set("PARENTID", ParentID);
        n.set("NODESIZE", NodeSize);
        n.set("DEPTHFROMROOT", DepthFromRoot);
        n.set("NODEDESC", NodeDesc);
        n.set("NODETITLE", NodeTitle);
        n.set("CORPUSID", CorpusID);

        return n;
    }

    public static ITSTreeNode createNode(Corpus c) {
        ITSTreeNode n = new ITSTreeNode();

        n.set("PARENTID", "-1");
        n.set("NODESIZE", "100");
        n.set("DEPTHFROMROOT", "0");
        n.set("NODEDESC", c.getName());
        n.set("NODETITLE", c.getName());
        n.set("CORPUSID", c.getID());

        return n;
    }

    // capitalize the first letter of every word
    public static String makeTitle(String s) {
        StringTokenizer st = new StringTokenizer(s.trim());
        String retval = "";

        try {
            while (st.hasMoreElements()) {
                StringBuffer temp = new StringBuffer(st.nextToken());

                retval += Character.toUpperCase(temp.charAt(0))
                        + temp.toString().substring(1) + (st.hasMoreTokens() ? " " : "");
            }
        } catch (Exception e) {
            retval = "";
        }

        return retval;
    }

    // replace underscores with spaces
    public static String readableFilename(File f) {
        String sName = makeTitle(f.getName().toLowerCase());

        return sName.replaceAll("_", " ");
    }
}
