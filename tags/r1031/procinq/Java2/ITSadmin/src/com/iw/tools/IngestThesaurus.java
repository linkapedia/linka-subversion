/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Jun 9, 2005
 * Time: 11:24:30 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.tools;

import com.iw.system.Thesaurus;
import com.iw.ui.ITSAdministrator;

import java.io.*;

public class IngestThesaurus {
    public static Thesaurus ingestThesaurus(File f, ITSAdministrator itsAdmin) throws Exception {
        Thesaurus t = itsAdmin.its.createThesaurus(f.getName());

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));
        String record = null; String predicate = null; String anchor = null;

        while ((record = dis.readLine()) != null) {
            record = record.trim();

            if (!record.equals("")) {
                // oracle's thesaurus terminology is as follows:
                //  SYN - synonym
                //  BT, BTG, or BTI - broader term
                //  NT, NTG, NTI or NTP  - narrower term
                //
                if ((record.toUpperCase().startsWith("BT ")) ||
                    (record.toUpperCase().startsWith("BTG ")) ||
                    (record.toUpperCase().startsWith("BTI ")) ||
                    (record.toUpperCase().startsWith("NT ")) ||
                    (record.toUpperCase().startsWith("NTG ")) ||
                    (record.toUpperCase().startsWith("NTI ")) ||
                    (record.toUpperCase().startsWith("NTP ")) ||
                    (record.toUpperCase().startsWith("SYN "))) {

                    predicate = record.substring(3).trim();

                    // Relationship type, 1 is a synonym, 2 is a broader term and 3 is a narrower term
                    String relationship = "1";
                    if (record.toUpperCase().startsWith("B")) relationship = "2";
                    if (record.toUpperCase().startsWith("N")) relationship = "3";

                    try { itsAdmin.its.addRelationship(t.getID(), anchor, predicate, relationship); }
                    catch (Exception e) {
                        System.err.println("** WARNING **");
                        e.printStackTrace(System.out);
                    }

                } else { anchor = record; }
            }

        }

        return t;
    }
}
