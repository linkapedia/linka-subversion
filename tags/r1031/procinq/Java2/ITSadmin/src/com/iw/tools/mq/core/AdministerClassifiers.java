/*
 * To administer classifiers
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Ago 12, 2011, 02:00 PM
 */
package com.iw.tools.mq.core;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import org.jfree.util.Log;

import com.iw.system.ITS;
import com.iw.tools.ImageUtils;
import com.iw.tools.ManageResource;
import com.iw.tools.mq.util.RenderCell;
import com.iw.tools.mq.util.RestMqUtil;
import com.iw.ui.ITSAdministrator;

public class AdministerClassifiers extends JPanel implements ActionListener {
	private static final long serialVersionUID = -8878594546692993300L;
	public ImageIcon iIndraweb = ImageUtils
			.getImage(this, "itsimages/logo.gif");
	private JPanel panelPrincipal;
	private JPanel panelStatus;
	private JPanel panelSearch;
	private JScrollPane scrollListClasifier;
	private JTable tblClassifiers;
	private JTextField txtSearch;
	private JLabel lblMessageNumClassifiers;
	private JLabel lblMessageNumClassifiersOn;
	private JLabel lblMessageNumClassifiersOff;
	private JTextField txtMessageNumClassifiers;
	private JTextField txtMessageNumClassifiersOn;
	private JTextField txtMessageNumClassifiersOff;
	private JToolBar bar;
	private ImageIcon icon;
	private JButton btnRefresh;
	private JButton btnState;
	private JButton btnGenerateDB;
	private DefaultTableModel modelTable;
	private ArrayList<String> arrayClassifiers = null;

	private TableCellRenderer renderer;

	public ITS its = null;
	public ITSAdministrator ITSframe = null;
	protected JInternalFrame MDIframe;
	private ITSAdministrator frame;
	private JInternalFrame internal;
	private JDesktopPane desktop;

	public AdministerClassifiers() throws Exception {
	}

	public AdministerClassifiers(ITSAdministrator frame) {
		this.frame = frame;
		showWindow();
	}

	private void showWindow() {
		this.desktop = this.frame.jdp;
		this.internal = new JInternalFrame("Classifiers", false, true, false,
				true);
		this.ITSframe = this.frame;
		this.internal.setFrameIcon(this.ITSframe.iIndraweb);
		configureControls();
		LoadClassifiers();
		this.internal.pack();
		this.internal.setVisible(true);
	}

	private void configureControls() {
		this.lblMessageNumClassifiers = new JLabel("No Classifiers");
		this.lblMessageNumClassifiersOn = new JLabel("No Classifiers (ON)");
		this.lblMessageNumClassifiersOff = new JLabel("No Classifiers (OFF)");

		this.icon = com.iw.tools.ImageUtils.getImage(this,"files/MqGui/images/state.png");

		this.btnRefresh = new JButton("Refresh");
		this.btnState = new JButton(this.icon);
		this.btnGenerateDB = new JButton("Builder");

		this.bar = new JToolBar();
		this.bar.add(this.btnRefresh);
		// this.bar.add(this.btnGenerateDB);//generate base de datos unificada
		this.bar.add(this.btnState);

		this.txtSearch = new JTextField(20);
		this.txtMessageNumClassifiers = new JTextField(10);
		this.txtMessageNumClassifiersOn = new JTextField(10);
		this.txtMessageNumClassifiersOff = new JTextField(10);

		String[] columNames = { "Name", "IP", "Works Procceseed",
				"Works Failed", "Uptime", "State" };

		this.modelTable = new DefaultTableModel(columNames, 0) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}

			public Class getColumnClass(int column) {
				return getValueAt(0, column).getClass();
			}
		};

		this.tblClassifiers = new JTable(this.modelTable);
		renderer = new RenderCell();
		tblClassifiers.setDefaultRenderer(String.class, renderer);
		tblClassifiers.setAutoCreateRowSorter(true);
		tblClassifiers
				.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblClassifiers.getTableHeader().setReorderingAllowed(false);

		this.panelPrincipal = new JPanel();
		this.panelStatus = new JPanel();
		this.panelSearch = new JPanel();
		this.scrollListClasifier = new JScrollPane(this.tblClassifiers);
		this.scrollListClasifier.setBorder(BorderFactory.createEmptyBorder(20,
				20, 20, 20));
		this.scrollListClasifier.setPreferredSize(new Dimension(800, 150));
		this.scrollListClasifier.revalidate();

		Border borde = BorderFactory.createEmptyBorder(10, 10, 10, 10);

		this.panelPrincipal.setBorder(BorderFactory.createTitledBorder(borde));
		this.scrollListClasifier.setBorder(BorderFactory
				.createTitledBorder(borde));
		this.panelStatus.setBorder(BorderFactory.createEmptyBorder(10, 250, 10,
				250));
		this.panelSearch.setBorder(BorderFactory.createTitledBorder(borde));

		// comboBox.addItemListener(this);

		this.txtSearch.addKeyListener(new key());
		this.txtMessageNumClassifiers.setEnabled(false);
		this.txtMessageNumClassifiersOn.setEnabled(false);
		this.txtMessageNumClassifiersOff.setEnabled(false);

		this.btnRefresh.addActionListener(this);
		this.btnState.addActionListener(this);
		this.btnState.setToolTipText("Change State");
		this.btnGenerateDB.addActionListener(this);
		this.btnGenerateDB.setToolTipText("To generated unified database");

		this.panelStatus.setLayout(new GridLayout(3, 2));
		this.panelStatus.add(this.lblMessageNumClassifiers);
		this.panelStatus.add(this.txtMessageNumClassifiers);
		this.panelStatus.add(this.lblMessageNumClassifiersOn);
		this.panelStatus.add(this.txtMessageNumClassifiersOn);
		this.panelStatus.add(this.lblMessageNumClassifiersOff);
		this.panelStatus.add(this.txtMessageNumClassifiersOff);

		this.panelSearch.setLayout(new FlowLayout());
		this.panelSearch.add(new JLabel("Search"));
		this.panelSearch.add(this.txtSearch);
		this.panelPrincipal.setLayout(new BorderLayout());
		this.panelPrincipal.add(this.scrollListClasifier, "North");
		this.panelPrincipal.add(this.panelStatus, "Center");
		this.panelPrincipal.add(this.panelSearch, "South");
		this.internal.getContentPane().add(this.bar, "North");
		this.internal.getContentPane().add(this.panelPrincipal);
		this.desktop.add(this.internal);

	}

	/**
	 * method to load classifier call in each change
	 */

	private void LoadClassifiers() {
		this.arrayClassifiers = new ArrayList<String>();

		RestMqUtil.getRestMqUtil().setInfoToClassifier();
		removeAllItemFromTable();
		for (String classifier : RestMqUtil.getRestMqUtil().getClassifiers()) {
			addRow(classifier);
			this.arrayClassifiers.add(classifier);
		}

		Integer n = new Integer(this.arrayClassifiers.size());
		this.txtMessageNumClassifiers.setText(n.toString());

		this.txtMessageNumClassifiersOn.setText(String.valueOf(RestMqUtil
				.getRestMqUtil().getClassifiersON()));
		this.txtMessageNumClassifiersOff.setText(String.valueOf(RestMqUtil
				.getRestMqUtil().getClassifiersOFF()));
	}

	public void openAtCenter() {
		Dimension winsize = getSize();
		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screensize.width - winsize.width) / 2,
				(screensize.height - winsize.height) / 2);
	}

	/**
	 * central method to remove rows of table
	 */
	private void removeAllItemFromTable() {
		while (this.modelTable.getRowCount() > 0)
			this.modelTable.removeRow(0);
	}

	private void addRow(String c) {
		Vector<String> rowData = new Vector<String>();
		rowData.add(RestMqUtil.getRestMqUtil().getName(c));
		rowData.add(c);
		rowData.add(RestMqUtil.getRestMqUtil().getWorksProcessed(c));
		rowData.add(RestMqUtil.getRestMqUtil().getWorksFailed(c));
		rowData.add(RestMqUtil.getRestMqUtil().getUptime(c));
		String s = RestMqUtil.getRestMqUtil().getState(c);
		String sa;
		if (s.equals("true")) {
			sa = "Running";
		} else {
			sa = "Stop";
		}
		rowData.add(sa);
		this.modelTable.addRow(rowData);
	}

	/**
	 * method for filter text
	 */
	private void updateList() {
		ArrayList<String> arrayCoincidence = null;
		arrayCoincidence = new ArrayList<String>();
		removeAllItemFromTable();
		if (this.txtSearch.getText().length() > 0) {
			for (String c : this.arrayClassifiers) {
				String name = RestMqUtil.getRestMqUtil().getName(c);
				if (name.length() < this.txtSearch.getText().length())
					continue;
				if (name.substring(0, this.txtSearch.getText().length())
						.equalsIgnoreCase(this.txtSearch.getText())) {
					arrayCoincidence.add(c.toString());
				}
			}

			for (String c : arrayCoincidence)
				addRow(c);
		} else {
			for (String c : this.arrayClassifiers)
				addRow(c);
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.btnRefresh)) {
			RestMqUtil.getRestMqUtil().setInfoToClassifier();
			LoadClassifiers();
		} else if (e.getSource().equals(this.btnState)) {
			if (tblClassifiers.getSelectedRowCount() > 0) {
				if (JOptionPane.showConfirmDialog(this, "Are you sure to change state of the selected classifiers?","Question",2) == JOptionPane.OK_OPTION) {
					int[] listClass = this.tblClassifiers.getSelectedRows();
					HashMap<String, String> mapClassifiers = new HashMap<String, String>();
					for (int c : listClass) {
						mapClassifiers.put(tblClassifiers.getValueAt(c, 1)
								.toString(), tblClassifiers.getValueAt(c, 5)
								.toString());
					}
					int result = RestMqUtil.getRestMqUtil()
							.changeStateClassifiers(mapClassifiers);
					if (result == 0) {
						JOptionPane.showMessageDialog(this,
								"Not conect with the server");
					} else {
						JOptionPane.showMessageDialog(this,
								"The request is made correctly: HttpResponse -> "
										+ result);
					}
				}
			} else {
				JOptionPane.showMessageDialog(this, "Select one or more classifiers");

			}

		} else if (e.getSource().equals(this.btnGenerateDB)) {
			int[] values = this.tblClassifiers.getSelectedRows();
			if (values != null)
				if (values.length != 0)
					JOptionPane.showMessageDialog(this,
							"Generated"
									+ this.modelTable.getDataVector()
											.elementAt(values[0]));
				else
					JOptionPane.showMessageDialog(this,
							"Select the classifiers");
		}
	}

	class key implements KeyListener {
		key() {
		}

		public void keyPressed(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) {
			if (e.getSource().equals(AdministerClassifiers.this.txtSearch))
				AdministerClassifiers.this.updateList();
		}

		public void keyTyped(KeyEvent e) {
		}
	}

}