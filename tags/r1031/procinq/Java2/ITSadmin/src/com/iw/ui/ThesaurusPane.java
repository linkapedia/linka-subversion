package com.iw.ui;

import com.iw.system.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

import com.iw.tools.SortedList;
import com.iw.tools.SwingWorker;

/**
 * The ThesaurusPane extends JPanel and encompasses the entire widget set used to manipulate any single
 *  thesaurus.   Thesaurus terms can be created, edited, removed and browsed through this interface.
 *
 *	@authors Michael A. Puscar, Indraweb Inc, All Rights Reserved.
 *
 *	@return ThesaurusPane object (subclass of JPanel)
 */
public class ThesaurusPane extends JPanel implements ActionListener, KeyListener, ListSelectionListener {
    // globally accessible widgets from within the pane.
    private SortedList wordList, simiList, broaList, narrList, relList;
    private JTextField jquery;
    private JTextArea jDescription;
    private JButton jsearch;
    private JDialog confirmDialog;
    private Component glass;

    private boolean bConditionMet = false;

    // globally accessible image buttons used for creating, editing, and removing thesaurus terms
    private JButton jwordadd = new JButton();
    private JButton jwordedit = new JButton();
    private JButton jwordremove = new JButton();
    private JButton jsimiadd = new JButton();
    private JButton jsimiedit = new JButton();
    private JButton jsimiremove = new JButton();
    private JButton jbroaadd = new JButton();
    private JButton jbroaedit = new JButton();
    private JButton jbroaremove = new JButton();
    private JButton jnarradd = new JButton();
    private JButton jnarredit = new JButton();
    private JButton jnarrremove = new JButton();
    private JButton jreladd = new JButton();
    private JButton jreledit = new JButton();
    private JButton jrelremove = new JButton();


    // data in the select list widget is globally defined here
    private Vector listData = new Vector();

    private ITSAdministrator ITSframe = null; // this is a pointer to the ITSframe for pointer manipulation
    private JInternalFrame frame = null;
    private String ThesaurusID = "1"; // every thesauruspane is used to manipulate only one thesaurus

    // ThesaurusPane constructor
    public ThesaurusPane(ITSAdministrator ITSframeIn, String ThesID, String ThesName) {
        ITSframe = ITSframeIn;
        ThesaurusID = ThesID;
        JDesktopPane jdesktoppane = ITSframe.jdp;

        // load preference information
        User u = ITS.getUser();

        // the pop up thesaurus manager is an internal MDI frame
        frame = new JInternalFrame("Browse Thesaurus: " + ThesName, false, true, true, true);
        frame.setLocation(60, 60);
        frame.setSize(960, 460);
        frame.setBackground(Color.lightGray);
        frame.setFrameIcon(ITSframeIn.iIndraweb);

        glass = frame.getGlassPane();

        // three panels exist for the frame: top, middle (scroll), and bottom
        JPanel topPanel = new JPanel();
        JLabel jl1 = new JLabel("Find: ");
        jl1.setFont(new Font("Arial", Font.BOLD, 12));

        jquery = new JTextField(50);
        if (u.useDynamicThesaurus()) jquery.addKeyListener(this); // thesaurus terms are loaded as the user types in this box

        jsearch = new JButton();

        ImageIcon iiSearch = com.iw.tools.ImageUtils.getImage(frame, "itsimages\\search.gif");
        Image i = iiSearch.getImage().getScaledInstance(23, 22, Image.SCALE_FAST);

        jsearch.setIcon(new ImageIcon(i));
        jsearch.setMargin(new Insets(1, 1, 1, 1));
        jsearch.addActionListener(this);

        topPanel.add(jl1);
        topPanel.add(jquery);
        topPanel.add(jsearch);

        // the middle panel contains four word lists for thesaurus terms plus image manipulation buttons
        wordList = new SortedList(listData);
        wordList.setFixedCellWidth(125);
        wordList.setVisibleRowCount(15);
        wordList.addKeyListener(this);
        simiList = new SortedList();
        simiList.setFixedCellWidth(125);
        simiList.setVisibleRowCount(15);
        simiList.addKeyListener(this);
        broaList = new SortedList();
        broaList.setFixedCellWidth(125);
        broaList.setVisibleRowCount(15);
        broaList.addKeyListener(this);
        narrList = new SortedList();
        narrList.setFixedCellWidth(125);
        narrList.setVisibleRowCount(15);
        narrList.addKeyListener(this);

        relList = new SortedList();
        relList.setFixedCellWidth(125);
        relList.setVisibleRowCount(15);
        relList.addKeyListener(this);

        wordList.setFont(new Font("Arial", Font.PLAIN, 12));
        simiList.setFont(new Font("Arial", Font.PLAIN, 12));
        broaList.setFont(new Font("Arial", Font.PLAIN, 12));
        narrList.setFont(new Font("Arial", Font.PLAIN, 12));
        relList.setFont(new Font("Arial", Font.PLAIN, 12));

        // set up an action if a thesaurus term is clicked in the list box
        wordList.addListSelectionListener(this);

        // Add the listbox to a scrolling pane
        JScrollPane scrollwordPane = new JScrollPane(wordList);
        JLabel sp1 = new JLabel("Anchor");
        sp1.setFont(new Font("Arial", Font.BOLD, 12));
        JScrollPane scrollsimiPane = new JScrollPane(simiList);
        JLabel sp2 = new JLabel("Similar");
        sp2.setFont(new Font("Arial", Font.BOLD, 12));
        JScrollPane scrollbroaPane = new JScrollPane(broaList);
        JLabel sp3 = new JLabel("Broader");
        sp3.setFont(new Font("Arial", Font.BOLD, 12));
        JScrollPane scrollnarrPane = new JScrollPane(narrList);
        JLabel sp4 = new JLabel("Narrower");
        sp4.setFont(new Font("Arial", Font.BOLD, 12));
        JScrollPane scrollrelPane = new JScrollPane(relList);
        JLabel sp5 = new JLabel("Related");
        sp5.setFont(new Font("Arial", Font.BOLD, 12));



        ImageIcon iiNew = com.iw.tools.ImageUtils.getImage(frame, "itsimages\\new.gif");
        ImageIcon iiEdi = com.iw.tools.ImageUtils.getImage(frame, "itsimages\\edit.gif");
        ImageIcon iiDel = com.iw.tools.ImageUtils.getImage(frame, "itsimages\\delete.gif");

        Image iNew = iiNew.getImage();
        Image iEdi = iiEdi.getImage();
        Image iDel = iiDel.getImage();

        jwordadd.setIcon(new ImageIcon(iNew));
        jwordadd.setMargin(new Insets(1, 1, 1, 1));
        jwordadd.setToolTipText("Add new anchor term");
        jsimiadd.setIcon(new ImageIcon(iNew));
        jsimiadd.setMargin(new Insets(1, 1, 1, 1));
        jsimiadd.setToolTipText("Add similar relationship");
        jbroaadd.setIcon(new ImageIcon(iNew));
        jbroaadd.setMargin(new Insets(1, 1, 1, 1));
        jbroaadd.setToolTipText("Add broader relationship");
        jnarradd.setIcon(new ImageIcon(iNew));
        jnarradd.setMargin(new Insets(1, 1, 1, 1));
        jnarradd.setToolTipText("Add narrower relationship");

        jreladd.setIcon(new ImageIcon(iNew));
        jreladd.setMargin(new Insets(1, 1, 1, 1));
        jreladd.setToolTipText("Add related relationship");


        jwordedit.setIcon(new ImageIcon(iEdi));
        jwordedit.setMargin(new Insets(1, 1, 1, 1));
        jwordedit.setToolTipText("Edit anchor term");
        jsimiedit.setIcon(new ImageIcon(iEdi));
        jsimiedit.setMargin(new Insets(1, 1, 1, 1));
        jsimiedit.setToolTipText("Edit similar term");
        jbroaedit.setIcon(new ImageIcon(iEdi));
        jbroaedit.setMargin(new Insets(1, 1, 1, 1));
        jbroaedit.setToolTipText("Edit broader term");
        jnarredit.setIcon(new ImageIcon(iEdi));
        jnarredit.setMargin(new Insets(1, 1, 1, 1));
        jnarredit.setToolTipText("Edit narrower term");

        jreledit.setIcon(new ImageIcon(iEdi));
        jreledit.setMargin(new Insets(1, 1, 1, 1));
        jreledit.setToolTipText("Edit related term");

        jwordremove.setIcon(new ImageIcon(iDel));
        jwordremove.setMargin(new Insets(1, 1, 1, 1));
        jwordremove.setToolTipText("Remove anchor term");
        jsimiremove.setIcon(new ImageIcon(iDel));
        jsimiremove.setMargin(new Insets(1, 1, 1, 1));
        jsimiremove.setToolTipText("Remove similar term");
        jbroaremove.setIcon(new ImageIcon(iDel));
        jbroaremove.setMargin(new Insets(1, 1, 1, 1));
        jbroaremove.setToolTipText("Remove broader term");
        jnarrremove.setIcon(new ImageIcon(iDel));
        jnarrremove.setMargin(new Insets(1, 1, 1, 1));
        jnarrremove.setToolTipText("Remove narrower term");

        jrelremove.setIcon(new ImageIcon(iDel));
        jrelremove.setMargin(new Insets(1, 1, 1, 1));
        jrelremove.setToolTipText("Remove related term");

        // events for new, edit, and remove terms are defined here
        jwordadd.addActionListener(this);
        jwordedit.addActionListener(this);
        jwordremove.addActionListener(this);
        jsimiadd.addActionListener(this);
        jsimiedit.addActionListener(this);
        jsimiremove.addActionListener(this);
        jbroaadd.addActionListener(this);
        jbroaedit.addActionListener(this);
        jbroaremove.addActionListener(this);
        jnarradd.addActionListener(this);
        jnarredit.addActionListener(this);
        jnarrremove.addActionListener(this);

        jreladd.addActionListener(this);
        jreledit.addActionListener(this);
        jrelremove.addActionListener(this);

        JPanel pScroll = new JPanel(null);
        pScroll.setAutoscrolls(true);

        // ABSOLUTE POSITIONING is being used, this panel is far too complex for a layout manager
        sp1.setBounds(30, 20, 80, 20);
        sp2.setBounds(205, 20, 80, 20);
        sp3.setBounds(380, 20, 80, 20);
        sp4.setBounds(555, 20, 80, 20);
        sp5.setBounds(730, 20, 80, 20);

        scrollwordPane.setBounds(30, 40, 141, 280);
        scrollsimiPane.setBounds(205, 40, 141, 280);
        scrollbroaPane.setBounds(380, 40, 141, 280);
        scrollnarrPane.setBounds(555, 40, 141, 280);
        scrollrelPane.setBounds(730, 40, 141, 280);

        jwordadd.setBounds(98, 17, 24, 24);
        jwordedit.setBounds(123, 17, 24, 24);
        jwordremove.setBounds(148, 17, 24, 24);
        jsimiadd.setBounds(273, 17, 24, 24);
        jsimiedit.setBounds(298, 17, 24, 24);
        jsimiremove.setBounds(323, 17, 24, 24);
        jbroaadd.setBounds(448, 17, 24, 24);
        jbroaedit.setBounds(473, 17, 24, 24);
        jbroaremove.setBounds(498, 17, 24, 24);
        jnarradd.setBounds(623, 17, 24, 24);
        jnarredit.setBounds(648, 17, 24, 24);
        jnarrremove.setBounds(673, 17, 24, 24);

        jreladd.setBounds(798, 17, 24, 24);
        jreledit.setBounds(823, 17, 24, 24);
        jrelremove.setBounds(848, 17, 24, 24);

        pScroll.add(sp1, BorderLayout.SOUTH);
        pScroll.add(jwordadd);
        pScroll.add(jwordedit);
        pScroll.add(jwordremove);
        pScroll.add(sp2, BorderLayout.SOUTH);
        pScroll.add(jsimiadd);
        pScroll.add(jsimiedit);
        pScroll.add(jsimiremove);
        pScroll.add(sp3, BorderLayout.SOUTH);
        pScroll.add(jbroaadd);
        pScroll.add(jbroaedit);
        pScroll.add(jbroaremove);
        pScroll.add(sp4, BorderLayout.SOUTH);
        pScroll.add(jnarradd);
        pScroll.add(jnarredit);
        pScroll.add(jnarrremove);
        pScroll.add(scrollwordPane);
        pScroll.add(scrollsimiPane);
        pScroll.add(scrollbroaPane);
        pScroll.add(scrollnarrPane);
        pScroll.add(scrollrelPane);

        pScroll.add(sp5, BorderLayout.SOUTH);
        pScroll.add(jreladd);
        pScroll.add(jreledit);
        pScroll.add(jrelremove);



        JPanel bottomPanel = new JPanel(new BorderLayout());

        jDescription = new JTextArea();
        jDescription.setRows(2);
        jDescription.setColumns(100);
        jDescription.setMargin(new Insets(10, 20, 10, 20));
        jDescription.setBackground(bottomPanel.getBackground());
        jDescription.setLineWrap(true);
        jDescription.setEditable(false);
        jDescription.setWrapStyleWord(true);

        bottomPanel.add(new JLabel("   "), BorderLayout.WEST);
        bottomPanel.add(jDescription, BorderLayout.SOUTH);

        frame.getContentPane().add(topPanel, BorderLayout.NORTH);
        frame.getContentPane().add(pScroll, BorderLayout.CENTER);
        frame.getContentPane().add(bottomPanel, BorderLayout.SOUTH);

        jdesktoppane.add(frame);
        frame.moveToFront();
        frame.setVisible(true);

        jdesktoppane.setVisible(true);
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        final ThesaurusPane tp = this;
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {

                    // On a jSearch event, load new thesaurus terms
                    if (e.getSource().equals(jsearch)) {
                        if (jquery.getText().equals("")) {
                            return;
                        } // never search on everything

                        try {
                            loadThesaurusTerms(jquery.getText());
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                            emptyBox(wordList);
                            System.err.println("No terms found for this query.");
                        } finally {
                            emptyRelationshipBoxes();
                        }
                    }
                    // Now define the various thesaurus manipulation actions for the image icons

                    // ADD:
                    if (e.getSource().equals(jwordadd)) {
                        new NewThesaurusTerm(ITSframe, tp);
                    }
                    if (e.getSource().equals(jsimiadd)) {
                        addThesaurusRelationship("similar", "1");
                    }
                    if (e.getSource().equals(jbroaadd)) {
                        addThesaurusRelationship("broader", "2");
                    }
                    if (e.getSource().equals(jnarradd)) {
                        addThesaurusRelationship("narrower", "3");
                    }
                    if (e.getSource().equals(jreladd)) {
                        addThesaurusRelationship("related", "4");
                    }

                    // EDIT:
                    if (e.getSource().equals(jwordedit)) {
                        ThesaurusWord tw = (ThesaurusWord) wordList.getSelectedValue();
                        if (tw == null) {
                            return;
                        }
                        String inputValue =
                                JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                        if (inputValue != null) {
                            editThesaurusTerm(wordList, tw, inputValue);
                        }
                    }
                    if (e.getSource().equals(jsimiedit)) {
                        ThesaurusWord tw = (ThesaurusWord) simiList.getSelectedValue();
                        if (tw == null) {
                            return;
                        }
                        String inputValue =
                                JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                        if (inputValue != null) {
                            editThesaurusTerm(simiList, tw, inputValue);
                        }
                    }
                    if (e.getSource().equals(jbroaedit)) {
                        ThesaurusWord tw = (ThesaurusWord) broaList.getSelectedValue();
                        if (tw == null) {
                            return;
                        }
                        String inputValue =
                                JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                        if (inputValue != null) {
                            editThesaurusTerm(broaList, tw, inputValue);
                        }
                    }
                    if (e.getSource().equals(jnarredit)) {
                        ThesaurusWord tw = (ThesaurusWord) narrList.getSelectedValue();
                        if (tw == null) {
                            return;
                        }
                        String inputValue =
                                JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                        if (inputValue != null) {
                            editThesaurusTerm(narrList, tw, inputValue);
                        }
                    }

                    if (e.getSource().equals(jreledit)) {
                        ThesaurusWord tw = (ThesaurusWord) relList.getSelectedValue();
                        if (tw == null) {
                            return;
                        }
                        String inputValue =
                                JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                        if (inputValue != null) {
                            editThesaurusTerm(relList, tw, inputValue);
                        }
                    }



                    // REMOVE:
                    if (e.getSource().equals(jwordremove)) { // remove the anchor term
                        ThesaurusWord tw = (ThesaurusWord) wordList.getSelectedValue();
                        if (tw == null) {
                            return;
                        }
                        int i = JOptionPane.showConfirmDialog(ITSframe, "This will remove the thesaurus term and all its " +
                                "relationships.  Continue?", "Remove Term", JOptionPane.YES_NO_OPTION);
                        if (i != 0) {
                            return;
                        }
                        removeAnchorTerm(tw);
                        emptyRelationshipBoxes();
                    }
                    if (e.getSource().equals(jsimiremove)) {
                        removeThesaurusTerm(simiList, "1");
                    } // remove similar term
                    if (e.getSource().equals(jbroaremove)) {
                        removeThesaurusTerm(broaList, "2");
                    } // remove broader term
                    if (e.getSource().equals(jnarrremove)) {
                        removeThesaurusTerm(narrList, "3");
                    } // remove narrower term
                    if (e.getSource().equals(jrelremove)) {
                        removeThesaurusTerm(relList, "4");
                    } // remove narrower term


                } catch (NotAuthorized ex) {
                    JOptionPane.showMessageDialog(ITSframe, "Sorry, You do not have permission to run this operation.",
                            "Error", JOptionPane.NO_OPTION);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    public void performAction(ActionEvent event) {
    }

    public void valueChanged(ListSelectionEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);
                // if the user clicked on the list box, update the other values
                if (e.getSource().equals(wordList) && !((ListSelectionEvent) e).getValueIsAdjusting()) {
                    ThesaurusWord tw = (ThesaurusWord) wordList.getSelectedValue();
                    if (tw == null) return;

                    try {
                        loadThesaurusRelationships(tw);
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                    }

                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    /** Handle the key typed event from the text field. */
    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        final ThesaurusPane tp = this;
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {

                    if (e.getSource().equals(jquery)) {
                        if (jquery.getText().equals("")) {
                            return;
                        } // never search on everything

                        // doing this is now a user preference
                        try {
                            loadThesaurusTerms(jquery.getText());
                        } catch (Exception ex) {
                            emptyBox(wordList);
                            System.err.println("No terms found for this query.");
                        } finally {
                            emptyRelationshipBoxes();
                        }

                    } else {
                        // ADD TERM - INSERT
                        if (((KeyEvent)e).getKeyCode() == KeyEvent.VK_INSERT) {
                            if (e.getSource().equals(wordList)) {
                                new NewThesaurusTerm(ITSframe, tp);
                            }
                            if (e.getSource().equals(simiList)) {
                                addThesaurusRelationship("similar", "1");
                            }
                            if (e.getSource().equals(broaList)) {
                                addThesaurusRelationship("broader", "2");
                            }
                            if (e.getSource().equals(narrList)) {
                                addThesaurusRelationship("narrower", "3");
                            }
                            if (e.getSource().equals(relList)) {
                                addThesaurusRelationship("related", "4");
                            }
                        }
                        // REMOVE TERM - DELETE
                        if (((KeyEvent)e).getKeyCode() == KeyEvent.VK_DELETE) {
                            if (e.getSource().equals(wordList)) {
                                ThesaurusWord tw = (ThesaurusWord) wordList.getSelectedValue();
                                if (tw == null) {
                                    return;
                                }
                                int i = JOptionPane.showConfirmDialog(ITSframe, "This will remove the thesaurus term and all its " +
                                        "relationships.  Continue?", "Remove Term", JOptionPane.YES_NO_OPTION);
                                if (i != 0) {
                                    return;
                                }
                                removeAnchorTerm(tw);
                                emptyRelationshipBoxes();
                            }
                            if (e.getSource().equals(simiList)) {
                                removeThesaurusTerm(simiList, "1");
                            } // remove similar term
                            if (e.getSource().equals(broaList)) {
                                removeThesaurusTerm(broaList, "2");
                            } // remove broader term
                            if (e.getSource().equals(narrList)) {
                                removeThesaurusTerm(narrList, "3");
                            } // remove narrower term
                            if (e.getSource().equals(relList)) {
                                removeThesaurusTerm(relList, "4");
                            } // remove narrower term
                        }
                        // EDIT TERM - F2
                        if (((KeyEvent)e).getKeyCode() == KeyEvent.VK_F2) {
                            if (e.getSource().equals(wordList)) {
                                ThesaurusWord tw = (ThesaurusWord) wordList.getSelectedValue();
                                if (tw == null) {
                                    return;
                                }
                                String inputValue =
                                        JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                                if (inputValue != null) {
                                    editThesaurusTerm(wordList, tw, inputValue);
                                }
                            }
                            if (e.getSource().equals(simiList)) {
                                ThesaurusWord tw = (ThesaurusWord) simiList.getSelectedValue();
                                if (tw == null) {
                                    return;
                                }
                                String inputValue =
                                        JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                                if (inputValue != null) {
                                    editThesaurusTerm(simiList, tw, inputValue);
                                }
                            }
                            if (e.getSource().equals(broaList)) {
                                ThesaurusWord tw = (ThesaurusWord) broaList.getSelectedValue();
                                if (tw == null) {
                                    return;
                                }
                                String inputValue =
                                        JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                                if (inputValue != null) {
                                    editThesaurusTerm(broaList, tw, inputValue);
                                }
                            }
                            if (e.getSource().equals(narrList)) {
                                ThesaurusWord tw = (ThesaurusWord) narrList.getSelectedValue();
                                if (tw == null) {
                                    return;
                                }
                                String inputValue =
                                        JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                                if (inputValue != null) {
                                    editThesaurusTerm(narrList, tw, inputValue);
                                }
                            }
                            if (e.getSource().equals(relList)) {
                                ThesaurusWord tw = (ThesaurusWord) relList.getSelectedValue();
                                if (tw == null) {
                                    return;
                                }
                                String inputValue =
                                        JOptionPane.showInputDialog("Please enter the new value of this thesaurus term.", tw.getName());

                                if (inputValue != null) {
                                    editThesaurusTerm(relList, tw, inputValue);
                                }
                            }

                        }
                    }
                } catch (NotAuthorized ex) {
                    JOptionPane.showMessageDialog(ITSframe, "Sorry, You do not have permission to run this operation.",
                            "Error", JOptionPane.NO_OPTION);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    public void emptyBox(JList list) {
        listData = new Vector();
        list.setListData(listData);
        list.validate();
    }

    public void emptyRelationshipBoxes() {
        listData = new Vector();

        simiList.setListData(listData);
        simiList.validate();
        broaList.setListData(listData);
        broaList.validate();
        narrList.setListData(listData);
        narrList.validate();
        relList.setListData(listData);
        relList.validate();
    }

    public void removeThesaurusTerm(SortedList List, String Relationship) throws NotAuthorized {
        ThesaurusWord tw = (ThesaurusWord) wordList.getSelectedValue();
        ThesaurusWord twS = (ThesaurusWord) List.getSelectedValue();
        if (tw == null) {
            return;
        }
        if (twS == null) {
            return;
        }

        if (!confirmWithKeyAdapter("This will remove this relationship permanently.  Continue?")) {
            return;
        }

        // If this is the last relationship, warn that it will also erase the thesaurus term.
        int size = (simiList.getModel().getSize() +
                    broaList.getModel().getSize() +
                    relList.getModel().getSize() +
                    narrList.getModel().getSize());
        if (size == 1) {
            if (!confirmWithKeyAdapter("This is the last relationship for this term.  If you remove " +
                    "this relationship, the thesaurus term will be removed as well.  Continue?")) {
                return;
            }
            removeAnchorTerm(tw);
            emptyRelationshipBoxes();
            return;
        }

        // invoke RemoveRelationship API call
        try {
            ITSframe.its.removeRelationship(ThesaurusID, tw.getID(), twS.getID(), Relationship);
            int iRelat = new Integer(Relationship).intValue();
            if (iRelat == 1) {
                for (int i = 0; i < simiList.getModel().getSize(); i++) {
                    ThesaurusWord tw2 = (ThesaurusWord) simiList.getModel().getElementAt(i);
                    if (!tw2.getID().equals(twS.getID())) {
                        try {
                            ITSframe.its.removeRelationship(ThesaurusID, twS.getID(), tw2.getID(), "1");
                        } catch (Exception e) {
                            System.out.println("Could not remove: " + twS.getName() + " : " + tw2.getName());
                            e.printStackTrace(System.out);
                        }
                        try {
                            ITSframe.its.removeRelationship(ThesaurusID, tw2.getID(), twS.getID(), "1");
                        } catch (Exception e) {
                            System.out.println("Could not remove: " + twS.getName() + " : " + tw2.getName());
                            e.printStackTrace(System.out);
                        }
                    }
                }
            }
/// rem -- not sure what to do for related terms here
            switch (iRelat) {
                case 2:
                    Relationship = "3";
                    break;
                case 3:
                    Relationship = "2";
                    break;
                case 4:
                    Relationship = "4";
                    break;
            }
            ITSframe.its.removeRelationship(ThesaurusID, twS.getID(), tw.getID(), Relationship);
        } catch (NotAuthorized ex) {
            throw ex;
        } catch (Exception e) {
            System.err.println("Could not remove relationship between " + tw.getName() + " and " + twS.getName());
        }

        // refresh thesaurus list
        try {
            loadThesaurusRelationships(tw);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            emptyBox(List);
        } // no more words?
    }

    public void editThesaurusTerm(SortedList list, ThesaurusWord tw, String sNewName) throws NotAuthorized {
        // edit = remove then add relationship
        try {
            ITSframe.its.editRelationship(tw.getName(), sNewName);
        } catch (NotAuthorized ex) {
            throw ex;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.err.println("Could not change name from " + tw.getName() + " to " + sNewName);
            JOptionPane.showMessageDialog(ITSframe, "Sorry, the term "+sNewName+" already exists in the database.",
                    "Error", JOptionPane.NO_OPTION);
        }

        // refresh thesaurus list
        try {
            int selectedIndex = wordList.getSelectedIndex();
            emptyRelationshipBoxes();
            loadThesaurusTerms(jquery.getText());
            wordList.setSelectedIndex(selectedIndex);

        } catch (Exception e) {
        } // no more words?
    }

    public void removeAnchorTerm(ThesaurusWord tw) throws NotAuthorized {
        try {
            ITSframe.its.removeAnchorTerm(ThesaurusID, tw.getID());
        } catch (NotAuthorized ex) {
            throw ex;
        } catch (Exception e) {
            System.err.println("Could not remove anchor term: " + tw.getName());
        }

        // refresh thesaurus list
        try {
            loadThesaurusTerms();
        } catch (Exception e) {
            emptyBox(wordList);
        } // no more words?
    }


    public boolean hasMoreRelationships(String Term1, String Type, String Term2) throws Exception {
        int iRelationship = -1;
// not sure how to handle related terms here
        if (Type.equals("similar")) { iRelationship = 1; }
        else if (Type.equals("broader")) { iRelationship = 2; }
        else { iRelationship = 3; }

        Vector vRelationships = ITSframe.its.getSynonyms(ThesaurusID, Term1);
        for (int i = 0; i < vRelationships.size(); i++) {
            ThesaurusWord tw = (ThesaurusWord) vRelationships.elementAt(i);
            if ((tw.getRelationship() == iRelationship) &&
                (!tw.getName().toLowerCase().equals(Term2.toLowerCase()))) {
                return true;
            }
        }

        return false;
    }

    public void addThesaurusRelationship(String Type, String Relationship) throws Exception {
        String inputValue = JOptionPane.showInputDialog("Please enter the new " + Type + " term for this thesaurus word.");
        if (inputValue != null) {
            if (hasMoreRelationships(inputValue, Type, Relationship)) {
                if (!confirmWithKeyAdapter("The term that you are adding, '"+inputValue+"', has existing relationships "+
                                           "in this thesaurus.  Continue?")) { return; }
            }
            addThesaurusRelationship(inputValue, Type, Relationship);
        }
    }

    public void addThesaurusRelationship(String Word, String Type, String Relationship) throws Exception {
        ThesaurusWord tw = (ThesaurusWord) wordList.getSelectedValue();
        if (tw == null) {
            return;
        }
        addThesaurusRelationship(tw, Word, Relationship);
    }

    public void addThesaurusRelationship(String AnchorTerm, ThesaurusWord twObjectTerm, String Relationship) throws NotAuthorized {
        try {
            ITSframe.its.addRelationship(ThesaurusID, AnchorTerm, twObjectTerm.getName(), Relationship);
        } catch (NotAuthorized ex) {
            throw ex;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.err.println("Could not add new relationship: " + AnchorTerm);
        }

        // refresh thesaurus list
        try {
            int selectedIndex = wordList.getSelectedIndex();
            loadThesaurusTerms();
            wordList.setSelectedIndex(selectedIndex);

            // now highlight the phrase

        } catch (Exception e) {
            emptyBox(wordList);
            e.printStackTrace(System.out);
        } // no more words?
    }

    public void addThesaurusRelationshipExternal(String AnchorTerm, String ObjectTerm, String Relationship)
            throws NotAuthorized {
        try {
            int iRelat = new Integer(Relationship).intValue();

            ITSframe.its.addRelationship(ThesaurusID, AnchorTerm, ObjectTerm, Relationship);
            // now create the reciprical relationship:

            // not sure what to do with related here

            switch (iRelat) {
                case 2:
                    Relationship = "3";
                    break;
                case 3:
                    Relationship = "2";
                    break;
            }
            ITSframe.its.addRelationship(ThesaurusID, ObjectTerm, AnchorTerm, Relationship);
        } catch (NotAuthorized ex) {
            JOptionPane.showMessageDialog(ITSframe, "Sorry, You do not have permission to run this operation.",
                    "Error", JOptionPane.NO_OPTION);
            throw ex;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                    "Error", JOptionPane.NO_OPTION);
            System.err.println("Could not add new relationship: " + AnchorTerm);
        }

        // refresh thesaurus list
        try {
            loadThesaurusTerms();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } // no more words?
    }

    public void addThesaurusRelationship(ThesaurusWord twAnchorTerm, String ObjectTerm, String Relationship) throws NotAuthorized {
        try {
            int iRelat = new Integer(Relationship).intValue();
            ITSframe.its.addRelationship(ThesaurusID, twAnchorTerm.getName(), ObjectTerm, Relationship);
// not sure what to do with related here
            // create the reciprical relationship:
            switch (iRelat) {
                case 2:
                    Relationship = "3";
                    break;
                case 3:
                    Relationship = "2";
                    break;
            }
            ITSframe.its.addRelationship(ThesaurusID, ObjectTerm, twAnchorTerm.getName(), Relationship);

            // create sibling relationships
            if (iRelat == 1) {
                for (int i = 0; i < simiList.getModel().getSize(); i++) {
                    ThesaurusWord tw = (ThesaurusWord) simiList.getModel().getElementAt(i);
                    ITSframe.its.addRelationship(ThesaurusID, ObjectTerm, tw.getName(), "1");
                    ITSframe.its.addRelationship(ThesaurusID, tw.getName(), ObjectTerm, "1");
                }
            }
        } catch (NotAuthorized ex) {
            throw ex;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.err.println("Could not add new relationship: " + twAnchorTerm.getName());
        }

        // refresh thesaurus list
        try {
            loadThesaurusRelationships(twAnchorTerm);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } // no more words?
    }

    public void loadThesaurusTerms() throws Exception {
        loadThesaurusTerms(jquery.getText());
    }

    public void loadThesaurusTerms(String Like) throws Exception {
        //if (Like.trim().replaceAll("%", "").equals("")) { return; }

        listData = ITSframe.its.getThesaurusWords(ThesaurusID, Like);
        wordList.setListData(listData);
        wordList.sort();
        wordList.validate();

        System.out.println("Loaded: " + listData.size() + " thesaurus words starting with '" + Like + "'.");
    }

    public void loadThesaurusRelationships(ThesaurusWord tw) throws Exception {
        ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        if (tw == null) return; // do nothing if for whatever reason nothing is selected

        try {
            listData = ITSframe.its.getSynonyms(ThesaurusID, new Integer(tw.getID()).intValue());
        } catch (Exception e) {
            listData = new Vector();
        }
        jDescription.setText("You selected the anchor term: \"" + tw.getName() + "\" with relationships");

        Vector vSimi = new Vector();
        Vector vBroa = new Vector();
        Vector vNarr = new Vector();
        Vector vRel = new Vector();

        for (int i = 0; i < listData.size(); i++) {
            tw = (ThesaurusWord) listData.elementAt(i);
            switch (tw.getRelationship()) {
                case 1:
                    vSimi.add(tw);
                    jDescription.setText(jDescription.getText() + ", similar term: \"" + tw.getName() + "\"");
                    break;
                case 2:
                    vBroa.add(tw);
                    jDescription.setText(jDescription.getText() + ", broader term: \"" + tw.getName() + "\"");
                    break;
                case 3:
                    vNarr.add(tw);
                    jDescription.setText(jDescription.getText() + ", narrower term: \"" + tw.getName() + "\"");
                    break;
                case 4:
                    vRel.add(tw);
                    jDescription.setText(jDescription.getText() + ", related term: \"" + tw.getName() + "\"");
                default:
                    break;
            }
        }
        jDescription.setText(jDescription.getText() + ".");

        // Load the relationship list boxes as appropriate.
        if (!vSimi.isEmpty()) {
            simiList.setListData(vSimi);
            simiList.validate();
        } else {
            simiList.setListData(new Vector());
        }
        if (!vBroa.isEmpty()) {
            broaList.setListData(vBroa);
            broaList.validate();
        } else {
            broaList.setListData(new Vector());
        }
        if (!vNarr.isEmpty()) {
            narrList.setListData(vNarr);
            narrList.validate();
        } else {
            narrList.setListData(new Vector());
        }
        if (!vRel.isEmpty()) {
            relList.setListData(vRel);
            relList.validate();
        } else {
            relList.setListData(new Vector());
        }

        simiList.sort();
        broaList.sort();
        narrList.sort();
        relList.sort();

        ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    public boolean confirmWithKeyAdapter(String message) {
        JOptionPane pane = new JOptionPane(message, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        JPanel jp = (JPanel) pane.getComponent(1);
        Component[] c = jp.getComponents();

        JButton buttonYes = (JButton) c[0];
        JButton buttonNo = (JButton) c[1];

        KeyAdapter key = new KeyAdapter() {
            public void keyPressed(KeyEvent ex) {
                if (ex.getKeyCode() == KeyEvent.VK_Y) {
                    confirmDialog.dispose();
                    bConditionMet = true;
                }
                if (ex.getKeyCode() == KeyEvent.VK_N) {
                    confirmDialog.dispose();
                    bConditionMet = false;
                }
            }
        };
        buttonYes.addKeyListener(key);
        buttonNo.addKeyListener(key);

        confirmDialog = pane.createDialog(null, "Question");
        confirmDialog.show();
        Object selectedValue = null;
        selectedValue = pane.getValue();

        try {
            int i = ((Integer) selectedValue).intValue();
            if (i != 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
        } // keyboard short cut was used, no action required

        return bConditionMet;
    }
}
