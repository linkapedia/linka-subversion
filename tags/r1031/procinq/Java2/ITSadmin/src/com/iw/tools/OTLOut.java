package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class OTLOut extends ExportElement {
    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");
    private Hashtable filtercommonWords = new Hashtable();
    private Hashtable sigcommonWords = new Hashtable();

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        //This file drives the naming of the output files.  Cengage delivers a file which maps the topic names to
        //an id and a preferred file name.  We match the node description (not title) to the first value,
        //the topic name, in the file.
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\tlnames.txt"))));
        
        String record = null; 
        Hashtable tlfilename = new Hashtable();
        Hashtable tltopic = new Hashtable();
        try {
            while ((record=dis.readLine())!= null) 
            {
            	record = record.replaceAll("\"", "");
            	String[] sArr = record.split(",");
            	tlfilename.put(sArr[0],sArr[2]);
            	tltopic.put(sArr[0],sArr[1]);
            }
        } 
        catch (IOException e)
        {
        	System.err.println("Warning: There was an error while reading the file tlnames.txt");
        }
        
        //the commonwords file contains the words from Cengage that they want filtered from the must haves only.
        DataInputStream disfilter = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\filtercommonwords.txt"))));

        String filterrecord = null;
        try 
        {
            while ( (filterrecord=disfilter.readLine()) != null ) {
            	filtercommonWords.put(filterrecord.toLowerCase(), "1"); }
        }
        catch (IOException e) 
        {
        	System.err.println("Warning: There was an error while reading the file filtercommonwords.txt");
        }

        //the commonwords file contains the words from Cengage that they want filtered from the must haves only.
        disfilter = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        try 
        {
            while ( (filterrecord=disfilter.readLine()) != null ) {
            	sigcommonWords.put(filterrecord.toLowerCase(), "1"); }
        }
        catch (IOException e) 
        {
        	System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+c.getID(), 1, 50000);
        if (vNodes.size() < 1) return null;

        Vector files = new Vector();

        File dir = new File(c.getName().replaceAll(" ", "_"));
        if (!dir.exists()) dir.mkdir();

        //loop through each node in the taxonomy and create a separate .otl file for it
        for (int loop = 0; loop < vNodes.size(); loop++) 
        {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(loop);

            File f = new File("c:/tl/"+tlfilename.get(n.get("NODEDESC"))+".otl");
            FileOutputStream fos = new FileOutputStream(f);
            System.out.println("**Writing file:**" + n.get("NODEDESC"));
            Writer out = new OutputStreamWriter(fos, "UTF8");

            // *** WRITE THE HEADER HERE
            out.write("$control: 1\n\n");
            out.write("/opname = '_TypoEqual Typo'\n");
            out.write(tlfilename.get(n.get("NODEDESC"))+ " <And>\n");

            out.write("/author = \"ITS_TL_SHELL\"\n");
            
            String sNewTitle = n.get("NODEDESC").replaceAll("\\(", " ");	//replace open parentheses
            sNewTitle = sNewTitle.replaceAll("\\)", " "); 		//replace close parentheses
            sNewTitle = sNewTitle.replaceAll("  ", " ").trim();	//replace any double spaces that we just added
            sNewTitle = sNewTitle.replaceAll(" ","_");			//replace spaces with underscore
                        
            //out.write("* 0.90 " + n.get("NODEDESC").replaceAll(" ","_").replaceAll("(","".replaceAll(")","")).trim()+ "_pos \n");
            out.write("* 0.90 " + sNewTitle + "_pos \n");
            out.write("** 0.55 <In>\n");
            out.write("/zonespec = \"`TI`\"\n");
            out.write("***<Any>\n");

            // no longer adding title - duplicate in must have with different case, which they don't care about.
            /*
            // get title
            String[] sArr = n.get("NODETITLE").trim().split(" ");
            if (sArr.length !=1)
            {
                // this is a phrase
                out.write("**** <Many><Phrase>\n");
                out.write("/author = \"nodetitle\"\n");
                for (int i = 0; i < sArr.length; i++) 
                {
                    out.write("***** \"" + sArr[i].trim() + "\"\n");
                }
            }
            else
            {
            	if (sArr.length == 3)
            		out.write("**** <Case><Word>\n");
            	else
            		out.write("**** <Word>\n");
            	
                out.write("/author = \"nodetitle\"\n");
                out.write("/wordtext = \"" + n.get("NODETITLE").trim() + "\"\n");
            }
            */

            // get thesaurus expansions
            Vector vThesaurus = new Vector();
            Corpus corp = server.getCorpora(n.get("CORPUSID"));
            Vector vThesauri = server.getThesauri(corp);

            for (int i = 0; i < vThesauri.size(); i++) 
            {
                Thesaurus t = (Thesaurus) vThesauri.elementAt(i);
                Vector vterms = new Vector();

                // for each thesaurus, attempt to find the thesaurus word so
                // that we can do a synonym lookup
                try 
                {
                    vterms = server.getThesaurusWords(t.getID(), n.get("NODETITLE").toLowerCase(), true);
                    for (int k = 0; k < vterms.size(); k++) 
                    {
                        ThesaurusWord tw = (ThesaurusWord) vterms.elementAt(k);
                        if (tw.getName().toLowerCase().equals(n.get("NODETITLE").toLowerCase())) 
                        {
                            try 
                            {
                                Vector synonyms = server.getSynonyms(t.getID(), Integer.parseInt(tw.getID()));
                                for (int l = 0; l < synonyms.size(); l++) 
                                {
                                    ThesaurusWord tw2 = (ThesaurusWord) synonyms.elementAt(l);
                                    tw2.setThesaurusName(t.getName());
                                    vThesaurus.add(tw2);
                                }
                            } catch (Exception e) { }
                        }
                    }
                } catch (Exception e) { }
            }

            //Count child nodes (thesaurus terms, must haves, and signatures
            //If there is more than 1024 total, print and pop-up error, and then continue
            int iTotalCount = 0;
            
            iTotalCount += vThesaurus.size();
            if (iTotalCount>1024)
            {
            	System.out.println("**Greater than 1024 thesaurus entries!");
            	System.out.println("**UNABLE TO EXPORT NODE " + n.get("NODETITLE"));
            	out.write("\nERROR\n");
                out.write("**Greater than 1024 thesaurus entries!\n");
                out.write("** " + vThesaurus.size() + " thesaurus terms.\n");
                out.write("**Unable to export this node!\n");
            	out.write("ERROR");
                out.close();
                System.out.println("**File complete and closed**" + n.get("NODEDESC"));

                nodesProcessed++;
                if (ppb != null) {
                    long perc = (nodesProcessed*100) / totalNodes;
                    ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
                }
                files.add(f);
                
                String message = "Node " + n.get("NODETITLE") + " has greater than 1024 thesaurus, musthave and signature entries! Unable to export node."; 
                JOptionPane.showMessageDialog(null, message, "Information", JOptionPane.NO_OPTION);
            	continue;
            }
            System.out.println("**Number of thesaurus entries:**" + vThesaurus.size());
            System.out.println("**   for NodeTitle**" + n.get("NODETITLE"));

            out.write(OutThes(vThesaurus, "***"));

             // get must haves
            Vector vHave = new Vector();

            vHave = server.getMusthaves(n.get("NODEID"));
            iTotalCount += vHave.size();
            if (iTotalCount>1024)
            {
            	System.out.println("**Greater than 1024 thesaurus and must have entries!");
            	System.out.println("**UNABLE TO EXPORT NODE " + n.get("NODETITLE"));
            	out.write("\nERROR\n");
                out.write("**Greater than 1024 thesaurus and must have entries!\n");
                out.write("** " + vThesaurus.size() + " thesaurus terms.\n");
                out.write("** " + vHave.size() + " must have terms.\n");
                out.write("**Unable to export this node!\n");
            	out.write("ERROR");
                out.close();
                System.out.println("**File complete and closed**" + n.get("NODEDESC"));

                nodesProcessed++;
                if (ppb != null) {
                    long perc = (nodesProcessed*100) / totalNodes;
                    ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
                }
                files.add(f);

                String message = "Node " + n.get("NODETITLE") + " has greater than 1024 thesaurus, musthave and signature entries! Unable to export node."; 
                JOptionPane.showMessageDialog(null, message, "Information", JOptionPane.NO_OPTION);
            	continue;
            }
            System.out.println("**Number of Must Haves:**" + vHave.size());
            System.out.println("**   for NodeTitle**" + n.get("NODETITLE"));

            // logic for outputing must haves
            out.write(OutMustHave(vHave, "***"));

            // put in title plus any must haves and thesaurus expansions
            out.write("** 0.40 <Any>\n");
            out.write("*** <In>\n");
            out.write("/zonespec = \"`AA`\"\n");
            out.write("**** <Any>\n");

            //Do not add title since they are duplicates in the must haves with capitalization, which they don't care about
            //out.write(OutTitle(n,"****"));	
            out.write(OutThes(vThesaurus,"****"));
            out.write(OutMustHave(vHave,"****"));

            // here goes the same things that are in the first section
            out.write("*** <In>\n");
            out.write("/zonespec = \"`FHW`\"\n");
            out.write("**** <Any>\n");

            //Do not add title since they are duplicates in the must haves with capitalization, which they don't care about
            //out.write(OutTitle(n,"****"));
            out.write(OutThes(vThesaurus,"****"));
            out.write(OutMustHave(vHave,"****"));
            
            // same as above
            out.write("** 0.1 <Many><Any>\n");
            out.write("/author = \"TL -ITS Shell\"\n");

            out.write(OutTitle(n,"**"));
            out.write(OutThes(vThesaurus,"**"));
            out.write(OutMustHave(vHave,"**"));

            //put in sigs here
            server.getNodeSignatures(n);
            Vector vSignatures = server.getNodeSignatures(n);
            iTotalCount += vSignatures.size();
            if (iTotalCount>1024)
            {
            	System.out.println("**Greater than 1024 thesaurus, must have and signature entries!");
            	System.out.println("**UNABLE TO EXPORT NODE " + n.get("NODETITLE"));
            	out.write("\nERROR\n");
                out.write("**Greater than 1024 thesaurus, must have and signature entries!\n");
                out.write("** " + vThesaurus.size() + " thesaurus terms.\n");
                out.write("** " + vHave.size() + " must have terms.\n");
                out.write("** " + vSignatures.size() + " signature terms.\n");
                out.write("**Unable to export this node!\n");
            	out.write("ERROR");
                out.close();
                System.out.println("**File complete and closed**" + n.get("NODEDESC"));

                nodesProcessed++;
                if (ppb != null) {
                    long perc = (nodesProcessed*100) / totalNodes;
                    ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
                }
                files.add(f);

                String message = "Node " + n.get("NODETITLE") + " has greater than 1024 thesaurus, musthave and signature entries! Unable to export node."; 
                JOptionPane.showMessageDialog(null, message, "Information", JOptionPane.NO_OPTION);
            	continue;
            }
            System.out.println("**Number of Sigs:**" + vSignatures.size());
            System.out.println("**   for NodeTitle**" + n.get("NODETITLE"));
            StringBuffer sigBuffer = new StringBuffer();

            if (vSignatures.size()>0)
            {
	         	for (int i = 0; i < vSignatures.size(); i++) 
	            {
	            	Signature s = (Signature) vSignatures.elementAt(i);
	            	String sSignature = s.getWord().trim();
	                
	                if (!sigcommonWords.containsKey(sSignature))
	                	 sigBuffer.append(GetEvidenceString(sSignature, "**", "signature"));
	            }
	            
	          	out.write(sigBuffer.toString());	//output signature terms
            }
            
            sNewTitle = n.get("NODEDESC").replaceAll("\\(", " ");	//replace open parentheses
            sNewTitle = sNewTitle.replaceAll("\\)", " "); 		//replace close parentheses
            sNewTitle = sNewTitle.replaceAll("  ", " ").trim();	//replace any double spaces that we just added
            sNewTitle = sNewTitle.replaceAll(" ","_");			//replace spaces with underscore
                        
            //out.write("* 1.00 " + n.get("NODETITLE").replaceAll(" ","_")+ "_neg <Not><Any>\n");
            out.write("* 1.00 " + sNewTitle + "_neg <Not><Any>\n");
            
            out.write("** <In> \n");
            out.write("/zonespec = \"`TI`\"\n");
            out.write("*** <Any> \n");
            out.write("**** 'appoint'\n");
            out.write("**** \"appointment\"\n");
            out.write("**** \"acquire\"\n");
            out.write("**** \"calendar\"\n");
            out.write("**** \"acquisition\"\n");
            out.write("**** \"earnings\"\n");
            out.write("**** \"hire\"\n");
            out.write("**** \"merge\"\n");
            out.write("**** \"merger\"\n");
            out.write("**** \"name\"\n");
            out.write("**** \"nomination\"\n");
            out.write("**** \"obituary\"\n");
            out.write("**** \"quarter\"\n");
            out.write("**** \"quarterly\"\n");
            out.write("**** <Many><Phrase>\n");
            out.write("***** \"letter\"\n");
            out.write("***** \"to\"\n");
            out.write("***** \"the\"\n");
            out.write("***** \"editor\"\n");

            out.write("** <In>\n");
            out.write("/zonespec = \"`FHW`\"\n");
            out.write("*** <Any>\n");
            out.write("**** <Word>\n");
            out.write("/wordtext = \"illus\"\n");
            out.write("**** \"hardbound\"\n");
            out.write("**** \"paperback\"\n");
            out.write("**** \"hardback\"\n");
            out.write("**** \"softcover\"\n");
            out.write("**** \"hardcover\"\n");
            out.write("**** <Word>\n");
            out.write("/wordtext = \"pp\"\n");
            out.write("**** <Many><Phrase>\n");
            out.write("***** \"to\"\n");
            out.write("***** \"the\"\n");
            out.write("***** \"editor\"\n");
            out.write("** <Word>\n");
            out.write("/wordtext = \"ISBN\"\n");
            out.close();
            
            System.out.println("**File complete and closed**" + n.get("NODEDESC"));

            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }

            files.add(f);
        }

        return (File) files.elementAt(0);
    }
    
    //**************************************************************
    private String OutMustHave(Vector vHave, String star)
    {
    	StringBuffer buffer = new StringBuffer();

    	if (vHave.size() > 0)
        {
        	for (int i = 0; i < vHave.size(); i++) 
        	{
                String sMustHave = vHave.elementAt(i).toString().trim();
                if (!filtercommonWords.containsKey(sMustHave.toLowerCase()))
                	buffer.append(GetEvidenceString(sMustHave, star, "must have"));
        	}
        }

    	return buffer.toString();	// output must haves from data structure
    }

    //**************************************************************
    private String OutThes(Vector vThs, String star)
    {
    	StringBuffer buffer = new StringBuffer();
    	if (vThs.size() >0)
    	{
         	for (int i = 0; i < vThs.size(); i++) 
         	{
         	   String sThesaurusTerm = vThs.elementAt(i).toString().trim();
               if (!filtercommonWords.containsKey(sThesaurusTerm))
            	   buffer.append(GetEvidenceString(sThesaurusTerm,star,"thesaurus"));
         	}
          }

    	 return buffer.toString();	 // output thesaurus terms from data structure
    }

    //**************************************************************
    private String GetEvidenceString(String sEvidence, String star, String sAuthor)
    {	//Evidence can be must have term, thesaurus term or signature term
    	StringBuffer buffer = new StringBuffer();

    	//determine if the must have contains any invalid characters 
        //(invalid is anything outside of range: letters a-z, numbers 0-9, hyphen, underscore)
        Pattern p = Pattern.compile("[^a-zA-Z0-9-_ ]");
        Matcher m = p.matcher(sEvidence);
        StringBuffer sbInvalidReplaced = new StringBuffer(sEvidence);
        boolean bInvalidFound = false;
 
        String[] sVhv = null;
        sEvidence.trim().split(" ");

        while (m.find())
        {
        	bInvalidFound = true;
        	sbInvalidReplaced.setCharAt(m.start(),' ');
        }
 
        if (bInvalidFound)		
        {	//format using <order><near/3>, because invalid chars replaced with spaces
        	sVhv = sbInvalidReplaced.toString().trim().split(" ");

        	buffer.append(star + "* <Order><Near/3>\n");
        	buffer.append("/author = \"" + sAuthor + "\"\n");
        	String sOutput = null;
            for (int j = 0; j < sVhv.length; j++) 
            {	
                //only add if not a space
            	sOutput = sVhv[j].trim();
            	if (sOutput.length()>=1)
          			buffer.append(star + "** \"" + sOutput + "\"\n");
           }
        }
        else
        {	//all chars are valid, so treat normally
           	sVhv = sEvidence.trim().split(" ");

            if (sVhv.length !=1)	// this is a phrase
            {
            	buffer.append(star + "* <Many><Phrase>\n");	
            	buffer.append("/author = \"" + sAuthor + "\"\n");
            	for (int j = 0; j < sVhv.length; j++) 
            	{
            		buffer.append(star + "** \"" + sVhv[j].trim() + "\"\n");
            	}
            }
            else
            {
            	int elementLength = sEvidence.length();
            	if (elementLength > 2)
            	{
                   	//if single word contains a single hyphen, and post-hyphen string is all numeric,
            		//then break into phrase at hyphen
                    Pattern p2 = Pattern.compile("[-]");
                    Matcher m2 = p2.matcher(sEvidence);
                    boolean bHyphen = false;
                    boolean bNumericOnly = false;
                   	int currentIndex = 0;
                   	String currentSubString = null;
                    StringBuffer hyphenBuffer = new StringBuffer();
                   
                    while (m2.find())
                    {
                    	bHyphen = true;
                    	if (m2.start()>currentIndex)	//populate other chars from original string
	                    {
	                    	hyphenBuffer.append(sEvidence.substring(currentIndex, (m2.start())));
	                        hyphenBuffer.append(" ");
	                    }
	                    		
                      	currentIndex = m2.end();	//reset index
                    }
                    
                    if (bHyphen)
                    {
                    	//populate last string from evidence
                     	currentSubString = sEvidence.substring(currentIndex, sEvidence.length());
                    	hyphenBuffer.append(currentSubString);
 
                    	//need to determine if the last post-hyphen string is numeric only
                    	//and break into phrase at that last hyphen
                    	bNumericOnly = true; 	//assume it is until proven otherwise
                    	for (int k=0;k<currentSubString.length();k++)
                    	{
                    		if ((currentSubString.charAt(k) > '0') && (currentSubString.charAt(k) <= '9')){}
                    		else
                			{
                    			bNumericOnly = false;  //hit a char that is not numeric
                    			break;
                			}
                    	}
                    }
                    
                    if (bHyphen && bNumericOnly)
                    {
                        buffer.append(GetEvidenceString(hyphenBuffer.toString(), star, sAuthor));
                    }
                    else
                    {
	            		if (elementLength == 3)
	            		{
	            			buffer.append(star + "* <Case><Word>\n");
	                    	buffer.append("/author = \"" + sAuthor + "\"\n");
	            		}
	                	else
	                	{
	                		buffer.append(star + "* <Word>\n");
	                    	buffer.append("/author = \"" + sAuthor + "\"\n");
	                	}
	            		
	                   	buffer.append("/wordtext = \"" + sEvidence + "\"\n");
                    }
               	}
            }
        }
        return buffer.toString();
    }
    
   //**************************************************************
    private String OutTitle(Node n, String star){
    	StringBuffer buffer = new StringBuffer();

    	//not checking node title for invalid chars b/c they came from customer
    	String[] sArr = n.get("NODETITLE").trim().split(" ");
        if (sArr.length !=1)	// this is a phrase
        {
        	buffer.append(star + "* <Many><Phrase>\n");
            buffer.append("/author = \"nodetitle\"\n");
        	for (int i = 0; i < sArr.length; i++) 
        	{
        		buffer.append(star + "** \"" + sArr[i].trim() + "\"\n");
        	}
        }
        else
        {
        	if (n.get("NODETITLE").trim().length()==3)
        		buffer.append(star + "* <Case><Word>\n");
        	else
        		buffer.append(star + "* <Word>\n");
            buffer.append("/author = \"nodetitle\" \n");
        	buffer.append("/wordtext = \"" + n.get("NODETITLE").trim() + "\"\n");
        }

    	return buffer.toString();
    }

    //**************************************************************
    private File createOrGetFile(String path) throws Exception 
    {
        File f = new File(path);
        
        if (!f.exists()) 
        	f.mkdir();

        return f;
    }

    //**************************************************************
    private String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') 
            	buffer.append(".*"); 
            else if (chars[i] == '?') 
            	buffer.append("."); 
            else 
            	buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method
}