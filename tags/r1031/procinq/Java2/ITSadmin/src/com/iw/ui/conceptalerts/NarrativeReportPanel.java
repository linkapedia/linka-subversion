package com.iw.ui.conceptalerts;

import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.ui.*;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

public class NarrativeReportPanel extends DocumentSearch implements ActionListener {
    protected RowColorJTable resultsTable;
    public String narrativeID = "-1";

    public NarrativeReportPanel (ITSAdministrator frame) throws Exception { super(frame); }
    public NarrativeReportPanel (ITSAdministrator frame, Vector documents) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        user = ITS.getUser();

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Narrative Report", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(675, 480);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        cancelButton = new JButton("Close");
        cancelButton.addActionListener(this);
        openButton = new JButton("Open");
        openButton.addActionListener(this);
        relatedButton = new JButton("Related");
        relatedButton.addActionListener(this);
        documentButton = new JButton("Classify");
        documentButton.addActionListener(this);

        chartButton = new JButton("Chart");
        chartButton.addActionListener(this);
        chartButton.setToolTipText("Generate a bar chart with a historical perspective of this document set.");

        resultsTable = new RowColorJTable(2, ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS.intValue(), (Object) new String(""));
        loadResults(documents);

        alertButton = new JButton("Alert");
        alertButton.addActionListener(this);
        alertButton.setToolTipText("Create an alert on this query.");

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                            {
                                layout.add(new JScrollPane(resultsTable));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(cancelButton);
                                        layout.addSpace(10);
                                        layout.add(openButton);
                                        layout.addSpace(10);
                                        layout.add(relatedButton);
                                        layout.addSpace(10);
                                        //layout.add(new JSeparator());
                                        layout.add(documentButton);
                                        layout.addSpace(10);
                                        layout.add(chartButton);
                                        layout.addSpace(10);
                                        layout.add(alertButton);
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();

            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(searchButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();

    }

    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        final DocumentSearch ds = this;

        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    if (e.getSource().equals(cancelButton)) {
                        MDIframe.dispose();
                    } else if (e.getSource().equals(browseButton)) {
                        try {
                            TopicSearch ts = new TopicSearch(ITSframe, ds);
                            ts.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    } else if (e.getSource().equals(alertButton)) { // open create alert dialog
                        String sCQL = "SELECT <NARRATIVE> WHERE NARRATIVEID = "+narrativeID+" CACHE";
                        System.out.println("CQL: "+sCQL);

                        AddAlert aa = new AddAlert(ITSframe, sCQL);
                        aa.MDIframe.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        aa.setVisible(true);
                        aa.MDIframe.moveToFront();
                        aa.show();

                    } else if (e.getSource().equals(chartButton)) { // open chart
                        if (resultsTable.getRowCount() == 0) { return; }

                        Vector v = new Vector();

                        // use selected rows to build the chart, or if none are selected, use everything
                        if (resultsTable.getSelectedRow() == -1) {
                            for (int i = 0; i < resultsTable.getRowCount(); i++) {
                                try { v.add((com.iw.system.Document) resultsTable.getModel().getValueAt(i, 0)); }
                                catch (ClassCastException e) { }
                            }
                        } else {
                            for (int i = 0; i < resultsTable.getSelectedRows().length; i++) {
                                try { v.add((com.iw.system.Document) resultsTable.getModel().getValueAt(i, 0)); }
                                catch (ClassCastException e) { }
                            }
                        }

                        DocumentChart dc = new DocumentChart(ITSframe, v);
                        ITSframe.jdp.add(dc);
                        dc.setBounds(10, 30, 900, 580);
                        dc.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        dc.setVisible(true);
                        dc.moveToFront();
                        dc.show();
                    } else if (e.getSource().equals(openButton)) {
                        viewDocument();
                    } else if (e.getSource().equals(relatedButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row >= 0) {
                            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);
                            try {
                                NodeDocView ndv = new NodeDocView(ITSframe, d, null);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    } else if (e.getSource().equals(documentButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row >= 0) {
                            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);
                            if (user.modeAbstractDocument() && d.get("DOCUMENTSUMMARY").equals("")) {
                                JOptionPane.showMessageDialog(ITSframe, "Sorry, that abstract is empty.", "Information", JOptionPane.NO_OPTION);
                                return;
                            }
                            try {
                                NodeDocView ndv = new NodeDocView(ITSframe, d, user);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    protected void loadResults(Vector vDocs) throws Exception {
        if (vDocs.size() == 0) {
            JOptionPane.showMessageDialog(ITSframe, "No documents were found to match your search criteria.",
                    "Information", JOptionPane.NO_OPTION);

            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);
            return;
        }

        int iTotal = 0;

        DefaultSortTableModel dtm = new DefaultSortTableModel(vDocs.size()-1, 3);
        dtm.setEditable(false);
        for (int i = 0; i < vDocs.size(); i++) {
            Document d = (Document) vDocs.elementAt(i);

            if (i != 0) {
                dtm.setValueAt(d, i-1, 0);
                dtm.setValueAt(d.get("DOCUMENTSUMMARY"), i-1, 1);

                float score = 100*((float) new Float(d.get("THECOUNT")).floatValue()/(float) iTotal);
                dtm.setValueAt(((int) score)+".0", i-1, 2);
            } else {
                iTotal = new Integer(d.get("THECOUNT")).intValue();
            }
        }
        Vector v = new Vector();
        v.add("Document Title");
        v.add("Abstract");
        v.add("Simularity");
        dtm.setColumnIdentifiers(v);

        resultsTable.setModel(dtm);
    }

    public void viewDocument() {
        try {
            int iSelectedRow = resultsTable.getSelectedRow();
            if (iSelectedRow < 0) {
                JOptionPane.showMessageDialog(ITSframe, "You must first select a document to view.", "Information", JOptionPane.NO_OPTION);
                return;
            }
            int row = resultsTable.getSelectedRow();
            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);

            String sCommand = "";
            if (ITS.getUser().getPathPrefix().equals("")) {
                sCommand = "cmd /c explorer \"" + d.get("DOCURL")+"\"";
            } else {
                sCommand = "cmd /c explorer \"" + ITS.getUser().getPathPrefix() + d.get("DOCURL") +"\"";
            }

            //System.out.println("start view doc with command [" + sCommand + "]");
            java.lang.Runtime.getRuntime().exec(sCommand);


        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }
}
