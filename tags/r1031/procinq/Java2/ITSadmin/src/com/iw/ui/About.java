package com.iw.ui;

import java.awt.*;
import javax.swing.*;

public class About extends JPanel {
    Image image;

    public About() {
        image = Toolkit.getDefaultToolkit().getImage("itsimages/EditorDeskSpash.JPG");
        setOpaque(false);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
    }
}