package com.iw.ui.explain;

//import java.awt.*;

import java.awt.event.*;
import java.awt.*;
//import java.awt.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.event.*;
import javax.swing.border.*;

import com.oculustech.layout.*;
import com.iw.ui.ITSAdministrator;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstance;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.iw.tools.RowColorJTable;
import com.iw.tools.DefaultSortTableModel;
import org.jfree.data.DefaultPieDataset;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;

public class ExplainDisplayPaneLayoutManual extends OculusBox {
    protected JLabel jlNodeDocAndDescribe;
    protected JButton jbCancel;

    public ExplainDisplayPaneLayoutManual(ITSAdministrator itsAdminFrame,
                                          ExplainDisplayDataContainer explainDisplayDataContainer) throws Exception {
        initComponents(itsAdminFrame, explainDisplayDataContainer);
    }

    private void initComponents(ITSAdministrator itsAdminFrame,
                                ExplainDisplayDataContainer explainDisplayDataContainer) throws Exception {
        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        jbCancel = new JButton("Close");
        jbCancel.setDisplayedMnemonicIndex(0);
        jbCancel.setToolTipText("Exit this explain panel");

        layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
        {
            layout.nestGrid(1, 2);
            {
                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                {
                    Enumeration enumExplainDisplayDataInstances = explainDisplayDataContainer.enumExplainDisplayDataInstances();

                    JTabbedPane jtabbedPaneSCrollable = new JTabbedPane();
                    jtabbedPaneSCrollable.show();
                    jtabbedPaneSCrollable.setVisible(true);
                    layout.add(jtabbedPaneSCrollable);

                    while (enumExplainDisplayDataInstances.hasMoreElements()) {


                        JPanel jpanelScrollable = new JPanel();

                        BoxLayout boxlay = new BoxLayout(jpanelScrollable, BoxLayout.Y_AXIS);
                        jpanelScrollable.setLayout(boxlay);
                        jpanelScrollable.show();
                        jpanelScrollable.setVisible(true);
                        //layout.add(new JScrollPane(jpanelScrollable ));
                        //??? jpanelScrollable.add(new JScrollPane(tabbedPane));

                        String sNodeTitleInfo =
                                "Topic [" +
                                explainDisplayDataContainer.getNodeTitle() + "]";
                        String sDocTitleInfo =
                                "Document [" +
                                explainDisplayDataContainer.getDocTitle() + "]";
                        //Font font = UIManager.getFont ("Label.font");
                        //UIManager.put ("Label.font", new Font (font.getName (),
                        //                    font.getStyle (), 12));
                        //jTextFieldNodeAndDocTitleInfo.setFont();
                        JLabel jlTopicInfo = new JLabel(sNodeTitleInfo);
                        jlTopicInfo.setFont(java.awt.Font.decode("MS Sans Serif-BOLD-11"));
                        jpanelScrollable.add(jlTopicInfo);

                        JLabel jlDocInfo = new JLabel(sDocTitleInfo);
                        jlDocInfo.setFont(java.awt.Font.decode("MS Sans Serif-BOLD-11"));
                        jpanelScrollable.add(jlDocInfo);

                        jpanelScrollable.add(new JLabel());

                        ExplainDisplayDataInstance explainDisplayDataInstance =
                                (ExplainDisplayDataInstance) enumExplainDisplayDataInstances.nextElement();

                        if (explainDisplayDataInstance instanceof ExplainDisplayDataInstanceTABLE &&
                                explainDisplayDataInstance.getRenderAs().equals(
                                        ExplainDisplayDataContainer.sExplainDataRenderType_TABLE)) {

                            ExplainDisplayDataInstanceTABLE eddiTABLE = (ExplainDisplayDataInstanceTABLE) explainDisplayDataInstance;
                            //System.out.println("processing table with eddiTABLE.getDescription() [" + eddiTABLE.getDescription() + "]" ); //JScrollPane jScrollPane = new JScrollPane ();
                            //jpanelScrollable.add(new JLabel( eddiTABLE.getDescription() ));
                            JLabel jlTableDesc = new JLabel(eddiTABLE.getDescription());
                            jlTableDesc.setFont(java.awt.Font.decode("MS Sans Serif-BOLD-11"));
                            jpanelScrollable.add(jlTableDesc);
                            //JTable jTableExplainPage= new JTable();
                            Vector vRowColorRule = eddiTABLE.getRowColoringRule();
                            RowColorJTable jTableExplainPage = null;
                            //System.out.println("vRowColorRule == null [" + (vRowColorRule == null)+ "]" );
                            if (vRowColorRule == null) {
                                jTableExplainPage = new RowColorJTable(
                                        -1, -1, null);
                            } else {
                                jTableExplainPage = new RowColorJTable(
                                        ((Integer) vRowColorRule.elementAt(0)).intValue(),
                                        ((Integer) vRowColorRule.elementAt(1)).intValue(),
                                        (Object) vRowColorRule.elementAt(2));
                            }


                            jTableExplainPage.setToolTipText(eddiTABLE.getTooltip());

                            TableModelExplainNodeDocScore tmENDS = new TableModelExplainNodeDocScore(eddiTABLE.getsArrColNames(), eddiTABLE.getoArrArrData());


                            DefaultSortTableModel dstm = new DefaultSortTableModel(
                                    tmENDS.getData(), tmENDS.getColumnNames());
                            dstm.setEditable(false);
                            jTableExplainPage.setModel(dstm);

                            jTableExplainPage.setEnabled(false);

                            jTableExplainPage.show();
                            jTableExplainPage.setVisible(true);
                            jpanelScrollable.add(new JScrollPane(jTableExplainPage));
                            jtabbedPaneSCrollable.addTab(eddiTABLE.getTabName(), new JScrollPane(jpanelScrollable));

                        } else if (explainDisplayDataInstance instanceof ExplainDisplayDataInstanceTABLE &&
                                explainDisplayDataInstance.getRenderAs().equals(
                                        ExplainDisplayDataContainer.sExplainDataRenderType_PIECHART)) {
                            DefaultPieDataset pieDataset = new DefaultPieDataset();
                            ExplainDisplayDataInstanceTABLE eddiTABLE = (ExplainDisplayDataInstanceTABLE) explainDisplayDataInstance;

                            // 2 cols in a pie chart driven by a table
                            Object[][] oArrArrData = eddiTABLE.getoArrArrData();
                            for (int i = 0; i < oArrArrData.length; i++) {
                                pieDataset.setValue((String) oArrArrData[i][0], (Double) oArrArrData[i][1]);

                            }
                            JFreeChart chart = ChartFactory.createPieChart
                                    (eddiTABLE.getDescription(),
                                            pieDataset, // Dataset
                                            true, // Show legend
                                            true, // ??
                                            true                  // ??
                                    );

                            ChartPanel cp = new ChartPanel(chart);
                            cp.setName("Frequency Chart");
                            //jpanelScrollable.add(new JLabel(eddiTABLE.getDescription()));
                            jpanelScrollable.add(cp);
                            jtabbedPaneSCrollable.addTab(eddiTABLE.getTabName(), jpanelScrollable);
                        } else {
                            throw new Exception("unknown explain data instance type - can't render [" +
                                    explainDisplayDataInstance.getClass().getName() + "]");
                        }
                    } // while for each explain display

                    jtabbedPaneSCrollable.show();
                    jtabbedPaneSCrollable.setVisible(true);

                    layout.parent();

                }
            }
            layout.add(jbCancel);
            layout.parent();
        }
        layout.parent();
    }

/*

    public Object[][] convertMakeOArrArrComparable(Object[][] oArrArrData)
    {
        Object oArrArrRet = new Object[oArrArrData.length][oArrArrData[0].length];
        for ( int iRow = 0; iRow < oArrArrData.length; iRow++)
        {
            for ( int jCol = 0; jCol < oArrArrData.length; jCol++)
            {
                Object o = oArrArrData[iRow][jCol];
                if ( o instanceof java.util.String )
                {

                }
            }
        }

    }

*/

}
