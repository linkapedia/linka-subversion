/*
    DocumentChart.java

    This class generates a series of charts given a vector of documents.  Charts are generated based upon
    a time series, where the minimum and maximum dates in the range are specified by the user.   Each genre
    is generated as a separate series.
*/
package com.iw.ui;

import org.jfree.chart.*;
import org.jfree.chart.renderer.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.data.*;
import org.jfree.data.time.*;
import org.jfree.util.SortOrder;
import org.jfree.ui.Spacer;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.text.NumberFormat;

import com.iw.system.*;
import com.oculustech.layout.*;

public class DocumentChart extends JInternalFrame implements ActionListener, FocusListener {
    // document chart attribute
    protected ITSAdministrator admin;
    protected Component glass;
    protected Vector documents;
    protected Vector genreData;

    // document tool widgets
    protected JLabel fromLabel;
    protected JTextField fromText;
    protected JLabel toLabel;
    protected JTextField toText;
    protected JLabel timeLabel;
    protected JComboBox timeList;
    protected JLabel genreLabel;
    protected JComboBox genreList;
    protected JComboBox chartList;

    // two panels in a split pane
    JPanel toolPanel;
    ChartPanel chartPanel;

    public DocumentChart(ITSAdministrator admin, Vector vDocuments) {
        super("Document Chart", true, true, true, true);
        this.admin = admin; this.documents = vDocuments;
        setFrameIcon(admin.iIndraweb);

        createPanels();
    }

    // create both panels and set up a glass pane for hourglass operations
    public void createPanels() {
        glass = getGlassPane();

        toolPanel = createToolPanel();
        chartPanel = createChartPanel();

        JSplitPane splitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT,
                toolPanel, new JScrollPane(chartPanel));

        splitPane.setDividerLocation(200);
        splitPane.setOneTouchExpandable(false);

        getContentPane().add(splitPane, BorderLayout.CENTER);
    }

    // the tool panel contains the widgets for manipulating the chart
    public JPanel createToolPanel() {
        JPanel toolPanel = new JPanel();

        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        fromLabel = new JLabel("From:");
        fromLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        fromLabel.setToolTipText("Alter the start date for this range of documents.");

        fromText = new JTextField("");
        fromText.setToolTipText("Alter the start date for this range of documents.");

        toLabel = new JLabel("To:");
        toLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        toLabel.setHorizontalTextPosition(SwingConstants.RIGHT);
        toLabel.setToolTipText("Alter the end date for this range of documents.");

        toText = new JTextField("");
        toText.setToolTipText("Alter the end date for this range of documents.");

        timeLabel = new JLabel("Interval:");
        timeLabel.setToolTipText("Select a time interval to partition this chart.");

        // currently only monthly and weekly date ranges are supported
        Vector timeData = new Vector();
        timeData.add("Monthly"); timeData.add("Weekly");
        timeList = new JComboBox(timeData);
        timeList.addActionListener(this);
        timeList.setToolTipText("Select a time interval to partition this chart.");

        genreLabel = new JLabel("Genre:");
        genreLabel.setToolTipText("Restrict this chart to a single genre type.");

        // genreData is persistent, it is the vector of current genres and will be accessed again later
        genreData = new Vector();
        try { genreData = admin.its.getFolders(); }
        catch (Exception e) { e.printStackTrace(System.out); }
        genreData.add(0, "ALL");

        genreList = new JComboBox(genreData);
        genreList.setToolTipText("Restrict this chart to a single genre type.");
        genreList.setSelectedIndex(0);
        genreList.addActionListener(this);

        Vector chartData = new Vector();
        chartData.add("Stacked Area Chart");
        chartData.add("Bar Chart");
        chartData.add("Line Chart");
        chartData.add("Scatter Chart");
        chartList = new JComboBox(chartData);
        chartList.setToolTipText("Select the appropriate chart.");
        chartList.addActionListener(this);

        // set initial time frame and intervals
        Calendar c = getMinDate();
        fromText.setText((c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.DAY_OF_MONTH)+"/"+c.get(Calendar.YEAR));

        c = getMaxDate();

        toText.setText((1+c.get(Calendar.MONTH))+"/"+c.get(Calendar.DAY_OF_MONTH)+"/"+c.get(Calendar.YEAR));

        fromText.addFocusListener(this);
        toText.addFocusListener(this);

        layout.nestGrid(2,6);
        {
            layout.add(fromLabel);
            layout.add(fromText,OculusLayout.STRETCH_ONLY_TO_ALIGN,OculusLayout.NO_STRETCH);
            layout.add(toLabel);
            layout.add(toText,OculusLayout.STRETCH_ONLY_TO_ALIGN,OculusLayout.NO_STRETCH);
            layout.add(timeLabel);
            layout.add(timeList);
            layout.addSpace(10);
            layout.addSpace(10);
            layout.add(genreLabel);
            layout.add(genreList);
            layout.add(new JLabel("Chart:"));
            layout.add(chartList);
            layout.parent();
        }

        toolPanel.add(ob);

        return toolPanel;
    }

    // initialize the chart panel - the chart panel must take a chart by default, so we provide an empty one
    public ChartPanel createChartPanel() {
        return new ChartPanel(ChartFactory.createAreaChart("", "", "", new DefaultCategoryDataset(), PlotOrientation.VERTICAL, false, false, false));
    }

    // this boolean function returns true if the user has selected a monthly display range in the tools panel
    public boolean isMonthly() {
        if (!timeList.getSelectedItem().toString().equals("Monthly")) { return false; }
        return true;
    }

    // fill a three dimensional array where dimension 1 is the date interval (monthly or weekly), dimension 2
    //    is the year (normalized to 0), and dimension 3 is the genre/folder.   only items within the
    //    cFrom <--> cTo range will be considered.
    public int[][][] fillDataArray(Calendar cTo, Calendar cFrom, Vector vGenres) {

        int[][][] dataValues = new int[53][(cTo.get(Calendar.YEAR)-cFrom.get(Calendar.YEAR))+1][vGenres.size()];

        // loop through each document in our document vector
        for (int i = 0; i < documents.size(); i++) {
            Document d = (Document) documents.elementAt(i);

            // date format: 2003-11-12 09:08:37.0
            // Format DateLastFound into a Calendar object
            Calendar c = null;

            try {
                c = getDateLastFound(d);

                if (c.after(cFrom) && c.before(cTo)) {
                    int dateInterval = c.get(Calendar.WEEK_OF_YEAR);
                    if (isMonthly()) dateInterval = c.get(Calendar.MONTH);
                    int year = c.get(Calendar.YEAR) - cFrom.get(Calendar.YEAR);

                    dataValues[dateInterval][year][getIndex(vGenres, d.get("GENREID"))]++;
                }
            } catch (Exception e) { } // this error is acceptable
        }

        return dataValues;
    }

    private Calendar getDateLastFound(Document d) throws Exception {
        Calendar c = Calendar.getInstance();

        try {
            String sLastCapture = d.get("DATELASTFOUND").substring(0, 19);

            c.set(new Integer(sLastCapture.substring(0,4)).intValue(), // Year
                new Integer(sLastCapture.substring(5,7)).intValue()-1, // Month
                new Integer(sLastCapture.substring(8,10)).intValue(), // Day
                new Integer(sLastCapture.substring(11,13)).intValue(), // Hour
                new Integer(sLastCapture.substring(14,16)).intValue(), // Minutes
                new Integer(sLastCapture.substring(17,19)).intValue()); // Seconds
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("warning: document "+d.get("DOCTITLE")+" does not have a valid date last found.");
            throw e;
        }

        return c;
    }

    public Calendar getMinDate() {
        Calendar cMinDate = Calendar.getInstance();

        int startingPoint = 0;

        // loop through each document in our document vector
        for (int i = 0; i < documents.size(); i++) {
            Document d = (Document) documents.elementAt(i);

            // date format: 2003-11-12 09:08:37.0
            // Format DateLastFound into a Calendar object
            Calendar c = null;

            try {
                c = getDateLastFound(d);

                if (i == startingPoint) cMinDate = c;
                if (c.before(cMinDate)) cMinDate = c;

            } catch (Exception e) { startingPoint++; }
        }

        return cMinDate;
    }

    public Calendar getMaxDate() {
        Calendar cMaxDate = Calendar.getInstance();

        int startingPoint = 0;

        // loop through each document in our document vector
        for (int i = 0; i < documents.size(); i++) {
            Document d = (Document) documents.elementAt(i);

            Calendar c = null;

            try {
                c = getDateLastFound(d);

                if (i == startingPoint) cMaxDate = c;
                if (c.after(cMaxDate)) cMaxDate = c;

                //System.out.println("max date: "+cMaxDate+" vs "+d.get("DATELASTFOUND"));

            } catch (Exception e) {
                startingPoint++;
            }
        }

        return cMaxDate;
    }

    // ***************************************************************************************
    // FUNCTIONS USED TO CREATE DATASETS FROM VECTOR OF DOCUMENTS
    // ***************************************************************************************

    // the XY dataset object is used for area charts
    public DefaultTableXYDataset createXYDataset() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
        Date fromDate = new Date();
        Date toDate = new Date();

        try {
            fromDate = sdf.parse(fromText.getText());
            toDate = sdf.parse(toText.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(admin, "Incorrect date to/from syntax specified.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        long interval = toDate.getTime() - fromDate.getTime();
        if (interval < 0) {
            JOptionPane.showMessageDialog(admin, "FROM date must be less than TO date.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        Calendar cFrom = Calendar.getInstance();
        Calendar cTo = Calendar.getInstance();
        cFrom.setTime(fromDate);
        cTo.setTime(toDate);

        if (cFrom.get(Calendar.YEAR) < 1975) {
            JOptionPane.showMessageDialog(admin, "FROM date must be 1975 or later.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        // get only the list of genres which apply to our data set
        Vector vGenres = getGenres(cFrom, cTo);

        int[][][] dataValues = fillDataArray(cTo, cFrom, vGenres);

        // if the selected genre is not ALL, restrict results to the selected genre
        Genre selectedGenre = null;
        if (genreList.getSelectedItem() instanceof Genre) {
            selectedGenre = (Genre) genreList.getSelectedItem();
        }

        // initialize the time series array
        XYSeries[] timeSeries = new XYSeries[vGenres.size()];
        for (int k = 0; k < vGenres.size(); k++) {
            Genre g = (Genre) vGenres.elementAt(k);
            XYSeries ts = new XYSeries(g.getName(), false);

            timeSeries[k] = ts;
        }

        // loop through each date interval and set a plot, even if it is 0.
        int dateIntervalLow = cFrom.get(Calendar.WEEK_OF_YEAR);
        int dateIntervalHigh = cTo.get(Calendar.WEEK_OF_YEAR);
        int maxInterval = 51;
        if (isMonthly()) {
            dateIntervalLow = cFrom.get(Calendar.MONTH);
            dateIntervalHigh = cTo.get(Calendar.MONTH);
            maxInterval = 11;
        }
        int year = cTo.get(Calendar.YEAR) - cFrom.get(Calendar.YEAR);

        // for each year, for each month/week, for each genre, fill each series object
        for (int i = 0; i <= year; i++) {
            for (int j = 0; j <= maxInterval; j++) {
                if ((j == 0) && (i == 0)) { j = dateIntervalLow; }
                if ((i == year) && (j > dateIntervalHigh)) { break; }

                Calendar c = Calendar.getInstance();
                c.set(Calendar.YEAR, cFrom.get(Calendar.YEAR)+i);

                if (isMonthly()) {
                    c.set(Calendar.MONTH, j);
                    c.set(Calendar.DAY_OF_MONTH, 1);
                }
                else { c.set(Calendar.WEEK_OF_YEAR, j); }

                try {
                    for (int k = 0; k < vGenres.size(); k++) {
                        Genre g = (Genre) vGenres.elementAt(k);
                        if ((selectedGenre == null) || (g.equals(selectedGenre))) {
                            timeSeries[k].add(c.getTime().getTime(), dataValues[j][i][k]);
                        }
                    }
                } catch (Exception e) { e.printStackTrace(System.out); }
            }
        }

        DefaultTableXYDataset dataset = new DefaultTableXYDataset();
        for (int k = 0; k < vGenres.size(); k++) {
            dataset.addSeries(timeSeries[k]);
        }

        return dataset;
    }

    // time series collections are used in creating our bar charts, scatter charts and line graphs
    public TimeSeriesCollection createTimeDataset() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
        Date fromDate = new Date();
        Date toDate = new Date();

        try {
            fromDate = sdf.parse(fromText.getText());
            toDate = sdf.parse(toText.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(admin, "Incorrect date to/from syntax specified.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        long interval = toDate.getTime() - fromDate.getTime();
        if (interval < 0) {
            JOptionPane.showMessageDialog(admin, "FROM date must be less than TO date.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        Calendar cFrom = Calendar.getInstance();
        Calendar cTo = Calendar.getInstance();
        cFrom.setTime(fromDate);
        cTo.setTime(toDate);

        if (cFrom.get(Calendar.YEAR) < 1975) {
            JOptionPane.showMessageDialog(admin, "FROM date must be 1975 or later.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

                // get only the list of genres which apply to our data set
        Vector vGenres = getGenres(cFrom, cTo);

        int[][][] dataValues = fillDataArray(cTo, cFrom, vGenres);

        // if the selected genre is not ALL, restrict results to the selected genre
        Genre selectedGenre = null;
        if (genreList.getSelectedItem() instanceof Genre) {
            selectedGenre = (Genre) genreList.getSelectedItem();
        }

        // initialize the time series array
        TimeSeries[] timeSeries = new TimeSeries[vGenres.size()];
        for (int k = 0; k < vGenres.size(); k++) {
            Genre g = (Genre) vGenres.elementAt(k);
            TimeSeries ts;

            if (isMonthly()) { ts = new TimeSeries(g.getName(), Month.class); }
            else { ts = new TimeSeries(g.getName(), Week.class); }

            timeSeries[k] = ts;
        }

        // loop through each date interval and set a plot, even if it is 0.
        int dateIntervalLow = cFrom.get(Calendar.WEEK_OF_YEAR);
        int dateIntervalHigh = cTo.get(Calendar.WEEK_OF_YEAR);
        int maxInterval = 51;
        if (isMonthly()) {
            dateIntervalLow = cFrom.get(Calendar.MONTH);
            dateIntervalHigh = cTo.get(Calendar.MONTH);
            maxInterval = 11;
        }
        int year = cTo.get(Calendar.YEAR) - cFrom.get(Calendar.YEAR);

        // for each year, for each interval (month or week), for each genre fill our series struct
        for (int i = 0; i <= year; i++) {
            for (int j = 0; j <= maxInterval; j++) {
                if ((j == 0) && (i == 0)) { j = dateIntervalLow; }
                if ((i == year) && (j > dateIntervalHigh)) { break; }

                Calendar c = Calendar.getInstance();
                c.set(Calendar.YEAR, cFrom.get(Calendar.YEAR)+i);

                if (isMonthly()) { c.set(Calendar.MONTH, j); }
                else { c.set(Calendar.WEEK_OF_YEAR, j); }

                try {
                    for (int k = 0; k < vGenres.size(); k++) {
                        Genre g = (Genre) vGenres.elementAt(k);
                        if ((selectedGenre == null) || (g.equals(selectedGenre))) {
                            try {
                                TimeSeriesDataItem tsdi;
                                if (isMonthly()) {
                                    tsdi = new TimeSeriesDataItem(
                                            new Month(c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR)),
                                            dataValues[j][i][k]);
                                } else {
                                    tsdi = new TimeSeriesDataItem(
                                            new Week(c.get(Calendar.WEEK_OF_YEAR), c.get(Calendar.YEAR)),
                                            dataValues[j][i][k]);
                                }

                                timeSeries[k].add(tsdi);
                            } catch (SeriesException e) {} // e.printStackTrace(System.out); }
                        }
                    }
                } catch (Exception e) { e.printStackTrace(System.out); }
            }
        }

        TimeSeriesCollection dataset = new TimeSeriesCollection();
        for (int k = 0; k < vGenres.size(); k++) {
            dataset.addSeries(timeSeries[k]);
        }

        return dataset;
    }

    // the category dataset is necessary to view three dimensional charts and stacked bar charts.  unfortunately,
    //    it is not compatible with a time series in the current version of JFreeChart (0.916)
    public CategoryDataset createCategoryDataset() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
        Date fromDate = new Date();
        Date toDate = new Date();

        try {
            fromDate = sdf.parse(fromText.getText());
            toDate = sdf.parse(toText.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(admin, "Incorrect date to/from syntax specified.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        long interval = toDate.getTime() - fromDate.getTime();
        if (interval < 0) {
            JOptionPane.showMessageDialog(admin, "FROM date must be less than TO date.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        Calendar cFrom = Calendar.getInstance();
        Calendar cTo = Calendar.getInstance();
        cFrom.setTime(fromDate);
        cTo.setTime(toDate);

        if (cFrom.get(Calendar.YEAR) < 1975) {
            JOptionPane.showMessageDialog(admin, "FROM date must be 1975 or later.", "Error", JOptionPane.NO_OPTION);
            return null;
        }

        // get only the list of genres which apply to our data set
        Vector vGenres = getGenres(cFrom, cTo);
        DefaultCategoryDataset ds = new DefaultCategoryDataset();

        int[][][] dataValues = fillDataArray(cTo, cFrom, vGenres);

        // loop through each date interval and set a plot, even if it is 0.
        int dateIntervalLow = cFrom.get(Calendar.WEEK_OF_YEAR);
        int dateIntervalHigh = cTo.get(Calendar.WEEK_OF_YEAR);
        int maxInterval = 51;
        if (isMonthly()) {
            dateIntervalLow = cFrom.get(Calendar.MONTH);
            dateIntervalHigh = cTo.get(Calendar.MONTH);
            maxInterval = 11;
        }
        int year = cTo.get(Calendar.YEAR) - cFrom.get(Calendar.YEAR);

        // if the selected genre is not ALL, restrict results to the selected genre
        Genre selectedGenre = null;
        if (genreList.getSelectedItem() instanceof Genre) {
            selectedGenre = (Genre) genreList.getSelectedItem();
        }

        for (int i = 0; i <= year; i++) {
            for (int j = 0; j <= maxInterval; j++) {
                if ((j == 0) && (i == 0)) { j = dateIntervalLow; }
                if ((i == year) && (j > dateIntervalHigh)) { break; }

                Calendar c = Calendar.getInstance();
                c.set(Calendar.YEAR, cFrom.get(Calendar.YEAR)+i);

                if (isMonthly()) {
                    c.set(Calendar.MONTH, j);
                } else {
                    c.set(Calendar.WEEK_OF_YEAR, j);
                }

                SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    for (int k = 0; k < vGenres.size(); k++) {
                        Genre g = (Genre) vGenres.elementAt(k);
                        if ((selectedGenre == null) || (g.equals(selectedGenre))) {
                            ds.addValue(dataValues[j][i][k], g.getName(),
                                new ChartDate(sdf2.parse((c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.DAY_OF_MONTH)+"/"+c.get(Calendar.YEAR)), isMonthly()));
                        }
                    }
                } catch (Exception e) { e.printStackTrace(System.out); }
            }
            //dkv.sortByValues(SortOrder.ASCENDING);
        }

        return ds;
    }

    // ***************************************************************************************
    // FUNCTIONS USED TO CREATE CHARTS GIVEN A DATASET
    // ***************************************************************************************

    // create a time series chart (line graph over time)
    public JFreeChart createTimeSeriesChart(XYDataset ds) {
        JFreeChart chart = ChartFactory.createTimeSeriesChart("Documents over Time", "Date", "Documents", ds, true, true, false);
        XYPlot xyplot = chart.getXYPlot();
        org.jfree.chart.renderer.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if(xyitemrenderer instanceof StandardXYItemRenderer)
        {
            StandardXYItemRenderer standardxyitemrenderer = (StandardXYItemRenderer) xyitemrenderer;
            standardxyitemrenderer.setPlotShapes(true);
            standardxyitemrenderer.setShapesFilled(true);
        }
        DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
        dateaxis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        xyplot.setForegroundAlpha(0.5F);
        xyplot.setAxisOffset(new Spacer(1, 5D, 5D, 5D, 5D));
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinesVisible(true);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinesVisible(true);
        xyplot.setRangeGridlinePaint(Color.white);

        NumberAxis rangeAxis = (NumberAxis) xyplot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    // create a bar chart with clustered bars to represent each series
    public JFreeChart createBarChart(IntervalXYDataset ds) {
        JFreeChart chart = ChartFactory.createXYBarChart("Documents over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);

        SimpleDateFormat simpledateformat;
        if (isMonthly()) { simpledateformat = new SimpleDateFormat("MM/yy"); }
        else { simpledateformat = new SimpleDateFormat("MM/dd/yy"); }

        //TimeSeriesToolTipGenerator timeseriestooltipgenerator = new TimeSeriesToolTipGenerator(simpledateformat, NumberFormat.getInstance());

        // then customise it a little...
        //chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white, 1000, 0, Color.blue));

        XYItemRenderer renderer = chart.getXYPlot().getRenderer();
        //renderer.setToolTipGenerator(timeseriestooltipgenerator);

        XYPlot plot = chart.getXYPlot();
        ClusteredXYBarRenderer cxybr = new ClusteredXYBarRenderer();
        //cxybr.setToolTipGenerator(new TimeSeriesToolTipGenerator());

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
        plot.setRenderer(cxybr);

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    public JFreeChart createStackedAreaXYChart(TableXYDataset ds) {
        JFreeChart chart = ChartFactory.createStackedAreaXYChart("Documents over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);
        XYPlot xyplot = chart.getXYPlot();
        org.jfree.chart.renderer.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if(xyitemrenderer instanceof StandardXYItemRenderer)
        {
            StandardXYItemRenderer standardxyitemrenderer = (StandardXYItemRenderer) xyitemrenderer;
            standardxyitemrenderer.setPlotShapes(true);
            standardxyitemrenderer.setShapesFilled(true);
        }
        try {
            DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
            dateaxis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));
        } catch (Exception e) {
            DateAxis dateaxis = new DateAxis("Date");
            dateaxis.setLowerMargin(0.0D);
            dateaxis.setUpperMargin(0.0D);
            xyplot.setDomainAxis(dateaxis);
        }

        xyplot.setForegroundAlpha(0.5F);
        xyplot.setAxisOffset(new Spacer(1, 5D, 5D, 5D, 5D));
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinesVisible(true);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinesVisible(true);
        xyplot.setRangeGridlinePaint(Color.white);

        NumberAxis rangeAxis = (NumberAxis) xyplot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    public JFreeChart createScatterChart(XYDataset ds) {
        JFreeChart chart = ChartFactory.createScatterPlot("Documents over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);
        XYPlot xyplot = chart.getXYPlot();
        org.jfree.chart.renderer.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if(xyitemrenderer instanceof StandardXYItemRenderer)
        {
            StandardXYItemRenderer standardxyitemrenderer = (StandardXYItemRenderer) xyitemrenderer;
            standardxyitemrenderer.setPlotShapes(true);
            standardxyitemrenderer.setShapesFilled(true);
        }
        try {
            DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
            dateaxis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));
        } catch (Exception e) {
            DateAxis dateaxis = new DateAxis("Date");
            dateaxis.setLowerMargin(0.0D);
            dateaxis.setUpperMargin(0.0D);
            xyplot.setDomainAxis(dateaxis);
        }

        xyplot.setForegroundAlpha(0.5F);
        xyplot.setAxisOffset(new Spacer(1, 5D, 5D, 5D, 5D));
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinesVisible(true);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinesVisible(true);
        xyplot.setRangeGridlinePaint(Color.white);

        NumberAxis rangeAxis = (NumberAxis) xyplot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    public JFreeChart createAreaChart(XYDataset ds) {
        JFreeChart chart = ChartFactory.createXYAreaChart("Documents over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);
        XYPlot xyplot = chart.getXYPlot();
        org.jfree.chart.renderer.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if(xyitemrenderer instanceof StandardXYItemRenderer)
        {
            StandardXYItemRenderer standardxyitemrenderer = (StandardXYItemRenderer) xyitemrenderer;
            standardxyitemrenderer.setPlotShapes(true);
            standardxyitemrenderer.setShapesFilled(true);
        }
        try {
            DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
            dateaxis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));
        } catch (Exception e) {
            DateAxis dateaxis = new DateAxis("Date");
            dateaxis.setLowerMargin(0.0D);
            dateaxis.setUpperMargin(0.0D);
            xyplot.setDomainAxis(dateaxis);
        }

        xyplot.setForegroundAlpha(0.5F);
        xyplot.setAxisOffset(new Spacer(1, 5D, 5D, 5D, 5D));
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinesVisible(true);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinesVisible(true);
        xyplot.setRangeGridlinePaint(Color.white);

        NumberAxis rangeAxis = (NumberAxis) xyplot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    public JFreeChart createStackedAreaChart (XYDataset ds) {

        SimpleDateFormat simpledateformat;
        if (isMonthly()) { simpledateformat = new SimpleDateFormat("MM/yy"); }
        else { simpledateformat = new SimpleDateFormat("MM/dd/yy"); }

        //TimeSeriesToolTipGenerator timeseriestooltipgenerator = new TimeSeriesToolTipGenerator(simpledateformat, NumberFormat.getInstance());
        DateAxis dateaxis = new DateAxis("Date");
        dateaxis.setLowerMargin(0.0D);
        dateaxis.setUpperMargin(0.0D);
        NumberAxis numberaxis = new NumberAxis("Documents");
        numberaxis.setAutoRangeIncludesZero(false);

        StackedXYAreaRenderer stackedxyarearenderer = new StackedXYAreaRenderer(5);
        stackedxyarearenderer.setOutline(true);
        stackedxyarearenderer.setBaseItemLabelsVisible(new Boolean(true));
        stackedxyarearenderer.setItemLabelsVisible(true);

        stackedxyarearenderer.setShapePaint(Color.gray);
        stackedxyarearenderer.setShapeStroke(new BasicStroke(0.5F));
        stackedxyarearenderer.setShape(new java.awt.geom.Ellipse2D.Double(-3D, -3D, 6D, 6D));
        dateaxis.setTickLabelsVisible(true);
        XYPlot xyplot = new XYPlot(ds, dateaxis, numberaxis, stackedxyarearenderer);
        xyplot.setForegroundAlpha(0.5F);
        xyplot.setAxisOffset(new Spacer(1, 5D, 5D, 5D, 5D));
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinesVisible(true);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinesVisible(true);
        xyplot.setRangeGridlinePaint(Color.white);
        numberaxis.configure();
        JFreeChart chart = new JFreeChart(null, JFreeChartConstants.DEFAULT_TITLE_FONT, xyplot, true);

        /*
        NumberAxis rangeAxis = (NumberAxis) xyplot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());
        */

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    // used for categories
    public JFreeChart create3DBarChart(CategoryDataset ds) {
        JFreeChart chart = ChartFactory.createStackedBarChart3D("Documents over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot categoryPlot = chart.getCategoryPlot();
        CategoryAxis ca = categoryPlot.getDomainAxis();
        ValueAxis va = categoryPlot.getRangeAxis();
        va.setFixedAutoRange(1);
        ca.setTickMarksVisible(true);
        ca.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(0.39269908169872414D));
        ca.setLowerMargin(0.0D);
        ca.setUpperMargin(0.0D);

        if (ds.getRowCount() > 24) ca.setVisible(false);

        categoryPlot.setForegroundAlpha(0.5F);
        categoryPlot.setAxisOffset(new Spacer(1, 5D, 5D, 5D, 5D));
        categoryPlot.setBackgroundPaint(Color.lightGray);
        categoryPlot.setDomainGridlinesVisible(true);
        categoryPlot.setDomainGridlinePaint(Color.white);
        categoryPlot.setRangeGridlinesVisible(true);
        categoryPlot.setRangeGridlinePaint(Color.white);

        NumberAxis rangeAxis = (NumberAxis) categoryPlot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    public JFreeChart createStackedAreaChart(CategoryDataset ds) {
        JFreeChart chart = ChartFactory.createStackedAreaChart("Documents Over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot categoryPlot = chart.getCategoryPlot();
        CategoryAxis ca = categoryPlot.getDomainAxis();
        ValueAxis va = categoryPlot.getRangeAxis();
        va.setFixedAutoRange(1);

        ca.setTickMarksVisible(true);
        ca.setCategoryLabelPositionOffset(10);
        ca.setVisible(false);

        CategoryAxis hca = categoryPlot.getDomainAxis();

        NumberAxis rangeAxis = (NumberAxis) categoryPlot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    public JFreeChart createLineChart(CategoryDataset ds) {
        JFreeChart chart = ChartFactory.createLineChart("Documents Over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot categoryPlot = chart.getCategoryPlot();
        CategoryAxis ca = categoryPlot.getDomainAxis();
        ValueAxis va = categoryPlot.getRangeAxis();
        va.setFixedAutoRange(1);

        ca.setTickMarksVisible(true);
        ca.setCategoryLabelPositionOffset(10);
        ca.setVisible(false);

        CategoryAxis hca = categoryPlot.getDomainAxis();

        NumberAxis rangeAxis = (NumberAxis) categoryPlot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        return chart;
    }

    public JFreeChart createWaterfallChart(CategoryDataset ds) {
        JFreeChart chart = ChartFactory.createWaterfallChart("Documents Over Time", "Date", "Documents", ds, PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot categoryPlot = chart.getCategoryPlot();
        CategoryAxis ca = categoryPlot.getDomainAxis();
        ValueAxis va = categoryPlot.getRangeAxis();
        va.setFixedAutoRange(1);

        ca.setTickMarksVisible(true);
        ca.setCategoryLabelPositionOffset(10);
        ca.setVisible(false);

        CategoryAxis hca = categoryPlot.getDomainAxis();

        NumberAxis rangeAxis = (NumberAxis) categoryPlot.getRangeAxis();
        rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

        Legend legend = chart.getLegend();
        legend.setAnchor(Legend.EAST);

        categoryPlot.setBackgroundPaint(Color.lightGray);
        categoryPlot.setRangeGridlinePaint(Color.white);
        categoryPlot.setRangeGridlinesVisible(true);
        categoryPlot.setAxisOffset(new Spacer(1, 5D, 5D, 5D, 5D));

        BarRenderer barrenderer = (BarRenderer) categoryPlot.getRenderer();
        barrenderer.setDrawBarOutline(false);
        barrenderer.setItemLabelsVisible(true);

        return chart;
    }

    // get a date in the past given a multiplier
    public Calendar getPastDate(int field, int multiplier) {
        Calendar cNow = Calendar.getInstance();
        cNow.setTime(new Date());

        cNow.add(field, multiplier);

        return cNow;
    }

    // rebuild the category chart (currently not used)
    public void rebuildCategoryChart() {
        CategoryDataset ds = createCategoryDataset();

        //JFreeChart chart = create3DBarChart(ds);
        JFreeChart chart = null;
        switch (chartList.getSelectedIndex()) {
            case 0: chart = create3DBarChart(ds); break;
            case 1: chart = createStackedAreaChart(ds); break;
            case 2: chart = createLineChart(ds); break;
            case 3: chart = createWaterfallChart(ds); break;
        }
        chartPanel.setChart(chart);
    }

    // rebuild the chart from current data in the toolbar.   this function is called on each toolbar item change
    public void rebuildChart() {
        XYDataset ds;

        //JFreeChart chart = create3DBarChart(ds);
        JFreeChart chart = null;
        switch (chartList.getSelectedIndex()) {
            case 0: ds = createXYDataset(); chart = createStackedAreaXYChart((TableXYDataset) ds); break;
            case 1: ds = createTimeDataset(); chart = createBarChart((IntervalXYDataset) ds); break;
            case 2: ds = createTimeDataset(); chart = createTimeSeriesChart(ds); break;
            case 3: ds = createTimeDataset(); chart = createScatterChart(ds); break;
        }
        chartPanel.setChart(chart);
    }

    public int getIndex (Vector vGenres, String genreID) {
        for (int i = 0; i < vGenres.size(); i++) {
            Genre g = (Genre) vGenres.elementAt(i);

            if (g.getID().equals(genreID)) { return i; }
        }
        return -1;
    }

    // return a vector of genre objects that contain documents
    public Vector getGenres(Calendar cFrom, Calendar cTo) {
        Vector v = new Vector();
        Hashtable ht = new Hashtable();

        for (int i = 0; i < documents.size(); i++) {
            Document d = (Document) documents.elementAt(i);

            // date format: 2003-11-12 09:08:37.0
            // Format DateLastFound into a Calendar object
            Calendar c = null;

            try {
                c = getDateLastFound(d);

                if (c.after(cFrom) && c.before(cTo)) {
                    if (!ht.containsKey(d.get("GENREID"))) {
                        ht.put(d.get("GENREID"), "1");
                        for (int j = 1; j < genreData.size(); j++) {
                            Genre g = (Genre) genreData.elementAt(j);
                            if (g.getID().equals(d.get("GENREID"))) { v.add(g); }
                        }
                    }
                }

            } catch (Exception e) {}
        }

        return v;
    }

    // ACTION ITEMS: on each value change, rebuild the chart
    public void actionPerformed(ActionEvent e) {
        rebuildChart();
    }
    public void focusLost(FocusEvent e) {
        rebuildChart();
    }
    public void focusGained(FocusEvent e) {}

    // chart date is used to display the date correctly in a categorydataset built chart
    public class ChartDate extends Date {

		private static final long serialVersionUID = 3590725314664312751L;
		private boolean monthly = true;

        public ChartDate() { super(); }
        public ChartDate(Date d) {
            super();
            setTime(d.getTime());
        }
        public ChartDate(Date d, boolean b) {
            super();
            setTime(d.getTime());
            monthly = b;
        }

        public Calendar getCalendar() {
            Calendar c = Calendar.getInstance();
            c.setTime(this);

            return c;
        }
        public String toString() {
            SimpleDateFormat format;
            if (monthly) format = new SimpleDateFormat("MM-yy");
            else format = new SimpleDateFormat("MM-dd-yy");

            return format.format(this);
        }
        public int compareTo(ChartDate o) {
            Calendar c = getCalendar();

            if (c.after(o)) return 1;
            return -1;
        }

    }
}
