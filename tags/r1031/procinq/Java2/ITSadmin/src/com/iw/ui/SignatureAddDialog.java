/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Nov 18, 2003
 * Time: 3:01:49 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.oculustech.layout.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SignatureAddDialog extends JPanel implements ActionListener {
    protected JTextField textTerm;
    protected JTextField textWeight;
    protected JButton buttonOK;
    protected JButton buttonCancel;

    private JInternalFrame MDIframe;

    public SignatureAddDialog(ITSAdministrator frame) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        MDIframe = new JInternalFrame("Add Signature Term", false, true, false, false);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(225, 110);
        MDIframe.setBackground(Color.lightGray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        textTerm = new JTextField(); textTerm.setMinimumSize(new Dimension(250,textTerm.getMinimumSize().height));
        textWeight = new JTextField(); textWeight.setMaximumSize(new Dimension(50,textTerm.getMinimumSize().height));

        buttonOK = new JButton("OK");

        buttonCancel = new JButton("Cancel");
        buttonCancel.setHorizontalAlignment(SwingConstants.RIGHT);
        buttonCancel.setHorizontalTextPosition(SwingConstants.RIGHT);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                            {
                                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(new JLabel("Please enter a signature term and weight."));
                                        layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                        {
                                            layout.add(textTerm);
                                            layout.add(textWeight);
                                            layout.parent();
                                        }
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                        {
                            layout.addSpace(85);
                            layout.nestBox(OculusLayout.HORIZONTAL);
                            {
                                layout.add(buttonOK,OculusLayout.NO_STRETCH,OculusLayout.NO_STRETCH);
                                layout.add(buttonCancel,OculusLayout.NO_STRETCH,OculusLayout.NO_STRETCH);
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.parent();
        }

        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == buttonCancel) { MDIframe.dispose(); } // cancel: close the window
        if (event.getSource() == buttonOK) {

        }
    }
}
