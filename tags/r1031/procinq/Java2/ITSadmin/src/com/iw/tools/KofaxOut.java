package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class KofaxOut extends ExportElement {
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable filtercommonWords = new Hashtable();
    public String corpusID = null;
    public String corpusName = null;
    public String wordName = "terms";

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));
        corpusID = c.getID();
        corpusName = returnLegalFileName(c.getName()).trim();

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        DataInputStream disfilter = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\filtercommonwords.txt"))));
        

        String filterrecord = null;
        try {
            while ( (filterrecord=disfilter.readLine()) != null ) { filtercommonWords.put(filterrecord.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file filterwords.txt");
        }

        
        
        
        
        
        
        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { System.err.println("No root node found for corpus "+corpusID); return null; }

        File f = new File(c.getName().replaceAll(" ", "_")+".kfx");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");


        buildChildren(server, rootNode, out, "root");

        out.close();

        return f;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer out, String CurrentPath) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() < 1) return;
        
        CurrentPath = CurrentPath + "/" + returnLegalFileName(p.get("NODEID"));
        out.write("\"" + p.get("NODEID") + "\", \"" + p.get("NODETITLE")+ "\"\n");
        
        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);
            String fileName = "";
            //if (vSignatures.size() > 0) fileName = writeSample(n,p, vSignatures, CurrentPath);
            fileName = writeSample(n,p, vSignatures, CurrentPath);

                       
            if (n.get("NODEID").equals(n.get("LINKNODEID"))) buildChildren(server, n, out, CurrentPath);

            
            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }
        }
    }

    private String writeSample(ITSTreeNode n, ITSTreeNode p, Vector signatures, String CurrentPath) throws Exception {
        //if (signatures.size() == 0) throw new Exception("This topic has no signatures.");

        try {
            File f = createOrGetFile("kofax");
            f = createOrGetFile("kofax/"+corpusName);
            f = createOrGetFile("kofax/"+corpusName + "/root");
            f = createOrGetFile("kofax/"+corpusName+"/"+CurrentPath.trim());
            f = createOrGetFile("kofax/"+corpusName+"/"+CurrentPath.trim()+ "/" + returnLegalFileName(n.get("NODEID").trim()));

            f = new File("kofax/"+corpusName+"/"+CurrentPath.trim()+ "/" + returnLegalFileName(n.get("NODEID").trim()) +"/sample.htm");
            if (f.exists()) f.delete();

            // write header
            FileOutputStream fos = new FileOutputStream(f);
            Writer out = new OutputStreamWriter(fos, "UTF8");

            out.write("<HTML>\r\n<HEAD>\r\n<TITLE>"+n.get("NODETITLE")+"</TITLE>\r\n</HEAD>\r\n<BODY>\r\n");

            // calculate the filler frequency
            double totalwordfreq = 0;
            double maxweight = 0;
            if (signatures.size() > 0) { 
            for (int i = 0; i < signatures.size(); i++) {
                Signature s = (Signature) signatures.elementAt(i);
                totalwordfreq = totalwordfreq + s.getWeight();
                if (maxweight < s.getWeight()) maxweight=s.getWeight();
               
            }
            }
            if (maxweight==0) maxweight = 10;

            double nodesize = Double.parseDouble(n.get("NODESIZE"));
            double fillweight = nodesize - totalwordfreq;
            if (fillweight < 0) fillweight = 0;

            // make sure we have node title and parent title in here.
            
            signatures.add(0, new Signature("fillerword", fillweight));
            signatures.add(0, new Signature(n.get("NODETITLE"), maxweight));
            if (n.get("PARENTID").equalsIgnoreCase("-1")){
            	
            }
            else {signatures.add(0, new Signature(p.get("NODETITLE"), maxweight));
            }
            

            double outer = 0; double total = 0;

            //System.out.println("**DEBUG** node: "+n.get("NODETITLE")+" file: "+f.getAbsolutePath()+" nodesize: "+nodesize+" fillweight: "+
            //        fillweight+" totalwordfreq: "+totalwordfreq+" signatures: "+signatures.size());

            // loop through the signatures and write them
            while (total < nodesize) {
                outer++;
                for (int i = 0; i < signatures.size(); i++) {
                    Signature s = (Signature) signatures.elementAt(i);
                    //System.out.println("**DEBUG** signature: "+s.getWord()+" weight: "+s.getWeight()+" outer: "+outer);
                    if (s.getWeight() >= outer) { out.write(s.getWord()+" "); total++; }
                }

                out.write("\r\n");

                if (total < nodesize) {
                    outer++;
                    for (int i = signatures.size()-1; i > -1; i--) {
                        Signature s = (Signature) signatures.elementAt(i);
                        if (s.getWeight() >= outer) { out.write(s.getWord()+" "); total++; }
                    }
                    out.write("\r\n");
                }
            }

            // write footer
            out.write("</BODY>\r\n</HTML>\r\n");
            out.close();

            return "sample/"+corpusID+"/"+n.get("NODEID")+"/sample.htm";
        } catch (Exception e) { e.printStackTrace(System.err); throw e; }
    }

    private File createOrGetFile(String path) throws Exception {
        File f = new File(path);
        if (!f.exists()) f.mkdir();

        return f;
    }


    private String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method

    private String returnWords(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");
        boolean FirstFlag = true;
        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!commonWords.containsKey(sArr[i].toLowerCase())) {
            	if (sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )==-1){
            		if (!FirstFlag) sb.append(" | ");
            		sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
            		FirstFlag = false;
            }
            } 	
        }

        return sb.toString();
    }
    
    private String returntermswithtitle(String Phrase, String Title) {
        StringBuffer sb = new StringBuffer("");
        sb.append(Phrase.toString());
        
        
        Title = Title.replaceAll(",", "");

        String[] sArr = Title.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!commonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.toString().toLowerCase().indexOf("nocase(\""+replaceWildcards(sArr[i].toLowerCase())+"\")" )==-1){
                		sb.append(" | ");
                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
                        
                       	}
            }
        }
            
        return getEncodedName(sb.toString());
    }
       
    
    private String returnWordsAmpersand(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");
        
        boolean FirstFlag = true;
        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!filtercommonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )==-1){
                		if (!FirstFlag) sb.append(" &amp; ");
                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
                        if (FirstFlag)FirstFlag = false;
                       	}
            }
        }
            
        return getEncodedName(sb.toString());
    }
    
    
    public static String getEncodedName(String s) {
        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        // s = s.replaceAll("\"", "&quot;");
        // inxight doesnt want to have these encoded
        s = s.replaceAll(">", "&gt;");
        s = s.replaceAll("'", "&apos;");

        return s;
    }
    
    

    private String returnWordsAnd(String Phrase) {
        StringBuffer sb = new StringBuffer("");

        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (i > 0) sb.append(" | ");
            if (!commonWords.containsKey(sArr[i].toLowerCase())) sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
        }

        return sb.toString();
    }
    
    private String returnLegalFileName(String OrigFileName) {
    	
        OrigFileName = OrigFileName.replaceAll("/","");
        OrigFileName = OrigFileName.replaceAll(":","");
        OrigFileName = OrigFileName.replaceAll(">","");
        OrigFileName = OrigFileName.replaceAll("<","");
        OrigFileName = OrigFileName.replaceAll("[?]","");
        OrigFileName = OrigFileName.replaceAll("\\]","");
        OrigFileName = OrigFileName.replaceAll("\\[","");
        OrigFileName = OrigFileName.replaceAll("|","");
        OrigFileName = OrigFileName.replaceAll("@","");
        OrigFileName = OrigFileName.replaceAll("\"","");
        OrigFileName = OrigFileName.replaceAll("[)]","");
        OrigFileName = OrigFileName.replaceAll("[(]","");
        OrigFileName = OrigFileName.replaceAll("[.]","");
        OrigFileName = OrigFileName.trim();
        
        return OrigFileName;
        
        
      
    }
    
}