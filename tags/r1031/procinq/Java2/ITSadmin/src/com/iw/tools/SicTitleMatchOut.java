package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class SicTitleMatchOut extends ExportElement {
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable filtercommonWords = new Hashtable();
    public String corpusID = null;
    public String wordName = "terms";

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));
        corpusID = c.getID();

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        DataInputStream disfilter = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\filtercommonwords.txt"))));
        

        String filterrecord = null;
        try {
            while ( (filterrecord=disfilter.readLine()) != null ) { filtercommonWords.put(filterrecord.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file filterwords.txt");
        }

        
        
        
        
        
        
        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { System.err.println("No root node found for corpus "+corpusID); return null; }

        File f = new File(c.getName().replaceAll(" ", "_")+"-titles.txt");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        //out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        //out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
        //          " Corpus "+c.getID()+".  All rights reserved. -->\n");
        //out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        //out.write("<inxight-taxonomy xmlns=\"http://www.inxight.com/taxonomy/4.0\" xmlns:st=\"http://www.inxight.com/taxonomy/startree/4.0\" name=\"\">\n");
        //out.write("        <st:display-properties layout=\"radial\" clockwise=\"true\" stretchfactor=\"1.0\" style=\"-1\" ");
        //out.write("backgroundcolor=\"0xffffff\" selectioncolor=\"0x2cd300\" highlightcolor=\"0x2cd300\" ");
        //out.write("textsizemode=\"fittonodearea\" maxchars=\"30\" nchars=\"13\" nodearrangement=\"top\" nodelabelalignment=\"center\">\n");
        //out.write("          <st:textfont name=\"dialog\" size=\"12\" bold=\"false\" italic=\"false\"></st:textfont>\n");
        //out.write("     </st:display-properties>\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        //out.write("        <node>\n");
        //out.write("           <label>"+rootNode.get("NODETITLE")+"</label>\n");
        //out.write("           <description>"+rootNode.get("NODEDESC")+"</description>\n");
        //out.write("           <hierarchy enforce-parent=\"false\" aggregate=\"false\"></hierarchy>\n");
        //out.write("           <attributes>\n");
        //out.write("              <threshold>0.0</threshold>\n");
        //out.write("           </attributes>\n");
        //out.write("           <st:node-display-properties nodecolor=\"0xff0033\" textcolor=\"0xffffff\"></st:node-display-properties>\n");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, out, "Root", "RootParent");

        //out.write("        </node>\n");
        //out.write("</inxight-taxonomy>\n");

        out.close();

        return f;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer out, String SICCODE, String SICParent) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() < 1) return;
        
        
        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);
            String fileName = "";
        //    if (vSignatures.size() > 0) fileName = writeSample(n, vSignatures);

            //if (p.get("PARENTID").equalsIgnoreCase("-1")){
            	if (n.get("NODETITLE").startsWith("SIC")){
            	       
            	System.out.println("**DEBUG DEPTH**" + p.get("PARENTID"));
            	System.out.println("**DEBUG NODETITLE**" + n.get("NODETITLE"));
            	
            	String LEV2NODE = n.get("NODETITLE");
            	//SICParent = n.get("NODETITLE");
            	SICParent = "RootParent";
            	
            	String[] sSIC = LEV2NODE.split(" ");
            	SICCODE = sSIC[1];
            	// this is taking the second word in the title.
            	System.out.println("**DEBUG SIC**" + SICCODE);
            	
            	
            	//  add some stuff to create a sic code contains rule here
                String spaces = "           ";
                out.write(getEncodedName(n.get("NODETITLE").trim())+" | ");
                        
            	out.write(returnWordsAmpersand(FixTitle(n.get("NODETITLE"))) + "\r\n");
      
        }
                  
            if (n.get("NODETITLE").startsWith("SIC")){
            	SICParent = n.get("NODETITLE");
            }
            if (n.get("NODEID").equals(n.get("LINKNODEID"))) buildChildren(server, n, out, SICCODE,SICParent);

            //out.write(spaces+"   </node>\n");
            //out.write(spaces+"</link>\n");

            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }
        }
    }

    
    private File createOrGetFile(String path) throws Exception {
        File f = new File(path);
        if (!f.exists()) f.mkdir();

        return f;
    }


    private String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method

    private String returnWords(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");

        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!commonWords.containsKey(sArr[i].toLowerCase())) {
                if (i > 0) sb.append(" | ");
                sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
            }
        }

        return sb.toString();
    }
    
    private String returntermswithtitle(String Phrase, String Title) {
        StringBuffer sb = new StringBuffer("");
        sb.append(Phrase.toString());
        
        
        Title = Title.replaceAll(",", "");

        String[] sArr = Title.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!commonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.toString().toLowerCase().indexOf("nocase(\""+replaceWildcards(sArr[i].toLowerCase())+"\")" )==-1){
                		sb.append(" | ");
                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
                        
                       	}
            }
        }
            
        return getEncodedName(sb.toString());
    }
    
       
    private String FixTitle(String Phrase){
    	  StringBuffer sb = new StringBuffer("");
          Phrase = Phrase.replaceAll(",", "");
          String[] sArr = Phrase.split(" ");

          for (int i = 0; i < sArr.length; i++) {
          	if (sArr[i].toLowerCase().equals("not")){
          		return getEncodedName(sb.toString());
          	}
          	if (sArr[i].toLowerCase().equals("except")){
          		return getEncodedName(sb.toString());
          	}
        	if (sArr[i].toLowerCase().equals("without")){
          		return getEncodedName(sb.toString());
          	}
          	if (sArr[i].toLowerCase().equals("excluding")){
          		return getEncodedName(sb.toString());
          		
          		
          	}
          		
             sb.append(sArr[i]+" ");
             
          }
              
          return getEncodedName(sb.toString());
    	
    	
    	
    }
    
    
    private String returnWordsAmpersand(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");
        
        boolean FirstFlag = true;
        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
        	if (sArr[i].toLowerCase().equals("not")){
        		return getEncodedName(sb.toString());
        	}
        	if (sArr[i].toLowerCase().equals("except")){
        		return getEncodedName(sb.toString());
        	}
        	if (sArr[i].toLowerCase().equals("excluding")){
        		return getEncodedName(sb.toString());
        	}
        		
            if (!filtercommonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )==-1){
                		if (!FirstFlag) sb.append(" , ");
                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append(replaceWildcards(sArr[i]));
                        if (FirstFlag)FirstFlag = false;
                       	}
            }
        }
            
        return getEncodedName(sb.toString());
    }
    
    

    private String returnWordsAnd(String Phrase) {
        StringBuffer sb = new StringBuffer("");

        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (i > 0) sb.append(" | ");
            if (!commonWords.containsKey(sArr[i].toLowerCase())) sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
        }

        return sb.toString();
    }
    
    public static String getEncodedName(String s) {
        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        // s = s.replaceAll("\"", "&quot;");
        // inxight doesnt want to have these encoded
        s = s.replaceAll(">", "&gt;");
        s = s.replaceAll("'", "&apos;");

        return s;
    }
    
    
}