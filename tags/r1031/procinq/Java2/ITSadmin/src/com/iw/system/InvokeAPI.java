package com.iw.system;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.xeustechnologies.jtar.TarInputStream;
import org.xml.sax.InputSource;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.io.*;
import java.net.*;
import java.math.BigInteger;

import Logging.APIProps;
import com.iw.ui.PopupProgressBar;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

// InvokeAPI is a standardized JAVA wrapper used to invoke the Indraweb ITS API
//
// Example usage:
//
//  InvokeAPI API = new InvokeAPI ("security.TSLogin", htArgs);
//	HashTree htResults = API.Execute();

public class InvokeAPI {
	private String sTaxonomyServer;
	private String sAPIcall;
	private Hashtable htArguments;
	private String SessionKey = null;

	public PrintWriter out = null;

	private final int MAXIMUM_RETRY_ON_FAIL = 3;

	// Accessor methods to private variables
	public String GetTaxonomyServer() {
		return sTaxonomyServer;
	}

	public void SetTaxonomyServer(String sTaxonomyServer_) {
		sTaxonomyServer = sTaxonomyServer_;
	}

	public String GetAPIcall() {
		return sAPIcall;
	}

	public String GetSessionKey() {
		return SessionKey;
	}

	public Hashtable GetAPIarguments() {
		return htArguments;
	}

	final private String boundary = "ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
	final private String twoHyphens = "--";
	final private String lineEnd = "\r\n";

	// Object constructors
	public InvokeAPI(String sAPIcall, Hashtable htArguments,
			String sTaxonomyServer) {
		this.sAPIcall = sAPIcall;
		this.sTaxonomyServer = sTaxonomyServer;
		this.htArguments = htArguments;

		if (htArguments.containsKey("SKEY")) {
			this.SessionKey = (String) htArguments.get("SKEY");
		}
	}

	public InvokeAPI(String sAPIcall, Hashtable htArguments) {

		this.sAPIcall = sAPIcall;
		this.htArguments = htArguments;
		this.sTaxonomyServer = (String) htArguments.get("api");

		if (htArguments.containsKey("SKEY")) {
			this.SessionKey = (String) htArguments.get("SKEY");
		}
	}

	// This is an internal function -- if a tag name already exists, append to
	// it
	private String CreateName(Hashtable htHash, String sName) {
		int loop = 0;
		String sOrig = sName;

		// If the tag already exists, append to it and make it unique
		while (htHash.containsKey(sName)) {
			loop++;
			sName = sOrig + loop;
		}

		return sName;
	}

	// Execute the API call. Returns success 0 for success, -1 for failure.
	public HashTree Execute(boolean bDebug, boolean bUseProxy) throws Exception {
		return Execute(bDebug, bUseProxy, true);
	}

	public TarInputStream tarExecute(boolean bDebug, boolean bUseProxy)
			throws Exception {
		String sURL = sTaxonomyServer + sAPIcall;
		Enumeration e = htArguments.keys();

		VectorTree vOriginal = new VectorTree();
		vOriginal.SetParent(null);
		VectorTree vObject = vOriginal;

		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);

			if (!sKey.equals("api")) {
				try {
					sValue = URLEncoder.encode(sValue, "UTF-8");
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				sURL = sURL + "&" + sKey + "=" + sValue;
			}
		}

		// Make a socket connection to the server
		HttpURLConnection httpCon = null;
		try {
			long lStart = System.currentTimeMillis();

			URL myURL = null;

			if (!bUseProxy) {
				myURL = new URL(sURL);
			} else {
				myURL = new URL("http", // protocol,
						"172.16.101.110", // host name or IP of proxy server to
											// use
						8080, // proxy port or �1 to indicate the default port
								// for the protocol
						sURL); // the original URL, specified in the �file�
								// parameter
			}

			if (bDebug) {
				System.out.println("API call: " + sURL);
			}
			httpCon = (HttpURLConnection) myURL.openConnection();

			if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println("Http error: "
						+ httpCon.getResponseMessage());
				throw new Exception("Http error: "
						+ httpCon.getResponseMessage());
			}

			InputStream is = httpCon.getInputStream();
			int b = 0;
			byte[] data = new byte[2048];
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			GZIPInputStream gis = new GZIPInputStream(is);
			while ((b = gis.read(data, 0, data.length)) != -1) {
				bao.write(data, 0, b);
			}
			TarInputStream tis = new TarInputStream(new ByteArrayInputStream(
					bao.toByteArray()));
			httpCon.disconnect();
			return tis;
		} catch (Exception except) {
			System.out.println(except.getMessage());
			return null;
		}
	}

	public HashTree Execute(boolean bDebug, boolean bUseProxy,
			boolean bReplaceQuotes) throws Exception {
		String sURL = sTaxonomyServer + sAPIcall;
		Enumeration e = htArguments.keys();

		HashTree htOriginal = new HashTree();
		htOriginal.SetParent(null);
		HashTree htObject = htOriginal;

		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);

			if (!sKey.equals("api")) {
				if (bReplaceQuotes) {
					// Remove any apostraphies (because they are illegal) and
					// change spaces into + signs
					sKey = sKey.replaceAll("'", "''");
					sValue = sValue.replaceAll("'", "''");
				}
				sKey = sKey.replaceAll(" ", "+");
				sValue = URLEncoder.encode(sValue, "UTF-8");

				sURL = sURL + "&" + sKey + "=" + sValue;
			}
		}

		// Make a socket connection to the server
		HttpURLConnection httpCon = null;
		try {
			long lStart = System.currentTimeMillis();

			URL myURL = null;

			if (!bUseProxy) {
				myURL = new URL(sURL);
			} else {
				myURL = new URL("http", // protocol,
						"172.16.101.110", // host name or IP of proxy server to
											// use
						8080, // proxy port or �1 to indicate the default port
								// for the protocol
						sURL); // the original URL, specified in the �file�
								// parameter
			}

			if (bDebug) {
				System.out.println("API call: " + sURL);
			}

			int iErrorCount = 0;
			while (httpCon == null) {
				httpCon = (HttpURLConnection) myURL.openConnection();

				try {
					if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
						System.out.println("Http error: " + sURL + " "
								+ httpCon.getResponseMessage());
						httpCon = null;
					}
				} catch (Exception ex) {
					iErrorCount++;

					if (iErrorCount > MAXIMUM_RETRY_ON_FAIL) {
						System.out
								.println("Fatal error! "
										+ MAXIMUM_RETRY_ON_FAIL
										+ " consecutive network failures "
										+ "while attempting to reach the Procinct Server.");
						throw new ITSConnectionFailure(
								MAXIMUM_RETRY_ON_FAIL
										+ " consecutive network failures while attempting "
										+ "to reach the Procinct Server.");
					}

					System.out
							.println("Warning! Http error: " + sURL + " "
									+ ex.getMessage() + " .. retrying.. "
									+ iErrorCount);
					httpCon = null;

					Thread.currentThread().sleep(5000);
				}
			}
			InputStream is = httpCon.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is,
					"UTF-8"));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				if (bDebug) {
					System.out.println(sData);
				}
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");

				String sTag1;

				try {
					sTag1 = sData.substring(iTag1Start + 1, iTag1End);
				} catch (Exception eTag) {
					sTag1 = "";
				}

				String sTag2 = "";

				if (iTag1End != iTag2End) {
					sTag2 = sData.substring(iTag2Start + 2, iTag2End);
				}

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				if (sTag1.equals("SESSIONEXPIRED")) {
					System.out.println("Session Expired!");
					throw new SessionExpired("Session expired");
				}
				if (sTag1.equals("DATAERROR")) {
					throw new NotAuthorized("Data integrity violation");
				}

				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"UTF-8\" ?"))
						&& (!sTag1.equals("TSRESULT"))
						&& (!sTag2.equals("TSRESULT"))
						&& (!sTag1.equals("APIGETS"))
						&& (!sTag1.equals("CALLCOUNT"))
						&& (!sTag1.equals("CLASSLOAD"))
						&& (!sTag1.equals("TIMEOFCALL_MS"))) {

					if (bDebug && sTag1.equals("NODE")) {
						System.out.println("debug mode");
					}

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {
						sTag1 = new String(CreateName(htObject, sTag1));
						// System.out.println("<BR>Begin object: "+sTag1);

						if (sTag1.equals("TS_ERROR")) {
							httpCon.disconnect();
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							httpCon.disconnect();
							return htOriginal;
						}

						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1, htObject);

					}

					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2);
						htObject = (HashTree) htObject.GetParent();
					}

					// if both s1Tag and s2Tag are populated, store the field
					// into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String(getStringBetweenThisAndThat(
								sData, "<" + sTag1 + ">", "</" + sTag2 + ">"));
						// out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

						// If the variable value contains a "CDATA", eliminate
						// it here
						if (sTag.indexOf("<![CDATA") != -1) {
							sTag = new String(sTag.substring(9,
									(sTag.length() - 3)));
						}

						if (htObject.containsKey(sTag1))
							sTag1 = new String(CreateName(htObject, sTag1));

						htObject.put(sTag1, sTag);
					}
				}
			}

			httpCon.disconnect();
			httpCon = null;

			return htObject;
		} catch (NotAuthorized na) {
			throw na;
		} catch (SessionExpired se) {
			throw se;
		} catch (Exception except) {
			except.printStackTrace(System.out);
			throw except;
		} finally {
			if (httpCon != null) {
				httpCon.disconnect();
				httpCon = null;
			}
		}
	}

	// ******** Bring results back as vector instead of hash tree
	public Vector vExecute() throws Exception {
		return vExecute(false, false);
	}

	public Vector vExecute(boolean bDebug, boolean bUseProxy) throws Exception {
		String sURL = sTaxonomyServer + sAPIcall;
		Enumeration e = htArguments.keys();

		VectorTree vOriginal = new VectorTree();
		vOriginal.SetParent(null);
		VectorTree vObject = vOriginal;

		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);

			if (!sKey.equals("api")) {
				sValue = URLEncoder.encode(sValue, "UTF-8");
				sURL = sURL + "&" + sKey + "=" + sValue;
			}
		}

		// Make a socket connection to the server
		HttpURLConnection httpCon = null;
		try {
			long lStart = System.currentTimeMillis();

			URL myURL = null;

			if (!bUseProxy) {
				myURL = new URL(sURL);
			} else {
				myURL = new URL("http", // protocol,
						"172.16.101.110", // host name or IP of proxy server to
											// use
						8080, // proxy port or �1 to indicate the default port
								// for the protocol
						sURL); // the original URL, specified in the �file�
								// parameter
			}

			if (bDebug) {
				System.out.println("API call: " + sURL);
			}
			httpCon = (HttpURLConnection) myURL.openConnection();

			if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println("Http error: "
						+ httpCon.getResponseMessage());
				throw new Exception("Http error: "
						+ httpCon.getResponseMessage());
			}

			InputStream is = httpCon.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is,
					"UTF-8"));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				if (bDebug) {
					System.out.println(sData);
				}
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");

				String sTag1;

				try {
					sTag1 = sData.substring(iTag1Start + 1, iTag1End);
				} catch (Exception eTag) {
					sTag1 = "";
				}

				String sTag2 = "";

				if (iTag1End != iTag2End) {
					sTag2 = sData.substring(iTag2Start + 2, iTag2End);
				}

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				if (sTag1.equals("SESSIONEXPIRED")) {
					System.out.println("Session Expired!");
					throw new SessionExpired("Session expired");
				}

				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"UTF-8\" ?"))
						&& (!sTag1.equals("TSRESULT"))
						&& (!sTag2.equals("TSRESULT"))
						&& (!sTag1.equals("APIGETS"))
						&& (!sTag1.equals("CALLCOUNT"))
						&& (!sTag1.equals("CLASSLOAD"))
						&& (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						if (sTag1.equals("TS_ERROR")) {
							httpCon.disconnect();
							return vOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							httpCon.disconnect();
							return vOriginal;
						}
						if (sTag1.equals("INTERNALERRMSG")) {
							httpCon.disconnect();
							return vOriginal;
						}

						VectorTree vOldObject = vObject;
						vObject = new VectorTree();
						vObject.SetParent(vOldObject);
						vOldObject.addElement(vObject);
					}

					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						vObject = (VectorTree) vObject.GetParent();
					}

					// if both s1Tag and s2Tag are populated, store the field
					// into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String(getStringBetweenThisAndThat(
								sData, "<" + sTag1 + ">", "</" + sTag2 + ">"));

						// If the variable value contains a "CDATA", eliminate
						// it here
						if (sTag.indexOf("<![CDATA") != -1) {
							sTag = new String(sTag.substring(9,
									(sTag.length() - 3)));
						}

						VectorValue vv = new VectorValue(sTag1, sTag);
						vObject.addElement(vv);
					}
				}
			}

			httpCon.disconnect();
			long lStop = System.currentTimeMillis() - lStart;
			// System.out.println("<!-- Timing: " + lStop + " ms -->");

			return vObject;
		} catch (SessionExpired se) {
			throw se;
		} catch (Exception except) {
			return null;
		}
	}

	// ******** Bring results back as DOM tree instead of hash tree or vector
	public org.dom4j.Document dExecute() {
		return dExecute(false, false);
	}

	public org.dom4j.Document dExecute(boolean bDebug, boolean bUseProxy) {
		return dExecute(bDebug, bUseProxy, null);
	}

	public org.dom4j.Document dExecute(boolean bDebug, boolean bUseProxy,
			PopupProgressBar bar) {
		try {
			long lStart = System.currentTimeMillis();
			URL myURL = getURL(bUseProxy, bDebug);
			HttpURLConnection httpCon = getHttpURLConnection(bUseProxy, bDebug);
			InputStream is = httpCon.getInputStream();

			SAXReader xmlReader = new SAXReader(false);

			ProgressStatusHandler handler = new ProgressStatusHandler(bar);
			xmlReader.setDefaultHandler(handler);

			if (bDebug) {
				ErrorHandler eh = new ErrorHandler() {
					public void fatalError(SAXParseException s) {
						System.out.println("fatal error.");
					}

					public void error(SAXParseException s) {
						System.out.println("error.");
					}

					public void warning(SAXParseException s) {
						System.out.println("warning.");
					}
				};
				xmlReader.setErrorHandler(eh);
			}
			InputSource ins = new InputSource(is);
			ins.setEncoding("UTF-8");

			org.dom4j.Document doc = xmlReader.read(ins);

			httpCon.disconnect();
			long lStop = System.currentTimeMillis() - lStart;
			// System.out.println("<!-- Timing: " + lStop + " ms -->");

			return doc;
		} catch (Exception except) {
			except.printStackTrace();
		}
		return null;
	}

	/**
	 * send bao(Gzip) to server
	 * @param bao
	 * @param bDebug
	 * @param bUseProxy
	 * @return
	 */
	public org.dom4j.Document dExecuteBaoGZ(ByteArrayOutputStream bao,
			boolean bDebug, boolean bUseProxy) {
		long lStart = System.currentTimeMillis();

		// Make a socket connection to the server
		HttpURLConnection httpURLConn = null;
		DataOutputStream outStream;

		try {
			

			URL theURL = getURL(bUseProxy, bDebug);
			//URL theURL = new URL("http://localhost:8080/reciveArchiveGzip-1.0-SNAPSHOT/reciveF?nodeid="+ htArguments.get("NodeID"));
			httpURLConn = (HttpURLConnection) theURL.openConnection();

			httpURLConn.setRequestMethod("POST");
			httpURLConn.setRequestProperty("Connection", "Keep-Alive");
			httpURLConn.setDoOutput(true);
			httpURLConn.setUseCaches(false);
			httpURLConn.setRequestProperty("Accept-Charset",
					"iso-8859-1,*,utf-8");
			httpURLConn.setRequestProperty("Accept-Language", "en");
			httpURLConn.setRequestProperty("Content-type",
					"multipart/form-data; boundary=" + boundary);

			// open output stream to server, POST data and multipart form up to
			// the file data
			outStream = new DataOutputStream(httpURLConn.getOutputStream());

			// send file format .gz to improve time
			outStream.write((twoHyphens + boundary + lineEnd).getBytes());
			outStream.write("Content-Disposition: form-data; name=\"getFileGz\""
							.getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(lineEnd.getBytes());
			// data--------------------------
			outStream.write(lineEnd.getBytes());
			outStream.writeBytes(twoHyphens + boundary + lineEnd);

			// file
			outStream.write(("Content-Disposition: form-data; name=" + "\""
					+ "file" + "\" " + "filename=\"file1.gz\"").getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write("Content-Type: application/x-gzip".getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(bao.toByteArray());
			outStream.write(lineEnd.getBytes());
			outStream.write((twoHyphens + boundary + twoHyphens).getBytes());
			outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// close streams
			outStream.flush();
			outStream.close();

			// display server response data on console.
			InputStream is = httpURLConn.getInputStream();

			SAXReader xmlReader = new SAXReader(false);
			org.dom4j.Document doc = xmlReader.read(is);

			long lStop = System.currentTimeMillis() - lStart;

			// System.out.println("<!-- Timing: " + lStop + " ms -->");

			return doc;
		} catch (Exception except) {
			except.printStackTrace();
		} finally {
			if (httpURLConn != null) {
				httpURLConn.disconnect();
				httpURLConn = null;
			}
		}
		return null;
	}

	public org.dom4j.Document dExecute(File f) {
		return dExecute(f, false, false, false);
	}

	public org.dom4j.Document dExecute(File f, boolean bDebug,
			boolean bUseProxy, boolean sendGZ) {
		long lStart = System.currentTimeMillis();

		// Make a socket connection to the server
		HttpURLConnection httpURLConn = null;
		DataOutputStream outStream;
		DataInputStream inStream;
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		FileInputStream fileInputStream = null;

		try {
			// create FileInputStream to read from file

			fileInputStream = new FileInputStream(f);

			// URL theURL = getURL(bUseProxy, bDebug);
			URL theURL = new URL(
					"http://localhost:8080/reciveArchiveGzip-1.0-SNAPSHOT/reciveF?nodeid="
							+ htArguments.get("NodeID"));
			httpURLConn = (HttpURLConnection) theURL.openConnection();

			httpURLConn.setRequestMethod("POST");
			httpURLConn.setRequestProperty("Connection", "Keep-Alive");
			httpURLConn.setDoOutput(true);
			httpURLConn.setUseCaches(false);
			httpURLConn.setRequestProperty("Accept-Charset",
					"iso-8859-1,*,utf-8");
			httpURLConn.setRequestProperty("Accept-Language", "en");
			httpURLConn.setRequestProperty("Content-type",
					"multipart/form-data; boundary=" + boundary);

			// open output stream to server, POST data and multipart form up to
			// the file data
			outStream = new DataOutputStream(httpURLConn.getOutputStream());

			if (sendGZ) {
				// send file format .gz to improve time
				outStream.write((twoHyphens + boundary + lineEnd).getBytes());
				outStream
						.write("Content-Disposition: form-data; name=\"getFileGz\""
								.getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(lineEnd.getBytes());
				// data--------------------------
				outStream.write(lineEnd.getBytes());
				outStream.writeBytes(twoHyphens + boundary + lineEnd);

				// file
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				GZIPOutputStream gos = new GZIPOutputStream(bao);

				byte[] data = new byte[1024];
				int b = 0;
				while ((b = fileInputStream.read(data)) != -1) {
					gos.write(data, 0, b);
				}
				gos.flush();
				gos.close();
				outStream.write(("Content-Disposition: form-data; name=" + "\""
						+ "file" + "\" " + "filename=\"file1.gz\"").getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write("Content-Type: application/x-gzip".getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(bao.toByteArray());
				outStream.write(lineEnd.getBytes());
				outStream
						.write((twoHyphens + boundary + twoHyphens).getBytes());
				outStream.writeBytes(twoHyphens + boundary + twoHyphens
						+ lineEnd);

			} else {
				outStream.writeBytes(twoHyphens + boundary + lineEnd);
				outStream
						.writeBytes("Content-Disposition: form-data; name=\"import\";"
								+ " filename=\"" + f.getName() + "\"" + lineEnd);
				outStream.writeBytes(lineEnd);

				// create a buffer of maximum size
				// (not really necessary, but might speed up things a bit)
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];
				// int tot=bufferSize; //debug

				// read file data and write it into form
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				while (bytesRead > 0) {
					outStream.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);// might
																			// change
																			// in
																			// the
																			// last
																			// loop
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}

				// send multipart form data necessary after file data
				outStream.writeBytes(lineEnd);
				outStream.writeBytes(twoHyphens + boundary + twoHyphens
						+ lineEnd);
			}
			// close streams
			fileInputStream.close();
			fileInputStream = null;
			outStream.flush();
			outStream.close();

			// display server response data on console.
			InputStream is = httpURLConn.getInputStream();

			SAXReader xmlReader = new SAXReader(false);
			org.dom4j.Document doc = xmlReader.read(is);

			long lStop = System.currentTimeMillis() - lStart;

			// System.out.println("<!-- Timing: " + lStop + " ms -->");

			return doc;
		} catch (Exception except) {
			except.printStackTrace();
		} finally {
			if (httpURLConn != null) {
				httpURLConn.disconnect();
				httpURLConn = null;
			}
		}
		return null;
	}

	public org.dom4j.Document dPostExecuteStatus(File file,
			PopupProgressBar bar, boolean sendGZ) throws Exception {

		HttpURLConnection httpURLConn = null;
		DataOutputStream outStream;
		DataInputStream inStream;
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		SAXReader xmlReader = new SAXReader(false);

		ProgressStatusHandler handler = new ProgressStatusHandler(bar);
		xmlReader.setDefaultHandler(handler);

		// create FileInputStream to read from file
		FileInputStream fileInputStream = new FileInputStream(file);

		String sURL = sTaxonomyServer + sAPIcall;
		Enumeration et = htArguments.keys();

		// Build URL to call Taxonomy Server
		while (et.hasMoreElements()) {
			String sKey = (String) et.nextElement();
			String sValue = (String) htArguments.get(sKey);

			sURL = sURL + "&" + sKey + "=" + sValue;

			// Remove any apostraphies (because they are illegal) and change
			// spaces into + signs
			sURL = sURL.replaceAll(" ", "+");
			sURL = sURL.replaceAll("'", "");
		}

		URL theURL = new URL(sURL);
		httpURLConn = (HttpURLConnection) theURL.openConnection();

		httpURLConn.setRequestMethod("POST");
		httpURLConn.setRequestProperty("Connection", "Keep-Alive");
		httpURLConn.setDoOutput(true);
		httpURLConn.setUseCaches(false);
		httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
		httpURLConn.setRequestProperty("Accept-Language", "en");
		httpURLConn.setRequestProperty("Content-type",
				"multipart/form-data; boundary=" + boundary);

		// open output stream to server, POST data and multipart form up to
		// the file data
		outStream = new DataOutputStream(httpURLConn.getOutputStream());
		if (sendGZ) {
			// send file format .gz to improve time
			outStream.write((twoHyphens + boundary + lineEnd).getBytes());
			outStream
					.write("Content-Disposition: form-data; name=\"getFileGz\""
							.getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(lineEnd.getBytes());
			// data--------------------------
			outStream.write(lineEnd.getBytes());
			outStream.writeBytes(twoHyphens + boundary + lineEnd);

			// file
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			GZIPOutputStream gos = new GZIPOutputStream(bao);

			byte[] data = new byte[1024];
			int b = 0;
			while ((b = fileInputStream.read(data)) != -1) {
				gos.write(data, 0, b);
			}
			gos.flush();
			gos.close();
			outStream.write(("Content-Disposition: form-data; name=" + "\""
					+ "file" + "\" " + "filename=\"file1.gz\"").getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write("Content-Type: application/x-gzip".getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(bao.toByteArray());
			outStream.write(lineEnd.getBytes());
			outStream.write((twoHyphens + boundary + twoHyphens).getBytes());
			outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

		} else {
			outStream.writeBytes(twoHyphens + boundary + lineEnd);
			outStream
					.writeBytes("Content-Disposition: form-data; name=\"import\";"
							+ " filename=\"" + file.getName() + "\"" + lineEnd);
			outStream.writeBytes(lineEnd);

			// create a buffer of maximum size
			// (not really necessary, but might speed up things a bit)
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];
			// int tot=bufferSize; //debug

			// read file data and write it into form
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0) {
				outStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);// might
																		// change
																		// in
																		// the
																		// last
																		// loop
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multipart form data necessary after file data
			outStream.writeBytes(lineEnd);
			outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		}
		// close streams
		fileInputStream.close();
		outStream.flush();
		outStream.close();

		// display server response data on console.
		InputStream is = httpURLConn.getInputStream();
		org.dom4j.Document doc = xmlReader.read(is);

		return doc;

	}

	public org.dom4j.Document dExecutePost() {
		return dExecutePost(false, false);
	}

	public org.dom4j.Document dExecutePost(boolean bDebug, boolean bUseProxy) {
		long lStart = System.currentTimeMillis();

		HttpURLConnection httpURLConn = null;

		int maxBufferSize = 1048576;
		FileInputStream fileInputStream = null;
		try {
			URL theURL = getURLPost(bUseProxy, bDebug);
			Enumeration e = this.htArguments.keys();
			httpURLConn = (HttpURLConnection) theURL.openConnection();
			httpURLConn.setRequestMethod("POST");
			httpURLConn.setRequestProperty("Connection", "Keep-Alive");
			httpURLConn.setDoInput(true);
			httpURLConn.setDoOutput(true);
			httpURLConn.setUseCaches(false);
			httpURLConn.setDefaultUseCaches(false);
			httpURLConn
					.addRequestProperty("Content-Type", "application/binary");
			httpURLConn.setAllowUserInteraction(false);

			httpURLConn.setDefaultUseCaches(false);
			PrintWriter out = new PrintWriter(new DataOutputStream(
					httpURLConn.getOutputStream()));

			int loop = 0;

			while (e.hasMoreElements()) {
				String sKey = (String) e.nextElement();
				String sValue = (String) this.htArguments.get(sKey);

				if ((sKey.toUpperCase().equals("API"))
						|| (sKey.toUpperCase().equals("SKEY")))
					continue;
				loop++;

				String parameterString = URLEncoder.encode(sKey, "UTF-8") + "="
						+ sValue;
				if (loop > 0) {
					parameterString = "&" + parameterString;
				}

				out.print(parameterString);

				out.flush();
			}

			out.flush();
			out.close();

			InputStream is = httpURLConn.getInputStream();

			SAXReader xmlReader = new SAXReader(false);
			org.dom4j.Document doc = xmlReader.read(is);

			long lStop = System.currentTimeMillis() - lStart;

			org.dom4j.Document localDocument1 = doc;
			return localDocument1;
		} catch (Exception except) {
			except.printStackTrace();
		} finally {
			if (httpURLConn != null) {
				httpURLConn.disconnect();
				httpURLConn = null;
			}
		}
		return null;
	}

	// Send POST API call
	public HashTree PostExecute(File f) throws Exception {
		return PostExecute(f, true, false);
	}

	/**
	 * 
	 * @param f
	 * @param bUseImportDir
	 * @param sendGZ
	 *            (use true to get ByteArrayInputStream in the server)
	 * @return
	 * @throws Exception
	 */
	public HashTree PostExecute(File f, boolean bUseImportDir, boolean sendGZ)
			throws Exception {
		try {
			HashTree htOriginal = new HashTree();
			htOriginal.SetParent(null);
			HashTree htObject = htOriginal;

			HttpURLConnection httpURLConn = null;
			DataOutputStream outStream;
			DataInputStream inStream;
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;

			// create FileInputStream to read from file
			FileInputStream fileInputStream = new FileInputStream(f);

			String sURL = sTaxonomyServer + sAPIcall;
			Enumeration et = htArguments.keys();

			// Build URL to call Taxonomy Server
			while (et.hasMoreElements()) {
				String sKey = (String) et.nextElement();
				String sValue = (String) htArguments.get(sKey);

				sURL = sURL + "&" + sKey + "=" + sValue;

				// Remove any apostraphies (because they are illegal) and change
				// spaces into + signs
				sURL = sURL.replaceAll(" ", "+");
				sURL = sURL.replaceAll("'", "");
			}

			URL theURL = new URL(sURL);
			httpURLConn = (HttpURLConnection) theURL.openConnection();

			httpURLConn.setRequestMethod("POST");
			httpURLConn.setRequestProperty("Connection", "Keep-Alive");
			httpURLConn.setDoOutput(true);
			httpURLConn.setUseCaches(false);
			httpURLConn.setRequestProperty("Accept-Charset",
					"iso-8859-1,*,utf-8");
			httpURLConn.setRequestProperty("Accept-Language", "en");
			httpURLConn.setRequestProperty("Content-type",
					"multipart/form-data; boundary=" + boundary);

			// open output stream to server, POST data and multipart form up to
			// the file data
			outStream = new DataOutputStream(httpURLConn.getOutputStream());
			if (sendGZ) {
				// send file format .gz to improve time
				outStream.write((twoHyphens + boundary + lineEnd).getBytes());
				outStream
						.write("Content-Disposition: form-data; name=\"getFileGz\""
								.getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(lineEnd.getBytes());
				// data--------------------------
				outStream.write(lineEnd.getBytes());
				outStream.writeBytes(twoHyphens + boundary + lineEnd);

				// file
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				GZIPOutputStream gos = new GZIPOutputStream(bao);

				byte[] data = new byte[1024];
				int b = 0;
				while ((b = fileInputStream.read(data)) != -1) {
					gos.write(data, 0, b);
				}
				gos.flush();
				gos.close();
				outStream.write(("Content-Disposition: form-data; name=" + "\""
						+ "file" + "\" " + "filename=\"file1.gz\"").getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write("Content-Type: application/x-gzip".getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(bao.toByteArray());
				outStream.write(lineEnd.getBytes());
				outStream
						.write((twoHyphens + boundary + twoHyphens).getBytes());
				outStream.writeBytes(twoHyphens + boundary + twoHyphens
						+ lineEnd);

			} else {
				outStream.writeBytes(twoHyphens + boundary + lineEnd);
				outStream
						.writeBytes("Content-Disposition: form-data; name=\"import\";"
								+ " filename=\"" + f.getName() + "\"" + lineEnd);
				outStream.writeBytes(lineEnd);

				// create a buffer of maximum size
				// (not really necessary, but might speed up things a bit)
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];
				// int tot=bufferSize; //debug

				// read file data and write it into form
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				while (bytesRead > 0) {
					outStream.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);// might
																			// change
																			// in
																			// the
																			// last
																			// loop
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}

				// send multipart form data necessary after file data
				outStream.writeBytes(lineEnd);
				outStream.writeBytes(twoHyphens + boundary + twoHyphens
						+ lineEnd);
			}
			// close streams
			fileInputStream.close();
			outStream.flush();
			outStream.close();

			// display server response data on console.
			InputStream is = httpURLConn.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				// api.Log.Log(sData);
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");

				String sTag1;

				try {
					sTag1 = sData.substring(iTag1Start + 1, iTag1End);
				} catch (Exception eTag) {
					sTag1 = "";
				}

				String sTag2 = "";

				if (iTag1End != iTag2End) {
					sTag2 = sData.substring(iTag2Start + 2, iTag2End);
				}

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1
						.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?"))
						&& (!sTag1.equals("TSRESULT"))
						&& (!sTag2.equals("TSRESULT"))
						&& (!sTag1.equals("APIGETS"))
						&& (!sTag1.equals("CALLCOUNT"))
						&& (!sTag1.equals("IndraXMLArgs"))
						&& (!sTag1.equals("CLASSLOAD"))
						&& (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						sTag1 = new String(CreateName(htObject, sTag1));
						// out.println("<BR>Begin object: "+sTag1);

						if (sTag1.equals("TS_ERROR")) {
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							throw new Exception("Internal stack trace");
						}

						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1, htObject);

					}

					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2);
						htObject = (HashTree) htObject.GetParent();
					}

					// if both s1Tag and s2Tag are populated, store the field
					// into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String(getStringBetweenThisAndThat(
								sData, "<" + sTag1 + ">", "</" + sTag2 + ">"));
						// out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

						// If the variable value contains a "CDATA", eliminate
						// it here
						if (sTag.indexOf("<![CDATA") != -1) {
							sTag = new String(sTag.substring(9,
									(sTag.length() - 3)));
						}

						htObject.put(sTag1, sTag);
					}
				} else {
					// System.out.println("InvokeAPI ignoring XML line [" +
					// sTag1 + "]" );
				}
			}

			return htObject;
		} catch (Exception except) {
			throw except;
		}

	}

	/*
	 * //post send bynary data public HashTree PostExecute(byte[] data,boolean
	 * bUseImportDir) throws Exception { try { HashTree htOriginal = new
	 * HashTree(); htOriginal.SetParent(null); HashTree htObject = htOriginal;
	 * 
	 * HttpURLConnection httpURLConn = null; DataOutputStream outStream;
	 * DataInputStream inStream; int bytesRead, bytesAvailable, bufferSize;
	 * byte[] buffer; int maxBufferSize = 1 * 1024 * 1024; String sURL =
	 * sTaxonomyServer + sAPIcall; Enumeration et = htArguments.keys(); while
	 * (et.hasMoreElements()) { String sKey = (String) et.nextElement(); String
	 * sValue = (String) htArguments.get(sKey);
	 * 
	 * sURL = sURL + "&" + sKey + "=" + sValue;
	 * 
	 * // Remove any apostraphies (because they are illegal) and change //
	 * spaces into + signs sURL = sURL.replaceAll(" ", "+"); sURL =
	 * sURL.replaceAll("'", ""); }
	 * 
	 * URL theURL = new URL(sURL); httpURLConn = (HttpURLConnection)
	 * theURL.openConnection();
	 * 
	 * httpURLConn.setRequestMethod("POST");
	 * httpURLConn.setRequestProperty("Connection", "Keep-Alive");
	 * httpURLConn.setDoOutput(true); httpURLConn.setUseCaches(false);
	 * httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
	 * httpURLConn.setRequestProperty("Accept-Language", "en");
	 * httpURLConn.setRequestProperty("Content-type",
	 * "multipart/form-data; boundary=" + boundary);
	 * 
	 * // open output stream to server, POST data and multipart form up to the
	 * file data outStream = new
	 * DataOutputStream(httpURLConn.getOutputStream());
	 * 
	 * outStream.writeBytes(twoHyphens + boundary + lineEnd);
	 * outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" +
	 * " filename=\"" + "image.jpg" + "\"" + lineEnd);
	 * outStream.writeBytes(lineEnd); outStream.write(data); while (bytesRead >
	 * 0) { outStream.write(buffer, 0, bufferSize); bytesAvailable =
	 * fileInputStream.available(); bufferSize = Math.min(bytesAvailable,
	 * maxBufferSize);//might change in the last loop bytesRead =
	 * fileInputStream.read(buffer, 0, bufferSize); } // send multipart form
	 * data necessary after file data outStream.writeBytes(lineEnd);
	 * outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
	 * 
	 * 
	 * outStream.flush(); outStream.close(); // display server response data on
	 * console. InputStream is = httpURLConn.getInputStream(); BufferedReader
	 * buf = new BufferedReader(new InputStreamReader(is)); String sData = new
	 * String();
	 * 
	 * while ((sData = buf.readLine()) != null) { //api.Log.Log(sData); int
	 * iTag1Start = sData.indexOf("<"); int iTag1End = sData.indexOf(">"); int
	 * iTag2Start = sData.lastIndexOf("</"); int iTag2End =
	 * sData.lastIndexOf(">");
	 * 
	 * String sTag1;
	 * 
	 * try { sTag1 = sData.substring(iTag1Start + 1, iTag1End); } catch
	 * (Exception eTag) { sTag1 = ""; }
	 * 
	 * String sTag2 = "";
	 * 
	 * if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start + 2,
	 * iTag2End); }
	 * 
	 * // This is how we detect an END tag if ((!sTag1.equals("")) &&
	 * (sTag1.charAt(0) == '/')) { sTag2 = new String(sTag1.substring(1)); sTag1
	 * = new String(""); }
	 * 
	 * // HACK! HACK! Ignore lines that start with.. if
	 * ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) &&
	 * (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
	 * (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
	 * (!sTag1.equals("IndraXMLArgs")) && (!sTag1.equals("CLASSLOAD")) &&
	 * (!sTag1.equals("TIMEOFCALL_MS"))) {
	 * 
	 * // If s1Tag is populated but s2Tag is not, start the object if
	 * ((!sTag1.equals("")) && (sTag2.equals(""))) {
	 * 
	 * sTag1 = new String(CreateName(htObject, sTag1));
	 * //out.println("<BR>Begin object: "+sTag1);
	 * 
	 * if (sTag1.equals("TS_ERROR")) { return htOriginal; } if
	 * (sTag1.equals("INTERNALSTACKTRACE")) { throw new
	 * Exception("Internal stack trace"); }
	 * 
	 * HashTree htOldObject = htObject; htObject = new HashTree();
	 * htObject.SetParent(htOldObject); htOldObject.put(sTag1, htObject);
	 * 
	 * }
	 * 
	 * // if s2Tag is populated but s1Tag is not, end the object if
	 * ((!sTag2.equals("")) && (sTag1.equals(""))) { //
	 * out.println("<BR>End object: "+sTag2); htObject = (HashTree)
	 * htObject.GetParent(); }
	 * 
	 * // if both s1Tag and s2Tag are populated, store the field into object
	 * started if ((!sTag2.equals("")) && (!sTag1.equals(""))) { String sTag =
	 * new String(getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" +
	 * sTag2 + ">"));
	 * //out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->"); //
	 * out.println("<BR>Add tag: "+sTag1+" value: "+sTag);
	 * 
	 * // If the variable value contains a "CDATA", eliminate it here if
	 * (sTag.indexOf("<![CDATA") != -1) { sTag = new String(sTag.substring(9,
	 * (sTag.length() - 3))); }
	 * 
	 * htObject.put(sTag1, sTag); } } else {
	 * //System.out.println("InvokeAPI ignoring XML line [" + sTag1 + "]" ); } }
	 * 
	 * return htObject; } catch (Exception except) { throw except; }
	 * 
	 * }
	 */

	public HashTree PostExecute(ArrayList<FileMetadata> listFile,
			byte[] infoData, boolean bUseImportDir) throws Exception {
		try {
			HashTree htOriginal = new HashTree();
			htOriginal.SetParent(null);
			HashTree htObject = htOriginal;

			HttpURLConnection httpURLConn = null;
			OutputStream outStream;
			DataInputStream inStream;
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;
			String sURL = sTaxonomyServer + sAPIcall;
			Enumeration et = htArguments.keys();
			while (et.hasMoreElements()) {
				String sKey = (String) et.nextElement();
				String sValue = (String) htArguments.get(sKey);

				sURL = sURL + "&" + sKey + "=" + sValue;

				// Remove any apostraphies (because they are illegal) and change
				// spaces into + signs
				sURL = sURL.replaceAll(" ", "+");
				sURL = sURL.replaceAll("'", "");
			}

			URL theURL = new URL(sURL);
			httpURLConn = (HttpURLConnection) theURL.openConnection();

			httpURLConn.setRequestMethod("POST");
			httpURLConn.setRequestProperty("Connection", "Keep-Alive");
			httpURLConn.setDoOutput(true);
			httpURLConn.setUseCaches(false);
			httpURLConn.setRequestProperty("Accept-Charset",
					"iso-8859-1,*,utf-8");
			httpURLConn.setRequestProperty("Accept-Language", "en");
			httpURLConn.setRequestProperty("Content-type",
					"multipart/form-data; boundary=" + boundary);

			// open output stream to server, POST data and multipart form up to
			// the file data
			// outStream = new DataOutputStream(httpURLConn.getOutputStream());
			outStream = httpURLConn.getOutputStream();
			outStream.write((twoHyphens + boundary + lineEnd).getBytes());
			outStream.write("Content-Disposition: form-data; name=\"infoData\""
					.getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(lineEnd.getBytes());
			outStream.write(infoData);
			outStream.write(lineEnd.getBytes());
			outStream.write((twoHyphens + boundary).getBytes());
			for (FileMetadata f : listFile) {
				outStream.write(lineEnd.getBytes());
				outStream.write(("Content-Disposition: form-data; name=" + "\""
						+ f.getId() + "\" " + "filename=\"file1.jpg\"")
						.getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write("Content-Type: application/binary".getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(lineEnd.getBytes());
				outStream.write(f.getImage());
				outStream.write(lineEnd.getBytes());
				outStream
						.write((twoHyphens + boundary + twoHyphens).getBytes());
			}
			outStream.flush();
			outStream.close();
			// display server response data on console.
			InputStream is = httpURLConn.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				// api.Log.Log(sData);
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");

				String sTag1;

				try {
					sTag1 = sData.substring(iTag1Start + 1, iTag1End);
				} catch (Exception eTag) {
					sTag1 = "";
				}

				String sTag2 = "";

				if (iTag1End != iTag2End) {
					sTag2 = sData.substring(iTag2Start + 2, iTag2End);
				}

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1
						.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?"))
						&& (!sTag1.equals("TSRESULT"))
						&& (!sTag2.equals("TSRESULT"))
						&& (!sTag1.equals("APIGETS"))
						&& (!sTag1.equals("CALLCOUNT"))
						&& (!sTag1.equals("IndraXMLArgs"))
						&& (!sTag1.equals("CLASSLOAD"))
						&& (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						sTag1 = new String(CreateName(htObject, sTag1));
						// out.println("<BR>Begin object: "+sTag1);

						if (sTag1.equals("TS_ERROR")) {
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							throw new Exception("Internal stack trace");
						}

						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1, htObject);

					}

					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2);
						htObject = (HashTree) htObject.GetParent();
					}

					// if both s1Tag and s2Tag are populated, store the field
					// into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String(getStringBetweenThisAndThat(
								sData, "<" + sTag1 + ">", "</" + sTag2 + ">"));
						// out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

						// If the variable value contains a "CDATA", eliminate
						// it here
						if (sTag.indexOf("<![CDATA") != -1) {
							sTag = new String(sTag.substring(9,
									(sTag.length() - 3)));
						}

						htObject.put(sTag1, sTag);
					}
				} else {
					// System.out.println("InvokeAPI ignoring XML line [" +
					// sTag1 + "]" );
				}
			}

			return htObject;
		} catch (Exception except) {
			throw except;
		}
	}

	// Send POST API call
	public Vector vPostExecute(File f) throws Exception {
		return vPostExecute(f, true);
	}

	public Vector vPostExecute(File f, boolean bUseImportDir) throws Exception {
		try {
			VectorTree vOriginal = new VectorTree();
			vOriginal.SetParent(null);
			VectorTree vObject = vOriginal;

			HttpURLConnection httpURLConn = null;
			DataOutputStream outStream;
			DataInputStream inStream;
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024;

			// create FileInputStream to read from file
			FileInputStream fileInputStream = new FileInputStream(f);

			String sURL = sTaxonomyServer + sAPIcall;
			Enumeration et = htArguments.keys();

			// Build URL to call Taxonomy Server
			while (et.hasMoreElements()) {
				String sKey = (String) et.nextElement();
				String sValue = (String) htArguments.get(sKey);

				sURL = sURL + "&" + sKey + "=" + sValue;

				// Remove any apostraphies (because they are illegal) and change
				// spaces into + signs
				sURL = sURL.replaceAll(" ", "+");
				sURL = sURL.replaceAll("'", "");
			}

			URL theURL = new URL(sURL);
			httpURLConn = (HttpURLConnection) theURL.openConnection();

			httpURLConn.setRequestMethod("POST");
			httpURLConn.setRequestProperty("Connection", "Keep-Alive");
			httpURLConn.setDoOutput(true);
			httpURLConn.setUseCaches(false);
			httpURLConn.setRequestProperty("Accept-Charset",
					"iso-8859-1,*,utf-8");
			httpURLConn.setRequestProperty("Accept-Language", "en");
			httpURLConn.setRequestProperty("Content-type",
					"multipart/form-data; boundary=" + boundary);

			// open output stream to server, POST data and multipart form up to
			// the file data
			outStream = new DataOutputStream(httpURLConn.getOutputStream());

			outStream.writeBytes(twoHyphens + boundary + lineEnd);
			outStream
					.writeBytes("Content-Disposition: form-data; name=\"import\";"
							+ " filename=\"" + f.getName() + "\"" + lineEnd);
			outStream.writeBytes(lineEnd);

			// create a buffer of maximum size
			// (not really necessary, but might speed up things a bit)
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];
			// int tot=bufferSize; //debug

			// read file data and write it into form
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0) {
				outStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);// might
																		// change
																		// in
																		// the
																		// last
																		// loop
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multipart form data necessary after file data
			outStream.writeBytes(lineEnd);
			outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// close streams
			fileInputStream.close();
			outStream.flush();
			outStream.close();

			// display server response data on console.
			InputStream is = httpURLConn.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				// api.Log.Log(sData);
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");

				String sTag1;

				try {
					sTag1 = sData.substring(iTag1Start + 1, iTag1End);
				} catch (Exception eTag) {
					sTag1 = "";
				}

				String sTag2 = "";

				if (iTag1End != iTag2End) {
					sTag2 = sData.substring(iTag2Start + 2, iTag2End);
				}

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1
						.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?"))
						&& (!sTag1.equals("TSRESULT"))
						&& (!sTag2.equals("TSRESULT"))
						&& (!sTag1.equals("APIGETS"))
						&& (!sTag1.equals("CALLCOUNT"))
						&& (!sTag1.equals("IndraXMLArgs"))
						&& (!sTag1.equals("CLASSLOAD"))
						&& (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						if (sTag1.equals("TS_ERROR")) {
							return vOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							throw new Exception("Internal stack trace");
						}

						VectorTree vOldObject = vObject;
						vObject = new VectorTree();
						vObject.SetParent(vOldObject);
						vOldObject.addElement(vObject);
					}

					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2);
						vObject = (VectorTree) vObject.GetParent();
					}

					// if both s1Tag and s2Tag are populated, store the field
					// into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String(getStringBetweenThisAndThat(
								sData, "<" + sTag1 + ">", "</" + sTag2 + ">"));
						// out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

						// If the variable value contains a "CDATA", eliminate
						// it here
						if (sTag.indexOf("<![CDATA") != -1) {
							sTag = new String(sTag.substring(9,
									(sTag.length() - 3)));
						}

						vObject.addElement(sTag);
					}
				}
			}

			return vObject;
		} catch (Exception except) {
			throw except;
		}

	}

	private HttpURLConnection getHttpURLConnection(boolean bUseProxy,
			boolean bDebug) throws Exception {
		HttpURLConnection httpCon = null;
		URL myURL = getURL(bUseProxy, bDebug);
		httpCon = (HttpURLConnection) myURL.openConnection();

		if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
			System.out.println("Http error: " + httpCon.getResponseMessage());
			throw new Exception("Http error: " + httpCon.getResponseMessage());
		}
		return httpCon;
	}

	private URL getURL(boolean bUseProxy, boolean bDebug) throws Exception {
		if (sTaxonomyServer == null)
			throw new Exception("getURL  : no taxonomy server speficied");
		String sURL = sTaxonomyServer + sAPIcall;
		// api.Log.Log ("InvokeAPI.java : sURL [" + sURL + "]" );
		Enumeration e = htArguments.keys();

		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);

			if (!sKey.equals("api"))
				sURL = sURL + "&" + sKey + "="
						+ URLEncoder.encode(sValue, "UTF-8");
		}

		// System.out.println("URL: "+sURL);

		// Make a socket connection to the server

		URL myURL = null;

		if (!bUseProxy) {
			myURL = new URL(sURL);
		} else {
			myURL = new URL("http", // protocol,
					"172.16.101.110", // host name or IP of proxy server to use
					8080, // proxy port or �1 to indicate the default port for
							// the protocol
					sURL); // the original URL, specified in the �file�
							// parameter
		}

		if (bDebug) {
			System.out.println("API call: " + sURL);
		}

		return myURL;

	}

	private URL getURLPost(boolean bUseProxy, boolean bDebug) throws Exception {
		if (sTaxonomyServer == null)
			throw new Exception("getURL  : no taxonomy server speficied");
		String sURL = sTaxonomyServer + sAPIcall + "&SKEY=" + SessionKey;
		// api.Log.Log ("InvokeAPI.java : sURL [" + sURL + "]" );
		// System.out.println("URL: "+sURL);

		// Make a socket connection to the server

		URL myURL = null;

		if (!bUseProxy) {
			myURL = new URL(sURL);
		} else {
			myURL = new URL("http", // protocol,
					"172.16.101.110", // host name or IP of proxy server to use
					8080, // proxy port or �1 to indicate the default port for
							// the protocol
					sURL); // the original URL, specified in the �file�
							// parameter
		}

		if (bDebug) {
			System.out.println("API call: " + sURL);
		}

		return myURL;

	}

	// get locations of strings
	public static int getLocationOfNthOfThese(String s, String s1, int index1) {
		int currentPointer = -1;
		int incrementAmount = 0;

		for (int i = 0; i < index1; i++) {
			String curString = s.substring(currentPointer + 1);
			incrementAmount = curString.indexOf(s1);

			if (incrementAmount == -1) {
				return -1;
			} else {
				currentPointer += incrementAmount + 1;
			}
		}
		return currentPointer;
	}

	public static String removeChar(String s, char c) {
		String r = "";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}
		return r;
	}

	// string parsing routine
	public static String getStringBetweenThisAndThat(String s, String s1,
			String s2) {
		int index1 = 1;
		int index2 = 1;
		String returnStr = null;

		int startLoc = 0;
		if (index1 > 0) {
			startLoc = getLocationOfNthOfThese(s, s1, index1) + 1;
			if (startLoc == -1) {
				return "";
			}
		}

		if (index2 > 0) {
			int endLoc = 0;
			int totalLen = startLoc + s1.length();
			if (totalLen == -1) {
				return "";
			}

			endLoc = getLocationOfNthOfThese(s, s2, index2);
			if (endLoc == -1) {
				returnStr = s.substring(startLoc + s1.length() - 1);
			} else {
				try {
					returnStr = s.substring(startLoc + s1.length() - 1, endLoc);
				} catch (Exception e) {
					returnStr = "";
				}
			}
		} else {
			returnStr = s.substring(startLoc + 1, s1.length());
		}

		return returnStr;
	}
}