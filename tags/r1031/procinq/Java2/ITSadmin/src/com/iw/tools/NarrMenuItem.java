/*
 * User: indraweb
 * Created by IntelliJ IDEA.
 * Date: May 24, 2005
 * Time: 10:37:12 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.tools;

import javax.swing.*;

public class NarrMenuItem extends JMenuItem {
    private String NarrativeID;
    private String NarrativeName;

    public NarrMenuItem() { super(); }
    public NarrMenuItem(String ID, String Name) {
        super(Name);
        NarrativeID = ID;
        NarrativeName = Name;
    }

    public String getNarrativeID() { return NarrativeID; }
    public String getNarrativeName() { return NarrativeName; }
}
