package com.iw.ui;

// package declaration here

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.tools.ImageUtils;

public class Preferences extends OculusBox implements ActionListener {

    // all
    protected JButton okButton;
    protected JButton cancelButton;

    // general
    protected JTextField documentText;
    protected JTextField abstractText;
    protected JTextField wordsText;
    protected JCheckBox dynamicCheck;
    protected JCheckBox writeToLog;
    protected JCheckBox lockCheck;
    protected JCheckBox sameLineStyles;
    protected JCheckBox ruleOneCheck;
    protected JCheckBox HKCheck;
    protected JCheckBox lingpipeCheck;
    protected JCheckBox capitalCheck;
    protected JTextField prefixText;
    protected JComboBox corpusMTbox;

    // GUi prefs
    protected JCheckBox dndCheck;
    protected JCheckBox treCheck;
    protected JCheckBox tchCheck;
    protected JCheckBox joiCheck;

    // corpus availability
    protected JList availableList;
    protected JButton leftArrowButton;
    protected JButton rightArrowButton;
    protected JList selectedList;
    protected JButton btnCacheFile;

    // concept alerts
    protected JButton buttonRed;
    protected JLabel redLabel;
    protected JButton buttonYellow;
    protected JLabel yellowLabel;
    protected JButton greenButton;
    protected JLabel greenLabel;
    protected JTextField greenText;
    protected JTextField yellowText;
    protected JTextField redText;

    // corpus ingestion
    protected JLabel includeNumbersLabel;
    protected JTextField includeNumbersText;
    protected JLabel phraseLenMinLabel;
    protected JLabel numParentTitleTermAsSigsLabel;
    protected JTextField numParentTitleTermAsSigsText;
    protected JLabel phraseLenMaxLabel;
    protected JLabel wordFreqMaxForSigLabel;
    protected JTextField wordFreqMaxForSigText;
    protected JLabel delimitersLabel;
    protected JTextField phraseLenMinText;
    protected JTextField phraseLenMaxText;
    protected JTextField delimitersText;

    private JInternalFrame frame;
    private ITSAdministrator ITSframe;
    private User u;

    public Preferences(ITSAdministrator ITSframe) {
        u = ITS.getUser(); this.ITSframe = ITSframe;

        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        frame = new JInternalFrame("Edit Preferences", false, true, true, false);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setFrameIcon(ITSframe.iIndraweb);

        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        documentText = new JTextField("1");
        documentText.setToolTipText("To enable document classification, enter the document weighting."+
            "If document text is not used, enter 0.");
        documentText.setColumns(5);
        abstractText = new JTextField("0");
        abstractText.setToolTipText("To enable abstract classification, enter the abstract weighting."+
            "If abstract text is not used, enter 0.");
        abstractText.setColumns(5);
        wordsText = new JTextField("50");
        wordsText.setToolTipText("Set the character threshold for node rollup.");
        wordsText.setColumns(5);

        prefixText = new JTextField("");
        prefixText.setToolTipText("If a path prefix is required to open documents, enter it here.");
        prefixText.setColumns(10);

        dynamicCheck = new JCheckBox();
        dynamicCheck.setToolTipText("Check this box to run thesaurus search dynamically with each letter typed.");

        dndCheck = new JCheckBox();
        dndCheck.setToolTipText("Check this box to enable prompts on drag and drop functions.");

        treCheck = new JCheckBox();
        treCheck.setToolTipText("Check this box to enable prompts on topic removal.");

        tchCheck = new JCheckBox();
        tchCheck.setToolTipText("Check this box to enable prompts on topic changes.");

        joiCheck = new JCheckBox();
        joiCheck.setToolTipText("Check this box to enable promps to topic merges.");

        writeToLog = new JCheckBox();
        writeToLog.setToolTipText("Check this box to activate client side logging.");

        lockCheck = new JCheckBox();
        lockCheck.setToolTipText("Check this box to restrict user functions.");

        sameLineStyles = new JCheckBox();
        sameLineStyles.setToolTipText("Allow style changes on the same line to create different entries.");

        ruleOneCheck = new JCheckBox();
        ruleOneCheck.setToolTipText("Enable new topic on ingestion if line has Capitals and whitespace.");

        HKCheck = new JCheckBox("Dr. Kon algorithm");
        HKCheck.setToolTipText("Enable Dr. Kon algorithm for detecting phrases.");

        lingpipeCheck = new JCheckBox("Chunker");
        lingpipeCheck.setToolTipText("Enable the Chunker phrase detection algorithm.");

        capitalCheck = new JCheckBox("Capitalization");
        capitalCheck.setToolTipText("Enable phrase extraction on capitalization");

        corpusMTbox = new JComboBox();
        corpusMTbox.setToolTipText("Set display for corpus management thesaurus box.");
        corpusMTbox.addItem("Full");
        corpusMTbox.addItem("Partial");
        corpusMTbox.addItem("None");

        if (u != null) {
            documentText.setText(u.getDocumentWeight()+"");
            abstractText.setText(u.getAbstractWeight()+"");
            wordsText.setText(u.getCharacterThreshold()+"");
            dynamicCheck.setSelected(u.useDynamicThesaurus());
            writeToLog.setSelected(u.writeToLog());
            lockCheck.setSelected(u.lockFunctions());
            sameLineStyles.setSelected(u.sameLines());
            prefixText.setText(u.getPathPrefix());
            dndCheck.setSelected(u.PromptForDragAndDrop());
            treCheck.setSelected(u.PromptForTopicRemoval());
            tchCheck.setSelected(u.PromptForTopicChange());
            joiCheck.setSelected(u.PromptForJoinChange());
            ruleOneCheck.setSelected(u.isRuleOneSet());
            corpusMTbox.setSelectedIndex(u.getCorpusMTLevel());
            HKCheck.setSelected(u.isHKSet());
            lingpipeCheck.setSelected(u.isLingpipeSet());
            capitalCheck.setSelected(u.isCapitalSet());
        }

        okButton = new JButton("OK");
        okButton.setToolTipText("Click OK to accept preference changes.");
        okButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);

        Vector vCorpo = new Vector();
        DefaultListModel dlmA = new DefaultListModel();
        DefaultListModel dlmS = new DefaultListModel();

        try { vCorpo = ITSframe.its.getCorpora(); }
        catch (Exception e) { e.printStackTrace(System.out); }

        for (int i = 0; i < vCorpo.size(); i++) {
            Corpus c = (Corpus) vCorpo.elementAt(i);
            if (u.usingCorpus(c.getID())) { dlmS.addElement(c); }
            else { dlmA.addElement(c); }
        }

        availableList = new JList(dlmA);
        availableList.setToolTipText("This is the list of taxonomies that are available for use.");

        leftArrowButton = new JButton(ImageUtils.getImage(ITSframe, "itsimages/left.gif"));
        leftArrowButton.setToolTipText("Remove a taxonomy from the selection list.");
        leftArrowButton.addActionListener(this);

        rightArrowButton = new JButton(ImageUtils.getImage(ITSframe, "itsimages/right.gif"));
        rightArrowButton.setToolTipText("Add a taxonomy to the selection list.");
        rightArrowButton.addActionListener(this);

        selectedList = new JList(dlmS);
        selectedList.setToolTipText("This is the list of taxonomies that will be classified against.");
        
        btnCacheFile= new JButton("Save cache file");
        btnCacheFile.addActionListener(this);

        buttonRed = new JButton("jBut");
        buttonRed.setBackground(Color.red);
        buttonRed.setForeground(Color.red);
        buttonRed.setToolTipText("Critical concept threshold percentage.");

        redLabel = new JLabel("Critical concept alert threshold:");
        redLabel.setToolTipText("Critical concept threshold percentage.");

        buttonYellow = new JButton("JBut");
        buttonYellow.setBackground(Color.yellow);
        buttonYellow.setForeground(Color.yellow);
        buttonYellow.setToolTipText("Severe concept threshold percentage.");

        yellowLabel = new JLabel("Severe concept alert threshold:");
        yellowLabel.setToolTipText("Severe concept threshold percentage.");

        greenButton = new JButton("JBut");
        greenButton.setBackground(Color.green);
        greenButton.setForeground(Color.green);
        greenButton.setToolTipText("Warning concept threshold percentage.");

        greenLabel = new JLabel("Minor concept alert threshold:");
        greenLabel.setToolTipText("Warning concept threshold percentage.");

        redText = new JTextField("75");
        redText.setColumns(5);
        redText.setToolTipText("Critical concept threshold percentage.");
        redText.setMinimumSize(new Dimension(5,redText.getMinimumSize().height));

        yellowText = new JTextField("50");
        yellowText.setColumns(5);
        yellowText.setToolTipText("Severe concept threshold percentage.");
        yellowText.setMinimumSize(new Dimension(5,yellowText.getMinimumSize().height));

        greenText = new JTextField("25");
        greenText.setColumns(5);
        greenText.setToolTipText("Minor concept threshold percentage.");
        greenText.setMinimumSize(new Dimension(5,greenText.getMinimumSize().height));

        includeNumbersLabel = new JLabel("Include Numbers:");
        includeNumbersLabel.setToolTipText("Set to 'true' to admit numbers are signature terms.");

        includeNumbersText =
                new JTextField(u.getIncludeNumbers() == null ? "false" : u.getIncludeNumbers()+"");
        includeNumbersText.setToolTipText("Set to 'true' to admit numbers are signature terms.");

        phraseLenMinLabel = new JLabel("Minimum Phrase Length:");
        phraseLenMinLabel.setToolTipText("Minimum number of words that can constitute a phrase.");

        numParentTitleTermAsSigsLabel = new JLabel("Parent Title Terms:");
        numParentTitleTermAsSigsLabel.setToolTipText("Number of parent signature terms to include as part of a signature.");

        numParentTitleTermAsSigsText =
                new JTextField(u.getNumParentTitleTermAsSigs() == null ? "3" : u.getNumParentTitleTermAsSigs()+"");
        numParentTitleTermAsSigsText.setToolTipText("Number of parent signature terms to include as part of a signature.");

        phraseLenMaxLabel = new JLabel("Maximum Phrase Length:");
        phraseLenMaxLabel.setToolTipText("Maximum number of words that can constitute a phrase.");

        wordFreqMaxForSigLabel = new JLabel("Word Freq (Max):");
        wordFreqMaxForSigLabel.setToolTipText("Maximum word frequency to be allowed as a signature.");

        wordFreqMaxForSigText =
                new JTextField(u.getWordFreqMaxForSig() == null ? "75" : u.getWordFreqMaxForSig()+"");
        wordFreqMaxForSigText.setToolTipText("Maximum word frequency to be allowed as a signature.");

        delimitersLabel = new JLabel("Delimiters:");
        delimitersLabel.setToolTipText("Delimiter characters for article word parsing.");

        phraseLenMinText =
                new JTextField(u.getPhraseLenMin() == null ? "2" : u.getPhraseLenMin()+"");
        phraseLenMinText.setToolTipText("Minimum number of words that can constitute a phrase.");

        phraseLenMaxText =
                new JTextField(u.getPhraseLenMax() == null ? "4" : u.getPhraseLenMax()+"");
        phraseLenMaxText.setToolTipText("Maximum number of words that can constitute a phrase.");

        delimitersText =
                new JTextField(u.getDelimiters() == null ? "\"?!/, &+.:;#*?\"" : u.getDelimiters());
        delimitersText.setToolTipText("Delimiter characters for article word parsing.");

        if (u != null) {
            redText.setText(u.getCriticalThreshold()+"");
            yellowText.setText(u.getSevereThreshold()+"");
            greenText.setText(u.getMinorThreshold()+"");
        }

        layout.addSpace(10);
        layout.nestTabbedPane();
        {
            layout.nestBoxAsTab("General", OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.HORIZONTAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestGrid(2,15);
                            {
                                layout.add(new JLabel("Document classification weight:"));
                                layout.add(documentText, OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                                layout.add(new JLabel("Abstract classification weight:"));
                                layout.add(abstractText, OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                                layout.add(new JLabel("Rollup character threshold:"));
                                layout.add(wordsText, OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                                layout.addSpace(5);
                                layout.addSpace(5);
                                layout.add(new JLabel("Run thesaurus search dynamic:"));
                                layout.add(dynamicCheck);
                                layout.addSpace(5);
                                layout.addSpace(5);
                                layout.add(new JLabel("Activate client side logging:"));
                                layout.add(writeToLog);
                                layout.addSpace(5);
                                layout.addSpace(5);
                                layout.add(new JLabel("Lock administrative functions:"));
                                layout.add(lockCheck);
                                layout.addSpace(5);
                                layout.addSpace(5);
                                layout.add(new JLabel("Allow different styles per line:"));
                                layout.add(sameLineStyles);
                                layout.addSpace(5);
                                layout.addSpace(5);
                                layout.add(new JLabel("Corpus-Thesaurus box display:"));
                                layout.add(corpusMTbox);
                                layout.addSpace(5);
                                layout.addSpace(5);
                                layout.add(new JLabel("Open document file prefix:"));
                                layout.add(prefixText, OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.nestBoxAsTab("Prompts", OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.HORIZONTAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.nestGrid(2,8);
                        {
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(new JLabel("Prompt on drag and drop? "));
                            layout.add(dndCheck);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(new JLabel("Prompt on topic removal? "));
                            layout.add(treCheck);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(new JLabel("Prompt to save on topic change? "));
                            layout.add(tchCheck);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(new JLabel("Prompt to save on merge topics? "));
                            layout.add(joiCheck);
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.nestBoxAsTab("Classify", OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.addSpace(10);
                        layout.add(new JLabel(" Available Taxonomies"));
                        layout.add(new JScrollPane(availableList));
                        layout.parent();
                    }
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.addSpace(10);
                        layout.add(leftArrowButton);
                        layout.addSpace(10);
                        layout.add(rightArrowButton);
                        layout.parent();
                    }
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.addSpace(10);
                        layout.add(new JLabel(" Selected Taxonomies"));
                        layout.add(new JScrollPane(selectedList));
                        layout.add(btnCacheFile);
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.parent();
            }
            layout.nestBoxAsTab("Alerts", OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestGrid(2,3);
                            {
                                layout.setGridCellJustification(OculusLayout.CENTER,OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL);
                                {
                                    layout.add(redLabel);
                                    layout.parent();
                                }
                                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(redText,OculusLayout.STRETCH_ONLY_TO_ALIGN,OculusLayout.NO_STRETCH);
                                    layout.parent();
                                }
                                layout.setGridCellJustification(OculusLayout.CENTER,OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL);
                                {
                                    layout.add(yellowLabel);
                                    layout.parent();
                                }
                                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(yellowText,OculusLayout.STRETCH_ONLY_TO_ALIGN,OculusLayout.NO_STRETCH);
                                    layout.parent();
                                }
                                layout.setGridCellJustification(OculusLayout.CENTER,OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL);
                                {
                                    layout.addSpace(10);
                                    layout.add(greenLabel);
                                    layout.parent();
                                }
                                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(greenText,OculusLayout.STRETCH_ONLY_TO_ALIGN,OculusLayout.NO_STRETCH);
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.nestBoxAsTab("Ingestion", OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                    {
                        layout.addSpace(10);
                        layout.nestGrid(2,16);
                        {
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(includeNumbersLabel);
                            layout.add(includeNumbersText,OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(phraseLenMinLabel);
                            layout.add(phraseLenMinText,OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(numParentTitleTermAsSigsLabel);
                            layout.add(numParentTitleTermAsSigsText,OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(phraseLenMaxLabel);
                            layout.add(phraseLenMaxText,OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(wordFreqMaxForSigLabel);
                            layout.add(wordFreqMaxForSigText,OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
                            layout.addSpace(5);
                            layout.addSpace(5);
                            layout.add(new JLabel("Enable ingestion rule capitalization with whitespace."));
                            layout.add(ruleOneCheck);
                            layout.add(new JLabel("Selected ingestion phrase parsing routines: "));
                            layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                            {
                                layout.add(HKCheck);
                                layout.add(lingpipeCheck);
                                layout.add(capitalCheck);
                                layout.parent();
                            }
                            //layout.add(delimitersLabel);
                            //layout.add(delimitersText);
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(okButton);
            layout.addSpace(10);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(10);

        frame.setLocation(60, 60);
        ITSframe.jdp.add(frame);

        frame.getRootPane().setDefaultButton(okButton);
        frame.setContentPane(this);
        frame.show();

        frame.setContentPane(this);
        frame.pack();
        try {
            this.frame.setMaximum(true);
          }
          catch (PropertyVetoException e1) {
            e1.printStackTrace();
          }
        frame.show();
    }

    public String getCorpusList() {
        String s = "";

        for (int i = 0; i < selectedList.getModel().getSize(); i++) {
            if (i != 0) { s = s + ","; }
            Corpus c = (Corpus) selectedList.getModel().getElementAt(i);
            s = s + c.getID();
        }
        if (selectedList.getModel().getSize() == 0) { s = "-1"; }

        return s;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(okButton)) {
            try {
                int dweight = 0; int aweight = 0; int threshold = 0;
                int critical = 0; int severe = 0; int minor = 0; int level = 0;
                try {
                    dweight = new Integer(documentText.getText()).intValue();
                    aweight = new Integer(abstractText.getText()).intValue();
                    threshold = new Integer(wordsText.getText()).intValue();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(ITSframe, "Your user preferences could not be saved. "+
                      "Abstract and document weight must be an integer value.", "Error", JOptionPane.NO_OPTION);

                    return;
                }

                try {
                    critical = new Integer(redText.getText()).intValue();
                    severe = new Integer(yellowText.getText()).intValue();
                    minor = new Integer(greenText.getText()).intValue();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(ITSframe, "Your user preferences could not be saved. "+
                      "Concept alert thresholds must be integer values.", "Error", JOptionPane.NO_OPTION);

                    return;
                }

                if ((dweight <= 0) && (aweight <= 0)) {
                    JOptionPane.showMessageDialog(ITSframe, "Your user preferences could not be saved. "+
                      "Either abstract or document weight must have a value greater than 0.", "Error", JOptionPane.NO_OPTION);

                    return;
                }

                if ((!includeNumbersText.getText().toLowerCase().equals("false")) &&
                    (!includeNumbersText.getText().toLowerCase().equals("true"))) {
                    JOptionPane.showMessageDialog(ITSframe, "Your user preferences could not be saved. "+
                      "The include numbers field must be either true or false.", "Error", JOptionPane.NO_OPTION);

                    return;
                }

                /*
                if ((corpusMTbox.getSelectedIndex() == 0) && (ITSframe.its.getServerVersion().equals("ProcinQ Server 3.140"))) {
                    JOptionPane.showMessageDialog(ITSframe, "Full thesaurus mode is not available in this version of the server. "+
                       "This preference has been reset to partial.", "Information", JOptionPane.NO_OPTION);
                    corpusMTbox.setSelectedIndex(1);
                } */

                u.setCorpusMTlevel(corpusMTbox.getSelectedIndex());
                u.setDocumentWeight(dweight);
                u.setAbstractWeight(aweight);
                u.setCharacterThreshold(threshold);
                u.useDynamicThesaurus(dynamicCheck.isSelected());
                u.writeToLog(writeToLog.isSelected());
                u.lockFunctions(lockCheck.isSelected());
                u.setSameLineStyles(sameLineStyles.isSelected());
                u.setRuleOne(ruleOneCheck.isSelected());
                u.setCorporaToUse(getCorpusList());

                u.setPromptDragAndDrop(dndCheck.isSelected());
                u.setPromptTopicRemoval(treCheck.isSelected());
                u.setPromptTopicChange(tchCheck.isSelected());
                u.setPromptJoinChange(joiCheck.isSelected());

                u.setCriticalThreshold(critical);
                u.setSevereThreshold(severe);
                u.setMinorThreshold(minor);

                u.setIncludeNumbers(includeNumbersText.getText());
                try {
                    u.setNumParentTitleTermAsSigs(Integer.parseInt(numParentTitleTermAsSigsText.getText()));
                    u.setWordFreqMaxForSig(Integer.parseInt(wordFreqMaxForSigText.getText()));
                    u.setPhraseLenMin(Integer.parseInt(phraseLenMinText.getText()));
                    u.setPhraseLenMax(Integer.parseInt(phraseLenMaxText.getText()));
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(ITSframe, "Your user preferences could not be saved. "+
                      "One or more fields on the Ingestion tab contain an illegal non-numeric value.", "Error", JOptionPane.NO_OPTION);

                    return;
                }

                u.setPathPrefix(prefixText.getText());

                // booleans for ingestion phrasing
                u.setHK(HKCheck.isSelected());
                u.setLP(lingpipeCheck.isSelected());
                u.setLPC(capitalCheck.isSelected());

                // if write to log has been invoked, turn it on
                if ( u.writeToLog() ) ITS.activateLogging();
                else ITS.deactivateLogging();

                try { u.serializeObject("user.data"); }
                catch (Exception ex) {
                    JOptionPane.showMessageDialog(ITSframe, "Your user preferences could not be saved. "+
                      "Reason: "+ex.getMessage(), "Error", JOptionPane.NO_OPTION);
                } finally { frame.dispose(); }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(ITSframe, "One or more of your parameters have improper syntax.", "Error", JOptionPane.NO_OPTION);
                return;
            }
        } else if (e.getSource().equals(cancelButton)) { frame.dispose();
        } else if (e.getSource().equals(leftArrowButton)) {
            if (selectedList.getSelectedIndex() == -1) {
                JOptionPane.showMessageDialog(ITSframe, "There are no taxonomies selected.", "Error", JOptionPane.NO_OPTION);
                return;
            }

            DefaultListModel dlm = (DefaultListModel) availableList.getModel();
            for (int i = 0; i < selectedList.getSelectedIndices().length; i++) {
                int index = selectedList.getSelectedIndices()[i];

                Corpus c = (Corpus) selectedList.getModel().getElementAt(index);
                dlm.addElement(c);
            }
            availableList.setModel(dlm);

            int length = selectedList.getSelectedIndices().length;
            DefaultListModel dlm2 = (DefaultListModel) selectedList.getModel();
            for (int i = length-1; i >= 0; i--) {
                int index = selectedList.getSelectedIndices()[i];

                dlm2.remove(index);
            }
            selectedList.setModel(dlm2);
        } else if (e.getSource().equals(rightArrowButton)) {
            if (availableList.getSelectedIndex() == -1) {
                JOptionPane.showMessageDialog(ITSframe, "There are no taxonomies selected.", "Error", JOptionPane.NO_OPTION);
                return;
            }

            DefaultListModel dlm = (DefaultListModel) selectedList.getModel();
            for (int i = 0; i < availableList.getSelectedIndices().length; i++) {
                int index = availableList.getSelectedIndices()[i];

                Corpus c = (Corpus) availableList.getModel().getElementAt(index);
                dlm.addElement(c);
            }
            selectedList.setModel(dlm);

            int length = availableList.getSelectedIndices().length;
            DefaultListModel dlm2 = (DefaultListModel) availableList.getModel();
            for (int i = length-1; i >= 0; i--) {
                int index = availableList.getSelectedIndices()[i];

                dlm2.remove(index);
            }
            availableList.setModel(dlm2);
        }else if(e.getSource().equals(btnCacheFile)){
        	Corpus c;
        	Object seleccion=null;
        	seleccion=selectedList.getSelectedValue();
        	
        	if(seleccion==null){
        		JOptionPane.showMessageDialog(this, "Select taxonomy");
        		return;
        	}
        	c=(Corpus)seleccion;
        	System.out.println(c.getID());
            try {
    			URL url = new URL(
    					"http://10.3.8.203:8080/webdav/corpus_source/SigWordCountsFile_"+c.getID()+"_nonStem.txt");
    			URLConnection urlCon = url.openConnection();
    			System.out.println(urlCon.getContentType());
    			InputStream is= urlCon.getInputStream();
    			FileOutputStream fos = null;  
    			javax.swing.JFileChooser jF1= new javax.swing.JFileChooser();
    			String ruta = "";
    			try{
    				int res=jF1.showSaveDialog(this);
    				if(res==jF1.APPROVE_OPTION){
    					ruta = jF1.getSelectedFile().getAbsolutePath();
    					fos= new FileOutputStream(ruta);
            			byte[] array = new byte[1000]; // buffer temporal de lectura.
            			int leido = is.read(array);
            			while (leido > 0) {
            				fos.write(array, 0, leido);
            				leido = is.read(array);
            			}
            			JOptionPane.showMessageDialog(this, "Archive saved successfully: "+ruta);
    				}else if(res==jF1.CANCEL_OPTION){
    				 return;
    				}
    			}catch (Exception ex){
    				ex.printStackTrace();
    			}
    			
    			is.close();
    			fos.close();
    		} catch (Exception ex) {
    			ex.printStackTrace();
    			JOptionPane.showMessageDialog(this, "Taxonomy Not Found");
    			
    		}
        }
    }
}

