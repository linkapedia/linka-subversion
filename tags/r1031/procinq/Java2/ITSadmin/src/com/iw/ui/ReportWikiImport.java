package com.iw.ui;

import com.iw.system.ITS;
import com.iw.system.WikipediaReportModel;
import com.iw.system.WrapperListReport;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringReader;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.dom4j.QName;

public class ReportWikiImport extends JInternalFrame implements ActionListener {

    private JTable table;
    private JTextField txtRowNum;
    private JLabel lblRowNum;
    private JButton btnReport;
    private DefaultTableModel modelTable;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");

    public ReportWikiImport(ITSAdministrator frame) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);
        setSize(700, 400);//330
        setTitle("Report Wiki Import");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(8, 12, 670, 279);
        getContentPane().add(scrollPane);

        table = new JTable();
        modelTable = new DefaultTableModel(
                new Object[][]{},
                new String[]{
            "REQUESTID", "DATE", "START POINT", "CORPUS", "DEPTH", "STATUS"
        });
        table.setModel(modelTable);

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
                setCursor(cursor);
                tableMouseClicked(evt);
                cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                setCursor(cursor);
            }
        });

        scrollPane.setViewportView(table);
        lblRowNum = new JLabel("Number rows to get");
        lblRowNum.setBounds(30, 320, 117, 25);
        txtRowNum = new JTextField(30);
        txtRowNum.setBounds(150, 320, 117, 25);
        btnReport = new JButton("Run");
        btnReport.setBounds(300, 320, 117, 25);
        btnReport.addActionListener(this);
        getContentPane().add(lblRowNum);
        getContentPane().add(txtRowNum);
        getContentPane().add(btnReport);
        //getContentPane().add(scrollPane);

        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }

    private void loadData() {
        try {
            org.dom4j.Document nodes = ITSframe.its.getReportWikipedia(txtRowNum.getText());
            JAXBContext jc = JAXBContext.newInstance(WrapperListReport.class);
            StringReader reader = new StringReader(nodes.getRootElement().element(new QName("REPORT")).asXML());

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            WrapperListReport wikiReport = (WrapperListReport) unmarshaller.unmarshal(reader);

            modelTable = new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                "REQUESTID", "DATE", "START POINT", "CORPUS", "DEPTH", "STATUS"
            }) {
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };

            table.setModel(modelTable);

            if (wikiReport.getList() != null) {
                Date date;
                for (WikipediaReportModel node : wikiReport.getList()) {
                    date = new Date(Long.parseLong(node.getTimestamp()));
                    modelTable.addRow(new Object[]{node.getRequestId(), date.toString(), node.getWikipediaStartPoint(), node.getCorpus(), node.getDepth(), node.getStatus()});
                }
            } else {
                JOptionPane.showMessageDialog(this, "Not found process");
            }
        } catch (Exception e) {
            System.out.println("Error reading wikipedia report" + e.getMessage());
        }
    }

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            //String nodeid = null;
            int rowindex = table.getSelectedRow();
            //nodeid = table.getModel().getValueAt(rowindex, 0).toString();
            //editNode(nodeid);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnReport)) {
            loadData();
        }
    }
}
