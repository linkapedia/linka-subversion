// package declaration here
package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.oculustech.layout.*;
import com.iw.system.ITS;
import com.iw.system.ThesaurusRelationship;
import com.iw.system.NotAuthorized;
import com.iw.ui.ITSAdministrator;
import com.iw.ui.ThesaurusPane;

public class NewThesaurusTerm extends JPanel implements ActionListener {
    protected JLabel pleaseEnterTheThesaurusTermAnd;
    protected JLabel thesaurusTerm;
    protected JTextField textThesaurusTerm;
    protected JLabel relatedTerm;
    protected JTextField textRelatedTerm;
    protected JLabel relationship;
    protected JComboBox listRelationships;
    protected JButton createButton;
    protected JButton cancelButton;

    private JInternalFrame MDIframe = null;
    private ITS its = null;
    private ITSAdministrator ITSframe = null;
    private ThesaurusPane tp = null;

    public NewThesaurusTerm(ITSAdministrator frame, ThesaurusPane pane) {
        OculusBox ob = new OculusBox();
        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        JDesktopPane jdesktoppane = frame.jdp;

        // the pop up thesaurus term creation is an internal MDI frame
        MDIframe = new JInternalFrame("Create Anchor Term", false, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(375, 160);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;
        tp = pane;

        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        pleaseEnterTheThesaurusTermAnd = new JLabel("Please enter the anchor term and relationship.");
        pleaseEnterTheThesaurusTermAnd.setFont(Font.decode("SansSerif-BOLD-11"));

        thesaurusTerm = new JLabel("Anchor Term:");
        thesaurusTerm.setFont(Font.decode("SansSerif-11"));

        textThesaurusTerm = new JTextField();

        relatedTerm = new JLabel("Related Term:");
        relatedTerm.setFont(Font.decode("SansSerif-11"));

        textRelatedTerm = new JTextField();

        relationship = new JLabel("Relationship:");
        relationship.setFont(Font.decode("SansSerif-11"));

        Vector v = new Vector();
        v.add(new ThesaurusRelationship("1", "Similar term"));
        v.add(new ThesaurusRelationship("2", "Broader term"));
        v.add(new ThesaurusRelationship("3", "Narrower term"));

        listRelationships = new JComboBox(v);
        listRelationships.setMaximumRowCount(3);
        listRelationships.setMinimumSize(new Dimension(300, listRelationships.getMinimumSize().height));

        createButton = new JButton("Create");
        createButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.add(pleaseEnterTheThesaurusTermAnd);
                layout.addSpace(10);
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(thesaurusTerm);
                    layout.add(textThesaurusTerm);
                    layout.parent();
                }
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(relatedTerm);
                    layout.add(textRelatedTerm);
                    layout.parent();
                }
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(relationship);
                    layout.add(listRelationships);
                    layout.parent();
                }
                layout.parent();
            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(createButton);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        setVisible(true);
        jdesktoppane.add(MDIframe);

        textThesaurusTerm.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                textThesaurusTerm.requestFocus();
                textThesaurusTerm.removeFocusListener(this);
            }
        });

        MDIframe.getRootPane().setDefaultButton(createButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();

        textThesaurusTerm.requestFocus();
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent event) {
        MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {

            if (event.getSource() == cancelButton) {
                MDIframe.dispose();
            }
            if (event.getSource() == createButton) {
                if (textThesaurusTerm.getText().equals("")) {
                    JOptionPane.showMessageDialog(textThesaurusTerm, "Sorry, the anchor term field is empty.");
                    return;
                }
                if (textRelatedTerm.getText().equals("")) {
                    JOptionPane.showMessageDialog(textThesaurusTerm, "Sorry, the related term field is empty.");
                    return;
                }
                tp.addThesaurusRelationshipExternal(textThesaurusTerm.getText(),
                        textRelatedTerm.getText(),
                        ((ThesaurusRelationship) listRelationships.getSelectedItem()).getID());
                MDIframe.dispose();
            }
        } catch (NotAuthorized ex) { MDIframe.dispose();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }
}
