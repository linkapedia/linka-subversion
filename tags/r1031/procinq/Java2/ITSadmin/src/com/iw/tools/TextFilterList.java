package com.iw.tools;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JTextField;

public class TextFilterList extends JTextField {
	private static final long serialVersionUID = 1244434926365652344L;
	private DefaultListModel modelList;
	private ArrayList<Object> arrayTaxonomies;

	public TextFilterList(DefaultListModel modelList) {
		this.arrayTaxonomies = new ArrayList<Object>();
		for (int i = 0; i < modelList.getSize(); i++) {
			this.arrayTaxonomies.add(modelList.get(i));
		}
		this.modelList = modelList;
		addKeyListener(new key());
	}

	private void removeAllItemFromList() {
		this.modelList.removeAllElements();
	}

	private void updateList() {
		ArrayList<Object> arrayCoincidence = null;
		arrayCoincidence = new ArrayList<Object>();
		removeAllItemFromList();
		if (getText().length() > 0) {
			for (Object c : this.arrayTaxonomies) {
				String name = c.toString().trim();
				if (name.length() >= getText().length()) {
					if (name.substring(0, getText().length()).equalsIgnoreCase(
							getText())) {
						arrayCoincidence.add(c);
					}
				}

			}

			for (Object c : arrayCoincidence){
				this.modelList.addElement(c);
			}
		} else {
			for (Object c : arrayTaxonomies){
				this.modelList.addElement(c);
			}
		}
	}

	class key implements KeyListener {
		key() {
		}

		public void keyPressed(KeyEvent arg0) {
		}

		public void keyReleased(KeyEvent arg0) {
			TextFilterList.this.updateList();
		}

		public void keyTyped(KeyEvent arg0) {
		}
	}
}
