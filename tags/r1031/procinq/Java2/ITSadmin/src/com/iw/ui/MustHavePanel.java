package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.ui.ITSAdministrator;

public class MustHavePanel extends JPanel implements ActionListener {
    protected JLabel topicName2;
    protected JLabel operation;
    protected ButtonGroup btnGroup;
    protected JRadioButton radioType;
    protected JRadioButton radioType2;
    protected JCheckBox checkUNT;
    protected JLabel labelUPT;
    protected JCheckBox checkUPT;
    protected JCheckBox checkMode;
    protected JCheckBox useRegex;
    protected ButtonGroup groupOptionsCharacterGroup;
    protected JRadioButton removeAllInCharacterGroup;
    protected JRadioButton useAllInCharacterGroup;
    protected JTextField textTerm;
    protected JButton buttonApply;
    protected JButton buttonCancel;

    private JInternalFrame MDIframe;
    private ITSAdministrator itsadmin;
    private ITS its;
    private ITSTreeNode Node;

    public MustHavePanel(ITSAdministrator itsFrame, ITSTreeNode n) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        its = itsFrame.its; Node = n; itsadmin = itsFrame;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Must Have Panel", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(600, 250);
        MDIframe.setBackground(Color.lightGray);
        MDIframe.setResizable(false);

        MDIframe.setFrameIcon(itsFrame.iIndraweb);

        
        JDesktopPane jdesktoppane = itsFrame.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        topicName2 = new JLabel(n.get("NODETITLE"));
        topicName2.setFont(Font.decode("MS Sans Serif-BOLD-11"));
        topicName2.setToolTipText("Topic to which this operation will be applied");

        operation = new JLabel("Operation");
        operation.setToolTipText("Choose the operation.");

        btnGroup = new ButtonGroup();
        
        radioType = new JRadioButton("Breadth");
        radioType.setToolTipText("Change all topics for the given depth level");

        radioType2 = new JRadioButton("Depth");
        radioType2.setSelected(true);
        
        btnGroup.add(radioType);
        btnGroup.add(radioType2);

        checkUNT = new JCheckBox();

        labelUPT = new JLabel("Use Parent Title");
        labelUPT.setToolTipText("");

        checkUPT = new JCheckBox();
        checkMode = new JCheckBox();
        checkMode.setSelected(true);
        useRegex = new JCheckBox();
        
        groupOptionsCharacterGroup = new ButtonGroup();
        removeAllInCharacterGroup = new JRadioButton("Default: remove all character group");
        useAllInCharacterGroup = new JRadioButton("use the content in characters group");
        
        groupOptionsCharacterGroup.add(removeAllInCharacterGroup);
        groupOptionsCharacterGroup.add(useAllInCharacterGroup);
        
        removeAllInCharacterGroup.setSelected(true);

        textTerm = new JTextField();
        textTerm.setText(n.get("NODETITLE"));
        textTerm.setColumns(50);

        buttonApply = new JButton("Apply");
        buttonApply.setToolTipText("Apply changes");
        buttonApply.addActionListener(this);

        buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.nestGrid(2,10);
                    {
                        layout.add(new JLabel("Topic Name"));
                        layout.add(topicName2);
                        layout.add(operation);
                        layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                        {
                            layout.add(radioType);
                            layout.add(radioType2);
                            layout.parent();
                        }
                        layout.add(new JLabel("Use Node Title"));
                        layout.add(checkUNT);
                        layout.add(labelUPT);
                        layout.add(checkUPT);
                        layout.add(new JLabel("Use Regex"));
                        layout.add(useRegex);
                        layout.add(new JLabel("Use Full Phrase"));
                        layout.add(checkMode);
                        layout.add(removeAllInCharacterGroup);
                        layout.add(useAllInCharacterGroup);
                        layout.add(new JLabel("Term"));
                        layout.add(textTerm);
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(buttonApply);
            layout.addSpace(10);
            layout.add(buttonCancel);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
    }

    public void openAtUpperCenter() {
        Dimension winsize = MDIframe.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        MDIframe.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(buttonCancel)) {
            MDIframe.dispose(); // close the window when the action is taken
            return;
        }

        String depth = null; String terms = null;
        if (radioType.isSelected()) depth = Node.get("DEPTHFROMROOT");
        if (!textTerm.equals("")) terms = textTerm.getText();
        
        //0 is a DEFault  parameter
        String choiceGroupOption = "0";
        if(useAllInCharacterGroup.isSelected()){
        	choiceGroupOption ="1";
        }

        try {
            String success = its.addMusthaveRecursive(Node.get("NODEID"), depth, checkUNT.isSelected(), checkUPT.isSelected(), checkMode.isSelected(), useRegex.isSelected(), terms,choiceGroupOption);
            JOptionPane.showMessageDialog(itsadmin, success, "Information", JOptionPane.NO_OPTION);
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            JOptionPane.showMessageDialog(itsadmin, "Could not add terms: "+ex.getMessage(), "Error", JOptionPane.NO_OPTION);
        }

        MDIframe.dispose(); // close the window when the action is taken

    }
}
