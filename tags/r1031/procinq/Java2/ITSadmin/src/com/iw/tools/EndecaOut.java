package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class EndecaOut extends ExportElement {
    public String corpusID = "";
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {

        corpusID = c.getID();
        if (ppbin != null) this.ppb = ppbin;

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { throw new Exception("No root node found for corpus "+corpusID); }

        File f = new File(c.getName().replaceAll(" ", "_")+"-endc.xml");
        if (f.exists()) { throw new Exception("Error: File: "+f.getAbsolutePath()+" already exists."); }

        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
        out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<external_dimensions>\r\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        writeNode(rootNode, server, out);

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, out);

        out.write("</external_dimensions>\r\n");

        out.close();

        return f;
    }

    private void writeNode(ITSTreeNode n, ITS server, Writer out) throws Exception {
        int depth = new Integer(n.get("DEPTHFROMROOT")).intValue();

        for (int i = 0; i < depth; i++) { out.write(" "); }
        out.write("<node name=\""+n.get("NODETITLE").trim()+"|"+ n.get("NODEID") + "\" id=\""+n.get("NODEID")+"\">\r\n");

        Vector vSignatures = server.getNodeSignatures(n);

        if (vSignatures.size() > 0) {
            for (int j = 0; j < vSignatures.size(); j++) {
                Signature s = (Signature) vSignatures.elementAt(j);
                for (int i = 0; i < depth; i++) { out.write(" "); }
                out.write("   <synonym name=\""+s.getWord().trim()+"\" weight=\""+s.getWeight()+"\" classify=\"true\" search=\"false\"/>\r\n");
            }
        }
        nodesProcessed++;
        if (ppb != null) {
            long perc = (nodesProcessed*100) / totalNodes;
            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
        }
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer out) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() > 0) {
            for (int i = 0; i < vNodes.size(); i++) {
                ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
                writeNode(n, server, out);

                buildChildren(server, n, out);
            }
        }

        int depth = new Integer(p.get("DEPTHFROMROOT")).intValue();

        for (int i = 0; i < depth; i++) { out.write(" "); }
        out.write("</node>\n");
    }

    private static String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method

    private String returnWords(String Phrase) {
        StringBuffer sb = new StringBuffer("");

        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (i > 0) sb.append(" | ");
            if (!commonWords.containsKey(sArr[i].toLowerCase())) sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
        }

        return sb.toString();
    }
}