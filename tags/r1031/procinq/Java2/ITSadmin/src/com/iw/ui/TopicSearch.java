package com.iw.ui;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.oculustech.layout.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;
import java.util.Hashtable;
import java.text.DecimalFormat;

import com.iw.tools.SortedList;
import com.iw.tools.JSortTable;
import com.iw.tools.DefaultSortTableModel;
import com.iw.tools.SwingWorker;

public class TopicSearch extends JPanel implements ActionListener, ListSelectionListener, MouseListener {
    public ITS its = null;
    public ITSAdministrator ITSframe = null;

    protected Component glass;
    protected JInternalFrame MDIframe;

    private JPanel openPanel;
    private boolean buildQuery = false;
    private DocumentSearch docFrame = null;

    // GUI tools
    protected JLabel instructionsLabel;
    protected JLabel taxonomyLabel;
    protected JLabel typeLabel;
    protected JLabel queryLabel;
    protected JButton searchButton;
    protected JButton deleteButton;
    protected JButton clearButton;
    protected JComboBox taxonomyDropDown;
    protected JRadioButton containRadio;
    protected JRadioButton startsRadio;
    protected JRadioButton endsRadio;
    protected JTextField queryText;
    protected JSortTable resultsTable;
    protected JTextArea queryTextArea;
    protected JButton buttonAnd;
    protected JButton buttonOr;
    protected JButton buttonAndNot;
    protected JButton buttonOrNot;

    public TopicSearch() {}
    public TopicSearch(ITSAdministrator frame) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Topic Search", true, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(855, 530);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        glass = MDIframe.getGlassPane();

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        buildTopicSearch(MDIframe, ob, false);

        queryTextArea.setVisible(false);
        queryTextArea.setEnabled(false);
        MDIframe.getRootPane().setDefaultButton(searchButton);
    }

    // use this constructor for topic search as a query builder
    public TopicSearch(ITSAdministrator frame, DocumentSearch dsframe) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Topic Search", true, true, true, true);
        MDIframe.setLocation(75, 75);
        MDIframe.setSize(675, 530);
        MDIframe.setBackground(Color.lightGray);

        its = frame.its;
        ITSframe = frame;
        docFrame = dsframe;

        glass = MDIframe.getGlassPane();
        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        buildQuery = true;

        buildTopicSearch(MDIframe, ob, true);

        buttonAnd.setVisible(true);
        buttonOr.setVisible(true);
        buttonAndNot.setVisible(true);
        buttonOrNot.setVisible(true);

        queryTextArea.setVisible(true);
        queryTextArea.setLineWrap(true);
        queryTextArea.setFont(Font.decode("MS Sans Serif-11"));

        MDIframe.getRootPane().setDefaultButton(searchButton);
    }

    public void buildTopicSearch(JInternalFrame MDIframe, OculusBox ob, boolean bQueryTextBox)
            throws Exception {
        JDesktopPane jdesktoppane = ITSframe.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        searchButton = new JButton("Search");
        searchButton.addActionListener(this);
        deleteButton = new JButton("Delete");
        deleteButton.addActionListener(this);
        clearButton = new JButton("Close");
        clearButton.addActionListener(this);

        taxonomyLabel = new JLabel("Taxonomy:");
        taxonomyLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        Vector v = its.getCorpora();
        v.addElement("All");
        taxonomyDropDown = new JComboBox(v);
        //taxonomyDropDown.setSize(10,30);
        taxonomyDropDown.setSelectedItem("All");

        typeLabel = new JLabel("Search Type:");
        typeLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        containRadio = new JRadioButton("Containing");
        startsRadio = new JRadioButton("Starts With");
        endsRadio = new JRadioButton("Ends With");
        startsRadio.setSelected(true);

        containRadio.addActionListener(this);
        startsRadio.addActionListener(this);
        endsRadio.addActionListener(this);

        queryLabel = new JLabel("Search Term:");
        queryLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        queryText = new JTextField();

        resultsTable = new JSortTable();
        //resultsTable.setRowSelectionAllowed(false);
        resultsTable.addMouseListener(this);

        queryTextArea = new JTextArea("", 5, 10);

        buttonAnd = new JButton("And");
        buttonAnd.addActionListener(this);
        buttonAnd.setFont(Font.decode("MS Sans Serif-11"));
        buttonAnd.setVisible(false);
        buttonOr = new JButton("Or");
        buttonOr.addActionListener(this);
        buttonOr.setFont(Font.decode("MS Sans Serif-11"));
        buttonOr.setVisible(false);
        buttonAndNot = new JButton("And Not");
        buttonAndNot.addActionListener(this);
        buttonAndNot.setFont(Font.decode("MS Sans Serif-11"));
        buttonAndNot.setVisible(false);
        buttonOrNot = new JButton("Or Not");
        buttonOrNot.addActionListener(this);
        buttonOrNot.setFont(Font.decode("MS Sans Serif-11"));
        buttonOrNot.setVisible(false);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestGrid(2, 5);
                                {
                                    layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.addSpace(10);
                                        layout.add(taxonomyLabel);
                                        layout.parent();
                                    }
                                    layout.add(taxonomyDropDown);
                                    layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.addSpace(10);
                                        layout.add(typeLabel);
                                        layout.parent();
                                    }
                                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.add(containRadio);
                                        layout.add(startsRadio);
                                        layout.add(endsRadio);
                                        layout.parent();
                                    }
                                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.addSpace(10);
                                        layout.add(queryLabel);
                                        layout.parent();
                                    }
                                    layout.add(queryText);
                                    layout.addSpace(1);
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(new JScrollPane(resultsTable));
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.addSpace(1);
                                    if (bQueryTextBox) {
                                        layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                        {
                                            layout.add(new JScrollPane(queryTextArea));
                                            layout.parent();
                                        }
                                    }
                                    layout.parent();
                                }
                                layout.addSpace(10);
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(searchButton);
                                        layout.addSpace(10);
                                        layout.add(deleteButton);
                                        layout.addSpace(10);
                                        layout.add(clearButton);
                                        layout.parent();
                                    }
                                    layout.addSpace(10);
                                    layout.parent();
                                }
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.add(buttonAnd);
                                    layout.addSpace(10);
                                    layout.add(buttonOr);
                                    layout.addSpace(10);
                                    layout.add(buttonAndNot);
                                    layout.addSpace(10);
                                    layout.add(buttonOrNot);
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    public JInternalFrame getMDIframe() { return MDIframe; }
    public Component getGlass() { return glass; }

    private void loadResults(JSortTable table) throws Exception {
        int iSearchType = 0;
        if (startsRadio.isSelected()) {
            iSearchType = 1;
        }
        if (endsRadio.isSelected()) {
            iSearchType = 2;
        }

        Vector vNodes = new Vector();
        Hashtable htCorpus = new Hashtable();

        if (taxonomyDropDown.getSelectedItem().equals("All")) {
            vNodes = its.topicSearch(queryText.getText(), iSearchType);
        } else {
            vNodes = its.topicSearch(queryText.getText(), iSearchType, (Corpus) taxonomyDropDown.getSelectedItem());
        }

        if (vNodes.size() == 0) {
            JOptionPane.showMessageDialog(ITSframe, "No results were found matching your search criteria.",
                    "Information", JOptionPane.NO_OPTION);

            DefaultSortTableModel dtm = new DefaultSortTableModel();
            table.setModel(dtm);
            return;
        }

        DefaultSortTableModel dtm = new DefaultSortTableModel(vNodes.size(), 3);
        dtm.setEditable(false);
        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            dtm.setValueAt(n, i, 0);
            dtm.setValueAt(n.get("NODEDESC"), i, 1);

            if (!htCorpus.containsKey(n.get("CORPUSID"))) {
                htCorpus.put(n.get("CORPUSID"), its.getCorpora(n.get("CORPUSID")));
            }

            dtm.setValueAt((Corpus) htCorpus.get(n.get("CORPUSID")), i, 2);
        }
        Vector v = new Vector();
        v.add("Title");
        v.add("Definition");
        v.add("Taxonomy");
        dtm.setColumnIdentifiers(v);

        table.setModel(dtm);
    }

    public void valueChanged(ListSelectionEvent event) {
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    if (e.getSource().equals(containRadio)) {
                        if (containRadio.isSelected()) {
                            startsRadio.setSelected(false);
                            endsRadio.setSelected(false);
                        }
                    }
                    if (e.getSource().equals(startsRadio)) {
                        if (startsRadio.isSelected()) {
                            containRadio.setSelected(false);
                            endsRadio.setSelected(false);
                        }
                    }
                    if (e.getSource().equals(endsRadio)) {
                        if (endsRadio.isSelected()) {
                            startsRadio.setSelected(false);
                            containRadio.setSelected(false);
                        }
                    }
                    if (e.getSource().equals(searchButton)) {
                        try {
                            loadResults(resultsTable);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    }
                    if (e.getSource().equals(deleteButton)) {
                        int rows[] = resultsTable.getSelectedRows();
                        DecimalFormat twoDigits = new DecimalFormat("0");

                        System.out.println("debug: row length is "+rows.length);

                        // pop up a progress bar to show deletion in progress
                        PopupProgressBar ppb = new PopupProgressBar(ITSframe, 1, 100);
                        ppb.show();

                        // loop through each row that is selected
                        for (int i = 0; i < rows.length; i++) {
                            ITSTreeNode n = (ITSTreeNode) resultsTable.getModel().getValueAt(rows[i], 0);

                            // if this node has children, pop up a warning
                            try {
                                int children = its.numberOfChildren(n);
                                if (children > 0) {
                                    if (JOptionPane.showConfirmDialog(ITSframe, "The topic "+n.get("NODETITLE")+" has "+
                                        children+" direct descendents.  If you continue, all of these children will "+
                                        "be lost.  Continue?", "Confirm", JOptionPane.YES_NO_OPTION) == 1)
                                            throw new NonFatalWarning();
                                }
                                its.deleteNode(n);
                            } catch (NonFatalWarning e) {
                                // do nothing
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                                 JOptionPane.showMessageDialog(ITSframe, "Warning: Topic "+n.get("NODETITLE")+" could "+
                                         "not be removed.  Reason: "+e.getMessage(), "Error", JOptionPane.NO_OPTION);
                            }

                            long perc = ((i+1)*100) / rows.length;
                            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
                        }

                        loadResults(resultsTable);

                        JOptionPane.showMessageDialog(ITSframe, "Your topic deletion request has been successfully completed.",
                                "Information", JOptionPane.NO_OPTION);

                        ppb.dispose();
                    }

                    if (e.getSource().equals(clearButton)) {
                        if (buildQuery) {
                            docFrame.topicText.setText(queryTextArea.getText());
                        }
                        MDIframe.dispose();
                    }
                    if ((e.getSource().equals(buttonAnd)) ||
                            (e.getSource().equals(buttonOr)) ||
                            (e.getSource().equals(buttonAndNot)) ||
                            (e.getSource().equals(buttonOrNot))) {

                        int row = resultsTable.getSelectedRow();
                        ITSTreeNode n = (ITSTreeNode) resultsTable.getModel().getValueAt(row, 0);
                        String sqta = queryTextArea.getText();

                        if (queryTextArea.getText().equals("")) {
                            queryTextArea.setText("\"" + n.get("NODETITLE") + "\"");
                            return;
                        }

                        if (e.getSource().equals(buttonAnd)) {
                            queryTextArea.setText(sqta + " AND \"" + n.get("NODETITLE") + "\"");
                        } else if (e.getSource().equals(buttonOr)) {
                            queryTextArea.setText(sqta + " OR \"" + n.get("NODETITLE") + "\"");
                        } else if (e.getSource().equals(buttonAndNot)) {
                            queryTextArea.setText(sqta + " AND NOT \"" + n.get("NODETITLE") + "\"");
                        } else if (e.getSource().equals(buttonOrNot)) {
                            queryTextArea.setText(sqta + " OR NOT \"" + n.get("NODETITLE") + "\"");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);


                try {

                    if (((MouseEvent) e).isShiftDown() ||
                            ((MouseEvent) e).isControlDown() ||
                            ((MouseEvent) e).isAltDown()) {
                        return;
                    }
                    Point p = ((MouseEvent) e).getPoint();
                    int row = resultsTable.rowAtPoint(p);

                    if (((MouseEvent) e).getClickCount() == 2) {
                        ITSTreeNode n = (ITSTreeNode) resultsTable.getModel().getValueAt(row, 0);                        
                        Corpus c = (Corpus) resultsTable.getModel().getValueAt(row, 2);

                        JDesktopPane jdesktoppane = ITSframe.jdp;
                        CorpusManagement DragAndDrop = null;
                        try {
                            DragAndDrop = new CorpusManagement(ITSframe, c, n);
                        } catch (Exception ex) {
                            System.out.println("Could not create corpus management frame.");
                            ex.printStackTrace(System.out);
                            return;
                        }

                        DragAndDrop.setBounds(50, 50, 850, 550);
                        DragAndDrop.setVisible(true);
                        DragAndDrop.setDefaultCloseOperation(
                                WindowConstants.DISPOSE_ON_CLOSE);

                        jdesktoppane.add(DragAndDrop);
                        DragAndDrop.moveToFront();

                        jdesktoppane.setVisible(true);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();

    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }
}
