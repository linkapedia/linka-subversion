package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class tlexport extends ExportElement {
    public ITSTreeNode Node = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable filtercommonWords = new Hashtable();
    //public String corpusID = null;
    public String wordName = "terms";

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Node n) throws Exception {
    
    
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));
    

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        DataInputStream disfilter = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\filtercommonwords.txt"))));
        

        String filterrecord = null;
        try {
            while ( (filterrecord=disfilter.readLine()) != null ) { filtercommonWords.put(filterrecord.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file filterwords.txt");
        }
        
        File f = new File(n.get("NODETITLE").replaceAll(" ", "_")+"-its.otl");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("$control: 1\n\n");
        out.write("/opname = '_TypoEqual Typo'\n");
        out.write(n.get("NODETITLE").replaceAll(" ","_").trim()+ " <And>\n");
        out.write("          /author = \"ITS_TL_SHELL\" \n");
        out.write("* 0.90 " + n.get("NODETITLE").replaceAll(" ","_").trim()+ "_pos \n");
        out.write("** 0.55 <In>\n");
        out.write("         /zonespec = \"`TI`\"\n");
        out.write("***<Any>\n");
        
        
        // get title
        String[] sArr = n.get("NODETITLE").trim().split(" ");
        if (sArr.length !=1){
        	// this is a phrase
        	out.write("**** <Many><Phrase>\n");   
        	for (int i = 0; i < sArr.length; i++) {
        		out.write("***** \"" + sArr[i].trim() + "\"\n");
        		out.write(" /author =  \"nodetitle\" \n");
        		
        	}
        	
        }
        else{
        	out.write("**** <Case><Word>\n");
        	out.write("/wordtext = \"" + n.get("NODETITLE").trim() + "\"\n");
        	out.write(" /author =  \"nodetitle\" \n");
        }
        
        
        
        
        
        // get thesaurus expansions
        Vector vThesaurus = new Vector();
        Corpus corp = server.getCorpora(n.get("CORPUSID"));
        Vector vThesauri = server.getThesauri(corp);

        for (int i = 0; i < vThesauri.size(); i++) {
            Thesaurus t = (Thesaurus) vThesauri.elementAt(i);
            Vector vterms = new Vector();

            // for each thesaurus, attempt to find the thesaurus word so
            // that we can do a synonym lookup
            try {
                vterms = server.getThesaurusWords(t.getID(), n.get("NODETITLE").toLowerCase(), true);
                for (int k = 0; k < vterms.size(); k++) {
                    ThesaurusWord tw = (ThesaurusWord) vterms.elementAt(k);
                    if (tw.getName().toLowerCase().equals(n.get("NODETITLE").toLowerCase())) {
                        try {
                            Vector synonyms = server.getSynonyms(t.getID(), Integer.parseInt(tw.getID()));
                            for (int l = 0; l < synonyms.size(); l++) {
                                ThesaurusWord tw2 = (ThesaurusWord) synonyms.elementAt(l);
                                tw2.setThesaurusName(t.getName());
                                vThesaurus.add(tw2);
                            }
                        } catch (Exception e) { }
                    }
                }
            } catch (Exception e) { }

           // if (corpusMTlevel == 0) {
            if (false) {
                String[] words = n.get("NODETITLE").split(" ");
                for (int j = 0; j < words.length; j++) {
                    try {
                        vterms = server.getThesaurusWords(t.getID(), words[j].toLowerCase(), true);
                        for (int k = 0; k < vterms.size(); k++) {
                            ThesaurusWord tw = (ThesaurusWord) vterms.elementAt(k);
                            if (tw.getName().toLowerCase().equals(words[j].toLowerCase())) {
                                try {
                                    Vector synonyms = server.getSynonyms(t.getID(), Integer.parseInt(tw.getID()));
                                    for (int l = 0; l < synonyms.size(); l++) {
                                        ThesaurusWord tw2 = (ThesaurusWord) synonyms.elementAt(l);
                                        tw2.setThesaurusName(t.getName());
                                        vThesaurus.add(tw2);
                                    }
                                } catch (Exception e) { }
                            }
                        }
                    } catch (Exception e) { }
                }
            }
        }

        //vHave = server.getMusthaves(n.get("NODEID"));
        System.out.println("**DEBUG Number of thesaurus entries:**" + vThesaurus.size());
        System.out.println("**DEBUG NodeTitle**" + n.get("NODETITLE"));
        
        out.write(OutThes(vThesaurus, "***"));
        
        
        // get must haves
        Vector vHave = new Vector();
        
        
        vHave = server.getMusthaves(n.get("NODEID"));
        System.out.println("**DEBUG Number of Must Haves:**" + vHave.size());
        System.out.println("**DEBUG NodeTitle**" + n.get("NODETITLE"));
        
        // logic for outputing must haves
        
        out.write(OutMustHave(vHave, "***"));
        
        
        // put in title plus any must haves and thesaurus expansions
        
        out.write("** 0.40 <Any>\n");
        out.write("*** <In>\n");
        out.write("         /zonespec = \"`AA`\"\n");
        out.write("**** <Any>\n");
        //out.write("***** <Many><Phrase>\n");
        //out.write("*****");
        
        out.write(OutTitle(n,"****"));
        out.write(OutThes(vThesaurus,"****"));
        out.write(OutMustHave(vHave,"****"));
        
        // here goes the same things that are in the first seection
        
        out.write("*** <In>\n");
        out.write("         /zonespec = \"`FHW`\"\n");
        out.write("**** <Any>\n");
        //out.write("***** <Many><Phrase>\n");
        //out.write("*****");
        
        out.write(OutTitle(n,"****"));
        out.write(OutThes(vThesaurus,"****"));
        out.write(OutMustHave(vHave,"****"));
        // same as above
        
        out.write("** 0.1 <Many><Any>\n");
        out.write("         /author = \"TL -ITS Shell\"\n");
        
        out.write(OutTitle(n,"**"));
        out.write(OutThes(vThesaurus,"**"));
        out.write(OutMustHave(vHave,"**"));
        
        //put in sigs here -- special handle phrased and all capped things
       
        server.getNodeSignatures(Node);
        Vector vSignatures = server.getNodeSignatures(Node);
        //Signatures signatures = new Signatures(vSignatures);                
        //Signatures signatures = new Signatures(vSignatures);
        System.out.println("**DEBUG Number of Sigs:**" + vSignatures.size());
        System.out.println("**DEBUG NodeTitle**" + n.get("NODETITLE"));
        
        for (int i = 0; i < vSignatures.size(); i++) {
        	Signature s = (Signature) vSignatures.elementAt(i);
        	
        	String[] sVhv = s.getWord().trim().split(" ");
            if (sVhv.length !=1){
            	// this is a phrase
            	out.write("*** <Many><Phrase>\n");   
            	for (int j = 0; j < sVhv.length; j++) {
            		out.write("**** \"" + sVhv[j].trim() + "\"\n");
            		out.write(" /author =  \"signature\" \n");
            	}
            	
            }
            else{
            	if ((s.getWord().trim().length()>2) && (!commonWords.containsKey(s.getWord().trim())))
            	{
            	out.write("*** <Case><Word>\n");
            	out.write("    /wordtext = \"" + s.getWord().trim() + "\"\n");
            	out.write(" /author =  \"signature\" \n");
            	}
            }
          }
        	
        
        
        out.write("* 1.00 " + n.get("NODETITLE").replaceAll(" ","_")+ "_neg <Not><Any>\n"); 
        out.write("** <In> \n");
        out.write("	/zonespec = \"`TI`\"\n");
        out.write("*** <Any> \n");
        out.write("**** 'appoint'\n"); 
        out.write("**** \"appointment\"\n");
        out.write("**** \"acquire\"\n");
        out.write("**** \"calendar\"\n");
        out.write("**** \"acquisition\"\n");
        out.write("**** \"earnings\"\n");
        out.write("**** \"hire\"\n");
        out.write("**** \"merge\"\n");
        out.write("**** \"merger\"\n");
        out.write("**** \"name\"\n");
        out.write("**** \"nomination\"\n");
        out.write("**** \"obituary\"\n");
        out.write("**** \"quarter\"\n");
        out.write("**** \"quarterly\"\n");
        out.write("**** <Many><Phrase>\n");
        out.write("*****  \"letter\"\n");
        out.write("*****  \"to\"\n");
        out.write("***** \"the\"\n");
        out.write("***** \"editor\"\n");
        
        
        
       // out.write("**** <Many><Word>\n");
       // out.write("      /wordtext = \"selected\"\n");
       // out.write("      /author = \"cohallor\"\n");
       // out.write("**** <Many><Phrase>\n");
       // out.write("      /author = \"cohallor\"\n");
       // out.write("***** 'chosen' \n");
       // out.write("***** 'by' \n");
       // out.write("**** <Many><Phrase>\n");
       // out.write("      /author = \"cohallor\"\n");
       // out.write("***** 'letter' \n");
       // out.write("***** 'to' \n");
       // out.write("***** 'the' \n");
       // out.write("***** 'editor' \n");
       // out.write("**** <Many><Phrase>\n");
       // out.write("      /author = \"cohallor\"\n");
       // out.write("***** 'picked' \n");
       // out.write("***** 'by' \n");
       // out.write("**** <Many><Stem>\n");
       // out.write("      /wordtext = \"deal\"\n");
       // out.write("      /author = \"cohallor\"\n");
        out.write("** <In>\n");
        out.write("	/zonespec = \"`FHW`\"\n");
        out.write("*** <Any>\n");
        out.write("**** <Word>\n");
        out.write("      /wordtext = \"illus\"\n");
        out.write("**** \"hardbound\"\n");
        out.write("**** \"paperback\"\n");
        out.write("**** \"hardback\"\n");
        out.write("**** \"softcover\"\n");
        out.write("**** \"hardcover\"\n");
        out.write("**** <Word>\n");
        out.write("      /wordtext = \"pp\"\n");
        out.write("**** <Many><Phrase>\n");
        out.write("***** \"available\"\n");
        out.write("***** \"due\"\n");
        out.write("***** \"to\"\n");
        out.write("***** \"copyright\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"will\"\n");
        //out.write("***** \"be\"\n");
        //out.write("***** \"held\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"has\"\n");
        //out.write("***** \"joined\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"voted\"\n");
        //out.write("***** \"onto\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"cash\"\n");
        //out.write("***** \"transaction\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"assume\"\n");
        //out.write("***** \"ownership\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"letter\"\n");
        //out.write("***** \"of\"\n");
        //out.write("***** \"credit\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"key\"\n");
        //out.write("***** \"products\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"strategic\"\n");
        //out.write("***** \"partnership\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"service\"\n");
        //out.write("***** \"bundle\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"has\"\n");
        //out.write("***** \"been\"\n");
        //out.write("***** \"selected\"\n");
        //out.write("**** <Many><Phrase>\n");
        //out.write("***** \"FTTH\"\n");
        //out.write("***** \"Council\"\n");
        out.write("** <Word>\n");
        out.write("      /wordtext = \"ISBN\"\n");
        out.close();
        return f;
    }

  
    private String OutMustHave(Vector vHave, String star){
    	
    	StringBuffer buffer = new StringBuffer();
        if (vHave.size() >0){
        	for (int i = 0; i < vHave.size(); i++) {
    		String[] sVhv = vHave.elementAt(i).toString().trim().split(" ");
            if (sVhv.length !=1){
            	// this is a phrase
            	buffer.append(star + "* <Many><Phrase>\n");   
            	for (int j = 0; j < sVhv.length; j++) {
            		buffer.append(star + "** \"" + sVhv[j].trim() + "\"\n");
            		//buffer.append(" / from must haves \n");
            	}
            	
            }
            else{
            	if ((vHave.elementAt(i).toString().trim().length()>2) && (!commonWords.containsKey(vHave.elementAt(i).toString().trim())))
            	{
            	buffer.append(star + "* <Case><Word>\n");
            	buffer.append("    /wordtext = \"" + vHave.elementAt(i).toString().trim() + "\"\n");
            	//buffer.append(" / from must haves \n");
            	}
            }
          }
        	buffer.append(" /author =  \"must haves\" \n");
        }
    
    	// output must haves from data structure
    	
    return buffer.toString();
    }
    
    private String OutThes(Vector vThs, String star){
    	StringBuffer buffer = new StringBuffer();
    	 if (vThs.size() >0){      	
         	for (int i = 0; i < vThs.size(); i++) {
         		
         		String[] sThs = vThs.elementAt(i).toString().trim().split(" ");
                 if (sThs.length !=1){
                 	// this is a phrase
                 	buffer.append(star + "* <Many><Phrase>\n");   
                 	for (int j = 0; j < sThs.length; j++) {
                 		buffer.append(star + "** \"" + sThs[j].trim() + "\"\n");
                 		//buffer.append(" / from thesaurus \n");
                 	}
                 	
                 }
                 else{
                	 
                	if ((vThs.elementAt(i).toString().trim().length()>2) && (!commonWords.containsKey((vThs.elementAt(i).toString().trim()))))
                	{
                 	buffer.append(star + "* <Case><Word>\n");
                 	buffer.append("     /wordtext =  \"" + vThs.elementAt(i).toString().trim() + "\"\n");
                 	//buffer.append(" / from thesaurus \n");
                	}
                 }
               }
         	buffer.append(" /author =  \"thesaurus\" \n");
         }
        
    	// output must haves from data structure
    	
    return buffer.toString();
    }
    
    private String OutTitle(Node n, String star){
    	StringBuffer buffer = new StringBuffer();
    	
    	
    	String[] sArr = n.get("NODETITLE").trim().split(" ");
        if (sArr.length !=1){
        	// this is a phrase
        	buffer.append(star + "* <Many><Phrase>\n");   
        	for (int i = 0; i < sArr.length; i++) {
        		buffer.append(star + "** \"" + sArr[i].trim() + "\"\n");
        		
        		
        	}
        	//buffer.append(" / from nodetitle \n");
        	
        }
        else{
        	buffer.append(star + "* <Case><Word>\n");
        	buffer.append("    /wordtext = \"" + n.get("NODETITLE").trim() + "\"\n");
        	//buffer.append(" / from nodetitle \n");
        }
       
    	
        buffer.append(" /author =  \"nodetitle\" \n");
    	return buffer.toString();
    }
    
    
    
    private File createOrGetFile(String path) throws Exception {
        File f = new File(path);
        if (!f.exists()) f.mkdir();

        return f;
    }


    private String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method

  
}