/**
 * this class is to send info data images (nodes, taxonomy)
 */

package com.iw.system; 

public class FileMetadata {
	
	//nodeid can be any value in any table as a corpusid(is only to reference)
	private String nodeId;
	private String Description;
	private String uri;
	private byte[] image;
	private String id;
	
	public void setId(String id)
	{
		this.id=id;
	}
	public String getId()
	{
		return this.id;
	}
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	
}
 