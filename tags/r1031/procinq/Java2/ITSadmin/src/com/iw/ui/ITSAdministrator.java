package com.iw.ui;

import com.iw.system.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;

/**
 * ITSAdministrator contains the Main class for the ITS Administration JAVA
 * client. This class will prompt for login information and collect user data
 * from the API as applicable. If login is successful, a frame window will be
 * created. This frame will serve as the tool window for the duration. The
 * window size is defined by the current desktop resolution.
 * 
 * @authors Michael A. Puscar, Indraweb Inc, All Rights Reserved.
 * 
 * @note Session and ITS server information are stored as a public property of
 *       the frame.
 * @return ITSAdministrator object (subclass of Frame)
 */

public class ITSAdministrator extends JFrame {

	public JDesktopPane jdp = null; // provide a pointer to the desktop pane
									// from the ITS Administrator
	public ITS its = null; // ITS object provides session as well as server
							// information
	public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this,"itsimages/logo.gif");

	public ITSAdministrator() {
		super("ProcinQ Editor's Desktop");
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);

		com.incors.plaf.alloy.AlloyLookAndFeel.setProperty("alloy.licenseCode",
				"7#Michael_Hoey#gecuh5#6ygjkk");
		try {
			javax.swing.LookAndFeel alloyLnF = new com.incors.plaf.alloy.AlloyLookAndFeel();
			javax.swing.UIManager.setLookAndFeel(alloyLnF);

			// set a unicode font for the given widget defaults - need to be
			// able to support foreign chars
			FontUIResource font = new FontUIResource("Arial Unicode MS",
					Font.PLAIN, 12);
			UIManager.put("FormattedTextField.font", font);
			UIManager.put("MenuItem.acceleratorFont", font);
			UIManager.put("RadioButtonMenuItem.font", font);
			UIManager.put("Viewport.font", font);
			UIManager.put("PasswordField.font", font);
			UIManager.put("CheckBoxMenuItem.font", font);
			UIManager.put("RadioButton.font", font);
			UIManager.put("Table.font", font);
			UIManager.put("EditorPane.font", font);
			UIManager.put("ColorChooser.font", font);
			UIManager.put("ToolBar.font", font);
			UIManager.put("ProgressBar.font", font);
			UIManager.put("Menu.font", font);
			UIManager.put("Button.font", font);
			UIManager.put("TitledBorder.font", font);
			UIManager.put("PopupMenu.font", font);
			UIManager.put("Menu.acceleratorFont", font);
			UIManager.put("TextArea.font", font);
			UIManager.put("ComboBox.font", font);
			UIManager.put("InternalFrame.titleFont", font);
			UIManager.put("CheckBoxMenuItem.acceleratorFont", font);
			UIManager.put("ToggleButton.font", font);
			UIManager.put("TabbedPane.font", font);
			UIManager.put("TextField.font", font);
			UIManager.put("ToolTip.font", font);
			UIManager.put("List.font", font);
			UIManager.put("DesktopIcon.font", font);
			UIManager.put("OptionPane.font", font);
			UIManager.put("Panel.font", font);
			UIManager.put("MenuBar.font", font);
			UIManager.put("Tree.font", font);
			UIManager.put("ScrollPane.font", font);
			UIManager.put("TableHeader.font", font);
			UIManager.put("MenuItem.font", font);
			UIManager.put("TextPane.font", font);
			UIManager.put("CheckBox.font", font);
			UIManager.put("RootPane.titleFont", font);
			UIManager.put("Label.font", font);
			UIManager.put("Spinner.font", font);
			UIManager.put("RadioButtonMenuItem.acceleratorFont", font);
			UIManager.put("Slider.font", font);

		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			// You may handle the exception here
		}

		addWindowListener(new WindowAdapterSimple());

		setSizeHalfScreen();
		setBackground(Color.DARK_GRAY);

		setIconImage(iIndraweb.getImage());

		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		// Pop-up a LOGIN dialog box. Do not make the frame visible unless the
		// login is successful.
		LoginDialog jdialoglogin = null;
		try {
			jdialoglogin = new LoginDialog(this);
			jdialoglogin.setVisible(true);
			// contentPane.add(jdialoglogin);
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		if (jdialoglogin.its == null) {
			System.exit(-1);
		}
		its = jdialoglogin.its; // store session information here

		ITSMenu menu = null;

		try {
			menu = new ITSMenu(this);
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		JMenuBar menubar = menu.buildMenu();
		JToolBar tools = menu.buildToolbar();

		menubar.setMargin(new Insets(0, 0, 0, 0));
		tools.setMargin(new Insets(0, 0, 0, 0));

		setJMenuBar(menubar);

		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.add(menu, BorderLayout.CENTER);
		contentPane.add(tools, BorderLayout.NORTH);
		setVisible(true);
	}

	// this function will create an MDI thesaurus pane as a child of the ITS
	// Frame.
	public ThesaurusPane openThesaurus(ITSAdministrator ITSframe, Thesaurus t) {
		com.incors.plaf.alloy.AlloyLookAndFeel.setProperty("alloy.licenseCode",
				"7#Michael_Hoey#gecuh5#6ygjkk");
		try {
			javax.swing.LookAndFeel alloyLnF = new com.incors.plaf.alloy.AlloyLookAndFeel();
			javax.swing.UIManager.setLookAndFeel(alloyLnF);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			// You may handle the exception here
		}

		ThesaurusPane tp = new ThesaurusPane(ITSframe, t.getID(), t.getName());
		jdp.add(tp);

		tp.setVisible(true);

		return tp;
	}

	// the ITS frame defaults to screensize / 1.5
	public void setSizeHalfScreen() {
		Dimension winsize = this.getSize(), screensize = Toolkit
				.getDefaultToolkit().getScreenSize();
		setSize((int) (screensize.width / 1.5),
				(int) (screensize.height / 1.25));
		// System.out.println("Width: "+screensize.width+" Height: "+screensize.height);
	}

	// main function
	public static void main(String[] args) {

		User u = ITS.getUser();
		if ((u != null) && (u.writeToLog()))
			ITS.activateLogging();

		ITSAdministrator jframeApp = new ITSAdministrator();
		jframeApp.setVisible(true);

	}
}
