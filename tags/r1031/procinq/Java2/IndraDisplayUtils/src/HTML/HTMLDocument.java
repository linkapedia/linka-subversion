package HTML;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.lang.reflect.*;
import java.net.URLEncoder;
import java.net.URLDecoder;

import javax.servlet.*;
import javax.servlet.http.*;

import com.indraweb.execution.Session;
import com.iw.system.*;
import com.iw.system.Corpus;
import Logging.*;
import Server.*;

// HTML Document
//
// This object and its corresponding methods relate to creation of
// HTML templates for use in building the dynamic display.

public class HTMLDocument
{
	private String sTemplateDir;	// variables specifies from which directory to pull the template
	private Hashtable htVariables;	// contains a list of variables to be replaced when template is read
	
	// Accessor methods to private variables
	public String GetTemplateDir() { return sTemplateDir; }
	public Hashtable GetHash() { return htVariables; }
	public void SetHash(Hashtable htVariables) { 
		Enumeration e = htVariables.keys();
		
		// Add values of htVariables into our object properties
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
            String sValue = "";
			try { sValue = (String) htVariables.get(sKey); }
            catch (Exception ex) { }

			this.htVariables.put(sKey, sValue);
		}
	}
    public void SetHash(Document d) {
        Enumeration eD = d.htd.keys();
        while (eD.hasMoreElements()) {
            String Key = (String) eD.nextElement();
            String Val = (String) d.htd.get(Key);

            htVariables.put(Key.toUpperCase(), Val);
        }
    }
    public void SetHash(NodeDocument d) {
        Enumeration eD = d.htnd.keys();
        while (eD.hasMoreElements()) {
            String Key = (String) eD.nextElement();
            String Val = (String) d.htnd.get(Key);

            htVariables.put(Key.toUpperCase(), Val);
        }
    }
    public void SetHash(Node n) {
        Enumeration eN = n.htn.keys();
        while (eN.hasMoreElements()) {
            String Key = (String) eN.nextElement();
            String Val = (String) n.htn.get(Key);

            htVariables.put(Key.toUpperCase(), Val);
        }
    }
    public void SetHash(Corpus c) {
        htVariables.put("CORPUSACTIVE", c.getActive());
        htVariables.put("CORPUSDESC", c.getDescription());
        htVariables.put("CORPUSID", c.getID());
        htVariables.put("CORPUSNAME", c.getName());
    }

    public void ClearHash () {
        htVariables = new Hashtable();
    }

	// Object constructors
	public HTMLDocument (String sTemplateDir) {
		
		this.sTemplateDir = sTemplateDir;
		this.htVariables = new Hashtable();
	}
	public HTMLDocument () {
		String sCurrTemplate = Session.cfg.getProp("Template");
		
		this.sTemplateDir = sCurrTemplate;
		this.htVariables = new Hashtable();
	}

	// Add a variable into our variable hash table; these values are read and replaced in the template
	public Hashtable AddVariable(String key, String value) {
		htVariables.put(key.toUpperCase(), value);
		
		return htVariables;
	}

	// Add a variable into our variable hash from the cookie object
	public Hashtable AddVariable(String key, HttpServletRequest req) {
		Cookie Cookies[] = req.getCookies();
			
		if (req.getCookies() != null) {
			int cLen = Cookies.length;
			
			for (int i=0; i < cLen; i++) { if (Cookies[i].getName().equals(key)) { 
				try { htVariables.put(key.toUpperCase(), URLDecoder.decode(Cookies[i].getValue(), "UTF-8")); }
                catch (UnsupportedEncodingException e) { } // ignore unsupported encoded characters
			}}
		}
		return htVariables;
	}
	
	// Print out the header information
	public void WriteHeader (PrintWriter out) {
		String sTemplateFile = "header.tpl";
		ReadWriteAndReplace(out, sTemplateFile);
	}
	public void WriteHeader (FileWriter out) throws Exception {
		String sTemplateFile = "header.tpl";
		ReadWriteAndReplace(out, sTemplateFile);
		out.flush();
	}

	// Print out the footer information
	public void WriteFooter (PrintWriter out) {
		String sTemplateFile = "footer.tpl";
		ReadWriteAndReplace(out, sTemplateFile);
	}
	public void WriteFooter (FileWriter out) throws Exception {
		String sTemplateFile = "footer.tpl";
		ReadWriteAndReplace(out, sTemplateFile);
		out.flush();
	}

	// Print out the error information
	public void WriteError (PrintWriter out, String ErrorMessage) {
		String sTemplateFile = "error.tpl";
		AddVariable("ErrorMessage", ErrorMessage);
		ReadWriteAndReplace(out, sTemplateFile);
	}
	public void WriteError (FileWriter out, String ErrorMessage) throws Exception {
		String sTemplateFile = "error.tpl";
		AddVariable("ErrorMessage", ErrorMessage);
		ReadWriteAndReplace(out, sTemplateFile);
		out.flush();
	}

	// Print out the success template
	public void WriteSuccess (PrintWriter out, String SuccessMessage) {
		AddVariable("SuccessMessage", SuccessMessage);
		WriteHeader(out);
		out.println("<blockquote>"+SuccessMessage+"</blockquote>");
		WriteFooter(out);
	}
	public void WriteSuccess (FileWriter out, String SuccessMessage) 
	throws Exception {
		AddVariable("SuccessMessage", SuccessMessage);
		WriteHeader(out);
		out.flush();
		out.write("<blockquote>"+SuccessMessage+"</blockquote>\n");
		WriteFooter(out);
		out.flush();
	}

	// Print out the given template
	public void WriteTemplate (PrintWriter out, String sTemplate) {
		ReadWriteAndReplace(out, sTemplate);
	}
	public void WriteTemplate (FileWriter out, String sTemplate) throws Exception {
		ReadWriteAndReplace(out, sTemplate);
		out.flush();
	}

	// Read the template file, replace variables, and write to standard out
	private void ReadWriteAndReplace (PrintWriter out, String sFilename) {
		String sTemplateFile = sTemplateDir+"/"+sFilename;

				
		try {
			BufferedReader bLine = new BufferedReader(new FileReader(sTemplateFile));
			String sData = new String();
			
			// Read each line from the template
			while ((sData = bLine.readLine()) != null) {
				// Replace all variables in this line with their corresponding values.
				// Variables in templates are noted by beginning and ending with two pound characters.
				//
				// Example: ##CORPUS_NAME##
				// 
				// Variable names will be case insensitive.  
				
				// Damn!  This would be so much easier in Perl!!
				// Loop while we still have variables in this line..
				int iStart = 0;
				int iLastIndex = 0;
				
				while ((iStart = sData.indexOf("##", iLastIndex)) != -1) {
					//out.println("Debug: "+sData+" iStart: "+iStart);
					
					// Get the variable name.   To do this find the index where it ends.
					int iEnd = sData.indexOf("##", iStart+1);
					
					if (iEnd != -1) { // If we received -1 for some reason, it is not a variable
						String sVariableName = sData.substring(iStart+2, iEnd);
						String sVariableValue = new String("");

						sData = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sData,sVariableName,sVariableName.toUpperCase()));
						
						// If the variable name is located in the hash table, replace the
						// variable in the template with its corresponding value
						if (htVariables.containsKey(sVariableName.toUpperCase()) == true) {
							sVariableValue = (String) htVariables.get(sVariableName.toUpperCase());
						}

						sData = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sData,"##"+sVariableName+"##",sVariableValue));
						iStart = 0;
					} else { iStart = iEnd +2; }
				}
				
				// Finally!  Print out this line.
				out.println(sData);
			}
			bLine.close();
		}
		catch (FileNotFoundException e) {
			out.println("Template file not found.");	
			e.printStackTrace(out);

		}
		catch (Exception e) { Log.LogError(e, out); }
	}
	private void ReadWriteAndReplace (FileWriter out, String sFilename) 
		throws Exception {
		
		String sTemplateFile = sTemplateDir+"/"+sFilename;
				
		try {
			BufferedReader bLine = new BufferedReader(new FileReader(sTemplateFile));
			String sData = new String();
			
			// Read each line from the template
			while ((sData = bLine.readLine()) != null) {
				// Replace all variables in this line with their corresponding values.
				// Variables in templates are noted by beginning and ending with two pound characters.
				//
				// Example: ##CORPUS_NAME##
				// 
				// Variable names will be case insensitive.  
				
				// Damn!  This would be so much easier in Perl!!
				// Loop while we still have variables in this line..
				int iStart = 0;
				int iLastIndex = 0;
				
				while ((iStart = sData.indexOf("##", iLastIndex)) != -1) {
					//out.println("Debug: "+sData+" iStart: "+iStart);
					
					// Get the variable name.   To do this find the index where it ends.
					int iEnd = sData.indexOf("##", iStart+1);
					
					if (iEnd != -1) { // If we received -1 for some reason, it is not a variable
						String sVariableName = sData.substring(iStart+2, iEnd);
						String sVariableValue = new String("");

						sData = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sData,sVariableName,sVariableName.toUpperCase()));
						
						// If the variable name is located in the hash table, replace the
						// variable in the template with its corresponding value
						if (htVariables.containsKey(sVariableName.toUpperCase()) == true) {
							sVariableValue = (String) htVariables.get(sVariableName.toUpperCase());
						}

						sData = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sData,"##"+sVariableName+"##",sVariableValue));
						iStart = 0;
					} else { iStart = iEnd +2; }
				}
				
				// Finally!  Print out this line.
				out.write(sData+"\n");
			}
			bLine.close();
		}
		catch (FileNotFoundException e) {
			Log.Log("Template file not found.\n");	
		}
		catch (Exception e) { Log.LogError(e); }
	}
}
