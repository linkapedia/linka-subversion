/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 17, 2003
 * Time: 3:54:07 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package HTML.tableemitter;

import java.util.Vector;

public class TableDef {

    int iTableWidthPct = -1;
    private Vector vColumnDefs = new Vector();

    public TableDef ( int iTableWidthPct_ )     {
        iTableWidthPct = iTableWidthPct_;
    }

    public void addCol ( ColumnDef col )     {
        vColumnDefs.addElement ( col );
    }

    public int getTableWidthPct ()     {
        return iTableWidthPct;
    }

    public Vector getColumnDefs() {
        return vColumnDefs;
    }
}
