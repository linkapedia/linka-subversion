/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 17, 2003
 * Time: 4:04:25 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package HTML.tableemitter;

import java.io.PrintWriter;
import java.util.Vector;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

public class TableEmit {

    public static void emitTable(String sSQL,
                                 TableDef tabledef,
                                 Connection dbc,
                                 PrintWriter out) throws Exception {
        // table
        //    <table width="76%" border="0">
        out.write("<table width=\"" + tabledef.getTableWidthPct() + "%\" border=\"0\">");
        // now cols
        Vector vColDefs = tabledef.getColumnDefs();
        ColumnDef[] coldefArr = new ColumnDef[vColDefs.size()];
        vColDefs.copyInto(coldefArr);

        // write header
        out.write("<tr>\r\n");
        for (int jCol = 0; jCol < coldefArr.length; jCol++) {
            emitTableCell(coldefArr[jCol], coldefArr[jCol].getColName(), out);
        }
        out.write("</tr>\r\n");
        System.out.println("pre emitDataRows");

        emitDataRows(sSQL, out, dbc, coldefArr);
        System.out.println("post emitDataRows");
    }

    private static void emitDataRows(String sSQL, PrintWriter out, Connection dbc, ColumnDef[] coldefArr )
            throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = dbc.createStatement();
            System.out.println("executing query [" + sSQL + "]");
            rs = stmt.executeQuery(sSQL);
            System.out.println("past executing query [" + sSQL + "]");
            while ( rs.next() ) {
                String[] sArr = new String[coldefArr.length];
                for (int jCol = 0; jCol < sArr.length; jCol++) {
                    sArr[jCol] = rs.getString(jCol + 1);
                }
                emitDataRow (coldefArr, sArr, out );
            }

        }
        finally {
            if (rs != null)
                stmt.close();
            if (stmt != null)
                stmt.close();
        }
    }

    // data rows
    private static void emitDataRow(ColumnDef[] coldefArr, String[] sArr, PrintWriter
            out)
            throws Exception {
        // header row
        out.write("<tr>");
        for (int iRow = 0; iRow < coldefArr.length; iRow++) {
            // <td width="73%" valign="top">List the nodes, corpora, and crawler for the last hour.
            emitTableCell(coldefArr[iRow], sArr[iRow], out);
            // href example out.write("<td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor"><b>Nodes Processed</b></a></td>
        }
        out.write("</tr>\r\n");
    }

    public static void emitTableCell(ColumnDef
            coldef,
                                     String sData,
                                     PrintWriter out)
            throws Exception {
        out.write("<td width=" + coldef.getPctWidth() + "%\" valign=\"top\">" + sData + "</td>");
    }
}

