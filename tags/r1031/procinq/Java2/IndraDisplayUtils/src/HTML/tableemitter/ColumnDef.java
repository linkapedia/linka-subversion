/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 17, 2003
 * Time: 3:54:19 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package HTML.tableemitter;

public class ColumnDef {

    String sColName = null;
    int iPctWidth = -1;
    public ColumnDef ( String sColName_, int iPctWidth_  )
    {
        sColName = sColName_;
        iPctWidth = iPctWidth_;
    }

    public String getColName() {
        return sColName;
    }
    public int getPctWidth() {
        return iPctWidth;
    }

}
