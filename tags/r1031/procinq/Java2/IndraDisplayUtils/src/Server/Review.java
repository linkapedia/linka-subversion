package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;
import com.indraweb.execution.Session;

import java.io.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Logging.*;
import com.iw.system.*;

public class Review {
    public static void handleTSapiRequest(APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
            throws Exception {

        String submit = (String) props.get ("submit");

		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Review Edited Results");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-nosearch.tpl");

            if (submit != null) {
                HashTree htArguments = new HashTree(props); String sDocumentIDList = "0";

                // loop through all the approved results
                Enumeration e4 = req.getParameterNames();
                while (e4.hasMoreElements()) {
                    String sParmName = (String) e4.nextElement();
                    String sParmValu = req.getParameter(sParmName);

                    if (sParmName.startsWith("DOCID") && sParmValu.equals("on")) {
                        sDocumentIDList = sDocumentIDList + ", " + sParmName.substring(5);
                    }
                }

                htArguments.put("DocumentIDList", sDocumentIDList);
                InvokeAPI API = new InvokeAPI ("tsclassify.TSApproveReviewedResult", htArguments);
                HashTree htResults = API.Execute(false, false);

                if (htResults.containsKey("SUCCESS")) {
                    out.println("<SCRIPT>alert('Results were approved successfully.');</SCRIPT>");
                } else {
                    out.println("<SCRIPT>alert('Results could not be approved at this time.');</SCRIPT>");
                }
            }

            ITS its = new ITS((String) props.get("SKEY"));
            Vector vNodeDocuments = its.reviewBatchResults();

            String sPrevDocID = "-1";

            for (int i = 0; i < vNodeDocuments.size(); i++) {
                NodeDocument nd = (NodeDocument) vNodeDocuments.elementAt(i);

                Document.SetHash(nd);
                Document.AddVariable("EDOCURL", nd.getEncodedField("DOCURL"));
                Document.AddVariable("EDOCSUMMARY", nd.getEncodedField("EDOCSUMMARY"));
                Document.AddVariable("SCORE", nd.get("SCORE1"));

                if (nd.get("DOCUMENTID").equals(sPrevDocID)) { Document.WriteTemplate(out, "review-nodedoc.tpl"); }
                else { Document.WriteTemplate(out, "review.tpl"); sPrevDocID = nd.get("DOCUMENTID"); }
            }

            if (vNodeDocuments.size() == 0)
                out.println("<blockquote>There are no documents available for review.</blockquote>");

            Document.WriteFooter(out);
        } catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
        }
    }
}
/*
  <NODEID>215512</NODEID> <DOCUMENTID>729133</DOCUMENTID>
  <NODETITLE> <![CDATA[ Special Practices ]]> </NODETITLE>
  <DOCTITLE> <![CDATA[ hkon testroc 7 </DOCTITLE>
  <DOCURL> <![CDATA[ hkon testroc 7 ]]> </DOCURL>
*/