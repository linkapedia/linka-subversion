package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;
import com.iw.system.*;

public class RebuildIndexes
{
	// Rebuild interMedia indicies
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			// Call: RebuildIndexes
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tsother.TSRebuildIndexes", htArguments);
			HashTree htResults = API.Execute(false, false);

			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Success");
            Document.WriteSuccess(out, "Full text indexes were rebuild successfully.");
		} catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	}
}
