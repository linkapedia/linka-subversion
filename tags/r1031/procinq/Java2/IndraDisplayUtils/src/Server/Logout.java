package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;
import com.iw.system.*;

public class Logout
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			// Call: Logout
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("security.TSLogout", htArguments);
			HashTree htResults = API.Execute(false, false);

			// Write the session information into the user's cookie
			Cookie cSessionCookie = new Cookie("SKEY", "0");
			cSessionCookie.setMaxAge(-1000);
			res.addCookie(cSessionCookie);
				
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Taxonomy Server Login");
			Document.WriteTemplate(out, "header-admin.tpl");
			Document.WriteTemplate(out, "login.tpl");
			Document.WriteFooter(out);
		} catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	}
}
