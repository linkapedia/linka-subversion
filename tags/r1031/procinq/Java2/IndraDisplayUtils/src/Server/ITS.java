package Server;

import javax.swing.table.TableModel;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.awt.*;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;
import com.iw.guiservershared.TableModelXMLBeanSerializable;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import org.dom4j.Element;
import com.iw.system.*;
import com.iw.utils.PDFUtils;
import org.textmining.text.extraction.WordExtractor;

/**
 * The ITS object defines both a user session and contains methods used to interface with the ITS API.  Each of
 * these methods are simply abstract another layer onto the raw InvokeAPI class.
 *
 * A pointer to the ITS object should be kept persistently as it is the container for both the user's session
 *  key and the location of the API server.
 *
 * NOTE: this specific version deals with the WEB based API only
 *
 *	@authors Michael A. Puscar, Indraweb Inc, All Rights Reserved.
 *
 *  @note   Session and ITS server information are stored as a public property of the frame.
 *	@return An ITS object.
 */

public class ITS extends Component implements Serializable {
    private String SKEY = "NONE";
    private String API = com.indraweb.execution.Session.cfg.getProp("API");

    public ITS() {
    }

    public ITS(String SessionKey) {
        if (SessionKey != null) {
            this.SKEY = SessionKey;
        }
        this.API = com.indraweb.execution.Session.cfg.getProp("API");
    }
    public ITS(String SessionKey, String API) {
        if (SessionKey != null) {
            this.SKEY = SessionKey;
        }
        if (API != null) {
            this.API = API;
        }
    }

    // Accessor functions
    public void SetSessionKey(String SessionKey) {
        if (SessionKey != null) {
            this.SKEY = SessionKey;
        }
    }

    public void SetAPI(String API) {
        if (API != null) {
            this.API = API;
        }
    }

    public String getSessionKey() {
        return SKEY;
    }

    public String getAPI() {
        return API;
    }

    public HashTree getArguments() {
        HashTree ht = new HashTree();
        ht.put("api", API);
        ht.put("SKEY", SKEY);

        return ht;
    }

    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeAPI(sAPIcall, htArgs, true); }
    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs, boolean bReplaceQuotes) throws Exception {
        return InvokeAPI(sAPIcall, htArgs, bReplaceQuotes, false);
    }
    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs, boolean bReplaceQuotes, boolean bDebug)
            throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        HashTree ht = null;
        try { ht = API.Execute(bDebug, false, bReplaceQuotes); }
        catch (SessionExpired e) { sessionTimedOut(); }
        catch (Exception e) { throw e; }

        return ht;
    }

    public Vector InvokeVectorAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeVectorAPI(sAPIcall, htArgs, false); }
    public Vector InvokeVectorAPI(String sAPIcall, Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        Vector v = new Vector();
        try { v = API.vExecute(bDebug, false); }
        catch (SessionExpired e) { sessionTimedOut(); }
        catch (Exception e) { throw e; }

        return v;
    }

    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeDomAPI(sAPIcall, htArgs, false); }
    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try { d = API.dExecute(bDebug, false); }
        catch (Exception e) { throw e; }

        return d;
    }

    // hardcoded (for now), will read from file
    public String getClientVersion() {
        return "CDMS Web Client 1.3";
    }

    // get the server version
    public String getServerVersion() {
        Hashtable htArgs = getArguments();

        try {
            HashTree htResults = InvokeAPI("tsother.TSGetVersion", htArgs);
            if (!htResults.containsKey("VERSION")) { return "Server version unavailable."; }

            return (String) htResults.get("VERSION");
        } catch (Exception e) {
             return "Server version unavailable.";
        }
    }

    public Vector getFolders() throws Exception {
        return getFolders(null); }
    public Vector getFolders(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) htArgs.put("CorpusID", c.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsgenre.TSListFolders", htArgs);

            if (!htResults.containsKey("GENRES")) {
                return new Vector();
            }

            HashTree htGenres = (HashTree) htResults.get("GENRES");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                Genre g = new Genre(htGenre); v.add(g);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getSubjectAreas() throws Exception {
        return getSubjectAreas(null); }
    public Vector getSubjectAreas(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) htArgs.put("CorpusID", c.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tssubjectarea.TSListSubjectAreas", htArgs);

            if (!htResults.containsKey("SUBJECTAREAS")) {
                return new Vector();
            }

            HashTree htGenres = (HashTree) htResults.get("SUBJECTAREAS");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                SubjectArea sa = new SubjectArea(htGenre); v.add(sa);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getMyAlerts(String DN) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        //System.out.println("getMyAlerts: "+DN);

        htArgs.put("UserID", DN);

        try {
            HashTree htResults = InvokeAPI("tsnotification.TSViewAlertsByDN", htArgs, true, true);
            if (!htResults.containsKey("ALERTS")) { return v; }

            HashTree htGroups = (HashTree) htResults.get("ALERTS");
            Enumeration eG = htGroups.elements();

            while (eG.hasMoreElements()) {
                HashTree htGroup = (HashTree) eG.nextElement();

                String runFreq = (String) htGroup.get("RUNFREQ");
                FrequencyDrop fd = new FrequencyDrop("Never", runFreq);

                if (runFreq.equals("1")) { fd = new FrequencyDrop("Daily", runFreq); }
                else if (runFreq.equals("7")) { fd = new FrequencyDrop("Weekly", runFreq); }
                else if (runFreq.equals("30")) { fd = new FrequencyDrop("Monthly", runFreq); }

                Alert a = new Alert((String) htGroup.get("ALERTID"),
                        (String) htGroup.get("ALERTNAME"),
                        (String) htGroup.get("USERDN"),
                        (String) htGroup.get("EMAIL"),
                        (String) htGroup.get("QUERY"),
                        (String) htGroup.get("NOTES"),
                        fd);

                v.add(a);
            }
        } catch (Exception e) { throw e; }

        //System.out.println("Results: "+v.size());
        return v;
    }

    public Vector getConceptAlerts() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();
        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsdocument.TSGetNarratives", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("DOCUMENTS");
            if (eDocs == null) return new Vector();

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                Iterator i2 = eDoc.elements().iterator();
                Document d = new Document(i2);

                v.add(d);
            }
            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector searchCorpora(String query) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("query", query);

        Vector v = new Vector();

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscorpus.TSSearchCorpora", htArgs, false);
            Element elemRoot = doc.getRootElement();

            Element eDocs = elemRoot.element("NODES");
            if (eDocs == null) return new Vector();

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                Iterator i2 = eDoc.elements().iterator();
                if (eDoc.getQualifiedName().equals("NODE")) v.add(new Node(i2));
                else v.add(i2);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getMyGroups() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        try {
            HashTree htResults = InvokeAPI("security.TSGetMyGroups", htArgs);
            if (!htResults.containsKey("GROUPS")) { return v; }

            HashTree htGroups = (HashTree) htResults.get("GROUPS");
            Enumeration eG = htGroups.elements();

            while (eG.hasMoreElements()) {
                HashTree htGroup = (HashTree) eG.nextElement();
                SecurityGroup sg = new SecurityGroup();
                sg.setID((String) htGroup.get("DC"));
                sg.setName((String) htGroup.get("GROUPNAME"));
                v.add(sg);
            }
        } catch (Exception e) { throw e; }

        return v;
    }

    public Vector getSecurityGroups(String CorpusID, String Access) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (CorpusID != null) htArgs.put("CorpusID", CorpusID);

        // invoke the API
        try {
            HashTree htResults;

            if (CorpusID != null) { htResults = InvokeAPI("tscorpus.TSListCorpusAccess", htArgs); }
            else { htResults = InvokeAPI("tsuser.TSListGroups", htArgs); }

            if (!htResults.containsKey("GROUPS")) {
                return new Vector();
            }

            HashTree htGroups = (HashTree) htResults.get("GROUPS");
            Enumeration eG = htGroups.elements();
            while (eG.hasMoreElements()) {
                HashTree htGroup = (HashTree) eG.nextElement();
                SecurityGroup sg = new SecurityGroup(htGroup);

                if (Access == null) { v.add(sg); }
                else if (Access.equals(sg.getAccess())) { v.add(sg); }
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getRepositories() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        try {
            HashTree htResults = InvokeAPI("tsrepository.TSGetRepositoryList", htArgs);

            if (!htResults.containsKey("REPOSITORIES")) {
                throw new Exception("There are no repositories in this database.");
            }

            HashTree htReps = (HashTree) htResults.get("REPOSITORIES");
            Enumeration eR = htReps.elements();
            while (eR.hasMoreElements()) {
                HashTree htRepository = (HashTree) eR.nextElement();

                // repository type 1 is reserved, do not include in the list
                Repository R = new Repository(htRepository);
                if (!R.getID().equals("1")) { v.add(R); }
            }

        } catch (Exception e) { throw e; }

        return v;
    }

    public void applyNodeRefresh(Node Node) throws Exception {
        applyNodeRefresh(Node, false, -1, null); }
    public void applyNodeRefresh(Node Node, boolean bAffinityNodeTitlesOnly) throws Exception {
        applyNodeRefresh(Node, bAffinityNodeTitlesOnly, -1, null); }
    public void applyNodeRefresh(Node Node, boolean bAffinityNodeTitlesOnly,
                                 int iMaxDocsPerNode) throws Exception {
        applyNodeRefresh(Node, bAffinityNodeTitlesOnly, iMaxDocsPerNode, null); }
    public void applyNodeRefresh(Node Node, boolean bAffinityNodeTitlesOnly,
                                 int iMaxDocsPerNode, String sRepositoryID) throws Exception {

        Hashtable htArgs = getArguments();
        htArgs.put("NODEID", Node.get("NODEID"));
        if (bAffinityNodeTitlesOnly) { htArgs.put("PropAffinityNodeTitlesOnly", "true"); }
        if (iMaxDocsPerNode < 1000) { htArgs.put("MAXDOCSPERNODE", iMaxDocsPerNode+""); }
        else { htArgs.put("MAXDOCSPERNODE", "-1"); }
        if (sRepositoryID != null) { htArgs.put("REPOSITORYID", sRepositoryID); }

        try {
            HashTree htResults = InvokeAPI("tsclassify.TSNodeRefreshApply", htArgs);
            if (!htResults.containsKey("NUMNODESDONE")) { throw new Exception("Success tag missing from return."); }
        } catch (Exception e) { throw e; }
    }

    // Get the root node of a corpus
    // Arguments: CorpusID
    // Returns: NODE object
    public Node getCorpusRoot(String CorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSGetCorpusRoot", htArgs, true, false);

            if (!htResults.containsKey("NODES")) {
                throw new Exception("Corpus ID " + CorpusID + " does not have a root node.");
            }

            HashTree htNodes = (HashTree) htResults.get("NODES");
            HashTree htNode = (HashTree) htNodes.get("NODE");

            return new Node(htNode);

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getCorpora() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        // invoke the API
        try {
            Vector vResults = InvokeVectorAPI("tscorpus.TSListCorpora", htArgs);

            if (vResults.size() < 1) {
                System.out.println("Sorry: "+vResults.size()+" elements found.");
                throw new Exception("There are no taxonomies loaded in this system.");
            }

            Vector vCorpora = (Vector) vResults.elementAt(0);

            Enumeration eC = vCorpora.elements();
            while (eC.hasMoreElements()) {
                Vector vCorpus = (Vector) eC.nextElement();
                Corpus c = new Corpus(vCorpus);

                v.add(c); // add this node to the vector
            }

            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public Corpus getCorpora(String sCorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", sCorpusID);
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSGetCorpusProps", htArgs);

            if (!htResults.containsKey("CORPUS")) {
                throw new Exception("There are no taxonomies loaded with corpus id " + sCorpusID + ".");
            }

            HashTree htCorpus = (HashTree) htResults.get("CORPUS");
            Corpus c = new Corpus(htCorpus);

            return c;
        } catch (Exception e) {
            throw e;
        }
    }

    public Corpus editCorpusProps (Corpus c) throws Exception {
        c.setDescription(c.getDescription().replaceAll("\n", " "));

        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("CorpusName", c.getName());
        htArgs.put("CorpusDesc", c.getDescription());
        htArgs.put("CorpusActive", c.getActive());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSEditCorpusProps", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Attempt to modify corpus "+c.getName()+" failed.");
            }

            return c;
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeCorpusFolder (Corpus c) throws Exception {
        removeCorpusFolder (c, null); }
    public void removeCorpusFolder (Corpus c, Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (g != null) htArgs.put("FolderID", g.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            InvokeAPI("tsgenre.TSRemoveCorpusFolder", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addCorpusFolder (Corpus c, Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("FolderID", g.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            InvokeAPI("tsgenre.TSAddCorpusFolder", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeCorpusSubject (Corpus c) throws Exception {
        removeCorpusSubject (c, null); }
    public void removeCorpusSubject (Corpus c, SubjectArea sa) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (sa != null) htArgs.put("SubjectAreaID", sa.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tssubjectarea.TSRemoveCorpusSubject", htArgs);
            //if (!ht.containsKey("SUCCESS")) { throw new Exception("Could not remove corpus subject areas."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addCorpusSubject (Corpus c, SubjectArea sa) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("SubjectAreaID", sa.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tssubjectarea.TSAddCorpusSubject", htArgs);
            if (!ht.containsKey("SUCCESS")) { throw new Exception("Could not add corpus subject area."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeCorpusThesaurus (Corpus c) throws Exception {
        removeCorpusThesaurus (c, null); }
    public void removeCorpusThesaurus (Corpus c, Thesaurus t) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (t != null) htArgs.put("ThesaurusID", t.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tsthesaurus.TSRemoveThesaurusCorpus", htArgs);
            //if (!ht.containsKey("SUCCESS")) { throw new Exception("Could not remove corpus subject areas."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addCorpusThesaurus (Corpus c, Thesaurus t) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("ThesaurusID", t.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tsthesaurus.TSAddThesaurusCorpus", htArgs);
            if (!ht.containsKey("SUCCESS")) { throw new Exception("Could not add thesaurus relationship area."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addNodeDocument (Node n, Document d, String Score, String Summary) throws Exception {
        addNodeDocument(n.get("NODEID"), d.get("DOCUMENTID"), Score, Summary); }
    public void addNodeDocument (NodeDocument nd) throws Exception {
        addNodeDocument(nd.get("NODEID"), nd.get("DOCUMENTID"), nd.get("SCORE1"), nd.get("DOCSUMMARY")); }
    public void addNodeDocument (NodeDocument nd, boolean bRemoveFirst) throws Exception {
        addNodeDocument(nd.get("NODEID"), nd.get("DOCUMENTID"), nd.get("SCORE1"), nd.get("DOCSUMMARY"), bRemoveFirst); }
    public void addNodeDocument (String NodeID, String DocID, String Score, String Summary) throws Exception {
        addNodeDocument(NodeID, DocID, Score, Summary, true); }
    public void addNodeDocument (String NodeID, String DocID, String Score, String Summary, boolean bRemoveFirst) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocID);
        htArgs.put("NodeID", NodeID);
        htArgs.put("permanent", "true");

        //System.out.println("insert into nodedocument: did: "+DocID+" nid: "+NodeID+" score1: "+Score);
        try { // invoke the API

            HashTree htResults;

            if (bRemoveFirst) { htResults = InvokeAPI("tsnode.TSRemoveNodeDocument", htArgs); }

            htArgs = getArguments();
            htArgs.put("DocumentID", DocID);
            htArgs.put("NodeID", NodeID);
            htArgs.put("docsummary", Summary);
            htArgs.put("score1", Score);

            htResults = InvokeAPI("tsdocument.TSAddNodeDocument", htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) { throw new NotAuthorized(); }
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getThesauri() throws Exception {
        return getThesauri(null); }
    public Vector getThesauri(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();

        if (c != null) htArgs.put("CorpusID", c.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults;
            if (c == null) { htResults = InvokeAPI("tsthesaurus.TSGetThesaurusList", htArgs); }
            else { htResults = InvokeAPI("tscorpus.TSGetCorpusThesaurus", htArgs); }

            if (!htResults.containsKey("THESAURI")) {
                return new Vector(); //throw new Exception("There are no thesauri loaded in this system.");
            }

            HashTree htThesauri = (HashTree) htResults.get("THESAURI");
            Enumeration eT = htThesauri.elements();
            while (eT.hasMoreElements()) {
                HashTree htThesaurus = (HashTree) eT.nextElement();
                Thesaurus t = new Thesaurus(htThesaurus);

                v.add(t);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getThesaurusWords(String ThesaurusID) throws Exception {
        return getThesaurusWords(ThesaurusID, null);
    }

    public Vector getThesaurusWords(String ThesaurusID, String Like) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        if (Like != null) {
            htArgs.put("Like", Like);
        }
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsthesaurus.TSGetWords", htArgs);

            if (!htResults.containsKey("WORDS")) {
                throw new Exception("There are no thesaurus words loaded in this thesaurus.");
            }

            HashTree htThesWords = (HashTree) htResults.get("WORDS");
            Enumeration eT = htThesWords.elements();
            while (eT.hasMoreElements()) {
                HashTree htWord = (HashTree) eT.nextElement();
                ThesaurusWord tw = new ThesaurusWord(htWord);

                v.add(tw);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getNodeSignatures(Node n) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        htArgs.put("NodeID", n.get("NODEID"));

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSGetNodeSigs", htArgs);

            if (!htResults.containsKey("SIGNATURES")) {
                return new Vector();
            }

            HashTree htSignatures = (HashTree) htResults.get("SIGNATURES");
            Enumeration eS = htSignatures.elements();
            while (eS.hasMoreElements()) {
                HashTree htSignature = (HashTree) eS.nextElement();
                Signature s = new Signature(htSignature);

                v.add(s);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getSynonyms(String ThesaurusID, String Word) {
        try {
            Vector vWords = getThesaurusWords(ThesaurusID, Word);
            if (vWords.size() == 0) { return new Vector(); } // word may not exist yet

            ThesaurusWord tw = (ThesaurusWord) vWords.elementAt(0);

            return getSynonyms(ThesaurusID, new Integer(tw.getID()).intValue());  }
        catch (Exception e) { return new Vector(); } // none were found, return empty vector
    }
    public Vector getSynonyms(String ThesaurusID, int WordID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("WordID", ""+WordID);
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsthesaurus.TSGetWordProps", htArgs);

            if (!htResults.containsKey("WORDS")) {
                throw new Exception("There are no thesaurus words loaded in this thesaurus.");
            }

            HashTree htThesWords = (HashTree) htResults.get("WORDS");
            Enumeration eT = htThesWords.elements();
            while (eT.hasMoreElements()) {
                HashTree htWord = (HashTree) eT.nextElement();
                ThesaurusWord tw = new ThesaurusWord(htWord);

                v.add(tw);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public void removeAnchorTerm(String ThesaurusID, String WordID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("WordID", WordID);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSRemoveWordAnchor", htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) { throw new NotAuthorized(); }
            if (htResults.containsKey("DEBUG")) {
                throw new Exception("Thesaurus term could not be removed, see log file for more details.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeRelationship(String ThesaurusID, String AnchorID,
                                   String ObjectID, String Relationship) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("WordID1", AnchorID);
        htArgs.put("WordID2", ObjectID);
        htArgs.put("Relationship", Relationship);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSRemoveWordRelationship", htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) { throw new NotAuthorized(); }
            if (htResults.containsKey("DEBUG")) {
                throw new Exception("Thesaurus relationship could not be removed, see log file for more details.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addRelationship(String ThesaurusID, String AnchorName,
                                String ObjectName, String Relationship) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("Word1", AnchorName);
        htArgs.put("Word2", ObjectName);
        htArgs.put("Relationship", Relationship);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSAddWordRelationship", htArgs, false);

            if (htResults.containsKey("NOTAUTHORIZED")) { throw new NotAuthorized(); }
            if (htResults.containsKey("DEBUG")) {
                throw new Exception((String) htResults.get("DEBUG"));
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Genre addFolder(Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("FolderName", g.getName());

        if (g.getActive()) { htArgs.put("FolderStatus", "1"); }
        else { htArgs.put("FolderStatus", "0"); }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsgenre.TSCreateFolder", htArgs);

            if (!htResults.containsKey("GENRE")) {
                throw new Exception("Could not create a new folder.");
            }

             g = new Genre((HashTree) htResults.get("GENRE"));

            return g;
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeFolder(Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("FolderID", g.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsgenre.TSRemoveFolder", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Could not remove a folder.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void editRelationship(String OldName, String NewName) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("OldName", OldName);
        htArgs.put("NewName", NewName);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSEditWordName", htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) { throw new NotAuthorized(); }
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Thesaurus relationship could not be remamed, see log file for more details.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Thesaurus createThesaurus(String ThesaurusName) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusName", ThesaurusName);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSCreateThesaurus", htArgs);

            if (htResults.containsKey("DEBUG")) {
                throw new Exception((String) htResults.get("DEBUG"));
            }

            return new Thesaurus((HashTree) htResults.get("THESAURUS"));
        } catch (Exception e) {
            throw e;
        }
    }

    public void synchronizeThesaurus() throws Exception {
        Hashtable htArgs = getArguments();

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSThesaurusSynch", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addRefreshNode(String NodeID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsclassify.TSAddRefreshNode", htArgs);
            if (!htResults.containsKey("SUCCESS")) { throw new Exception("API level failure."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeRefreshNode(String NodeID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsclassify.TSDeleteRefreshNodeList", htArgs);
            if (!htResults.containsKey("SUCCESS")) { throw new Exception("API level failure."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getRefreshNodeList() throws Exception {
        return getRefreshNodeList(null); }
    public Vector getRefreshNodeList(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) htArgs.put("CorpusID", c.getID());

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsclassify.TSGetRefreshNodes", htArgs);
            if (!htResults.containsKey("NODES")) { return new Vector(); }

            // Process NODE RESULTS
            HashTree htNodes = (HashTree) htResults.get("NODES");

            Enumeration eN = htNodes.elements();
            while (eN.hasMoreElements()) {
                HashTree htNode = (HashTree) eN.nextElement();
                Node n = new Node(htNode);

                v.add(n); // add this node to the vector
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public String getNodeCorpus(String NodeID) throws Exception {
        Node n = getNodeProps(NodeID);
        return n.get("CORPUSID");
    }

   public Vector reviewBatchResults () {
        Hashtable htArgs = getArguments();

        Vector v = new Vector();

        try {
            Hashtable htResults = InvokeAPI("tsclassify.TSReviewBatchResults", htArgs);
            if (!htResults.containsKey("NODEDOCUMENTS")) {
                api.Log.Log("No results found for TSReviewBatchResults.");
                return new Vector();
            }

            HashTree htNodes = (HashTree) htResults.get("NODEDOCUMENTS");
            Enumeration eH = htNodes.elements();
            while (eH.hasMoreElements()) {
                HashTree htNode = (HashTree) eH.nextElement();
                NodeDocument nd = new NodeDocument(htNode);

                v.add(nd);
            }
        } catch (Exception e) { e.printStackTrace(System.out);  }

        return v;
    }

    // TSListAllNodesWithChildCount
    public Vector getNodesWithChildren (Corpus c) throws Exception {
        return getNodesWithChildren (c, -1); }
    public Vector getNodesWithChildren (Corpus c, int Depth) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (Depth >= 0) { htArgs.put("Depth", Depth+""); }

        Vector v = new Vector();

        try {
            Hashtable htResults = InvokeAPI("tsnode.TSListAllNodesWithChildCount", htArgs);
            if (!htResults.containsKey("NODES")) {
                throw new Exception("Invalid corpus identifier specified.");
            }

            HashTree htNodes = (HashTree) htResults.get("NODES");
            Enumeration eH = htNodes.elements();
            while (eH.hasMoreElements()) {
                HashTree htNode = (HashTree) eH.nextElement();
                Node n = new Node(htNode);
                n.set("CLIENTID", (String) htNode.get("COUNT"));

                v.add(n);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out); throw e; }

        return v;
    }

    // TSListAllNodesWithSigCount
    public Vector getNodesWithSignatures (Corpus c) throws Exception {
        return getNodesWithChildren (c, -1); }
    public Vector getNodesWithSignatures (Corpus c, int Depth) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (Depth >= 0) { htArgs.put("Depth", Depth+""); }

        Vector v = new Vector();

        try {
            Hashtable htResults = InvokeAPI("tsnode.TSListAllNodesWithSigCount", htArgs);
            if (!htResults.containsKey("NODES")) {
                throw new Exception("Invalid corpus identifier specified.");
            }

            HashTree htNodes = (HashTree) htResults.get("NODES");
            Enumeration eH = htNodes.elements();
            while (eH.hasMoreElements()) {
                HashTree htNode = (HashTree) eH.nextElement();
                Node n = new Node(htNode);
                n.set("CLIENTID", (String) htNode.get("COUNT"));

                v.add(n);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out); throw e; }

        return v;
    }

    public Vector getNodesWithDocuments (Corpus c) throws Exception {
        return getNodesWithDocuments(c, -1); }
    public Vector getNodesWithDocuments (Corpus c, int Depth) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (Depth >= 0) { htArgs.put("Depth", Depth+""); }

        Vector v = new Vector();

        try {
            Hashtable htResults = InvokeAPI("tsnode.TSListAllNodesWithCount", htArgs);
            if (!htResults.containsKey("NODES")) return new Vector(); // no results found

            HashTree htNodes = (HashTree) htResults.get("NODES");
            Enumeration eH = htNodes.elements();
            while (eH.hasMoreElements()) {
                HashTree htNode = (HashTree) eH.nextElement();
                Node n = new Node(htNode);
                n.set("CLIENTID", (String) htNode.get("COUNT"));

                v.add(n);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out); throw e; }

        return v;
    }

    // Given a NODEID, get the node properties
    // Arguments: NodeID
    // Returns: NODE object

    public Node getNodeProps(String NodeID) throws Exception {
        String sCQL = "SELECT <NODE> WHERE NODEID = "+NodeID;

        Vector vNode = CQL(sCQL);
        if (vNode.size() == 0) { throw new Exception("NODE ID " + NodeID + " does not exist."); }
        else { return (Node) vNode.elementAt(0); }
    }

    public Vector getNodeTree(String NodeID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        Vector v = new Vector(); // return struct

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSGetNodeTree", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) return new Vector();

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                Node n = new Node(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getRelatedNodes(String DocumentID, String DocURL)
            throws Exception {
        return getRelatedNodes(DocumentID, DocURL, "50.0");
    }

    public Vector getRelatedNodes(String DocumentID, String DocURL, String ScoreThreshold) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        htArgs.put("DocURL", DocURL);
        htArgs.put("ScoreThreshold", ScoreThreshold);

        Vector v = new Vector(); // return struct

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsdocument.TSGetRelatedNodes", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("NODEDOCUMENTS");
            if (eNodes == null) return new Vector();

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                Node n = new Node(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getCorpusNodeContents(String CorpusID, String NodeID, String Depth) {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);
        if (NodeID != null) htArgs.put("NodeID", NodeID);
        if (Depth != null) htArgs.put("MaxDepth", Depth);

        Vector v = new Vector();

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscorpus.TSListCorpusNodeContents", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) return new Vector();

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                Node n = new Node(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            api.Log.LogError(e);
            return new Vector();
        }

    }

    public Vector getDocumentsBySignature(String SignatureWord, String Frequency, String Genre) {
        Hashtable htArgs = getArguments();
        htArgs.put("Signature", SignatureWord);
        if (Frequency != null) htArgs.put("Frequency", Frequency);
        if (Genre != null) htArgs.put("GenreID", Genre);

        Vector v = new Vector();

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsdocument.TSGetDocumentBySignature", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("DOCUMENTS");
            if (eDocs == null) return new Vector();

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                Iterator i2 = eDoc.elements().iterator();
                Document d = new Document(i2);

                v.add(d);
            }

            return v;
        } catch (Exception e) {
            api.Log.LogError(e);
            return new Vector();
        }
    }

    public Vector getDocumentsByNodes(String NodeList, String Genre) {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeList", NodeList);
        if ((Genre != null) && (!Genre.equals("0"))) { htArgs.put("GenreID", Genre); }

        Vector v = new Vector();

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsdocument.TSGetDocumentsByNodes", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("DOCUMENTS");
            if (eDocs == null) return new Vector();

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                Iterator i2 = eDoc.elements().iterator();
                Document d = new Document(i2);

                v.add(d);
            }

            return v;
        } catch (Exception e) {
            api.Log.LogError(e);
            return new Vector();
        }
    }


    public Document getDocProps(String DocumentID) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCUMENTID = " + DocumentID);
        if (v.size() == 0) {
            throw new Exception("No documents found.");
        }
        return (Document) v.elementAt(0);
    }

    public Document getDocPropsByURL(String DocURL) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCURL = '" + DocURL + "'");
        if (v.size() == 0) {
            throw new Exception("No documents found.");
        }
        return (Document) v.elementAt(0);
    }

    public Vector getSimilarDocuments(String DocumentID, String GenreID, String Date) throws Exception {
        return getSimilarDocuments(DocumentID, GenreID, Date, true); }
    public Vector getSimilarDocuments(String DocumentID, String GenreID, String Date,  boolean useCache) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        if (useCache) htArgs.put("UseCache", "1");
        if (GenreID != null) { htArgs.put("GenreID", GenreID); }
        if (Date != null) { htArgs.put("Date", Date); }

        Vector v = new Vector();

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsdocument.TSGetSimilarDocumentsByDocumentID", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("DOCUMENTS");
            if (eDocs == null) return new Vector();

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                Iterator i2 = eDoc.elements().iterator();
                Document d = new Document(i2);

                v.add(d);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Document getDocPropsLikeURL(String DocURL) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCURL LIKE '%%" + DocURL + "%%'");
        if (v.size() == 0) {
            throw new Exception("No documents found.");
        }
        return (Document) v.elementAt(0);
    }

    public Document addDocument(String DocumentURL, Hashtable NameValPairs) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocURL", DocumentURL);

        Enumeration eNV = NameValPairs.keys();
        while (eNV.hasMoreElements()) {
            String sKey = (String) eNV.nextElement();
            String sVal = (String) NameValPairs.get(sKey);

            htArgs.put(sKey, sVal);
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSAddDocument", htArgs);

            if (!htResults.containsKey("DOCUMENT")) {
                return (Document) getDocPropsByURL(DocumentURL);
            }
            return new Document((HashTree) htResults.get("DOCUMENT"));

        } catch (Exception e) {
            throw e;
        }
    }

    public Document editDocument(Document d) throws Exception {
        Hashtable NameValPairs = new Hashtable();
        if ((!d.get("GENREID").equals("null")) && (!d.get("GENREID").equals(""))) {
            NameValPairs.put("genreid", d.get("GENREID"));
        }
        if ((!d.get("DOCTITLE").equals("null")) && (!d.get("DOCTITLE").equals(""))) {
            NameValPairs.put("doctitle", d.get("DOCTITLE"));
        }
        if ((!d.get("DOCUMENTSUMMARY").equals("null")) && (!d.get("DOCUMENTSUMMARY").equals(""))) {
            NameValPairs.put("documentsummary", d.get("DOCUMENTSUMMARY"));
        }
        if ((!d.get("DOCURL").equals("null")) && (!d.get("DOCURL").equals(""))) {
            NameValPairs.put("docurl", d.get("DOCURL"));
        }
        if ((!d.get("DATELASTFOUND").equals("null")) && (!d.get("DATELASTFOUND").equals(""))) {
            NameValPairs.put("datelastfound", d.FixDate(d.get("DATELASTFOUND")));
        }
        if ((!d.get("VISIBLE").equals("null")) && (!d.get("VISIBLE").equals(""))) {
            NameValPairs.put("visible", d.get("VISIBLE"));
        }

        return editDocument(d.get("DOCUMENTID"), NameValPairs);
    }

    public Document editDocument(String DocumentID, Hashtable NameValPairs) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);

        Enumeration eNV = NameValPairs.keys();
        while (eNV.hasMoreElements()) {
            String sKey = (String) eNV.nextElement();
            String sVal = (String) NameValPairs.get(sKey);

            htArgs.put(sKey, sVal);
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSEditDocument", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Document modification failed.");
            }
            return (Document) getDocProps(DocumentID);

        } catch (Exception e) {
            throw e;
        }
    }

    public void editNodeDocument(String DocumentID, String NodeID, String DocURL, String DocTitle, String DocSummary, String Score)
            throws Exception {        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        htArgs.put("NodeID", NodeID);
        htArgs.put("Score1", Score);
        htArgs.put("DocTitle", DocTitle);
        htArgs.put("DocURL", DocURL);
        htArgs.put("DocSummary", DocSummary);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSEditDocProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public Node addNode(Node n) throws Exception {
        Hashtable htArguments = getArguments();

        htArguments.put("ParentID", n.get("PARENTID"));
        htArguments.put("NodeSize", n.get("NODESIZE"));
        htArguments.put("NodeDesc", n.get("NODEDESC"));
        htArguments.put("NodeTitle", n.get("NODETITLE"));
        htArguments.put("CorpusID", n.get("CORPUSID"));

        if ((n.get("NODEID") != null) && (!n.get("NODEID").equals(""))) {
            if (!n.get("NODEID").equals("-1")) htArguments.put("LinkNodeID", n.get("LINKNODEID"));
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSAddNode", htArguments);
            return getNodeProps((String) htResults.get("NODEID"));
        } catch (NotAuthorized ae) { throw ae;
        } catch (Exception e) { throw e; }
    }

    public void addNodeFromTopicMap(Node n) throws Exception {
        Hashtable htArguments = getArguments();

        htArguments.put("NodeID", n.get("NODEID"));
        htArguments.put("ParentID", n.get("PARENTID"));
        htArguments.put("NodeSize", n.get("NODESIZE"));
        //htArguments.put("NodeDesc", n.get("NODEDESC"));
        htArguments.put("NodeTitle", n.get("NODETITLE"));
        htArguments.put("CorpusID", n.get("CORPUSID"));
        htArguments.put("NodeIndexWithinParent", n.get("NODEINDEXWITHINPARENT"));
        htArguments.put("DepthFromRoot", n.get("DEPTHFROMROOT"));

        if (n.get("LINKNODEID") != null) {
            if (!n.get("LINKNODEID").equals("-1")) htArguments.put("LinkNodeID", n.get("LINKNODEID"));
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSAddNode", htArguments);
        } catch (NotAuthorized ae) { throw ae;
        } catch (Exception e) { throw e; }

        saveSignatures(n, n.getSignatures(), false);
    }

    public Node emptyNode(Node Parent) {
        Node n = new Node();
        n.set("NODEID","-1");
        n.set("PARENTID",Parent.get("NODEID"));
        n.set("NODESIZE","100");
        n.set("NODEDESC"," ");
        n.set("NODETITLE","Untitled Topic");
        n.set("CORPUSID",Parent.get("CORPUSID"));

        return n;
    }

    public Node emptyNode(String sCorpusID) {
        return emptyNode(sCorpusID, "Untitled Topic");
    }

    public Node emptyNode(String sCorpusID, String sNodeTitle) {
        Node n = new Node();
        n.set("PARENTID", "-1");
        n.set("NODESIZE","1");
        n.set("NODEDESC"," ");
        n.set("NODETITLE",sNodeTitle);
        n.set("CORPUSID",sCorpusID);

        return n;
    }

    public void setCorpusAdminSecurity (Corpus c, SecurityGroup g) throws Exception {
        Hashtable htArguments = getArguments();

        // after corpus is created, add the admin group
        htArguments.put("CorpusID", c.getID());
        htArguments.put("Access", "1");
        htArguments.put("GroupID", g.getID()); // Admin group
        HashTree htResults = InvokeAPI ("tscorpus.TSEditCorpusAccess", htArguments);

        if (!htResults.containsKey("SUCCESS")) { throw new Exception("cannot change corpus permissions"); }

        return;
    }

    public void setCorpusReadSecurity (Corpus c, SecurityGroup g) throws Exception {
        Hashtable htArguments = getArguments();

        // after corpus is created, add the admin group
        htArguments.put("CorpusID", c.getID());
        htArguments.put("Access", "0");
        htArguments.put("GroupID", g.getID()); // Admin group
        HashTree htResults = InvokeAPI ("tscorpus.TSEditCorpusAccess", htArguments);

        if (!htResults.containsKey("SUCCESS")) { throw new Exception("cannot change corpus permissions"); }

        return;
    }

    public void revokeCorpusSecurity (Corpus c, SecurityGroup g) throws Exception {
        Hashtable htArguments = getArguments();

        // after corpus is created, add the admin group
        htArguments.put("CorpusID", c.getID());
        htArguments.put("Access", "-1");
        htArguments.put("GroupID", g.getID()); // Admin group
        HashTree htResults = InvokeAPI ("tscorpus.TSEditCorpusAccess", htArguments);

        if (!htResults.containsKey("SUCCESS")) { throw new Exception("cannot change corpus permissions"); }

        return;
    }

    public Node copyNode(Node n) throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("NodeID", n.get("NODEID"));
        htArguments.put("ParentID", n.get("PARENTID"));

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSCopyNode", htArguments);
            if (!htResults.containsKey("NODEID")) {
                throw new Exception("error: attempt to copy a node that was deleted"); }
            return getNodeProps((String) htResults.get("NODEID"));
        } catch (Exception e) {
            throw e;
        }
    }

    public void organizeIndexes(String ParentID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", ParentID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSOrganizeNodeIndexByParent", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                System.out.println("Could not reorganize the indexes.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNodeParent(String NodeID, String ParentID)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("ParentID", ParentID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public Node editNodeParent(String NodeID, String ParentID, String Index)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("ParentID", ParentID);
        htArgs.put("NodeIndexWithinParent", Index);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
            if (!htResults.containsKey("SUCCESS")) { throw new Exception("could not edit node properties"); }
            return getNodeProps(NodeID);
        } catch (Exception e) {
            throw e;
        }
    }

    public Node editNodeParentCorpus(String NodeID, String ParentID, String Index, String CorpusID)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("ParentID", ParentID);
        htArgs.put("NodeIndexWithinParent", Index);
        htArgs.put("CorpusID", CorpusID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
            return getNodeProps(NodeID);
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNodeIndex(String NodeID, int Index) throws Exception {
        editNodeIndex(NodeID, Index + "");
    }

    public void editNodeIndex(String NodeID, String Index)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("NodeIndexWithinParent", Index);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNode(Node n) throws Exception {
        editNode(n.get("LINKNODEID"), n.get("NODETITLE"), n.get("NODEDESC"), n.get("NODESIZE"), n.get("NODESTATUS"));
    }

    public void editNode(String NodeID, String NodeTitle, String NodeDesc, String NodeSize, String NodeStatus)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("NodeTitle", NodeTitle);
        htArgs.put("NodeDesc", NodeDesc);
        htArgs.put("NodeSize", NodeSize);
        htArgs.put("NodeStatus", NodeStatus);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteNode(Node n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("LINKNODEID"));
        htArgs.put("permanently", "true");
        htArgs.put("recursive", "true");

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSDeleteNode", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean deleteDocument(Document d) throws Exception {
        return deleteDocument(d.get("DOCUMENTID")); }
    public boolean deleteDocument(String sDocumentID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", sDocumentID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSDeleteDocument", htArgs);
            if (htResults.containsKey("SUCCESS")) return true;
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    public void buildSignatures(File f, Node n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("post", "true");

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsnode.TSCreateSigsFromDoc", htArgs);
            HashTree htResults = API.PostExecute(f);
        } catch (Exception e) {
            throw e;
        }
    }

    public void purgeConceptCache(Document d) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ConceptID", d.get("DOCUMENTID"));

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tspurge.TSPurgeConceptCache", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void alphabetizeChildren(Node n) throws Exception {
        alphabetizeChildren(n, false);
    }

    public void alphabetizeChildren(Node n, boolean Recurse) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        if (Recurse) htArgs.put("recursive", "true");

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSAlphabetizeChildren", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveFolder(Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("FolderID", g.getID());
        htArgs.put("FolderName", g.getName());

        String sFolderStatus = "1";
        if (!g.getActive()) { sFolderStatus = "0"; }

        htArgs.put("FolderStatus", sFolderStatus);

        try {
            HashTree htResults = InvokeAPI("tsgenre.TSEditGenreProps", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Signature removal failed");
            }
        } catch (Exception e) { System.out.println("could not edit genre props for genre "+g.getID()); throw e; }
    }

    public void purgeSignatures(String CorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);

        // http://ITSSERVER/itsapi/ts?fn=&SKEY=-132981656
        try {
            HashTree htResults = InvokeAPI("tspurge.TSPurgeSigCache", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("System could not purge this signature cache file.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveSignatures(String NodeID, Vector Signatures) throws Exception {
        saveSignatures(NodeID, Signatures, true); }
    public void saveSignatures(Node n, Vector Signatures) throws Exception {
        saveSignatures(n.get("NODEID"), Signatures, true); }
    public void saveSignatures(Node n, Vector Signatures, boolean bRemoveFirst) throws Exception {
        saveSignatures(n.get("NODEID"), Signatures, bRemoveFirst); }
    public void saveSignatures(String NodeID, Vector Signatures, boolean bRemoveFirst) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        if (bRemoveFirst) {
            try {
                HashTree htResults = InvokeAPI("tsnode.TSRemoveSignatures", htArgs);
                if (!htResults.containsKey("SUCCESS")) {
                    throw new Exception("Signature removal failed");
                }
            } catch (Exception e) {
                //System.out.println("NodeID: " + NodeID + " has no signatures to be removed.  That's okay.");
            }
        }

        if (Signatures.size() > 0) {
            String str = "";
            for (int i = 0; i < Signatures.size(); i++) {
                Signature s = (Signature) Signatures.elementAt(i);
                if (i != 0) { str = str + ","; }
                str = str + s.getWord()+"||"+s.getWeight();
            }
            htArgs.put("Signatures", str);
            HashTree htResults = InvokeAPI("tsnode.TSAddNodeSignatures", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Signature addition failed");
            }
        }
    }

    public void deleteNodeDocument(String DocumentID, String NodeID)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        htArgs.put("NodeID", NodeID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSRemoveNodeDocument", htArgs);
            if (!htResults.containsKey("SUCCESS")) { throw new Exception("remove relationship failed"); }

        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteNodeDocuments(String DocumentID) throws Exception {
        deleteNodeDocuments(DocumentID, false); }
    public void deleteNodeDocuments(String DocumentID, boolean bPermanent)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        if (bPermanent) htArgs.put("Permanent", "true");

        // invoke the API
        try {
            InvokeAPI("tsnode.TSRemoveNodeDocuments", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector CQL(String Query) throws Exception {
        return CQL(Query, 1, 500);
    }

    public Vector CQL(String Query, int Start, int RowMax) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("query", Query);
        htArgs.put("start", "" + Start);
        htArgs.put("rowmax", "" + RowMax);

        Vector v = new Vector();
        System.out.println("CQL: "+Query);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscql.TSCql", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            // return different structures depending upon NODE or DOCUMENT
            if (Query.toUpperCase().indexOf("<NODE>") != -1) {
                Element eNodes = elemRoot.element("NODES");
                if (eNodes == null) return new Vector();

                Iterator i = eNodes.elements().iterator();
                while (i.hasNext()) {
                    Element eNode = (Element) i.next();

                    Iterator i2 = eNode.elements().iterator();
                    Node n = new Node(i2);

                    v.add(n);
                }
            } else if ((Query.toUpperCase().indexOf("<DOCUMENT>") != -1) ||
                       (Query.toUpperCase().indexOf("<NARRATIVE>") != -1)) {
                Element eDocs = elemRoot.element("DOCUMENTS");
                if (eDocs == null) return new Vector();

                Iterator i = eDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eDoc = (Element) i.next();

                    Iterator i2 = eDoc.elements().iterator();
                    com.iw.system.Document d = new com.iw.system.Document(i2);

                    v.add(d);
                }
            } else if (Query.toUpperCase().indexOf("<NODEDOCUMENT>") != -1) {
                Element eNodeDocs = elemRoot.element("NODEDOCUMENTS");
                if (eNodeDocs == null) return new Vector();

                Iterator i = eNodeDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eNodeDoc = (Element) i.next();

                    Iterator i2 = eNodeDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            } else {
                throw new Exception("The CQL selection object was invalid.");
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public TableModel tsThesaurusNodeReport ( Hashtable htArgs  ) throws Exception {

        Hashtable htArgsMerge = mergeHashTables( getArguments(), htArgs, true);

        Vector v = new Vector();

        try {
            InvokeAPI API = new InvokeAPI("tsthesaurus.TSThesaurusNodeReport", htArgsMerge);
            org.dom4j.Document docExecuteSqlResult = API.dExecute(false, false);
            Element elemRoot = docExecuteSqlResult.getRootElement();
            Element elem_DATATABLE = elemRoot.element("OBJECT_TABLEMODEL_BEANXML");
            if ( elem_DATATABLE == null )
                throw new Exception("System error: tsthesaurus.TSThesaurusNodeReport returns no tablemodel." );

            String sXMLized_DATATABLE = elem_DATATABLE.getText().trim();
            Vector vTableModelXMLizedFormReturn = (Vector) XMLBeanSerializeHelper.deserializeFromString(sXMLized_DATATABLE.trim());
            TableModelXMLBeanSerializable tableModelIWSerializable = new TableModelXMLBeanSerializable();
            tableModelIWSerializable.setFromIWXMLData(new ByteArrayInputStream(sXMLized_DATATABLE.getBytes()));

            return tableModelIWSerializable;
        } catch (Exception e) {
            throw e;
        }
    }

    // take a text file and wrap it with HTML
    public static void wrapHTMLstatic(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        StringBuilder sOut = new StringBuilder();
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut.append(sData);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>None</TITLE></HEAD>\n<BODY>");
            out.print(sOut.toString());
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public static void wrapHTMLstatic (File f, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        StringBuilder sOut = new StringBuilder();
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut.append(sData).append("\r\n");
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(sOut);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        StringBuilder sOut = new StringBuilder();
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut.append(sData);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>None</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public File wrapHTML(String title, String buffer) throws Exception {
        PrintWriter out = null;

        File tempFile = new File("file.html");
        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>"+title+"</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(buffer);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }

        return tempFile;
    }

    public static void wrapHTMLstatic(String s, File tempFile) throws Exception {
        PrintWriter out = null;

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(String s, File tempFile) throws Exception {
        PrintWriter out = null;

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public static void wrapHTML(File f, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        StringBuilder sOut = new StringBuilder();
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut.append(sData).append("\r\n");
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(sOut);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    // 0: contains, 1: starts with, 2: ends with
    public Vector topicSearch(String SearchTerm, int Type) throws Exception {
        String query = SearchTerm;

        switch (Type) {
            case 0:
                query = "%%" + query + "%%";
                break;
            case 1:
                query = query + "%%";
                break;
            case 2:
                query = "%%" + query;
                break;
        }

        query = query.replaceAll("'", "''");
        query = "SELECT <NODE> WHERE NODETITLE LIKE '" + query + "' ORDER BY NODETITLE ASC";
        return CQL(query, 1, 500);
    }

    public Vector topicSearch(String SearchTerm, int Type, Corpus corpus) throws Exception {
        String query = SearchTerm;

        switch (Type) {
            case 0:
                query = "%%" + query + "%%";
                break;
            case 1:
                query = query + "%%";
                break;
            case 2:
                query = "%%" + query;
                break;
        }

        query = query.replaceAll("'", "''");
        query = "SELECT <NODE> WHERE NODETITLE LIKE '" + query + "' AND CORPUSID = " + corpus.getID() + " ORDER BY NODETITLE ASC";
        return CQL(query, 1, 500);
    }

    public Vector getAncestorTree(Node n) throws Exception {
        return getAncestorTree(n.get("LINKNODEID"));
    }
    public Vector getAncestorTree(String sNodeID) throws Exception {
        Vector v = new Vector(); // return struct

        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", sNodeID);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSGetNodeTree", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) return new Vector();

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                Node n = new Node(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    // classify: using the HTTP POST method
    public Vector classify (File f, boolean bPost, String sCorpora, String sPath) throws Exception {
        File tempFile = null; Vector v = new Vector(); boolean bUsingTempFile = false;

         // if the file is of type text, it needs an HTML wrapper before being posted
         if (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
             tempFile = new File("temp.html");
             if (tempFile.exists()) {
                 tempFile.delete();
             }
             System.out.println("Filtering: " + f.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

             try {
                 wrapHTML(f, tempFile); bUsingTempFile = true;
             } catch (Exception ex) {
                 ex.printStackTrace(); throw ex;
             }
         }
         // if the file is of type PDF, filter it before posting
         else if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
             bUsingTempFile = true;
             tempFile = ITS.PDFtoHTML(f,null);
         } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
             bUsingTempFile = true;
             tempFile = ITS.MSWORDtoHTML(f,null);
         } else { tempFile = f; bUsingTempFile = false; }

        Hashtable htArgs = getArguments();
        htArgs.put("explainscores", "false");
        if (bPost) { htArgs.put("post", "true"); }
        else { htArgs.put("post", "false"); }

        if (sPath != null) htArgs.put("DBURL", sPath);

        htArgs.put("Corpora", sCorpora);

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);
            org.dom4j.Document doc = API.dExecute(tempFile);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eDocs == null) throw new Exception("ClassificationResultSet tag does not exist in XML output");

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                if (eDoc.getQName().getName().equals("NODE")) {
                    Iterator i2 = eDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (bUsingTempFile) { tempFile.delete(); }
        }

        return v;
    }

    // classify: using the HTTP POST method
    public Vector classify (File f, boolean bPost, String sCorpora) throws Exception {
        File tempFile = null; Vector v = new Vector(); boolean bUsingTempFile = false;

         // if the file is of type text, it needs an HTML wrapper before being posted
         if (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
             tempFile = new File("temp.html");
             if (tempFile.exists()) {
                 tempFile.delete();
             }
             System.out.println("Filtering: " + f.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

             try {
                 wrapHTML(f, tempFile); bUsingTempFile = true;
             } catch (Exception ex) {
                 ex.printStackTrace(); throw ex;
             }
         }
         // if the file is of type PDF, filter it before posting
         else if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
             bUsingTempFile = true;
             tempFile = ITS.PDFtoHTML(f,null);
         } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
             bUsingTempFile = true;
             tempFile = ITS.MSWORDtoHTML(f,null);
         } else { tempFile = f; bUsingTempFile = false; }

        Hashtable htArgs = getArguments();
        htArgs.put("explainscores", "false");
        if (bPost) { htArgs.put("post", "true"); }
        else { htArgs.put("post", "false"); }

        htArgs.put("Corpora", sCorpora);

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);
            org.dom4j.Document doc = API.dExecute(tempFile);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eDocs == null) throw new Exception("ClassificationResultSet tag does not exist in XML output");

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                if (eDoc.getQName().getName().equals("NODE")) {
                    Iterator i2 = eDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (bUsingTempFile) { tempFile.delete(); }
        }

        return v;
    }

    // handles both document and abstract classify
    // returns a vector of NodeDocument objects
    public Vector classify (int DocumentID, String DocTitle, boolean bDocClassify, String sCorpora ) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocIDSource", "" + DocumentID);

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        htArgs.put("Corpora", sCorpora);

        Vector v = new Vector(); // return set

        Vector vResults = InvokeVectorAPI("tsclassify.TSClassifyDoc", htArgs);

        if (vResults.size() < 3) {
            throw new Exception("ClassificationResultSet tag does not exist in XML output docid [" + DocumentID + "]");
        }

        Vector vDocs = (Vector) vResults.elementAt(2);

        Enumeration eD = vDocs.elements(); int loop = 0;
        while (eD.hasMoreElements()) {
            loop++; Object o = eD.nextElement();
            if (loop > 1) { // item number one is NUMNODEDOCSCORES
                Vector vNodeDocument = (Vector) o;
                NodeDocument nd = new NodeDocument(vNodeDocument);
                nd.set("DOCUMENTID",DocumentID+""); nd.set("DOCTITLE",DocTitle);

                v.add(nd); // add this document to the vector
            }
        }
        return v;
    }

    public Vector thesaurusExpand (Node n, Vector signatures) throws Exception {
        return thesaurusExpand(n.get("CORPUSID"), n.get("NODETITLE"), signatures);
    }
    public Vector thesaurusExpand (NodeDocument nd, Vector signatures) throws Exception {
        return thesaurusExpand(nd.get("CORPUSID"), nd.get("DOCTITLE"), signatures);
    }
    public Vector thesaurusExpand (String CorpusID, String NodeTitle, Vector signatures) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);
        htArgs.put("NodeTitle", NodeTitle);

        String dynamicnodesigs = "";
        for (int i = 0; i < signatures.size(); i++) {
            if (i != 0) dynamicnodesigs = dynamicnodesigs + "|||";
            Signature s = (Signature) signatures.elementAt(i);
            dynamicnodesigs = dynamicnodesigs + s.getWeight() + "," + s.getWord();
        }
        htArgs.put("Sigs", dynamicnodesigs);

        Vector v = new Vector(); // return vector
        HashTree htResults = InvokeAPI("tsthesaurus.TSThesaurusExpand", htArgs, true, true);
        if (!htResults.containsKey("SIGNATURES")) { return v; }

        HashTree htSignatures = (HashTree) htResults.get("SIGNATURES");
        Enumeration eS = htSignatures.elements();

        while (eS.hasMoreElements()) {
            HashTree htSignature = (HashTree) eS.nextElement();
            Signature s = new Signature(htSignature);
            v.add(s);

            if (htSignature.containsKey("THESAURUSTERM")) {
                HashTree htThesaurusTerms = (HashTree) htSignature.get("THESAURUSTERM");
                Enumeration eT = htThesaurusTerms.elements();

                while (eT.hasMoreElements()) {
                    String sWord = (String) eT.nextElement();
                    Signature st = new Signature(sWord, s.getWeight());
                    v.add(st);
                }
            }
        }

        return v;
    }

    // titles and thesaurus - all for a tsclassify to expand a pure sig set
    // NOT A MOD OF THE INPUT SIGNATURES OBJECT ... NEW OBJECT RETURNED
    public Signatures signatureExpand ( Signatures signatures, NodeDocument nd) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", ""+ nd.get("NODEID"));


        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tssignature.TSSignatureExpand", htArgs);
            Element elemRoot = doc.getRootElement ();

            Element eSignatures = elemRoot.element("SIGNATURES");
            Iterator iter = eSignatures.elements().iterator();

            return new Signatures (iter);

        } catch (Exception e) {
            throw e;
        }
    }

    // classify a document into a specific node, given dynamic node parameters
    // returns a new NodeDocument object
    /* "&DynamicNodeSigs=3,term1|||5,term2||thesterm21||thesterm22|||7,term3||thesterm31" +
                         "&DCNodeTitle=hello world" +
                         "&DCNumTitleWords=1" +
                         "&DCNodeSize=51" +
                         "&DCBuildThesaurus=false"
     */
    public NodeDocument classify (NodeDocument nd, Vector signatures) throws Exception {
        return classify(nd, signatures, true); }
    public NodeDocument classify (NodeDocument nd, Vector vSignatureObjects, boolean bThesAndTitleExpand) throws Exception {
        NodeDocument ndResult = null;

        Hashtable htArgs = getArguments();
        htArgs.put("DocIDSource", nd.get("DOCUMENTID"));
        htArgs.put("DocTitle", nd.get("DOCTITLE"));

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        htArgs.put("Corpora", nd.get("CORPUSID"));

        // thesaurus and title expand these signatures
        Signatures signaturesObject = ITSHelper.vSignatureObjectsToSignaturesObject(vSignatureObjects);
        Signatures signaturesObjectExpanded = signaturesObject;
        if (bThesAndTitleExpand)
            signaturesObjectExpanded = signatureExpand (signaturesObject, nd);

        String dynamicnodesigs = "";

        Vector vSignatureObjectsEpanded = signaturesObjectExpanded.getVecOfSignatureObjects();
        for (int i = 0; i < vSignatureObjects.size(); i++) {
            Signature s = (Signature) vSignatureObjects.elementAt(i);
                if (i != 0)
                    dynamicnodesigs = dynamicnodesigs + "|||";
                dynamicnodesigs = dynamicnodesigs + s.getWeight() + "," + s.getWord();
        }
        htArgs.put("DynamicNodeSigs", dynamicnodesigs);
        htArgs.put("DynamicNodeID", ""+nd.get("NODEID"));

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsclassify.TSClassifyDoc", htArgs);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eNodes == null) return null;

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                if (eNode.getQName().getName().equals("NODE")) {
                    Iterator i2 = eNode.elements().iterator();
                    ndResult = new NodeDocument(i2);
                }
            }

            return ndResult;
        } catch (Exception e) {
            throw e;
        }
    }

    // classify a document into a specific node
    // returns a new NodeDocument object
    public NodeDocument classify (NodeDocument nd) throws Exception {
        NodeDocument ndResult = null;

        Hashtable htArgs = getArguments();
        htArgs.put("DocIDSource", nd.get("DOCUMENTID"));
        htArgs.put("NodeSet", nd.get("DOCUMENTID"));

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        htArgs.put("Corpora", nd.get("CORPUSID"));

        Vector vResults = InvokeVectorAPI("tsclassify.TSClassifyDoc", htArgs);

        if (vResults.size() < 3) {
            throw new Exception("ClassificationResultSet tag does not exist in XML output docid [" + nd.get("DOCUMENTID") + "]");
        }

        Vector vDocs = (Vector) vResults.elementAt(2);

        Enumeration eD = vDocs.elements(); int loop = 0;
        while (eD.hasMoreElements()) {
            loop++; Object o = eD.nextElement();
            if (loop > 1) { // item number one is NUMNODEDOCSCORES
                Vector vNodeDocument = (Vector) o;
                ndResult = new NodeDocument(vNodeDocument);
            }
        }
        return ndResult;
    }

    public Vector classifyNarrative (String DocTitle, String DocText, String sCorpora) throws Exception {
        Hashtable htArgs = getArguments();

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");

        htArgs.put("Corpora", sCorpora);

        Vector v = new Vector();
        File tempFile = wrapHTML(DocTitle, DocText);

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);
            org.dom4j.Document doc = API.dExecute(tempFile);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eDocs == null) throw new Exception("ClassificationResultSet tag does not exist in XML output");

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                if (eDoc.getQName().getName().equals("NODE")) {
                    Iterator i2 = eDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            tempFile.delete();
        }

        return v;
    }

    // get data container containing classify explain data
    public ExplainDisplayDataContainer getExplainData ( int iNodeID, int iDocIDSource,
            String sNodeTitle, String sDocTitle, String sCorpusID,
            boolean bClassifyDocument)
        throws Exception
    {
        Hashtable htArgs = getArguments();

        htArgs.put("Corpora", sCorpusID);

        if (iDocIDSource != -1) {
            htArgs.put("DocIDSource", ""+iDocIDSource);
        }
        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put ("batch" , "false");
        htArgs.put ("nodetoscoreexplain" , ""+iNodeID);

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        org.dom4j.Document doc = API.dExecute(false, false);

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer =
                new ExplainDisplayDataContainer (iNodeID, iDocIDSource, sNodeTitle, sDocTitle);
        explainDisplayDataContainer.setFromSerializableForm(vDisplayDataInstances);

        return explainDisplayDataContainer;
    }

    // get data container containing classify explain data with custom signature set
    public ExplainDisplayDataContainer getExplainData ( Node n, int iDocIDSource,
            String sDocTitle, Vector vSignatures, boolean bClassifyDocument )
        throws Exception
    {
        Hashtable htArgs = getArguments();

        htArgs.put("Corpora", n.get("CORPUSID"));

        if (iDocIDSource != -1) {
            htArgs.put("DocIDSource", ""+iDocIDSource);
        }
        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put ("batch" , "false");
        htArgs.put ("nodetoscoreexplain" , n.get("NODEID"));

        // dynamic parameters
//        htArgs.put("DCNodeTitle", n.get("NODETITLE"));
//        htArgs.put("DCNumTitleWords", "1");
//        htArgs.put("DCNodeSize", n.get("NODESIZE"));
//        htArgs.put("DCBuildThesaurus", "false");

        //vSignatures = thesaurusExpand(n, vSignatures);

        String dynamicnodesigs = "";
        for (int i = 0; i < vSignatures.size(); i++) {
            Signature s = (Signature) vSignatures.elementAt(i);

            if (i != 0) dynamicnodesigs = dynamicnodesigs + "|||";
            dynamicnodesigs = dynamicnodesigs + s.getWeight() + "," + s.getWord();
        }
        htArgs.put("DynamicNodeSigs", dynamicnodesigs);

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        org.dom4j.Document doc = API.dExecute(false, false);

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer =
                new ExplainDisplayDataContainer (Integer.parseInt(n.get("NODEID")), iDocIDSource, n.get("NODETITLE"), sDocTitle);
        explainDisplayDataContainer.setFromSerializableForm(vDisplayDataInstances);

        return explainDisplayDataContainer;
    }

    // get data container containing classify explain data
    public ExplainDisplayDataContainer getExplainData ( int iNodeID, String sNodeTitle, String sDocTitle,
                                                        String sCorpusID, File f)
        throws Exception
    {
        Hashtable htArgs = getArguments();

        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put ("batch" , "false");
        htArgs.put ("nodetoscoreexplain" , ""+iNodeID);
        htArgs.put ("Corpora", sCorpusID);

        File tempFile = null; boolean bUsingTempFile = false;

         // if the file is of type text, it needs an HTML wrapper before being posted
         if (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
             tempFile = new File("temp.html");
             if (tempFile.exists()) {
                 tempFile.delete();
             }
             System.out.println("Filtering: " + f.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

             try {
                 wrapHTMLstatic(f, tempFile); bUsingTempFile = true;
             } catch (Exception ex) {
                 ex.printStackTrace(); throw ex;
             }
         }

        // if the file is of type PDF, filter it before posting
        else if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
             bUsingTempFile = true;
             tempFile = ITS.PDFtoHTML(f,null);
        } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
             bUsingTempFile = true;
             tempFile = ITS.MSWORDtoHTML(f,null);
        } else { tempFile = f; bUsingTempFile = false; }

        htArgs.put("Corpora", sCorpusID);

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        org.dom4j.Document doc = API.dExecute(tempFile, false, false);

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer =
                new ExplainDisplayDataContainer (iNodeID, -1, sNodeTitle, sDocTitle);
        explainDisplayDataContainer.setFromSerializableForm(vDisplayDataInstances);

        if (bUsingTempFile) tempFile.delete();

        return explainDisplayDataContainer;
    }

    // get data container containing classify explain data
    public ExplainDisplayDataContainer getExplainData ( int iNodeID, int iDocIDSource,
            String sNodeTitle, String sDocTitle, String sCorpusID, File f)
        throws Exception
    {
        Hashtable htArgs = getArguments();

        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put ("batch" , "false");
        htArgs.put ("nodetoscoreexplain" , ""+iNodeID);
        htArgs.put("Corpora", sCorpusID);

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        File tempFile = null; Vector v = new Vector(); boolean bUsingTempFile = false;

         // if the file is of type text, it needs an HTML wrapper before being posted
         if (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
             tempFile = new File("temp.html");
             if (tempFile.exists()) {
                 tempFile.delete();
             }
             System.out.println("Filtering: " + f.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

             try {
                 wrapHTML(f, tempFile); bUsingTempFile = true;
             } catch (Exception ex) {
                 ex.printStackTrace(); throw ex;
             }
         }

         // if the file is of type PDF, filter it before posting
         else if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
              bUsingTempFile = true;
              tempFile = ITS.PDFtoHTML(f,null);
         } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
              bUsingTempFile = true;
              tempFile = ITS.MSWORDtoHTML(f,null);
         } else { tempFile = f; bUsingTempFile = false; }

        org.dom4j.Document doc = API.dExecute(f, false, false);

        if (bUsingTempFile) tempFile.delete();

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer =
                new ExplainDisplayDataContainer (iNodeID, iDocIDSource, sNodeTitle, sDocTitle);
        explainDisplayDataContainer.setFromSerializableForm(vDisplayDataInstances);

        return explainDisplayDataContainer;
    }

    public static Vector getExplainDataInternal (org.dom4j.Document doc) throws Exception {
        Element elemRoot = doc.getRootElement();
        Element elemIWERROR = elemRoot.element("IWERROR");
        if ( elemIWERROR != null )
        {
            Element elemERRMSG = elemIWERROR.element("ERRMSG");

            if ( elemERRMSG == null )
                throw new Exception ("invalid IWERROR TAG from server");
            String sServerMSGwithUserMsgEmbedded = elemERRMSG.getTextTrim();
            String sStartUSERERRMSG = "##USERERRMSG##";
            String sEndUSERERRMSG = "##/USERERRMSG##";
            int iIndexStartUSERERRMSG = sServerMSGwithUserMsgEmbedded.indexOf(sStartUSERERRMSG);
            int iIndexEndUSERERRMSG = sServerMSGwithUserMsgEmbedded.indexOf(sEndUSERERRMSG);

            String sUserError = sServerMSGwithUserMsgEmbedded.substring(iIndexStartUSERERRMSG+sStartUSERERRMSG.length(), iIndexEndUSERERRMSG);
            JOptionPane.showMessageDialog(null, "Failed to retrieve explain data [" + sUserError  + "]", "Information", JOptionPane.NO_OPTION);
            return null;

        }
        Element elemEXPLAINDISPLAYSET = elemRoot.element("EXPLAINDISPLAYSET");
        String sXMLizedExplainDisplayDataContainer = elemEXPLAINDISPLAYSET.getText().trim();

        return (Vector) XMLBeanSerializeHelper.deserializeFromString(sXMLizedExplainDisplayDataContainer.trim());
    }
    // Rebuild full text indexes
    public boolean rebuildIndexes() throws Exception {
        try {
            HashTree htArguments = getArguments();
            HashTree htResults = InvokeAPI("tsother.TSRebuildIndexes", htArguments);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        }

        return true;
    }

    public void synchronizeBatchRun() throws Exception {
        synchronizeBatchRun("RDATA");
    }

    public void synchronizeBatchRun(String Tablespace) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("Tablespace", Tablespace);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsclassify.TSLoadBatchRun", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    // Filter a PDF document to an HTML document
    public static File PDFtoHTML(File PDFfile, String tempFileName) throws Exception {
        File tempFile = null;

        try {
            if (tempFileName == null || tempFileName.trim().equals("")) {
                tempFile = new File("temp.html");
            } else {
                tempFile = new File(tempFileName);
            }
            System.out.println("Filtering PDF: " + PDFfile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
            PDFUtils.convertPDFToHTML(PDFfile, tempFile);
            wrapHTMLstatic(tempFile);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            throw ex;
        }

        return tempFile;
    }
    // Filter a WORD document to an HTML document

    public static File MSWORDtoHTML(File WordFile, String tempFileName) throws Exception {
        File tempFile = null;
        try {
            if (tempFileName == null || tempFileName.trim().equals("")) {
                tempFile = new File("temp.html");
            } else {
                tempFile = new File(tempFileName);
            }
            if (tempFile.exists()) {
                tempFile.delete();
            }
            System.out.println("Filtering MSWORD: " + WordFile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

            FileInputStream in = new FileInputStream(WordFile.getAbsolutePath());
            WordExtractor extractor = new WordExtractor();

            String s = extractor.extractText(in);
            wrapHTMLstatic(s, tempFile);
            in.close();
        } catch (Exception exception) {
            exception.printStackTrace(System.out);
            throw exception;
        }

        return tempFile;
    }

    private void sessionTimedOut() {
        JOptionPane.showMessageDialog(null, "Your session has timed out.  Please restart the application.", "Information", JOptionPane.NO_OPTION);
        System.exit(-1);
    }

    // collect up the contents of two ht;s into one - leaves originals unchanged
    private static Hashtable mergeHashTables(Hashtable ht1, Hashtable ht2, boolean bOverlapAllowed) throws Exception {
        Hashtable htMerged = new Hashtable();

        Enumeration e = ht1.keys();
        Object k1;
        while (e.hasMoreElements()) {
            k1 = (Object) e.nextElement();
            Object v1 = ht1.get(k1);
            htMerged.put(k1, v1);
        }

        Enumeration e2 = ht2.keys();
        Object k2;
        while (e2.hasMoreElements()) {
            k2 = (Object) e2.nextElement();
            if (!bOverlapAllowed && ht1.get(k2) != null) {
                throw new Exception("merge hashtables - overlap occurred [" + k2 + "]");
            }
            Object v2 = ht2.get(k2);
            htMerged.put(k2, v2);
        }
        return htMerged;
    }
}
