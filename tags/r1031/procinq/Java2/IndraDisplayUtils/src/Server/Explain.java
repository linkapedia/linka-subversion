package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;

import java.io.*;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Enumeration;
import java.net.*;
import java.util.zip.*;

import HTML.*;
import Logging.*;
import com.iw.system.*;

public class Explain
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sContentType = (String) props.get ("Content-type");
		String sFileName = (String) props.get ("import");
		String sFilePath = (String) props.get ("FilePath");
		String sNodeID = (String) props.get ("NodeID");

		try {

			if (sContentType != null) {
				// Send classify request, read and display results
				if ((!sContentType.equals("text/html")) &&
					(!sContentType.equals("text/plain"))) {
					//Log.Log("Attempt to classify document of type: "+sContentType);
					throw new Exception("Sorry, this ITS version classifies HTML or text documents only.");
				}

				HashTree htArguments = new HashTree(props);
				htArguments.put("ExplainScores", "true");
				htArguments.put("post", "false");
				htArguments.put("NodeToScoreExplain", sNodeID);
				Vector vResults = PostExecute(out, sFileName,
											   "tsclassify.TSClassifyDoc",
											   htArguments);


				if (vResults.size()  < 4) {
					throw new Exception("Sorry, no classification results found "+
						"for the topic identifier that was specified. ");
				}

				VectorTree vExplain = null;

                try {
                       vExplain = (VectorTree) vResults.elementAt(1);
                } catch ( Exception e)
                {
                    VectorTree.printVector(vResults, 0);
                    throw e;
                }
/*
                api.Log.Log ("-----------------Explain vector tree output :" );
                VectorTree.printVector(vResults, 0);
                api.Log.Log ("-----------------End Explain vector tree output :" );
*/
				HTMLDocument Document = new HTMLDocument();

				String sNodeTitle = (String) vExplain.elementAt(1);
				if (sNodeTitle.length() > 50) {
					sNodeTitle = sNodeTitle.substring(0,47)+"...";
				}

				Document.AddVariable("NODEID", (String) vExplain.elementAt(0));
				Document.AddVariable("NODETITLE", sNodeTitle);
				Document.AddVariable("DOCURL", (String) vExplain.elementAt(2));
				Document.AddVariable("DOCTITLE", (String) vExplain.elementAt(3));

				// retrieve document frequency and coverage
				Vector vClassResults = (VectorTree) vResults.elementAt(3);

/*
                api.Log.Log ("-----------------Explain 2 SC1 vector tree output :" );
                VectorTree.printVector(vClassResults, 0);
                api.Log.Log ("-----------------End Explain 2 SC1 vector tree output :" );
*/
				Enumeration eC = vClassResults.elements();
				while (eC.hasMoreElements()) {
					try {
						Vector v = (Vector) eC.nextElement();

                        //  api.Log.Log ("EXPLAIN: com.indraweb.util.UtilSets.vToStr(v) [" + com.indraweb.util.UtilSets.vToStr(v) + "]" ) ;

						if (sNodeID.equals((String) v.elementAt(1))) {
							Document.AddVariable("SC1", (String) v.elementAt(3));
						}
					} catch (Exception e) {}
				}

				Document.AddVariable("SFQ", "");


                // get frq data
                //hbk 2003 09 25 String sData = (String) vExplain.elementAt((vExplain.size()-3) );
                String sData = (String) vExplain.elementAt((vExplain.size()-1) );
                out.println("<!-- frq: "+sData+" elementAt: "+(vExplain.size()-1)+"-->");
                //System.out.println("<!-- cov: "+sData+" elementAt: "+(vExplain.size()-4)+"-->");
                int iStart = sData.lastIndexOf("[");
                int iEnd = sData.lastIndexOf("]");
                Document.AddVariable("FRQ", sData.substring(iStart+1, iEnd));

                // get cov data
                sData = (String) vExplain.elementAt((vExplain.size()-2));
                out.println("<!-- cov: "+sData+" -->");
                iStart = sData.lastIndexOf("[");
                iEnd = sData.lastIndexOf("]");
                Document.AddVariable("COV", sData.substring(iStart+1, iEnd));

				Document.AddVariable("Title", "Explain a Document");
				Document.WriteTemplate(out, "header-admin.tpl");
				Document.WriteTemplate(out, "explain/explain-head.tpl");

				// Loop through each explanation .. but filter out things that aren't explanations
                for (int loop = 6; loop < vExplain.size()-3; loop++) {
					sData = (String) vExplain.elementAt(loop);

					// split into readable results..
					//0. wd- [summary] d1cnt [8] d2cnt [0] incr [0.0]
					iStart = sData.indexOf("[");
					iEnd = sData.indexOf("]");
					Document.AddVariable("SIGNATURE",
										 sData.substring(iStart+1, iEnd));

					sData = sData.substring(iEnd+1);
					iStart = sData.indexOf("[");
					iEnd = sData.indexOf("]");
					Document.AddVariable("SOURCE",
										 sData.substring(iStart+1, iEnd));

					sData = sData.substring(iEnd+1);
					iStart = sData.indexOf("[");
					iEnd = sData.indexOf("]");
					Document.AddVariable("DOC",
										 sData.substring(iStart+1, iEnd));

					Document.WriteTemplate(out, "explain/explain-result.tpl");
				}

				Document.WriteTemplate(out, "explain/explain-foot.tpl");
				Document.WriteFooter(out);

			} else {
				// Just display the explain page
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Explain a Document");
				Document.WriteTemplate(out, "header-admin.tpl");
				Document.WriteTemplate(out, "explain/explain.tpl");
				Document.WriteFooter(out);
			}
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			out.print("<!-- "); e.printStackTrace(out); out.println(" -->");
			Log.LogError(e, out);
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE><BR>"+sErrorMsg+"</BLOCKQUOTE>");
		}
	}

	// Send POST API call
	public static Vector PostExecute (PrintWriter out, String filename,
							   String sAPIcall, HashTree htArguments) {
		try {
			VectorTree vOriginal = new VectorTree();
			vOriginal.SetParent(null);
			VectorTree vObject = vOriginal;

			// Do a multi-part post
			String boundary="ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
		    String twoHyphens="--";
			String lineEnd="\r\n";

	        HttpURLConnection httpURLConn = null;
			DataOutputStream outStream;
			DataInputStream inStream;
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
	        int maxBufferSize = 1*1024*1024;

	        // create FileInputStream to read from file
			FileInputStream fileInputStream = new FileInputStream(new File(com.indraweb.execution.Session.cfg.getProp("ImportDir")+filename));

			String sTaxonomyServer = com.indraweb.execution.Session.cfg.getProp("API");
			String sURL = sTaxonomyServer+sAPIcall;
			Enumeration et = htArguments.keys();

			// Build URL to call Taxonomy Server
			while (et.hasMoreElements()) {
				String sKey = (String) et.nextElement();
				String sValue = (String) htArguments.get(sKey);

				sURL = sURL+"&"+sKey+"="+sValue;

				// Remove any apostraphies (because they are illegal) and change
				// spaces into + signs
				sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL," ","+"));
				sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL,"'",""));
			}

            URL theURL = new URL(sURL);
            httpURLConn  = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

			// Set up POST parameters
			StringBuffer params = new StringBuffer();
		    params.setLength(0);
			Enumeration e = htArguments.keys();
			params.append("fn="+sAPIcall);

            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream (httpURLConn.getOutputStream ());

			outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\""+ filename + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable,maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable,maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();
            outStream.flush ();
            outStream.close ();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream ();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");

				String sTag1;

				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }

				String sTag2 = "";

				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);

				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) &&
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
 					 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						if (sTag1.equals("TS_ERROR")) {
							httpURLConn.disconnect();
							return vOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							httpURLConn.disconnect();
							return vOriginal;
						}
						VectorTree vOldObject = vObject;
						vObject = new VectorTree();
						vObject.SetParent(vOldObject);
						vOldObject.addElement(vObject);

					}

					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2);
						vObject = (VectorTree) vObject.GetParent();
					}

					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String (com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						//out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

						// If the variable value contains a "CDATA", eliminate it here
						if (com.indraweb.util.UtilStrings.getStrContains(sTag,"<![CDATA")) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}

						vObject.addElement(sTag);
					}
				}
			}

			return vObject;
		}
		catch (Exception except) {
			HTML.HTMLDocument Document = new HTML.HTMLDocument();
			Log.LogError(except, out);
			Document.WriteError(out, except.toString());
			return null;
		}

	}
}