package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;
import com.indraweb.execution.Session;

import java.io.*;
import java.util.*;

import HTML.*;
import Logging.*;
import com.iw.system.*;

public class Classify {
    public static String addHTMLHeaders (String Text) throws Exception {
        return "<HTML>\n<HEAD><TITLE>None</TITLE></HEAD>\n<BODY>\n"+Text+"\n</BODY>\n</HTML>";
    }
    public static File getTempFile (String sPath) throws Exception {
       return getTempFile(sPath, "html");
    }
    public static File getTempFile (String sPath, String sExtension) throws Exception {
        Random wheel = new Random() ; // seeded from the clock
        File tempFile = null;

        do {
            // generate random a number 10,000,000 .. 99,999,999
            int unique = ( wheel.nextInt() & Integer. MAX_VALUE ) %90000000 + 10000000 ;

            tempFile = new File( sPath, Integer.toString ( unique) + "." + sExtension );
        } while ( tempFile.exists () );

        // We "finally" found a name not already used. Nearly always the first time.
        // Quickly stake our claim to it by opening/closing it to create it.
        // In theory somebody could have grabbed it in that tiny window since
        // we checked if it exists, but that is highly unlikely.
        synchronized (tempFile) {
            try {
                new FileOutputStream( tempFile ).close ();
                //System.out.println("Temp file successfully created: "+tempFile.getAbsolutePath());
            }
            catch (Exception e) {
                System.out.println("Could not generate temp file.");
            }
        }

        return tempFile;
	}

    public static void handleTSapiRequest(APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
            throws Exception {
        String sDocText = (String) props.get("doctext");
        String sScoreThreshold = (String) props.get("scorethreshold", "49.9");
        String sSubjectArea = (String) props.get("subjectarea");

        try {
            if (sDocText != null) {
                // Read scoring threshold from cookie information
                Cookie Cookies[] = req.getCookies();

                if (req.getCookies() != null) {
                    int cLen = Cookies.length;

                    for (int i = 0; i < cLen; i++) {
                        if (Cookies[i].getName().equals("SCORETHRESHOLD")) {
                            sScoreThreshold = Cookies[i].getValue();
                        }
                    }
                }

                if (sSubjectArea.equals("-1")) {
                    api.Log.Log( "Subject area not selected." );
                    throw new Exception ( "Sorry, you must provide the system with a subject area for classification." ) ;
                }

                // select the corpora associated with this subject area and include as a param to classify
                String sCorporaToUse = "";  Hashtable htCorporaToUse = new Hashtable();

                HashTree htArguments = new HashTree(props);
                InvokeAPI API = new InvokeAPI("tssubjectarea.TSListCorporaSubjectAreas", htArguments);
                HashTree htResults = API.Execute(false, false);
                if (!htResults.containsKey("SUBJECTAREAS")) {
                    api.Log.Log( "Subject area not selected." );
                    throw new Exception ( "Sorry, you must provide the system with a subject area for classification." ) ;
                }
                HashTree htSubjects = (HashTree) htResults.get("SUBJECTAREAS");
                Enumeration eH = htSubjects.elements();

                while (eH.hasMoreElements()) {
                    HashTree htSubject = (HashTree) eH.nextElement();
                    if (((String) htSubject.get("SUBJECTAREAID")).equals(sSubjectArea)) {
                        HashTree htCorpora = (HashTree) htSubject.get("CORPORA");
                        Enumeration eC = htCorpora.elements();
                        while (eC.hasMoreElements()) {
                            HashTree htCorpus = (HashTree) eC.nextElement();
                            htCorporaToUse.put((String) htCorpus.get("CORPUSID"), "1");
                        }
                    }
                }

                Enumeration eKeys = htCorporaToUse.keys(); int loop = 0;
                while (eKeys.hasMoreElements()) {
                    loop++; if (loop > 1) { sCorporaToUse = sCorporaToUse + ","; }
                    sCorporaToUse = sCorporaToUse + (String) eKeys.nextElement();
                }

                out.println("<!-- using: "+sCorporaToUse+" as corpus list -->");

                ITS its = new ITS((String) props.get("SKEY"));

                // write whatever is in that text box into the temporary file
                File f = getTempFile(Session.sIndraHome+"/classifyfiles/");
                if (sDocText.toLowerCase().indexOf("<HTML>") == -1) { sDocText = addHTMLHeaders(sDocText); }
                PrintWriter fout = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), true));
                fout.println(sDocText); fout.close();

                Vector vNodeDocuments = its.classify(f, false, sCorporaToUse);

                if (vNodeDocuments.size() == 0) {
                    api.Log.Log( "ClassificationResultSet tag does not exist." );
                    throw new Exception ( "Sorry, this document did not classify into any topics in the system." ) ;
                }

                f.delete();

                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Classification Results");
                Document.WriteTemplate(out, "header-admin.tpl");

                for (int i = 0; i < vNodeDocuments.size(); i++) {
                    NodeDocument nd = (NodeDocument) vNodeDocuments.elementAt(i);
                    float fScore = new Float(nd.get("SCORE1")).floatValue();

                    // Ensure threshold match
                    if (fScore > new Float(sScoreThreshold).floatValue()) {
                        i = i + 1;
                        if (i == 1) {
                            Document.WriteTemplate(out, "classify-start.tpl");
                        }
                        String sNodeTree
                                = Taxonomy.Search.BuildNodeTree(out, nd.get("NODEID"), props);
                        sNodeTree = "<LI> <a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID=" +
                                nd.get("NODEID") + "'><i><b>" +
                                nd.get("NODETITLE") + "</b></i></a> &nbsp; " +
                                "<BR>" + sNodeTree +"<P>";
                        Document.SetHash(nd);
                        Document.AddVariable("NODETREE", sNodeTree);
                        Document.WriteTemplate(out, "search.tpl");
                        out.println("<!-- Score: "+fScore+" -->\n");
                    }
                }
                if (loop != 0) {
                    Document.WriteTemplate(out, "search-end.tpl");
                } else {
                    out.println("<blockquote>Sorry, this document did not classify into any topics in the system.<br></blockquote>");
                }
                Document.WriteFooter(out);

            } else {
                HashTree htArguments = new HashTree(props);
                InvokeAPI API = new InvokeAPI("tssubjectarea.TSListSubjectAreas", htArguments);
                HashTree htResults = API.Execute(false, false);
                String sSubjectOption = "";

                if (htResults.containsKey("SUBJECTAREAS")) {
                    HashTree htSubjectAreas = (HashTree) htResults.get("SUBJECTAREAS");
                    Enumeration eH = htSubjectAreas.elements();

                    while (eH.hasMoreElements()) {
                        HashTree htSubject = (HashTree) eH.nextElement();
                        sSubjectOption = sSubjectOption+"<option value="+(String)htSubject.get("SUBJECTAREAID")+
                                         "> "+(String)htSubject.get("SUBJECTNAME")+"\n";
                    }
                }

                // Just display the classify page
                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Classify a Document");
                Document.AddVariable("SUBJECTAREA", sSubjectOption);
                Document.WriteTemplate(out, "header-admin.tpl");
                Document.WriteTemplate(out, "classify.tpl");
                Document.WriteFooter(out);
            }
        } catch (Exception e) {
            HTMLDocument Document = new HTMLDocument();
            Log.LogError(e, out);
            String sErrorMsg = e.getMessage();
            Document.WriteError(out, "<BLOCKQUOTE><BR>" + sErrorMsg + "</BLOCKQUOTE>");
        }
    }

    private static void qs(Vector v) {

        Vector vl = new Vector();                      // Left and right sides
        Vector vr = new Vector();
        HashTree el;
        float key;                                    // key for splitting

        if (v.size() < 2) return;                      // 0 or 1= sorted

        HashTree htResult = (HashTree) v.elementAt(0);
        key = new Float((String) htResult.get("SCORE1")).floatValue();

        // Start at element 1
        for (int i = 1; i < v.size(); i++) {
            el = (HashTree) v.elementAt(i);
            if (new Float((String) el.get("SCORE1")).floatValue() < key)
                vr.addElement(el); // Add to right
            else
                vl.addElement(el);                     // Else add to left
        }

        qs(vl);                                        // Recursive call left
        qs(vr);                                        //    "        "  right
        vl.addElement(v.elementAt(0));

        addVect(v, vl, vr);
    }

    // Add two vectors together, into a destination Vector
    private static void addVect(Vector dest, Vector left, Vector right) {
        int i;

        dest.removeAllElements();                     // reset destination

        for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
        for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));

    }

    public static Vector Sort(HashTree htHash) {
        Vector vNodes = new Vector();
        Enumeration e = htHash.keys();

        // Fill the vector with all the results
        while (e.hasMoreElements()) {
            String s = (String) e.nextElement();
            if (s.startsWith("NODE")) {
                HashTree htResult = (HashTree) htHash.get(s);
                vNodes.addElement(htResult);
            }
        }

        // Sort this vector  - engine sorts - no need
        //qs(vNodes);
        HashTree[] htArr = new HashTree [ vNodes.size() ];
        vNodes.copyInto ( htArr );
        java.util.Arrays.sort( htArr, new comparehtResults());
        Vector vReturn = new Vector();
        for ( int i = htArr.length-1; i >= 0; i-- )
        {
            vReturn.addElement ( htArr[i] );
        }

        return vReturn;
    }

    private static class comparehtResults
       implements java.util.Comparator   {

       public int compare ( Object htResult1, Object htResult2 )
       {
           float key1 = new Float((String) ((HashTree)htResult1).get("SCORE1")).floatValue();
           float key2 = new Float((String) ((HashTree)htResult2).get("SCORE1")).floatValue();

           if ( key1 < key2 )
               return -1;
           else if ( key1 > key2 )
               return 1;
           else
               return 0;
       }

    }

}
