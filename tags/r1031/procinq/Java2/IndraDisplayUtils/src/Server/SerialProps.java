package Server;

import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import Logging.*;

public class SerialProps implements Serializable {
	private File fileData;
	public APIProps Props = new APIProps();
	public byte[] bArr;	
	
	// Accessor and set methods for new Hashtable attribute, htParent..
	public File GetFile() { return fileData; }
	public void SetFile (File fileData) {
		this.fileData = fileData;	
	}
}
