package Server;

import java.util.Enumeration;
import java.util.Vector;

public class VectorTree extends Vector {

	private static final long serialVersionUID = -1485903360429158415L;
	private VectorTree vParent;

	// Accessor and set methods for new Hashtable attribute, htParent..
	public VectorTree GetParent() { return vParent; }
	public void SetParent(VectorTree vParent) {
		this.vParent = vParent;	
	}
	
	// Another constructor
	public VectorTree () {}

    public void printMe () throws Exception
    {
        printVectorTree( this, 0 );
    }
    private static void printVectorTree (VectorTree vt, int iRecurseDepth)  throws Exception
    {
        Enumeration enumeration = vt.elements();
        StringBuffer sbWhiteSpace = new StringBuffer();
        for ( int i = 0; i < iRecurseDepth; i++ )
            sbWhiteSpace.append ("\t");

        int j = -1;
        while ( enumeration.hasMoreElements()  )
        {
            j++;
            Object o = enumeration.nextElement();
            String sClassName = o.getClass().getName();
            if (sClassName.equals("Server.VectorTree"))
                printVectorTree((VectorTree)o, iRecurseDepth+1);
            else if (sClassName.equals("java.lang.String"))
                api.Log.Log (sbWhiteSpace.toString() +  j + ". VT.String [" + o + "]");
            else
                throw new Exception ("unknown vectortree element class type [" + sClassName + "]");
        }

    }
    public static void printVector(Vector v, int iRecurseDepth) throws Exception
    {
        Enumeration enumeration = v.elements();
        StringBuffer sbWhiteSpace = new StringBuffer();
        for ( int i = 0; i < iRecurseDepth; i++ )
            sbWhiteSpace.append ("\t");

        int j = -1;
        while ( enumeration.hasMoreElements()  )
        {
            j++;
            Object o = enumeration.nextElement();
            String sClassName = o.getClass().getName();
            //api.Log.Log (sbWhiteSpace.toString() + j + ". EXPLAIN o.getClass().getName()  [" + o.getClass().getName() + "]"  );
            if (sClassName.equals("Server.VectorTree"))
                printVectorTree((VectorTree)o, iRecurseDepth+1);
            else if (sClassName.equals("java.lang.String"))
                api.Log.Log (sbWhiteSpace.toString() +  j + ". V.String [" + o + "]");
            else if (sClassName.equals("java.lang.Vector"))
                printVector((Vector) o, iRecurseDepth+1);
            else
                throw new Exception ("unknown vectortree element class type [" + sClassName + "]");
        }


    }
}
