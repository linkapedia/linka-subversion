package com.iw.repository;

import java.io.*;
import java.util.*;
import java.net.HttpURLConnection;
import java.net.URL;

import com.iw.system.*;
import com.iw.fcrawl.*;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import java.io.FileOutputStream;

// The repository class should be subclassed for each unique repository type.
//   This class is responsible for holding specific knowledge about:
//
//   1. How to access and retrieve information from this repository
//   2. How to retrieve "new" documents (new information since last scan)
//
//   After a path to this document has been established, use the standard methods
//   that exist to change the document security, reindex the document in the full
//   text engine (assuming this is allowed), and reclassify the document using the ITS
//
//   o Any new attributes or methods necessary should be added in the subclass.
//   o The "GetNewObjects" function should be overridden by each subclass
public class Repository {
    // Private variables

    private String Identifier = "";
    private String Type = "";
    private String Name = "";
    private String Location = "";
    private String Username = "";
    private String Password = "";
    private String Date = "";
    private boolean FullText = false;
    private boolean bCrawlInProgress = false;
    private Vector vNewDocuments = new Vector();
    private HashTree htArguments = new HashTree();

    // Accessor methods to private variables
    public String GetID() {
        return Identifier;
    }

    public String GetType() {
        return Type;
    }

    public String GetName() {
        return Name;
    }

    public String GetLocation() {
        return Location;
    }

    public String GetUsername() {
        return Username;
    }

    public String GetPassword() {
        return Password;
    }

    public boolean bFullText() {
        return FullText;
    }

    public boolean bCrawlInProgress() {
        return bCrawlInProgress;
    }

    public Vector GetNewDocuments() {
        return vNewDocuments;
    }

    public HashTree GetArguments() {
        return htArguments;
    }

    public String SetUsername(String sUsername) {
        Username = sUsername;
        return Username;
    }

    public String SetPassword(String sPassword) {
        Password = sPassword;
        return Password;
    }

    public String SetLocation(String sLocation) {
        Location = sLocation;
        return Location;
    }

    public String SetName(String sName) {
        Name = sName;
        return Name;
    }

    public HashTree SetArguments(HashTree ht) {
        htArguments = ht;
        return htArguments;
    }

    public String SetDate(HashTree htFolder) throws Exception {
        Date = GetCurrentDate(htFolder);
        return Date;
    }

    public boolean SetCrawlInProgress(boolean crawl) {
        bCrawlInProgress = crawl;
        return crawl;
    }

    public void AddNewDocument(String Path) {
        if (!vNewDocuments.contains(Path)) {
            vNewDocuments.addElement(Path);
        }
    }

    public void AddNewDocument(FNodeDocument ND) {
        vNewDocuments.addElement(ND);
    }
    // 3 Object constructor(s)-- The first two are defined implicitly

    public Repository(String Identifier, String Type, String Name, String Location,
            String Username, String Password, boolean FullText, HashTree ht) {
        this.Identifier = Identifier;
        this.Type = Type;
        this.Name = Name;
        this.Location = Location;
        this.Username = Username;
        this.Password = Password;
        this.FullText = FullText;
        this.htArguments = ht;
    }

    public Repository(String Identifier, String Type, String Name, String Location,
            String Username, String Password, String FullText, HashTree ht) {
        this.Identifier = Identifier;
        this.Type = Type;
        this.Name = Name;
        this.Location = Location;
        this.Username = Username;
        this.Password = Password;
        this.htArguments = ht;

        if (FullText.equals("1")) {
            this.FullText = true;
        } else {
            this.FullText = false;
        }
    }
    // This constructor is generated from the output of an API call

    public Repository(HashTree htRepository, HashTree htArguments) {
        this.Identifier = (String) htRepository.get("REPOSITORYID");
        this.Type = (String) htRepository.get("REPOSITORYTYPE");
        this.Name = (String) htRepository.get("REPOSITORYNAME");
        this.Location = (String) htRepository.get("REPOSITORYLOCATION");
        this.Username = (String) htRepository.get("USERNAME");
        this.Password = (String) htRepository.get("PASSWORD");
        this.htArguments = htArguments;

        String sFT = (String) htRepository.get("FULLTEXT");

        if (sFT.equals("1")) {
            this.FullText = true;
        } else {
            this.FullText = false;
        }
    }

    public Repository() {
    }

    // Kickoff a crawler thread to scan the filesubsystem for documents
    public CrawlerThread KickoffCrawler(HashTree htFolder) {
        CrawlerThread ct = new CrawlerThread(htFolder, this);
        ct.start();

        return ct;
    }

    // Kickoff a thread to re-classify documents with problems from last run
    public ProblemThread KickoffCrawler(HashTree htFolder, HashTree htArgs) {
        ProblemThread pt = new ProblemThread("/tmp/problem-documents.log", this);
        pt.start();

        return pt;
    }

    public ClassifierThread KickoffClassfier(com.iw.fcrawl.Machine M) {
        ClassifierThread ct = new ClassifierThread(this);
        ct.Machine = M;

        ct.start();

        return ct;
    }

    // define threads: there are three types, crawler, problem and classifier threads
    private static class CrawlerThread extends Thread {

        HashTree htFolder = new HashTree();
        Repository R = null;

        public CrawlerThread(HashTree ht, Repository DocRepository) {
            htFolder = ht;
            R = DocRepository;
        }

        public void run() {
            R.GetNewObjects(htFolder);
        }
    }

    // define threads: there are three types, crawler, problem and classifier threads
    private static class ProblemThread extends Thread {

        Repository R = null;
        File F = new File("/tmp/problem-documents.log");

        public ProblemThread(String File, Repository DocRepository) {
            R = DocRepository;
            File F = new File(File);
        }

        // open problem-documents.txt and re-run each entry
        public void run() {
            try {
                if (F.exists()) {
                    FileReader fr = new FileReader(F);
                    BufferedReader buf = new BufferedReader(fr);

                    String sData = new String();

                    while ((sData = buf.readLine()) != null) {
                        String s[] = sData.split("\t");
                        R.AddNewDocument(s[0]);
                    }
                }
                F.delete();
            } catch (Exception e) {
                e.printStackTrace(System.out);
                return;
            }
        }
    }

    // define threads: there are three types, crawler, problem and classifier threads
    private static class ClassifierThread extends Thread {

        Repository R = null;
        com.iw.fcrawl.Machine Machine = null;

        public ClassifierThread(Repository DocRepository) {
            R = DocRepository;
        }

        public void run() {
            while (R.bCrawlInProgress() || (!R.GetNewDocuments().isEmpty())) {
                String sDocPath = "";
                synchronized (R) {
                    Vector v = R.GetNewDocuments();

                    if (!v.isEmpty()) {
                        sDocPath = (String) v.firstElement();
                        v.removeElement(sDocPath);
                    }
                }
                if (!sDocPath.equals("")) {
                    // Update security, reclassify, and reindex document
                    if (!R.UpdateDocumentSecurity(sDocPath, Machine)) {
                        System.out.println("Warning: Could not update document security for document: " + sDocPath);
                    }
                    try {
                        if (!R.ClassifyDocument(sDocPath, Machine)) {
                            try {
                                R.LogError(sDocPath, Machine);
                            } catch (Exception e) {
                                System.out.println("Warning: Could not open log file for writing.");
                            }
                        }
                    } catch (com.iw.fcrawl.ClassifyException ce) {
                        try {
                            R.LogError(sDocPath, Machine, ce);
                        } catch (Exception e) {
                            System.out.println("Warning: Could not open log file for writing.");
                        }
                    }
                    R.AddDocumentIndex(sDocPath, Machine);
                }
                try {
                    this.wait(1000);
                } catch (Exception e) {
                }
            }
        }
    }

    // GetNewObjects: The String version of this method should be overridden in each subclass
    public void GetNewObjects(HashTree htFolder) {
        String sDate = (String) htFolder.get("DATELASTCAPTURE");
        String sPath = (String) htFolder.get("PATH");
        String sRecurse = (String) htFolder.get("RECURSE");
        boolean bRecurse = true;

        if (sRecurse.equals("0")) {
            bRecurse = false;
        }

        GetNewObjects(sPath, sDate, bRecurse);
    }

    public void GetNewObjects(String sPath, String sDateLastCapture, boolean bRecurse) {
        System.out.println("Base repository class invoked for GetNewObjects (programming error).");
    }

    // Get the document source and return in a string.  This function assumes that the source path is
    //    either HTTP or a FILE path.   Specific repository types can override this function to specify
    //    other ways to retrieve documents.
    public String getSource(String sPath) throws Exception {
        //System.out.println("**DEBUG** getSource called with: "+sPath);

        StringBuilder sData = new StringBuilder();
        File tempFile = null;
        boolean bRemoveTempFile = false;

        if (sPath.toLowerCase().startsWith("http")) {
            HttpURLConnection httpCon = null;

            String s = "";

            try {
                URL myURL = new URL(sPath);
                bRemoveTempFile = true;

                httpCon = (HttpURLConnection) myURL.openConnection();
                httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
                httpCon.setDoInput(true);
                httpCon.setDoOutput(true);
                httpCon.setUseCaches(false);
                httpCon.setDefaultUseCaches(false);

                if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new Exception("Http error opening " + myURL.getUserInfo() + " : " + httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
                }

                InputStream is = httpCon.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                String sExt = "htm";
                if (httpCon.getContentType().equals("text/html")) {
                    sExt = "html";
                }
                if (httpCon.getContentType().equals("plain/text")) {
                    sExt = "txt";
                }
                if (httpCon.getContentType().equals("application/pdf")) {
                    sExt = "pdf";
                }
                if (httpCon.getContentType().indexOf("word") != -1) {
                    sExt = "doc";
                }

                int len = 0;
                FileOutputStream fos = null;
                byte[] buffer = new byte[512];
                int bytesRead = 0;

                while ((len = bis.read(buffer)) > 0) {
                    bos.write(buffer, 0, len);
                }
                bis.close();
                httpCon.disconnect();

                tempFile = getTempFile("/temp/", sExt);
                System.out.println("Writing to temp file: " + tempFile.getAbsolutePath());

                fos = new FileOutputStream(tempFile);
                fos.write(bos.toByteArray());

                bos.flush();
                fos.flush();
                bos.close();
                fos.close();

            } catch (Exception e) {
                System.out.println("HTTP/File Write failure attempting to retrieve : " + sPath + " : " + e.toString());
                return null;
            }
        } else {
            tempFile = new File(sPath);
        }

        try {
            if (sPath.toLowerCase().endsWith(".pdf")) {
                tempFile = FcrawlUtils.PDFtoHTML(new File(sPath));
            } else if (sPath.toLowerCase().endsWith(".doc")) {
                tempFile = FcrawlUtils.MSWORDtoHTML(new File(sPath));
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
            throw e;
        }

        // if the file pointer we have is a UNC, use samba to retrieve it, otherwise use a standard retrieval method
        if (tempFile.getAbsolutePath().startsWith("\\\\")) {
            // USE SAMBA FOR UNC
            String connectString = "smb://";
            try {
                // Build the samba connection descriptor here.  In order to retrieve the source, we must use a UNC
                // "smb://domain;user:pass <at> machine/share/dir/dir/dir/file"
                if ((!Username.trim().equals("")) && (Username != null)) {
                    connectString = connectString + Username + ":" + Password + "@";
                }
                connectString = connectString + tempFile.getAbsolutePath().substring(2).replace('\\', '/');

                System.out.println("Attempting to read: " + connectString);

                SmbFile f = new SmbFile(connectString);
                SmbFileInputStream in = new SmbFileInputStream(f);

                BufferedReader buf = new BufferedReader(new InputStreamReader(in));

                String s = "";
                while ((s = buf.readLine()) != null) {
                    sData.append(s);
                }

            } catch (Exception e) {
                System.err.println("Unable to resolve samba connect descriptor: " + connectString);
                throw e;
            }
        } else { // use a more traditional method of retrieval
            BufferedReader in = new BufferedReader(new FileReader(tempFile));
            while (in.ready()) {
                sData.append(in.readLine());
            }
            in.close();
        }

        if (bRemoveTempFile) {
            tempFile.delete();
        }

        return sData.toString();
    }

    // Get and store the current date - will be used to update the timestamp later.
    public String GetCurrentDate() throws Exception {
        InvokeAPI API = new InvokeAPI("tsother.TSGetCurrentDate", htArguments);
        HashTree htResults = API.Execute(false, false);

        if (!htResults.containsKey("DATE")) {
            System.out.println("Could not retrieve current date from the API.");
            return "";
        } else {
            String sDate = (String) htResults.get("DATE");
            return sDate;
        }
    }

    public String GetCurrentDate(HashTree htFolder) throws Exception {
        InvokeAPI API = new InvokeAPI("tsother.TSGetCurrentDate", htArguments);
        HashTree htResults = API.Execute(false, false);

        if (!htResults.containsKey("DATE")) {
            System.out.println("Could not retrieve current date from the API.");
            return "";
        } else {
            String sDate = (String) htResults.get("DATE");
            return sDate;
        }
    }

    // If an error occurs, store the document off so it can be run later
    public void LogError(String sDocPath, com.iw.fcrawl.Machine m)
            throws Exception {
        int i = m.FoundProblem();
        synchronized (this) {
            FileWriter out = new FileWriter("/tmp/problem-documents.log", true);
            System.out.println("Warning: Error returned from classifier.  This error has been registered on "
                    + "machine: " + m.GetAPI() + " " + i + " times.");
            out.write(sDocPath + "\t" + m.GetAPI() + "\r\n");
            out.close();
        }
        return;
    }

    // If an error occurs, store the document off so it can be run later
    public void LogError(String sDocPath, com.iw.fcrawl.Machine m, com.iw.fcrawl.ClassifyException ce)
            throws Exception {
        int i = m.FoundProblem();
        synchronized (this) {
            FileWriter out = new FileWriter("/tmp/problem-documents.log", true);
            System.out.println("Warning: Error returned from classifier.  This error has been registered on "
                    + "machine: " + m.GetAPI() + " " + i + " times.");
            out.write(sDocPath + "\t" + m.GetAPI() + "\t" + ce.TS_EXCEPTION_GETMESSAGE + "\r\n");
            out.close();
        }
        return;
    }

    public static File getTempFile(String sPath) throws Exception {
        return getTempFile(sPath, "html");
    }

    public static File getTempFile(String sPath, String sExtension) throws Exception {
        Random wheel = new Random(); // seeded from the clock
        File tempFile = null;

        do {
            // generate random a number 10,000,000 .. 99,999,999
            int unique = (wheel.nextInt() & Integer.MAX_VALUE) % 90000000 + 10000000;
            String sFirstCharacter = (unique + "").substring(0, 1);
            String sSecondCharacter = (unique + "").substring(1, 2);

            tempFile = new File(sPath + sFirstCharacter + "/" + sSecondCharacter + "/", Integer.toString(unique) + "." + sExtension);
        } while (tempFile.exists());

        // We "finally" found a name not already used. Nearly always the first time.
        // Quickly stake our claim to it by opening/closing it to create it.
        // In theory somebody could have grabbed it in that tiny window since
        // we checked if it exists, but that is highly unlikely.
        synchronized (tempFile) {
            try {
                new FileOutputStream(tempFile).close();
                //System.out.println("Temp file successfully created: "+tempFile.getAbsolutePath());
            } catch (Exception e) {
                System.out.println("Could not generate temp file.");
                //Runtime r = Runtime.getRuntime (); r.wait(1000); // pause a second
                //return getTempFile(sPath, sExtension);
            }
        }

        //System.out.println("Creating temporary file: "+tempFile.getAbsolutePath());
        return tempFile;
    }

    // insert just this document into the database.  this is usually called prior
    //  to a document filter.
    public String InsertDocument(String URL, String Path) throws Exception {
        htArguments.put("DocURL", URL);
        htArguments.put("filepath", Path);

        InvokeAPI API = new InvokeAPI("tsdocument.TSAddDocument", htArguments);
        HashTree htResults = API.Execute(false, false);

        if (!htResults.containsKey("DOCUMENTID")) {
            System.out.println("Could not insert document: " + Path);
            return "-1";
        }

        return (String) htResults.get("DOCUMENTID");
    }

    public boolean UpdateFolderTimestamp(String Path, String Timestamp) throws Exception {
        htArguments.put("Path", Path);
        htArguments.put("Date", Timestamp);
        InvokeAPI API = new InvokeAPI("tsrepository.TSSetFolderDate", htArguments);
        HashTree htResults = API.Execute(false, false);

        if (!htResults.containsKey("SUCCESS")) {
            System.out.println("Could not reset date for path: " + Path);
            return false;
        }

        return true;
    }

    // These three functions are repository specific and must be overridden
    public boolean ClassifyDocument(String sPath, com.iw.fcrawl.Machine M) throws com.iw.fcrawl.ClassifyException {
        return false;
    }

    public boolean UpdateDocumentSecurity(String sPath, com.iw.fcrawl.Machine M) {
        return false;
    }

    public boolean ReindexDocuments() {
        return false;
    }

    public boolean ReindexDocument(Hashtable ht, com.iw.fcrawl.Machine M) {
        return false;
    }

    public void AddDocumentIndex(String sPath, com.iw.fcrawl.Machine M) {
        return;
    }
}
