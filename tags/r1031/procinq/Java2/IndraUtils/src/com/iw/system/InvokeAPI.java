package com.iw.system;

import Logging.APIProps;
import Logging.Log;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.dom4j.io.SAXReader;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb All rights reserved. Date: Jul 23, 2003 Time: 1:35:47 PM
 */
// InvokeAPI is a standardized JAVA wrapper used to invoke the Indraweb ITS API
//
// Example usage:
//
//  InvokeAPI API = new InvokeAPI ("security.TSLogin", htArgs);
//	HashTree htResults = API.Execute();
public class InvokeAPI {

    private static final Logger log = Logger.getLogger(InvokeAPI.class);
    private String sTaxonomyServer;
    private String sAPIcall;
    private Hashtable htArguments;
    private String SessionKey = null;
    public PrintWriter out = null;
    final private String boundary = "ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
    final private String twoHyphens = "--";
    final private String lineEnd = "\r\n";

    // Accessor methods to private variables
    public String GetTaxonomyServer() {
        return sTaxonomyServer;
    }

    public void SetTaxonomyServer(String sTaxonomyServer_) {
        sTaxonomyServer = sTaxonomyServer_;
    }

    public String GetAPIcall() {
        return sAPIcall;
    }

    public String GetSessionKey() {
        return SessionKey;
    }

    public Hashtable GetAPIarguments() {
        return htArguments;
    }

    // Object constructors
    public InvokeAPI(String sAPIcall, Hashtable htArguments, String sTaxonomyServer) {
        this.sAPIcall = sAPIcall;
        this.sTaxonomyServer = sTaxonomyServer;
        this.htArguments = htArguments;

        if (htArguments.containsKey("SKEY")) {
            this.SessionKey = (String) htArguments.get("SKEY");
        }
    }

    public InvokeAPI(String sAPIcall, Hashtable htArguments) {

        this.sAPIcall = sAPIcall;
        this.htArguments = htArguments;
        this.sTaxonomyServer = com.indraweb.execution.Session.cfg.getProp("API");

        if (htArguments.containsKey("SKEY")) {
            this.SessionKey = (String) htArguments.get("SKEY");
        }
    }

    // This is an internal function -- if a tag name already exists, append to it
    private String CreateName(Hashtable htHash, String sName) {
        int loop = 0;
        String sOrig = sName;

        // If the tag already exists, append to it and make it unique
        while (htHash.containsKey(sName)) {
            loop++;
            sName = sOrig + loop;
        }

        return sName;
    }

    // Execute the API call.   Returns success 0 for success, -1 for failure.
    public HashTree Execute(boolean bDebug, boolean bUseProxy) throws Exception {
        return Execute(bDebug, bUseProxy, true, true);
    }

    public HashTree Execute(boolean bDebug, boolean bUseProxy, boolean bReplaceQuotes) throws Exception {
        return Execute(bDebug, bUseProxy, bReplaceQuotes, true);
    }

    public HashTree Execute(boolean bDebug, boolean bUseProxy, boolean bReplaceQuotes, boolean bURLencode) throws Exception {
        String sURL = sTaxonomyServer + sAPIcall;
        Enumeration e = htArguments.keys();

        HashTree htOriginal = new HashTree();
        htOriginal.SetParent(null);
        HashTree htObject = htOriginal;

        // Build URL to call Taxonomy Server
        while (e.hasMoreElements()) {
            String sKey = (String) e.nextElement();
            String sValue = (String) htArguments.get(sKey);

            if (!sKey.equals("api")) {
                if (bReplaceQuotes) {
                    sKey = sKey.replaceAll("'", "''");
                    sValue = sValue.replaceAll("'", "''");
                }
                sKey = sKey.replaceAll(" ", "+");
                if (bURLencode) {
                    sValue = URLEncoder.encode(sValue, "UTF-8");
                }

                sURL = sURL + "&" + sKey + "=" + sValue;
            }
        }

        //api.Log.Log("URL: "+sURL);

        // Make a socket connection to the server
        HttpURLConnection httpCon = null;
        try {
            URL myURL = null;

            if (!bUseProxy) {
                myURL = new URL(sURL);
            } else {
                myURL = new URL("http", // protocol,
                        "172.16.101.110", // host name or IP of proxy server to use
                        8080, // proxy port or -1 to indicate the default port for the protocol
                        sURL);  // the original URL, specified in the "file" parameter
            }

            log.debug("API call: " + sURL);
            while (httpCon == null) {
                httpCon = (HttpURLConnection) myURL.openConnection();

                try {
                    if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        log.error("Http error: " + sURL + " " + httpCon.getResponseMessage());
                        httpCon = null;
                    }
                } catch (Exception ex) {
                    log.warn("Warning! Http error: " + sURL + " " + ex.getMessage() + " .. retrying.. ");
                    httpCon = null;
                    Thread.sleep(5000);
                }
            }

            InputStream is = httpCon.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                log.debug(sData);
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = "";
                }

                if (sTag1.equals("SESSIONEXPIRED")) {
                    log.error("Session Expired!");
                    throw new SessionExpired("Session expired");
                }
                if (sTag1.equals("DATAERROR")) {
                    throw new NotAuthorized("Data integrity violation");
                }

                if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"UTF-8\" ?"))
                        && (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT"))
                        && (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT"))
                        && (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

                    if (bDebug && sTag1.equals("NODE")) {
                        log.debug("debug mode");
                    }

                    // If s1Tag is populated but s2Tag is not, start the object
                    if ((!sTag1.equals("")) && (sTag2.equals(""))) {
                        sTag1 = CreateName(htObject, sTag1);
                        //System.out.println("<BR>Begin object: "+sTag1);

                        if (sTag1.equals("TS_ERROR")) {
                            httpCon.disconnect();
                            return htOriginal;
                        }
                        if (sTag1.equals("INTERNALSTACKTRACE")) {
                            httpCon.disconnect();
                            return htOriginal;
                        }

                        HashTree htOldObject = htObject;
                        htObject = new HashTree();
                        htObject.SetParent(htOldObject);
                        htOldObject.put(sTag1, htObject);

                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if ((!sTag2.equals("")) && (sTag1.equals(""))) {
                        // out.println("<BR>End object: "+sTag2);
                        htObject = (HashTree) htObject.GetParent();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                        String sTag = getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">");
                        //out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
                        // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                        // If the variable value contains a "CDATA", eliminate it here
                        if (sTag.indexOf("<![CDATA") != -1) {
                            sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                        }

                        if (htObject.containsKey(sTag1)) {
                            sTag1 = CreateName(htObject, sTag1);
                        }

                        htObject.put(sTag1, sTag);
                    }
                }
            }

            httpCon.disconnect();
            httpCon = null;

            return htObject;
        } catch (NotAuthorized na) {
            log.error("An exception ocurred. ", na);
            throw na;
        } catch (SessionExpired se) {
            log.error("An exception ocurred. ", se);
            throw se;
        } catch (Exception except) {
            log.error("An exception ocurred. ", except);
            throw except;
        } finally {
            if (httpCon != null) {
                httpCon.disconnect();
                httpCon = null;
            }
        }
    }

    // ******** Bring results back as vector instead of hash tree
    public Vector vExecute() throws Exception {
        return vExecute(false, false);
    }

    public Vector vExecute(boolean bDebug, boolean bUseProxy) throws Exception {
        String sURL = sTaxonomyServer + sAPIcall;
        Enumeration e = htArguments.keys();

        VectorTree vOriginal = new VectorTree();
        vOriginal.SetParent(null);
        VectorTree vObject = vOriginal;

        // Build URL to call Taxonomy Server
        while (e.hasMoreElements()) {
            String sKey = (String) e.nextElement();
            String sValue = (String) htArguments.get(sKey);

            if (!sKey.equals("api")) {
                sValue = URLEncoder.encode(sValue, "UTF-8");
                sURL = sURL + "&" + sKey + "=" + sValue;
            }
        }

        // Make a socket connection to the server
        HttpURLConnection httpCon = null;
        try {
            URL myURL = null;

            if (!bUseProxy) {
                myURL = new URL(sURL);
            } else {
                myURL = new URL("http", // protocol,
                        "172.16.101.110", // host name or IP of proxy server to use
                        8080, // proxy port or -1 to indicate the default port for the protocol
                        sURL);  // the original URL, specified in the "file" parameter
            }

            log.debug("API call: " + sURL);
            httpCon = (HttpURLConnection) myURL.openConnection();

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                log.error("Http error: " + httpCon.getResponseMessage());
                throw new Exception("Http error: " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                if (bDebug) {
                    log.error(sData);
                }
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = "";
                }

                if (sTag1.equals("SESSIONEXPIRED")) {
                    log.error("Session Expired!");
                    throw new SessionExpired("Session expired");
                }

                // HACK! HACK! Ignore lines that start with..
                if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"UTF-8\" ?"))
                        && (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT"))
                        && (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT"))
                        && (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

                    // If s1Tag is populated but s2Tag is not, start the object
                    if ((!sTag1.equals("")) && (sTag2.equals(""))) {

                        if (sTag1.equals("TS_ERROR")) {
                            httpCon.disconnect();
                            return vOriginal;
                        }
                        if (sTag1.equals("INTERNALSTACKTRACE")) {
                            httpCon.disconnect();
                            return vOriginal;
                        }
                        if (sTag1.equals("INTERNALERRMSG")) {
                            httpCon.disconnect();
                            return vOriginal;
                        }

                        VectorTree vOldObject = vObject;
                        vObject = new VectorTree();
                        vObject.SetParent(vOldObject);
                        vOldObject.addElement(vObject);
                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if ((!sTag2.equals("")) && (sTag1.equals(""))) {
                        vObject = (VectorTree) vObject.GetParent();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                        String sTag = getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">");

                        // If the variable value contains a "CDATA", eliminate it here
                        if (sTag.indexOf("<![CDATA") != -1) {
                            sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                        }

                        VectorValue vv = new VectorValue(sTag1, sTag);
                        vObject.addElement(vv);
                    }
                }
            }

            httpCon.disconnect();
            return vObject;
        } catch (SessionExpired se) {
            throw se;
        } catch (Exception except) {
            return null;
        }
    }

    // ******** Bring results back as DOM tree instead of hash tree or vector
    public org.dom4j.Document dExecute() {
        return dExecute(false, false);
    }

    public org.dom4j.Document dExecute(boolean bDebug, boolean bUseProxy) {
        try {
            HttpURLConnection httpCon = getHttpURLConnection(bUseProxy);
            InputStream is = httpCon.getInputStream();

            SAXReader xmlReader = new SAXReader(false);
            if (bDebug) {
                InvokeHandler handler = new InvokeHandler();
                xmlReader.setDefaultHandler(handler);
                ErrorHandler eh = new ErrorHandler() {

                    @Override
                    public void fatalError(SAXParseException s) {
                        log.error("fatal error.");
                    }

                    @Override
                    public void error(SAXParseException s) {
                        log.error("error.");
                    }

                    @Override
                    public void warning(SAXParseException s) {
                        log.warn("warning.");
                    }
                };
                xmlReader.setErrorHandler(eh);
            }
            InputSource ins = new InputSource(is);
            ins.setEncoding("UTF-8");

            org.dom4j.Document doc = xmlReader.read(ins);

            httpCon.disconnect();
            return doc;
        } catch (Exception except) {
            log.error("An exception ocurred.", except);
        }
        return null;
    }

    public org.dom4j.Document dExecute(File f) {
        return dExecute(f, false, false);
    }

    public org.dom4j.Document dExecute(File f, boolean bDebug, boolean bUseProxy) {
        log.debug("dExecute(File, boolean, boolean)");
        // Make a socket connection to the server
        HttpURLConnection httpURLConn = null;
        DataOutputStream outStream;
        FileInputStream fileInputStream;
        InputStream is = null;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        try {
            // create FileInputStream to read from file
            fileInputStream = new FileInputStream(f);

            URL theURL = getURL(bUseProxy);
            httpURLConn = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream(httpURLConn.getOutputStream());

            outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\"" + f.getName() + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();
            fileInputStream = null;
            outStream.flush();
            outStream.close();

            // display server response data on console.
            is = httpURLConn.getInputStream();
            SAXReader xmlReader = new SAXReader(false);
            org.dom4j.Document doc = xmlReader.read(is);

            return doc;
        } catch (Exception exception) {
            log.error("Error in InvokeAPI::dExecute(File,boolean,boolean)", exception);
        } finally {
            try {
                if (httpURLConn != null) {
                    httpURLConn.disconnect();
                    httpURLConn = null;
                }
                if (is != null) {
                    is.close();
                }
            } catch (Exception exception) {
                log.error("Error in InvokeAPI::dExecute(File,boolean,boolean)", exception);
            }
        }
        return null;
    }

    // Send POST API call
    public HashTree PostExecute(File f) throws Exception {
        return PostExecute(f, true);
    }

    public HashTree PostExecute(File f, boolean bUseImportDir) throws Exception {
        try {
            HashTree htOriginal = new HashTree();
            htOriginal.SetParent(null);
            HashTree htObject = htOriginal;

            HttpURLConnection httpURLConn = null;
            DataOutputStream outStream;
            DataInputStream inStream;
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            // create FileInputStream to read from file
            FileInputStream fileInputStream = new FileInputStream(f);

            String sURL = sTaxonomyServer + sAPIcall;
            Enumeration et = htArguments.keys();

            // Build URL to call Taxonomy Server
            while (et.hasMoreElements()) {
                String sKey = (String) et.nextElement();
                String sValue = (String) htArguments.get(sKey);

                sURL = sURL + "&" + sKey + "=" + sValue;

                // Remove any apostraphies (because they are illegal) and change
                // spaces into + signs
                sURL = sURL.replaceAll(" ", "+");
                sURL = sURL.replaceAll("'", "");
            }

            URL theURL = new URL(sURL);
            httpURLConn = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream(httpURLConn.getOutputStream());

            outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\"" + f.getName() + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();
            outStream.flush();
            outStream.close();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                //api.Log.Log(sData);
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = "";
                }

                // HACK! HACK! Ignore lines that start with..
                if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?"))
                        && (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT"))
                        && (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT"))
                        && (!sTag1.equals("IndraXMLArgs"))
                        && (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

                    // If s1Tag is populated but s2Tag is not, start the object
                    if ((!sTag1.equals("")) && (sTag2.equals(""))) {

                        sTag1 = CreateName(htObject, sTag1);
                        //out.println("<BR>Begin object: "+sTag1);

                        if (sTag1.equals("TS_ERROR")) {
                            return htOriginal;
                        }
                        if (sTag1.equals("INTERNALSTACKTRACE")) {
                            throw new Exception("Internal stack trace");
                        }

                        HashTree htOldObject = htObject;
                        htObject = new HashTree();
                        htObject.SetParent(htOldObject);
                        htOldObject.put(sTag1, htObject);

                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if ((!sTag2.equals("")) && (sTag1.equals(""))) {
                        // out.println("<BR>End object: "+sTag2);
                        htObject = (HashTree) htObject.GetParent();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                        String sTag = getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">");
                        //out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
                        // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                        // If the variable value contains a "CDATA", eliminate it here
                        if (sTag.indexOf("<![CDATA") != -1) {
                            sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                        }

                        htObject.put(sTag1, sTag);
                    }
                } else {
                    //System.out.println("InvokeAPI ignoring XML line [" + sTag1 + "]" );
                }
            }

            return htObject;
        } catch (Exception except) {
            throw except;
        }

    }

    // Send POST API call
    public Vector vPostExecute(File f) throws Exception {
        return vPostExecute(f, true);
    }

    public Vector vPostExecute(File f, boolean bUseImportDir) throws Exception {
        try {
            VectorTree vOriginal = new VectorTree();
            vOriginal.SetParent(null);
            VectorTree vObject = vOriginal;

            HttpURLConnection httpURLConn = null;
            DataOutputStream outStream;
            DataInputStream inStream;
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            // create FileInputStream to read from file
            FileInputStream fileInputStream = new FileInputStream(f);

            String sURL = sTaxonomyServer + sAPIcall;
            Enumeration et = htArguments.keys();

            // Build URL to call Taxonomy Server
            while (et.hasMoreElements()) {
                String sKey = (String) et.nextElement();
                String sValue = (String) htArguments.get(sKey);

                sURL = sURL + "&" + sKey + "=" + sValue;

                // Remove any apostraphies (because they are illegal) and change
                // spaces into + signs
                sURL = sURL.replaceAll(" ", "+");
                sURL = sURL.replaceAll("'", "");
            }

            URL theURL = new URL(sURL);
            httpURLConn = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream(httpURLConn.getOutputStream());

            outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\"" + f.getName() + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();
            outStream.flush();
            outStream.close();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                //api.Log.Log(sData);
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = "";
                }

                // HACK! HACK! Ignore lines that start with..
                if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?"))
                        && (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT"))
                        && (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT"))
                        && (!sTag1.equals("IndraXMLArgs"))
                        && (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

                    // If s1Tag is populated but s2Tag is not, start the object
                    if ((!sTag1.equals("")) && (sTag2.equals(""))) {

                        if (sTag1.equals("TS_ERROR")) {
                            return vOriginal;
                        }
                        if (sTag1.equals("INTERNALSTACKTRACE")) {
                            throw new Exception("Internal stack trace");
                        }

                        VectorTree vOldObject = vObject;
                        vObject = new VectorTree();
                        vObject.SetParent(vOldObject);
                        vOldObject.addElement(vObject);
                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if ((!sTag2.equals("")) && (sTag1.equals(""))) {
                        // out.println("<BR>End object: "+sTag2);
                        vObject = (VectorTree) vObject.GetParent();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                        String sTag = getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">");
                        //out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
                        // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                        // If the variable value contains a "CDATA", eliminate it here
                        if (sTag.indexOf("<![CDATA") != -1) {
                            sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                        }

                        vObject.addElement(sTag);
                    }
                }
            }

            return vObject;
        } catch (Exception except) {
            throw except;
        }

    }

    // If we are patient enough to get the entire XML footprint, use Execute.   For results, which
    //  must be streamed, use "Stream Results".
    //
    // Returns TRUE if 1 or more results, false otherwise.
    public int StreamResults(PrintWriter out, HTML.HTMLDocument Document, int iStart, int iEnd) {
        return StreamResults(out, Document, iStart, iEnd, "", true);
    }

    public int StreamResults(PrintWriter out, HTML.HTMLDocument Document, int iStart, int iEnd, String sKEY, boolean bAdminAccess) {
        String sURL = sTaxonomyServer + sAPIcall;
        Enumeration e = htArguments.keys();
        int iResults = 0;

        // Build URL to call Taxonomy Server
        while (e.hasMoreElements()) {
            String sKey = (String) e.nextElement();
            String sValue = (String) htArguments.get(sKey);

            sURL = sURL + "&" + sKey + "=" + sValue;

            // Remove any apostraphies (because they are illegal) and change
            // spaces into + signs
            sURL = com.indraweb.util.UtilStrings.replaceStrInStr(sURL, " ", "+");
            sURL = com.indraweb.util.UtilStrings.replaceStrInStr(sURL, "'", "");
        }

        // Make a socket connection to the server
        try {
            long lStart = System.currentTimeMillis();

            URL myURL = new URL(sURL);
            out.println("<!-- " + sURL + " -->");
            HttpURLConnection httpCon = (HttpURLConnection) myURL.openConnection();

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String sData = new String();

            Hashtable htResult = new Hashtable();
            int loop = 0;

            while ((sData = buf.readLine()) != null) {
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = "";
                }

                // On start tag, set up environment
                if (sTag1.equals("NODEDOCUMENT")) {
                    loop++;
                    htResult = new Hashtable();
                }

                // On end tag, stream out to document
                if (sTag2.equals("NODEDOCUMENT")) {
                    // Set URLencoded variables
                    String sEDocURL = "";
                    if (htResult.containsKey("DOCURL")) {
                        sEDocURL = URLEncoder.encode((String) htResult.get("DOCURL"), "UTF-8");
                    }
                    String sEDocSum = "";
                    if (htResult.containsKey("DOCSUMMARY")) {
                        sEDocSum = URLEncoder.encode((String) htResult.get("DOCSUMMARY"), "UTF-8");
                    }
                    String sEDocTitle = "";
                    if (htResult.containsKey("DOCTITLE")) {
                        sEDocTitle = URLEncoder.encode((String) htResult.get("DOCTITLE"), "UTF-8");
                    }

                    String Type = (String) htResult.get("TYPE");
                    String sNodeID = (String) htResult.get("NODEID");
                    String sDocID = (String) htResult.get("DOCUMENTID");
                    String sDocURL = (String) htResult.get("DOCURL");
                    String sDocumentSummary = (String) htResult.get("DOCUMENTSUMMARY");
                    String sDateLastFound = ((String) htResult.get("DATELASTFOUND")).substring(0, ((String) htResult.get("DATELASTFOUND")).length() - 5);

                    Document.AddVariable("STARS", Taxonomy.DisplayResults.BuildStars((String) htResult.get("SCORE1")));
                    Document.AddVariable("LOOPI", new Integer((iStart - 1) + loop).toString());
                    Document.AddVariable("EDOCURL", sEDocURL);
                    Document.AddVariable("EDOCSUM", sEDocSum);
                    Document.AddVariable("EDOCTITLE", sEDocTitle);
                    Document.AddVariable("DATELASTFOUND", sDateLastFound);

                    Document.AddVariable("EXPLAIN", "(<a href=\"/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=" + sEDocURL
                            + "&explainscores=true&nodetoscoreexplain=" + sNodeID + "&explainscoresshowdocwords=true&SKEY=" + sKEY + "\">explain</a>)");

                    Document.AddVariable("NUGGETS", "");
                    /*
                     * nuggets no longer used if ((Type != null) && (Type.equals("0")) && bUseNuggets) { Document.AddVariable("NUGGETS", " or <a href='/servlet/Main?template=Taxonomy." +
                     * "GetNuggets&NodeID=" + sNodeID + "&DocID=" + sDocID + "&DocURL=" + sEDocURL + "&DocTitle=" + sEDocTitle + "'>nuggets</a>"); }
                     */

                    Document.SetHash(htResult);
                    if (sDocURL.length() > 60) {
                        Document.AddVariable("PDOCURL", sDocURL.substring(0, 60) + "...");
                    } else {
                        Document.AddVariable("PDOCURL", sDocURL);
                    }

                    // temporarily removed
                    //if (!sDocumentSummary.toUpperCase().equals("NULL")) {
                    //Document.AddVariable("DOCSUMMARY", sDocumentSummary);
                    //}
                    if (bAdminAccess) {
                        Document.WriteTemplate(out, "result.tpl");
                    } else {
                        Document.WriteTemplate(out, "result-reader.tpl");
                    }
                }

                // if both s1Tag and s2Tag are populated, store the field into object started
                if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                    String sTag = com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">");
                    // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                    // If the variable value contains a "CDATA", eliminate it here
                    if (com.indraweb.util.UtilStrings.getStrContains(sTag, "<![CDATA")) {
                        sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                    }

                    if (sTag1.equals("NUMRESULTS")) {
                        iResults = new Integer(sTag).intValue();
                    } else {
                        htResult.put(sTag1, sTag);
                    }
                }
            }

            httpCon.disconnect();
            long lStop = System.currentTimeMillis() - lStart;
            out.println("<!-- Timing: " + lStop + " ms -->");

            return iResults;
        } catch (Exception except) {
            Log.LogError(except, out);
            return 0;
        }
    }

    public int StreamFullTextResults(PrintWriter out, HTML.HTMLDocument Document, APIProps props, String q, String sCorpusID) {
        String sURL = com.indraweb.execution.Session.cfg.getProp("FullText") + q;
        sURL = com.indraweb.util.UtilStrings.replaceStrInStr(sURL, " ", "+");
        sURL = com.indraweb.util.UtilStrings.replaceStrInStr(sURL, "'", "");

        int loop = 0;

        // Make a socket connection to the server
        try {
            long lStart = System.currentTimeMillis();

            URL myURL = new URL(sURL);
            out.println("<!-- " + sURL + " -->");
            HttpURLConnection httpCon = (HttpURLConnection) myURL.openConnection();

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String sData = new String();

            Hashtable htResult = new Hashtable();

            while ((sData = buf.readLine()) != null) {
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = "";
                }

                // On start tag, set up environment
                if (sTag1.equals("FULLTEXTRESULT")) {
                    loop++;
                    htResult = new Hashtable();
                    if (loop == 1) {
                        Document.WriteTemplate(out, "search-start-full-text.tpl");
                    }
                }

                // On end tag, stream out to document
                if (sTag2.equals("FULLTEXTRESULT")) {
                    if (loop <= new Integer((String) props.get("fulltext")).intValue()) {
                        HashTree htArgumentsLocal = new HashTree(props);
                        htArgumentsLocal.put("DocumentURL", (String) htResult.get("DOCURL"));
                        InvokeAPI API = new InvokeAPI("tsdocument.TSGetIdracInfo", htArgumentsLocal);
                        HashTree htR = API.Execute(false, false);
                        HashTree htD = (HashTree) htR.get("DOCUMENT");

                        Document.SetHash(htD);
                        Document.SetHash(htResult);

                        // Set URLencoded variables
                        Document.AddVariable("STARS", Taxonomy.DisplayResults.BuildStars((String) htResult.get("SCORE1")));
                        Document.AddVariable("LOOPI", "" + loop);

                        String sEDocURL = URLEncoder.encode((String) htResult.get("DOCURL"), "UTF-8");
                        String sEDocSum = URLEncoder.encode((String) htD.get("DOCSUMMARY"), "UTF-8");
                        String sEDocTitle = URLEncoder.encode((String) htD.get("DOCTITLE"), "UTF-8");
                        Document.AddVariable("EDOCURL", sEDocURL);
                        Document.AddVariable("EDOCSUM", sEDocSum);
                        Document.AddVariable("EDOCTITLE", sEDocTitle);
                        Document.AddVariable("CORPUS", sCorpusID);

                        Document.WriteTemplate(out, "result-full-text.tpl");
                    }
                }

                // if both s1Tag and s2Tag are populated, store the field into object started
                if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                    String sTag = com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">");
                    // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                    // If the variable value contains a "CDATA", eliminate it here
                    if (com.indraweb.util.UtilStrings.getStrContains(sTag, "<![CDATA")) {
                        sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                    }
                    htResult.put(sTag1, sTag);
                }
            }
            if (loop > new Integer((String) props.get("fulltext")).intValue()) {
                Document.WriteTemplate(out, "search-full-text-more.tpl");
            }
            if (loop != 0) {
                if ((String) props.get("fulltextonly") == null) {
                    Document.WriteTemplate(out, "search-full-text-end.tpl");
                } else {
                    Document.WriteTemplate(out, "search-end.tpl");
                    Document.WriteFooter(out);
                }
            } else {
                Document.WriteTemplate(out, "search-start-full-text.tpl");
                out.println("<BR>Sorry, no results matching your search term were found.");
            }
            httpCon.disconnect();
            long lStop = System.currentTimeMillis() - lStart;
            out.println("<!-- Timing: " + lStop + " ms -->");

            return loop;
        } catch (Exception except) {
            Log.LogError(except, out);
            return loop;
        }
    }

    public int StreamSearchResults(PrintWriter out, HTML.HTMLDocument Document, String sKEY, APIProps props, int iVResults) {
        String sURL = sTaxonomyServer + sAPIcall;
        Enumeration e = htArguments.keys();
        int iResults = 0;
        String sResultType = "0";

        // Build URL to call Taxonomy Server
        while (e.hasMoreElements()) {
            String sKey = (String) e.nextElement();
            String sValue = (String) htArguments.get(sKey);

            sURL = sURL + "&" + sKey + "=" + sValue;

            // Remove any apostraphies (because they are illegal) and change
            // spaces into + signs
            sURL = com.indraweb.util.UtilStrings.replaceStrInStr(sURL, " ", "+");
            sURL = com.indraweb.util.UtilStrings.replaceStrInStr(sURL, "'", "");
        }

        // Make a socket connection to the server
        try {
            long lStart = System.currentTimeMillis();

            URL myURL = new URL(sURL);
            out.println("<!-- " + sURL + " -->");
            HttpURLConnection httpCon = (HttpURLConnection) myURL.openConnection();

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String sData = new String();

            Hashtable htResult = new Hashtable();
            int loop = 0;

            while ((sData = buf.readLine()) != null) {
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = "";
                }

                // On start tag, set up environment
                if (sTag1.equals("NODE")) {
                    loop++;
                    htResult = new Hashtable();
                    if ((String) props.get("fulltext") == null) {
                        if (loop == 1) {
                            Document.WriteTemplate(out, "search-start.tpl");
                        }
                    }
                }

                // On end tag, stream out to document
                if (sTag2.equals("NODE")) {
                    String sNodeTitle = (String) htResult.get("NODETITLE");
                    if (sNodeTitle.length() > 65) {
                        sNodeTitle = sNodeTitle.substring(0, 65) + "...";
                    }

                    String sNodeTree = Taxonomy.Search.BuildNodeTree(out, (String) htResult.get("PARENTID"), props);
                    sNodeTree = "<LI> <a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="
                            + (String) htResult.get("NODEID") + "'><i><b>"
                            + sNodeTitle + "</b></i></a><BR>" + sNodeTree + "<P>";
                    Document.SetHash(htResult);
                    Document.AddVariable("NODETREE", sNodeTree);


                    // If result types differ, print out new header
                    if (!sResultType.equals((String) htResult.get("RESULTTYPE"))) {
                        sResultType = (String) htResult.get("RESULTTYPE");
                        Document.WriteTemplate(out, "search-end.tpl");

                        if (sResultType.equals("2")) {
                            Document.WriteTemplate(out, "related-start.tpl");
                        }
                        if (sResultType.equals("1")) {
                            Document.WriteTemplate(out, "thes-start.tpl");
                        }
                    }

                    Document.WriteTemplate(out, "search.tpl");
                }

                // if both s1Tag and s2Tag are populated, store the field into object started
                if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                    String sTag = com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">");
                    // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                    // If the variable value contains a "CDATA", eliminate it here
                    if (com.indraweb.util.UtilStrings.getStrContains(sTag, "<![CDATA")) {
                        sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                    }
                    htResult.put(sTag1, sTag);
                }
            }

            if (loop != 0) {
                Document.WriteTemplate(out, "search-end.tpl");
            } else {
                if (iVResults == 0) {
                    Document.WriteTemplate(out, "search-full-text-end.tpl");
                }
                out.println("<br>Sorry, no category results matching your search term were found.<p></blockquote>");
            }
            Document.WriteFooter(out);
            httpCon.disconnect();
            long lStop = System.currentTimeMillis() - lStart;
            out.println("<!-- Timing: " + lStop + " ms -->");

            return iResults;
        } catch (Exception except) {
            Log.LogError(except, out);
            return 0;
        }

    }

    private HttpURLConnection getHttpURLConnection(boolean bUseProxy) throws Exception {
        HttpURLConnection httpCon = null;
        URL myURL = getURL(bUseProxy);
        httpCon = (HttpURLConnection) myURL.openConnection();

        if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
            log.error("Http error: " + httpCon.getResponseMessage());
            throw new Exception("Http error: " + httpCon.getResponseMessage());
        }
        return httpCon;
    }

    private URL getURL(boolean bUseProxy) throws Exception {
        if (sTaxonomyServer == null) {
            throw new Exception("getURL  : no taxonomy server speficied");
        }
        String sURL = sTaxonomyServer + sAPIcall;
        Enumeration e = htArguments.keys();

        // Build URL to call Taxonomy Server
        while (e.hasMoreElements()) {
            String sKey = (String) e.nextElement();
            String sValue = (String) htArguments.get(sKey);

            if (!sKey.equals("api")) {
                sURL = sURL + "&" + sKey + "=" + URLEncoder.encode(sValue, "UTF-8");
            }
        }

        log.debug("URL generated: " + sURL);

        // Make a socket connection to the server
        URL myURL = null;
        if (!bUseProxy) {
            myURL = new URL(sURL);
        } else {
            myURL = new URL("http", // protocol,
                    "172.16.101.110", // host name or IP of proxy server to use
                    8080, // proxy port or '1' to indicate the default port for the protocol
                    sURL);  // the original URL, specified in the 'file' parameter
        }

        log.debug("API call: " + sURL);

        return myURL;

    }
    // get locations of strings

    public static int getLocationOfNthOfThese(String s, String s1, int index1) {
        int currentPointer = -1;
        int incrementAmount = 0;

        for (int i = 0; i < index1; i++) {
            String curString = s.substring(currentPointer + 1);
            incrementAmount = curString.indexOf(s1);

            if (incrementAmount == -1) {
                return -1;
            } else {
                currentPointer += incrementAmount + 1;
            }
        }
        return currentPointer;
    }

    public static String removeChar(String s, char c) {
        String r = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c) {
                r += s.charAt(i);
            }
        }
        return r;
    }

    // string parsing routine
    public static String getStringBetweenThisAndThat(String s, String s1, String s2) {
        int index1 = 1;
        int index2 = 1;
        String returnStr = null;

        int startLoc = 0;
        if (index1 > 0) {
            startLoc = getLocationOfNthOfThese(s, s1, index1) + 1;
            if (startLoc == -1) {
                return "";
            }
        }

        if (index2 > 0) {
            int endLoc = 0;
            int totalLen = startLoc + s1.length();
            if (totalLen == -1) {
                return "";
            }

            endLoc = getLocationOfNthOfThese(s, s2, index2);
            if (endLoc == -1) {
                returnStr = s.substring(startLoc + s1.length() - 1);
            } else {
                try {
                    returnStr = s.substring(startLoc + s1.length() - 1, endLoc);
                } catch (Exception e) {
                    returnStr = "";
                }
            }
        } else {
            returnStr = s.substring(startLoc + 1, s1.length());
        }

        return returnStr;
    }
}