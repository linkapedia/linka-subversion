package com.iw.repository;

import org.dom4j.io.*;
import org.dom4j.*;
import org.xml.sax.*;

import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.net.*;

import com.iw.system.*;
import com.iw.fcrawl.*;
import com.iw.utils.PDFUtils;

// This is the repository class for the Verity Web Service.
public class Verity extends Repository
{
	// Private variable htVectors contains an entry for each corpus containing
	//   documents tagged to be reindexed through Verity
	private Hashtable htVectors = new Hashtable();
	private int iClassifyCounter = 0;
	private int iCrawlCounter = 0;

	// Specific filesystem routine to retrieve new objects from the server that have
	//  changed since DateLastCapture.  Example:
    //
    // http://cw-search.schering.net/veritybin/vr_is.dll?action=xml&resultcount=5000&querytext=websitename<CONTAINS>newsroom<AND>date>2004-12-26<AND>date<2005-01-03
	public void GetNewObjects(String sFeed, String sDateLastCapture, boolean bRecurse) {
        // In order to find new objects, we call over to the Verity web service
        System.out.println("\n\rVerity: querying web service for the feed: "+sFeed);

        String URL = "http://cw-search.schering.net/veritybin/vr_is.dll?action=xml&resultcount=5000&querytext=websitename<CONTAINS>"+sFeed;
        URL = URL + "<AND>date>"+sDateLastCapture.substring(0,4)+"-"+sDateLastCapture.substring(5,7)+"-"+sDateLastCapture.substring(8,10);

        // step 1: call into the verity web service and retrieve the XML response
        System.out.println("Using: "+URL);
        org.dom4j.Document doc = dExecute(URL);

        // step 2: parse document results from the response and add objects accordingly
        Element elemRoot = doc.getRootElement ();
        List eItems = elemRoot.elements("Item");
        Iterator i = eItems.iterator();

        /*
        <Item id="1">
            <Score>0.93</Score>
            <DisplayTitle>Slow-Motion Miracle: One Boy's Journey Out of Autism's Grasp</DisplayTitle>
            <Summary>An 8-year-old boy's continuing journey to break free of the grip of autism reflects a struggle that more than 150,000 American children have faced in the past decade.</Summary>
            <DocSize>0 B</DocSize>
            <DisplayAuthor>Carmen Dr. Ruh-Pohlenz</DisplayAuthor>
            <ModifyDate>2004-12-29</ModifyDate>
            <Keywords>NYT</Keywords>
            <Collection>CW-Quicksearch</Collection>
            <URL>http%3A%2F%2Fcw%2Eschering%2Enet%2FAPPS%2FBE%2FCW%2DNewsroom%2FCW%2DNewsroom%2Ensf%2FContent%2FD26AF55652C33D7FC1256F7900267CDC</URL>
            <StreamURL>http%3A%2F%2Fcw%2Eschering%2Enet%2FAPPS%2FBE%2FCW%2DNewsroom%2FCW%2DNewsroom%2Ensf%2FContent%2FD26AF55652C33D7FC1256F7900267CDC</StreamURL>
            <DocKey>notes%3A%2F%2FBES093%2FBE%2FSRV%2FSHG%21%21APPS%2FBE%2FCW%2DNewsroom%2FCW%2DNewsroom%2Ensf%21%21%2A%21%21D26AF55652C33D7FC1256F7900267CDC</DocKey>
        </Item>
        */
        // loop through each item and add the
        while (i.hasNext()) {
            Element eItem = (Element) i.next();
            Element eURL = eItem.element("URL");
            Element eTit = eItem.element("DisplayTitle");

            try {
                String sDocumentURL = URLDecoder.decode(eURL.getText(), "UTF-8");
                String sDocumentTit = eTit.getText();

                System.out.println("Adding URL ("+sDocumentURL+") Title ("+sDocumentTit+")");
                synchronized (this) { iCrawlCounter++; AddNewDocument(sDocumentURL+"-fnord-"+sDocumentTit); }
            } catch (UnsupportedEncodingException e) {
                System.err.println("UnsupportedEncodingException ..");
                e.printStackTrace(System.err);
            }
        }

		System.out.println("");
		System.out.flush();
	}

	// These three functions are repository specific and must be overridden
	public boolean ClassifyDocument (String sPath, com.iw.fcrawl.Machine M) throws com.iw.fcrawl.ClassifyException {
        System.out.println("ClassifyDocument.. "+sPath);

        String s[] = sPath.split("-fnord-");
        String sURL = s[0]; String sTitle = s[1];

		HashTree htArguments = GetArguments();
  		if (iCrawlCounter == 0) { iCrawlCounter = GetNewDocuments().size(); }

        // download the file and write it to a temporary location
        System.out.println("Verity:Retrieving: "+sURL);
        File tempFile = getData(sURL);
        if (tempFile == null) { System.out.println("Error! Received a NULL pointer attempting to retrieve URL: "+sURL); return false; }

		System.out.println("Verity:Classifying: "+sURL);

		try {
            String sPostedPath = sPath;
            sPostedPath = tempFile.getAbsolutePath();

			String api = M.GetAPI();
			String rid = (String) htArguments.get("rid");
			String gid = (String) htArguments.get("gid");
			String batch = (String) htArguments.get("batch");
			String docsumm = (String) htArguments.get("docsumm");
            String corpora = (String) htArguments.get("corpora");

			HashTree htLocalArguments = new HashTree();

			htLocalArguments.put("SKEY", M.GetSKEY());
            htLocalArguments.put("api", api);
			htLocalArguments.put("RepositoryID", rid);
			htLocalArguments.put("ShowScores", "false");
			htLocalArguments.put("DBURL", sURL);
            htLocalArguments.put("DocTitle", sTitle);
			htLocalArguments.put("post", "true");
			htLocalArguments.put("batch", batch);
			htLocalArguments.put("WANTDOCSUMMARY", "true");
			if (gid != null) { htLocalArguments.put("GenreID", gid); }
            if (corpora != null) { htLocalArguments.put("Corpora", corpora); }

            try {
                InvokeAPI API = new InvokeAPI ("tsclassify.TSClassifyDoc", htLocalArguments);
                System.out.println("Posting file: "+sPostedPath);
                HashTree htResults = API.PostExecute(new File(sPostedPath));
                iClassifyCounter++;

                System.out.println(iClassifyCounter+" of "+iCrawlCounter+" complete.");
                System.out.println("\nFilesystem:Document: "+sPath+" classified successfully! ("+iClassifyCounter+")");
            } catch (com.iw.fcrawl.ClassifyException ce) {
                System.out.println("There was a system error while classifying document: "+sPath);
                System.out.println("Error code: "+ce.TS_ERROR_CODE);
                System.out.println("Error message: "+ce.TS_EXCEPTION_GETMESSAGE);
                System.out.println("Error description: "+ce.TS_ERROR_DESC);
                System.out.println("");

                throw ce;
            } finally { if (tempFile != null) tempFile.delete(); }

		} catch (Exception e) { e.printStackTrace(System.out); return false; }
        finally { tempFile.delete(); }

		return true;
	}
	public boolean UpdateDocumentSecurity (String sPath, com.iw.fcrawl.Machine M) { return true; }
	public boolean ReindexDocuments () { return true; }

    public File getData (String URL) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        boolean bPDFDocument = false;
        boolean bWordDocument = false;

        String s = "";

        try {
            URL myURL = new URL(URL);

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "htm";
            if (httpCon.getContentType().equals("text/html")) { sExt = "html"; }
            if (httpCon.getContentType().equals("plain/text")) { sExt = "txt"; }
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; bPDFDocument = true; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; bWordDocument = true; }

            int len = 0; File tempFile = null; FileOutputStream fos = null;

            byte[] buffer = new byte[512];
            int bytesRead = 0;

            while( (len=bis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
            } bis.close(); httpCon.disconnect();

            tempFile = getTempFile("/temp/", sExt);
            System.out.println("Writing to temp file: "+tempFile.getAbsolutePath());

            fos = new FileOutputStream(tempFile);
            fos.write(bos.toByteArray());

            bos.flush(); fos.flush();
            bos.close(); fos.close();

            // If this is a PDF document, and WriteToFile is true, we will use the PDFBox libraries
            // to effect a conversion.
            //System.out.println("Write to File: "+bWriteToFile+" PDF Doc: "+bPDFDocument+" Filename: "+tempFile.getAbsolutePath());
            if (bPDFDocument) {
                System.out.println("Filtering from PDF: "+tempFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath()+".html");
                try { PDFUtils.convertPDFToHTML(tempFile, tempFile.getAbsolutePath()+".html"); }
                catch (java.lang.OutOfMemoryError e) {
                    System.err.println("Out of memory trying to filter file "+ tempFile.getAbsolutePath() +" .. skipping ...");
                    return tempFile;
                }
                tempFile.delete();
                tempFile = new File(tempFile.getAbsolutePath()+".html");
            }
            if (bWordDocument) {
                System.out.println("Filtering from MSWORD: "+tempFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath()+".html");
                tempFile.delete();
                tempFile = FcrawlUtils.MSWORDtoHTML(new File(tempFile.getAbsolutePath()));
                tempFile = new File(tempFile.getAbsolutePath()+".html");
            }

            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("Item content retrieved "+URL+" in "+lEnd+" ms.");

            //System.out.println("RETURNING: ### "+s+" ###");
            return tempFile;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms attempting to retrieve : "+URL+" : "+e.toString());
            //e.printStackTrace(System.out);
            return null;
        } finally {
            // Check to make sure that we have not exceeded our maximum run time.  If we have,
            //  please dump our memory image out to disk and empty out our processing.

            ///// TO DO /////
        }
    }

    // override standard repository method and derive date from the filesystem
	public String GetCurrentDate(HashTree htFolder) throws Exception {
		String sPath = (String) htFolder.get("PATH");

        try {
            File f = new File("/temp/.file");
            FileOutputStream out = new FileOutputStream(f);
            out.write(
                    0); out.close();

            // Convert to: 'MM/DD/YYYY HH24:MI:SS'
            java.util.Date d = new java.util.Date(f.lastModified());
            SimpleDateFormat gmtf = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");
            //gmtf.setTimeZone( TimeZone.getTimeZone( "UTC" ) );

            System.out.println("Current date: "+gmtf.format(d));
            f.delete();

            return gmtf.format(d);

        } catch (Exception e) {
            System.out.println("Warning! Could not write a file to the target filesystem.");
            e.printStackTrace(System.out);

            return GetCurrentDate();
        }
    }

	// Lookup this document to find all associated corpora.   Then put the document
	//  path into the vector for each corpus, plus the overall vector.   These vectors
	//  will later be used to reindex documents in the full text verity engine.
	public void AddDocumentIndex(String sPath, com.iw.fcrawl.Machine M) {

		/* This will be re-done differently, soon.
		HashTree htArguments = GetArguments();

		htArguments.put("Path", sPath);
		InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentCorpora", htArguments);
		HashTree htResults = API.Execute();

		if (!htResults.containsKey("DOCUMENT")) {
			//System.out.println("The document "+sPath+" did not classify into any topics.");
			return;
		} */
		return;
	}

	// constructor: just use the standard constructor
	public Verity (HashTree ht, HashTree ht2) {
		SetArguments(ht2);
	}

    // ******** Bring results back as DOM tree instead of hash tree or vector
    public org.dom4j.Document dExecute(String URL) { return dExecute (URL, false, false); }
    public org.dom4j.Document dExecute (String URL, boolean bDebug, boolean bUseProxy) {
        try {
            long lStart = System.currentTimeMillis();
            URL myURL = new URL(URL);
            HttpURLConnection httpCon = getHttpURLConnection (myURL, bUseProxy, bDebug);
			InputStream is = httpCon.getInputStream();

            SAXReader xmlReader = new SAXReader(false);

            if (bDebug) {
                ErrorHandler eh = new ErrorHandler() {
                    public void fatalError (SAXParseException s) { System.out.println("fatal error."); }
                    public void error (SAXParseException s) { System.out.println("error."); }
                    public void warning (SAXParseException s) { System.out.println("warning."); }
                };
                xmlReader.setErrorHandler(eh);
            }
            InputSource ins = new InputSource(is);
            ins.setEncoding("UTF-8");

            org.dom4j.Document doc = xmlReader.read(ins);

            httpCon.disconnect();
            long lStop = System.currentTimeMillis() - lStart;
            //System.out.println("<!-- Timing: " + lStop + " ms -->");

            return doc;
        } catch (Exception except) {
            except.printStackTrace();
        }
        return null;
    }

    private HttpURLConnection getHttpURLConnection (URL myURL, boolean bUseProxy, boolean bDebug)
        throws Exception
    {
        HttpURLConnection httpCon = null;
        httpCon = (HttpURLConnection) myURL.openConnection();

        if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
            System.out.println("Http error: " + httpCon.getResponseMessage());
            throw new Exception("Http error: " + httpCon.getResponseMessage());
        }
        return httpCon;
    }

    public org.dom4j.Document InvokeDomAPI(String URL) throws Exception {
        return InvokeDomAPI(URL, false); }
    public org.dom4j.Document InvokeDomAPI(String URL, boolean bDebug) throws Exception {
        org.dom4j.Document d = null;
        try { d = dExecute(URL, bDebug, false); }
        catch (Exception e) { throw e; }

        return d;
    }

}
