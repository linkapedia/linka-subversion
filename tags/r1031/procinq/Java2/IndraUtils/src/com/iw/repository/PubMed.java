package com.iw.repository;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

import com.iw.system.*;
import com.iw.tools.*;

// PubMed is an extension of the WebCache repository.  It does not extract hyperlinks from
//  crawled documents as the WebCache and WebCrawl repositories do.  Instead, documents are
//  extracted serially by document identifier, starting with the most recent and working
//  backwards.   Two variables govern these start and stop points, iDocumentID and iStopID.
public class PubMed extends WebCache {

    public int iDocumentID = 22552098;
    public int iStopID = 22502099;

    // constructor: WebCache specific constructor
	public PubMed (HashTree ht, HashTree ht2) {
        // set attributes
        SetName((String) ht.get("REPOSITORYNAME"));
        SetLocation((String) ht.get("REPOSITORYLOCATION"));

        File fProxyConfigurationFile = null;
        if (ht2.containsKey("proxyconfig")) { fProxyConfigurationFile = new File((String) ht2.get("proxyconfig")); }
		if ((fProxyConfigurationFile != null) && (fProxyConfigurationFile.exists())) {
            System.out.println("Loading proxy configuration file..");
            try { ReadProxyFile(fProxyConfigurationFile); } catch (Exception e) {}
		}

        if (vProxies.size() > 0) { bUseProxies = true; }
        if (ht2.containsKey("interval")) { lInterval = 1000 * new Integer((String) ht2.get("interval")).longValue(); }
        if (ht2.containsKey("documentid")) { iDocumentID = new Integer((String) ht2.get("documentid")).intValue(); }
        if (ht2.containsKey("stopid")) { iDocumentID = new Integer((String) ht2.get("stopid")).intValue(); }

        int iHour = 24*365;
        if (!ht2.containsKey("endtimes")) { System.out.println("No ending time specified.  Assuming infinite run."); }
        else { iHour = new Integer((String) ht2.get("endtimes")).intValue(); }

        // set stop time, if applicable
        java.util.Date d = c.getTime();
        c.add(Calendar.HOUR, iHour);

		SetArguments(ht2);

        CacheFile = GetLocation()+"/crawlcachedata.txt"; System.out.println("Cache file: "+CacheFile);
        QueueFile = GetLocation()+"/queuedata.txt"; System.out.println("Queue file: "+QueueFile);
	}

    // Specific web crawl routine to Pub Med abstracts!  Fill the crawl counter serially in descending order.
	public void GetNewObjects(String sURL, String sDateLastCapture, boolean bRecurse) {
		System.out.println("Looking for Pub Med classification candidates in "+sURL);

        while (iDocumentID > iStopID) {
            iCrawlCounter++;

            String sURLelement = "http://www.ncbi.nlm.nih.gov/entrez/queryd.fcgi?cmd=Retrieve&db=PubMed&list_uids="+iDocumentID+"&dopt=Abstract";
            String sData = getData(sURLelement);

            iDocumentID = iDocumentID - 1;
        }

		System.out.println(iCrawlCounter+" new web pages found in Pub Med.");
		System.out.println("");
		System.out.flush();
	}
}