/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: May 13, 2004
 * Time: 12:09:59 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.system;

import com.indraweb.utils.oracle.CustomFields;

import java.util.Hashtable;
import java.sql.Connection;

public class FieldData {
    public static Hashtable getFieldData (String objectName, Connection dbc) {
        try { return CustomFields.getFields(objectName, dbc); }
        catch (Exception e) { e.printStackTrace(System.out); return new Hashtable(); }
    }
}
