/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Dec 5, 2002
 * Time: 4:12:15 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.utils.strings;

import java.util.Hashtable;
import java.util.Enumeration;
import com.indraweb.utils.sets.UtilSets;



public class UtilStem
{

	// ********************************************
	public static StringAndCount[] getStemmed_SCArr ( StringAndCount[] scArrNodeSigWordsSortedDesc, boolean bSorted)
	// ********************************************
		throws Exception
	{
		Hashtable htStemmed = getStemmed_ht ( scArrNodeSigWordsSortedDesc );
		StringAndCount[] scArrStemAgg = new com.indraweb.utils.strings.StringAndCount [ htStemmed.size() ];

		int iLoop = 0;
		Enumeration e = htStemmed.keys();
		while ( e.hasMoreElements() )
		{
			String sWordStemmed = (String) e.nextElement();
			int iCount_stemmedAgg = ((Integer) htStemmed.get (sWordStemmed)).intValue();
			scArrStemAgg[iLoop] = new StringAndCount(sWordStemmed, iCount_stemmedAgg);
			iLoop++;
		}

		if ( bSorted )
			api.Log.LogFatal("bSorted not implemented in getStemmed_SCArr");

		return scArrStemAgg;
	}

	// ********************************************
	public static Hashtable getStemmed_ht ( StringAndCount[] scArrNodeSigWordsSortedDesc )
	// ********************************************
	{
		long ltimeStart = System.currentTimeMillis();
		Hashtable htStemmed = new Hashtable();

		Porter porter = Porter.getInstance();
		for ( int i = 0; i < scArrNodeSigWordsSortedDesc.length; i++ )
		{
			String	sWord = scArrNodeSigWordsSortedDesc[i].word;
			int		iCount = scArrNodeSigWordsSortedDesc[i].count;
			if ( !sWord.trim().equals(""))
			{
				String sWordStem = porter.stem ( sWord );
				//System.out.println ( sWord+":"+sWordStem+":"+iCount );
				UtilSets.hash_increment_count_for_string ( htStemmed,  sWordStem, iCount );
			}
		}
		//System.out.println("time to stem #words [" + scArrNodeSigWordsSortedDesc.length + "] ms [" +
		//				   UtilProfiling.elapsedTimeMillis(ltimeStart) + "]");
		return htStemmed;
	}

	// ********************************************
	public static Hashtable getStemmed_ht ( Hashtable htWordAndCounts )
	// ********************************************
	{
		// stem each word in a htWC - accrue counters per word on collision
		Hashtable htStemmed = new Hashtable();
		Enumeration e = htWordAndCounts.keys();
		Porter porter = Porter.getInstance();
		while ( e.hasMoreElements() )
		{
			String sWord = (String) e.nextElement();
			int iCount = ((Integer) htWordAndCounts.get (sWord)).intValue();
			sWord = porter.stem (sWord);
			UtilSets.hash_increment_count_for_string ( htStemmed,  sWord, iCount );
		}

		return htStemmed;
	}



}
