/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 12, 2003
 * Time: 12:15:30 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.utils.sets;

import java.util.Hashtable;
import java.util.HashSet;

public class HashedSets {

    private Hashtable htOKeyToHashSet = new Hashtable();
    public void addToSet ( Object oSetKey, Object oNewSetMember )
    {
        HashSet hs = (HashSet) htOKeyToHashSet.get ( oSetKey );
        if ( hs == null )
        {
            synchronized ( htOKeyToHashSet )
            {
                hs = (HashSet) htOKeyToHashSet.get ( oSetKey );  // in case someone else got in here first
                if ( hs == null ) // need to check additional times
                {
                    hs = new HashSet();
                    htOKeyToHashSet.put ( oSetKey, hs );
                }
            }
        }
        hs.add ( oNewSetMember );
    }

    public HashSet getSet ( Object oSetKey )
    {
        return (HashSet) htOKeyToHashSet.get ( oSetKey );
    }
}
