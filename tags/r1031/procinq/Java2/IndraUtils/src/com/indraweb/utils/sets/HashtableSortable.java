/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Oct 29, 2003
 * Time: 6:00:48 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.indraweb.utils.sets;

import java.util.*;

public class HashtableSortable extends Hashtable
{

    private Hashtable htInverted = new Hashtable ();
    private Hashtable htObjectValuesToHashSetKeysHavingThem = new Hashtable ();

    public HashtableSortable ()
    {
        super ();
    }

    public Object enumSortedKeys ()
    {
        Object[] oArrKeys = getArrayOfSortedKeys ();
        Enumeration enumSortedKeys = this.keys ();
        int iLoop = 0;
        Vector vSortedKeys = new Vector ();
        for ( int i = 0 ; i < oArrKeys.length ; i++ )
        {
            vSortedKeys.addElement (oArrKeys[i]);
        }
        return vSortedKeys.elements ();
    }

    public Object[] getArrayOfSortedKeys ()
    {
        Object[] oArrKeys = new Object[this.size ()];
        Enumeration enumKeys = this.keys ();
        int iLoop = 0;
        while (enumKeys.hasMoreElements ())
        {
            oArrKeys[iLoop] = enumKeys.nextElement ();
            iLoop++;
        }
        Arrays.sort (oArrKeys);
        return oArrKeys;

    }

    public Enumeration enumValuesSortedUnique ()
    {
        Object[] oArrValuesSortedUnique = getArrayOfValuesSortedUnique ();
        Enumeration enumValuesSortedUnique = this.keys ();
        int iLoop = 0;
        Vector voArrValuesSortedUnique = new Vector ();
        for ( int i = 0 ; i < oArrValuesSortedUnique.length ; i++ )
        {
            voArrValuesSortedUnique.addElement (oArrValuesSortedUnique[i]);
        }
        return voArrValuesSortedUnique.elements ();

    }

    public Object[] getArrayOfValuesSortedUnique ()
    {
        Hashtable htSeen = new Hashtable ();
        Vector vUniqueValues = new Vector ();

        Enumeration enumKeys = this.keys ();
        int iLoop = 0;
        while (enumKeys.hasMoreElements ())
        {
            vUniqueValues.add (enumKeys.nextElement ());
        }

        Object[] oArrSortedUniqueValues = com.indraweb.util.UtilSets.convertVectorTo_ObjArray (vUniqueValues);
        return oArrSortedUniqueValues;

    }

    public void fillInvertedIndex () // probably not thread-safe here
    {
        HashSet hsKeysThisValue = new HashSet ();


        Enumeration enumValues = enumValuesSortedUnique ();
        while (enumValues.hasMoreElements ())
        {
            // performance optimization : maybe keep an array of all keys and values so indexing can be by array.
            // based on assumption that arrays are faster than hashtables/vectors


            Object oValueIndexing = enumValues.nextElement ();
            Enumeration enumKeys = enumValuesSortedUnique ();
            while (enumKeys.hasMoreElements ())
            {
                Object oKey = enumKeys.nextElement ();
                Object oValue = enumKeys.nextElement ();
                if ( oValue.equals( oValueIndexing ) )
                {
                    addToOrCreateHashsetInHashTable ( htObjectValuesToHashSetKeysHavingThem, oValue, oKey);
                }


            }
        }




    }

    public Iterator enumerateSortedKeysForASingleValue (Object oValue, boolean bSort)
    {
        HashSet hs = (HashSet) htObjectValuesToHashSetKeysHavingThem.get ( oValue );
        Object[] oArrKeysThisValue = hs.toArray();
        ArrayList arrList = new ArrayList(hs);
        Collections.sort ( arrList );
        return arrList.iterator();

    }
    public static void addToOrCreateHashsetInHashTable (
            Hashtable ht, Object oKey, Object oValue
    )
    {
        HashSet hs = (HashSet) ht.get ( oKey );
        if ( hs == null )
        {
            hs = new HashSet ();
        }
        hs.add ( oKey );
    }


    public Iterator enumerateSortedKeysAcrossSortedValues (Object oValue, boolean bSort)
    {
        return null;
    }

}
