
import java.util.*;
import java.io.*;
import java.sql.*;
import java.lang.reflect.*;

import javax.servlet.*;
import javax.servlet.http.*;

import api.Log;
import api.TSException;
import api.TSDBException;
import api.TSNoSuchMethodException;
import api.TSNoSuchObjectException;
import api.util.MultiPartMisc;
import api.util.VeritableOutputStream;

import com.oreilly.servlet.multipart.*;
import com.indraweb.util.UtilFile;

import api.SerialProps;

import com.indraweb.execution.Session;

import com.indraweb.ir.clsStemAndStopList;
import com.iw.license.IndraLicense;

/**
 *	Main servlet class for IndraWeb Taxonomy Server API
 *	All rights reserved.
 *	(c)IndraWeb.com,Inc. 2001-2002
 *
 *	@authors hkon, mpuscar
 *
 *	@param	req	Contains the name-value pairs:
 *						fn=functionname
 *						parm1=parameterValue1
 *						parm2=parameterValue2 ...
 *
 *	@param	res	response is written as XML to the output stream of res

 *	@return	xml via output stream
 */

public class ts extends HttpServlet {
    // This is a "convenience method" so, after overriding it there is no need to call the super()
    // This function takes the place of the initializeSession method in the engine project.

    public void init()
            throws ServletException {
        // If the session is already initialized, return.

        if (com.indraweb.execution.Session.GetbInitedSession()) return;

        // Load environment from JRUN config application variables first
        Enumeration eParams = getServletContext().getInitParameterNames();
        Hashtable htprops = new Hashtable();

        while (eParams.hasMoreElements()) {
            String sParamName = (String) eParams.nextElement();
            String sParamValu = (String) getServletContext().getInitParameter(sParamName);

            htprops.put(sParamName, sParamValu);
/*
            UtilFile.addLineToFile("c:/t2.t", new java.util.Date() +
                 " : in ts init() 0.4 ; paramname [" + sParamName + "] " +
                 " ; paramval [" + sParamValu + "]\r\n");
*/
        }
        Session.sIndraHome = (String) getServletContext().getInitParameter("IndraHome");
        System.out.println("ts init set Session.sIndraHome to [" + Session.sIndraHome + "]" );
        Session.cfg = new com.indraweb.execution.ConfigProperties(htprops);

        // added by MAP 6/24/04, create license object and store in session
        String licenseKey = Session.cfg.getProp("licenseKey", false, "0");
        System.out.println("license key ("+licenseKey+") initializing...");

        if (licenseKey.equals("0")) {
            System.out.println("Warning! No license key was specified, starting server in evaluation mode.");
        } else {
            try { licenseKey = com.indraweb.utils.license.LicenseUtils.licenseToBitmap(licenseKey); }
            catch (Exception e) {
                System.out.println("Fatal error! Invalid license key specified.");
                e.printStackTrace(System.out);
                throw new ServletException("Fatal error! Invalid license key specified: "+licenseKey);
            }
        }

        Session.license = new IndraLicense(licenseKey);

        Session.SetbInitedSession(true);

        api.Log.Log("TS SERVER INITED");

        return;
    }


    public void doSerialize(HttpServletRequest req,
                            HttpServletResponse res)
            throws ServletException, IOException {
        api.APIProps apiprops = new api.APIProps();
        // UtilFile.addLineToFile("/temp/temp.txt", "enter ts doSerialize outer  [" + icallctr_ts + "]\r\n");
        //res.setContentType("text/xml; charset=UTF-8");
        PrintWriter out = getPrintWriter(res);
        SerialProps sDataP;

        try {
            ObjectInputStream objin = new ObjectInputStream(req.getInputStream());
            Object o = objin.readObject();
            sDataP = (SerialProps) o;
            File f = sDataP.GetFile();
            if (f.exists()) {
                f.delete();
            }

            String sFileName = Session.cfg.getProp("ImportDir") + "/" + f.getName();
            FileOutputStream fos = new FileOutputStream(sFileName);
            fos.write(sDataP.bArr);
            fos.flush();
            fos.close();

            sDataP.Props.put("URL", "file:///" + sFileName);

            doGet(req, res, sDataP.Props);

        } catch (Exception e) {
            TSException tse = new TSException(api.emitxml.EmitGenXML_ErrorInfo.ERR_TS_ERROR, e.getMessage());
            api.emitxml.EmitGenXML_ErrorInfo.emitException("", tse, out);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        icallctr_ts++;
        //UtilFile.addLineToFile("/temp/temp.txt", "enter ts doPost outer  [" + icallctr_ts + "]\r\n");
        if (request.getContentType().equals("application/x-java-serialized-object")) {
            doSerialize(request, response);
            return;
        }
        //UtilFile.addLineToFile("/temp/temp.txt", "enter ts doPost inner 1 [" + icallctr_ts + "]");
        if (!request.getContentType().substring(0, 19).equals("multipart/form-data")) {
            doGet(request, response);
        } else {
            //UtilFile.addLineToFile("/temp/temp.txt", "in ts doPost multipart [" + icallctr_ts + "]\r\n");
            MultipartParser mpr = new MultipartParser(request, 1000000000);
            //response.setContentType("text/xml; charset=UTF-8");
            Part p = null;
            String sFilename = null;
            api.APIProps apiprops = new api.APIProps();

            try {
                // get indrahome from jrun config
                String sIndraHome = Session.sIndraHome;
                if (sIndraHome == null)
                    throw new Exception("IndraHomeFolder not specified in jrun config");

                // loop through each part that was sent
                while ((p = mpr.readNextPart()) != null) {
                    String sParmName = p.getName();
                    String sParmValue = new String("");
                    if (p.isParam()) {
                        ParamPart pp = (ParamPart) p;
                        sParmValue = pp.getStringValue();
                    }
                    if (p.isFile()) {
                        FilePart fp = (FilePart) p;

                        // Write content type and file name into props
                        apiprops.put("Content-type", fp.getContentType());
                        sParmValue = fp.getFileName();

                        if (fp.getFileName() != null) {
                            // 10/17/2004 - do not write input stream to a file, keep it in memory instead
                            InputStream is = fp.getInputStream();
                            BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                            String sData = new String(); StringBuffer sb = new StringBuffer();

                            while ((sData = buf.readLine()) != null) { sb.append(sData); }

                            is.close();

                            System.out.println("SB length before: "+sb.length());
                            sb = removeIllegalCharacters(sb);
                            System.out.println("SB length after: "+sb.length());

                            if (fp.getFileName().endsWith(".txt")) {
                                apiprops.put("inputstream", "<HTML>\n<HEAD><TITLE>"+fp.getFileName()+"</TITLE>\n</HEAD>"+
                                        "\n<BODY>\n"+sb.toString()+"\n</BODY>\n</HTML>");
                            } else { apiprops.put("inputstream", sb.toString()); }
                            apiprops.put("fileposted", "true");
                        }
                        apiprops.put("URL", fp.getFilePath());
                    }

                    if (sParmValue != null) {
                        apiprops.put(sParmName, sParmValue);
                    }
                }
                doGet(request, response, apiprops);
            }
                    // If an error occurs attempting to invoke the class, or it does not exist, print the main page
            catch (Exception e) {
                api.Log.LogFatal("Failure in receiving MIME attached HTTP call.", e);
                Log.LogError("caught within ts.java.doPost(), re-throwing", e);
                api.emitxml.EmitGenXML_ErrorInfo.emitException("caught within ts.java.doPost(), re-throwing", e, System.err);
                throw new ServletException("General exception in ts.java Post see api.log ", e);
            } finally { // cleanup extraneous files
                if (sFilename != null) {
                    File f = new File(sFilename);
                    if (f.exists() && !com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebugLeaveClassifyTempFiles.txt") ) {
                        f.delete();
                    } else {
                        api.Log.Log ("file not existing to delete or IndraDebugLeaveClassifyTempFiles.txt switch set not to [" + sFilename + "]" );
                    }
                }
            }
        }
        //UtilFile.addLineToFile("/temp/temp.txt", "exiting ts doPost outer  b[" + icallctr_ts + "]\r\n");

    }

    // *******************************************
    public void doGet(HttpServletRequest req,
                      HttpServletResponse res)
            throws ServletException, IOException {
        icallctr_ts++;
        //UtilFile.addLineToFile("/temp/temp.txt", "enter ts doget outer  [" + icallctr_ts + "]\r\n");
        Properties props = new Properties();
        api.APIProps apiprops = new api.APIProps(props);
        //UtilFile.addLineToFile("/temp/temp.txt", "enter ts doget inner [" + icallctr_ts + "] fn [" + apiprops.get("fn") + "]\r\n");
        doGet(req, res, apiprops, false);
    }  // doGet
    public void doGet(HttpServletRequest req,
                      HttpServletResponse res,
                      api.APIProps apiprops)
            throws ServletException, IOException {

        doGet(req, res, apiprops, false);
    }  // doGet
    private static int icallctr_ts   = 0;
    public void doGet(HttpServletRequest req,
                      HttpServletResponse res,
                      api.APIProps apiprops,
                      boolean testcase)
            throws ServletException, IOException {

        Connection dbc = null;
        String sWhichDB = "API";
        PrintWriter out = null;
        try {
            //UtilFile.addLineToFile("/temp/temp.txt", "enter ts doget inner [" + icallctr_ts + "]\r\n");
            //res.setContentType("text/xml; charset=UTF-8");

            res.setContentType("text/xml; charset=UTF-8");
            out = res.getWriter();

            Enumeration e4 = req.getParameterNames();
            while (e4.hasMoreElements()) {
                String sParmName = (String) e4.nextElement();
                String sISO = req.getParameter(sParmName);
                byte bytes[] = new byte[sISO.length()];
                sISO.getBytes(0, sISO.length(), bytes, 0);
                String sParmValue = (new String(bytes,"UTF-8"));
                apiprops.put(sParmName, sParmValue);
            }

            //UtilFile.addLineToFile("/temp/temp.txt", "enter ts doget inner [" + icallctr_ts + "] fn [" + apiprops.get("fn") + "]\r\n");
            try {
                //System.out.println("ts.java has sWhichDB [" + sWhichDB + "] pre DB APIDBInterface.getConnection.");
                sWhichDB = (String) apiprops.get("whichdb", "API");
            } catch (Exception e) {
                System.out.println("in exception [" + Log.stackTraceToString ( e ) + "]" );
                System.out.println("USING API AS ERROR DEFAULT" );
                sWhichDB = "API";
            }

            // don't init a dbc for tsmetasearch - dont want to waste them
            String sFn = (String) apiprops.get("fn", true);
            //System.out.println("sFn = [" + sFn + "]" );
            //System.out.println("ts getting DBC for sWhichDB  [" + sWhichDB + "]");
            dbc = api.util.APIDBInterface.getConnection(sWhichDB);
            if (dbc == null) {
                throw new Exception("Database connection is NULL DB [" + sWhichDB + "]");
            }

            // instantiate security
            api.security.DocumentSecurity.getSecurityHash(dbc);

            Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));

            // if a proxy has been specified in the configuration, it is set here
            if (Session.cfg.getProp("proxyHost") != null) {
                String sHost = Session.cfg.getProp("proxyHost");
                if (!sHost.toLowerCase().equals("none")) {
                    SetProxyProperties(Session.cfg.getProp("proxyHost"), Session.cfg.getProp("proxyPort"));
                    //api.Log.Log("### Note: api will be using a proxy [" + Session.cfg.getProp("proxyHost") + "]");
                } //else
                  //  api.Log.Log("### Note: api will not be using a proxy.");
            } //else
              //  api.Log.Log("### Note: api will not be using a proxy.");

            api.APIHandler.doGetMine(req, res, apiprops, out, dbc, true, testcase);
        } catch (Exception e) {
            Log.LogError("caught within ts.java.doGet(), re-throwing", e);
            api.emitxml.EmitGenXML_ErrorInfo.emitException("caught within ts.java.doGet(), re-throwing", e, out);
            throw new ServletException("General exception in ts.java Get see api.log ", e);
        } finally {
            try {
                dbc = api.util.APIDBInterface.freeConnection(dbc);
            } catch (Exception e) {
                api.Log.LogError("error freeing dbc", e);
            }
            //UtilFile.addLineToFile("/temp/temp.txt", "exiting ts doget [" + icallctr_ts + "]\r\n");

        }
        //try { dbc.close(); } catch (Exception e) { api.Log.LogError(e); }
    } // public void doGet(HttpServletRequ	est req,

    public void destroy() {
        try {
            Iterator It = api.util.APIDBInterface.getDBsUsed();
            while (It.hasNext()) {
                String sDB = (String) It.next();
                api.Log.Log("SERVER SHUTDOWN - DBdestroy [" + sDB + "]");
                api.statics.DBConnectionJX.destroy(sDB);
            }
        } catch (Exception e) {
            api.Log.LogError("error on destroy", e);
        }

    }

    public StringBuffer removeIllegalCharacters(String s) {
        StringBuffer sb = new StringBuffer(s);
        return removeIllegalCharacters(sb);
    }

    public StringBuffer removeIllegalCharacters(StringBuffer sb) {
        for (int i = 0; i < sb.length(); i++) {
            if ((sb.charAt(i) < 32) && (sb.charAt(i) != 13)) {
                sb.deleteCharAt(i);
                --i; // We just shortened this buffer.
            }
        }

        return sb;
    }

    public void SetProxyProperties(String proxyHost, String proxyPort) {
        //api.Log.Log("### Note: api will be using a proxy: "+proxyHost+":"+proxyPort);

        System.getProperties().put("proxySet", "true");
        System.getProperties().put("proxyHost", proxyHost);
        System.getProperties().put("proxyPort", proxyPort);
    }

    public PrintWriter getPrintWriter(HttpServletResponse res) throws IOException {
        return new PrintWriter ( new OutputStreamWriter (new VeritableOutputStream(res.getOutputStream()), "UTF-8"));
    }

} // public class ts extends HttpServlet


