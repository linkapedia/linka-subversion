package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author andres
 */
public class ListMusthaveGateReport {

    private Integer valueUpdated;
    private List<ModelMusthaveGateReport> list;

    @XmlElement(type = Integer.class)
    public Integer getValueUpdated() {
        return valueUpdated;
    }

    public void setValueUpdated(Integer valueUpdated) {
        this.valueUpdated = valueUpdated;
    }

    @XmlElement(type = ModelMusthaveGateReport.class)
    public List<ModelMusthaveGateReport> getList() {
        return list;
    }

    public void setList(List<ModelMusthaveGateReport> list) {
        this.list = list;
    }
}
