package com.iw.system;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author andres
 */
public class ModelNodeReport {

    private String nodeId;
    private String nodeTitle;
    private String nodeMusthaves;
    private String nodeSignatures;
    private String nodeImages;
    private String nodeLinks;

    @XmlElement(type=String.class)
    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @XmlElement(type=String.class)
    public String getNodeImages() {
        return nodeImages;
    }

    public void setNodeImages(String nodeImages) {
        this.nodeImages = nodeImages;
    }

    @XmlElement(type=String.class)
    public String getNodeLinks() {
        return nodeLinks;
    }

    public void setNodeLinks(String nodeLinks) {
        this.nodeLinks = nodeLinks;
    }

    @XmlElement(type=String.class)
    public String getNodeMusthaves() {
        return nodeMusthaves;
    }

    public void setNodeMusthaves(String nodeMusthaves) {
        this.nodeMusthaves = nodeMusthaves;
    }

    @XmlElement(type=String.class)
    public String getNodeSignatures() {
        return nodeSignatures;
    }

    public void setNodeSignatures(String nodeSignatures) {
        this.nodeSignatures = nodeSignatures;
    }

    @XmlElement(type=String.class)
    public String getNodeTitle() {
        return nodeTitle;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }
}
