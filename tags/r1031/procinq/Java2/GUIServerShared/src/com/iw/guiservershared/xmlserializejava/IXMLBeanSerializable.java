package com.iw.guiservershared.xmlserializejava;

import java.io.InputStream;

public interface IXMLBeanSerializable
{
    public Object getSerializableForm();
    public void setFromSerializableForm(Object oSerializableForm) throws Exception;
    public String getXMLVisualDebug ( int iDepthRecursionForIndent ) throws Exception;
    public boolean bValidateDeserializableForm (Object o) throws Exception;


}
