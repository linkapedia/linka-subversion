
package com.iw.guiservershared.externalexplain;

import com.iw.guiservershared.xmlserializejava.IXMLBeanSerializable;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import java.util.Enumeration;

/**
 * Container class for data intended for table display
 *
 */

public class ExplainDisplayDataInstanceTABLE  extends ExplainDisplayDataInstance implements IXMLBeanSerializable
{
    String[] sArrColNames = null;
    Object[][] oArrArrData = null;
    Vector vOArrDataRows = null;
    Vector vColoringRule = null;


    public static final Integer ICOMPARE_EQUALS = new Integer (1);
    public static final Integer ICOMPARE_GT = new Integer (2);
    public static final Integer ICOMPARE_GTE = new Integer (3);
    public static final Integer ICOMPARE_LT = new Integer (4);
    public static final Integer ICOMPARE_LTE = new Integer (5);

    public ExplainDisplayDataInstanceTABLE  ()
    {
    }

    public ExplainDisplayDataInstanceTABLE (
                                            String sCalloutName,
                                            String sTabName,
                                            String sRenderAs,
                                            String sDescription,
                                            String sTooltip,
                                            String[] sArrColNames,
                                            Object[][] oArrArrData,
                                            Vector vColoringRule )
    {
        this.sClassPath = this.getClass().getName();

        this.sCalloutName = sCalloutName;
        this.sTabName = sTabName;
        this.sRenderAs = sRenderAs;
        this.sDescription = sDescription ;
        this.sTooltip = sTooltip;
        this.sArrColNames = sArrColNames;
        this.oArrArrData = oArrArrData;
        this.vColoringRule = vColoringRule;
        //System.out.println("1 vColoringRule == null [" + (vColoringRule == null) + "]");
    }

    public ExplainDisplayDataInstanceTABLE (
                                            String sCalloutName,
                                            String sTabName,
                                            String sRenderAs,
                                            String sDescription,
                                            String sTooltip,
                                            String[] sArrColNames,
                                            Vector vOArrDataRows,
                                            Vector vColoringRule)
    {
        this.sClassPath = this.getClass().getName();

        this.sCalloutName = sCalloutName;
        this.sTabName = sTabName;
        this.sRenderAs = sRenderAs;
        this.sDescription = sDescription ;
        this.sTooltip = sTooltip;
        this.sArrColNames = sArrColNames;
        this.vOArrDataRows = vOArrDataRows;
        this.vColoringRule = vColoringRule;
        //System.out.println("2 vColoringRule == null [" + (vColoringRule == null) + "]");
    }

    public void addVectorRow (Object[] oArrRow)
    {
        vOArrDataRows.addElement(oArrRow);
    }

    public void addVectorRow (String sName, String sValue)
    {
        Object[] oArr = new Object[2];
        oArr[0] = sName;
        oArr[1] = sValue;
        vOArrDataRows.addElement(oArr);
    }
    public void addVectorRow (Object[] oArrRow, int iPositionForAddElement)
    {
        vOArrDataRows.add(iPositionForAddElement, oArrRow);
    }

    public void addVectorRow (String sName, String sValue, int iPositionForAddElement)
    {
        Object[] oArr = new Object[2];
        oArr[0] = sName;
        oArr[1] = sValue;
        vOArrDataRows.add(iPositionForAddElement, oArr);
    }

    public Object getSerializableForm()
    {

        Vector vTopLevelSerializableForm = new Vector();
        vTopLevelSerializableForm.addElement(this.getClass().getName());
        vTopLevelSerializableForm.addElement(sCalloutName);
        vTopLevelSerializableForm.addElement(sTabName);
        vTopLevelSerializableForm.addElement(sRenderAs);
        vTopLevelSerializableForm.addElement(sDescription);
        vTopLevelSerializableForm.addElement(sTooltip);
        vTopLevelSerializableForm.addElement(sArrColNames);
        // if have been working off of vector row adds ...
        if ( oArrArrData == null )
        {
            oArrArrData = buildoArrArrFromVector ( vOArrDataRows,sArrColNames.length );
        }
        vTopLevelSerializableForm.addElement(oArrArrData);
        vTopLevelSerializableForm.addElement(vColoringRule);
        return vTopLevelSerializableForm;

    }

    // for construction on deserialized side
    // object will be a vector
    public void setFromSerializableForm (Object oThisInSerializableForm ) throws Exception
    {

        Vector vThisInSerializableForm = (Vector) oThisInSerializableForm;
        // zeroth element contains the classpath
        int iVar = 0;
        this.sClassPath = (String) vThisInSerializableForm.elementAt(iVar++);
        if (!this.sClassPath.equals(this.getClass().getName()))
        {
            throw new Exception ("Class mismatch in deserialize [" + this.sClassPath + "] " +
            " vs. [" + this.getClass().getName() + "]");

        }
        this.sCalloutName = (String) vThisInSerializableForm.elementAt(iVar++);
        this.sTabName = (String) vThisInSerializableForm.elementAt(iVar++);
        this.sRenderAs = (String) vThisInSerializableForm.elementAt(iVar++);
        this.sDescription = (String) vThisInSerializableForm.elementAt(iVar++);
        this.sTooltip = (String) vThisInSerializableForm.elementAt(iVar++);
        this.sArrColNames = (String[]) vThisInSerializableForm.elementAt(iVar++);
        this.oArrArrData = (Object[][]) vThisInSerializableForm.elementAt(iVar++);
        Object o = vThisInSerializableForm.elementAt(iVar++);
        this.vColoringRule = (Vector) o;
    }

    public DefaultTableModel getTableModel()
    {
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.setDataVector(oArrArrData, sArrColNames);
        return dtm;
    }
    public String getXMLVisualDebug ( int iDepthRecursionForIndent )
    {
        StringBuffer sb = new StringBuffer();

        String sIndent = new String(new byte[iDepthRecursionForIndent]);
        sb.append(sIndent +"<OBJECT>\r\n");
        sb.append(sIndent+" <CLASSNAME>" + this.getClass().getName() + "</CLASSNAME>\r\n");
        sb.append(sIndent+" <CLASSPATH>" + sClassPath + "</CLASSPATH>\r\n");
        sb.append(sIndent+" <COLCOUNT>" + sArrColNames.length + "</COLCOUNT>\r\n");
        sb.append(sIndent+" <DESCRIPTION>" + sDescription+ "</DESCRIPTION>\r\n");
        sb.append(sIndent+" <RENDERAS>" + sRenderAs+ "</RENDERAS>\r\n");
        sb.append(sIndent+" <DATAROWS>" + (this.getoArrArrData()[0]).length + "</RENDERAS>\r\n");
        sb.append(sIndent+"</OBJECT>\r\n");
        //String[] sArrColNames = null;
        //Object[][] oArrArrData = null;

        return sb.toString();

    }
    public boolean bValidateDeserializableForm (Object o) throws Exception
    {return true;}

    public String[] getsArrColNames()
    {
        return sArrColNames;
    }
    public Object[][] getoArrArrData()
    {
        return oArrArrData;
    }
    public void setoArrArrData(Object[][] oArrArrData)
    {
        this.oArrArrData = oArrArrData;
    }

    public Vector getRowColoringRule ()
    {
        return vColoringRule; // Integer IcolID, Integer ICcomparator Constant, Object oValueToCompareTo
    }

}
