package com.iw.license;

import java.math.BigInteger;

public class IndraLicense {
    private String bitmask = "0";

    // constructors
    public IndraLicense(String bitmask) {
        this.bitmask = bitmask;
        padMask();
    }
    public IndraLicense(BigInteger bi) {
        this.bitmask = bi.toString(2);
        padMask();
    }

    // accessor method, used by TSLOGIN
    public String getBitmask() { return bitmask; }

    // authorization function
    public boolean isAuthorized(int bit) {
        if (bitmask.length() < bit) return false;
        if (bitmask.toCharArray()[bit] == '0') return false;

        return true;
    }

    public void padMask() {
        int len = bitmask.length();
        if (len < 6) for (int i = 0; i < (6-len); i++) bitmask = "0"+bitmask; }

    // list constants here, each pertaining to a given piece of functionality within the system
    public static final int EXPORT = 0;
    public static final int INGESTION = 1;
    public static final int PEOPLE_SEARCH = 2;
    public static final int THESAURUS = 3;
    public static final int NARRATIVE = 4;
    public static final int IDRAC = 5;
}
