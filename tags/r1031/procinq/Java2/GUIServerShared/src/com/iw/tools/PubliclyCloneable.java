/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Nov 20, 2003
 * Time: 4:02:45 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.tools;

public interface PubliclyCloneable extends Cloneable {
    public Object clone();
}
