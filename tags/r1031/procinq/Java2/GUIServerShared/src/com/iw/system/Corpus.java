package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 24, 2003
 * Time: 1:35:47 PM
 */

/*
<CORPUSID>10000002</CORPUSID>
<CORPUS_NAME>Biological agents</CORPUS_NAME>
<CORPUSDESC>null</CORPUSDESC>
<ROCF1>0.0</ROCF1>
<ROCF2>0.0</ROCF2>
<ROCF3>0.0</ROCF3>
<ROCC1>0.0</ROCC1>
<ROCC2>0.0</ROCC2>
<ROCC3>0.0</ROCC3>
<CORPUSACTIVE>1</CORPUSACTIVE>
 */
import java.io.Serializable;
import java.util.Vector;

public class Corpus implements Comparable, Serializable {
    // User attributes

    private String CorpusID;
    private String CorpusName;
    private String CorpusDesc;
    private String CorpusActive;
    private String corpusAffinity;
    private String corpusTerms;

    // constructor(s)
    public Corpus() {
    }

    public Corpus(HashTree ht) {
        CorpusID = (String) ht.get("CORPUSID");
        CorpusName = (String) ht.get("CORPUS_NAME");

        if (ht.containsKey("CORPUSDESC")) {
            CorpusDesc = (String) ht.get("CORPUSDESC");
        }
        if (ht.containsKey("CORPUSACTIVE")) {
            CorpusActive = (String) ht.get("CORPUSACTIVE");
        }
        if (ht.containsKey("CORPUSAFFINITY")) {
            this.corpusAffinity = ((String) ht.get("CORPUSAFFINITY"));
        }
        if (ht.containsKey("CORPUSTERMS")) {
            this.corpusTerms = ((String) ht.get("CORPUSTERMS"));
        }
    }

    public Corpus(Vector v) {
        for (int i = 0; i < v.size(); i++) {
            VectorValue vv = (VectorValue) v.elementAt(i);

            if (vv.getName().equals("CORPUSID")) {
                CorpusID = vv.getValue();
            }
            if (vv.getName().equals("CORPUS_NAME")) {
                CorpusName = vv.getValue();
            }
            if (vv.getName().equals("CORPUSDESC")) {
                CorpusDesc = vv.getValue();
            }
            if (vv.getName().equals("CORPUSACTIVE")) {
                CorpusActive = vv.getValue();
            }
            if (vv.getName().equals("CORPUSAFFINITY")) {
                this.corpusAffinity = vv.getValue();
            }
            if (!vv.getName().equals("CORPUSTERMS")) {
                continue;
            }
            this.corpusTerms = vv.getValue();
        }
    }

    // accessor functions
    public String getID() {
        return CorpusID;
    }

    public String getName() {
        return CorpusName;
    }

    public String getDescription() {
        return CorpusDesc;
    }

    public String getActive() {
        return CorpusActive;
    }

    public String getAffinity() {
        return this.corpusAffinity;
    }

    public String getTerms() {
        return this.corpusTerms;
    }

    public void setID(String ID) {
        CorpusID = ID;
    }

    public void setName(String Name) {
        CorpusName = Name;
    }

    public void setDescription(String Description) {
        CorpusDesc = Description;
    }

    public void setActive(String Active) {
        CorpusActive = Active;
    }

    public void setAffinity(String affinity) {
        this.corpusAffinity = affinity;
    }

    public void setTerms(String terms) {
        this.corpusTerms = terms;
    }

    // generic set: used by SAX parser
    public void set(String Key, String Value) {
        if (Key.equals("ID")) {
            setID(Value);
        } else if (Key.equals("BASENAMESTRING")) {
            setName(Value);
        } else if (Key.equals("CORPUSDESC")) {
            setDescription(Value);
        } else if (Key.equals("CORPUSACTIVE")) {
            setActive(Value);
        } else if (Key.equals("CORPUSAFFINITY")) {
            setAffinity(Value);
        } else if (Key.equals("CORPUSTERMS")) {
            setTerms(Value);
        }
    }

    @Override
    public String toString() {
        return CorpusName;
    }

    @Override
    public int compareTo(Object o) {
        return CorpusName.compareTo(o.toString());
    }
}
