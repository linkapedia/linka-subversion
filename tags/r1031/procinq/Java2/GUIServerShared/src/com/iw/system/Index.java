package com.iw.system;


// this object represents a single index with the ITS
public class Index {
    private String Name;
    private String Table;
    private String Field;

    public Index () {}
    public Index (String Name, String Table, String Field) {
        this.Name = Name; this.Table = Table; this.Field = Field;
    }
    public Index (String Name) {
        this.Name = Name;
    }

    public String getName() { return Name; }
    public String getTable() { return Table; }
    public String getField() { return Field; }

    public void setName(String Name) { this.Name = Name; }
    public void setTable(String Table) { this.Table = Table; }
    public void setField(String Field) { this.Field = Field; }
    // lowercase table col match
    public boolean isThisTableField (String Table, String Field) {
        return  this.Table.toUpperCase().equals(Table.toUpperCase()) &&
                this.Field.toUpperCase().equals(Field.toUpperCase());
    }

}
