package com.iw.tools;

import java.util.*;
import java.sql.*;
import com.javaexchange.dbConnectionBroker.DbConnectionBroker;

// interface to java exchange db connection pool com.javaexchange...

public class DBConnection
{
    public static Hashtable htDbConnectionBrokers = new Hashtable();
    private static Object oSynchCountConnectionsAPI = new Object();
    private static Object oSynchCountConnectionsSVR = new Object();
    private static int iCountConnectionGetsAPI = 0;
    private static int iCountConnectionGetsSVR = 0;
    private static int iCountConnectionFreeAPI = 0;
    private static int iCountConnectionFreeSVR = 0;

    public static String ip = "207.103.213.114";  // defaults
    public static String dbname = "client"; // defaults
    public static String dbuser = "sbooks"; // defaults
    public static String dbpass = "racer9"; // defaults

    private static Hashtable htConnectionsOut = new Hashtable();

    public static Connection getConnection()
		throws Exception
	{
        return getConnection("no desc" );
    }
    public static Connection getConnection(String sDesc )
		throws Exception
	{
		DbConnectionBroker dbcbToUse = (DbConnectionBroker) htDbConnectionBrokers.get ("API");
		if ( dbcbToUse == null )
		{
            String sDB_OracleJDBC = dbname;
			try
			{
                System.out.println("Creating new DbConnectionBroker..");
				dbcbToUse = new DbConnectionBroker (
						"oracle.jdbc.driver.OracleDriver",
                        "jdbc:oracle:thin:@"+ip+":1521:"+dbname,
						dbuser,
						dbpass,
						5,   // min conns
						200,  // max cnns
						"db.log",
						1 //maxConnTime: Time in days between connection resets. (Reset does a basic cleanup)
					);
                System.out.println("Created.");
				htDbConnectionBrokers.put ("API", dbcbToUse);
			}

			catch ( Exception e)
			{
				System.out.println("Error constructing DB connection broker for DB [" + sDB_OracleJDBC  + "] \r\n");
				throw e;
			}
		}

		Connection dbcRtn = null;

        long lStartTime = System.currentTimeMillis();
        long lMaxWaitTime = 60 * 1000 ; // 1 minute
        int iNulCounter = 0;
        while ( true )
        {
            dbcRtn = dbcbToUse.getConnection();
            if (  dbcRtn != null )
            {
                if ( iNulCounter > 0 )
                    System.out.println(" WAS NULL - GOT DBC SUCCESS EVENTUALLY : t [" + Thread.currentThread().getName() + "] dbc non null after null iNulCounter [" + iNulCounter + "]" );
                break;
            }
            else  {
                iNulCounter++;
                System.out.println(" DBC NULL: t [" + Thread.currentThread().getName() + "] dbc null after null iNulCounter [" + iNulCounter + "]" );
                if ( ( System.currentTimeMillis() - lStartTime ) > lMaxWaitTime )
                {
                    throw new Exception ("dcb null after max wait : t [" + Thread.currentThread().getName() + "] dbc null after null  iNulCounter [" + iNulCounter + "]" );
                }
            }

        }

        htConnectionsOut.put ( dbcRtn, new Long ( System.currentTimeMillis() ));
		return dbcRtn;

	}	 // ection getDBConnection()

    public static Connection freeConnection ( Connection dbc, String sWhichDB )
		throws Exception
	{
        return freeConnection ( dbc, sWhichDB, "");
    }

	// put connection back in the pool for use
	public static Connection freeConnection ( Connection dbc, String sWhichDB, String sDesc )
		throws Exception
	{

        if ( dbc != null )
        {
            Long LTimeGivenOut = (Long) htConnectionsOut.get ( dbc );
            if ( LTimeGivenOut == null ) {
                System.out.println("DBC FREE ERROR t [" + Thread.currentThread().getName() + "] dbc not recognized as having been given out trying to return to [" + sWhichDB + "] sDesc [" + sDesc + "]" );
                throw new Exception ("DBC FREE ERROR t [" + Thread.currentThread().getName() + "] dbc not recognized as having been given out [" + sWhichDB + "] sDesc [" + sDesc + "]" );
            }
            else
            {
                htConnectionsOut.remove(dbc);
            }

            DbConnectionBroker dbcb = (DbConnectionBroker) htDbConnectionBrokers.get (sWhichDB);
            if ( dbcb == null )
                throw new Exception ("internal program error no such DB connection pool [" + sWhichDB + "]");
            dbcb.freeConnection ( dbc );
            if ( sWhichDB.equals("API") ) {
                synchronized ( oSynchCountConnectionsAPI ) {
                    iCountConnectionFreeAPI++;
                }
            }
            else if ( sWhichDB.equals("SVR") ){
                synchronized ( oSynchCountConnectionsSVR ) {
                    iCountConnectionFreeSVR++;
                }
            }
            else
                throw new Exception ("unknown free DB pool [" + sWhichDB + "]");


            return null;
        }
        else  // assume that if called with null caller just wants to ses counts
        {
            return null;
        }
	}

	public static void destroy ( String sWhichDB )
		throws Exception
	{
        System.out.println ("destroy api.statics.DBConnectionJX sWhichDB [" + sWhichDB + "]");
		DbConnectionBroker dbcb = (DbConnectionBroker) htDbConnectionBrokers.get (sWhichDB);
		if ( dbcb == null )
			throw new Exception ("internal program error no such DB connection pool [" + sWhichDB + "]");
		dbcb.destroy ( );
	}
}
