package com.iw;

import org.pdfbox.Main;
import org.textmining.text.extraction.WordExtractor;

import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

import com.iw.system.*;
import com.iw.tools.*;
import com.iw.repository.*;
import com.iw.fcrawl.Machine;

/*
   FCrawl.class

   This is the Main routine used to both extract information from repositories and
   process any new information that is found accordingly.

   The generic class "Repository" contains most of the functions necessary.   To add
   a new repository one can subclass the Repository class and add any new functionality.
       RepositoryID			-	The unique repository identifier
	   UserId				-	Username into the Indraweb Taxonomy Server
	   Password				-   Password into the Indraweb Taxonomy Server

   Optional input arguments:
	   Corpora				-	A comma separated list indicating the corpora to classify
							    against.   Defaults to configuration file if not provided.
	   Genre				-	Specify the genre (folder) identifier for this document set
	   Config				-   Location of configuration file containing API and thread info
       Batch			    -   Run FCrawl in BATCH mode?  Default is false
	   DocSumm				-   Create document summaries ? Default is true
	   ErrorRun				-	Re-run all documents in the problem queue (C:\problem-documents.log)

   Web Crawl specific optional input arguments:
       EndTimes             -   (WebCache only) Specify the maximum amount of time to run for (currently used only by WebCache)
       Restore              -   (WebCache only) Restore state from file
       Interval             -   (Web only) Minimum interval (in seconds) from which to grab files PER PROXY (if applicable)
       ProxyConfig          -   (Web only) Points to a configuration file a series of proxy servers to download
                                documents, used for IP spoofing.
       DocumentID           -   (PubMed only) Specify the document identifier starting point (descending).
       StopID               -   (PubMed only) Specify the document identifier stopping point (descending).

   Main.class work flow:

    1.  Read configuration file and retrieve API locations and thread information
    2.  Log into each ITS machine and retrieve a persistent session key identifier.
	3.	Invalidate the cache files on each of the API servers
	4.  Call the ITS and retrieve repository information including:
			RepositoryType	-	Drives which repository object is to be instantiated
			RepositoryLoc	-	Unique location specific to the repository
			UserId			-	Username into the repository
			Password		-	Password into the repository
			FullText		-	Can this repository handle full text? (Boolean)
    5.  Extract each watched folder in the repository:
			Path			-	Path to folder in the repository
			Recurse			-	Do we recurse subdirectories below this folder? (Boolean)
			DateLastCapture	-	Date of last time this folder was modified

	6.  FOR EACH Watched Folder in this repository..

		A.  Spin a crawler thread for each "watched folder" being searched.  This "crawler thread"
			will spider through the folder, retrieving documents and storing them in a queue.
			Crawl functionality is written in the Repository subclass
		B.	Use the repository object to extract new information from the repository using
			the DateLastCapture attribute obtained in step 3.

    7.  Spin off X classify threads for each API specified in the configuration file.   These
		classify threads will pull documents from the queue and send them to one of the available
		APIs to be classified.

	That's it!

    UPDATE: 10/15/2002
    Changed FCRAWL to be multi-threaded.  Begin classifying files as they are found, and run
    multiple classifications at the same time (threads is now an optional parameter).

*/

public class FCrawl {
	// This is our MAIN function 
	public static void main(String args[])
	{
		// extract command line arguments and put into a hashtable
		HashTree htArgs = getArgHash(args);
		int iNumberOfThreads = 1;
		
		try {
			// Throw an exception if any required parameters are missing
			if ((!htArgs.containsKey("rid")) ||
				(!htArgs.containsKey("userid")) ||
				(!htArgs.containsKey("config")) ||
				(!htArgs.containsKey("password"))) { 
				throw new Exception("Usage: FCrawl -rid rid -userid "+
					"username -password password -config configlocation"); 
			}

            if (!htArgs.containsKey("corpora")) {
                System.out.println("Warning! Corpora argument is not specified on the command line.");
            }

			// Step 1: Open the configuration file for reading, but first, ensure that it exists
			File fConfigurationFile = new File((String) htArgs.get("config"));
			if (!fConfigurationFile.exists()) {
				throw new Exception ("Configuration file: "+(String)htArgs.get("config")+
									 "not found.");
			}
			
			// Run FCRAWL in Batch Mode? 
			if (!htArgs.containsKey("batch")) { htArgs.put("batch", "false"); }
			if (!htArgs.containsKey("docsumm")) { htArgs.put("docsumm", "true"); }
            if (!htArgs.containsKey("language")) { htArgs.put("language", "english"); }

			// read the configuration file, store machines and thread information
			Hashtable htMachines =  // machine/thread info stored in hash table
				ReadConfigurationFile(fConfigurationFile);
			if (htMachines.isEmpty()) {
				throw new Exception ("Error reading configuration file: "+
									 (String)htArgs.get("config")+
									 ".  File is empty or corrupted.");
			}				
			
			// Step 2: Attempt to log in, exit on failure
			if (!Login(htArgs, htMachines)) { 
				System.out.println("No active machines; nothing to do.  Quitting...");
				return; 
			}
			
			// Step 3: Invalidate the signature cache for each machine
			//InvalidateSigCache(htMachines);
			
			// Step 2: Retrieve repository information and get 
			//   the watched folders for this repository
			HashTree htRep = GetRepositoryProps(htArgs);
			HashTree htFolders = null;

            if (!htArgs.containsKey("askonce")) htFolders = GetWatchedFolders(htArgs);
            else htFolders = GetNodeRefreshList(htArgs);

			// If either the repository type does not exist or the folders are empty, exit
			if ((htRep == null) || (htFolders == null)) { 
				System.out.println("There are no watched folders for this "+
								   "repository identifier.  Quitting...");
				return; 
			}

			// Get class information for the class we need to instantiate
			String sClassName = (String) htRep.get ("REPOSITORYCLASS");
			Class cls = Class.forName ("com.iw.repository."+sClassName);

			// Get HashTree class information
			//  The class is instantiated dynamically from the database
			sClassName = "com.iw.system.HashTree";
			Class cH = Class.forName (sClassName);

			Class partypes[] = new Class[2];
			partypes[0] = cH;
			partypes[1] = cH;
			Constructor ct = cls.getConstructor(partypes);
            Object arglist[] = new Object[2];
			arglist[0] = htRep;
			arglist[1] = htArgs;

			com.iw.repository.Repository R = (com.iw.repository.Repository) ct.newInstance(arglist);
			
			System.out.println("Repository instance initialized: "+R.getClass().toString());
			System.out.println(htFolders.size()+" watched folder(s) found.\r\n");
			
			// Step 3: Spin off X threads, one to crawl each filespace (watched folders) and 
			//   X more to classify files as they are found.
			Vector vCrawlerThreads = new Vector();
			
			R.SetCrawlInProgress(true);
			
			Enumeration eR = htMachines.elements();

			while (eR.hasMoreElements()) {
				Machine M = (Machine) eR.nextElement();
				System.out.println(M.GetAPI()+": Creating "+M.GetNumberOfThreads()+
								   " threads to process classifications.");
				for (int loop = 0; loop < M.GetNumberOfThreads(); loop++) {
					Thread t = R.KickoffClassfier(M);
					System.out.println("Classify thread: "+t.getName()+" initialized.");
					M.vClassifyThreads.addElement(t);
				}
			}

			// start timing
			long lStart = System.currentTimeMillis();
			if (htArgs.containsKey("errorrun")) { 
				System.out.println("ErrorRun was selected.  Examing problem log.");
				Thread t = R.KickoffCrawler(new HashTree(), htArgs); 
				System.out.println("Problem solver thread: "+t.getName()+" initialized.");
				vCrawlerThreads.addElement(t);
			} else {
				Enumeration eF = htFolders.elements();
				while (eF.hasMoreElements()) {
					HashTree htFolder = (HashTree) eF.nextElement();
					Thread t = R.KickoffCrawler(htFolder); 
					System.out.println("Crawler thread: "+t.getName()+" initialized.");
					String sDate = R.SetDate(htFolder);

					vCrawlerThreads.addElement(t);
				}
			}
			R.ReindexDocuments();
			
			// wait here until crawlers have completed
			Enumeration eCrawlers = vCrawlerThreads.elements();
			while (eCrawlers.hasMoreElements()) {
				Thread t = (Thread) eCrawlers.nextElement(); t.join();
			}
			// once all crawlers have completed, set flag in repository 
			R.SetCrawlInProgress(false);
			System.out.println("All files have been crawled successfully (but still classifying).");
			System.out.println("Crawl completed in "+(System.currentTimeMillis() - lStart)+" ms.");
			
			Enumeration eR2 = htMachines.elements();
			while (eR2.hasMoreElements()) {
				Machine M = (Machine) eR2.nextElement();
				Enumeration eClassify = M.vClassifyThreads.elements();
				while (eClassify.hasMoreElements()) {
					Thread t = (Thread) eClassify.nextElement(); t.join();
				}
			}
			
			Enumeration eF2 = htFolders.elements();
			while (eF2.hasMoreElements()) {
				HashTree htFolder = (HashTree) eF2.nextElement();
				String sDate = R.SetDate(htFolder);
                if (htFolder.containsKey("PATH")) {
                    String sPath = (String) htFolder.get("PATH");
                    R.UpdateFolderTimestamp((String) htFolder.get("PATH"), sDate);
                }
			}
			
			System.out.println("Classifications complete.");
			System.out.println("Classifications completed in "+(System.currentTimeMillis() - lStart)+" ms.");
			
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			System.out.println(e.getMessage());
			return;
		}
	}
	
	// Read and parse the FCRAWL configuration file.  Each line in the configuration file
	// must contain an API server and the number of corresponding threads to weave.  
	// These two values are separated by a "tab" character.
	// Example: 
	//    http://localhost:8101/	12

	public static Hashtable ReadConfigurationFile (File f)
	throws Exception {
		FileInputStream fis = null;
		try { 
			fis = new FileInputStream(f);  
			BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
			String sData = new String();
		
			Hashtable htMachines = new Hashtable();
		
			while ((sData = buf.readLine()) != null) {
				int iSeparate = sData.indexOf("\t");
				String sAPI = sData.substring(0, iSeparate);
				String sThreads = sData.substring(iSeparate+1, sData.length());
				int iThreads = new Integer(sThreads).intValue();
				Machine m = new Machine(iThreads, sAPI);
				
				htMachines.put(sAPI, m);
				
				System.out.println("USE API: "+sAPI+" threads: "+sThreads);
			}
			
			return htMachines;
			
		} catch (Exception e) { return new Hashtable(); 
		} finally { fis.close(); }
	}
	
	// Log the user into the server and retrieve the session key.  Session key is then
	// added into the argument list to all API functions
	public static boolean Login (HashTree htArgs) {
		try {
			// Log the user into the server
			InvokeAPI API = new InvokeAPI ("security.TSLogin", htArgs);
			HashTree htResults = API.Execute();
			
			// If there is no "subscriber" tag, an error has occured
			if (!htResults.containsKey("SUBSCRIBER")) {
				throw new Exception("You have specified an invalid username, password combination.");
			}
			
			// Get user hash tree
			HashTree htUser = (HashTree) htResults.get("SUBSCRIBER");
				
			if (!htUser.containsKey("KEY")) {
				throw new Exception("You have specified an invalid username, password combination.");
			}

			// Login successful.  Get the session key and put into args for future arguments
			String SKEY = (String) htUser.get("KEY");
			htArgs.put("SKEY", SKEY);
			
			return true;
		} catch (Exception e) {
			System.out.println("Error in login routine: "+e.getMessage());
			return false;
		}
	}

	// this is the LOGIN method used in a multi-threaded environment
	public static boolean Login (HashTree htArgs, Hashtable htMachines) {
		
		boolean bLoginSuccess = false;
		
		Enumeration eM = htMachines.keys();
			
		while (eM.hasMoreElements()) {
			try {
				String key = (String) eM.nextElement();
				Machine m = (Machine) htMachines.get(key);
				HashTree htUser = new HashTree(); 
				
				System.out.println("Logging in ("+m.GetAPI()+")");
				
				htArgs.put("api", m.GetAPI());
				
				// Log the user into the server
				InvokeAPI API = new InvokeAPI ("security.TSLogin", htArgs);
				HashTree htResults = API.Execute();
			    System.out.println("Call made.");

				// If there is no "subscriber" tag, an error has occured
				if (htResults.containsKey("SUBSCRIBER")) {
					// Get user hash tree
					htUser = (HashTree) htResults.get("SUBSCRIBER");
				}
				
				if (!htUser.containsKey("KEY")) {
					System.out.println(key+": invalid username/password combination.  This "+
									   "machine will be removed from the active queue.");
					htMachines.remove(key);
				} else { 
					bLoginSuccess = true; 
					System.out.println("Login successful.");

					// Login successful.  Get the session key and put into args for future arguments
					String SKEY = (String) htUser.get("KEY");
					m.SetSKEY(SKEY); htMachines.put(key, m);
					
					// This will be the "default" machine for fetching parameters
					htArgs.put("api", m.GetAPI()); htArgs.put("SKEY", SKEY);
				}
			} catch (Exception e) {
				System.out.println("Error in login routine: "+e.getMessage());
			}
		}
		return bLoginSuccess;
	}
		
	// Remove signature cache files from each machine, ensuring that they are homogenious
	public static boolean InvalidateSigCache (Hashtable htMachines) {
		Enumeration eM = htMachines.elements();
		HashTree htArgs = new HashTree();
		
		while (eM.hasMoreElements()) {
			Machine m = (Machine) eM.nextElement();
			htArgs.put("api", m.GetAPI());
			htArgs.put("SKEY", m.GetSKEY());
				
			// Invalidate the signature cache
			InvokeAPI API = new InvokeAPI ("tspurge.TSPurgeSigCache", htArgs);
			HashTree htResults = API.Execute();
			
			// If there is no "success" tag, an error has occured
			if (!htResults.containsKey("SUCCESS")) { 
				System.out.println("Warning: could not invalidate signature cache "+
								   "for server ("+m.GetAPI()+")");
			}
		}
		return true;
	}

	
	// GetRepositoryProps from the API 
	public static HashTree GetRepositoryProps (HashTree htArgs) {
		try {
			// Log the user into the server
			InvokeAPI API = new InvokeAPI ("tsrepository.TSGetRepositoryProps", htArgs);
			HashTree htResults = API.Execute();
			
			// If there is no "subscriber" tag, an error has occured
			if (!htResults.containsKey("REPOSITORY")) {
				throw new Exception("The repository ID that was specified could not be found.");
			}
			
			// Get user hash tree
			HashTree htRep = (HashTree) htResults.get("REPOSITORY");
				
			return htRep;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	// GetWatchedFolders from the API 
	public static HashTree GetWatchedFolders (HashTree htArgs) {
		try {
			htArgs.put("FORMAT", "0");			
			InvokeAPI API = new InvokeAPI ("tsrepository.TSGetWatchedFolders", htArgs);
			HashTree htResults = API.Execute();
			
			// If there is no "subscriber" tag, an error has occured
			if (!htResults.containsKey("FOLDERS")) {
				throw new Exception("There are no watched folders specified for this repository.");
			}
			
			// Get user hash tree
			HashTree htFolders = (HashTree) htResults.get("FOLDERS");
				
			return htFolders;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

    public static HashTree GetNodeRefreshList (HashTree htArgs) {
		try {
			InvokeAPI API = new InvokeAPI ("tsclassify.TSGetRefreshNodeList", htArgs);
			HashTree htResults = API.Execute();

			// If there is no "subscriber" tag, an error has occured
			if (!htResults.containsKey("NODES")) {
				throw new Exception("There are no watched folders specified for this repository.");
			}

			// Get user hash tree
			return htResults;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	// GetArgHash takes command line arguments and parses them into a Hash structure
	public static HashTree getArgHash ( String[] args) {
		HashTree htHash = new HashTree();
		
		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim(); 	
				String sVal = new String("");
				
				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				} 
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}
		
		return htHash;
	}

    // Filter a PDF document to an HTML document
    public static File PDFtoHTML(File PDFfile) throws Exception {
        File tempFile = new File("temp.html");
        //org.apache.log4j.helpers.LogLog.setQuietMode(true);

        try {
            System.out.println("Filtering: " + PDFfile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
            Main.PDFtoHTML(PDFfile.getAbsolutePath(), tempFile.getAbsolutePath());
            wrapHTML(tempFile);
        } catch (Exception ex) {
            ex.printStackTrace(System.out); throw ex;
        }

        return tempFile;
    }
    // Filter a WORD document to an HTML document
    public static File MSWORDtoHTML(File WordFile) throws Exception {
        File tempFile = new File("temp.html");
        if (tempFile.exists()) {
            tempFile.delete();
        }

        System.out.println("Filtering: "+ WordFile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

        FileInputStream in = new FileInputStream (WordFile.getAbsolutePath());
        WordExtractor extractor = new WordExtractor();

        String s = extractor.extractText(in);
        wrapHTML(s, WordFile.getName(), tempFile);

        return tempFile;
    }

    // take a text file and wrap it with HTML
    public static void wrapHTML(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>"+f.getName()+"</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public static void wrapHTML(String s, String title, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>"+title+"</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

}