
import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.cactus.*;
import org.apache.cactus.server.runner.ServletTestRunner;
import org.dom4j.io.SAXReader;
import org.dom4j.*;
import org.xml.sax.*;

import java.util.*;
import java.io.*;

import com.indraweb.util.UtilFile;
import com.iw.tools.Dom4jHelper;
import api.tests.*;
import api.APIHandler;


/**
 * IndraTestCase.java
 *  IndraTestCase is invoked automatically by the ServletTestRuner inside of Cactus.  This template will
 *  read a pregenerated XML file with test cases, load them into an array and call each of them.  Individual
 *  test criteria (assertions) will be evaluated and the results displayed in XML.
 *
 *	@authors Michael Puscar, Indraweb Inc., All Rights Reserved.
 */

public class IndraTestCase extends ServletTestCase
{
    public static int iTestNumber = -1;
    public static TestItem[] testItemArr;
   // private static int iCountInstancesForLookup = 0;
    private StringBuffer sbTestInfo = null;

    //private String sTheName = null;
    public IndraTestCase ( String theName )
    {
        super(theName);
    }

    // suite()
    // This function is invoked by the ServletTestRunner and is used to define the forthcoming test suite.
    //  An XML file is read from the temp directory containing a series of API functions and arguments, and
    //  these are used to instantiate test objects.
    public static Test suite() throws Exception
    {
        // parse the test case XML file and build each test case
        //System.out.println("IndraTestCase running file ["  + APIHandler.sCDMSTESTFILE   + "]" );
        File f = new File ( APIHandler.sCDMSTESTFILE );
        if (!f.exists()) throw new Exception("Could not read file [" + APIHandler.sCDMSTESTFILE  + "]");

        String sXmlTestFileAsString = UtilFile.getFileAsString ( APIHandler.sCDMSTESTFILE );
        org.dom4j.Document d = Dom4jHelper.getDomFromstring(sXmlTestFileAsString + "</TESTCASES>") ;
        Vector vTestCases = getTestItems(d);

        if (vTestCases.size() == 0) throw new Exception("There were no test cases found in the file "+f.getAbsolutePath());

        testItemArr = new TestItem [ vTestCases.size() ];

        IndraTestSuite indraTestSuite = new IndraTestSuite("Indraweb Test");

        for (int i = 0; i < vTestCases.size(); i++)
        {
            TestItem ti = (TestItem) vTestCases.elementAt(i);
            IndraTestSuite its = new IndraTestSuite(IndraTestCase.class, ti.getFunctionName());

            indraTestSuite.addTest( its );
            testItemArr[i] = ti;
        }

        api.Log.Log(vTestCases.size()+" test items queued for testing.");

        return indraTestSuite;
    }

    // the endDoGet response signifies the end of an API call and is the function where any assertions will occur.
    public void endDoGet(WebResponse cSideResponse) {
        TestItem ti = testItemArr[iTestNumber];
        if ((iTestNumber+1) == testItemArr.length) iTestNumber = -1; // reset

        String str = "";
        try { str = cSideResponse.getText(); }
        catch (Exception e) {
            api.Log.Log("General error detected in endDoGet()! "+stackTraceToString(e)+".\r\n");
            failCondition(ti.getFunctionName(), "General parsing error detected.", e);
        }

        if ((str == null) || (str.equals(""))) {
            api.Log.Log("API call "+ti.getFunctionName()+" returned empty string.");
            failCondition(ti.getFunctionName(),
                    "API call "+ti.getFunctionName()+" returned empty string.", new Exception(""));
        }

        org.dom4j.Document doc = null;
        try { doc = com.iw.tools.Dom4jHelper.getDomFromstring(str); }
        catch (Exception e) {
            api.Log.Log("SAX XML parsing error detected in endDoGet()! "+stackTraceToString(e)+".\r\n");
            api.Log.Log("XML follows.");
            api.Log.Log(str);
            failCondition(ti.getFunctionName(), "SAX XML parsing error detected.", e);
        }

        // run all of the positive assertion test cases
        if (ti.getPositiveAssertions().size() > 0) {
            Enumeration ePositives = ti.getPositiveAssertions().elements();
            while (ePositives.hasMoreElements()) {
                Assertion as = (Assertion) ePositives.nextElement();

                if (as.getExistsWhere().equalsIgnoreCase("XMLTAG"))
                    if (!containsTag(doc, as.getSearchText())) failCondition(ti.getFunctionName(), as, doc, ti);
                else if (!containsText(str, as.getSearchText())) failCondition(ti.getFunctionName(), as, ti);
            }
        }

        // run all of the negative assertion test cases
        if (ti.getNegativeAssertions().size() > 0) {
            Enumeration eNegatives = ti.getNegativeAssertions().elements();
            while (eNegatives.hasMoreElements()) {
                Assertion as = (Assertion) eNegatives.nextElement();

                if (as.getExistsWhere().equalsIgnoreCase("XMLTAG"))
                    if (containsTag(doc, as.getSearchText())) failCondition(ti.getFunctionName(), as, doc, ti);
                else if (containsText(str, as.getSearchText())) failCondition(ti.getFunctionName(), as, ti);
            }
        }
    }

    public void beginDoGet(WebRequest webRequest) { iTestNumber++; }

    public void testDoGet() {
        ts servlet = new ts();
        TestItem ti = testItemArr [ iTestNumber ];
        Properties props = System.getProperties ();
        synchronized ( this.getClass ())
        {
           // System.out.println("putting its instance hashcode [" + this.hashCode() + "] as props hbk27:" + ti.getFunctionName());
            //props.put (this, new StringBuffer("hbk27:" + ti.getFunctionName()));
            //IndraTestCase is a key into the system properties object to pass data back to the caller for xmk
            if ( props.get ("IndraTestCase") != null )
            {
                System.out.println("fatal test error - double indratestcases");
                System.out.println("fatal test error - double indratestcases");
                System.out.println("fatal test error - double indratestcases");
            }
            //System.out.println("putting [" + ti.getFunctionName() + "[" + ti.getAPICallURL() + "]" );
            Hashtable htTestInfo = new Hashtable();
            htTestInfo.put ("FnName", ti.getFunctionName());
            htTestInfo.put ("testURL", ti.getAPICallURL());
            Hashtable htTestInfoFromXML = ti.getTestInfoHT();
            Enumeration eTestInfoFromXML = htTestInfoFromXML.keys() ;
            while ( eTestInfoFromXML.hasMoreElements() )
            {
                Object oKey = eTestInfoFromXML.nextElement();
                htTestInfo.put ( oKey, htTestInfoFromXML.get(oKey));
            }
            props.put ("IndraTestCase", htTestInfo);
        }

        try { servlet.doGet(request, response, ti.getArguments(), true); }
        catch (Exception e) { api.Log.Log("Excneption in testDoGet()! "+stackTraceToString(e)+".\r\n"); }
    }

    private boolean containsTag(org.dom4j.Document doc, String tagName) {
        try { return Dom4jHelper.containsTag(tagName, doc.getRootElement()); }
        catch (Exception e) { api.Log.Log("Exception in containsTag()! "+stackTraceToString(e)+".\r\n"); }

        return false;
    }
    private boolean containsText(String xml, String text) {
        if (xml.toLowerCase().indexOf(text.toLowerCase()) != -1) return true;
        return false;
    }

    public void failCondition(String functionName, String failMessage, Exception e) {
        fail("Assertion 1 failed! Function ["+functionName+"] failMessage ["+failMessage+"] Stacktrace ["+
                stackTraceToString(e) + "]" );
    }
    public void failCondition(String functionName, Assertion as, TestItem ti) {
        String s = "Assertion 2 failed! \r\nFunction ["+functionName+"] \r\nFunction ["+functionName+"] " + getTestIDAndSeq(ti) + "\r\nDesc ["+as.getDesc()+"] \r\nExists ["+as.getExists()+
                   "] \r\nExists Where[ "+as.getExistsWhere()+"] \r\nSearch Text ["+as.getSearchText()+"]";
        if (ti.getArguments().size() > 0) {
            s = s + " Failed API Call Arguments ";
            Enumeration eA = ti.getArguments().keys();
            int i = 0;
            while (eA.hasMoreElements()) {
                String name = (String) eA.nextElement();
                String value = (String) ti.getArguments().get(name);

                s = s.concat("\r\n" + i + ". ["+name+"] value ["+value+"]");
                i++;
            }
        }

        fail(s);

    }

    public void failCondition(String functionName, Assertion as, Exception e) {
        fail("Assertion 3 failed! Function ["+functionName+"] Desc ["+as.getDesc()+"] Exists ["+as.getExists()+
                "] Exists Where ["+as.getExistsWhere()+"] Search Text ["+as.getSearchText()+"] Stacktrace: ["+
                stackTraceToString(e)+ "]");
    }
    public void failCondition(String functionName, Assertion as, org.dom4j.Document doc, TestItem ti) {
        // display the internal stack trace from the API, if one can be found
        // error: TSRESULT/IW_ERROR
        org.dom4j.Node n = doc.selectSingleNode("TSRESULT/IW_ERROR");
        if (n == null) failCondition(functionName, as, ti);

        String s = "";
        if (ti.getArguments().size() > 0) {
            s = s.concat("\r\n Failed API Call Arguments ");
            Enumeration eA = ti.getArguments().keys();
            int i = 0;
            while (eA.hasMoreElements()) {
                String name = (String) eA.nextElement();
                String value = (String) ti.getArguments().get(name);

                s = s.concat("\r\n" + i + ". name ["+name+"] value ["+value+"]");
                i++;
            }
        }

        fail("Assertion 4 failed! Function[ "+functionName+"] " + getTestIDAndSeq(ti) + " Desc ["+as.getDesc()+"] Exists ["+as.getExists()+
                "] Exists Where ["+as.getExistsWhere()+"] Search Text ["+as.getSearchText()+ "][" +
                s+"] Stacktrace ["+n.getText()+ "]");
    }
    private String getTestIDAndSeq(TestItem ti)
    {
        return "TestID [" + ti.getTestID() + "] TestSeq [" + ti.getTestSequence() + "]";
    }
    public static String stackTraceToString ( Throwable e ) {
		try {
			StringWriter sw = new StringWriter ( );
			PrintWriter pw = new PrintWriter ( sw );
			e.printStackTrace ( pw ) ;
			return "StackTRACE " + sw.toString ( ) + " ";
		} catch (Exception e2) { api.Log.Log("Exception in stackTraceToString()! "+stackTraceToString(e2)+".\r\n"); }

        return "";
	}

    public static Vector getTestItems (org.dom4j.Document doc) throws Exception  {
        Vector v = new Vector();
        api.Log.Log("entering getTestItems()");

        try {
            Element elemRoot = doc.getRootElement ();

            String sTESTDESC = null;
            Iterator i = elemRoot.elements().iterator();
            while (i.hasNext()) {
                Element elemTestCase = (Element) i.next();
                if ( elemTestCase.getName().equalsIgnoreCase("testcase") )
                {
                    Iterator i2 = elemTestCase.elements().iterator();

                    TestItem ti = new TestItem(i2);

                    v.add(ti);
                }
            }
        } catch (Exception e2) { api.Log.Log("Exception in stackTraceToString()! "+stackTraceToString(e2)+".\r\n"); }

        api.Log.Log("leaving getTestItems() size:"+v.size());
        return v;
    }

    public static org.dom4j.Document read(File f) {
        FileInputStream fis = null;
        try {
             fis = new FileInputStream(f);
        } catch (Exception e2) { api.Log.Log("Exception in read(f)! "+stackTraceToString(e2)+".\r\n"); }
        return read(fis);
    }
    public static org.dom4j.Document read(InputStream is) {
        try {
            long lStart = System.currentTimeMillis();

            SAXReader xmlReader = new SAXReader(false);
            ErrorHandler eh = new ErrorHandler() {
                    public void fatalError (SAXParseException s) {
                        api.Log.Log("Fatal error in processing XML test case document."); }
                    public void error (SAXParseException s) {
                        api.Log.Log("Error in processing XML test case document."); }
                    public void warning (SAXParseException s) {
                        api.Log.Log("Warning in processing XML test case document."); }
            };
            xmlReader.setErrorHandler(eh);

            InputSource ins = new InputSource(is);
            org.dom4j.Document doc = xmlReader.read(ins);

            long lStop = System.currentTimeMillis() - lStart;
            //System.out.println("<!-- Timing: " + lStop + " ms -->");

            return doc;
        } catch (Exception except) {
            except.printStackTrace();
        }
        return null;
    }
  /*  public java.lang.String getName()
    {

            api.Log.Log ("remove this2");
            api.Log.Log ("remove this2");
            api.Log.Log ("remove this2");
            api.Log.Log ("remove this2");
            Exception e = new Exception("no error2 - just dumping stack ");
            api.Log.Log ("HBK2 JUNIT TEST DUMPTRACE : " + api.Log.stackTraceToString(e));
            return super.getName();
    }
*/

}
