package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.iw.system.Node;
import com.indraweb.database.*;
import api.Log;

/**
 * Remove all leading numbers from titles of all nodes underneath the given node to be lower case.  If the "recursive"
 * flag is set, all node descendents under the given node are capitalized recursively.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of this node.
 *  @param  NodeTitle The node title currently assigned to this node.
 *  @param  Recursive   If true, all node descendents will be organized.  Default is false.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSStripChildren&NodeID=2000&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Nodes numbers stripped successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSStripChildren {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sNodeTitle = (String) props.get("NodeTitle", true);
        String sRecursive = (String) props.get("recursive");
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            // for authorization purposes, need to know the corpus id of this node
            String sSQL = " select CorpusId from Node where NodeID = " + sNodeID;
            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);
            rs.next();

            int iCorpusId = rs.getInt(1);
            rs.close();
            stmt.close();

            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            boolean bRecurse = false;
            if (sRecursive != null) {
                bRecurse = true;
            }

            Node n = new Node(); n.set("NODEID", sNodeID); n.set("NODETITLE", sNodeTitle);
            stripNode(n, dbc, bRecurse);
            out.println("<SUCCESS>Nodes stripped successfully.</SUCCESS>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }

    public static void stripNode(Node n, Connection dbc, boolean bRecurse)
            throws Exception {

        setNodeTitle(n, dbc);

        if (bRecurse) {
            String sSQL = Node.getSQL(dbc)+" where n.parentid = " + n.get("NODEID") +" order by lower(nodetitle) asc";
            Statement stmt = null;
            ResultSet rs = null;

            Vector vChildrenToStrip = new Vector();

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery (sSQL);
                int i = 0;

                while (rs.next()) {
                    i++; Node n2 = new Node(rs);
                    if (bRecurse) { vChildrenToStrip.add(n2); }
                }
            } catch (Exception e) { throw e; }
            finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }

            for (int i = 0; i < vChildrenToStrip.size(); i++)
                stripNode((Node) vChildrenToStrip.elementAt(i), dbc, true);
        }
    }

    public static void setNodeTitle(Node n, Connection dbc)
            throws Exception {

        String sNodeTitle = n.get("NODETITLE");
        String sNodeID = n.get("NODEID");

        sNodeTitle = getStripped(sNodeTitle.toLowerCase());
        sNodeTitle = sNodeTitle.replaceAll("'", "''"); // replace quotes for database update

        String sSQL = "update node set nodetitle = '" + sNodeTitle + "', dateupdated = sysdate where NodeID = "+sNodeID;
        Statement stmt = null;

        try {
			stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
        } catch (Exception e) { throw e; }
        finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }

    // strip all preceding numbers from titles, e.g. 1.0, 1.2.3, etc.
    public static String getStripped(String str) {
        String sout = ""; boolean strip = true;
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (strip && (ch == ' ')) { strip = false; }
            else if (strip && ((Character.isDigit(ch)) || (ch == '.'))) { strip = true; }
            else { strip = false; sout = sout + ch; }
        }
        return sout;
    }
}
