package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 * Retrieve all nodes (and their direct parent) containing a signature word or words.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Keywords    Signature term or terms
 *  @param  CorpusID (optional) Restrict this search by corpus using a unique corpus identifier
 *  @param  Occur (optional)    Assign a minimum frequency threshold for matches.  Default is 4.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSNodeTitleSigSearch&Keywords=test&SKEY=9919294812

 *	@return	a series of NODE objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>1000</NODEID>
			<CORPUSID>5</CORPUSID>
			<NODETITLE>Anthrax</NODETITLE>
			<PARENTID>990</PARENTID>
			<INDEXWITHINPARENT>1</INDEXWITHINPARENT>
			<DEPTHFROMROOT>3</DEPTHFROMROOT>
			<DATESCANNED>null</DATESCANNED>
			<DATEUPDATED>null</DATEUPDATED>
			<PNODEID>990</PNODEID>
			<PCORPUSID>5</PCORPUSID>
			<PNODETITLE>Biological Warfare</PNODETITLE>
			<PPARENTID>15</PPARENTID>
			<PINDEXWITHINPARENT>3</PINDEXWITHINPARENT>
			<PDEPTHFROMROOT>2</PDEPTHFROMROOT>
			<PDATESCANNED>null</PDATESCANNED>
			<PDATEUPDATED>null</PDATEUPDATED>
        </NODE>
 ...
 </TSRESULT>
  \endverbatim
 */
public class TSNodeTitleSigSearch
{
	private static int iCallCounter = 0;
	private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

	// TSNodeTitleSearch (sessionid, keywords, corpusid)
	// Note: Corpusid is OPTIONAL
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		synchronized (ICallCounterSemaphore)
			{ iCallCounter++; }
		out.println ("<CALLCOUNT_FN>" + iCallCounter + "</CALLCOUNT_FN>");

		String sKeywords = (String) props.get ("Keywords");		// Keywords for node search
		String sCorpusID = (String) props.get ("CorpusID");		// Restrict to corpus? (optional)
		String sOccur = (String) props.get ("Occur");			// Restrict "like" to freq (optional)
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		// If not specified, use "4"
		if (sOccur == null) { sOccur = "4"; }
		
		// Break sKeywords into array of words separated by spaces
		out.println ("<KEYWORDS> "+sKeywords+"</KEYWORDS>");
		
        Vector vKeywords = new Vector();

        while ( true ) {
			int endLoc = sKeywords.indexOf (' ') ;
            if (endLoc == -1) {
			    vKeywords.addElement( sKeywords );
                break; // last element
            } else {
                vKeywords.addElement( sKeywords.substring ( 0, endLoc ) );
                sKeywords = sKeywords.substring ( endLoc + 1  ) ;
			}
		}

		try {
			// Select all nodes that matched this search, along with their direct parent
			String sSQL = " select a.LinkNodeId,a.CorpusId,a.NodeTitle,"+
						  " a.ParentId,a.NodeIndexWithinParent,a.DepthFromRoot, " +
						  " to_char(a.datescanned, 'MM/DD/YY HH:MI'), to_char(a.dateupdated, 'MM/DD/YY HH:MI')," +
						  " b.LinkNodeId,b.CorpusId,b.NodeTitle,"+
						  " b.ParentId,b.NodeIndexWithinParent,b.DepthFromRoot, " +
						  " to_char(b.datescanned, 'MM/DD/YY HH:MI'), to_char(b.dateupdated, 'MM/DD/YY HH:MI')" +
						  " from Node a, Node b where a.nodestatus != -1 and b.nodestatus != -1 "+
						  " and a.parentid = b.linknodeid and ";
			for (int loop = 0; loop < vKeywords.size(); loop++) {
				if (loop != 0) { sSQL = sSQL + "and "; }	
				sSQL = sSQL + "upper(a.NodeTitle) like upper('%"+vKeywords.elementAt(loop)+"%') ";
			}
				
			if (sCorpusID != null) { sSQL = sSQL + "and a.CorpusID = "+sCorpusID+" "; }

			sSQL = sSQL + " order by a.NodeTitle, b.NodeTitle, a.CorpusID asc";
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.   
				
				int iNodeId = rs.getInt(1);
				int iCorpusId = rs.getInt(2);
				String sNodeTitle = rs.getString(3);
				int iParentId = rs.getInt(4);
				int iNIWP = rs.getInt(5);
				int iDepthFromRoot = rs.getInt(6);
				String sDateScanned = rs.getString(7);
				String sDateUpdated = rs.getString(8);
				int ibNodeId = rs.getInt(9);
				int ibCorpusId = rs.getInt(10);
				String sbNodeTitle = rs.getString(11);
				int ibParentId = rs.getInt(12);
				int ibNIWP = rs.getInt(13);
				int ibDepthFromRoot = rs.getInt(14);
				String sbDateScanned = rs.getString(15);
				String sbDateUpdated = rs.getString(16);

				if ((Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) ||
                    (u.IsAuthorized(iCorpusId, out))) {
					loop = loop + 1;
					if (loop == 1) { out.println ("<NODES> "); }
					out.println ("   <NODE> ");
					out.println ("   <NODEID>"+iNodeId+"</NODEID>");
					out.println ("   <CORPUSID>"+iCorpusId+"</CORPUSID>");
					out.println ("   <NODETITLE>"+sNodeTitle+"</NODETITLE>");
					out.println ("   <PARENTID>"+iParentId+"</PARENTID>");
					out.println ("   <INDEXWITHINPARENT>"+iNIWP+"</INDEXWITHINPARENT>");
					out.println ("   <DEPTHFROMROOT>"+iDepthFromRoot+"</DEPTHFROMROOT>");
					out.println ("   <DATESCANNED>"+sDateScanned+"</DATESCANNED>");
					out.println ("   <DATEUPDATED>"+sDateUpdated+"</DATEUPDATED>");
					out.println ("   <PNODEID>"+ibNodeId+"</PNODEID>");
					out.println ("   <PCORPUSID>"+ibCorpusId+"</PCORPUSID>");
					out.println ("   <PNODETITLE>"+sbNodeTitle+"</PNODETITLE>");
					out.println ("   <PPARENTID>"+ibParentId+"</PPARENTID>");
					out.println ("   <PINDEXWITHINPARENT>"+ibNIWP+"</PINDEXWITHINPARENT>");
					out.println ("   <PDEPTHFROMROOT>"+ibDepthFromRoot+"</PDEPTHFROMROOT>");
					out.println ("   <PDATESCANNED>"+sbDateScanned+"</PDATESCANNED>");
					out.println ("   <PDATEUPDATED>"+sbDateUpdated+"</PDATEUPDATED>");
					out.println ("   </NODE>");
				}
			}
			rs.close();
		    stmt.close();
		
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Sorry no matches for keywords: "+sKeywords+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}

			out.println ("</NODES> ");
		}
		
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		} 
		
		// Now select all nodes that were LIKE the keywords
		// select n.nodetitle, a.nodetitle from signature s, node n, node a where 
		// n.nodeid = s.nodeid and n.parentid = a.nodeid and upper(n.nodetitle) not 
		// like upper('%word%') and upper(s.signatureword) = upper('$word') and 
		// s.signatureoccurences > $occur");

		try {
			String sSQL = " select a.NodeId,a.CorpusId,a.NodeTitle,"+
						  " a.ParentId,a.NodeIndexWithinParent,a.DepthFromRoot, " +
						  " to_char(a.datescanned, 'MM/DD/YY HH:MI'), to_char(a.dateupdated, 'MM/DD/YY HH:MI')," +
						  " b.NodeId,b.CorpusId,b.NodeTitle,"+
						  " b.ParentId,b.NodeIndexWithinParent,b.DepthFromRoot, " +
						  " to_char(b.datescanned, 'MM/DD/YY HH:MI'), to_char(b.dateupdated, 'MM/DD/YY HH:MI')" +
						  " from Node a, Node b, Signature s where a.parentid = b.nodeid and "+
						  " a.nodeid = s.nodeid and s.SignatureOccurences > "+sOccur+" ";
			for (int loop = 0; loop < vKeywords.size(); loop++) {
				sSQL = sSQL + "and ";	
				sSQL = sSQL + "upper(s.SignatureWord) like upper('%"+vKeywords.elementAt(loop)+"%') "+
					   " and upper(a.NodeTitle) not like upper('%"+vKeywords.elementAt(loop)+"%') ";
			}
				
			if (sCorpusID != null) { sSQL = sSQL + "and a.CorpusID = "+sCorpusID+" "; }

			sSQL = sSQL + " order by a.NodeTitle, b.NodeTitle, a.CorpusID asc";
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.   
				loop = loop + 1;
				
				int iNodeId = rs.getInt(1);
				int iCorpusId = rs.getInt(2);
				String sNodeTitle = rs.getString(3);
				int iParentId = rs.getInt(4);
				int iNIWP = rs.getInt(5);
				int iDepthFromRoot = rs.getInt(6);
				String sDateScanned = rs.getString(7);
				String sDateUpdated = rs.getString(8);
				int ibNodeId = rs.getInt(9);
				int ibCorpusId = rs.getInt(10);
				String sbNodeTitle = rs.getString(11);
				int ibParentId = rs.getInt(12);
				int ibNIWP = rs.getInt(13);
				int ibDepthFromRoot = rs.getInt(14);
				String sbDateScanned = rs.getString(15);
				String sbDateUpdated = rs.getString(16);
		
				if (loop == 1) { out.println ("<LIKENODES> "); }
				out.println ("   <NODE> ");
				out.println ("   <NODEID>"+iNodeId+"</NODEID>");
				out.println ("   <CORPUSID>"+iCorpusId+"</CORPUSID>");
				out.println ("   <NODETITLE>"+sNodeTitle+"</NODETITLE>");
				out.println ("   <PARENTID>"+iParentId+"</PARENTID>");
				out.println ("   <INDEXWITHINPARENT>"+iNIWP+"</INDEXWITHINPARENT>");
				out.println ("   <DEPTHFROMROOT>"+iDepthFromRoot+"</DEPTHFROMROOT>");
				out.println ("   <DATESCANNED>"+sDateScanned+"</DATESCANNED>");
				out.println ("   <DATEUPDATED>"+sDateUpdated+"</DATEUPDATED>");
				out.println ("   <PNODEID>"+ibNodeId+"</PNODEID>");
				out.println ("   <PCORPUSID>"+ibCorpusId+"</PCORPUSID>");
				out.println ("   <PNODETITLE>"+sbNodeTitle+"</PNODETITLE>");
				out.println ("   <PPARENTID>"+ibParentId+"</PPARENTID>");
				out.println ("   <PINDEXWITHINPARENT>"+ibNIWP+"</PINDEXWITHINPARENT>");
				out.println ("   <PDEPTHFROMROOT>"+ibDepthFromRoot+"</PDEPTHFROMROOT>");
				out.println ("   <PDATESCANNED>"+sbDateScanned+"</PDATESCANNED>");
				out.println ("   <PDATEUPDATED>"+sbDateUpdated+"</PDATEUPDATED>");
				out.println ("   </NODE>");
			}			
			rs.close();
		    stmt.close();
			// If no results found, throw an exception
			if (loop == 0) {
				out.println ( "<DEBUG>Sorry no matches for keywords: "+sKeywords+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}
			
			out.println ("</LIKENODES> ");
		}
		catch ( TSException tse )	
		{
			out.println("<DEBUG>No related nodes found.</DEBUG>");
		} 
	}
}
