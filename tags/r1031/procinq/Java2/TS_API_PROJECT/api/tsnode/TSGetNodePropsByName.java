package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 * Retrieve the properties of all nodes within the system given a node title.  Node title is not
 *  case sensitive in this context.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeTitle   Title of the node whose properties are being searched.
 *  @param  Corpora (optional)  Restrict search to a list of corpora using a comma separated list of unique corpus identifiers
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodePropsByName&NodeTitle=Anthrax&SKEY=8919294812

 *	@return	a series of NODE object
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>1087101</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Anthrax]]> </NODETITLE>
            <PARENTID>1087099</PARENTID>
            <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>0</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]></NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087101</LINKNODEID>
        </NODE>
        <NODE>
            <NODEID>1087678</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Anthrax]]> </NODETITLE>
            <PARENTID>1087099</PARENTID>
            <NODEINDEXWITHINPARENT>2</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>0</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>2</NODESIZE>
            <LINKNODEID>1087678</LINKNODEID>
        </NODE>
 </TSRESULT>
  \endverbatim
 */
public class TSGetNodePropsByName
{
	private static int iCallCounter = 0;
	private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

	// TSGetNodeProps (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

		String sNodeTitle = (String) props.get ("NodeTitle", true);
        String sCorpusList = (String) props.get ("Corpora");

		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // if the node title contains ampersands they must be escaped
        if (sNodeTitle.indexOf('&') != -1) {
            sNodeTitle.replaceAll("&", "&'||'");
        }

        Statement stmt = null; ResultSet rs = null;

		String sSQL = Node.getSQL(dbc)+" where lower(NodeTitle) = '"+sNodeTitle.toLowerCase()+"'";
        try {
            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            if (sCorpusList != null) { sSQL = sSQL+" and CorpusID in ("+sCorpusList+")"; }
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
                if (loop == 1) { out.println("<NODES>"); }
                Node n = new Node(rs);
                n.emitXML(out, u, true);
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
			}
            out.println("</NODES>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
	}
}
