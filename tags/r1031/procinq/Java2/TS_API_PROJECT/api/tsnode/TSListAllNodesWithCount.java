package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.results.NodeDocumentResult;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
//import api.results.*;

import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.iw.system.Node;
import com.iw.system.User;

/** Return all nodes in a given corpus with a count corresponding to the number of document associations for each.
 *   Results may optionally be limited to a specific depth level within the taxonomy.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID  Unique corpus identifier.
 *  @param  Depth (optional)    Specify a depth level to return
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSListAllNodesByCount&CorpusID=9&SKEY=8919294812

 *	@return	a series of NODE objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>1087175</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Root Node]]> </NODETITLE>
            <PARENTID>-1</PARENTID>
            <NODEINDEXWITHINPARENT>3</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>1</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087175</LINKNODEID>
            <COUNT>22</COUNT>
        </NODE>
        <NODE>
            <NODEID>1087101</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
            <PARENTID>1087099</PARENTID>
            <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>0</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087101</LINKNODEID>
            </COUNT>21</COUNT>
        </NODE>
 ...
    </NODES>
 </TSRESULT>
  \endverbatim
 */
public class TSListAllNodesWithCount
{
    public static User u;

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sKey = (String) props.get("SKEY", true);
		String sDepth = (String) props.get("Depth"); // optional
		u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            if (!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
                Hashtable htAuth = u.GetAuthorizedHash(out);
                if (!htAuth.containsKey(sCorpusID)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            }

            selectNodes(sCorpusID, sDepth, dbc, out);
		}

		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}

	public static void selectNodes (String sCorpusID, String sDepth, Connection dbc, PrintWriter out)
	throws Exception {

		String sSQL = Node.getSQLwithoutTable(dbc)+", count(d.nodeid) COUNT from Node n, NodeDocument d where N.CorpusID = "+sCorpusID+
            " and N.NodeID = D.NodeID (+)";
        if (sDepth != null) sSQL = sSQL +" and N.DepthFromRoot = "+sDepth;
        sSQL = sSQL + Node.getGroupBy(dbc);
        Statement stmt = dbc.createStatement();
		ResultSet rs = stmt.executeQuery (sSQL);

		int loop = 0;
		while ( rs.next() ) {
            loop++; if (loop == 1) { out.println("<NODES>"); }

            Node n = new Node(rs);
            n.emitXML(out, u, true);
		}

        if (loop > 0) out.println("</NODES>");
		rs.close();
	    stmt.close();
	}
}

