package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.classification.Data_WordsToNodes;
import com.iw.system.User;

/**
 * Add a signature word, frequency pair relationship to the specified node.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID    Unique node identifier of this node.
 *  @param  Word    Signature word to be added.
 *  @param  Frequency   Frequency attribute of the signature word being added.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSAddNodeSigWord&NodeID=2000&Word=Test&Frequency=30&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Signature word added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddNodeSignatures
{
	// TSAddNodeSigWord (sessionid, nodeid, word, frequency)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sSigs = (String) props.get ("Signatures", true);
        String sLang = (String) props.get ("lang", "EN");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null; ResultSet rs = null;

        if (sLang != null)
            api.Log.Log("DEBUG: TSAddNodeSignatures invoked with lang: "+sLang);
        else api.Log.Log("DEBUG: TSAddNodeSignatures invoked without a lang argument.");

		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);
			rs.next();

			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
			rs = null; stmt = null;

			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // sample: abdominal||29,abdominal ultrasound||12,aneurysm||6,aortic aneurysm||5,b-mode||4,doppler||6,echoes||5,imaging||12,ionizing||4
            String[] SigFreqPairs = sSigs.split(","); int loop = 0;
            for (int i = 0; i < SigFreqPairs.length; i++) {
                String[] WordFreq = SigFreqPairs[i].split("\\|\\|");
                if (WordFreq.length == 2) {
                    String sWord = WordFreq[0]; String sFreq = WordFreq[1];

                    sSQL = " insert into Signature (NodeId, SignatureWord, SignatureOccurences, Lang) "+
                           " values ("+sNodeID+",'"+sWord+"',"+sFreq+", '"+sLang+"')";

                    api.Log.Log("Signature insert: "+sSQL);

                    try {
                        stmt = dbc.createStatement();

                        // If query statement failed, throw an exception
                        if (stmt.executeUpdate (sSQL) == 0) {
                            out.println ( "<DEBUG>Failed to insert: "+sNodeID+" "+sWord+" "+sFreq+" </DEBUG>");
                        } else { loop++; }
                    } catch (Exception e) {
                        api.Log.LogError ( "*WARNING* Failed to insert: "+sNodeID+" "+sWord+" "+sFreq);
                    } finally { if (stmt != null) { stmt.close(); stmt = null; }}
                }
            }
			out.println("<SUCCESS>"+loop+" signature words added successfully.</SUCCESS>");

            // purge signature cache by corpus
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus
                    ("TSAddNodeSignatures", iCorpusId);

			sSQL = " update Node set NodeLastSync = SYSDATE, dateupdated = sysdate where NodeID = " +sNodeID;
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);
			rs.next();
			rs.close(); stmt.close();
            rs = null; stmt = null;
		}

		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
