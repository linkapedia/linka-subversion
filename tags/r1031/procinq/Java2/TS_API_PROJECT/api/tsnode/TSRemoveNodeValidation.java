package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Remove a node from the node validation queue.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID    Unique node identifier of the node to be removed from the validation queue.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSRemoveNodeValidation&NodeID=102&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node validation deleted successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveNodeValidation
{
	// TSAddNodeSigWord (sessionid, nodeid, word, frequency)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
			
			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			sSQL = " delete from NodeValidation where NodeID = "+sNodeID;
			stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from Validation table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Node validation deleted successfully.</SUCCESS>");
		    stmt.close();
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
