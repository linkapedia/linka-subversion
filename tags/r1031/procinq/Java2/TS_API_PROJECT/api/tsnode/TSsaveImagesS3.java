package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.CheckNocropImage;
import api.util.NodeUtils;
import api.util.search.images.WorkerImagesS3;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSsaveImagesS3 {

    private static final Logger LOG = Logger.getLogger(TSsaveImagesS3.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        LOG.debug("TSsaveImagesS3 Node");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        String corpusId = (String) props.get("corpusid");
        String recursively = (String) props.get("recursively");

        if (nodeid == null || corpusId == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty() || corpusId.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        if (recursively == null || recursively.isEmpty()) {
            recursively = "false";
        }
        //get list ids
        List<Long> list = null;
        List<byte[]> imagesCorpus = null;
        long nodeidL;
        try {
            nodeidL = Long.parseLong(nodeid);
            if (recursively.equals("true")) {
                list = nu.getTreeNodesNodeid(nodeidL);
            } else {
                list = new ArrayList<Long>();
                list.add(nodeidL);
            }
        } catch (NumberFormatException ex) {
            LOG.error("Error convert nodeid to integer", ex);
            return;
        }

        //validate images from the taxonomy
        try {
            imagesCorpus = nu.getImagesByCorpusId(Integer.parseInt(corpusId));
        } catch (SQLException e) {
            LOG.error("Error in the consult SQL", e);
            out.println("<RESULT>Error in the sql statement getting corpus image</RESULT>");
            return;
        }

        if (imagesCorpus.size() < 5) {
            out.println("<RESULT>The taxonomy Require 5 images to export</RESULT>");
            return;
        }

        if (list != null && !list.isEmpty()) {
            DecimalFormat twoDigits = new DecimalFormat("0");
            long percent = 0;
            WorkerImagesS3 worker = new WorkerImagesS3();
            int result = 0;
            List<String> messages = new ArrayList<String>();
            List<byte[]> images = null;
            int numNodes = list.size();
            int cont = 0;
            boolean isImageForNocrop = false;

            for (long id : list) {
                try {
                    images = nu.getImagesNodeId(id);
                } catch (SQLException e) {
                    LOG.error("Error getting images from nodeId: " + id, e);
                    continue;
                }
                //validate images nodes for nocrop                             
                BufferedImage image = null;
                for (byte[] byImage : images) {
                    try {
                        image = ImageIO.read(new ByteArrayInputStream(byImage));
                        if (CheckNocropImage.check(image)) {
                            isImageForNocrop = true;
                            break;
                        }
                    } catch (Exception e) {
                        LOG.error("Error reading a image", e);
                    }
                }

                if (images != null && !images.isEmpty()) {
                    if (!isImageForNocrop) {
                        Random rand = new Random(System.currentTimeMillis());
                        int ran = rand.nextInt(imagesCorpus.size());
                        List<byte[]> tempImagesCorpus = new ArrayList<byte[]>();
                        tempImagesCorpus.add(imagesCorpus.get(ran));
                        //save no crop taxonomy
                        result = worker.processNoCrop(id, tempImagesCorpus);
                    } else {
                        result = worker.processNoCrop(id, images);
                    }
                    result += worker.process(id, images);

                    //build messages
                    messages.add(id + " # Images saved -> " + result);
                } else {
                    //save ramdom taxonomy image for this nodeid
                    Random rand = new Random(System.currentTimeMillis());
                    int ran = rand.nextInt(imagesCorpus.size());
                    List<byte[]> imagesCorpusRandom = new ArrayList<byte[]>();
                    imagesCorpusRandom.add(imagesCorpus.get(ran));
                    result = worker.process(id, imagesCorpusRandom);
                    result += worker.processNoCrop(id, imagesCorpusRandom);
                    //build messages
                    messages.add(id + " # Images saved from taxonomy -> " + result);
                    LOG.info("The nodeid " + id + " Not have images");
                }
                result = 0;
                isImageForNocrop = false;
                //show progress
                percent = ((cont + 1) * 100) / numNodes;
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                LOG.debug("finished one proccess: " + percent + "%");
                out.flush();
                cont++;

            }
            //show result nodes with error
            out.println("<RESULT>");
            out.println("Process finished, see the log");
            out.println("<MSG>images with error</MSG>");
            out.println("<MESSAGES>");
            for (String msg : messages) {
                out.println("<MESSAGE>" + msg + "</MESSAGE>");
            }
            out.println("</MESSAGES>");
            out.println("</RESULT>");
        } else {
            out.println("<RESULT>Not found nodeIds for start the process</RESULT>");
        }
    }
}
