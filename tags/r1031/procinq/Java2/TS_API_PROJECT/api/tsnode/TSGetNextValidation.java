package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Get the next node queued for validation in the given corpus.  The node identifier and document count will
 *  be delivered in the response.  If the validation queue is empty, a NO_ROWS_FOUND exception will be thrown.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID  Unique corpus identifier of the corpus being validated.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNextValidation&CorpusID=5&SKEY=9919294812

 *	@return	NODEID, DOCUMENTCOUNT object pairs.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODE>
        <NODEID>9988</NODEID>
		<DOCUMENTCOUNT>100</DOCUMENTCOUNT>
    </NODE>
   </TSRESULT>
  \endverbatim
 */
public class TSGetNextValidation
{
	// Get the next node for this corpus available for validation.
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select min(nodeid), documentcount from "+
						  " nodevalidation where nodeid in (select "+
						  " nodeid from node where corpusID = "+sCorpusID+")"+
						  " group by documentcount";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			String sNodeID = null; String sDocCount = null;
			
			try {
				rs.next();
				sNodeID = rs.getString(1);
				sDocCount = rs.getString(2);
				rs.close(); stmt.close();
			} catch (Exception e) {
				// if no rows were found, throw an exception
				throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);	
			}
	
			out.println("<NODE>");
			out.println("   <NODEID>"+sNodeID+"</NODEID>");
			out.println("   <DOCUMENTCOUNT>"+sDocCount+"</DOCUMENTCOUNT>");
			out.println("</NODE>");
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
