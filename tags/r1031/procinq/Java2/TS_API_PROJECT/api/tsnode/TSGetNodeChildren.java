package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Retrieve all direct children of the given node.  Node children which are links to other areas of the
 *  system will contain a different NODEID from their LINKNODEID attribute.  This function is a wrapper
 *  function and simply calls CQL using SELECT <NODE> WHERE PARENTID = (nodeid) ORDER BY NODEINDEXWITHINPARENT ASC
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of the node whose children are being retrieved.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodeChildren&NodeID=5000&SKEY=9919294812

 *	@return	a series of NODE objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>1087101</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
            <PARENTID>1087099</PARENTID>
            <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>0</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]></NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087101</LINKNODEID>
        </NODE>
        <NODE>
            <NODEID>1087678</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Analytical Techniques]]> </NODETITLE>
            <PARENTID>1087099</PARENTID>
            <NODEINDEXWITHINPARENT>2</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>0</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>2</NODESIZE>
            <LINKNODEID>1087678</LINKNODEID>
        </NODE>
 ...
 </TSRESULT>
  \endverbatim
 */
public class TSGetNodeChildren
{
	private static int iCallCounter = 0;
	private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

	// TSGetNodeChildren (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {
		String iParentID = (String) props.get ("NodeID", true);

		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        String query = "SELECT <NODE> WHERE PARENTID = "+iParentID+" ORDER BY NODEINDEXWITHINPARENT ASC";
        props.put("query", query);

        api.tscql.TSCql.handleTSapiRequest(props, out, dbc);

        return;
    }
}
