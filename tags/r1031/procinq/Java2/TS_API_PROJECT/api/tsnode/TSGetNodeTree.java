package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 * Retrieve all direct ancestors of a given node.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodeTree&NodeID=999012&SKEY=8919294812

 *	@return	a series of NODE objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>1087175</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Fertilizers]]> </NODETITLE>
            <PARENTID>1087101</PARENTID>
            <NODEINDEXWITHINPARENT>3</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>1</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087175</LINKNODEID>
        </NODE>
        <NODE>
            <NODEID>1087101</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
            <PARENTID>1087099</PARENTID>
            <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>0</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087101</LINKNODEID>
        </NODE>
 ...
    </NODES>
 </TSRESULT>
  \endverbatim
 */
public class TSGetNodeTree
{
    public static User u;

	public static String WriteNode (PrintWriter out, Connection dbc, String sNodeID)
		throws Exception
	{
		String sSQL = Node.getSQL(dbc)+" where NodeId = " + sNodeID;
		Statement stmt = dbc.createStatement();	
		ResultSet rs = stmt.executeQuery (sSQL);
		
		int loop = 0;
		String sParentId = "";
		while ( rs.next() ) {
			loop = loop + 1;
            Node n = new Node(rs);
            n.emitXML(out, u, true);

            sParentId = n.get("PARENTID");
		}

		rs.close();
	    stmt.close();
		
		return sParentId;
	}
	
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			if (sNodeID.equals("-1")) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND); }
			
			String sParent = sNodeID;
			out.println("<NODES>");
			while (!sParent.equals("-1")) {	sParent = WriteNode(out, dbc, sParent); }
			out.println("</NODES>");

		} catch ( TSException tse )	{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
