package api.tsnode;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.system.Node;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Search and replace within the node source from the given node, and optionally all descendants.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param NodeID Unique node identifier of this node.
 * @param Replace A series of key-value pairs indicating the searches to be replaced in the format: KEY||VALUE,
 * @param replacetitle If true, replace the node title
 * @param replacesource If true, replace the node source
 * @param Recursive (optional) if true, search/replace recursively
 *
 * @note http://ITSSERVER/itsapi/ts?fn=tsnode.TSCopyNode&NodeID=2000&ParentID=2006&SKEY=9919294812
 *
 * @return	NODEID identifier of the newly created node \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <NODEID>20029</NODEID> </TSRESULT> \endverbatim
 */
public class TSSearchReplaceNode {

    private static int Count = 0;
    private static int Total = 0;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sReplace = (String) props.get("replace", true);
        String replaceTitle = (String) props.get("replacetitle");
        String replaceSource = (String) props.get("replacesource");
        String sRecurse = (String) props.get("recursive");
        String sShowStatus = (String) props.get("ShowStatus");

        Hashtable searchReplace = new Hashtable();

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
        }

        // load the replace encoded string into a Hashtable for processing
        String[] values = sReplace.split("@@");

        for (int j = 0; j < values.length; j++) {
            String[] keyval = values[j].split("\\|\\|");

            searchReplace.put(keyval[0], keyval[1]);
        }

        // first get the total number of children that we will be copying
        String sSQL = "select count(*) from node start with nodeid = " + sNodeID + " connect by prior nodeid = parentid";
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            Total = rs.getInt(1);
        } catch (Exception e) {
            api.Log.LogError("error in tssearchreplacenode, sql: " + sSQL, e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        if (!nodeExists(sNodeID, dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_SUCH_OBJECT);
        }
        synchronized (new Integer(sNodeID)) {
            if (sShowStatus != null) {
                searchReplace(sNodeID, searchReplace, sRecurse, replaceTitle, replaceSource, out, dbc);
            } else {
                searchReplace(sNodeID, searchReplace, sRecurse, replaceTitle, replaceSource, null, dbc);
            }
        }

        out.println("<SUCCESS>true</SUCCESS>");

        Count = 0;
        Total = 0;
    }

    public static boolean nodeExists(String NodeID, Connection dbc) throws Exception {
        String sSQL = "select nodeid from node where nodeid = " + NodeID;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            int loop = 0;

            while (rs.next()) {
                loop++;
            }

            if (loop == 0) {
                return false;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        return true;
    }

    public static void replaceSource(String NodeID, Hashtable searchReplace, Connection dbc)
            throws TSException {
        // get the node source
        String sNodeSource = "";
        try {
            sNodeSource = UtilClob.getClobStringFromNodeData(new Long(NodeID).longValue(), dbc);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return;
        } // ignore an error here, return empty string

        Enumeration eH = searchReplace.keys();
        while (eH.hasMoreElements()) {
            String key = (String) eH.nextElement();
            String val = (String) searchReplace.get(key);

            sNodeSource = sNodeSource.replaceAll(key, val);
        }

        try {
            UtilClob.insertClobToNodeData(new Long(NodeID).longValue(), sNodeSource, dbc);
        } catch (Exception e) {
            api.Log.LogError(e);
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
        }

    }

    public static void replaceTitle(String NodeID, Hashtable searchReplace, Connection dbc)
            throws SQLException {
        Node n = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            String sSQL = Node.getSQL(dbc) + " where NodeID = " + NodeID;
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            rs.next();
            n = new Node(rs);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return;  // ignore an error here, return empty string
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        Enumeration eH = searchReplace.keys();
        while (eH.hasMoreElements()) {
            String key = (String) eH.nextElement();
            String val = (String) searchReplace.get(key);

            n.set("NODETITLE", n.get("NODETITLE").replaceAll(key, val));
        }

        try {
            String sSQL = "update node set nodetitle = '" + n.get("NODETITLE") + "', dateupdated = sysdate where NodeID = " + NodeID;
            stmt = dbc.createStatement();
            if (stmt.executeUpdate(sSQL) == 0) {
                Log.Log("Warning: Could not update node title " + n.get("NODETITLE") + " for node ID " + NodeID);
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);  // ignore an error here, return empty string
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

    }

    public static void searchReplace(String NodeID, Hashtable searchReplace, String Recurse,
            String replaceTitle, String replaceSource, PrintWriter out,
            Connection dbc)
            throws Exception {

        DecimalFormat twoDigits = new DecimalFormat("0");

        if (replaceSource != null) {
            replaceSource(NodeID, searchReplace, dbc);
        }
        if (replaceTitle != null) {
            replaceTitle(NodeID, searchReplace, dbc);
        }

        long percent = ((Count + 1) * 100) / Total;
        if (out != null) {
            out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
            out.flush();
        }
        Count++;

        // now look for children and replace them, if applicable
        if (Recurse != null) {
            Vector vChildren = new Vector();
            String sSQL = "select nodeid from node where parentid = " + NodeID;

            Statement stmt = null;
            ResultSet rs = null;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    vChildren.add(rs.getString(1));
                }

            } catch (Exception e) {
                throw e;
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            //api.Log.Log(vChildren.size()+" children found, invoking copyNodeRecursive.");
            for (int i = 0; i < vChildren.size(); i++) {
                searchReplace((String) vChildren.elementAt(i), searchReplace, "true", replaceTitle, replaceSource, out, dbc);
            }
        }
    }
}
