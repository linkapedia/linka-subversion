package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.tsnode.musthavegate.components.OutputFormatXml;
import api.tsnode.musthavegate.components.TSMusthaveFirstPassController;
import api.tsnode.musthavegate.components.TSMusthavesBO;
import api.util.NodeUtils;
import com.iw.system.MustHaveGateReport;
import java.io.PrintWriter;
import java.security.InvalidParameterException;
import java.sql.Connection;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * this class is for apply the logic for the musthaves first pass
 *
 * use inherint parent musthaves use parent musthave use taxonomy musthaves
 * (current musthave in the system)
 *
 * @author andres
 */
public class TSMusthaveFirstPass {
    
    private static final Logger log = Logger.getLogger(TSsaveImagesS3.class);
    
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {
        
        log.debug("TSMusthaveFirstPass");
        //options from the gui

        TSMusthaveFirstPassController controller = new TSMusthaveFirstPassController();
        Map<String, String> parameters;
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        
        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        try {
            parameters = controller.parseParameters(props);
        } catch (InvalidParameterException e) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS, e.getMessage());
        }
        //add aditional parameters
        parameters.put("nodeid", nodeid);
        TSMusthavesBO musthaveBo = new TSMusthavesBO(parameters);
        MustHaveGateReport result = null;
        if (parameters.get("option1").equals("1")) {
            result = musthaveBo.updateWithDirectParent();
        } else if (parameters.get("option1").equals("2")) {
            result = musthaveBo.updateWithSpecificParent();
        } else if (parameters.get("option1").equals("3")) {
            result = musthaveBo.updateToTaxonomyMusthave();
        } else if (parameters.get("option1").equals("4")) {
            result = musthaveBo.updateToUserTyped();
        }
        
        OutputFormatXml outXml = new OutputFormatXml();
        out.println(outXml.getData(result));
        
        log.info("FINISHED TSMusthaveFirstPass");
    }
}
