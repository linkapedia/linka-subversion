package api.tsnode;


import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;
import com.indraweb.database.JDBCIndra_Connection;
import com.iw.classification.Data_WordsToNodes;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 * This function is used to alter the properties of a given node.  In addition to altering basic node properties,
 *  this function should be used to alter a node parent or change a node position in a hierarchy.  If the corpus
 *  attribute of a node is altered, all node descendents will automatically be changed.  This is done to
 *  preserve data integrity.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  NodeID  Unique node identifier of the node to be altered.
 *	@param  CorpusID (optional) Unique corpus identifier indicating this node's taxonomy.
 *  @param  ParentID (optional) Unique node identifier of this node's parent topic.
 *  @param  NodeTitle (optonal) Title of this node.
 *  @param  NodeSize (optional) Size of the original corpus source for this node.
 *  @param  NodeDesc (optional) Description for this node, no more than 4000 characters.
 *  @param  NodeIndexWithinParent (optional)    Position of this node within its parent.  If not specified, node will be placed at the end.
 *  @param  DepthFromRoot (optional)    Number of node parents in the ancestor tree to the root node of this corpus.
 *  @param  DateScanned (optional)  Date of last scan by a system crawler.  This field is only used in systems using the ITS harvester.
 *  @param  DateUpdated (optional)  Date of last update by a system crawler.  This field is only used in systems using the ITS harvester.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSEditNodeProps&NodeID=2000&NodeTitle=hkon&SKEY=-100299411

 *	@return	SUCCESS tag, if successful, along with the new node identifier.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>edit update operation complete</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditNodeProps
{
	// TSEditCorpusProps (sessionid, userid, email, password, firstname, lastname, emailstatus, scorethreshold, resultsperpage, userstatus)
	// Note: All parameters except sessionid and userid are OPTIONAL
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// NOTE : required fields MUST appear first in the list above.
		String sTableName = "node";
		int iNumRequiredFields = 1; 
		String[] sArrFields_URLNameSpace = {
			"NodeID",
			"CorpusID",
			"NodeTitle",
			"NodeSize",
			"ParentID",
			"NodeIndexWithinParent",
			"DepthFromRoot",
			"DateScanned",
			"DateUpdated",
			"NodeStatus",
			"NodeLastSync",
                        "NodeDesc",
                        "BRIDGENODE",
                        "LISTNODE",
                        "NODETYPE",
                        "NODEMUSTHAVEGATEINHERITANCE"
                        
		};

		props.put("NodeLastSync", "SYSDATE");
        props.put("DateUpdated", "SYSDATE");

		String[] sArrFields_DBNameSpace = sArrFields_URLNameSpace;
		String sNodeID = (String) props.get("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
			
			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // purge signature cache by corpus
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus
                    ("TSEditNodeProps", iCorpusId);

            // if node index within parent was included, increment siblings
            String sProp_NODEINDEXWITHINPARENT = (String) props.get ("NodeIndexWithinParent");
            String sProp_ParentID = (String) props.get ("ParentID");
            String sProp_CorpusID = (String) props.get ("CorpusID");

            // if parent and NIWP are changing, get the max and store in variable
            if ((sProp_ParentID != null) && (sProp_NODEINDEXWITHINPARENT != null)) {
                if (sProp_NODEINDEXWITHINPARENT.equals("end")) {
                    String sSQLGetMaxNIWP =
                            "select max(NodeIndexWithinParent) from node where parentid = " + sProp_ParentID;
                    sProp_NODEINDEXWITHINPARENT = ""+
                            (1+ (int) JDBCIndra_Connection.executeQueryAgainstDBLong (sSQLGetMaxNIWP, dbc));
                    props.put("NodeIndexWithinParent", sProp_NODEINDEXWITHINPARENT);
                }
            }
            /* decided this rule is not just
            if (isLink(sNodeID, dbc)) {
                out.println("<ERROR>Links may not moved.</ERROR>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_DATA_INTEGRITY_VIOLATION);
            } */

            // if parentID is being changed, ensure the parent is not a link and that the node is not
            // being put underneath one of his own descendents
            if (sProp_ParentID != null) {
                if (isLink(sProp_ParentID, dbc)) {
                    out.println("<DATAERROR>Links may not have any direct descendents.</DATAERROR>");
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_DATA_INTEGRITY_VIOLATION);
                }

                String sSQL2 = "select nodeid from node start with nodeid = "+sNodeID+" connect by prior nodeid = parentid";
                stmt = null; rs = null;
                try {
                    stmt = dbc.createStatement();
                    rs = stmt.executeQuery (sSQL2);

                    while (rs.next()) {
                        String sDescendentID = rs.getString(1);
                        if (sDescendentID.equals(sProp_ParentID)) {
                            throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_DATA_INTEGRITY_VIOLATION);
                        }
                    }

                } catch (Exception e) {
                    throw new TSException (	EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION,
                        "e.msg ["	+ e.getMessage() + "] " +
                        " SQL ["	+ sSQL.toString() + "] " +
                        " e.stk ["	+ Log.stackTraceToString(e) + "]" );
                } finally {
                    if (rs != null) { rs.close(); rs = null; }
                    if (stmt != null) { stmt.close(); stmt = null; }
                }
            }

            // if CORPUSID was changed, we must changed it recursively for all children
            // in order to preserve our data integrity
            if (sProp_CorpusID != null) {
                setCorporaRecursively(sNodeID, sProp_CorpusID, dbc);
                // purge signature cache by corpus
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus
                        ("TSEditNodeProps (node corpus changed)", Integer.parseInt(sProp_CorpusID));

            }

			sSQL = api.sqlinteract.SQLModelUpdate.genSQLUpdate ( sTableName,
														  props,
														  sArrFields_URLNameSpace,
														  sArrFields_DBNameSpace,
														  iNumRequiredFields
															);

			out.flush();
			stmt = null;

			try
			{
				stmt = dbc.createStatement();

				// If query statement failed, throw an exception
				if (stmt.executeUpdate (sSQL) == 0) {
					out.println ( "<DEBUG>The specified record does not exist to update</DEBUG>");
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND);
				}
			}
			catch ( Exception e )
			{
				throw new TSException (	EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION,
					"e.msg ["	+ e.getMessage() + "] " +
					" SQL ["	+ sSQL.toString() + "] " +
					" e.stk ["	+ Log.stackTraceToString(e) + "]" );
			}
			finally
			{
				out.println (EmitValue.genValue ("SUCCESS" , "edit update operation complete"  ));
				stmt.close();
			}
		}
		// Catch exception
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}

    // return whether or not this node is actually a link
    public static boolean isLink (String sNodeID, Connection dbc) throws Exception {
        Statement stmt = null; ResultSet rs = null;
        String sSQL = "select linknodeid from node where nodeid = "+sNodeID;

        try {
        	stmt = dbc.createStatement(); rs = stmt.executeQuery (sSQL);

            while (rs.next()) { if (sNodeID.equals(rs.getString(1))) { return false; }}

        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
        return true;
    }

    public static String getParent (String sNodeID, Connection dbc) throws Exception {
        Statement stmt = null; ResultSet rs = null;
        String sSQL = "select parentid from node where nodeid = "+sNodeID;

        try {
        	stmt = dbc.createStatement(); rs = stmt.executeQuery (sSQL);

            while (rs.next()) { return rs.getString(1); }

        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
        return "-1";
    }

    // preserve data integrity
    public static void setCorporaRecursively (String sNodeID, String sCorpusID, Connection dbc)
    throws Exception {
        String sSQL = "select nodeid from node start with parentid = "+sNodeID+" connect by prior nodeid = parentid";
        Statement stmt = null; 
        ResultSet rs = null; 
        Vector vNodes = new Vector();

        try {
        	stmt = dbc.createStatement(); rs = stmt.executeQuery (sSQL);

            while (rs.next()) { vNodes.add(rs.getString(1)); }
        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }

        // return if empty
        if (vNodes.isEmpty()) return;

        sSQL = "update node set corpusid = "+sCorpusID+" where nodeid IN ("+sNodeID;
        for (int i = 0; i < vNodes.size(); i++) { sSQL = sSQL + ","+(String)vNodes.elementAt(i); }
        sSQL = sSQL + ")";

        try {
            stmt = dbc.createStatement();
            if (stmt.executeUpdate (sSQL) == 0) {
                throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_NODES_THIS_PARENT_NODE);
            }
        } catch (Exception e) { throw e; }
        finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }
}
