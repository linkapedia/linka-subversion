package api.tsnode.musthavegate.components;

import com.iw.system.ListMusthaveGateReport;
import com.iw.system.ModelMusthaveGateReport;
import com.iw.system.MustHaveGateReport;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * logic by each condition
 *
 * @author andres
 */
public class TSMusthavesBO {

    Map<String, String> parameters;
    TSMusthaveGateDAO dao;
    private static final Logger log = Logger.getLogger(TSMusthavesBO.class);

    public TSMusthavesBO(Map<String, String> paremeters) {
        this.parameters = paremeters;
        dao = new TSMusthaveGateDAO();
    }

    /**
     * get TreeNodes from oracle
     *
     * @return
     * @throws TSMustHaveGateExceptionDAO
     */
    private List<TreeNode> getTreeNodes() throws TSMustHaveGateExceptionDAO {
        List<TreeNode> nodes = null;
        try {
            nodes = dao.getTreeNodes(parameters.get("nodeid"));
        } catch (SQLException e) {
            throw new TSMustHaveGateExceptionDAO("SQLException", e);
        }
        return nodes;
    }

    /**
     * update the nodes with the specific parameter
     *
     * @param nodes
     * @param musthaveGateInherance
     * @throws TSMustHaveGateExceptionDAO
     */
    private void updateTreeNodes(List<TreeNode> nodes, Integer musthaveGateInherance) throws TSMustHaveGateExceptionDAO {
        try {
            dao.updateNodeMusthaveGateInheritance(nodes, musthaveGateInherance);
        } catch (SQLException e) {
            throw new TSMustHaveGateExceptionDAO("SQLException", e);
        }
    }

    /**
     * update the nodes with +1 in the field NODEMUSTHAVEGATEINHERITANCE
     *
     * @return
     */
    public MustHaveGateReport updateWithDirectParent() {
        List<TreeNode> list = null;
        MustHaveGateReport result = new MustHaveGateReport();
        try {
            list = getTreeNodes();
        } catch (TSMustHaveGateExceptionDAO ex) {
            log.error("Error getting the nodes to work", ex);
            return result;
        }
        //build the tree
        BuilderTree builder = new BuilderTree(list);
        builder.build();
        int fromLevelInt = Integer.parseInt(parameters.get("fromlevel"));
        int toLevelInt = Integer.parseInt(parameters.get("tolevel"));
        list = builder.getTreeNodes(fromLevelInt, toLevelInt);
        try {
            updateTreeNodes(list, 1);
            List<ListMusthaveGateReport> resultList = new ArrayList<ListMusthaveGateReport>();
            resultList.add(getMusthaveReportObject(list, 1));
            result.setData(resultList);
        } catch (TSMustHaveGateExceptionDAO ex) {
            log.error("Error saving the nodes", ex);
        }
        return result;
    }

    /**
     * update the nodes with (SelectedLevel - level) in the field
     * NODEMUSTHAVEGATEINHERITANCE granparent = +2 parent = +1
     *
     * @return
     */
    public MustHaveGateReport updateWithSpecificParent() {
        List<TreeNode> list = null;
        MustHaveGateReport result = new MustHaveGateReport();
        try {
            list = getTreeNodes();
        } catch (TSMustHaveGateExceptionDAO ex) {
            log.error("Error getting the nodes to work", ex);
            return result;
        }
        //build the tree
        BuilderTree builder = new BuilderTree(list);
        builder.build();
        //logic
        int fromLevelInt = Integer.parseInt(parameters.get("fromlevel"));
        int toLevelInt = Integer.parseInt(parameters.get("tolevel"));
        int levelParentInt = Integer.parseInt(parameters.get("level"));
        List<ListMusthaveGateReport> resultList = new ArrayList<ListMusthaveGateReport>();
        int i = fromLevelInt;
        while (i <= toLevelInt || toLevelInt == -1) {
            list = builder.getTreeNodes(i);
            if (list == null || list.isEmpty()) {
                //NOT MORE DATA
                break;
            }
            int valueToUpdated = i - levelParentInt;
            try {
                updateTreeNodes(list, valueToUpdated);
            } catch (TSMustHaveGateExceptionDAO ex) {
                log.error("Error saving the nodes", ex);
                continue;
            }
            resultList.add(getMusthaveReportObject(list, valueToUpdated));
            i++;
        }
        result.setData(resultList);
        return result;
    }

    /**
     * update the nodes with (null) in the field NODEMUSTHAVEGATEINHERITANCE
     *
     * @return
     */
    public MustHaveGateReport updateToTaxonomyMusthave() {
        List<TreeNode> list = null;
        MustHaveGateReport result = new MustHaveGateReport();
        try {
            list = getTreeNodes();
        } catch (TSMustHaveGateExceptionDAO ex) {
            log.error("Error getting the nodes to work", ex);
            return result;
        }
        //build the tree
        BuilderTree builder = new BuilderTree(list);
        builder.build();
        int fromLevelInt = Integer.parseInt(parameters.get("fromlevel"));
        int toLevelInt = Integer.parseInt(parameters.get("tolevel"));
        list = builder.getTreeNodes(fromLevelInt, toLevelInt);
        try {
            updateTreeNodes(list, null);
            List<ListMusthaveGateReport> resultList = new ArrayList<ListMusthaveGateReport>();
            resultList.add(getMusthaveReportObject(list, null));
            result.setData(resultList);
        } catch (TSMustHaveGateExceptionDAO ex) {
            log.error("Error saving the nodes", ex);
        }
        return result;
    }

    /**
     * update the nodes with 0 in the field NODEMUSTHAVEGATEINHERITANCE
     *
     * @return
     */
    public MustHaveGateReport updateToUserTyped() {
        List<TreeNode> list = null;
        MustHaveGateReport result = new MustHaveGateReport();
        try {
            list = getTreeNodes();
        } catch (TSMustHaveGateExceptionDAO ex) {
            log.error("Error getting the nodes to work", ex);
            return result;
        }
        //build the tree
        BuilderTree builder = new BuilderTree(list);
        builder.build();
        int fromLevelInt = Integer.parseInt(parameters.get("fromlevel"));
        int toLevelInt = Integer.parseInt(parameters.get("tolevel"));
        String typedByUser = parameters.get("textbyuser");
        list = builder.getTreeNodes(fromLevelInt, toLevelInt);
        try {
            updateTreeNodes(list, 0);
            try {
                dao.insertMusthaveGate(list, typedByUser);
            } catch (SQLException e) {
                throw new TSMustHaveGateExceptionDAO("SQLException", e);
            }
            List<ListMusthaveGateReport> resultList = new ArrayList<ListMusthaveGateReport>();
            resultList.add(getMusthaveReportObject(list, 0));
            result.setData(resultList);
        } catch (TSMustHaveGateExceptionDAO ex) {
            log.error("Error saving the nodes", ex);
        }
        return result;
    }

    /**
     * get objet to show in the report
     *
     * @param nodes
     * @param data
     * @return
     */
    private ListMusthaveGateReport getMusthaveReportObject(List<TreeNode> nodes, Integer data) {
        ListMusthaveGateReport listResult = new ListMusthaveGateReport();
        List<ModelMusthaveGateReport> list = new ArrayList<ModelMusthaveGateReport>();
        ModelMusthaveGateReport model = null;
        for (TreeNode node : nodes) {
            model = new ModelMusthaveGateReport();
            model.setNodeId(node.getNodeInfo().getId());
            model.setNodeTitle(node.getNodeInfo().getTitle());
            list.add(model);
        }
        listResult.setValueUpdated(data);
        listResult.setList(list);
        return listResult;
    }
}
