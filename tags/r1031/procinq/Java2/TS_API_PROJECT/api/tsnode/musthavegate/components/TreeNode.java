package api.tsnode.musthavegate.components;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author andres
 */
public class TreeNode {

    private List<TreeNode> children;
    private ModelTreeNode nodeInfo;
    private boolean root;

    public TreeNode(ModelTreeNode nodeInfo, boolean isRoot) {
        this.nodeInfo = nodeInfo;
        root = isRoot;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public ModelTreeNode getNodeInfo() {
        return nodeInfo;
    }

    public void setNodeInfo(ModelTreeNode nodeInfo) {
        this.nodeInfo = nodeInfo;
    }

    public boolean isRoot() {
        return root;
    }

    public void printTree() {
        printTree("", true);
    }

    public void printTree(String prefix, boolean isTail) {
        System.out.println(prefix + (isTail ? "|--" : "|-- ") + this.nodeInfo.getTitle());
        for (Iterator<TreeNode> iterator = this.children.iterator(); iterator.hasNext();) {
            iterator.next().printTree(prefix + (isTail ? "   " : " |  "), !iterator.hasNext());
        }
    }
}
