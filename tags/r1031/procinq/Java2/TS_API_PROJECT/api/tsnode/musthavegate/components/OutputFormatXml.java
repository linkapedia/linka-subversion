package api.tsnode.musthavegate.components;

import api.report.OutputFormatException;
import com.iw.system.MustHaveGateReport;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author andres
 */
public class OutputFormatXml implements OutputFormat {

    public String getData(MustHaveGateReport data) throws OutputFormatException {
        JAXBContext jc;
        StringWriter writer = new StringWriter();
        try {
            jc = JAXBContext.newInstance(MustHaveGateReport.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.marshal(data, writer);
        } catch (JAXBException ex) {
            throw new OutputFormatException("Error parsing xml data", ex.getCause());
        }
        return writer.toString();
    }
}
