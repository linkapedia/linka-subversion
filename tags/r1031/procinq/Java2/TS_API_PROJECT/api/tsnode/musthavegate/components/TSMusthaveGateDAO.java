package api.tsnode.musthavegate.components;

import api.util.NodeUtils;
import com.iw.db.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * comunication with oracle
 *
 * @author andres
 */
public class TSMusthaveGateDAO {

    private static final Logger log = Logger.getLogger(NodeUtils.class);
    private static final Integer BATCH_UPDATER = 100;

    /**
     * get the nodes in a good structure
     *
     * @param queryNodeId
     * @return
     * @throws SQLException
     */
    public List<TreeNode> getTreeNodes(String queryNodeId) throws SQLException {
        log.debug("TSMusthaveGateDAO: getTreeNodesAsTree");
        List<TreeNode> list = new ArrayList<TreeNode>();
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("TSMusthaveGateDAO: Error getting connection from oracle");
        }
        String sql = "SELECT NODEID, NODETITLE, PARENTID FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, queryNodeId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String nodeid = String.valueOf(rs.getInt("nodeid"));
                String nodetitle = rs.getString("nodetitle");
                String parentid = rs.getString("parentid");
                ModelTreeNode modelTreeNode;
                modelTreeNode = new ModelTreeNode();
                modelTreeNode.setId(nodeid);
                modelTreeNode.setTitle(nodetitle);
                modelTreeNode.setParentid(parentid);
                TreeNode treeNode;
                //look the rootNode
                if (queryNodeId.equals(nodeid)) {
                    treeNode = new TreeNode(modelTreeNode, true);
                } else {
                    treeNode = new TreeNode(modelTreeNode, false);
                }
                list.add(treeNode);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * updating the nodes in batch
     *
     * @param nodes
     * @param musthaveGateInheritance
     * @throws SQLException
     */
    public void updateNodeMusthaveGateInheritance(List<TreeNode> nodes, Integer musthaveGateInheritance) throws SQLException {
        log.debug("TSMusthaveGateDAO: updateNodeMusthaveGate");
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("TSMusthaveGateDAO: Error getting connection from oracle");
        }
        String sql = "UPDATE SBOOKS.NODE SET nodemusthavegateinheritance = ? where nodeid = ?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        conn.setAutoCommit(false);
        try {
            pstmt = conn.prepareStatement(sql);
            int cont = 0;
            for (TreeNode node : nodes) {
                cont++;
                if (musthaveGateInheritance == null) {
                    pstmt.setNull(1, Types.INTEGER);
                } else {
                    pstmt.setInt(1, musthaveGateInheritance);
                }
                pstmt.setInt(2, Integer.parseInt(node.getNodeInfo().getId()));
                pstmt.addBatch();
                if (cont == BATCH_UPDATER) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            //the last data
            if (cont != 0) {
                //update the batch
                pstmt.executeBatch();
                pstmt.clearBatch();
            }
            conn.commit();
        } catch (Exception e) {
            log.error("Error updating nodes");
            conn.rollback();
            throw new SQLException("Error updating nodes", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * insert data into musthave gate
     *
     * @param nodes
     * @param textByUser
     * @throws SQLException
     */
    public void insertMusthaveGate(List<TreeNode> nodes, String textByUser) throws SQLException {
        log.debug("TSMusthaveGateDAO: insertMusthaveGate");
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("TSMusthaveGateDAO: Error getting connection from oracle");
        }
        String sql = "insert into NODEMUSTHAVEGATE (NodeId, MustWord, Lang) values (?, ?, ?)";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        conn.setAutoCommit(false);
        try {
            pstmt = conn.prepareStatement(sql);
            int cont = 0;
            for (TreeNode node : nodes) {
                cont++;
                pstmt.setInt(1, Integer.parseInt(node.getNodeInfo().getId()));
                pstmt.setString(2, textByUser);
                pstmt.setString(3, "EN");
                pstmt.addBatch();
                if (cont == BATCH_UPDATER) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            //the last data
            if (cont != 0) {
                //update the batch
                pstmt.executeBatch();
                pstmt.clearBatch();
            }
            conn.commit();
        } catch (Exception e) {
            log.error("Error updating nodes");
            conn.rollback();
            throw new SQLException("Error updating nodes", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
