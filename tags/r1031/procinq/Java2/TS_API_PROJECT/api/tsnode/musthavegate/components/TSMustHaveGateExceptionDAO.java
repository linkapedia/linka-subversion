package api.tsnode.musthavegate.components;

/**
 *
 * @author andres
 */
public class TSMustHaveGateExceptionDAO extends Exception {

    public TSMustHaveGateExceptionDAO() {
    }

    public TSMustHaveGateExceptionDAO(String message) {
        super(message);
    }

    public TSMustHaveGateExceptionDAO(Throwable cause) {
        super(cause);
    }

    public TSMustHaveGateExceptionDAO(String message, Throwable cause) {
        super(message, cause);
    }
}
