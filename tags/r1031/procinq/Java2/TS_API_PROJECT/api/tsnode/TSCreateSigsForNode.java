package api.tsnode;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.encyclopedia.DocForScore;
import com.indraweb.execution.Session;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.PhraseParms;
import com.indraweb.ir.clsStemAndStopList;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.util.UtilRandom;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.system.User;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Create signatures for a single node
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param NODEID Node to add signature terms to, and determines the corpus to run against
 * @param VERBOSE (optional) Default is false. True indicates to output debugging data to system.out
 * @param PARSEPARMS* (optional) ParseParms for document parse.
 * @param PHRASEPARMS* (optional) ParseParms for phrase extract as signature terms.
 * @param SIGNATUREPARMS* (optional) ParseParms for phrase extract as signature terms.
 *
 *
 * @return XML stream of terms and counts if successful. \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <TERMCOUNT> <TERM>uranium</TERM> <COUNT>83</COUNT> </TERMCOUNT> <TERMCOUNT>
 * <TERM>du</TERM> <COUNT>57</COUNT> </TERMCOUNT> <TERMCOUNT> <TERM>depleted</TERM> <COUNT>55</COUNT> </TERMCOUNT> ... \endverbatim
 */
public class TSCreateSigsForNode {

    public static void handleTSapiRequest(HttpServletRequest req, HttpServletResponse res, api.APIProps props, PrintWriter out, Connection dbc) throws Exception {

        File tempFile = null;
        try {
            ParseParms parseparms = new ParseParms(props);
            PhraseParms phraseparms = new PhraseParms(props);
            SignatureParms sigparms = new SignatureParms(props);

            String sKey = (String) props.get("SKEY", true);

            String sNodeID = (String) props.get("NODEID", true);

            String sSQLGetNodeTitle = "select nodetitle from node where nodeid = " + sNodeID;
            String sNodeTitle = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLGetNodeTitle, dbc);

            String sData = "";
            try {
                sData = UtilClob.getClobStringFromNodeData(new Long(sNodeID).longValue(), dbc);
            } catch (Exception e) {
                e.printStackTrace(System.out);
            } // ignore an error here, return empty string

            // if classify text was streamed in, add HTML headers (until we can eliminate the parser)
            sData = "<HTML>\n<HEAD><TITLE>" + sNodeTitle + "</TITLE>\n</HEAD>"
                    + "\n<BODY>\n" + sData + "\n</BODY>\n</HTML>";

            tempFile = getTempFile("C:\\temp\\", "html");
            PrintWriter tout = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), true));
            tout.println(sData);
            tout.close();

            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            if (u == null || !u.IsMember(out)) {
                Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                Log.LogError("in TSCreateSigsFromDoc", e);
                throw e;
            }

            DocForScore docForScore = new DocForScore(
                    tempFile.getAbsolutePath(),
                    tempFile.getAbsolutePath(),
                    true,
                    false, // no stem needed for sig inserts to db
                    sNodeTitle,
                    null,
                    true,
                    null,
                    true,
                    true,
                    1,
                    parseparms,
                    sData);

            int iSigGenRangeSize = props.getint("SigGenRangeSize", "" + SignatureParms.getSigGenRangeSize_Default());

            //restricted to sig table as well String sSQL = "select n.nodeid from node n, signature s where n.nodeid = s.nodeid and corpusid = " + iProp_CORPUSID +
            //        " and n.nodeid in ( select nodeid from signature ) order by nodeid ";
            // delete old sigs
            String sSQLdelsigs = "delete from signature where nodeid = " + sNodeID;
            JDBCIndra_Connection.executeUpdateToDB(sSQLdelsigs, dbc, false, -1);

            String sSQLGetCorpusThisNode = "select corpusid from node where nodeid = " + sNodeID;
            int iCorpusID = (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLGetCorpusThisNode, dbc);
            String sSQL = "select n.nodeid from node n where corpusid = " + iCorpusID;
            //System.out.println("sSQL [" + sSQL+"]");
            Vector vINodeIDsThisCorpus = JDBCIndra_Connection.executeQueryAgainstDBVector(sSQL, dbc);
            Vector vNodeIDsRandom = UtilRandom.randomPermuteVector(vINodeIDsThisCorpus, 0);

            Object[] oArrNodeIDsRandom = convertVectorTo_ObjArrayCapped(vNodeIDsRandom, iSigGenRangeSize);
            String sNodeIDList = com.indraweb.util.UtilSets.convertObjectArrayToStringWithDelims(oArrNodeIDsRandom, ",");

            String sSQLGetSigs = "select signatureword, signatureoccurences, nodeid from signature "
                    + " where nodeid in (" + sNodeIDList + ") ";

            Hashtable htDocForScoreWordsNCountINOUT = docForScore.getHtWordsNCount(Session.cfg.getPropBool("StemOnClassifyNodeDocScoring"));
            //System.out.println("1 htDocForScoreWordsNCountINOUT.size() [" + htDocForScoreWordsNCountINOUT.size()+"]");

            if (docForScore == null) {
                api.Log.Log("docForScore is null.");
            }
            if (docForScore.getTopic() == null) {
                api.Log.Log("docForScore.getTopic() is null.");
            }
            if (phraseparms == null) {
                api.Log.Log("phraseparms is null.");
            }

            docForScore.getTopic().addPhrasesToCounts(phraseparms);

            // BIG CALL OUT - REMOVE SIGS THAT THIS DOC DOES NOT HAVE HIGHER THAN NEIGHBORS

            filterOutTermsOccuringMoreFreqInExistingSigs(sSQLGetSigs,
                    htDocForScoreWordsNCountINOUT,
                    sigparms.dSigTieDifferenceEpsilonPercent,
                    dbc);
            //System.out.println("2 htDocForScoreWordsNCountINOUT.size() [" + htDocForScoreWordsNCountINOUT.size()+"]");

            Hashtable htStop = clsStemAndStopList.getHTStopWordListStemmed();
            Enumeration e = htDocForScoreWordsNCountINOUT.keys();
            Vector vSigResults = new Vector();
            int i = 0;
            while (e.hasMoreElements()) {
                String sTerm = (String) e.nextElement();
                sTerm = sigparms.signatureTermQualifierModifier(sTerm);
                if (sTerm != null && htStop.get(sTerm) == null) {
                    Integer ICount = (Integer) htDocForScoreWordsNCountINOUT.get(sTerm);
                    SigResult sigresult = new SigResult(sTerm, ICount.intValue(), htDocForScoreWordsNCountINOUT.size());
                    //api.Log.Log (i + ". adding final term [" + sTerm + "] count [" + ICount.intValue() + "]" );
                    vSigResults.addElement(sigresult);
                }
                i++;
            }

            // sort and truncate
            java.util.Collections.sort(vSigResults, new SigResultComparator());

            // by now it's sorted and truncated - emit XML
            Enumeration e2 = vSigResults.elements();
            int iIndex = 0;
            StringBuffer sbAddNodeTermCount = null;
            sbAddNodeTermCount = new StringBuffer();
            Vector vFieldNames = new Vector();
            vFieldNames.addElement("NODEID");
            vFieldNames.addElement("SIGNATUREWORD");
            vFieldNames.addElement("SIGNATUREOCCURENCES");

            Hashtable htStopTerms = null;
            htStopTerms = clsStemAndStopList.getHTStopWordList();

            while (e2.hasMoreElements()) {
                // exit when have enough sig terms for this new one
                if (sigparms.iMaxSigTermsPerNode > 0 && iIndex == sigparms.iMaxSigTermsPerNode) {
                    break;
                }

                SigResult sigresult = (SigResult) e2.nextElement();
                String sTerm = sigresult.sTerm.trim();
                // if not a stop term - with stem on or off ...
                if (htStopTerms.get(sTerm) == null) {

                    out.println("<TERMCOUNT>");
                    //out.println(" <INDEX>" + iIndex + "</INDEX>");
                    out.println(" <TERM>" + sTerm + "</TERM>");
                    out.println(" <COUNT>" + sigresult.iCount + "</COUNT>");

                    Statement stmt = null;
                    sSQL = " insert into Signature (NodeId, SignatureWord, SignatureOccurences) "
                            + " values (" + sNodeID + ",'" + sTerm + "'," + sigresult.iCount + ")";
                    try {
                        stmt = dbc.createStatement();

                        // If query statement failed, throw an exception
                        if (stmt.executeUpdate(sSQL) == 0) {
                            out.println("<DEBUG>Failed to insert: " + sNodeID + " " + sTerm + " " + sigresult.iCount + " </DEBUG>");
                        }
                    } catch (Exception ez) {
                        out.println("<DEBUG>Failed to insert: " + sNodeID + " " + sTerm + " " + sigresult.iCount + " </DEBUG>");
                        //throw e;
                    } finally {
                        if (stmt != null) {
                            stmt.close();
                            stmt = null;
                        }
                    }

                }
                out.println("</TERMCOUNT>");
                iIndex++;
            } // for each sig term

            String sSQLUpdateNodeSize = "update node set NodeSize = " + docForScore.getSCarr(false).length
                    + ", dateupdated = sysdate where nodeid = " + sNodeID;
            JDBCIndra_Connection.executeUpdateToDB(sSQLUpdateNodeSize, dbc, false, -1);

            out.flush();
            dbc.commit();
            out.println("<SUCCESS>" + "</SUCCESS>");


        } catch (Exception eTopLevel) {
            //String stack = Log.stackTraceToString (e21);
            Log.LogError("in TSCreateSigsFromDoc", eTopLevel);
            EmitGenXML_ErrorInfo.emitException("TSException", eTopLevel, out);
        } finally {
            tempFile.delete();
        }
    } // public static void handleTSapiRequest ( HttpServletRequest req, api.APIProps props, PrintWriter out, Connection dbc )

    public static File getTempFile(String sPath, String sExtension) throws Exception {
        Random wheel = new Random(); // seeded from the clock
        File tempFile = null;

        do {
            // generate random a number 10,000,000 .. 99,999,999
            int unique = (wheel.nextInt() & Integer.MAX_VALUE) % 90000000 + 10000000;
            String sFirstCharacter = (unique + "").substring(0, 1);
            String sSecondCharacter = (unique + "").substring(1, 2);

            tempFile = new File(sPath + sFirstCharacter + "/" + sSecondCharacter + "/", Integer.toString(unique) + "." + sExtension);
        } while (tempFile.exists());

        // We "finally" found a name not already used. Nearly always the first time.
        // Quickly stake our claim to it by opening/closing it to create it.
        // In theory somebody could have grabbed it in that tiny window since
        // we checked if it exists, but that is highly unlikely.
        synchronized (tempFile) {
            try {
                new FileOutputStream(tempFile).close();
                //System.out.println("Temp file successfully created: "+tempFile.getAbsolutePath());
            } catch (Exception e) {
                System.out.println("Could not generate temp file.");
                //Runtime r = Runtime.getRuntime (); r.wait(1000); // pause a second
                //return getTempFile(sPath, sExtension);
            }
        }

        //System.out.println("Creating temporary file: "+tempFile.getAbsolutePath());
        return tempFile;
    }

    private static class SigResult {

        int iIndex = -1;
        String sTerm = null;
        int iNodeDocSize = -1;
        int iCount = -1;
        double dFreq = -1.0;

        public SigResult(String sTerm_, int iCount_, int iNodeDocSize) {

            sTerm = sTerm_;
            iCount = iCount_;
            dFreq = (double) ((double) iCount / (double) iNodeDocSize);
        }
    }

    private static class SigResultComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {

            SigResult sr1 = (SigResult) o1;
            SigResult sr2 = (SigResult) o2;
            if (sr1.dFreq < sr2.dFreq) {
                return 1;
            } else if (sr1.dFreq > sr2.dFreq) {
                return -1;
            } else {
                return sr1.sTerm.compareTo(sr2.sTerm);
            }
        }

        @Override
        public boolean equals(Object obj) {
            return false;
        }
    }

    private static void filterOutTermsOccuringMoreFreqInExistingSigs(String sSQLGetSigs,
            Hashtable htDocForScoreWordsNCountINOUT,
            double dSigTieDifferenceEpsilon,
            Connection dbc) throws Exception {


        //api.Log.Log ("in filterOutTermsOccuringMoreFreqInExistingSigs words pre [" + UtilSets.htToString(htDocForScoreWordsNCountINOUT, "\r\n")+ "]" );

        Hashtable htNodeIDToIntNodeSize = new Hashtable();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQLGetSigs);

            int iLoop = 0;
            while (rs.next()) {
                String sWord = rs.getString(1).toLowerCase();
                int iExistingSigsCount = rs.getInt(2);
                int iNodeID = rs.getInt(3);
                Integer INodeSize = (Integer) htNodeIDToIntNodeSize.get("" + iNodeID);
                if (INodeSize == null) {
                    String sSQLGetNodeSize = "select nodesize from node where nodeid = " + iNodeID;
                    int iNodeSize = (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLGetNodeSize, dbc);
                    INodeSize = new Integer(iNodeSize);
                    htNodeIDToIntNodeSize.put("" + iNodeID, INodeSize);
                }
                double dExistingSigsWordFreq = (double) iExistingSigsCount / INodeSize.doubleValue();
                Integer IDocCount = (Integer) htDocForScoreWordsNCountINOUT.get(sWord);
                if (IDocCount == null) {
                    IDocCount = new Integer(0);
                }
                //api.Log.Log (iLoop + ". doc size [" + (double) htDocForScoreWordsNCountINOUT.size() + "] pre node size [" + INodeSize+ "]" );
                //api.Log.Log (iLoop + ".doc count [" + IDocCount  + "] node count [" + iExistingSigsCount + "]" );
                //int iDocCount = com.indraweb.utils.sets.UtilSets.hash_get_count_for_string (htdocWordFreq_StrToDouble_INOUT , sWord.toLowerCase ());
                double dDocFreq = (double) (IDocCount.doubleValue() / (double) htDocForScoreWordsNCountINOUT.size());
                if (dDocFreq < (dExistingSigsWordFreq - (dSigTieDifferenceEpsilon / (double) 100))) {
                    htDocForScoreWordsNCountINOUT.remove(sWord.toLowerCase());
//                    api.Log.Log (iLoop + ". removing doc term [" + sWord.toLowerCase() +
//                            "] freq [" + dDocFreq + "] dExistingSigsWordFreq [" + dExistingSigsWordFreq + "]" );
                }
//                else
//                    api.Log.Log (iLoop + ". keeping doc term [" + sWord.toLowerCase() +
//                            "] freq [" + dDocFreq + "] dExistingSigsWordFreq [" + dExistingSigsWordFreq + "]" );

                iLoop++;
            }

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        //api.Log.Log ("after filterOutTermsOccuringMoreFreqInExistingSigs words pre [" + UtilSets.htToString(htDocForScoreWordsNCountINOUT, "\r\n")+ "]" );

    }

    // ***************************************************
    public static Object[] convertVectorTo_ObjArrayCapped(Vector v, int iCap) // ***************************************************
    {
        int iSize = iCap;
        if (v.size() < iCap) {
            iSize = v.size();
        }
        Object[] oArr = new Object[iSize];
        Enumeration e = v.elements();
        int i = 0;
        while (e.hasMoreElements() && i < iSize) {
            oArr[i] = e.nextElement();
            i++;
        }
        return oArr;
    }
}
