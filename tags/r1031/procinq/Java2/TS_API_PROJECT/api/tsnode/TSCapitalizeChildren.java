package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.iw.system.Node;
import com.indraweb.database.*;
import api.Log;

/**
 * Change the titles of all nodes underneath the given node to be lower case, with the first letter of each word
 *  capitalized.  If the "recursive" flag is set, all node descendents under the given node are capitalized recursively.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of this node.
 *  @param  NodeTitle The node title currently assigned to this node.
 *  @param  Recursive   If true, all node descendents will be organized.  Default is false.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSCapitalizeChildren&NodeID=2000&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Nodes capitalized successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSCapitalizeChildren {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sNodeTitle = (String) props.get("NodeTitle", true);
        String sRecursive = (String) props.get("recursive");
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            // for authorization purposes, need to know the corpus id of this node
            String sSQL = " select CorpusId from Node where NodeID = " + sNodeID;
            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);
            rs.next();

            int iCorpusId = rs.getInt(1);
            rs.close();
            stmt.close();

            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            boolean bRecurse = false;
            if (sRecursive != null) {
                bRecurse = true;
            }

            Node n = new Node(); n.set("NODEID", sNodeID); n.set("NODETITLE", sNodeTitle);
            capitalizeNode(n, dbc, bRecurse);
            out.println("<SUCCESS>Nodes capitalized successfully.</SUCCESS>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }

    public static void capitalizeNode(Node n, Connection dbc, boolean bRecurse)
            throws Exception {

        setNodeTitle(n, dbc);

        if (bRecurse) {
            String sSQL = Node.getSQL(dbc)+" where n.parentid = " + n.get("NODEID") +" order by lower(nodetitle) asc";
            Statement stmt = null;
            ResultSet rs = null;

            Vector vChildrenToCapitalize = new Vector();

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery (sSQL);
                int i = 0;

                while (rs.next()) {
                    i++; Node n2 = new Node(rs);
                    if (bRecurse) { vChildrenToCapitalize.add(n2); }
                }
            } catch (Exception e) { throw e; }
            finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }

            for (int i = 0; i < vChildrenToCapitalize.size(); i++)
                capitalizeNode((Node) vChildrenToCapitalize.elementAt(i), dbc, true);
        }
    }

    public static void setNodeTitle(Node n, Connection dbc)
            throws Exception {

        String sNodeTitle = n.get("NODETITLE");
        String sNodeID = n.get("NODEID");

        sNodeTitle = getCapitalized(sNodeTitle.toLowerCase());
        sNodeTitle = sNodeTitle.replaceAll("'", "''"); // replace quotes for database update

        String sSQL = "update node set nodetitle = '" + sNodeTitle + "', dateupdated = sysdate where NodeID = "+sNodeID;
        Statement stmt = null;

        try {
			stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
        } catch (Exception e) { throw e; }
        finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }

    public static String getCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if ((Character.isLetter(ch)) && (!Character.isLetter(prevCh)) && (prevCh != '\''))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        return sout;
    }
}
