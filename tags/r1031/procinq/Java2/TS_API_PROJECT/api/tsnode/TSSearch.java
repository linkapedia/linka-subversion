package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import api.results.*;

/**
 * Find all nodes with an association to the given signature word
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  Query   Signature word search term
 *	@param  CorpusID (optional) Restrict results to nodes containing this unique corpus identifier.
 *  @param  Thesaurus (optional)    If set to true, resolve signature term with available thesauri during search
 *  @param  MaxResults (optional)   Maximum number of results to return from this query.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSSearch&Thesaurus=true&CorpusID=0&SKEY=2103570700&Related=true&MaxResults=4&query=test&start=1&rowmax=4

 *	@return	a series of NODE objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>2556</NODEID>
            <CORPUSID>5</CORPUSID>
            <NODETITLE><![CDATA[ Abraham]]></NODETITLE>
            <PARENTID>24933</PARENTID>
            <NODEINDEXWITHINPARENT>3</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>1</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[]]></NODEDESC>
            <NODESIZE>139</NODESIZE>
            <LINKNODEID>2556</LINKNODEID>
            <RESULTTYPE>2</RESULTTYPE>
        </NODE>
        <NODE>
            <NODEID>2756</NODEID>
            <CORPUSID>5</CORPUSID>
            <NODETITLE><![CDATA[ Airplane]]></NODETITLE>
            <PARENTID>24723</PARENTID>
            <NODEINDEXWITHINPARENT>2</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>1</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[]]></NODEDESC>
            <NODESIZE>733</NODESIZE>
            <LINKNODEID>2756</LINKNODEID>
            <RESULTTYPE>2</RESULTTYPE>
        </NODE>
 ...
    </NODES>
 </TSRESULT>
  \endverbatim
 */
public class TSSearch
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sQuery = (String) props.get ("Query", true);
		String sKey = (String) props.get("SKEY", true);
		String sRelated = (String) props.get("Related", "true");
		String sThesaurus = (String) props.get("Thesaurus");
		String sCorpusID = (String) props.get("CorpusID", "0");
		String sMaxResults = (String) props.get("MaxResults", "99");
		String sPrintSQL = (String) props.get("SQL");
		
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
		
		Query q = new Query(sQuery);
		if (sPrintSQL != null) { q.PrintOutSQL(); }

		long lStart = System.currentTimeMillis();

		if (sRelated != null) { q.BuildRelatedQuery(sCorpusID); }
		long lStop = System.currentTimeMillis() - lStart;
		//out.println("<!-- Timing: "+lStop+" ms -->");

		int iNumResults = q.RunQuery(out, dbc, u, new Integer(sMaxResults).intValue());
		out.println("<NUMRESULTS>"+iNumResults+"</NUMRESULTS>");

		return;
	}
}
