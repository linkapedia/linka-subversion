package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.system.Node;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Retrieve the corpus source of a given node within the system.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param NodeID Unique node identifier of the node whose children are being retrieved.
 *
 * @note http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodeSource&NodeID=1087101&SKEY=9919294812
 *
 * @return	NODE object \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <NODE> <NODEID>1087101</NODEID> <CORPUSID>300012</CORPUSID> <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
 * <PARENTID>1087099</PARENTID> <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT> <DEPTHFROMROOT>0</DEPTHFROMROOT> <DATESCANNED>null</DATESCANNED> <DATEUPDATED>null</DATEUPDATED>
 * <NODESTATUS>1</NODESTATUS> <NODEDESC><![CDATA[ ]]></NODEDESC> <NODESIZE>1</NODESIZE> <LINKNODEID>1087101</LINKNODEID> <NODESOURCE><![CDATA[The presidential debates of 1992 were ...]]></NODEDESC>
 * </NODE> </TSRESULT> \endverbatim
 */
public class TSGetNodeSource {
    // TSGetNodeSource (sessionid, nodeid)

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
        ResultSet rs = null;
        try {
            if (sNodeID.equals("-1")) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
            }

            String sNodeSource = "";
            try {
                sNodeSource = UtilClob.getClobStringFromNodeData(new Long(sNodeID).longValue(), dbc);
            } catch (Exception e) {
                e.printStackTrace(System.out);
            } // ignore an error here, return empty string

            String sSQL = Node.getSQL(dbc) + " where NodeId = " + sNodeID;
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            int loop = 0;
            while (rs.next()) {
                loop = loop + 1;
                Node n = new Node(rs);
                out.println("<NODE>");
                n.emitXML(out, u, false);
                out.println("   <NODESOURCE><![CDATA[" + sNodeSource + "]]></NODESOURCE>");
                out.println("</NODE>");
            }

            rs.close();
            stmt.close();
            // If no results found, throw an exception
            if (loop == 0) {
                out.println("<DEBUG>No information found for Node ID: " + sNodeID + " </DEBUG>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
            }

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}
