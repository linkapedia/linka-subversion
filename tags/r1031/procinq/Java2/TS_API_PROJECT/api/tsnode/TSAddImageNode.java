package api.tsnode;

import api.FileMetadata;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import org.apache.log4j.Logger;

/**
 * Add a Image to node.
 *
 *	@authors Andres Restrepo, All Rights Reserved.
 *
 *	@return	SUCCESS tag, if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
<TSRESULT>
<SUCCESS>images add ok</SUCCESS>
</TSRESULT>
\endverbatim
 */
public class TSAddImageNode {

    // TSAddImageNode (imageid,description,url,image(blob),nodeid)
    private static final Logger log = Logger.getLogger(TSAddImageNode.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        List<FileMetadata> listFileMetada=(List<FileMetadata>)props.get("inputstream", null);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        if(listFileMetada.isEmpty())
        {
            log.debug("List FIle is Empty");
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR) , out);
            return;
        }
        String nodeid=listFileMetada.get(0).getNodeId();
        /*if (nodeid != null) {
            log.debug("DEBUG: TSAddImageNode invoked with lang: " + nodeid);
        } else {
            log.debug("DEBUG: TSAddImageNode invoked without a nodeid argument.");
        }*/
        try {
            // for authorization purposes, need to know the corpus id of this node
            String sSQL = " select CorpusId from Node where NodeID = " + nodeid;
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();
            int iCorpusId = rs.getInt(1);
            stmt.close();
            rs.close();
            rs = null;
            stmt = null;
            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            sSQL = "insert into NODEIMAGES(imageid, description, url, nodeid, image) values(SEC_IMAGESNODES.nextval, ?, ?, ?, ?)";
            pstmt = dbc.prepareStatement(sSQL);
            for(FileMetadata file:listFileMetada)
            {
                log.debug(file.toString());
                pstmt.setString(1, file.getDescription());
                pstmt.setString(2, file.getUri());
                pstmt.setInt(3, Integer.parseInt(nodeid));
                pstmt.setBinaryStream(4, new ByteArrayInputStream(file.getImage()));
                pstmt.executeUpdate();
            }
            log.debug("upload image file successful");
            out.println("<SUCCESS>images add ok</SUCCESS>");
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
}
