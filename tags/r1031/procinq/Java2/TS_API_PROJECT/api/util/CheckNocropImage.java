package api.util;

import java.awt.image.BufferedImage;
import org.apache.log4j.Logger;
import org.linkapedia.images.util.ManageResourcesUtil;

/**
 *
 * @author andres
 */
public class CheckNocropImage {

    private static final Logger log = Logger.getLogger(CheckNocropImage.class);
    private static Integer defaultBoxw;
    private static Integer defaultBoxh;

    static {
        try {
            defaultBoxw = Integer.parseInt(ManageResourcesUtil.getProperties().getProperty("org.linkapedia.images.nocropimage.boxw"));
            defaultBoxh = Integer.parseInt(ManageResourcesUtil.getProperties().getProperty("org.linkapedia.images.nocropimage.boxh"));
        } catch (Exception e) {
            log.error("Error reading the defaultBoxw or defaultBoxh in CheckNocropImage:", e);
        }
    }

    public static boolean check(BufferedImage image) throws IllegalArgumentException {
        return check(image, defaultBoxw, defaultBoxh);
    }

    public static boolean check(BufferedImage image, Integer boxw, Integer boxh) throws IllegalArgumentException {
        if (image == null) {
            throw new IllegalArgumentException("Sorry image can't be null");
        }
        if (boxw == null) {
            throw new IllegalArgumentException("Sorry boxw can't be null");
        }
        if (boxh == null) {
            throw new IllegalArgumentException("Sorry boxh can't be null");
        }
        int width = image.getWidth();
        int height = image.getHeight();

        if (width > boxw && height > boxh) {
            return true;
        }
        return false;
    }
}
