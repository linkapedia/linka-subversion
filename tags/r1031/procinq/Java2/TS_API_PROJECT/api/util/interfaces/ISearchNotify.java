/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.util.interfaces;

import java.util.Map;

/**
 *
 * @author andres
 */
public interface ISearchNotify<T> {

    
    void onSearchResult(T result,Map<String,String> params) throws Exception;
    void pr();
}
