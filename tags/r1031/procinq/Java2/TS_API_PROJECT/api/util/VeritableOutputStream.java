package api.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;



public class VeritableOutputStream extends FilterOutputStream {
    private boolean bUTF8 = false;

    public VeritableOutputStream(OutputStream out) {
        super(out);
    }

    @Override
    public void write(int b) throws IOException {
        if (isAcceptable(b)) {
            out.write(b);
        }
    }

    @Override
    public void write(byte[] b) throws IOException {
        for (int i = 0; i < b.length; i++)
            write((int) b[i]);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        for (int i = off; i < len; i++)
            write((int) b[i]);
    }

    public boolean isAcceptable(int b) {
        if (b > 31) {
            return true;
        }
        if ((b == 13) || (b == 9) || (b == 10)) {
            return true;
        }
        return false;
    }
}
