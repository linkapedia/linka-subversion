/*
 * class to manager properties of google api
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Ago 12, 2011, 9:00 AM
 */
package api.util.google.crawl;

import api.util.ConfServer;
import api.util.EmitFilterGoogle;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class APIGoogle {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(APIGoogle.class);
    private String url;
    private String customSearchID;
    private String key;
    private String languageResult;
    private String safe;
    private String num;
    private String filter;
    private String path;
    private String paramSearch;
    private String start;

    /**
     * Constructor read Conf.properties
     */
    public APIGoogle() {
        this.url = ConfServer.getValue("google.api.url");
        this.key = ConfServer.getValue("google.api.key");
        this.languageResult = ConfServer.getValue("google.api.languageResult");
        this.safe = ConfServer.getValue("google.api.safe");
        this.filter = ConfServer.getValue("google.api.filter");
    }

    public void setCustomSearchID(String custom) {
        customSearchID = custom;
    }

    private String getCustomSearchID() {
        StringBuilder sb = new StringBuilder();
        if (customSearchID != null && !customSearchID.trim().equals("")) {
            sb.append("cx=");
            if (customSearchID.equals(String.valueOf(EmitFilterGoogle.CUSTOM_SEARCH_ORG_EDU))) {            
                sb.append(ConfServer.getValue("google.api.customSearhID_edu_org"));
            } else if (customSearchID.equals(String.valueOf(EmitFilterGoogle.CUSTOM_SEARCH_COM))) {
                sb.append(ConfServer.getValue("google.api.customSearhID_com"));
            }else{
                sb.append("uncaught");
            }
            sb.append("&");
        } else {
            sb.append("");
        }
        return sb.toString();
    }

    /**
     * set the num to complete the url
     *
     * @param n
     */
    public void setNum(String n) {
        this.num = n;
    }

    /**
     * to get result number
     *
     * @return
     */
    private String getNum() {
        StringBuilder sb = new StringBuilder();
        if (this.num != null && !this.num.trim().equals("")) {
            sb.append("num=");
            sb.append(this.num);
            sb.append("&");
            return sb.toString();
        } else {
            return "";
        }

    }

    /**
     * set the start result to complete the url
     *
     * @param n
     */
    public void setStart(String n) {
        this.start = n;
    }

    private String getStart() {
        StringBuilder sb = new StringBuilder();
        if (this.start != null && !this.start.trim().equals("")) {
            sb.append("start=");
            sb.append(this.start);
            sb.append("&");
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * set the searh term into url
     *
     * @param searchTerm
     */
    public void setSearchParameter(String searchTerm) {
        this.paramSearch = searchTerm;
    }

    private String getSearchParameter() {
        StringBuilder sb = new StringBuilder();
        sb.append("q=");
        if (this.paramSearch != null && !this.start.trim().equals("")) {
            try {
                String encode = URLEncoder.encode(this.paramSearch, "UTF-8");
                sb.append(encode);
            } catch (UnsupportedEncodingException ex) {
                LOG.error("APIGoogle setSearchParameter " + ex.getMessage());
                sb.append(paramSearch);
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * return the complete url to retun Json Object or atom xml
     *
     * @return
     */
    @Override
    public String toString() {
        path = url + getCustomSearchID() + getKey() + getLanguageResult() + getSafe() + getFilter() + getNum() + getStart() + getSearchParameter();
        return path;
    }

    /**
     * methods to build the url
     *
     * @return
     */
    private String getFilter() {
        StringBuilder sb = new StringBuilder();
        if (filter != null && !filter.trim().equals("")) {
            sb.append("filter=");
            sb.append(filter);
            sb.append("&");
        } else {
            sb.append("");
        }
        return sb.toString();
    }

    private String getSafe() {
        StringBuilder sb = new StringBuilder();
        if (safe != null && !safe.trim().equals("")) {
            sb.append("safe=");
            sb.append(safe);
            sb.append("&");
        } else {
            sb.append("");
        }
        return sb.toString();
    }

    private String getLanguageResult() {
        StringBuilder sb = new StringBuilder();
        if (languageResult != null && !languageResult.trim().equals("")) {
            sb.append("lr=");
            sb.append(languageResult);
            sb.append("&");
        } else {
            sb.append("");
        }
        return sb.toString();
    }

    private String getKey() {
        StringBuilder sb = new StringBuilder();
        if (key != null && !key.trim().equals("")) {
            sb.append("key=");
            sb.append(key);
            sb.append("&");
        } else {
            sb.append("");
        }
        return sb.toString();
    }
}
