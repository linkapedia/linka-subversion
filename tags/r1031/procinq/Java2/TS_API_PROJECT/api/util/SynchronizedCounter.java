/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 22, 2003
 * Time: 10:17:49 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.util;

public class SynchronizedCounter
{
    int iCount = 0;

    public synchronized void incrVal ( int iIncrValue )
    {
        iCount += iIncrValue;
    }

    public int getVal ()
    {
        return iCount;
    }
}
