/*
 * util to connect with mediawiki api and return results
 * @authors Andres F. Restrepo A.
 *
 * @version 1.0
 * Date : Dic 13, 2011, 18:53 AM
 */
package api.util;

import info.bliki.wiki.filter.PlainTextConverter;
import info.bliki.wiki.model.Configuration;
import info.bliki.wiki.model.WikiModel;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

public class WikiUtil {

    private static final Logger log = Logger.getLogger(WikiUtil.class);
    
    public String getSingleContent(String title)throws Exception{
        log.debug("getSingleContent");
        if (title == null) {
            return "";
        }
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        List<Element> itemsRev = new ArrayList<Element>();
        
        String source = "";
        map.put("action", "query");
        map.put("prop", "revisions");
        map.put("rvprop", "content");
        map.put("format", "xml");
        
        try {
            map.put("titles", URLEncoder.encode(title, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        try {
            itemsRev = getItemsRevision(getURL(map));
            // put to mapToReturn
            if (itemsRev != null && itemsRev.size() > 0) {
                try {
                    source = filerSourceWithLinks(itemsRev.get(0).getChild("revisions").getChildText("rev"));
                } catch (Exception e) {
                    log.error("Error parsing source", e);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return source;
    }

    /**
     * remove data recursively between openChar and closeChar
     *
     * @param source
     * @param openChar
     * @param closeChar
     * @return
     */
    private static String remoneUnUsedCharacter(String source, String openChar, String closeChar) {
        log.debug("remoneUnUsedCharacter");
        StringBuilder sb = new StringBuilder();
        sb.append(source);
        Stack<Integer> stack = new Stack<Integer>();
        int index_open = sb.indexOf(openChar);
        if (index_open == -1) {
            return null;
        }
        int index_close = 0;
        stack.push(index_open);
        index_open = sb.indexOf(openChar, index_open + 1);
        while (!stack.isEmpty()) {
            if (index_open > -1) {
                stack.push(index_open);
                index_open = sb.indexOf(openChar, index_open + 1);
            } else {
                int index = stack.pop();
                index_close = sb.indexOf(closeChar, index);
                if (index_close == -1) {
                    continue;
                }
                sb.delete(index, index_close + 1);
            }
        }
        return sb.toString();
    }

    /**
     *
     * @param pages @sreturn the content to all pages array[pageid]
     */
    public HashMap<String, String> getContent(ArrayList<String> pages) {
        log.debug("getContent");
        ArrayList<String> pagesToConsult = new ArrayList<String>();
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        HashMap<String, String> mapToReturn = new HashMap<String, String>();
        List<Element> itemsRev = new ArrayList<Element>();
        map.put("action", "query");
        map.put("prop", "revisions");
        map.put("rvprop", "content");
        map.put("format", "xml");
        int k = 0;
        for (int i = 0; i < pages.size(); i++) {
            pagesToConsult.add(pages.get(i));
            if ((k == 20) || (i + 1 == pages.size())) {
                try {
                    map.put("pageids",
                            URLEncoder.encode(StringUtils.join(
                            pagesToConsult.toArray(), "|"), "UTF-8"));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                try {
                    itemsRev = getItemsRevision(getURL(map));
                    // put to mapToReturn
                    for (Element e : itemsRev) {
                        mapToReturn.put(e.getAttributeValue("pageid"),
                                filerSourceWithLinks(e.getChild("revisions").getChildText("rev")));
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                k = 0;
                pagesToConsult.clear();
            }
            k++;
        }
        return mapToReturn;
        
    }

    /**
     *
     * @param source
     * @return
     */
    private String filerSourceWithLinks(String source) {
        log.debug("filerSourceWithLinks");
        String sourceFilter = source;

        // remove trash
        sourceFilter = removeTrash(sourceFilter);
        ArrayList<String> linksWiki = new ArrayList<String>();

        // delete spaces init and end
        sourceFilter = sourceFilter.trim();
        // remove <ref
        sourceFilter = sourceFilter.replaceAll("(<ref(.*?)</ref>)", "");
        // remove {{*}}
        //sourceFilter = sourceFilter.replaceAll("(\\{\\{(.*?)}})", "");
        sourceFilter = remoneUnUsedCharacter(source, "{", "}");
        // remove image link
        sourceFilter = sourceFilter.replaceAll("(\\[\\[(Image:.*?)]])", "");
        // remove File link
        sourceFilter = sourceFilter.replaceAll("(\\[\\[(File:.*?)]])", "");
        // remove commentaries HTML
        sourceFilter = sourceFilter.replaceAll("<!--(.*?)-->", " ");
        
        linksWiki = getLinksFromSource(sourceFilter);
        StringBuilder linkedSection = new StringBuilder();
        if (!linksWiki.isEmpty()) {
            linkedSection.append("<LinkedSection>");
            linkedSection.append(StringUtils.join(linksWiki.toArray(), "|"));
            linkedSection.append("</LinkedSection>\"");
        }
        
        WikiModel wikiModel = new WikiModel(
                Configuration.DEFAULT_CONFIGURATION, Locale.ENGLISH,
                "${image}", "${title}");
        wikiModel.setUp();
        sourceFilter = "\""
                + wikiModel.render(new PlainTextConverter(), sourceFilter);
        
        sourceFilter += linkedSection;

        // remove characters
        sourceFilter = sourceFilter.replaceAll("[=*\"}\'{\\]\\[]", " ");

        // System.out.println(sourceFilter);
        if ((sourceFilter.equals("")) || (sourceFilter.equals(" "))) {
            sourceFilter = null;
        }
        return sourceFilter;
    }

    /**
     *
     * @param source
     * @return
     */
    private String removeTrash(String source) {
        log.debug("removeTrash");
        int index = 0;
        String sourceFilter = source;
        sourceFilter = sourceFilter.replaceAll("(== {0,}(See also) {0,}==)",
                "==See also==");
        sourceFilter = sourceFilter.replaceAll("(== {0,}(Notes) {0,}==)",
                "==Notes==");
        sourceFilter = sourceFilter.replaceAll("(== {0,}(References) {0,}==)",
                "==References==");
        sourceFilter = sourceFilter.replaceAll(
                "(== {0,}(External links) {0,}==)", "==External links==");
        sourceFilter = sourceFilter.replaceAll("(== {0,}(Source){0,} ==)",
                "==Source==");
        
        if (sourceFilter.contains("==See also==")) {
            index = sourceFilter.lastIndexOf("==See also==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==Notes==")) {
            index = sourceFilter.lastIndexOf("==Notes==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==References==")) {
            index = sourceFilter.lastIndexOf("==References==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==External links==")) {
            index = sourceFilter.lastIndexOf("==External links==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==Source==")) {
            index = sourceFilter.lastIndexOf("==Source==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        return sourceFilter;
    }

    /**
     *
     * @param source
     * @return
     */
    private ArrayList<String> getLinksFromSource(String source) {
        log.debug("getLinksFromSource");
        ArrayList<String> links = new ArrayList<String>();
        Pattern pa = null;
        Matcher ma = null;
        String l = "";
        
        pa = Pattern.compile("(\\[\\[(.*?)]])");
        ma = pa.matcher(source);
        while (ma.find()) {
            l = ma.group(2);
            if (l.contains("|")) {
                l = l.substring(l.lastIndexOf("|") + 1, l.length());
            }
            if ((!l.isEmpty()) && (!l.contains("Category:"))) {
                links.add(l);
            }
            
        }
        return links;
        
    }

    /**
     *
     * @param map
     * @return
     * @throws MalformedURLException
     */
    private URL getURL(LinkedHashMap<String, String> map)
            throws MalformedURLException {
        log.debug("getURL");
        Iterator<String> it = map.keySet().iterator();
        StringBuilder param = new StringBuilder();
        String sParam = "";
        param.append(ConfServer.getValue("wikipedia.crawl.api"));
        while (it.hasNext()) {
            String e = it.next();
            String value = map.get(e);
            param.append(e).append("=").append(value).append("&");
        }
        sParam = param.toString();
        int index = sParam.lastIndexOf("&");
        if (index > -1) {
            sParam = sParam.substring(0, index);
        }
        return new URL(sParam);
    }

    /**
     *
     * @param url
     * @return elements of xml
     */
    @SuppressWarnings("unchecked")
    private List<Element> getItemsRevision(URL url) {
        log.debug("getItemsRevision");
        HttpURLConnection con = null;
        SAXBuilder saxBuilder = null;
        Document doc = null;
        StringReader in = null;
        List<Element> items = null;
        try {
            con = (HttpURLConnection) url.openConnection();
            if ((con != null) && (con.getResponseCode() == 200)) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        con.getInputStream(), "UTF-8"));
                String line = "";
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                saxBuilder = new SAXBuilder();
                in = new StringReader(sb.toString());
                doc = saxBuilder.build(in);
                
                items = new ArrayList<Element>();
                items = XPath.selectNodes(doc, "/api/query/pages/page");
                return items;
            } else {
                System.out.println("Error getItemsRevision");
                return null;
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
        
    }

    /**
     *
     * @param title
     * @return
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public String getPageIdFromTitle(String title) throws IOException {
        log.debug("getPageIdFromTitle");
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        HttpURLConnection con = null;
        SAXBuilder xmlBuilder = null;
        Document xmlDoc = null;
        StringReader in = null;
        List<Element> itemsXp = null;
        map.put("action", "query");
        map.put("prop", "info");
        map.put("titles", URLEncoder.encode(title, "UTF-8"));
        map.put("format", "xml");
        try {
            URL url = getURL(map);
            con = (HttpURLConnection) url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    con.getInputStream(), "UTF-8"));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            xmlBuilder = new SAXBuilder();
            in = new StringReader(sb.toString());
            xmlDoc = xmlBuilder.build(in);
        } catch (IOException e) {
            return "0";
        } catch (JDOMException e) {
            return "0";
        }
        itemsXp = new ArrayList<Element>();
        try {
            itemsXp = XPath.selectNodes(xmlDoc, "/api/query/pages/page");
        } catch (JDOMException e) {
            e.printStackTrace();
        }
        if (itemsXp != null && !itemsXp.isEmpty()) {
            Element e = itemsXp.get(0);
            if (e != null) {
                return e.getAttributeValue("pageid");                
            }
        }
        return "0";
        
    }
}
