
package api.util.bean;

/**
 *
 * @author andres
 */
public class SignatureBean {
    private String word;
    private int ocurrence;
    private String lang;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getOcurrence() {
        return ocurrence;
    }

    public void setOcurrence(int ocurrence) {
        this.ocurrence = ocurrence;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
   
    
}
