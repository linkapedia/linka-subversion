package api.util.bean;

/**
 *
 * @author andres
 */
public class SearchBean {
    private String nodeid;
    private String term;

    public SearchBean(String nodeid,String term) {
        this.nodeid=nodeid;
        this.term= term; 
    }

    public String getNodeid() {
        return nodeid;
    }

    public void setNodeid(String nodeid) {
        this.nodeid = nodeid;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }
}
