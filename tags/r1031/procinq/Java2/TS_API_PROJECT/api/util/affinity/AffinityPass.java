package api.util.affinity;

import com.iw.bo.CorpusBO;
import com.iw.bo.MustHaveBO;
import com.iw.bo.interfaces.ICorpusBO;
import com.iw.bo.interfaces.IMustHaveBO;
import com.iw.scoring.NodeForScore;
import com.yuxipacific.services.cache.CacheHandler;
import com.yuxipacific.services.cache.CacheObject;
import java.sql.Connection;
import java.util.Vector;
import org.apache.log4j.Logger;

public class AffinityPass {

    private static final String TERMS_CACHE_IDENTIFIER = "terms";
    private static final Logger log = Logger.getLogger(AffinityPass.class);
    private ICorpusBO corpusBO = null;
    private IMustHaveBO mustHaveBO = null;

    public AffinityPass(Connection conn) {
        corpusBO = new CorpusBO(conn);
        mustHaveBO = new MustHaveBO(conn);
    }

    private AffinityPass() {
    }

    /**
     * Affinity terms to the taxonomy
     *
     * @param terms
     * @param dbc
     * @return return true if some terms of affinity is into doc
     */
    public boolean checkAffinityTermsTax(Vector<String> terms, int corpusID) {
        log.debug("checkAffinityTermsTax(Vector, Integer)");
        String affinityTerm;
        CacheObject termsCache = CacheHandler.getCachedObjectHandler(TERMS_CACHE_IDENTIFIER);
        try {
            //Check if we have the object in the cache.
            affinityTerm = (String) termsCache.getObject(corpusID);
            if (affinityTerm == null) {
                affinityTerm = getCorpusBO().getAffinityTermsByCorpusID(corpusID);
                termsCache.setObject(corpusID, affinityTerm);
            }

            if (affinityTerm != null && !affinityTerm.trim().equals("")) {
                for (String term : terms) {
                    if (affinityTerm.contains(term)) {
                        return true;
                    }
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            log.error("An error ocurred: ", ex);
        }
        return false;
    }

    /**
     * Must haves to each node
     *
     * @param terms
     * @param nfs
     * @param dbc
     * @return return true if some musthave is into doc
     */
    public boolean checkAffinityTermsNode(Vector<String> terms, NodeForScore nfs) {
        log.debug("checkAffinityTermsNode(Vector, Integer)");
        String mustHaves;
        long nodeID;
        try {
            nodeID = nfs.getNodeID();
            if (nodeID < Integer.MIN_VALUE || nodeID > Integer.MAX_VALUE) {
                throw new IllegalArgumentException(nodeID + " cannot be cast to int without changing its value.");
            }
            mustHaves = getMustHaveBO().getMustHavesByNodeID((int) nodeID);
            if (mustHaves != null && !mustHaves.trim().equals("")) {
                for (String term : terms) {
                    if (mustHaves.contains(term)) {
                        return true;
                    }
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            log.error("An error ocurred: ", ex);
        }
        return false;
    }

    public ICorpusBO getCorpusBO() {
        return corpusBO;
    }

    public void setCorpusBO(ICorpusBO corpusBO) {
        this.corpusBO = corpusBO;
    }

    public IMustHaveBO getMustHaveBO() {
        return mustHaveBO;
    }

    public void setMustHaveBO(IMustHaveBO mustHaveBO) {
        this.mustHaveBO = mustHaveBO;
    }
}
