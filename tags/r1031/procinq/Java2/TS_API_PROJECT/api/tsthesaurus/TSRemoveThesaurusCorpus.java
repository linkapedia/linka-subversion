package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove a corpus relationship from the given thesaurus. Note removing the corpus relationship will automatically
 *   purge the signature cache of any corpus using the given thesaurus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  ThesaurusID (optional)   Unique thesaurus identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSRemoveThesaurusCorpus&CorpusID=4&ThesaurusID=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Corpus thesaurus relationship removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveThesaurusCorpus
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sThID = (String) props.get ("ThesaurusID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			String sSQL = "delete from CorpusThesaurus where CorpusID = "+sCorpusID;
            if (sThID != null) sSQL = sSQL+" and ThesaurusID = "+sThID;
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from CorpusThesaurus table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

            out.println("<SUCCESS>Corpus thesaurus relationship removed successfully.</SUCCESS>");
			// purge signature cache files
			api.tspurge.TSPurgeSigCache.PurgeThesaurusSignatures(sThID, out, dbc);

			stmt.close();
		}
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
