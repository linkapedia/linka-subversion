package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Get all properties and relationships of a given thesaurus term.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusID   Unique thesaurus identifier
 *  @param  WordID   Unique thesaurus term identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSGetWordProps&ThesaurusID=1&WordID=4900&SKEY=993135977

 *	@return	a series of thesaurus term objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <WORDS>
            <WORD>
                <ANCHORID>4900</ANCHORID>
                <ANCHORNAME>couch</ANCHORNAME>
                <OBJECTID>899</OBJECTID>
                <OBJECTNAME>sofa</OBJECTNAME>
                <RELATIONSHIPCODE>1</RELATIONSHIPCODE>
            </WORD>
 ...
  \endverbatim
 */
public class TSGetWordProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sTID = (String) props.get ("ThesaurusID", true);
		String sWID = (String) props.get ("WordID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED); }

			String sSQL = " select WA.thesaurusword, WB.thesaurusword, R.WordAnchor, "+
						  " R.WordRelated, R.Relationship from ThesaurusRelations R, "+
						  " ThesaurusWord WA, ThesaurusWord WB where WA.WordID = "+sWID+" and "+
						  " R.WordAnchor = "+sWID+" and wb.wordid = r.wordrelated and "+
						  " R.ThesaurusID = "+sTID;
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				if (loop == 1) { out.println ("<WORDS>"); }
				
				String sAnchorName = rs.getString(1);
				String sRelatsName = rs.getString(2);
				int iAnchorID = rs.getInt(3);
				int iRelatsID = rs.getInt(4);
				int iRelationship = rs.getInt(5);
		
				out.println ("   <WORD> ");
				out.println ("   <ANCHORID>"+iAnchorID+"</ANCHORID>");
				out.println ("   <ANCHORNAME>"+sAnchorName+"</ANCHORNAME>");
				out.println ("   <OBJECTID>"+iRelatsID+"</OBJECTID>");
				out.println ("   <OBJECTNAME>"+sRelatsName+"</OBJECTNAME>");
				out.println ("   <RELATIONSHIPCODE>"+iRelationship+"</RELATIONSHIPCODE>");
				out.println ("   </WORD>");
			}
		
			rs.close();
		    stmt.close();
			
			if (loop == 0) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			out.println ("</WORDS>");
			
		}
		
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
