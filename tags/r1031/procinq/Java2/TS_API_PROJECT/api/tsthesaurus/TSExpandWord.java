package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * This API call has been deprecated, and should no longer be used.
 * @note    This API call has been deprecated.
 */
public class TSExpandWord
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		out.println("This function has been deprecated.");
	}
}
