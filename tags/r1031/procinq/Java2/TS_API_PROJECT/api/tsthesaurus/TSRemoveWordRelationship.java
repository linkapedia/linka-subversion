package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove a relationship between two thesaurus terms.  Note altering the relationship will automatically
 *   purge the signature cache of any corpus using the given thesaurus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusID   Unique thesaurus identifier
 *  @param  Word1   Anchor term from whom the relationship was defined.
 *  @param  Word2   Object term of the relationship being removed.
 *  @param  Relationship    Relationship type, 1 is a synonym, 2 is a broader term and 3 is a narrower term
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSRemoveWordRelationship&ThesaurusID=1&Word1=couch&Word2=sofa&Relationship=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Relationship removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveWordRelationship
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sThID = (String) props.get ("ThesaurusID", true);
		String sWordID1 = (String) props.get ("WordID1", true);
		String sWordID2 = (String) props.get ("WordID2", true);
		String sRelationship = (String) props.get ("Relationship", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            // Ensure user is a member of the admin group
			if (!u.IsMember(out)) {
                out.println("<NOTAUTHORIZED>You are not authorized to use this function</NOTAUTHORIZED>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
			
			String sSQL = " delete from ThesaurusRelations where WordAnchor = "+sWordID1+" and "+
						  " WordRelated = "+sWordID2+" and Relationship = "+sRelationship+" "+
						  " and ThesaurusID = "+sThID;
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from ThesaurusRelations table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
            out.println("<SUCCESS>Relationship removed successfully.</SUCCESS>");

			// purge signature cache files
			api.tspurge.TSPurgeSigCache.PurgeThesaurusSignatures(sThID, out, dbc);

			stmt.close();
		}
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
