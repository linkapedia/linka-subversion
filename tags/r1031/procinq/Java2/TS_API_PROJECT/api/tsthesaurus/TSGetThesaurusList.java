package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Get all available thesauri in the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSGetThesaurusList&SKEY=993135977

 *	@return	a series of thesaurus objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <THESAURI>
            <THESAURUS>
                <THESAURUSID>1</THESAURUSID>
                <THESAURUSNAME>IDRAC Thesaurus</THESAURUSNAME>
            </THESAURUS>
            <THESAURUS>
                <THESAURUSID>1121</THESAURUSID>
                <THESAURUSNAME>regress02</THESAURUSNAME>
            </THESAURUS>
            <THESAURUS>
                <THESAURUSID>13360</THESAURUSID>
                <THESAURUSNAME>al Qaeda</THESAURUSNAME>
            </THESAURUS>
 ...
  \endverbatim
 */
public class TSGetThesaurusList
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED); }

			String sSQL = " select ThesaurusID, ThesaurusName from Thesaurus"; 
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				if (loop == 1) { out.println ("<THESAURI>"); }
				
				int iThesId = rs.getInt(1);
				String sThesName = rs.getString(2);	
		
				out.println ("   <THESAURUS> ");
				out.println ("   <THESAURUSID>"+iThesId+"</THESAURUSID>");
				out.println ("   <THESAURUSNAME>"+sThesName+"</THESAURUSNAME>");
				out.println ("   </THESAURUS>");
			}
		
			rs.close();
		    stmt.close();
			
			if (loop == 0) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			out.println ("</THESAURI>");
			
		}
		
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
