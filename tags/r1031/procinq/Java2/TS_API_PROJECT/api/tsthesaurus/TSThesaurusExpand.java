package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.tsnode.TSGetNodeSigs;
import api.tsclassify.TSClassifyDoc;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.util.*;
import com.indraweb.signatures.SigExpandersTitleThesaurus;
import com.iw.scoring.NodeForScore;

/**
 *  Get the thesaurus expanded version of a term vector.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Sigs    String sequence indicating counts and terms.
 *    Example: "3,term1|||5,term2|||7,term3"
 *  @param  CorpusID   Unique thesaurus identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSThesaurusExpand&CorpusID=4&Sigs=3,term1|||5,term2 is phrase||thesterm21||thesterm22 is phrase|||7,term3||thesterm31&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Corpus thesaurus relationship added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSThesaurusExpand
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sSigs = (String) props.get ("Sigs", true);
		String sKey = (String) props.get("SKEY", true);
		String sNodeTitle = (String) props.get("NodeTitle", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

			if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }


            Vector vStrSigWords = new Vector();
            Vector vIntSigCounts = new Vector();
            Vector[] vArrUnder_ArrThesaurusList_ = null;

            //IntContainer ic_out_titleCount = new IntContainer();
            vArrUnder_ArrThesaurusList_ = SigExpandersTitleThesaurus.vecTermsFromString (
                                            //sNodeTitle,
                                            sSigs,
                                            vStrSigWords,
                                            vIntSigCounts
                                            //ic_out_titleCount
                                        );

            //api.Log.Log ("vStrSigWords post title [" +  UtilSets.vToStr(vStrSigWords)+ "]" );
            // add title to words and add also thesaurus terms
            Vector[] vArrUnderThesWords = null;

        TSGetNodeSigs.emitTermAndThesSigs(
                        vStrSigWords,
                        vIntSigCounts,
                        //vArrUnderThesWords,
                        "NA",
                        out,
                        0
                    );


		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/

		}
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
