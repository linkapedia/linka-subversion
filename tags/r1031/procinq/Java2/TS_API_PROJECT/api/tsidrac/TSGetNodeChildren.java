package api.tsidrac;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 * This API call has been deprecated, and should no longer be used.   Please use CQL to retrieve node children.
 * @note    This API call has been deprecated.
 */
public class TSGetNodeChildren {
	// TSGetNodeChildren (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sParentID = (String) props.get ("NodeID", true);
		String sCountries = (String) props.get ("Country", true);

		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sParentID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();

            // security now handled by the node emitter
			//if (!u.IsAuthorized(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			// Format sCountries to conduct SQL query correctly
			if (!sCountries.equals("ALL")) {
				sCountries = "'"+sCountries+"'";
				sCountries = com.indraweb.util.UtilStrings.replaceStrInStr(sCountries, ",", "','");
			}
			
			sSQL = Node.getSQL(dbc)+" where NodeStatus != -1 and ParentId = " + sParentID;
 			if (!sCountries.equals("ALL")) {
				sSQL = sSQL +
				   " and NodeID in (select NodeID from CustomNode where attributeID = 1 and "+
				   " attributeVAL in ("+sCountries+")) ";
			}
            sSQL=sSQL+" ORDER BY NODEINDEXWITHINPARENT";

			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);
 
			int loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.   To do this, hold off
				//  on writing out the initial stream <NODES> until we have found at 
				//  least one result.   -MAP 10/15/01
				loop = loop + 1;
				if (loop == 1) { out.println ("<NODES>"); }

                Node n = new Node(rs);

                out.println("<NODE>");
                n.emitXML(out, u, false);

				if (!sCountries.equals("ALL")) {
					out.println ("   <COUNTRIES>");

					// print out associated countries
					sSQL = " select attributeVAL from CustomNode where NodeID = "+n.get("LINKNODEID")+
						   " and attributeID = 1 and attributeVAL in ("+sCountries+")";
					Statement stmt2 = dbc.createStatement();	
					ResultSet rs2 = stmt2.executeQuery (sSQL);
					while ( rs2.next() ) { 
						String s = rs2.getString(1);
						out.println("       <COUNTRY>"+s+"</COUNTRY>");
					} rs2.close(); stmt2.close();
				
					out.println ("   </COUNTRIES>");
				}
				out.println ("   </NODE>");
			}
		
			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No nodes found for parent node "+sParentID+" </DEBUG>"); 
				//throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_NODES_THIS_PARENT_NODE);
                return;
			}

			out.println ("   </NODES>");
			
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
