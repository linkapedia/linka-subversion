package api.tsidrac;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Get the list of available languages from the database.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsidrac.TSGetLanguages&SKEY=-132981656

 *	@return	LANGUAGES tag containing the list of available languages, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <LANGUAGES>
         <LANGUAGE>US</LANGUAGE>
         <LANGUAGE>DE</LANGUAGE>
         <LANGUAGE>FR</LANGUAGE>
         <LANGUAGE>SP</LANGUAGE>
...
      </LANGUAGES>
  </TSRESULT>
  \endverbatim
 */
public class TSGetLanguages
{
	// TSListGroupMembers (sessionid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

 			String sSQL = "select Language from Languages";
			Statement stmt = dbc.createStatement();
			ResultSet rs = stmt.executeQuery (sSQL);

            int loop = 0;
            while (rs.next()) {
                loop++;
                if (loop == 1) { out.println("<LANGUAGES>"); }
                String sLanguage = rs.getString(1);
                if (sLanguage != null) { out.println("   <LANGUAGE>"+sLanguage+"</LANGUAGE>"); }
            }

            if (loop == 0) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
            out.println("</LANGUAGES>");

  		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}
