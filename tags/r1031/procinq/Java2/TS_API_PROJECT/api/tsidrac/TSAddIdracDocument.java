package api.tsidrac;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;

import com.indraweb.execution.Session;
import com.iw.system.Document;
import com.iw.system.User;
import api.Log;

/**
 * Adds an IDRAC document relationship to the ITS server.  IDRAC documents differ from ITS documents as they
 *  contain additional document attributes.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocURL  The URL or UNC that links uniquely to this document.
 *  @param  DocTitle    The document title or subject heading.
 *  @param  DocSummary  (optional) A brief abstract of 4000 characters or less.
 *  @param  GenreID (optional) Genre/Folder identifier, if applicable.
 *  @param  FilePath    (optional) Absolute path to the file location on disk.
 *  @param  Visible (optional) Optional flag for whether the document comes back from standard CQL queries.
 *  @param  Group (optional) LDAP Security group with permissions to this document (default is all groups)
 *  @param  Bibliography (optional) (IDRAC attribute) Contains the document bibliography.
 *  @param  Language (optional) (IDRAC attribute) The language of this document.
 *  @param  Country (optional) (IDRAC attribute) The country relationship of this document.
 *  @param  IdracNumber (optional) (IDRAC attribute) The unique IDRAC number assigned to this document.
 *  @param  Outdated (optional) (IDRAC attribute) A value of 1 indicates that the document is outdated. Default is 0.
 *  @param  Form (optional) (IDRAC attribute) A value of 1 indicates that this document is a form.
 *  @param  AdoptionDate (optional) (IDRAC attribute) The document adoption date, default is the current date.
 *  @param  PublicationDate (optional) (IDRAC attribute) The document publication date, default is the current date.
 *  @param  EntryDate (optional) (IDRAC attribute) The document entry date, default is the current date.
 *  @param  RevisionDate (optional) (IDRAC attribute) The document revision date, default is the current date.
 *  @param  SystemDate (optional) (IDRAC attribute) The document system date, default is the current date.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsidrac.TSAddIdracDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	DOCUMENTID tag containing the document identifier, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <DOCUMENTID>38184</DOCUMENTID>
  </TSRESULT>
  \endverbatim
 */
public class TSAddIdracDocument
{
	// add an idrac document object
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            // Ensure user is a member of the admin group
            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // required fields:
            if (((String) props.get("genreid") == null) ||
                ((String) props.get("docurl") == null) ||
                ((String) props.get("doctitle") == null) ||
                ((String) props.get("documentsummary") == null)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }

            // if this docurl exists, we need to do an update, else we do an insert
            Document d = api.tsdocument.TSEditDocProps.GetDocument(props, dbc);
            int DocumentID = 0;

            if (d == null) { DocumentID = api.tsdocument.TSEditDocProps.InsertIdracDocument(props, dbc, out); }
            else {
                DocumentID = new Integer(d.get("DOCUMENTID")).intValue();
                api.tsdocument.TSEditDocProps.UpdateIdracDocument(DocumentID, props, dbc, out);
            }

            if (DocumentID != 0) { out.println("<DOCUMENTID>"+DocumentID+"</DOCUMENTID>"); }

		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
