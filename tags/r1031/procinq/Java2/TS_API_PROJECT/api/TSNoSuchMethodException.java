package api;

import api.emitxml.EmitGenXML_ErrorInfo;

public class TSNoSuchMethodException extends TSException
{
	public TSNoSuchMethodException ( String s )
	{
		super ( EmitGenXML_ErrorInfo.ERR_TS_NO_SUCH_METHOD_ON_KNOWN_OBJECT, s );
	} 
}
