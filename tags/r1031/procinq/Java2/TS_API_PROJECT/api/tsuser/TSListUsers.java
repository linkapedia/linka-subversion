package api.tsuser;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;

import api.emitxml.EmitGenXML_ErrorInfo;
import api.security.*;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * List all the LDAP users in the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  UserID(optional) The unique LDAP user identifier, if specific user properties are requested.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsuser.TSListUsers&SKEY=-132981656

 *	@return	a series of LDAP user objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUBSCRIBERS>
            <SUBSCRIBER>
                <ID>CN=david kelson,CN=Users,DC=indraweb</ID>
                <EMAIL>david@fda.gov</EMAIL>
                <PASSWORD>null</PASSWORD>
                <NAME>david kelson</NAME>
                <EMAILSTATUS>0</EMAILSTATUS>
                <SCORETHRESHOLD>50.0</SCORETHRESHOLD>
                <RESULTSPERPAGE>10</RESULTSPERPAGE>
                <USERSTATUS>-1</USERSTATUS>
            </SUBSCRIBER>
            <SUBSCRIBER>
                <ID>CN=otis brawley,CN=Users,DC=indraweb</ID>
                <EMAIL>otis@fda.gov</EMAIL>
                <PASSWORD>null</PASSWORD>
                <NAME>otis brawley</NAME>
                <EMAILSTATUS>0</EMAILSTATUS>
                <SCORETHRESHOLD>50.0</SCORETHRESHOLD>
                <RESULTSPERPAGE>10</RESULTSPERPAGE>
                <USERSTATUS>-1</USERSTATUS>
            </SUBSCRIBER>
...
        </SUBSCRIBERS>
    </TSRESULT>  \endverbatim
 */
public class TSListUsers
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sUserDC = (String) props.get("UserID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {
			Vector vUsers = new Vector();

			// Get the USER LIST from LDAP
			if (sUserDC == null) { vUsers = lc.GetAllUsers(); }
			else { vUsers.addElement((User) lc.GetUser(sUserDC)); }
			
			if (vUsers.size() == 0) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			if (vUsers.elementAt(0) == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
		
			Enumeration ev = vUsers.elements();
			if (vUsers.size() > 0) { out.println("<SUBSCRIBERS>"); }
			while (ev.hasMoreElements()) {
				u = (User) ev.nextElement();

				out.println ("   <SUBSCRIBER> ");
				out.println ("      <ID>"+u.GetDN()+","+lc.GetDC()+"</ID> ");
				out.println ("      <EMAIL>"+u.GetEmail()+"</EMAIL>");
				out.println ("      <PASSWORD>"+u.GetPassword()+"</PASSWORD>");
				out.println ("      <NAME>"+u.GetFullname()+"</NAME>");
				out.println ("      <EMAILSTATUS>"+u.GetEmailStatus()+"</EMAILSTATUS>");
				out.println ("      <SCORETHRESHOLD>"+u.GetScoreThreshold()+"</SCORETHRESHOLD>");
				out.println ("      <RESULTSPERPAGE>"+u.GetResultsPerPage()+"</RESULTSPERPAGE>");
				out.println ("      <USERSTATUS>"+u.GetUserStatus()+"</USERSTATUS>");
				out.println ("   </SUBSCRIBER>");
			}
			if (vUsers.size() > 0) { out.println("</SUBSCRIBERS>"); }
		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
