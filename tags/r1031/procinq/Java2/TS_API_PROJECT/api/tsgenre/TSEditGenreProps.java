package api.tsgenre;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Modify a folder in the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  FolderID    Unique identifier corresponding to this folder.
 *	@param  FolderName  Name of the newly created folder.
 *  @param  FolderStatus    Status of the folder, 1 is active, 0 is inactive, -1 is marked for deletion.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsgenre.TSEditGenreProps&FolderID=22&FolderName=Mike&FolderStatus=1&SKEY=993135977

 *	@return An error on failure.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CLASSLOAD>Sun Apr 07 18:17:24 EDT 2002</CLASSLOAD>
        <CALLCOUNT>743</CALLCOUNT>
        <TIMEOFCALL_MS>381</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSEditGenreProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// NOTE : required fields MUST appear first in the list above.
		String sTableName = "genre";
		int iNumRequiredFields = 1; 
		String[] sArrFields_URLNameSpace = {
			"FolderID",		// required
			"FolderName",
			"FolderStatus" };

		String[] sArrFields_DBNameSpace = {
			"GenreID",		// required
			"GenreName",
			"GenreStatus" };

		/*
		SQL> e genre;
		 Name                                      Null?    Type
		 ----------------------------------------- -------- ----------------------------
		 GENREID                                   NOT NULL NUMBER(4)
		 GENRENAME                                          VARCHAR2(20)
		 GENRESTATUS                                        NUMBER(1)
		*/

		try 
		{
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			String sSQL = api.sqlinteract.SQLModelUpdate.genSQLUpdate ( sTableName, 
														  props,
														  sArrFields_URLNameSpace,
														  sArrFields_DBNameSpace,
														  iNumRequiredFields
															);
			
			out.println ( "<DEBUG>SQL:" + sSQL + "</DEBUG>"); 
			out.flush();
			Statement stmt = null;
			try
			{
				stmt = dbc.createStatement();	

				// If query statement failed, throw an exception
				if (stmt.executeUpdate (sSQL) == 0) {
					out.println ( "<DEBUG>The specified record does not exist to update</DEBUG>"); 
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
				}
				out.println("<SUCCESS>1</SUCCESS>");
			}
			catch ( Exception e ) 
			{
				throw new TSException (	EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION,  
					"e.msg ["	+ e.getMessage() + "] " +
					" SQL ["	+ sSQL.toString() + "] " +
					" e.stk ["	+ api.Log.stackTraceToString(e) + "]" );
			}
			finally 
			{
				//out.println (EmitValue.genValue ("DEBUG" , "edit update operation complete"  ));
				stmt.close();
			}
		}
		// Catch exception
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
