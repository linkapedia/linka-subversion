package api.security;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Sets configuration values in the system.
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  {PARAM} Argument list is dynamic, apply list of name-value.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=security.TSSetConfigParams&LicenseKey=9201jdfj10j11&SKEY=-594900700

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>Log entries updated successfully.</SUCCESS>
        <CLASSLOAD>Mon Apr 08 20:59:48 EDT 2002</CLASSLOAD>
        <CALLCOUNT>476</CALLCOUNT>
        <TIMEOFCALL_MS>360</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSSetConfigParams
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
		
			// for authorization purposes, need to know the corpus id of this node
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			Enumeration eS = props.keys();
			boolean bSuccess = true;
		
			// pass all parameters along to the API
			while (eS.hasMoreElements()) {
				String sK = (String) eS.nextElement();
				String sV = (String) props.get(sK);
					
				sV = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sV,"'","'''"));
				if ((!sK.toLowerCase().equals("skey")) && 
					(!sK.toLowerCase().equals("emitxml")) &&
					(!sK.toLowerCase().equals("fn"))) {
					String sSQL = " update ConfigParams set ParamValue = '"+sV+"' "+
								  " where upper(ParamName) = upper('"+sK+"') ";
					Statement stmt = dbc.createStatement();	
					if (stmt.executeUpdate (sSQL) == 0) {
						api.Log.LogError("Warning: could not update ParamName "+sK+" with ParamValue "+sV+"\r\n");
						bSuccess = false;
					}
					stmt.close();
				}
			}
			if (bSuccess) { out.println("<SUCCESS>Log entries updated successfully.</SUCCESS>"); }
		} catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}
