package api.security;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import javax.naming.*;
import javax.naming.directory.*;

import com.iw.system.User;

import com.indraweb.execution.Session;
import com.indraweb.database.*;

import java.util.Hashtable;

import java.security.*;

/**
 * This API logs the user out of the Taxonomy Server.
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=security.TSLogout&SKEY=993135977

 *	@return	none
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CLASSLOAD>Mon Apr 08 20:59:48 EDT 2002</CLASSLOAD>
        <CALLCOUNT>476</CALLCOUNT>
        <TIMEOFCALL_MS>360</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSLogout
{
	// Log the user into the system and return a session key
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get ("SKEY", true);
		if (com.indraweb.execution.Session.htUsers.containsKey(sKey)) {
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
			com.indraweb.execution.Session.htUsers.remove(u);
			com.indraweb.execution.Session.htUsers.remove(sKey);
		}
	}
}
