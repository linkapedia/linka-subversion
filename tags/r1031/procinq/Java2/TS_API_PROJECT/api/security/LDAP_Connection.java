package api.security;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.security.*;

import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;

import api.TSException;
import api.emitxml.*;

import com.indraweb.execution.Session;
import com.iw.system.*;

public class LDAP_Connection
{
	private Hashtable htEnvironment = new Hashtable();
	private String sDomainController = null;
	private String sLdapHost = null;
	private String sLdapPort = null;
	private String sLdapUsername = null;
	private String sLdapPassword = null;
	private int Authentication = 0;
	private DirContext ctx = null;
	private PrintWriter out = null;
    private boolean bSecure = false;

	// Accessor methods to private variables
	public Hashtable GetHash() { return htEnvironment; }
	public void SetHash(Hashtable ht) {
        Enumeration eH = ht.keys();
        while (eH.hasMoreElements()) {
            String k = (String) eH.nextElement();
            String v = (String) ht.get(k);
        }
        this.htEnvironment = ht;
    }
	
	public String GetDC() { return sDomainController; }
	public String GetLdapHost() { return sLdapHost; }
	public String GetLdapPort() { return sLdapPort; }
	public String GetLdapUsername() { return sLdapUsername; }
	public String GetLdapPassword() { return sLdapPassword; }
	public int GetAuthentication() { return Authentication; }
	public String GetLdap() { return "ldap://"+sLdapHost+":"+sLdapPort+"/"+sDomainController; }
	public DirContext getDirContext() { return ctx; }
	public PrintWriter getOut() { return out; }

	// Object constructor -- draw defaults from configs
	public LDAP_Connection(PrintWriter out) {
		try {
			this.out = out;
			
			this.sDomainController = Session.cfg.getProp("DC");
			this.sLdapHost = Session.cfg.getProp("LdapHost");
			this.sLdapPort = Session.cfg.getProp("LdapPort");
			if (Session.cfg.getProp("AuthenticationSystem").equals("ActiveDirectory")) {
				this.Authentication = 1; 
			} else { this.Authentication = 0; }
		
			Hashtable hashtableEnvironment = this.htEnvironment;
			hashtableEnvironment.put(
				Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory"
			);

			hashtableEnvironment.put(Context.PROVIDER_URL, GetLdap());
		
			SetHash(hashtableEnvironment);
			this.ctx = new InitialDirContext(hashtableEnvironment);
		}
		catch (NamingException ne) { return; }
		catch (Exception e) { return; }
	}
	public LDAP_Connection(String sUsername, String sPassword, PrintWriter out) {

		this.out = out;
		try {
			this.sDomainController = Session.cfg.getProp("DC");
			this.sLdapHost = Session.cfg.getProp("LdapHost");
			this.sLdapPort = Session.cfg.getProp("LdapPort");

            // added 04/15/04 -- if security setting is NONE, bypass security
            if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
                this.sLdapUsername = sUsername;
                this.sLdapPassword = sPassword;
                return;
            }

			if (Session.cfg.getProp("AuthenticationSystem").equals("ActiveDirectory")) {
				this.Authentication = 1; 
				
				// If the authentication mechanism is Active Directory, we do not
				//  have the distinguished name as the username.  Instead we were sent
				//  the sAMAccountName.   So, what we need to do is look up the 
				//  distinguished name based on the account name, using the LDAP
				//  ADMINISTRATOR's username and password.
				if (!sUsername.toUpperCase().startsWith("CN=")) {
					this.sLdapUsername = Session.cfg.getProp("LdapUsername");
					this.sLdapPassword = Session.cfg.getProp("LdapPassword");
				
					Hashtable hashtableEnvironment = GetHash();
					hashtableEnvironment.put(
						Context.INITIAL_CONTEXT_FACTORY,
						"com.sun.jndi.ldap.LdapCtxFactory"
					);

                    if (Session.cfg.getPropBool("UseSSL", false, "false")) {
                        api.Log.Log("Using SSL authentication .. ");
                        String keystore = Session.cfg.getProp("KeystoreFile");
                        String keypw = Session.cfg.getProp("KeystorePass");
                        System.setProperty("javax.net.ssl.trustStore", keystore);
                        System.setProperty("javax.net.ssl.keyStore", keystore);
                        System.setProperty("javax.net.ssl.keyStorePassword", keypw);

                        hashtableEnvironment.put(
                                Context.SECURITY_PROTOCOL,
                                "ssl"
                        );
                    }

					hashtableEnvironment.put(Context.SECURITY_AUTHENTICATION,"simple");
					hashtableEnvironment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
					hashtableEnvironment.put(Context.PROVIDER_URL, GetLdap());
					hashtableEnvironment.put(Context.SECURITY_PRINCIPAL, sLdapUsername);
					hashtableEnvironment.put(Context.SECURITY_CREDENTIALS, sLdapPassword);

					SetHash(hashtableEnvironment);
					Context ct = new InitialContext(hashtableEnvironment); ct.close();
					DirContext ctx = new InitialDirContext(hashtableEnvironment);
				
		            SearchControls constraints = new SearchControls();
					constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
					NamingEnumeration sre = ctx.search("", "(sAMAccountName="+sUsername+")", constraints);
					SearchResult sr = (SearchResult) sre.next();
					Attributes attrs = sr.getAttributes();
					Attribute aUser = attrs.get("distinguishedName");
					sUsername = (String) aUser.get();
					ctx.close();
				}
				
			} else { this.Authentication = 0; }
			
			this.sLdapUsername = sUsername;
			this.sLdapPassword = sPassword;
		
			Hashtable hashtableEnvironment = GetHash();
			hashtableEnvironment.put(
				Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory"
			);

            if (Session.cfg.getPropBool("UseSSL", false, "false")) {
                api.Log.Log("Using SSL authentication .. ");
                String keystore = Session.cfg.getProp("KeystoreFile");
                String keypw = Session.cfg.getProp("KeystorePass");
                System.setProperty("javax.net.ssl.trustStore", keystore);
                System.setProperty("javax.net.ssl.keyStore", keystore);
                System.setProperty("javax.net.ssl.keyStorePassword", keypw);

                hashtableEnvironment.put(
                        Context.SECURITY_PROTOCOL,
                        "ssl"
                );
            }

            hashtableEnvironment.put(Context.SECURITY_AUTHENTICATION, "simple");
			hashtableEnvironment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			hashtableEnvironment.put(Context.PROVIDER_URL, GetLdap());
			hashtableEnvironment.put(Context.SECURITY_PRINCIPAL, sLdapUsername);
			hashtableEnvironment.put(Context.SECURITY_CREDENTIALS, sLdapPassword);
			
			SetHash(hashtableEnvironment);
			Context ctx = new InitialContext(hashtableEnvironment); ctx.close();
			this.ctx = new InitialDirContext(hashtableEnvironment);
		}
		catch (javax.naming.AuthenticationException ae) { 
			EmitGenXML_ErrorInfo.emitException 
			( "TSException", 
			  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD),
			  out );			
			return;
		}
		catch (NamingException ne) { api.Log.LogError(ne); return; }
		catch (Exception e) {  api.Log.LogError(e); return; }
	}

	public LDAP_Connection Refresh_Connection () {
            // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return this;
        }

        try {
			Hashtable hashtableEnvironment = GetHash();
			hashtableEnvironment.put(
				Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory"
			);

			hashtableEnvironment.put(Context.PROVIDER_URL, GetLdap());
			hashtableEnvironment.put(Context.SECURITY_PRINCIPAL, sLdapUsername);
			hashtableEnvironment.put(Context.SECURITY_CREDENTIALS, sLdapPassword);
			
            if (Session.cfg.getPropBool("UseSSL", false, "false")) {
                api.Log.Log("Using SSL authentication .. ");
                String keystore = Session.cfg.getProp("KeystoreFile");
                String keypw = Session.cfg.getProp("KeystorePass");
                System.setProperty("javax.net.ssl.trustStore", keystore);
                System.setProperty("javax.net.ssl.keyStore", keystore);
                System.setProperty("javax.net.ssl.keyStorePassword", keypw);

                hashtableEnvironment.put(
                        Context.SECURITY_PROTOCOL,
                        "ssl"
                );
            }

			SetHash(hashtableEnvironment);
			Context ctx = new InitialContext(hashtableEnvironment); ctx.close();
			this.ctx = new InitialDirContext(hashtableEnvironment);
		}
		catch (javax.naming.AuthenticationException ae) { 
			EmitGenXML_ErrorInfo.emitException 
			( "TSException", 
			  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD),
			  out );			
			return null;
		}
		catch (NamingException ne) { ne.printStackTrace(out); return null; }
		catch (Exception e) { e.printStackTrace(out); return null; }
		
		return this;
	}

    // close this LDAP connection
    public void close () {
        try { if (ctx != null) { this.ctx.close(); }} catch (Exception e) { api.Log.LogError(e); }
    }

	// Return all users in "Group"
	//   Once again, alternately try ActiveDirectory then OpenLDAP
	public Vector GetUsers (Group group) {
		if (GetAuthentication() == 1) { return GetUsers(group, "group"); }
		return GetUsers(group, "groupofNames");	
	}
	public Vector GetUsers (Group group, String sObjectClass) {
        // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return new Vector();
        }

		Hashtable hashtableEnvironment = GetHash();
		DirContext ctx = getDirContext();
		Vector vUsers = new Vector();
		String DN = group.GetDN();

		try {
			if (DN.endsWith(this.GetDC())) {
				DN = DN.substring(0, DN.length() - (this.GetDC().length()+1));
			}
			
		    SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.OBJECT_SCOPE);
			NamingEnumeration sre = ctx.search(DN, "(objectClass="+sObjectClass+")", constraints);
			SearchResult sr = (SearchResult) sre.next();
			Attributes attrs = sr.getAttributes();
			
		    NamingEnumeration ae = attrs.getAll() ;
			while (ae.hasMore()) {
				Attribute attr = (Attribute)ae.next();
				if (attr.getID().equals("member")) { 
					NamingEnumeration ex = attr.getAll();
					while (ex.hasMore()) { 
						String sDN = (String) ex.next();
						if (sDN.endsWith(this.GetDC())) {
							sDN = sDN.substring(0, sDN.length() - (this.GetDC().length()+1));
						}
						Refresh_Connection();
						User u = GetUser(sDN);
						if (u != null) { vUsers.addElement(u); }
					} 
				}
			}
			ctx.close();
		} catch (AuthenticationException e) { 
			api.Log.LogError(e);
			EmitGenXML_ErrorInfo.emitException 
				( "TSException", 
				  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD), 
				  out );				
		} catch (Exception e) { api.Log.LogError(e); return vUsers; }
	
		
		return vUsers;
	}

	// Given a DN, get a specific user
	public User GetUser (String DN) {
        // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return new User();
        }

		try {
			Hashtable hashtableEnvironment = GetHash();
			DirContext ctx = getDirContext();
			User user;

			if (DN.toUpperCase().endsWith(this.GetDC().toUpperCase())) {
				DN = DN.substring(0, DN.length() - (this.GetDC().length()+1));
			}
			
            SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.OBJECT_SCOPE);
			NamingEnumeration sre = ctx.search(DN, "(objectClass=*)", constraints);
			SearchResult sr = (SearchResult) sre.next();
			Attributes attrs = sr.getAttributes();
				
	        NamingEnumeration ae = attrs.getAll() ;
			user = new User();
			user.SetDN(DN);
			
			// Loop through LDAP attributes and set them in user.
			while (ae.hasMore()) {
				Attribute attr = (Attribute)ae.next() ;
					
				// Java language does not support Strings in SWITCH statements! (doh!)
				// Looks like an if then elseif scheme is necessary here...
					
				if (attr.getID().equals("mail")) { user.SetEmail((String) attr.get()); }
				else if (attr.getID().equals("sn")) { user.SetUsername((String) attr.get()); }
				else if (attr.getID().equals("cn")) { user.SetFullname((String) attr.get()); }
				else if (attr.getID().equals("scoreThreshold")) { 
					user.SetScoreThreshold(new Float((String) attr.get()).floatValue()); }
				else if (attr.getID().equals("resultsPerPage")) { 
					user.SetResultsPerPage(new Integer((String) attr.get()).intValue()); }
				else if (attr.getID().equals("userStatus")) { 
					user.SetUserStatus(new Integer((String) attr.get()).intValue()); }
				else if (attr.getID().equals("emailStatus")) { 
					user.SetEmailStatus(new Integer((String) attr.get()).intValue()); }
				else if (attr.getID().equals("nodesTracked")) { 
					NamingEnumeration ex = attr.getAll();
					while (ex.hasMore()) { user.GrantNodeTracked((String) ex.next()); }}
                else if (attr.getID().equals("wWWHomePage")) { user.SetDomainsAllowed((String) attr.get()); }
 				else if (attr.getID().equals("expertNodes")) {
					NamingEnumeration ex = attr.getAll();
					while (ex.hasMore()) {
						String sExpert = (String) ex.next();
						String sNodeID = sExpert.substring(0, sExpert.indexOf("A"));
						String sAccess = sExpert.substring(sExpert.indexOf("A")+1, sExpert.length());
						user.GrantNodeExpert(sNodeID, sAccess); 
					}
				}
			}
			ctx.close();
			return user;
		} catch (AuthenticationException e) { 
			EmitGenXML_ErrorInfo.emitException 
				( "TSException", 
				  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD), 
				  out );				
			return null;
		} catch (Exception e) {  api.Log.LogError(e); return null; }
	}
	
	public Vector GetAllUsers () {
		if (GetAuthentication() == 1) { return GetUsers("user"); }
		return GetUsers("inetOrgPerson");
	}

	public Vector GetIndraUsers () {
		return GetUsers("Indraperson");
	}

	// Return all users from object class "inetOrgPerson"
	public Vector GetUsers (String sObjectClass) {
		Vector vUsers = new Vector();

        // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return vUsers;
        }

		try {
			Hashtable hashtableEnvironment = GetHash();

			DirContext dx = getDirContext();
            SearchControls constraints = new SearchControls();
            constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration sre = dx.search("", "(objectClass="+sObjectClass+")", constraints);

			// for each USER...
			while (sre.hasMore()) {
				SearchResult sr = (SearchResult) sre.next();
				Attributes attrs = sr.getAttributes();
				
		        NamingEnumeration ae = attrs.getAll() ;
				User user = new User();
				user.SetDN(sr.getName());
				
				// Loop through LDAP attributes and set them in user.
				while (ae.hasMore()) {
					Attribute attr = (Attribute)ae.next() ;
					
					// Java language does not support Strings in SWITCH statements! (doh!)
					// Looks like an if then elseif scheme is necessary here...
					
					if (attr.getID().equals("mail")) { user.SetEmail((String) attr.get()); }
					else if (attr.getID().equals("sn")) { user.SetUsername((String) attr.get()); }
					else if (attr.getID().equals("cn")) { user.SetFullname((String) attr.get()); }
					else if (attr.getID().equals("scoreThreshold")) { 
						user.SetScoreThreshold(new Float((String) attr.get()).floatValue()); }
					else if (attr.getID().equals("resultsPerPage")) { 
						user.SetResultsPerPage(new Integer((String) attr.get()).intValue()); }
					else if (attr.getID().equals("userStatus")) { 
						user.SetUserStatus(new Integer((String) attr.get()).intValue()); }
					else if (attr.getID().equals("emailStatus")) { 
						user.SetEmailStatus(new Integer((String) attr.get()).intValue()); }
					else if (attr.getID().equals("nodesTracked")) { 
						NamingEnumeration ex = attr.getAll();
						while (ex.hasMore()) { user.GrantNodeTracked((String) ex.next()); }}
					else if (attr.getID().equals("expertNodes")) { 
						NamingEnumeration ex = attr.getAll();
						while (ex.hasMore()) {
							String sExpert = (String) ex.next();
							String sNodeID = sExpert.substring(0, sExpert.indexOf("A"));
							String sAccess = sExpert.substring(sExpert.indexOf("A")+1, sExpert.length());
							user.GrantNodeExpert(sNodeID, sAccess); 
						}
					}
				}
				vUsers.addElement(user);
			}

			dx.close();

		} catch (AuthenticationException e) { 
			EmitGenXML_ErrorInfo.emitException 
				( "TSException", 
				  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD), 
				  out );				
		} catch (NamingException e) { new Vector(); 
		} catch (Exception e) { return new Vector(); }
		
		return vUsers;
	}

	// Given a DN, get a specific group
	public Group GetIndraGroup (String DN) { return GetGroup(DN, "Indragroup"); }
	public Group GetGroup (String DN) {
		if (GetAuthentication() == 1) { return GetGroup(DN, "group"); }
		return GetGroup(DN, "groupofNames"); 
	}
	public Group GetGroup (String DN, String objectClass) {
        // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return new Group();
        }

		try {
			Hashtable hashtableEnvironment = GetHash();
			DirContext ctx = getDirContext();
			Group group; String SearchDN;

			if (DN.endsWith(this.GetDC())) {
				SearchDN = DN.substring(0, DN.length() - (this.GetDC().length()+1));
			} else { SearchDN = DN; }

            SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.OBJECT_SCOPE);
			NamingEnumeration sre = ctx.search(SearchDN, "(objectClass=*)", constraints);
            //api.Log.Log("GetGroup: "+DN+" calling search with "+SearchDN);

			SearchResult sr = (SearchResult) sre.next();
			Attributes attrs = sr.getAttributes();

	        NamingEnumeration ae = attrs.getAll() ;
			group = new Group();
			group.SetDN(DN);

			// Loop through LDAP attributes and set them in Group
			while (ae.hasMore()) {
				Attribute attr = (Attribute)ae.next() ;
 				if (attr.getID().equals("cn")) { group.SetGroupName((String) attr.get()); }
                else if (attr.getID().equals("description")) { group.SetDisplayName((String) attr.get()); }
				else if (attr.getID().equals("corpusAccess")) {
					NamingEnumeration ex = attr.getAll();
					while (ex.hasMore()) { group.GrantCorpus((String) ex.next()); }
				} else if (attr.getID().equals("adminAccess")) { 
					NamingEnumeration ex = attr.getAll();
					while (ex.hasMore()) { group.GrantAdmin((String) ex.next()); }
				}
			}
			ctx.close();
			return group;
		} catch (AuthenticationException e) {
            api.Log.Log("Error 1."); api.Log.LogError(e);
			EmitGenXML_ErrorInfo.emitException ( "TSException",
				  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD), 
				  out );				
			return null;
		} catch (Exception e) { api.Log.Log("Error 2."); api.Log.LogError(e); return null; }
	}
	
	// Get all groups of a specific class 
	public Hashtable GetAllGroups () { 
		if (GetAuthentication() == 1) { return GetGroups("group"); }
		return GetGroups("groupofNames"); 
	}
	public Hashtable GetIndraGroups () { return GetGroups("Indragroup"); }

	// Get all groups associated with "User"
	//   ActiveDirectory and OPENLDAP support different styles.   
	public Hashtable GetGroups (User u) {
		if (GetAuthentication() == 1) { return GetGroupsByUser(u, "user"); }
		return GetGroupsByMember(u, "organizationalPerson");
	}

    public Hashtable GetGroupsByMember (User u, String sObjectClass) {
        Hashtable UserGroups = new Hashtable();

        // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            UserGroups.put("Everyone", new Group("Everyone", "Everyone"));
            return UserGroups;
        }

        // new: 12/16/02 cache group permissions in the session object
        if (!u.GetGroupPermissions().isEmpty()) {
            return u.GetGroupPermissions();
        }

		try {
			Hashtable hashtableEnvironment = GetHash();
			String DN = u.GetDN();

			if (DN.toLowerCase().endsWith(this.GetDC().toLowerCase())) {
				DN = DN.substring(0, DN.length() - (this.GetDC().length()+1));
			}

            DirContext dx =  getDirContext();
            Attributes matchAttrs = new BasicAttributes(true); // ignore attribute name case
            matchAttrs.put(new BasicAttribute("member", u.GetDN()));

            // Search for objects with those matching attributes
            NamingEnumeration sre = ctx.search("", matchAttrs);

 			SearchResult sr = null;
            while (sre.hasMore()) {
                sr = (SearchResult) sre.next();

                Group group = new Group();
                group.SetDN(sr.getName());

                Attributes attrs = sr.getAttributes();
                NamingEnumeration ae = attrs.getAll() ;

                // Loop through LDAP attributes and set them in Group
                // ******************* THIS PART IS STILL IN PROGRESS *******************
                while (ae.hasMore()) {
                    Attribute attr = (Attribute)ae.next() ;
                    if (attr.getID().equals("cn")) { group.SetGroupName((String) attr.get()); }
                    else if (attr.getID().equals("description")) { group.SetDisplayName((String) attr.get()); }
                    else if (attr.getID().equals("corpusAccess")) {
                        NamingEnumeration ex = attr.getAll();
                        while (ex.hasMore()) { group.GrantCorpus((String) ex.next()); }
                    } else if (attr.getID().equals("adminAccess")) {
                        NamingEnumeration ex = attr.getAll();
                        while (ex.hasMore()) { group.GrantAdmin((String) ex.next()); }
                    }
                }
                UserGroups.put(group.GetDN(), group);
 			}
 			dx.close();

		} catch (AuthenticationException e) {
            api.Log.LogError(e);
			EmitGenXML_ErrorInfo.emitException
				( "TSException",
				  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD),
				  out );
		} catch (NamingException e) { api.Log.LogError(e); return new Hashtable();
		} catch (Exception e) { api.Log.LogError(e); return new Hashtable(); }

        u.SetGroupPermissions(UserGroups);
		return UserGroups;
	}

	public Hashtable GetGroupsByUser (User u, String sObjectClass) {
        Hashtable UserGroups = new Hashtable();

        // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            UserGroups.put("No security", "No security");
            return UserGroups;
        }

        // new: 12/16/02 cache group permissions in the session object
        if (!u.GetGroupPermissions().isEmpty()) {
            return u.GetGroupPermissions();
        }

		try {
			Hashtable hashtableEnvironment = GetHash();
			String DN = u.GetDN();

			if (DN.toLowerCase().endsWith(this.GetDC().toLowerCase())) {
				DN = DN.substring(0, DN.length() - (this.GetDC().length()+1));
			}
						
			DirContext dx = getDirContext();
		    SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.OBJECT_SCOPE);

 			NamingEnumeration sre = dx.search(DN, "(objectClass="+sObjectClass+")", constraints);

 			SearchResult sr = null;
            // if SRE is null, no results were found
            try { sr = (SearchResult) sre.next(); }
            catch (Exception e) { api.Log.Log("No groups found for user: "+u.GetDN()); throw new Exception(e); }

			Attributes attrs = sr.getAttributes();

		    NamingEnumeration ae = attrs.getAll() ;

			while (ae.hasMore()) {
				Attribute attr = (Attribute)ae.next();
				if (attr.getID().equals("memberOf")) {
					NamingEnumeration ex = attr.getAll();
					while (ex.hasMore()) { 
						String sDN = (String) ex.next();
						if (sDN.toLowerCase().endsWith(this.GetDC().toLowerCase())) {
							sDN = sDN.substring(0, sDN.length() - (this.GetDC().length()+1));
						}
						Refresh_Connection();
						Group g = GetGroup(sDN);
						if (g != null) { UserGroups.put(g.GetDN(), g); }
					} 
				}
			}
			dx.close();

		} catch (AuthenticationException e) { 
			EmitGenXML_ErrorInfo.emitException 
				( "TSException", 
				  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD),
				  out );				
		} catch (NamingException e) { api.Log.LogError(e); return new Hashtable(); 
		} catch (Exception e) { api.Log.LogError(e); return new Hashtable(); }
		
        u.SetGroupPermissions(UserGroups);
		return UserGroups;
	}
	
	// Return all users from object class "inetOrgPerson"
	public Hashtable GetGroups (String sObjectClass) { return GetGroups(sObjectClass, false); }
	public Hashtable GetGroups (String sObjectClass, boolean Refresh) {
        // added 04/15/04 -- if security setting is NONE, bypass security
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return new Hashtable();
        }

		// Groups are stored in our server cache.   Check there first.
		//Refresh = true; // Temporarily turn off caching MAP 02/06/02
		if (!Session.htGroups.isEmpty() && !Refresh) { return Session.htGroups; }

		try {
			Hashtable hashtableEnvironment = GetHash();
			
			DirContext dx = getDirContext();
            SearchControls constraints = new SearchControls();
            constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration sre = dx.search("", "(objectClass="+sObjectClass+")", constraints);

			// for each GROUP found...
			while (sre.hasMore()) {
				SearchResult sr = (SearchResult) sre.next();
				Attributes attrs = sr.getAttributes();
				
		        NamingEnumeration ae = attrs.getAll() ;
				Group group = new Group();
				group.SetDN(sr.getName());
				
				// Loop through LDAP attributes and set them in Group
				while (ae.hasMore()) {
					Attribute attr = (Attribute)ae.next() ;
					if (attr.getID().equals("cn")) { group.SetGroupName((String) attr.get()); }
                    else if (attr.getID().equals("description")) { group.SetDisplayName((String) attr.get()); }
					else if (attr.getID().equals("corpusAccess")) {
						NamingEnumeration ex = attr.getAll();
						while (ex.hasMore()) { group.GrantCorpus((String) ex.next()); }
					} else if (attr.getID().equals("adminAccess")) { 
						NamingEnumeration ex = attr.getAll();
						while (ex.hasMore()) { group.GrantAdmin((String) ex.next()); }
					}
				}
				Session.htGroups.put(group.GetDN(), group);
			}

			dx.close();

		} catch (AuthenticationException e) { 
			EmitGenXML_ErrorInfo.emitException 
				( "TSException", 
				  new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD), 
				  out );				
		} catch (NamingException e) { return null;  
		} catch (Exception e) { return null; }
		
		return Session.htGroups;
	}
					  
}
