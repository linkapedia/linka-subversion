/* DocumentSecurity.java

 This is a static class with various library functions relating to ITS document security.

 */
package api.security;

import com.iw.system.SecurityGroup;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class DocumentSecurity {

    private static Hashtable htSecurityHash = null;
    private static Hashtable htSecurityByName = null;

    // load all LDAP security groups into memory.  If there are LDAP groups that do not exist in the
    //   database, add them now.
    public static synchronized Hashtable getSecurityHash(Connection dbc) throws Exception {
        if (htSecurityHash == null) {
            htSecurityHash = new Hashtable();
            htSecurityByName = new Hashtable();

            // get all groups from LDAP
            LDAP_Connection lc = new LDAP_Connection(com.indraweb.execution.Session.cfg.getProp("LdapUsername"),
                    com.indraweb.execution.Session.cfg.getProp("LdapPassword"),
                    new PrintWriter(System.out));

            Hashtable htGroups = lc.GetAllGroups();

            String sSQL = "SELECT securityid, distinguishedname FROM security";

            Statement stmt = null;
            ResultSet rs = null;
            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    SecurityGroup sg = new SecurityGroup(rs);
                    htSecurityHash.put(sg.getID(), sg);
                    htSecurityByName.put(sg.getName().toUpperCase(), sg);

                    try {
                        htGroups.remove(sg.getName());
                    } catch (Exception e) {
                    }
                }

                api.Log.Log("Loaded " + htSecurityHash.size() + " security groups successfully.");
            } catch (Exception e) {
                api.Log.LogError(e);
                return new Hashtable();
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            // if there are any groups in LDAP that were not found in the database, add them now
            Enumeration eH = htGroups.elements();
            while (eH.hasMoreElements()) {
                Group g = (Group) eH.nextElement();

                sSQL = "insert into security (DISTINGUISHEDNAME) values ('" + g.GetDN() + "')";
                stmt = null;

                try { // update the document table
                    stmt = dbc.createStatement();
                    if (stmt.executeUpdate(sSQL) == 0) {
                        api.Log.Log("Insert security row failed (" + sSQL + ").");
                    }
                } catch (Exception e) {
                    api.Log.Log("Insert security row failed (" + sSQL + ").");
                } finally {
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                }
            }

        }

        return htSecurityHash;
    }

    // given a vector of security groups, return a valid HEXMAP
    public static String getSecurity(Vector vSecurityGroups) {
        char[] binaryMap = new char[1600];

        for (int i = 0; i < 1600; i++) {
            binaryMap[i] = '0';
        }

        for (int i = 0; i < vSecurityGroups.size(); i++) {
            SecurityGroup sg = (SecurityGroup) vSecurityGroups.elementAt(i);
            if (sg != null) {
                binaryMap[1599 - Integer.parseInt(sg.getID())] = '1';
            }
        }

        BigInteger bi = new BigInteger(new String(binaryMap), 2);

        return bi.toString(16);
    }

    // given a hashset of security IDs, return a valid HEXMAP
    public static String getSecurity(HashSet hsSecurity) {
        char[] binaryMap = new char[1600];

        for (int i = 0; i < 1600; i++) {
            binaryMap[i] = '0';
        }

        Iterator i = hsSecurity.iterator();
        while (i.hasNext()) {
            String securityID = (String) i.next();
            binaryMap[1599 - Integer.parseInt(securityID)] = '1';
        }
        binaryMap[1599] = '1'; // first field used for global access

        BigInteger bi = new BigInteger(new String(binaryMap), 2);

        return bi.toString(16);
    }

    // given a hexmap and the current security hash, return active security groups
    public static Vector getSecurity(String sHexMap) {
        Vector v = new Vector();

        BigInteger bi = new BigInteger(sHexMap, 16);
        char[] binaryMap = bi.toString(2).toCharArray();

        for (int i = binaryMap.length - 1; i >= 0; i--) {
            if (binaryMap[i] == '1') {
                v.add(htSecurityHash.get(((binaryMap.length - 1) - i) + ""));
            }
        }

        return v;
    }

    // return true/false on security permissions given two hex maps
    public static synchronized boolean isAuthorized(String sDocHexMap, String sUserHexMap) {
        BigInteger doc = new BigInteger(sDocHexMap, 16);
        BigInteger user = new BigInteger(sUserHexMap, 16);

        // AND the two bitmaps
        BigInteger bi = doc.and(user);
        long i = bi.getLowestSetBit();
        //api.Log.Log("doc: "+doc+" user: "+user+" lowestbit: "+i);
        if (i >= 0) {
            return true;
        }
        if (doc == user) {
            return true;
        }

        return false;
    }

    public static char[] emptyBitMap() {
        char[] binaryMap = new char[1600];
        for (int i = 0; i < 1600; i++) {
            binaryMap[i] = '0';
        }

        return binaryMap;
    }

    public static void propagateCompatibility(Connection dbc) throws Exception {
        String sSQL = "SELECT documentid, securityid FROM documentsecurity order by documentid";
        Statement stmt = null;
        ResultSet rs = null;

        String previousID = "";
        char[] binaryMap = emptyBitMap();

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) {
                String documentID = rs.getString(1);
                int securityID = rs.getInt(2);

                if ((!documentID.equals(previousID)) && (!previousID.equals(""))) {
                    updateSecurity(previousID, binaryMap);

                    previousID = documentID;
                    binaryMap = emptyBitMap();
                }
                if (previousID.equals("")) {
                    previousID = documentID;
                }

                binaryMap[1599 - securityID] = '1';
            }

            updateSecurity(previousID, binaryMap);

        } catch (Exception e) {
            api.Log.LogError(e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }

    public static SecurityGroup getSecurityByName(String Name) {
        return (SecurityGroup) htSecurityByName.get(Name.toUpperCase());
    }

    public static SecurityGroup getSecurityByID(String ID) {
        return (SecurityGroup) htSecurityHash.get(ID);
    }

    private static void updateSecurity(String documentID, char[] binaryMap) {
        BigInteger bi = new BigInteger(new String(binaryMap), 2);

        String sSQL = "update document set documentsecurity = " + bi.toString(16) + " where documentid = " + documentID;
        System.out.println(sSQL);
    }
}
