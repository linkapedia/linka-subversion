package api;

import api.emitxml.EmitGenXML_ErrorInfo;
import api.tests.TestItem;
import com.indraweb.execution.Session;
import com.indraweb.execution.ThreadInfoTestAssertion;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilStrings;
import com.iw.application.context.AppServletContextListener;
import com.iw.rest.beans.Classifier;
import com.iw.system.User;
import com.iw.tools.Dom4jHelper;
import java.io.*;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.dom4j.Element;

public class APIHandler {

    private static String sServerUpSince = null;
    private static int iCallCounter = 0;
    private static final Integer ICallCounterSemaphore = new Integer(0); // dummy
    // number -
    private static Hashtable<Thread, Boolean> htBooleanEmittingXML = new Hashtable<Thread, Boolean>();
    private static boolean bDebugging = false;
    public static final String sXMLHEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
    public static final String sCDMSTESTDIRHome = "/CDMStest/api/";
    public static final String sCDMSTESTFILEOnly = "CDMSTestCasesAPI.xml";
    public static final String sCDMSTESTFILE = sCDMSTESTDIRHome + sCDMSTESTFILEOnly;
    private static boolean bGotServerUpSince = false;
    private static String sXMLOutFileName_Trigger = null;
    private static String sXMLOutFileName_ArgsYN = null;
    private static String sXMLOutFileName_XML = null;
    private static final Object oCallCtr_classify_semaphore = new Object();
    private static int iCallCtr_classify = 0;
    //Log
    private static final Logger log = Logger.getLogger(APIHandler.class);

    public static boolean getEmittingXML() {
        Thread t = Thread.currentThread();
        return ((Boolean) htBooleanEmittingXML.get(t)).booleanValue();
    }

    public static void setEmittingXML(boolean bXMLtrue) {
        Thread t = Thread.currentThread();
        htBooleanEmittingXML.put(t, bXMLtrue);
    }

    public static void doGetMine(HttpServletRequest req, HttpServletResponse res, api.APIProps props, PrintWriter out, Connection dbc, boolean bXML) {
        doGetMine(req, res, props, out, dbc, bXML, false);
    }

    public static void doGetMine(HttpServletRequest req, HttpServletResponse res, api.APIProps props, PrintWriter out, Connection dbc, boolean bXML, boolean bTestCase) {
        log.debug("doGetMine()");
        int icallCounterAtStart = -1;
        // guarantee no two api calls get the same counter even across threads
        synchronized (ICallCounterSemaphore) {
            iCallCounter++;
            icallCounterAtStart = iCallCounter;
        }

        boolean bProp_TestGenerator = Session.cfg.getPropBool("TestGenerator", false, "false");
        if (bProp_TestGenerator == false) {
            bProp_TestGenerator = UtilFile.bFileExists("/temp/IndraDebugTestGeneratorOn.txt");
        }
        if (bProp_TestGenerator) {
            log.debug("test gen on [/temp/IndraDebugTestGeneratorOn.txt]");
        }

        boolean bProp_RunningTest = false;
        try {
            bProp_RunningTest = props.getbool("RunningTest", "false");
        } catch (Exception e) {
            bProp_RunningTest = false;
        }
        long lTestID = -1;
        if (bProp_TestGenerator) {
            log.debug("bProp_TestGenerator [" + bProp_TestGenerator + "]");
            lTestID = getTestSequence();
        }
        // hbk timer com.indraweb.profiler.Timer timer =
        // com.indraweb.profiler.Profiler.getTimer("method doGetMine", true);
        // hbk timer try
        // hbk timer {

        if (sXMLOutFileName_Trigger == null) {
            String sOSHost = (String) java.lang.System.getProperties().get("os.name");
            if (sOSHost.toLowerCase().indexOf("windows") >= 0) {
                sXMLOutFileName_XML = "c:/temp/xml.txt";
                sXMLOutFileName_Trigger = "c:/temp/IndraXMLToFile.txt";
                sXMLOutFileName_ArgsYN = "c:/temp/IndraXMLToFileArgs.txt";
            } else { // linux
                sXMLOutFileName_Trigger = "/tmp/IndraXMLToFile.txt";
                sXMLOutFileName_XML = "/tmp/xml.txt";
                sXMLOutFileName_ArgsYN = "/tmp/IndraXMLToFileArgs.txt";
            }
        }

        if (bProp_TestGenerator || com.indraweb.util.UtilFile.bFileExists(sXMLOutFileName_Trigger)) {
            if (!com.indraweb.util.UtilFile.bFileExists(sXMLOutFileName_Trigger)) {
                sXMLOutFileName_XML = null;
            }

            try {
                if (sXMLOutFileName_XML != null) {
                    log.debug("file [" + sXMLOutFileName_Trigger + "] present, streaming XML to [" + sXMLOutFileName_XML + "]");
                    boolean bIncludeArgs = false;
                    if (com.indraweb.util.UtilFile.bFileExists(sXMLOutFileName_ArgsYN)) {
                        bIncludeArgs = true;
                    }
                    if (bIncludeArgs) {
                        boolean bXMLArgsProgramFormAlso = UtilFile.bFileExists("/temp/IndraXMLToFileArgs_programStyleAlso.txt");

                        StringBuilder sbArgs = new StringBuilder();
                        StringBuilder sbArgsInURLForm = new StringBuilder("http://localhost/itsapi/ts?");
                        StringBuilder sbArgsProgramStyle = new StringBuilder("http://localhost/itsapi/ts?");
                        Enumeration<Object> enumeration = props.keys();
                        int i = 0;

                        sbArgs.append("IndraXMLargs ").append(i).append(". " + "fn" + ":").append((String) props.get("fn")).append("\r\n");
                        sbArgsInURLForm.append("fn" + "=").append((String) props.get("fn"));

                        while (enumeration.hasMoreElements()) {
                            String sPropKey = (String) enumeration.nextElement();
                            if (sPropKey.toLowerCase().equals("fn")) {
                                continue;
                            }
                            sbArgs.append("IndraXMLargs ").append(i).append(". ").append(sPropKey).append(":").append((String) props.get(sPropKey)).append("\r\n");
                            if (i >= 0) {
                                sbArgsInURLForm.append("&");
                            }
                            sbArgsInURLForm.append(sPropKey).append("=").append((String) props.get(sPropKey));
                            if (i >= 0) {
                                sbArgsProgramStyle.append("&");
                            }
                            sbArgsProgramStyle.append(sPropKey).append("=").append((String) props.get(sPropKey)).append("\"+\r\n");
                            i++;
                        }
                        String sArgsProgramStyle = "";
                        if (bXMLArgsProgramFormAlso) {
                            sArgsProgramStyle = " Program Form [\r\n" + sbArgsInURLForm + "]\r\n" + sbArgsProgramStyle.toString();
                        }

                        log.debug("API CALL counter [" + icallCounterAtStart + "] [" + new java.util.Date() + "] --- ARGS : \r\n[\r\n" + sbArgs.toString() + "]" + "] URL [" + sbArgsInURLForm + "]" + sArgsProgramStyle);
                    }
                }
                out = new XMLOutRedirector(out, sXMLOutFileName_XML, true, bProp_TestGenerator);
            } catch (Exception e) {
                log.error("error writing xml to XML out file", e);
            }
        }

        String sWhichDB = null;
        long lTimeStart = System.currentTimeMillis();
        String sFunction = null;

        try {
            sWhichDB = (String) props.get("whichdb", "API");
            // if ( != null )
            // throw new Exception
            // ("reserved word used in URL submission : emitXML");
            if (bXML) {
                out.println(sXMLHEADER);
                props.put("emitXML", "true");
            } else {
                props.put("emitXML", "false");
                out.println((new java.util.Date()).toString());
            }

            // STATIC INITS hbk 2002 04 27
            if (!bGotServerUpSince) {
                bGotServerUpSince = true;
                sServerUpSince = (new java.util.Date()).toString();
            }

            // OUTPUT
            if (bXML) {
                out.println("<TSRESULT>");
                if (props.getbool("showdebug", "false")) {
                    @SuppressWarnings("unchecked")
                    Enumeration<String> enumeration = req.getHeaderNames();
                    while (enumeration.hasMoreElements()) {
                        String sHeaderName = (String) enumeration.nextElement();
                        out.println("<req_" + sHeaderName + ">" + req.getHeader(sHeaderName) + "</req_" + sHeaderName + ">");
                    }
                }
            } else {
                out.println("<HTML>");
            }
            sFunction = "api." + props.get("fn", true);

            if (((!sFunction.equals("api.security.TSLogin")) && (!sFunction.equals("api.security.TSGetConfigParams")) && (!sWhichDB.equals("SVR")))) {
                String sKey = (String) props.get("SKEY");
                if ((sKey == null) && !bTestCase && !sFunction.equals("api.tsclassify.TSClassifyDoc")) {
                    log.error("****** API call REJECTED NO SESSION KEY [" + icallCounterAtStart + "] ***  [" + sFunction + "]");
                    out.println("<MISSINGSKEY>1</MISSINGSKEY>");
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
                } else {
                    User u = null;

                    // if in test case mode, grab a user object named after the
                    // admin user
                    if (bTestCase || sFunction.equals("api.tsclassify.TSClassifyDoc")) {
                        Enumeration<?> enumeration = com.indraweb.execution.Session.htUsers.keys();
                        while (enumeration.hasMoreElements() && ((u == null) || (!u.IsMember(out)))) {
                            sKey = (String) enumeration.nextElement();
                            u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
                            props.put("SKEY", sKey);
                        }
                    } else {
                        u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
                        log.debug("Getting the user associated with the key :" + sKey);
                        if (u != null) {
                            log.debug("Username: " + u.GetFullname());
                        }
                    }

                    if (!AppServletContextListener.getMachineType().equals("Server") && u == null) {
                        //To Enable the classification within the same classifier.
                        log.debug("API Call from the same classifier, Generating new User");
                        //FIXME This could trigger a memory problem, because we generate a user for each classification.
                        u = new User();
                        u.SetUsername("Classifier::" + Classifier.getLocalInstance().getId());

                    }
                    String sFn = (String) props.get("fn", true);
                    log.debug("Getting the user function <api." + sFn + "> to create the Object.");

                    // allow functions through eg for debugging - without
                    // user/authentication and security
                    boolean bAllowFnThruNoSecurity = sFn.equals("tsother.TSTestWait");
                    // DEBUG
                    // if (u == null && !bAllowFnThruNoSecurity)
                    if (!bDebugging && u == null && !bAllowFnThruNoSecurity) {
                        out.println("<SESSIONEXPIRED>1</SESSIONEXPIRED>");
                        log.debug("****** API call REJECTED SESSION EXPIRED (" + sKey + ") [" + icallCounterAtStart + "] ***  [" + sFunction + "]");
                        log.debug("****** Current size of SESSION queue: " + com.indraweb.execution.Session.htUsers.size() + " *****");
                        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
                    }

                    if (sFunction.equals("api.tsclassify.TSClassifyDoc")) {
                        String sRanGCYesNo = "";
                        int iGCFreq = Session.cfg.getPropInt("GCFrequency", false, "1");
                        if (iGCFreq > 0) {
                            synchronized (oCallCtr_classify_semaphore) {
                                if (iCallCtr_classify % iGCFreq == 0) {
                                    System.gc();
                                    sRanGCYesNo = "GCran [yes]";
                                }
                                iCallCtr_classify++;
                            }
                        }
                        long lTotalmem = Runtime.getRuntime().totalMemory();
                        long lFreemem = Runtime.getRuntime().freeMemory();

                        if (u != null) {
                            log.debug(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] dburl [" + props.get("dburl") + "] user [" + u.GetUsername() + ((sKey == null) ? "" : "." + sKey) + "] [" + sFunction + ":" + props.get("url") + "] " + sRanGCYesNo + " mem [" + lTotalmem + ":" + lFreemem + "]");
                        } else {
                            log.debug(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] dburl [" + props.get("dburl") + "]user [" + "no user" + "." + sKey + "] [" + sFunction + ":" + props.get("url") + "] " + sRanGCYesNo + " mem [" + lTotalmem + ":" + lFreemem + "]");
                        }

                    } else {
                        if (u != null) {
                            String username = (u.GetUsername() == null) ? "No Username" : u.GetUsername();
                            log.debug(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] user [" + username + "." + sKey + "] [" + sFunction + "]");
                        } else {
                            log.debug(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] user [" + "no user" + "." + sKey + "] [" + sFunction + "]");
                        }

                    }
                }
            } else {
                log.debug("OPEN CALL [" + icallCounterAtStart + "] fn [" + sFunction + "]");
            }

            // @todo eventually test caching and/or reusing the class and method
            // objects below
            // create the set of class objects for passing into the API call as
            // per java reflection

            //log.debug("Creating objects for the function Arguments.");
            Class<?> cAPIProperties = Class.forName("api.APIProps");
            Class<?> cPrintWriter = Class.forName("java.io.PrintWriter");
            Class<?> cConnection = Class.forName("java.sql.Connection");
            // Class[] cArr = { cHttpServletRequest, cHttpServletResponse,
            // cConnection };
            Class<?>[] cArr = {cAPIProperties, cPrintWriter, cConnection};
            // create the (super)class against which to invoke the API method
            // call
            Class<?> cTSapi = null;
            try {
                log.debug("Going to create the class to invoke the method.");
                cTSapi = Class.forName(sFunction);
                log.debug("Class <" + cTSapi.getName() + "> Created");
            } catch (ClassNotFoundException cnfe) {
                log.error("Class Not Found: " + cnfe.getMessage());
                throw new TSNoSuchObjectException("name [" + sFunction + "]");
            } catch (NoClassDefFoundError ex) {
                log.error("NoClassDefFoundError: " + ex.getMessage());
            } catch (Exception ex) {
                log.error("Exception: " + ex.getMessage());
                out.println(EmitGenXML_ErrorInfo.getsXML_JavaException("Exception", ex));
            }
            // api.tscorpus.TSGetCorpus.handleTSapiRequest (
            // *********************************************
            // first look for the class without the multipart
            // *********************************************
            Method method = null;
            // classify needed an additional parameter - this technique allows
            // multiple invoked method signs
            int iTRADITIONAL_NON_CLASSIFY = 1;
            int iCLASSIFYWITHREQUESTREQUIRED = 2;
            int iTSMETASEARCH = 3;

            int iWhichParameterList = iTRADITIONAL_NON_CLASSIFY;
            try {
                method = cTSapi.getMethod("handleTSapiRequest", cArr);
            } catch (NoSuchMethodException nsme) {
                String sFn = (String) props.get("fn");
                // if ( true ||
                // !sFn.equals("tsmetasearchnode.TSMetasearchNode"))
                Class<?> cHttpServletRequest = Class.forName("javax.servlet.http.HttpServletRequest");
                Class<?> cHttpServletResponse = Class.forName("javax.servlet.http.HttpServletResponse");
                // Class[] cArr = { cHttpServletRequest, cHttpServletResponse,
                // cConnection };
                Class<?>[] cArr2 = null;

                if (sFn.equals("tsmetasearchnode.TSMetasearchNode")) {
                    iWhichParameterList = iTSMETASEARCH;
                    cArr2 = new Class[4];
                    cArr2[0] = cHttpServletRequest;
                    cArr2[1] = cHttpServletResponse;
                    cArr2[2] = cAPIProperties;
                    cArr2[3] = cPrintWriter;

                } else {
                    // assume
                    // *********************************************
                    // else look for the class with the multipart
                    // *********************************************
                    // Class[] cArr = { cHttpServletRequest,
                    // cHttpServletResponse, cConnection };
                    iWhichParameterList = iCLASSIFYWITHREQUESTREQUIRED;
                    cArr2 = new Class[5];
                    cArr2[0] = cHttpServletRequest;
                    cArr2[1] = cHttpServletResponse;
                    cArr2[2] = cAPIProperties;
                    cArr2[3] = cPrintWriter;
                    cArr2[4] = cConnection;
                    // create the (super)class against which to invoke the API
                    // method call
                }

                try {
                    method = cTSapi.getMethod("handleTSapiRequest", cArr2);
                } catch (NoSuchMethodException nsme2) {
                    out.println("<DEBUG>no such method - even tried multipart </DEBUG>");
                    Log.LogError("no such method [" + sFunction + "]", nsme2);
                    com.indraweb.util.Log.NonFatalError("no such method [" + sFunction + "]", nsme2);
                    throw new TSNoSuchMethodException("no such method on invoke object [" + sFunction + "]");
                }

            } catch (Exception ex) {
                log.error("Exception: " + ex.getMessage());
            }

            try {
                if (iWhichParameterList == iTRADITIONAL_NON_CLASSIFY) {
                    Object[] oArr = {props, out, dbc};
                    method.invoke(cTSapi, oArr); // invoke any of the API
                    // functions
                } else if (iWhichParameterList == iCLASSIFYWITHREQUESTREQUIRED) {
                    Object[] oArr = {req, res, props, out, dbc};
                    method.invoke(cTSapi, oArr); // invoke any of the API
                    // functions
                } else if (iWhichParameterList == iTSMETASEARCH) {
                    Object[] oArr = {req, res, props, out};
                    method.invoke(cTSapi, oArr); // invoke any of the API
                    // functions

                } else {
                    throw new Exception("unknown iWhichParameterList value [" + iWhichParameterList + "]");
                }
            } catch (java.lang.reflect.InvocationTargetException ite) {
                log.error("EXCEPTION on back from call out : " + ite.getMessage(), ite);
                Throwable t2 = ite.getTargetException();
                String strace = Log.stackTraceToString(t2);
                out.println("<INTERNALERRMSG>" + t2.getMessage() + "</INTERNALERRMSG>");
                out.println("<INTERNALSTACKTRACE>" + strace + "</INTERNALSTACKTRACE>");

                try {
                    PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter("/IWError_OKtoDeleteThisFile.txt")));
                    file.write("<INTERNALERRMSG>" + t2.getMessage() + "</INTERNALERRMSG>" + "<INTERNALSTACKTRACE>" + strace + "</INTERNALSTACKTRACE>");
                    file.close();
                } catch (Exception e) {
                    log.error("can't put stacktrace info to /IWError_OKtoDeleteThisFile.txt : " + e.getMessage());
                }

                ThreadInfoTestAssertion.captureErrorPut(t2.getMessage() + ":" + strace);
            } catch (Exception e) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUTSIDE_INVOKED_METHOD, "outside error message [" + e.getMessage() + "]");
            }
        } catch (TSDBException dbe) {
            EmitGenXML_ErrorInfo.emitException("", dbe, out);
        } catch (TSNoSuchObjectException nsoe) {
            EmitGenXML_ErrorInfo.emitException("", nsoe, out);
        } catch (TSNoSuchMethodException nsme) {
            EmitGenXML_ErrorInfo.emitException("", nsme, out);
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("", tse, out);
        } catch (Exception e) {
            out.println(EmitGenXML_ErrorInfo.getsXML_JavaException("Exception", e));
        } finally {
            if (bXML) {
                out.println("<CLASSLOAD>" + sServerUpSince + "</CLASSLOAD>");
                out.println("<CALLCOUNT>" + icallCounterAtStart + "</CALLCOUNT>");
            }
            long lTime = (System.currentTimeMillis() - lTimeStart);
            log.debug("DONE CALL  [" + icallCounterAtStart + "] [" + sFunction + "] *** " + lTime + " ms");
            if (bXML) {

                out.println("<TIMEOFCALL_MS>" + (lTime) + "</TIMEOFCALL_MS>");

                // output IW_ERROR if internal error occured
                StringBuilder sbErrorsFail = ThreadInfoTestAssertion.captureErrorFailget();
                if (sbErrorsFail != null && !sbErrorsFail.toString().equals("")) {
                    out.println("   <IW_ERROR><![CDATA[" + sbErrorsFail.toString() + "]]></IW_ERROR>\r\n");
                }

                StringBuilder sbWarningsFail = ThreadInfoTestAssertion.captureWarningFailget();
                if (sbWarningsFail != null && !sbWarningsFail.toString().equals("")) {
                    out.println("   <IW_WARNING><![CDATA[" + sbWarningsFail.toString() + "]]></IW_WARNING>\r\n");
                }
                ThreadInfoTestAssertion.captureErrorsRemove();
                ThreadInfoTestAssertion.captureWarningsremove();
                out.println("</TSRESULT>");
            } else {
                out.println("<BR>Call Duration (ms) : " + (lTime));
                out.println("</HTML>");
            }

            try {
                if (bProp_TestGenerator && !bProp_RunningTest) {
                    try {
                        outputTestCase(props, (XMLOutRedirector) out, lTestID, icallCounterAtStart);
                    } catch (Exception e) {
                        Log.LogError("error outputting test case xml [" + ((XMLOutRedirector) out).getText(true) + "]", e);
                    }
                }
                out.flush();
                out.close();
            } catch (Exception e) {
                Log.LogError("error closing xml txt file stream", e);
            }
        }
    } // public void doGetMine ( Properties props, PrintWriter out)
    private static int iTestSequence = -1;

    private static synchronized long getTestSequence() {
        if (iTestSequence < 0) {
            iTestSequence = findCurrentMaxTestSequence();
        }
        iTestSequence++;

        return iTestSequence;

    }

    /**
     * for the file sCDMSTESTFILE get the largest current test number and increment by one
     */
    private static int findCurrentMaxTestSequence() {

        String textline;
        BufferedReader in;

        String sTestSequence = "0";
        try {
            // <TESTSEQUENCE>1</TESTSEQUENCE>
            if (UtilFile.bFileExists(sCDMSTESTFILE)) {
                in = new BufferedReader(new FileReader(sCDMSTESTFILE));

                while (in.ready()) {
                    textline = in.readLine();
                    if (textline.trim().startsWith("<TESTSEQUENCE>")) {
                        sTestSequence = UtilStrings.getStringBetweenThisAndThat(textline, "<TESTSEQUENCE>", "</TESTSEQUENCE>");
                    }
                }
                in.close();
            }

            int iReturnNextSequence = Integer.parseInt(sTestSequence);
            log.debug("next test id : " + iReturnNextSequence);
            return iReturnNextSequence;

        } catch (Exception e) {
            api.Log.LogFatal("Exception in enumFileLines", e);
            return -999;
        }

    }

    private static synchronized void outputTestCase(api.APIProps props, XMLOutRedirector outXML, long lTestSequence, int icallCounterAtStart) throws Exception {
        outputTestDoc(props, outXML, lTestSequence, icallCounterAtStart);
        long lStart = System.currentTimeMillis();

        boolean bIndraTestCasesFilePreExists = UtilFile.bFileExists(sCDMSTESTFILE);
        if (!bIndraTestCasesFilePreExists) {
            addToTestFile("<TESTCASES>", true);
        }

        Enumeration<Object> enumeration = props.keys();
        addToTestFile("<TESTCASE>", true);
        addToTestFile(" <TESTSEQUENCE>" + lTestSequence + "</TESTSEQUENCE>", true);
        addToTestFile(" <TESTID>" + System.currentTimeMillis() + "</TESTID>", true);
        addToTestFile(" <FN>" + (String) props.get("FN") + "</FN>", true);
        addToTestFile(" <TESTTIME>" + new java.util.Date().toString() + "</TESTTIME>", true);
        addToTestFile(" <SVRCALLCOUNT>" + icallCounterAtStart + "</SVRCALLCOUNT>", true);
        addToTestFile(" <DB>" + Session.cfg.getProp("DBString_API_OracleJDBC").toString() + "</DB>", true);
        String sKey = (String) props.get("SKEY");
        // Object o = props.get("SKEY");
        if (sKey != null) {
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            if (u != null) {
                addToTestFile(" <USER>" + u.GetUsername() + "</USER>", true);
            } else {
                addToTestFile(" <USER>" + "UNKNOWN" + "</USER>", true);
            }
        }

        String sAPIFN = null;
        StringBuilder sbArgsNonFn = new StringBuilder();
        addToTestFile(" <ARGS>\r\n", false);
        while (enumeration.hasMoreElements()) {
            String sPropKey = (String) enumeration.nextElement();
            if (sPropKey.toLowerCase().equalsIgnoreCase("fn")) {
                sAPIFN = (String) props.get(sPropKey);
            } else {
                sbArgsNonFn.append("  <ARG>\r\n");
                sbArgsNonFn.append("   <PARM>").append(sPropKey).append("</PARM>\r\n");
                sbArgsNonFn.append("   <VALUE><![CDATA[").append((String) props.get(sPropKey)).append("]]></VALUE>\r\n");
                sbArgsNonFn.append("  </ARG>\r\n");
            }
        }
        addToTestFile("  <ARG>\r\n", false);
        addToTestFile("   <PARM>fn</PARM>\r\n", false);
        addToTestFile("   <VALUE>" + sAPIFN + "</VALUE>\r\n", false);
        addToTestFile("  </ARG>\r\n", false);
        addToTestFile(sbArgsNonFn.toString(), false);

        addToTestFile(" </ARGS>", true);
        addToTestFile(" <ASSERTIONS>", true);
        String sXML = outXML.getText(true);
        log.debug("test generator input xml len (bytes) [" + sXML.length() + "]");
        try {
            org.dom4j.Document dom4jdoc = Dom4jHelper.getDomFromstring(sXML);
            Element element = dom4jdoc.getRootElement();
            emitAssertion("rootelement", element.getName(), true, true);
            emitAssertion("IW_ERROR", element.getName(), false, true);
            emitAssertion("IWERROR", element.getName(), false, true);
            emitAssertion("TS_ERROR", element.getName(), false, true);
            emitAssertion("TSERROR", element.getName(), false, true);
            emitAssertion("INTERNALSTACKTRACE", element.getName(), false, true);
            Iterator<?> iterElements = element.elements().iterator();

            while (iterElements.hasNext()) {
                element = (Element) iterElements.next();
                emitAssertion("level 2", element.getName(), true, true);
            }
        } catch (Exception e2) {
            api.Log.LogError("error in test gen, test case designed to fail at run time emitted");
            emitAssertion("rootelement", "error in test gen, test case designed to fail at run time emitted e2.getmessage [" + e2.getMessage() + "]", true, true);
        }

        addToTestFile(" </ASSERTIONS>", true);

        addToTestFile("</TESTCASE>", true);
        log.debug("test assertion gen took ms [" + (System.currentTimeMillis() - lStart) + "]");
    }

    private static synchronized void outputTestDoc(api.APIProps props, XMLOutRedirector outXML, long lTestSequence, int icallCounterAtStart) throws Exception {
        String s1TestPhaseName = null;
        String sFile = "/temp/IndraDebugTestGen_TestPhaseNameFile.txt";
        int iCountDesiredInFileLine0 = -1;
        if (UtilFile.bFileExists(sFile)) {
            String sTestPhaseName_fileLoc = UtilFile.getFileAsString(sFile).trim();
            Vector<String> vStrTestPhaseDescriptions = UtilFile.getVecOfFileStrings(sTestPhaseName_fileLoc);

            boolean bFound = false;
            iCountDesiredInFileLine0 = Integer.parseInt((((String) vStrTestPhaseDescriptions.elementAt(0))).trim());
            for (int i = 0; i < vStrTestPhaseDescriptions.size(); i++) {
                String sLine = (String) vStrTestPhaseDescriptions.elementAt(i);
                if (sLine.indexOf("$") > 0 && (sLine.indexOf("$") < 6)) {
                    String sCountToLeftOfDollar = UtilStrings.getsUpToNthOfThis_1based(sLine, "$", 1);
                    if (sCountToLeftOfDollar != null) {
                        int iCountSelfDeclaredByLine = Integer.parseInt(sCountToLeftOfDollar);
                        if (iCountSelfDeclaredByLine == iCountDesiredInFileLine0) {
                            s1TestPhaseName = (String) vStrTestPhaseDescriptions.elementAt(iCountDesiredInFileLine0);
                            bFound = true;
                        }
                    }
                }
            }
            if (!bFound) {
                throw new Exception("iCountDesiredInFileLine0 [" + iCountDesiredInFileLine0 + "] not found in file [" + sFile + "]");
            }

            if (s1TestPhaseName.indexOf("\t") < 0) {
                s1TestPhaseName = "\t" + s1TestPhaseName;
            }
        }

        String s2TestSeq = "" + lTestSequence;
        String s3Fn = (String) props.get("fn");

        String sFileNameOutputTestDoc = "/temp/IndraDebugTestDoc.txt";
        if (s3Fn.equalsIgnoreCase("security.tslogin")) {
            UtilFile.addLineToFile(sFileNameOutputTestDoc, "Database [" + Session.cfg.getProp("DBString_API_OracleJDBC").toString() + "]\r\n");
        }

        String s4Date = (new java.util.Date()).toString();
        String s5iCallCounter = "" + icallCounterAtStart;
        String s6URL = TestItem.getAPICallURL("http://localhost/itsapi/ts?", props);

        String sLineToAdd = iCountDesiredInFileLine0 + "\t" + s1TestPhaseName + "\t" + s2TestSeq + "\t" + s3Fn + "\t" + s4Date + "\t" + s5iCallCounter + "\t" + s6URL + "\r\n";

        UtilFile.addLineToFile(sFileNameOutputTestDoc, sLineToAdd);

    }

    private static void emitAssertion(String sDesc, String sElementgetName, boolean bMustExist, boolean bTrueAsAnXMLTagFalseAnywhereinXML) throws Exception {
        if (sDesc.equalsIgnoreCase("CLASSLOAD") || sDesc.equalsIgnoreCase("CLASSLOAD") || sDesc.equalsIgnoreCase("TIMEOFCALL_MS") || sDesc.equalsIgnoreCase("CALLCOUNT")) {
            return;
        }
        if (sElementgetName.equalsIgnoreCase("CLASSLOAD") || sElementgetName.equalsIgnoreCase("CLASSLOAD") || sElementgetName.equalsIgnoreCase("TIMEOFCALL_MS") || sElementgetName.equalsIgnoreCase("CALLCOUNT")) {
            return;
        }
        addToTestFile("  <ASSERTION>", true);
        addToTestFile("   <DESC>" + sDesc + "</DESC>", true);
        if (bMustExist) {
            addToTestFile("   <EXIST>" + "YES" + "</EXIST>", true);
        } else {
            addToTestFile("   <EXIST>" + "NO" + "</EXIST>", true);
        }

        if (bTrueAsAnXMLTagFalseAnywhereinXML) {
            addToTestFile("   <EXISTWHERE>" + "XMLTAG" + "</EXISTWHERE>", true);
            if (bMustExist) {
                addToTestFile("   <SEARCHTEXT>" + sElementgetName + "</SEARCHTEXT>", true);
            } else {
                addToTestFile("   <SEARCHTEXT>" + sDesc + "</SEARCHTEXT>", true);
            }

        } else {
            addToTestFile("   <EXISTWHERE>" + "ANYWHEREINXML" + "</EXISTWHERE>", true);
            addToTestFile("   <SEARCHTEXT>" + sDesc + "</SEARCHTEXT>", true);
        }
        addToTestFile("  </ASSERTION>", true);

    }

    private static synchronized void addToTestFile(String sLine, boolean bcrlf) throws Exception {
        PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(APIHandler.sCDMSTESTFILE, true)));
        String scrlf = "";
        if (bcrlf) {
            scrlf = "\r\n";
        }
        file.write(sLine + scrlf);
        file.close();
    }
}
