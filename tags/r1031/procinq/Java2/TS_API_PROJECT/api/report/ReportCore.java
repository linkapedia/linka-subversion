package api.report;

import com.iw.system.ModelNodeReport;
import com.iw.system.ModelReport;
import com.iw.system.ParamsQueryReport;
import com.linkapedia.core.persistence.NodeStaticNotFoundException;
import java.util.List;

/**
 *
 * @author andres
 */
public class ReportCore {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportCore.class);

    /**
     * generate a report by taxonomy
     */
    public String generateReport(String corpusId, ParamsQueryReport parameters, OutputFormat format) {
        ReportDAO dao = new ReportDAO();
        ModelReport modelR = new ModelReport();
        String result = "";
        try {
            List<ModelNodeReport> list = dao.getOracleData(corpusId, parameters);
            for (ModelNodeReport model : list) {
                StringBuilder sb = new StringBuilder(model.getNodeId());
                try {
                    model.setNodeLinks(String.valueOf(dao.getLinksDynamo(sb.reverse().toString())));
                } catch (NodeStaticNotFoundException ex) {
                    log.error("ReportCore: error getting nodestatic. not found" + ex.getMessage());
                }
            }
            modelR.setNodeCount(String.valueOf(list.size()));
            modelR.setNodes(list);
            result = format.getData(modelR);
        } catch (ReportDAOException ex) {
            log.error("ReportCore: error getting data from oracle. " + ex.getMessage());
        } catch (OutputFormatException ex) {
            log.error("ReportCore: error output format. " + ex.getMessage());
        }
        return result;
    }
}
