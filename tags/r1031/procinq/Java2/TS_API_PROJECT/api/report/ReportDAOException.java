package api.report;

/**
 *
 * @author andres
 */
public class ReportDAOException extends Exception {

    public ReportDAOException() {
    }

    public ReportDAOException(String message) {
        super(message);
    }

    public ReportDAOException(Throwable cause) {
        super(cause);
    }

    public ReportDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
