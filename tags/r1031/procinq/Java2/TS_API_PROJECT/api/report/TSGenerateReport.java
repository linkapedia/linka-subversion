package api.report;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import com.iw.system.ParamsQueryReport;
import java.io.PrintWriter;
import java.sql.Connection;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSGenerateReport {

    private static final Logger LOG = Logger.getLogger(TSGenerateReport.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        LOG.debug("TSGenerateReport");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String corpusId = (String) props.get("corpusId");
        String queryParams = (String) props.get("paramsquery");
        String nodeId = nu.getRootNode(Integer.parseInt(corpusId));
        if (corpusId == null || queryParams == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (corpusId.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeId), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }

        ReportCore bo = new ReportCore();
        OutputFormat output = new OutputFormatXML();

        JSONObject parameters = null;
        parameters = null;
        try {
            parameters = JSONObject.fromObject(queryParams);
            ParamsQueryReport bean = (ParamsQueryReport) JSONObject.toBean(parameters, ParamsQueryReport.class);
            if (bean != null) {
                String report = bo.generateReport(corpusId, bean, output);
                //send data to client
                out.println(report);
            } else {
                LOG.error("Error parsing data JSON == null");
            }
        } catch (Exception e) {
            LOG.error("Error parsing data JSON", e);
        }
        LOG.debug("FINISHED");
    }
}
