package api.report.wikipedia;

import com.iw.system.WrapperListReport;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author andres
 */
public class Transformation {

    public static String getData(WrapperListReport data) throws Exception {
        JAXBContext jc;
        StringWriter writer = new StringWriter();
        try {
            jc = JAXBContext.newInstance(WrapperListReport.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.marshal(data, writer);
        } catch (JAXBException ex) {
            throw new Exception("Error parsing xml data", ex.getCause());
        }
        return writer.toString();
    }
}
