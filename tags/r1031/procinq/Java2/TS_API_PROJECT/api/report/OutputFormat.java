
package api.report;

import com.iw.system.ModelReport;

/**
 *
 * @author andres
 */
public interface OutputFormat {
    
    public String getData(ModelReport data) throws OutputFormatException;
      
}
