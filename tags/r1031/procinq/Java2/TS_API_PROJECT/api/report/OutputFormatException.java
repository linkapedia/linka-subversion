package api.report;

/**
 *
 * @author andres
 */
public class OutputFormatException extends Exception {

    public OutputFormatException() {
    }

    public OutputFormatException(String message) {
        super(message);
    }

    public OutputFormatException(Throwable cause) {
        super(cause);
    }

    public OutputFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
