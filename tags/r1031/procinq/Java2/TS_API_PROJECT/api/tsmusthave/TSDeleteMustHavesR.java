package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import java.io.PrintWriter;
import java.sql.Connection;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 * this method delete all musthaves from one node recursively
 */
public class TSDeleteMustHavesR {

    private static final Logger LOG = Logger.getLogger(TSDeleteMustHavesR.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        LOG.debug("TSDeleteMustHavesR");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        try {
            LOG.debug("Delete musthaves start nodeid: "+nodeid);
            nu.deleteMH(Integer.parseInt(nodeid));
        } catch (Exception e) {
            throw e;
        }
        out.println("<SUCCESS>successfully.</SUCCESS>");
        LOG.debug("FINISHED");
    }
}
