// create table musthave (nodeid number(12) not null, mustword varchar2(125)) tablespace ddata;
// alter table musthave add foreign key (nodeid) references Node (nodeid) on delete cascade;
// create unique index musthaveidword on musthave (nodeid, mustword) tablespace RDATA;
package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class TSGetMustHaves {

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sLang = (String) props.get("lang", "EN");
        String sKey = (String) props.get("SKEY", true);
        String musthaveType = (String) props.get("musthavetype");
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
        ResultSet rs = null;
        try {
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
            }
            String musthaveTableName = "";
            if (musthaveType == null || musthaveType.equals("1")) {
                musthaveTableName = "musthave";
            } else {
                musthaveTableName = "nodemusthavegate";
            }
            String sSQL = " select mustword from " + musthaveTableName + " where nodeid =  " + sNodeID + " and lang = '" + sLang + "'";
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            int loop = 0;
            while (rs.next()) {
                loop = loop + 1;
                if (loop == 1) {
                    out.println("<WORDS>");
                }

                String sWordName = rs.getString(1);

                out.println("   <WORD> ");
                out.println("   <WORDNAME>" + sWordName + "</WORDNAME>");
                out.println("   </WORD>");
            }

            if (loop > 0) {
                out.println("</WORDS>");
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }
}
