/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Jul 13, 2006
 * Time: 11:05:58 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.system.Node;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TSAddMustHavesRecursive {

    private static boolean useCommon = false;
    private static String lang = "EN";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String depth = (String) props.get("depth");
        String suseNode = (String) props.get("useNode");
        String suseParent = (String) props.get("useParent");
        String susePhrases = (String) props.get("usePhrases");
        String sTerms = (String) props.get("Terms");
        String sRegex = (String) props.get("Regex");
        String sCommon = (String) props.get("common");
        String choiceGroupOption = (String) props.get("choiceGroupOption");
        lang = (String) props.get("lang", "EN");

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (sCommon != null) {
            useCommon = true;
        }

        Statement stmt = null;
        ResultSet rs = null;
        int iCorpusId = -1;
        try {
            // for authorization purposes, need to know the corpus id of this node
            String sSQL = "select CorpusId from Node where NodeID = " + sNodeID;
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            iCorpusId = rs.getInt(1);
            rs.close();
            stmt.close();
            rs = null;
            stmt = null;

            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
            return;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        int loop = 0;

        Vector v = new Vector(); // collect the nodes

        if (depth != null) {
            try {
                String sSQL = Node.getSQL(dbc) + " where corpusid =  " + iCorpusId + " and depthfromroot = " + depth + " order by lower(nodetitle) asc";
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    v.add(new Node(rs));
                }
            } catch (Exception e) {
                throw e;
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        } else {
            try {
                String sSQL = Node.getSQL(dbc) + " start with n.nodeid = " + sNodeID + " connect by prior nodeid = parentid";
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    v.add(new Node(rs));
                }
            } catch (Exception e) {
                throw e;
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        }

        int total = 0;
        for (int i = 0; i < v.size(); i++) { // for each node, do the deed
            total = total + addMustHave(suseNode, suseParent, susePhrases, sTerms, (Node) v.elementAt(i), sRegex, dbc, choiceGroupOption);
        }

        out.println("<SUCCESS>" + total + " must have words added successfully.</SUCCESS>");
    }

    private static int addMustHave(String useNode, String useParent, String usePhrases, String sTerms, Node n, String Regex, Connection dbc, String choiceGroupOption) throws Exception {
        if (n.get("NODESTATUS").equals("2")) {
            api.Log.Log("Warning: Node " + n.get("NODETITLE") + " is locked, skipping.");
            return 0;
        }

        Statement stmt = null;
        ResultSet rs = null;
        int loop = 0;
        String sNodeID = n.get("NODEID");
        if (choiceGroupOption.equals("0")) {
            sTerms = removeWordsInGroupCharacters(sTerms);
        } else if (choiceGroupOption.equals("1")) {
            sTerms = removeGroupCharacters(sTerms);
        }

        // if terms is not null, add terms
        if (sTerms != null && !sTerms.isEmpty()) {
            String[] terms = sTerms.split(",");
            for (int i = 0; i < terms.length; i++) {
                if (insertMustHave(terms[i].toLowerCase(), sNodeID, dbc)) {
                    loop++;
                }
            }
        }

        String titleNode = n.get("NODETITLE");
        if (useNode != null) {
            if (choiceGroupOption.equals("0")) {
                titleNode = removeWordsInGroupCharacters(titleNode);
            } else if (choiceGroupOption.equals("1")) {
                titleNode = removeGroupCharacters(titleNode);
            }
            String[] terms = titleNode.toLowerCase().split(" ");

            for (int i = 0; i < terms.length; i++) {
                if (insertMustHave(terms[i], sNodeID, dbc, false)) {
                    loop++;
                }
            }
        }

        titleNode = n.get("NODETITLE");
        if (usePhrases != null) {
            if (choiceGroupOption.equals("0")) {
                titleNode = removeWordsInGroupCharacters(titleNode);
                if (insertMustHave(titleNode.toLowerCase(), sNodeID, dbc, true)) {
                    loop++;
                }
            } else if (choiceGroupOption.equals("1")) {
                List<String> wordsAux = getCharactersInGroupCharacters(titleNode);
                titleNode = removeWordsInGroupCharacters(titleNode);

                //insert title sobrante
                if (insertMustHave(titleNode.toLowerCase(), sNodeID, dbc, true)) {
                    loop++;
                }
                //insert the other words
                for (String cad : wordsAux) {
                    if (insertMustHave(removeGroupCharacters(cad), sNodeID, dbc, false)) {
                        loop++;
                    }
                }

            }

        }

        if (useParent != null) {
            Node p = null;
            try {
                String sSQL = Node.getSQL(dbc) + " where nodeid = " + n.get("PARENTID");
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    p = new Node(rs);
                }
            } catch (Exception e) {
                throw e;
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            if (p != null) {
                String[] terms = p.get("NODETITLE").toLowerCase().split(" ");

                for (int i = 0; i < terms.length; i++) {
                    if (insertMustHave(terms[i], sNodeID, dbc)) {
                        loop++;
                    }
                }

                if (usePhrases != null) {
                    if (insertMustHave(p.get("NODETITLE").toLowerCase(), sNodeID, dbc)) {
                        loop++;
                    }
                }
            }
        }

        // if the regex pattern is not null, use it to extract new must have terms (this code is trash)
        if (Regex != null) {
            String firstletter = n.get("NODETITLE").substring(0, 1);
            Regex = Regex.replaceAll("NODETITLE", "[" + firstletter.toUpperCase() + firstletter.toLowerCase() + "]" + n.get("NODETITLE").substring(1).toLowerCase());

            // first, get this node's source
            String sNodeSource = "";
            try {
                sNodeSource = UtilClob.getClobStringFromNodeData(new Long(sNodeID).longValue(),dbc);
            } catch (Exception e) {
                e.printStackTrace(System.out);
            } // ignore an error here, return empty string

            // then extract all patterns and add as must have terms
            Pattern pat = Pattern.compile(Regex);
            Matcher mat = pat.matcher(sNodeSource);
            while (mat.find()) {
                insertMustHave(mat.group().toLowerCase(), sNodeID, dbc);
                loop++;
            }

            sNodeSource = null;
        }

        try {
            String sSQL = "update Node set NodeLastSync = SYSDATE, dateupdated = sysdate where NodeID = " + sNodeID;
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception tse) {
            throw tse;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        return loop;
    }

    private static boolean insertMustHave(String term, String nodeid, Connection dbc) throws Exception {
        return insertMustHave(term, nodeid, dbc, true);
    }

    private static boolean insertMustHave(String term, String nodeid, Connection dbc, boolean ignoreCommon) throws Exception {
        PreparedStatement stmt = null;
        term = term.replaceAll(",", "");
        term = term.replaceAll("&", "");
        if (term.length() < 1) {
            return false;
        }

        // cross reference each term with the common words list.  if it exists, don't add it
        Hashtable ht = com.indraweb.execution.Session.stopList.getHTStopWordList();

        // what's new: if this is a phrase, split it into words and check each for common
        if ((term.indexOf(" ") > -1) && useCommon && !ignoreCommon) {
            String[] words = term.split(" ");
            boolean common = true;

            for (int i = 0; i < words.length; i++) {
                if (!ht.containsKey(words[i].toLowerCase())) {
                    common = false;
                }
            }

            if (common) {
                api.Log.Log("Warning!  All words in phrase " + term + " exists in the commonwords.txt file.  Insert ignored.");
                return false;
            }
        }

        if (ht.containsKey(term.toLowerCase()) && useCommon && !ignoreCommon) {
            api.Log.Log("Warning!  Term " + term + " exists in the commonwords.txt file.  Insert ignored.");
            return false;
        }

        try {
            //String sSQL = "insert into musthave (NodeId, MustWord, Lang) values (" + nodeid + ", '" + term + "', '" + lang + "')";
            String sSQL = "insert into musthave (NodeId, MustWord, Lang) values(?,?,?)";
            stmt = dbc.prepareStatement(sSQL);
            stmt.setString(1, nodeid);
            stmt.setString(2, term);
            stmt.setString(3, lang);

            // If query statement failed, throw an exception
            if (stmt.executeUpdate() == 0) {
                api.Log.Log("{Warning} Failed to insert: " + nodeid + " " + term);
            } else {
                return true;
            }
        } catch (Exception e) {
            api.Log.Log("{Warning} Failed to insert: " + nodeid + " " + term + " " + e.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        return false;
    }

    /**
     * example (andres) = ""
     *
     * @param term
     * @return
     */
    private static String removeWordsInGroupCharacters(String term) {
        Pattern pattern = Pattern.compile("([\\(\\[\\{].*?(([\\)\\]\\}])|$))");
        return term.replaceAll(pattern.toString(), "");
    }

    /**
     * example (andres) = andres
     *
     * @param term
     * @return
     */
    private static String removeGroupCharacters(String term) {
        Pattern pattern = Pattern.compile("[\\(\\)\\{\\}\\[\\]]");
        return term.replaceAll(pattern.toString(), "");
    }

    /**
     * example (andres) es muy teso si señor (tipo) = es muy teso si señor |
     * list = andres,tipo
     *
     * @param term
     * @return
     */
    private static List<String> getCharactersInGroupCharacters(String term) {
        Pattern pattern = Pattern.compile("([\\(\\[\\{].*?(([\\)\\]\\}])|$))");
        Matcher mat = pattern.matcher(term);
        List<String> words = new ArrayList<String>();
        while (mat.find()) {
            words.add(mat.group());
        }
        return words;
    }
}
