package api.tsclassify;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.system.User;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.iw.system.Node;

/**
 * Returns all of the nodes that are scheduled for refresh in the system.   Note unlike TSGetRefreshNodeList,
 *  this API function returns only the nodes and not the signature relationships.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CorpusID (optional) Unique corpus identifier, used only if limiting results by corpus.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tsclassify.TSGetRefreshNodes&SKEY=801312271

 *	@return A series of ITS Node objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
      <NODE>
          <NODEID>1087101</NODEID>
          <CORPUSID>300012</CORPUSID>
          <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
          <PARENTID>1087099</PARENTID>
          <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
          <DEPTHFROMROOT>0</DEPTHFROMROOT>
          <DATESCANNED>null</DATESCANNED>
          <DATEUPDATED>null</DATEUPDATED>
          <NODESTATUS>1</NODESTATUS>
          <NODEDESC><![CDATA[ ]]></NODEDESC>
          <NODESIZE>1</NODESIZE>
          <LINKNODEID>1087101</LINKNODEID>
      </NODE>
 ...
 \endverbatim
 */
public class TSGetRefreshNodes {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sKey = (String) props.get("SKEY", true);
        String sCorpusID = (String) props.get("CorpusID");

        try {
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            String sSQL = Node.getSQL(dbc)+" where nodeid in (select nodeid from noderefresh) ";
            if (sCorpusID != null) { sSQL = sSQL + " and CorpusID = "+sCorpusID; }

            ResultSet rs = null;
            Statement stmt = null;
            int loop = 0;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    loop++;
                    if (loop == 1) { out.println("<NODES>"); }

                    Node n = new Node(rs);
                    n.emitXML(out, u, true);
                }
            } catch (SQLException qe) {
                EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
                return;
            } finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }

            if (loop == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
            }
            out.println("</NODES>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}