package api.tsclassify;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Returns the list of classifiers currently registered within the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CID (optional)  Unique classifier identifier for a classifier within the system.  Default is all.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tsclassify.TSGetClassifierList&SKEY=801312271

 *	@return A series of ITS CLASSIFIER objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <CLASSIFIERS>
        <CLASSIFIER>
            <CLASSIFIERID>1</CLASSIFIERID>
            <CLASSIFIERNAME>Frequency * Coverage</CLASSIFIERNAME>
            <COLNAME>SCORE2</COLNAME>
            <CLASSPATH>com.indraweb.ScoreStyle1.FCclass</CLASSPATH>
            <SORTORDER>ASC</SORTORDER>
            <ACTIVE>Active</ACTIVE>
        </CLASSIFIER>
...
 \endverbatim
 */
public class TSGetClassifierList
{
	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception
	{
        Statement stmt = null;
        ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            String sCID = (String) props.get ("CID");

			String sSQL = " select classifierID, classifierName, colName, classPath, "+
                          " sortOrder, active from classifier ";
            if (sCID != null) { sSQL = sSQL + "where classifierID = "+sCID; }
            else { sSQL = sSQL + "order by colName asc"; }
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				sCID = rs.getString(1);
				String sName = rs.getString(2);
				String sCol = rs.getString(3);
				String sPath = rs.getString(4);
                int iSort = rs.getInt(5);
				int iActive = rs.getInt(6);

                String sSort = "DESC";
                String sActive = "Active";
                if (iActive == 0) { sActive = "Inactive"; }
                if (iSort == 0) { sSort = "ASC"; }

                if (!sName.startsWith("StarCount"))
                {
                    loop = loop + 1;
                    if (loop == 1) { out.println("<CLASSIFIERS>"); }
                    out.println (" <CLASSIFIER>");
                    out.println ("   <CLASSIFIERID>"+sCID+"</CLASSIFIERID>");
                    out.println ("   <CLASSIFIERNAME>"+sName+"</CLASSIFIERNAME>");
                    out.println ("   <COLNAME>"+sCol+"</COLNAME>");
                    out.println ("   <CLASSPATH>"+sPath+"</CLASSPATH>");
                    out.println ("   <SORTORDER>"+sSort+"</SORTORDER>");
                    out.println ("   <ACTIVE>"+sActive+"</ACTIVE>");
                    out.println ("</CLASSIFIER>");
                }
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Error: There are no classifiers.</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}
			out.println("</CLASSIFIERS>");
		}
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
