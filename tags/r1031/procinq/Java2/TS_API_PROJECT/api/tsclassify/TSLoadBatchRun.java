package api.tsclassify;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.security.*;
import api.TSException;
import api.results.Locked;
import api.emitxml.*;

import com.indraweb.database.*;

/**
 * This function is used after a bulk classification run.   All results classification results currently in
 * the staging tables will be moved into the production document tables.   During this time, indexes
 * are removed and system performance will be degraded.   Depending upon the size of the document collection,
 * this procedure can sometimes take over an hour to complete.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  Tablespace (optional)  Tablespace to build indexes into.  The default is RDATA.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tsclassify.TSLoadBatchRun&SKEY=801312271

 *	@return A series of ITS CLASSIFIER objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <SUCCESS>Batch synchronization was successful.</SUCCESS>
  </TSRESULT>
 \endverbatim
 */
public class TSLoadBatchRun {
	// Description:
	// Move staging information derived from batch classify into the production system
    //
    // 1. Get the current DATE to the SECOND (and store in: STARTDATE)
    // 2. Lock the NODEDOCUMENT table against writes
    // 3. Select all DELETES from the StageDocCorpusDelete table and RUN them against NodeDocument
    // 4. Select all Node-Doc ID combinations where edited is TRUE and store in a hashtable for reference
    // 5. Drop the indexes from the NodeDocument table
    // 6. Select all INSERTS from the StageNodeDocInsert table and RUN them against NodeDocument
    //    *NOTE failures are okay, edited documents will remain and should not be updated
    // 7. Delete from staging tables where StageDate < STARTDATE (see #1)
    // 8. Restore all NodeDocument indexes
    // 9. Unlock the NODEDOCUMENT table

    static boolean bDebugMode = true;

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

        String Tablespace = (String) props.get("tablespace", "RDATA");

        Statement stmt = null; ResultSet rs = null; String sStartDate = ""; String sSQL = "";
        Vector vRowsToDelete; Vector vRowsToInsert;
		try {
            // ***************************************************************
            // 1. Get the current DATE to the SECOND (and store in: STARTDATE)
            try { sStartDate = getCurrentDate(dbc); }
            catch (Exception e) { api.Log.LogError("RunBatchUpdate: {FATAL} Could not retrieve current system date."); throw e; }

            // ***************************************************************
            // 2. Lock the NODEDOCUMENT table against writes
            String sMachineName = com.indraweb.execution.Session.cfg.getProp("MachineName");
            if (sMachineName == null) { sMachineName = "Unknown"; }
            Locked.LockNodeDocument(dbc, sMachineName);

            // ***************************************************************
            // 3a. Select all DELETES from the StageDocCorpusDelete table and ...
            try { vRowsToDelete = getResultsToDelete(dbc, sStartDate); }
            catch (Exception e) { api.Log.LogError("RunBatchUpdate: {FATAL} Could not retrieve results to delete."); throw e; }

            // ***************************************************************
            // 3b. RUN them against NodeDocument
            try { runResultsToDelete(dbc, vRowsToDelete); }
            catch (Exception e) { api.Log.LogError("RunBatchUpdate: {FATAL} Fatal error attempting to delete rows."); throw e; }

            // misc cleanup from step 2
            vRowsToDelete.clear(); vRowsToDelete = null;

            // 4. Remove all edited rows from stagenodedocinsert to avoid duplicate keys
            try { removeStageNodeDocInsertEdited(dbc); }
            catch (Exception e) { api.Log.LogError("RunBatchUpdate: {FATAL} Fatal error attempting to delete edited rows."); throw e; }

            // from steps 5-8, if there is a failure, re-create the indexes in the FINALLY clause
            try {
                // ***************************************************************
                // 5. Drop the indexes from the NodeDocument table
                try { dropIndexes(dbc); }
                catch (Exception e) { api.Log.LogError("RunBatchUpdate: {FATAL} Could not drop the ND indexes."); throw e; }

                // ***************************************************************
                // 6. Select all INSERTS from the StageNodeDocInsert table and ...
                //    *NOTE failures are okay, edited documents will remain and should not be updated
                try { runResultsToInsert(dbc, sStartDate); }
                catch (Exception e) { api.Log.LogError("RunBatchUpdate: {FATAL} Could not retrieve results to insert."); throw e; }

                // ***************************************************************
                // 7. Delete from staging tables where StageDate < STARTDATE (see #1)
                try { purgeStagingData(dbc, sStartDate); }
                catch (Exception e) { api.Log.LogError("RunBatchUpdate: {WARNING} Could not purge data from the staging table(s)."); throw e; }

            } catch (Exception e) { throw e;
            } finally {
                createIndexes(dbc, Tablespace); // 8. Restore all NodeDocument indexes
                Locked.UnlockNodeDocument(dbc); // 9. Unlocked the NODEDOCUMENT table
            }
        } catch (Exception e) {
            api.Log.LogError(e); return;
        }
        out.println("<SUCCESS>Batch synchronization was successful.</SUCCESS>");
    }

    // ***************************************************************
    // 1. Get the current DATE to the SECOND (and store in: STARTDATE)
    public static String getCurrentDate (Connection dbc) throws Exception {
        Statement stmt = null; ResultSet rs = null; String sStartDate = "";
        String sSQL = "select to_char(sysdate, 'MM-DD-YYYY HH24:MI:SS') from DUAL";
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL); rs.next();

            sStartDate = rs.getString(1);
            if (bDebugMode) api.Log.Log("getCurrentDate: "+sStartDate);
        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
        return sStartDate;
    }

    // ***************************************************************
    // 3a. Select all DELETES from the StageDocCorpusDelete table and ...
    // INSTRUCTIONS: select all distinct corpora from the StageDocCorpusDelete table.  Then
    //    for each corpus, select and delete all associated documents.
    public static Vector getResultsToDelete (Connection dbc, String sStartDate) throws Exception {
        Vector vRowsToDelete = new Vector();

        if (bDebugMode) api.Log.Log("getResultsToDelete..");

        Statement stmt = null; ResultSet rs = null;
        String sSQL = "select distinct(CORPUSID) from STAGEDOCCORPUSDELETE";
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            while ( rs.next() ) {
                int iCorpusID = rs.getInt(1);

                vRowsToDelete.add("delete from NodeDocument where Edited != 1 and "+
                                  "nodeid in "+
                                  "   (select nodeid from node where corpusid = "+iCorpusID+") and "+
                                  "documentid in "+
                                  "   (select documentid from stagedoccorpusdelete where "+
                                  "    corpusid = "+iCorpusID+" and stagingdate <= "+
                                  "    to_date('"+sStartDate+"', 'MM-DD-YYYY HH24:MI:SS'))");
            }
        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }

        return vRowsToDelete;
    }

    // ***************************************************************
    // 3b. RUN them against NodeDocument
    public static void runResultsToDelete (Connection dbc, Vector v) throws Exception {
        Statement stmt = null;
        if (bDebugMode) api.Log.Log("runResultsToDelete..");

        Enumeration eV = v.elements();
        while (eV.hasMoreElements()) {
            String sSQL = "";
            try { sSQL = (String) eV.nextElement(); if (bDebugMode) api.Log.Log("runResultsToDelete: "+sSQL);
                  stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); }
            catch (Exception e) { api.Log.LogError("RunBatchUpdate: {WARNING} Delete failed: "+sSQL, e); }
            finally { if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; } }
        }
    }

    // 4. Remove all edited rows from stagenodedocinsert to avoid duplicate keys
    public static void removeStageNodeDocInsertEdited (Connection dbc) throws Exception {
        Statement stmt = null; ResultSet rs = null;

        try {
            //String sSQL = "ALTER TABLE STAGENODEDOCINSERT ADD PRIMARY KEY (NODEID, DOCUMENTID)";
            //if (bDebugMode) api.Log.Log("create stagenodedocinsert primary key index... "+sSQL);
            //stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            String sSQL = "DELETE FROM STAGENODEDOCINSERT WHERE (NODEID, DOCUMENTID) IN "+
                   "   (SELECT NODEID, DOCUMENTID FROM NODEDOCUMENT WHERE EDITED = 1)";
            if (bDebugMode) api.Log.Log("removing edited rows from stagenodedocinsert... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            //sSQL = "ALTER TABLE STAGENODEDOCINSERT DROP PRIMARY KEY";
            //if (bDebugMode) api.Log.Log("create stagenodedocinsert primary key index... "+sSQL);
            //stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

        } catch (Exception e) { throw e; }
        finally { if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; } }
    }

    // 5. Drop the indexes from the NodeDocument table
    public static void dropIndexes (Connection dbc) throws Exception {
        Statement stmt = null; ResultSet rs = null;

        try {
            String sSQL = "ALTER TABLE NODEDOCUMENT DROP PRIMARY KEY"; if (bDebugMode) api.Log.Log("drop primary key index... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "DROP INDEX NODEDOCUMENTDOCUMENTID"; if (bDebugMode) api.Log.Log("dropIndex NodeDocumentDocumentID... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "DROP INDEX NODEDOCUMENTINDEXWITHINNODE"; if (bDebugMode) api.Log.Log("dropIndex NodeDocumentIndexWithinNode... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "DROP INDEX NODEDOCUMENTNODEID"; if (bDebugMode) api.Log.Log("dropIndex NodeDocumentNodeID... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "DROP INDEX NODEDOCUMENTNODEIDDOCIDSCORE1"; if (bDebugMode) api.Log.Log("dropIndex NodeDocumentNodeIDDocIDScore1... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "DROP INDEX NODEDOCUMENTEDITED"; if (bDebugMode) api.Log.Log("dropIndex NodeDocumentEdited... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

        } catch (Exception e) { api.Log.LogError(e); throw e; }
        finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
    }

    // 6. Select all INSERTS from the StageNodeDocInsert table and ...
    //    *NOTE failures are okay, edited documents will remain and should not be updated
    public static void runResultsToInsert (Connection dbc, String sStartDate) throws Exception {
        if (bDebugMode) api.Log.Log("runResultsToInsert... ");
        if (!InsertFromStaging (sStartDate, dbc)) {
            throw new Exception("RunBatchUpdate: {FATAL} stored procedure bulk insert failed.");
        }
        if (bDebugMode) api.Log.Log("runResultsToInsert: stored procedure completed successfully.");
    }

    // 7. Delete from staging tables where StageDate < STARTDATE (see #1)
    public static void purgeStagingData (Connection dbc, String sStartDate) throws Exception {
        Statement stmt = null; ResultSet rs = null;

        String sSQL = "delete from STAGENODEDOCINSERT where STAGINGDATE <= to_date('"+sStartDate+"', 'MM-DD-YYYY HH24:MI:SS')";
        if (bDebugMode) api.Log.Log("purgeStagingData... "+sSQL);
        try { stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); }
        catch (Exception e) { throw e; }
        finally { if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }}

        sSQL = "delete from STAGEDOCCORPUSDELETE where STAGINGDATE <= to_date('"+sStartDate+"', 'MM-DD-YYYY HH24:MI:SS')";
        if (bDebugMode) api.Log.Log("purgeStagingData... "+sSQL);
        try { stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); }
        catch (Exception e) { throw e; }
        finally { if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }}
    }

    // 8. Restore all NodeDocument indexes
    public static void createIndexes (Connection dbc) throws Exception { createIndexes (dbc, "RDATA"); }
    public static void createIndexes (Connection dbc, String Tablespace) throws Exception {
        Statement stmt = null; ResultSet rs = null;

        try {
            String sSQL = "ALTER TABLE NODEDOCUMENT ADD PRIMARY KEY (NODEID, DOCUMENTID)";
            if (bDebugMode) api.Log.Log("create primary key index... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "CREATE INDEX NODEDOCUMENTDOCUMENTID ON NODEDOCUMENT(DOCUMENTID) TABLESPACE "+Tablespace;
            if (bDebugMode) api.Log.Log("create index: NODEDOCUMENTDOCUMENTID... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "CREATE INDEX NODEDOCUMENTINDEXWITHINNODE ON NODEDOCUMENT(INDEXWITHINNODE) TABLESPACE "+Tablespace;
            if (bDebugMode) api.Log.Log("create index: NODEDOCUMENTINDEXWITHINNODE... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "CREATE INDEX NODEDOCUMENTNODEID ON NODEDOCUMENT(NODEID) TABLESPACE "+Tablespace;
            if (bDebugMode) api.Log.Log("createIndexes... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "CREATE INDEX NODEDOCUMENTEDITED ON NODEDOCUMENT(EDITED) TABLESPACE "+Tablespace;
            if (bDebugMode) api.Log.Log("createIndexes... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;

            sSQL = "CREATE INDEX NODEDOCUMENTNODEIDDOCIDSCORE1 ON NODEDOCUMENT(DOCUMENTID, NODEID, SCORE1) TABLESPACE "+Tablespace;
            if (bDebugMode) api.Log.Log("createIndexes... "+sSQL);
            stmt = dbc.createStatement(); stmt.executeUpdate (sSQL); stmt.close(); stmt = null;
         } catch (Exception e) { api.Log.LogError(e); throw e; }
        finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
    }

    public static boolean InsertFromStaging(String StagingDate, Connection dbc) {
        CallableStatement cs = null;

        try {
            cs = dbc.prepareCall ("begin INSERTFROMSTAGING (:1); end;"); cs.setString(1, StagingDate);
            cs.execute(); return true;
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (cs != null) { try { cs.close(); } catch (Exception e) {} cs = null; }}
    }

    public static String getCorpusNodes (int iCorpusID, Connection dbc) throws Exception {
		String sSQL = "select NODEID from NODE where CORPUSID = "+iCorpusID;
        Statement stmt = null; ResultSet rs = null; String sNodes = "0";

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            while ( rs.next() ) { sNodes = sNodes + ", "+rs.getString(1); }
        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
        return sNodes;
    }
}