package api.tsclassify;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.tsclassify.ROCHelper.DataCache_Classifier;
import api.tsclassify.ROCHelper.DataCache_RocBucketLocalClassifier;
import com.indraweb.execution.Session;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Rebucket all associated node-document scores given a taxonomy and new ROC values.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param CorpusID Unique corpus identifier of the taxonomy to be evaluated.
 *
 * @note http://ITSERVER/servlet/ts?fn=tsclassify.TSApplyROC&SKEY=801312271&CorpusID=22
 *
 * @return none \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <SUCCESS>1</SUCCESS> </TSRESULT> \endverbatim
 */
public class TSApplyROC {

    public static final String sCLASSIFIERNAME_StarCountInt = "StarCountInt";
    public static final String sCLASSIFIERNAME_StarCount = "StarCount";

    /**
     * 1. build a data structure to reflect db tables ROCBucketLocalClassifier and CLASSIFIER 2. iterate thru all node docs in a given corpus 3. change score cols relating to STAR COUNT AND STAR COUNT
     * INT given remaining scores and using the data structure
     *
     */
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sCorpusID = (String) props.get("CorpusID", true);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {

            if ((!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none"))
                    && (!u.IsAuthorized(sCorpusID, out))) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            int rval = doApply(sCorpusID, dbc);
            out.println("<SUCCESS>" + rval + "</SUCCESS>");
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }

    public static int doApply(String sCorpusID, Connection dbc) throws Exception {

        boolean bVerbose = com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebugVerboseTSApplyROC.txt");
        //Vector vDataRocBucketLocalClassifierAsc =
        //      DataCache_RocBucketLocalClassifier.getVecDataROCBucketLocalClassifier(sCorpusID, dbc);

        DataCache_Classifier.invalidateCache();
        DataCache_RocBucketLocalClassifier.invalidateCache();

        Vector vDataROCBucketLocalClassifier =
                DataCache_RocBucketLocalClassifier.getVecDataROCBucketLocalClassifier(sCorpusID, dbc);

        Vector vStrColNames = DataCache_RocBucketLocalClassifier.getSetOfColNamesThisROCSet(vDataROCBucketLocalClassifier, dbc);

        String sCommaListvStrColNames =
                com.indraweb.util.UtilStrings.convertVecToString(
                vStrColNames, ",");


        int iRepositoryID = -1;
        // special (debug? - should we make it a regular arg-based feature? )
        // mode for TSApplyROC - based on file make it for a repository not a corpus
        if (com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebugTSApplyROCCfg.txt")) {
            iRepositoryID = api.APIProps.getFromFile_int("/temp/IndraDebugTSApplyROCCfg.txt", "repositoryid", true);
            api.Log.Log("TSAPPLYROC : DEBUG OVERRIDE : from existence of IndraDebugTSApplyROCCfg.txt");
        }

        // what's new: 02.28.05
        // Oracle cursor is timing out on very large ND sets (50mil+ rows)
        // Select all nodes, then run a new query for each node.
        String sSQL = null;
        if (iRepositoryID == -1) {
            sSQL = "select distinct (nd.nodeid) from "
                    + " nodedocument nd, node n "
                    + // " rocbucketvalidation r where " +
                    " where     n.nodeid = nd.nodeid "
                    + " and n.corpusid = " + sCorpusID;
            //" and rownum < 100 " ;
        } else {
            sSQL = "select distinct (nd.nodeid) from "
                    + " nodedocument nd, node n "
                    + // " rocbucketvalidation r where " +
                    " where     n.nodeid = nd.nodeid "
                    + " and nd.documentid in ( select documentid from document where repositoryid = " + iRepositoryID + ")";
            //" and rownum < 100 " ;
        }

        Statement stmt = null;
        ResultSet rs = null;
        Vector vNodes = new Vector();
        int loop = 0;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) {
                vNodes.add(rs.getString(1));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            rs.close();
            stmt.close();
            rs = null;
            stmt = null;
        }

        api.Log.Log("TSApplyROC: [" + vNodes.size() + " unique topics found]");

        for (int i = 0; i < vNodes.size(); i++) {
            String sNodeID = (String) vNodes.elementAt(i);
            api.Log.Log("TSApplyROC Processing Node [" + (i + 1) + " of " + vNodes.size() + " ID: " + sNodeID + "]");

            if (iRepositoryID == -1) {
                sSQL = "select " + sCommaListvStrColNames + ", nd.nodeid, nd.documentid, "
                        + " nd.edited from "
                        + " nodedocument nd, node n "
                        + // " rocbucketvalidation r where " +
                        " where n.nodeid = " + sNodeID + " and nd.nodeid = " + sNodeID + " and n.nodeid = nd.nodeid "
                        + " and n.corpusid = " + sCorpusID;
                //" and rownum < 100 " ;
            } else {
                sSQL = "select " + sCommaListvStrColNames + ", nd.nodeid, nd.documentid, "
                        + " nd.edited from "
                        + " nodedocument nd, node n "
                        + // " rocbucketvalidation r where " +
                        " where n.nodeid = " + sNodeID + " and nd.nodeid = " + sNodeID + " and n.nodeid = nd.nodeid "
                        + " and nd.documentid in ( select documentid from document where repositoryid = " + iRepositoryID + ")";
                //" and rownum < 100 " ;
            }

            //api.Log.Log ("TSApplyROC SQL [" + sSQL + "]" );
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            Hashtable htScoresThisNodeDocPair_StrClassifierNameToDoubleScore1Val = new Hashtable();
            Hashtable htIntegerClassifierIDToFloatScore = new Hashtable();

            // for each row in ND for this corpus
            int iNumNDRecsDone = 0;
            boolean bCommit = false;

            Hashtable htStrColNamesToStrClassifierNames = DataCache_RocBucketLocalClassifier.gethtStrColNamesToClassifierNames(dbc);
            //Hashtable htStrStrClassifierNamesToColNames = DataCache_RocBucketLocalClassifier.gethtStrClassifierNamesToColNames();

            int iNumRecordsSkippedCuzEdited = 0;
            Statement stmt2 = dbc.createStatement();
            while (rs.next()) // get nd records with scores in them
            {
                loop = loop + 1;
                for (int iColIndex = 1; iColIndex <= vStrColNames.size(); iColIndex++) {
                    String sColName = (String) vStrColNames.elementAt(iColIndex - 1);
                    String sClassifierName = (String) htStrColNamesToStrClassifierNames.get(sColName);

                    double dScore = rs.getDouble(iColIndex);
                    htScoresThisNodeDocPair_StrClassifierNameToDoubleScore1Val.put(
                            sClassifierName,
                            new Double(dScore));

                }


                int iNodeID = rs.getInt(vStrColNames.size() + 1);
                int iDocumentID = rs.getInt(vStrColNames.size() + 2);
                int iEdited = rs.getInt(vStrColNames.size() + 3);


                // CALL FOR ACTUAL SCORE RESOLUTION
                // CALL FOR ACTUAL SCORE RESOLUTION
                // CALL FOR ACTUAL SCORE RESOLUTION
                if (iEdited == 0) {
                    // note: resolveScore1Val below has side ffect of adding scores to ingoing ht
                    if (loop % 500 == 0) {
                        dbc.commit();
                        api.Log.Log(loop + ". bucketing node [" + iNodeID + "] doc [" + iDocumentID + "]");
                    }
                    resolveScore1Val(htScoresThisNodeDocPair_StrClassifierNameToDoubleScore1Val,
                            vDataROCBucketLocalClassifier, (long) iNodeID, (long) iDocumentID, false, bVerbose, null);

                    double dScore1Val = ((Double) htScoresThisNodeDocPair_StrClassifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCountInt)).doubleValue();
                    double dScore3Val = ((Double) htScoresThisNodeDocPair_StrClassifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCount)).doubleValue();
                    String sSQLUpdate = "update nodedocument set "
                            + " score1 = " + dScore1Val
                            + ", score3 = " + dScore3Val
                            + " where nodeid = " + iNodeID + " and "
                            + " documentid = " + iDocumentID;

                    //api.Log.Log ("applying ROC update [" + loop+ "] [" + sSQLUpdate+ "]" );
                    stmt2.executeUpdate(sSQLUpdate);
                    //stmt2.close();

                    //com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLUpdate, dbc, bCommit, 1);
                    iNumNDRecsDone++;


                    // now that I have all the classifier scores for this
                } else {
                    iNumRecordsSkippedCuzEdited++;
                }
            }
            stmt2.close();
            dbc.commit();

            rs.close();
            stmt.close();
        }

        // If no results found, throw an exception
        if (loop == 0) {
            throw new Exception("No NodeDoc rows found in this corpus to apply ROC to");
        }

        return 0;
    }
    private static int iDebugCallCounter = 0;

    public static void resolveScore1Val(Hashtable htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val,
            Vector vDataRocBucketLocalClassifier, long lNodeID,
            long lDocID, boolean bDoNoClassifyDBAtAll, boolean bVerbose,
            ExplainDisplayDataInstanceTABLE eddiTABLE_ROC) {
        iDebugCallCounter++;

        /*
         * if ( true || iDebugCallCounter == 0) // debug - log only in here { for ( int iBucketIndex = vDataRocBucketLocalClassifier.size()-1; iBucketIndex >= 0 ; iBucketIndex--) {
         * DataCache_RocBucketLocalClassifier dataRoc = (DataCache_RocBucketLocalClassifier) vDataRocBucketLocalClassifier.elementAt(iBucketIndex); double dScoreCutoff = dataRoc.dCUTOFFSCOREGTE;
         * double dScoreThisClassifierThisNodeDoc = ((Double) htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get ( dataRoc.sClassifierName )).doubleValue(); // api.Log.Log(s +
         * iDebugCallCounter + ".FULL SET OF CUTOFFS : iClassifierIDThisBucket [" + dataRoc.sClassifierName + // "] lNodeID [" + lNodeID + "] " + // "] lDocID [" + lDocID + "] " + // "]
         * dataRoc.iROCSETTINGID [" + dataRoc.iROCSETTINGID + "] " + // "] dataRoc.dSCORE1VAL [" + dataRoc.dSCORE1VAL + "] " + // "] and dScoreThisClassifierThisNodeDoc [" +
         * dScoreThisClassifierThisNodeDoc + "] " + // " >= dScoreCutoff [" + dScoreCutoff + "] bucket index (desc) [" + iBucketIndex + "]" ); } }
         */

        boolean bFoundHit = false;

        DataCache_RocBucketLocalClassifier dataRocKeeper = null;
        double dScoreThisClassifierThisNodeDoc = -1.0;

        if (bDoNoClassifyDBAtAll) {
            htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.put(TSApplyROC.sCLASSIFIERNAME_StarCount, new Double(0.0));
            htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.put(TSApplyROC.sCLASSIFIERNAME_StarCountInt, new Double(0.0));
        }




        if (bVerbose) {
            api.Log.Log(iDebugCallCounter + " -- next node --");
        }
        int iBucketIndex = -1;
        //api.Log.Log  ("score results from external classifier [" + UtilSets.htToStr(htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val) + "]" );
        for (iBucketIndex = vDataRocBucketLocalClassifier.size() - 1; iBucketIndex >= 0; iBucketIndex--) {
            DataCache_RocBucketLocalClassifier dataRoc = (DataCache_RocBucketLocalClassifier) vDataRocBucketLocalClassifier.elementAt(iBucketIndex);

            /*
             * api.Log.Log (iDebugCallCounter + ". test iBucketIndex [" + iBucketIndex+ "] node [" + lNodeID + "] doc [" + lDocID + "] class name [" + dataRoc.sClassifierName + "] roc [" +
             * dataRoc.toString() + "]");
             */

            dScoreThisClassifierThisNodeDoc =
                    ((Double) htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get(dataRoc.sClassifierName)).doubleValue();
//            api.Log.Log (iDebugCallCounter + ". dScoreThisClassifierThisNodeDoc [" + dScoreThisClassifierThisNodeDoc + "] test iBucketIndex [" + iBucketIndex+ "] node [" + lNodeID + "] doc [" + lDocID + "] roc [" + dataRoc.toString() + "]");

            if (dScoreThisClassifierThisNodeDoc >= dataRoc.dCUTOFFSCOREGTE) {
                dataRocKeeper = dataRoc;
                if (bVerbose) {
                    api.Log.Log(iDebugCallCounter + ".BUCKET KEEPER [" + dataRocKeeper.iROCSETTINGID
                            + "] Classifier [" + dataRocKeeper.sClassifierName
                            + "] StarCount[" + htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCount) + "] "
                            + "] StarCountInt [" + htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCountInt) + "] "
                            + "] since dScoreThisClassifierThisNodeDoc [" + dScoreThisClassifierThisNodeDoc + "] "
                            + " >= dScoreCutoff [" + dataRocKeeper.dCUTOFFSCOREGTE + "] bucket index we're GT [" + iBucketIndex + "]");
                }
                break;  // found one
            } else {
                if (bVerbose) {
                    api.Log.Log(iDebugCallCounter + ".not this bucket not GTE than this rocid [" + dataRoc.iROCSETTINGID
                            + "] using dataRoc.sClassifierName [" + dataRoc.sClassifierName
                            + "] since NOT dScoreThisClassifierThisNodeDoc [" + dScoreThisClassifierThisNodeDoc + "] "
                            + " >= dScoreCutoff [" + dataRoc.dCUTOFFSCOREGTE + "] bucket index we're not GTE less than [" + iBucketIndex + "]");
                }
            }

        }

        if (dataRocKeeper != null) {
            htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.put(TSApplyROC.sCLASSIFIERNAME_StarCount, new Double(dataRocKeeper.iSTARCOUNT));
            htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.put(TSApplyROC.sCLASSIFIERNAME_StarCountInt, new Double(dataRocKeeper.dSCORE1VAL));
            //vStrClassifierOrScoreName= com.indraweb.util.UtilSets.convertHTKeysToVector(htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val);
            //sSetOfStrClassifierOrScoreName = com.indraweb.util.UtilStrings.convertVecToString(vStrClassifierOrScoreName, ",");
            //api.Log.Log("POST 1 score set[" + sSetOfStrClassifierOrScoreName + "]" );
        } else {
            htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.put(TSApplyROC.sCLASSIFIERNAME_StarCount, new Double(0.0));
            htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.put(TSApplyROC.sCLASSIFIERNAME_StarCountInt, new Double(0.0));
            //vStrClassifierOrScoreName= com.indraweb.util.UtilSets.convertHTKeysToVector(htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val);
            //sSetOfStrClassifierOrScoreName = com.indraweb.util.UtilStrings.convertVecToString(vStrClassifierOrScoreName, ",");
            //api.Log.Log("POST 2 score set[" + sSetOfStrClassifierOrScoreName  + "]" );


            if (bVerbose) {
                api.Log.Log(iDebugCallCounter + ".BACKSTOP 0 star dataRoc.sClassifierName [" + "none"
                        + "] StarCount[" + htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCount) + "] "
                        + "] StarCountInt [" + htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCountInt) + "] ");
            }
        }
        Double DCLASSIFIERNAME_StarCount = (Double) (Double) htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCount);
        Double DCLASSIFIERNAME_StarCountInt = (Double) htScoresThisNodeDocPair_StringClasifierNameToDoubleScore1Val.get(TSApplyROC.sCLASSIFIERNAME_StarCountInt);

        //System.out.println("hbk pre eddiTABLE_ROC != null");
        if (eddiTABLE_ROC != null) {
            // System.out.println("hbk in eddiTABLE_ROC != null");

            eddiTABLE_ROC.addVectorRow("Star Count numeric", "" + DCLASSIFIERNAME_StarCountInt.intValue());
//                    com.indraweb.util.UtilStrings. numFormatDouble
            //              (DCLASSIFIERNAME_StarCountInt.doubleValue(), 1), 0 );
            eddiTABLE_ROC.addVectorRow("Star Count", "" + getStarString(DCLASSIFIERNAME_StarCount), 0);
        } 
    }

    private static String getStarString(Double DStarCount) {
        int iStarCount = (int) DStarCount.doubleValue();
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < iStarCount; i++) {
            sb.append("* ");
        }
        return sb.toString();
    }
}
