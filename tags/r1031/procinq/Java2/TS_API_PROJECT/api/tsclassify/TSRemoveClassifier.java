package api.tsclassify;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Remove a custom classifier from the ITS system.   Note that removing a classifier will cause a cascading
 * delete and may affect the ROC settings of taxonomies within the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CID     Unique classifier identifier of the classifier to be removed.
 *
 *  @note   http://ITSSERVER/itsapi/ts?fn=tsclassify.TSReviewBatchResults&SKEY=-132981656

 *	@return A SUCCESS object if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <SUCCESS>Classifier removed successfully.</SUCCESS>
  </TSRESULT>
 \endverbatim
 */
public class TSRemoveClassifier
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sID = (String) props.get ("CID", true);

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = "delete from Classifier where ClassifierID = "+sID;
			Statement stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from classifier table failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Classifier removed successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
