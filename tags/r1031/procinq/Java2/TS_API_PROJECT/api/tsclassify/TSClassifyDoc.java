package api.tsclassify;

import api.APIProps;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.tsclassify.ROCHelper.DataCache_Classifier;
import api.tsclassify.ROCHelper.DataCache_RocBucketLocalClassifier;
import api.tsmetasearchnode.Data_MSAndScoreResult_acrossSEs;
import api.util.affinity.AffinityPass;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.database.SQLGenerators;
import com.indraweb.encyclopedia.DocForScore;
import com.indraweb.encyclopedia.Topic;
import com.indraweb.execution.Session;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.Summarizer;
import com.indraweb.ir.SummarizerResult;
import com.indraweb.ir.clsStemAndStopList;
import com.indraweb.profiler.Timer;
import com.indraweb.signatures.SigExpandersTitleThesaurus;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.util.UtilFile;
import com.iw.application.context.AppServletContextListener;
import com.iw.classification.Data_WordsToNodes;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.iw.repository.Repository;
import com.iw.rest.beans.Classifier;
import com.iw.scoring.NodeForScore;
import com.iw.scoring.scoring2.ScoresBetweenTwoTextObjects;
import com.iw.system.HashTree;
import com.iw.system.SecurityGroup;
import com.iw.system.User;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * Takes a document from either a multipart stream an http post call, from a URL, or from a database-contained file and classifies it (against one or more of the taxonomies within the system).
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param SKEY Session key corresponding to the current user session.
 * @param URL (optional) Unique document URL or UNC within the system
 * @param DBURL (optional) Document URL value to store in the database.
 * @param MaxNumOutput (optional) Maximum number of classification results to return in the XML return struct.
 * @param GenreID (optional) Genre identifier of this document.
 * @param DocTitle (optional) Title of this document. If none specified, ITS will attempt to extract one from the document.
 * @param DocSummary (optional) Abstract of this document. If none specified, ITS will generate one from the document text.
 * @param Post (optional) Post this result to the database? Boolean true/false, default is false.
 * @param Verbose (optional)
 * @param Batch (optional) Classify this document in batch mode? When invoked in batch mode, classify will write results to a staging table rather than production.
 * @param DocSumm (optional) Create a document summary if one was not given? Boolean true/false, default is true.
 * @param Phrasing (optional) Use phrasing when classifying this document? Boolean true/false, default is true.
 * @param ExplainScores (optional) Expand XML return structure to include an explanation of the results? Boolean true/false, default is false.
 * @param NodeToScoreExplain (optional) This is required when ExplainScores is true. Specifies the node identifier to explain.
 * @param NumScoresToExplain (optional) Number of scores to explain (maximum) if ExplainScores is true. Default is 10.
 * @param INCLUDENUMBERS (optional) Include numbers in the parsed document representation. Default is false.
 * @param NodeSet (optional) A comma delimited string of node id's to restrict the set of nodes that the classifier will operate against. This parameter overrides the system configuration.
 * @param Corpora (optional unless NodeSet specified) List of taxonomy identifiers to classify against, separated by commas. This parameter overrides the system configuration.
 * @param SecGroups (optional) A list of security identifiers representing LDAP groups with permission to this document. Default is no security for this document.
 * @param FulltextURL (optional) The absolute file path location of this document, usually referenced via the UNC
 * @param SweepCache (optional) Specifies if the corpora other than those in the currently specified Corpora parameter.
 * @param NumDocBytesToUse (optional) Specifies the number of bytes from the document that should be considered. Default is 21000.
 *
 * @note http://ITSERVER/servlet/ts?fn=tsclassify.TSApproveReviewedResult&SKEY=801312271&DocumentIDList=2,5,9,14,30
 *
 * @return object if successful.
 * <code>
 *<?xml version="1.0" encoding="UTF-8" ?>
 *<TSRESULT>
 *	<CALLCOUNT_FN>1</CALLCOUNT_FN>
 *	<CLASSIFICATIONRESULTSET>
 *	<NUMNODEDOCSCORES>30923</NUMNODEDOCSCORES>
 *	<NODE>
 *		<INDEX>145</INDEX>
 *		<NODEID>1326035</NODEID>
 *		<NODETITLE>Blood and urine tests</NODETITLE>
 *		<SCORE1>75.0</SCORE1>
 *	</NODE>
 *	<NODE>
 *		<INDEX>174</INDEX>
 *		<NODEID>1325987</NODEID>
 *		<NODETITLE>Multiple chemical sensitivity</NODETITLE>
 *		<SCORE1>75.0</SCORE1>
 *	</NODE>
 * </code>
 */
public class TSClassifyDoc {

    private static final Logger log = Logger.getLogger(TSClassifyDoc.class);
    private static final ResourceBundle rb = ResourceBundle.getBundle("system/configuration");
    private static int iCallCounter = 0;
    private static final Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on
    public static final String sGENERID_CLASSIFIER_INSERTS = "11";
    public static final String sCHANNELID_CLASSIFIER_INSERTS = "2000";
    public static int IDYNAMICNODEID = -999;
    private static final Object oSynchScoreEmitXML = new Object();
    public static int iNumNodesXMLOutput = 0;
    private static final Integer ISynchDBUpdate = new Integer(-1); // hbk 2003 03 01
    private static int iCallCounterThisFn = -1;

    public static void handleTSapiRequest(HttpServletRequest req, HttpServletResponse res, api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("handleTSapiRequest()");
        String[] sArrFileFileNameFullExplainTemp = null;
        int iProp_Dweight;
        int iProp_Sweight;
        long startTime = System.currentTimeMillis();
        try {

            synchronized (ICallCounterSemaphore) {
                iCallCounter++;
            }

            String sKey = null;
            if (AppServletContextListener.getMachineType().equals("Server")) {
                sKey = (String) props.get("SKEY", true);
            }

            //URL to validate the link in the new score.
            String sWebURLEncoded = (String) props.get("sWebURL");
            byte[] sWebURLBytes;
            Base64 base64 = new Base64();
            sWebURLBytes = base64.decode(sWebURLEncoded);
            String sWebURL = new String(sWebURLBytes);

            String sProp_URL = (String) props.get("URL");
            String sData = (String) props.get("inputstream");
            boolean bProp_WantDocSummary = props.getbool("WANTDOCSUMMARY", "true");
            int iProp_DocIDSource = props.getint("DOCIDSOURCE", "-1");

            String sDocAbstract = "None";
            String sUsername = null;
            String sPassword = null;

            // *NEW* 1/11/05 - if a document ID was specified, retrieve the document source and
            // store it in sData.  Each repository subclass has it's own method defined for
            // retrieving the source, so we must retrieve the REPOSITORYCLASSNAME field from the
            // repositorytype table.
            if (iProp_DocIDSource != -1) {
                String sSQL = "select RT.REPOSITORYCLASSNAME, R.USERNAME, R.PASSWORD, D.DOCUMENTSUMMARY, D.FULLTEXT "
                        + "from DOCUMENT D, REPOSITORY R, REPOSITORYTYPE RT "
                        + "where D.DOCUMENTID = " + iProp_DocIDSource + " and D.REPOSITORYID = R.REPOSITORYID and "
                        + "RT.REPOSITORYTYPEID = R.REPOSITORYTYPEID";

                Statement stmt = null;
                ResultSet rs = null;
                String sClassName = "";
                String sPath = "";
                try {
                    stmt = dbc.createStatement();
                    rs = stmt.executeQuery(sSQL);

                    if (rs.next()) {
                        sClassName = rs.getString(1);
                        sUsername = rs.getString(2);
                        sPassword = rs.getString(3);
                        sDocAbstract = rs.getString(4);
                        sPath = rs.getString(5);
                    } else {
                        throw new Exception();
                    }
                    rs.close();
                    rs = null;
                    stmt.close();
                    stmt = null;
                } catch (Exception e) {
                    log.error("The documentID " + iProp_DocIDSource + " was not found in this database.", e);
                    throw e;
                } finally {
                    if (rs != null) {
                        rs.close();
                        rs = null;
                    }
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                }

                Class cls = Class.forName("com.iw.repository." + sClassName);
                Class partypes[] = new Class[2];
                partypes[0] = Class.forName("com.iw.system.HashTree");
                partypes[1] = Class.forName("com.iw.system.HashTree");
                Constructor ct = cls.getConstructor(partypes);
                Object arglist[] = new Object[2];
                arglist[0] = new HashTree();
                arglist[1] = new HashTree();

                Repository R = (Repository) ct.newInstance(arglist);
                R.SetUsername(sUsername);
                R.SetPassword(sPassword);

                sData = R.getSource(sPath);
            }

            // if classify text was streamed in, add HTML headers (until we can eliminate the parser)
            if ((props.get("fileposted") == null) && (sProp_URL != null)) {
                sData = "<HTML>\n<HEAD><TITLE>" + sData.substring(0, 40) + "</TITLE>\n</HEAD>\n<BODY>\n" + sData + "\n</BODY>\n</HTML>";
            }

            // PARSEPARMS
            ParseParms parseparms = new ParseParms(props);
            String sProp_DBURL;
            out.println("<CALLCOUNT_FN>" + iCallCounter + "</CALLCOUNT_FN>");

            // FilePath can refer to either an external web site or to an internal file or folder path
            // relative to {IndraHome}/classify
            // if startswith http then will reference an external web site (special char's in url can cause problems here?)
            // if not provided then a multipart doc attachment assumed
            sProp_DBURL = (String) props.get("DBURL");
            int iProp_NumParentTitleTermsAsSigs = props.getint(SignatureParms.CONST_sNumParentTitleTermsAsSigs, "-1");
            if (iProp_NumParentTitleTermsAsSigs == -1) // should have been passed in - who - fcrawl?  html class ?
            {
                iProp_NumParentTitleTermsAsSigs = SignatureParms.getNumParentTitleTermsAsSigs_Default();
            }

            iProp_Dweight = props.getint("DWeight", "1");
            iProp_Sweight = props.getint("SWeight", "0");
            if (log.isDebugEnabled()) {
                log.debug("DWeight [" + iProp_Dweight + "] SWeight [" + iProp_Sweight + "]");
            }
            ///int iProp_DocIDAbstract = props.getint ("DOCIDABSTRACT" , "-1");
            String sProp_GenreID = (String) props.get("GenreID", sGENERID_CLASSIFIER_INSERTS);
            // optional - default = -1 - 'API Classifier Default'
            String sProp_DocTitle = (String) props.get("DocTitle", null);
            String sProp_DocSummary = (String) props.get("DocSummary", "");

            boolean bProp_post = props.getbool("post", "false");
            boolean bProp_sweepCache = props.getbool("sweepcache", "false");
            boolean bProp_batch = props.getbool("batch", "false");

            if (bProp_batch && !bProp_post) {
                throw new Exception("invalid mode combination - must post to run batch mode");
            }

            int iProp_RepositoryID;
            if (bProp_batch && bProp_post) {
                iProp_RepositoryID = Integer.parseInt((String) props.get("REPOSITORYID", true));
            } else {
                iProp_RepositoryID = Integer.parseInt((String) props.get("REPOSITORYID", "1"));
            }

            boolean bProp_DocSumm = props.getbool("DocSumm", "true");

            // ExplainScores
            boolean bExplainScores = props.getbool("ExplainScores", "false");
            boolean bWantPhrasing = props.getbool("phrasing", "true");
            // NodeToScoreExplain
            String sProp_iNodeToScoreExplain = (String) props.get("NodeToScoreExplain", null); // use callers pref if there
            int iProp_NodeToScoreExplain = -1;
            if (sProp_iNodeToScoreExplain != null) {
                iProp_NodeToScoreExplain = Integer.parseInt(sProp_iNodeToScoreExplain);	// -1 for all
            }            // NumScoresToExplain
            String sProp_NodeSet = (String) props.get("NodeSet", sProp_iNodeToScoreExplain); // overrides existing doc summary if any
            // GET CORPUS SET
            String sProp_Corpora = (String) props.get("Corpora", null); // overrides existing doc summary if any
            if (sProp_NodeSet != null && sProp_Corpora == null) {
                throw new Exception("if a NODESET is passed in then the CORPORA must be also to cover that node");
            }
            if (iProp_NodeToScoreExplain > -1 && sProp_Corpora == null) {
                throw new Exception("if a NODESET is passed in then the CORPORA must be also to cover that node");
            }

            // DYNAMIC node nfsdynamic CLASSIFY
            // 3,TERM||6,TERM2||4,... trigger to say : in dynamic node mode

            if (sProp_Corpora == null) {
                throw new Exception("Corpora parameter required for TSClassifyDoc");
            }


            //String sProp_DCNodeTitle = null;
            // for ROC setting and that thes expansion set
            // should some future
            //int iProp_DCNodeSize = -1;
            String sProp_DCSigs = (String) props.get("Sigs", null);
            if (sProp_DCSigs != null) {
                if (bProp_post) {
                    throw new Exception("If a dynamic node is passed in then post = true is not applicable.");
                }
                iProp_NodeToScoreExplain = IDYNAMICNODEID;
            }
            String sProp_SecGroups = (String) props.get("SecGroups", null); // overrides existing doc summary if any
            String sProp_FulltextURL = (String) props.get("FulltextURL", null); // overrides existing doc summary if any
            boolean bDelClassifiedFile = true;


            // 2002 12 09 Session.cfg.setProp("ExplainScores", sExplainScores); // engine will want to know whether true OR false
            // seems to be used in engine project scoreexplaindata.java
            User u;
            if (sKey != null) {

                u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            } else {
                //The request was done within the same classifier.
                //FIXME Replace this with some logic to not validate against the Key
                u = new User();
                u.SetUsername("Classifier::" + Classifier.getLocalInstance().getId());
            }

            if (u == null) // debug production
            {
                Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                log.error("in TSCLassifyDoc ", e);
                throw e;
            }


            //Ensure user is a member of the admin group
            if (bProp_post && !u.IsMember(out)) // DEBUG production
            {
                Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                log.error("in TSCLassifyDoc ", e);
                throw e;
            }

            boolean bUseNuggets = false; // was from configparams com.indraweb.execution.Session.cfg.getPropBool ("UseNuggets");

            int icfg_classification_DocCountMin = 1;
            //com.indraweb.execution.Session.cfg.getPropInt("cfg_classification_DocCountMin");
            int icfg_classification_SigCountMin = 1;

            com.indraweb.util.IntContainer intContainer_NumNDSDone_OutParmAccounting = new com.indraweb.util.IntContainer();
            com.indraweb.util.IntContainer intContainer_NumNodes_OutParmAccounting = new com.indraweb.util.IntContainer();

            String sURLfull = null;
            boolean bStemOn = Session.cfg.getPropBool("StemOnClassifyNodeDocScoring");
            DocForScore docForScore = null;
            try {
                //BUILD DOC FOR SCORE
                if ((iProp_DocIDSource > 0)) {  // source is located in sData
                    if (iProp_Dweight == 0 && iProp_Sweight == 0) {
                        throw new Exception("both document and supplemental weight are zero");
                    }

                    String sDocTitleToUse = sProp_DocTitle;
                    if (sDocTitleToUse == null) {
                        log.debug("Classify getting title from db - not passed in");
                        String sSQLGetdocTitle = "select doctitle from document where documentid = " + iProp_DocIDSource;
                        sDocTitleToUse = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLGetdocTitle, dbc);
                    }
                    DocForScore docForScoreSupplemental = null;

                    // DOCUMENT
                    if (iProp_Dweight != 0) {
                        docForScore = new DocForScore(
                                "file:///dev/null",
                                sProp_URL,
                                sWebURL,
                                true,
                                bStemOn,
                                sDocTitleToUse,
                                sDocAbstract,
                                bWantPhrasing,
                                null,
                                true,
                                false,
                                iProp_Dweight,
                                parseparms,
                                sData);
                    }

                    // ABSTRACT/SUPPLEMENT
                    if (iProp_Sweight != 0) {
                        docForScoreSupplemental = new DocForScore(
                                "file:///dev/null", // abstract/supplemental
                                sProp_URL,
                                sWebURL,
                                true,
                                bStemOn,
                                sDocTitleToUse,
                                sDocAbstract,
                                bWantPhrasing,
                                null,
                                true,
                                false,
                                iProp_Sweight,
                                parseparms,
                                sData);
                    }

                    if (iProp_Dweight == 0 && iProp_Sweight != 0) {
                        // if no main doc then just supplement
                        docForScore = docForScoreSupplemental;
                    }
                    if (iProp_Dweight != 0 && iProp_Sweight != 0) {
                        docForScore.blendInAnotherDoc(docForScoreSupplemental, bStemOn, log.isDebugEnabled());
                    }

                } else // usual mode - posted file
                {
                    String sURLCorpusRelative = sProp_URL;
                    if (!sURLCorpusRelative.toLowerCase().startsWith("http")) {
                        if (!sURLCorpusRelative.toLowerCase().startsWith("file")) {
                            sURLfull = "file:///" + rb.getString("system.classificationfiles.location") + sURLCorpusRelative;
                        } else {
                            sURLfull = sURLCorpusRelative;
                        }
                    } else {
                        sURLfull = sURLCorpusRelative;
                    }

                    docForScore = new DocForScore(
                            sURLfull,
                            sProp_URL,
                            sWebURL,
                            true,
                            bStemOn,
                            sProp_DocTitle,
                            sProp_DocSummary,
                            bWantPhrasing,
                            null,
                            true,
                            false,
                            1,
                            parseparms,
                            sData);
                } // else the usual posted mode

            } catch (Exception e) {
                log.error("error resolving url or constructing DFS in classify", e);
                throw e;
            }


            // dynamic classify
            NodeForScore nfsDynamicNodeClassify = null;
            if (sProp_DCSigs != null) {

                //int  iProp_CorpusID = (int) props.getint("CorpusID" , null);
                int iProp_NodeSize = (int) props.getint("NodeSize", true);
                String sProp_NodeTitle = (String) props.get("NodeTitle", true);
                //int  iProp_ParentID = (int) props.getint("ParentID" , true);
                Vector vStrPerNodeDBSigWords_ = new Vector();
                Vector vIntPerNodeDBSigCounts_ = new Vector();

                SigExpandersTitleThesaurus.vecTermsFromString(sProp_DCSigs, vStrPerNodeDBSigWords_, vIntPerNodeDBSigCounts_);

                // get vectors into array form for nfs
                String[] sArrSigTermsFinal = new String[vStrPerNodeDBSigWords_.size()];
                int[] iArrSigTermsFinal = new int[vIntPerNodeDBSigCounts_.size()];
                vStrPerNodeDBSigWords_.copyInto(sArrSigTermsFinal);
                for (int i = 0; i < vStrPerNodeDBSigWords_.size(); i++) {
                    iArrSigTermsFinal[i] = ((Integer) vIntPerNodeDBSigCounts_.elementAt(i)).intValue();
                }

                nfsDynamicNodeClassify =
                        new NodeForScore(
                        Integer.parseInt(sProp_Corpora),
                        IDYNAMICNODEID, // nodeid
                        -999, // parent nodeid ,
                        iProp_NodeSize,
                        sArrSigTermsFinal,
                        iArrSigTermsFinal,
                        bStemOn, // want stemmed also
                        0,
                        sProp_NodeTitle,
                        ParseParms.getDelimitersAll_Default());

                //nfsDynamicNodeClassify.printme(0, bStemOn);
            } // if sProp_DCSigs != null

            int iMaxClassifyNDInsertsPerNode = Session.cfg.getPropInt("MaxClassifyNDInsertsPerNode", false, "-1");

            tsClassifydoc(
                    sProp_URL,
                    sProp_DBURL,
                    iProp_RepositoryID,
                    sProp_GenreID,
                    sProp_DocSummary,
                    bProp_post,
                    out,
                    dbc,
                    iProp_NodeToScoreExplain,
                    sProp_DocTitle,
                    bWantPhrasing,
                    bUseNuggets,
                    bProp_batch,
                    bProp_DocSumm,
                    intContainer_NumNDSDone_OutParmAccounting,
                    icfg_classification_DocCountMin,
                    icfg_classification_SigCountMin,
                    intContainer_NumNodes_OutParmAccounting,
                    sProp_Corpora,
                    sProp_NodeSet,
                    bDelClassifiedFile,
                    sURLfull,
                    bStemOn,
                    bExplainScores,
                    //sProp_ExplainScoresShowDocWords ,
                    docForScore,
                    false,
                    null,
                    true,
                    sProp_SecGroups,
                    sProp_FulltextURL,
                    true,
                    u,
                    log.isDebugEnabled(),
                    bProp_WantDocSummary,
                    iProp_DocIDSource,
                    bProp_sweepCache,
                    nfsDynamicNodeClassify,
                    iProp_NumParentTitleTermsAsSigs,
                    iMaxClassifyNDInsertsPerNode);

            // ********* BATCH CLASSIFY WRITE
            if (bProp_post) {
                dbc.commit(); // still need to commit doc table chaneges doc db
            }
        } catch (Exception e21) {
            log.error("top of TSClassifyDoc", e21);
            throw e21;
        } finally {  // ACCOUNTING
            long endTime = System.currentTimeMillis();
            log.info("TSClassifyDoc Handler function total execution time: " + (endTime - startTime) + "ms");
            /*
             * if (bFileIndraDebugTSClassifyProfiler) { Profiler.printReport (); Profiler.clear (); }
             */
            boolean bLeaveTempFileIndicator = com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebug_LeaveClassifyTempFiles.txt");

            // posted files are deleted elsewhere
            // clear DOC temp file
            if (sArrFileFileNameFullExplainTemp != null) {
                if (!bLeaveTempFileIndicator
                        && sArrFileFileNameFullExplainTemp[0] != null
                        && sArrFileFileNameFullExplainTemp[0].toLowerCase().indexOf("classificationtest") < 0) {
                    // delete remove temp file
                    com.indraweb.util.UtilFile.deleteFile(sArrFileFileNameFullExplainTemp[0]);
                } else {
                    log.debug("TSClassify not deleteing position 1 temp file sArrFileFileNameFullExplainTemp[1] [" + sArrFileFileNameFullExplainTemp[0] + "]");
                }
                // clear ABSTRACT temp file
                // delete remove temp file
                if (!bLeaveTempFileIndicator && sArrFileFileNameFullExplainTemp.length > 1
                        && sArrFileFileNameFullExplainTemp[1] != null
                        && sArrFileFileNameFullExplainTemp[1].toLowerCase().indexOf("classificationtest") < 0) {
                    com.indraweb.util.UtilFile.deleteFile(sArrFileFileNameFullExplainTemp[1]);
                } else {
                    //if ( iProp_Sweight !=  0 )
                    //if ( bVerbose )
                    if (sArrFileFileNameFullExplainTemp.length > 1 && sArrFileFileNameFullExplainTemp[1] != null) {
                        log.debug("TSClassify not deleteing position 2 temp file [" + sArrFileFileNameFullExplainTemp[1] + "]");
                    }
                }
            }
        }

    }

    public static int tsClassifydoc(
            String sProp_URL, // doc source
            String sProp_DBURL,
            int iProp_RepositoryID,
            String sProp_GenreID,
            String sProp_DocSummary,
            boolean bProp_post,
            PrintWriter out,
            Connection dbc,
            int iNodeIDToExplain,
            String sProp_DocTitle,
            boolean bWantPhrasing,
            boolean bUseNuggets,
            boolean bProp_batch,
            boolean bProp_DocSumm,
            com.indraweb.util.IntContainer intContainer_NumNDSDone_OutParmAccounting,
            int icfg_classification_DocCountMin,
            int icfg_classification_SigCountMin,
            com.indraweb.util.IntContainer intContainer_NumNodes_outParmAccounting,
            String sProp_Corpora,
            String sProp_NodeSet,
            boolean bDelClassifiedFile,
            String sURLfull,
            boolean bStemOn,
            boolean bExplainScores,
            DocForScore docForScore,
            boolean bQuietXMLModeRefreshMultiCall,
            Data_MSAndScoreResult_acrossSEs data_MSAndScoreResult_acrossSEs,
            boolean bdeletePreExistingNodeDocRels, // false from metasarch currently
            String sProp_SecGroups,
            String sProp_FulltextURL,
            boolean bFullTextColumnPresent,
            User u,
            boolean bVerbose,
            boolean bProp_WantDocSummary,
            int iProp_DocIDSource,
            boolean bProp_sweepCache,
            NodeForScore nfsDynamicClassify,
            int iNumParentTitleTermsAsSigs,
            int iMaxClassifyNDInsertsPerNode)
            throws Exception {

        boolean bIndraDebugListNodeIDBeingScored = UtilFile.bFileExists("/temp/IndraDebugListNodeIDBeingScored.txt");
        if (bProp_sweepCache && sProp_Corpora == null) {
            throw new Exception("invalid call model : bProp_sweepCache && sProp_Corpora == null");
        }

        SignatureParms sigparms = new SignatureParms(new APIProps());
        sigparms.setNumParentTitleTermsAsSigs(iNumParentTitleTermsAsSigs);

        if (bProp_sweepCache) {
            Data_WordsToNodes.refreshCache(dbc, sProp_Corpora, bProp_sweepCache, sigparms);
        }
        long lStarttsClassifydoc = System.currentTimeMillis();
        boolean bDoNoClassifyDBAtAll = false;
        if (dbc.isClosed()) {
            throw new Exception("in TSClassify dbc.isClosed() ");
        }

        try {
            if (sProp_DBURL == null) {
                sProp_DBURL = sProp_URL;
            }


            Hashtable htCorporaToconsider = null;
            Vector vCorporaToConsider = null;
            if (sProp_Corpora != null) {
                htCorporaToconsider = new Hashtable();
                vCorporaToConsider = com.indraweb.util.UtilStrings.splitByStrLen1(sProp_Corpora, ",");
                Enumeration e = vCorporaToConsider.elements();
                while (e.hasMoreElements()) {
                    String sCorpusID = (String) e.nextElement();
                    Integer ICorpusID = new Integer(sCorpusID);
                    htCorporaToconsider.put(ICorpusID, ICorpusID);
                }
            }

            // GET NODE SET
            Hashtable htNodeSet = null;
            if (nfsDynamicClassify == null && sProp_NodeSet != null) {
                htNodeSet = new Hashtable();
                Vector vNodeSet = com.indraweb.util.UtilStrings.splitByStrLen1(sProp_NodeSet, ",");
                Enumeration eNodeSet = vNodeSet.elements();
                while (eNodeSet.hasMoreElements()) {
                    String sNodeID = ((String) eNodeSet.nextElement()).trim();
                    Integer INodeID = new Integer(sNodeID);
                    htNodeSet.put(INodeID, INodeID);
                }
            }

            // Filter tax New Affinity pass
            // Corpus**************************************************************
            long lAffinityPassStart = System.currentTimeMillis();
            AffinityPass affinityPass = new AffinityPass(dbc);
            Vector<String> terms = docForScore.getEnumDocWordsWithTitleWordsFirst_populateThesExpansionTermsPerCorpusOrCorpusSet(icfg_classification_DocCountMin, bStemOn, true, htCorporaToconsider, dbc);
            log.debug("Getting the document terms: {" + terms.size() + "} elements retrieved for the document: " + docForScore.getURL());
            Boolean bAffinityTax;
            for (int i = 0; i < vCorporaToConsider.size(); i++) {
                //TODO: get terms
                bAffinityTax = affinityPass.checkAffinityTermsTax(terms, Integer.parseInt((String) vCorporaToConsider.get(i)));
                if (!bAffinityTax) {
                    log.debug("No terms found that match the affinity terms from the tax.");
                    htCorporaToconsider.remove(vCorporaToConsider.get(i));
                }
            }
            long lAffinityPassEnd = System.currentTimeMillis();
            log.info("Affinity Pass finished in: " + (lAffinityPassEnd - lAffinityPassStart) + "ms");

            int iClassifyMinStarScoreForDBInsert = Session.cfg.getPropInt("ClassifyMinStarScoreForDBInsert");

            if (Session.stopList == null) {
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
            }

            int[] iArrCountNDInsertsThenUpdates = new int[2];
            iArrCountNDInsertsThenUpdates[0] = -1;
            iArrCountNDInsertsThenUpdates[1] = -1;
            try {
                // see if corpus has changes/new sig count cache files

                long lStartDocForScoreCreate = System.currentTimeMillis();

                if (!docForScore.bFileOpenedOk) {
                    return -1;
                } else if (docForScore.getHtWordsNCount(bStemOn).isEmpty()) {
                    return -2;
                } else {
                    //***********************************
                    // DB DOC INSERT 2
                    //***********************************
                    long lDocIDFinal = -1;

                    boolean bError = false;

                    if (iProp_DocIDSource > 0) {
                        lDocIDFinal = (long) iProp_DocIDSource;
                    }
                    if (bProp_post) {
                        try {
                            if (!bProp_batch) {
                                dbc.setAutoCommit(false);
                            }

                            if (iProp_DocIDSource <= 0) {
                                lDocIDFinal = insertOrUpdateDocToDB(sProp_DBURL,
                                        docForScore,
                                        out,
                                        sProp_GenreID,
                                        sProp_DocSummary,
                                        dbc,
                                        iProp_RepositoryID,
                                        htCorporaToconsider,
                                        bProp_batch,
                                        htNodeSet,
                                        data_MSAndScoreResult_acrossSEs,
                                        bdeletePreExistingNodeDocRels,
                                        sProp_SecGroups,
                                        sProp_FulltextURL,
                                        bFullTextColumnPresent,
                                        bStemOn);
                            }
                        } catch (Exception e) {
                            com.indraweb.util.Log.NonFatalError("ROLLBACK POSTING CLASS RESULTS TO DB", e);
                            String sErrMsg = "error during db doc table changes section of classification  - abort classification : " + e.getMessage() + ":"
                                    + "[" + sProp_DBURL + "]";
                            bError = true;
                            if (!bProp_batch) {
                                dbc.rollback();
                            }
                            throw new Exception(sErrMsg);
                        } finally {
                            if (!bProp_batch) {
                                dbc.commit();
                                dbc.setAutoCommit(true);
                            }
                        }
                    } // if post
                    docForScore.setDocID((int) lDocIDFinal);

                    //***********************************
                    // CLASSIFY - FIND NODES FOR THIS DOC, IN FORM OF DOC/TEXT OBJECT PAIRS
                    //***********************************
                    long ldocForScoreCreateTime = System.currentTimeMillis() - lStartDocForScoreCreate;
                    //com.indraweb.util.Log.logClearcr("icfg_classification_DocCountMin:"+icfg_classification_DocCountMin);

                    ScoresBetweenTwoTextObjects[] sbttoArr = null;

                    try {
                        com.indraweb.profiler.Timer timerWrapsCallToClassifyDoc = com.indraweb.profiler.Profiler.getTimer("wraps call to ClassifyText.classifyDoc", true);
                        Vector v =
                                com.iw.classification.ClassifyText.classifyDoc(
                                docForScore,
                                dbc,
                                icfg_classification_DocCountMin,
                                out,
                                bStemOn,
                                iNodeIDToExplain,
                                htCorporaToconsider,
                                bWantPhrasing,
                                intContainer_NumNDSDone_OutParmAccounting,
                                intContainer_NumNodes_outParmAccounting,
                                htNodeSet,
                                bQuietXMLModeRefreshMultiCall,
                                bProp_sweepCache,
                                nfsDynamicClassify,
                                sigparms);
                        sbttoArr = (ScoresBetweenTwoTextObjects[]) v.elementAt(0); // two text o    bjects plus scores
                        ExplainDisplayDataInstanceTABLE eddiTABLE_ROC = (ExplainDisplayDataInstanceTABLE) v.elementAt(1);
                        timerWrapsCallToClassifyDoc.stop();

                        if (sbttoArr == null) {
                            throw new Exception("darr is null in TSClassifyDoc");
                        }

                        if (bVerbose) {
                            com.indraweb.util.Log.logcr(Thread.currentThread().getName() + "dArrSize: doc [" + lDocIDFinal
                                    + "] dArr.length [" + sbttoArr.length + "] url [" + sProp_DBURL + "]");
                        }
                        Timer timerresolvebucket = com.indraweb.profiler.Profiler.getTimer("loop resolve buckets", true);
                        for (int i = 0; i < sbttoArr.length; i++) {

                            int iCorpusId = ((NodeForScore) sbttoArr[i].to1).getCorpusID();
                            Vector vDataROCBucketLocalClassifier =
                                    DataCache_RocBucketLocalClassifier.getVecDataROCBucketLocalClassifier(String.valueOf(iCorpusId), dbc);

                            // RESOLVE SCORE VALUE VIA ROC MULTICLASSIFIER
                            TSApplyROC.resolveScore1Val(sbttoArr[i].htScoresStrClassifierNameToDScore, vDataROCBucketLocalClassifier, ((NodeForScore) sbttoArr[i].to1).getNodeID(),
                                    lDocIDFinal, bDoNoClassifyDBAtAll, bVerbose, eddiTABLE_ROC);
                            if (bExplainScores) {
                                sbttoArr[i].scoreExplainData.emitExplainXML(sbttoArr[i], out, bStemOn);
                            }
                        }
                        timerresolvebucket.stop();
                    } catch (Exception e) {
                        out.println("<IWERROR>");
                        out.println("<ERRMSG>");
                        out.println(e.getMessage());
                        out.println("</ERRMSG>");
                        out.println("</IWERROR>");
                        log.error("error in classifyDoc 1", e);
                        bError = true;
                    }


                    if (!bError) // dont insert to db if error occured
                    {

                        //**********************************
                        // MULTIPLE NODES (PER DOC) SCORE INSERT TO DB
                        //***********************************
                        {
                            // Clean Doc sumary string
                            try {
                                if (sProp_DocSummary != null) {
                                    sProp_DocSummary = com.indraweb.util.UtilStrings.replaceStrInStr(sProp_DocSummary, "\n", "");
                                    sProp_DocSummary = com.indraweb.util.UtilStrings.replaceStrInStr(sProp_DocSummary, "\r", "");
                                    sProp_DocSummary = com.indraweb.util.UtilStrings.replaceStrInStr(sProp_DocSummary, "'", "''");
                                }
                            } catch (Exception e) {
                                log.error("error string replacing on [" + sProp_DocSummary + "]\r\n", e);
                            }

                            try {
                                if (!bProp_batch) {
                                    dbc.setAutoCommit(false);
                                }

                                com.indraweb.profiler.Timer timerWrapsinsertOrUpdateNodeDocInfotoDB =
                                        com.indraweb.profiler.Profiler.getTimer("insertOrUpdateNodeDocInfotoDB", true);

                                if (bProp_post || bProp_WantDocSummary) {
                                    iArrCountNDInsertsThenUpdates = processNodeDocInfo(
                                            sbttoArr,
                                            out,
                                            lDocIDFinal,
                                            dbc,
                                            iClassifyMinStarScoreForDBInsert,
                                            sProp_DocTitle,
                                            bUseNuggets,
                                            bProp_batch,
                                            sProp_DBURL,
                                            bProp_DocSumm,
                                            intContainer_NumNDSDone_OutParmAccounting,
                                            intContainer_NumNodes_outParmAccounting.getiVal(),
                                            bQuietXMLModeRefreshMultiCall,
                                            data_MSAndScoreResult_acrossSEs,
                                            bDoNoClassifyDBAtAll,
                                            bVerbose,
                                            sProp_DocSummary,
                                            bProp_post,
                                            bStemOn,
                                            bIndraDebugListNodeIDBeingScored,
                                            iMaxClassifyNDInsertsPerNode);
                                }
                                timerWrapsinsertOrUpdateNodeDocInfotoDB.stop();

                            } catch (Exception e) {
                                com.indraweb.util.Log.NonFatalError("ROLLBACK POSTING CLASS RESULTS TO DB", e);
                                String sErr = e.getMessage();
                                out.println("<ERROR_ROLLBACK>" + sErr + "</ERROR_ROLLBACK>");
                                if (!bProp_batch) {
                                    dbc.rollback();
                                }
                                log.error("TSCLassifyDoc : error during db node doc section of classification : " + e.getMessage(), e);
                                bError = true;
                            } finally {
                                //Log.Log ("commiting node doc changes in TSClassifyDoc\r\n");

                                if (!bProp_batch) {
                                    dbc.commit();
                                    dbc.setAutoCommit(true);
                                }
                            }
                        }

                        if (!bError) {
                            com.indraweb.profiler.Timer timerWrapsemitNodeDocXML = com.indraweb.profiler.Profiler.getTimer("emitNodeDocXML", true);
                            emitNodeDocXML(
                                    sbttoArr,
                                    out,
                                    iClassifyMinStarScoreForDBInsert,
                                    bStemOn,
                                    dbc,
                                    bProp_batch,
                                    bQuietXMLModeRefreshMultiCall,
                                    u);
                            timerWrapsemitNodeDocXML.stop();
                            //com.indraweb.util.Log.logcr("showScoresResultsXML took [" + UtilProfiling.elapsedTimeMillis(lStartXML) + "]");
                        } else {
                            com.indraweb.util.Log.NonFatalError("was error precall to showScoresResultsXML\r\n");
                        }

                    }		// if not error in classify do node doc updates

                    if (!bQuietXMLModeRefreshMultiCall) {
                        out.println("<CLASSIFYSTATS>");
                        out.println(
                                "<CLASSIFYRESULT>"
                                + " D " + lDocIDFinal
                                + " N " + intContainer_NumNodes_outParmAccounting.getiVal()
                                + " S " + intContainer_NumNDSDone_OutParmAccounting.getiVal()
                                + " NDI " + iArrCountNDInsertsThenUpdates[0]
                                + " NDU " + iArrCountNDInsertsThenUpdates[1]
                                + "</CLASSIFYRESULT>");
                        out.println("<DOCPARSETIMEMS>" + ldocForScoreCreateTime + "</DOCPARSETIMEMS>");
                        out.println("</CLASSIFYSTATS>");
                    } // if not quite mode

                }  // if to be classified doc file opened ok
            } catch (TSException tse) {
                log.error("throwing in TSException tse done EmitGenXML_ErrorInfo.emitException in TSClassifyDoc\r\n");
                EmitGenXML_ErrorInfo.emitException("TSException in TSClassifyDoc", tse, out);
                throw tse;
            }
            long lEndtsClassifydoc = System.currentTimeMillis();
            log.info("TSClassifyDoc function finished in: " + (lEndtsClassifydoc - lStarttsClassifydoc) + "ms");
            return 0; // success

        } catch (Exception e) {
            log.error("An exception ocurred", e);
            throw e;
        }

    } // public static int TsClassifydoc

    static class StrComparer implements Comparator {

        @Override
        public int compare(Object obj1, Object obj2) {
            if ((obj1 == null) || (obj2 == null)) {
                return 0;
            }
            return ((String) obj1).compareTo((String) obj2);
        }
    }    // ***********************************************************

    public static int emitNodeDocXML(ScoresBetweenTwoTextObjects[] dArr,
            // ***********************************************************
            PrintWriter out,
            int iClassifyMinStarScoreForDBInsert,
            boolean bStemOn,
            Connection dbc,
            boolean bProp_batch,
            boolean bQuietXMLModeRefreshMultiCall,
            User u)
            throws Exception {
        boolean bNoMinScore = false;
        if (UtilFile.bFileExists("/temp/IndraDebugNoMinClassifyScore.txt")) {
            bNoMinScore = true;
        }
        //Log.Log ("in showScoresResultsXML t [" + Thread.currentThread().getName()+ "]");
        int iNumdisplayed = 0;
        //if ( iNumdisplayed != 0 )
        synchronized (oSynchScoreEmitXML) {

            if (!bQuietXMLModeRefreshMultiCall) {
                out.println("<CLASSIFICATIONRESULTSET>");
                out.println("<NUMNODEDOCSCORES>" + dArr.length + "</NUMNODEDOCSCORES>");
            }

            //out.println ( "<NUMNODEDOCSCORES>"+dArr.length+"</NUMNODEDOCSCORES>");
            //com.indraweb.util.Log.logClearcr("# node doc scores this doc :"+dArr.length);
            //com.indraweb.util.Log.logClear ( "<NUMNODEDOCSCORES>"+dArr.length+"</NUMNODEDOCSCORES>\r\n");// hbk control

            // debug Hashtable htAuth = u.GetAuthorizedHash(out);
            // Hashtable htAuth = u.GetAuthorizedHash(out);

            if (!bProp_batch) {

                // for each node (?)
                DocForScore dfs = null;

                for (int i = 0; i < dArr.length; i++) {
                    // IF AT MAX TO DISPLAY
                    // SCORE STORAGE OBJECT
                    ScoresBetweenTwoTextObjects scoresBetweenTwoTextObjects = dArr[i];
                    dfs = (DocForScore) scoresBetweenTwoTextObjects.getTextObject2();

                    // NODE OBJECT
                    NodeForScore nfs = (NodeForScore) scoresBetweenTwoTextObjects.getTextObject1();
                    long lNodeID = nfs.getNodeID();
                    String sNodeTitle = nfs.getTitle(bStemOn);

                    // only write nodes to which the user has access
                    if (1 == 1) //if (htAuth.containsKey(iCorpusID+""))                // debug only ! hbk control 2003 0
                    {// {
                        // GET SCORE #1
                        double dScoreStarCountBucket = ((Double) scoresBetweenTwoTextObjects.getScore(
                                (TSApplyROC.sCLASSIFIERNAME_StarCountInt))).doubleValue();

                        if (dScoreStarCountBucket >= iClassifyMinStarScoreForDBInsert || bNoMinScore) {
                            //if (dScoreStarCountBucket >= 50) {
                            // GET SCORE #2
                            if (!bQuietXMLModeRefreshMultiCall) {
                                if (i == 0) {
                                    dfs = (DocForScore) scoresBetweenTwoTextObjects.getTextObject2();
                                }
                                out.println("<NODE>");
                                out.println("<INDEX>" + i + "</INDEX>");
                                out.println("<NODEID>" + lNodeID + "</NODEID>");
                                iNumNodesXMLOutput++;

                                String sDocSumm = nfs.getGist();
                                if (sDocSumm != null) {
                                    sDocSumm = sDocSumm.replace('\r', ' ');
                                    sDocSumm = sDocSumm.replace('\n', ' ');
                                }
                                out.println("<DOCSUMMARY><![CDATA[" + sDocSumm + "]]></DOCSUMMARY>");
                                if (!bStemOn) {
                                    out.println("<NODETITLE><![CDATA[" + sNodeTitle + "]]></NODETITLE>");
                                } else {
                                    String sTitleNodeUnStem = nfs.getTitle(false);
                                    //out.println ( "<NODETITLE><![CDATA[" + sTitleNodeUnStem + "]]></NODETITLE>" );
                                    //out.println ( "<NODETITLESTEM><![CDATA[" + sNodeTitle + "]]></NODETITLESTEM>" );
                                    out.println("<NODETITLE><![CDATA[" + sTitleNodeUnStem + "]]></NODETITLE>");
                                    out.println("<NODETITLESTEM><![CDATA[" + sNodeTitle + "]]></NODETITLESTEM>");
                                }
                                try {
                                    Vector vStrColNamesSorted = new Vector();
                                    vStrColNamesSorted.add("SCORE1");
                                    vStrColNamesSorted.add("SCORE2");
                                    vStrColNamesSorted.add("SCORE3");
                                    vStrColNamesSorted.add("SCORE4");
                                    vStrColNamesSorted.add("SCORE5");
                                    vStrColNamesSorted.add("SCORE6");
                                    vStrColNamesSorted.add("SCORE7");
                                    vStrColNamesSorted.add("SCORE8");
                                    vStrColNamesSorted.add("SCORE9");
                                    vStrColNamesSorted.add("SCORE10");
                                    vStrColNamesSorted.add("SCORE11");
                                    vStrColNamesSorted.add("SCORE12");

                                    //New score
                                    vStrColNamesSorted.add("SCORE0");

                                    // then emit in sorted order the score col names and scores
                                    Enumeration enumStrColNamesSorted = vStrColNamesSorted.elements();
                                    while (enumStrColNamesSorted.hasMoreElements()) {
                                        //String sColName = DataCache_Classifier.getStrColNameFromStrClassifierName((String) enumStrColNamesSorted.nextElement(), dbc);
                                        String sColName = (String) enumStrColNamesSorted.nextElement();
                                        String sClassifierName = DataCache_Classifier.getStrClassifierNameFromStrColName(sColName, dbc);
                                        if (sClassifierName == null) {
                                            throw new Exception("No Classifier name for col [" + sColName + "]");
                                        }
                                        Double DScore = (Double) dArr[i].htScoresStrClassifierNameToDScore.get(sClassifierName);
                                        if (DScore == null) {
                                            throw new Exception("External classifier callout : failed to generate score for Classname [" + sClassifierName + "]");
                                        }
                                        out.println("<" + sColName + ">" + com.indraweb.util.UtilStrings.numFormatDouble(DScore.doubleValue(), 3) + "</" + sColName + ">");
                                    }
                                } catch (Exception e) {
                                    log.error("exception in classify xml out scores\r\n", e);
                                }
                                out.println("</NODE>");

                            }
                            iNumdisplayed++;
                        }
                    }
                }
            }

            com.indraweb.util.stem.Porter.iNumStemCalls = 0;
            com.indraweb.util.stem.Porter.ltimeInStem = 0;
            if (!bQuietXMLModeRefreshMultiCall) {
                out.println("</CLASSIFICATIONRESULTSET>");
            }
            out.flush();
        }
        return iNumdisplayed;

    }  // private static void showScoresResultsXML (ScoresBetweenTwoTextObjects[] dArr, Connection dbc )

    // DOC TABLE INSERT *************************************************
    private static long insertOrUpdateDocToDB(
            // *************************************************
            String sURLCorpusDB,
            DocForScore docForScore,
            PrintWriter out,
            String sGenreID,
            String sProp_DocSummary,
            Connection dbc,
            int iRepositoryID,
            Hashtable htCorporaToconsider,
            boolean bProp_batch,
            Hashtable htNodeSet,
            Data_MSAndScoreResult_acrossSEs data_MSAndScoreResult_acrossSEs,
            boolean bdeletePreExistingNodeDocRels,
            String sProp_SecGroups,
            String sProp_FulltextURL,
            boolean bFullTextColumnPresent,
            boolean bStemOn)
            throws Exception {
        com.indraweb.profiler.Timer timer = com.indraweb.profiler.Profiler.getTimer("insertOrUpdateDocToDB ", true);
        long ldocIDPreExisting_ThisURL = -1;
        //        Timer timer_insertOrUpdateDocToDB = Profiler.getTimer("method TsClassifydoc->insertOrUpdateDocToDB", true);
        long lDocIDFinal = -1;
        try {
            synchronized (ISynchDBUpdate) {
                iCallCounterThisFn++;
                String sURLCorpusDB_doubledQuotes = com.indraweb.util.UtilStrings.replaceStrInStr(sURLCorpusDB, "'", "''");

                String sSQL1_doesRecordExistYet_FilterStatus = "select documentid,  filterstatus from "
                        + " document where "
                        + " DOCURL = '" + sURLCorpusDB_doubledQuotes + "'";

                int iDocEditedFlag = -1;
                boolean bDocEditedFlag = false;

                Statement stmt = null;
                ResultSet rs = null;

                try {
                    stmt = dbc.createStatement();
                    rs = stmt.executeQuery(sSQL1_doesRecordExistYet_FilterStatus);

                    if (rs.next()) {
                        ldocIDPreExisting_ThisURL = rs.getLong(1);
                        iDocEditedFlag = rs.getInt(2);
                        bDocEditedFlag = (iDocEditedFlag == 2);
                    }
                } catch (Exception e) {
                    log.error("error in ExecuteQueryAgainstDBlong sSQL1_doesRecordExistYet_FilterStatus [" + sSQL1_doesRecordExistYet_FilterStatus + "]", e);
                    throw e;
                } finally {
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                    if (rs != null) {
                        rs.close();
                        rs = null;
                    }
                }

                // ************** DOCUMENT table if update not insert
                // if in batch mode particularly set up for an insert statement only - in the else
                if (data_MSAndScoreResult_acrossSEs != null) {
                    if (ldocIDPreExisting_ThisURL < 0) {
                        data_MSAndScoreResult_acrossSEs.incrDocToDBByInsert();
                    } else {
                        if (bDocEditedFlag) {
                            data_MSAndScoreResult_acrossSEs.incrDocNotUpdateDueToEditFlag();
                        } else {
                            data_MSAndScoreResult_acrossSEs.incrDocToDBByUpdate();
                        }
                    }
                }

                String sDocSummCleaned = null;

                if (docForScore.getDocSumm() != null && !docForScore.getDocSumm().equals("")) {
                    sDocSummCleaned = com.indraweb.util.UtilStrings.replaceStrInStr(docForScore.getDocSumm(), "'", "''");
                    sDocSummCleaned = com.indraweb.util.UtilStrings.replaceStrInStr(sDocSummCleaned, "\n", " ");
                    sDocSummCleaned = com.indraweb.util.UtilStrings.replaceStrInStr(sDocSummCleaned, "\r", " ");

                    if (sDocSummCleaned.length() > 800) {
                        sDocSummCleaned = sDocSummCleaned.substring(0, 800);
                    }
                }

                if (!sProp_DocSummary.equals("")) {
                    sDocSummCleaned = sProp_DocSummary;
                }

                // IF DOCID PRE-EXISTED - UPDATE NEEDED
                if (ldocIDPreExisting_ThisURL > 0) // if update needed
                {
                    if (bdeletePreExistingNodeDocRels) {
                        deletePreExistingNodeDocRels(ldocIDPreExisting_ThisURL,
                                htCorporaToconsider,
                                dbc,
                                bProp_batch,
                                sURLCorpusDB_doubledQuotes,
                                htNodeSet);
                    }
                    // OK node doc rel's erased - now update doc
                    Vector vStringsUpdateList = new Vector();
                    Vector vStringsWhereList = new Vector();


                    if (!bDocEditedFlag) {
                        String sTitleCleaned = com.indraweb.util.UtilStrings.replaceStrInStr(docForScore.getTitle(false), "'", "''");
                        if (sTitleCleaned.length() > 255) {
                            sTitleCleaned = sTitleCleaned.substring(0, 255);
                        }
                        vStringsUpdateList.addElement("DOCTITLE =  '" + sTitleCleaned + "'");

                        if (sDocSummCleaned != null) {
                            vStringsUpdateList.addElement("DOCUMENTSUMMARY =  '" + sDocSummCleaned + "'");
                        }

                        vStringsUpdateList.addElement("GENREID =  '" + sGenreID + "'");
                        vStringsUpdateList.addElement("REPOSITORYID = " + iRepositoryID);
                    }

                    // these two regardless is edited flag or not
                    vStringsUpdateList.addElement("DATELASTFOUND = sysdate");
                    vStringsWhereList.addElement(" documentid  = " + ldocIDPreExisting_ThisURL);

                    vStringsUpdateList.addElement("DOCSIZE = " + docForScore.getHtWordsNCount(bStemOn).size());

                    String sSQLDocUpdate = SQLGenerators.genSQLUpdate // update document
                            (
                            "document",
                            vStringsUpdateList,
                            vStringsWhereList);

                    // IF BATCH - EMIT STRING FOR DOC UPDATE
                    executeUpdateToDB(sSQLDocUpdate, dbc, false, false, 1);
                    //com.indraweb.util.Log.logcr("Done sSQLDocUpdatein [" + UtilProfiling.elapsedTimeMillis ( lStart_sSQLDocUpdate ) + "] " +
                    //       " sSQLDocUpdate [" + sSQLDocUpdate + "]" );

                    // STREAM CHANGE HBK CONTROL 2003 02 04 out.println("<DBUPDATEPREEXISTINGDOC>" + "ID:" + ldocIDPreExisting_ThisURL + "</DBUPDATEPREEXISTINGDOC>");

                    lDocIDFinal = ldocIDPreExisting_ThisURL;
                } // if doc ID pre-existed
                // ************** DOCUMENT table  if insert not update
                else if (ldocIDPreExisting_ThisURL <= 0) // if insert then
                {
                    lDocIDFinal = JDBCIndra_Connection.executeQueryAgainstDBLong("select document_seq.nextval from dual", dbc);
                    Vector vStrings_colNames = new Vector();
                    Vector vStrings_Values = new Vector();

                    vStrings_colNames.addElement("DOCUMENTID");

                    vStrings_Values.addElement("" + lDocIDFinal);

                    vStrings_colNames.addElement("GENREID");
                    vStrings_Values.addElement(sGenreID);

                    vStrings_colNames.addElement("REPOSITORYID");
                    vStrings_Values.addElement("" + iRepositoryID);

                    String sTitleCleaned = com.indraweb.util.UtilStrings.replaceStrInStr(docForScore.getTitle(false), "'", "''");
                    if (sTitleCleaned.length() > 255) {
                        sTitleCleaned = sTitleCleaned.substring(0, 255);
                    }
                    vStrings_colNames.addElement("DOCTITLE");
                    vStrings_Values.addElement("'" + sTitleCleaned + "'");

                    if (sDocSummCleaned != null) {
                        vStrings_colNames.addElement("DOCUMENTSUMMARY");
                        vStrings_Values.addElement("'" + sDocSummCleaned + "'");
                    }

                    vStrings_colNames.addElement("DOCURL");
                    vStrings_Values.addElement("'" + sURLCorpusDB_doubledQuotes + "'");

                    if (bFullTextColumnPresent) {
                        vStrings_colNames.addElement("FULLTEXT");
                        if (sProp_FulltextURL == null) {
                            vStrings_Values.addElement("'" + sURLCorpusDB_doubledQuotes + "'");
                        } else {
                            String sProp_FulltextURL_doubledQuotes = com.indraweb.util.UtilStrings.replaceStrInStr(sProp_FulltextURL, "'", "''");
                            vStrings_Values.addElement("'" + sProp_FulltextURL_doubledQuotes + "'");
                        }
                    }

                    vStrings_colNames.addElement("DATELASTFOUND");
                    vStrings_Values.addElement("sysdate");

                    vStrings_colNames.addElement("DOCSIZE");
                    vStrings_Values.addElement("" + docForScore.getHtWordsNCount(bStemOn).size());

                    String sSQL1_insertDocTable = SQLGenerators.genSQLInsert("document", vStrings_colNames, vStrings_Values);

                    try {
                        JDBCIndra_Connection.executeUpdateToDB(sSQL1_insertDocTable, dbc, false, 1);
                    } catch (Exception e5) {
                        log.error("error 1 in executeUpdateToDB on SQL [" + sSQL1_insertDocTable + "]", e5);
                        throw e5;
                    }
                    out.println("<DBINSERTDOC>" + "ID:" + lDocIDFinal + "</DBINSERTDOC>");
                }
            } // // synchronized ( ISynchDBUpdate )
        } finally {
            if (sProp_SecGroups != null) {
                Vector vGroupsToAdd = com.indraweb.utils.strings.UtilStrings.splitByStrLen1(sProp_SecGroups, ";");
                Enumeration eStringGroups = vGroupsToAdd.elements();

                Vector v = new Vector();
                while (eStringGroups.hasMoreElements()) {
                    String sGroup = (String) eStringGroups.nextElement();
                    SecurityGroup sg = api.security.DocumentSecurity.getSecurityByName(sGroup.toUpperCase());

                    if (sg != null) {
                        v.add(sg);
                    }
                }

                api.tsdocument.TSEditDocProps.InsertDocumentSecurity((int) lDocIDFinal, v, dbc, out);
            } else {
                Vector v = new Vector();

                SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID("0");
                v.add(sg);

                api.tsdocument.TSEditDocProps.InsertDocumentSecurity((int) lDocIDFinal, v, dbc, out);
            }

//          timer_insertOrUpdateDocToDB.stop();

            timer.stop();
        }
        //com.indraweb.util.Log.logcr("Done insertOrUpdateDocToDB in [" + UtilProfiling.elapsedTimeMillis(lStart) + "]");
        return lDocIDFinal;
    } // doc insert update

    public static int[] processNodeDocInfo(
            ScoresBetweenTwoTextObjects[] dArr,
            PrintWriter out,
            long lDocIDFinal,
            Connection dbc,
            double dClassifyMinStarScoreForDBInsert,
            String sProp_Doctitle,
            boolean bUseNuggets,
            boolean bProp_batch,
            String sURLForDB,
            boolean bProp_DocSumm,
            com.indraweb.util.IntContainer intContainer_NumNDSDone_OutParmAccounting,
            int iNumNodes,
            boolean bQuietXMLModeRefreshMultiCall,
            Data_MSAndScoreResult_acrossSEs data_MSAndScoreResult_acrossSEs,
            boolean bDoNoClassifyDBAtAll,
            boolean bVerbose,
            String sProp_DocSummary,
            boolean bProp_post,
            boolean bStemOn,
            boolean bIndraDebugListNodeIDBeingScored,
            int iMaxClassifyNDInsertsPerNode)
            throws Exception {

        boolean bDebugBooleanDontDoFinalNDInsert = Session.cfg.getPropBool("DebugBooleanDontDoFinalNDInsert", false, "false");

        long lstart = System.currentTimeMillis();
        if (bVerbose) {
            com.indraweb.util.Log.log("entering ND insert loop with [" + dArr.length + "] node doc pair candidates");
        }
        String sNodeDocTableRealOrStage = null;
        if (bProp_batch) {
            sNodeDocTableRealOrStage = "STAGENODEDOCINSERT";
        } else {
            sNodeDocTableRealOrStage = "NODEDOCUMENT";
        }

        int[] iArrCountNDInsertsThenUpdates = new int[2];
        int iNumInserted = 0;
        DocForScore dfs = null;
        if (bProp_post && !bProp_batch && !bQuietXMLModeRefreshMultiCall) {
            out.println("<DBCHANGESNODEDOC>");
            out.println("<DOCUMENTID>" + lDocIDFinal + "</DOCUMENTID>");
        }
        if (bVerbose || bIndraDebugListNodeIDBeingScored) {
            log.debug("# node doc scores done [" + dArr.length + "]");
        }
        for (int i = 0; i < dArr.length && (iMaxClassifyNDInsertsPerNode < 0 || iNumInserted < iMaxClassifyNDInsertsPerNode); i++) // pre node within this one doc
        {

            ScoresBetweenTwoTextObjects sbtto = dArr[i];
            NodeForScore nfs = (NodeForScore) sbtto.getTextObject1();
            dfs = (DocForScore) sbtto.getTextObject2();
            long lNodeID = nfs.getNodeID();
            if (bIndraDebugListNodeIDBeingScored) {
                log.debug("node being scored [" + lNodeID + "] [" + (i + 1) + "] of [" + dArr.length + "]");
            }
            double dScoreStarCountInt = ((Double) sbtto.htScoresStrClassifierNameToDScore.get(
                    TSApplyROC.sCLASSIFIERNAME_StarCountInt)).doubleValue();


            if ((dScoreStarCountInt >= dClassifyMinStarScoreForDBInsert)) //if (dScoreStarCountInt >= 50)
            {

                Hashtable htStrColNamesToDoubleScoreValues = new Hashtable();
                Iterator iterStrClassifierNames = DataCache_Classifier.gethsAllStrClassifierNames(dbc).iterator();
                //sbtto.printScores();
                while (iterStrClassifierNames.hasNext()) {
                    String sClassifierName = (String) iterStrClassifierNames.next();
                    Double DScoreThisClassifierName = sbtto.getScore(sClassifierName);
                    String sColName = DataCache_Classifier.getStrColNameFromStrClassifierName(sClassifierName, dbc);
                    htStrColNamesToDoubleScoreValues.put(sColName, DScoreThisClassifierName);
                }

                // *********
                // INSERT TO NODEDOCUMENT (update if exception)
                // *********

                Vector vFieldNames = new Vector();
                Vector vValues = new Vector();

                vFieldNames.addElement("NODEID");
                vValues.addElement("" + lNodeID);

                vFieldNames.addElement("DOCUMENTID");
                // IF BATCH
                vValues.addElement("" + lDocIDFinal);

                vFieldNames.addElement("DOCUMENTTYPE");
                vValues.addElement("1");

                // SCORES (INSERT MODE)
                Enumeration enumStrColNames = htStrColNamesToDoubleScoreValues.keys();
                while (enumStrColNames.hasMoreElements()) {
                    String sColName = (String) enumStrColNames.nextElement();
                    Double DScore = (Double) htStrColNamesToDoubleScoreValues.get(sColName);

                    vFieldNames.addElement(sColName);
                    vValues.addElement("" + DScore.toString()); // master score used by all
                }

                String[] sArrSigs = nfs.getSarr(bStemOn, true, true);
                Vector vSigs = new Vector();
                for (int j = 0; j < sArrSigs.length; j++) {
                    vSigs.addElement(sArrSigs[j]);
                }

                SummarizerResult sumResult = null;
                boolean bDoNodeDocGist = (sProp_DocSummary == null || sProp_DocSummary.equals("")) && (bUseNuggets || bProp_DocSumm);
                if (bDoNodeDocGist) {
                    try {
                        sumResult = Summarizer.createSummary3_snippet(
                                lNodeID, // INodeID.intValue(),
                                dfs.getURL(),
                                dfs.getTopic(),
                                600,
                                20,
                                vSigs,
                                nfs.getTitle(false),
                                bUseNuggets);
                    } catch (Exception e) {
                        log.error("ERROR IN SUMMARY CREATE FOR NODEID [" + lNodeID + "]");
                        throw e;
                    }

                    //com.indraweb.util.Log.logcr("createSummary3_snippet  took [" + UtilProfiling.elapsedTimeMillis(lStart_Snippet) + "]");
                }

                if (bProp_post && bUseNuggets) {
                    insertSnippetNuggetContent(lNodeID, lDocIDFinal, sumResult, dbc, bProp_batch);
                }
                // either create a summary or use first x words
                String sDocSummaryFinal = "";
                if (bDoNodeDocGist) {
                    if (bProp_DocSumm) {
                        //long lStart_summ = System.currentTimeMillis();
                        sDocSummaryFinal = sumResult.getDocSummary(false);

                        //com.indraweb.util.Log.logcr("creating a real summ for summary in [" + UtilProfiling.elapsedTimeMillis(lStart_summ) + "]");
                    } else {
                        //com.indraweb.util.Log.logcr("just using first X words for summary");
                        Topic t = dfs.getTopic();
                        String sAllTextInOne = t.sbAllTextInOne.toString();
                        int iLenToKeep = com.indraweb.util.UtilSets.min(200, (sAllTextInOne.length() - 1));
                        sDocSummaryFinal = sAllTextInOne.substring(0, iLenToKeep);
                    }
                }

                sDocSummaryFinal = com.indraweb.util.UtilStrings.replaceStrInStr(sDocSummaryFinal, "'", "''");

                if (!sDocSummaryFinal.trim().equals("") && sDocSummaryFinal != null) {
                    vFieldNames.addElement("DOCSUMMARY");
                    vValues.addElement("'" + sDocSummaryFinal.substring(0,
                            com.indraweb.util.UtilSets.min(999, sDocSummaryFinal.length())) + "'");
                    nfs.setGist(sDocSummaryFinal);
                    sbtto.setTextObject1(nfs);
                    nfs = (NodeForScore) sbtto.getTextObject1();
                }

                vFieldNames.addElement("DATESCORED");
                vValues.addElement("sysdate");

                String sSQLInsertNODEDOCUMENT = null;
                sSQLInsertNODEDOCUMENT = SQLGenerators.genSQLInsert(// insert into nodedocument
                        sNodeDocTableRealOrStage + " ",
                        vFieldNames,
                        vValues);
                iArrCountNDInsertsThenUpdates[0]++;
                // INSERT NODEDOCUMENT

                // IF OR NOBATCH
                if (bProp_post && !bDoNoClassifyDBAtAll) {
                    try {
                        if (!bDebugBooleanDontDoFinalNDInsert) {
                            JDBCIndra_Connection.executeInsertToDB(sSQLInsertNODEDOCUMENT, dbc, false, false);
                            iNumInserted++;
                        }
                        if (!bProp_batch && !bQuietXMLModeRefreshMultiCall) {
                            out.println("<DBINSERTNODEDOC" + i + ">" + "IDs:node:" + lNodeID + ";doc:" + lDocIDFinal + "</DBINSERTNODEDOC" + i + ">");
                        }

                    } catch (Exception e) {
                        // assuming dup key exception on nodedoc insert, update instead
                        log.error("unable to insert ND rec [" + sSQLInsertNODEDOCUMENT + "]", e);
                        if (bVerbose) {
                            log.debug(i + ". exception on insert (edited?) - skipping [" + sSQLInsertNODEDOCUMENT + "] [" + e.getMessage() + "]");
                        }
                    }
                }

            } // if ((bInsertZeroStars || dScore3001_Bucket > 0) || dScore4 > 1 )
            else {
                if (bVerbose) {
                    log.debug(i + ". node [" + lNodeID + "] doc [" + lDocIDFinal + "] starint [" + dScoreStarCountInt + "] < [" + dClassifyMinStarScoreForDBInsert + "] ");
                }


            }
        }  // for ( int i = 0; i < dArr.length; i++ )
        if (iNumInserted > 500 || bVerbose) {
            com.indraweb.util.Log.log(Thread.currentThread().getName() + ": iNumInserted : [" + iNumInserted + "] completed ND docid [" + lDocIDFinal + "] insert loop with dArr.length [" + dArr.length + "] and iNumInserted [" + iNumInserted + " node doc pairs in [" + (System.currentTimeMillis() - lstart) + "] ms");
        }

        com.indraweb.profiler.Profiler.addStatistic("DocID", "" + lDocIDFinal);
        com.indraweb.profiler.Profiler.addStatisticCount("#NDinsert", iNumInserted);

        if (bProp_post && !bProp_batch && !bQuietXMLModeRefreshMultiCall) {
            out.println("</DBCHANGESNODEDOC>");
        }


        /*
         * classifyStatCollector.addDataSetOneClassify( iNumNodes, 1 intContainer_NumNDSDone_OutParmAccounting.i, iNum0Star, iNum1Star, iNum2Star, iNum3Star );
         */

        //com.indraweb.util.Log.logcr("Done insertOrUpdateNodeDocInfotoDB in [" + UtilProfiling.elapsedTimeMillis(lStart) + "] iNumInserted [" + iNumInserted + "]");
        return iArrCountNDInsertsThenUpdates;
    }
    /*
     * SQL> desc documentchannel; Name Null? Type ----------------------------------------- -------- ---------------------------- DOCUMENTID NOT NULL NUMBER(9) CHANNELID NOT NULL NUMBER(4) NODEID NOT
     * NULL NUMBER(12)
     *
     * SQL> desc nodedocument; Name Null? Type ----------------------------------------- -------- ---------------------------- NODEID NOT NULL NUMBER(12) DOCUMENTID NOT NULL NUMBER(9) DOCUMENTTYPE
     * NUMBER(2) SCORE1 NUMBER(63) SCORE2 NUMBER(63) SCORE3 NUMBER(63) SCORE4 NUMBER(63) SCORE5 NUMBER(63) SCORE6 NUMBER(63) DOCSUMMARY VARCHAR2(1000) DATESCORED DATE
     */

    // hbk timer }
    /*
     * <html> <form name="import" method="POST" ENCTYPE="multipart/form-data" action="http://66.134.131.40:8100/servlet/ts"> <blockquote> <input type="hidden" name="fn"
     * value="tsclassify.TSClassifyDoc"> <input type="hidden" name="URL" value="/corpussource/classific"> <b>Classify Document:</b> <input type="file" class="Wf" name="import" id="attFbutton"> <input
     * type="submit" name="submit" value="Import" > </blockquote> </form> </html>
     *
     *
     */
    static void insertSnippetNuggetContent(long lNodeID,
            long lDocIDFinal,
            SummarizerResult sumResult,
            Connection dbc,
            boolean bProp_batch)
            throws Exception {
        com.indraweb.profiler.Timer timer = com.indraweb.profiler.Profiler.getTimer("3 insertSnippetNuggetContent", true);
        //com.indraweb.util.Log.logcr("in insertSnippetNuggetContent");
        //Nodedocumentsnippet snippet nugget / insert nugget then snippet then ndsnippet
/*
         * SQL> Name Null? Type ----------------------------------------- -------- ---------------------------- NUGGETID NOT NULL NUMBER(9) NUGGET VARCHAR2(4000)
         *
         * SQL> desc snippet; Name Null? Type ----------------------------------------- -------- ---------------------------- SNIPPETID NOT NULL NUMBER(9) NUGGETID NOT NULL NUMBER(9) SNIPPET
         * VARCHAR2(4000)
         *
         * SQL> desc nodedocumentsnippet; Name Null? Type ----------------------------------------- -------- ---------------------------- NODEID NOT NULL NUMBER(12) DOCUMENTID NOT NULL NUMBER(9)
         * SNIPPETID NOT NULL NUMBER(9)
         */

        try {
            if (!bProp_batch) {
                if (sumResult.vStrDocSnippets.size() != sumResult.vStrDocNuggets.size()) {
                    throw new Exception("sumResult.vStrDocSnippets.size() != sumResult.vStrDocNuggets.size()");
                }

                int iMax = sumResult.vStrDocSnippets.size();
                for (int i = 0; i < iMax; i++) {

                    // NUGGET
                    long lNuggetID = JDBCIndra_Connection.executeQueryAgainstDBLong("select nugget_seq.nextval from dual", dbc);
                    //com.indraweb.util.Log.logClear ("lNuggetID:" + lNuggetID);	 // hbk control
                    String sqlInsertNug = "insert into nugget ( NUGGETID, NUGGET ) "
                            + " values (" + lNuggetID + ", "
                            + "'" + com.indraweb.util.UtilStrings.replaceStrInStr((String) sumResult.vStrDocNuggets.elementAt(i), "'", "''") + "')";
                    JDBCIndra_Connection.executeInsertToDB(sqlInsertNug, dbc, false);


                    // SNIPPET
                    long lSnippetID = JDBCIndra_Connection.executeQueryAgainstDBLong("select snippet_seq.nextval from dual", dbc);
                    //com.indraweb.util.Log.logClear ("lSnippetID:" + lSnippetID);	 // hbk control
                    String sqlInsertSnip = "insert into snippet (SNIPPETID, NUGGETID, SNIPPET ) "
                            + " values ("
                            + lSnippetID + ", "
                            + lNuggetID + ", "
                            + "'" + com.indraweb.util.UtilStrings.replaceStrInStr((String) sumResult.vStrDocSnippets.elementAt(i), "'", "''") + "')";
                    JDBCIndra_Connection.executeInsertToDB(sqlInsertSnip, dbc, false);

                    // NDSnippet
                    String sqlInsertNDSnip = "insert into nodedocumentsnippet (NODEID, DOCUMENTID, SNIPPETID ) "
                            + " values ("
                            + lNodeID + ", "
                            + lDocIDFinal + ", "
                            + lSnippetID + ")";
                    JDBCIndra_Connection.executeInsertToDB(sqlInsertNDSnip, dbc, false);
                }
            }
            // SNIPPET

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("error in snippet and nugget insert\r\n", e);
        } finally {
            timer.stop();
        }
    } // insertSnippetNuggetContent

    static void deletePreExistingNodeDocRels(long ldocIDPreExisting_ThisURL,
            Hashtable htCorporaToconsider,
            Connection dbc,
            boolean bProp_batch,
            String sURLCorpusDB_doubledQuotes,
            Hashtable htNodeSet // not in that mode if null
            )
            throws Exception {
        com.indraweb.profiler.Timer timerdeletePreExistingNodeDocRels = com.indraweb.profiler.Profiler.getTimer("deletePreExistingNodeDocRels", true);
        Enumeration eIntegerNodeOrCorpusIDs = null;
        // if in the traditional mode - delete all ND rels this doc -
        // since will be scoring across ALL nodes for that doc in this process is assumed
        if (htNodeSet == null) {
            if (htCorporaToconsider == null) //                eIntegerNodeOrCorpusIDs = com.iw.classification.Data_WordsToNodes.getVIntCorporaAsPerCfg ().elements ();
            {
                throw new Exception("htCorporaToconsider == null");
            } else {
                eIntegerNodeOrCorpusIDs = htCorporaToconsider.keys();
            }
        } else {
            eIntegerNodeOrCorpusIDs = htNodeSet.keys();
        }

        while (eIntegerNodeOrCorpusIDs.hasMoreElements()) {
            Integer ICorpusOrNodeID = (Integer) eIntegerNodeOrCorpusIDs.nextElement();
            String sSQLDel_preNDs = null;
            if (htNodeSet == null) {
                if (bProp_batch) {
                    sSQLDel_preNDs = "insert into STAGEDOCCORPUSDELETE (documentid, corpusid ) values ("
                            + ldocIDPreExisting_ThisURL + "," + ICorpusOrNodeID + ")";
                } else {
                    sSQLDel_preNDs = "delete from nodedocument where documentid = " + ldocIDPreExisting_ThisURL
                            + " and (edited != 1 or edited is null) "
                            + " and nodeid in ( select nodeid from node where corpusid = " + ICorpusOrNodeID + ")";
                }
            } else {
                if (bProp_batch) {
                    throw new Exception("can not combine batch mode and node restrict mode");
                }
                sSQLDel_preNDs = "delete from nodedocument where documentid = " + ldocIDPreExisting_ThisURL
                        + " and (edited != 1 or edited is null) "
                        + " and nodeid = " + ICorpusOrNodeID;
            }

            Statement stmt = dbc.createStatement();

            // If query statement failed, throw an exception
            try {
                stmt.executeUpdate(sSQLDel_preNDs);
            } finally {
                stmt.close();
            }
        }
        timerdeletePreExistingNodeDocRels.stop();
    }

    // ----------------------------------------------------------
    public static int executeUpdateToDB(String aSQL,
            Connection dbc,
            boolean bCommit,
            boolean bFatalIfThrowable,
            int iMinUpdates)
            throws Exception {
        // ----------------------------------------------------------
        //Log.logClear("JDBCIndra_Connection.java executeUpdateToDB aSQL [" + aSQL + "]\r\n");
        Statement stmt = null;
        int iNumUpdates = -1;
        if (dbc.isClosed()) {
            log.error("TSClassifyDoc dbc closed [" + dbc + "]");
        }
        // hbk timer Timer timer = Profiler.getTimer("method insertOrUpdateNodeDocInfotoDB->executeUpdateToDB", true);
        try {
            stmt = dbc.createStatement();
            iNumUpdates = stmt.executeUpdate(aSQL);
            if (iNumUpdates < iMinUpdates) {
                log.error("TSClassifyDoc update did not fix min number records [" + aSQL + "]\r\n");
                throw new Exception("update did not fix min number records [" + aSQL + "]");
            }
            if (bCommit) {
                dbc.commit();
            }
        } catch (Throwable e) {
            if (bFatalIfThrowable) {
                log.error("throwable in TSClassifyDoc.executeUpdateToDB [" + aSQL + "]", e);
            } else {
                log.error("error in TSClassifyDoc.executeUpdateToDB [" + aSQL + "]", e);
                throw new Exception("error in TSClassifyDoc.executeUpdateToDB [" + aSQL + "]" + e.getMessage());
            }
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            // hbk timer timer.stop();
        }
        return iNumUpdates;
    }
}