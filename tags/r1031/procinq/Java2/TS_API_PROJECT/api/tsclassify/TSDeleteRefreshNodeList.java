package api.tsclassify;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.iw.system.User;

/**
 * Deletes all of the nodes scheduled for refresh, or optionally just a single node.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  NodeID  Unique node identifier of the topic to be removed from the refresh list.
 *
 *  @note   http://ITSERVER/servlet/ts?fn=tsclassify.TSDeleteRefreshNodeList&SKEY=801312271&NodeID=2203289

 *	@return A SUCCESS object if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <SUCCESS>1.</SUCCESS>
  </TSRESULT>
 \endverbatim
 */
public class TSDeleteRefreshNodeList {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sKey = (String) props.get("SKEY", true);
        String sNodeID = (String) props.get("NODEID");

        try {
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            String sSQL = " delete from NodeRefresh";
            Statement stmt = null;
            if (sNodeID != null) {
                sSQL = sSQL + " where NodeID = " + sNodeID;
            }

            try {
                stmt = dbc.createStatement();
                stmt.executeUpdate(sSQL);
            } catch (SQLException qe) {
                EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
                return;
            } finally {
                stmt.close();
                stmt = null;
            }

            out.println("<SUCCESS>1</SUCCESS>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}