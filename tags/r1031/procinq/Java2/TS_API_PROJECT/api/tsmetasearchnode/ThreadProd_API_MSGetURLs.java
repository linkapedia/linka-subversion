/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Jan 22, 2003
 * Time: 3:27:44 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsmetasearchnode;


import com.indraweb.util.UtilProfiling;
import com.indraweb.util.Log;
import com.iw.threadmgr.ThreadGenericProdCons;
import com.iw.threadmgr.WorkQManager;
import com.iw.scoring.NodeForScore;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.Types;
import java.util.Vector;

public class ThreadProd_API_MSGetURLs  extends ThreadGenericProdCons
 {
    NodeForScore nfs;
    int iSEID;

    public ThreadProd_API_MSGetURLs ( String sDesc_,
                                      api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQForMS_,
                                      NodeForScore nfs_,
                                      int iSEID_ // search engine ID
                                     )
    {
        super(sDesc_, workQForMS_ );
        nfs = nfs_;
        iSEID = iSEID_;
    }

    public void run() {
        try {
            //    while ( com.indraweb.util.UtilFile.bFileExists( "c:/temp/stopp.hk") )
            //        com.indraweb.util.clsUtils.pause_this_many_milliseconds(1000);
            try {

                //Log.logcr("in ThreadProd_API_MSGetURLs.run() with nodeid [" + nfs.getNodeID() + "]");
                com.iw.metasearch.SearchCollector.searchCollect
                    (
                        iSEID,
                        nfs,
                        (api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode) super.workQ
                    );

            } catch (Throwable t) {
                System.out.println("error 31 in 'ThreadProducerMetaSearch.run()'" + t.getMessage());
                t.printStackTrace();
            }
            //System.out.println("done producer thread [" + Thread.currentThread().getName() + "] bStopLooping [" + bStopLooping + "]");
        } catch (Exception e) {
            System.out.println("error 4 in 'ThreadProducer_MSGetNextNode.run()'" + e.getMessage());
        } finally {
            //com.indraweb.util.Log.logClearcr ("Exiting ThreadProd_API_MSGetURLs workQ.getTotalWorkUnitsAdded() [" +
              //      workQ.getNumJobsAdded()+ "] sDesc [" + sDesc + "]");
        }
    }


}
