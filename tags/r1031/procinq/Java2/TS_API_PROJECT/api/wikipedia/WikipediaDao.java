package api.wikipedia;

import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public interface WikipediaDao {

    public Map<String, String> saveNodes(List<WikipediaPage> nodes) throws DAOException;

    public String saveNode(WikipediaPage page) throws DAOException;

    public String saveCorpus(WikipediaCategory category) throws DAOException;

    public boolean saveSourceNodes(Map<String, String> nodes) throws DAOException;

    public void saveMessagesProcessStatus(String requestId, List<String> messages) throws DAOException;

    public void updateProcessStatus(String requestId, String status) throws DAOException;
}
