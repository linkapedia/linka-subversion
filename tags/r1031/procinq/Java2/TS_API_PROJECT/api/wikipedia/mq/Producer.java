package api.wikipedia.mq;

import com.npstrandberg.simplemq.MessageInput;
import com.npstrandberg.simplemq.MessageQueue;

/**
 * intance for only one producer
 *
 * @author andres
 */
public class Producer {

    private static Producer instance = null;
    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    protected Producer() {
        // singleton
    }

    public static Producer getInstance() {
        if (instance == null) {
            instance = new Producer();
        }
        return instance;
    }

    public boolean sendMessage(MessageWiki message) {
        MessageInput mi = new MessageInput();
        mi.setObject(message);
        return messageQueue.send(mi);
    }
}
