package api.wikipedia.mq;

import java.io.Serializable;

/**
 *
 * @author andres
 */
public class MessageWiki implements Serializable {

    //request id to monitor the status in oracle
    private String requestId;
    //parameters to build the tree
    private String corpusName;
    private String categoryName;
    private String depth;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getCorpusName() {
        return corpusName;
    }

    public void setCorpusName(String corpusName) {
        this.corpusName = corpusName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }
}
