package api.wikipedia;

import api.util.ConfServer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * layer to comunicate with the connector logic 
 * @author andres
 */
public class WikipediaBot {

    private String userName;
    private String userPassword;
    private static final String CATEGORY_MEMBERS_SUBCAT = "subcat";
    private static final String CATEGORY_MEMBERS_PAGE = "page";
    private Conector conector;
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WikipediaBot.class);

    public WikipediaBot(String user, String password, Conector conector) {
        this.userName = user;
        this.userPassword = password;
        this.conector = conector;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Conector getConector() {
        return conector;
    }

    public void setConector(Conector conector) {
        this.conector = conector;
    }

    //action methods ------
    /**
     * get list for the categories
     *
     * @param categoryId
     * @param chunk request by chunk
     * @param max maximun number of categories to get
     * @return
     */
    public LinkedHashMap<String, String> getCategories(String categoryId, int chunk, int max) {
        try {
            return conector.getCategoryMembers(categoryId, chunk, max, CATEGORY_MEMBERS_SUBCAT);
        } catch (ConectorException ex) {
            log.error("Error getting categories from:" + categoryId, ex);
            return null;
        }
    }

    /**
     * get list for the categories chunk : default parameter in properties file
     * max : default parameter in properties file
     *
     * @param categoryId
     * @return
     */
    public LinkedHashMap<String, String> getCategories(String categoryId) {
        try {
            return conector.getCategoryMembers(categoryId, ConfServer.getIntValue("wikipedia.crawl.default.request.subcat.chunk"),
                    ConfServer.getIntValue("wikipedia.crawl.default.request.subcat.max"), CATEGORY_MEMBERS_SUBCAT);
        } catch (ConectorException ex) {
            log.error("Error getting categories from:" + categoryId, ex);
            return null;
        }
    }

    /**
     * get list for the pages chunk : default parameter in properties file max :
     * default parameter in properties file
     *
     * @param categoryId
     * @return
     */
    public LinkedHashMap<String, String> getPages(String categoryId) {
        try {
            return conector.getCategoryMembers(categoryId, ConfServer.getIntValue("wikipedia.crawl.default.request.page.chunk"),
                    ConfServer.getIntValue("wikipedia.crawl.default.request.page.max"), CATEGORY_MEMBERS_PAGE);
        } catch (ConectorException ex) {
            log.error("Error getting pages from:" + categoryId, ex);
            return null;
        }
    }

    /**
     * get list for the pages
     *
     * @param categoryId
     * @param chunk request by chunk
     * @param max maximun number of categories to get
     * @return
     */
    public LinkedHashMap<String, String> getPages(String categoryId, int chunk, int max) {
        try {
            return conector.getCategoryMembers(categoryId, chunk, max, CATEGORY_MEMBERS_PAGE);
        } catch (ConectorException ex) {
            log.error("Error getting pages from:" + categoryId, ex);
            return null;
        }
    }

    /**
     * get the category id for the first pass
     *
     * @param categoryTitle
     * @return
     */
    public String getCategoryId(String categoryTitle) {
        try {
            return conector.getCategoryId(categoryTitle);
        } catch (ConectorException ex) {
            log.error("Error getting category id from:" + categoryTitle, ex);
            return null;
        }
    }

    /**
     * get source for one pageId
     *
     * @param pageId
     * @return
     */
    public String getSource(String pageId) {
        try {
            List<String> pagesId = new ArrayList<String>();
            pagesId.add(pageId);
            return conector.getSource(pagesId, 1).get(pageId) == null ? "" : conector.getSource(pagesId, 1).get(pageId);
        } catch (ConectorException ex) {
            log.error("Error getting source from:" + pageId, ex);
            return null;
        }

    }

    /**
     * get all source from the list pageIds
     *
     * @param pagesId
     * @return
     */
    public Map<String, String> getSource(List<String> pagesId, int chunk) {
        try {
            return conector.getSource(pagesId, chunk);
        } catch (ConectorException ex) {
            log.error("Error getting source from:" + pagesId.toString(), ex);
            return null;
        }
    }

    /**
     * get all source from the list pageIds
     *
     * @param pagesId
     * @return
     */
    public Map<String, String> getSource(List<String> pagesId) {
        try {
            return conector.getSource(pagesId, 40);
        } catch (ConectorException ex) {
            log.error("Error getting source from:" + pagesId.toString(), ex);
            return null;
        }
    }
}
