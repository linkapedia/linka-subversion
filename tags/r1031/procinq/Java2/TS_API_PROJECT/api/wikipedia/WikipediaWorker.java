package api.wikipedia;

import api.util.ConfServer;
import api.wikipedia.mq.MessageWiki;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this class contain the logic from wikipedia stuff
 *
 * @author andres
 */
public class WikipediaWorker {

    private WikipediaBot bot;
    private WikipediaDao dao;
    private List<String> messages;
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WikipediaWorker.class);
    private static final String PROCESS_IN_PROCESS = "IN PROCESS";
    private static final String PROCESS_FINISHED = "FINISHED";

    public WikipediaWorker() {
        messages = new ArrayList<String>();
    }

    public WikipediaBot getBot() {
        return bot;
    }

    public void setBot(final WikipediaBot bot) {
        if (bot == null) {
            throw new IllegalArgumentException("Wikipedia bot can't be null");
        }
        this.bot = bot;
    }

    public WikipediaDao getDao() {
        return dao;
    }

    public void setDao(final WikipediaDao dao) {
        if (dao == null) {
            throw new IllegalArgumentException("Wikipedia Dao can't be null");
        }
        this.dao = dao;
    }

    /**
     * start with the process to create the taxonomy
     *
     * @param parameters
     */
    public void start(MessageWiki parameters) {
        String corpusName = parameters.getCorpusName();
        String categoryName = parameters.getCategoryName();
        String depthWorker = parameters.getDepth();

        //update in oracle with request id in process
        try {
            dao.updateProcessStatus(parameters.getRequestId(), PROCESS_IN_PROCESS);
        } catch (DAOException ex) {
            log.error("Error updating the wikipedia status by IN PROCESS", ex);
        }

        //create the corpus
        WikipediaCategory subcat = new WikipediaCategory();
        subcat.setCorpusName(corpusName);
        subcat.setCorpusActivate("1");
        String corpusId = "";
        try {
            corpusId = dao.saveCorpus(subcat);
        } catch (DAOException ex) {
            log.error("Error getting corpusId", ex);
        }

        //get category id to start the process
        String categoryid = bot.getCategoryId(categoryName);
        if (!corpusId.equals("") || categoryid != null) {
            //start the process to create the nodes
            buildNodesRecursively("-1", corpusId, 1, Integer.valueOf(depthWorker), categoryid, categoryName);
        } else {
            messages.add("Error creating the taxonomy, can't create corpus or categoryId");
        }
        //update notify oracle request id finished and save messages
        try {
            dao.updateProcessStatus(parameters.getRequestId(), PROCESS_FINISHED);
        } catch (DAOException ex) {
            log.error("Error updating the wikipedia status by IN FINISHED", ex);
        }
        try {
            dao.saveMessagesProcessStatus(parameters.getRequestId(), messages);
        } catch (DAOException ex) {
            log.error("Error saving messages in wikipedia status", ex);
        }
    }

    /**
     * build the taxonomy recursively
     *
     * @param nodeid
     * @param corpusid
     * @param currentDepth
     * @param depth
     * @param categoryId
     * @param categoryName
     */
    public void buildNodesRecursively(String nodeid, String corpusid, int currentDepth, int depth, String categoryId, String categoryName) {
        Map<String, String> categories;
        Map<String, String> pages;
        Map<String, String> pagesSource;

        List<String> pagesIdAux = new ArrayList<String>();
        //normalize name
        categoryName = UtilWikipedia.getTitleNormalized(categoryName);

        //create node
        WikipediaPage page = new WikipediaPage();
        page.setPageId(categoryId);
        page.setCorpusId(corpusid);
        page.setNodeTitle(categoryName);
        page.setNodeParentId(nodeid);
        page.setDepthFromRoot(currentDepth - 1 + "");
        page.setNodeSize("100");

        //save category node
        String currentNodeId;
        try {
            currentNodeId = dao.saveNode(page);
        } catch (DAOException ex) {
            log.error("Error creating root node", ex);
            messages.add("Error creating root node, not save in oracle");
            return;
        }

        //save add folders at level1 wikipedia,crawl.default.autofolders.level1
        if (currentDepth == 1) {
            saveCategoryFolders(corpusid, currentNodeId);
        }

        categories = bot.getCategories(categoryId);
        pages = bot.getPages(categoryId);
        boolean savePages = false;
        boolean savePagesSource = false;
        Map<String, String> pagesToNodes;

        if (pages == null) {
            messages.add("Not getting the pages for category: " + categoryId);
            log.error("Not getting the pages for category: " + categoryId);
        } else {
            //get source for the pages
            pagesIdAux.addAll(pages.keySet());
            pagesSource = bot.getSource(pagesIdAux);
            if (pagesSource == null) {
                messages.add("Not getting the sources for pages in: " + categoryId);
                log.error("Not getting the sources for pages in: " + categoryId);
            } else {
                //search and save the source for the category
                saveSourceCategory(pages, pagesSource, currentNodeId, categoryName);

                //parameters to build the nodes in oracle
                Map<String, String> parametersNodes = new HashMap<String, String>();
                parametersNodes.put("corpusid", corpusid);
                parametersNodes.put("depthFromRoot", currentDepth + "");
                parametersNodes.put("parentid", currentNodeId);
                parametersNodes.put("nodedesc", "");

                //pages process
                try {
                    //save the nodes and get the mapper to save source
                    pagesToNodes = dao.saveNodes(getPages(pages, parametersNodes));
                    savePages = true;
                } catch (DAOException ex) {
                    log.error("Error saving pages for node: " + currentNodeId, ex);
                    pagesToNodes = new HashMap<String, String>();
                }

                Map<String, String> sourceNodes = new HashMap<String, String>();
                String sourceTemp;
                for (Map.Entry pageNode : pagesToNodes.entrySet()) {
                    sourceTemp = pagesSource.get(pageNode.getValue().toString());
                    //apply the filter for the source
                    sourceTemp = UtilWikipedia.getSourceNormalized(sourceTemp);
                    if (sourceTemp == null || sourceTemp.isEmpty()) {
                        messages.add("The node: " + pageNode.getKey().toString() + " haven't source after normalized source");
                    } else {
                        sourceNodes.put(pageNode.getKey().toString(), sourceTemp);
                    }
                }
                //save source
                savePagesSource = saveSource(sourceNodes);
            }
        }

        if (savePages) {
            messages.add("saving pages for level " + currentDepth + " ok!");
        }
        if (savePagesSource) {
            messages.add("saving source for pages at level " + currentDepth + " ok!");
        }
        if (categories == null) {
            messages.add("Not getting the subcategories for category: " + categoryId);
            log.error("Not getting the subcategories for category: " + categoryId);
            return;
        }
        //cicle for all categories and apply the same pass recursively
        for (Map.Entry categoryAux : categories.entrySet()) {
            if (currentDepth < depth) {
                //build the pages by each category recursively
                buildNodesRecursively(currentNodeId, corpusid, currentDepth + 1, depth,
                        categoryAux.getKey().toString(), categoryAux.getValue().toString());
            } else {
                break;
            }
        }
    }

    /**
     * create the source for a node with children
     *
     * @param pages
     * @param pagesSource
     * @param currentNodeId
     * @param categoryName
     */
    private void saveSourceCategory(Map<String, String> pages, Map<String, String> pagesSource, String currentNodeId, String categoryName) {
        String pageFromCategory = null;
        String sourceCategory;
        for (Map.Entry pagesAux : pages.entrySet()) {
            if (pagesAux.getValue().toString().equalsIgnoreCase(categoryName)) {
                pageFromCategory = pagesAux.getKey().toString();
                break;
            }
        }
        if (pageFromCategory == null) {
            sourceCategory = "";
            messages.add("The category node: " + categoryName + " with id: " + currentNodeId + " haven't source");
        } else {
            sourceCategory = pagesSource.get(pageFromCategory);
            //remove the page equal to node category
            pages.remove(pageFromCategory);
            //filter the source
            //save source node
            Map<String, String> categorySourceToSave = new HashMap<String, String>();
            String sourceTempCategory = UtilWikipedia.getSourceNormalized(sourceCategory);
            if (sourceTempCategory == null || sourceTempCategory.isEmpty()) {
                messages.add("The category node: " + categoryName + " with id: " + currentNodeId + " haven't source after normalized the source");
            } else {
                categorySourceToSave.put(currentNodeId, sourceTempCategory);
            }
            try {
                dao.saveSourceNodes(categorySourceToSave);
            } catch (DAOException ex) {
                log.error("Error saving source for category node" + currentNodeId, ex);
                messages.add("Error saving source for category node" + currentNodeId);
            }
        }
    }

    /**
     * save the source for the nodes
     *
     * @param map
     * @return
     */
    private boolean saveSource(Map<String, String> map) {
        boolean savePagesSource = false;
        try {
            if (map == null || map.isEmpty()) {
                return false;
            }
            dao.saveSourceNodes(map);
            savePagesSource = true;
        } catch (DAOException ex) {
            log.error("Error saving source for the pages:", ex);
        }
        return savePagesSource;
    }

    /**
     * get neccesary data to save the node
     *
     * @param pages
     * @param addParemeters
     * @return
     */
    private List<WikipediaPage> getPages(Map<String, String> pages, Map<String, String> addParemeters) {
        List<WikipediaPage> pagesList = new ArrayList<WikipediaPage>();
        WikipediaPage page;
        for (Map.Entry pagesAux : pages.entrySet()) {
            page = new WikipediaPage();
            page.setPageId(pagesAux.getKey().toString());
            page.setNodeTitle(pagesAux.getValue().toString());
            page.setCorpusId(addParemeters.get("corpusid"));
            page.setDepthFromRoot(addParemeters.get("depthFromRoot"));
            page.setNodeParentId(addParemeters.get("parentid"));
            page.setNodeDesc(addParemeters.get("nodedesc"));
            page.setNodeSize("100");
            pagesList.add(page);
        }
        return pagesList;
    }

    /**
     * save in the first level the specific folder
     *
     * @param corpusId
     * @param parentId
     */
    private void saveCategoryFolders(String corpusId, String parentId) {
        String folder;
        try {
            folder = ConfServer.getValue("wikipedia.crawl.default.autofolders.level1");
        } catch (Exception e) {
            log.error("Error getting folders for level 1", e);
            folder = "";
        }
        if (folder.isEmpty()) {
            return;
        }
        String[] folders = folder.split(",");
        List<WikipediaPage> list = new ArrayList<WikipediaPage>();
        WikipediaPage node;
        for (String page : folders) {
            node = new WikipediaPage();
            node.setCorpusId(corpusId);
            node.setDepthFromRoot("1");
            node.setNodeParentId(parentId);
            node.setNodeSize("100");
            node.setNodeTitle(page);
            list.add(node);
        }
        try {
            dao.saveNodes(list);
        } catch (DAOException ex) {
            log.error("Error saving folders at level 1", ex);
            messages.add("Error saving folders at level 1");
        }
    }
}
