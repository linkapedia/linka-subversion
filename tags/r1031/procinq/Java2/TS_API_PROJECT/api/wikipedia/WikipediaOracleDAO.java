package api.wikipedia;

import com.indraweb.execution.Session;
import com.iw.db.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class WikipediaOracleDAO implements WikipediaDao {

    private static final Logger log = Logger.getLogger(WikipediaOracleDAO.class);
    private static final int BATCH_UPDATER = 100;

    public Map<String, String> saveNodes(List<WikipediaPage> nodes) throws DAOException {
        Map<String, String> nodesPagesRelation = new HashMap<String, String>();
        if (nodes == null || nodes.isEmpty()) {
            return nodesPagesRelation;
        }
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: saveNodes(.. Error getting connection from oracle");
        }
        boolean bIndraweb = Session.cfg.getPropBool("Indraweb", false, "false");
        long sProp_NodeID;
        String sql = "INSERT INTO SBOOKS.NODE (NODEID,PARENTID,NODESIZE,DEPTHFROMROOT,NODEDESC,NODETITLE,"
                + "CORPUSID) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement pstmt = null;
        try {
            sProp_NodeID = com.indraweb.utils.oracle.UniqueSpecification.getSubsequentNodeID(conn, bIndraweb);
            com.indraweb.utils.oracle.UniqueSpecification.processWikipedia = true;//block the method
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(sql);
            int cont = 0;
            for (WikipediaPage node : nodes) {
                cont++;
                pstmt.setLong(1, sProp_NodeID);
                pstmt.setInt(2, Integer.parseInt(node.getNodeParentId()));
                pstmt.setInt(3, Integer.parseInt(node.getNodeSize()));
                pstmt.setInt(4, Integer.parseInt(node.getDepthFromRoot()));
                pstmt.setString(5, node.getNodeDesc());
                pstmt.setString(6, node.getNodeTitle());
                pstmt.setInt(7, Integer.parseInt(node.getCorpusId()));

                //save relationship
                nodesPagesRelation.put(String.valueOf(sProp_NodeID), node.getPageId());

                pstmt.addBatch();
                sProp_NodeID++;
                if (cont == BATCH_UPDATER) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            //the last data
            if (cont != 0) {
                //update the batch
                pstmt.executeBatch();
                pstmt.clearBatch();
            }
            conn.commit();
        } catch (Exception e) {
            log.error("Error updating nodes", e);
            try {
                conn.rollback();
            } catch (SQLException ex) {
                throw new DAOException("Error in roolback", ex);
            }
            throw new DAOException("Exception updating nodes", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
            com.indraweb.utils.oracle.UniqueSpecification.processWikipedia = false;//Unblock the method
        }
        return nodesPagesRelation;
    }

    public String saveNode(WikipediaPage page) throws DAOException {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: saveNode(.. Error getting connection from oracle");
        }
        String sProp_NodeID = "";
        String sql = "INSERT INTO SBOOKS.NODE (NODEID,PARENTID,NODESIZE,DEPTHFROMROOT,NODEDESC,NODETITLE,"
                + "CORPUSID) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            sProp_NodeID = "" + com.indraweb.utils.oracle.UniqueSpecification.getSubsequentNodeID(conn, false);
            pstmt.setInt(1, Integer.parseInt(sProp_NodeID));
            pstmt.setInt(2, Integer.parseInt(page.getNodeParentId()));
            pstmt.setInt(3, Integer.parseInt(page.getNodeSize()));
            pstmt.setInt(4, Integer.parseInt(page.getDepthFromRoot()));
            pstmt.setString(5, page.getNodeDesc());
            pstmt.setString(6, page.getNodeTitle());
            pstmt.setInt(7, Integer.parseInt(page.getCorpusId()));
            pstmt.execute();
        } catch (Exception e) {
            log.error("Error updating node", e);
            throw new DAOException("Exception updating nodes", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
        return sProp_NodeID;
    }

    public String saveCorpus(WikipediaCategory category) throws DAOException {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: saveCorpus(.. Error getting connection from oracle");
        }
        String sql = "INSERT INTO SBOOKS.CORPUS (CORPUSID,CORPUS_NAME,CORPUSACTIVE)"
                + " VALUES (?,?,?)";
        PreparedStatement pstmt = null;
        int iCorpusID = 0;
        try {
            pstmt = conn.prepareStatement(sql);
            iCorpusID = com.indraweb.utils.oracle.UniqueSpecification.getSubsequentCorpusID(conn, false);
            pstmt.setInt(1, iCorpusID);
            pstmt.setString(2, category.getCorpusName());
            pstmt.setInt(3, Integer.parseInt(category.getCorpusActivate()));
            pstmt.execute();
        } catch (Exception e) {
            log.error("Error updating corpus", e);
            throw new DAOException("Exception updating corpus", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
        return String.valueOf(iCorpusID);
    }

    public boolean saveSourceNodes(Map<String, String> nodes) throws DAOException {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: saveSourceNodes(.. Error getting connection from oracle");
        }
        String sql = "INSERT INTO SBOOKS.NODEDATA (NODEID,NODESOURCE) VALUES (?,?)";

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(sql);
            int cont = 0;
            for (Map.Entry node : nodes.entrySet()) {
                cont++;
                pstmt.setInt(1, Integer.parseInt(node.getKey().toString()));
                pstmt.setString(2, node.getValue().toString());

                pstmt.addBatch();
                if (cont == BATCH_UPDATER) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            //the last data
            if (cont != 0) {
                //update the batch
                pstmt.executeBatch();
                pstmt.clearBatch();
            }
            conn.commit();
        } catch (Exception e) {
            log.error("Error updating nodes", e);
            try {
                conn.rollback();
            } catch (SQLException ex) {
                throw new DAOException("Error in roolback", ex);
            }
            throw new DAOException("Exception updating source", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
        return true;
    }

    public void saveMessagesProcessStatus(String requestId, List<String> messages) throws DAOException {
        if (messages == null || messages.isEmpty()) {
            return;
        }
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: saveMessagesProcessStatus(.. Error getting connection from oracle");
        }
        String sql = "UPDATE SBOOKS.WIKIPEDIAREPORT SET MESSAGES=? WHERE REQUESTID=?";
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            JSONArray jsonArray = JSONArray.fromObject(messages);
            pstmt.setString(1, jsonArray.toString());
            pstmt.setInt(2, Integer.valueOf(requestId));
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Updating wikipediaProcess failed");
            }
        } catch (Exception e) {
            log.error("Error saving process status in wikipedia", e);
            throw new DAOException("Error saving messages in wikipedia", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }

    public void updateProcessStatus(String requestId, String status) throws DAOException {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: updateProcessStatus(.. Error getting connection from oracle");
        }
        String sql = "UPDATE SBOOKS.WIKIPEDIAREPORT SET STATUS=? WHERE REQUESTID=?";
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, status);
            pstmt.setInt(2, Integer.valueOf(requestId));
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Updating wikipediaProcess failed");
            }
        } catch (Exception e) {
            log.error("Error saving process status in wikipedia", e);
            throw new DAOException("Error saving process status in wikipedia", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
