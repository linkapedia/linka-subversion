package api.wikipedia;

/**
 *
 * @author andres
 */
public class ConectorException extends Exception {

    public ConectorException() {
    }

    public ConectorException(String message) {
        super(message);
    }

    public ConectorException(Throwable cause) {
        super(cause);
    }

    public ConectorException(String message, Throwable cause) {
        super(message, cause);
    }
}
