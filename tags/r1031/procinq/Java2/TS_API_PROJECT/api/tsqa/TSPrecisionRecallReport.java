package api.tsqa;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

public class TSPrecisionRecallReport    {

    public static int iREPORTTYPE_PRECISION = 1;
    public static int iREPORTTYPE_RECALL = 2;


    public static TrecCompareCounters queryAndReport (
            int iReportType,
            int iQidIdx,
            int iQid,
            Connection dbc,
            String sFileNameOut,
			int iNumTopScoringRecordsToConsider )
        throws Exception
    {
        // loop thru qIDs

        // ******** WHICH DOCS DID ONE MISS AND NOT THE OTHER
        String sSQLDocsfoundbyTrecAndNotIW = null;
        Hashtable htDocsUniqueAllScores = new Hashtable();
        Hashtable htNodeColonDocSemiColonScore_to_dprd = new Hashtable();
			
			
		// QUERY TYPE 1A PRECISION 
        if ( iReportType == iREPORTTYPE_PRECISION )
        {
            sSQLDocsfoundbyTrecAndNotIW =
            " select distinct (d.documentid) " +
            " from trecqnode tqn, nodedocument nd, document d, node n " +
            " where " +
            //" docurl like '\\\\66.134.131.60\\60gb db drive\\trec\\tars\\%' " +
            " tqn.version = 2 " +
            " and tqn.nodeid = n.nodeid " +
            " and n.corpusid = 100040 " +
            " and n.nodeid = nd.nodeid "  +
            " and nd.score1 > 0 " +
            " and d.documentid = nd.documentid " +
            " and questionid = " + iQid +
            " minus " +
                " select distinct (documentid)  from trecqrel tqr , trecqnode  tqn " +
                //  " select documentid  from trecqrel tqr , trecqnode  tqn " +
                " where qestionid = " + iQid +
                " and tqn.questionid = tqr.qestionid  " +
                " and tqn.version = 2 " +
                " and status = 1 "                     ;
        }
		// QUERY TYPE 1B RECALL 
        else if ( iReportType == iREPORTTYPE_RECALL )
        {
            sSQLDocsfoundbyTrecAndNotIW =
                    " select distinct (documentid)  from trecqrel tqr , trecqnode  tqn " +
                    //  " select documentid  from trecqrel tqr , trecqnode  tqn " +
                " where qestionid = " + iQid +
                " and tqn.questionid = tqr.qestionid  " +
                " and tqn.version = 2 " +
                " and status = 1 " +
                " minus " +
                " select d.documentid  " +
                " from trecqnode tqn, nodedocument nd, document d, node n " +
                " where " +
                //" docurl like '\\\\66.134.131.60\\60gb db drive\\trec\\tars\\%' " +
                " tqn.version = 2 " +
                " and tqn.nodeid = n.nodeid " +
                " and n.corpusid = 100040 " +
                " and nd.score1 > 0  " +
                " and n.nodeid = nd.nodeid "  +
                " and d.documentid = nd.documentid " +
                " and questionid = " + iQid  ;
        } else  
			throw new Exception ("invalid report type");

        com.indraweb.util.Log.logClear("\r\n top sql query this Q report type [" + iReportType + "] [" + sSQLDocsfoundbyTrecAndNotIW +"]\r\n");
        java.sql.Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery(sSQLDocsfoundbyTrecAndNotIW);
		String sQDesc = null;
        try {
            int i3 = 0;
            while ( rs.next() )
            {
                int iDocumentid = rs.getInt(1);
                //com.indraweb.util.Log.logClearcr("\r\n" + iNumDataRowsMissingThisQ+ ". hbk nesxt doc iDocumentid :" + iDocumentid +"\r\n");
				// QUERY TYPE 1B ASSUME IN NODE-DOC TABLE  
                if ( iDocumentid   > 0 )
                {
                    boolean bFoundandPrintedFullRecord = false;
                    if ( true )
                    {
						
						int iNumNodeDocScoresGT_zero = 0;
						int iNumNodeDocScoresGT_fifty = 0;
						String sScoreConstraint = "";
						if ( iReportType == iREPORTTYPE_RECALL )
							sScoreConstraint = " and nd.score1 = 0  ";
						
                        String sSQLData =
                            " select distinct  qdesc, d.documentid, doctitle, docurl, " +
                                " n.nodetitle, n.nodeid, corpusid, score1, " +
                                " score5, score6 " +
                            " From document d, nodedocument nd, node n , trecqrel tqr , trecqnode  tqn " +
                            " where n.nodeid = nd.nodeid " +
                            " and nd.documentid = d.documentid " +
                            " and tqn.nodeid = n.nodeid " +
                            " and tqn.version = 2 " +
                            " and tqn.questionid = " + iQid +
							sScoreConstraint + 
                            " and tqn.questionid = tqr.qestionid  " +
                            " and n.corpusid = 100040 " +
                            " and d.documentid = " + iDocumentid  +
                            " order by d.documentid ";

                        //com.indraweb.util.Log.logClear("\r\nsSQLData :" + sSQLData +"\r\n");
                        java.sql.Statement stmt2 = dbc.createStatement();
                        ResultSet rs2 = stmt2.executeQuery(sSQLData );
                        try {
                            while ( rs2.next() )
                            {
                                int iColCount = 1;

                                sQDesc = rs2.getString(iColCount++);
                                int iDocumentid2 = rs2.getInt(iColCount++);
                                String sDoctitle = rs2.getString(iColCount++);
                                String sDdocurl = rs2.getString(iColCount++);
                                String sNodeTitle= rs2.getString(iColCount++);
                                int iNodeid = rs2.getInt(iColCount++);
                                int iCorpusid = rs2.getInt(iColCount++);
                                double dScore1 = rs2.getDouble(iColCount++);
                                double dScore5 = rs2.getDouble(iColCount++);
                                double dScore6 = rs2.getDouble(iColCount++);
								// STORE STRINGS IN A NEW sortable score and string Struct 
								// INDEXED BY 
								// STORE STRINGS IN A TABLE 
								
								data_PRReportDetail dprd = new data_PRReportDetail();
								dprd.sFileNameOut = sFileNameOut;
								dprd.iQidIdx = iQidIdx;
								dprd.iQid = iQid;
								dprd.sQDesc = sQDesc;
								dprd.iDocumentid2 = iDocumentid2;
								dprd.sDoctitle = sDoctitle;
								dprd.sDdocurl = sDdocurl;
								dprd.sNodeTitle = sNodeTitle;
								dprd.iNodeid = iNodeid;
								dprd.iCorpusid = iCorpusid;
								dprd.dScore1 = dScore1;
								dprd.dScore5 = dScore5;
								dprd.dScore6 = dScore6;
								dprd.dScore5X6 = dScore5*dScore6;
								
								dprd.iReportType = iReportType;

								htNodeColonDocSemiColonScore_to_dprd.put (
									"" + iNodeid + ":" + iDocumentid2 + ";" + dprd.dScore5X6,
									dprd);
									
                                bFoundandPrintedFullRecord = true;
                            } // while ( rs2.next() )

                        } finally {
                            stmt2.close();
                            rs2.close();
                        }
                    }
                    if ( true && !bFoundandPrintedFullRecord )
                    {
                        String sSQLData =
                            " select d.documentid, doctitle, docurl " +
                            " " +
                            " From document d" +
                            " where d.documentid = " + iDocumentid ;

                        //com.indraweb.util.Log.logClearcr("\r\nsSQLData :" + sSQLData +"\r\n");
                        java.sql.Statement stmt2 = dbc.createStatement();
                        ResultSet rs2 = stmt2.executeQuery(sSQLData );
                        try {
                            while ( rs2.next() )
                            {
                                int iColCount = 1;

                                sQDesc = getQDescFromQid ( iQid, dbc);
                                int iDocumentid2 = rs2.getInt(iColCount++);
                                String sDoctitle = rs2.getString(iColCount++);
                                String sDdocurl = rs2.getString(iColCount++);
                                int iNodeid = -1;
                                String sNodeTitle= "NA";
                                int iCorpusid = -1;
                                double dScore1 = -1;
                                double dScore5 = -1;
                                double dScore6 = -1;
                                com.indraweb.util.UtilFile.addLineToFile(sFileNameOut,
                                    iQidIdx + "#2#" +
                                    iQid+ "\t" +
                                    sQDesc+ "\t" +
                                    iDocumentid2+ "\t" +
                                    iNodeid + "\t" +
                                    sDoctitle+ "\t" +
                                    sDdocurl+ "\t" +
                                    sNodeTitle+ "\t" +
                                    iCorpusid+ "\t" +
                                    dScore1 + "\t" +
                                    dScore5 + "\t" +
                                    dScore6 + "\t"    +
                                    dScore5*dScore6 + "\r\n"
                                );
								// ACCOUNTING BASED ON DOC ONLY RECORDS 
								htDocsUniqueAllScores.put ( new Integer ( iDocumentid ), "dummy");
								
                            } // while ( rs2.next() )

                        } finally {
                            stmt2.close();
                            rs2.close();
                        }
                    }
                } // if docid > 0


            } // while rs - next docid in the top level distinct false pos or neg set 
        }
        finally {
            rs.close();
            stmt.close();
        }

        TrecCompareCounters tcc = getsupplementalStats  ( iQid, iReportType, dbc);

		//tcc.iNumDistinctFalseDocs_GT_fifty = htDocsWithOneOrMoreNodeDocScores_GtFifty.size();
		//tcc.iNumDistinctFalseDocs_GT_zero = htDocsWithOneOrMoreNodeDocScores_GtZero.size();
		tcc.iNumDistinctFalseOrMissingDocs_noScoreConstraint = htDocsUniqueAllScores.size();
		tcc.sQtitle = sQDesc;
		tcc.setDPRD(htNodeColonDocSemiColonScore_to_dprd);
		tcc.iNumTopScoringRecordsToConsider = iNumTopScoringRecordsToConsider;
		return tcc;

    } // public static TrecCompareCounters queryAndReport (

    private static java.util.Hashtable htCache_method_getQDescFromQid = new Hashtable();
    private static String getQDescFromQid  ( int iQid, Connection dbc )
    throws Exception
    {
        String sReturnQdesc = (String) htCache_method_getQDescFromQid.get ( new Integer (iQid));
        if ( sReturnQdesc == null )
        {
            String sSQL = "select qdesc from trecqnode where questionid = " + iQid + " and version = 2";
            sReturnQdesc = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBStr(sSQL, dbc);
            htCache_method_getQDescFromQid.put  (new Integer ( iQid), sReturnQdesc );
        }
        return sReturnQdesc ;
    }


    //java.util.Hashtable htCache
    private static TrecCompareCounters getsupplementalStats  ( int iQid, int iReportType, Connection dbc )
    throws Exception
    {

        String sSQLGetNodesForQ = "select nodeid from trecqnode where questionid = " + iQid +
            " and version = 2" ;
        Vector vINodeIDs = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLGetNodesForQ, dbc);
        StringBuffer  sbNoideIDList = new StringBuffer();
        for ( int ix = 0; ix < vINodeIDs.size(); ix++ )
        {
            if ( ix > 0 )
                sbNoideIDList.append(",");
            sbNoideIDList.append(vINodeIDs.elementAt(ix));
        }
        ///Log.logClearcr("***** node list [" + sbNoideIDList + "]");
        //UtilSets.convertVectorTo_intArray(vINodeIDs);

        // GET # DOcs by TREc
        String sSQL = "select count (distinct urlpiece) from trecqrel where status = 1 and qestionid = " +
            iQid ;
        long lcounttrecHitsthisQ = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBLong(sSQL, dbc);
        // GET # DOcs by IW
        sSQL = "select count (distinct documentid) from nodedocument nd where nodeid in (" + sbNoideIDList + ") and score1 > 0";
        long lcountIWDistinctDocsThisQGt0 = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBLong(sSQL, dbc);

        sSQL = "select count (*) from nodedocument nd where score1 > 0 and nodeid in (" + sbNoideIDList + ")";
        long lcountIWNodeDocIDsThisQGt0 = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBLong(sSQL, dbc);

		//com.indraweb.util.Log.logClearcr("new TrecCompareCounterslcounttrecHitsthisQ : " + lcounttrecHitsthisQ); 
         return new TrecCompareCounters(
			iQid,
			iReportType,
			(int) lcounttrecHitsthisQ,
            (int) lcountIWDistinctDocsThisQGt0,
            (int) lcountIWNodeDocIDsThisQGt0 ,
            (int) vINodeIDs.size(),
            sbNoideIDList.toString()
        )    ;
        //com.indraweb.util.UtilFile.addLineToFile(sFileNameOut,
            //"*** " +
                //"Q [" + iQid +
                //"]\r\n #trec hits [" + lcounttrecHitsthisQ +
                //"]\r\n #IW hits [" + lcountIWHitsthisQ +
                //"]\r\n #NodeIDs [" + vINodeIDs.size()  + "] NoideIDList [" + sbNoideIDList +
                //"]\r\n #false pos [" + iNumdataRowsMissing +
                //"]\r\n"
        //);
    }

}

/*
TrecPS = trec positive set =
 select documentid from trecqrel where qestionid = 403 and status = 1;    (21)

TNS = trec negative set =
 select documentid from trecqrel where qestionid = 403 and status = 0;    (1025)

IWPS = indraweb positive set =
 select d.documentid from trecqnode tqn, nodedocument nd, document d where tqn.nodeid = nd.nodeid and  d.documentid = nd.documentid and docurl like '\\66.134.131.60\60gb db drive\trec\tars\%' and questionid = 403 and nd.score1 > 0
*/
