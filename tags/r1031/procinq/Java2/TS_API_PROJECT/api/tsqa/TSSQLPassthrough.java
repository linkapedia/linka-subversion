package api.tsqa;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

public class TSSQLPassthrough
{
	// Purge all nodes marked for deletion
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        String sSQL = (String) props.get("SQL", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
		try {
			// for authorization purposes, need to know the corpus id of this node
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            stmt = dbc.createStatement();




            if (sSQL.toUpperCase().startsWith("SELECT")) {

                boolean bVerbose = com.indraweb.util.UtilFile.bFileExists("/temp/IndraCQLVerbose.txt");
                boolean bUsePreparedStatement = com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebug_usePreparedStatement.txt");
                boolean bUseFileAsCannedQuery = com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebug_UseFileAsCannedQuery.txt");

                if ( bUseFileAsCannedQuery)
                {
                    sSQL = com.indraweb.util.UtilFile.getFileAsString("/temp/IndraDebug_UseFileAsCannedQuery.txt").trim();
                }
                if (bVerbose)
                {
                    com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputCQL.txt", "PASSTHRU SQL [" +
                        sSQL + "] bUsePreparedStatement [" + bUsePreparedStatement + "] bUseFileAsCannedQuery [" + bUseFileAsCannedQuery + "]\r\n\r\n" );

                    // let's print the hex in detail :
                    for ( int i = 0; i < sSQL.length(); i++)
                    {
                      //  String sPiece = sSQL.substring(i, i+1);
                        char c = sSQL.charAt(i);

                        com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputCQL.txt",
                                "i [" + i + "] " +
                                //"sPiece.length() [" + sPiece.length() + "] " +
                                "char [" + c + "] " +
                                "Direct(c)  [" + Character.getDirectionality(c) + "] " +
                                "Number(c)  [" + Character.getNumericValue(c) + "] " +
                                "isDefined(c)  [" + Character.isDefined(c) + "] " +
                                "isIgnorable(c)  [" + Character.isIdentifierIgnorable(c) + "] " +
                                "isUnicodeIDPart(c)  [" + Character.isUnicodeIdentifierPart(c) + "] " +
                                "isUnicodeIDStart(c)  [" + Character.isUnicodeIdentifierStart(c) + "] " +
                                "Type(c)  [" + Character.getType(c) + "]\r\n\r\n "
                        );
                    }

                }
                // prepared statements are used by CQL
                if (bUsePreparedStatement){
                    PreparedStatement ps2 = dbc.prepareStatement(sSQL);
                    rs = ps2.executeQuery();
                }
                else
                    rs = stmt.executeQuery (sSQL);

                rsmd = rs.getMetaData();
                int iColumns = rsmd.getColumnCount();
                int loop = 0;

                while ( rs.next() ) {
                    loop++; if (loop == 1) { out.println("<RESULTSET>"); }
                    out.println("   <RESULT>");
                    for (int j = 1; j <= iColumns; j++) {
                        out.print("      <"+rsmd.getColumnLabel(j).toUpperCase()+">");
                        out.print(rs.getString(j));
                        out.println("</"+rsmd.getColumnLabel(j).toUpperCase()+">");
                    }
                    out.println("   </RESULT>");
                }

                if (loop != 0) { out.println("</RESULTSET>"); }
            } else {
                if (stmt.executeUpdate(sSQL) == 0) {
                    out.println("<ERROR>Your update request has FAILED</ERROR>");
                    throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
                }
            }
		}
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally {
            if (rsmd != null) { rsmd = null; }
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
