package api.results;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

import api.APIProps;

public class HashTree extends Hashtable {
	private HashTree htParent;

	// Accessor and set methods for new Hashtable attribute, htParent..
	public HashTree GetParent() { return htParent; }
	public void SetParent(HashTree htParent) {
		this.htParent = htParent;	
	}
	
	// Another constructor
	public HashTree () {}
	public HashTree (APIProps props) {
		String SessionKey = (String) props.get("SKEY");
		if (SessionKey != null) { this.put("SKEY", SessionKey); }
	}
}
