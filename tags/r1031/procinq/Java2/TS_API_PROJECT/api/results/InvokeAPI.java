package api.results;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.zip.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class InvokeAPI
{
	private String sTaxonomyServer;
	private String sAPIcall;	
	private Hashtable htArguments;
	private String SessionKey = null;
	
	// Accessor methods to private variables
	public String GetTaxonomyServer() { return sTaxonomyServer; }
	public String GetAPIcall() { return sAPIcall; }
	public String GetSessionKey() { return SessionKey; }
	public Hashtable GetAPIarguments() { return htArguments; }
	
	// Object constructors
	public InvokeAPI (String sAPIcall, Hashtable htArguments, String sTaxonomyServer) {
		
		this.sAPIcall = sAPIcall;
		this.sTaxonomyServer = sTaxonomyServer;
		this.htArguments = htArguments;
		
		if (htArguments.containsKey("SKEY")) { this.SessionKey = (String) htArguments.get("SKEY"); }
		
	}
	public InvokeAPI (String sAPIcall, Hashtable htArguments) {
		
		this.sAPIcall = sAPIcall;
		this.htArguments = htArguments;
		this.sTaxonomyServer = com.indraweb.execution.Session.cfg.getProp("API");

		if (htArguments.containsKey("SKEY")) { this.SessionKey = (String) htArguments.get("SKEY"); }
	}
	
	public String CreateName (Hashtable htHash, String sName) {

		// If the tag already exists, append to it and make it unique
		if (htHash.containsKey(sName)) {
			sName = sName + "a";
			sName = CreateName(htHash, sName);
		} 
		
		return sName;
	}
	
	// Execute API function given a compressed stream of input
	public static HashTree Execute (InputStream is) throws UnsupportedEncodingException {
		BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		String sData = new String();
		HashTree htOriginal = new HashTree();
		htOriginal.SetParent(null);
		HashTree htObject = htOriginal;
		
		try {
			while ((sData = buf.readLine()) != null) {
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");
				
				String sTag1;
			
				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }
				
				String sTag2 = "";
				
				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) { 
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);
				
				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) && 
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
	 				 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {
	
						InvokeAPI api = new InvokeAPI("", htObject);
						sTag1 = new String (api.CreateName(htObject, sTag1));
						//out.println("<BR>Begin object: "+sTag1); 

						if (sTag1.equals("TS_ERROR")) {
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							return htOriginal;
						}
						
						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1,htObject);		
					}
				
					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2); 
						htObject = (HashTree) htObject.GetParent();
					}
				
					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String (com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag); 
					
						// If the variable value contains a "CDATA", eliminate it here
						if (com.indraweb.util.UtilStrings.getStrContains(sTag,"<![CDATA")) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}
						
						htObject.put(sTag1, sTag);
					}
				}
			}
			return htObject;
		}
		catch (Exception except) {
			return null;
		}

	
	}
	
	// Execute API function given a compressed stream of input
	public static HashTree Execute (ZipInputStream is) throws UnsupportedEncodingException {
		BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		String sData = new String();
		HashTree htOriginal = new HashTree();
		htOriginal.SetParent(null);
		HashTree htObject = htOriginal;
		
		try {
			while ((sData = buf.readLine()) != null) {
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");
				
				String sTag1;
			
				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }
				
				String sTag2 = "";
				
				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) { 
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);
				
				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) && 
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
	 				 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {
	
						// If the tag already exists, append to it and make it unique
						while (htObject.containsKey(sTag1)) {
							sTag1 = sTag1 + "a";
						} 

						//out.println("<BR>Begin object: "+sTag1); 

						if (sTag1.equals("TS_ERROR")) {
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							return htOriginal;
						}
						
						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1,htObject);		
					}
				
					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2); 
						htObject = (HashTree) htObject.GetParent();
					}
				
					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String (com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						//api.Log.LogError("Add tag: "+sTag1+" value: "+sTag); 
					
						// If the variable value contains a "CDATA", eliminate it here
						if (com.indraweb.util.UtilStrings.getStrContains(sTag,"<![CDATA")) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}
						
						htObject.put(sTag1, sTag);
					}
				}
			}
			return htObject;
		}
		catch (Exception except) {
			api.Log.LogError("Attempt to read compressed zip file failed!", except);
			return null;
		}

	
	}
	
	// Execute the API call.   Returns success 0 for success, -1 for failure.
	public HashTree Execute (PrintWriter out) throws Exception {
		String sURL = sTaxonomyServer+sAPIcall;
		Enumeration e = htArguments.keys();

		HashTree htOriginal = new HashTree();
		htOriginal.SetParent(null);
		HashTree htObject = htOriginal;
			
		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);
			
			sURL = sURL+"&"+sKey+"="+sValue; 
			
			// Remove any apostraphies (because they are illegal) and change
			// spaces into + signs
			sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL," ","+"));
			sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL,"'",""));
		}
		
		// Make a socket connection to the server
		try {
			long lStart = System.currentTimeMillis();

			URL myURL = new URL(sURL); 
			//out.println("<DEBUG> URL: "+sURL+" </DEBUG>");
			HttpURLConnection httpCon = (HttpURLConnection) myURL.openConnection(); 
		
			if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) { 
				throw new Exception("Http error: " + httpCon.getResponseMessage());
			} 
		
			InputStream is = httpCon.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String sData = new String();
			
			while ((sData = buf.readLine()) != null) {
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");
				
				String sTag1;
				
				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }
				
				String sTag2 = "";
				
				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) { 
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);
				
				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) && 
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
 					 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						sTag1 = new String (CreateName(htObject, sTag1));
						//out.println("<BR>Begin object: "+sTag1); 

						if (sTag1.equals("TS_ERROR")) {
							httpCon.disconnect();
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							httpCon.disconnect();
							return htOriginal;
						}
						
						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1,htObject);
						
					}
				
					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2); 
						htObject = (HashTree) htObject.GetParent();
					}
				
					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String (com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag); 
						
						// If the variable value contains a "CDATA", eliminate it here
						if (com.indraweb.util.UtilStrings.getStrContains(sTag,"<![CDATA")) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}
						
						htObject.put(sTag1, sTag);
					}
				}
			}
			
			httpCon.disconnect();
			long lStop = System.currentTimeMillis() - lStart;
			// out.println("<!-- Timing: "+lStop+" ms -->");
			
			return htObject;
		}
		catch (Exception except) {
			//out.println("<!--"); except.printStackTrace(out); out.println("-->");
			throw except;
		}
	}
	
	// Send POST API call
	public HashTree PostExecute (String filename) {
		try {
			HashTree htOriginal = new HashTree();
			htOriginal.SetParent(null);
			HashTree htObject = htOriginal;

			// Do a multi-part post
			String boundary="ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
		    String twoHyphens="--";
			String lineEnd="\r\n";

	        HttpURLConnection httpURLConn = null;
			DataOutputStream outStream;
			DataInputStream inStream;
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
	        int maxBufferSize = 1*1024*1024;

	        // create FileInputStream to read from file
			FileInputStream fileInputStream = new FileInputStream(new File(filename));

			//String sURL = "http://localhost:8101/servlet/ts?fn="+sAPIcall;
			String sURL = sTaxonomyServer+sAPIcall;
			Enumeration et = htArguments.keys();
			
			// Build URL to call Taxonomy Server
			while (et.hasMoreElements()) {
				String sKey = (String) et.nextElement();
				String sValue = (String) htArguments.get(sKey);
			
				sURL = sURL+"&"+sKey+"="+sValue; 
				
				// Remove any apostraphies (because they are illegal) and change
				// spaces into + signs
				sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL," ","+"));
				sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL,"'",""));
			}

			api.Log.Log("PostExecute calling to "+sURL);
            URL theURL = new URL(sURL);
            httpURLConn  = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

			// Set up POST parameters
			StringBuffer params = new StringBuffer();
		    params.setLength(0);
			Enumeration e = htArguments.keys();
			params.append("fn="+sAPIcall);
			
            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream (httpURLConn.getOutputStream ());

			outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\""+ filename + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable,maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable,maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();
            outStream.flush ();
            outStream.close ();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream ();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String sData = new String();
			
			while ((sData = buf.readLine()) != null) {
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");
				
				String sTag1;
				
				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }
				
				String sTag2 = "";
				
				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) { 
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);
				
				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) && 
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
 					 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						sTag1 = new String (CreateName(htObject, sTag1));
						//out.println("<BR>Begin object: "+sTag1); 

						if (sTag1.equals("TS_ERROR")) {
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							return htOriginal;
						}
				
						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1,htObject);
						
					}
				
					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2); 
						htObject = (HashTree) htObject.GetParent();
					}
				
					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String (com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						//out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->"); 
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag); 
						
						// If the variable value contains a "CDATA", eliminate it here
						if (com.indraweb.util.UtilStrings.getStrContains(sTag,"<![CDATA")) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}
						
						htObject.put(sTag1, sTag);
					}
				}
			}
			
			return htObject;
		}
		catch (Exception except) {
			api.Log.LogError("Attempt to post MIME attachment failed!", except);
			return null;
		}
		
	}
}
