package api.results;

import java.io.*;

// NodeDocumentResult class is an object container for the XML 
//  returned to DisplayResults 
public class NodeDocumentResult
{
	// local variables
	private String NodeID;
	private String DocID;
	private String FolderID;
	private String DocTitle;
	private String DocURL;
	private String DocSummary;
	private String Score1;
	private String Score2;
	private String Score3;
	private String Score4;
	private String Score5;
	private String Score6;
	private String DateScored;
	private String DocumentType;
    private String FullText;

    // used in IDRAC only
    private String Country;
    private String Bib;
    private String Lang;
    private String IdracNum;
    private String AdoptDate;
    private String PubDate;
    private String EntryDate;
    private String RevDate;
    private String SysDate;
    private String IsOutdated;

	// local variables: ROC cut off(s)
	private double dROCF1 = 0.0;
	private double dROCF2 = 0.0;
	private double dROCF3 = 0.0;
	private double dROCC1 = 0.0;
	private double dROCC2 = 0.0;
	private double dROCC3 = 0.0;

	// Accessor methods to private variables
	public String GetNodeID() { return NodeID; }
	public String GetDocID() { return DocID; }
	public String GetFolderID() { return FolderID; }
	public String GetDocTitle() { return DocTitle; }
	public String GetDocURL() { return DocURL; }
	public String GetDocSummary() { return DocSummary; }
	public String GetScore1() { return Score1; }
	public String GetScore2() { return Score2; }
	public String GetScore3() { return Score3; }
	public String GetScore4() { return Score4; }
	public String GetScore5() { return Score5; }
	public String GetScore6() { return Score6; }
	public String GetDateScored() { return DateScored; }
	public String GetDocumentType() { return DocumentType; }
    public String GetFullText() { if (FullText != null) { return FullText; } else { return GetDocURL(); } }

    // used in IDRAC only
    public String GetCountry() { return Country; }
    public String GetBib() { return Bib; }
    public String GetLang() { return Lang; }
    public String GetIdracNum() { return IdracNum; }
    public String GetAdoptDate() { return AdoptDate; }
    public String GetPubDate() { return PubDate; }
    public String GetEntryDate() { return EntryDate; }
    public String GetRevDate() { return RevDate; }
    public String GetSysDate() { return SysDate; }
    public String GetIsOutdated() { return IsOutdated; }

	// Object constructor
	public NodeDocumentResult(String sNodeID, String sDocID, String sFolderID, String sDocTitle,
							  String sDocURL, String sDocSummary, String sScore1, String sScore2,
							  String sScore3, String sScore4, String sScore5, String sScore6,
							  String sDateScored) {

		this.NodeID = sNodeID; this.DocID = sDocID;
		this.FolderID = sFolderID;	this.DocTitle = sDocTitle;
		this.DocURL = sDocURL; this.DocSummary = sDocSummary;
		this.Score1 = sScore1; this.Score2 = sScore2;
		this.Score3 = sScore3; this.Score4 = sScore4;
		this.Score5 = sScore5; this.Score6 = sScore6;
		this.DateScored = sDateScored;
	}

    // CIFA constructor
    public NodeDocumentResult(String sNodeID, String sDocID, String sFolderID, String sDocTitle,
							  String sDocURL, String sDocSummary, String sScore1, String sScore2,
							  String sScore3, String sScore4, String sScore5, String sScore6,
							  String sDateScored, String sFullText) {

		this.NodeID = sNodeID; this.DocID = sDocID;
		this.FolderID = sFolderID;	this.DocTitle = sDocTitle;
		this.DocURL = sDocURL; this.DocSummary = sDocSummary;
		this.Score1 = sScore1; this.Score2 = sScore2;
		this.Score3 = sScore3; this.Score4 = sScore4;
		this.Score5 = sScore5; this.Score6 = sScore6;
        this.FullText = sFullText;
		this.DateScored = sDateScored;
	}

    // IDRAC constructor
	public NodeDocumentResult(String sNodeID, String sDocID, String sFolderID, String sDocTitle,
							  String sDocURL, String sDocSummary, String sCountry, String sBib,
                              String sLang, String sIdracNum, String sAdoptDate, String sPubDate,
                              String sEntryDate, String sRevDate, String sSysDate, String sIsOutdated,
                              String sScore1, String sScore2,
							  String sScore3, String sScore4, String sScore5, String sScore6,
							  String sDateScored) {

		this.NodeID = sNodeID; this.DocID = sDocID;
		this.FolderID = sFolderID;	this.DocTitle = sDocTitle;
		this.DocURL = sDocURL;	this.DocSummary = sDocSummary;
		this.Score1 = sScore1;	this.Score2 = sScore2;
		this.Score3 = sScore3;	this.Score4 = sScore4;
		this.Score5 = sScore5;	this.Score6 = sScore6;
		this.DateScored = sDateScored;

        // idrac specific fields
        this.Country = sCountry; this.Bib = sBib; this.Lang = sLang; this.IdracNum = sIdracNum;
        this.AdoptDate = sAdoptDate; this.PubDate = sPubDate; this.EntryDate = sEntryDate;
        this.RevDate = sRevDate; this.SysDate = sSysDate; this.IsOutdated = sIsOutdated;
	}

    public NodeDocumentResult(String sNodeID, String sDocID, String sFolderID, String sDocTitle,
							  String sDocURL, String sDocSummary, String sScore1, String sScore2,
							  String sScore3, String sScore4, String sScore5, String sScore6,
							  String sDateScored, String sDocumentType, String sFullText) {

		this.NodeID = sNodeID;
		this.DocID = sDocID;
		this.FolderID = sFolderID;
		this.DocTitle = sDocTitle;
		this.DocURL = sDocURL;
		this.DocSummary = sDocSummary;
		this.Score1 = sScore1;
		this.Score2 = sScore2;
		this.Score3 = sScore3;
		this.Score4 = sScore4;
		this.Score5 = sScore5;
		this.Score6 = sScore6;
		this.DateScored = sDateScored;
		this.DocumentType = sDocumentType;
        this.FullText = sFullText;
	}

	// Overriding hashCode function to return a sortable score
	public int hashCode() {
		try {
			Float fScore1 = new Float(0.0);	Float fScore2 = new Float(0.0);
			Float fScore3 = new Float(0.0);	Float fScore4 = new Float(0.0);
			Float fScore5 = new Float(0.0);	Float fScore6 = new Float(0.0);
			int iFinalScore;

			fScore1 = fScore1.valueOf(GetScore1());
			fScore2 = fScore2.valueOf(GetScore2());
			fScore3 = fScore3.valueOf(GetScore3());

			// updated 7/29/2002: if the cut offs for this corpus are defined, use
			// them as the basis for sorting rather than the hardcoded ones

			// here are HK's keys: 10102 (F*C), 10101 (C), 10100 (F)
			fScore5 = fScore5.valueOf(GetScore5());
			fScore6 = fScore6.valueOf(GetScore6());

			double dScore5 = new Double(Score5).doubleValue();
			double dScore6 = new Double(Score6).doubleValue();

			// First, reset score1 so that it is bucketed properly on the front end
			// Then, return the appropriate value
			if (dROCC3 == 10100) { // frequency
				if (dScore6 > dROCF1) { Score1 = "50"; }
				if (dScore6 > dROCF2) { Score1 = "75"; }
				if (dScore6 > dROCF3) { Score1 = "100"; }

				fScore1 = fScore1.valueOf(GetScore1());
			}
			else if (dROCC3 == 10101) { // coverage
				if (dScore5 > dROCF1) { Score1 = "50"; }
				if (dScore5 > dROCF2) { Score1 = "75"; }
				if (dScore5 > dROCF3) { Score1 = "100"; }

				fScore1 = fScore1.valueOf(GetScore1());
			}
			else if (dROCC3 == 10102) { // frequency * coverage
				if ((dScore6 * dScore5) > dROCF1) { Score1 = "50"; }
				if ((dScore6 * dScore5) > dROCF2) { Score1 = "75"; }
				if ((dScore6 * dScore5) > dROCF3) { Score1 = "100"; }

				fScore1 = fScore1.valueOf(GetScore1());
			}

			iFinalScore = (fScore1.intValue() * 100000) +
						  (fScore2.intValue() * 100) + fScore3.intValue();

			return iFinalScore;
		} catch (Exception e) { api.Log.LogError(e); return 0; }
	}

	// Overriding hashCode function to return a sortable score
	public int GetStarScore() {
		try {
			Float fScore1 = new Float(0.0);	Float fScore2 = new Float(0.0);
			Float fScore3 = new Float(0.0);	Float fScore4 = new Float(0.0);
			Float fScore5 = new Float(0.0);	Float fScore6 = new Float(0.0);
			int iFinalScore;

			fScore1 = fScore1.valueOf(GetScore1());
			fScore2 = fScore2.valueOf(GetScore2());
			fScore3 = fScore3.valueOf(GetScore3());

			// updated 7/29/2002: if the cut offs for this corpus are defined, use
			// them as the basis for sorting rather than the hardcoded ones
			if (!((dROCF1 == 0.0) && (dROCF2 == 0.0) && (dROCF3 == 0.0) &&
				  (dROCC1 == 0.0) && (dROCC2 == 0.0) && (dROCC3 == 0.0))) {

				// here are HK's keys: 10102 (F*C), 10101 (C), 10100 (F)
				fScore5 = fScore5.valueOf(GetScore5());
				fScore6 = fScore6.valueOf(GetScore6());

				double dScore5 = new Double(Score5).doubleValue();
				double dScore6 = new Double(Score6).doubleValue();

				// First, reset score1 so that it is bucketed properly on the front end
				// Then, return the appropriate value
				if (dROCC3 == 10100) { // frequency
					if (dScore6 > dROCF1) { Score1 = "50"; }
					if (dScore6 > dROCF2) { Score1 = "75"; }
					if (dScore6 > dROCF3) { Score1 = "100"; }

					fScore1 = fScore1.valueOf(GetScore1());
				}
				else if (dROCC3 == 10101) { // coverage
					if (dScore5 > dROCF1) { Score1 = "50"; }
					if (dScore5 > dROCF2) { Score1 = "75"; }
					if (dScore5 > dROCF3) { Score1 = "100"; }

					fScore1 = fScore1.valueOf(GetScore1());
				}
				else if (dROCC3 == 10102) { // frequency * coverage
					if ((dScore6 * dScore5) > dROCF1) { Score1 = "50"; }
					if ((dScore6 * dScore5) > dROCF2) { Score1 = "75"; }
					if ((dScore6 * dScore5) > dROCF3) { Score1 = "100"; }

					fScore1 = fScore1.valueOf(GetScore1());
				}
			}

			return fScore1.intValue();
		} catch (Exception e) { api.Log.LogError(e); return 0; }
	}

	public void SetCutoffs(double f1, double f2, double f3, double c1, double c2, double c3) {
		this.dROCC1 = c1; this.dROCC2 = c2; this.dROCC3 = c3;
		this.dROCF1 = f1; this.dROCF2 = f2; this.dROCF3 = f3;
	}
	// print XML out to a printwriter
	public void EmitXML (PrintWriter out) {
		out.println ("   <NODEDOCUMENT> ");
		out.println ("	    <NODEID>"+NodeID+"</NODEID>");
		out.println ("	    <DOCUMENTID>"+DocID+"</DOCUMENTID>");
		out.println ("      <FOLDERID>"+FolderID+"</FOLDERID>");
		out.println ("      <DOCTITLE><![CDATA["+DocTitle+"]]></DOCTITLE>");
		out.println ("      <DOCURL><![CDATA["+DocURL+"]]></DOCURL>");
		out.println ("      <DOCSUMMARY><![CDATA["+Clean(DocSummary)+"]]></DOCSUMMARY>");
		out.println ("      <SCORE1>"+Score1+"</SCORE1>");
		out.println ("      <SCORE2>"+Score2+"</SCORE2>");
		out.println ("      <SCORE3>"+Score3+"</SCORE3>");
		out.println ("      <SCORE4>"+Score4+"</SCORE4>");
		out.println ("      <SCORE5>"+Score5+"</SCORE5>");
		out.println ("      <SCORE6>"+Score6+"</SCORE6>");
		out.println ("      <DATESCORED>"+DateScored+"</DATESCORED>");
		out.println ("		<TYPE>"+DocumentType+"</TYPE>");


        out.println ("   </NODEDOCUMENT>");
	}

    public String Clean (String dirtyString) {
        if (dirtyString == null) { return "None available"; }
        String sReturn = "";
        for (int k = 0; k < dirtyString.length(); k++) {
            int ichar = (int) dirtyString.charAt(k);
            if ((ichar != 13) && (ichar != 10)) { sReturn = sReturn + dirtyString.charAt(k); }
        }

        return sReturn;
    }

}
