package api.tscorpus;

import api.util.search.images.SearchImages;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import org.apache.log4j.Logger;


/*
 * get image from url and save into database(Table CORPUSIMAGES)
 * 
 */
public class TSAddImageCorpusURL {

    private static final Logger log = Logger.getLogger(TSAddImageCorpusURL.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("handleTSapiRequest(APIProps, PrintWriter, Connection");
        try {
            String corpusid = (String) props.get("corpusid");
            String url = (String) props.get("url");
            
            log.debug("corpusID: "+ corpusid);
            log.debug("url: "+ url);
            String sSQL = "";
            PreparedStatement pstmt = null;
            

            //call uil images  to return a images from url
            SearchImages sImages = new SearchImages();
            HashMap<String, Object> mapInfo = new HashMap<String, Object>();

            sSQL = "insert into CORPUSIMAGES(imageid, description, url, corpusid, image) values(SEC_IMAGESCORPUS.nextval, ?, ?, ?, ?)";
            pstmt = dbc.prepareStatement(sSQL);

            try{
                mapInfo = sImages.getImageInfoURL(url);
            }catch(IOException ioe){
                log.error("TSAddImageNodeURL: IOException check url "+ioe.getMessage());
                mapInfo=null;
                return;
            }catch(Exception e){
                log.error("TSAddImageNodeURL: Exception check url "+e.getMessage());
                mapInfo=null;
                return;
            }
            if (mapInfo != null) {

                pstmt.setString(1, (String) mapInfo.get("title"));
                pstmt.setString(2, (String) mapInfo.get("url"));
                pstmt.setInt(3, Integer.parseInt(corpusid));
                pstmt.setBinaryStream(4, new ByteArrayInputStream((byte[]) mapInfo.get("image")));
                pstmt.executeUpdate();

                log.debug("upload image url successful");
                out.println("<SUCCESS>images add ok</SUCCESS>");
            } else {
                log.debug("upload image url error");
            }
        } catch (Exception e) {
            log.error("An exception ocurred: ", e);
            throw e;
        }
    }
}
