package api.tscorpus;

import api.FileMetadata;
import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.CheckNocropImage;

import com.iw.system.User;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

/**
 * Add a Image to corpus.
 *
 * @authors Andres Restrepo, All Rights Reserved.
 *
 * @return	SUCCESS tag, if successful. \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <SUCCESS>image add ok</SUCCESS>
 * </TSRESULT>
 * \endverbatim
 */
public class TSAddImageCorpus {

    // TSAddImageCorpus (imageid,description,url,image(blob),corpusid)
    private static final Logger log = Logger.getLogger(TSAddImageCorpus.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        log.debug("TSAddImageCorpus");
        List<FileMetadata> listFileMetada = (List<FileMetadata>) props.get("inputstream", null);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String sSQL;
        if (listFileMetada.isEmpty()) {
            log.debug("List FIle is Empty");
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR), out);
            return;
        }
        String corpusid = listFileMetada.get(0).getNodeId();
        if (corpusid == null) {
            log.debug("not corpusid selected");
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR), out);
            return;
        }
        try {
            if (!u.IsAdmin(Integer.parseInt(corpusid), out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            sSQL = "insert into CORPUSIMAGES(imageid, description, url, corpusid, image) values(SEC_IMAGESCORPUS.nextval, ?, ?, ?, ?)";
            pstmt = dbc.prepareStatement(sSQL);
            for (FileMetadata file : listFileMetada) {
                pstmt.setString(1, file.getDescription());
                pstmt.setString(2, file.getUri());
                pstmt.setInt(3, Integer.parseInt(corpusid));

                InputStream in = new ByteArrayInputStream(file.getImage());
                BufferedImage image = ImageIO.read(in);

                //check if the image is good to create a nocrop version
                try {
                    if (!CheckNocropImage.check(image)) {
                        out.println("<MESSAGE>the image " + file.getUri() + " not pass the filter for nocrop image</MESSAGE>");
                        continue;
                    }
                } catch (IllegalArgumentException e) {
                    log.error(e.getMessage());
                    continue;
                }

                pstmt.setBinaryStream(4, new ByteArrayInputStream(file.getImage()));
                pstmt.executeUpdate();
            }
            log.debug("process finished successful");
            out.println("<SUCCESS>process finished successful</SUCCESS>");
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    public static boolean checkNoCropImage(BufferedImage image, int boxw, int boxh) {
        int width = image.getWidth();
        int height = image.getHeight();

        if (width > boxw && height > boxh) {
            return true;
        }
        return false;
    }
}
