package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 *  Recalculated the combined scores for all of the documents in a corpus.  This takes the new ROC cutoff
 *  calculated from the sampling process and re-applies the results to the database.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *
 *  @note   http://ITSERVER/itsapi/ts?fn=tscorpus.TSRebucket&CorpusID=100046&SKEY=1543463905

 *	@return	an error if unsuccessful
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>1</SUCCESS>
    </TSRESULT>
  \endverbatim
 */
public class TSRebucket
{
	// TSRebucket (corpusid) 
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		int iCorpusID = props.getint ("CorpusID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
		if ( u == null || !u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

		CallableStatement cstmtClose = dbc.prepareCall ("begin Rebucket (:1); end;");
		cstmtClose.setLong(1, iCorpusID );
		cstmtClose.executeUpdate();
		cstmtClose.close();
		cstmtClose = null;
		
		out.println("<SUCCESS>1</SUCCESS>");
	}
}
