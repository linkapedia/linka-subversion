package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.iw.system.User;
import com.iw.system.Node;

public class TSSearchCorpora
{
	// TSListCorpera (sessionid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{

        int loop = 0;

        Statement stmt = null; ResultSet rs = null;
		try {
			String sKey = (String) props.get("SKEY", true);
            String sQuery = (String) props.get("query", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = Node.getSQL(dbc)+" where contains(n.nodetitle, '"+sQuery+"')>0 and n.corpusid = 101 and "+
                    "n.linknodeid != n.nodeid order by n.nodetitle asc";
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			while ( rs.next() ) {
                Node n = new Node(rs);
                loop++; if (loop == 1) out.println("<NODES>");

                n.emitXML(out, u, true);
            }

			rs.close();
		    stmt.close();

            sSQL = " select CorpusID, Corpus_Name, CorpusDesc, "+
                   " Rocf1, Rocf2, Rocf3, Rocc1, Rocc2, Rocc3, CorpusActive"+
                   " from Corpus where corpusactive = 1 and corpusid in (select corpusid from node where"+
                   " contains(nodetitle, '"+sQuery+"')>0) order by corpus_name asc";
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            while ( rs.next() ) {
                int iCorpusID = rs.getInt(1);
                String sCorpusName = rs.getString(2);
                String sCorpusDesc = rs.getString(3);
                float fRocF1 = rs.getFloat(4);
                float fRocF2 = rs.getFloat(5);
                float fRocF3 = rs.getFloat(6);
                float fRocC1 = rs.getFloat(7);
                float fRocC2 = rs.getFloat(8);
                float fRocC3 = rs.getFloat(9);
                int iCorpusActive = rs.getInt(10);

                if (true) {
                    // Since we cannot get the number of results from the Result Object, we
                    //  need to count with a loop counter.   If results are 0 we want to return
                    //  an exception (error) without printing any streams.   To do this, hold off
                    //  on writing out the initial stream <CORPERA> until we have found at
                    //  least one result.   -MAP 10/14/01
                    loop = loop + 1;
                    if (loop == 1) { out.println ("<NODES>"); }

                    out.println (" <CORPUS>");
                    out.println ("   <CORPUSID>"+iCorpusID+"</CORPUSID>");
                    out.println ("   <CORPUS_NAME><![CDATA["+sCorpusName+"]]></CORPUS_NAME>");
                    out.println ("   <CORPUSDESC><![CDATA["+sCorpusDesc+"]]></CORPUSDESC>");
                    out.println ("   <ROCF1>"+fRocF1+"</ROCF1>");
                    out.println ("   <ROCF2>"+fRocF2+"</ROCF2>");
                    out.println ("   <ROCF3>"+fRocF3+"</ROCF3>");
                    out.println ("   <ROCC1>"+fRocC1+"</ROCC1>");
                    out.println ("   <ROCC2>"+fRocC2+"</ROCC2>");
                    out.println ("   <ROCC3>"+fRocC3+"</ROCC3>");
                    out.println ("   <CORPUSACTIVE>"+iCorpusActive+"</CORPUSACTIVE>");
                    out.println ("</CORPUS>");
                }
            }

            rs.close();
            stmt.close();

            // If no results found, throw an exception
            if ( loop == 0) {
                out.println ( "<DEBUG>No corpora found</DEBUG>");
                throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_CORPORA_THIS_DB);
            }

            if (loop > 0) {	out.println ("</NODES>"); }

        } catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { rs.close(); stmt.close(); rs = null; stmt = null; }
	}
}
