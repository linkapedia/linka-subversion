package api.tscorpus;


import java.text.DecimalFormat;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.execution.Session;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 * Copy the given taxonomy from one location to another.  This function will also copy all node relationships.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID  Unique node identifier of this node.
 *  @param  CorpusName  Name of the new corpus that will be created.
 *  @param  copySignatures  If true, copy signature relationships when copying the nodes.
 *  @param  copySource  If true, copy topic source when copying the nodes.
 *  @param  copyDocs    If true, copy the document relationships when copying the nodes.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tscorpus.TSCopyCorpus&CorpusID=5&CorpusName=Worldbook+Copy&SKEY=9919294812

 *	@return	CORPUSID identifier of the newly created node
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <CORPUSID>15</CORPUSID>
  </TSRESULT>
  \endverbatim
 */
public class TSCopyCorpus {
    private static int Count = 0;
    private static int Total = 0;

    private static final Object oSemaphore = new Object();

    private static boolean bCopySigs = false;
    private static boolean bCopySource = false;
    private static boolean bCopyDocs = false;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)             throws Exception {
        synchronized (oSemaphore) {
            String sCorpus = (String) props.get("Corpus", true);
            String sCorpusName = (String) props.get("CorpusName", "Copy of ID "+sCorpus);
            String copySigs = (String) props.get("copySignatures");
            String copySource = (String) props.get("copySource");
            String copyDocs = (String) props.get("copyDocs");
            String sShowStatus = (String) props.get("ShowStatus");

            String sKey = (String) props.get("SKEY", true);
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
            }

            if ((copySigs != null) && (copySigs.toLowerCase().equals("true"))) bCopySigs = true;
            if ((copySource != null) && (copySource.toLowerCase().equals("true"))) bCopySource = true;
            if ((copyDocs != null) && (copyDocs.toLowerCase().equals("true"))) bCopyDocs = true;

            int iCorpusID = Integer.parseInt(sCorpus);
            int iDepth = -1;

            // get the total number of children that we will be copying
            String sSQL = "select count(*) from node where corpusid = " + sCorpus;
            Statement stmt = null;
            ResultSet rs = null;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                rs.next();

                Total = rs.getInt(1);
            } catch (Exception e) {
                api.Log.LogError("error in tscopycorpus, sql: " + sSQL, e);
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            // 1) Get the CORPUSID of the new PARENT node
            String sParentID = "";
            sSQL = " select NodeID, DepthFromRoot from Node where ParentID = -1 and CorpusID = " + sCorpus;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                rs.next();

                sParentID = rs.getString(1);
                iDepth = rs.getInt(2);
                if (!u.IsAdmin(iCorpusID, out)) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                }
            } catch (TSException tse) {
                EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
            } catch (Exception e) {
                api.Log.LogError("error in tscopycorpus, sql: " + sSQL, e);
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            // create the new corpus that will be used
            iCorpusID = TSCreateCorpus.createCorpus(props, out, dbc);

            if (sShowStatus != null)
                copyNodeRecursive(sParentID, "-1", iCorpusID + "", out, (iDepth + 1), dbc);
            else
                copyNodeRecursive(sParentID, "-1", iCorpusID + "", null, (iDepth + 1), dbc);

            out.println("<CORPUSID>" + iCorpusID + "</CORPUSID>");

            Count = 0;
            Total = 0;
        }
    }

    public static String copyNodeRecursive(String NodeID, String ParentID, String CorpusID, PrintWriter out,
                                           int Depth, Connection dbc)
    throws Exception {
        //api.Log.Log("Call into copyNodeRecursive with: NID: "+NodeID+" PID: "+ParentID+" CID: "+CorpusID+" Depth: "+Depth);
        DecimalFormat twoDigits = new DecimalFormat("0");

        boolean bIndraweb = Session.cfg.getPropBool("Indraweb", false, "false");

        String sNextID = "" + com.indraweb.utils.oracle.UniqueSpecification.getSubsequentNodeID(dbc, bIndraweb);
        Vector vChildren = new Vector();

        // insert the copied node
        long lStart = System.currentTimeMillis();
        String sSQL = " INSERT INTO node (nodeid, corpusid, nodetitle, nodesize, parentid, "+
                      " nodeindexwithinparent, depthfromroot, datescanned, dateupdated, nodestatus, "+
                      " nodeltitle, nodelastsync, nodedesc) SELECT "+sNextID+", "+CorpusID+", NodeTitle, "+
                      " NodeSize, "+ParentID+", NodeIndexWithinParent, "+Depth+", DateScanned, "+
                      " DateUpdated, NodeStatus, NodeLTitle, NodeLastSync, NodeDesc from Node "+
                      " WHERE NodeID = "+NodeID;
        Statement stmt = null; ResultSet rs = null;

        try {
			stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
        } catch (Exception e) { throw e;
        } finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }

        long lStop = System.currentTimeMillis() - lStart;
        api.Log.Log("Node insert successful in "+ lStop + " ms");

        // copy relationships as well
        // SIGNATURE
        if (bCopySigs) {
            lStart = System.currentTimeMillis();
            sSQL = " INSERT INTO signature (nodeid, signatureword, signatureoccurences, lang) SELECT "+sNextID+
                   ", SIGNATUREWORD, SIGNATUREOCCURENCES, LANG FROM signature WHERE NodeID = "+NodeID;
            try {
                stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
            } catch (Exception e) { throw e;
            } finally {
                if (stmt != null) { stmt.close(); stmt = null; }
            }
            lStop = System.currentTimeMillis() - lStart;
            //api.Log.Log("Signature transfer successful in "+ lStop + " ms");
        }

        // MUSTHAVE
        sSQL = " INSERT INTO musthave (nodeid, mustword, lang) SELECT " + sNextID +
                ", MUSTWORD, LANG FROM musthave WHERE NodeID = " + NodeID;
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        // IDRACNODE
        lStart = System.currentTimeMillis();
        sSQL = " INSERT INTO idracnode (nodeid, nodecountry) SELECT "+sNextID+", nodecountry "+
               " FROM idracnode WHERE NodeID = "+NodeID;
        try {
			stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
        } catch (Exception e) { throw e;
        } finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
        lStop = System.currentTimeMillis() - lStart;
        //api.Log.Log("IDRAC Node transfer successful in "+ lStop + " ms");

        if (bCopySource) {
            // NODEDATA
            lStart = System.currentTimeMillis();
            sSQL = " INSERT INTO nodedata (nodeid, nodesource) SELECT "+sNextID+", nodesource"+
                   " FROM nodedata WHERE NodeID = "+NodeID;
            try {
                stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
            } catch (Exception e) { throw e;
            } finally {
                if (stmt != null) { stmt.close(); stmt = null; }
            }
            lStop = System.currentTimeMillis() - lStart;
            //api.Log.Log("NodeData transfer successful in "+ lStop + " ms");
        }

        if (bCopyDocs) {
            // NODEDOCUMENT
            lStart = System.currentTimeMillis();
            sSQL = " INSERT INTO nodedocument (nodeid, documentid, documenttype, score1, score2, score3, "+
                   " score4, score5, score6, score7, score8, score9, score10, score11, score12, docsummary, "+
                   " datescored, indexwithinnode) SELECT "+sNextID+", documentid, documenttype, score1, score2,  "+
                   " score3, score4, score5, score6, score7, score8, score9, score10, score11, score12, "+
                   " docsummary, datescored, indexwithinnode "+
                   " FROM nodedocument WHERE NodeID = "+NodeID;
            try {
                stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
            } catch (Exception e) { throw e;
            } finally {
                if (stmt != null) { stmt.close(); stmt = null; }
            }
            lStop = System.currentTimeMillis() - lStart;
            //api.Log.Log("NodeDocument transfer successful in "+ lStop + " ms");
        }

        long percent = ((Count + 1) * 100) / Total;
        if (out != null) {
            out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
            out.flush();
        } Count++;

        // now look for children and insert them, if applicable
        lStart = System.currentTimeMillis();
        sSQL = "select nodeid from node where parentid = "+NodeID+" and nodeid != "+sNextID;

        //api.Log.Log("Get children: "+sSQL);

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) { vChildren.add(rs.getString(1)); }

        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }

        lStop = System.currentTimeMillis() - lStart;
        api.Log.Log("Get Children successful in "+ lStop + " ms "+vChildren.size()+" length.");

        for (int i = 0; i < vChildren.size(); i++) {
            copyNodeRecursive((String)vChildren.elementAt(i), sNextID, CorpusID, out, Depth, dbc);
        }
        return sNextID;
    }
}
