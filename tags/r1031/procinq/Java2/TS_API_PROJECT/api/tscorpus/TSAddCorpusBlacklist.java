package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;

import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 *  Adds a term to the blacklist for a given corpus.  Results will not be presented if they match the blacklist pattern.
 *  For example to remove all information from the joes.com the blacklist would be www.joes.com.
 *  Node ID is optional in this call.  Include a Node ID if you want to create a black list entry only for a given
 *  node and not for the entire corpus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  BlackList_Term  A whole or partial string matching the blacklisted URL
 *  @param  NodeID (optional)   Unique node identifier, used only to restrict blacklisting by topic.
 *
 *  @note    http://itsserver/servlet/ts?fn=tscorpus.TSAddCorpusBlackList&Blak&CorpusID=4&SKEY=993135977

 *	@return	SUCCESS tag if the corpus was removed successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Blacklist term was added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddCorpusBlacklist {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID");
		String sBlack = (String) props.get ("BlackList_Term");
		// Node ID is optional
		String sNodeID = (String) props.get ("NodeID");
		
		try {
			
			if ((sCorpusID == null) || (sBlack == null)) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
			}
			
			String sSQL = " insert into Blacklist (CorpusID, UrlChunk, DateCreated";
			if (sNodeID != null) { sSQL = sSQL + ", NodeID"; }
			sSQL = sSQL + ") values ("+sCorpusID+",'"+sBlack+"',SYSDATE";
			if (sNodeID != null) { sSQL = sSQL + ","+sNodeID; }
			sSQL = sSQL + ")";
			
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Attempt to add new blacklist item failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

		    stmt.close();

            out.println("<SUCCESS>Blacklist term was added successfully.</SUCCESS>");
		} catch ( TSException tse )	 {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
