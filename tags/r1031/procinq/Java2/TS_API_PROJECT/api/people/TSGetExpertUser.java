package api.people;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.*;

import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 * Returns an XML structure containing all narratives that are classified into the given <i>node identifier</i>.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique topic identifier from which to retrieve documents.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tsdocument.TSGetExpertUser&NodeID=1088308&SKEY=828710192

 *	@return	A series of ITS DOCUMENT objects ordered by BUCKET SCORE descending.
 *  \verbatim
 <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <NARRATIVES>
            <DOCUMENT>
                <DOCUMENTID>200001</DOCUMENTID>
                <CORPUSID>300012</CORPUSID>
                <PARENTID>1087909</PARENTID>
                <SCORE1>75.0</SCORE1>
                <DOCSUMMARY><![CDATA[ operating and technology expenditure, with theoperating and technology expenditure, with the... Armscor to determine client needs, to establish... obtain military systems and equipment, both... upgrading of computer network... It is an acquisition organisation and the... needs, to establish technology, to... needs, to establish technology, to... for example, the acquisition cash flow for... - Making its acquisition services available to... - Providing acquisition support to defence...]]></DOCSUMMARY>
                <DATESCORED>2002-10-13 22:20:58.0</DATESCORED>
                <INDEXWITHINNODE>1</INDEXWITHINNODE>
                <GENREID>121</GENREID>
                <NODETITLE><![CDATA[ Impacts of New Technology]]></NODETITLE>
                <DOCTITLE><![CDATA[ Armscor s Net Income Reportedly Tumbles]]></DOCTITLE>
                <DOCURL><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></DOCURL>
                <DOCUMENTSUMMARY><![CDATA[ null]]></DOCUMENTSUMMARY>
                <DATELASTFOUND>2002-10-16 09:50:48.0</DATELASTFOUND>
                <VISIBLE>1</VISIBLE>
                <FULLTEXT><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></FULLTEXT>
            </DOCUMENT>
 ...
  \endverbatim
 */
public class TSGetExpertUser
{
    public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        // call into the narrative table with a hardcoded
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        String sNodeID = (String) props.get("NodeID", true);

        String sSQL = Document.getSQLwithoutTable(dbc)+", nd.score1 from nodedocument nd, document d, narrative n "+
           " where n.documentid = d.documentid and nd.documentid = d.documentid and nd.nodeid = "+sNodeID+
           " and narrativeid = 2 order by nd.score1 desc";
        Statement stmt = null; ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            int loop = 0;
            while ( rs.next() ) {
                loop = loop + 1;
                if (loop == 1) { out.println ("<NARRATIVES>"); }

                Document d = new Document(rs);
                d.emitXML(out, u, true);
            }

            if (loop > 0) out.println("</NARRATIVES>");
        } catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
            finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }
	}
}
