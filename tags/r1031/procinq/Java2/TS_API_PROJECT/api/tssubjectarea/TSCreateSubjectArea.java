package api.tssubjectarea;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 *  Create a new subject area in the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  SubjectName Name of the subject area to be created.
 *  @param  SubjectAreaStatus(optional)   Initial status of the subject area, default is 1 (active)
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSCreateSubjectArea&SubjectName=Test&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Subject Area created successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSCreateSubjectArea
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sName = (String) props.get ("SubjectName", "Untitled");
		String sStatus = (String) props.get ("SubjectAreaStatus", "1");

        Statement stmt = null;
		try {

			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " insert into SubjectArea (SubjectName, SubjectAreaStatus) values "+
						  " ('"+sName+"',"+sStatus+")";
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Attempt to add new subject area failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CREATE_GENRE_FAILURE);
			}
            stmt.close();

			sSQL = " select SubjectAreaID from SubjectArea where SubjectName = '"+sName+"'";
			stmt = dbc.createStatement();

			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;

				int iID = rs.getInt(1);

				out.println ("   <SUBJECTAREA> ");
				out.println ("      <SUBJECTAREAID>"+iID+"</SUBJECTAREAID>");
				out.println ("      <SUBJECTNAME>"+sName+"</SUBJECTNAME>");
				out.println ("      <SUBJECTAREASTATUS>"+sStatus+"</SUBJECTAREASTATUS>");
				out.println ("   </SUBJECTAREA>");
			}
			rs.close();
		    stmt.close();
			out.println("<SUCCESS>Subject Area created successfully.</SUCCESS>");

			if (loop == 0) { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CREATE_GENRE_FAILURE); }
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
