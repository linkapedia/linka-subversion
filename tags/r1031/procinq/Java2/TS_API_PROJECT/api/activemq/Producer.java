package api.activemq;

import java.io.PrintWriter;
import java.sql.Connection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.iw.activemq.beans.ConfigurationPackage;
import com.iw.activemq.procinq.TopicProducer;

public class Producer {
	private static Logger log = LogManager.getLogger(Producer.class);
	public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
		log.debug("Producer::handleTSapiRequest()");
		String task = props.getProperty("task", "stop");
		try {
			ConfigurationPackage confPackage = new ConfigurationPackage(task);
			TopicProducer.sendMessage(confPackage);
			System.out.println("Message Sent");
		} catch (Exception ex) {
			System.out.println("Message Failed");
		}
	}
}