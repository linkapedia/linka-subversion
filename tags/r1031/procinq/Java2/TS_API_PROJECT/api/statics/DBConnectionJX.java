package api.statics;

import api.Log;
import com.indraweb.execution.Session;
import com.javaexchange.dbConnectionBroker.DbConnectionBroker;
import java.sql.Connection;
import java.util.Hashtable;
import org.apache.log4j.Logger;

// interface to java exchange db connection pool com.javaexchange...
public class DBConnectionJX {

    private static final Logger log = Logger.getLogger(DBConnectionJX.class);
    public static Hashtable htDbConnectionBrokers = new Hashtable();
    private final static Object oSynchCountConnectionsAPI = new Object();
    private final static Object oSynchCountConnectionsSVR = new Object();
    private static int iCountConnectionGetsAPI = 0;
    private static int iCountConnectionGetsSVR = 0;
    private static int iCountConnectionFreeAPI = 0;
    private static int iCountConnectionFreeSVR = 0;
    private static Hashtable htConnectionsOut = new Hashtable();

    public static Connection getConnection(String sWhichDB) throws Exception {
        return getConnection(sWhichDB, "no desc");
    }

    public static Connection getConnection(String sWhichDB, String sDesc) throws Exception {
        if (sWhichDB == null) {
            throw new Exception("whichdb == null");
        }
        DbConnectionBroker dbcbToUse = (DbConnectionBroker) htDbConnectionBrokers.get(sWhichDB);
        //Thread.dumpStack();
        if (dbcbToUse == null) {
            String sDB_OracleJDBC = Session.cfg.getProp("DBString_" + sWhichDB + "_OracleJDBC");
            try {
                String sDB_user = Session.cfg.getProp("DBString_" + sWhichDB + "_User");
                String sDB_pass = Session.cfg.getProp("DBString_" + sWhichDB + "_Pass");

                dbcbToUse = new DbConnectionBroker(
                        "oracle.jdbc.driver.OracleDriver",
                        sDB_OracleJDBC,
                        sDB_user,
                        sDB_pass,
                        5, // min conns
                        200, // max cnns
                        Session.cfg.getProp("IndraHome") + "/db.log",
                        1 //maxConnTime: Time in days between connection resets. (Reset does a basic cleanup)
                        );
                htDbConnectionBrokers.put(sWhichDB, dbcbToUse);
            } catch (Exception e) {
                Log.LogError("Error constructing DB connection broker for DB [" + sDB_OracleJDBC + "] \r\n", e);
                throw e;
            }
        }

        Connection dbcRtn = null;

        long lStartTime = System.currentTimeMillis();
        long lMaxWaitTime = 60 * 1000; // 1 minute
        int iNulCounter = 0;
        while (true) {
            dbcRtn = dbcbToUse.getConnection();
            if (dbcRtn != null) {
                if (iNulCounter > 0) {
                    System.out.println(" WAS NULL - GOT DBC SUCCESS EVENTUALLY : t [" + Thread.currentThread().getName() + "] dbc non null after null [" + sWhichDB + "] iNulCounter [" + iNulCounter + "]");
                }
                break;
            } else {
                iNulCounter++;
                System.out.println(" DBC NULL: t [" + Thread.currentThread().getName() + "] dbc null after null [" + sWhichDB + "] iNulCounter [" + iNulCounter + "]");
                if ((System.currentTimeMillis() - lStartTime) > lMaxWaitTime) {
                    com.indraweb.util.Log.logcr("ERROR : dcb null after max wait : t [" + Thread.currentThread().getName() + "] dbc null after null [" + sWhichDB + "] iNulCounter [" + iNulCounter + "]");
                    throw new Exception("dcb null after max wait : t [" + Thread.currentThread().getName() + "] dbc null after null [" + sWhichDB + "] iNulCounter [" + iNulCounter + "]");
                }
            }

        }

        if (sWhichDB.equals("API")) {
            synchronized (oSynchCountConnectionsAPI) {
                iCountConnectionGetsAPI++;
            }
        } else if (sWhichDB.equals("SVR")) {
            synchronized (oSynchCountConnectionsAPI) {
                iCountConnectionGetsSVR++;
            }
        } else {
            throw new Exception("unknown get DB pool [" + sWhichDB + "]");
        }


        htConnectionsOut.put(dbcRtn, new Long(System.currentTimeMillis()));
        return dbcRtn;

    }

    public static Connection freeConnection(Connection dbc, String sWhichDB) throws Exception {
        return freeConnection(dbc, sWhichDB, "");
    }

    /**
     * put connection back in the pool for use
     *
     * @param dbc
     * @param sWhichDB
     * @param sDesc
     * @return
     * @throws Exception
     */
    public static Connection freeConnection(Connection dbc, String sWhichDB, String sDesc) throws Exception {
        if (dbc != null) {
            Long LTimeGivenOut = (Long) htConnectionsOut.get(dbc);
            if (LTimeGivenOut == null) {
                System.out.println("DBC FREE ERROR t [" + Thread.currentThread().getName() + "] dbc not recognized as having been given out trying to return to [" + sWhichDB + "] sDesc [" + sDesc + "]");
                throw new Exception("DBC FREE ERROR t [" + Thread.currentThread().getName() + "] dbc not recognized as having been given out [" + sWhichDB + "] sDesc [" + sDesc + "]");
            } else {
                htConnectionsOut.remove(dbc);
            }

            DbConnectionBroker dbcb = (DbConnectionBroker) htDbConnectionBrokers.get(sWhichDB);
            if (dbcb == null) {
                throw new Exception("internal program error no such DB connection pool [" + sWhichDB + "]");
            }
            dbcb.freeConnection(dbc);
            if (sWhichDB.equals("API")) {
                synchronized (oSynchCountConnectionsAPI) {
                    iCountConnectionFreeAPI++;
                }
            } else if (sWhichDB.equals("SVR")) {
                synchronized (oSynchCountConnectionsSVR) {
                    iCountConnectionFreeSVR++;
                }
            } else {
                throw new Exception("unknown free DB pool [" + sWhichDB + "]");
            }
            return null;
        } else {
            // assume that if called with null caller just wants to ses counts
            return null;
        }
    }

    public static void destroy(String sWhichDB) throws Exception {
        log.debug("destroy api.statics.DBConnectionJX sWhichDB [" + sWhichDB + "]");
        DbConnectionBroker dbcb = (DbConnectionBroker) htDbConnectionBrokers.get(sWhichDB);
        if (dbcb == null) {
            throw new Exception("internal program error no such DB connection pool [" + sWhichDB + "]");
        }
        dbcb.destroy();
    }
}
