package api;

import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;
import api.Log;

public class TSException extends Exception
{
	private int iTSReturnCode = -1;
	private String sTSErrorMessage = null;
	
	public TSException ( int iTSReturnCode )
	{
		super ( EmitGenXML_ErrorInfo.getErrText (iTSReturnCode));
		Log.LogError(this);
		this.iTSReturnCode = iTSReturnCode;
	}
	
	public TSException ( int iTSReturnCode, String sTSErrorMessage )
	{
		super ( EmitGenXML_ErrorInfo.getErrText (iTSReturnCode) + ":" +sTSErrorMessage);
		Log.LogError(sTSErrorMessage, this);
		this.iTSReturnCode = iTSReturnCode;
		this.sTSErrorMessage = sTSErrorMessage;
	}
	
	public int getTSReturnCode()
	{
		return iTSReturnCode;	
	}
	public String getTSErrorMessage()
	{
		return sTSErrorMessage;	
	}
	
}
