package api.tests;

import org.dom4j.Element;

import java.util.*;
import java.io.PrintWriter;
import java.sql.ResultSet;

public class Assertion {
    // Combined attributes
    private String description;
    private boolean exists;
    private String existsWhere;
    private String searchText;

    // constructor(s)
    public Assertion() { }
    public Assertion(Element eAssertion) {
        Element eDesc = eAssertion.element("DESC");
        Element eExist = eAssertion.element("EXIST");
        Element eExistWhere = eAssertion.element("EXISTWHERE");
        Element eSearchText = eAssertion.element("SEARCHTEXT");

        description = eDesc.getText();
        existsWhere = eExistWhere.getText();
        searchText = eSearchText.getText();

        if (eExist.getText().toUpperCase().equals("YES")) exists = true;
        else exists = false;
        //System.out.println (
        //        "Assertion description [" + description + "]" +
        //        "exists [" + exists + "]" +
        //        "existsWhere [" + existsWhere + "]" +
        //        "searchText [" + searchText + "]" );
    }

    public String getDesc() { return description; }
    public String getExistsWhere() { return existsWhere; }
    public String getSearchText() { return searchText; }
    public boolean getExists() { return exists; }
    public boolean isAPositiveOnIWErrorOrTSError() {
        return exists && isFailTag ( searchText ) && description.indexOf("FAIL WANTED") < 0 ;

    }

    public static boolean isFailTag ( String sTag )
    {
        return sTag.equalsIgnoreCase("TSERROR") ||
         sTag.equalsIgnoreCase("TS_ERROR") ||
         sTag.equalsIgnoreCase("IWERROR") ||
         sTag.equalsIgnoreCase("IW_ERROR") ||
         sTag.equalsIgnoreCase("INTERNALSTACKTRACE");
    }
}
