package api.tscql;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.Log;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

import sql4j.CQLInterpreter.*;

/**
 *  Submits a CQL query to the system for execution.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Query   CQL Query for information on how to format, refer to CQL earlier in this manual
 *  @param  Start (optional)    Start with result number N.  This is a useful parameter for next/previous functionality.
 *  @param  Rowmax (optional)   Maximum number of rows to be returned.  It is not related to the Start parameter.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tscql.TSCql&SKEY=985786764&query=SELECT+<DOCUMENT>+WHERE+((NODETITLE+%3D+%27Acute+Heart+Failure%27))&start=1&rowmax=25

 *	@return	CQL queries return the object selected.  Currently <NODE>, <DOCUMENT>, and <NODEDOCUMENT> objects are valid requests.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <NODEDOCUMENTS>
            <NODEDOCUMENT>
                <NODEID>1087920</NODEID>
                <DOCUMENTID>200001</DOCUMENTID>
                <CORPUSID>300012</CORPUSID>
                <PARENTID>1087909</PARENTID>
                <SCORE1>75.0</SCORE1>
                <DOCSUMMARY><![CDATA[ operating and technology expenditure, with theoperating and technology expenditure, with the... Armscor to determine client needs, to establish... obtain military systems and equipment, both... upgrading of computer network... It is an acquisition organisation and the... needs, to establish technology, to... needs, to establish technology, to... for example, the acquisition cash flow for... - Making its acquisition services available to... - Providing acquisition support to defence...]]></DOCSUMMARY>
                <DATESCORED>2002-10-13 22:20:58.0</DATESCORED>
                <INDEXWITHINNODE>1</INDEXWITHINNODE>
                <GENREID>121</GENREID>
                <NODETITLE><![CDATA[ Impacts of New Technology]]></NODETITLE>
                <DOCTITLE><![CDATA[ Armscor s Net Income Reportedly Tumbles]]></DOCTITLE>
                <DOCURL><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></DOCURL>
                <DOCUMENTSUMMARY><![CDATA[ null]]></DOCUMENTSUMMARY>
                <DATELASTFOUND>2002-10-16 09:50:48.0</DATELASTFOUND>
                <VISIBLE>1</VISIBLE>
                <FULLTEXT><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></FULLTEXT>
            </NODEDOCUMENT>
 ...
  \endverbatim
 */
public class TSCql
{
// TSListCorpera (sessionid)
	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
		if (u == null) {
            Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            Log.LogError("User session error in TSCql", e);
            throw e;
        }

		String sProp_CQLquery = (String) props.get("query", true);
        String sProp_Date = (String) props.get("date");
        int iProp_RowMax = props.getint("rowmax", "-1");
        int iProp_Start = props.getint("start", "1");

        // if selecting IDRACDOCUMENT, use plain ol' DOCUMENT instead
        if (sProp_CQLquery.toUpperCase().indexOf("<IDRACDOCUMENT>") != -1)
            sProp_CQLquery = sProp_CQLquery.replaceAll("IDRACDOCUMENT", "DOCUMENT");

        // redirection here: if selecting a narrative result, do not invoke CQL at all
        if (sProp_CQLquery.toUpperCase().indexOf("<NARRATIVE>") != -1) {
            // SELECT <NARRATIVE> WHERE NARRATIVEID = X
            Pattern p = Pattern.compile("\\s*NARRATIVEID\\s*=\\s*(.*?)\\ (.*)", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(sProp_CQLquery); boolean bResult = m.find();

            if (!bResult) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR); }

            String sID = m.group(1); String sType = m.group(2);
            if (sID.equals("")) { sID = sType; sType = "CACHE"; }

            props.put("DocumentID", sID);
            if (sType.trim().toUpperCase().equals("NOCACHE")) props.put("UseCache", "0");
            else props.put("UseCache", "1");

            if (sProp_Date != null) props.put("datefull", sProp_Date);

            api.Log.Log("Date prop: "+sProp_Date);
            api.tsdocument.TSGetSimilarDocumentsByDocumentID.handleTSapiRequest(props, out, dbc);
            return;
        }

        sql4j.CQLInterpreter.CQLxmlGen.topLevelCallFromTSCql (
            sProp_CQLquery, iProp_Start, iProp_RowMax, out, dbc, false, true, u );

	}
}
