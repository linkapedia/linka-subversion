package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Retrieve the current build version of this CDMS server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TGetVersion&SKEY=-132981656

 *	@return	VERSION object
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <VERSION>CDMS 2.4 release candidate 3</VERSION>
  </TSRESULT>
  \endverbatim
 */
public class TSGetVersion
{
	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception
	{
		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            if (u == null) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            out.println("<VERSION>ProcinQ Server 3.501 Release Candidate 2</VERSION>");
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
