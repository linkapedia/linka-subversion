package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.utils.oracle.*;
import com.iw.system.Index;

import com.iw.system.User;
import com.iw.system.IndraField;
/**
 * Get table and data type information from the database.  Because tables are dynamic and contain variable
 *  metadata, property tools should call this function to determine what fields the object contains.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  TABLE   The Oracle table being queried.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TSGetTableInfo&Table=DOCUMENT&SKEY=-132981656

 *	@return	A TABLE object containing fields and data types.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
     <TABLE>
         <FIELD>
            <NAME>DOCUMENTID</NAME>
            <DATATYPE>NUMBER</DATATYPE>
            <LENGTH>9</LENGTH>
         </FIELD>
         <FIELD>
            <NAME>DOCTITLE</NAME>
            <DATATYPE>VARCHAR</DATATYPE>
            <LENGTH>256</LENGTH>
        </FIELD>
 ...
  </TSRESULT>
  \endverbatim
 */
public class TSGetTableInfo
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        String sKey = (String) props.get("SKEY", true);
        String sTable = (String) props.get("Table", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

        Hashtable htFields = CustomFields.getFields(sTable, dbc);

        if (htFields.size() > 0) {
            out.println("<TABLE>");
            Enumeration eH = htFields.elements();
            while (eH.hasMoreElements()) {
                IndraField inf = (IndraField) eH.nextElement();

                out.println("  <FIELD>");
                out.println("     <NAME>"+inf.getName()+"</NAME>");
                out.println("     <DATATYPE>"+inf.getType()+"</DATATYPE>");
                out.println("     <LENGTH>"+inf.getLength()+"</LENGTH>");
                out.println("  </FIELD>");
            }
            out.println("</TABLE>");
        }

    }
}
