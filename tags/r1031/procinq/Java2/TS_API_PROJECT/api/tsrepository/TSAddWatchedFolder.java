package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Add a new watched folder to the given repository.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique repository identifier of the target repository.
 *  @param  Path    Path to the location of the folder in this repository.
 *  @param  Recurse (optional)  Set recurse to 1 if crawler should recurse below the path provided.  Default is 0.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSAddWatchedFolder&SKEY=-132981656&RID=1&Path=/home/documents/scr/

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Watched folder added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddWatchedFolder
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sID = (String) props.get ("RID", true);
		String sPath = (String) props.get ("Path", true);
		String sRecurse = (String) props.get ("Recurse", "0");

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			String sSQL = "insert into WatchFolder (RepositoryID, Path, Recurse, DateLastCapture) "+
						  "values ("+sID+", '"+sPath+"', "+sRecurse+", SYSDATE-10000)";
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Insert into watched folder table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Watched folder added successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse )	 {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
