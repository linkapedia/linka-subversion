package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Edit a watched folder in the given repository.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique repository identifier of the target repository.
 *  @param  OldPath Old path to the location of the folder in this repository.
 *  @param  Path    New Path to the location of the folder in this repository.
 *  @param  Recurse (optional)  Set recurse to 1 if crawler should recurse below the path provided.  Default is 0.
 *  @param  Date    Date of update, if not now.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSEditWatchedFolder&SKEY=-132981656&RID=1&OldPath=/home/documents/scr/&Path=/home/docs/scr

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Watched folder updated successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditWatchedFolder
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sID = (String) props.get ("RID", true);
		String sOld = (String) props.get("OldPath", true);
		String sPath = (String) props.get ("Path", true);
		String sRecurse = (String) props.get ("Recurse", "0");
		String sDate = (String) props.get ("Date");

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			String sSQL = " update WatchFolder set Path = '"+sPath+"', "+
						  " Recurse = "+sRecurse;
			if (sDate != null) { sSQL = sSQL + ", DateLastCapture = "+sDate; }
			
			sSQL = sSQL +" where Path = '"+sOld+"' and RepositoryID = "+sID;
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Update watched folder table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Watched folder updated successfully.</SUCCESS>");
		    stmt.close();
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
