package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Edit the properties of a repository in this server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique repository identifier of the target repository.
 *  @param  Type (optional) Unique repository type identifier indicating the type of repository being added.
 *  @param  Name (optional) Name of this repository.
 *  @param  Location (optional) Location of this repository.
 *  @param  Username (optional) Username that is required to access this location, if applicable.
 *  @param  Password (optional) Password that is required to access this location, if applicable.
 *  @param  FullText (optional) Set to 1 if documents included in the full text index.  Default is 1.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSEditRepositoryProps&SKEY=-132981656&RID=1&Name=Testing

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Repository altered successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditRepositoryProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sID = (String) props.get ("RID", true);
		String sType = (String) props.get ("Type");
		String sName = (String) props.get ("Name");
		String sLoc = (String) props.get ("Location");
		String sUsername = (String) props.get ("Username");
		String sPassword = (String) props.get ("Password");
		String sFullText = (String) props.get ("Full");

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			/*
			SQL> describe repository
			 Name                                      Null?    Type
			----------------------------------------- -------- ----------------------------
			 REPOSITORYID                              NOT NULL NUMBER(5)
			 REPOSITORYTYPEID                          NOT NULL NUMBER(5)
			 REPOSITORYNAME                            NOT NULL VARCHAR2(256)
			 REPOSITORYLOC                                      VARCHAR2(500)
			 USERNAME                                           VARCHAR2(50)
			 PASSWORD                                           VARCHAR2(50)
			 FULLTEXT                                           NUMBER(1)
			*/
			
			String sSQL = " update Repository set"; 
			boolean bComma = false;
			if (sType != null) { sSQL = sSQL + " RepositoryTypeID = "+sType; bComma = true; }
			if (sName != null) { 
				if (bComma) { sSQL = sSQL + ", "; }
				sSQL = sSQL + " RepositoryName = '"+sName+"'";
			}
			if (sLoc != null) {
				if (bComma) { sSQL = sSQL + ", "; }
				sSQL = sSQL + " RepositoryLoc = '"+sLoc+"'";
			}
			if (sUsername != null) {
				if (bComma) { sSQL = sSQL + ", "; }
				sSQL = sSQL + " Username = '"+sUsername+"'";
			}
			if (sPassword != null) {
				if (bComma) { sSQL = sSQL + ", "; }
				sSQL = sSQL + " Password = '"+sPassword+"'";
			}
			if (sFullText != null) {
				if (bComma) { sSQL = sSQL + ", "; }
				sSQL = sSQL + " FullText = "+sFullText;
			}
			sSQL = sSQL + " where RepositoryID = "+sID;
			
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Edit repository failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Repository altered successfully.</SUCCESS>");
		    stmt.close();
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
