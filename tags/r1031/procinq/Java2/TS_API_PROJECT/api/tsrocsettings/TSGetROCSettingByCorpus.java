package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Return the ROC setting identifier associated with the given corpus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CorpusID     Unique corpus (taxonomy) identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSGetROCSettingByCorpus&corpusid=1&SKEY=993135977

 *	@return	an ROC setting object
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <ROCSETTINGID>1</ROCSETTINGID>
  </TSRESULT>
  \endverbatim
 */
public class TSGetROCSettingByCorpus
{
	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception {

        Statement stmt = null;
        ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            String sCID = (String) props.get ("corpusid", true);

            String sSQL = " select rocSettingID from corpus where corpusID = "+sCID;

            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);
            rs.next();

            String s = rs.getString(1);
            out.println("<ROCSETTINGID>"+s+"</ROCSETTINGID>");
		}
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
