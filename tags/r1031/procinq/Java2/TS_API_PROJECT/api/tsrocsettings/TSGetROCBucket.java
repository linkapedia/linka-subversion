package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Return all local classifiers associated with this ROC setting identifier.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique ROC setting identifier.
 *  @param  StarCount (optonal) Return a single local classifier based on the tier (star count)
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSGetROCBucket&rid=1&SKEY=993135977

 *	@return	a series of local classifier objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <LOCALCLASSIFIERS>
        <LOCALCLASSIFIER>
            <ROCSETTINGID>1</ROCSETTINGID>
            <CLASSIFIERID>30</CLASSIFIERID>
            <CLASSIFIERNAME>f*c^15</CLASSIFIERNAME>
            <STARCOUNT>1</STARCOUNT>
            <COSTRATIOFPTOFN>0.2</COSTRATIOFPTOFN>
            <CUTOFFSCOREGTE>0.3</CUTOFFSCOREGTE>
            <SCORE1VAL>50</SCORE1VAL>
            <TRUEPOS>0</TRUEPOS>
            <FALSEPOS>0.25</FALSEPOS>
        </LOCALCLASSIFIER>
        <LOCALCLASSIFIER>
            <ROCSETTINGID>1</ROCSETTINGID>
            <CLASSIFIERID>30</CLASSIFIERID>
            <CLASSIFIERNAME>f*c^15</CLASSIFIERNAME>
            <STARCOUNT>2</STARCOUNT>
            <COSTRATIOFPTOFN>1</COSTRATIOFPTOFN>
            <CUTOFFSCOREGTE>0.6667</CUTOFFSCOREGTE>
            <SCORE1VAL>75</SCORE1VAL>
            <TRUEPOS>0</TRUEPOS>
            <FALSEPOS>0.25</FALSEPOS>
        </LOCALCLASSIFIER>
...
  </TSRESULT>
  \endverbatim
 */
public class TSGetROCBucket
{
	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception {

        Statement stmt = null;
        ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            String sRID = (String) props.get ("rid", true);
            String sStar = (String) props.get ("starcount");

            String sSQL = " select R.starCount, R.costRatioFPtoFN, R.cutOffScoreGTE, R.classifierID, "+
                          " R.score1Val, R.truePos, R.falsePos, C.classifierName from rocBucketLocalClassifier R, "+
                          " Classifier C where C.classifierID = R.classifierID and C.active = 1 and "+
                          " R.rocSettingID = "+sRID;
            if (sStar != null) { sSQL = sSQL + " and R.starCount = "+sStar; }

            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            int loop = 0;
            while ( rs.next() ) {
                sStar = rs.getString(1);
                String sCost = rs.getString(2);
                String sCut = rs.getString(3);
                String sClassID = rs.getString(4);
                String sScore = rs.getString(5);
                String sTrue = rs.getString(6);
                String sFalse = rs.getString(7);
                String sClassName = rs.getString(8);

                loop = loop + 1;
                if (loop == 1) { out.println("  <LOCALCLASSIFIERS>"); }
                out.println ("      <LOCALCLASSIFIER>");
                out.println ("          <ROCSETTINGID>"+sRID+"</ROCSETTINGID>");
                out.println ("          <CLASSIFIERID>"+sClassID+"</CLASSIFIERID>");
                out.println ("          <CLASSIFIERNAME>"+sClassName+"</CLASSIFIERNAME>");
                out.println ("          <STARCOUNT>"+sStar+"</STARCOUNT>");
                out.println ("          <COSTRATIOFPTOFN>"+sCost+"</COSTRATIOFPTOFN>");
                out.println ("          <CUTOFFSCOREGTE>"+sCut+"</CUTOFFSCOREGTE>");
                out.println ("          <SCORE1VAL>"+sScore+"</SCORE1VAL>");
                out.println ("          <TRUEPOS>"+sTrue+"</TRUEPOS>");
                out.println ("          <FALSEPOS>"+sFalse+"</FALSEPOS>");
                out.println ("      </LOCALCLASSIFIER>");
            }
            if ( loop != 0) { out.println(" </LOCALCLASSIFIERS>"); }
		}
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
