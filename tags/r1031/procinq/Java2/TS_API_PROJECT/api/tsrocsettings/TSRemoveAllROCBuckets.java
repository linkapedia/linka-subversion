package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Given an ROC setting identifier, remove all associated buckets.  This function is typically used to clear
 *   out existing buckets before a new set of buckets is installed.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ROCSettingID     Unique ROC setting identifier.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSRemoveAllROCBuckets&rocsettingid=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>All ROC Buckets removed successfully..</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveAllROCBuckets
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sRID = (String) props.get ("rocsettingid", true);

        Statement stmt = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " delete from rocBucketLocalClassifier where rocsettingID = "+sRID;
 			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>ROCbucket removal failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>All ROC Buckets removed successfully.</SUCCESS>");
		    stmt.close();
		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
