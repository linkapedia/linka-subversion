package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Edit the properties of an ROC cutoff bucket.  The unique (primary key) or distinguishing attributes of an
 *   ROC bucket are the ROCSettingID in combination with the tier (star count).
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ROCSettingID     Unique ROC setting identifier for this bucket.
 *  @param  StarCount   The tier or layer that this bucket represents (typically 1, 2, or 3).
 *	@param  ClassifierID (optional) Unique identifier corresponding to the classifier used in this ROC range.
 *  @param  Cost (optional)    The cost ratio of false positives to false negatives.
 *  @param  CutOff (optional)  The cut off score for this bucket range (GTE)
 *  @param  Score (optional)    The "score1" representation for this bucket.  Default values are 100 for 3, 75 for 2, and 50 for 1.
 *  @param  TruePos (optional)  True positive percentage in this range.
 *  @param  FalsePos (optional) False positive percentage in this range.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSEditROCBucketProps&classifierid=1&rocsettingid=1&starcount=1&cost=0.2&cutoff=0.3&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>ROC Bucket updated successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditROCBucketProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sRID = (String) props.get ("rocsettingid", true);
        String sStar = (String) props.get ("starcount", true);

        // optional fields
        String sCID = (String) props.get ("classifierid");
        String sCost = (String) props.get ("cost");
        String sCut = (String) props.get ("cutoff");
        String sScore = (String) props.get ("score");
        String sTrue = (String) props.get ("truepos");
        String sFalse = (String) props.get ("falsepos");

        Statement stmt = null; ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " update rocBucketLocalClassifier set rocSettingID="+sRID;
            if (sCID != null) { sSQL = sSQL + ", classifierID = "+sCID; }
            if (sCost != null) { sSQL = sSQL + ", costRatioFPtoFN = "+sCost; }
            if (sCut != null) { sSQL = sSQL + ", cutoffScoreGTE = "+sCut; }
            if (sScore != null) { sSQL = sSQL + ", score1Val = "+sScore; }
            if (sTrue != null) { sSQL = sSQL + ", truePos = "+sTrue; }
            if (sFalse != null) { sSQL = sSQL + ", falsePos = "+sFalse; }

            sSQL = sSQL + " where rocsettingid = "+sRID+" and starCount = "+sStar;
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Update rocbucket failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>ROC Bucket updated successfully.</SUCCESS>");
		    stmt.close();
		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
