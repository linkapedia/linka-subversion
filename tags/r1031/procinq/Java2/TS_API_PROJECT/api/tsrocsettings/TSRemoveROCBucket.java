package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove a single ROC bucket from a given an ROC setting and the tier.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ROCSettingID     Unique ROC setting identifier.
 *  @param  StarCount   Tier (star count) of the bucket to be removed.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSRemoveROCBucket&rocsettingid=1&starcount=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>All ROC Bucket removed successfully..</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveROCBucket
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sRID = (String) props.get ("rocsettingid", true);
        String sStar = (String) props.get ("starcount", true);

        Statement stmt = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " delete from rocBucketLocalClassifier where rocsettingID = "+sRID+" and starCount = "+sStar;
 			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Removal of rocbucket failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>ROC Bucket removed successfully.</SUCCESS>");
		    stmt.close();
		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
