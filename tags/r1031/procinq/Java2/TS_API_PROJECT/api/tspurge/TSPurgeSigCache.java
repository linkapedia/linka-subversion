package api.tspurge;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.ir.Thesaurus;
import com.iw.classification.Data_WordsToNodes;

/**
 * Purge the signature cache from this server.  The signature cache is automatically generated when a classification
 *  takes place and the cache is empty.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CorpusID (optional) Unique corpus identifier if only a signal corpus cache should be purged.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tspurge.TSPurgeSigCache&SKEY=-132981656

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Signature files purged successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSPurgeSigCache
{
	// Purge all nodes marked for deletion
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
		String sCorpusID = (String) props.get("CorpusID");
		
		try {
			// for authorization purposes, need to know the corpus id of this node
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			boolean bSuccessFail = false;
			
			if (sCorpusID == null) { bSuccessFail = PurgeSignatures(out, dbc); }
			else { bSuccessFail = PurgeSignatures(sCorpusID, out, dbc); }
			
			if (bSuccessFail) { out.println("<SUCCESS>Signature files purged successfully.</SUCCESS>"); }
			else { out.println("<FAILURE>Signature purge failed</FAILURE>"); }
		} catch ( TSException tse ) { 
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
	
	public static boolean PurgeSignatures(PrintWriter out, Connection dbc) {
		try {
			String sFilePath = com.indraweb.execution.Session.cfg.getProp ("IndraHome")+"/corpussource/";

			String sSQL = " select CorpusID from Corpus";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			while ( rs.next() ) {
				String sCID = rs.getString(1);
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus("TSPurgeSigCache.PurgeSignatures()",Integer.parseInt(sCID));
			}
			
		} catch (Exception e) {
			api.Log.LogError(e);
			return false; 
		}
		return true;
	}
		
	public static boolean PurgeSignatures(String sCorpusID, PrintWriter out, Connection dbc) {
		try {
			String sFilePath = com.indraweb.execution.Session.cfg.getProp ("IndraHome")+"/corpussource/";

			String sSQL = " select CorpusID from Corpus";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			while ( rs.next() ) {
				String sCID = rs.getString(1);
				
				if ((sCorpusID == null) || (sCID.equals(sCorpusID))) {
                    Data_WordsToNodes.invalidateNodeSigCache_byCorpus("TSPurgeSigCache.PurgeSignatures() 2", Integer.parseInt(sCID));
				}
			}
			
		} catch (Exception e) {
			api.Log.LogError(e);
			return false;
		}
		
		return true;
	}		
	public static boolean PurgeThesaurusSignatures(String sThesID, PrintWriter out, Connection dbc) {
		try {
			String sFilePath = com.indraweb.execution.Session.cfg.getProp ("IndraHome")+"/corpussource/";

			String sSQL = " select CorpusID from CorpusThesaurus where ThesaurusID = "+sThesID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			while ( rs.next() ) {
				String sCID = rs.getString(1);
				
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus("TSPurgeSigCache.PurgeThesaurusSignatures", Integer.parseInt(sCID));
                Thesaurus.cacheInvalidate(Integer.parseInt(sCID), true);
			}
			
		} catch (Exception e) {
			api.Log.LogError(e);
			return false;
		}
		
		return true;
	}	
}

