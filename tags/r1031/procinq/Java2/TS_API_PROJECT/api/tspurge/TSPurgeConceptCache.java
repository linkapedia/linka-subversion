package api.tspurge;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Purge the concept alert cache, either for a given narrative or all narratives.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CONCEPTID (optional)    Concept (document) identifier to be purged
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TSPurgeConceptCache&SKEY=-132981656

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>All objects were purged successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSPurgeConceptCache
{
	// TSPurgeObjects (sessionid)
	// Warning! This func
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		try {
			String sKey = (String) props.get("SKEY", true);
            String sConceptID = (String) props.get("CONCEPTID");

			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			// Select each Corpus marked for deletion
			String sSQL = "delete from conceptalertcache";
            if (sConceptID != null) sSQL = sSQL + " where conceptid = "+sConceptID;

			Statement stmt = null;

            try {
                stmt = dbc.createStatement();

                if (stmt.executeUpdate (sSQL) == 0) {
                    out.println ( "<DEBUG>Cannot purge objects ("+sSQL+")</DEBUG>");
                    throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR);
                }
            } catch (Exception e) { throw e;
            } finally { if (stmt != null) { stmt.close(); stmt = null; }}


            out.println("<SUCCESS>All objects were purged successfully.</SUCCESS>");
		}

		// Catch exception
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
