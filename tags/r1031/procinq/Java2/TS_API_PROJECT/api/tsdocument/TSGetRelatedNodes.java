package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.results.*;
import api.security.*;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Retrieves the related topic for a given Document URL or Document identifier.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocURL  The URL or UNC that links uniquely to this document.
 *	@param  DocumentID (optional)  Unique document identifier.  API executes more efficiently with this information.
 *
 *  @note    http://ITSSERVER//itsapi/ts?fn=tsdocument.TSGetRelatedNodes&DocumentID=200001&DocURL=%5C%5C66.134.131.60%5CFBIS4-23527.HTM&SKEY=1059639451&ScoreThreshold=25.0

 *	@return	A series of ITS NODEDOCUMENT objects ordered by BUCKET SCORE descending.
 *  \verbatim
 <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <NODEDOCUMENTS>
            <NODEDOCUMENT>
                <NODEID>1087920</NODEID>
                <DOCUMENTID>200001</DOCUMENTID>
                <CORPUSID>300012</CORPUSID>
                <PARENTID>1087909</PARENTID>
                <SCORE1>75.0</SCORE1>
                <DOCSUMMARY><![CDATA[ operating and technology expenditure, with theoperating and technology expenditure, with the... Armscor to determine client needs, to establish... obtain military systems and equipment, both... upgrading of computer network... It is an acquisition organisation and the... needs, to establish technology, to... needs, to establish technology, to... for example, the acquisition cash flow for... - Making its acquisition services available to... - Providing acquisition support to defence...]]></DOCSUMMARY>
                <DATESCORED>2002-10-13 22:20:58.0</DATESCORED>
                <INDEXWITHINNODE>1</INDEXWITHINNODE>
                <GENREID>121</GENREID>
                <NODETITLE><![CDATA[ Impacts of New Technology]]></NODETITLE>
                <DOCTITLE><![CDATA[ Armscor s Net Income Reportedly Tumbles]]></DOCTITLE>
                <DOCURL><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></DOCURL>
                <DOCUMENTSUMMARY><![CDATA[ null]]></DOCUMENTSUMMARY>
                <DATELASTFOUND>2002-10-16 09:50:48.0</DATELASTFOUND>
                <VISIBLE>1</VISIBLE>
                <FULLTEXT><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></FULLTEXT>
            </NODEDOCUMENT>
 ...
  \endverbatim
 */
public class TSGetRelatedNodes
{
	static class Container { public List C; }
    static class NodeResultCompare implements Comparator {
        public int compare(Object obj1, Object obj2) {
            NodeResult nr1 = (NodeResult) obj1;
            NodeResult nr2 = (NodeResult) obj2;

            try {
                // compare on score1 first
                if (new Integer(nr1.GetScore1()).intValue() >
                    new Integer(nr2.GetScore1()).intValue()) { return 1; }
                if (new Integer(nr1.GetScore1()).intValue() <
                    new Integer(nr2.GetScore1()).intValue()) { return -1; }

                return nr1.GetNodeTitle().compareTo(nr2.GetNodeTitle());
            } catch (Exception e) { api.Log.LogError(e); return 0; }
        }
    }

	// TSRelatedNodes (docurl)
	//
	// Given a docurl, return a list of nodes where this document also
	//  scored, sorted by node score.   Query against both the server and client databases.
	//  Thus, we will spin a thread to talk to the server database.
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// modified 9/30/2003 -- no longer call "server" database, and use CQL

		String sDocURL = (String) props.get ("DocURL", true);
		String sDocumentID = (String) props.get ("DocumentID");
        String sWithNodeTree = (String) props.get ("NodeTree");

		String sScoreThreshold = (String) props.get ("ScoreThreshold", "75");
		float fScoreThreshold = new Float(sScoreThreshold).floatValue();
 		int iScoreThreshold = (int) fScoreThreshold;

		String sKey = (String) props.get("SKEY");
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED); }

        // if the document identifier was not specified, try to look it up now
        if (sDocumentID == null) {
            com.iw.system.Document d = api.tsdocument.TSEditDocProps.GetDocument(props, dbc);
            sDocumentID = d.get("DOCUMENTID");
        }
        if (sDocumentID == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_SUCH_OBJECT); }

        // build the CQL query here
        String query = "SELECT <NODEDOCUMENT> WHERE DOCUMENTID = "+sDocumentID+" AND "+
                       "SCORE1 >= "+iScoreThreshold;
        query = query + " ORDER BY SCORE1 DESC";

        props.put("query", query);
        props.put("start", "1");
        props.put("rowmax", "500");

        api.tscql.TSCql.handleTSapiRequest(props, out, dbc);

        return;
    }
}