package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Validates a node document pair during user validation in ROC Tuning.  All document validations are
 * typically set to false with TSInvalidateDocument, prior to using this API.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID  Unique document identifier.
 *  @param  NodeID  Unique node identifier.
 *
 *  @note     http://ITSSERVER/servlet/ts?fn=tsdocument.TSValidateDocuments&SKEY=-723397494&NodeID=90863&DocumentID=4921

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Document validation added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSValidateDocument
{
	// TSValidateDocument
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDocumentID = (String) props.get ("DocumentID", true);
		String sNodeID = (String) props.get ("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " update ROCValidation set ROCValidation = 1"+
						  " where NodeID = "+sNodeID+" and DocumentID = "+
						  sDocumentID;
			Statement stmt = dbc.createStatement();	
			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Update validation failed: "+sSQL+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Document validation added successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}