package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Resets the values of documents for a node id to false. Used to clear the validation list before user validation.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  NodeID  Unique node identifier.
 *
 *  @note     http://ITSSERVER/servlet/ts?fn=tsdocument.TSInvalidateDocuments&SKEY=-723397494&NodeID=90863

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Document invalidation completed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSInvalidateDocuments
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " update ROCValidation set ROCValidation = 0"+
						  " where NodeID = "+sNodeID;
			
			Statement stmt = dbc.createStatement();	
			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Invalidation failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Document invalidation completed successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}