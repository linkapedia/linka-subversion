package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.sql.Date;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.iw.system.Document;
import com.iw.system.User;

/**
 *  Returns an XML structure containing all documents that classified into the same topics as the given document
 *  identifier.  Results are grouped and ordered by the number of topics in common with the given document.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID  Unique document identifier.
 *  @param  GenreID (optional) Genre/Folder identifier, if applicable
 *  @param  ScoreThreshold (optional)   Scoring threshold to limit documents, defaults to 75 (2 stars and above).
 *  @param  Date (optional) Restrict document results by date.  This parameter represents a minimum threshold of
 *          days in age for the document.   Example: documents within the last two weeks: 14.  If not specified,
 *          the threshold is not used and all documents are returned.
 *  @param  UseCache (optional) If the UseCache flag is set, results will be drawn from the cache first.  If no results
 *          are found in the cache, one will be generated.  Note if GENREID is specified this flag will automatically
 *          be set to false.   UseCache is either 1 (true) or 0 (false).  The default is 0.
 *  @param  OverrideCache (optional)    May only be set of UseCache is 1.  This will force a rebuild of the cache
 *          regardless of its current status.  OverrideCache is either 1 (true) or 0 (false).  The default is 0.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSGetSimilarDocumentsByDocumentID&SKEY=-132981656&DocumentID=29677

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <DOCUMENTS>
            <DOCUMENT>
                <DOCUMENTID>29517</DOCUMENTID>
                <DOCTITLE><![CDATA[Chemical Reactions]]></DOCTITLE>
                <DOCURL><![CDATA[http://66.134.131.62:8101/document/21k2/cc00.html]]></DOCURL>
                <COUNT>14</COUNT>
            </DOCUMENT>
 ...
  \endverbatim
 */
public class TSGetSimilarDocumentsByDocumentID {
    public static String sScoreThreshold = "50.0";
    public static int iMaxRecordCount = Session.cfg.getPropInt("MaxRecordCount");
    public static User user = null;
    private static boolean full = false;

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {
		String sDocumentID = (String) props.get ("DocumentID", true);
        String sGenreID = (String) props.get ("GenreID");
        String sThreshold = (String) props.get ("ScoreThreshold");
        String sUseCache = (String) props.get ("UseCache", "1");
        String sMax = (String) props.get ("MaxResults");
        String sDate = (String) props.get ("Date");
        String sDateFull = (String) props.get ("datefull");

        api.Log.Log("sDateFull: "+sDateFull);

        if (sDateFull != null) { full = true; sDate = sDateFull; }

        if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.NARRATIVE))
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

        // set max record
        int iMax = iMaxRecordCount;
        if (sMax != null) iMax = new Integer(sMax).intValue();

        boolean bUseCache = false;
        if (sUseCache.equals("1")) bUseCache = true;

        //boolean bUseCache = false; boolean bOverrideCache = false;
        String sKey = (String) props.get("SKEY", true);
        user = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (sThreshold != null) sScoreThreshold = sThreshold;

        int iNumResults = 0;
        iNumResults = getResults(sDocumentID, sGenreID, sDate, iMax, bUseCache, dbc, out);

        // If no results found, throw an exception
        if ( iNumResults == 0) {
            out.println("<NUMRESULTS>0</NUMRESULTS>");
            throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
        }
	}

    public static int getResults (String sDocumentID, String sGenreID, String sDate,
                                  int iMax, boolean bUseCache, Connection dbc, PrintWriter out)
            throws Exception {

        int iTotal = 0; int iHighestCount = 0;
        Statement stmt = null; ResultSet rs = null; int iCacheResults = 0;
        Hashtable ht = new Hashtable();

        String sSQL = "";
        try {
            // if useCache is false, first run the stored procedure to fill the temporary table
            if (!bUseCache) buildConceptCache(sDocumentID, dbc);

            // second, select information from the temporary table
            sSQL = Document.getSQLwithoutTable(dbc)+", THECOUNT from (select documentid, count thecount "+
                    "from conceptalertcache where conceptid = "+sDocumentID+" order by thecount desc) a, "+
                    "document d where a.documentid = d.documentid and rownum < 501 ";
            if (sGenreID != null) { sSQL = sSQL + " and d.GENREID = "+sGenreID; }
            if ((sDate != null) && full) { sSQL = sSQL + " AND d.datelastfound > '"+sDate+"'"; }
            if ((sDate != null) && !full) { sSQL = sSQL + " AND d.datelastfound > SYSDATE-"+sDate; }

            api.Log.Log("SQL: "+sSQL+" ("+sDate+") ("+full+")");

            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            //api.Log.Log("getResults (sql: "+sSQL+") ("+(System.currentTimeMillis() - lStart)+" ms) ");

            while (rs.next() && (iCacheResults <= iMax)) {
                Document d = new Document(rs);
                //d.setVisible(rs.getString(26));

                int iCount = Integer.parseInt(d.get("THECOUNT"));

                iCacheResults++;

                // second result divided by the first result is our working percentage
                if (iCacheResults == 1) { iTotal = iCount; out.println("<DOCUMENTS>"); }
                if (iCacheResults == 2) iHighestCount = iCount;

                out.println ("   <DOCUMENT> ");
                d.emitXML(out, user, false);
                out.println ("      <COUNT>"+iCount+"</COUNT>");
                out.println ("   </DOCUMENT>");

                if (iCacheResults == iMax) break;
            }

            if ((iCacheResults == 0) && (bUseCache)) {
                iCacheResults = getResults(sDocumentID, sGenreID, sDate, iMax, false, dbc, out);
            } else if (iCacheResults > 0) { out.println("</DOCUMENTS>"); }

             // update the document with the new percentage
            if ((iTotal != 0) && (!bUseCache)) {
                float score = 100*((float) iHighestCount/(float) iTotal);
                updateTroubleStatus(sDocumentID, score, dbc);
                //api.Log.Log("updating document: "+score+" = 100*((float) "+iHighestCount+"/"+iTotal);
            }

        } catch (Exception e) { api.Log.LogError("query failed: "+sSQL); api.Log.LogError(e); throw e;
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
            dbc.commit();
            dbc.setAutoCommit(true);
        }

        return iCacheResults;
    }

    public static void updateTroubleStatus (String DocumentID, float Percentage, Connection dbc)
            throws Exception {

        Statement stmt = null;

        try {
            String sSQL = "update Document set TroubleStatus = "+(int) Percentage+" where DocumentID = "+DocumentID;

            if (false) {
                api.Log.Log("updateTroubleStatus: attempting call SQL: "+sSQL);
                long lStart = System.currentTimeMillis();
            }
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
            //api.Log.Log("updateTroubleStatus ("+(System.currentTimeMillis() - lStart)+" ms) ");

        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }

    public static String getDocumentSecurityList (User u) {
        HashSet hsToUse = u.GetSecurityPermissions ();
        String s = "0";

        Iterator i = hsToUse.iterator();
        while (i.hasNext()) { s = s + ","+(String) i.next(); }

        return s;
    }

    // build concept cache
    public static boolean buildConceptCache (String sDocumentID, Connection dbc) {
        CallableStatement cs = null;

        if (true) { api.Log.Log("begin GetSimilarDocs ("+sDocumentID+"); end;"); }
        try {
            cs = dbc.prepareCall ("begin GetSimilarDocs (:1); end;"); cs.setString(1, sDocumentID);
            cs.execute(); return true;
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (cs != null) { try { cs.close(); } catch (Exception e) {} cs = null; }}
    }

    /*
    public static void writeToCache (String sDocumentID, Document document, int iCount, Connection dbc)
            throws Exception {

        PreparedStatement stmt = null;

        try {
            // 1) Insert into the cache table.
            String sSQL = " insert into ConceptAlertCache (conceptid, documentid, count) values "+
                          " ("+sDocumentID+", "+document.getID()+", "+iCount+")";

            if (false) {
                api.Log.Log("writeToCache: attempting call SQL: "+sSQL);
                long lStart = System.currentTimeMillis();
            }
            stmt = dbc.prepareStatement(sSQL);
            if (stmt.executeUpdate() == 0) {
                api.Log.LogError("insert failed: "+sSQL);
                throw new Exception ("error inserting into document cache failed .. "+sSQL);
            }

            //api.Log.Log("writeToCache ("+(System.currentTimeMillis() - lStart)+" ms) ");

        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }

    public static void deleteCache (String DocumentID, Connection dbc)
            throws Exception {

        Statement stmt = null;

        try {
            String sSQL = " delete from ConceptAlertCache where DocumentID = "+DocumentID;

            if (false) {
                api.Log.Log("deleteCache: attempting call SQL: "+sSQL);
                long lStart = System.currentTimeMillis();
            }
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
            //api.Log.Log("deleteCache ("+(System.currentTimeMillis() - lStart)+" ms) ");

        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }
    */

    /*
    public static int printDocumentResults (ResultSet rs, String DocumentID, int iMax, PrintWriter out, Connection dbc)
        throws Exception { return printDocumentResults (rs, DocumentID, iMax, out, dbc, false); }
    public static int printDocumentResults (ResultSet rs, String DocumentID, int iMax, PrintWriter out, Connection dbc, boolean bWriteCache)
        throws Exception {

        int loop = 0;

		while ( rs.next() ) {

            Document d = new Document(rs);
            int iCount = rs.getInt(26);

            //if (isDocumentAuthorized(d.getID(), user, dbc)) { // meets security criteria
                loop = loop + 1;
                if (loop == 1) { out.println("<DOCUMENTS>"); }

                out.println ("   <DOCUMENT> ");
                d.emitXML(out, u, false);
                out.println ("      <COUNT>"+iCount+"</COUNT>");
                out.println ("   </DOCUMENT>");
            //}

            //if (bWriteCache) { writeToCache(DocumentID, d, iCount, dbc); }

            if (loop == iMax) break;
        }

        if (loop != 0) {
            out.println("</DOCUMENTS>");
            out.println("<NUMRESULTS>"+loop+"</NUMRESULTS>");
        }

        return loop;
    } */

    /*
    public static int getCacheResults (String sDocumentID, int iMax, boolean bOverwriteCache,
                                           String sDate, String sGenreID, Connection dbc, PrintWriter out)
        throws Exception {

        Statement stmt = null; ResultSet rs = null; int iResults = 0;

        // First: attempt to retrieve the results from the cache table.  Skip if an overwrite cache was requested.
        if (!bOverwriteCache) {
            try {
                String sSQL = Document.getSQLwithoutTable()+",c.count from Document d, ConceptAlertCache c, "+
                              " DocumentSecurity s WHERE d.documentid = c.documentid and c.conceptid = "+sDocumentID+
                              " and d.documentid = s.documentid and s.securityid in ("+getDocumentSecurityList(user)+") ";
                if (sGenreID != null) { sSQL = sSQL + " AND d.genreid = "+sGenreID; }
                if (sDate != null) { sSQL = sSQL + " AND d.datelastfound > SYSDATE-"+sDate; }
                sSQL = sSQL + " ORDER BY c.COUNT DESC";

                if (true) {
                    long lStart = System.currentTimeMillis();
                    api.Log.Log("getCacheResults: attempting call SQL: "+sSQL);
                }
                stmt = dbc.createStatement();
                rs = stmt.executeQuery (sSQL);

                //api.Log.Log("getCacheResults ("+(System.currentTimeMillis() - lStart)+" ms) ");
                iResults = printDocumentResults(rs, sDocumentID, iMax, out, dbc);
            } catch (Exception e) { api.Log.LogError(e); throw e;
            } finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }
        }

        // if results were found in the cache, we are done, return true.
        if (iResults != 0) { return iResults; }

        // Second: no results were found in the cache.  Now attempt to get results the normal way.
        boolean bWriteCache = true; if (sDate != null) { bWriteCache = false; }

        //api.Log.Log("inside getCacheResults: "+bWriteCache);
        if (bWriteCache) { deleteCache(sDocumentID, dbc); }
        iResults = getResults (bWriteCache, sDocumentID, null, sDate, iMax, dbc, out);
        return iResults;
    }
    */
}