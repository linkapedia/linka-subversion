package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.security.*;
import api.TSException;
import api.emitxml.*;
import api.results.Locked;

import com.indraweb.database.*;
import com.indraweb.utils.oracle.CustomFields;
import com.iw.system.Document;
import com.iw.system.User;
import com.iw.system.SecurityGroup;
import com.iw.system.IndraField;

public class TSEditDocProps {

    /**
     * This is an internal API function. Please use TSEditDocument or TSAddNodeDocument to either edit document
     * properties or add node-document relationships (respectively).
     *
     * @param	SKEY Session key corresponding to the current user session.
     * @param DocURL The URL or UNC that links uniquely to this document.
     * @param NodeID Unique node identifier.
     * @param DocTitle (optional) The document title or subject heading.
     * @param GenreID (optional) Genre/Folder identifier, if applicable
     * @param FilterStatus (optional) Set to 1 if document is active, 0 otherwise.
     * @param DocSummary (optional) A brief abstract of 4000 characters or less.
     * @param Score1 (optional) Node bucket score (star count)
     * @param Security (optional) A series of security identifiers separated by commas, indicating access permissions
     * @param Score2 (optional) Floating point representation of classifier 2 score value
     * @param Score3 (optional) Floating point representation of classifier 3 score value
     * @param Score4 (optional) Floating point representation of classifier 4 score value
     * @param Score5 (optional) Floating point representation of classifier 5 score value
     * @param Score6 (optional) Floating point representation of classifier 6 score value
     *
     * @note http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0
     * <p/>
     * @return	SUCCESS tag if the relationship was added successfully.
     * \verbatim
     * <?xml version="1.0" encoding="UTF-8" ?>
     * <TSRESULT>
     * <SUCCESS>Document modification successful!</SUCCESS>
     * </TSRESULT>
     * \endverbatim
     */
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        // Begin by attempting to update the database tables NodeDocument and Document
        // DOCUMENT table first..
        String sDocURL = (String) props.get("docurl", true);
        String sNodeID = (String) props.get("nodeid", true);
        String sSecurity = (String) props.get("security");

        String sDocumentID = new String("");
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        int iCorpusId = 0;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            if (Locked.NodeDocumentLocked(dbc)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED);
            }

            // for authorization purposes, need to know the corpus id of this node
            String sSQL = " select CorpusId from Node where NodeID = " + sNodeID;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                rs.next();

                iCorpusId = rs.getInt(1);
            } catch (Exception e) {
                throw e;
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (Exception e) {
                    }
                    rs = null;
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (Exception e) {
                    }
                    stmt = null;
                }
            }
            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            // Fetch the DOCID from the DOCURL, because it is the only field guaranteed to
            //  be unique between the client and server databases.   If it does not exist,
            //  do not attempt an update as the document does not exist in the client database.
            Document d = GetDocument(props, dbc);

            if ((d == null) || (d.get("DOCUMENTID").equals(""))) {
                TSEditDocProps tsedit = new TSEditDocProps();
                throw tsedit.new TSDocDoesNotExist();
            }
            sDocumentID = d.get("DOCUMENTID");

            props.put("documentid", d.get("DOCUMENTID"));
            props.put("filterstatus", "2");

            String sTableName = "document";
            int iNumRequiredFields = 1;
            String[] sArrFields_URLNameSpace = {
                "documentid", // required
                "genreid", "doctitle", "docurl", "filterstatus"};
            String[] sArrFields_DBNameSpace = {
                "documentid", // required
                "genreid", "doctitle", "docurl", "filterstatus"};
            sSQL = api.sqlinteract.SQLModelUpdate.genSQLUpdate(sTableName, props, sArrFields_URLNameSpace, sArrFields_DBNameSpace,
                    iNumRequiredFields);

            if (!sSQL.equals("")) {
                try {
                    stmt = null;
                    stmt = dbc.createStatement();

                    // If no rows were returned, document does not exist in the client
                    //   database.   It will need to be inserted.
                    if (stmt.executeUpdate(sSQL) == 0) {
                        TSEditDocProps tsedit = new TSEditDocProps();
                        throw tsedit.new TSDocDoesNotExist();
                    }
                } catch (Exception e) {
                    throw e;
                } finally {
                    if (stmt != null) {
                        try {
                            stmt.close();
                        } catch (Exception e) {
                        }
                        stmt = null;
                    }
                }
            }

            // for batch processing, added document update date
            UpdateDocumentDate(d.get("DOCUMENTID"), dbc);

            // if the "security" argument was specified, update this document's security hexmap
            if (sSecurity != null) {
                String securityArr[] = sSecurity.split(",");
                Vector v = new Vector();

                for (int i = 0; i < securityArr.length; i++) {
                    SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID(securityArr[i]);
                    v.add(sg);
                }

                InsertDocumentSecurity(Integer.parseInt(d.get("DOCUMENTID")), v, dbc, out);
            }

            // DOCUMENT update completed successfully.    Now attempt NODEDOCUMENT ...
            sTableName = new String("nodedocument");
            props.put("edited", "1");

            iNumRequiredFields = 2;
            String[] sArrFields_URLNameSpaces = {
                "nodeid", // required
                "documentid", // required
                "documenttype", "score1", "score2", "score3", "score4", "score5", "score6", "docsummary", "edited"};
            String[] sArrFields_DBNameSpaces = {
                "nodeid", // required
                "documentid", // required
                "documenttype", "score1", "score2", "score3", "score4", "score5", "score6", "docsummary", "edited"};
            sSQL = api.sqlinteract.SQLModelUpdate.genSQLUpdate(sTableName, props, sArrFields_URLNameSpaces, sArrFields_DBNameSpaces,
                    iNumRequiredFields);

            if (!sSQL.equals("")) {
                try {
                    stmt = null;
                    stmt = dbc.createStatement();

                    // If no rows were returned, nodedocument row does not exist in the client
                    //   database.   It will need to be inserted.
                    if (stmt.executeUpdate(sSQL) == 0) {
                        TSEditDocProps tsedit = new TSEditDocProps();
                        throw tsedit.new TSNodeDocDoesNotExist();
                    }
                } catch (Exception e) {
                    throw e;
                } finally {
                    if (stmt != null) {
                        try {
                            stmt.close();
                        } catch (Exception e) {
                        }
                        stmt = null;
                    }
                }
            }
            out.println("<SUCCESS>Document modification successful!</SUCCESS>");
        } // Catch: if Document could not be updated, insert it along with the Node Doc.
        catch (TSDocDoesNotExist tsddne) {
            dbc.setAutoCommit(false);
            try {
                Document d = InsertDocument(props, dbc, out);

                sDocumentID = d.get("DOCUMENTID");
                props.put("documentid", sDocumentID);
                sDocumentID = (String) props.get("documentid");

                if (sDocumentID.equals("")) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
                }
                int rval = InsertNodeDocument(props, dbc, out);
                if (rval != 0) {
                    throw new TSException(rval);
                }
                dbc.commit();
            } catch (TSException tse) {
                dbc.rollback();
                EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
            }
        } // Catch: if NodeDocument could not be updated, insert it (assume: document already exists)
        catch (TSNodeDocDoesNotExist tsnddne) {
            try {
                int rval = InsertNodeDocument(props, dbc, out);
                if (rval != 0) {
                    throw new TSException(rval);
                }
            } catch (TSException tse) {
                EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
            }
        } // Catch exception
        catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }
            dbc.setAutoCommit(true);
        }
    }

    public static Document GetDocument(api.APIProps props, Connection dbc)
            throws Exception {
        String sDocURL = (String) props.get("docurl");
        String sSQL = Document.getSQL(dbc) + " where d.DocURL = '" + sDocURL + "'";

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();
            Document d = new Document(rs);

            api.Log.Log("GetDocument: " + sDocURL + " ID: " + d.get("DOCUMENTID"));
            return d;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            api.Log.LogError("failure: " + sSQL);
            return null;
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }
        }
    }

    public static Document InsertDocument(api.APIProps props, Connection dbc, PrintWriter out)
            throws Exception {

        Hashtable htFields = CustomFields.getFields("DOCUMENT", dbc);

        // Check to ensure that the NodeDocument table is writable
        if (Locked.NodeDocumentLocked(dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED);
        }

        // required
        String sGenreID = (String) props.get("genreid");
        String sDocTitle = (String) props.get("doctitle");
        String sDocURL = (String) props.get("docurl");
        String sGroupDN = (String) props.get("group");

        // security is optional - default to global access
        String sSecurity = (String) props.get("security");

        // All required parameters must be present in order to insert into the document table
        if ((sGenreID == null) || (sDocTitle == null) || (sDocURL == null)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }

        Statement stmt = null;

        String sSQL = "insert into document (documentid, filterstatus, ";
        String sSQL2 = "values (null, 2, ";

        Enumeration eX = props.keys();
        int loop = 0;
        while (eX.hasMoreElements()) {
            String sKey = (String) eX.nextElement();
            String sValue = (String) props.get(sKey);

            if ((sKey.toUpperCase().equals("GENREID")) && sValue.equals("0")) {
                sValue = "1";
            }
            if (sKey.toUpperCase().equals("DOCSUMMARY")) {
                sKey = "DOCUMENTSUMMARY";
            }

            if ((!sKey.toUpperCase().equals("SKEY")) && (!sKey.toUpperCase().equals("EMITXML")) && (!sKey.toUpperCase().equals("FN"))
                    && (!sKey.toUpperCase().equals("GROUP"))
                    && (!sKey.toUpperCase().equals("NODEID")) && (!sKey.toUpperCase().equals("SECURITY")) && (!sKey.toUpperCase().equals("SCORE1"))) {
                if (sKey.toLowerCase().equals("filepath")) {
                    sKey = "fulltext";
                }
                if (sValue.indexOf('&') != -1) {
                    sValue = sValue.replaceAll("&", "&'||'");
                }

                loop++;
                if (loop != 1) {
                    sSQL = sSQL + ", ";
                    sSQL2 = sSQL2 + ", ";
                }
                sSQL = sSQL + sKey;

                IndraField inf = (IndraField) htFields.get(sKey.toUpperCase());

                if (inf.getType().equals("DATE")) {
                    sSQL2 = sSQL2 + "to_date('" + sValue + "','YYYY-MM-DD HH24:MI:SS')";
                } else {
                    sSQL2 = sSQL2 + "'" + cleanBadCharacters(sValue) + "'";
                }
            }
        }
        sSQL = sSQL + ") ";
        sSQL2 = sSQL2 + ") ";

        try { // update the document table
            stmt = dbc.createStatement();
            //out.println("<SQL>"+sSQL+sSQL2+"</SQL>");
            if (stmt.executeUpdate(sSQL + sSQL2) == 0) {
                api.Log.Log("Insert document failed: " + sSQL + sSQL2);
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
            }
            stmt.close();
        } catch (Exception e) {
            api.Log.Log("Insert document failed (2): " + sSQL + sSQL2);
            throw e;
        }

        // get the current document id
        Document d = GetDocument(props, dbc);
        String sDocID = d.get("DOCUMENTID");
        if ((sDocID == null) || (sDocID.equals(""))) {
            api.Log.Log("Insert document returned nothing.");
            return null;
        }

        int iDocument = new Integer(sDocID).intValue();

        // NOTE!! For backwards compatibility reasons, we must observe the GroupDN argument
        if (sGroupDN != null) {
            SecurityGroup sg = api.security.DocumentSecurity.getSecurityByName(sGroupDN.toUpperCase());

            if (sg != null) {
                Vector v = new Vector();
                v.add(sg);
                InsertDocumentSecurity(iDocument, v, dbc, out);
                String hexmap = api.security.DocumentSecurity.getSecurity(v);
                d.set("DOCUMENTSECURITY", hexmap);
            }
        } else if (sSecurity != null) { // this is the new security code --
            String securityArr[] = sSecurity.split(",");
            Vector v = new Vector();

            for (int i = 0; i < securityArr.length; i++) {
                SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID(securityArr[i]);
                v.add(sg);
                String hexmap = api.security.DocumentSecurity.getSecurity(v);
                d.set("DOCUMENTSECURITY", hexmap);
            }

            InsertDocumentSecurity(Integer.parseInt(d.get("DOCUMENTID")), v, dbc, out);
        } else { // no security -- that's okay, we'll default to global access (1)
            Vector v = new Vector();

            SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID("0");
            v.add(sg);

            String hexmap = api.security.DocumentSecurity.getSecurity(v);
            d.set("DOCUMENTSECURITY", hexmap);

            InsertDocumentSecurity(Integer.parseInt(d.get("DOCUMENTID")), v, dbc, out);
        }

        return d;
    }

    public static int InsertIdracDocument(api.APIProps props, Connection dbc, PrintWriter out)
            throws Exception {
        // Check to ensure that the NodeDocument table is writable
        if (Locked.NodeDocumentLocked(dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED);
        }

        String sDocumentID = "0";

        // required
        String sGenreID = (String) props.get("genreid");
        String sDocTitle = (String) props.get("doctitle");
        String sDocURL = (String) props.get("docurl");
        String sAbstract = (String) props.get("documentsummary");
        String sVisible = (String) props.get("visible");

        if (sVisible == null) {
            sVisible = "1";
        } // default to visible for all documents

        if (sDocTitle.indexOf('&') != -1) {
            sDocTitle.replaceAll("&", "&'||'");
        }
        if (sAbstract.indexOf('&') != -1) {
            sAbstract.replaceAll("&", "&'||'");
        }

        Statement stmt = null;
        ResultSet rs = null;
        try {
            // optional
            String sBibliography = (String) props.get("bibliography", "");
            if (sBibliography.indexOf('&') != -1) {
                sBibliography.replaceAll("&", "&'||'");
            }
            String sFullText = (String) props.get("filepath", "");
            String sLanguage = (String) props.get("language", "");
            String sCountry = (String) props.get("country", "");
            String sIdracNumber = (String) props.get("idracnumber", "");
            String sOutdated = (String) props.get("outdated", "0");
            String sForm = (String) props.get("form", "0");
            String sGroupDN = (String) props.get("group");

            // optional dates
            // get default system date now --
            Calendar now = Calendar.getInstance();
            String sDate = "";
            int year = now.get(Calendar.YEAR);
            int month = now.get(Calendar.MONTH) + 1;
            int day = now.get(Calendar.DAY_OF_MONTH);

            // get today's date
            if (month > 9) {
                sDate = "" + month;
            } else {
                sDate = "0" + month;
            }
            if (day > 9) {
                sDate = sDate + "/" + day;
            } else {
                sDate = sDate + "/0" + day;
            }
            sDate = sDate + "/" + year;

            String sAdoption = (String) props.get("adoptiondate", sDate);
            String sPublication = (String) props.get("publicationdate", sDate);
            String sEntry = (String) props.get("entrydate", sDate);
            String sRevision = (String) props.get("revisiondate", sDate);
            String sSystem = (String) props.get("systemdate", sDate);

            // optional security information
            String sSecurity = (String) props.get("security");

            //sAbstract = sAbstract.replaceAll("'", "''");
            //sDocTitle = sDocTitle.replaceAll("'", "''");

            // All required parameters must be present in order to insert into the document table
            if ((sGenreID == null) || (sDocTitle == null) || (sAbstract == null) || (sDocURL == null)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }

            // First, get the next document identifier
            String sSQL = "select document_seq.nextval from dual";
            sDocumentID = "0";
            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                rs.next();
                sDocumentID = rs.getString(1);
            } catch (Exception e) {
                throw e;
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (Exception e) {
                    }
                    rs = null;
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (Exception e) {
                    }
                    stmt = null;
                }
            }

            dbc.setAutoCommit(false);

            PreparedStatement stmt1 = null;
            PreparedStatement stmt2 = null;
            try {
                // if the abstract is more than 4000 characters, split it into two fields
                if (sAbstract.length() > 8000) {
                    sSQL = "insert into document (filterstatus, documentid, genreid, doctitle, docurl, documentsummary, documentsummary2, "
                            + "documentsummary3, fulltext, visible) values (2, " + sDocumentID + ", " + sGenreID + ", '" + cleanBadCharacters(sDocTitle) + "', "
                            + " '" + sDocURL + "', ?, ?, ?, '" + sFullText + "', " + sVisible + ")";
                    stmt1 = dbc.prepareStatement(sSQL);
                    stmt1.setString(1, cleanBadCharacters(sAbstract.substring(0, 3999)));
                    stmt1.setString(2, cleanBadCharacters(sAbstract.substring(3999, 7999)));
                    stmt1.setString(3, cleanBadCharacters(sAbstract.substring(7999, sAbstract.length())));
                } else if (sAbstract.length() > 4000) {
                    sSQL = "insert into document (filterstatus, documentid, genreid, doctitle, docurl, documentsummary, documentsummary2, "
                            + "fulltext, visible) values (2, " + sDocumentID + ", " + sGenreID + ", '" + cleanBadCharacters(sDocTitle) + "', "
                            + " '" + sDocURL + "', ?, ?, '" + sFullText + "', " + sVisible + ")";
                    stmt1 = dbc.prepareStatement(sSQL);
                    stmt1.setString(1, cleanBadCharacters(sAbstract.substring(0, 3999)));
                    stmt1.setString(2, cleanBadCharacters(sAbstract.substring(3999, sAbstract.length())));
                } else {
                    sSQL = "insert into document (filterstatus, documentid, genreid, doctitle, docurl, documentsummary, "
                            + "fulltext, visible) values (2, " + sDocumentID + ", " + sGenreID + ", '" + cleanBadCharacters(sDocTitle) + "', "
                            + " '" + sDocURL + "', ?, '" + sFullText + "', " + sVisible + ")";
                    stmt1 = dbc.prepareStatement(sSQL);
                    stmt1.setString(1, cleanBadCharacters(sAbstract));
                }
                if (stmt1.executeUpdate() == 0) {
                    api.Log.LogError("Insert failed: " + sSQL);
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
                }

                sSQL = "insert into idracdocument (documentid, bibliography, language, country, "
                        + "idracnumber, adoptiondate, publicationdate, entrydate, revisiondate, systemdate, "
                        + "outdated, form) values (" + sDocumentID + ", ?, '" + sLanguage + "', "
                        + "'" + sCountry + "', '" + sIdracNumber + "', "
                        + "to_date('" + sAdoption + "', 'MM/DD/YY'), to_date('" + sPublication + "', 'MM/DD/YY'), "
                        + "to_date('" + sEntry + "', 'MM/DD/YY'), to_date('" + sRevision + "', 'MM/DD/YY'), "
                        + "to_date('" + sSystem + "', 'MM/DD/YY'), " + sOutdated + ", " + sForm + ") ";
                stmt2 = dbc.prepareStatement(sSQL);
                stmt2.setString(1, cleanBadCharacters(sBibliography) + " ");

                if (stmt2.executeUpdate() == 0) {
                    api.Log.LogError("Insert failed: " + sSQL);
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
                }

                dbc.commit();
            } catch (Exception e) {
                dbc.rollback();
                throw e;
            } finally {
                if (stmt1 != null) {
                    stmt1.close();
                    stmt1 = null;
                }
                if (stmt2 != null) {
                    stmt2.close();
                    stmt2 = null;
                }
            }

            dbc.setAutoCommit(true);

            // NOTE!! For backwards compatibility reasons, we must observe the GroupDN argument
            if (sGroupDN != null) {
                InsertDocumentSecurity(Integer.parseInt(sDocumentID), sGroupDN, dbc, out);
            } else if (sSecurity != null) { // this is the new security code --
                String securityArr[] = sSecurity.split(",");
                Vector v = new Vector();

                for (int i = 0; i < securityArr.length; i++) {
                    SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID(securityArr[i]);
                    v.add(sg);
                }

                InsertDocumentSecurity(Integer.parseInt(sDocumentID), v, dbc, out);
            } else { // no security -- that's okay, we'll default to global access (1)
                Vector v = new Vector();

                SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID("0");
                v.add(sg);

                InsertDocumentSecurity(Integer.parseInt(sDocumentID), v, dbc, out);
            }
        } catch (TSException tse) {
            dbc.rollback();
            api.Log.LogError(tse);
            return tse.getTSReturnCode();
        } catch (Exception e) {
            dbc.rollback();
            api.Log.LogError(e);
            return -1;
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }
            dbc.commit();
            dbc.setAutoCommit(true);
        }

        return Integer.parseInt(sDocumentID);
    }

    public static boolean UpdateIdracDocument(int DocumentID, api.APIProps props, Connection dbc, PrintWriter out)
            throws Exception {
        // Check to ensure that the NodeDocument table is writable
        if (Locked.NodeDocumentLocked(dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED);
        }

        PreparedStatement pstmt = null;
        try {
            // optional
            String sDocURL = (String) props.get("docurl");
            String sGenreID = (String) props.get("genreid");
            String sDocTitle = (String) props.get("doctitle");
            String sBibliography = (String) props.get("bibliography");
            String sAbstract = (String) props.get("documentsummary");
            String sLanguage = (String) props.get("language");
            String sCountry = (String) props.get("country");
            String sIdracNumber = (String) props.get("idracnumber");
            String sOutdated = (String) props.get("outdated");
            String sForm = (String) props.get("form");
            String sGroupDN = (String) props.get("group");
            String sVisible = (String) props.get("visible");

            // optional dates
            String sAdoption = (String) props.get("adoptiondate");
            String sPublication = (String) props.get("publicationdate");
            String sEntry = (String) props.get("entrydate");
            String sRevision = (String) props.get("revisiondate");
            String sSystem = (String) props.get("systemdate");

            // optional - change security
            String sSecurity = (String) props.get("security");

            //sAbstract = sAbstract.replaceAll("'", "''");
            //sDocTitle = sDocTitle.replaceAll("'", "''");

            // update both document and idracdocument tables
            dbc.setAutoCommit(false);

            // only update doc table if one of the doc fields changed
            if ((sDocURL != null) || (sGenreID != null) || (sVisible != null)
                    || (sDocTitle != null) || (sAbstract != null)) {

                String sSQL = "update document set FilterStatus = 2, DocumentID = " + DocumentID;
                if (sDocURL != null) {
                    sSQL = sSQL + ", DocURL = '" + sDocURL + "'";
                }
                if (sGenreID != null) {
                    sSQL = sSQL + ", GenreID = " + sGenreID;
                }
                if (sVisible != null) {
                    sSQL = sSQL + ", Visible = " + sVisible;
                }
                if (sDocTitle != null) {
                    if (sDocTitle.indexOf('&') != -1) {
                        sDocTitle.replaceAll("&", "&'||'");
                    }
                    sSQL = sSQL + ", DocTitle = '" + cleanBadCharacters(sDocTitle) + "'";
                }
                if (sAbstract != null) {
                    if (sAbstract.indexOf('&') != -1) {
                        sAbstract.replaceAll("&", "&'||'");
                    }

                    if (sAbstract.length() > 8000) {
                        sSQL = sSQL + ", DocumentSummary = ?, DocumentSummary2 = ?, DocumentSummary3 = ?";
                    } else if (sAbstract.length() > 4000) {
                        sSQL = sSQL + ", DocumentSummary = ?, DocumentSummary2 = ?";
                    } else {
                        sSQL = sSQL + ", DocumentSummary = ?";
                    }
                }

                sSQL = sSQL + " where DocumentID = " + DocumentID;

                pstmt = dbc.prepareStatement(sSQL);

                if (sAbstract.length() > 8000) {
                    pstmt.setString(1, cleanBadCharacters(sAbstract.substring(0, 3999)));
                    pstmt.setString(2, cleanBadCharacters(sAbstract.substring(3999, 7999)));
                    pstmt.setString(3, cleanBadCharacters(sAbstract.substring(7999, sAbstract.length())));
                } else if (sAbstract.length() > 4000) {
                    pstmt.setString(1, cleanBadCharacters(sAbstract.substring(0, 3999)));
                    pstmt.setString(2, cleanBadCharacters(sAbstract.substring(3999, sAbstract.length())));
                } else {
                    pstmt.setString(1, cleanBadCharacters(sAbstract));
                }
                if (pstmt.executeUpdate() == 0) {
                    dbc.rollback();
                    dbc.setAutoCommit(true);
                    return false;
                }
            }

            if ((sBibliography != null) || (sLanguage != null) || (sCountry != null)
                    || (sIdracNumber != null) || (sAdoption != null) || (sPublication != null)
                    || (sEntry != null) || (sRevision != null) || (sSystem != null)
                    || (sOutdated != null) || (sForm != null)) {

                String sSQL = "update IdracDocument set DocumentID = " + DocumentID;
                if (sBibliography != null) {
                    if (sBibliography.indexOf('&') != -1) {
                        sBibliography.replaceAll("&", "&'||'");
                    }
                    sSQL = sSQL + ", Bibliography = ?";
                }
                if (sLanguage != null) {
                    sSQL = sSQL + ", Language = '" + sLanguage + "'";
                }
                if (sCountry != null) {
                    sSQL = sSQL + ", Country = '" + sCountry + "'";
                }
                if (sIdracNumber != null) {
                    sSQL = sSQL + ", IdracNumber = '" + sIdracNumber + "'";
                }
                if (sOutdated != null) {
                    sSQL = sSQL + ", Outdated = " + sOutdated;
                }
                if (sForm != null) {
                    sSQL = sSQL + ", Form = " + sForm;
                }

                if (sAdoption != null) {
                    sSQL = sSQL + ", AdoptionDate = to_date('" + sAdoption + "', 'MM/DD/YY')";
                }
                if (sPublication != null) {
                    sSQL = sSQL + ", PublicationDate = to_date('" + sPublication + "', 'MM/DD/YY')";
                }
                if (sEntry != null) {
                    sSQL = sSQL + ", EntryDate = to_date('" + sEntry + "', 'MM/DD/YY')";
                }
                if (sRevision != null) {
                    sSQL = sSQL + ", RevisionDate = to_date('" + sRevision + "', 'MM/DD/YY')";
                }
                if (sSystem != null) {
                    sSQL = sSQL + ", SystemDate = to_date('" + sSystem + "', 'MM/DD/YY')";
                }

                sSQL = sSQL + " where DocumentID = " + DocumentID;
                pstmt = dbc.prepareStatement(sSQL);

                if (sBibliography != null) {
                    pstmt.setString(1, cleanBadCharacters(sBibliography));
                }
                if (pstmt.executeUpdate() == 0) {
                    dbc.rollback();
                    dbc.setAutoCommit(true);
                    return false;
                }
            }

            dbc.commit();
            dbc.setAutoCommit(true);

            // NOTE!! For backwards compatibility reasons, we must observe the GroupDN argument
            if (sGroupDN != null) {
                InsertDocumentSecurity(DocumentID, sGroupDN, dbc, out);
            } else if (sSecurity != null) { // this is the new security code --
                String securityArr[] = sSecurity.split(",");
                Vector v = new Vector();

                for (int i = 0; i < securityArr.length; i++) {
                    SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID(securityArr[i]);
                    v.add(sg);
                }

                InsertDocumentSecurity(DocumentID, v, dbc, out);
            }

            return true;
        } catch (Exception e) {
            dbc.rollback();
            api.Log.LogError(e);
            return false;
        } finally {
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
            dbc.setAutoCommit(true);
        }
    }

    public static boolean UpdateDocument(String DocumentID, api.APIProps props, Connection dbc, PrintWriter out)
            throws Exception {

        String sGroupDN = (String) props.get("group");

        // Check to ensure that the NodeDocument table is writable
        if (Locked.NodeDocumentLocked(dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED);
        }

        Statement stmt = null;

        // get the list of valid fields that can be updated
        Hashtable htFields = CustomFields.getFields("DOCUMENT", dbc);

        String sSQL = "update document set FilterStatus = 2, updateddate = sysdate, ";
        Enumeration eX = props.keys();
        int loop = 0;
        while (eX.hasMoreElements()) {
            String sKey = (String) eX.nextElement();
            String sValue = (String) props.get(sKey);

            if ((htFields.containsKey(sKey.toUpperCase())) && (!sKey.toUpperCase().equals("FILTERSTATUS"))
                    && (!sKey.toUpperCase().equals("GROUP")) && (!sKey.toUpperCase().equals("UPDATEDDATE"))) {

                IndraField inf = (IndraField) htFields.get(sKey.toUpperCase());

                if (sValue.indexOf('&') != -1) {
                    sValue = sValue.replaceAll("&", "&'||'");
                }
                loop++;
                if (loop != 1) {
                    sSQL = sSQL + ", ";
                }

                if (inf.getType().equals("DATE")) {
                    sSQL = sSQL + sKey + " = to_date('" + sValue + "','YYYY-MM-DD HH24:MI:SS')";
                } else {
                    sSQL = sSQL + sKey + " = '" + cleanBadCharacters(sValue) + "'";
                }
            }
        }
        sSQL = sSQL + " where DocumentID = " + DocumentID;
        //out.println("<SQL>"+sSQL+"</SQL>");

        try { // update the document table
            stmt = dbc.createStatement();
            //out.println("<SQL>"+sSQL+"</SQL>");
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            return false;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        // update security if it was specified (optional)
        String sSecurity = (String) props.get("security");

        if (sGroupDN != null) {
            SecurityGroup sg = api.security.DocumentSecurity.getSecurityByName(sGroupDN.toUpperCase());

            if (sg != null) {
                Vector v = new Vector();
                v.add(sg);
                InsertDocumentSecurity(Integer.parseInt(DocumentID), v, dbc, out);
            }
        } else if (sSecurity != null) { // this is the new security code --
            String securityArr[] = sSecurity.split(",");
            Vector v = new Vector();

            for (int i = 0; i < securityArr.length; i++) {
                SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID(securityArr[i]);
                v.add(sg);
            }

            InsertDocumentSecurity(Integer.parseInt(DocumentID), v, dbc, out);
        }

        return true;
    }

    // idrac only supports one security group per document
    // EDIT 4/29/04 -- we must now support multiple security groups per document
    public static boolean InsertDocumentSecurity(int DocumentID, String GroupDN, Connection dbc, PrintWriter out)
            throws Exception {

        SecurityGroup sg = api.security.DocumentSecurity.getSecurityByName(GroupDN.toUpperCase());
        if (sg == null) {
            return false;
        }

        Vector v = new Vector();
        v.add(sg);

        return InsertDocumentSecurity(DocumentID, v, dbc, out);
    }

    public static boolean InsertDocumentSecurity(int DocumentID, int SecurityID, Connection dbc, PrintWriter out)
            throws Exception {

        SecurityGroup sg = api.security.DocumentSecurity.getSecurityByID(SecurityID + "");
        Vector v = new Vector();
        v.add(sg);

        return InsertDocumentSecurity(DocumentID, v, dbc, out);
    }

    public static boolean InsertDocumentSecurity(int DocumentID, Vector v, Connection dbc, PrintWriter out) throws Exception {
        String hexmap = api.security.DocumentSecurity.getSecurity(v);
        Statement stmt = null;
        String sSQL = "update document set documentsecurity = '" + hexmap + "' where DocumentID = " + DocumentID;

        try { // insert security permissions
            stmt = dbc.createStatement();
            if (stmt.executeUpdate(sSQL) == 0) {
                return false;
            }
        } catch (Exception e) {
            return false;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        return true;
    }

    // insert document security given document identifier, security identifier
    public static boolean UpdateDocumentDate(String DocumentID, Connection dbc) throws Exception {
        // Check to ensure that the NodeDocument table is writable
        if (Locked.NodeDocumentLocked(dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED);
        }

        Statement stmt = null;
        ResultSet rs = null;
        String sSQL = "update document set updateddate = SYSDATE where DocumentID = " + DocumentID;

        try { // insert security permissions
            stmt = dbc.createStatement();
            if (stmt.executeUpdate(sSQL) == 0) {
                return false;
            }
        } catch (Exception e) {
            return false;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        return true;
    }

    // ********************************************************************************
    private static String cleanBadCharacters(String s) {
        if (s == null) {
            return s;
        }
        String sNew = new String();
        for (int k = 0; k < s.length(); k++) {
            int ichar = (int) s.charAt(k);
            if (ichar > 300) {
                api.Log.Log("Warning! Found illegal character with ascii value: " + ichar);
            } else {
                sNew = sNew + s.charAt(k);
            }
        }

        return sNew;
    }

    /*
     * SQL> describe nodedocument;
     * Name Null? Type
     * ----------------------------------------- -------- ----------------------------
     * NODEID NOT NULL NUMBER(12)
     * DOCUMENTID NOT NULL NUMBER(9)
     * DOCUMENTTYPE NUMBER(2)
     * SCORE1 NUMBER(63)
     * SCORE2 NUMBER(63)
     * SCORE3 NUMBER(63)
     * SCORE4 NUMBER(63)
     * SCORE5 NUMBER(63)
     * SCORE6 NUMBER(63)
     * DOCSUMMARY VARCHAR2(1000)
     * DATESCORED DATE
     */
    public static int InsertNodeDocument(api.APIProps props, Connection dbc, PrintWriter out)
            throws Exception {
        // Check to ensure that the NodeDocument table is writable
        if (Locked.NodeDocumentLocked(dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED);
        }

        String sDocumentID = (String) props.get("documentid");
        String sNodeID = (String) props.get("nodeid");
        String sDocumentType = (String) props.get("documenttype");
        String sScore1 = (String) props.get("score1");
        String sScore2 = (String) props.get("score2");
        String sScore3 = (String) props.get("score3");
        String sScore4 = (String) props.get("score4");
        String sScore5 = (String) props.get("score5");
        String sScore6 = (String) props.get("score6");
        String sDocSummary = (String) props.get("docsummary");
        String sIndex = (String) props.get("index", "1");

        // NodeID, DocumentID, DocSummary are all required fields
        if ((sNodeID == null) || (sDocumentID == null) || (sDocSummary == null)) {
            return EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS;
        }
        // for batch processing, added document update date
        if (!UpdateDocumentDate(sDocumentID, dbc)) {
            return 0;
        }

        // NodeID and DocumentID are required, all others are optional
        // Set the default values for parameters that were not supplied.
        if (sDocumentType == null) {
            sDocumentType = "1";
        }
        if (sScore1 == null) {
            sScore1 = "100.0";
        }
        if (sScore2 == null) {
            sScore2 = "100.0";
        }
        if (sScore3 == null) {
            sScore3 = "100.0";
        }
        if (sScore4 == null) {
            sScore4 = "100.0";
        }
        if (sScore5 == null) {
            sScore5 = "100.0";
        }
        if (sScore6 == null) {
            sScore6 = "100.0";
        }

        String sSQL = "insert into nodedocument (nodeid, documentid, documenttype, score1,  "
                + "score2, score3, score4, score5, score6, docsummary, indexwithinnode, edited) values ("
                + sNodeID + "," + sDocumentID + "," + sDocumentType + "," + sScore1 + "," + sScore2 + ","
                + sScore3 + "," + sScore4 + "," + sScore5 + "," + sScore6 + ", '" + cleanBadCharacters(sDocSummary) + "', '" + sIndex + "', 1)";
        Statement stmt = null;

        try {
            stmt = dbc.createStatement();
            if (stmt.executeUpdate(sSQL) == 0) {
                return EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION;
            }
            out.println("<SUCCESS>Insert successful</SUCCESS>");
        } catch (Exception e) {
            return EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        return 0;
    }

    public class TSDocDoesNotExist extends Exception {
    }

    public class TSNodeDocDoesNotExist extends Exception {
    }
}
