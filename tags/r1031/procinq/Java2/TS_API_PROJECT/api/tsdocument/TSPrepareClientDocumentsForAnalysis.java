package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import api.results.*;

/**
 * Select <i>count</i> documents from topic <i>nodeid</i> and place them into the ROC validation queue for review.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  NodeID  Unique node identifier.
 *  @param  Count   Number of documents from this topic to draw for analysis.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSPrepareClientDocumentsForAnalysis&SKEY=922482&NodeID=29491&Count=10

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>{Count} documents updated.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSPrepareClientDocumentsForAnalysis
{
	// TSPrepareClientDocumentsForAnalysis
	//
	// Take every Nth document from the client for the given node and update it in the
	//  client database, where N = TOTAL DOCS / DOCS REQUESTED ..
	
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sCount = (String) props.get ("Count", true);
		String bRetrieveFromClient = (String) props.get("FromClient");
		
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;

			ResultSet rs = null;
			Statement stmt = null;
			int iCorpusId = -1;
	
			try {
				stmt = dbc.createStatement();	
				rs = stmt.executeQuery (sSQL);
				rs.next();
			
				iCorpusId = rs.getInt(1);
			} catch ( SQLException qe) { 
				EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
			} finally { rs.close(); stmt.close(); }
			
			// verify authorization to do this..
			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			// Documents will be stored in this vector later..
			Vector vDocuments = new Vector();
			sSQL = " select D.DocumentID, D.DocTitle, D.DocURL, N.DocSummary, "+
					  " N.Score1, N.Score2, N.Score3, N.Score4, N.Score5, N.Score6, "+
					  " to_char(N.DateScored, 'MM/DD/YYYY HH24:MI:SS'), D.GenreID from Document D, "+ 
					  " NodeDocument N where D.DocumentId = N.DocumentID AND N.NodeID = "+sNodeID;
			
			try {
				stmt = dbc.createStatement();
				rs = stmt.executeQuery (sSQL);			

			} 
			catch ( SQLException qe) { 
				EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
			} 
			
			int loop = 0;
			while (rs.next()) {
				String sDocID = rs.getString(1);
				String sDocTitle = rs.getString(2);
				String sDocURL = rs.getString(3);
				String sDocSummary = rs.getString(4);
				String fScore1 = rs.getString(5);		
				String fScore2 = rs.getString(6);
				String fScore3 = rs.getString(7);	
				String fScore4 = rs.getString(8);		
				String fScore5 = rs.getString(9);	
				String fScore6 = rs.getString(10);			
				String sDateScored = rs.getString(11);				
				String sGenreID = rs.getString(12);

                if (sDocTitle == null) { sDocTitle = "No title."; }
                if (sDocSummary == null) { sDocSummary = "None."; }

				NodeDocumentResult ndr
					= new NodeDocumentResult(sNodeID, sDocID, sGenreID, 
											 new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocTitle,"'","''")),
											 new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocURL,"'","''")),
											 new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocSummary,"'","''")),
											 fScore1, fScore2, fScore3, 
											 fScore4, fScore5, fScore6,
											 sDateScored, "0");
				vDocuments.addElement(ndr);
			}

			// MATH TIME.  We don't want all of the documents, only every Nth.
			//  So we must first calculate N
			float N = new Float(vDocuments.size()).floatValue() / new Float(sCount).floatValue();
			if (N < 1.0) { N = new Float(1.0).floatValue(); }
			float Ncounter = N; int iUpdateCounter = 0;
			
			for (int i = 1; i <= vDocuments.size(); i++) {
				int iN = java.lang.Math.round (Ncounter);
				if (i == iN) { 
					try { 
						UpdateClient(dbc, (NodeDocumentResult) vDocuments.elementAt(i-1), out);
						iUpdateCounter++; Ncounter = Ncounter + N;
					} catch (Exception e) {
						api.Log.LogError("Error updating repository type for ROC", e);
						throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
					}
				}
			}

			if (iUpdateCounter > 0) { 
				out.println("<SUCCESS>"+iUpdateCounter+" documents updated.</SUCCESS>");
			}
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
	public static boolean UpdateClient (Connection dbc, NodeDocumentResult ndr, PrintWriter out) 
	throws Exception {

		// First: Insert into the Document table
		String sSQL = "update document set RepositoryID = 1 where DocumentID = "+ndr.GetDocID();
		Statement stmt = dbc.createStatement();	
		stmt.executeUpdate (sSQL); 
	    stmt.close();
		
		// Finally: Insert into the ROCValidation table
		sSQL = "insert into ROCValidation values ("+ndr.GetNodeID()+", "+ndr.GetDocID()+", 0)";
		stmt = dbc.createStatement();	
		stmt.executeUpdate (sSQL); 
	    stmt.close();
		
		return true;
	}
}
