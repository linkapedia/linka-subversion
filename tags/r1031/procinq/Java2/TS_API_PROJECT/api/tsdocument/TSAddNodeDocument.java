package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;

import com.iw.system.User;
import com.indraweb.execution.Session;
import api.Log;

/**
 * Adds a topic-document relationship to the ITS server.  Typically done through a classification request, but
 *  may also be done through this API.  This function requires <i>administrative privledges<i>.
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID  Unique document identifier.
 *  @param  NodeID  Unique node identifier.
 *  @param  DocSummary  (optional) A brief abstract of 4000 characters or less.
 *  @param  Score1 (optional)   Node bucket score (star count)
 *  @param  Score2 (optional)   Floating point representation of classifier 2 score value
 *  @param  Score3 (optional)   Floating point representation of classifier 3 score value
 *  @param  Score4 (optional)   Floating point representation of classifier 4 score value
 *  @param  Score5 (optional)   Floating point representation of classifier 5 score value
 *  @param  Score6 (optional)   Floating point representation of classifier 6 score value
 *  @param  Index (optional)    Document index within topic results
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */

public class TSAddNodeDocument
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Statement stmt = null;

        try {
            // Ensure user is a member of the admin group
            if ((u == null) || (!u.IsMember(out))) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // required fields:
            String sNodeID = (String) props.get ("NodeID", true);
            String sDocumentID = (String) props.get ("DocumentID", true);
            String sDocSum = (String) props.get ("docsummary");

            if (sDocSum == null) { props.put("docsummary", "none"); }

            if (api.tsdocument.TSEditDocProps.InsertNodeDocument(props, dbc, out) == 0) {
                out.println("<SUCCESS>Node document relationship added successfully</SUCCESS>");
            }

		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { if (stmt != null) { stmt.close(); }}
	}
}
