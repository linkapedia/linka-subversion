package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Corpus;
import api.results.*;

/**
 * Get star associations for a document in all nodes.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID  Unique document identifier.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSGetDocumentStars&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetDocumentStars
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

		String sDocumentID = (String) props.get ("DocumentID", true);

		try {
			// Select all node document relationships from the database 
			String sSQL = " select D.NodeID,D.Score1,N.NodeTitle,N.CorpusID from NodeDocument D, "+
						  " Node N where N.NodeID = D.NodeID and "+
						  " D.DocumentID = "+sDocumentID+" order by D.Score1";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			// hashtable of Corpora and Nodes to avoid redundancy
			Hashtable htCorpora = new Hashtable();
			Hashtable htNodes = new Hashtable();
			
			int loop = 0;
			while ( rs.next() ) {
				String sNodeID = rs.getString(1);
                String sScore = rs.getString(2);
                String sTitle = rs.getString(3);
				String sCorpusID = rs.getString(4);
				
				// if the total score (stars) is better than zero, display it
				if (new Integer(sScore).intValue() > 0) {
					loop = loop + 1;
					if (loop == 1) { out.println("<NODEDOCUMENTS>"); }
				
					out.println("<NODEDOCUMENT>");
					out.println("   <DOCUMENTID>"+sDocumentID+"</DOCUMENTID>");
					out.println("   <NODEID>"+sNodeID+"</NODEID>");
                    out.println("   <CORPUSID>"+sCorpusID+"</CORPUSID>");
					out.println("   <NODETITLE>"+sTitle+"</NODETITLE>");
					out.println("   <SCORE>"+sScore+"</SCORE>");
					out.println("</NODEDOCUMENT>");
				}
			}

			rs.close();
		    stmt.close();
		
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No information found.</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			} else { out.println("</NODEDOCUMENTS>"); }
			out.println("<NUMRESULTS>"+loop+"</NUMRESULTS>");
		}
		
		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
	
	public static String GetCorpusIDfromNodes (String sNodeID, Connection dbc) 
	throws Exception {
		String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
		Statement stmt = dbc.createStatement();	
		ResultSet rs = stmt.executeQuery (sSQL);
		rs.next();
			
		String sCorpusID = rs.getString(1);
		rs.close(); stmt.close();

		return sCorpusID;
	}

	public static Corpus GetCorpusFromCorpusID (String sCorpusID, Connection dbc)
	throws Exception {
		Corpus c = null;
		
		String sSQL = " select CorpusID, Corpus_Name from Corpus where CorpusActive != -1";
		Statement stmt = dbc.createStatement();	
		ResultSet rs = stmt.executeQuery (sSQL);

		int loop = 0;
		rs.next();
		
		c = new Corpus();

        c.setID(rs.getString(1));
        c.setName(rs.getString(2));

		return c;
	}
}
