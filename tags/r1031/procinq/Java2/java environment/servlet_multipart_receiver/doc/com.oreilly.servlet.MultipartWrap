<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<!--NewPage-->
<html>
<head>
<!-- Generated by javadoc on Tue Jun 19 18:02:26 PDT 2001 -->
<title>
  Class com.oreilly.servlet.MultipartWrapper
</title>
</head>
<body>
<a name="_top_"></a>
<pre>
<a href="packages.html">All Packages</a>  <a href="tree.html">Class Hierarchy</a>  <a href="Package-com.oreilly.servlet.html">This Package</a>  <a href="com.oreilly.servlet.MultipartResponse.html#_top_">Previous</a>  <a href="com.oreilly.servlet.ParameterParser.html#_top_">Next</a>  <a href="AllNames.html">Index</a></pre>
<hr>
<h1>
  Class com.oreilly.servlet.MultipartWrapper
</h1>
<pre>
java.lang.Object
   |
   +----javax.servlet.ServletRequestWrapper
           |
           +----javax.servlet.http.HttpServletRequestWrapper
                   |
                   +----com.oreilly.servlet.MultipartWrapper
</pre>
<hr>
<dl>
  <dt> public class <b>MultipartWrapper</b>
  <dt> extends HttpServletRequestWrapper
</dl>
A request wrapper to support MultipartFilter.  
 The filter capability requires Servlet API 2.3.
 <p>
 See Jason Hunter's June 2001 article in JavaWorld for a full explanation of 
 the class usage.
<p>
<dl>
  <dt> <b>Version:</b>
  <dd> 1.0, 2001/06/19
  <dt> <b>Author:</b>
  <dd> <b>Jason Hunter</b>, Copyright &#169; 2001
</dl>
<hr>
<a name="index"></a>
<h2>
  <img src="images/constructor-index.gif" width=275 height=38 alt="Constructor Index">
</h2>
<dl>
  <dt> <img src="images/yellow-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#MultipartWrapper(javax.servlet.http.HttpServletRequest, java.lang.String)"><b>MultipartWrapper</b></a>(HttpServletRequest, String)
  <dd> 
</dl>
<h2>
  <img src="images/method-index.gif" width=207 height=38 alt="Method Index">
</h2>
<dl>
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getContentType(java.lang.String)"><b>getContentType</b></a>(String)
  <dd> 
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getFile(java.lang.String)"><b>getFile</b></a>(String)
  <dd> 
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getFileNames()"><b>getFileNames</b></a>()
  <dd> 
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getFilesystemName(java.lang.String)"><b>getFilesystemName</b></a>(String)
  <dd> 
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getParameter(java.lang.String)"><b>getParameter</b></a>(String)
  <dd> 
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getParameterMap()"><b>getParameterMap</b></a>()
  <dd> 
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getParameterNames()"><b>getParameterNames</b></a>()
  <dd> 
  <dt> <img src="images/red-ball-small.gif" width=6 height=6 alt=" o ">
	<a href="#getParameterValues(java.lang.String)"><b>getParameterValues</b></a>(String)
  <dd> 
</dl>
<a name="constructors"></a>
<h2>
  <img src="images/constructors.gif" width=231 height=38 alt="Constructors">
</h2>
<a name="MultipartWrapper"></a>
<a name="MultipartWrapper(javax.servlet.http.HttpServletRequest, java.lang.String)"><img src="images/yellow-ball.gif" width=12 height=12 alt=" o "></a>
<b>MultipartWrapper</b>
<pre>
 public MultipartWrapper(HttpServletRequest req,
                         String dir) throws IOException
</pre>
<a name="methods"></a>
<h2>
  <img src="images/methods.gif" width=151 height=38 alt="Methods">
</h2>
<a name="getParameterNames()"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getParameterNames"><b>getParameterNames</b></a>
<pre>
 public Enumeration getParameterNames()
</pre>
<dl>
  <dd><dl>
    <dt> <b>Overrides:</b>
    <dd> <a href="javax.servlet.ServletRequestWrapper.html#getParameterNames()">getParameterNames</a> in class ServletRequestWrapper
  </dl></dd>
</dl>
<a name="getParameter(java.lang.String)"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getParameter"><b>getParameter</b></a>
<pre>
 public String getParameter(String name)
</pre>
<dl>
  <dd><dl>
    <dt> <b>Overrides:</b>
    <dd> <a href="javax.servlet.ServletRequestWrapper.html#getParameter(java.lang.String)">getParameter</a> in class ServletRequestWrapper
  </dl></dd>
</dl>
<a name="getParameterValues(java.lang.String)"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getParameterValues"><b>getParameterValues</b></a>
<pre>
 public String[] getParameterValues(String name)
</pre>
<dl>
  <dd><dl>
    <dt> <b>Overrides:</b>
    <dd> <a href="javax.servlet.ServletRequestWrapper.html#getParameterValues(java.lang.String)">getParameterValues</a> in class ServletRequestWrapper
  </dl></dd>
</dl>
<a name="getParameterMap()"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getParameterMap"><b>getParameterMap</b></a>
<pre>
 public Map getParameterMap()
</pre>
<dl>
  <dd><dl>
    <dt> <b>Overrides:</b>
    <dd> <a href="javax.servlet.ServletRequestWrapper.html#getParameterMap()">getParameterMap</a> in class ServletRequestWrapper
  </dl></dd>
</dl>
<a name="getFileNames()"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getFileNames"><b>getFileNames</b></a>
<pre>
 public Enumeration getFileNames()
</pre>
<a name="getFilesystemName(java.lang.String)"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getFilesystemName"><b>getFilesystemName</b></a>
<pre>
 public String getFilesystemName(String name)
</pre>
<a name="getContentType(java.lang.String)"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getContentType"><b>getContentType</b></a>
<pre>
 public String getContentType(String name)
</pre>
<a name="getFile(java.lang.String)"><img src="images/red-ball.gif" width=12 height=12 alt=" o "></a>
<a name="getFile"><b>getFile</b></a>
<pre>
 public File getFile(String name)
</pre>
<hr>
<pre>
<a href="packages.html">All Packages</a>  <a href="tree.html">Class Hierarchy</a>  <a href="Package-com.oreilly.servlet.html">This Package</a>  <a href="com.oreilly.servlet.MultipartResponse.html#_top_">Previous</a>  <a href="com.oreilly.servlet.ParameterParser.html#_top_">Next</a>  <a href="AllNames.html">Index</a></pre>
</body>
</html>
