package com.iw.system;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.Date;

// this object represents the instance of a single notification e-mail to be sent
public class NotificationEmail {
    // private variables
    private String Text = ""; // HTML text to be printed
    private boolean bResultsFound = false; // results found yet? if no, do not send e-mail
    private Hashtable htVariables = new Hashtable(); // variables to be replaced

    // public variables
    public String EmailAddress = ""; // Address where e-mail is to be sent
    public String TemplateDir = "C:\\templates\\"; // directory where templates are located

 	// Constructor method for Notification E-mail
	public NotificationEmail (String Email) {
        EmailAddress = Email;
        WriteHeader();
	}
	public NotificationEmail (String Email, String TemplateDirectory) {
        EmailAddress = Email; TemplateDir = TemplateDirectory;
        WriteHeader();
	}

    // accessor function for ResultsFound -- have we found any results?
    public boolean HasResults() { return bResultsFound; }

    // set hash variables for read and replace
    public Hashtable SetHash(Hashtable ht) { htVariables = ht; return htVariables; }

    // given a VectorTree set of CQL document results and the alert name,
    // write this piece into the TEXT field
    public void WriteResults(Vector vDocuments, Alert a) {
        if (vDocuments != null) {
            bResultsFound = true;

            System.out.println("Alert: "+a.getName()+" found "+vDocuments.size()+" results.");
            htVariables.put("ALERTNAME", a.getName());
            htVariables.put("QUERY", a.getQuery());
            ReadWriteAndReplace("email-results-head.tpl");

            Enumeration eV = vDocuments.elements();
            while (eV.hasMoreElements()) {
                Object o;
                try {
                    o = eV.nextElement();
                    WriteResult(o);
                } catch (Exception e) {
                    System.out.println("Warning: Results could not be written properly.");
                    e.printStackTrace(System.out);
                    return;
                }
            }

            ReadWriteAndReplace("email-results-foot.tpl");
        }
    }

    // write header / footer of the e-mail into the TEXT field
    public void WriteHeader() {
        htVariables.put("DATE", (String) new Date().toString());
        ReadWriteAndReplace("email-header.tpl");
    }
    public void WriteFooter() { ReadWriteAndReplace("email-footer.tpl"); }

    public void WriteNoResultsFound (String AlertName) {
        htVariables.put("ALERTNAME", AlertName);
        ReadWriteAndReplace("email-noresults.tpl");
    }

    // write result
    //  <DOCUMENTID>, <GENREID>, <DOCTITLE>, <DOCURL>, <DOCUMENTSUMMARY>, <BIBLIOGRAPHY>,
    //  <LANGUAGE>, <COUNTRY>, <IDRACNUMBER>, <ADOPTIONDATE>, <PUBLICATIONDATE>, <ENTRYDATE>,
    //  <REVISIONDATE>, <SYSTEMDATE>, <SYSTEMDATE2>, <OUTDATED>, <FORM>
    public void WriteResult(Object o) {
        try {
            if (o instanceof NodeDocument) htVariables = ((NodeDocument) o).htnd;
            else if (o instanceof Document) htVariables = ((Document) o).htd;
            else if (o instanceof Node) htVariables = ((Node) o).htn;

            htVariables.put("DATE", (String) new Date().toString());
        } catch (Exception e) { e.printStackTrace(System.out); }

        ReadWriteAndReplace("email-result.tpl");
    }

    // debugging purposes: write template output to a file for verification
    public void WriteToFile (String AlertName) {
        String sFilename = "C:\\Notification\\"+AlertName+".html";
        System.out.println("Writing to file: "+sFilename);

        try {
          FileOutputStream fos = new FileOutputStream(new File(sFilename));
          fos.write(Text.getBytes());
          fos.close();
        } catch (Exception e) {}
    }

    public void SendEmail (String FullName, String Email) {
        SendEmail(FullName, Email, "ProcinQ Alert", "alert@mycompany.com", "ProcinQ Alert", "127.0.0.1");
    }
    public void SendEmail (String FullName, String Email, String FromName,
                           String FromEmail, String Subject, String MailServer) {
        try {
            SMTP smtp = new SMTP(MailServer);
            smtp.sendMail(FromName, FromEmail, FullName, Email, Subject, Text);
        } catch (Exception e) {
            System.out.println("Attempt to e-mail: "+Email+" failed.");
            e.printStackTrace(System.out);
        }
    }

    // Read the template file, replace variables, and write to standard out
	private void ReadWriteAndReplace (String sFilename) {
		String sTemplateFile = TemplateDir+"/"+sFilename;

		try {
			BufferedReader bLine = new BufferedReader(new FileReader(sTemplateFile));
			String sData = new String();

			// Read each line from the template
			while ((sData = bLine.readLine()) != null) {
				// Replace all variables in this line with their corresponding values.
				// Variables in templates are noted by beginning and ending with two pound characters.
				//
				// Example: ##CORPUS_NAME##
				//
				// Variable names will be case insensitive.

				int iStart = 0; int iLastIndex = 0;

				while ((iStart = sData.indexOf("##", iLastIndex)) != -1) {
					// Get the variable name.   To do this find the index where it ends.
					int iEnd = sData.indexOf("##", iStart+1);

					if (iEnd != -1) { // If we received -1 for some reason, it is not a variable
						String sVariableName = sData.substring(iStart+2, iEnd);
						String sVariableValue = new String("");

                        sData = sData.replaceAll(sVariableName, sVariableName.toUpperCase());

						// If the variable name is located in the hash table, replace the
						// variable in the template with its corresponding value
						if (htVariables.containsKey(sVariableName.toUpperCase()) == true) {
							sVariableValue = (String) htVariables.get(sVariableName.toUpperCase());
						}

                        sData = sData.replaceAll("##"+sVariableName+"##", sVariableValue);
						iStart = 0;
					} else { iStart = iEnd +2; }
				}

				// Finally!  Print out this line.
                Text = Text + sData + "\r\n";
			}
			bLine.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Template file not found.");
			e.printStackTrace(System.out);

		}
		catch (Exception e) { e.printStackTrace(System.out); }
	}
}
