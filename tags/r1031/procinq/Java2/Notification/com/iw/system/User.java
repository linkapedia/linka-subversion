package com.iw.system;
import com.iw.license.IndraLicense;

import java.awt.*;
import java.io.*;
import java.util.Vector;
import javax.swing.*;

/*
    User class: this is a component of the ContentPane in the JAVA GUI.  It contains all user
    information including preferences.
 */

public class User implements Serializable {
    // User attributes
    private String ID;
    private String Username;
    private String Email;
    private String Password;
    private String Name;
    private String EmailStatus;
    private String ScoreThreshold;
    private String ResultsPerPage;
    private String UserStatus;
    private String SessionKey;
    private String License;

    // user classify preferences
    private int documentWeight = 1; // if 0, use abstract in classification
    private int abstractWeight = 0; // if 0, use document in classification
    private boolean dynamicThesaurus = false;
    private boolean writeToLog = false;
    private String corporaToUse = "-1";

    // user concept alert thresholds
    private int criticalThreshold = 75;
    private int severeThreshold = 50;
    private int minorThreshold = 25;

    // corpus ingestion preferences
    private Boolean includeNumbers = new Boolean(false);
    private Integer numParentTitleTermAsSigs = new Integer(3);
    private Integer wordFreqMaxForSig= new Integer(75);
    private Integer phraseLenMin= new Integer(2);
    private Integer phraseLenMax= new Integer(4);
    private String delimiters = "\"?!/, &+.:;#*?\"";

    // miscellaneous
    private String ServerLocation;
    private String pathPrefix = "";

    // constructor(s)
    public User (HashTree ht) {
        ID = (String) ht.get("ID");
        Password = (String) ht.get("PASSWORD");
        Email = (String) ht.get("EMAIL");
        Name = (String) ht.get("NAME");
        EmailStatus = (String) ht.get("EMAILSTATUS");
        ScoreThreshold = (String) ht.get("SCORETHRESHOLD");
        ResultsPerPage = (String) ht.get("RESULTSPERPAGE");
        UserStatus = (String) ht.get("USERSTATUS");
        SessionKey = (String) ht.get("KEY");
        License = (String) ht.get("LICENSE");
    }
    public User () {}

    // accessor functions
    public String getID() { return ID; }
    public String getUsername() { return Username; }
    public String getEmail() { return Email; }
    public String getPassword() { return Password; }
    public String getName() { return Name; }
    public String getEmailStatus() { return EmailStatus; }
    public String getScoreThreshold() { return ScoreThreshold; }
    public String getResultsPerPage() { return ResultsPerPage; }
    public String getUserStatus() { return UserStatus; }
    public String getSessionKey() { return SessionKey; }
    public String getServer() { return ServerLocation; }
    public IndraLicense getLicense() { return new IndraLicense(License); }

    public int getDocumentWeight() { return documentWeight; }
    public int getAbstractWeight() { return abstractWeight; }
    public boolean useDynamicThesaurus() { return dynamicThesaurus; }
    public boolean writeToLog() { return writeToLog; }
    public String getCorporaToUse() { return corporaToUse; }

    public int getCriticalThreshold() { return criticalThreshold; }
    public int getSevereThreshold() { return severeThreshold; }
    public int getMinorThreshold() { return minorThreshold; }

    public Boolean getIncludeNumbers() { return includeNumbers; }
    public Integer getNumParentTitleTermAsSigs() { return numParentTitleTermAsSigs; }
    public Integer getWordFreqMaxForSig() { return wordFreqMaxForSig; }
    public Integer getPhraseLenMin() { return phraseLenMin; }
    public Integer getPhraseLenMax() { return phraseLenMax; }
    public String getDelimiters() { return delimiters; }

    public String getPathPrefix() { return pathPrefix; }

    public boolean modeClassifyDocument() { if (abstractWeight == 0) { return true; } return false; }
    public boolean modeAbstractDocument() { if (documentWeight == 0) { return true; } return false; }
    public boolean modeCustomClassify() {
        if ((abstractWeight != 0) && (documentWeight != 0)) { return true; } return false;
    }
    public boolean usingCorpus(String CorpusID) {
        String[] Corpora = corporaToUse.split(",");
        for (int i = 0; i < Corpora.length; i++) {
            if (CorpusID.equals(Corpora[i])) { return true; }
        }

        return false;
    }

    public void serializeObject (String Filename) throws Exception {
        Password = "";

        FileOutputStream fos = new FileOutputStream(Filename);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);
        oos.close();
    }

    public static User deserializeObject (String Filename) throws Exception {
        FileInputStream fis = new FileInputStream(Filename);
        BufferedInputStream bis = new BufferedInputStream(fis);
        ObjectInputStream ois = new ObjectInputStream(bis);
        User u = (User) ois.readObject();
        ois.close();

        return u;
    }

    // get the custom document columns for this user
    public static Vector getDocumentCustomColumns(Object o) {
        return new Vector();   
    }

    public boolean IsAuthorized (String s, PrintWriter o) { return true; }
    public boolean IsAuthorized (String s) { return true; }

    public void setServer(String Server) { ServerLocation = Server; }
    public void setUsername(String ID) { Username = ID; }

    public void setDocumentWeight(int weight) { documentWeight = weight; }
    public void setAbstractWeight(int weight) { abstractWeight = weight; }
    public void useDynamicThesaurus(boolean dynamic) { dynamicThesaurus = dynamic; }
    public void writeToLog(boolean write) { writeToLog = write; }
    public void setCorporaToUse(String corpora) { corporaToUse = corpora; }

    public void setCriticalThreshold(int threshold) { criticalThreshold = threshold; }
    public void setSevereThreshold(int threshold) { severeThreshold = threshold; }
    public void setMinorThreshold(int threshold) { minorThreshold = threshold; }

    public void setIncludeNumbers(String include) {
        if (include.toLowerCase().equals("true")) this.includeNumbers = new Boolean(true);
        else this.includeNumbers = new Boolean(false);
    }
    public void setIncludeNumbers(boolean include) { this.includeNumbers = new Boolean(include); }
    public void setNumParentTitleTermAsSigs(int terms) { this.numParentTitleTermAsSigs = new Integer(terms); }
    public void setWordFreqMaxForSig(int freq) { this.wordFreqMaxForSig =  new Integer(freq); }
    public void setPhraseLenMin(int length) { this.phraseLenMin =  new Integer(length); }
    public void setPhraseLenMax(int length) { this.phraseLenMax =  new Integer(length); }
    public void setDelimiters(String delimiters) { this.delimiters = delimiters; }

    public void setPathPrefix(String prefix) { pathPrefix = prefix; }
}
