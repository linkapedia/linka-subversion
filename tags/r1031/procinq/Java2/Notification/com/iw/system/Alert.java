package com.iw.system;

import java.awt.*;
import java.io.*;
import java.util.Vector;
import java.util.Iterator;

import org.dom4j.Element;

public class Alert {
    // Alert attributes
    private String ID;
    private String Name;
    private String UserDN;
    private String Email;
    private String From;
    private String Query;
    private String Notes;
    private String RunFreq;
    private String LastRun;
    private String ModDate;

    // constructor(s)
    public Alert (Vector v) {
        ID = (String) v.elementAt(0);
        Name = (String) v.elementAt(1);
        UserDN = (String) v.elementAt(2);
        Email = (String) v.elementAt(3);
        From = (String) v.elementAt(4);
        Query = (String) v.elementAt(5);
        Notes = (String) v.elementAt(6);
        RunFreq = (String) v.elementAt(7);
        LastRun = (String) v.elementAt(8);
        ModDate = (String) v.elementAt(9);
    }
    public Alert (Element e) {
        ID = e.element("ALERTID").getText();
        Name = e.element("ALERTNAME").getText();
        UserDN = e.element("USERDN").getText();
        Email = e.element("EMAIL").getText();
        From = e.element("FROMEMAIL").getText();
        Query = e.element("QUERY").getText();
        Notes = e.element("NOTES").getText();
        RunFreq = e.element("RUNFREQ").getText();
        LastRun = e.element("LASTRUN").getText();
        ModDate = e.element("MODDATE").getText();
    }
    public Alert () {}


    // accessor functions
    public String getID() { return ID; }
    public String getName() { return Name; }
    public String getUserDN() { return UserDN; }
    public String getEmail() { return Email; }
    public String getFromEmail() { return From; }
    public String getQuery() { return Query; }
    public String getNotes() { return Notes; }
    public String getRunFreq() { return RunFreq; }
    public String getLastRun() { return LastRun; }
    public String getModDate() { return ModDate; }

    public void setID(String ID) { this.ID = ID; }
    public void setName(String Name) { this.Name = Name; }
    public void setUserDN(String UserDN) { this.UserDN = UserDN; }
    public void setEmail(String Email) { this.Email = Email; }
    public void setFromEmail(String From) { this.From = From; }
    public void setQuery(String Query) { this.Query = Query; }
    public void setNotes(String Notes) { this.Notes = Notes; }
    public void setRunFreq(String RunFreq) { this.RunFreq = RunFreq; }
    public void setLastRun(String LastRun) { this.LastRun = LastRun; }
    public void setModDate(String ModDate) { this.ModDate = ModDate; }

}
