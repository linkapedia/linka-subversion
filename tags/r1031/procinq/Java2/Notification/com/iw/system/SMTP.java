package com.iw.system;

import java.io.*;
import java.net.*;
import java.util.Date;

/**
    A simple SMTP mailer.  Permits sending of mail from an application
    without using the bloated JavaMail API and Beans architecture.  A
    single SMTP object can be shared by multiple threads, although one
    thread could change the verbosity of output in the middle of another
    thread sending mail.
*/
public class SMTP {
    private final InetAddress host;
    private final int port;
    private boolean verbose;

    public SMTP(String host) throws UnknownHostException {
        this.host = InetAddress.getByName(host);
        this.port = 25;
    }

    public SMTP(String host, int port) throws UnknownHostException {
        this.host = InetAddress.getByName(host);
        this.port = port;
    }

    public void sendMail(String fromName, String fromAddr, String toName, String toAddr,
          String subj, String msg) throws IOException, SMTPException {

        System.out.println("\r\nSending e-mail to: "+toAddr);

        Socket hostSocket = new Socket(host, port);
        BufferedReader in = new BufferedReader(new InputStreamReader(hostSocket.getInputStream()));
        PrintStream out = new PrintStream(hostSocket.getOutputStream());

        try {
          checkResponse(in);
          send(out, "HELO");
          checkResponse(in);
          send(out, "MAIL FROM: <" + fromAddr + ">");
          checkResponse(in);
          send(out, "RCPT TO: <" + toAddr + ">");
          checkResponse(in);
          send(out, "DATA");
          checkResponse(in);
          send(out, "Date: " + new Date().toString());
          send(out, "From: " + fromName + " <" + fromAddr + ">");
          send(out, "To: " + toName + " <" + toAddr + ">");
          send(out, "Content-Type: text/html; charset=\"iso-8859-1\"");
          send(out, "Subject: " + subj);
          send(out, "");

          String message[] = msg.split("\n");
          for(int line = 0; line < message.length; line++) {
               if(message[line].equals(".")) { send(out, ".."); }
               else { send(out, message[line]); }
          }

          send(out, "\n");
          send(out, ".");
          send(out, "\n");
          checkResponse(in);
        } finally {
          send(out, "QUIT");
          /* to ensure this gets done in a timely fashion */
          try {
               hostSocket.close();
          } catch(IOException e) { /* ignore */ }
        }
    } /* end sendMail() */

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    protected void send(PrintStream out, String s) {
        if(verbose) System.out.println(s);
        out.println(s);
    }

    protected void checkResponse(BufferedReader in) throws IOException, SMTPException {
        String response = in.readLine();
        if(verbose) System.out.println(response);
        int code = Integer.parseInt(response.substring(0, 3));
        if (code > 400) throw new SMTPException(code, response);
    }

    private class SMTPException extends Exception {
        public final int code;

        public SMTPException(int code, String msg) {
            super(msg);
            this.code = code;

            System.out.println("SMTP Exception: "+code+" error.");
        }
    } /* end class SMTPException */
} /* end class SMTP */
