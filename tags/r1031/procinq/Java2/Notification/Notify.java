import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

import com.iw.system.*;

/*
   Notify.class

   This is the main function for the ITS CQL-based notification system.   When invoked,
   the notification system will do the following:

   1. Log into the ITS API server.
   2. Select all alerts (via the ITSAPI) where DateLastRun > Today - RunFrequency.
   3. Group these alerts by e-mail address (only one e-mail is sent per address)
   4. Run CQL alerts, with additional constraints:
        a) where datelastfound > Today - RunFrequency
        b) where there are no relationships to the "no-alert" special keyword
   5. Load HTML notification template and send out e-mails

   Arguments:

   -userid                          Username to use when logging into the API server
   -password                        Password to use when logging into the API server
   -api (optional)                  IP address of the API server to use
   -templatedir (optional)          Location of the notification templates

*/

public class Notify {
    public static ITS its = null;

    // This is our MAIN function
    public static void main(String args[]) {

        // extract command line arguments and put into a hashtable
        HashTree htArgs = getArgHash(args);

        // extract optional parameters, and if blank install defaults
        String fromName = (String) htArgs.get("fromname");
        if (fromName == null) fromName = "ProcinQ Alert";
        String fromEmai = (String) htArgs.get("fromemail");
        if (fromEmai == null) fromEmai = "procinq@mycompany.com";
        String subject = (String) htArgs.get("subject");
        if (subject == null) subject = "ProcinQ Alert";
        String mailHost = (String) htArgs.get("mailhost");
        if (mailHost == null) mailHost = "127.0.0.1";

        if (!htArgs.containsKey("api")) {
            System.out.println("Usage: java -cp . Notify -userid username -password password");
            return;
        }
        htArgs.put("api", "http://" + (String) htArgs.get("api") + "/");

        try {
            // Throw an exception if any required parameters are missing
            if ((!htArgs.containsKey("userid")) ||
                    (!htArgs.containsKey("password"))) {
                throw new Exception("Usage: java -cp . Notify -userid username -password password");
            }

            // Step 1: Attempt to log in to the ITS API server, exit on failure
            its = new ITS();
            its.SetAPI((String) htArgs.get("api"));
            try {
                User u = its.Login((String) htArgs.get("userid"), (String) htArgs.get("password"));
                its.SetSessionKey(u.getSessionKey());
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println("Login failed; nothing to do.  Quitting...");
                return;
            }

            if (its.getSessionKey().equals("NONE")) {
                System.out.println("Login failed; nothing to do.  Quitting...");
                return;
            }

            System.out.println("Login succeeded.");

            // time this process (for diagnostics only)
            long lStart = System.currentTimeMillis();

            // Step 2: Select all alerts (via the ITSAPI) where DateLastRun > Today - RunFrequency.
            // ( Group these alerts by e-mail address (only one e-mail is sent per address )
            Vector vt = its.getNotificationAlerts((String) htArgs.get("userid"));
            if (vt == null) {
                System.out.println("All alerts are up to date.  Goodbye!");
                return;
            }
            Enumeration eV = vt.elements();

            //  4. Run CQL alerts, with additional constraints:
            //  a) where datelastfound > Today - RunFrequency
            //  b) where there are no relationships to the "no-alert" special keyword
            String sProcessingEmail = "";
            String Name = "";
            NotificationEmail ne = null;
            while (eV.hasMoreElements()) {
                Alert a = (Alert) eV.nextElement();

                if (!sProcessingEmail.equals((String) a.getEmail())) {
                    // first, wrap up previous e-mail
                    if (!sProcessingEmail.equals("")) {
                        ne.WriteFooter(); // ne.WriteToFile(Name);
                        if (ne.HasResults()) {
                            ne.SendEmail(sProcessingEmail, sProcessingEmail, fromName, fromEmai, subject, mailHost);
                        }
                    }

                    Name = a.getName();
                    sProcessingEmail = a.getEmail();
                    if (htArgs.containsKey("templatedir")) {
                        ne = new NotificationEmail(sProcessingEmail, (String) htArgs.get("templatedir"));
                    } else {
                        ne = new NotificationEmail(sProcessingEmail);
                    }
                }
                // Flag this alert as in progress
                its.FlagAlert(a.getID());

                // Run this alert
                Vector vDocuments = ExecuteCQL(a);
                ne.WriteResults(vDocuments, a);
            }
            if (ne == null) {
                System.out.println("There are no alerts to run.");
                return;
            }
            ne.WriteFooter(); //ne.WriteToFile(Name);

            // 5. Load HTML notification template and send out e-mails
            if (ne.HasResults()) {
                ne.SendEmail(sProcessingEmail, sProcessingEmail, fromName, fromEmai, subject, mailHost);
            }

            System.out.println("Notification run complete.");
            System.out.println("Notification run completed in " + (System.currentTimeMillis() - lStart) + " ms.");

        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println(e.getMessage());
            return;
        }
    }

    //  4. Run CQL alerts, with additional constraints:
    //  a) where datelastfound > Today - RunFrequency
    //  b) where there are no relationships to the "no-alert" special keyword
    public static Vector ExecuteCQL(Alert a) {
        String CQL = "";
        try {
            //  add: where datelastfound > Today - RunFrequency
            if (a.getRunFreq().equals("-1")) return new Vector();

            CQL = a.getQuery();

            CQL = CQL + " and DateLastFound > SYSDATE - " + a.getRunFreq();

            // add: security permission information if not a narrative
            if (a.getQuery().toUpperCase().indexOf("<NARRATIVE") == -1)
                CQL = CQL + " SECURITY:\"" + a.getUserDN().replaceAll("\\|", ",") + "\"";

            Vector vResults = its.CQL(CQL, 1, 500);

            // If there is no elements, an error has occured
            if (vResults.size() < 1) {
                throw new NoResultsFound();
            }
            return vResults;

        } catch (NoResultsFound nrf) {
            System.out.println("No results found for query: " + CQL);
            return new Vector();
        } catch (Exception e) {
            System.err.println("Encountered unrecoverable error while processing query: " + CQL);
            e.printStackTrace(System.out);
            return null;
        }
    }

    // GetArgHash takes command line arguments and parses them into a Hash structure
    public static HashTree getArgHash(String[] args) {
        HashTree htHash = new HashTree();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}