/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Dec 14, 2003
 * Time: 4:51:20 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.iw.metasearch;

import com.indraweb.execution.Session;
import com.iw.guiservershared.TableModelXMLBeanSerializable;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;

import javax.swing.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPOutputStream;
import java.util.Hashtable;

import api.Log;

public class MainTest4JDBCTableModel
{
/*    public static void main (String[] args)
    {

    }*/
    // C:\downloads\IGLU sourceforge datatable resultset
    public static void test1 ()
    {
        Connection dbc = localInitSession ("API");
         try {
        System.out.println( "start" );
        System.out.println( " test1(dbc) [" + test1(dbc) + "]" );
        System.out.println( "done" );
        } catch (Exception e )
        {
            e.printStackTrace();
            e.printStackTrace();
        }
    }

    public static String test1(Connection dbc) throws Exception
    {
        // called by maintest

        String sSQL = "	select nodeid, nodetitle from node where corpusid = 18";
        //Statement stmt = dbc.createStatement ();
/*
        Statement stmt = dbc.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = stmt.executeQuery (sSQL);

        //while (rs.next ())
        //{
        //}

        iglu.jdbc.JDBCTableModel jtm = new iglu.jdbc.JDBCTableModel (rs);



        rs.close ();
        stmt.close ();
*/

        TableModelXMLBeanSerializable tm2 = new TableModelXMLBeanSerializable ();
        tm2.fillDataWithSQLCall(sSQL, dbc, null, null, null, null, false);


        TableModelXMLBeanSerializable tm3 = null;
        if ( true )
        {
            String sFileName = "c:/temp/serializeToFileTM.xml";
            XMLBeanSerializeHelper.serializeToFile(tm2, sFileName);
            tm3 = (TableModelXMLBeanSerializable)XMLBeanSerializeHelper.deserializeFromFile(sFileName);
        }


/*        JFrame jframe = new JFrame();
        jframe.setSize(400,400);
        JPanel jPanel = new JPanel();
        jPanel.setSize(400,400);
        jPanel.setVisible(true);
        jPanel.show();
        jframe.getContentPane().add (jPanel);
        jframe.setVisible(true);
        jframe.show();
        JTable jtable = new JTable();
        jtable.setSize(300,300);
        jtable.show();
        jtable.setVisible(true);
        //jtable.setModel(jtm);
        jtable.setModel(tm3);
        jPanel.add(jtable);
        */



        System.out.println( " sSQL [" + sSQL + "]" );
        System.out.println( " jtm.getColumnCount() [" + tm2.getColumnCount() + "]" );
        System.out.println( " jtm.getRowCount() [" + tm2.getRowCount() + "]" );
        for ( int i = 0; i < tm2.getColumnCount(); i++)
        {
            System.out.println( i+ ". jtm.getColumnName(i) [" + tm2.getColumnName(i) + "]" );
            System.out.println( i+ ". jtm.getColumnClass(i) [" + tm2.getColumnClass(i) + "]" );
            //System.out.println( i+ ". jtm.getBookmarkString(i) [" + tm2.getBookmarkString(i) + "]" );
            System.out.println( i+ ". jtm.getValueAt(i) [" + tm2.getValueAt(0, i) + "]" );
        }



        //jtm.

/*
   FileOutputStream fos = new FileOutputStream("c:/temp/temp.txt");
// Save to file
        GZIPOutputStream gzos = new GZIPOutputStream(fos);
// Compressed
        ObjectOutputStream out = new ObjectOutputStream(gzos);
// Save objects
        out.writeObject(jtm // Write the entire Vector of scribbles
        out.flush();                 // Always flush the output.
        out.close();
*/
        return null;
    }
    private static Connection localInitSession (String sWhichDB)
    {
        if (!com.indraweb.execution.Session.GetbInitedSession ())
        {
//				com.indraweb.util.UtilFile.addLineToFile("/temp/temp.txt", new java.util.Date() +	": doing MainTest init session  \r\n");
            Hashtable htprops = new Hashtable ();
            // Load	environment	from JRUN config application variables first
            String sOSHost = (String) java.lang.System.getProperties ().get ("os.name");
            if (sOSHost.toLowerCase ().indexOf ("windows") >= 0)
            {
                Session.sIndraHome = "C:/Program Files/ITS";
            }
            else
            {
                Session.sIndraHome = "/tmp/IndraHome";
            }

            Session.sLogNameDetail = Session.sIndraHome + "/logs/LogDetail.txt";

            htprops.put ("ImportDir" , "C:/TEMP");
            htprops.put ("AuthenticationSystem" , "ActiveDirectory");
            htprops.put ("MachineName" , "indraweb-e5j05c");
            htprops.put ("DBString_API_User" , "sbooks");
            htprops.put ("DBString_SVR_User" , "sbooks");
            htprops.put ("DBString_API_Pass" , "racer9");
            htprops.put ("DBString_SVR_Pass" , "indra9");

            //String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:client";  // :client :schering :gaea
            //String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:client";  // :client :schering :gaea
            //String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:client";  // :client :schering :gaea
            String sDB = "jdbc:oracle:thin:@66.134.131.37:1521:perseus";  // :client :schering :gaea
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            htprops.put ("DBString_API_OracleJDBC" , sDB);
            System.out.println ("DBString_API_OracleJDBC:" + sDB);
            //htprops.put	( "DBString_API_OracleJDBC", "jdbc:oracle:thin:@207.103.213.109:1521:client");
            //htprops.put	( "DBString_SVR_OracleJDBC", "jdbc:oracle:thin:@207.103.213.109:1521:gaea");
            htprops.put ("DBString_SVR_OracleJDBC" , "jdbc:oracle:thin:@207.103.213.109:1521:gaea");
            htprops.put ("IndraHome" , Session.sIndraHome);
            htprops.put ("IndraFolderHome" , Session.sIndraHome);		//}

            htprops.put ("BatchClassFTPHost" , "shell.theworld.com");		//}
            htprops.put ("BatchClassFTPUser" , "hkon2");		//}
            htprops.put ("BatchClassFTPPass" , "xxxx");		//}
            htprops.put ("BatchClassFTPRemoteDir" , "/usr/tmp");		//}
            htprops.put ("outputToFile" , "true");
            htprops.put ("outputToScreen" , "true");

            Session.cfg = new com.indraweb.execution.ConfigProperties (htprops);
            Session.SetbInitedSession (true);
            Session.cfg.setProp ("outputToScreen" , "true");

            /*
            System.out.println(
                    " API DB [" + htprops.get ( "DBString_API_OracleJDBC") + "]"   +
                    " SVR DB [" + htprops.get ( "DBString_SVR_OracleJDBC") + "]"
            );
            */

        }

        //com.indraweb.util.UtilFile.addLineToFile("c:/temp/temp.txt", new java.util.Date()	+ ": done chech	or init	session	 \r\n");
        return initializeDB (sWhichDB);


    }

    public static Connection initializeDB (String sWhichDB)
    {
        try
        {
            Connection dbc = api.statics.DBConnectionJX.getConnection (sWhichDB , "Maintest.initializeDB");
            Hashtable htargs = new Hashtable ();

            String sSQL = "	select ParamName, ParamValue from ConfigParams ";
            Statement stmt = dbc.createStatement ();
            ResultSet rs = stmt.executeQuery (sSQL);

            while (rs.next ())
            {
                String sK = rs.getString (1);
                String sV = rs.getString (2);

                if (sV == null)
                {
                    sV = "";
                }
                htargs.put (sK , sV);
            }

            Session.cfg.addToProperties (htargs);
            rs.close ();
            stmt.close ();
            return dbc;
        } catch ( Exception e )
        {
            Log.LogError ("error	getting	DB connection internal stack trace" , e);
            return null;
        }
    }



}
