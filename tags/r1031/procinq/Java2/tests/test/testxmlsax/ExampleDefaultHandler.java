package test.testxmlsax;

// an example sink for content events. It simply prints what it sees
 class ExampleDefaultHandler extends org.xml.sax.helpers.DefaultHandler
    implements org.xml.sax.ContentHandler
{
  public void startElement ( String namespace, String localname, String sTagType, org.xml.sax.Attributes attributes)
      throws org.xml.sax.SAXException
  {
    System.out.println ("startElement: \"" + sTagType + "\".");
  }

  public void endElement ( String namespace, String localname, String sTagType)
      throws org.xml.sax.SAXException
  {
    System.out.println ("endElement: \"" + sTagType + "\".");
  }

   public void characters ( char[] ch, int start, int len)
  {
     String sText = new String (ch, start, len).trim();
    if (sText.length () > 0)
      System.out.println("characters " + sText);
  }
}
