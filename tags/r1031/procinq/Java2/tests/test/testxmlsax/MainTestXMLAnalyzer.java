/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Dec 30, 2003
 * Time: 7:09:21 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package test.testxmlsax;

import com.indraweb.network.InputStreamsGen;
import com.indraweb.html.UrlHtmlAttributes;

import java.net.URL;
import java.io.InputStream;

/** An example application */
final public class MainTestXMLAnalyzer
{
    /** read an example XML file */
    final public static void main (final String[] args)
            throws Exception
    {
         System.out.println("test1 from a file");
        com.indraweb.utils.parsexmlhtml.UtilXmlSaxParser.parseXML("c:/temp/t.xml", new ExampleDefaultHandler());

        System.out.println("test2 from a URL");
        URL url = new URL("http://66.134.131.35/itsapi/ts?fn=security.TSLogin&UserID=hkon&password=racer9");
        com.indraweb.utils.parsexmlhtml.UtilXmlSaxParser.parseXML(url, new ExampleDefaultHandler());
    }
}