/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jun 24, 2004
 * Time: 6:03:44 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.ir;

public class PhraseParms
{

    //public boolean bWantPhrases = true;
    public static int iPhraseCountMultiplier_default = 1;
    public static int iPhraseLenMin_default = 2;
    public static int iPhraseLenMax_default = 4;
    public static int iPhraseRepeatMin_default = 3;
    public static boolean bPhraseWordsCanStartWithNonLetters_default = false;

    public int iPhraseCountMultiplier = iPhraseCountMultiplier_default;
    public int iPhraseLenMin = iPhraseLenMin_default;
    public int iPhraseLenMax = iPhraseLenMax_default;
    public int iPhraseRepeatMin = iPhraseRepeatMin_default;
    public boolean bPhraseWordsCanStartWithNonLetters = bPhraseWordsCanStartWithNonLetters_default;


    public PhraseParms ( )
    {
    }
    public PhraseParms (
            int iPhraseCountMultiplier_,
            int PhraseLenMin_,
            int PhraseLenMax_,
            int PhraseRepeatMin_,
            boolean bPhraseWordsCanStartWithNonLetters_ // false
    )
    {
        iPhraseCountMultiplier = iPhraseCountMultiplier_;
        iPhraseLenMin = PhraseLenMin_;
        iPhraseLenMax = PhraseLenMax_;
        iPhraseRepeatMin = PhraseRepeatMin_;
        bPhraseWordsCanStartWithNonLetters = bPhraseWordsCanStartWithNonLetters_; // false

    }
    public PhraseParms  (
            api.APIProps props // false
    ) throws Exception
    {
        iPhraseCountMultiplier = props.getint("PhraseCountMultiplier", ""+iPhraseCountMultiplier_default);
        iPhraseLenMin = props.getint("PhraseLenMin", ""+iPhraseLenMin_default);
        iPhraseLenMax = props.getint("PhraseLenMax", ""+iPhraseLenMax_default);
        iPhraseRepeatMin = props.getint("PhraseRepeatMin", ""+iPhraseRepeatMin_default);
        bPhraseWordsCanStartWithNonLetters = props.getbool("PhraseWordsCanStartWithNonLetters", ""+bPhraseWordsCanStartWithNonLetters);
        //this.printme();
    }

    public void printme()
    {
        api.Log.Log ("iPhraseCountMultiplier [" + iPhraseCountMultiplier + "]");
        api.Log.Log ("iPhraseLenMin [" +iPhraseLenMin + "]");
        api.Log.Log ("iPhraseLenMax [" + iPhraseLenMax+ "]");
        api.Log.Log ("iPhraseRepeatMin [" + iPhraseRepeatMin+ "]");
        api.Log.Log ("bPhraseWordsCanStartWithNonLetters [" + bPhraseWordsCanStartWithNonLetters + "]");
    }


}
