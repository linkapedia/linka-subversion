package com.indraweb.database;

import java.util.*;

public class SQLGenerators
{
	

	// *******************************************************************
	public static String genSQLInsert ( String sTableName, 
	// *******************************************************************
									    Vector vStrings_Fields, 
										Vector vStrings_Values )
		throws Exception
	{
		if ( vStrings_Fields.size() != vStrings_Values.size() )
			throw new Exception ("genSQLInsert : vector lens not match");
		StringBuffer sbColnames = new StringBuffer();
		StringBuffer sbValues = new StringBuffer();
		for ( int i = 0; i < vStrings_Fields.size(); i++ )
		{
			if ( i == 0)
				sbColnames.append(vStrings_Fields.elementAt(i));
			else
				sbColnames.append("," + vStrings_Fields.elementAt(i));
			
			String sValue = (String)vStrings_Values.elementAt(i);
			if ( !sValue.toLowerCase().equals("sysdate"))
				sValue =  sValue ;
			if ( i == 0)
				sbValues.append(sValue);
			else
				sbValues.append("," + sValue);
		} 
		
		String sReturn = "insert into " + sTableName + 
			   " (" + sbColnames.toString() + ")" + 
			   " values (" + sbValues.toString() + ")";

		//System.out.println("insert sql [" + sReturn + "]");
		return sReturn;
	} // public static String genSQLInsert ( String sTableName, 
		
	// *******************************************************************
	public static String genSQLUpdate ( String sTableName, 
	// *******************************************************************
									    Vector vStrings_FieldNValuesUpdate, 
									    Vector vStrings_FieldNValuesWhere
										)
		throws Exception
	{
		if ( vStrings_FieldNValuesUpdate.size() != vStrings_FieldNValuesUpdate.size() )
			throw new Exception ("genSQLUpdate : vector lens not match");
		StringBuffer sbUpdates = new StringBuffer();
		for ( int i = 0; i < vStrings_FieldNValuesUpdate.size(); i++ )
		{
			if ( i == 0)
				sbUpdates.append( " set " + vStrings_FieldNValuesUpdate.elementAt(i) );
			else
				sbUpdates.append( ", " + vStrings_FieldNValuesUpdate.elementAt(i) );
			
		} 
		
		StringBuffer sbWhere = new StringBuffer();
		for ( int i = 0; i < vStrings_FieldNValuesWhere.size(); i++ )
		{
			if ( i == 0)
				sbWhere.append( " where " + vStrings_FieldNValuesWhere.elementAt(i)); 
			else
				sbWhere.append( " and " + vStrings_FieldNValuesWhere.elementAt(i)); 
		} 
		
		String sReturn = "update " + sTableName + sbUpdates.toString() + sbWhere ;
		//System.out.println("update sql [" + sReturn + "]");
		return sReturn;
	}
		
		
	/*
	public static String genSQLUpdate ( Properties props, 
											 String[] sArrFieldNames, 
											 String[] sArrFieldNames_DB,
										 String[] sArrQuotes
									)
	{
		
		
		for ( int loop = 0; loop > sArrFields.length; loop++ ) {
			// Throw an exception if ANY of the optional parameters were passed
			if ( loop < iNumRequiredFields ) {
				if ( props.get (sArrFields[loop]) == null )
				{
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS, "missing field : " + sArrFields[loop]);
				}
			}				
		}

		// Most of the parameters to this function are OPTIONAL
		// Build a query string based on the optional parameters
		StringBuffer sb = new StringBuffer ();
		sb.append  ("update " + sTableName + " set " );

			
		// add all the field=value pairs
		boolean bAnyFieldsYet = false;
		for ( int loop = iNumRequiredFields; loop < sArrFields.length; loop++ )
		{
			String sValue = (String) props.get (sArrFields[loop]);
			if ( sValue != null )
			{
				if ( bAnyFieldsYet )
					sb.append(", ");
				sb.append ( sArrFields_DBNames[loop] + " = " + sArrQuotes[loop] + 
							sValue + sArrQuotes[loop] );
				bAnyFieldsYet = true;
			}
		} 
			
		if ( !bAnyFieldsYet )
		{
			throw new TSException ( EmitGenXML_ErrorInfo.ERR_TS_NO_UPDATE_FIELDS );
		}
		else
		{
			// Tack on the WHERE clause
			sb.append (" where ");
			
			bAnyFieldsYet = false;
			for ( int loop = 0; loop < iNumRequiredFields; loop++ )
			{
				String sValue = (String) props.get (sArrFields[loop]);
				if ( bAnyFieldsYet )
					sb.append(" and ");
				sb.append ( sArrFields_DBNames[loop] + " = " + sArrQuotes[loop] + 
							 sValue + sArrQuotes[loop] );
				bAnyFieldsYet = true;
			} 
			
			String sSQL = sb.toString();
			out.println ( "<DEBUG>SQL:" + sSQL + "</DEBUG>"); 
			Log.log ( "sql:" + sSQL);
			out.flush();
			Statement stmt = null;
			try
			{
				stmt = dbc.createStatement();	

				// If query statement failed, throw an exception
				if (stmt.executeUpdate (sSQL) == 0) {
					out.println ( "<DEBUG>The specified corpus / client does not exist</DEBUG>"); 
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND);
				}
			}
			catch ( Exception e ) 
			{
				throw new TSException (	EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION,  
					"e.msg ["	+ e.getMessage() + "] " +
					" SQL ["	+ sb.toString() + "] " +
					" e.stk ["	+ Log.stackTraceToString(e) + "]" );
			}
			finally 
			{
				out.println (EmitValue.genValue ("DEBUG" , "corpus edit complete"  )); 
				stmt.close();
			}
			
		}
	}
	*/
}