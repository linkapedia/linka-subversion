package com.indraweb.util;

import java.io.*;
import java.util.*;
import com.indraweb.database.*;

public class UtilFileData_DBConnect
{
	public static String buildUpdateSQL ( String sTableName_, 
										  int[] iColIndexes_, 
										  String[] sArrColNames_,
										  String[] sArrRowDataInsert_,
										  String sWhereClause_,
										  UtilFileData ufd_
										  )
	{
		StringBuffer sbUpdateColValueList = 
			new StringBuffer ("update " + sTableName_ + " set " );
		
		for ( int i = 0; i < iColIndexes_.length; i++ )
		{
			if ( i > 0)
			{
				sbUpdateColValueList.append ( ", " );	
			}
			sbUpdateColValueList.append ( ufd_.getColName ( iColIndexes_ [ i ] ) + " = " );
			sbUpdateColValueList.append ( 
											ufd_.getColQuoteString ( iColIndexes_ [ i ] ) +
											sArrRowDataInsert_ [ iColIndexes_ [ i ] ] +
											ufd_.getColQuoteString ( iColIndexes_ [ i ]  ) 
										  );
		} 

		sbUpdateColValueList.append ( sWhereClause_ );
		
		//String sSql = "update " + sTableName_ + " set " + 
		
		return sbUpdateColValueList.toString();
		
	}
	
	public static String buildSQLWhere (	
					int[] iArrColsForValueForWhereConstraints_source, 
					String[] sArrColsForNamesForWhereConstraints_target, 
					String[] sArrRowData_,
					UtilFileData ufd_)
	{
		StringBuffer sbUpdateColValueList = new StringBuffer ( " where " );
		for ( int iCol = 0; iCol < iArrColsForValueForWhereConstraints_source.length; iCol++ )
		{
			if ( iCol > 0)
			{
				sbUpdateColValueList.append ( " and " );	
			}
			sbUpdateColValueList.append ( sArrColsForNamesForWhereConstraints_target [iCol] + " = " );
			sbUpdateColValueList.append ( 
											ufd_.getColQuoteString ( iCol ) +
											sArrRowData_ [ iCol ] +
											ufd_.getColQuoteString ( iCol ) + " " 
										  );

		} 
		//String sSql = "update " + sTableName_ + " set " + 
		
		return sbUpdateColValueList.toString();
	}
}
