package com.indraweb.workfactory;

import java.util.*;
import com.indraweb.util.*;
import com.indraweb.execution.*;

public class WorkToDoPool
{
	boolean noMoreWorkToCome = false;
	
	Vector	vOfWorkTasksToDo			= new Vector();
	Vector	vOfWorkTasksStarted			= new Vector();
	Vector	vOfWorkTasksCompleted		= new Vector();
	String	sToDoPoolComment = null;
	
	public WorkToDoPool ( String sToDoPoolComment_ )
	{
		sToDoPoolComment = sToDoPoolComment_;	
	}
	
	public synchronized void addWorkTask ( WorkTask wu )
	{
		vOfWorkTasksToDo.addElement	( wu );	
		notify();
	} 

	public void setNoMoreWorkToCome (  )
	{
		noMoreWorkToCome = true;	
	} 
	
	public synchronized void removeWorkTask ( WorkTask wu )
	{
		vOfWorkTasksStarted.removeElement	( wu );	
		vOfWorkTasksCompleted.addElement ( wu ) ;
	} 
	
	public synchronized boolean isMoreWorkToCome()
	{
		if ( vOfWorkTasksToDo.size() == 0 && noMoreWorkToCome )
			return false;
		else
			return true;
	}	
	
	public synchronized int getNumPoolItems()
	{
		return vOfWorkTasksToDo.size();
	} 
	
	/**
	 * @param iThreadIndex informational only - which thread slot is looking for work
	 */
	
	public synchronized WorkTask getNextWorkTask ( int iThreadIndex, long lTimeoutMS )
	{
		WorkTask wu = null;
		long ltStartGetNextWork = System.currentTimeMillis();
		int counter = 0;
		// Log.log ("vOfWorkTasksToDo.size() [ " + vOfWorkTasksToDo.size() + "]\r\n" );
		while ( vOfWorkTasksToDo.size() == 0 )
		{
			counter++;
			clsUtils.pause_this_many_milliseconds ( 500 ) ;
			try 
			{
				if ( noMoreWorkToCome ) 
				{
					if ( false )
					{
					
						if ( (System.currentTimeMillis() - ltStartGetNextWork) > lTimeoutMS ) 
						{
							Log.log (counter + ". noMoreWorkToCome for pool [" + sToDoPoolComment + "] timeout task never came in ["  + lTimeoutMS + "] ms is this an error ? !\r\n");
							return null;						
						} 
					}
					else
					{
						if ( vOfWorkTasksToDo.size() > 0 )
						{
							Log.FatalError (counter + ". noMoreWorkToCome for pool [" + sToDoPoolComment + "] - but came ayway !\r\n" );
						}
						else
						{
							Log.log (counter + ". noMoreWorkToCome for pool [" + sToDoPoolComment + "] - returning null task\r\n" );
							return null;						
						}
							  
					}
				}
				else
				{
					Log.log ( "WORK WAIT thread index [" + iThreadIndex + "]\r\n");
					wait();
					Log.log ( "WORK WAIT OVER thread index [" + iThreadIndex + "]\r\n");
				}
				
			} 
			catch ( Exception e )
			{
				Log.printStackTrace_mine ( 93, e ) ;
			}
		} 
		wu = ( WorkTask ) vOfWorkTasksToDo.elementAt  ( 0 );
		vOfWorkTasksToDo.removeElementAt  ( 0 );
		wu.setState_inProcess();
		vOfWorkTasksStarted.addElement ( wu ); 
		return wu;
	} 

} // class WorkToDoPool
