package com.indraweb.signatures;

import java.util.*;
import java.io.*;

import com.indraweb.util.*;
public class WordFrequencies
{
	
	public static int ICOUNTFILE_MODEL_BNC = 1;
	public static int ICOUNTFILE_MODEL_IW = 2;
	
	private Hashtable htWordCounts_String_to_Integer = new Hashtable();
	private Hashtable htCountsToStrVecs = new Hashtable();
	private int iSigMinCountInclusive = -1;

	public Hashtable gethtWordCounts_String_to_Integer()
	{
		return htWordCounts_String_to_Integer;	
	} 
	
	public Integer getWord ( String sWord )
	{
		return (Integer) htWordCounts_String_to_Integer.get(sWord);
	} 
	
	/**
	 * Add to a hashtable words and counts of words occuring more than X times in a corpus.
	 * Text file is the list of frequencies of occurences of words in BNC corpus or in WB IW corpus.
	 * For signature word removal.
	 * 
	 * ICOUNTFILE_MODEL_BNC: C:\Documents and Settings\Indraweb\IndraHome\WordFrequencies\all.al
	 *  Constructor : from a file with say 1M lines of form 
	 *  sample format : 61 zooming vvg 52
	 *  61 = count of words in the files processed
	 *  zooming = word
	 *  vvg = part of speech ( see http://www.hcu.ox.ac.uk/BNC/what/gramtag.html ) 
	 *  52 = count of files the word occurred in
	 * 
	 * ICOUNTFILE_MODEL_IW
	 * "C:\Documents and Settings\Indraweb\IndraHome\WordFrequencies/SigWordFreqs_IWModel.txt
	 * 
	 * @param model == 1 for sample input format British National Corpus : 61 zooming vvg 52
	 */
	public WordFrequencies ( String sFileName,
							 int iSigMinCountInclusive,
							 int model,
							 boolean bCollectOrderedListAlso)
	{
        this.iSigMinCountInclusive = iSigMinCountInclusive;
		long lStart = System.currentTimeMillis();
        String textline;
		java.io.BufferedReader in = null;

		try 
		{ 
			// api.Log.Log ("start WordFrequencies[" + sFileName + "]\r\n");
			in = new java.io.BufferedReader(new java.io.FileReader( sFileName));		

			int iCount = 0;
			boolean bEndComments = false;
			String sWordLastLine = "";
			// some words occur on >1 line - assumes file is alpha sorted
			int iCurrentWordAccumulated = 0; 
			int iLinesRead = 0;
			int iTextEliminated = 0;
			int iTextRepeat = 0;
			//while (in.ready() && iLinesRead < 10001 ) // hbk control remove 100000
			while (in.ready())
			{
				textline = in.readLine();
				if ( textline == null || textline.length() < 3 )
					break;
				iLinesRead++;
				if ( bEndComments == false ) 
				{
					if (textline.startsWith("#"))	
						continue;
					else
						bEndComments = true;
				} 
				// BNC file has many strange characters - words we don't want to keep 
				if (model == ICOUNTFILE_MODEL_IW ||( model == ICOUNTFILE_MODEL_BNC && !eliminateLine ( textline, model ) ) ) // don't keep the coded lines 
				{
					String[] sArr = parseLine ( textline, model, iLinesRead );
					String sWordThisLine = sArr[1];
					Integer ICountThisWordThisLine = new Integer (sArr[0]);
					try 
					{
						ICountThisWordThisLine = new Integer ( sArr[0] );
					}
					catch ( Exception e )
					{
						Log.FatalError ("bad Int parse", e);	
					} 
						
					// repeats vcan occur in the BNC doc - add em up
					if ( sWordThisLine.equals (sWordLastLine) || sWordLastLine.equals("") )
					{
                        iCurrentWordAccumulated += ICountThisWordThisLine.intValue();
						iTextRepeat++;
					}
					else // this word is different - output last word stats
					{
						if ( iCurrentWordAccumulated >= iSigMinCountInclusive )
						{
							Integer ICurrentWordAccumulated = new Integer (iCurrentWordAccumulated);
							htWordCounts_String_to_Integer.put ( sWordLastLine, ICurrentWordAccumulated );
							//Log.logClear ("adding \t"+sWordLastLine + "\t" + iCurrentWordAccumulated + "\r\n");
							if ( bCollectOrderedListAlso )
							{
								Vector vWordsThisCount = (Vector) htCountsToStrVecs.get ( ICurrentWordAccumulated );
								//Log.logClear ("\t"+sWordLastLine + "\t" + iCurrentWordAccumulated + "\tremove\r\n");
								if ( vWordsThisCount == null )
								{
									vWordsThisCount = new Vector();
									htCountsToStrVecs.put (ICurrentWordAccumulated, vWordsThisCount);
								}
								vWordsThisCount.addElement(sWordLastLine);
							}							
							iCount++;

						}
						//else // was below threshhold - infrequent word - don't store all those infrequent words 
						//{
							//if ( model == ICOUNTFILE_MODEL_IW )
								//break;  // assumes descending file for IW model anyway	
						//} 
						iCurrentWordAccumulated = ICountThisWordThisLine.intValue();
					}
					sWordLastLine = sWordThisLine;
				}
				else
				{
					iTextEliminated++;	
				}
			}
			
			if ( iLinesRead < 80000 )
				Log.logClear ("bad : iLinesRead < 80000 : " + iLinesRead);
			
			in.close();
          long lDur = (System.currentTimeMillis()-lStart) ;
			api.Log.Log ("DONE word freq ms [" + lDur + "] icount [" + iCount + "] linesread [" + iLinesRead+ "] iTextEliminated + [" + iTextEliminated+
            "] iTextRepeat [" + iTextRepeat + "] ht [" + htWordCounts_String_to_Integer.size() + "]" );
			//			  " iCount + iTextEliminated + iTextRepeat [" + (iCount + iTextEliminated + iTextRepeat) + "] remove\r\n");
		} 
		
		catch ( Exception  e ) 
		{
			Log.FatalError ("Exception in word freq constructor", e ); 
		}		
	} 
	
	// **********************************************
	private static String[] parseLine ( String s, int model, int iLineNum )
	// **********************************************
	{
		String[] sArrReturn = new String[2];
		// count and word 
		try 
		{
			if ( model == ICOUNTFILE_MODEL_BNC )
			{
				int index = 0;
				while ( true )
				{
					int endLoc = s.indexOf ( " " ) ;
					if (endLoc == -1) // end of line 
					{
						break; // last element
					} 
					else if ( index == 0 ) // count
					{
						sArrReturn[0] = s.substring ( 0, endLoc );	
						s = s.substring ( endLoc + 1  ) ;
					} 
					else if ( index == 1 ) // word
					{
						sArrReturn[1] = s.substring ( 0, endLoc );	
						break;
					}
					index++;
				} 
			}
			else if ( model == ICOUNTFILE_MODEL_IW ) 
			{
				int index = 0;
				while ( true )
				{
					int endLoc = s.indexOf ( "\t" ) ;
					if (endLoc == -1) // end of line 
					{
						break; // last element
					} 
					else if ( index == 0 ) // word
					{
						sArrReturn[1] = s.substring ( 0, endLoc );	
						s = s.substring ( endLoc + 1  ) ;
					} 
					else if ( index == 1 ) // count
					{
						sArrReturn[0] = s.substring ( 0, endLoc );	
						break;
					}
					index++;
				} 
			} 
		}
		catch ( Exception e ) 
		{
			Log.FatalError ( "can't parse line [" + s + "] line # [" + iLineNum + "]", e );
		}	
					
		return sArrReturn;
	}
	
	private static boolean eliminateLine ( String textline, int model ) 
	{
		if ( model == ICOUNTFILE_MODEL_BNC ) 
		{
			if (	textline.indexOf( "&") == -1 &&
					textline.indexOf( "/") == -1 &&
					textline.indexOf( "%") == -1 &&
					textline.indexOf( "+") == -1 &&
					textline.indexOf( "*") == -1 &&
					textline.indexOf( ":") == -1 &&
					textline.indexOf( "--") == -1 &&
					textline.indexOf( " -") == -1 &&
					textline.indexOf( ",") == -1 &&
					textline.indexOf( "_") == -1 &&
					textline.indexOf( ".") == -1 &&
					textline.indexOf( ";") == -1 &&
					textline.indexOf( ")") == -1 &&
					textline.indexOf( "(") == -1 &&
					textline.indexOf( "!") == -1 &&
					textline.indexOf( "'") == -1 ) 
				return false;
			else
				return true;
		}
		else // ICOUNTFILE_MODEL_IW
			return false;
			
	
	} 
	
	public void printWordsAndWeights ( String sCommentHeader, 
											  String sFileName 
											   )
		throws Exception
	{
		
		UtilFile.deleteFile ( sFileName ) ;
		
		// first sort the set of Integes 
		Integer[] IArr = new Integer[htCountsToStrVecs.size()];
		Enumeration e = htCountsToStrVecs.keys();
		{
			int i = 0;
			while ( e.hasMoreElements() ) 
			{ 
				IArr[i] = (Integer) e.nextElement();
				i++;
			} 
		}
		com.indraweb.util.sort.SortIntegers.sortIntArr ( IArr );
		
		// now that they're sorted by count - get the vector for each count 
		// and sort that and print the word and the count 
		PrintWriter pfile = new PrintWriter ( new BufferedWriter( new FileWriter( sFileName, true )));	
		pfile.write (sCommentHeader);
		try 
		{
			// for each count value 
			for ( int i2 = 0; i2 < IArr.length; i2++ )
			{
				Vector vStrWordsThisCount = (Vector) htCountsToStrVecs.get ( IArr[i2] );
				// now sort the strings where there are multiple words per count value
				com.indraweb.util.sort.SortStrings.sortStringVec (vStrWordsThisCount);
				
				// for each word within count 
				for ( int j = 0 ; j < vStrWordsThisCount.size(); j++ )
				{
					pfile.write ( IArr[i2] + " " + vStrWordsThisCount.elementAt (j) + "\r\n" );
				} 
			} 
		}
		finally
		{
			if ( pfile != null )
			{
				pfile.close();	
			} 
		} 
	} 
}
