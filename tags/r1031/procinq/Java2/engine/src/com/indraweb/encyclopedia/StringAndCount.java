package com.indraweb.encyclopedia;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

import com.indraweb.util.UtilStrings;
import com.indraweb.util.UtilStrings;
import com.indraweb.signatures.Signature;

import com.ms.util.ArraySort;

public class StringAndCount
{
	
	public String word;
 	public int  count;

    public static int iNumtimesSCConstructorCalled = 0; // hbk control 2002 12 27
	public StringAndCount (String w, int c)
	{
		word = w;
		count = c;
        iNumtimesSCConstructorCalled++; // hbk control 2002 12 27
        if ( iNumtimesSCConstructorCalled  % 50000 == 0 )
            System.out.println( "StringAndCount constructor #:" + iNumtimesSCConstructorCalled );
	}

    public static String toString ( String[] sArr, int[] iArr )
    {
        Vector vSC = new Vector();
        for ( int i = 0; i < sArr.length; i++)
        {
            vSC.addElement(new StringAndCount (sArr[i], iArr[i]));
        }
        StringAndCount[] scarr = new StringAndCount[vSC.size()];
        vSC.copyInto(scarr );
        return toString(scarr);

    }
	// ***************************************************
	public static StringAndCount[] convertHTWordCounts_ToSortedStrandCount ( Hashtable htWordCounts,
																boolean bRemoveNumbers
																)
	// ***************************************************
	{
		Vector vSandC = new Vector();
		Enumeration e = htWordCounts.keys();
		while ( e.hasMoreElements () )
		{
			String s = (String) e.nextElement();
			if ( !bRemoveNumbers || !UtilStrings.isNumericInteger ( s ) )
			{
				Integer ICount = (Integer) htWordCounts.get ( s );
				int iCount = ICount.intValue();
				StringAndCount sandc = new StringAndCount (s, iCount);
				vSandC.addElement ( sandc );
			}
		} 
		
		StringAndCount[] sandcArr = new StringAndCount[vSandC.size()] ;
		vSandC.copyInto (sandcArr);
		ArraySort.sort ( sandcArr, new Signature.comparisonClassStringAndCount ( Signature.comparisonClassStringAndCount.DESC ) );
		return sandcArr;
 	
	} 
	
// ***************************************************
    public static String convertStringAndCountObjectArray ( Object[] oArr, String sDelim )
	// ***************************************************
	{
		StringBuffer sb = new StringBuffer();
		if ( oArr != null )
		{
			for ( int i = 0; i < oArr.length; i++ )
			{
				if ( i > 0 )
					sb.append ( sDelim );
				StringAndCount sc = (StringAndCount) oArr[i];
				sb.append ( sc.word+"#"+sc.count );
			}
		}
		return sb.toString();
	}

// ***************************************************
    public static StringAndCount[] getScArrFromVecs ( Vector vStr, Vector vInt)
	// ***************************************************
	{
        Vector vSC = new Vector();
        for ( int i = 0; i < vStr.size(); i++)
        {
            vSC.addElement(new StringAndCount ((String) vStr.elementAt(i), ((Integer)vInt.elementAt(i)).intValue()));
        }
        StringAndCount[] scarr = new StringAndCount[vSC.size()];
        vSC.copyInto(scarr );
        return scarr;
	}

    public static String toString (Vector vStr, Vector vInt)
    {
        StringAndCount[] scarr = getScArrFromVecs(vStr, vInt);

        return toString(scarr) ;

    }

    public String toString ()
    {
        return "s [" + word + "] [" + count + "]";
    }

    public static String toString ( StringAndCount[] scArr)
    {
        StringBuffer sb = new StringBuffer("\r\n");
        for (int i = 0; i < scArr.length; i++ )
        {
            sb.append("sigs "+i + ". " + scArr[i].toString() + ";\r\n");
        }
        return sb.toString();
    }

}
