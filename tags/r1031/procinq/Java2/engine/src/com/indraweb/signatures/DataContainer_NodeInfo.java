/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 3, 2004
 * Time: 5:27:40 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.signatures;

public class DataContainer_NodeInfo
{
    public String sNodeTitle = null;
    public String sNodeTitleParent = null;
    public int iNodeIDParent = -1;
    public int iNodeSize = -1;

    int iCallCtr = 0;

    public void printMe ( String sCallerDesc )
    {
        iCallCtr++;
        api.Log.Log ( "\r\n\r\n===== DataContainer_NodeInfo PRINTME # [" + iCallCtr + "." + sCallerDesc + "] DataContainer_NodeInfo ==============================================" );
        api.Log.Log ( "sNodeTitle [" + sNodeTitle + "]" );
        api.Log.Log ( "sNodeTitleParent [" + sNodeTitleParent + "]" );
        api.Log.Log ( "iNodeIDParent [" + iNodeIDParent + "]" );
        api.Log.Log ( "iNodeSize [" + iNodeSize + "]" );
        //api.Log.Log ("hsTitleTerms [" + UtilSets.hsToStr(hsTitleTerms, ";", false)+ "]");
        //api.Log.Log ("vt [" + UtilSets.vToStr(vTitleTerms)+ "]");


        api.Log.Log ( "===== END DataContainer_NodeInfo PRINTME DataContainer_NodeInfo ==============================================\r\n" );
    }
}

