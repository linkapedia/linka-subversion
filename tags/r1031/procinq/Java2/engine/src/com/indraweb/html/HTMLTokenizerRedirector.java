package com.indraweb.html;

import com.indraweb.network.ExceptionStreamGenFail;
import com.indraweb.network.InputStreamsGen;
import com.indraweb.util.Log;
import com.indraweb.util.UtilStrings;
import com.ms.util.HTMLTokenizer;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

public class HTMLTokenizerRedirector {

    /**
     * Returns an HTMLTokenizer post redirect as specified
     *
     * @param isTopStrm an HTTP stream
     * @param bFollowRedirects determines if we follow an html/http redirect
     * @param urlHtmlAttributes_
     * @param iTimedSocketTimeout_
     * @param iRecurseLevel
     * @param bVerbose
     * @return
     * @throws ExceptionStreamGenFail
     * @throws Exception
     */
    public static HTMLTokenizer getTokenizer(InputStream isTopStrm,
            boolean bFollowRedirects,
            UrlHtmlAttributes urlHtmlAttributes_,
            int iTimedSocketTimeout_,
            int iRecurseLevel,
            boolean bVerbose)
            throws ExceptionStreamGenFail, Exception {
        if (isTopStrm == null) {
            Log.FatalError("HTMLTokenizerRedirector.java got null stream\r\n");
        }

        HTMLTokenizer lHTMLTok = new HTMLTokenizer(isTopStrm);
        String strNextLine;

        try {
            lHTMLTok.nextToken();
            strNextLine = lHTMLTok.toString(); // get another line from the HTML tokenizer
            if (bFollowRedirects) {
                int iLocationIndex = strNextLine.toLowerCase().indexOf("location: ");
                if (iLocationIndex >= 0) //  if found
                {
                    urlHtmlAttributes_.setURL(UtilStrings.getsUpToNthOfThis_1based(strNextLine.substring(iLocationIndex + 10), " ", 1));
                    isTopStrm = InputStreamsGen.getDirectInputStreamFromURL(urlHtmlAttributes_, iTimedSocketTimeout_, bVerbose);

                    if (isTopStrm != null) {
                        iRecurseLevel = iRecurseLevel + 1;

                        if (iRecurseLevel > 5) {
                            throw new ExceptionStreamGenFail("RECURSE ERROR in HTMLTokenizerRedirector.java on url [" + urlHtmlAttributes_.getsURL() + "]\r\n");
                        }

                        lHTMLTok = HTMLTokenizerRedirector.getTokenizer(
                                isTopStrm,
                                bFollowRedirects,
                                urlHtmlAttributes_,
                                iTimedSocketTimeout_,
                                iRecurseLevel,
                                bVerbose);
                    } else {
                        throw new ExceptionStreamGenFail("isTopStrm == null in HTMLTokenizerRedirector.java on url [" + urlHtmlAttributes_.getsURL() + "]\r\n");
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace(System.err);
            throw e;
        } catch (IOException e) {
            e.printStackTrace(System.err);
            throw e;
        }
        return lHTMLTok;
    }
}
