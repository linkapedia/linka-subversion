		
package com.indraweb.util;

import java.io.*;
import java.util.*;
import com.indraweb.execution.Session;

public class UtilFile
{
	
	public static String createTempFile ( String sSourceFileName, 
										  String sTempFolderNoTrailSlash,
										  String sFileNameTrailerWithPeriod )
		throws Exception
	{

		//Log.logClear("creating folder : " + sTempFolderNoTrailSlash + "\r\n");
		if ( !bDirExists ( sTempFolderNoTrailSlash ) )
			UtilFile.createFolder ( sTempFolderNoTrailSlash );
		long lTimeFileNameTemp = System.currentTimeMillis();
		String sTempFileNameFQ = null;
		
		// now create temp file and modify it 
		int i = -1;
		for ( i = 0; i < 1000; i++ )
		{
			sTempFileNameFQ = sTempFolderNoTrailSlash + "/" + (lTimeFileNameTemp + i) + sFileNameTrailerWithPeriod ;	
			if ( !UtilFile.bFileExists(sTempFileNameFQ) )
				break;
		} 
			
		if ( i == 1000 ) 
			throw new Exception ("error can't create temp file [" + sTempFileNameFQ + "]");
		
		fileCopy ( sSourceFileName, sTempFileNameFQ );
		//Log.logClear("createTempFile sSourceFileName to sTempFileNameFQ [" + 
			//		 sSourceFileName + " to " + sTempFileNameFQ  + "\r\n");
		return sTempFileNameFQ;
			
	} 
	

	// ---------------------------------------------------------------------------	
	public static int renameFileAsBackup ( String sFileName ) 
		throws Exception
	// ---------------------------------------------------------------------------	
	{
		
		String sFileNameBack = null;
		int i = -1;
		for( i = 0; i < 3000; i++ )
		{
			sFileNameBack = UtilStrings.replaceStrInStr ( sFileName, ".","_" + i + "." );
			if ( !UtilFile.bFileExists ( sFileNameBack ) )
				break;
		}
		if ( i == 3000 )
		{
			Utilemail2.sendMail ("hkon@indraweb.com", 
				"info@indraweb.com",
				"Too many log files machine [" + Session.cfg.getProp ("MachineName") + "]" ,
				"subject has all conent",
				Session.cfg.getProp ("SMTPServer")
				 );
			Utilemail2.sendMail ("mpuscar@indraweb.com", 
				"info@indraweb.com",
				"Too many log files machine [" + Session.cfg.getProp ("MachineName") + "]" ,
				"subject has all conent",
				Session.cfg.getProp ("SMTPServer")
				 );
			Log.FatalError ("can't backup file - already files named like filename_1.txt to filename_1000.txt");
		}
										
		UtilFile.renameFile ( sFileName,sFileNameBack );										
		return i;  // renamed to 
	} 
	
	
	
	// ---------------------------------------------------------------------------	
	public static String getDirFromFQFileName ( String sFQFileName ) 
	// ---------------------------------------------------------------------------	
	{
		int iLocLastSlash = sFQFileName.lastIndexOf ( "/" ) ;
		return sFQFileName.substring ( 0, iLocLastSlash );
	} 
	
	// ---------------------------------------------------------------------------	
	public static String getFileNameFromFQFileName ( String sFQFileName ) 
	// ---------------------------------------------------------------------------	
	{
		int iLocLastSlash = sFQFileName.lastIndexOf ( "/" ) ;
		return sFQFileName.substring ( iLocLastSlash+1 );
	} 

	// ---------------------------------------------------------------------------	
	public static String getFileNameNoExtFromFQFileName ( String sNFQFileName ) 
	// ---------------------------------------------------------------------------	
	{
		int iLocLastSlash = sNFQFileName.lastIndexOf ( "/" ) ;
		int iLocLastDot = sNFQFileName.lastIndexOf ( "." ) ;
		return sNFQFileName.substring ( iLocLastSlash+1,iLocLastDot );
	} 

	// ---------------------------------------------------------------------------	
	public static String getFileExtFromFQFileName ( String sNFQFileName ) 
	// ---------------------------------------------------------------------------	
	{
		int iLocLastSlash = sNFQFileName.lastIndexOf ( "." ) ;
		return sNFQFileName.substring ( iLocLastSlash+1 );
	} 

	
	
	
	static Integer intSynchPrints = new Integer (1);
	// ---------------------------------------------------------------------------	
	
	// ---------------------------------------------------------------------------	
	public static void print ( String s ) 
	// ---------------------------------------------------------------------------	
	{
		synchronized ( intSynchPrints ) 
		{
			System.out.print ( s );
			UtilFile.addLineToFile ( Session.cfg.getProp("IndraHome") + "/IndraLog.txt", s ) ; 
		}
	} 
	
	
	// ---------------------------------------------------------------------------	
	public static synchronized boolean deleteLineInFileFirstOccurenceOnly (String sFName, String sLineToDelete) 
		// ---------------------------------------------------------------------------	
	{

	// open source file for read
	// open temp file for write 
	// loop thru source file
	// if line != line then
	//   write to temp file
	
		boolean bWasLineFound = false;
		String sFNameTemp = "c:\\temp\\temp_deleteLineInFileFirstOccurenceOnly.txt";
		try { 
			String textline;
			BufferedReader in = new BufferedReader(new FileReader(sFName));		
			PrintWriter tempFile = new PrintWriter(new BufferedWriter(new FileWriter(sFNameTemp)))	;	
			
			while (in.ready()) {
				textline = in.readLine() ; 
				if	(!textline.equals (sLineToDelete))
					tempFile.write (textline + "\r\n");
				else
					bWasLineFound = true;
			}
			if (!bWasLineFound) 
				Log.FatalError("Line not found in file to delete" + sFName + ", " + sLineToDelete + "\r\n");
					   
			tempFile.close();
			in.close();
			
			try {
				com.ms.wfc.io.File.delete(sFName);
				com.ms.wfc.io.File.copy(sFNameTemp, sFName);
				}
			catch (Exception e) {};
			return bWasLineFound;
		} 
		
		catch (FileNotFoundException e) {
			Log.FatalError ("FileNotFoundException in deleteLineInFileFirstOccurenceOnly " + sFName ); 
		}
		catch (IOException  e) {
			Log.FatalError ("IOException in deleteLineInFileFirstOccurenceOnly " + sFName ); 
		}		
		return bWasLineFound;
	
	} // public static synchronized boolean deleteLineInFileFirstOccurenceOnly (String sFName, String sLineToDelete) 
	
	
	static Integer fileAccessSynchInteger = new Integer(1);
	// -------------------------------------------------------------------
	public static String GetValueFromConfigFile (String filename, String strParmNAme )  
	// -------------------------------------------------------------------
	{
		synchronized ( fileAccessSynchInteger ) 
		{
			String textline;
			try { 
				BufferedReader in = new BufferedReader ( new FileReader ( filename ));		
				
				while (in.ready()) {
					textline = in.readLine() ; 
					if	(textline.trim().startsWith (strParmNAme) ) 
					{
						in.close();
						return (textline.substring (textline.indexOf("[")+1, textline.indexOf("]")));
					}
				}
				
				Log.FatalError ("parameter not found in file [" + 
									filename + "] parameter [" + strParmNAme + "]" );	
						   
				in.close();
				
				
			} 
		
			catch (FileNotFoundException e) {
				Log.FatalError ("FileNotFoundException in Class clsUtils method GetValueFromConfigFile file [" + 
					filename + "] parameter [" + strParmNAme + "]" ); 
			}
			catch (IOException  e) {
				Log.FatalError ("IOException in Class clsUtils method GetValueFromConfigFile file [" + 
					filename + "] parameter [" + strParmNAme + "]" ); 
			}		
			return ("");
		} // synchronized ( fileAccessSynchInteger ) 
		
	}

	
	
	// -------------------------------------------------------------------
	public static boolean bFileExists (String sFN) 
	// -------------------------------------------------------------------
	{
		if ( sFN.toLowerCase().startsWith ("file:///") )
		{
			sFN = UtilStrings.getStrAfterThisToEnd1Based ( sFN, "file:///", 1 );
		}
		else if ( sFN.toLowerCase().startsWith ("file://") )
		{
			sFN = UtilStrings.getStrAfterThisToEnd1Based ( sFN, "file://", 1 );
		}
			
		File f = new File (sFN);
		return f.exists();
	} 
	
	// -------------------------------------------------------------------
	public static long fileSize (String sFN) 
	// -------------------------------------------------------------------
	{
		File f = new File (sFN);
		return f.length();
	} 
	
	// -------------------------------------------------------------------
	public synchronized static void createFolder (String sFolderPath) 
	// -------------------------------------------------------------------
		throws Exception
	{
		
		sFolderPath = UtilStrings.replaceStrInStr(sFolderPath, "\\", "/");
		Vector vPath = UtilStrings.splitByStrLen1(sFolderPath, "/");
		
		StringBuffer sbpath = new StringBuffer();
		for ( int i = 0; i < vPath.size(); i++ )
		{
			String sToAdd = (String) vPath.elementAt(i);	
			if ( UtilStrings.getStrContains ( sToAdd, ":") )
			{
				sbpath.append ( sToAdd );
				continue;
			}
			else
			{
				sbpath.append("/"+sToAdd);
				if ( !bDirExists ( sbpath.toString()) )
				{
					createFolderOneLevel ( 	sbpath.toString() );
				}
					
			}
		} 
	}

	
	public static void createFolderOneLevel ( String sFolderPath ) 
		throws Exception
	{
		
		if ( !bDirExists ( sFolderPath  ))
		{
			File f = new File ( sFolderPath );
			if ( !f.mkdir() )
			{
				Log.FatalError ("unable to create folder [" + sFolderPath + "]\r\n");
				throw new Exception ( "unable to create folder [" + sFolderPath + "]");
			}
		} 
		
	
	} 
	// -------------------------------------------------------------------
	public static boolean bDirExists (String sFN) 
	// -------------------------------------------------------------------
	{
		
		if ( sFN.toLowerCase().startsWith ("file:///") )
		{
			sFN = UtilStrings.getStrAfterThisToEnd1Based ( sFN, "file:///", 1 );
		}
		else if ( sFN.toLowerCase().startsWith ("file://") )
		{
			sFN = UtilStrings.getStrAfterThisToEnd1Based ( sFN, "file://", 1 );
		}
			
		File f = new File (sFN);
		return f.isDirectory();
	} 
	
	// -------------------------------------------------------------------
	public static String sGetLineNumberFromFileZeroBased	 (String fname, int linenum )  
	{
	// -------------------------------------------------------------------
		String textline;
		try { 
			BufferedReader in = new BufferedReader(new FileReader(fname));		
			
			int lineBeingRead = 0;
			while (in.ready()) {
				textline = in.readLine() ; 
				if	(linenum == lineBeingRead) {
					in.close();
					return (textline);
				}
				lineBeingRead++;
			}
			Log.FatalError ("line number [] not found in file [] only went to around []\r\n" + 
									linenum + ", " + fname + ", " + lineBeingRead );	
			in.close();
			return "";
			
		} 
		
		catch (FileNotFoundException e) {
			Log.FatalError ("FileNotFoundException in sGetLineNumberZeroBased" ); 
		}
		catch (IOException  e) {
			Log.FatalError ("IOException in sGetLineNumberZeroBased" ); 
		}		
		return ("");
		
	} // public static String sGetLineNumberFromFileZeroBased	 (String fname, int linenum )  
	

	// -------------------------------------------------------------------------------
	public static void addLineToFileKill (String filename, String lineToadd) {
	// -------------------------------------------------------------------------------
	
		try {
			PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(filename)))	;	
			file.write (lineToadd);
			file.close();
		}
		catch (IOException e) {
			Log.FatalError ("can't add line to file " + filename + ", " + lineToadd, e);
		}
	}	
	
	//-----------------------------------------------------------------------------
	// METHODinsert_line_in_file 
	//  eg, for an activity log
	//-----------------------------------------------------------------------------
	public static int insert_line_in_file (String filename, String lineToInsert, int PositionForInsert) {
	// -------------------------------------------------------------------------------
		// PositionForInsert = 0 for begin of file
		// PositionForInsert = -1 for end of file
						
	
		return 0;
	}
		
	// ---------------------------------------------------------------------------	
	public static synchronized boolean deleteLineInFileStartsWithFirstOccurenceOnly (String sFName, String sLineToDelete) {
	// ---------------------------------------------------------------------------	


	// open source file for read
	// open temp file for write 
	// loop thru source file
	// if line != line then
	//   write to temp file
	
		boolean bWasLineFound = false;
		String sFNameTemp = "c:\\temp\\temp_deleteLineInFileFirstOccurenceOnly.txt";
		try { 
			String textline;
			BufferedReader in = new BufferedReader(new FileReader(sFName));		
			PrintWriter tempFile = new PrintWriter(new BufferedWriter(new FileWriter(sFNameTemp)))	;	
			
			while (in.ready()) {
				textline = in.readLine() ; 
				if	(!textline.startsWith (sLineToDelete))
					tempFile.write (textline + "\r\n");
				else
					bWasLineFound = true;
			}
			if (!bWasLineFound) 
				Log.FatalError("Line not found in file to delete" + sFName + ", " + sLineToDelete + "\r\n");
					   
			tempFile.close();
			in.close();
			
			try {
				com.ms.wfc.io.File.delete(sFName);
				com.ms.wfc.io.File.copy(sFNameTemp, sFName);
				}
			catch (Exception e) {};
			return bWasLineFound;
		} 
		
		catch (FileNotFoundException e) {
			Log.FatalError ("FileNotFoundException in deleteLineInFileFirstOccurenceOnly " + sFName ); 
		}
		catch (IOException  e) {
			Log.FatalError ("IOException in deleteLineInFileFirstOccurenceOnly " + sFName ); 
		}		
		return bWasLineFound;
	
	}

	
	static boolean bSemaphore = false;
	// -------------------------------------------------------------------------------
	public static void addLineToFile (String filename, String lineToadd) {
	// -------------------------------------------------------------------------------
		//System.out.println ("in addline to file [" + filename + "] [" + lineToadd + "]" );
		
		//System.out.println ("PAUSE  400\r\n");
		//clsUtils.pause_this_many_milliseconds ( 400 );
		if ( filename == null )
			System.out.println ("addLineToFile : null filename for line to add of [" + lineToadd + "]" );
		int iTry = 0;
		while ( true ) 
		{
			
			try 
			{
				// works both for existing and new files 
				PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))	;	
				file.write (lineToadd);
				file.close();
					break;
			}
			catch (FileNotFoundException	 e) 
			{
				long lPauseTime =  (long) ( (double) iTry * (double) 1000 ) ;
				if ( iTry < 10 ) 
				{
					Log.NonFatalError (" addLineToFile location 1 will retry can't write - this is retry #[" + iTry + "] and pause (ms) of  [" + lPauseTime + 
								   "] file locked [" + filename + "] text [" + lineToadd +"] retry up to 10 times ", e);
					iTry++;				
					clsUtils.pause_this_many_milliseconds ( lPauseTime ) ;
				}
				else
				{
					Log.FatalError (" addLineToFile location 1 done retrying - GIVE UP on try [" + iTry + "] and pause (ms) of  [" + lPauseTime + 
						"] file locked [" + filename + "] text [" + lineToadd +"] gave up after 10 times ", e);
					
				} 
			}
		catch (Exception	 e) 
		{
			Log.FatalError ("can't add line to file [" + filename + "] text [" + lineToadd +"]", e);
			clsUtils.pause_this_many_milliseconds ( 10000 )  ;
		}
	}	
	}	
	
	// -------------------------------------------------------------------------------
	public static void writeVecStringsToFile (String filename, Vector v) {
	// -------------------------------------------------------------------------------
		try 
		{
			PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))	;	

			Enumeration e = v.elements ();
			
			while ( e.hasMoreElements () ) 
			{
				file.write ( (String) e.nextElement ()  ) ;
			} 
			
			file.close();
		}
		catch (IOException e) 
		{
			Log.printStackTrace_mine (82, e);
			Log.FatalError ("failure in write vecs line to file " + filename );
		}
	}	
	
	// -------------------------------------------------------------------------------
	public static void deleteFile ( String filename ) throws Exception
	// -------------------------------------------------------------------------------
	{
		File x = new File ( filename ) ;
		x.delete();
	} 

	// -------------------------------------------------------------------------------
	public static void renameFile ( String filename, String newName ) throws Exception
	// -------------------------------------------------------------------------------
	{
		File x = new File ( filename ) ;
		File newone = new File ( newName ) ;
		x.renameTo( newone );
	} 

	// -------------------------------------------------------------------------------
	public static void ensureFolderExists ( String folderName ) throws Exception
	// -------------------------------------------------------------------------------
	{
		try 
		{
			if ( !bFileExists ( folderName ) )
			{
				File x = new File ( folderName ) ;
				x.mkdir ();
			}
			
		}
		catch ( Exception e )
		{
			Log.printStackTrace_mine (83, e);	
		} 

		if ( !bFileExists ( folderName ) )
		{
			Log.FatalError ( "can't create folder (one level at a time apparently [" + folderName + "]") ;
		}

	} 
		
	// -------------------------------------------------------------------
	public static int countNumLinesInFileStartWith (String fname, String startCheck )  
	{
	// -------------------------------------------------------------------
		String textline;
		int numMatches = 0;
		BufferedReader in = null;
		try 
		{ 
			in = new BufferedReader(new FileReader(fname));		
			
			while (in.ready()) 
			{
				textline = in.readLine() ; 
				if	(textline.startsWith ( startCheck ))
				{
					numMatches++;
				}
			}
			in.close();
			return numMatches;
			
		} 
		
		catch (FileNotFoundException e) {
			Log.FatalError ("FileNotFoundException in sGetLineNumberZeroBased" ); 
		}
		catch (IOException  e) {
			Log.FatalError ("IOException in sGetLineNumberZeroBased" ); 
		}		
		finally
		{
			try {in.close();} catch ( Exception e ) {Log.printStackTrace_mine (84, e);};
		} 
		return -1;
		
	} // public static String sGetLineNumberFromFileZeroBased	 (String fname, int linenum )  
			
	
	// -------------------------------------------------------------------
	public static int countNumLinesNonBlank	 (String fname )  
	{
	// -------------------------------------------------------------------
		String textline;
		int numMatches = 0;
		BufferedReader in = null;
		try 
		{ 
			in = new BufferedReader(new FileReader(fname));		
			
			while (in.ready()) 
			{
				textline = in.readLine() ; 
				if	( !textline.trim().equals("") )
				{
					numMatches++;
				}
			}
			in.close();
			return numMatches;
			
		} 
		
		catch (FileNotFoundException e) {
			Log.FatalError ("FileNotFoundException in sGetLineNumberZeroBased" ); 
		}
		catch (IOException  e) {
			Log.FatalError ("IOException in sGetLineNumberZeroBased" ); 
		}		
		finally
		{
			try {in.close();} catch ( Exception e ) {Log.printStackTrace_mine (85, e);};
		} 
		return -1;
		
	} // public static String sGetLineNumberFromFileZeroBased	 (String fname, int linenum )  

	
	

	
	
	
	
	
	
	
	// ---------------------------------------------------------------------------	
	public static void addcolumnToFile ( 
										String				fnameTarget, 
										String[]			sColToAdd,
										String				delimiter,
										String				fNameTemp,
										String				colHeader
										) 
	// ---------------------------------------------------------------------------	
	{
		boolean fileExistedAlready = false;
		if (UtilFile.bFileExists ( fnameTarget )	)
			fileExistedAlready = true;
		
		Vector vOutContent = new Vector(); 
		
		try 
		{
			BufferedReader in = null;
			if ( fileExistedAlready ) 
			{
				// reading the existing file
				in = new BufferedReader(new FileReader( fnameTarget ));		
			}
			
			int linenum = 0;
			String textlineIn = null;
			String textlineOut = null;
				
			for ( int i = 0; i <= sColToAdd.length; i++ )  
			{
				String sToAdd = null;
				if ( i == 0 ) 
					sToAdd = colHeader;
				else
					sToAdd = sColToAdd[i-1];
				
				if ( fileExistedAlready ) // if the existing file has as many entries
				{
					if ( in.ready() ) 
					{
						textlineOut = in.readLine() + delimiter + sToAdd + "\r\n";
					} 
					else
					{
						Log.FatalError ("got to a case I thought I never would if Sig is shrinking [" + fnameTarget + "]");
						
					} 
				} 
				else
				{
					textlineOut = delimiter + sToAdd  + "\r\n";
				}
				if ( sToAdd.indexOf ( delimiter ) > -1 ) 
					Log.FatalError ("UtilFile.addColumnToFile() : delimiter in string to add [" + textlineIn + 
										"] line [" + linenum + "]" ) ;
				
				vOutContent.addElement ( textlineOut ); 
				
			} // for ( int i = 0; i < sColToAdd.length; i++ )  

			
			
			
			
			if ( fileExistedAlready )
			{
				while ( in.ready() ) 
				{
					textlineOut = in.readLine() + delimiter +  "\r\n";
					vOutContent.addElement ( textlineOut ) ;
				}				
				in.close();
			}
			
			PrintWriter pwFileOut = null;
			
			if ( fileExistedAlready ) 
				pwFileOut = new PrintWriter(new BufferedWriter(new FileWriter( fNameTemp )))	;	
			else
				pwFileOut = new PrintWriter(new BufferedWriter(new FileWriter( fnameTarget )))	;	
				
			for ( Enumeration e = vOutContent.elements() ;  
				 e.hasMoreElements() ; )
			{
				String s = ( String ) e.nextElement ();
				pwFileOut.write ( s );
			} 
			pwFileOut.close();

			if ( fileExistedAlready ) 
			{
				UtilFile.deleteFile ( fnameTarget );	
				UtilFile.renameFile ( fNameTemp, fnameTarget );
			}
		}  // try 
		catch ( Exception e ) 
		{ 
			Log.FatalError ( "UtilFile.addcolumnToFile() ", e );
		} 
	}
	
	// -------------------------------------------------------------------------------------------
	public static String StripFileAndIndraHome	(String sURL) 
	// -------------------------------------------------------------------------------------------
	{

		
		if (!sURL.toLowerCase().startsWith("file:///"))
		{
			Log.FatalError ("not start w/ file : " + sURL + " in StripFileAndIndraHome");
		}
		
		
		String s2 = new String ("File///" + Session.cfg.getProp("IndraHome") ).toLowerCase();
		
		int x = s2.length();
		
		s2 = sURL.substring (x+1);	
		
		return s2;
	}  // public static String StripFileAndIndraHome	(String sURL) 
	// -------------------------------------------------------------------
	public static Vector getVecOfFileStrings ( String fname )  
	{
	// -------------------------------------------------------------------
		String textline;
		BufferedReader in = null;
		Vector v = new Vector();

		try 
		{ 
			in = new BufferedReader(new FileReader(fname));		
			
			while (in.ready()) 
			{
				textline = in.readLine() ; 
				v.addElement ( textline );
			}
			in.close();
			
		} 
		
		catch ( Exception  e ) 
		{
			Log.FatalError ("Exception in enumFileLines", e ); 
		}		
		return v ;
		
	} // public static String getVecOfFileStrings
	
	// -------------------------------------------------------------------
	public static String getFileAsString ( String fname )  
	{
	// -------------------------------------------------------------------
		String textline;
		BufferedReader in = null;
		Vector v = new Vector();
		StringBuffer sb = new StringBuffer();

		try 
		{ 
			in = new BufferedReader(new FileReader(fname));
			
			while (in.ready()) 
			{
				textline = in.readLine() ; 
				sb.append( textline+ "\r\n");
			}
			in.close();
		} 
		
		catch ( Exception  e ) 
		{
			Log.FatalError ("Exception in enumFileLines", e ); 
		}		
		return sb.toString() ;
		
	} // public static Vector getFileAsString ( String fname )  
	
	// -------------------------------------------------------------------
	public static Enumeration enumFileLines ( String fname )  
	{
	// -------------------------------------------------------------------
		return getVecOfFileStrings(fname).elements();
		
	} // public static String sGetLineNumberFromFileZeroBased	 (String fname, int linenum )  
	// -------------------------------------------------------------------------------
	public static int deleteFilesOnlyRecursive ( String sFolder, boolean bFatalIfCantDeleteOne ) throws Exception
	// -------------------------------------------------------------------------------
	{
		
		Vector v = new Vector();
		long lStart = System.currentTimeMillis();
		UtilFileEnumerator.enumerateFilesInFolder_Recursive ( v, sFolder,  -1, null, null );
		Enumeration e = v.elements();
		int iNumdeleted = 0;
		while ( e.hasMoreElements() )
		{
			String sFileName = null;
			try
			{
				sFileName = (String) e.nextElement();
				File x = new File ( sFileName ) ;
				x.delete();
				iNumdeleted++;
			} 
			catch ( Exception e2 )
			{
				if ( bFatalIfCantDeleteOne )
				{
					throw new Exception ("error mid deletes in [" + sFolder + "] " + 
							"file [" + sFileName + "] " + e2.getMessage() );
				} 
			} 
		} 
		return iNumdeleted;
	} 

	
	public static void fileCopy ( String sFileSource, String sFileTarget)
	{
		String sFileAsAstring = UtilFile.getFileAsString (sFileSource);
		UtilFile.addLineToFileKill (sFileTarget, sFileAsAstring);
	}

    /**
     * getPWofUniqueFileNameTemp takes a disk path and a random generator seed to create a temp
     * file in the path and lock the file.
     *
     * @return Vector contains the filename and the printwriter
     */
    public static Vector getLocked4WriteUniqueTempFile ( String sPath, long lSeed )
    {
        String sFileNameFull = null;
        Vector vRet = new Vector();
        PrintWriter pwReturn = null;
        int i = 0;
        while (true)
        {
            i++;
            // this is a good CS puzzle - how really to do it ? - open the file first
            sFileNameFull = sPath + System.currentTimeMillis () + Thread.currentThread ().getName () + ".tmp";
            // if file already exists
            if (UtilFile.bFileExists (sFileNameFull))
            {
                java.util.Random rand2 = new java.util.Random (lSeed);
                int iRand = UtilRandom.getRandomInt_Min0_MaxSizeMinus1 (rand2 , 100);
                clsUtils.pause_this_many_milliseconds (iRand);
            }
            else
            {
                try
                {
                    pwReturn = new PrintWriter (new BufferedWriter (new FileWriter (sFileNameFull , true)));
                    vRet.addElement(sFileNameFull);
                    vRet.addElement(pwReturn);
                    //writeOracleFileToDisk (iDocIDSource , pwReturn , iNumBytes , dbc);
                    break;

                } catch ( Exception e )
                {
                    e.printStackTrace ();
                }

            }

            if (i % 10 == 0)
            {
                api.Log.Log ("in file name get while loop");
            }
        }
        return vRet;

    }


}