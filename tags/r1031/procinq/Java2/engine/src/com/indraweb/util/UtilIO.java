package com.indraweb.util;

import java.io.*;

public class UtilIO
{
	/**
	 * Utility class - belongs elsewhere - to send stdout and stderr
	 * to a print stream such as a file or to stdout
	 */
	public static void assignoutput ()
		throws IOException
	{

		java.io.FileOutputStream fostream = new java.io.FileOutputStream ( "c:/temp/LogCache.txt", true );
		java.io.PrintStream ps = new java.io.PrintStream( fostream, true );
		System.setOut ( ps ); 
		System.setErr ( ps ); 
		//System.out.println("*********************************************************************" );	
		//System.out.println("\r\n\r\n\r\n" + "Log Started : "  + new java.util.Date() + "\r\n" );	
		
		//object.instanceof (com) 
							  //then cast as ITDTPoslist  com.ms.com.release(((ISTSD)o ).);
		// com or hashtable 
	} // public static void assignoutput ()
    // ------------------------------------------------------------
	public static String stringFromInputStream ( InputStream is )
	// ------------------------------------------------------------
	{
		try
		{
			StringBuffer sb = new StringBuffer ();
			byte[] byteArr = new byte[1000];
			while ( is.read ( byteArr ) > 0 )
			{
				String s = new String ( byteArr ) ;
				sb.append ( s ) ;
			}
			return sb.toString();

		}
		catch ( Exception e)
		{
			Log.printStackTrace_mine (79, e);
			return e.getMessage();
		}
	}



}
