# --------------------
# $Archive: /portfolio construction/portcon/appserver/tramagent/profiler/default.mak $
# $Modtime: 4/28/00 12:17p $
# $Revision: 4 $
# $Author: Hkon $
#
# Makefile for all the
# portfolio construction.portcon.appserver.tramagent.profiler
# project
#

include $(TOOLSINC)/build.inc


PROJSCOPE	= appserver.tramagent.
PROJNAME	= profiler

BUILDAPPSVR	= $(BUILDPORTCON)/appserver
BUILDTRAMAGENT	= $(BUILDAPPSVR)/tramagent
BUILDPROJ	= $(BUILDTRAMAGENT)/$(PROJNAME)

VPATH		= $(BUILDPROJ)

include $(TOOLSINC)/portcon.inc


# ---------------------
# External dependencies
#


# -----------
# Local rules
#

$(OBJS) : $(SRCS)
	$(JAVAC) $(JFLAGS) -make -cdb $(PROJNAME).cdb $?

local : about.build $(OBJS)

all : local
