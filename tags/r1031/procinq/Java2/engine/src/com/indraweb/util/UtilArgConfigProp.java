/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jun 23, 2004
 * Time: 11:02:40 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.util;

import java.util.Hashtable;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class UtilArgConfigProp
{
    // GetArgHash takes command line arguments and parses them into a Hash structure
    public static Hashtable getArgHash ( String[] args ) throws Exception
    {
        Hashtable htHash = new Hashtable ();


        for ( int i = 0; i < args.length; i++ )
        {
            if ( args[i].startsWith ( "-" ) )
            {
                String sKey = ( String ) args[i].substring ( 1 ).trim ();
                String sVal = new String ( "" );

                if ( ( ( i + 1 ) != args.length ) && ( !args[i + 1].startsWith ( "-" ) ) )
                {
                    sVal = ( String ) args[i + 1];
                }
                htHash.put ( sKey.toLowerCase (), sVal );
            }
        }

        return htHash;
    }

    // GetArgHash takes command line arguments and parses them into a Hash structure
    public static Hashtable getPropFromFile ( String sFileName ) throws Exception
    {
        Hashtable htHash = new Hashtable ();
        Properties prop = new Properties ();
        FileInputStream fis = new FileInputStream ( sFileName );
        prop.load(fis);
        fis.close();

        return prop;
    }

    // GetArgHash takes command line arguments and parses them into a Hash structure
     public static void putPropToFile ( String sFileName , Properties prop, String sHeader) throws Exception
     {
         FileOutputStream fos = new FileOutputStream ( sFileName );
         prop.store( fos, sHeader );
         fos.close ();
     }



}
