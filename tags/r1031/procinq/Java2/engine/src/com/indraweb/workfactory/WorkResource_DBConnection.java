package com.indraweb.workfactory;

import java.sql.Connection;
import java.util.Vector;

public class WorkResource_DBConnection extends WorkResource {
    // vector is just a single 

    public WorkResource_DBConnection(Vector vOneWorkResourceUnit_) {
        vOneWorkResourceUnit = vOneWorkResourceUnit_;
    }

    @Override
    public void closeResources() {
        try {
            Connection dbc = (Connection) super.vOneWorkResourceUnit.elementAt(0);
            if (dbc != null) {
                com.indraweb.util.Log.logClear("closing work resource db connection\r\n");
                dbc.close();
            }
        } catch (Exception e) {
            com.indraweb.util.Log.FatalError("in closeResources", e);
        }
    }
}
