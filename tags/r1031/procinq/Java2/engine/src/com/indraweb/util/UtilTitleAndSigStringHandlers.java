package com.indraweb.util;

import com.indraweb.ir.ParseParms;
import com.indraweb.signatures.SignatureParms;

import java.util.Hashtable;
import java.util.HashSet;

public class UtilTitleAndSigStringHandlers
{
    // ******************************************
	public static String[] convertTitleToSigStyleSArr ( String sNodeTitle,
                                                        boolean bStemOn, String sDelims)
	// ******************************************
	{
        return convertTitleToSigStyleSArr (sNodeTitle, bStemOn, -1, sDelims);
	}

    // ******************************************
	public static String[] convertTitleToSigStyleSArr ( String sNodeTitle,
                                                        boolean bStemOn,
                                                        int iMaxWanted_preThes,
                                                        String sDelims)
	// ******************************************
	{
        sNodeTitle = sNodeTitle.toLowerCase() + " ";
        String[] sArrRet = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption (
            sNodeTitle, sDelims, true, true, true, bStemOn, iMaxWanted_preThes );  // delimiter set classify file build title parse

        return  sArrRet;

	}

    // ******************************************
	public static String[] convertTitleToSigStyleSArr_parseparms ( String sNodeTitle,
                                                                   boolean bStemOn,
                                                                   ParseParms parseparms,
                                                                   int iMaxWanted_preThes)
	// ******************************************
	{
        sNodeTitle = sNodeTitle.toLowerCase() + " ";
        String[] sArrRet = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption (
            sNodeTitle, parseparms.getDelimiters(), true, true, true, bStemOn, iMaxWanted_preThes);  // delimiter set classify file build title parse

        return  sArrRet;

	}

	// ******************************************
	public static String convertTitleToTitleForTitleMatch ( String sNodeTitle, boolean bStemOn, String sDelims )
	// ******************************************
	{
		String[] sArrNodeTitle = convertTitleToSigStyleSArr ( sNodeTitle, bStemOn, -1, sDelims);
		StringBuffer sb = new StringBuffer();
		for ( int i = 0; i < sArrNodeTitle.length; i++ )
		{
			sb.append(sArrNodeTitle[i] + " ");			
		} 
		return  sb.toString();
	} 
	
}
