package com.indraweb.util.sort;

public interface IComparator        // from ....xmlio
{
    int compare(Object o1, Object o2) throws Exception;// standard java compare -1, 0, 1
}
