package com.indraweb.html;

import java.util.Date;

public class UrlHtmlAttributes {

    public final static int UrlHtmlAttr_ToCache_INVALID_REQUEST = 1;
    public final static int UrlHtmlAttr_ToCache_CACHE_READ_PENDING = 2;
    public final static int UrlHtmlAttr_ToCache_OPENSTREAMFAIL = 3;
    public final static int UrlHtmlAttr_ToCache_COMPLETED = 4;
    public final static int UrlHtmlAttr_ToCache_FINISHED_TRUNCATED = 5;
    public final static int UrlHtmlAttr_ToCache_TIMEDOUTCompliant = 6;
    public final static int UrlHtmlAttr_ToCache_TIMEDOUTNonCompliant = 7;
    public final static int UrlHtmlAttr_ToCache_FAILED = 8;
    public int rtnCodeForURLRead = -1;
    public String completedReturnCodeInfo = null;
    public String sURL = null;
    String title = null;
    String author = null;
    String text = null;
    Date d = null;
    public long docID = -1;
    //private String returnMsg = null;
    int linkCount;
    int iFileLenCollected;
    int iFileSizeStated;
    int numWords;
    boolean bFileLenClipped;

    public UrlHtmlAttributes(String sURL_) {
        sURL = sURL_;
    }

    public UrlHtmlAttributes(String sURL_, String text_) {
        sURL = sURL_;
        text = text_;
    }

    public UrlHtmlAttributes(String sURL_, long docID_) {
        sURL = sURL_;
        docID = docID_;
    }

    public UrlHtmlAttributes(String sURL_, long docID_, long cacheID_) {
        sURL = sURL_;
        docID = docID_;
    }

    public String getText() {
        return text;
    }

    public String getsURL() {
        return sURL;
    }

    public void setURL(String sURL_) {
        sURL = sURL_;
    }
}
