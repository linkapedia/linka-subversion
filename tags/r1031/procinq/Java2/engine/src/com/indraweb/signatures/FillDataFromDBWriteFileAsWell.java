/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 3, 2004
 * Time: 5:25:46 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.signatures;

import com.indraweb.ir.Thesaurus;
import com.indraweb.ir.ParseParms;
import com.indraweb.util.UtilProfiling;
import com.indraweb.util.UtilSets;
import com.indraweb.encyclopedia.StringAndCount;
import com.iw.scoring.NodeForScore;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;
import com.ms.util.ArraySort;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import org.apache.log4j.Logger;

public class FillDataFromDBWriteFileAsWell {

    private static final Logger log = Logger.getLogger(FillDataFromDBWriteFileAsWell.class);

    public static int fillFromDB_writeSigFile(int iCorpusID, boolean bStemmedAlso, Connection dbc, SignatureParms sigparms, ParseParms parseparms) {
	boolean bverbose = false;
	long lStart = -1;
	int iNumNFSCreatedReturn = 0;
	try {
	    Thesaurus thesaurus = Thesaurus.cacheGetThesaurus(false, iCorpusID, true, dbc);
	    Vector vStrFromDB = new Vector();
	    Vector vIntFromDB = new Vector();
	    String sSQL = " select n.nodeid, signatureword, SIGNATUREOCCURENCES from signature s, node n "
		    + " where signatureoccurences != 0 "
		    + " and n.nodeid = n.linknodeid "
		    + " and n.nodeid = s.nodeid "
		    + " and corpusid = " + iCorpusID
		    + "  order by nodeid asc, SIGNATUREOCCURENCES  desc  ";

	    java.sql.Statement stmt = dbc.createStatement();

	    lStart = System.currentTimeMillis();
	    ResultSet rs = stmt.executeQuery(sSQL);
//            api.Log.Log ( "filling nfs from db Create sig file query loop started, corpus [" + iCorpusID + "]" );

	    // loop thru nodeid, signatureword, SIGNATUREOCCURENCES
	    int iNumReadsCompleted = 0;
	    int iNumNodesEmitted = 0;

	    //int iMaxSigWordCountThisNode = -1;
	    //api.Log.Log ("debug capped iNumNodesEncountered < 1000 ");
	    //api.Log.Log ("debug capped iNumNodesEncountered < 1000  ");
	    //while (rs.next () && iNumNodesEncountered <= 1000 )
	    long lStartLoop = System.currentTimeMillis();
	    //int iLoopNodeCount= 0;
	    // DEBUG
            /*
	    api.Log.Log ("capping iLoopNodeCount< 200 ");
	    while (rs.next () && iLoopNodeCount < 200 )
	     */
	    int iMaxNumNodesToGetFromDBForACorpus = -1;
//            if ( UtilFile.bFileExists ( "/temp/IndraDebugMaxNumNodesToGetFromDBForACorpus.txt" ) )
//            {
//                String sMaxNumNodes = ( String ) UtilFile.getVecOfFileStrings ( "/temp/IndraDebugMaxNumNodesToGetFromDBForACorpus.txt" ).elementAt ( 0 );
//                iMaxNumNodesToGetFromDBForACorpus = Integer.parseInt ( sMaxNumNodes );
//                api.Log.Log ( "debug capping node count per corpus at [" + iMaxNumNodesToGetFromDBForACorpus + "] due to /temp/IndraDebugMaxNumNodesToGetFromDBForACorpus.txt" );
//            }
	    long iNodeIDLastLoop = -1;
	    int iLoop = -1;
	    long iNodeIDCurrent = -1;
	    int iNumSigsFromDBThisNode = 0;
	    while (rs.next()) //            api.Log.Log ("capping iLoopNodeCount< 200 ");
	    //            api.Log.Log ("capping iLoopNodeCount< 200 ");
	    //            api.Log.Log ("capping iLoopNodeCount< 200 ");
	    //            while (rs.next () && iLoop < 300)
	    {
		iLoop++;
		if (iMaxNumNodesToGetFromDBForACorpus > 0 && iLoop > iMaxNumNodesToGetFromDBForACorpus) {
		    break;
		}

		iNodeIDCurrent = rs.getLong(1);
		//api.Log.Log ("rs.next iNodeIDCurrent [" + iNodeIDCurrent + "]" );

		String sWord = rs.getString(2);
		int iWordCountInNode = rs.getInt(3);

		if (bverbose) // hbk 2004 07 21
		{
		    api.Log.Log(iLoop + ". filling mem "
			    + " node [" + iNodeIDCurrent + "] "
			    + " count [" + iWordCountInNode + "] "
			    + " sWord [" + sWord + "] ");
		}
//                if ( iLoop == 0 )
//                {
//                    iNodeIDLastLoop = iNodeID;
//                }
		if (iNodeIDCurrent != iNodeIDLastLoop) {

		    if (iNodeIDLastLoop == -1) // start up head issue for synch o
		    {
			iNumSigsFromDBThisNode = 0;

			iNodeIDLastLoop = iNodeIDCurrent;
		    } else {
			iNumSigsFromDBThisNode = 0;
			// api.Log.Log ("vStrPerNodeDBSigWords [" + UtilSets.vToStr(vStrPerNodeDBSigWords)+ "]" );
			iNumNFSCreatedReturn++;

			//api.Log.Log ("AA. in fill from db - nfs create [" + UtilSets.vToStr(vStrPerNodeDBSigWords)+ "]" );
			//api.Log.Log ("AA. in fill from db - nfs create [" + UtilSets.vToStr(vStrFromDB)+ "]" );
			NodeForScore nfsNew = createNFSFromDBSigs_PreExpand // NOT TAIL
				(
				dbc,
				iNodeIDLastLoop,
				vStrFromDB,
				vIntFromDB,
				iCorpusID,
				bStemmedAlso,
				sigparms,
				thesaurus,
				parseparms);
			Data_NodeIDsTo_NodeForScore.loadDataStructIndexingData_oneNFS(nfsNew);



			if (iNumNodesEmitted % 2000 == 0) // hbk control comment for production
			{
			    api.Log.Log("load corpus [" + iCorpusID
				    + "], node # [" + iNumNodesEmitted
				    + "] ms [" + (System.currentTimeMillis() - lStartLoop)
				    + "]");	 // hbk control
			}
			iNumNodesEmitted++;

			vStrFromDB = new Vector();
			vIntFromDB = new Vector();

			iNodeIDLastLoop = iNodeIDCurrent;

			//iMaxSigWordCountThisNode = -1;
		    }
		} // if new nodeid in loop
		else {
		    iNumSigsFromDBThisNode++;
		}



		// iNumSigsFromDBThisNode  == 1 first time in
		if (sWord != null) //                        &&
		//                        (iNumSigsFromDBThisNode < sigparms.iMaxSigTermsPerNode ||
		//                        sigparms.iMaxSigTermsPerNode  < 0 )
		//                )
		{
		    // do we have the required min # sig occurences
		    // see if we've already found a node having this word
		    //api.Log.Log (" in fillData_FromDB " + iNodeIDCurrent + "\t" + sWord + "\t" + iWordCountInNode + "\r\n");
		    vStrFromDB.add(sWord);
		    vIntFromDB.add(new Integer(iWordCountInNode));

		    //sigContRawDB_cumWithTitles.add ( sWord.toLowerCase (),new Integer ( iWordCountInNode ) );
		    //if ( iWordCountInNode > iMaxSigWordCountThisNode )
		    //    iMaxSigWordCountThisNode = iWordCountInNode;


		} else {
		    api.Log.Log("capped");
		}

		iNumReadsCompleted++;

	    } // while ( rs.next() )

	    if (iNumReadsCompleted > 0) // tail for the non existent final record
	    {
		iNumNFSCreatedReturn++;
		//api.Log.Log ("AA(tail). in fill from db - nfs create [" + UtilSets.vToStr(vStrFromDB)+ "]" );
		//api.Log.Log ("AA(tail). in fill from db - nfs create [" + UtilSets.vToStr(vIntFromDB)+ "]" );
		NodeForScore nfsNew = createNFSFromDBSigs_PreExpand // TAIL
			(
			dbc,
			iNodeIDCurrent,
			vStrFromDB,
			vIntFromDB,
			iCorpusID,
			bStemmedAlso,
			//true,
			sigparms,
			thesaurus,
			parseparms);
		Data_NodeIDsTo_NodeForScore.loadDataStructIndexingData_oneNFS(nfsNew);
	    }
	    api.Log.Log("LOADED corpus from DB ["
		    + iCorpusID
		    + "] # nodes [" + iNumNFSCreatedReturn + "] "
		    + "new mem node count [" + Data_NodeIDsTo_NodeForScore.getNumNodes() + "] ms ["
		    + UtilProfiling.elapsedTimeMillis(lStart) + "] ");
	    rs.close();
	    stmt.close();
	    //pw.write (SignatureFile.CLOSESTRING);

	    //Data_NodeIDsTo_NodeForScore.printDataStructNodeToNFS ( false, null, true );

	} catch (Exception e) {
	    api.Log.LogError("exception file in createfile corpus [" + iCorpusID + "]\r\n", e);
	} finally {
	    /*
	    if (pw != null)
	    pw.close ();
	     */
	}
	return iNumNFSCreatedReturn;

    }

    // this is called when filling the data struct from the db ... pre NFS instantiation
    // then after all NFS are instiantiated ... the file is written
    private static DataContainer_NodeInfo getNodeDataNotSigs(Connection dbc, long iNodeID, int iCorpusID) throws Exception {
	log.debug("getNodeDataNotSigs(Connection, long, int)");
	DataContainer_NodeInfo dcRet = new DataContainer_NodeInfo();

	{ // localize the get of this node title and parentid and nodesize

	    Statement stmt2 = null;
	    ResultSet rs2 = null;
	    log.debug("Searching parent node for node {" + iNodeID + "}");
	    String sSQL2 = "select nodetitle, parentid, nodesize from node where nodeid = " + iNodeID;
	    try {
		stmt2 = dbc.createStatement();
		rs2 = stmt2.executeQuery(sSQL2);
		if (rs2.next()) {
		    dcRet.sNodeTitle = rs2.getString("nodetitle");
		    dcRet.iNodeIDParent = rs2.getInt("parentid");
		    dcRet.iNodeSize = rs2.getInt("nodesize");
		} else {
		    log.error("No Parent found for node{" + iNodeID + "}");
		    throw new Exception("No Parent found for node{" + iNodeID + "}");
		}
		stmt2.close();
		stmt2 = null;
		rs2.close();
		rs2 = null;
	    } catch (Exception ex) {
		log.error("Exception getting node stats: " + ex.getMessage(), ex);
	    } finally {
		if (stmt2 != null) {
		    stmt2.close();
		}
		if (rs2 != null) {
		    rs2.close();
		}
	    }
	    // add node title as full phrase and individual terms to vector

	} // node title sig words
	if (dcRet.iNodeIDParent >= 0) {
	    // localize parent node title
	    Statement stmt2 = null;
	    ResultSet rs2 = null;
	    log.debug("Searching node title for parent node{" + dcRet.iNodeIDParent + "}");
	    String sSQL2 = "select nodetitle from node where nodeid = " + dcRet.iNodeIDParent;
	    try {
		stmt2 = dbc.createStatement();
		rs2 = stmt2.executeQuery(sSQL2);
		if (rs2.next()) {
		    dcRet.sNodeTitleParent = rs2.getString(1);
		} else {
		    log.error("No node title found for parent node{" + dcRet.iNodeIDParent + "}");
		    throw new Exception("No node title found for parent node{" + dcRet.iNodeIDParent + "}");
		}
		stmt2.close();
		stmt2 = null;
		rs2.close();
		rs2 = null;
	    } catch (Exception ex) {
		log.error("Exception getting node stats: " + ex.getMessage(), ex);
	    } finally {
		if (stmt2 != null) {
		    stmt2.close();
		}
		if (rs2 != null) {
		    rs2.close();
		}
	    }
	} // node title sig words
	return dcRet;
    }

    private static NodeForScore createNFSFromDBSigs_PreExpand(
	    Connection dbc,
	    long iNodeID,
	    Vector vStr,
	    Vector vInt,
	    int iCorpusID,
	    boolean bStemmedAlso,
	    //boolean bExpand,
	    SignatureParms sigparms,
	    Thesaurus thesaurus,
	    ParseParms parseparms) throws Exception {
	NodeForScore nfsNew = null;
	try {
	    DataContainer_NodeInfo dc = getNodeDataNotSigs(dbc, iNodeID, iCorpusID);

	    // hbk 2004 07 21 api.Log.Log ("sigs pre max-reduce (pre sig-thes-expand) [" + iNodeID + "] pre max sigs per node reduce [" +UtilSets.vToStr(vStr) + "]" );
	    StringAndCount[] scArr = StringAndCount.getScArrFromVecs(vStr, vInt);
	    comparisonClassStringAndCount sAndC = new comparisonClassStringAndCount(comparisonClassStringAndCount.DESC);


	    ArraySort.sort(scArr, sAndC); // sorty by word count
	    int iMaxSize = sigparms.iMaxSigTermsPerNode;
	    if (scArr.length < iMaxSize) {
		iMaxSize = scArr.length;
	    }

	    // pre expand
//        String[] sArr = new String[iMaxSize];
//        int[] iArr = new int[iMaxSize];
	    Vector vReducedStr = new Vector();
	    Vector vReducedInt = new Vector();
	    for (int i = 0; i < iMaxSize; i++) {
//            sArr[i] = scArr[i].word;
//            iArr[i] = scArr[i].count;
		vReducedStr.addElement(scArr[i].word);
		vReducedInt.addElement(new Integer(scArr[i].count));
		// hbk 2004 07 21 api.Log.Log (i + ". keeping [" + scArr[i].toString() + "]");
	    }
	    // hbk 2004 07 21 api.Log.Log ("sigs post max-reduce pre sig-thes-expand [" + StringAndCount.toString(vReducedStr, vReducedInt)+ "] pre max sigs per node reduce [" +UtilSets.vToStr(vStr) + "]" );

	    //api.Log.Log ("sig cont pre expand and dedup [" +sigContFinal.toString() + "] " );
	    SignaturesContainer sigContFinal = // will be -1 prior if in here
		    SigExpandersTitleThesaurus.sigExpandAndDedup(
		    vReducedStr,
		    vReducedInt,
		    dc.sNodeTitle,
		    dc.sNodeTitleParent,
		    true, //bThesExpandAndSigFilter
		    sigparms,
		    thesaurus,
		    parseparms,
		    false // iNodeID == 1402665
		    );

	    String[] sArrSigTermsFinal = UtilSets.convertVectorTo_StringArray(sigContFinal.getVStr());
	    int[] iArrSigCountsFinal = UtilSets.convertVectorTo_intArray(sigContFinal.getVInt());

	    nfsNew = new NodeForScore(
		    iCorpusID,
		    (long) iNodeID,
		    (long) dc.iNodeIDParent,
		    dc.iNodeSize,
		    sArrSigTermsFinal,
		    iArrSigCountsFinal,
		    bStemmedAlso,
		    sigContFinal.getTitleTermCount(),
		    dc.sNodeTitle,
		    ParseParms.getDelimitersAll_Default());
	} catch (Exception e) {
	    log.error("An exception ocurred. " + e.getMessage());
	    throw e;
	}
	return nfsNew;

    }

    // ************************************************************************
    public static class comparisonClassStringAndCount implements com.ms.util.Comparison {

	public final static int ASC = 1;
	public final static int DESC = -1;
	int order = -2;

	public comparisonClassStringAndCount(int order_) {
	    order = order_;
	}

	public int compare(Object o1, Object o2) {

	    if (((StringAndCount) o1).count < ((StringAndCount) o2).count) {
		return -1 * order;
	    } else if (((StringAndCount) o1).count > ((StringAndCount) o2).count) {
		return 1 * order;
	    } else {
		return order * ((StringAndCount) o1).word.compareTo(((StringAndCount) o1).word);  // hbk 2001 4 27
	    }
	}

	public int compare(StringAndCount o1, StringAndCount o2) {
	    if (o1.count < o2.count) {
		return -1 * order;
	    } else if (o1.count > o2.count) {
		return 1 * order;
	    } else {
		return order * ((StringAndCount) o1).word.compareTo(((StringAndCount) o1).word);  // hbk 2001 4 27
	    }
	}
    }  // public  class comparisonClassStringAndCount
}
