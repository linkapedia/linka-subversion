package com.indraweb.signatures;

import com.indraweb.corpusinstall.CorpusFileDescriptor;
import com.indraweb.database.DBAccess_Central_connection;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.encyclopedia.Encyclopedia;
import com.indraweb.encyclopedia.StringAndCount;
import com.indraweb.encyclopedia.Topic;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.PhraseParms;
import com.indraweb.util.Log;
import com.indraweb.util.UtilFileAndDirNames;
import com.indraweb.util.UtilSets;
import com.ms.util.ArraySort;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

public class Signature {

    // ************************************************************************ 2
    public static void step3_breakIntoSigRangesAndOrthog(
            int iCorpusID_,
            Vector vCFDNodesLeavesOrAll,
            java.sql.Connection dbc_,
            boolean bVerboseSigLogging,
            boolean bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest,
            ParseParms parseParms,
            SignatureParms sigParms,
            PhraseParms phraseParms,
            WordFrequencies wordfreq)
            // ************************************************************************
            throws Throwable {
        //bVerboseSigLogging = true;

        api.Log.Log("step3 vCFDNodesLeavesOrAll.size()  [" + vCFDNodesLeavesOrAll.size() + "] ");

        int iAlreadyFreedForGarbageCollectorUpTo = 0;

        // get enum of int arrs containing: start point, end point, some strange index on cycs can't understand at the moment
        try {

            int iSigGenRangeSize = sigParms.iSigGenRangeSize;
            //int iSigGenRangeSize = 10;
            //int iSigGenRangeSize = 250;
            api.Log.Log("iSigGenRangeSize set to [" + iSigGenRangeSize + "]");
            //sigParms.printme();

            Vector vIntArrStartEndPairs = UtilSets.genCorpusGrouping_IntPairArrs(
                    vCFDNodesLeavesOrAll.size(),
                    iSigGenRangeSize);

            Enumeration eIntArrStartEndPairs = vIntArrStartEndPairs.elements();

            api.Log.Log("Start Cross-set Orthog Run: vCFDNodesLeavesOrAll.size [" + vCFDNodesLeavesOrAll.size() + "] vIntArrStartEndPairs.size [" + vIntArrStartEndPairs.size() + "]");


            int iNumSigWordsAcrossAllBatches = 0;
            int iNumsigSetsGenerated = 0;

            // ******************************************************
            // for all corpus topic groupings
            // ******************************************************
            int iIndex_StartEndPairs = -1;
            while (eIntArrStartEndPairs.hasMoreElements()) {
                iIndex_StartEndPairs++;
                int[] iArr = null;

                long lStartThisBatchOfNodes = System.currentTimeMillis();


                iArr = (int[]) eIntArrStartEndPairs.nextElement();

                int iStartIndex = iArr[0];
                int iEndIndex = iArr[1];

                api.Log.Log("***** create cyc # [" + (iIndex_StartEndPairs + 1) + "] of [" + vIntArrStartEndPairs.size() + "] iStartIndexGTE [" + iStartIndex + "] iEndIndexLTE [" + (iEndIndex - 1) + "]");
                Vector vCFDNodesLeavesOrAll_VectorSlice = UtilSets.getVectorSlice(vCFDNodesLeavesOrAll, iStartIndex, iEndIndex);

                // ##### NEW ENCYC ###################################
                // ##### NEW ENCYC ###################################
                // ##### NEW ENCYC ###################################


                Encyclopedia cyc = new Encyclopedia(
                        iCorpusID_, // send in say 250 of 30000 in a vec
                        vCFDNodesLeavesOrAll_VectorSlice,
                        dbc_,
                        parseParms,
                        wordfreq,
                        sigParms);

                //api.Log.Log ("cyc.getNumTopics() : " + cyc.getNumTopics());
                if (bVerboseSigLogging) {
                    api.Log.Log("Cyc this batch pre sig [" + cyc.toString() + "]");
                }


                api.Log.Log("o. Run # [" + (iIndex_StartEndPairs + 1) + "] of [" + vIntArrStartEndPairs.size() + "] start [" + iStartIndex + "] end [" + iEndIndex + "]");
                int iNumSigWordsThisBatch = -1;
                try {
                    //api.Log.Log ("start create sigs for orthog Run # [" + (iIndex_StartEndPairs+1) + "] (1base) of [" + vIntArrStartEndPairs.size () + "] iStartIndex [" + iStartIndex + "] iEndIndex [" + iEndIndex + "]");


                    iNumSigWordsThisBatch = step3_1_Create_Signatures(
                            cyc,
                            null, // sArTitleOrSigToEmbed_,
                            iCorpusID_,
                            dbc_,
                            bVerboseSigLogging,
                            wordfreq,
                            bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest,
                            parseParms,
                            sigParms,
                            phraseParms);
                    //api.Log.Log ("Num Sig WordsThisBatch [" + iNumSigWordsThisBatch + "]");
//                   api.Log.Log ("done create sigs for orthog Run # " +
//                    " [" + (iIndex_StartEndPairs+1) + "] " +
//                    " of [" + vIntArrStartEndPairs.size () + "] " +
//                    " iStartIndexGTE [" + iStartIndex + "] " +
//                    " iEndIndex [" + iEndIndex + "]");
                } catch (Exception e) {
                    api.Log.LogFatal("error in step2d_Create_Signatures", e);
                }

                // done with all cfd's pre to start - after start may be reused in end condition remember
                // free them for the GC to grab up
                for (int i = iAlreadyFreedForGarbageCollectorUpTo; i < iStartIndex; i++) {
                    vCFDNodesLeavesOrAll.setElementAt(null, i);
                    iAlreadyFreedForGarbageCollectorUpTo = iStartIndex;
                }

                iNumSigWordsAcrossAllBatches += iNumSigWordsThisBatch;

                // updateNodeSize ( vCFDNodesLeavesOrAll_VectorSlice, dbc_, bReleaseTopic);
                for (int i = 0; i < vCFDNodesLeavesOrAll_VectorSlice.size(); i++) {
                    CorpusFileDescriptor cfd = (CorpusFileDescriptor) vCFDNodesLeavesOrAll_VectorSlice.elementAt(i);
                    //api.Log.Log (i + ". CFD post orthog pre delete  topic [" + cfd.toString() + "]" );
                    cfd.setThisCFDTopicAndHTsNull();
                }

                iNumsigSetsGenerated++;
                api.Log.Log(
                        "done signature set [" + (iNumsigSetsGenerated)
                        + "] of [" + vIntArrStartEndPairs.size()
                        + "] start [" + iStartIndex + "] end [" + iEndIndex + "]"
                        + " #Sigs across batches so far [" + iNumSigWordsAcrossAllBatches
                        + "] duration this batch ms [" + (System.currentTimeMillis() - lStartThisBatchOfNodes)
                        + "] range size [" + iSigGenRangeSize
                        + "] cyc.getNumTopics [" + cyc.getNumTopics()
                        + "] total files [" + vCFDNodesLeavesOrAll.size()
                        + "]");
            }  // while ( eIntArrStartEndPairs.hasMoreElements() )

        } catch (Exception e) {
            Log.printStackTrace_mine(1, e);
        }
    } // public static void step3_breakIntoSigRangesAndOrthog ( int iCorpusID_,
    /**
     * take a cyc of some number of nodes and generate the sigs for that set of nodes withing the cyc.dfd's will be cyc.m_saAllTopicsAllWords [ row ] has the words indexed by row
     */
    private static int iCounter_Create_Signatures = 0;

    public static int step3_1_Create_Signatures(
            Encyclopedia cyc,
            String[] sArTitleOrSigToEmbed_,
            int iCorpusID_,
            java.sql.Connection dbc,
            boolean bVerboseSigLogging,
            com.indraweb.signatures.WordFrequencies wordfreq,
            boolean bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest,
            ParseParms parseParms,
            SignatureParms sigParms,
            PhraseParms phraseParms)
            throws Throwable {

//        api.Log.Log ("===== START sig parms at create sig time : ");
//        sigParms.printme();
//        phraseParms.printme();
//        api.Log.Log ("===== END sig parms at create sig time : ");

        char[] cArrWordsMadeOfOnlyTheseWontBeInserted = parseParms.getDelimiters().toCharArray();

        int iNumWordsOfferedAsPossibleSigsForInsert = 0;
        int iNumActualInsertsInclParentTitle = 0;
        try {


            // api.Log.Log ("in create_signatures");
            // api.Log.Log ("orthog set on #topics [" +
            //   cyc.getNumTopics() + "] # words [" +
            //   cyc.m_saAllTopicsAllWords.length + "]\r\n" );
            int iCol;

            // precalc all the best of columns
            int iNumRows_wordsAllTopicsThisCyc = cyc.htWordsAllCorpusTopics.size();

            // ROW WISE / per word
            // not yet filtered, eg
            boolean[][] bArrArrWordsByTopics = step3_1_1_orthogonalizeRowWise(cyc, iNumRows_wordsAllTopicsThisCyc, dbc, bVerboseSigLogging, wordfreq, sigParms);
            // COLUMN WISE / per topic
            int sigsCompleted = 0;

            // now we have a proper rectangular matrix of words in rows by topics in cols

            //*******************************
            // ALL IMPORTANT ORTHOG
            //*******************************
            Vector[] arrVecsOfIID_PerNode = WordNodeMatrixHelper.step3_1_2_organizeWordsKeptPerNode(
                    bArrArrWordsByTopics,
                    cyc,
                    bVerboseSigLogging,
                    sigParms,
                    wordfreq);

            // for sorting sig words within
            comparisonClassStringAndCount sAndC = new comparisonClassStringAndCount(comparisonClassStringAndCount.DESC);
            for (iCol = 0; iCol < cyc.getNumTopics(); iCol++) {
                Topic t = cyc.getCorpusTopic(iCol);
                //api.Log.Log ("topic : " + t.toString());
                iCounter_Create_Signatures++;
                long lNodeID = -1;

                CorpusFileDescriptor cfd = cyc.getCorpusCFD(iCol);
                String sURLLookup = cfd.getURL().substring(8);
                if (bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest) {
                    lNodeID = DBAccess_Central_connection.getNodeIDFromFile(sURLLookup, iCorpusID_, dbc);
                } else {
                    lNodeID = cfd.lNodeID;
                }

                // ADDED BY MAP 5/15/2006
                // If the node status is 2, the node is locked and should not be processed
                int status = JDBCIndra_Connection.executeSQLInt("select nodestatus from node where nodeid = " + lNodeID, dbc);
                if (status == 2) {
                    api.Log.Log("Skipping node: " + lNodeID + " because it is flagged as locked in the database.");
                } else {
                    String sSQLPriorSigDelete = "delete from signature where nodeid = " + lNodeID;
                    JDBCIndra_Connection.executeUpdateToDB(sSQLPriorSigDelete, dbc, false, -1);

                    String sSQLUpdateNodeSize = "update node set dateupdated = sysdate, nodesize =  "
                            + cyc.getCorpusCFD(iCol).getThisCFDTopic(null, wordfreq, sigParms).htWordsDocOnly_noDescendants.size()
                            + " where nodeid = " + lNodeID;
                    //api.Log.Log ("sSQLUpdateNodeSize [" + sSQLUpdateNodeSize + "]" );
                    JDBCIndra_Connection.executeUpdateToDB(sSQLUpdateNodeSize, dbc, false, -1);
                    Vector vIIDPerNode = arrVecsOfIID_PerNode[iCol];
                    if (vIIDPerNode == null) {
                        // api.Log.Log ("do nothing");; // do nothing int idebugme = 0;
                    } else {
                        if ((iCol + 1) % 25 == 0) {
                            dbc.commit();
                        }

                        //Topic topicInCyc = cyc.getCorpusTopic ( col );
                        // CLEANING OF iwSigWords Titles - UtilStrings.cleanAndStripEGTitleTextToLower
                        //api.Log.Log ( "Topic.iTopicObjectCounter [" + Topic.iTopicObjectCounter+ "]" );
                        //if (cfd.getThisCFDTopic (null).bHaveNonTitleText)
                        {
                            //String sFinalTitle = UtilStrings.cleanAndStripEGTitleText ToLower (cyc.getCorpusTopic (iCol).m_DocumentTitle , "(),;-.");

                            Vector vSC_ThisTopic = new Vector();

                            //Integer IColTopic = new Integer (iCol);
                            // get sorted sig arr this topic only

                            // to be used after sig words are ID'd for this topic - to strip out
                            // those frequently occuring
                            for (int iSigWordIndexInNode = 0; iSigWordIndexInNode < vIIDPerNode.size(); iSigWordIndexInNode++) {
                                String sWord = cyc.m_saAllTopicsAllWords[
                                        ((IntIDAndDoubleValue) vIIDPerNode.elementAt(iSigWordIndexInNode)).iRowWordID];
                                // htSignatureWords.put ( m_saAllTopicsAllWords [ row ], new Double ( matrix [ row ][ col ] ) );
                                // hbk 2001 04 19 changed from ht topic local to ht tree global
                                Integer IWordCountInNode = new Integer(((IntIDAndDoubleValue) vIIDPerNode.elementAt(iSigWordIndexInNode)).iWordcountInNode);

                                // commented : do it in the create file piece - not in the insert to DB piece as here
                                // if over corpus-based (BNC or IW WB corpus 5) wordFreqMax
                                // skip - don't add to orthog candidate list if so
                                if (wordfreq.getWord(sWord) != null) {
                                    //api.Log.Log ("word freq sig elim for word [" + sWord+"] word count in node [" +
                                    //  			  IWordCountInNode + "] in node title [" + cyc.getCorpusCFD(iCol).getTitle() +"]\r\n" ); // hbk control
                                    continue;
                                }

                                //configparam signature generator
                                // this is just for terms going into the DB
                                // does not include title or thes terms yet
                                // that happens at file write time

                                String sSigTermToInsert = sigParms.signatureTermQualifierModifier(sWord);
                                if (sSigTermToInsert != null) {
                                    vSC_ThisTopic.addElement(
                                            new StringAndCount(sSigTermToInsert, IWordCountInNode.intValue()));
                                    // String stemword = new String ( clsStemAndStopList.m_fStemWord ( cyc.m_saAllTopicsAllWords [ row ] ) );
                                    ///api.Log.Log (iSigWordIndexInNode  + ". node [" + t.getNodeID()+ "] gets word [" + sSigTermToInsert + "] at count [" + IWordCountInNode + "]" );
                                }

                            } // for (int iSigWordIndexInNode = 0 ; iSigWordIndexInNode < vIIDPerNode.size(); iSigWordIndexInNode++)

                            //                        if ( t.getNodeID() == 100000010 )
                            //                            api.Log.Log ("node [" + t.getNodeID()+ "] writing sigs todb [" + UtilSets.vToStr(vSC_ThisTopic) + "]" );


                            StringAndCount[] scArrSortedSigWordCountThisTopic = new StringAndCount[vSC_ThisTopic.size()];
                            vSC_ThisTopic.copyInto(scArrSortedSigWordCountThisTopic);
                            ArraySort.sort(scArrSortedSigWordCountThisTopic, sAndC); // sorty by word count

                            // *********************************************
                            // Delete previous signatures from the DB
                            // *********************************************

                            //String sURLTail = UtilStrings.getAllAfterLastOfThis (sURLLookup , "/");
                            //sURLLookup = sURLLookup.substring (0 , sURLLookup.length () - ( sURLTail.length () + 1 ));
                            //if (UtilStrings.getStrContains (sURLLookup.toLowerCase () , Session.sIndraHome.toLowerCase ()))
                            //    sURLLookup = UtilStrings.getAllAfterLastOfThis_newForURLCorpusHome (sURLLookup.toLowerCase () , Session.sIndraHome.toLowerCase ());


                            if (lNodeID < 0) {
                                Log.FatalError("node not found for url [" + sURLLookup + "]\r\n");
                            }

                            // *********************************************
                            // Insert signatures to the DB
                            // *********************************************
                            cfd.setSignature(scArrSortedSigWordCountThisTopic);

                            cfd.getParentCFD(); // may have a side effect - so keep ?


                            //Log.logClear ("ready to insert sigs;node index [" + iCounter_Create_Signatures+ "] to db for node [" + lNodeID + "] " +
                            // "] url [" + cyc.getCorpusCFD(iCol).getURL() + "]\r\n" );
                            //                            String sSQLDebug = "select path from nodepath where nodeid = " + lNodeID;
                            //                            String sNodePath = JDBCIndra_Connection.ExecuteQueryAgainstDBStr(sSQLDebug, dbc);
                            //                            api.Log.Log ("insert node to sig id [" + lNodeID + "] " +
                            //                                "path [" + sNodePath + "]" +
                            //                                "scarr [" + StringAndCount.toString(scArrSortedSigWordCountThisTopic) + "]"
                            //                            );

                            // *********************************************
                            // DO SIGNATURE INSERT
                            // *********************************************
                            //api.Log.Log ("pre insert [" + StringAndCount.toString(scArrSortedSigWordCountThisTopic )+ "]" );
                            if (sigParms.signatureNodeQualifier((int) lNodeID, cfd, dbc, wordfreq)) {
                                iNumActualInsertsInclParentTitle += DBAccecssSigWords.insertToDBSigWords(
                                        scArrSortedSigWordCountThisTopic,
                                        dbc,
                                        lNodeID,
                                        sigParms.iNumParentTitleTermsAsSigs,
                                        sigParms.iMaxSigTermsPerNode //iNodeIDParent,
                                        );
                                iNumWordsOfferedAsPossibleSigsForInsert += scArrSortedSigWordCountThisTopic.length;
                            }
                            // check this
                            cfd.setThisCFDTopicAndHTsNull();

                            //else
                            //throw new Exception ("nodeid [" + lNodeID + "] iCounter_Create_Signatures [" + iCounter_Create_Signatures+ "] already has sig terms prior to inserts in corpus [" + iCorpusID_ + "] iNumSigsAlreadyThisNode [" + iNumSigsAlreadyThisNode + "]\r\n");

                            sigsCompleted++;
                            //System.out.print ( iCol + "." +
                            //				   scArrSortedSigWordCountThisTopic.length + "|" ) ;
                        }
                    }
                }
            } // for (col = 0 ; col < iNumCorpusTopics; col++)
            dbc.commit();
            //Log.log ( "completed [" + sigsCompleted + "] topic sigs \n" );

            // double dAvgSigWordsPerTopic = iTotaNumsigWordsAcrossTopics / Session.cfg.getPropInt ("SigGenRangeSize");

        } catch (Exception e) {
            Log.printStackTrace_mine(40, e);
            Log.FatalError("Excepion in step2d_Create_Signatures ", e);
        }

        //api.Log.Log ("done create_signatures");
        //api.Log.Log ("iNumWordsOfferedAsPossibleSigsForInsert [" + iNumWordsOfferedAsPossibleSigsForInsert + "]");
        //api.Log.Log ("iNumActualInserts [" + iNumActualInserts + "]");
        return iNumActualInsertsInclParentTitle;
    }  // create signatures

    /**
     * @param cyc collection of topics
     * @param iNumRows_wordsAllTopicsThisCyc num rows (words) total
     * @param dbc_ db connection if needed (probably not used as of this version 2002 04 10)
     *
     * for each word get an array of integer (col index) and dFreq (word freq in doc) this will be a sorted descending list
     *
     * first dimension = row count / words second dimension = col count / topics
     */
    private static boolean[][] step3_1_1_orthogonalizeRowWise(Encyclopedia cyc,
            int iNumRows_wordsAllTopicsThisCyc,
            java.sql.Connection dbc_,
            boolean bVerboseSigLogging,
            WordFrequencies wordfreq,
            SignatureParms sigparms)
            // ************************************************************************
            throws Exception, Throwable {   // for each word find the topic with the most of those
        String sWord = null;

        //api.Log.Log ("cyc-tostring:"+cyc.toString());
        if (bVerboseSigLogging) {
            api.Log.Log("in orthogonalizeRowWise cyc num topics [" + cyc.getNumTopics() + "] num rows [" + iNumRows_wordsAllTopicsThisCyc + "]\r\n");
        }

        for (int iRow = 0; iRow < iNumRows_wordsAllTopicsThisCyc; iRow++) {
            sWord = cyc.m_saAllTopicsAllWords[iRow];
            //api.Log.Log ("words index [" + iRow + "] word [" + sWord + "]\r\n");
        }
        boolean[][] bArrArrWordsByTopics = new boolean[iNumRows_wordsAllTopicsThisCyc][cyc.getNumTopics()];

        for (int iRow = 0; iRow < iNumRows_wordsAllTopicsThisCyc; iRow++) {
            // contains a sorted filtered set of id's and values
            sWord = cyc.m_saAllTopicsAllWords[iRow];
            bArrArrWordsByTopics[iRow] = getbArr_TopicsHavingThisWord(cyc, sWord, dbc_, bVerboseSigLogging, wordfreq, sigparms);
            //            api.Log.Log (iRow + ". sWord [" + sWord+ "] row [" + iRow + "] bArrArrWordsByTopics [ iRow ] [" +
            //                    UtilSets.bArrToStr ( bArrArrWordsByTopics [ iRow ]) + "]" );
        }
        return bArrArrWordsByTopics;
    }

    // get the columns that hit the threshhold (eg are max) for this word
    // ************************************************************************
    //private static boolean bCalledAlready = false;
    private static boolean[] getbArr_TopicsHavingThisWord(Encyclopedia cyc,
            String word,
            java.sql.Connection dbc_,
            boolean bVerboseSigLogging,
            WordFrequencies wordfreq,
            SignatureParms sigparms)
            // ************************************************************************
            throws Exception, Throwable {
        boolean[] bArrPerTopicThisWord = new boolean[cyc.getNumTopics()];

        for (int iColTopic = 0; iColTopic < cyc.getNumTopics(); iColTopic++) {
            //if (cyc.getCorpusCFD (iColTopic).getHT_CFD_AggregatedWithTree (dbc_).get (word) != null)
            if (cyc.getCorpusCFD(iColTopic).getTopic_htWordsDocOnly_noDescendants(dbc_, wordfreq, sigparms).get(word) != null) {
                bArrPerTopicThisWord[iColTopic] = true;
                if (bVerboseSigLogging) {
                    Log.log("word [" + word + "] in col index [" + iColTopic
                            + "] occurs [" + cyc.getCorpusCFD(iColTopic).getTopic_htWordsDocOnly_noDescendants(dbc_, wordfreq, sigparms).get(word) + "] times "
                            + " in title [" + cyc.getCorpusCFD(iColTopic).getTitle() + "]\r\n\r\n");
                }

            } else {
                bArrPerTopicThisWord[iColTopic] = false;
            }

            //if ( !bCalledAlready  )
            //    api.Log.Log ("cyc.getCorpusCFD (iColTopic).getThisCFDTopic(null).sbAllTextInOne [" +
            //            cyc.getCorpusCFD (iColTopic).getThisCFDTopic(null).sbAllTextInOne + "]" );
        }  // for all topics
        //bCalledAlready = true;
        return bArrPerTopicThisWord;
    } // getbArr_TopicsHavingThisWord

    // ************************************************************************
    public class HTMLDocStringAndScore {
        // ************************************************************************

        String mS;
        public int originalIndex;
        public double mScore;

        HTMLDocStringAndScore(String s, double score) {
            mS = s;
            mScore = score;
        }
    }  // public class HTMLDocStringAndScore

    // ************************************************************************
    public static class comparisonClassStringAndCount implements com.ms.util.Comparison // ************************************************************************
    {

        public final static int ASC = 1;
        public final static int DESC = -1;
        int order = -2;

        public comparisonClassStringAndCount(int order_) {
            order = order_;
        }

        @Override
        public int compare(Object o1, Object o2) {

            if (((StringAndCount) o1).count < ((StringAndCount) o2).count) {
                return -1 * order;
            } else if (((StringAndCount) o1).count > ((StringAndCount) o2).count) {
                return 1 * order;
            } else {
                return order * ((StringAndCount) o1).word.compareTo(((StringAndCount) o1).word);  // hbk 2001 4 27
            }
        }

        public int compare(StringAndCount o1, StringAndCount o2) {
            if (o1.count < o2.count) {
                return -1 * order;
            } else if (o1.count > o2.count) {
                return 1 * order;
            } else {
                return order * ((StringAndCount) o1).word.compareTo(((StringAndCount) o1).word);  // hbk 2001 4 27
            }
        }
    }  // public  class comparisonClassStringAndCount

    // ************************************************************************
    public class comparisonClassHTMLDocStringAndScore implements com.ms.util.Comparison {
        // ************************************************************************

        @Override
        public int compare(Object o1, Object o2) {
            if (((HTMLDocStringAndScore) o1).mScore < ((HTMLDocStringAndScore) o2).mScore) {
                return -1;
            }
            if (((HTMLDocStringAndScore) o1).mScore > ((HTMLDocStringAndScore) o2).mScore) {
                return 1;
            }

            return 0;
        }

        public int compare(HTMLDocStringAndScore o1, HTMLDocStringAndScore o2) {
            if (o1.mScore < o2.mScore) {
                return -1;
            }
            if (o1.mScore > o2.mScore) {
                return 1;
            }

            return 0;
        }
    } // public  class comparisonClassHTMLDocStringAndScore

    // ***********************************************************
    public static void printCDFTree(CorpusFileDescriptor cfd, int iTreeLevel_, java.sql.Connection dbc_,
            WordFrequencies wordfreq, SignatureParms sigparms)
            throws Throwable // ***********************************************************
    {
        try {
            // PRINT ME
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < iTreeLevel_; i++) {
                sb.append(" ");
            }
            Log.log("DIR::  " + sb.toString()
                    + "type[" + cfd.iFileOrDirType + "]"
                    + "node[" + cfd.lNodeID + "]"
                    + "HTloc[" + cfd.getThisCFDTopic(dbc_, wordfreq, sigparms).htWordsDocOnly_noDescendants.size() + "]"
                    + cfd.getTitle() + " : " + UtilFileAndDirNames.StripIndraHome(cfd.getURL()) + "\r\n");
            Hashtable htLoc1 = cfd.getThisCFDTopic(dbc_, wordfreq, sigparms).htWordsDocOnly_noDescendants;
            printHTWords("Loc", htLoc1);

            // PRINT MY FILES
            Vector vCorpusFileDescriptors_children = cfd.getChildrenFiles_not000html();
            Enumeration e_Children = vCorpusFileDescriptors_children.elements();
            while (e_Children.hasMoreElements()) {
                CorpusFileDescriptor cdfChildFiles_not000html = (CorpusFileDescriptor) e_Children.nextElement();
                Log.log("FILE:: " + sb.toString()
                        + "type[" + cdfChildFiles_not000html.iFileOrDirType + "]"
                        + "node[" + cfd.lNodeID + "]"
                        + "HTloc[" + cdfChildFiles_not000html.getThisCFDTopic(dbc_, wordfreq, sigparms).htWordsDocOnly_noDescendants.size() + "]"
                        + cdfChildFiles_not000html.getTitleForceFetch(dbc_, wordfreq, sigparms) + " : "
                        + UtilFileAndDirNames.StripIndraHome(cdfChildFiles_not000html.getURL())
                        + "\r\n");
                Hashtable htLoc = cdfChildFiles_not000html.getThisCFDTopic(dbc_, wordfreq, sigparms).htWordsDocOnly_noDescendants;
                printHTWords("Loc", htLoc);
            }

            // PRINT MY FOLDERS
            vCorpusFileDescriptors_children = cfd.getChildrenFolders();
            Enumeration e_ChildFolderCFDs = vCorpusFileDescriptors_children.elements();
            while (e_ChildFolderCFDs.hasMoreElements()) {
                printCDFTree((CorpusFileDescriptor) e_ChildFolderCFDs.nextElement(), iTreeLevel_ + 1, dbc_, wordfreq, sigparms);
            }
        } catch (Exception e) {
            Log.FatalError("in printtree ", e);
        }
        // above level that we want to orthog at ... just
        //aggregateSignatures ( );
    }

    // ***********************************************************
    public static void printHTWords(String htNAme, Hashtable ht) // ***********************************************************
    {
        Enumeration e = ht.keys();
        int i = 0;
        while (e.hasMoreElements()) {
            i++;
            String s = (String) e.nextElement();

            Integer I = (Integer) ht.get(s);
            //Log.log (i +  ". ht[" + htNAme + "] word [" + s + "] count [" + I + "]\r\n");
        }

    }

    public static boolean isInt(String s) {
        boolean bisInt = true;
        try {
            Integer.parseInt(s);
            //Log.logClear ("pure number - elim word [" + sWord+ "]\r\n");
        } catch (NumberFormatException e) {
            bisInt = false; // don't keep pure numbers
            //Log.logClear ("not a pure number - keep  word [" + sWord+ "]\r\n");
            // wasn't a number - keep it
            // bKeepWord = true already;
        }
        return bisInt;

    }

    public static boolean isDate(String s) {
        boolean bisDate = true;
        StringTokenizer st = new StringTokenizer(s, "-/");
        while (st.hasMoreElements()) {
            String sBetweenDashOrSlash = (String) st.nextElement();
            if (!isInt(sBetweenDashOrSlash)) {
                bisDate = false;  // not a num so not a date format
                break;
            }
        }
        //if ( bisDate )
        //	Log.logClear("is a date[" + s + "]\r\n");

        return bisDate;
    }
}