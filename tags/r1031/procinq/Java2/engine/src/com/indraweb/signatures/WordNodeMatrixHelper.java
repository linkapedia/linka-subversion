package com.indraweb.signatures;

import java.util.*;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilFile;
import com.indraweb.encyclopedia.Encyclopedia;

/*
	Class written 2002 04 10 in the process of changing orthog routines
	Change history
		2002 04 10 written hbk
*/
public class WordNodeMatrixHelper
{
	/**
	 * @param bArrArrWordsByTopics is a 2D boolean[][] incident matrix -
	 * true where a node contains a wordconstant array length rows are words,
	 * cols are nodes
	 *
	 * @param cyc contains the  topics of interest and the basis for this word and topic space
	 *
	 * uses the IntIDAndDoubleValue data struc:
	 * (1) intID,
	 * (2) int count this word in topic and
	 * (3) freq this word within topic
	 * data structure
	 *
	 * @return one array of Vectors, one vec element per topic
	 *
	 */
	public static Vector[] step3_1_2_organizeWordsKeptPerNode ( boolean[][] bArrArrWordsByTopics,
                                                      Encyclopedia cyc,
													  boolean bverboseSigLogging,
                                                      SignatureParms sigParms,
                                                      WordFrequencies wordfreq)
		throws Throwable
	{
		// bverboseSigLogging = true ; // hbk control remove
        //writeMatrix ( bArrArrWordsByTopics, cyc, cyc.m_saAllTopicsAllWords );
		int iNumWordRows =  bArrArrWordsByTopics.length;
		int iNumNodes = cyc.getNumTopics();
		Vector[] arrVecsOfIID_PerNode = new Vector [ iNumNodes ];
		String[] sArrAllTopicsAllWords = cyc.m_saAllTopicsAllWords;
        //api.Log.Log ("sArrAllTopicsAllWords [" + UtilSets.sArrToStr(sArrAllTopicsAllWords) + "]" );

		String sWord = null;
		for ( int iRow = 0; iRow < iNumWordRows; iRow++ )
		{
            boolean bVerbose = false;
			sWord = sArrAllTopicsAllWords[iRow];
//            if ( sWord.equalsIgnoreCase("cookbook"))
//            {
//                api.Log.Log ("starting [" + sWord + "]" );
//                bVerbose = true;
//            }
            //api.Log.Log (iRow + ". organizeWordsKeptPerNode testing word [" +sWord + "]" );
			int iNumNodesCol = bArrArrWordsByTopics[iRow].length;
			Vector vIIDsNodeCountThisWord = new Vector();

            //api.Log.Log ("step3_1_2_organizeWordsKeptPerNode iRow [" + iRow + "] starting [" + sWord + "] sBoolArr : [" + sBoolArr + "]");

            // gen a list of which topics have this word in what amount
            //api.Log.Log ("orthog see which cols keep word [" +sWord + "]" );
			for ( int jCol = 0; jCol < iNumNodesCol; jCol++ )
			{
                //String csTopicTitle = cyc.getCorpusCFD(jCol).getTitle();
				if ( bArrArrWordsByTopics[iRow][jCol])
				{
					int iwordCountTopicI = UtilSets.hash_get_count_for_string (
						cyc.getCorpusCFD(jCol).getTopic_htWordsDocOnly_noDescendants(null, wordfreq, sigParms), sArrAllTopicsAllWords[iRow] ) ;
                    //api.Log.Log ("wc for wd ["+sWord+"] col ["+jCol+"] cnt["+iwordCountTopicI+"]");
                    if ( bVerbose )
                        api.Log.Log ("word ["+sWord+"] appears in topic jcol ["+jCol +"] [" + cyc.getCorpusCFD ( jCol ).getURL() +"] with count ["+iwordCountTopicI+"] ");
					int iFreqDenom = cyc.getCorpusCFD ( jCol ).getTopic_htWordsDocOnly_noDescendants( null , wordfreq, sigParms).size();
					double dFreq = (double) iwordCountTopicI/ (double) iFreqDenom;
					if (bVerbose || bverboseSigLogging)
						api.Log.Log ("irow [" + iRow + "] word [" + sWord +
													"] iwordCountTopicI [" + iwordCountTopicI + "] iFreqDenom [" + iFreqDenom + "] dFreq [" + dFreq + "] node index jCol [" +  jCol + "] to vIIDsThisWord.size [" + (vIIDsNodeCountThisWord.size() + 1) + "]\r\n");
					IntIDAndDoubleValue temp_IntIDAndDoubleValue =
						new IntIDAndDoubleValue (jCol, iRow, iwordCountTopicI, dFreq);
                    if ( bVerbose )
                        api.Log.Log ("add that a node has a term temp_IntIDAndDoubleValue [" + temp_IntIDAndDoubleValue.toString() + "]\r\n");

					vIIDsNodeCountThisWord.addElement ( temp_IntIDAndDoubleValue );
				}
			} // for all topics

			IntIDAndDoubleValue[] arrSortedIID_NodeCountThisWord =
				IntIDAndDoubleValue.sortVecIntIDAndDoubleValues ( vIIDsNodeCountThisWord );

			vIIDsNodeCountThisWord = step3_1_2_1_filterWordsPerNode ( arrSortedIID_NodeCountThisWord, sigParms.dSigTieDifferenceEpsilonPercent );

			// now add each of the kept words to the respective topic vectors of iid
			Enumeration e = vIIDsNodeCountThisWord.elements();
			while ( e.hasMoreElements() )
			{
				IntIDAndDoubleValue iidWordByTopic = (IntIDAndDoubleValue) e.nextElement();
				// here is the tricky inversion piece
				// the iid contains the column index - use it to build per node vectors
				Vector vIIDs_thisNode = arrVecsOfIID_PerNode [ iidWordByTopic.iColNodeID ];
				if ( vIIDs_thisNode == null )
				{
					// if we haven't hit a word for this topic yet
					arrVecsOfIID_PerNode [ iidWordByTopic.iColNodeID ] = new Vector();
					vIIDs_thisNode = arrVecsOfIID_PerNode [ iidWordByTopic.iColNodeID ];
				}
				vIIDs_thisNode.addElement ( iidWordByTopic );
			}
		} // for each row / word

		// now sort descending words per node
		for ( int iColNode = 0; iColNode < iNumNodes; iColNode++ )
		{
			Vector vIntIDAndDoubleValue_thisNode = arrVecsOfIID_PerNode[iColNode];
			// if ANY words were assigned to this doc as a max
			if ( vIntIDAndDoubleValue_thisNode != null )
			{
				IntIDAndDoubleValue[] arrIntIDAndDoubleValue_thisNode = IntIDAndDoubleValue.sortVecIntIDAndDoubleValues ( vIntIDAndDoubleValue_thisNode );
				arrVecsOfIID_PerNode[iColNode] = UtilSets.convertObjectArrayToVector ( arrIntIDAndDoubleValue_thisNode );
				for ( int jIIDIndexThisNode = 0;
					  jIIDIndexThisNode < arrVecsOfIID_PerNode[iColNode].size();
					  jIIDIndexThisNode++ )
				{
					int iRowWordIndex = ((IntIDAndDoubleValue) arrVecsOfIID_PerNode[iColNode].elementAt(jIIDIndexThisNode)).iRowWordID;
					if (bverboseSigLogging)
						com.indraweb.util.Log.logClear("node index [" + iColNode + "] title [" + cyc.getCorpusCFD(iColNode).getTitle() + "] at iid [" +
												   jIIDIndexThisNode + "] has word [" + iRowWordIndex + "." +
												   sArrAllTopicsAllWords[iRowWordIndex] + "]\r\n" );
				}
			}
		}



		return arrVecsOfIID_PerNode;
	}

	/**
	 * filters which nodes get to keep this word in their sig list
	 * currently based on a tie for max as the criteria
	 *
	 * hbk 2002 04 28 this is the function during orthog which word-freq-limits the
	 * sig set
	 *
	 */
	private static Vector step3_1_2_1_filterWordsPerNode
		( IntIDAndDoubleValue[] arrSortedIntIDAndDoubleValue_ThisWord,
          double dPercentEpsilon )
	{
        //String sWordDebugOnly)

		Vector vIntIDAndDoubleValue = new Vector();
		double dMaxFreq = -1;
        //api.Log.Log ("term [" + sWordDebugOnly + "] in orthog, term array [" + IntIDAndDoubleValue.toString( arrSortedIntIDAndDoubleValue_ThisWord) + "]" );
		for ( int i = 0; i < arrSortedIntIDAndDoubleValue_ThisWord.length; i++ )
		{
			if ( i == 0 )
			{
				dMaxFreq = arrSortedIntIDAndDoubleValue_ThisWord[i].dWordFreqInNode;
				vIntIDAndDoubleValue.addElement ( arrSortedIntIDAndDoubleValue_ThisWord[i] );
			}
			else
			{
				double dThisFreq	 = arrSortedIntIDAndDoubleValue_ThisWord[i].dWordFreqInNode;
				double dPercentDiff = java.lang.Math.abs ( (double) 100 * (dMaxFreq - dThisFreq) / dMaxFreq);
				if ( dPercentDiff < dPercentEpsilon )
				{
					vIntIDAndDoubleValue.addElement ( arrSortedIntIDAndDoubleValue_ThisWord[i] );
				}
				else // sorted desc., so first one outside the bounds signals time to stop
					break;
			}
		} // for each col having this word
        //api.Log.Log ("word [" + sWordDebugOnly + "] kept by [" + vIntIDAndDoubleValue.size() + "] cols" );
		return vIntIDAndDoubleValue;
	}


    private static void writeMatrix ( boolean[][] bArrArrWordsByTopics,
                                                      Encyclopedia cyc,
                                      String[] sArrTerms,
                                      WordFrequencies wordfreq,
                                      SignatureParms sigparms) throws Throwable
    {

        int iNumWordRows =  bArrArrWordsByTopics.length;
		int iNumNodes = cyc.getNumTopics();
		Vector[] arrVecsOfIID_PerNode = new Vector [ iNumNodes ];
		String[] sArrAllTopicsAllWords = cyc.m_saAllTopicsAllWords;
        //api.Log.Log ("sArrAllTopicsAllWords [" + UtilSets.sArrToStr(sArrAllTopicsAllWords) + "]" );
        String sFileName = "/temp/tempmat.txt";
        int iNumFIleLines = 0;
        api.Log.Log ("writing to file [" + sFileName + "]" );
        iNumFIleLines++;
        StringBuffer sbHeaderRow = new StringBuffer(UtilSets.sArrToStr( sArrTerms ));
        sbHeaderRow.append ( "header"+" "+ new java.util.Date() + "\t");

        for ( int jCol = 0; jCol < iNumNodes; jCol++ )
        {
            sbHeaderRow.append ( cyc.getCorpusTopic(jCol).getNodeID()  + "\t" );
        }
        UtilFile.addLineToFileKill(sFileName, sbHeaderRow.toString()+"\r\n");
        //api.Log.Log ("step3_1_2_organizeWordsKeptPerNode iRow [" + iRow + "] starting [" + sWord + "] sBoolArr : [" + sBoolArr + "]");

        String sWord = null;

        //api.Log.Log ("all words [" + UtilSets.sArrToStr ( sArrAllTopicsAllWords )+ "]" );
		for ( int iRow = 0; iRow < iNumWordRows; iRow++ )
		{
            boolean bVerbose = false;
			sWord = sArrAllTopicsAllWords[iRow];
            //api.Log.Log (iRow + ". organizeWordsKeptPerNode testing word [" +sWord + "]" );
			int iNumNodesCol = bArrArrWordsByTopics[iRow].length;
            if ( iNumNodesCol != iNumNodesCol )
                throw new Exception ("iNumNodesCol != iNumNodesCol ");
			Vector vIIDsNodeCountThisWord = new Vector();

            StringBuffer sbRowOfCountsPerWord = new StringBuffer();

            // gen a list of which topics have this word in what amount
			for ( int jCol = 0; jCol < iNumNodesCol; jCol++ )
			{
                //String csTopicTitle = cyc.getCorpusCFD(jCol).getTitle();
                //if ( bArrArrWordsByTopics[iRow][jCol])
				{
					int iwordCountTopicI = UtilSets.hash_get_count_for_string (
						cyc.getCorpusCFD(jCol).getTopic_htWordsDocOnly_noDescendants(null, wordfreq, sigparms), sArrAllTopicsAllWords[iRow] ) ;
                    //api.Log.Log ("wc for wd ["+sWord+"] col ["+jCol+"] cnt["+iwordCountTopicI+"]");
                    //if ( bVerbose )                 //
                        //api.Log.Log ("word ["+sWord+"] appears in topic jcol ["+jCol +"] with count ["+iwordCountTopicI+"] ");
					int iFreqDenom = cyc.getCorpusCFD ( jCol ).getTopic_htWordsDocOnly_noDescendants( null, wordfreq, sigparms ).size();
					double dFreq = (double) iwordCountTopicI/ (double) iFreqDenom;
					//if (bVerbose || bverboseSigLogging)
					//sbRowOfCountsPerWord.append ( iwordCountTopicI + "\t" );
                    sbRowOfCountsPerWord.append(
                            "iwordCountTopicI [" + iwordCountTopicI +
                            "] barr [" + bArrArrWordsByTopics[iRow][jCol] +
                            "] irow [" + iRow +
                            "] word [" + sWord +
                            "] iFreqDenom [" + iFreqDenom +
                            "] iFreqDenom [" + iFreqDenom +
                            "] dFreq [" + dFreq +
                            "] node index jCol [" +  jCol +
                            "] to vIIDsThisWord.size [" + (vIIDsNodeCountThisWord.size() + 1) + "]\t");
					IntIDAndDoubleValue temp_IntIDAndDoubleValue =
						new IntIDAndDoubleValue (jCol, iRow, iwordCountTopicI, dFreq);
                    if ( bVerbose )
                        api.Log.Log ("temp_IntIDAndDoubleValue [" + temp_IntIDAndDoubleValue.toString() + "]\r\n");

					vIIDsNodeCountThisWord.addElement ( temp_IntIDAndDoubleValue );
				}
			} // for all topics
            UtilFile.addLineToFile ( sFileName, sbRowOfCountsPerWord.toString() +"\r\n");
            iNumFIleLines++;

//			IntIDAndDoubleValue[] arrSortedIID_NodeCountThisWord =
//				IntIDAndDoubleValue.sortVecIntIDAndDoubleValues ( vIIDsNodeCountThisWord );

			// now add each of the kept words to the respective topic vectors of iid
//			Enumeration e = vIIDsNodeCountThisWord.elements();
//			while ( e.hasMoreElements() )
//			{
//				IntIDAndDoubleValue iidWordByTopic = (IntIDAndDoubleValue) e.nextElement();
//				// here is the tricky inversion piece
//				// the iid contains the column index - use it to build per node vectors
//				Vector vIIDs_thisNode = arrVecsOfIID_PerNode [ iidWordByTopic.iColNodeID ];
//				if ( vIIDs_thisNode == null )
//				{
//					// if we haven't hit a word for this topic yet
//					arrVecsOfIID_PerNode [ iidWordByTopic.iColNodeID ] = new Vector();
//					vIIDs_thisNode = arrVecsOfIID_PerNode [ iidWordByTopic.iColNodeID ];
//				}
//				vIIDs_thisNode.addElement ( iidWordByTopic );
//			}
		} // for each row / word
        api.Log.Log ("file has [" + iNumFIleLines+ "] lines");


    }
}
