package com.indraweb.util;

import com.indraweb.execution.ConfigProperties;
import com.indraweb.execution.Session;
import com.indraweb.execution.ThreadInfo;
import com.indraweb.execution.ThreadInfoTestAssertion;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import org.apache.log4j.Logger;

public class Log {
    private static final Logger log = Logger.getLogger(Log.class);

    public static void FatalError(String errorstring, Throwable t) {
        String s = Log.stackTraceToString(t);
        FatalError(" Fatal error string [" + errorstring + "\r\n" + s);
    }

    public static void FatalError(String errorstring) {
        log.fatal(" FATAL ERROR [" + errorstring + "]\r\n");
        ThreadInfoTestAssertion.captureErrorPut("com.indraweb.util.Log.FatalError time [" + new java.util.Date() + "] FATAL ERROR [" + errorstring + "]");
        ConfigProperties c = Session.cfg;

        if (c == null) {
            log.fatal("Session.cfg is null, pausing 2 secs then returning");
            clsUtils.pause_this_many_milliseconds(2000);
            return;
        }

        clsUtils.pause_this_many_milliseconds(500);
        Log.log("EXCEPTION FATAL ERROR [" + errorstring + "] \r\n");
        try {
            String s = ThreadInfo.getInitThreadInfo() + ":" + " EXCEPTION FATAL [ " + new Date() + " " + errorstring + "]\r\n";
            System.out.println("in Log.java errstr [" + s + "]");
            if (errorstring.startsWith("property not set") || errorstring.startsWith("received two config")) {
                log.error(s);
            } else {
                Log.log(new Date() + " MGR FATAL ERROR" + s);
            }

            //MessageBox(0, s +  " \r\nEXITING AFTER KEY HIT IN DOS BOX",         "Fatal Error", 0);   
            //clsUtils.waitForKeyboardInput("FATAL ERROR [" + s +  "] EXITING AFTER KEY HIT IN DOS BOX "  );   
            //		HKonLib.PauseForKeyboardInput();  // give user a chance to see error 
            clsUtils.pause_this_many_milliseconds(1000); // in case of looping etc
        } catch (Exception e) {
            // Thread.currentThread().dumpStack();
            log.fatal(" FatalError ", e);
        }
    }

    //-----------------------------------------------------------------------------
    public static void LogPopup(String statusstring, boolean terminateAfterPopup) //-----------------------------------------------------------------------------
    {
        try {
            String s = "\r\n[" + statusstring + "] " + ThreadInfo.getInitThreadInfo() + ":" + " terminate [" + terminateAfterPopup + "] [ " + new Date() + " ]\r\n";
            Log.log(s);

            //MessageBox(0, s +  " \r\nEXITING AFTER KEY HIT IN DOS BOX",         "Program Checkpoint terminate [" + terminateAfterPopup + "]", 0);   
            //clsUtils.waitForKeyboardInput ( s +  "HIT KEY Program Checkpoint, terminate [" + terminateAfterPopup + "]"           );   
            //HKonLib.PauseForKeyboardInput();  // give user a chance to see error 
        } catch (Exception e) {
            // Thread.currentThread().dumpStack();
            log.error(new Date() + " exception within in LogPopup [" + e.getMessage() + "]");
            try {
                Log.log("at keyboard wait in exception state [" + e.getMessage() + "]\r\n");
            } catch (Exception e2) {
                log.error(" IO error in waitForKeyboardInput");
            }
        }
    }

    //-----------------------------------------------------------------------------
    // NonFatalError with throwable
    //-----------------------------------------------------------------------------
    public static void NonFatalError(String errorstring, Throwable t) {
        //String s = stackTraceToString_oneLine ( e );
        String s = stackTraceToString(t);
        s = errorstring + " :\r\n" + s;
        NonFatalError(s);
    } // public static void NonFatalError (String errorstring, Exception e)

    // -------------------------------------------------------------------
    public static void NonFatalError(String errorstring) // -------------------------------------------------------------------
    {
        String s = ThreadInfo.getInitThreadInfo() + ":" + " NONFATAL ERROR [ " + errorstring + " ] \r\n";
        Log.log(s, true);
        ThreadInfoTestAssertion.captureErrorPut("com.indraweb.util.Log.NonFatalError time [" + new java.util.Date()
                + "] NonFatalError [" + s + "]");
    } // public static void NonFatalError ( String errorstring )

    // -------------------------------------------------------------------
    public static void printStackTrace_mine(int ID, Exception e) // -------------------------------------------------------------------
    {
        // Log.log  ( "StraceID[ " + ID + " ]" + stackTraceToString_oneLine ( e ) + "\r\n");
        Log.log("StraceID[ " + ID + " ]" + stackTraceToString(e) + "\r\n");
    }
    static int iLineNum = 0;
    // -------------------------------------------------------------------

    public static void log(String FName, String Line, boolean showtime, boolean showThread) {
        // -------------------------------------------------------------------
        //if ( !Line.endsWith ( "\r\n" ) )
        //{
        //Log.NonFatalError ("LINE NOT ENDS CRLF - see stderr for stacktrace [" + Line + "]\r\n");
        //Thread.dumpStack();
        //}


        //if ( showThread )
        //	Line = "<" + ThreadInfo.getInitThreadInfo() + "> " +  Line  ;

        log.info(iLineNum + ". " + Line);
        //}
        iLineNum++;

    } // public static void ActivityLog (String FName, String Line, boolean showtime) {

    // -------------------------------------------------------------------
    public static void log(String Line, boolean showtime) {
        // -------------------------------------------------------------------
        log(Session.sLogNameDetail, Line, showtime, true);
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    public static void log(String Line) {
        // -------------------------------------------------------------------
        log(Session.sLogNameDetail, Line, true, true);
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    public static void logcr(String Line) {
        // -------------------------------------------------------------------
        log(Session.sLogNameDetail, Line + "\r\n", true, true);
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    public static void logAgg(String Line) {
        // -------------------------------------------------------------------
        log(Session.cfg.getProp("IndraHome") + "/IndraLogAggregate.txt", Line, true, true);
        log(Line);
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    //public static void logSummary ( String Line ) {
    // -------------------------------------------------------------------
    //log ( Session.sLogNameDetail, Line, true, true );
    //log ( Session.sLogNameSumm, Line, true, true );
    //} // public static void ActivityLog ( String Line, boolean showtime) {
    // -------------------------------------------------------------------
    public static void log(String Line, String filename) {
        // -------------------------------------------------------------------
        log(Session.cfg.getProp("IndraHome") + "/logs/" + filename, Line, false, true);
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    public static void logClear(String Line) {
        // -------------------------------------------------------------------
        log(Session.sLogNameDetail, Line, false, false);
    } // public static void ActivityLog ( String Line, boolean showtime) {
    // -------------------------------------------------------------------

    public static void logClearcr(String Line) {
        // -------------------------------------------------------------------
        log(Session.sLogNameDetail, Line + "\r\n", false, false);
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    public static void logClear(String sFileName, String Line) {
        // -------------------------------------------------------------------
        log(sFileName, Line, false, false);
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    public static void log(String msg, Exception e) {
        // -------------------------------------------------------------------
        String s = stackTraceToString(e) + "\r\n";
        // s = UtilStrings.stringToOneline ( s, '*' );
        log(msg + " ::: " + s);
        ThreadInfoTestAssertion.captureErrorPut("Log.log time [" + new java.util.Date()
                + "] msg + s [" + msg + " ::: " + s + "]");
    } // public static void ActivityLog ( String Line, boolean showtime) {

    // -------------------------------------------------------------------
    //public static void log ( Exception e ) {
    // -------------------------------------------------------------------
    //	String s = stackTraceToString ( e ) ;
    //s = UtilStrings.stringToOneline ( s, '*' );
    //	log ( s );
    //} // public static void ActivityLog ( String Line, boolean showtime) {
    // -------------------------------------------------------------------
    public static String stackTraceToString_oneLine(Exception e) // -------------------------------------------------------------------
    {
        String s = stackTraceToString(e);  //where did my one line code go ?
        // return UtilStrings.stringToOneline ( s, '*' );
        return s;
    } // public static String stackTraceToString_oneLine ( Exception e ) 

    // -------------------------------------------------------------------
    public static String stackTraceToString(Throwable e) // -------------------------------------------------------------------
    {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return "StackTRACE " + sw.toString() + " ";
        } catch (Exception e2) {
            return "bad stack2string";
        }
    }// public static String stackTraceToString ( Exception e ) 		
}
