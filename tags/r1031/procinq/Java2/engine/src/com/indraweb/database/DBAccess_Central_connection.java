package com.indraweb.database;

import com.indraweb.encyclopedia.StringAndCount;
import com.indraweb.util.Log;
import com.indraweb.util.UtilStrings;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;

public class DBAccess_Central_connection {
    // *************************************

    public static synchronized String getNodeTitle(
            // *************************************
            boolean includeParentTitles_,
            boolean cleaned_,
            //boolean xbForceSigKeyTitleWords,
            //boolean bWantDelimiters,
            long lNodeID_,
            Connection dbc)
            throws Exception {

        String sReturn = null;
        sReturn = getNodeTitle_synched_localRecurse(
                lNodeID_,
                includeParentTitles_,
                cleaned_,
                dbc);

        if (sReturn == null) // debug test
        {
            sReturn = getNodeTitle_synched_localRecurse(
                    lNodeID_,
                    includeParentTitles_,
                    cleaned_,
                    dbc);
        }

        return sReturn;
    } // getNodeTitle

    // *************************************
    private static String getNodeTitle_synched_localRecurse(long lNodeID_,
            // *************************************
            boolean includeAncestorTitles_,
            boolean cleaned_,
            Connection dbc)
            throws Exception {

        String sThisNodeTitle = null;

        String sNodeTitle_return = null;

        {


            // get this node title
            // is this the current/active node ?
            sThisNodeTitle = getNodeTitleSimple_NoAncestry(lNodeID_, dbc);

            // get parent node title
            long lCurrParent_NodeID = getParentNodeID_FromNodeID(lNodeID_, dbc);

            if (lCurrParent_NodeID > 0 && includeAncestorTitles_) {
                String sCurrParent_Title = getNodeTitle_synched_localRecurse(
                        lCurrParent_NodeID,
                        includeAncestorTitles_,
                        cleaned_,
                        dbc);

                sNodeTitle_return = sCurrParent_Title + sThisNodeTitle;
            } else {
                sNodeTitle_return = sThisNodeTitle;
            }

            if (cleaned_) {
                sNodeTitle_return = UtilStrings.cleanAndStripEGTitleTextToLower(sNodeTitle_return, "(),;.");
            }


            return sNodeTitle_return;
        }
    }  // private static String getNodeTitle_synched_localRecurse (	long lNodeID_,

    // *************************************
    public static String getNodeTitleSimple_NoAncestry(long lNode_, Connection dbc_)
            // *************************************
            throws Exception {

        String sNodeTitle_local = null;
        {
            String sql = "select nodeTitle from node where nodeid = '" + lNode_ + "'";
            try {
                sNodeTitle_local = JDBCIndra_Connection.executeQueryAgainstDBStr(sql, dbc_);
            } catch (Exception e) {

                boolean bDBclosed = false;
                if (dbc_.isClosed()) {
                    bDBclosed = true;
                } else {
                    bDBclosed = false;
                }

                Log.FatalError("FATAL bDBclosed [" + bDBclosed + "] in getParentNodeID_FromNodeID() : error getting node ID [" + sql + "]", e);
            }


        }

        return sNodeTitle_local;
    }

    // *************************************
    public static long getParentNodeID_FromNodeID(long lNode_, Connection dbc_)
            // *************************************
            throws Exception {

        long lReturn = -1;
        String sql = "select parentID from node "
                + "  where nodeid = '" + lNode_ + "'";
        try {
            lReturn = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbc_);
        } catch (Exception e) {
            Log.FatalError("getParentNodeID_FromNodeID() : error getting node ID [" + sql + "]", e);
        }

        return lReturn;
    }

    // *************************************
    public static String getNodeTitleFromNodeID(long lNodeID_, Connection dbc_)
            // *************************************
            throws Exception {


//		String htKey = lNodeID_ + ":";
        String sHashedTitle = null;
        String sql = "select nodetitle from node where nodeid = " + lNodeID_;
        try {
            sHashedTitle = JDBCIndra_Connection.executeQueryAgainstDBStr(sql, dbc_);
        } catch (Exception e) {
            Log.FatalError("getNodeTitleFromNodeID() : error getting node ID [" + sql + "]", e);
        }
        return sHashedTitle;
    }

    // *************************************
    public static long getNodeSizeFromNodeID(long lNodeID_, Connection dbc_)
            // *************************************
            throws Exception {


        long lReturn = -1;
        String sql = "select nodesize from node where nodeid = " + lNodeID_;
        try {
            lReturn = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbc_);
        } catch (Exception e) {
            Log.FatalError("getNodeTitleFromNodeID() : error getting node ID [" + sql + "]", e);
        }

        return lReturn;
    }

    // *************************************
    public static long getFileIDFromCorpusURL(String sURLCorpusRelative, Connection dbc_)
            // *************************************
            throws Exception {

        long lReturn = -1;
        String sql = "select fileid from corpusfile where path = '" + sURLCorpusRelative + "'";
        try {
            lReturn = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbc_);
        } catch (Exception e) {
            Log.FatalError("getNodeTitleFromNodeID() : error getting node ID [" + sql + "]", e);
        }

        return lReturn;
    }

    // *************************************
    public static long getNodeIDFromFile(String sCorpusFileURL, int iCorpusID, java.sql.Connection dbc_)
            // *************************************
            throws Exception {
        String sql = "select max(nodeID) from nodepath  "
                + " where path = '" + sCorpusFileURL + "'";
        long l = -1;
        try {
            l = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbc_);
            if (l < 0) {
                api.Log.Log("missing nodepath [" + sql + "]");
            }
        } catch (Exception e) {
            Log.FatalError("error getting node ID [" + sql + "]", e);
        }
        return l;

    }

    // *************************************
    public static String getNodePathCorpusfile(long lNodeID,
            boolean bCache,
            java.sql.Connection dbc_)
            // *************************************
            throws Exception {
        String sNodePathCorpusFile = null;
        {
            String sql = "select path from nodepath "
                    + " where nodeid = " + lNodeID;
            try {
                sNodePathCorpusFile = JDBCIndra_Connection.executeQueryAgainstDBStr(sql, dbc_);
            } catch (Exception e) {
                Log.FatalError("error getting node corpus path [" + sql + "]", e);
            }

        }

        return sNodePathCorpusFile;
    }

    // *************************************
    // get word list for metasearch
    public static synchronized StringAndCount[] getSigWordsAndCounts_SCArr_withTitle_SortedDesc(
            boolean includeAncestorTitles_,
            boolean cleaned_,
            boolean bForceSigKeyTitleWords,
            boolean bWantTitleDelimiters,
            long lNodeID_,
            java.sql.Connection dbc_,
            boolean bFatalIfNoSig)
            // *************************************
            throws Exception {
        String htKey = includeAncestorTitles_ + ":"
                + cleaned_ + ":"
                + bForceSigKeyTitleWords + ":"
                + bWantTitleDelimiters + ":"
                + lNodeID_;

        StringAndCount[] sigWordsAndCounts_withTitle_SortedDescrereturn = null;

        if (dbc_ == null) {
            Log.FatalError("sig words not cached and dbc_ == null for [" + htKey + "]");
        }

        Vector vecOfVecsDBResult = null;
        String sql = "select SignatureWord, SIGNATUREOCCURENCES from signature where nodeid = " + lNodeID_
                + " order by SIGNATUREOCCURENCES desc, SignatureWord ";
        // Log.log ("123321" +  sql ) ;
        vecOfVecsDBResult = JDBCIndra_Connection.executeDBQueryVecOfVecsReturned(sql, dbc_, -1);

        if (vecOfVecsDBResult.isEmpty()) {
            if (bFatalIfNoSig) {
                Log.FatalError("no signature results found in DB from [" + sql + "]");
            } else {
                return sigWordsAndCounts_withTitle_SortedDescrereturn;  // EARLY RETURN !!!
            }
        }
        Vector vOfWordsFromDB = (Vector) vecOfVecsDBResult.elementAt(0);
        Vector vOfCountsFromDB = (Vector) vecOfVecsDBResult.elementAt(1);

        // get title words into HT and ready for prepend
        // if title has only stop words
        String[] sArrTitleWords_stopAdjusted = getStrArrTitleWords_stopAdjusted(
                includeAncestorTitles_,
                cleaned_,
                lNodeID_,
                true,
                dbc_);

        // don't want to include in the array those words already included in the title
        // will put title in after get max counts
        Hashtable htTitleWords = new Hashtable();
        for (int i = 0; i < sArrTitleWords_stopAdjusted.length; i++) {
            htTitleWords.put(sArrTitleWords_stopAdjusted[i], sArrTitleWords_stopAdjusted[i]);
        }


        int iMaxNumOccurAnyWord = -1;
        Vector vOfStringsAndCounts = new Vector();
        for (int i = 0; i < vOfWordsFromDB.size(); i++) {
            String sWord = (String) vOfWordsFromDB.elementAt(i);

            if (sWord != null && !sWord.equals("titleRecord")) {
                int iCount = ((java.math.BigDecimal) vOfCountsFromDB.elementAt(i)).intValue();

                if (htTitleWords.get(sWord) == null) // if not a title word then keep it from the doc text
                {
                    vOfStringsAndCounts.addElement(new StringAndCount(sWord, iCount));
                }
                // get the max # so titles can be added as max + ....
                if (iMaxNumOccurAnyWord < iCount) {
                    iMaxNumOccurAnyWord = iCount;
                }
            }
        }
        // now get the title words into the vector - having counted max occur.
        for (int i = 0; i < sArrTitleWords_stopAdjusted.length; i++) {
            vOfStringsAndCounts.insertElementAt(new StringAndCount(sArrTitleWords_stopAdjusted[i], iMaxNumOccurAnyWord), i);
        }

        sigWordsAndCounts_withTitle_SortedDescrereturn = new StringAndCount[vOfStringsAndCounts.size()];

        vOfStringsAndCounts.copyInto(sigWordsAndCounts_withTitle_SortedDescrereturn);

        return sigWordsAndCounts_withTitle_SortedDescrereturn;
    }  // public static synchronized StringAndCount[] getSigWordsAndCounts_withTitle_SortedDesc ( )

    // *************************************
    public static String[] getStrArrTitleWords_stopAdjusted(
            boolean includeParentTitles_,
            boolean cleaned_,
            long lNodeID_,
            boolean bLowerCase_,
            java.sql.Connection dbc_)
            // *************************************
            throws Exception {
        String sNodeTitle = getNodeTitle(includeParentTitles_, cleaned_, lNodeID_, dbc_);
        if (bLowerCase_) {
            sNodeTitle = sNodeTitle.toLowerCase();
        }
        // delimiters
        String[] sArrNodeTitle = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption(sNodeTitle, "?!/, &+.;:#()\u00A0?\"\n\r[]\u001A>", true, true);
        return sArrNodeTitle;
    }

    // *************************************
    public static Integer getCorpusID_FromNodeID(Integer INodeID, Connection dbc)
            // *************************************
            throws Exception {
        Integer ICorpusReturn = null;

        String sql = "select corpusid from node "
                + "  where nodeid = " + INodeID.toString();

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sql);

            int iCorpusID = -1;
            if (rs.next()) {
                iCorpusID = rs.getInt(1);
            }
            if (iCorpusID == -1) {
                throw new Exception("either no nodeid [" + iCorpusID + "] or has no corpus.");
            } else {
                ICorpusReturn = new Integer(iCorpusID);
            }
        } finally {
            rs.close();
            stmt.close();
        }
        return ICorpusReturn;

    } // public static Integer getCorpusID_FromNodeID ( Integer INodeID, Connection dbc )

    //**************************
    public static String getCorpusName(int iCorpusID, boolean bShortTrueLongFalse, java.sql.Connection dbc)
            //**************************
            throws Exception {
        String whichField = null;
        if (bShortTrueLongFalse) {
            whichField = "corpusShortName";
        } else {
            whichField = "corpus_name";
        }

        String sql = "select " + whichField + " from corpus where corpusid = " + iCorpusID;
        String s = null;
        try {
            s = JDBCIndra_Connection.executeQueryAgainstDBStr(sql, dbc);
        } catch (Exception e) {
            Log.FatalError("getParentNodeID_FromNodeID() : error getting corpus name [" + sql + "]", e);
        }
        return s;
    }
}
