
/**
 * ExceptionRecord.java
 *
 *
 * Created: Fri Jan 14 11:17:46 2000
 *
 * @author 
 * @version
 */

package com.indraweb.profiler;

import java.util.*;
import java.io.*;

public class ExceptionRecord  {
    
    private Hashtable locations = new Hashtable();
    private String name;
    
    ExceptionRecord(Throwable e ) {
	name = e.toString();
    }
    
    void recordException(Throwable e )
    {
	String location = pullLocationFromStackTrace( e ); 
	Integer count = (Integer) locations.get( location );
	if( count == null ) {
	    count = new Integer( 1 );
	} else {
	    count = new Integer( count.intValue() + 1 );
	}
	locations.put( location, count );
    }

    void printReport()
    {
	System.out.println("=======================================");
	System.out.println("Exception: " + name );
	System.out.println("Locations: ");
	Enumeration enumeration = locations.keys();
	while( enumeration.hasMoreElements() ) {
	    String location = (String) enumeration.nextElement();
	    Integer count = (Integer) locations.get( location );
	    System.out.println("------------------");
	    System.out.println("Count: "+ count.toString());
	    System.out.println(location);
	}
    }

    private String pullLocationFromStackTrace( Throwable e )
    {
	StringWriter sw = new StringWriter();
	e.printStackTrace( new PrintWriter( sw ) );
	
	StringTokenizer st = new StringTokenizer( sw.toString(), "\n\r");
	try {
	    // return the first two lines of the trace.
	    String location = st.nextToken();  // pull off the message line.
	    location += st.nextToken() ;
	    return location;
	} catch (NoSuchElementException nsee) {
	    return "error getting location";
	}
    }
} // ExceptionRecord
