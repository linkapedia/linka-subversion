/**
 * Profiler.java
 * Created: Wed Jan 12 15:37:04 2000
 *
 * @author 
 * @version
 */
package com.indraweb.profiler;

import com.indraweb.util.UtilStrings;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class Profiler {

    private static Map<String, Timer> timers = new HashMap<String, Timer>();
    private static Map<String, ExceptionRecord> exceptionRecords = new HashMap();
    private static Map htStats_StrStatNameToObject = new HashMap();

    private Profiler() {
    }

    public static void clear() {
        timers = null;
        timers = new HashMap();
        exceptionRecords = null;
        exceptionRecords = new HashMap();
        htStats_StrStatNameToObject = new HashMap();
        api.Log.Log("profiler contents cleared");
    }

    public static void record(Throwable e) {
        ExceptionRecord record = (ExceptionRecord) exceptionRecords.get(e.toString());
        if (record == null) {
            record = new ExceptionRecord(e);
            exceptionRecords.put(e.toString(), record);
        }
        record.recordException(e);
    }

    /**
     * Return a running, named Timer object.
     * @param name name of the method or process being measured.
     * @return a start()ed Timer object.
     */
    public static synchronized Timer getStartedTimer(String name, boolean bStart) {
        Timer timer = getTimer(name, bStart);
        timer.start();
        return timer;
    }

    public static void removeTimerStartingWith(String substring) {
        if (substring != null && !substring.trim().equals("")) {
            Set<String> keys = timers.keySet();
            for (String methodName : keys) {
                if (methodName.startsWith(substring)) {
                    timers.remove(methodName);
                }
            }
        }
    }

    public static synchronized Timer getTimer(String name, boolean bStart) {
        // check if there are any timers for the requested
        // type already available and unused.  this should
        // save some object creation overhead.
        Timer timer = (Timer) timers.get(name);
        if (timer == null || timer.isRunning()) {
            // Create a new one.  This should only
            // happen the first time a timer is requested for a
            // given name or when multiple threads have entered
            // the same method and are attempting to time the same thing.
            timer = new Timer(name);
        }
        timers.put(name, timer);
        if (bStart) {
            timer.start();
        }
        return timer;
    }

    public static void printReport() {
        if ((timers.isEmpty()) && (exceptionRecords.isEmpty())) {
            return;
        }

        int i = 0;

        api.Log.Log("#################  PROFILER REPORT START #################");
        api.Log.Log("#####  Methods report  ######");
        Timer[] sortedTimers = new Timer[timers.size()];
        Set<String> methods = timers.keySet();
        for (String methodName : methods) {
            sortedTimers[i++] = (Timer) timers.get(methodName);
        }

        // dumb bubble sort
        for (i = 0; i < sortedTimers.length; i++) {
            for (int j = 0; j < sortedTimers.length; j++) {
                if (sortedTimers[j].compareTo(sortedTimers[i]) < 0) {
                    Timer t = sortedTimers[i];
                    sortedTimers[i] = sortedTimers[j];
                    sortedTimers[j] = t;
                }
            }
        }

        api.Log.Log("Total\t%Max\t#Call\tAvg\tBlockName");
        //if( timingData != null ) timingData.println("total\tcalls\tavg\tblock name");
        long lTotalMax = 0;
        for (i = 0; i < sortedTimers.length; i++) {
            try {
                Timer timer = sortedTimers[i];
                String methodName = (String) timer.getName();
                long lTotal = timer.getTotal();
                if (i == 0) {
                    lTotalMax = lTotal;
                }
                double dPctThisTimerOfLargestTimer = (double) 100 * (double) lTotal / (double) lTotalMax;
                String sTotalString = com.indraweb.util.UtilStrings.padString("" + timer.getTotal(), 6);
                api.Log.Log(sTotalString
                        + "\t" + com.indraweb.util.UtilStrings.numFormatDouble(dPctThisTimerOfLargestTimer, 1)
                        + "\t" + timer.getCount()
                        + "\t" + (timer.getTotal() / timer.getCount())
                        + "\t" + methodName);
                /*
                if( timingData != null )
                timingData.println(timer.getTotal() + "\t"+timer.getCount() + "\t" +
                (timer.getTotal() / timer.getCount()) + "\t"+methodName);
                 */

            } catch (NoSuchElementException e) {
                e.printStackTrace();
            }
        }
        //if( timingData != null ) timingData.close();

        if (exceptionRecords.size() > 0) {
            api.Log.Log("");

            api.Log.Log("#####  Exceptions report  ######");
            Collection<ExceptionRecord> exceptions = exceptionRecords.values();
            for (ExceptionRecord exceptionRecord : exceptions) {
                exceptionRecord.printReport();
            }
        }
        api.Log.Log("#####  Statistics report  ######");
        if (htStats_StrStatNameToObject.size() > 0) {
            //Vector v = sortedStats   - by date say

            Set<String> enumStats = htStats_StrStatNameToObject.keySet();
            int i2 = 0;
            for (String sKey : enumStats) {
                i2++;
                Object oStat = htStats_StrStatNameToObject.get(sKey);
                if (oStat instanceof String) {
                    String sStat = (String) oStat;
                    if (sStat.indexOf("#[") >= 0) {
                        String sCount = UtilStrings.getStringBetweenThisAndThat(sStat, "#[", "]");
                        int iCount = Integer.parseInt(sCount);
                        double dRatePerSec = ((double) 1000 * (double) iCount) / (double) lTotalMax;
                        String sRate = com.indraweb.util.UtilStrings.numFormatDouble(dRatePerSec, 2);
                        api.Log.Log(i2 + ". STAT [" + sKey + "] scount [" + sCount + "] rate [" + sRate + "]");
                    } else {
                        api.Log.Log(i2 + ". STAT [" + sKey + "] [" + oStat.toString() + "]");
                    }
                } else {
                    api.Log.Log(i2 + ". STAT [" + sKey + "] [" + oStat.toString() + "]");
                }
            }
        }
        api.Log.Log("#####  Exceptions report  ######");
        api.Log.Log("");
        api.Log.Log("#################  PROFILER REPORT END #################");
    }

    // ***************************************************
    public static long methodStart(long SequenceID, String methodName) // ***************************************************
    {
        long currentTime = System.currentTimeMillis();
        java.util.Date currDate = new java.util.Date();

        api.Log.Log("\t --- " + currDate + " SEQ[" + SequenceID + "] Start method " + methodName);

        return currentTime;
    }

    // ***************************************************
    public static void methodEnd(long SequenceID, String methodName, long startTime) // ***************************************************
    {
        java.util.Date currDate = new java.util.Date();

        api.Log.Log("\t --- " + currDate + " SEQ[" + SequenceID + "] End   method " + methodName
                + " Processed for " + ((System.currentTimeMillis() - startTime + 1) / 1000.0) + " Seconds.");
    }

    public static void addStatistic(String sKey, Object o) {
        // api.Log.Log ("Adding statistic " + sKey + ":" +  o.toString());
        htStats_StrStatNameToObject.put(new java.util.Date() + ":" + sKey, o);
    }

    public static void addStatisticCount(String sKey, int i) {
        String sStat = "#[" + i + "]";
        //api.Log.Log ("Adding statistic " + sKey + ":" +  sStat);
        htStats_StrStatNameToObject.put(new java.util.Date() + ":" + sKey, sStat);
    }
} // Profiler
