/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Oct 31, 2003
 * Time: 12:56:54 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.indraweb.ir.texttranslate;

import com.indraweb.utils.datasets.DataTables;
import com.indraweb.utils.datasets.DataTables;
import com.indraweb.utils.datasets.DataTable;

import java.sql.Connection;
import java.util.Vector;
import java.util.Hashtable;

public class Data_TextTranslate
{

    private static Hashtable htStringLangIDToStrLangName  = new Hashtable();
    private static long lhtStringLangIDToStrLangName = -1;
    public static final String sQueryLANGUAGE = "TableLANGUAGES";

    public static synchronized DataTable getDtLANGUAGE ( Connection dbc ) throws Exception
    {
        DataTable dt = DataTables.getDataTable( sQueryLANGUAGE, "select languageid, languagename from language", -1, dbc);
        if ( lhtStringLangIDToStrLangName < dt.lCachedTime )
        {
            htStringLangIDToStrLangName.clear();
            Object[][] oArrArrData_TableLANGUAGETRANSLATOR = dt.getoArrArrData();
            for ( int iRow = 0; iRow < dt.iNumRows; iRow++)
            {
                String s1 = oArrArrData_TableLANGUAGETRANSLATOR [iRow][0].toString();
                String s2 = oArrArrData_TableLANGUAGETRANSLATOR [iRow][1].toString();
                htStringLangIDToStrLangName.put (s1, s2);
            }
            lhtStringLangIDToStrLangName = dt.lCachedTime;
            dt.tablePrint();

        }
        return dt;

    }

    private static Hashtable htStrLangCLangCRankTo_StrTranslatorCP = new Hashtable();
    private static long lhtStrLangCLangCRankTo_StrTranslatorCP = -1;

    public static synchronized DataTable getDtLANGUAGETRANSLATOR ( Connection dbc ) throws Exception
    {
        DataTable dt = DataTables.getDataTable( "TableLANGUAGETRANSLATORS",
                "select languageidfrom, languageidto, rankpref, CLASSPATHTRANSLATOR from languagetranslator", -1, dbc);
        if ( lhtStrLangCLangCRankTo_StrTranslatorCP < dt.lCachedTime )
        {
            htStrLangCLangCRankTo_StrTranslatorCP.clear();
            Object[][] oArrArrData_TableLANGUAGETRANSLATOR = dt.getoArrArrData();
            for ( int iRow = 0; iRow < dt.iNumRows; iRow++)
            {

                String s1 = oArrArrData_TableLANGUAGETRANSLATOR[iRow][0]+":"+
                            oArrArrData_TableLANGUAGETRANSLATOR[iRow][1]+":"+
                            oArrArrData_TableLANGUAGETRANSLATOR[iRow][2];
                String s2 = oArrArrData_TableLANGUAGETRANSLATOR[iRow][3].toString();
                htStrLangCLangCRankTo_StrTranslatorCP.put (s1, s2);

            }
            lhtStrLangCLangCRankTo_StrTranslatorCP = dt.lCachedTime;
            dt.tablePrint();

        }
        return dt;
    }

    public static synchronized String getClasspathOfTranslator ( int iLangFrom, int iLangTo,
                                                                     int iRankPref, Connection dbc) throws Exception
    {
        String sRetCP = (String) htStrLangCLangCRankTo_StrTranslatorCP.get ( iLangFrom+":"+iLangTo+":"+iRankPref);

        if ( sRetCP == null )
            getDtLANGUAGETRANSLATOR(dbc);
        return (String) htStrLangCLangCRankTo_StrTranslatorCP.get
                ( iLangFrom+":"+iLangTo+":"+iRankPref );
    }

    public static synchronized String getLangName ( int iLangID, Connection dbc) throws Exception
    {
        System.out.println ("in getLangName ");

        String sRetLang = (String) htStrLangCLangCRankTo_StrTranslatorCP.get ( ""+iLangID );

        if ( sRetLang == null && DataTables.needsCaching (sQueryLANGUAGE, lhtStringLangIDToStrLangName, dbc))
            getDtLANGUAGE(dbc);
        return (String) htStringLangIDToStrLangName.get
                ( ""+iLangID );
    }


}
