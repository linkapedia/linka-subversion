package com.indraweb.corpusinstall;

import java.util.Vector;
import java.util.Properties;
import com.indraweb.util.*;
import com.indraweb.execution.*;
import com.indraweb.encyclopedia.Topic;
import com.indraweb.database.*;
import com.indraweb.html.*;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.PhraseParms;
import com.indraweb.signatures.WordFrequencies;
import com.indraweb.signatures.SignatureParms;

/**
 * populates a vec with all files and folders under  sDirName
 * recurse optional
 * includes tree level on way in
 * iNodeID_CurrentNext_ is an autonumbering scheme
 */
public class CreateTopicVector
{
	private static long lNodeID_CurrentNext = -1;
	private static boolean setInitialNode = false;
	private static int iNumFileNodesEncounteredThusFar = 0;
	// RECURSIVE START FROM ROOT
	public static Vector step1_1_getVecOfCorpusFileDescriptors_recursive
		( 
			Vector vCorpusFileDescriptors_output,
			String sDirName, 
			long lNodeID_CurrentNext_, 
			CorpusFileDescriptor cfpCurrentParent,
			boolean bRoot,
			boolean bFreeTopicObjects_,
			int iMaxNumVecsOfInterest,
			java.sql.Connection dbc_,
			int iNodeDepth_0_isRoot,
			boolean bVerboseSigLogging,
            PhraseParms phraseparms,
            ParseParms parseparms,
            WordFrequencies wordfreq,
            SignatureParms sigparms
		)
		throws Throwable
	{
		if ( bRoot  )
			iNumFileNodesEncounteredThusFar = 0;
			
		if ( iMaxNumVecsOfInterest > 0 && 
			 iMaxNumVecsOfInterest <= vCorpusFileDescriptors_output.size() )
		{
			// clsUtils.waitForKeyboardInput ("stopped early 1 in vec build at [" + iMaxNumVecsOfInterest + "]\r\n");
			return vCorpusFileDescriptors_output;
					
		}
		if ( !setInitialNode ) 
		{
			lNodeID_CurrentNext = lNodeID_CurrentNext_;
			setInitialNode = true;
		} 
		
		if ( !sDirName.endsWith( "/" ) )
		{
			sDirName = sDirName + "/";
		} 
		try 
		{
			if ( !UtilFile.bDirExists ( sDirName ) )
			{
				Log.FatalError ( "Encyclopedia.getVecCorpusArticleURLs() : sTopic not exists [" + sDirName + "]"  ) ;	
			} 

			// if first and root DIR
			if ( cfpCurrentParent == null )
			{
				cfpCurrentParent = new CorpusFileDescriptor (	
															sDirName,
															lNodeID_CurrentNext, 
															null,
															0,
															sDirName,
															CorpusFileDescriptor.iFILE_OR_DIR_TYPE_dir,
															iNodeDepth_0_isRoot, // node depth,
                                                            false, // clob not source in build hierarchy,
                                                            false, // bAddIndexingTextFromTitle_
                                                            false, // bTakeTitleFromStream_,
                                                            parseparms,
                                                            phraseparms

														);
										
				lNodeID_CurrentNext++;  // DIR - not 000.html
				vCorpusFileDescriptors_output.addElement ( cfpCurrentParent );
			}

			// BUILD OUT TREE
			String[] saFileList = UtilFileEnumerator.listFilesInFolderNonRecursive ( sDirName,
																					 true, 
																					 null,
																					 UtilFileEnumerator.iMODE_CONSTRAIN_NAME_NONE);
			
			// list the first level dirs 																	  
			for (int j = 0; j < saFileList.length; j++ )
			{
				if ( !UtilFile.bFileExists ( sDirName + saFileList [ j ] ) )
				{
					throw new Exception ( "Encyclopedia.getVecCorpusArticleURLs() : sTopic not exists [" + sDirName + saFileList [ j ] + "]"  ) ;	
				} 

				String filenameFullQual = sDirName + saFileList [ j ];
				// Log.log (vCorpusFileDescriptors.size() + "." + j +  ". corpus file [" + UtilFileAndDirNames.stripUpToAndIncl_IndraHome( filenameFullQual, false) + "]\r\n");

				if ( bVerboseSigLogging && filenameFullQual.endsWith ( "000.html") || filenameFullQual.endsWith ( "000.htm") )
					Log.logClear ("cfd URL [" + filenameFullQual + "]\r\n" ) ;
				// DIRS 
				if ( UtilFile.bDirExists ( filenameFullQual) ) 
				{
					
					CorpusFileDescriptor cfdThis = new CorpusFileDescriptor 
						(		
							filenameFullQual,
							lNodeID_CurrentNext, 
							cfpCurrentParent,
							j,
							UtilFileAndDirNames.StripIndraHome ( filenameFullQual ),
							CorpusFileDescriptor.iFILE_OR_DIR_TYPE_dir,
							iNodeDepth_0_isRoot,
                            false, // clob not source in build hierarchy
                                false,
                                false,
                                parseparms,
                                phraseparms
						);
												
					vCorpusFileDescriptors_output.addElement ( cfdThis );
					// taking advantage of this special corpus ?  one level nesting from top level to 
					CreateTopicVector.step1_1_getVecOfCorpusFileDescriptors_recursive
						( 
							vCorpusFileDescriptors_output,
							filenameFullQual , 
							lNodeID_CurrentNext, 
							cfdThis,
							false,
							bFreeTopicObjects_,
							iMaxNumVecsOfInterest,
							dbc_,
							iNodeDepth_0_isRoot + 1,
							bVerboseSigLogging,
                            phraseparms,
                            parseparms,
                            wordfreq,
                            sigparms
						);
					
				} 
				else  // FILES
				{
					
					CorpusFileDescriptor cfdThis = new CorpusFileDescriptor 
						( 
							filenameFullQual,
							lNodeID_CurrentNext,
							cfpCurrentParent,
							j, 
							filenameFullQual,
							CorpusFileDescriptor.iFILE_OR_DIR_TYPE_htmlfile,
							iNodeDepth_0_isRoot,
                            false, // clob not source in build hierarchy
                            false,
                            false,
                            parseparms,
                            phraseparms
						);
					iNumFileNodesEncounteredThusFar++;					
					if ( filenameFullQual.endsWith("000.html") )
					{

						CorpusFileDescriptor cfdParent = cfdThis.parentFileDescriptor;
						try 
						{
							//Log.log (iNumFileNodesEncounteredThusFar + ". uncomment this or not for topic fetch!\r\n");
							if ( bFreeTopicObjects_ )
							{
								// Topic tThis = cfdThis.getThisCFDTopic( dbc_ );
								String sFileName = UtilStrings.getStrAfterThisToEnd1Based ( cfdThis.getURL (), "file:///", 1 );
								String sTitleThis = UtilHTML.getTitleFastFromHTMLFile ( sFileName );
								cfdParent.setTitle ( sTitleThis.toLowerCase() );
								if (bVerboseSigLogging)
									Log.log ( j + ". file list " + sTitleThis + ": node :" + lNodeID_CurrentNext + "\r\n");
							}
							else
							{
								Topic tThis = cfdThis.getThisCFDTopic( dbc_, wordfreq, sigparms );
								String sTitleThis = tThis.m_DocumentTitle;
								cfdParent.setTitle ( sTitleThis.toLowerCase() );
								cfdThis.setThisCFDTopicAndHTsNull ();
								if (bVerboseSigLogging)
									Log.log ( j + ". file list" + sTitleThis + ": node :" + lNodeID_CurrentNext + "\r\n");
							}
							
						}
						catch ( Exception e ) 
						{
							Log.FatalError ( "asdasd" , e);	
						} 
						vCorpusFileDescriptors_output.addElement ( cfdThis );
						lNodeID_CurrentNext++;
					}
					else
					{
						Log.logClear("file not CNF, continuing [" + filenameFullQual + "]\r\n");	
					}
					
					// no sense wasting a nodeid an a 000 that will not be inserted - avoids every other nodeid phenomenon
				}

				if ( iMaxNumVecsOfInterest > 0 && iMaxNumVecsOfInterest <= vCorpusFileDescriptors_output.size() )
				{
					// clsUtils.waitForKeyboardInput ("stopped early 2 in vec build at [" + iMaxNumVecsOfInterest + "]\r\n" );
					return vCorpusFileDescriptors_output;
				}
				
			}  // for (int j = 0; j < saFileList.length; j++ )
			return vCorpusFileDescriptors_output;
		} 
		catch ( Exception e ) 
		{
			Log.FatalError ( " failed get topic vector ", e ) ;
		} 
		return null;
	} // public static Vector getTopicVectorFromCorpusDirPointer_style1_likeWBMed_alphaInNumberedFolderTopLevel 

}







		
