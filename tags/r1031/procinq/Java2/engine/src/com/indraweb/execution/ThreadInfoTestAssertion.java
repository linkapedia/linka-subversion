/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: May 26, 2004
 * Time: 8:15:30 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.execution;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class ThreadInfoTestAssertion {
    private static final Logger log = Logger.getLogger(ThreadInfoTestAssertion.class);
    private static Map htSBCaptureError = new HashMap();
    private static Map htSBCaptureWarning = new HashMap();

    public static void captureErrorPut(String sErrorInfo) {
        log.debug("captureErrorPut(String)");
        StringBuilder sbTestAssertionFail = (StringBuilder) htSBCaptureError.get(Thread.currentThread());
        if (sbTestAssertionFail == null) {
            sbTestAssertionFail = new StringBuilder();
        }
        if (sErrorInfo == null || sErrorInfo.trim().equals("")) {
            log.error("Call to captureErrorPut in error - no input string - continue ");
        } else {
            sbTestAssertionFail.append("\r\n### ").append(sErrorInfo);
            htSBCaptureError.put(Thread.currentThread(), sbTestAssertionFail);
            log.debug("updated assertion fail for thread [" + Thread.currentThread().getName() + "]  to sbTestAssertionFail [" + sbTestAssertionFail.toString() + "]");
        }
    }

    public static int captureErrorThreadsGetCount() {
        log.debug("captureErrorThreadsGetCount()");
        return htSBCaptureError.size();
    }

    public static StringBuilder captureErrorFailget() {
        log.debug("captureErrorFailget()");
        return (StringBuilder) htSBCaptureError.get(Thread.currentThread());
    }

    public static void captureErrorsRemove() {
        log.debug("captureErrorsRemove()");
        log.debug("removing assertion thread [" + Thread.currentThread().getName() + "] count pre remove [" + htSBCaptureError.size() + "]");
        htSBCaptureError.remove(Thread.currentThread());
    }

    public static void captureWarningPut(String sWarningInfo) {
        log.debug("captureWarningPut(String)");
        StringBuilder sbTestAssertionFail = (StringBuilder) htSBCaptureWarning.get(Thread.currentThread());
        if (sbTestAssertionFail == null) {
            sbTestAssertionFail = new StringBuilder();
        }
        if (sWarningInfo == null || sWarningInfo.trim().equals("")) {
            log.debug("Call to capturewarningput in error - no input string - continue ");
        } else {
            sbTestAssertionFail.append("\r\n### ").append(sWarningInfo);
            htSBCaptureWarning.put(Thread.currentThread(), sbTestAssertionFail);
            log.warn("updated assertion warning for thread [" + Thread.currentThread().getName() + "]  to sbTestAssertionFail [" + sbTestAssertionFail.toString() + "]");
        }
    }

    public static int captureWarningThreadsGetCount() {
        log.debug("captureWarningThreadsGetCount()");
        return htSBCaptureWarning.size();
    }

    public static StringBuilder captureWarningFailget() {
        log.debug("captureWarningFailget()");
        return (StringBuilder) htSBCaptureWarning.get(Thread.currentThread());
    }

    public static void captureWarningsremove() {
        log.debug("captureWarningsremove()");
        log.debug("removing assertion thread [" + Thread.currentThread().getName() + "] count pre remove [" + htSBCaptureWarning.size() + "]");
        htSBCaptureWarning.remove(Thread.currentThread());
    }
}
