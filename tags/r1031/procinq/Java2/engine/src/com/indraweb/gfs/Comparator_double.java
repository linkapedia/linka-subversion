package com.indraweb.gfs;


import java.util.*;

public class Comparator_double implements IComparator
{
	public static int compare (double d1, double d2) // standard java compare outputs -1, 0, 1
	{
		if ( d1 < d2 )
			return -1;
		else if ( d1 > d2 )
			return 1;
		else 
			return 0;
	}
	
	public int compare(Object o1, Object o2) 
	{
		return 0 ; // dummy to keep compiler happ - really want double compare	
	}

}
