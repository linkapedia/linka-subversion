package com.indraweb.util;

import java.util.*;
import java.io.*;
import com.indraweb.signatures.*;
import com.indraweb.execution.*;
import com.indraweb.encyclopedia.*;
import com.ms.util.*;


public class UtilSets
{

	// ***************************************************
	public static void deDupVector ( Vector v )
	// ***************************************************
	{
		if ( v != null && v.size() > 0 )
		{
			Hashtable ht = new Hashtable();

			/*
			for ( int i = 0; i < v.size() ; i++ )
			{
				Object o = v.elementAt(i);
				if ( o != null )
				{
					if ( ht.get (  o ) != null )
					{
						v.removeElementAt(i);
						i = i -	1;
					}
					else
						ht.put ( o, o );
				}
			}
			*/
			boolean bNullFound = false;
			for ( int i = v.size()-1; i >= 0 ; i-- )
			{
				Object o = v.elementAt(i);
				if ( o != null )
				{
					if ( ht.get ( v.elementAt(i)  ) == null )
					{
						ht.put ( o, o );
					}
					else
						v.removeElementAt(i);
				}
				else // dedup nulls as well - be strict in semantics
				{
					if ( bNullFound )
						v.removeElementAt(i);
					bNullFound = true;
				}
			}

		}
	}

	// ***************************************************
	public static Vector getVectorSlice ( Vector v , int iStart, int iEndNonInclusive)
	// ***************************************************
	{
		Vector vReturn = new Vector();
		for ( int i = iStart; i < iEndNonInclusive && i < v.size() ; i++ )
		{
			vReturn.addElement ( v.elementAt(i) );
		}
		return vReturn;
	}

	// ***************************************************
	public static Object[] getStrArraySlice ( String[] oArr , int iStart, int iEndNonInclusive)
	// ***************************************************
	{
		Vector vReturn = new Vector();
		for ( int i = iStart; i < iEndNonInclusive && i < oArr.length ; i++ )
		{
			vReturn.addElement ( oArr[i] );
		}
		String[] sArrReturn = new String[vReturn.size()];
		vReturn.copyInto (sArrReturn);
		return sArrReturn;
	}

	// ***************************************************
	public static void removeNumbersFromStringVector ( Vector v )
	// ***************************************************
	{
		for ( int i = v.size()-1; i >= 0 ; i-- )
		{
			String s = (String) v.elementAt ( i );
			if ( s != null && s.length() > 0 )
			{

				if ( Character.isDigit(s.charAt(0) ) )
				{
					try
					{
						Double D = Double.valueOf( s );
						v.removeElementAt ( i );
					}
					catch ( Exception e )
					{
					}
				}
			}
		}
	}

    // ***************************************************
	public static void vRemoveAllOccurOfObject  ( Vector v , Object o)
	// ***************************************************
	{
        int iIndex = v.indexOf(o);
        while ( iIndex >= 0 )
        {
            v.removeElementAt(iIndex);
            iIndex = v.indexOf(o);
        }
	}



    // ***************************************************
	public static Object[] objArrayRemoveAllOccurencesOf ( Object[] oArr, Object o)
	// ***************************************************
	{

        Vector vRet = new Vector ();
        for ( int i = 0; i < oArr.length; i++ )
        {
            if ( oArr[i] != o )
                vRet.addElement(oArr[i]);
        }

        Object[] oArrRet = new Object[vRet.size()];
        vRet.copyInto(oArrRet);
        return oArrRet;
	}


    // ***************************************************
	public static Object[] objArrayRemoveElementAt ( Object[] oArr, int iIndex)
	// ***************************************************
	{

        Object[] oArrRet = new Object [oArr.length-1];
        Vector vRet = new Vector ();
        int iOffSet = 0;
        for ( int i = 0; i < oArr.length; i++ )
        {
            if ( i != iIndex )
                vRet.addElement(oArr[i + iOffSet]);
            else
                iOffSet = -1;
        }
        return oArrRet;
	}



    // ***************************************************
	public static double[] convertVectorTo_doubleArray ( Vector v )
	// ***************************************************
	{
		double[] dArr = new double [ v.size() ];
		Enumeration e = v.elements();
		int i = 0;
		while ( e.hasMoreElements())
		{
			dArr [ i ] = ((Double) e.nextElement()).doubleValue() ;
			i++;
		}
		return dArr;
	}

	// ***************************************************
	public static int[] convertVectorTo_intArray	( Vector v )
	// ***************************************************
	{
		int[] iArr = new int [ v.size() ];
		Enumeration e = v.elements();
		int i = 0;
		while ( e.hasMoreElements())
		{
			iArr [ i ] = ((Integer) e.nextElement()).intValue() ;
			i++;
		}
		return iArr;
	}

    // ***************************************************
	public static String[] convertVectorTo_StringArray ( Vector v )
	// ***************************************************
	{
		String[] sArr = new String [ v.size() ];
		Enumeration e = v.elements();
		int i = 0;
		while ( e.hasMoreElements())
		{
			sArr [ i ] = (String) e.nextElement() ;
			i++;
		}
		return sArr;
	}

    // ***************************************************
	public static Object[] convertVectorTo_ObjArray ( Vector v )
	// ***************************************************
	{
		Object[] oArr = new String [ v.size() ];
		Enumeration e = v.elements();
		int i = 0;
		while ( e.hasMoreElements())
		{
			oArr [ i ] =  e.nextElement() ;
			i++;
		}
		return oArr;
	}


	// ***************************************************
	public static Hashtable convertVectorTo_HashTableSelf ( Vector v )
	// ***************************************************
	{
		Hashtable ht = new Hashtable();
		Enumeration e = v.elements();
		while ( e.hasMoreElements())
		{
			Object o = e.nextElement();
			ht.put ( o, o );
		}
		return ht;
	}




	// ***************************************************
	public static HashSet vToHS ( Vector v )
	// ***************************************************
	{
		HashSet hs = new HashSet();
		Enumeration e = v.elements();
		while ( e.hasMoreElements())
		{
			Object o = e.nextElement();
			hs.add( o );
		}
		return hs;
	}





	// ***************************************************
	public static Vector convertObjectArrayToVector ( Object[] oArr )
	// ***************************************************
	{
		Vector v = new Vector();
		if ( oArr != null )
		{
			for ( int i = 0; i < oArr.length; i++ )
			{
				v.addElement ( oArr[i] );
			}
		}
		return v;
	}


    // ***************************************************
	public static String convertObjectArrayToStringWithDelims ( Object[] oArr, String sDelim )
	// ***************************************************
	{
		StringBuffer sb = new StringBuffer();
		if ( oArr != null )
		{
			for ( int i = 0; i < oArr.length; i++ )
			{
				if ( i > 0 )
					sb.append ( sDelim );
				sb.append ( oArr[i].toString() );
			}
		}
		return sb.toString();
	}

    // ***************************************************
	public static String convertiArrayToStringWithDelims ( int[] iArr, String sDelim )
	// ***************************************************
	{
		StringBuffer sb = new StringBuffer();
		if ( iArr != null )
		{
			for ( int i = 0; i < iArr.length; i++ )
			{
				if ( i > 0 )
					sb.append ( sDelim );
				sb.append ( ""+ iArr[i]);
			}
		}
		return sb.toString();
	}

	// ***************************************************
	public static Enumeration convertObjectArrayToEnum ( Object[] oArr )
	// ***************************************************
	{
		Vector v = convertObjectArrayToVector ( oArr );
		return v.elements();
	}

	// ***************************************************
	public static Vector vectorCopy ( Vector v1 )
	// ***************************************************
	{
		Vector vCopy = new Vector ( v1.size() );
		Enumeration e = v1.elements();

		while ( e.hasMoreElements () )
		{
			vCopy.addElement ( e.nextElement() ) ;
		}
		return vCopy;
	}


	/*
	public static Vector difference_Vectors ( Vector v1, Vector v2, boolean leftOneToLHS_OfMinus )
	{
		Log.FatalError ( " not debugged ");
		Vector vDiffReturn = new Vector();
		Vector vLHS = null;
		Vector vRHS = null;
		if ( leftOneToLHS_OfMinus )
		{
			vLHS = v1;
			vRHS = v2;
		}
		else
		{
			vLHS = v2;
			vRHS = v1;
		}
		Vector vLHScopy = vectorCopy ( v1 );

		for ( int i = 0; i < vRHS.size(); i ++)
		{
			for ( int j = vLHS.size()-1; j >= 0; j++ )
			{
				if ( vRHS.elementAt ( i ) == vLHScopy.elementAt ( j ) )
				{
					vLHScopy.removeElementAt (	j  );
				}
			}
		}
		return vDiffReturn;
	}

	*/
	// ***************************************
	public static Vector difference_VectorMinusHashtable ( Vector v1, Hashtable ht )
	// ***************************************
	{
		Vector vDiffReturn = new Vector();


		for ( int i = 0; i < v1.size(); i ++)
		{
			if ( ht.get ( v1.elementAt ( i ) ) == null )
			{
				vDiffReturn.addElement (  v1.elementAt ( i ) ) ;
			}
		}
		return vDiffReturn;
	}

	// ***************************************************
	public static Vector concatenateVectors ( Vector vVector1, Vector vVector2 )
	// ***************************************************
	{

		Vector vVectorNew = new Vector();
		for (int loop = 0; loop < vVector1.size(); loop++) {
			vVectorNew.addElement(vVector1.elementAt(loop)); }
		for (int loop = 0; loop < vVector2.size(); loop++) {
			vVectorNew.addElement(vVector2.elementAt(loop)); }

		return vVectorNew;
	}

    // ***************************************
	public static Vector removeFromVecElemsInHashT (Vector v, Hashtable htToRemoveMaster )
	// ***************************************
	{
		int htsize = htToRemoveMaster.size();
		Hashtable  htToRemove_inBoth = new Hashtable();

		if ( htsize > 0 )
		{
			int vsize = v.size();
			for ( int i = 0; i < vsize ; i++ )
			{
				String s = (String) v.elementAt ( i );

				if ( htToRemoveMaster.get ( s ) != null )
					htToRemove_inBoth.put ( s, s );
			}

			Hashtable htToKeep = new Hashtable();
			Enumeration e = v.elements();
			while ( e.hasMoreElements() )
			{
				Object o = e.nextElement ();
				htToKeep.put  ( o, o );
			}

			e = htToRemove_inBoth.elements();
			while ( e.hasMoreElements() )
			{
				htToKeep.remove ( e.nextElement () );
			}


			Vector vKeep = new Vector();

			e = htToKeep.elements();
			while ( e.hasMoreElements() )
			{
				vKeep.addElement ( e.nextElement () );
			}
			return vKeep;
		}
		else
			return v;

	}

    // ***************************************
	public static void removeFromVecElemsInHashSet (Vector vInOut, HashSet hsToRemoveMaster )
	// ***************************************
	{
		int hssize = hsToRemoveMaster.size();
		Hashtable  htToRemove_inBoth = new Hashtable();

		if ( hssize > 0 )
		{
			int vsize = vInOut.size();
			for ( int i = vsize-1; i >= 0; i-- )
			{
				String s = (String) vInOut.elementAt ( i );

				if ( hsToRemoveMaster.contains( s ) )
					vInOut.removeElementAt(i);
			}
        }

	}

	// ***************************************
	public static int minOfIntArr ( int[] iArr )
	// ***************************************
	{

		if ( iArr == null || iArr.length == 0 )
			return -1;

		if ( iArr.length == 0 )
			Log.FatalError ( "zero length array in minOfIntArr()");
		int min = iArr[0];
		for (int i = 1 ; i < iArr.length; i++)
		{
			if ( min > 	iArr [ i ] )
				min = iArr [ i ] ;
		}
		return min;
	}

    // ***************************************
	public static int maxOfIntArr ( int[] iArr )
	// ***************************************
	{
		if ( iArr.length == 0 )
			Log.FatalError ( "zero length array in minOfIntArr()");
		int max = iArr[0];
		for (int i = 1 ; i < iArr.length; i++)
		{
			if ( max < 	iArr [ i ] )
				max = iArr [ i ] ;
		}
		return max;
	}

    // ***************************************
	public static int maxOfIntVector ( Vector vInt )
	// ***************************************
	{

		int max = ((Integer) vInt.elementAt(0)).intValue();
		for (int i = 1 ; i < vInt.size() ; i++)
		{
            int iNew = ((Integer) vInt.elementAt(i)).intValue() ;
			if ( max < iNew )
				max = iNew;
		}
		return max;
	}


	// ***************************************
	public static boolean isIntInIntArray ( int j, int[] iArr )
	// ***************************************
	{
		for ( int i = 0; i < iArr.length; i++)
		{
			if ( iArr[i] == j )
				return true;
		}
		return false;
	}

	// ***************************************
	public static boolean isStrInStrArray ( String s, String[] sArr )
	// ***************************************
	{
		for ( int i = 0; i < sArr.length; i++)
		{
			if ( sArr[i].equals ( s ) )
				return true;
		}
		return false;
	}

	// ***************************************
	public static int isIntInIntArray_getIndexFirstOccur ( int j, int[] iArr )
		throws Exception
	// ***************************************
	{
		for ( int i = 0; i < iArr.length; i++)
		{
			if ( iArr[i] == j )
				return i;
		}
		return -1;
	}

	// ***************************************
	public static int[] stringCoverage ( String s1, String s2 )
	// ***************************************
	{
		int iCover = 0;
		try
		{
			Vector v1 = new Vector();
			Vector v2 = new Vector();
			v1 = convertStringToStrVec_Tokenizer ( s1, " ", false );
			v2 = convertStringToStrVec_Tokenizer ( s2, " ", false );

			int[] iCoveragePerWord = new int[ v1.size() ];

			Enumeration	e = v1.elements();
			while ( e.hasMoreElements() )
			{
				String wordThisIndex = ( String) e.nextElement();
				for ( int i = 0; i < v2.size(); i++ )
				{
					if ( (( String ) v2.elementAt( i )).equals ( wordThisIndex) )
					{
						iCoveragePerWord[i]++;
					}
				}
			}

			return iCoveragePerWord;
		}
		catch ( Exception e )
		{
		 int i = 0;
		}
		return null;
	}

	// ***************************************
	public static double getCoverageOfOneStrArrByAnother ( String[] sArr1, String[] sArr2 )
	// ***************************************
	{
        int iNumHits = 0;
        // for each word in the first set
        for ( int i = 0; i < sArr1.length; i++ )
        {
            // for each word in the second  set
            for ( int j = 0; j < sArr1.length; j++ )
            {
                if ( sArr1[i].equals(sArr1[j]))
                {
                    iNumHits++;
                    break;
                }
            }
        }

        return (double) iNumHits / (double ) sArr1.length;

	}

	// ***************************************
	public static int getCoverageCountOfOneStrArrByAnother ( String[] sArr1, String[] sArr2 )
	// ***************************************
	{
		try
		{
			int iNumHits = 0;
			// for each word in the first set
			for ( int i = 0; i < sArr1.length; i++ )
			{
				// for each word in the second  set
				for ( int j = 0; j < sArr2.length; j++ )
				{
					if ( sArr1[i].equals(sArr2[j]))
					{
						iNumHits++;
						break;
					}
				}
			}

			return iNumHits;
		}
		catch ( Exception e )
		{
			return -1;
		}
	}

	// ***************************************
	public static Vector convertStringToStrVec_Tokenizer ( String s,
														   String tokenizerDelims,
														   boolean bRemoveStops )
	// ***************************************
	{
		StringTokenizer st = new 	StringTokenizer ( s, tokenizerDelims );
		Vector vOfWords = new Vector();
		while ( st.hasMoreElements() )
		{
			String stoken = (String) st.nextElement ();
			if ( bRemoveStops )
				if ( Session.stopList.getHTStopWordList().get ( stoken ) != null )
					continue;
				vOfWords.addElement ( stoken );
		}
		return vOfWords;
	}

	// ***************************************************
	public static double coverInIntArray ( int[] iArr )
	// ***************************************************
	{
		int iTotalHits = 0;
		for ( int i = 0; i < iArr.length; i++ )
		{
			if ( iArr [ i ] > 0 )
				iTotalHits++;
		}
		double d = ((double) iTotalHits) / ( iArr.length ) ;
		return d;
	}
	// ***************************************************
	public static double coverInIntArray ( int[] iArr, int basis )
	// ***************************************************
	{
		int iTotalHits = 0;
		for ( int i = 0; i < basis; i++ )
		{
			if ( iArr [ i ] > 0 )
				iTotalHits++;
		}
		double d = ((double) iTotalHits) / (double) basis ;
		return d;
	}
	// ***************************************************
	public static int[] concatenateIntArrays ( int[] iArr1, int[] iArr2 )
	// ***************************************************
	{

		int iTotalLen = iArr1.length + iArr2.length;
		int[] iArrNew = new int [ iTotalLen ] ;
		for ( int i = 0; i < iArr1.length; i++)
		{
			iArrNew[i] = iArr1[i];
		}
		for ( int i = 0; i < iArr2.length; i++)
		{
			iArrNew[ iArr1.length + i ] = iArr2[ i ];
		}

		return iArrNew;
	}

	// ***************************************************
	public static String[] concatenateStringArrays ( String[] iArr1, String[] iArr2 )
	// ***************************************************
	{

		int iTotalLen = iArr1.length + iArr2.length;
		String[] iArrNew = new String [ iTotalLen ] ;
		for ( int i = 0; i < iArr1.length; i++)
		{
			iArrNew[i] = iArr1[i];
		}
		for ( int i = 0; i < iArr2.length; i++)
		{
			iArrNew[ iArr1.length + i ] = iArr2[ i ];
		}

		return iArrNew;
	}

	// ***************************************************
	public static Object[] concatenateObjArrays ( Object[] iArr1, Object[] iArr2 )
	// ***************************************************
	{

		int iTotalLen = iArr1.length + iArr2.length;
		Object[] iArrNew = new Object [ iTotalLen ] ;
		for ( int i = 0; i < iArr1.length; i++)
		{
			iArrNew[i] = iArr1[i];
		}
		for ( int i = 0; i < iArr2.length; i++)
		{
			iArrNew[ iArr1.length + i ] = iArr2[ i ];
		}

		return iArrNew;
	}

	// ***************************************************
	public static void removeIntegersFromHT ( Hashtable ht,  int[] iArr, boolean confirmCountRedux )
	// ***************************************************
	{
		int iOrigHTSize = -1;
		if ( confirmCountRedux )
			iOrigHTSize = ht.size();

		for ( int i = 0; i < iArr.length; i++)
		{
			ht.remove ( new Integer ( iArr [ i ] ) );
		}
		if ( confirmCountRedux )
		{
			if ( ( iOrigHTSize - ht.size() ) != iArr.length  )
			{
				Log.FatalError ( "error in removeIntegersFromHT ");
			}
		}
	}

	// ***************************************************
	public static boolean confirmCover_HTIntegerKeysOverIntArr	( Hashtable ht,  int[] iArr )
	// ***************************************************
	{
		int[] iArrCopyCheck = (int[]) iArr.clone();

		for ( int i = 0; i < iArrCopyCheck.length; i++)
		{
			if ( ht.get ( new Integer ( iArrCopyCheck [ i ] ) )!= null )
			{
				iArrCopyCheck[i] = -1;
			}
		}
		for ( int i = 0; i < iArrCopyCheck.length; i++)
		{
			if ( iArrCopyCheck[i] > -1 )
				return false;
		}
		return true;
	}

	// ***************************************************
	public static int min ( int i1, int i2 )
	// ***************************************************
	{
		if ( i1 < i2 )
			return i1;
		return i2;
	}
	// ***************************************************
	public static int max ( int i1, int i2 )
	// ***************************************************
	{
		if ( i1 > i2 )
			return i1;
		return i2;
	}



	// ***************************************************
	public static boolean bArrContainsTrue ( boolean[] bArr )
	// ***************************************************
	{
		for ( int i = 0; i < bArr.length; i++ )
		{
			if ( bArr [ i ] == true )
				return true;
		}
		return false;
	}

	// ***************************************************
	public static Vector genCorpusGrouping_IntPairArrs ( int iNumTopicsTotalInCorpus, int iBatchSize  )
	// ***************************************************
	{

		Vector vIntArrPairs = new Vector();

		try
		{

			if ( iNumTopicsTotalInCorpus <= iBatchSize )
			{
				int[] iArr = new int [ 2 ];
				iArr [0] = 0;
				iArr [1] = iNumTopicsTotalInCorpus;

				vIntArrPairs.addElement ( iArr );
			}
			else
				{
				// **********************************************************************************
				// **** Whole sets
				// **********************************************************************************

				// Session.cfg.getPropInt ("SigGenRangeSize")

				int iNumWholeSets = iNumTopicsTotalInCorpus / iBatchSize;
				int remainder = iNumTopicsTotalInCorpus % iBatchSize;

				for ( int i = 0; i < iNumWholeSets; i++)
				{
					int iStartPoint_topicIndex	= i * iBatchSize;
					int iEndPoint_topicIndex	= i * iBatchSize + iBatchSize;

					int[] iArr = new int [ 2 ];
					iArr [0] = iStartPoint_topicIndex;
					iArr [1] = iEndPoint_topicIndex;

					vIntArrPairs.addElement ( iArr );
				}

				// *******************************************************************************
				// **** Fractional remainder
				// *******************************************************************************

				// total 400
				// size 250
				// rem = 150

				if ( remainder > 0 )
				{
					int iStartPoint_topicIndex = iNumTopicsTotalInCorpus - iBatchSize;
					int iEndPoint_topicIndex = iNumTopicsTotalInCorpus;

					int[] iArr = new int [ 2 ];
					iArr [0] = iStartPoint_topicIndex;
					iArr [1] = iEndPoint_topicIndex;

					vIntArrPairs.addElement ( iArr );
				}
			}

		}
		catch ( Exception e )
		{
			Log.printStackTrace_mine ( 1, e );
		}
		return vIntArrPairs ;
	} // genCorpusGrouping_IntPairArrs



	// ***************************************************
	public static void combineWordAndCountHTsIntoFirst ( Hashtable ht1, Hashtable ht2 )
	// ***************************************************
	{

		int iHT1sizeInit = ht1.size();
		int iHT2sizeInit = ht2.size();

		Enumeration e = ht2.keys();
		while (e.hasMoreElements())
		{
			String s = (String) e.nextElement();
			UtilSets.hash_increment_count_for_string (
							ht1, s,
							hash_get_count_for_string(ht2, s));
		}
		Log.log (":: HT combined [" + iHT1sizeInit + "] and [" + iHT2sizeInit + "] into [" + ht1.size() + "]\r\n");


	}


	// ----------------------------------------------------------------------------
	public static Hashtable mergeHashTablesAddCounts (Hashtable ht1, Hashtable ht2) {
	// aggreates two counting dictionaries, eg aggregating two nodes
	// ----------------------------------------------------------------------------


		Hashtable htMerged = new Hashtable();

		Enumeration e = ht1.keys();
		String s;
		while (e.hasMoreElements())  {
			s = (String) e.nextElement();
			UtilSets.hash_increment_count_for_string (
							htMerged, s,
							hash_get_count_for_string(ht1, s));
		}

		e = ht2.keys();
		while (e.hasMoreElements())  {
			s = (String) e.nextElement();
			UtilSets.hash_increment_count_for_string (
							htMerged, s,
							hash_get_count_for_string(ht2, s));
		}

		return htMerged;
	}





	//-----------------------------------------------------------------------------
	// METHODhash_increment_count_for_string
	//-----------------------------------------------------------------------------

	// ************
	public static int hash_increment_count_for_string (Hashtable hasht, String s, int iIncrAmt) {
	// ************
        int i, newCount;
		i = hash_get_count_for_string (hasht, s);
 		hasht.put (s, new Integer (i + iIncrAmt)) ;
		//newCount = ((Integer) hasht.get (s)).intValue() ;
		//HKonLib.print ("Incremented count for word [" + s + "] to [" + newCount + "]" );
        return i + 1;

	}

	// ************
	public static void hash_increment_count_for_VecOfstrings	 (Hashtable hasht,
																  Vector v,
																  int iIncrAmt,
																  boolean bRemoveStops,
																  boolean bStemmed) {
	// ************
        int i;
		Enumeration e = v.elements();
		Hashtable htStopList = null;
		if ( bRemoveStops )
		{
			if ( bStemmed )
				htStopList = Session.stopList.getHTStopWordList();
			else
				htStopList = Session.stopList.getHTStopWordListStemmed();
		}

		boolean bKeep = true;
		while ( e.hasMoreElements() )
		{
			String s = (String) e.nextElement();
			bKeep = true;
			if ( bRemoveStops )
			{
				if ( htStopList.get (s) != null )
				bKeep = false;
			}
			if ( bKeep )
			{
				i = hash_get_count_for_string (hasht, s);
 				hasht.put (s, new Integer (i + iIncrAmt)) ;
			}
		}

	}



	// ************
	public static int hash_increment_count_for_string_ArrayMode (Hashtable hasht, String s, int iIncrAmt) {
	// ************
 		int[] iArr = (int[]) hasht.get (s);
		iArr[0]+= iIncrAmt;
 		// hasht.put (s, iArr[new Integer (i + iIncrAmt)) ;
		//newCount = ((Integer) hasht.get (s)).intValue() ;
		//HKonLib.print ("Incremented count for word [" + s + "] to [" + newCount + "]" );
        return iArr [ 0 ];

	}

	public static void hashTableRestrict (Hashtable hasht, int iMinAmountRequired)
	{
		Enumeration e = hasht.keys();
		while ( e.hasMoreElements() )
		{
			String s = (String) e.nextElement()	;
			int i = UtilSets.hash_get_count_for_string ( hasht, s );
			if ( i < iMinAmountRequired )
				hasht.remove ( s );
		}
	}


	//-----------------------------------------------------------------------------
	// METHODhash_get_count_for_string
	//-----------------------------------------------------------------------------
 	public static int hash_get_count_for_string (Hashtable hasht, String s)
	{
 		int i ;

		try
		{
			if ( hasht.get (s) == null)
	 			return 0;
 	  		else
				i = ((Integer) hasht.get (s)).intValue();

			return i;
		}
		catch ( Exception e )
		{
			Log.printStackTrace_mine (80, e);
			Log.FatalError ( " in hash_get_count_for_string " );
		}
		return -1;
	}


	// -------------------------------------------------------------------------------
	public static void hashTableToFile (Hashtable ht, String sFName, String sComment) {
	// -------------------------------------------------------------------------------

		try {
			// print contents of totals
			PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(sFName)))	;
			file.write (sComment + "\r\n");
			file.write (" Total num keys :  " + ht.size());
			Enumeration e = ht.keys();
			while (e.hasMoreElements())  {
				String key = (String) e.nextElement();
				file.write("\"" + key + "\"," +
					UtilSets.hash_get_count_for_string( ht, key) + "\r\n");
			}
			file.close();
		}
		catch (IOException e) {
			Log.FatalError ("IOExcepion in hashTableToFile ");
		}

	}

    // ***************************************************
	public static void addArrayToHT ( Hashtable ht,  Object[] oArr, Object key  )
	// ***************************************************
	{
		for ( int i = 0; i < oArr.length; i++)
		{
			ht.put (key, oArr[i]);
		}
	}
    // ***************************************************
	public static void addArrayToHS ( HashSet hs,  Object[] oArr )
	// ***************************************************
	{
		for ( int i = 0; i < oArr.length; i++)
		{
			hs.add (oArr[i]);
		}
	}

	// ***************************************************
	public static Object[] projectArray ( Object[] oArrSource, int[] iArrProjectIndexes )
	// ***************************************************
	{
		Object[] oArrTarget = new Object [ iArrProjectIndexes.length ] ;
		for ( int i = 0; i < oArrSource.length; i++)
		{
			oArrTarget [i] = oArrSource  [ iArrProjectIndexes [ i ] ];
		}
		return oArrTarget;
	}


	// ***************************************************
	public static int countStrArrayOverlapStrArray ( String[] sArr1,
	// ***************************************************
													 String[] sArr2,
													 boolean bTrim,
													 boolean bCaseIgnore )
	{
		Hashtable ht1 = new Hashtable();
		String s = "temp";
		String sTemp = null;
		for ( int i	= 0;i < sArr1.length; i++ )
		{
			sTemp = sArr1[i];
			if ( bTrim )
				sTemp = sTemp.trim();
			if ( bCaseIgnore )
				sTemp = sTemp.toLowerCase();
			ht1.put ( sTemp, s );
		}
		int iOverlapCount = 0;
		for ( int i	= 0; i < sArr2.length; i++ )
		{
			String sTemp2 = null;
			sTemp2 = sArr2[i];
			if ( bTrim )
				sTemp2 = sTemp2.trim();
			if ( bCaseIgnore )
				sTemp2 = sTemp2.toLowerCase();

			if ( ht1.get ( sTemp2 ) != null )
			{
				iOverlapCount++;
			}
		}
		return iOverlapCount;
	}

	// ************
	public static double getCoverOfStrArrByHT ( String[] sArr, Hashtable ht )
	// ************
	{
		double dcover = 0;
		for ( int i = 0; i < sArr.length; i++ )
		{
			if ( ht.get ( sArr[i] ) != null )
			{
				dcover += (double) 1 / (double) sArr.length;
				// System.out.println("getCoverOfStrArrByHT [" + sArr[i] + "]" ); // hbk control
			}
		}
		return dcover;
	}

    // ************
	public static String convertHTKeysToStrCommaList ( Hashtable ht )
	// ************
	{
		Enumeration e = ht.keys();
		StringBuffer sb = new StringBuffer();
		int i = 0;
		while ( e.hasMoreElements() )
		{
			String sDelim = ",";
			if ( i == 0 )
				sDelim = "";

			sb.append(sDelim + e.nextElement() );
			i++;
		}
		return sb.toString();
	}

    // ************
	public static Vector convertHTKeysToVector ( Hashtable ht )
	// ************
	{
		Enumeration e = ht.keys();
		Vector v = new Vector();
		while ( e.hasMoreElements() )
		{
			v.add(e.nextElement());
		}
		return v;
	}

    // ************
	public static String[] convertHTKeysToStrArr ( Hashtable ht )
	// ************
	{
		Enumeration e = ht.keys();
		String[] sArr = new String[ht.size() ];
        int i = 0;
		while ( e.hasMoreElements() )
		{
			sArr[i] = (String) e.nextElement();
            i++;
		}
		return sArr;
	}
    // ************
	public static HashSet convertHTKeysToHashSet ( Hashtable ht )
	// ************
	{
        HashSet hsReturn = new HashSet();
		Enumeration e = ht.keys();
		while ( e.hasMoreElements() )
		{
			hsReturn.add(e.nextElement());
		}
		return hsReturn;
	}
    // ************
	public static String[] convertHSOfStrToStrArr ( HashSet hs )
	// ************
	{
		Iterator iter = hs.iterator();
		String[] sArr = new String[hs.size() ];
        int i = 0;
		while ( iter.hasNext() )
		{
			sArr[i] = (String) iter.next();
            i++;
		}
		return sArr;
	}

    // ************
	public static Vector convertStrArrToVector ( String[] sArr )
	// ************
	{
        Vector v = new Vector();
        for ( int i = 0; i < sArr.length; i++ )
        {
            v.add(sArr[i]);
        }
		return v;
	}

    public static String vToStr ( Vector v )
    {
        return vToStrDelim(v, "");

    }

    public static String vToStrDelim ( Vector v , String sDelim )
    {
        StringBuffer sb = new StringBuffer ();
        sb.append("Vec #[" + v.size()+ "]");
        for ( int i = 0; i < v.size(); i++ )
        {
            if ( v.elementAt(i) == null )
                sb.append("<" + i + "." + "NULL"+ ">;" + sDelim);
            else
                sb.append("<" + i + "." + v.elementAt(i).toString()+ ">;");
        }
        return sb.toString();

    }

    public static String vToStrPairDelimStr ( Vector v1 , Vector v2 , String sDelim)
    {
        StringBuffer sb = new StringBuffer ("\r\n");
        for ( int i = 0; i < v1.size(); i++ )
        {
            if ( i > 0 )
                sb.append(sDelim);
            sb.append(i + ". " + v1.elementAt(i).toString() + " : " + v2.elementAt(i).toString());
        }
        return sb.toString();

    }

    public static String vToStrPlain ( Vector v , String sDelim)
    {
        StringBuffer sb = new StringBuffer ();
        for ( int i = 0; i < v.size(); i++ )
        {
            if ( i > 0 )
                sb.append(sDelim);
            sb.append(v.elementAt(i).toString());
        }
        return sb.toString();

    }

    public static String hsToStr ( HashSet hs )
    {
        return hsToStr(hs, ",", false);
    }
    public static String hsToStr ( HashSet hs, String sDelim, boolean bClean )
    {
        Iterator I = hs.iterator();
        StringBuffer sb = new StringBuffer ();
        int iCount = 0;
        while ( I.hasNext())
        {
            if ( iCount > 0 )
                sb.append (sDelim);
            sb.append(I.next().toString()  );
            iCount++;

        }
        if ( bClean )
            return sb.toString();
        else
            return "HS [" + iCount + "] <" + sb.toString() + ">";
    }
    public static String sArrToStr(String[] sArr)
    {
        StringBuffer sb = new StringBuffer ();
        sb.append("len[" + sArr.length + "]" );
        for ( int i = 0; i < sArr.length ; i++ )
        {
            sb.append("<" + i + "." + sArr[i]+ ">;");
        }
        return sb.toString();

    }

    // ************
	public static String htKeysToStr ( Hashtable ht)
	// ************
	{
		Enumeration eKeys = ht.keys();
        HashSet hsKeys = new HashSet();

		while ( eKeys.hasMoreElements() )
		{
            hsKeys.add(eKeys.nextElement());

		//	hsReturn.add(eKeys.nextElement());
		}
        return UtilSets.hsToStr(hsKeys, ",", false);
	//	return hsReturn;
	}



    public static String bArrToStr( boolean[] bArr )
    {
        StringBuffer sb = new StringBuffer ();
        sb.append("len[" + bArr.length + "]" );
        for ( int i = 0; i < bArr.length ; i++ )
        {
            sb.append("<" + i + "." + bArr[i]+ ">;");
        }
        return sb.toString();

    }
    // ************
	public static String htToStr ( Hashtable ht)
	// ************
	{
        return htToStr(ht, "");
    }
    // ************
	public static String htToStr ( Hashtable ht, String sSpacer_CRLFForExample)
	// ************
	{
		Enumeration eKeys = ht.keys();

        int i = 0;
        StringBuffer sbResult = new StringBuffer();

        sbResult.append("ht size [" + ht.size ()+ "] ");

        while ( eKeys.hasMoreElements() )
		{
            Object oKey = eKeys.nextElement();
            Object oValue= ht.get(oKey);
            sbResult.append(sSpacer_CRLFForExample+ i + ". ht element <"+oKey+":"+oValue+">");
		//	hsReturn.add(eKeys.nextElement());
            i++;
        }
        return sbResult.toString();
	//	return hsReturn;
	}
    // ************
	public static HashSet htKeysToHashSet ( Hashtable ht)
	// ************
	{
		Enumeration eKeys = ht.keys();
        HashSet hsKeys = new HashSet();

		while ( eKeys.hasMoreElements() )
		{
            hsKeys.add(eKeys.nextElement());

		//	hsReturn.add(eKeys.nextElement());
		}
        return hsKeys;
	}

    // ************
	public static HashSet strToCharHashSet (String s)
	// ************
	{
        HashSet hs = new HashSet();
        {
            char[] cArrDelims = s.toCharArray ();
            for ( int i = 0 ; i < cArrDelims.length ; i++ )
            {
                Character CDelim = new Character (cArrDelims[i]);

                hs.add(CDelim);
            }
        }
        return hs;
	}

    // **********************************************
	public static String iArrToString(int[] iArr, String sSpacer )
	// **********************************************
	{
		StringBuffer sb = new StringBuffer("len [" + iArr.length + "]");
		for ( int i = 0; i < iArr.length; i++ )
		{
			if ( i == 0 )
				sb.append ( iArr[i] );
			else
				sb.append ( sSpacer + iArr[i] );

		}
		return sb.toString();
	}


}












