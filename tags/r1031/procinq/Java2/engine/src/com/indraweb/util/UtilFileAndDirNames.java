package com.indraweb.util;

import com.indraweb.execution.*;

public class UtilFileAndDirNames
{
	
	public static String stripUpToAndIncl_IndraHome ( String sURL, boolean bFatalIfNotFound  )
	{
		int iLocIndraHome = sURL.toLowerCase().indexOf ( Session.sIndraHome.toLowerCase() );
		if ( bFatalIfNotFound && iLocIndraHome < 0 )
			Log.FatalError ("indrahome string not found in url [" + sURL + "]\r\n" );
		if ( iLocIndraHome >= 0 )
			sURL = sURL.substring ( iLocIndraHome + Session.sIndraHome.length() );
		
		return sURL;
	} 
	
	// -------------------------------------------------------------------------------------------
	public static String StripIndraHome	(String s) 
	// -------------------------------------------------------------------------------------------
	{
		
		int iLocIndraHome = s.toLowerCase().indexOf ( Session.cfg.getProp("IndraHome").toLowerCase() );
		String s2 = null;
		
		if ( iLocIndraHome > 0 )
			s2 = s.substring (iLocIndraHome + Session.cfg.getProp("IndraHome").length() );	
		else
			s2 = s;	
		
		return s2;
	} 
}
