/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jun 24, 2004
 * Time: 5:59:34 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.signatures;

import com.indraweb.corpusinstall.CorpusFileDescriptor;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.execution.Session;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilStrings;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignatureParms {
    // TEMPLATES
    //private static final String sFunnyChars = "<>/!@#$%^&*(){}[]\":';?/.,\\|+=-_~`";

    private static final String sFunnyChars = "????<>/!@#$%^&*(){}[]\":';?/.,\\|+=-_~`\u00AD";
    public static final String CONST_sNumParentTitleTermsAsSigs = "NumParentTitleTermsAsSigs";
    private static final int iSigGenRangeSize_Default = 250;
    private static final int iWordFreqMaxForSig_Default = 75;
    private static final int iNumParentTitleTermsAsSigs_Default = 3;
    private static final int iMaxSigTermsPerNode_Default = Session.cfg.getPropInt("NumSigTermsUsedInVectorScore");
    ;  // -1;
    private static final double dSigTieDifferenceEpsilonPercent_Default = 25.0;
    private static final String sOnlyTheseCharsIsNG_default_default = sFunnyChars + "01234567890-";
    private static final String sNotaSigIfStartChars_default = sFunnyChars;
    private static final String sNotaSigIfEndChars_default = sFunnyChars;
    private static final char[] cArrOnlyTheseCharsIsNG_default = sFunnyChars.toCharArray();
    private static final char[] cArrBadStartChars_default = sNotaSigIfStartChars_default.toCharArray();
    private static final char[] cArrBadEndChars_default = sNotaSigIfEndChars_default.toCharArray();
    //private static boolean bTrimBadStartChars_default = false;
    //private static boolean bTrimBadEndChars_default = false;
    private static final boolean bIncludeNumbers_default = false;
    private static final boolean bKeepSigsForParentNodes_default = true;
    private static final boolean bKeepSigsIfNoSourceText_default = false;
    private static final int iMinSigLen_default = 2;
    // ACTUALS
    public int iSigGenRangeSize = iSigGenRangeSize_Default;
    public int iWordFreqMaxForSig = iWordFreqMaxForSig_Default;
    public int iNumParentTitleTermsAsSigs = iNumParentTitleTermsAsSigs_Default;
    public int iMaxSigTermsPerNode = iMaxSigTermsPerNode_Default;
    public double dSigTieDifferenceEpsilonPercent = dSigTieDifferenceEpsilonPercent_Default;
    private String sOnlyTheseCharsIsNG = null;
    private String sBadStartChars = null;
    private String sBadEndChars = null;
    private char[] cArrOnlyTheseCharsIsNG = null;
    private char[] cArrBadStartChars = null;
    private char[] cArrBadEndChars = null;
    //public boolean bTrimBadStartChars = null;
    //public boolean bTrimBadEndChars = null;
    public boolean bIncludeNumbers = false;
    private boolean bKeepSigsForParentNodes = bKeepSigsForParentNodes_default;
    private boolean bKeepSigsIfNoSourceText = bKeepSigsIfNoSourceText_default;
    private static int iMinSigLen = iMinSigLen_default;

//    private String sOnlyTheseCharsIsNG = sOnlyTheseCharsIsNG_default_default;
//    private String sBadStartChars = sBadStartChars_default;
//    private String sBadEndChars = sBadEndChars_default;
//    private char[] cArrOnlyTheseCharsIsNG    = cArrOnlyTheseCharsIsNG_default ;
//    private char[] cArrBadStartChars  = cArrBadStartChars_default;
//    private char[] cArrBadEndChars    = cArrBadEndChars_default;
//    //public boolean bTrimBadStartChars = bTrimBadStartChars_default;
//    //public boolean bTrimBadEndChars = bTrimBadEndChars_default;
//    public boolean bIncludeNumbers = bIncludeNumbers_default;
//
    public SignatureParms(
            api.APIProps props) throws Exception {
        //api.Log.Log ("instantiating sig parms object");
        iSigGenRangeSize = props.getint("SigGenRangeSize", "" + iSigGenRangeSize_Default);
        iWordFreqMaxForSig = props.getint("WordFreqMaxForSig", "" + iWordFreqMaxForSig_Default);
        iNumParentTitleTermsAsSigs = props.getint("NumParentTitleTermsAsSigs", "" + iNumParentTitleTermsAsSigs_Default);
        iMaxSigTermsPerNode = props.getint("MaxSigTermsPerNode", "" + iMaxSigTermsPerNode_Default);
        iMinSigLen = props.getint("MinSigLen", "" + iMinSigLen_default);

        dSigTieDifferenceEpsilonPercent = props.getdbl("TieDifferenceEpsilon", "" + dSigTieDifferenceEpsilonPercent_Default);

        sOnlyTheseCharsIsNG = (String) props.get("OnlyTheseCharsIsNG", sOnlyTheseCharsIsNG_default_default);
        if (sOnlyTheseCharsIsNG != null) {
            cArrOnlyTheseCharsIsNG = sOnlyTheseCharsIsNG.toCharArray();
        }

        sBadStartChars = (String) props.get("BadStartChars", sNotaSigIfStartChars_default);
        if (sBadStartChars != null) {
            cArrBadStartChars = sBadStartChars.toCharArray();
        }

        sBadEndChars = (String) props.get("BadEndChars", sNotaSigIfEndChars_default);
        if (sBadEndChars != null) {
            cArrBadEndChars = sBadEndChars.toCharArray();
        }

        //bTrimBadStartChars= props.getbool("TrimBadStartsWith", ""+bTrimBadStartChars_default);
        //bTrimBadEndChars  = props.getbool("TrimBadEndsWith"  , ""+bTrimBadEndChars_default);
        bIncludeNumbers = props.getbool("IncludeNumbers", "" + bIncludeNumbers_default);
        bKeepSigsForParentNodes = props.getbool("KeepSigsForParentNodes ", "" + bKeepSigsForParentNodes_default);
        bKeepSigsIfNoSourceText = props.getbool("KeepSigsForTitleOnlyNodes", "" + bKeepSigsIfNoSourceText_default);

    }

    public void printme() {
        api.Log.Log("iSigGenRangeSize [" + iSigGenRangeSize + "]");
        api.Log.Log("iWordFreqMaxForSig [" + iWordFreqMaxForSig + "]");
        api.Log.Log("iNumParentTitleTermsAsSigs [" + iNumParentTitleTermsAsSigs + "]");
        api.Log.Log("iMaxSigTermsPerNode [" + iMaxSigTermsPerNode + "]");
        api.Log.Log("dSigTieDifferenceEpsilon   [" + dSigTieDifferenceEpsilonPercent + "]");

        api.Log.Log("cArrOnlyTheseCharsIsNG [" + UtilStrings.cArrToString(cArrOnlyTheseCharsIsNG, ",") + "]");
        api.Log.Log("cArrBadStartChars [" + UtilStrings.cArrToString(cArrBadStartChars, ",") + "]");
        api.Log.Log("cArrBadEndChars [" + UtilStrings.cArrToString(cArrBadEndChars, ",") + "]");
        //api.Log.Log ("bTrimBadStartChars [" + bTrimBadStartChars+ "]");
        //api.Log.Log ("bTrimBadEndChars [" + bTrimBadEndChars + "]");
        api.Log.Log("bIncludeNumbers [" + bIncludeNumbers + "]");
        api.Log.Log("bKeepSigsForParentNodes [" + bKeepSigsForParentNodes + "]");
        api.Log.Log("bKeepSigsForTitleOnlyNodes [" + bKeepSigsIfNoSourceText + "]");
    }

    public static int getSigGenRangeSize_Default() {
        return iSigGenRangeSize_Default;
    }

    public static int getNumParentTitleTermsAsSigs_Default() {
        return iNumParentTitleTermsAsSigs_Default;
    }

    public int getNumParentTitleTermsAsSigs() {
        return iNumParentTitleTermsAsSigs;
    }

    public void setNumParentTitleTermsAsSigs(int iNumParentTitleTermsAsSigs) {
        this.iNumParentTitleTermsAsSigs = iNumParentTitleTermsAsSigs;
    }

    // the midifier part is a future placeholder for the ability to strip off end and begin chars etc.
    // and have rules about term handling
    //int icallsignatureTermQualifierModifier = 0;
    public String signatureTermQualifierModifier(String sTerm) // qualify
    {
        //icallsignatureTermQualifierModifier++;
        // api.Log.Log (icallsignatureTermQualifierModifier + ". sig term qualifying [" + sTerm + "]" );
        String sReturn = sTerm;

        // check startswith
        String sStartsWith = sTerm.substring(0, 1);
        // start char
        if (sBadStartChars.indexOf(sStartsWith) >= 0) {
            return null;
        }

        // end char
        String sEndsWith = sTerm.substring(sTerm.length() - 1);
        if (sBadEndChars.indexOf(sEndsWith) >= 0) {
            return null;
        }

        // numbers
        if (!bIncludeNumbers) {
            if (com.indraweb.util.UtilStrings.isNumericFasterFirstCharBased(sTerm)) {
                //api.Log.Log ("sig filter disqualifying number [" + sTerm + "]" );
                return null;
            }

            // filter out 1960's and 1960s
            if (regexpMatches("[0-9]+['?]*s", sTerm)) {
                return null;
            }

            /**
             * filter out adverbs
             *
             * [A-Za-z]+cally [A-Za-z]+ively [A-Za-z]+onally [A-Za-z]+ately [A-Za-z]+line [A-Za-z]+fully [A-Za-z]+ously [A-Za-z]+ingly [A-Za-z]+dly
             */
            String sIndraHome = com.indraweb.execution.Session.cfg.getProp("IndraHome");
            if (!UtilFile.bDirExists(sIndraHome)) {
                api.Log.Log("Home dir not exists [" + sIndraHome + "]");
                return null;
            } //else { api.Log.Log("Home dir found: "+sIndraHome); }
            BufferedReader in = null;
            try {
                File adverbs = new File(sIndraHome + "/adverbs.txt");
                //api.Log.Log("opening file: "+adverbs.getAbsolutePath());
                in = new BufferedReader(new FileReader(adverbs));
                while (in.ready()) {
                    String adverb = in.readLine();
                    if ((adverb != null) && (!adverb.equals("")) && (regexpMatches(adverb, sTerm))) {
                        api.Log.Log("term: " + sTerm + " matched on prevent term " + adverb + " .. returning null..");
                        return null;
                    } else {
                        //api.Log.Log("term: "+sTerm+" did not match on "+adverb+".");
                    }
                }
            } catch (Exception e) {
                api.Log.Log("Warning: could not read adverbs.txt, skipping..");
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (Exception ex) {
                }
            }
        }

        // nothing but these terms
        if (UtilStrings.isStringNothingButTheseChars(sTerm, cArrOnlyTheseCharsIsNG)) {
            return null;
        }

        // length
        if (sTerm.length() < iMinSigLen) {
            return null;
        }

        //api.Log.Log (icallsignatureTermQualifierModifier+ ". sig term qualifying [" + sTerm + "] becomes [" + sReturn + "]" );
        return sReturn;
    }
    // reg exp pattern test with caching of patterns
    static Hashtable htPatterns = new Hashtable();

    private static boolean regexpMatches(String sPattern, String sToTest) {
        Pattern p = (Pattern) htPatterns.get(sPattern);
        if (p == null) {
            p = Pattern.compile(sPattern);
            htPatterns.put(sPattern, p);
        }

        Matcher m = p.matcher(sToTest);
        boolean b = m.matches();

        m = null;
        p = null;

        return b;
    }
    // do we want to gen sigs for this node
    int icallsignatureNodeQualifier = 0;

    public boolean signatureNodeQualifier(int iNodeID, CorpusFileDescriptor cfd, Connection dbc, WordFrequencies wordfreq) // qualify
            throws Exception {
        icallsignatureNodeQualifier++;
        //if ( true )
        //{
        //    try
        //    {
        //        String sNodeTitle = cfd.getThisCFDTopic ( null ).getTitleNoCache();
        //        api.Log.Log (icallsignatureTermQualifierModifier+ ". start sig nodeid qualifying [" + iNodeID + "] title [" + sNodeTitle + "]" );
        //    } catch ( Throwable t )  {
        //        t.printStackTrace();
        //        throw new Exception ("Error in signatureNodeQualifier : " + t.getMessage());
        //    }
        //
        //}
        boolean bKeepNodeAPrioriDefault_return = true; // optimistic
        try {
            // SCREEN parent node
            if (!bKeepSigsForParentNodes) {
                boolean bIsParent = 0 > JDBCIndra_Connection.executeQueryAgainstDBLong(
                        "select nodeid from node where parentid = " + iNodeID + " and rownum = 1", dbc);
                if (bIsParent) {
                    bKeepNodeAPrioriDefault_return = false;
                }
            }
            // SCREEN no non title text
            if (!bKeepSigsIfNoSourceText) {
                if (!cfd.getThisCFDTopic(null, wordfreq, this).bHaveNonTitleText) {
                    bKeepNodeAPrioriDefault_return = false;
                }
            }
        } catch (Throwable t) {
            api.Log.LogError("error in signatureNodeQualifier");
            throw new Exception("error in signatureNodeQualifier [" + t.getMessage() + "]");
        }
        //if ( true )
        //{
        //    try
        //    {
        //        String sNodeTitle = cfd.getThisCFDTopic ( null ).getTitleNoCache();
        //        api.Log.Log (icallsignatureTermQualifierModifier+ ". sig nodeid qualifying [" + iNodeID + "] title [" + sNodeTitle + "] becomes [" + bKeepNodeAPrioriDefault_return + "]" );
        //    } catch ( Throwable t )  {
        //        t.printStackTrace();
        //        throw new Exception ("Error in signatureNodeQualifier : " + t.getMessage());
        //    }
        //
        //}
        return bKeepNodeAPrioriDefault_return;
    }
}
