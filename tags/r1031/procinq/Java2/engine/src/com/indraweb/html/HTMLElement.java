// created 2000 01 25 to support new non stream parser
// File : HTMLElement.java

package com.indraweb.html;


import com.indraweb.util.Log;
import com.indraweb.util.UtilStrings;

import java.util.StringTokenizer;


public class HTMLElement {
    private String tokenizedHTMLLine = null;
    private int tokenizedLineTypeMsoft = -1;
    private int tokenizedLineTypeMine = -1;  // e.g., 1 - 25
    private String sHTMLtype = null; // eg "Tag" ot "text"
    private String sHTMLtag = null; // e.g., A or P or null for text
    private String sHTMLattrs = null; // HREF=http:// ....
    private String sHTMLtext = null; // e.g., Entertainment for text type only

    public HTMLElement(
            int tokenTypeMsoft,
            String tokenizedHTMLLine_
            ) {
        //MSoft tag types
        //TT_BEGIN_TAG 2;
        //TT_COMMENT 4;
        //TT_END_TAG 3;
        //TT_TEXT 1;

        tokenizedLineTypeMsoft = tokenTypeMsoft;
        tokenizedHTMLLine = tokenizedHTMLLine_;

        try {
            tokenizedLineTypeMine = UtilHTML.HTMLTagType(new StringTokenizer(tokenizedHTMLLine_));

            //StringTokenizer st = new StringTokenizer(tokenizedHTMLLine_);

            sHTMLtype = UtilStrings.getStringBetweenThisAndThat(tokenizedHTMLLine_, "type=", ",tag=");
            sHTMLtag = UtilStrings.getStringBetweenThisAndThat(tokenizedHTMLLine_, "tag=", ",attrs=");
            sHTMLattrs = UtilStrings.getStringBetweenThisAndThat(tokenizedHTMLLine_, "attrs=", ",text=");
            sHTMLtext = UtilStrings.getStrAfterThisToEnd1Based(tokenizedHTMLLine_, ",text=", 1);
        } catch (Exception e) {
            Log.printStackTrace_mine(52, e);
        }

    }

    public String getFullTokenizedHTMLLineLine() {
        return tokenizedHTMLLine;
    }

    public int getIntHTMLTypeMine() {
        return tokenizedLineTypeMine;
    }

    // REVIEW
    public int isOPENAHref() {
        return tokenizedLineTypeMine;
    }

    public String getsHTMLType() {
        return sHTMLtype;
    }

    public String getsHTMLTag() {
        return sHTMLtag;
    }

    public String getsHTMLAttrs() {
        return sHTMLattrs;
    }

    public String getsHTMLTextOrURL() {
        String s = null;
        if (getIntHTMLTypeMine() == UtilHTML.HTMLTagType_OpenAHref) {
            s = getsHrefURL();

        } else if (tokenizedLineTypeMine == UtilHTML.HTMLTagType_TEXT) {
            s = getsHTMLText();
        }
        return s;
    }

    public String getTypeDecodeToString() {
        if (tokenizedLineTypeMine >= 0)
            return UtilHTML.decodeCodeToElemTypeMine(tokenizedLineTypeMine);
        else
            return " unknown ? LT 0 ?";
    }

    public String getsHTMLText() {
        return sHTMLtext;
    }

    public String getsHrefURL() {

        String s = null;

        if (tokenizedLineTypeMine == UtilHTML.HTMLTagType_OpenAHref) {
            try {
                if (UtilStrings.getStrContains(sHTMLattrs.toLowerCase(), ",")) {
                    s = getStringBetweenThisAndThat(sHTMLattrs.toLowerCase(), "{href=", 1, ",", 1);
                } else {
                    s = getStringBetweenThisAndThat(sHTMLattrs.toLowerCase(), "{href=", 1, "}", 1);
                }


            } catch (Exception e) {
                Log.printStackTrace_mine(53, e);
            }
        } else {
        }

        return s;
    }

    // -------------------------------------------------------------------------------
    // this function stolen from utilstrings so as not to break something else !!!
    // changed the index to be one greater on end of 'href=' don't grab the '=' as we were
    public static String getStringBetweenThisAndThat(String s,
                                                     String s1,
                                                     int index1,
                                                     String s2,
                                                     int index2)
            // -------------------------------------------------------------------------------
    {
        String returnStr = null;

        int startLoc = 0;
        if (index1 > 0) {
            startLoc = UtilStrings.getLocationOfNthOfThese(s, s1, index1);
            if (startLoc == -1) {
                //HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
                return "";    // EARLY RETURN!!!!!!!!!!!!!
            }
        } else {
            startLoc = -1;
        }

        if (index2 > 0) {
            int endLoc = 0;
            int totalLen = startLoc + s1.length();
            if (totalLen == -1) {
                //HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
                return "";
            }

            endLoc = UtilStrings.getLocationOfNthOfThese(s, s2, index2);
            if (endLoc == -1) {
                //HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
                return "";    // EARLY RETURN!!!!!!!!!!!!!
            }
            try {
                returnStr = s.substring(startLoc + s1.length(), endLoc);
            } catch (Exception e) {
                //HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
                returnStr = "";
            }
        } else {
            returnStr = s.substring(startLoc + 1, s1.length());
        }

        //HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + returnStr);

        return returnStr;
        /* test cases :
		// String s = "abcdefghijklmnopqrstuvwxyza23defghijklmnopqrstuvwxyz";

		not working : need a non zero len string to do start , 0
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 1);
		HKonLib.getStringBetweenThisAndThat (s, "e", 2, "n", 1);
		HKonLib.getStringBetweenThisAndThat (s, "a", 0, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "a", 1, "e", 1);
		HKonLib.getStringBetweenThisAndThat (s, "b", 1, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 1, "", 0);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 5, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 5);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 0);

		test getting a comment field's text - from text= to end ... how
		*/


    }

    // ******************************************************************************************
    public String getsHTMLAll()
            // ******************************************************************************************
    {
        String s = "< sHTMLtype [" + sHTMLtype +
                "] sHTMLtag [" + sHTMLtag +
                "] sHTMLattrs [" + sHTMLattrs +
                "] sHTMLtext [" + sHTMLtext +
                "]";
        return s;

    }

    // ******************************************************************************************
    public String summarizeHTMLElemRecord(String preamble)
            // BLOCK******************************************************************************************
    {
        StringBuffer sb = new StringBuffer();
        String typeMineDeocdeStr = UtilHTML.decodeCodeToElemTypeMine(tokenizedLineTypeMine);
        sb.append(preamble + ". HTMLTypeDecode ( typeMine ) [" + typeMineDeocdeStr + "]\r\n");
        sb.append(preamble + ". typeMine [" + tokenizedLineTypeMine + "]\r\n");

        sb.append(preamble + ". getsHTMLTextOrURL [" + getsHTMLTextOrURL() + "]\r\n");
        sb.append(preamble + ". getLine [" + getFullTokenizedHTMLLineLine() + "]\r\n\r\n");

        return sb.toString();

    }

}