package com.indraweb.corpusinstall;

import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.encyclopedia.StringAndCount;
import com.indraweb.encyclopedia.Topic;
import com.indraweb.execution.Session;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.PhraseParms;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.signatures.WordFrequencies;
import com.indraweb.util.*;
import com.indraweb.util.sort.IComparator;
import com.indraweb.utils.oracle.UtilClob;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class CorpusFileDescriptor implements IComparator {

    private String sURL = null;
    public long lNodeID = -1;
    private String sNodeTitle = null;
    public CorpusFileDescriptor parentFileDescriptor = null;
    public int iIndexWithinParent = -1;
    public int iFileOrDirType = -1;
    private boolean bBeenInsertedToDb = false;
    private Topic topicThisCFD = null;
    private Vector vChildList = null;
    private StringAndCount[] signature = null;
    private boolean bSignatureCompleted = false;
    //private Hashtable htCFD_LocalNoTree = null;
    //private Hashtable htCFD_TreeAggregated = null;
    private int iNodeDepth_0_IsRoot = -1;
    private boolean bClobMode_getTextFromDB = false;
    private boolean bAddIndexingTextFromTitle = false;
    private boolean bTakeTitleFromStream = false;
    private ParseParms parseParms = null;
    private PhraseParms phraseparms = null;
    private String sFileName = null;
    public static int iFILE_OR_DIR_TYPE_dir = 1;
    public static int iFILE_OR_DIR_TYPE_htmlfile = 2;
    //api.APIProps props = null;

    // CONSTRUCTOR ************************************
    public CorpusFileDescriptor(String sFileName_,
            // ************************************************
            long lNodeID_,
            CorpusFileDescriptor parentFileDescriptor_,
            int iIndexWithinParent_,
            String sNodeTitle_,
            int iFileOrDirType_,
            int iNodeDepth_0_IsRoot_,
            boolean bClobMode_getTextFromDB_,
            boolean bAddIndexingTextFromTitle_,
            boolean bTakeTitleFromStream_,
            ParseParms parseParms_,
            PhraseParms phraseparms_) {
        //Log.log ("CorpusFileDescriptor url [" + sURL_ + "]\r\n");
        sFileName = sFileName_;
        sURL = "file:///" + sFileName;
        lNodeID = lNodeID_;
        parentFileDescriptor = parentFileDescriptor_;
        if (parentFileDescriptor_ != null) {
            parentFileDescriptor_.addChild(this);
        }
        iIndexWithinParent = iIndexWithinParent_;
        iFileOrDirType = iFileOrDirType_;
        try {
            sNodeTitle = sNodeTitle_.toLowerCase();
        } catch (Exception e) {
            api.Log.LogError("error in nodetitle handling sURL [" + sURL + "] sNodeTitle  [" + sNodeTitle + "] lNodeID_ [" + lNodeID_ + "]");
        }
        iNodeDepth_0_IsRoot = iNodeDepth_0_IsRoot_;
        bClobMode_getTextFromDB = bClobMode_getTextFromDB_;
        bAddIndexingTextFromTitle = bAddIndexingTextFromTitle_;
        bTakeTitleFromStream = bTakeTitleFromStream_;
        this.parseParms = parseParms_;
        this.phraseparms = phraseparms_;
        //Log.log (
        //"cfd num \t" + iNumOfTheseCreated++ +  					//"\t parent node \t"+ iNodeParentCFD  +  					//"\t node \t"+ iNodeID  +  					//"\t parenturl \t"+ sParentURL  +  					//"\t url \t"+ getURL()  +  					//"\t\r\n\r\n") ;
    }

    //
/*
     * public Hashtable getHT_CFD_AggregatedWithTree ( java.sql.Connection dbc_ ) // ************************************************ throws Exception, Throwable { if ( htCFD_TreeAggregated == null )
     * { try { // INCLUDE ME htCFD_LocalNoTree	= getTopic_htWordsDocOnly_noDescendants( dbc_ );
     *
     * if ( htCFD_LocalNoTree == null ) htCFD_TreeAggregated = new Hashtable(); else htCFD_TreeAggregated = (Hashtable) htCFD_LocalNoTree.clone();
     *
     *
     * // INCLUDE MY FILES Vector vCorpusFileDescriptors_children = getChildrenFiles_not000html(); Enumeration e_ChildrenFiles_not000html = vCorpusFileDescriptors_children.elements(); while (
     * e_ChildrenFiles_not000html.hasMoreElements() ) { CorpusFileDescriptor cdfChildFiles_not000html = ( CorpusFileDescriptor ) e_ChildrenFiles_not000html.nextElement();
     * UtilSets.combineWordAndCountHTsIntoFirst ( htCFD_TreeAggregated, cdfChildFiles_not000html.getHT_CFD_AggregatedWithTree( dbc_ ) ); Signature.printCDFTree ( cdfChildFiles_not000html, 1, dbc_ ); }
     *
     * // INCLUDE MY FOLDERS vCorpusFileDescriptors_children = getChildrenFolders(); Enumeration e_ChildFolderCFDs = vCorpusFileDescriptors_children.elements(); while (
     * e_ChildFolderCFDs.hasMoreElements() ) { CorpusFileDescriptor cdfChildFolder = ( CorpusFileDescriptor ) e_ChildFolderCFDs.nextElement(); UtilSets.combineWordAndCountHTsIntoFirst (
     * htCFD_TreeAggregated, cdfChildFolder.getHT_CFD_AggregatedWithTree( dbc_ ) ); } api.Log.Log ( "Processing CFD local title [" + getTitle() + "] url [" + this.getURL() + "] " + " words [" +
     * UtilSets.htToStr ( htCFD_LocalNoTree ) + "]\r\n"); api.Log.Log ( "Processing CFD aggregated title [" + getTitle() + "] url [" + this.getURL() + "] " + " words [" + UtilSets.htToStr (
     * htCFD_TreeAggregated ) + "]\r\n"); } catch ( Exception e ) { Log.FatalError ("in printtree ",e); } } return htCFD_TreeAggregated; }
     */
    // ************************************************
    public Hashtable getTopic_htWordsDocOnly_noDescendants(java.sql.Connection dbc_, WordFrequencies wordfreq,
            SignatureParms sigparms)
            // ************************************************
            throws Exception, Throwable {
        if (topicThisCFD == null) {
            Topic t = getThisCFDTopic(dbc_, wordfreq, sigparms);
//            api.Log.Log ("topicThisCFD has title [" + t.getTitleStopElim(false, true)+ "]" );
//            api.Log.Log ("topicThisCFD has text [" + t.sbAllTextInOne.toString() + "]" );
//            api.Log.Log ("topicThisCFD has htCFD_LocalNoTree [" + UtilSets.htToStr( htCFD_LocalNoTree ) + "]" );
        }
        return topicThisCFD.htWordsDocOnly_noDescendants;

        // this.topicThisCFD_toNull();
        //return htCFD_LocalNoTree;
    }

    // ************************************************
    public int compare(Object o1, Object o2) // standard java compare -1, 0, 1
    // ************************************************
    {
        return (compare((CorpusFileDescriptor) o1, (CorpusFileDescriptor) o2));
    }

    // ************************************************
    private int compare(CorpusFileDescriptor o1, CorpusFileDescriptor o2) // standard java compare -1, 0, 1
    // ************************************************
    {
        if (o1.isFolder() && o2.isFile()) {
            return -1;
        } else if (o1.isFile() && o2.isFolder()) {
            return 1;
        } else {
            return (o1.getURL().compareTo(o2.getURL()));
        }
    }

    // ************************************************
    public CorpusFileDescriptor getParentCFD() // ************************************************
    {
        return parentFileDescriptor;
    }

    // ************************************************
    public void setSignature(StringAndCount[] signature_) // ************************************************
    {
        signature = signature_;
        bSignatureCompleted = true;
    }

    // ************************************************
    public boolean isSigCompleted() // ************************************************
    {
        return bSignatureCompleted;
    }

    // ************************************************
    public int getNumChildren() // ************************************************
    {
        if (vChildList == null) {
            return 0;
        } else {
            return vChildList.size();
        }
    }

    // ************************************************
    public boolean isChild() // ************************************************
    {
        return (vChildList == null || vChildList.size() == 0);
    }

    // ************************************************
    public boolean isFolder() // ************************************************
    {
        return (iFileOrDirType == iFILE_OR_DIR_TYPE_dir);
    }

    // ************************************************
    public boolean isFile() // ************************************************
    {
        return (iFileOrDirType == iFILE_OR_DIR_TYPE_htmlfile);
    }

    // ************************************************
    public Vector getChildren() throws Exception // ************************************************
    {
        if (vChildList == null || vChildList.size() == 0) {
            return null;
        } else {
            com.indraweb.util.sort.Sort.sort(vChildList, (IComparator) vChildList.elementAt(0));
        }

        return vChildList;
    }

    // ************************************************
    public Vector getChildrenFilesAndFolders_not000html() throws Exception // ************************************************
    {
        Vector v = new Vector();
        if (vChildList == null || vChildList.size() == 0) {
            return v;
        } else {

            for (int i = 0; i < vChildList.size(); i++) {
                CorpusFileDescriptor cfdChild = ((CorpusFileDescriptor) vChildList.elementAt(i));
                String sURL = cfdChild.getURL();
                if (!sURL.toLowerCase().endsWith("000.html")) {
                    v.addElement(cfdChild);
                }
            }
            if (v.size() > 0) {
                com.indraweb.util.sort.Sort.sort(v, (IComparator) v.elementAt(0));
            }
            return v;
        }
    }

    // ************************************************
    public Vector getChildrenFiles_not000html() throws Exception // ************************************************
    {
        Vector v = new Vector();
        if (vChildList == null || vChildList.size() == 0) {
            return v;
        } else {
            for (int i = 0; i < vChildList.size(); i++) {
                CorpusFileDescriptor cfdChild = ((CorpusFileDescriptor) vChildList.elementAt(i));
                String sURL = cfdChild.getURL();
                if (cfdChild.isFile() && !sURL.toLowerCase().endsWith("000.html")) {
                    v.addElement(vChildList.elementAt(i));
                }
            }
            if (v.size() > 0) {
                com.indraweb.util.sort.Sort.sort(v, (IComparator) v.elementAt(0));
            }
            return v;
        }
    }

    // ************************************************
    public Vector getChildrenFiles() throws Exception // ************************************************
    {
        Vector v = new Vector();
        if (vChildList == null || vChildList.size() == 0) {
            return v;
        } else {
            for (int i = 0; i < vChildList.size(); i++) {
                CorpusFileDescriptor cfdChild = ((CorpusFileDescriptor) vChildList.elementAt(i));
                String sURL = cfdChild.getURL();
                if (cfdChild.isFile()) {
                    v.addElement(vChildList.elementAt(i));
                }
            }
            if (v.size() > 0) {
                com.indraweb.util.sort.Sort.sort(v, (IComparator) v.elementAt(0));
            }
            return v;
        }
    }

    // ************************************************
    public Vector getChildrenFolders() throws Exception // ************************************************
    {
        Vector v = new Vector();
        if (vChildList == null || vChildList.size() == 0) {
            return v;
        } else {
            for (int i = 0; i < vChildList.size(); i++) {
                if (((CorpusFileDescriptor) vChildList.elementAt(i)).isFolder()) {
                    v.addElement(vChildList.elementAt(i));
                }
            }
            if (v.size() > 0) {
                com.indraweb.util.sort.Sort.sort(v, (IComparator) v.elementAt(0));
            }
            return v;
        }
    }

    // ************************************************
    public String getTitle() // ************************************************
    {
        return sNodeTitle;
    }

    // ************************************************
    public String getTitleSpecial_noODPCleaner() // ************************************************
    {
        return sNodeTitle;
    }

    // ************************************************
    public String getTitleForceFetch(java.sql.Connection dbc_, WordFrequencies wordfreq, SignatureParms sigparms)
            throws Exception, Throwable // ************************************************
    {
        if (topicThisCFD == null) {
            getThisCFDTopic(dbc_, wordfreq, sigparms);
        }

        return sNodeTitle;
    }

    // ************************************************
    public void setTitle(String sNewTitle_) // ************************************************
    {
        sNodeTitle = sNewTitle_;
    }

    // ************************************************
    public void setThisCFDTopicAndHTsNull()
            throws Exception // ************************************************
    // allows for deref and garbge colelction as needed
    {
        topicThisCFD = null;
        //htCFD_LocalNoTree = null;
        //htCFD_TreeAggregated = null;
    }
    private static int iCallCounter_ThisCFDTopic_bClobMode = 0;
    // ************************************************

    public Topic getThisCFDTopic(java.sql.Connection dbc_, WordFrequencies wordfreq, SignatureParms sigparms)
            throws Exception, Throwable // ************************************************
    {
        return getThisCFDTopic(dbc_, false, wordfreq, sigparms);
    }
    // ************************************************

    public Topic getThisCFDTopic(java.sql.Connection dbc_, boolean bWantStringBuffForAllTextInOne,
            WordFrequencies wordfreq, SignatureParms sigparms)
            throws Exception, Throwable // ************************************************
    {
        String sTempFileName = null;
        if (topicThisCFD == null) {

            String sTextForTopic = null;
            //boolean bWantPhrasesInSigGen = Session.cfg.getPropBool ("WantPhrasesInSigGen");
            boolean bWantPhrasesInSigGen = true;
            if (bWantPhrasesInSigGen) {
                bWantStringBuffForAllTextInOne = true;
            }
            UrlHtmlAttributes uh = null;

            if (bClobMode_getTextFromDB) {
                iCallCounter_ThisCFDTopic_bClobMode++;

                // this error logic should be better - why throwing exception
                try {
                    sTextForTopic = UtilClob.getClobStringFromNodeData(lNodeID, dbc_);
                } catch (Exception e) {
                    api.Log.Log("no clog data or error in clob data access, node [" + lNodeID + "]");
                    sTextForTopic = "";
                }

                //api.Log.Log ("node [" + lNodeID + "] sTextForTopic [" + sTextForTopic + "]");
                uh = new UrlHtmlAttributes("NODEDATA sourced from nodedata [" + lNodeID + "]");

            } else // not bClobMode
            {
                //Log.NonFatalError ( " topic not here in CFD, initializing topic for url [" + sURL + "]\r\n" );
                long lTimeFileNameTemp = System.currentTimeMillis();

                String sTempFolder = Session.sIndraHome + "/temp";
                if (!UtilFile.bDirExists(sTempFolder)) {
                    UtilFile.createFolderOneLevel(sTempFolder);
                }

                int i = -1;
                for (i = 0; i < 1000; i++) {
                    sTempFileName = sTempFolder + "/" + (lTimeFileNameTemp + i) + "000.html";
                    if (!UtilFile.bFileExists(sTempFileName)) {
                        break;
                    }
                }

                if (i == 1000) {
                    throw new Exception("error can't create temp file [" + sTempFileName + "]");
                }

                // copy url file to temp file for font etc scan
                //Log.logClear ("sTempFileName [" + sTempFileName + "] sFileName [" + sFileName + "]\r\n");
                String sFinalSourceName = sFileName;
                if (!sFinalSourceName.endsWith("000.html")) {
                    sFinalSourceName = sFinalSourceName + "/000.html";
                }
                //com.ms.wfc.io.File.copy(sFinalSourceName ,sTempFileName);
                UtilFile.fileCopy(sFinalSourceName, sTempFileName);

                com.indraweb.execution.tasks.Task_SweepModFileHierarchy.processFile(sTempFileName, true);

                String sLocalSURL = "file:///" + sTempFileName;

                // Dir
                if (!sLocalSURL.endsWith("000.html") && !sLocalSURL.endsWith("/")
                        && !sLocalSURL.endsWith("\\")) {
                    sLocalSURL = sLocalSURL + "/";
                }
                if (!sLocalSURL.endsWith("000.html")) {
                    sLocalSURL = sLocalSURL + "000.html";
                }
                if (!UtilFile.bFileExists(sLocalSURL)) {
                    Log.FatalError("no 000.html file [" + sLocalSURL + "]\r\n");
                }
                if (sLocalSURL.toLowerCase().startsWith("/corpussource")) {
                    sLocalSURL = Session.sIndraHome + sLocalSURL;
                }

                uh = new UrlHtmlAttributes(sLocalSURL);

                //api.Log.Log ("FINAL TOPIC URL [" + sLocalSURL + "]\r\n");

            }

            //api.Log.Log ("new topic for node [" + lNodeID + "]" );
//                PhraseParms phraseParmsFromDB = new  PhraseParms
//                    (
//                        Session.cfg.getPropInt("PhraseCountMultiplier", false, ""+PhraseParms.iPhraseCountMultiplier_default),
//                        Session.cfg.getPropInt("PhraseLenMin", false, ""+PhraseParms.iPhraseLenMin_default),
//                        Session.cfg.getPropInt("PhraseLenMax", false, ""+PhraseParms.iPhraseLenMax_default),
//                        Session.cfg.getPropInt("PhraseRepeatMin", false, ""+PhraseParms.iPhraseRepeatMin_default),
//                        Session.cfg.getPropBool("PhraseWordsCanStartWithNonLetters", false, ""+PhraseParms.bPhraseWordsCanStartWithNonLetters_default));

            topicThisCFD = new Topic // 1
                    (
                    uh,
                    true, // fatal if not found
                    dbc_, // dbc
                    null,
                    true, // being called for sig gen ,
                    30000, // corpus files on disk usually   no read problems
                    bWantStringBuffForAllTextInOne,
                    bWantPhrasesInSigGen,
                    false,
                    false,
                    1,
                    sNodeTitle,
                    sTextForTopic,
                    bAddIndexingTextFromTitle,
                    bTakeTitleFromStream,
                    phraseparms,
                    parseParms,
                    (int) lNodeID,
                    wordfreq,
                    sigparms);
            // setTitle ( topicThisCFD.m_DocumentTitle );
            //if ( !bClobMode_getTextFromDB )
            //    UtilFile.deleteFile ( sTempFileName );
        }
        // kill the temp file I created
        if (sTempFileName != null && UtilFile.bFileExists(sTempFileName)) {
            UtilFile.deleteFile(sTempFileName);
        }
        return topicThisCFD;
    }
    static int iNumOfTheseCreated = 0;

    // ************************************************
    public void addChild(CorpusFileDescriptor cdfChild_) // ************************************************
    {
        if (vChildList == null) {
            vChildList = new Vector();
        }
        vChildList.addElement(cdfChild_);
    }

    // ************************************************
    public String getURL() // ************************************************
    {
        return sURL;
    }

    // ************************************************
    public boolean isThisADir() // ************************************************
    {
        if (iFileOrDirType == -1) {
            Log.FatalError("file type not initialized");
        }

        if (iFileOrDirType == iFILE_OR_DIR_TYPE_dir) {
            return true;
        } else {
            return false;
        }

    }
    /**
     * insert this CDF's hierarchy information into the database. this is pre signature. and pre SEE. SEE also
     */
    static int iNodePathInsertCounts = 0;
    // ************************************************

    public void updateDB_WithThis_CFD_NodeAndNodePathAndNodeData(int iCorpusID,
            Vector vTrackerOfDBUpdates,
            java.sql.Connection dbc_,
            boolean bCommit,
            boolean bFreeMemForGCBetNodeInsAndSigGen,
            WordFrequencies wordfreq,
            SignatureParms sigparms)
            // ************************************************
            throws Exception, Throwable {
        if (parentFileDescriptor != null && !parentFileDescriptor.bBeenInsertedToDb) {
            parentFileDescriptor.updateDB_WithThis_CFD_NodeAndNodePathAndNodeData(
                    iCorpusID,
                    vTrackerOfDBUpdates,
                    dbc_,
                    bCommit,
                    bFreeMemForGCBetNodeInsAndSigGen,
                    wordfreq,
                    sigparms);
        }

        // ******************************
        // insert to NODE and NODEPATH tables
        // ******************************

        //Log.logClear ("start (phase 1) node insert #[" + iCountOfNodesInserted + "]\r\n");

        //Log.log ( " updateDB_WithThis_CFD_NodeAndNodePathAndNodeData considering processing of [" + sURL+ "]\r\n");
        if (!sURL.toLowerCase().endsWith("000.html") && bBeenInsertedToDb == false) {
            //if ( sURL.toLowerCase().startsWith ("file:") )
            String sOraCleanUrl = UtilStrings.replaceStrInStr(sURL, "'", "''");
            sOraCleanUrl = UtilFileAndDirNames.stripUpToAndIncl_IndraHome(sOraCleanUrl, false);


            // ******************************
            // insert NODE table 1
            // ******************************

            //Log.log ( " Hier. insert node [" + lNodeID + "] sURL [" + sURL + "]\r\n");
            String sParentNodeID = "-1";

            if (parentFileDescriptor != null) {
                sParentNodeID = Long.toString(parentFileDescriptor.lNodeID);
            }

            long lNodeSize = getTopic_htWordsDocOnly_noDescendants(dbc_, wordfreq, sigparms).size();
            //lNodeSize = -111;

            String sTitleTempTruncate = this.getTitle();
            int iLenToKeepOfTitle = UtilSets.min(255, sTitleTempTruncate.length());
            sTitleTempTruncate = sTitleTempTruncate.substring(0, iLenToKeepOfTitle);

            String sOraCleanTitle = UtilStrings.replaceStrInStr(sTitleTempTruncate, "'", "''");


            String sNodeTitleForInsert = "'" + sOraCleanTitle + "'";
            String sqlUpdate = "insert into node ( nodeID, corpusID , nodeTitle, nodeSize, DateScanned, DateUpdated, "
                    + " ParentID, NODEINDEXWITHINPARENT, DEPTHFROMROOT ) values ( "
                    + lNodeID + ","
                    + iCorpusID + ","
                    + sNodeTitleForInsert + "," + // nodeTitle
                    lNodeSize + "," + // nodesize
                    " SYSDATE, "
                    + " SYSDATE, "
                    + sParentNodeID + ", "
                    + iIndexWithinParent + ", "
                    + iNodeDepth_0_IsRoot
                    + ")";
            try {
                //.Log.Log ("sqlUpdate cfd [" + sqlUpdate + "]" );
                JDBCIndra_Connection.executeInsertToDB(sqlUpdate, dbc_, bCommit);
                vTrackerOfDBUpdates.addElement(new Long(lNodeID));
            } catch (Exception e) {
                api.Log.LogError("a1 " + sqlUpdate, e);
                throw e;
            }


            // ******************************
            // insert NODEPATH table 2
            // ******************************
            // be sure at 30,000
            if (sOraCleanUrl.toLowerCase().startsWith("file:///")) {
                sOraCleanUrl = sOraCleanUrl.substring(8);
            }
            if (sOraCleanUrl.endsWith("/")) {
                sOraCleanUrl = sOraCleanUrl.substring(0, sOraCleanUrl.length() - 1);
            }

            iNodePathInsertCounts++;
            sqlUpdate = "insert into nodepath ( nodeid, path ) values ( "
                    + lNodeID + ",'" + // fileid
                    sOraCleanUrl // path
                    //sOraCleanUrl.toLowerCase()  // path
                    + "')";

            try {
                //api.Log.Log (iNodePathInsertCounts + ". nodepath insert node [" + this.lNodeID + "] insert sql [" + sqlUpdate  + "]" );
                JDBCIndra_Connection.executeInsertToDB(sqlUpdate, dbc_, bCommit);
            } catch (Exception e) {
                api.Log.LogError("a1 " + sqlUpdate, e);
                throw e;
            }

            // ******************************
            // insert NODEDATA table 3
            // ******************************
            String sTopicText = this.getThisCFDTopic(null, wordfreq, sigparms).getAllTextInOne_delimRemoved(false, false);
            //api.Log.Log ("nodedata clob insert [" + lNodeID + "] [" + sTopicText + "]" );
            UtilClob.insertClobToNodeData(lNodeID, sTopicText, dbc_);

            bBeenInsertedToDb = true;  // or else don't want to
        }				 // if ( !sURL.toLowerCase().endsWith( "000.html") )

        if (bFreeMemForGCBetNodeInsAndSigGen) {
            this.setThisCFDTopicAndHTsNull();
            //System.gc();
        } else {
            api.Log.Log("updateDB_WithThis_CFD_NodeAndNodePathAndNodeData  not clearing");
        }
    } // updateDB_WithThis_CFD_NodeAndNodePathAndNodeData

    // ************************************************
    public StringAndCount[] getSignature() // ************************************************
    {
        if (signature == null) {
        } else {
            return signature;
        }

        return null;
    }

    // ************************************************
    public static void getAllLeafsUnderMe_Recursive(CorpusFileDescriptor cfd, Vector vInOutAggregator)
            // ************************************************
            throws Exception {
        try {
            // RECURSE INTO MY FOLDERS IF ANY
            Vector vCFD_childrenFolders = cfd.getChildrenFolders();
            if (vCFD_childrenFolders != null && vCFD_childrenFolders.size() > 0) {
                Enumeration eCFD_childrenFolders = vCFD_childrenFolders.elements();
                while (eCFD_childrenFolders.hasMoreElements()) {
                    getAllLeafsUnderMe_Recursive((CorpusFileDescriptor) eCFD_childrenFolders.nextElement(), vInOutAggregator);
                }
            } else {
                vInOutAggregator.addElement(cfd);
            }
        } catch (Exception e) {
            Log.FatalError("in printtree ", e);
        }
    }

    public String toString() {
        try {
            String stopic = null;
            if (topicThisCFD != null) {
                stopic = topicThisCFD.toString();
            }

            return "\r\n\r\n ==== START CFD ============= id [" + lNodeID + "] file [" + sFileName
                    + "] \r\nstopic [" + stopic
                    + "] \r\n ==== END CFD ============";
            //return "\r\nCFD file [" + sFileName + "] \r\nstopic [" + stopic + "] \r\n cfd.getTopic_htWordsDocOnly_noDescendants [" + UtilSets.htToStr(getTopic_htWordsDocOnly_noDescendants( null) ) + "]" ;
        } catch (Throwable e) {
            e.printStackTrace();
            return "exception in cfd.tostring";
        }

    }

    public Topic getTopicThisCFD_noCache() {
        return topicThisCFD;
    }
}
