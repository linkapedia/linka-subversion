package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class EditGroupUsers
{
	public static Vector ExtractUsers(String sUsers) {
		Vector vUsers = new Vector();
		
		if (sUsers.equals("")) { return vUsers; }
		int iUserIndex = 0;
		
		while (sUsers.indexOf(",") != -1) {
			String sUser = sUsers.substring(iUserIndex, sUsers.indexOf(","));
			vUsers.addElement(sUser);
			
			sUsers = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sUsers,sUser+",",""));
		}
		if (sUsers.length() != 0) { 
			vUsers.addElement(sUsers);
		}
		
		return vUsers;
	}
		
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sGroupID = (String) props.get ("GroupID");
		String sGroupUserList = (String) props.get("GroupUsers_lst");

		// If no group user list was sent, display the page
		if (sGroupUserList == null) {
			try {
				if (sGroupID == null) { throw new Exception("A fatal error has occured."); }

				// Call: ListGroupMembers to retrieve Subscriber data
				HashTree htArguments = new HashTree(props);
				htArguments.put("GroupID", sGroupID);
				InvokeAPI API = new InvokeAPI ("tsuser.TSListGroups", htArguments);
				HashTree htResults = API.Execute(false, false);
				
				if (htResults.containsKey("SESSIONEXPIRED")) {
					Server.Login.handleTSapiRequest(props, out, req, res);
					return;
				}
				
				if (!htResults.containsKey("GROUPS")) { throw new Exception("Sorry, your session has unexpectedly timed out."); }
				HashTree htGroups = (HashTree) htResults.get("GROUPS");
				HashTree htGroup = (HashTree) htGroups.get("GROUP");
				Hashtable htFullUserList = new Hashtable();

				// Begin building the document
				HTMLDocument Document = new HTMLDocument();
				Document.SetHash(htGroup);
				Document.AddVariable("Title", "Edit user access list for a group");
				Document.AddVariable("OPTION_CORPUS_LIST", req);
				Document.WriteTemplate(out, "header-admin.tpl");
				Document.WriteTemplate(out, "group/usergroup-head.tpl");
			
				// Get all members of this group
				API = new InvokeAPI ("tsuser.TSListGroupMembers", htArguments);
				htResults = API.Execute(false, false);

				if (htResults.containsKey("SUBSCRIBERS")) { 
					HashTree htSubscribers = (HashTree) htResults.get("SUBSCRIBERS");
					Enumeration e2 = htSubscribers.elements();
					
					while (e2.hasMoreElements()) {
						HashTree htSubscriber = (HashTree) e2.nextElement();
						htFullUserList.put((String) htSubscriber.get("ID"),"1");
						Document.SetHash(htSubscriber);
						Document.WriteTemplate(out, "group/user-group.tpl");
					}
				}
			
				// Get all users!
				htArguments = new HashTree(props);
				API = new InvokeAPI ("tsuser.TSListUsers", htArguments);
				htResults = API.Execute(false, false);
			
				Document.WriteTemplate(out, "group/usergroup-middle.tpl");
				if (htResults.containsKey("SUBSCRIBERS")) { 
					HashTree htSubscribers = (HashTree) htResults.get("SUBSCRIBERS");
					Enumeration e2 = htSubscribers.elements();
					
					while (e2.hasMoreElements()) {
						HashTree htSubscriber = (HashTree) e2.nextElement();
						if (!htFullUserList.containsKey((String) htSubscriber.get("ID"))) {
							Document.SetHash(htSubscriber);
							Document.WriteTemplate(out, "group/user-group.tpl");
						}
					}
				}

				Document.WriteTemplate(out, "group/usergroup-foot.tpl");
				Document.WriteTemplate(out, "footer.tpl");
				
			} catch (Exception e) {
					HTMLDocument Document = new HTMLDocument();
					String sErrorMsg = e.getMessage();
					Document.WriteError(out, "<BLOCKQUOTE><BR>"+sErrorMsg+"</BLOCKQUOTE>");
					Log.LogError(e, out);
			}	
		} else {
			// Call: RemoveGroupUser -- remove all users in the group
			HashTree htArguments = new HashTree(props);
			htArguments.put("GroupID", sGroupID);
			InvokeAPI API = new InvokeAPI ("tsuser.TSRemoveGroupUser", htArguments);
			HashTree htResults = API.Execute(false, false);
				
			if (htResults.containsKey("SESSIONEXPIRED")) {
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}
				
			// Now, loop through each user specified to be in the user list.   For each one,
			// Call: AddGroupUser
			Vector vUserIDs = ExtractUsers(sGroupUserList);

			try {
				for (int i=0; i < vUserIDs.size(); i++) {
					htArguments = new HashTree(props);
					htArguments.put("UserID", (String) vUserIDs.elementAt(i));
					htArguments.put("GroupID", sGroupID);
					out.println("<!-- calling addusergroup -->");
					API = new InvokeAPI ("tsuser.TSAddGroupUser", htArguments);
					API.Execute(false, false);
				}
			} catch (Exception e) {
			  Log.LogError(e, out);
			}
			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "The specified group's user list has been updated successfully.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.ViewGroup'>"+
							  "Click here to return to the group menu</a>.";

			Document.WriteSuccess(out, sSuccess);
		}
	}
}
