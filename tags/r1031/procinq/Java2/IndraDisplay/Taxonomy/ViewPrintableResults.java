package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;
import java.net.URLEncoder;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.indraweb.execution.Session;
import com.iw.system.Node;
import com.iw.system.*;

import HTML.*;
import Server.*;
import Logging.*;

public class ViewPrintableResults {
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sCQL = (String) props.get ("CQL");		// The CQL to retrieve results for

        HTMLDocument Document = new HTML.HTMLDocument();
		Document.AddVariable("Title", "Printable Results");
		Document.AddVariable("OPTION_CORPUS_LIST", req);
        Document.AddVariable("CQL", sCQL);

        Document.WriteTemplate(out, "printable/head.tpl");

        ITS its = new ITS((String) props.get("SKEY"));
        Vector v = its.CQL(sCQL);

        if (v.size() == 0) {
            out.println("<tr><td><b>No results found.</b></td></tr>");
        } else {
            for (int i = 0; i < v.size(); i++) {
                Object o = (Object) v.elementAt(i);

                if (o instanceof NodeDocument) {
                    NodeDocument nd = (NodeDocument) v.elementAt(i);
                    Document.SetHash(nd);
                } else if (o instanceof Document) {
                    Document d = (Document) v.elementAt(i);
                    d.set("DOCSUMMARY", d.get("DOCUMENTSUMMARY"));
                    Document.SetHash(d);
                }
                Document.WriteTemplate(out, "printable/result.tpl");
            }
        }
        Document.WriteTemplate(out, "printable/foot.tpl");
    }
}
