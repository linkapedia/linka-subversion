package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;
import com.iw.system.*;


public class Tools
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Taxonomy Server Administration");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-nosearch.tpl");
			Document.WriteTemplate(out, "tools.tpl");

            HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tsuser.TSIsAdministrator", htArguments);
			HashTree htResults = API.Execute(false, false);

            if (htResults.containsKey("AUTHORIZED")) { Document.WriteTemplate(out, "tools-admin.tpl"); }
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
