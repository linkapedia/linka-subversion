package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class ProjFront
{
	// Display the revised front page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
        String id = (String) props.get ("zzzid", null);
        String tab = (String) props.get ("tab", "1");

        ITS its = new ITS((String) props.get("SKEY"));

        // Read EMAIL, DISTINGUISHED_NAME from cookie
        Cookie Cookies[] = req.getCookies();
        String sName = "User"; String sUserID = "null";

        if (req.getCookies() != null) {
            int cLen = Cookies.length;

            // look through and read from cookies
            for (int i=0; i < cLen; i++) {
                if (Cookies[i].getName().equals("ID")) { sUserID = (String) Cookies[i].getValue(); }
                if (Cookies[i].getName().equals("NAME")) { sName = (String) Cookies[i].getValue(); }
           }
        }
        sUserID = sUserID.replace('|', ',');

        // 4. The list of products (narratives) in the system
        Vector vNarrati = its.getConceptAlerts();

		HTMLDocument Document = new HTML.HTMLDocument();
		Document.AddVariable("Title", "ProcinQ Front Page");
        Document.AddVariable("Name", sName);
		Document.AddVariable("OPTION_CORPUS_LIST", req);

        // write header template
		Document.WriteTemplate(out, "front/schering-header"+tab+".tpl");

        if (tab.equals("2")) {
            Document.AddVariable("TYPE", "Projects");
            Document.WriteTemplate(out, "front/product-box-head.tpl");
            for (int i = 0; i < vNarrati.size(); i++) {
                Document d = (Document) vNarrati.elementAt(i);
                String query = URLEncoder.encode("SELECT <NARRATIVE> WHERE NARRATIVEID = "+d.get("DOCUMENTID"), "UTF-8");

                Document.SetHash(d.htd);
                Document.AddVariable("DOCURL", "/servlet/Main?template=Taxonomy.CQL&query="+query);
                if (d.get("NARRATIVEID").equals("3")) Document.WriteTemplate(out, "front/product-box-result.tpl");
            }
            if (vNarrati.size() == 0) Document.WriteTemplate(out, "front/product-noresults.tpl");
            Document.WriteTemplate(out, "front/product-box-foot.tpl");
        }

        if (tab.equals("1")) {
            Vector vMyAlerts = its.getMyAlerts(sUserID);

            // 2 write the alert box
            Document.WriteTemplate(out, "front/alert-box-head.tpl");
            for (int i = 0; i < vMyAlerts.size(); i++) {
                Alert a = (Alert) vMyAlerts.elementAt(i);
                if (a.getName().length() > 35) { a.setName(a.getName().substring(0, 32)+"..."); }

                Document.AddVariable("ALERTNAME", a.getName());
                Document.AddVariable("QUERY", URLEncoder.encode(a.getCQL(), "UTF-8"));
                Document.AddVariable("FREQ", a.getRunFrequency().getName());
                Document.WriteTemplate(out, "front/alert-box-result.tpl");
            }
            if (vMyAlerts.size() == 0) Document.WriteTemplate(out, "front/alert-noresults.tpl");
            Document.WriteTemplate(out, "front/alert-box-foot.tpl");

            out.println("<P>");

            Vector vProjects = its.getMyGroups();
            Vector vProjAlerts = new Vector();
            SecurityGroup selectedGroup = null;

            // set it
            if (vProjects.size() > 0) {
                if (id == null) {
                    selectedGroup = (SecurityGroup) vProjects.elementAt(0);
                } else {
                    for (int i = 0; i < vProjects.size(); i++) {
                        SecurityGroup sg = (SecurityGroup) vProjects.elementAt(i);
                        if (sg.getID().equals(id)) selectedGroup = sg;
                    }
                }

                vProjAlerts = its.getMyAlerts(selectedGroup.getID());
            }

            // create the dropdown for the user's groups
            String sProjects = "";
            for (int i = 0; i < vProjects.size(); i++) {
                SecurityGroup sg = (SecurityGroup) vProjects.elementAt(i);
                if (sg.getName().length() > 16) { sg.setName(sg.getName().substring(0, 13)+"..."); }
                if (sg.getID().equals(selectedGroup.getID()))
                    sProjects = sProjects + "<option selected value="+sg.getID()+"> "+sg.getName()+"\n";
                else
                    sProjects = sProjects + "<option value="+sg.getID()+"> "+sg.getName()+"\n";
            }

            Document.AddVariable("PROJECTS", sProjects);


            // 1 write the project box
            Document.WriteTemplate(out, "front/project-box-head.tpl");
            for (int i = 0; i < vProjAlerts.size(); i++) {
                Alert a = (Alert) vProjAlerts.elementAt(i);
                if (a.getName().length() > 35) { a.setName(a.getName().substring(0, 32)+"..."); }

                Document.AddVariable("ALERTNAME", a.getName());
                Document.AddVariable("QUERY", URLEncoder.encode(a.getCQL(), "UTF-8"));
                Document.AddVariable("FREQ", a.getRunFrequency().getName());
                Document.WriteTemplate(out, "front/project-box-result.tpl");
            }
            if (vProjAlerts.size() == 0) Document.WriteTemplate(out, "front/project-noresults.tpl");
            Document.WriteTemplate(out, "front/project-box-foot.tpl");
        }

        if (tab.equals("4")) {
            // 3 write the taxonomy box
            Vector vCorpora = its.getCorpora();

            Document.WriteTemplate(out, "front/taxonomy-box-head.tpl");
            for (int i = 0; i < vCorpora.size(); i++) {
                Corpus c = (Corpus) vCorpora.elementAt(i);
                Document.AddVariable("CID", c.getID());
                Document.AddVariable("CNAME", c.getName());
                Document.WriteTemplate(out, "front/taxonomy-box-result.tpl");
            }
            if (vCorpora.size() == 0) Document.WriteTemplate(out, "front/taxonomy-noresults.tpl");
            Document.WriteTemplate(out, "front/taxonomy-box-foot.tpl");
        }

        if (tab.equals("3")) {
            // 4 write the product box
            Document.AddVariable("TYPE", "Products");
            Document.WriteTemplate(out, "front/product-box-head.tpl");
            for (int i = 0; i < vNarrati.size(); i++) {
                Document d = (Document) vNarrati.elementAt(i);
                Document.SetHash(d.htd);
                String query = URLEncoder.encode("SELECT <NARRATIVE> WHERE NARRATIVEID = "+d.get("DOCUMENTID"), "UTF-8");
                Document.AddVariable("DOCURL", "/servlet/Main?template=Taxonomy.CQL&query="+query);
                if (d.get("NARRATIVEID").equals("1")) Document.WriteTemplate(out, "front/product-box-result.tpl");
            }
            if (vNarrati.size() == 0) Document.WriteTemplate(out, "front/product-noresults.tpl");
            Document.WriteTemplate(out, "front/product-box-foot.tpl");
        }

        out.println("<P>");

		Document.WriteFooter(out);
	}
}
