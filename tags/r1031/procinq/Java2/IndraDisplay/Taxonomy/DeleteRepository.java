package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class DeleteRepository
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sID = (String) props.get ("RID");

		// Call: GetRepositoryProps
		HashTree htArguments = new HashTree(props);
		htArguments.put("RID", sID);
			
		InvokeAPI API = new InvokeAPI ("tsrepository.TSRemoveRepository", htArguments);
		HashTree htResults = API.Execute(false, false);

		if (htResults.containsKey("SESSIONEXPIRED")) {		
			Server.Login.handleTSapiRequest(props, out, req, res);
			return;
		}
						
		if (!htResults.containsKey("SUCCESS")) {
			HTMLDocument Document = new HTMLDocument();
			String sFail = "The selected repository could not be removed: Permission denied.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.Repository'>"+
							  "Click here to return to the repository menu</a>.";
			Document.WriteSuccess(out, sFail);
		} else {
			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "The selected repository has been removed successfully.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.Repository'>"+
							  "Click here to return to the repository menu</a>.";
			Document.WriteSuccess(out, sSuccess);
		}
	}
}
