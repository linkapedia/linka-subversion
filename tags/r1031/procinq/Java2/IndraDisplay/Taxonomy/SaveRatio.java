package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class SaveRatio
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception {

		String sRID = (String) props.get ("RID", true);
        String ratio = (String) props.get ("ratio", true);

		try {
			// Call: GetCorpusProps
			HashTree htArguments = new HashTree(props);
			htArguments.put("RID", sRID);
            htArguments.put("ratio", ratio);
			InvokeAPI API = new InvokeAPI ("tsrocsettings.TSEditROCSettingsProps", htArguments);
			HashTree htResults = API.Execute(false, false);

			// A success is indicated if there is no TSERROR tag in the return
			if (!htResults.containsKey("SUCCESS")) { throw new Exception("Edit ROC Settings: Permission Denied."); }

			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "ROC settings have been altered successfully!<P>"+
                              "<script>window.opener.location = window.opener.location.href;</script>"+
							  "<a href='javascript:window.close()'>"+
							  "Click here to return to the ROC settings menu</a>.";

			Document.WriteSuccess(out, sSuccess);

		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	}
}
