package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;

public class EditCorpusWatched
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sID = (String) props.get ("RepID", true);
        String sPath = (String) props.get ("Path", true);
        String submit = (String) props.get ("submit");

        if (submit != null) { // query was submitted, process and save results
        } else {
            try {
                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Assign Corpora to Watched Folder");
                Document.AddVariable("OPTION_CORPUS_LIST", req);
                Document.WriteTemplate(out, "popup-corpora-watched-folder.tpl");
                Document.WriteFooter(out);
            } catch (Exception e) {
                HTMLDocument Document = new HTMLDocument();
                String sErrorMsg = e.getMessage();
                Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
                Log.LogError(e, out);
            }
        }
	}
}
