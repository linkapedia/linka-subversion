package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class Search
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			// this page can be invoked three ways:
			//  1) request to display simple search RESULTS
			//  2) request to display advanced search PAGE
			//  3) request to display advanced search RESULTS
			String advanced = (String) props.get ("advanced");	// advanced ?
			String query = (String) props.get("query");
			String keywords = (String) props.get("Keywords");
			String region = (String) props.get("corpusid");

			if ((query == null) && (keywords != null)) { query = keywords; }
			
			if ((region == null) && (advanced != null)) { // #2
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Advanced Search");
				Document.AddVariable("OPTION_CORPUS_LIST", req);
				Document.WriteTemplate(out, "header-admin.tpl");
				Document.WriteTemplate(out, "search/advanced-search.tpl");
				Document.WriteFooter(out);
			} else if ((query != null) && (advanced == null)) {
				/* SIMPLE SEARCH QUERY was submitted.

				Here is some sample CQL:

				SELECT <DOCUMENT> WHERE (ABSTRACT LIKE 'prilosec' OR
				ABSTRACT LIKE 'immunodeficiency') AND
				(FULLTEXT LIKE 'prilosec' OR FULLTEXT LIKE 'immunodeficiency') AND
				nodetitle = 'Guideline' AND NODETITLE = 'Hiv Antibody'
                */

				// a) Replace troublesome characters
				// b) Break query string into components
				//query = new String(com.indraweb.util.UtilStrings.replaceStrInStr(query,"'","''"));
				Vector vQuery = com.indraweb.util.UtilStrings.splitByStrLen1(query, " ");

				// FULL TEXT query, TITLE, BIBLIOGRAPHY, ABSTRACT, CATEGORY_NAME
				String sQuery5 = "SELECT <NODE> WHERE ("+BuildQuery(query, "NODETITLE")+")";
				String sQuery1 = "SELECT <DOCUMENT> WHERE ("+BuildQuery(query, "FULLTEXT")+") ";
				String sQuery2 = "SELECT <DOCUMENT> WHERE ("+BuildQuery(query, "DOCTITLE")+") ";
                String sQuery3 = query;

                // tack on corpus ID if it was sent in a constraint
                if ((region != null) && (!region.equals("0"))) { sQuery5 = sQuery5 + "AND CORPUSID = '"+region+"'"; }

				out.println("<!-- CQL Query (Category Text): "+sQuery5+" -->");
				out.println("<!-- CQL Query (Full text): "+sQuery1+" -->");
				out.println("<!-- CQL Query (Document title): "+sQuery2+" -->");
                out.println("<!-- Related Query (Node title): "+sQuery3+" -->");

				// Spin a thread for each query, run in parallel and print results as each return
				// order: category, full text, title, abstract, then bibliography
				QThread qt5 = new QThread(sQuery5, props, out, 4); qt5.start();
				QThread qt1 = new QThread(sQuery1, props, out); qt1.start();
				QThread qt2 = new QThread(sQuery2, props, out); qt2.start();
                QThread qt3 = null;

                if (region == null) { qt3 = new QThread(sQuery3, props, out, 4); }
                else { qt3 = new QThread(sQuery3, props, out, region); }
                qt3.bRelated = true; qt3.start();

				// Print HTML header information, then process each result set
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Simple Search Results");
				Document.AddVariable("QUERY", "Search Results for Terms:&nbsp;&nbsp;"+query);
				Document.AddVariable("CORPUSID", region);
                Document.AddVariable("OPTION_CORPUS_LIST", req);

				Document.WriteTemplate(out, "header-admin.tpl");
				Document.WriteTemplate(out, "search/head.tpl");

				// CATEGORY RESULTS first
				qt5.join(); HashTree htResults = qt5.GetResults();
				if (!htResults.containsKey("NODES")) {
					Document.AddVariable("MAXRES", "0");
					Document.AddVariable("NUMRES", "0");
					Document.WriteTemplate(out, "search/category-head.tpl");
					out.println("<!-- no category hits found for: "+qt5.sQuery+" --> ");
					api.Log.Log("Warning: no category hits found for query: "+qt5.sQuery);
					Document.WriteTemplate(out, "search/no-category-result.tpl");
				} else {
					// Process NODE RESULTS
					HashTree htNodes = (HashTree) htResults.get("NODES");

					// pass CQL query in for More Results
					Document.AddVariable("CQL", URLEncoder.encode(sQuery5, "UTF-8"));

					// determine number of results and write into variables
					int iMaxRes = new Integer((String) htResults.get("NUMRESULTS")).intValue();
					int iNumRes = iMaxRes; if (iNumRes > 4) { iNumRes = 4; }
					Document.AddVariable("NUMRES", iNumRes+"");
					Document.AddVariable("MAXRES", ""+iMaxRes);
					Document.WriteTemplate(out, "search/category-head.tpl");

					Enumeration eN = htNodes.elements();
					Document.WriteTemplate(out, "search/category-start.tpl");

					int loop = 0;
					while (eN.hasMoreElements()) {
						loop++;
						if (loop == 3) {
							out.println("<!-- middle -->");
							Document.WriteTemplate(out, "search/category-middle.tpl");
						}
						HashTree htNode = (HashTree) eN.nextElement();

						String sParentID = (String) htNode.get("PARENTID");
						String sParentTitle = "Root";

						if (!sParentID.equals("-1")) {
							HashTree htArguments = new HashTree(props);
							htArguments.put("NodeID", sParentID);
							InvokeAPI API = new InvokeAPI ("tsnode.TSGetNodeProps", htArguments);
							HashTree htNodeResults = API.Execute(false, false);

							if (htNodeResults.containsKey("NODE")) {
								sParentTitle = (String) ((HashTree) htNodeResults.get("NODE")).get("NODETITLE");
							}
						}

						Document.AddVariable("PARENT", sParentTitle);
						Document.SetHash(htNode);
						Document.WriteTemplate(out, "search/category-result.tpl");
					}
					Document.WriteTemplate(out, "search/category-foot.tpl");
				}

				// FULL TEXT RESULTS
				out.println("<!-- about to join full text .. -->"); out.flush();
				qt1.join(); htResults = qt1.GetResults();
				out.println("<!-- joined full text .. -->"); out.flush();

                Document.ClearHash();

				if (!htResults.containsKey("DOCUMENTS")) {
					Document.AddVariable("MAXRES", "0");
					Document.AddVariable("NUMRES", "0");
					Document.WriteTemplate(out, "search/fulltext-head.tpl");
					out.println("<!-- no full text hits found for: "+qt1.sQuery+" --> ");
					api.Log.Log("Warning: no full text hits found for query: "+qt1.sQuery);
					Document.WriteTemplate(out, "search/no-fulltext-result.tpl");
				} else {
					// Process DOCUMENT RESULTS
					HashTree htDocuments = (HashTree) htResults.get("DOCUMENTS");

					// pass CQL query in for More Results
					Document.AddVariable("CQL", URLEncoder.encode(sQuery1, "UTF-8"));

					// determine number of results and write into variables
					int iMaxRes = new Integer((String) htResults.get("NUMRESULTS")).intValue();
					int iNumRes = iMaxRes; if (iNumRes > 5) { iNumRes = 5; }
					Document.AddVariable("NUMRES", iNumRes+"");
					Document.AddVariable("MAXRES", ""+iMaxRes);
					Document.WriteTemplate(out, "search/fulltext-head.tpl");

					Enumeration eN = htDocuments.elements();
					Document.WriteTemplate(out, "search/fulltext-start.tpl");

					int loop = 0;
					while (eN.hasMoreElements()) {
						loop++; Document.AddVariable("LOOP", loop+"");
						HashTree htDoc = (HashTree) eN.nextElement();
						htDoc.put("DOCUMENTSUMMARY",
							(TruncateDocumentSummary((String) htDoc.get("DOCUMENTSUMMARY"))));

						// encode title or url
						String sEDocTitle = URLEncoder.encode((String) htDoc.get("DOCTITLE"), "UTF-8");
						String sEDocURL = URLEncoder.encode((String) htDoc.get("DOCURL"), "UTF-8");
						Document.AddVariable("EDOCURL", sEDocURL);
						Document.AddVariable("EDOCTITLE", sEDocTitle);

						Document.SetHash(htDoc);
						Document.WriteTemplate(out, "search/fulltext-result.tpl");
					}
				}
				Document.WriteTemplate(out, "search/fulltext-foot.tpl");

				// TITLE RESULTS
				out.println("<!-- about to join doc title .. -->"); out.flush();
				qt2.join(); htResults = qt2.GetResults();
				out.println("<!-- joined doc title .. -->"); out.flush();
				if (!htResults.containsKey("DOCUMENTS")) {
					Document.AddVariable("NUMRES", "0");
					Document.AddVariable("MAXRES", "0");
					Document.WriteTemplate(out, "search/doctitle-head.tpl");
					out.println("<!-- no title hits found for: "+qt2.sQuery+" --> ");
					api.Log.Log("Warning: no title hits found for query: "+qt1.sQuery);
					Document.WriteTemplate(out, "search/no-doctitle-result.tpl");
				} else {
					// Process DOCUMENT RESULTS
					HashTree htDocuments = (HashTree) htResults.get("DOCUMENTS");

					// pass CQL query in for More Results
					Document.AddVariable("CQL", URLEncoder.encode(sQuery2, "UTF-8"));

					// determine number of results and write into variables
					int iMaxRes = new Integer((String) htResults.get("NUMRESULTS")).intValue();
					int iNumRes = iMaxRes; if (iNumRes > 5) { iNumRes = 5; }
					Document.AddVariable("NUMRES", iNumRes+"");
					Document.AddVariable("MAXRES", ""+iMaxRes);
					Document.WriteTemplate(out, "search/doctitle-head.tpl");

					Enumeration eN = htDocuments.elements();
					Document.WriteTemplate(out, "search/fulltext-start.tpl");

					int loop = 0;
					while (eN.hasMoreElements()) {
						loop++; Document.AddVariable("LOOP", loop+"");
						HashTree htDoc = (HashTree) eN.nextElement();
						htDoc.put("DOCUMENTSUMMARY",
							(TruncateDocumentSummary((String) htDoc.get("DOCUMENTSUMMARY"))));

						// encode title or url
						String sEDocTitle = URLEncoder.encode((String) htDoc.get("DOCTITLE"), "UTF-8");
						String sEDocURL = URLEncoder.encode((String) htDoc.get("DOCURL"), "UTF-8");
						Document.AddVariable("EDOCURL", sEDocURL);
						Document.AddVariable("EDOCTITLE", sEDocTitle);

						Document.SetHash(htDoc);
						Document.WriteTemplate(out, "search/fulltext-result.tpl");
					}
				}
				Document.WriteTemplate(out, "search/fulltext-foot.tpl");

                // CATEGORY RESULTS first
				qt3.join(); htResults = qt3.GetResults();
				if (!htResults.containsKey("NODES")) {
					Document.AddVariable("MAXRES", "0");
					Document.AddVariable("NUMRES", "0");
					Document.WriteTemplate(out, "search/category-head.tpl");
					out.println("<!-- no category hits found for: "+qt3.sQuery+" --> ");
					api.Log.Log("Warning: no category hits found for query: "+qt3.sQuery);
					Document.WriteTemplate(out, "search/no-category-result.tpl");
				} else {
					// Process NODE RESULTS
					HashTree htNodes = (HashTree) htResults.get("NODES");

					// pass CQL query in for More Results
					Document.AddVariable("CQL", "&rquery="+URLEncoder.encode(query, "UTF-8"));

					// determine number of results and write into variables
					int iMaxRes = new Integer((String) htResults.get("NUMRESULTS")).intValue();
					int iNumRes = iMaxRes; if (iNumRes > 4) { iNumRes = 4; }
					Document.AddVariable("NUMRES", iNumRes+"");
					Document.AddVariable("MAXRES", ""+iMaxRes);
					Document.WriteTemplate(out, "search/related-category-head.tpl");

					Enumeration eN = htNodes.elements();
					Document.WriteTemplate(out, "search/category-start.tpl");

					int loop = 0;
					while (eN.hasMoreElements()) {
						loop++;
						if (loop == 3) {
							out.println("<!-- middle -->");
							Document.WriteTemplate(out, "search/category-middle.tpl");
						}
						HashTree htNode = (HashTree) eN.nextElement();

						String sParentID = (String) htNode.get("PARENTID");
						String sParentTitle = "Root";

						if (!sParentID.equals("-1")) {
							HashTree htArguments = new HashTree(props);
							htArguments.put("NodeID", sParentID);
							InvokeAPI API = new InvokeAPI ("tsnode.TSGetNodeProps", htArguments);
							HashTree htNodeResults = API.Execute(false, false);

							if (htNodeResults.containsKey("NODE")) {
								sParentTitle = (String) ((HashTree) htNodeResults.get("NODE")).get("NODETITLE");
							}
						}

						Document.AddVariable("PARENT", sParentTitle);
						Document.SetHash(htNode);
						Document.WriteTemplate(out, "search/category-result.tpl");
					}
					Document.WriteTemplate(out, "search/category-foot.tpl");
				}
                Document.WriteFooter(out);

			} else {
				// ADVANCED SEARCH WAS REQUESTED
				String Qfulltext = (String) props.get ("fulltext");
                String Qfulltextor = (String) props.get ("fulltextor");
				String Qtitle = (String) props.get("title");
                String Qdate = (String) props.get("datelastfound");
				String Qkeywords = (String) props.get("keywords");
				String sCQL = (String) props.get("cquery");
                String sRelated = (String) props.get("rquery");
				String sStart = (String) props.get("start", "1");
				String sType = (String) props.get("type");

                // full text combines require a very strange syntax.  adjust it here if applicable
                if (Qfulltextor == null) Qfulltextor = "";
                if (Qfulltextor.length() > 0) {
                    if (Qfulltext.length() > 0) Qfulltext = "("+Qfulltext+")";
                    if (Qtitle.length() > 0) Qtitle = "("+Qtitle+")";
                }

				// get RESULTSPERPAGE from COOKIE
				int iResultsPerPage = 20;
				Cookie Cookies[] = req.getCookies();

				if (req.getCookies() != null) {
					int cLen = Cookies.length;
					for (int i=0; i < cLen; i++) {
						if (Cookies[i].getName().equals("RESULTSPERPAGE")) {
							String sEnd = (String) Cookies[i].getValue();
							iResultsPerPage = new Integer(sEnd).intValue();
						}
					}
				}

				int iStart = new Integer(sStart).intValue();

				if (sCQL == null) {
					sCQL = "SELECT <DOCUMENT> WHERE ";

					// Build the CQL advanced query
					if (!Qfulltext.equals("")) {
						sCQL=sCQL+"("+BuildQuery(Qfulltext,"FULLTEXT")+")";
					}
					if (!Qtitle.equals("")) {
						if (sCQL.endsWith(")")) { sCQL = sCQL + " AND "; }
						sCQL=sCQL+"("+BuildQuery(Qtitle,"DOCTITLE")+")";
					}
                    // MM/DD/YYYY
                    // if the date has changed, parse it into the format that CQL expects
                    if ((!Qdate.equals("")) && (!Qdate.startsWith("MM"))) {
                        try {
                            if (Qdate.length() != 10) { throw new MalformedDateException(); }

                            int month = -1; int day = -1; int year = -1;

                            try { month = Integer.parseInt(Qdate.substring(0, 2));
                                  day = Integer.parseInt(Qdate.substring(3, 5));
                                  year = Integer.parseInt(Qdate.substring(6));
                            } catch (Exception e) { throw new MalformedDateException(); }

                            String mon = "";
                            switch (month) {
                                case 1:  mon = "JAN"; break;
                                case 2:  mon = "FEB"; break;
                                case 3:  mon = "MAR"; break;
                                case 4:  mon = "APR"; break;
                                case 5:  mon = "MAY"; break;
                                case 6:  mon = "JUN"; break;
                                case 7:  mon = "JUL"; break;
                                case 8:  mon = "AUG"; break;
                                case 9:  mon = "SEP"; break;
                                case 10: mon = "OCT"; break;
                                case 11: mon = "NOV"; break;
                                case 12: mon = "DEC"; break;
                                default: throw new MalformedDateException();
                            } Qdate = day+"-"+mon+"-"+Qdate.substring(8);

                            if (sCQL.endsWith(")")) { sCQL = sCQL + " AND "; }
                            boolean bUseDate = true;

                            if (bUseDate) sCQL=sCQL+"(DATELASTFOUND >= '"+Qdate+"')";
                        } catch (MalformedDateException mde) {
                            HTMLDocument doc = new HTMLDocument();
                            doc.WriteError(out, "Illegal date format in the advanced search field.");
                        } catch (Exception e) { throw e; }
                    }
					if (!Qkeywords.equals("")) {
						if (sCQL.endsWith(")")) { sCQL = sCQL + " AND "; }
                        // nodetitle query in the form of: "A" OR "B" AND "C"
                        Vector v = com.indraweb.util.UtilStrings.splitByStrLenLong(query, " ");
                        int i = 0; sCQL=sCQL+"((NODETITLE = '";

                        for (i = 0; i < v.size(); i++) {
                            String s = (String) v.elementAt(i);
                            if ((s.toLowerCase().equals("and")) ||
                                (s.toLowerCase().equals("or")) ||
                                (s.toLowerCase().startsWith("near")) ||
                                (s.toLowerCase().equals("not"))) {
                                sCQL = sCQL+"') "+s+" (NODETITLE = '";
                            } else {
                                s = com.indraweb.util.UtilStrings.replaceStrInStr(s, "\"", "");
                                sCQL=sCQL+s; String s2 = null;

                                if ((i+1) < v.size()) { s2 = (String) v.elementAt(i+1); }
                                if ((s2 != null) &&
                                    (!s2.toLowerCase().equals("and")) &&
                                    (!s2.toLowerCase().equals("or")) &&
                                    (!s2.toLowerCase().startsWith("near")) &&
                                    (!s2.toLowerCase().equals("not"))) { sCQL=sCQL+" "; }
                          }
                        }
                        sCQL=sCQL+"'))";
 					}
	                if ((region != null) && (!region.equals("All"))) { sCQL = sCQL + "AND CORPUSID = '"+region+"'"; }
                    if (!Qfulltextor.equals("")) {
                        sCQL=sCQL+" COMBINE FULLTEXT LIKE '("+Qfulltextor+")'";
                    }
				}

                out.println("<!-- CQL: "+sCQL+" -->");

  				// send off the query
				QThread qt = null;

                if (sRelated != null) {
                    if (region == null) { qt = new QThread(sRelated, props, out, 1000); }
                    else { qt = new QThread(sRelated, props, out, region, 1000); }
                    qt.bRelated = true; sType = "node";
                } else if (sType != null) { qt = new QThread(sCQL, props, out, 1000);  }
				else { qt = new QThread(sCQL, props, out, iResultsPerPage, iStart);  }
				qt.start();
				qt.join(); HashTree htResults = qt.GetResults();

                String escaped = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sCQL, "'", "''"));
                api.Log.Log("Before: "+escaped);
                escaped = URLEncoder.encode(escaped, "UTF-8");
                api.Log.Log("After: "+escaped);

  				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Advanced Search Results");
                Document.AddVariable("ESCAPED", escaped);
                Document.AddVariable("BLORG", URLEncoder.encode(sCQL, "UTF-8"));

				// MH has requested the SELECT <DOCUMENT> FROM be lopped off of the
				// CQL displayed to the user ..
				String sCQLtoUser = sCQL;
				if (sCQL.startsWith("SELECT <DOCUMENT> WHERE")) {
					sCQLtoUser = "Show Documents where: "+com.indraweb.util.UtilStrings.replaceStrInStr(sCQLtoUser,"SELECT <DOCUMENT> WHERE","");
				} else { sCQLtoUser = "Show Documents where: "+com.indraweb.util.UtilStrings.replaceStrInStr(sCQLtoUser,"SELECT <NODE> WHERE",""); }

                if (sRelated != null) { sCQLtoUser = "(related) "+sRelated; }
				Document.AddVariable("QUERY", sCQLtoUser);

				Document.WriteTemplate(out, "header-admin.tpl");
				Document.WriteTemplate(out, "search/head-full.tpl");

				// yuck.. advanced searches could take NODES too if MORE RESULTS
				// was selected from the SIMPLE SEARCH page.   So we need a big IF..
				if (sType == null) {
					if (!htResults.containsKey("DOCUMENTS")) {
						out.println("<blockquote>Sorry, no results were found.</blockquote>");
						return;
					} else {
						HashTree htDocuments = (HashTree) htResults.get("DOCUMENTS");

						// determine number of results and write into variables
						int iMaxRes = new Integer((String) htResults.get("NUMRESULTS")).intValue();
						int iNumRes = java.lang.Math.min(iMaxRes, (iStart+iResultsPerPage-1));
						Document.AddVariable("NUMRES", iNumRes+"");
						Document.AddVariable("MAXRES", ""+iMaxRes);

						Enumeration eD = htDocuments.elements();
						Document.AddVariable("START", ""+iStart);
						Document.WriteTemplate(out, "search/advanced-head.tpl");

						int loop = iStart-1;
						while (eD.hasMoreElements()) {
							loop++; Document.AddVariable("LOOP", loop+"");
							HashTree htDocument = (HashTree) eD.nextElement();
							htDocument.put("DOCUMENTSUMMARY",
								(TruncateDocumentSummary((String) htDocument.get("DOCUMENTSUMMARY"))));

							// encode title or url
							out.println("<!-- DocID: "+(String) htDocument.get("DOCUMENTID")+" -->");
							String sEDocTitle = URLEncoder.encode((String) htDocument.get("DOCTITLE"), "UTF-8");
							String sEDocURL = URLEncoder.encode((String) htDocument.get("DOCURL"), "UTF-8");
							Document.AddVariable("EDOCURL", sEDocURL);
							Document.AddVariable("EDOCTITLE", sEDocTitle);

							Document.SetHash(htDocument);
							Document.WriteTemplate(out, "search/fulltext-result.tpl");
						}
						if (iMaxRes > iResultsPerPage) {
							String sNextPrev = BuildNextPrev(iStart, iResultsPerPage, iMaxRes, URLEncoder.encode(sCQL, "UTF-8"));
							Document.AddVariable("NEXTPREV", sNextPrev);
						}
						Document.WriteTemplate(out, "search/fulltext-foot.tpl");
					}
				} else {
					if (!htResults.containsKey("NODES")) {
						out.println("<blockquote>Sorry, no results were found.</blockquote>");
						return;
					} else {
						// Process NODE RESULTS
						HashTree htNodes = (HashTree) htResults.get("NODES");

						// determine number of results and write into variables
						int iMaxRes = new Integer((String) htResults.get("NUMRESULTS")).intValue();
						int iNumRes = iMaxRes;
						Document.AddVariable("NUMRES", iNumRes+"");
						Document.AddVariable("MAXRES", ""+iMaxRes);
						Document.WriteTemplate(out, "search/category-head.tpl");

						Enumeration eN = htNodes.elements();
						Document.WriteTemplate(out, "search/category-start.tpl");

						int loop = 0;
						while (eN.hasMoreElements()) {
							loop++;
							HashTree htNode = (HashTree) eN.nextElement();
							String sNodeTree
								= BuildNodeTree
								(out, (String) htNode.get("NODEID"), props);

							Document.AddVariable("NODETREE", sNodeTree);
							htNode.put("NODETITLE", TruncateDocumentSummary
								((String) htNode.get("NODETITLE"), 25));

							Document.SetHash(htNode);

							Document.WriteTemplate(out, "search/advanced-category-result.tpl");
						}
						Document.WriteTemplate(out, "search/category-foot.tpl");
					}
				}
			Document.WriteFooter(out);
			}
		} catch (Exception e) { e.printStackTrace(out); }
	}

	public static String BuildNextPrev (int Start, int Results, int Max, String CQL) {
		String sNP = new String("<table width=100% cellpadding=2 "+
								"cellspacing=0 border=0 align=center><tr><td align=center>");
		boolean bNext = false;
		boolean bPrev = false;

		String sURL = "/servlet/Main?template=Taxonomy.Search&cquery="+CQL;

		if ((Start - Results) > 0) { bPrev = true; }
		if (((Start + Results - 1) + Results) <= Max) { bNext = true; }

		if (bPrev) { sNP = sNP+"[<a href='"+sURL+"&start="+(Start-Results)+"'>Prev</a>] "; }
		else { sNP = sNP+"[Prev] "; }

		// ----- figure out what's in-between ..
		int loop = 0;
		for (int i = 1; i <= Max; i = i + Results) {
			loop++;
			if (i != Start) {
				sNP = sNP + "[ <a href='"+sURL+"&start="+i+"'>"+loop+"</a>] ";
			} else { sNP = sNP + " ["+loop+"] "; }
		}

		if (bNext) { sNP = sNP+"[<a href='"+sURL+"&start="+(Start+Results)+"'>Next</a>] "; }
		else { sNP = sNP+"[Next] "; }

		sNP = sNP + "</td></tr></table>";
		return sNP;
	}

	private static class QThread extends Thread {
		// query is public
		public String sQuery = "";
        public boolean bRelated = false;

		// private access only to these variables
		private APIProps props = null;
		private PrintWriter out = null;
		private HashTree htResults = new HashTree();
		private int iNumResults = 5;
        private String sRegion = null;
		private int iStart = 1; // default to start at first result

		// the CONSTRUCTORS
		public QThread (String sQuery, APIProps props, PrintWriter out) {
			this.sQuery = sQuery;
			this.props = props;
			this.out = out;
			this.iNumResults = 5;
			this.iStart = 1;
		}
        public QThread (String sQuery, APIProps props, PrintWriter out, String sRegion) {
			this.sQuery = sQuery;
			this.props = props;
			this.out = out;
			this.iNumResults = 4;
			this.iStart = 1;
            this.sRegion = sRegion;
        }
        public QThread (String sQuery, APIProps props, PrintWriter out, String sRegion, int iNumResults) {
			this.sQuery = sQuery;
			this.props = props;
			this.out = out;
			this.iNumResults = 4;
			this.iStart = 1;
            this.sRegion = sRegion;
			this.iNumResults = iNumResults;
        }
        public QThread (String sQuery, APIProps props, PrintWriter out, int iNumResults) {
			this.sQuery = sQuery;
			this.props = props;
			this.out = out;
			this.iNumResults = iNumResults;
			this.iStart = 1;
		}
		public QThread (String sQuery, APIProps props, PrintWriter out,
						int iNumResults, int iStart) {
			this.sQuery = sQuery;
			this.props = props;
			this.out = out;
			this.iNumResults = iNumResults;
			this.iStart = iStart;
		}

		// Accessor method for results set
		public HashTree GetResults() { return htResults; }

		public void run() {
			HashTree htArguments = new HashTree(props);

			htArguments.put("query", sQuery);
			htArguments.put("rowmax", ""+iNumResults);
			htArguments.put("start", ""+iStart);
            InvokeAPI API = null;

            if (bRelated) {
                if (sRegion != null) { htArguments.put("CorpusID", sRegion); }
                htArguments.put("Related", "true");
                htArguments.put("Thesaurus", "true");
                htArguments.put("MaxResults", ""+iNumResults);
                API = new InvokeAPI ("tsnode.TSSearch", htArguments);
            } else { API = new InvokeAPI ("tscql.TSCql", htArguments); }
            try { htResults = API.Execute(false, false, false); }
            catch (Exception e) { api.Log.LogError(e); return; }
		}
	}

	public static String BuildQuery (String query, String sQueryType) {
		return BuildQuery (query, sQueryType, "LIKE");
	}

	public static String BuildNodeTree(PrintWriter out, String sNodeID, APIProps props) {
        ITS its = new ITS((String) props.get("SKEY"));
        Vector vNodes = new Vector();
        try { vNodes = its.getNodeTree(sNodeID); }
        catch (Exception e) { api.Log.LogError(e); }

		// If this node doesn't exist, return an empty string
		if (vNodes.size() < 1) { return ""; }

		String sTaxonomy = new String("");

        for (int i = 0; i < vNodes.size(); i++) {
			Node n = (Node) vNodes.elementAt(i);

			if (n.get("NODETITLE").length() > 25) {
				n.set("NODETITLE", n.get("NODETITLE").substring(0, 25)+"...");
			}

			// If this is the parent node, return now
			if (sTaxonomy.equals("")) {
				sTaxonomy = "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
					    n.get("NODEID")+"'> "+
					    n.get("NODETITLE")+"</a> ";
			} else {
				sTaxonomy = "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
					    n.get("NODEID")+"'> "+
					    n.get("NODETITLE")+"</a> > "+sTaxonomy;
			}

		}
		return sTaxonomy;
	}
	public static String BuildQuery (String query, String sQueryType, String sOperator) {
		return sQueryType+" "+sOperator+" '"+query+"'";
 	}

	public static Hashtable GetNextWord (char[] cArr, int i, int j, String sQuery) {
		Hashtable ht = new Hashtable();
		String sWord = "";

		if (j >= cArr.length) { ht.put("", j+""); return ht; }

		if (cArr[j] == '"') {
			j++;
			while ((cArr[j] != '"') && (j < sQuery.length())) { j++; }
			sWord = new String().copyValueOf(cArr, i+1, ((j-i)-1));
		} else {
			while ((cArr[j] != ' ') && (cArr[j] != ')') && (j < sQuery.length())) { j++; }
			sWord = new String().copyValueOf(cArr, i, (j-i));
			if (cArr[j] == ')') { j=j-1; }
		}

		ht.put(sWord, j+"");
		return ht;
	}

	// if document is greater than 300 characters, truncate at
	// next available whitespace
	public static String TruncateDocumentSummary (String sDocSummary) {
		return TruncateDocumentSummary (sDocSummary, 300); }
	public static String TruncateDocumentSummary (String sDocSummary, int length) {
		if (sDocSummary == null) { sDocSummary = "(no abstract found)"; }
		while ((sDocSummary.length() > length) && (sDocSummary.charAt(length) != ' ')) { length++; }
		if (sDocSummary.length() > length) {
			sDocSummary = sDocSummary.substring(0, length)+"...";
		}

		if (sDocSummary.equals("null")) { sDocSummary = "No abstract found."; }
		return sDocSummary;
	}
}