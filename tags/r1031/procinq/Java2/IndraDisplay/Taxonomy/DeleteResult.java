package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class DeleteResult
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sDocID = (String) props.get ("DocumentID", true);

		try {
			// Finally! Call: TSAddCorpusBlacklist
            HashTree htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			htArguments.put("DocumentID", sDocID);
			InvokeAPI API = new InvokeAPI ("tsnode.TSRemoveNodeDocument", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (!htResults.containsKey("SUCCESS")) {
				throw new Exception("Sorry, that document cannot be removed from this topic..");
			}
			
			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "The specified document has been deleted successfully.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+sNodeID+"'>"+
							  "Click here to return to the result list for this topic</a>.";

			Document.WriteSuccess(out, sSuccess);
				
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}	
	}
}
