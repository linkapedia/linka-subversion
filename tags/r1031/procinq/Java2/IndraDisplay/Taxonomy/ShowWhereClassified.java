package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class ShowWhereClassified {
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			String sFolder = (String) props.get ("Folder", true);	
			String sNext = (String) props.get ("Next", "1");	
			
			HashTree htArguments = new HashTree(props);
			htArguments.put("Folder", sFolder);

			InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentsClassified", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			} 
			
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Show Where Documents Classified");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.AddVariable("FOLDER", sFolder);
			Document.WriteTemplate(out, "header-admin.tpl");

			if (!htResults.containsKey("DOCUMENTS")) {
				out.println("<blockquote>No documents from this folder have been classified.</blockquote>");
				
			} else {
				int iNumResults = new Integer((String) htResults.get("NUMRESULTS")).intValue();
				int iNext = new Integer(sNext).intValue();
				
				Document.WriteTemplate(out, "where-head.tpl");

				HashTree htDocuments = (HashTree) htResults.get("DOCUMENTS");
				Enumeration eD = htDocuments.elements();

				while (eD.hasMoreElements()) {
					HashTree htDocument = (HashTree) eD.nextElement();
					Document.SetHash(htDocument);
				
					String sDocURL = (String) htDocument.get("DOCURL");
					String sEDocURL = URLEncoder.encode(sDocURL, "UTF-8");
					String sEDocTitle = URLEncoder.encode((String) htDocument.get("DOCTITLE"), "UTF-8");
					
					Document.AddVariable("EDOCURL", sEDocURL);
					Document.AddVariable("EDOCTITLE", sEDocTitle);
					Document.AddVariable("LOOP", iNext+"");
					
					if (sDocURL.length() > 60) { Document.AddVariable("PDOCURL", sDocURL.substring(0, 60)+"..."); }
					else { Document.AddVariable("PDOCURL", sDocURL); }

					// Now find out where this document stored and put that information
					// into a variable to be printed out in the template
					htArguments.put("DocumentID", (String) htDocument.get("DOCUMENTID"));
					InvokeAPI sAPI = new InvokeAPI ("tsdocument.TSGetDocumentStars", htArguments);
					HashTree htsResults = sAPI.Execute(false, false);
					int iThreeStars = 0; int iTwoStars = 0; int iOneStars = 0;
					
					if (htsResults.containsKey("NODEDOCUMENTS")) {
					
						HashTree htNodeDocuments = (HashTree) htsResults.get("NODEDOCUMENTS");
						Enumeration eND = htNodeDocuments.elements();
						
						while (eND.hasMoreElements()) {
							HashTree htNodeDocument = (HashTree) eND.nextElement();
							int sScore = new Integer((String) htNodeDocument.get("SCORE")).intValue();
							
							if (sScore == 50) { iOneStars++; }
							else if (sScore == 75) { iTwoStars++; }
							else if (sScore > 99) { iThreeStars++; }
						}
						
						Document.AddVariable("THREESTARS", ""+iThreeStars);
						Document.AddVariable("TWOSTARS", ""+iTwoStars);
						Document.AddVariable("ONESTARS", ""+iOneStars);
						
						Document.AddVariable("STARS", "Three stars: "+iThreeStars+
													  ", Two stars: "+iTwoStars+
													  ", One star: "+iOneStars);
					} else {
						Document.AddVariable("STARS", "This document did not classify into any topics.");	
					}
					Document.WriteTemplate(out, "where.tpl");
					iNext++;

				} 
				WritePrevNext(iNumResults, iNext, sFolder, out);
				
				Document.WriteTemplate(out, "where-foot.tpl");
			} 
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
	
	public static void WritePrevNext(int iNumResults, int iNext, String sFolder, PrintWriter out) {
		int iNextStart = iNext;
		int iPrevStart = iNext - 20;
		int iPrevEnd = iNext - 10;

		out.println("<table width=650 border=0><tr><td align=left>");
		
		out.println("&nbsp; &nbsp; &nbsp; ");
		
		if (iPrevStart < 1) { out.println("[Prev 10] "); }
		else {
			out.println("[<a href='/servlet/Main?template=Taxonomy.ShowWhereClassified&Folder="+
					sFolder+"&Next="+iPrevStart+"'>Prev 10</a>] ");
		}

		out.println("&nbsp; &nbsp; ");
	
		if (iNextStart >= iNumResults) { out.println("[Next 10] "); }
		else {
			out.println("[<a href='/servlet/Main?template=Taxonomy.ShowWhereClassified&Folder="+
					sFolder+"&Next="+iNextStart+"'>Next 10</a>] ");
		}
		
		out.println("</td></tr></table>");
	}
}
