package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;

public class Help
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sHelpText = (String) props.get ("helptext");	

		if (sHelpText == null) { sHelpText = "help-base"; }
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Taxonomy Server Help");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-admin.tpl");
			Document.WriteTemplate(out, "help/"+sHelpText+".tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
