package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;
import java.net.URLEncoder;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.indraweb.execution.Session;
import com.iw.system.Node;
import com.iw.system.*;

import HTML.*;
import Server.*;
import Logging.*;

public class DisplayResults
{
	public static String BuildStars(String sScore1) {
		String sStars = "<NOBR>";
		Float fScore1 = new Float(sScore1);
		
		String sImageLoc = Session.cfg.getProp("ImageLocation");
		
		if (fScore1.intValue() >= 50.0) { sStars = sStars + "<IMG height=14 src=\""+sImageLoc+"star.gif\" alt=\"1 Star (score is 50 or above)\" width=14>"; }
		if (fScore1.intValue() >= 75.0) { sStars = sStars + "<IMG height=14 src=\""+sImageLoc+"star.gif\" alt=\"2 Stars (score is 75 or above)\" width=14>"; }
		if (fScore1.intValue() >= 100.0) { sStars = sStars + "<IMG height=14 src=\""+sImageLoc+"star.gif\" alt=\"3 Stars (score is 75 or above)\" width=14>"; }
		
		sStars = sStars+"</NOBR>";
		return sStars;
	}

	// Build a GENRE_LIST variable, once again by inserting HTML from the java code
	// It's ugly but too difficult to templatize.   
	//
	// If the only genre available is PUBLIC, return an empty string (in other words, no genres)
	public static String BuildGenreList(HashTree htGenres, String sNodeID, String sFolderID) 
	throws Exception {
		Enumeration e = htGenres.elements();
		String sGenreHTML = new String("");
		
		int loop = 0;
		while (e.hasMoreElements()) {
			HashTree htGenre = (HashTree) e.nextElement();
			int iGenreID = new Integer((String) htGenre.get("GENREID")).intValue();
			int iGenreStatus = new Integer((String) htGenre.get("GENRESTATUS")).intValue();
			
			// Ignore Genre ID 1, check if genre is active
			if ((iGenreID != 1) && (iGenreStatus == 1)) {
				loop = loop + 1;
				String sName = (String) htGenre.get("GENRENAME");	
				String sGenrelink = "/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+sNodeID+"&GenreID="+iGenreID;
				
				// Looks different if in current genre
				if (!(sFolderID == null) && (new Integer(sFolderID).intValue() == iGenreID)) {
					sGenreHTML = sGenreHTML + 
								 "<td width=10%><font size=-1 face=arial color=white><b>"+
								 "<div class=genrewb>"+sName+"</div></b></font></td>"; 
				} else {
					sGenreHTML = sGenreHTML + 
								 "<td class=genrewb width=10%><font size=-1 face=arial><a class=genrewb href="+
								 sGenrelink+">"+sName+"</a></font></td>"; 
				}
			}
		}
		
		// If there were genres found, insert the public genre..
		if (loop > 0) {
			if (sFolderID == null) {
				sGenreHTML = "<td class=genrewb width=10%><font size=-1 face=arial>"+
							 "<div class=genrewb>Public</div></font></td>" + sGenreHTML; 
			} else {
				String sGenrelink = "/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+sNodeID;
				sGenreHTML = "<td class=genrewb width=10%><font size=-1 face=arial>"+
							 "<a class=genrewb href="+
							 sGenrelink+">Public</a></font></td>" + sGenreHTML; 
			}
		}
		
		return sGenreHTML;
	}
	
	// Construct the previous, next HTML.. one of the few cases where HTML will be
	// hardcoded into the JAVA template code
	//
	// Ex: 
	// [Prev]   1</a>  <a href=2.htm>2</a>  <a href=3.htm>3</a>  <a href=2.htm> [Next]</a>
	
	public static String BuildPrevNext(PrintWriter out, int iStart, int iEnd, int iResults, int iResultsPerPage, String sNodeID, String sGenreID) {
        out.println("<!-- nodeid: "+sNodeID+" -->");

		int iNextStart = iEnd+1;
		int iNextEnd = iEnd+iResultsPerPage;
		int iPrevStart = iStart-iResultsPerPage;
		int iPrevEnd = iStart - 1;
		String sHTML = "";

		if (sGenreID == null) { sGenreID = "0"; }
		
		// If going back would take us to some incomplete number in the set, reset
		if ((iPrevStart < 1) && (iPrevEnd > 0)) {
			iPrevStart = 1; iPrevEnd = iResultsPerPage;
		}
		
		// Build PREVIOUS tag
		if (iPrevStart > 0) { 
			sHTML = "[<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+sNodeID+
					"&GenreID="+sGenreID+"&Start="+iPrevStart+"&End="+iPrevEnd+"'>Prev</a>] &nbsp; ";
		} else { sHTML = "[Prev] &nbsp; "; }
	
		// If the next end point is greater than the number of results, set it equal to the results
		if (iNextEnd > iResults) { iNextEnd = iResults; }
		
		int loop = 1;
		while (loop < iResults) {
			int iLoopEnd = loop + iResultsPerPage -1;
			if (iLoopEnd > iResults) { iLoopEnd = iResults; }
			int iPageNumber = (loop / iResultsPerPage) + 1;
			sHTML = sHTML + "[<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+sNodeID+
							"&GenreID="+sGenreID+"&Start="+loop+"&End="+iLoopEnd+"'>"+iPageNumber+"</a>] &nbsp; ";
			loop = iLoopEnd + 1;
		}
		
		// Build NEXT tag
		if (iNextStart < iResults) { 
			sHTML = sHTML + "[<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+sNodeID+
							"&GenreID="+sGenreID+"&Start="+iNextStart+"&End="+iNextEnd+"'>Next</a>] ";
		} else { sHTML = sHTML + "[Next]"; }
		
		
		return sHTML;
	}
	
    public static String BuildNodeTree(PrintWriter out, String sNodeID, APIProps props) {
        ITS its = new ITS((String) props.get("SKEY"));
        Vector vNodes = new Vector();
        try { vNodes = its.getNodeTree(sNodeID); }
        catch (Exception e) { api.Log.LogError(e); }

		// If this node doesn't exist, return an empty string
		if (vNodes.size() < 1) { return ""; }

		String sTaxonomy = new String("");

        for (int i = 0; i < vNodes.size(); i++) {
			Node n = (Node) vNodes.elementAt(i);

			if (n.get("NODETITLE").length() > 25) {
				n.set("NODETITLE", n.get("NODETITLE").substring(0, 25)+"...");
			}

			// If this is the parent node, return now
			if (sTaxonomy.equals("")) {
				sTaxonomy = "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
					    n.get("NODEID")+"'> "+
					    n.get("NODETITLE")+"</a> ";
			} else {
				sTaxonomy = "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
					    n.get("NODEID")+"'> "+
					    n.get("NODETITLE")+"</a> > "+sTaxonomy;
			}

		}
		return sTaxonomy;
	}

	// Display an HTML document containing the results
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID");		// The nodeID to retrieve results for
		String sGenreID = (String) props.get ("GenreID");	// The genreID (optional)
		String sStart = (String) props.get("Start");		// Where do I start? (optional)
		String sCorpusID = (String) props.get("CorpusID");	// The corpusID (optional)
        String sProps = (String) props.get("Props");        // show taxonomy props only?
		String ID = (String) props.get("ID");
        String Sort = (String) props.get("sort", "0");
		String sEnd = "";

        String OrderBy = "SCORE1 DESC";
        if (Sort.equals("1")) OrderBy = "DOCTITLE ASC";
        else if (Sort.equals("2")) OrderBy = "GENREID ASC";
        else if (Sort.equals("3")) OrderBy = "DATELASTFOUND DESC";

		long lStart; long lStop;

		if ((sGenreID != null) && (sGenreID.equals("0"))) { sGenreID = null; }
		
		try {
			// If CorpusID was supplied, go to the root node of this corpus
			if ((sNodeID == null) && (sCorpusID != null)) {
				HashTree htArguments = new HashTree(props);
				htArguments.put("CorpusID", sCorpusID);
				InvokeAPI API = new InvokeAPI ("tscorpus.TSGetCorpusRoot", htArguments);
				HashTree htResults = API.Execute(false, false);
				
				if (!htResults.containsKey("NODES")) { sNodeID = null; } 
				else {
					HashTree htNodes = (HashTree) htResults.get("NODES");
					HashTree htNode = (HashTree) htNodes.get("NODE");

					sNodeID = (String) htNode.get("NODEID");
				}
			}
			
			// Start and end for rows
			int iStart = 0;	int iEnd = 0; String sScoreThreshold = "75";
			int iResultsPerPage = 9; String sStaticNodeID = sNodeID;

			if (sStart == null) { iStart = 1; } else { iStart = new Integer(sStart).intValue(); }

			// Read scoring threshold, results per page from cookie information
			Cookie Cookies[] = req.getCookies();
			
			if (req.getCookies() != null) {
				int cLen = Cookies.length;
			
				for (int i=0; i < cLen; i++) {
					if (Cookies[i].getName().equals("RESULTSPERPAGE")) { 
						sEnd = (String) Cookies[i].getValue();
						iResultsPerPage = new Integer(sEnd).intValue(); 
						iEnd = iStart + iResultsPerPage - 1;
						
					}
					if (Cookies[i].getName().equals("SCORETHRESHOLD")) { 
						sScoreThreshold = Cookies[i].getValue();
					}
				}
			}
			if (sEnd.equals("")) { iEnd = iStart + 9; }
			
			// Build API argument list and get the properties of this NODE
			HashTree htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			if (sGenreID != null) { htArguments.put("GenreID", sGenreID); }
			
			// Call: GetNodeProps to retrieve Node data
			InvokeAPI API = new InvokeAPI ("tsnode.TSGetNodeProps", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (!htResults.containsKey("NODE")) {
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}
			
			HashTree htNode = (HashTree) htResults.get("NODE");
            Node node = new Node (htNode);

			// If a NODE was not returned, the node does not exist, print out an error screen
			if (!htResults.containsKey("NODE")) { throw new Exception("That topic has expired."); }
			
			String sNodeName = node.get("NODETITLE");
			if (sNodeName.length() > 40) { 
				node.set("NODETITLE", sNodeName.substring(0,37)+"...");
			}

            HTMLDocument Document = new HTMLDocument();
            if (sProps != null) {
                Document.SetHash(node);
                Document.WriteHeader(out);

                Document.WriteTemplate(out, "taxonomy-properties.tpl");

                Document.WriteFooter(out);
                return;
            }

            // Determine the user's access level to this corpus (node)
			HashTree htArgPerm = new HashTree(props);
			htArgPerm.put("CorpusID", node.get("CORPUSID"));
			API = new InvokeAPI ("tscorpus.TSListCorpusAccess", htArgPerm);
			htResults = API.Execute(false, false);
				
			boolean bAdminAccess = true;
			
			if (htResults.containsKey("NOTAUTHORIZED")) { bAdminAccess = false; }
			
			boolean bNodeHasChildren = false; boolean bNodeHasResults = false;
			HashTree htNodeDocuments = null; HashTree htNodes = null;

            // this CQL is not actually run, but it is saved for ALERTS
            if (sScoreThreshold.endsWith(".0")) {
                sScoreThreshold = sScoreThreshold.substring(0,sScoreThreshold.length()-2);
            }

            String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = "+node.get("LINKNODEID")+" AND SCORE1 >= "+sScoreThreshold;
            if (sGenreID != null) {
                sCQL = sCQL + " AND GENREID = "+sGenreID;
                Document.AddVariable("GenreID", sGenreID);
            }

            Document.AddVariable("QUERY", sCQL);
            sCQL = sCQL + " ORDER BY "+OrderBy;
            Document.AddVariable("CQLU", URLEncoder.encode(sCQL, "UTF-8"));

            String orderOut = "";
            if (Sort.equals("0")) orderOut = orderOut+"<option SELECTED> Score\n";
            else orderOut = orderOut+"<option> Score\n";
            if (Sort.equals("1")) orderOut = orderOut+"<option SELECTED> Title\n";
            else orderOut = orderOut+"<option> Title\n";
            if (Sort.equals("2")) orderOut = orderOut+"<option SELECTED> Genre\n";
            else orderOut = orderOut+"<option> Genre\n";
            if (Sort.equals("3")) orderOut = orderOut+"<option SELECTED> Date\n";
            else orderOut = orderOut+"<option> Date\n";
            Document.AddVariable("SORDER", orderOut);

            out.println("<!-- CQL: "+sCQL+" -->");
			Document.SetHash(htNode);
			Document.AddVariable("TITLE", node.get("NODETITLE")+" Results");
			Document.AddVariable("CATEGORY_HIERARCHY", BuildNodeTree(out, sNodeID, props));
			Document.AddVariable("OPTION_CORPUS_LIST", req);

			// Call: ListFolders to retrieve Genre data
			htArguments.put("CorpusID", node.get("CORPUSID"));
			API = new InvokeAPI ("tsgenre.TSListFolders", htArguments);
			htResults = API.Execute(false, false);
			HashTree htGenres;
			
			if (htResults.containsKey("GENRES")) { htGenres = (HashTree) htResults.get("GENRES"); }
			else { htGenres = new HashTree(props); }
			
			try {
				Document.AddVariable("GENRE_LIST", (String) BuildGenreList(htGenres, sNodeID, sGenreID));
			} catch (Exception e) { Document.AddVariable("GENRE_LIST", ""); }
			Document.WriteHeader(out);

			// Call: GetNodeChildren to retrieve any children nodes and their corresponding links
            // Note: GetNodeChildren now uses CQL.  Results are returned sorted, no need to sort anymore.
            ITS its = new ITS((String) props.get("SKEY"));
            Vector vNodes = its.CQL("SELECT <NODE> WHERE PARENTID = "+sNodeID+" ORDER BY NODEINDEXWITHINPARENT");

			if (bAdminAccess) {	Document.WriteTemplate(out, "results-head.tpl"); }
			else { Document.WriteTemplate(out, "results-head-reader.tpl"); }

			// If this Node has children, print out the "has children" template
            if (vNodes.size() > 0) {
				Document.WriteTemplate(out, "blue-bar.tpl");
				Document.WriteTemplate(out, "child-head.tpl"); 

				for (int i = 0; i < (vNodes.size() / 2) + (vNodes.size() % 2); i++) {
					int iRight = i+(vNodes.size() / 2) + (vNodes.size() % 2);

					if (i != 0) { Document.WriteTemplate(out, "child-start.tpl"); }

                    Node n = (Node) vNodes.elementAt(i); Node n2 = null;
					if (iRight < vNodes.size()) {
						n2 = (Node) vNodes.elementAt(iRight);
					}

                    sNodeID = n.get("NODEID");
                    sCorpusID = n.get("CORPUSID");
                    String sParentID = n.get("PARENTID");
					String sNodeTitle = n.get("NODETITLE");

                    if (sNodeTitle.length() > 35) {
						sNodeTitle = sNodeTitle.substring(0, 35)+"...";
					}

					Document.AddVariable("CNODEID", sNodeID);
					Document.AddVariable("CNODETITLE", sNodeTitle);

                    if ((n.isLink()) && (((String) n.get("CORPUSID")).equals("101"))) {
                        Document.AddVariable("CNODEID", n.get("LINKNODEID"));
                        Document.WriteTemplate(out, "child-tax.tpl");
                    } else { Document.WriteTemplate(out, "child.tpl"); }

					if (n2 != null) {
                        sNodeID = n2.get("NODEID");
                        sCorpusID = n2.get("CORPUSID");
                        sParentID = n2.get("PARENTID");
                        sNodeTitle = n2.get("NODETITLE");

						if (sNodeTitle.length() > 35) {
							sNodeTitle = sNodeTitle.substring(0, 35)+"...";
						}

						Document.WriteTemplate(out, "child-middle.tpl");
						Document.AddVariable("CNODEID", sNodeID);
						Document.AddVariable("CNODETITLE", sNodeTitle);
                        if ((n2.isLink()) && (((String) n2.get("CORPUSID")).equals("101"))) {
                            Document.AddVariable("CNODEID", n2.get("LINKNODEID"));
                            Document.WriteTemplate(out, "child-tax.tpl");
                        } else { Document.WriteTemplate(out, "child.tpl"); }
					}
					Document.WriteTemplate(out, "child-end.tpl");
				}
				Document.WriteTemplate(out, "child-foot.tpl");
				bNodeHasChildren = true;
			}  else { 
				bNodeHasChildren = false; 
			}
			int iResults = 0;
				
			// hack (would like this done better) 
			// if genre id 12, this is PEOPLE SEARCH
			if ((sGenreID != null) && (sGenreID.equals("12"))) {
				// Call: GetExpertUser 
				htArguments.put("NodeID", sNodeID);
				API = new InvokeAPI ("people.TSGetExpertUser", htArguments);
				htResults = API.Execute(false, false);
				
				Document.WriteTemplate(out, "peopletab-result-head.tpl");
				if (htResults.containsKey("NARRATIVES")) {
					HashTree htSubs = (HashTree) htResults.get("NARRATIVES");

					Enumeration eSubs = htSubs.elements();
					
					while (eSubs.hasMoreElements()) {
						iResults++;
						HashTree htUser = (HashTree) eSubs.nextElement();
					}

					Vector vUsers = SortNIWP(htSubs);
					for (int i = 0; i < vUsers.size(); i++) {
						HashTree htUser = (HashTree) vUsers.elementAt(i);
						Document.SetHash(htUser);
                        int level = Integer.parseInt((String) htUser.get("SCORE1"));
                        if (level > 0) level = level / 10;
                        Document.AddVariable("LEVEL", level+"");

						Document.WriteTemplate(out, "peopletab-result.tpl");
					}

			} else { out.println("<tr><td><font face='Arial'>&nbsp; <br>No experts found for this topic.</font></td></tr>"); }

				Document.WriteTemplate(out, "peopletab-result-foot.tpl");
				Document.WriteTemplate(out, "results-foot.tpl");
				Document.WriteFooter(out);

				return;
			} else {
				if (bNodeHasChildren == true) {	Document.WriteTemplate(out, "blue-bar.tpl"); }

				// Call: GetNodeDocument and stream back XML results
                if (sGenreID != null) { htArguments.put("FolderID", sGenreID); }
				htArguments.put("Start", ""+iStart); htArguments.put("End", ""+iEnd);
				htArguments.put("ScoreThreshold", sScoreThreshold);
                htArguments.put("Order", OrderBy);
				API = new InvokeAPI ("tsdocument.TSGetNodeDocument", htArguments);
				iResults = API.StreamResults(out, Document, iStart, iEnd, (String) props.get("SKEY"), bAdminAccess);
			}

			if (iResults != 0) {
				Document.AddVariable("PREVNEXT", Taxonomy.DisplayResults.BuildPrevNext(out, iStart, iEnd, iResults, iResultsPerPage, sStaticNodeID, sGenreID));
				Document.WriteTemplate(out, "results-foot.tpl");
				if (iEnd > iResults) { iEnd = iResults; }

				Document.AddVariable("START", ""+iStart);
				Document.AddVariable("END", ""+iEnd);
				Document.AddVariable("RESULTS", ""+iResults);
				Document.AddVariable("NODETITLE", node.get("NODETITLE"));
				Document.AddVariable("USERID", ID);
				Document.AddVariable("NODEID", sNodeID);
				Document.WriteTemplate(out, "more-results.tpl");

			} else {
				if (sGenreID != null) { Document.WriteTemplate(out, "genre-javascript.tpl"); }
				if (sGenreID != null) {
					out.println("<blockquote>There are no document results in your selected folder.</blockquote>");
				} else if (bNodeHasChildren == false) {
					Document.WriteTemplate(out, "genre-javascript.tpl");
					out.println("<BR><blockquote>No document results found for this topic.</blockquote>");
				}
			}

			Document.WriteFooter(out);
		}
		catch (InvalidArguments e) {
			Log.LogError(e, out);
			HTMLDocument Document = new HTMLDocument();
			Document.WriteError(out, "<BLOCKQUOTE><BR>Sorry, that corpus does not contain any topics.</BLOCKQUOTE>");
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}

	}

    // these functions are no longer used by DisplayResults, but may still be invoked by other routines
    private static void qsNIWP(Vector v) {

		Vector vl = new Vector();                      // Left and right sides
		Vector vr = new Vector();
		HashTree el;
		int key;                                    // key for splitting

		if (v.size() < 2) return;                      // 0 or 1= sorted

		HashTree htResult = (HashTree) v.elementAt(0);
		key = new Integer((String) htResult.get("SCORE1")).intValue();

	    // Start at element 1
	    for (int i=1; i < v.size(); i++) {
	       el = (HashTree) v.elementAt(i);
		   if (new Integer((String) el.get("SCORE1")).intValue() > key)
			   vr.addElement(el); // Add to right
	       else vl.addElement(el);                     // Else add to left
	    }

	    qsNIWP(vl);                                        // Recursive call left
		qsNIWP(vr);                                        //    "        "  right
		vl.addElement(v.elementAt(0));

		addVect(v, vl, vr);
	}

	// Add two vectors together, into a destination Vector
	private static void addVect( Vector dest, Vector left, Vector right ) {

		int i;

		dest.removeAllElements();                     // reset destination

		for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
		for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));

	}

	public static Vector SortNIWP(HashTree htHash) {
		Vector vNodeDocuments = new Vector();
		Enumeration e = htHash.elements();

		// Fill the vector with all the results
		while (e.hasMoreElements()) {
			HashTree htResult = (HashTree) e.nextElement();
			vNodeDocuments.addElement(htResult);
		}

		// Sort this vector
		qsNIWP(vNodeDocuments);

		return vNodeDocuments;
	}


}
