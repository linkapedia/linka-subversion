package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class EditResult
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID");	
		String sGenreID = (String) props.get ("GenreID");
        String sDocumentID = (String) props.get ("DocumentID");
		String sDocTitle = (String) props.get ("DocTitle");
		String sDocURL = (String) props.get ("DocURL");
		String sScore1 = (String) props.get ("Score1");
		String sDocSum = (String) props.get ("DocSummary");
        String sReload = (String) props.get ("Reload", "1");

        if (sDocumentID != null && sDocumentID.equals("")) sDocumentID = null;

		try {
			// Check for required arguments
			if (sNodeID == null) { throw new Exception("Illegal request - topic identifier not found in argument list."); }
			
			if (props.containsKey("submit")) {
				if ((sDocTitle == null) || (sDocURL == null) || (sScore1 == null) || 
					(sDocSum == null) || (sDocURL.equals("")) || (sDocTitle.equals(""))) {
					throw new Exception("One or more of your document fields were left blank!  Please use your <b>back</b> button and try again.");
				}
				
				if ((sGenreID == null) || (sGenreID.equals(""))) { sGenreID = "0"; }

                // if DOCURL exists but not DOCUMENTID, insert the document and get the document ID
                if (sDocumentID == null) {
                    HashTree htArguments = new HashTree(props);
                    htArguments.put("genreid", sGenreID);
                    htArguments.put("doctitle", sDocTitle);
                    htArguments.put("docurl", sDocURL);
                    htArguments.put("documentsummary", sDocSum);
                    InvokeAPI API = new InvokeAPI ("tsdocument.TSAddDocument", htArguments);
                    HashTree htResults = API.Execute(false, false);

                    if (!htResults.containsKey("DOCUMENT"))
                        throw new Exception("Could not add this document to the system.  Please use your <b>back</b> button and try again.");

                    HashTree htDocument = (HashTree) htResults.get("DOCUMENT");
                    sDocumentID = (String) htDocument.get("DOCUMENTID");
                } else {
                    HashTree htArguments = new HashTree(props);
                    htArguments.put("genreid", sGenreID);
                    htArguments.put("doctitle", sDocTitle);
                    htArguments.put("documentid", sDocumentID);
                    htArguments.put("documentsummary", sDocSum);
                    InvokeAPI API = new InvokeAPI ("tsdocument.TSEditDocument", htArguments);
                    HashTree htResults = API.Execute(false, false);

                    if (!htResults.containsKey("SUCCESS"))
                        throw new Exception("Could not modify this document.  Please use your <b>back</b> button and try again.");
                }

                // now that the document is updated, add the node relationship (if applicable)
                HashTree htArguments = new HashTree(props);
                htArguments.put("nodeid", sNodeID);
                htArguments.put("documentid", sDocumentID);
                htArguments.put("permanent", "true");
                InvokeAPI API = new InvokeAPI ("tsnode.TSRemoveNodeDocument", htArguments);
                API.Execute(false, false);

                htArguments = new HashTree(props);
                htArguments.put("nodeid", sNodeID);
                htArguments.put("documentid", sDocumentID);
                htArguments.put("score1", sScore1);
                htArguments.put("docsummary", sDocSum);
                API = new InvokeAPI ("tsdocument.TSAddNodeDocument", htArguments);
                HashTree htResults = API.Execute(false, false);

                if (!htResults.containsKey("SUCCESS"))
                    throw new Exception("Could not add this document to this topic.  Please use your <b>back</b> button and try again.");

                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Success");
                Document.AddVariable("NodeID", sNodeID);
                Document.AddVariable("GenreID", sGenreID);
                Document.AddVariable("Reload", sReload);

                Document.WriteTemplate(out, "edit-results-success.tpl");
			} else {

				// Populate default fields
				if ((sGenreID == null) || (sGenreID.equals(""))) { sGenreID = "0"; }
				if ((sDocTitle == null) || (sDocTitle.equals(""))) { sDocTitle = "Untitled Document"; }
				if (sDocURL == null) { sDocURL = ""; }
				if (sScore1 == null) { sScore1 = "125.0"; }
				if (sDocSum == null) { sDocSum = ""; }
				
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Alter Document Results");
				Document.AddVariable("NodeID", sNodeID);
				Document.AddVariable("DocTitle", com.indraweb.util.UtilStrings.replaceStrInStr(sDocTitle, "\"", ""));
				Document.AddVariable("DocURL", sDocURL);
				Document.AddVariable("GenreID", sGenreID);
				Document.AddVariable("Score1", sScore1);
				Document.AddVariable("DocSummary", sDocSum);
                Document.AddVariable("Reload", sReload);
                if (sDocumentID != null) Document.AddVariable("DocumentID", sDocumentID);

				// Create score option pulldown
				String sOptions = new String("");
			
				if (sScore1.equals("125.0")) { sOptions = sOptions + "<option value='125.0' selected > Editor reviewed \n"; }
				else { sOptions = sOptions + "<option value='125.0'> Editor reviewed \n"; }
				if (sScore1.equals("100.0")) { sOptions = sOptions + "<option value='100.0' selected > Three stars \n"; }
				else { sOptions = sOptions + "<option value='100.0'> Three stars \n"; }
				if (sScore1.equals("75.0")) { sOptions = sOptions + "<option value='75.0' selected > Two stars \n"; }
				else { sOptions = sOptions + "<option value='75.0'> Two stars \n"; }
				if (sScore1.equals("70.0")) { sOptions = sOptions + "<option value='50.0' selected > One star \n"; }
				else { sOptions = sOptions + "<option value='50.0'> One star \n"; }
			
				Document.AddVariable("Options", sOptions);
				Document.WriteTemplate(out, "edit-result.tpl");
			}
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.AddVariable("ErrorMsg", "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Document.WriteTemplate(out, "result-error.tpl");
			Log.LogError(e, out);
		}
	
	}
}
