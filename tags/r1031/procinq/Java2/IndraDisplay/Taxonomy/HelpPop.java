package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;

public class HelpPop
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sHelpText = (String) props.get ("helptext");	

		if (sHelpText == null) { sHelpText = "help-base"; }
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Taxonomy Server Help");
			Document.WriteTemplate(out, "help/head.tpl");
			Document.WriteTemplate(out, "help/"+sHelpText+".tpl");
			Document.WriteTemplate(out, "help/foot.tpl");
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
