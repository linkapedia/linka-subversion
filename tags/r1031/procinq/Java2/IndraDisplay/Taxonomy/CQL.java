package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.Node;
import com.iw.system.*;

public class CQL
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Create Custom CQL Query");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-nosearch.tpl");

			if (props.get("query") == null) {
				Document.WriteTemplate(out, "CQL/display.tpl");
			} else {
				String query = (String) props.get("query", true);
				String start = (String) props.get("start", "1");
                String Sort = (String) props.get("sort", "-1");

                // sorting ************************
                String OrderBy = "";

                if (Sort.equals("0") && query.toLowerCase().indexOf("<nodedocument>") > 0) OrderBy = "SCORE1 DESC";
                else if (Sort.equals("1")) OrderBy = "DOCTITLE ASC";
                else if (Sort.equals("2")) OrderBy = "GENREID ASC";
                else if (Sort.equals("3")) OrderBy = "DATELASTFOUND DESC";

                if ((!OrderBy.equals("")) && (query.toLowerCase().indexOf("order by") > 0)) {
                    query = query.substring(0, query.toLowerCase().indexOf("order by"));
                    query = query + " ORDER BY "+OrderBy;
                } else if (!OrderBy.equals("")) { query = query + " ORDER BY "+OrderBy; }

                String orderOut = "";
                if (Sort.equals("0"))
                    orderOut = orderOut + "<option SELECTED> Score\n";
                else
                    orderOut = orderOut + "<option> Score\n";
                if (Sort.equals("1"))
                    orderOut = orderOut + "<option SELECTED> Title\n";
                else
                    orderOut = orderOut + "<option> Title\n";
                if (Sort.equals("2"))
                    orderOut = orderOut + "<option SELECTED> Genre\n";
                else
                    orderOut = orderOut + "<option> Genre\n";
                if (Sort.equals("3"))
                    orderOut = orderOut + "<option SELECTED> Date\n";
                else
                    orderOut = orderOut + "<option> Date\n";
                Document.AddVariable("SORDER", orderOut);

                if (query.toLowerCase().indexOf("<node>") > 0) { Document.AddVariable("SORDER", ""); }

                // end sorting **************

				int iStart = new Integer(start).intValue();
				
				// get RESULTSPERPAGE from COOKIE
				int iResultsPerPage = 20;
				Cookie Cookies[] = req.getCookies();
			
				if (req.getCookies() != null) {
					int cLen = Cookies.length;
					for (int i=0; i < cLen; i++) {
						if (Cookies[i].getName().equals("RESULTSPERPAGE")) { 
							String sEnd = (String) Cookies[i].getValue();
							iResultsPerPage = new Integer(sEnd).intValue(); 
						}
					}
				}

                String escaped = new String(com.indraweb.util.UtilStrings.replaceStrInStr(query, "'", "''"));
                escaped = new String(com.indraweb.util.UtilStrings.replaceStrInStr(escaped, "'", "\\'"));

				Document.AddVariable("QUERY", query);
                Document.AddVariable("ESCAPED", escaped);
                Document.AddVariable("CQLU", URLEncoder.encode(query, "UTF-8"));

				Document.WriteTemplate(out, "CQL/head.tpl");
				Vector vResults = new Vector();

                try { vResults = CQLquery(props, query, ""+iResultsPerPage, start, out); }
                catch (Exception e) {
                    out.println("<blockquote>There is a syntax error in your CQL.  "+
                                "Please validate your query and try again.</blockquote>");
                    Document.WriteFooter(out);
                    return;
                }

                if (vResults.size() == 0) {
                    out.println("<blockquote>No results were found to match your request.</blockquote>");
                    Document.WriteFooter(out);
                    return;
                }

                if ((query.toLowerCase().indexOf("<document>") > 0) ||
                    (query.toLowerCase().indexOf("<narrative>") > 0)) {
					int iNumRes = java.lang.Math.min(vResults.size(), (iStart+iResultsPerPage-1));
					Document.AddVariable("NUMRES", iNumRes+"");
					Document.AddVariable("MAXRES", ""+vResults.size());

					Document.AddVariable("START", ""+iStart);
					Document.WriteTemplate(out, "CQL/advanced-head.tpl");
					
					int loop = iStart-1;
                    for (int i = 0; i < vResults.size(); i++) {
						loop++; Document.AddVariable("LOOP", loop+"");
						Document d = (Document) vResults.elementAt(i);

						String sEDocTitle = d.getEncodedField("DOCTITLE");
						String sEDocURL = d.getEncodedField("DOCURL");
							
						Document.AddVariable("EDOCURL", sEDocURL);
						Document.AddVariable("EDOCTITLE", sEDocTitle);

                        Document.SetHash(d);

    					Document.WriteTemplate(out, "CQL/fulltext-result.tpl");
					}
					if (vResults.size() > iResultsPerPage) {
						String sNextPrev = BuildNextPrev(iStart, iResultsPerPage, vResults.size(), URLEncoder.encode(query, "UTF-8"));
						Document.AddVariable("NEXTPREV", sNextPrev);
					}
					Document.WriteTemplate(out, "CQL/fulltext-foot.tpl");
				} else if (query.toLowerCase().indexOf("<node>") > 0) {
					int iNumRes = vResults.size();
					Document.AddVariable("NUMRES", iNumRes+"");
					Document.AddVariable("MAXRES", ""+vResults.size());
					Document.WriteTemplate(out, "CQL/category-head.tpl");
					Document.WriteTemplate(out, "CQL/category-start.tpl");
					
					int loop = 0;
                    for (int i = 0; i < vResults.size(); i++) {
						loop++;
                        Node n = (Node) vResults.elementAt(i);

						String sNodeTree
							= BuildNodeTree
							(out, n.get("NODEID"), props);

						Document.AddVariable("NODETREE", sNodeTree);
						if (n.get("NODETITLE").length() > 25)
                            n.set("NODETITLE", n.get("NODETITLE").substring(0, 25)+" ...");

                        Document.SetHash(n);

                        Document.WriteTemplate(out, "CQL/advanced-category-result.tpl");
					}
					Document.WriteTemplate(out, "CQL/category-foot.tpl");
                } else if (query.toLowerCase().indexOf("<nodedocument>") > 0) {
					int iNumRes = java.lang.Math.min(vResults.size(), (iStart+iResultsPerPage-1));
					Document.AddVariable("NUMRES", iNumRes+"");
					Document.AddVariable("MAXRES", ""+vResults.size());

					Document.AddVariable("START", ""+iStart);
					Document.WriteTemplate(out, "CQL/advanced-head.tpl");

                    int loop = iStart-1;
                    for (int i = 0; i < vResults.size(); i++) {
						loop++; Document.AddVariable("LOOPI", loop+"");
						NodeDocument nd = (NodeDocument) vResults.elementAt(i);

                        String sEDocTitle = nd.getEncodedField("DOCTITLE");
						String sEDocURL = nd.getEncodedField("DOCURL");
                        String sEDocSum = nd.getEncodedField("DOCSUMMARY");

                        Document.AddVariable("EDOCURL", sEDocURL);
                        Document.AddVariable("EDOCTITLE", sEDocTitle);
                        Document.AddVariable("EDOCSUM", sEDocSum);

                        Document.SetHash(nd);

                        if (nd.get("DOCURL").length() > 40) { Document.AddVariable("PDOCURL", nd.get("DOCURL").substring(0, 40) + "...");
                        } else { Document.AddVariable("PDOCURL", nd.get("DOCURL")); }

                        if (!nd.get("DOCSUMMARY").toUpperCase().equals("NULL"))
                            Document.AddVariable("DOCSUMMARY", nd.get("DOCUMENTSUMMARY"));

                        Document.WriteTemplate(out, "result-reader.tpl");
                    }
                    Document.WriteTemplate(out, "results-foot.tpl");

                } else {
					out.println(" <blockquote>Invalid query.  CQL query must begin with either "+
								" select <document> or select <node>, please consult your "+
								" ITS User Manual for more information.</blockquote><p>");
				}
			}
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}

	}

    public static Vector CQLquery (APIProps props, String query,
									 String results, String start, PrintWriter out)
		throws Exception {

        ITS its = new ITS((String) props.get("SKEY"));
        try {
            Vector vResults = its.CQL(query, Integer.parseInt(start), Integer.parseInt(results));
            return vResults;
        } catch (Exception e) { throw e; }
	}

	public static String BuildNextPrev (int Start, int Results, int Max, String CQL) {
		String sNP = new String("<table width=100% cellpadding=2 "+
								"cellspacing=0 border=0 align=center><tr><td align=center>");
		boolean bNext = false;
		boolean bPrev = false;

		String sURL = "/servlet/Main?template=Taxonomy.CQL&query="+CQL;

		if ((Start - Results) > 0) { bPrev = true; }
		if (((Start + Results - 1) + Results) <= Max) { bNext = true; }

		if (bPrev) { sNP = sNP+"[<a href='"+sURL+"&start="+(Start-Results)+"'>Prev</a>] "; }
		else { sNP = sNP+"[Prev] "; }

		// ----- figure out what's in-between ..
		int loop = 0;
		for (int i = 1; i <= Max; i = i + Results) {
			loop++;
			if (i != Start) {
				sNP = sNP + "[ <a href='"+sURL+"&start="+i+"'>"+loop+"</a>] ";
			} else { sNP = sNP + " ["+loop+"] "; }
		}

		if (bNext) { sNP = sNP+"[<a href='"+sURL+"&start="+(Start+Results)+"'>Next</a>] "; }
		else { sNP = sNP+"[Next] "; }

		sNP = sNP + "</td></tr></table>";
		return sNP;
	}

    public static String BuildNodeTree(PrintWriter out, String sNodeID, APIProps props) {
        ITS its = new ITS((String) props.get("SKEY"));
        Vector vNodes = new Vector();
        try { vNodes = its.getNodeTree(sNodeID); }
        catch (Exception e) { api.Log.LogError(e); }

		// If this node doesn't exist, return an empty string
		if (vNodes.size() < 1) { return ""; }

		String sTaxonomy = new String("");

        for (int i = 0; i < vNodes.size(); i++) {
			Node n = (Node) vNodes.elementAt(i);

			if (n.get("NODETITLE").length() > 25) {
				n.set("NODETITLE", n.get("NODETITLE").substring(0, 25)+"...");
			}

			// If this is the parent node, return now
			if (sTaxonomy.equals("")) {
				sTaxonomy = "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
					    n.get("NODEID")+"'> "+
					    n.get("NODETITLE")+"</a> ";
			} else {
				sTaxonomy = "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
					    n.get("NODEID")+"'> "+
					    n.get("NODETITLE")+"</a> > "+sTaxonomy;
			}

		}
		return sTaxonomy;
	}
}
