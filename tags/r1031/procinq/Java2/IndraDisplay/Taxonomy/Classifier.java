package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class Classifier
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Manage Classifiers");
			Document.WriteTemplate(out, "header-nosearch.tpl");
			Document.WriteTemplate(out, "classifier/manage-head.tpl");

			// Call: GetClassifierList
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tsclassify.TSGetClassifierList", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (htResults.containsKey("SESSIONEXPIRED")) {
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}

			if (!htResults.containsKey("CLASSIFIERS")) {
				out.println("&nbsp; There are no classifiers on this server.");
			} else {
				HashTree htR = (HashTree) htResults.get("CLASSIFIERS");
				Enumeration eR = htR.elements();

				Document.WriteTemplate(out, "classifier/manage-list-head.tpl");
				while (eR.hasMoreElements()) {
					HashTree htClassifier = (HashTree) eR.nextElement();
					Document.SetHash(htClassifier);
					Document.WriteTemplate(out, "classifier/manage-element.tpl");
				}
				Document.WriteTemplate(out, "classifier/manage-list-foot.tpl");
			}

			Document.WriteTemplate(out, "classifier/manage-foot.tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}

	}
}
