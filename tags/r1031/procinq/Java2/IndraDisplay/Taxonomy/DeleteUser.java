package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class DeleteUser
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sUserID = (String) props.get ("UserID");	
		try {
			if (sUserID == null) { throw new HTML.InvalidArguments(2, "No keywords given"); }

			// Call: GetCorpusProps
			HashTree htArguments = new HashTree(props);
			htArguments.put("UserID", sUserID);
			InvokeAPI API = new InvokeAPI ("tsuser.TSRemoveUser", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}
				
			// A success is indicated if there is no TSERROR tag in the return
				if (htResults.containsKey("TS_ERROR")) {
					throw new Exception("Sorry, that user cannot be deleted at this time.");
			}

			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "The specified user has been deleted successfully.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.AdministerUsers'>"+
							  "Click here to return to the user menu</a>.";

			Document.WriteSuccess(out, sSuccess);
				
		}
		catch (InvalidArguments e) {
			HTMLDocument Document = new HTMLDocument();
			Document.WriteError(out, "Sorry, there are invalid or missing arguments in your request.");
			Log.LogError(e, out); 
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}	
	}
}
