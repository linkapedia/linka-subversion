package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Logging.*;
import com.iw.system.*;

import com.indraweb.execution.Session;
import Server.ITS;

public class SimilarDocuments
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
        String sNodeList = (String) props.get ("nodelist");
        String sDocumentID = (String) props.get ("documentid");
        String sDocText = (String) props.get ("doctext");
        String sGenre = (String) props.get ("genreid");
        String sScoreThreshold = (String) props.get ("scorethreshold", "75.0");
        HTMLDocument Document = new HTMLDocument();

        if (sDocumentID != null) {
            try {
                ITS its = new ITS((String) props.get("SKEY"));
                Vector vDocuments = its.getSimilarDocuments(sDocumentID, sGenre, null);

                String numResults = vDocuments.size()+"";
                if (numResults.equals("0")) {
                    Document.WriteSuccess(out, "No document results were found matching that node set.");
                } else {
                    Document.AddVariable("NUMRESULTS", numResults);

                    Document.WriteTemplate(out, "header-admin.tpl");
                    Document.WriteTemplate(out, "similar/head.tpl");

                    int loop = 0;
                    for (int i = 0; i < vDocuments.size(); i++) {
                        Document d = (Document) vDocuments.elementAt(i);
                        loop++;

                        Document.SetHash(d);
                        Document.AddVariable("EDOCTITLE", d.getEncodedField("DOCTITLE"));
                        Document.AddVariable("EDOCURL", d.getEncodedField("DOCURL"));
                        Document.AddVariable("LOOP", loop+"");

                        Document.WriteTemplate(out, "search/signature-result.tpl");
                    }
                    Document.WriteTemplate(out, "search/fulltext-foot.tpl");
                    Document.WriteFooter(out);
                 }
             } catch (Exception e) {
                 String sErrorMsg = e.getMessage();
                 Document.WriteError(out, "<BLOCKQUOTE>Sorry, your request could not be processed at this time.</BLOCKQUOTE>");
                 Log.LogError(e, out);
             }
        } else if ((sNodeList == null) && (sDocText == null)) {
			Document.AddVariable("Title", "Get Similar Documents");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-admin.tpl");
            Document.WriteTemplate(out, "similar/classify.tpl");
        } else if (sNodeList == null) {
            // Call: ListFolders
            HashTree htArguments = new HashTree(props);
            InvokeAPI API = new InvokeAPI ("tsgenre.TSListFolders", htArguments);
            HashTree htResults = API.Execute(false, false);

            String sGenreSelectList = "<option value=0> All Folders ";

            if (htResults.containsKey("GENRES")) {
                HashTree htGenres = (HashTree) htResults.get("GENRES");
                Enumeration eG = htGenres.elements();

                while (eG.hasMoreElements()) {
                    HashTree htGenre = (HashTree) eG.nextElement();
                    String sGenreID = (String) htGenre.get("GENREID");
                    String sGenreName = (String) htGenre.get("GENRENAME");
                    String sGenreStatus = (String) htGenre.get("GENRESTATUS");

                    if (sGenreStatus.equals("1")) {
                        sGenreSelectList = sGenreSelectList +
                            "<option value="+sGenreID+"> "+sGenreName;
                    }
                }
            }

            Document.AddVariable("GENRESELECTLIST", sGenreSelectList);

            htArguments = new HashTree(props);
            htArguments.put("post", "false");
            API = new InvokeAPI("tsclassify.TSClassifyDoc", htArguments);

            // write whatever is in that text box into the temporary file
            File f = Server.Classify.getTempFile(Session.sIndraHome+"/classifyfiles/");
                if (sDocText.toLowerCase().indexOf("<HTML>") == -1) { sDocText = Server.Classify.addHTMLHeaders(sDocText); }
                PrintWriter fout = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), true));
                fout.println(sDocText); fout.close();

                htResults = API.PostExecute(f, false);

                if (!htResults.containsKey("CLASSIFICATIONRESULTSET")) {
                    api.Log.Log( "ClassificationResultSet tag does not exist." );
                    throw new Exception ( "Sorry, this document did not classify into any topics in the system." ) ;
                }

                f.delete();

                Document.AddVariable("Title", "Classification Results");
                Document.WriteTemplate(out, "header-admin.tpl");

                HashTree htClass = (HashTree) htResults.get("CLASSIFICATIONRESULTSET");
                Vector vNodes = Server.Classify.Sort(htClass);
                Enumeration eT = vNodes.elements();

                // Loop through each result and print it
                int loop = 0;
                while (eT.hasMoreElements()) {
                    HashTree htNodeBucket = (HashTree) eT.nextElement();
                    float fScore = new Float((String) htNodeBucket.get("SCORE1")).floatValue();

                    // Ensure threshold match
                    if (fScore > new Float(49.9).floatValue()) {
                        loop = loop + 1;
                        if (loop == 1) {
                            Document.WriteTemplate(out, "similar/classify-start.tpl");
                        }
                        String sNodeTree
                                = Taxonomy.Search.BuildNodeTree(out, (String) htNodeBucket.get("NODEID"), props);
                        sNodeTree = loop+". <a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID=" +
                                (String) htNodeBucket.get("NODEID") + "'><i><b>" +
                                (String) htNodeBucket.get("NODETITLE") + "</b></i></a> &nbsp; " +
                                "<BR>" + sNodeTree + "<P>";
                        Document.SetHash(htNodeBucket);
                        Document.AddVariable("NODETREE", sNodeTree);
                        Document.WriteTemplate(out, "similar/search.tpl");
                    }
                }
                if (loop != 0) {
                    Document.WriteTemplate(out, "similar/search-end.tpl");
                } else {
                    out.println("<blockquote>Sorry, this document did not classify into any topics in the system.<br></blockquote>");
                }
                Document.WriteFooter(out);
        } else {
            try {
                // loop through the input arguments - any that start with CLASSIFY are NODE ID's
                String sNodeIDs = "0";
                Enumeration eR = req.getParameterNames();
                while (eR.hasMoreElements()) {
                    String s = (String) eR.nextElement();
                    if (s.startsWith("CLASSIFY")) {
                        sNodeIDs = sNodeIDs+","+s.substring(8, s.length());
                    }
                }

                ITS its = new ITS((String) props.get("SKEY"));
                Vector vDocuments = its.getDocumentsByNodes(sNodeIDs, sGenre);

                String numResults = vDocuments.size()+"";
                if (numResults.equals("0")) {
                    Document.WriteSuccess(out, "No document results were found matching that node set.");
                } else {
                    Document.AddVariable("NUMRESULTS", numResults);

                    Document.WriteTemplate(out, "header-admin.tpl");
                    Document.WriteTemplate(out, "similar/head.tpl");

                    int loop = 0;
                    for (int i = 0; i < vDocuments.size(); i++) {
                        loop++;
                        Document d = (Document) vDocuments.elementAt(i);
                        Document.SetHash(d);
                        Document.AddVariable("EDOCTITLE", d.getEncodedField("DOCTITLE"));
                        Document.AddVariable("EDOCURL", d.getEncodedField("DOCURL"));

                        Document.AddVariable("LOOP", loop+"");

                        Document.WriteTemplate(out, "search/signature-result.tpl");
                    }
                    Document.WriteTemplate(out, "search/fulltext-foot.tpl");
                    Document.WriteFooter(out);
                }
            } catch (Exception e) {
                String sErrorMsg = e.getMessage();
                Document.WriteError(out, "<BLOCKQUOTE>Sorry, your request could not be processed at this time.</BLOCKQUOTE>");
                Log.LogError(e, out);
            }
        }
	}
}
