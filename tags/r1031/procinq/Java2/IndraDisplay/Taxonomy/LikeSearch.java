package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class LikeSearch
{
	// Display search results
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sKeywords = (String) props.get ("Keywords"); // search keywords
		String sNodeID = (String) props.get ("NodeID");		// The nodeID to retrieve results (optional)
		String sCorpusID = (String) props.get ("CorpusID");	// The corpusID (optional)
		String sOccur = (String) props.get ("OccurID");		// The occur threshold (optional)
		
		try {
			if (sKeywords == null) { throw new HTML.InvalidArguments(2, "No keywords given"); }
			if (sOccur == null) { sOccur = "4"; }

			// Build API argument list
			HashTree htArguments = new HashTree(props);
			htArguments.put("Keywords", sKeywords);
			if (sNodeID != null) { htArguments.put("NodeID", sNodeID); }
			if (sOccur != null) { htArguments.put("Occur", sOccur); }
			if ((sCorpusID != null) && (new Integer(sCorpusID).intValue() != 0)) { htArguments.put("CorpusID", sCorpusID); }
			
			// Call: NodeTitleSearch
			InvokeAPI API = new InvokeAPI ("tsnode.TSNodeTitleSigSearch", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (htResults.containsKey("SESSIONEXPIRED")) {
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}

			HTMLDocument Document = new HTMLDocument();
			
			// First print out the direct node searches
			if (!htResults.containsKey("NODES")) {
				throw new Exception ("<blockquote><i>&nbsp; <BR>No topics were found to match your search query: "+sKeywords+"</P></blockquote></i>");
			} else {
				HashTree htNodes = (HashTree) htResults.get("NODES");
				Enumeration e = htNodes.elements();
				Document.WriteHeader(out);
				Document.AddVariable("QUERY",sKeywords);
				
				int loop = 0;
				while (e.hasMoreElements()) { 
					loop = loop + 1;
					if (loop == 1) { Document.WriteTemplate(out, "search-start.tpl"); }
					HashTree htNode = (HashTree) e.nextElement();
					String sNodeTree = Taxonomy.Search.BuildNodeTree(out, (String) htNode.get("PARENTID"), props);
					sNodeTree = "<LI> <a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
									(String) htNode.get("NODEID")+"'>"+
									(String) htNode.get("NODETITLE")+"</a>"+
									   "  (<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
									(String) htNode.get("PNODEID")+"'><i><b>"+
									(String) htNode.get("PNODETITLE")+"</b></i></a>)<BR>"+sNodeTree+"<P>";
					Document.SetHash(htNode);
					Document.AddVariable("NODETREE", sNodeTree);
					Document.WriteTemplate(out, "search.tpl");
				}
				if (loop != 0) { Document.WriteTemplate(out, "search-end.tpl"); }
			}
			// Now print out the nodes "like"
			if (htResults.containsKey("LIKENODES")) {
				HashTree htNodes = (HashTree) htResults.get("LIKENODES");
				Enumeration e = htNodes.elements();
				
				int loop = 0;
				while (e.hasMoreElements()) { 
					loop = loop + 1;
					if (loop == 1) { Document.WriteTemplate(out, "search-likestart.tpl"); }
					HashTree htNode = (HashTree) e.nextElement();
					String sNodeTree = Taxonomy.Search.BuildNodeTree(out, (String) htNode.get("PARENTID"), props);
					sNodeTree = "<LI> <a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
									(String) htNode.get("NODEID")+"'>"+
									(String) htNode.get("NODETITLE")+"</a>"+
									   "  (<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+
									(String) htNode.get("PNODEID")+"'><i><b>"+
									(String) htNode.get("PNODETITLE")+"</b></i></a>)<BR>"+sNodeTree+"<P>";
					Document.SetHash(htNode);
					Document.AddVariable("NODETREE", sNodeTree);
					Document.WriteTemplate(out, "search.tpl");
				}
				if (loop != 0) { Document.WriteTemplate(out, "search-end.tpl"); }
			}
			Document.WriteFooter(out);
		}
		catch (InvalidArguments e) {
			HTMLDocument Document = new HTMLDocument();
			Document.WriteError(out, "Sorry, there are invalid or missing arguments in your request.");
			Log.LogError(e, out); 
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}	
	}
}
