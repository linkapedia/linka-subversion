package People;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class AddExpert
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String ID = (String) props.get("ID");	// ID from the cookie
		String sUserID = (String) props.get ("UserID", ID);	
		String sNodeID = (String) props.get ("NodeID");	

		try {
			if ((sNodeID == null) || (sUserID == null)) { 
				throw new HTML.InvalidArguments(2, "Internal error: missing key words"); 
			}
			
			HashTree htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			htArguments.put("UserID", sUserID);
			htArguments.put("Status", "0");
			htArguments.put("Level", "-1");
					
			InvokeAPI API = new InvokeAPI ("people.TSSetExpert", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}

			// Call API function to select expert topics from the database
			htArguments = new HashTree(props);
			htArguments.put("UserID", sUserID);
			htArguments.put("Level", "-1");
				
			API = new InvokeAPI ("people.TSGetExpert", htArguments);
			htResults = API.Execute(false, false);
			
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-admin.tpl");
			
			if (!htResults.containsKey("NODES")) {
				Document.WriteTemplate(out, "people-no-results.tpl");
				Document.WriteFooter(out);
				return;
			}
			
			HashTree htNodes = (HashTree) htResults.get("NODES");
			Enumeration eNodes = htNodes.elements();
			
			// Call API to get User information for display
			htArguments = new HashTree(props);
			htArguments.put("UserID", sUserID);
				
			API = new InvokeAPI ("tsuser.TSListUsers", htArguments);
			htResults = API.Execute(false, false);
			
			if (!htResults.containsKey("SUBSCRIBERS")) {
				throw new Exception("System Error: That subscriber was not found.");
			}
			
			HashTree htUsers = (HashTree) htResults.get("SUBSCRIBERS");
			HashTree htUser = (HashTree) htUsers.get("SUBSCRIBER");

			String sTabList = People.Expert.BuildTabList("Unverified", sUserID);
			
			Document.AddVariable("Title", "Add Expert Areas");
			Document.AddVariable("Tab", "Unverified");
			Document.AddVariable("TabList", sTabList);
			Document.SetHash(htUser);			
			
			Document.WriteTemplate(out, "people-result-start.tpl");

			while (eNodes.hasMoreElements()) {
				HashTree htNode = (HashTree) eNodes.nextElement();
				String sThisLevel = (String) htNode.get("LEVEL");

				Document.SetHash(htNode);
				Document.AddVariable("NODETREE", Taxonomy.DisplayResults.BuildNodeTree(out, (String) htNode.get("NODEID"), props));
				Document.AddVariable("CHECKED"+sThisLevel, "checked");
				
				Document.WriteTemplate(out, "people-result.tpl");
			}
			
			Document.WriteTemplate(out, "people-result-end.tpl");
			Document.WriteFooter(out);
			
		}
		catch (InvalidArguments e) {
			HTMLDocument Document = new HTMLDocument();
			Document.WriteError(out, "Sorry, there are invalid or missing arguments in your request.");
			e.printStackTrace(out); 
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}	
	}
}
