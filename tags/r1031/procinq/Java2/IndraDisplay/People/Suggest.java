package People;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

import com.indraweb.network.*;
import com.indraweb.execution.Session;

public class Suggest {
    public static void handleTSapiRequest(APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
            throws Exception {
        String submit = (String) props.get("Submit");
        String ID = (String) props.get("ID", true);

        if (submit == null) {
            try {
                HashTree htArguments = new HashTree(props);
                InvokeAPI API = new InvokeAPI("tssubjectarea.TSListSubjectAreas", htArguments);
                HashTree htResults = API.Execute(false, false);
                String sSubjectOption = "";

                if (htResults.containsKey("SUBJECTAREAS")) {
                    HashTree htSubjectAreas = (HashTree) htResults.get("SUBJECTAREAS");
                    Enumeration eH = htSubjectAreas.elements();

                    while (eH.hasMoreElements()) {
                        HashTree htSubject = (HashTree) eH.nextElement();
                        sSubjectOption = sSubjectOption+"<option value="+(String)htSubject.get("SUBJECTAREAID")+
                                         "> "+(String)htSubject.get("SUBJECTNAME")+"\n";
                    }
                }

                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Suggest Expert Areas");
                Document.AddVariable("SUBJECTAREA", sSubjectOption);
                Document.AddVariable("OPTION_CORPUS_LIST", req);
                Document.WriteTemplate(out, "header-admin.tpl");

                Document.AddVariable("SELECTUSERS", (String) props.get("ID", true));
                Document.WriteTemplate(out, "suggest.tpl");
                Document.WriteFooter(out);
            } catch (Exception e) {
                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Suggest Expert Areas");
                Document.WriteTemplate(out, "header-admin.tpl");
                String sErrorMsg = e.getMessage();
                Document.WriteError(out, "<BLOCKQUOTE>" + sErrorMsg + "</BLOCKQUOTE>");
                Log.LogError(e, out);
            }
        } else {
            String sDocText = (String) props.get("doctext", true);
            String sUserID = (String) props.get("UserID", ID);
            String sSubjectArea = (String) props.get("subjectarea");

            if (sSubjectArea.equals("-1")) {
                api.Log.Log("Subject area not selected.");
                throw new Exception("Sorry, you must provide the system with a subject area for classification.");
            }

            // select the corpora associated with this subject area and include as a param to classify
            String sCorporaToUse = "";
            Hashtable htCorporaToUse = new Hashtable();

            HashTree htArguments = new HashTree(props);
            InvokeAPI API = new InvokeAPI("tssubjectarea.TSListCorporaSubjectAreas", htArguments);
            HashTree htResults = API.Execute(false, false);
            if (!htResults.containsKey("SUBJECTAREAS")) {
                api.Log.Log("Subject area not selected.");
                throw new Exception("Sorry, you must provide the system with a subject area for classification.");
            }
            HashTree htSubjects = (HashTree) htResults.get("SUBJECTAREAS");
            Enumeration eH = htSubjects.elements();

            while (eH.hasMoreElements()) {
                HashTree htSubject = (HashTree) eH.nextElement();
                if (((String) htSubject.get("SUBJECTAREAID")).equals(sSubjectArea)) {
                    HashTree htCorpora = (HashTree) htSubject.get("CORPORA");
                    Enumeration eC = htCorpora.elements();
                    while (eC.hasMoreElements()) {
                        HashTree htCorpus = (HashTree) eC.nextElement();
                        htCorporaToUse.put((String) htCorpus.get("CORPUSID"), "1");
                    }
                }
            }

            Enumeration eKeys = htCorporaToUse.keys();
            int loop = 0;
            while (eKeys.hasMoreElements()) {
                loop++;
                if (loop > 1) {
                    sCorporaToUse = sCorporaToUse + ",";
                }
                sCorporaToUse = sCorporaToUse + (String) eKeys.nextElement();
            }

            out.println("<!-- using: " + sCorporaToUse + " as corpus list -->");

            htArguments = new HashTree(props);
            htArguments.put("post", "false");
            htArguments.put("Corpora", sCorporaToUse);
            API = new InvokeAPI("tsclassify.TSClassifyDoc", htArguments);

            // write whatever is in that text box into the temporary file
            File f = Server.Classify.getTempFile(Session.sIndraHome + "/classifyfiles/");
            if (sDocText.toLowerCase().indexOf("<HTML>") == -1) {
                sDocText = Server.Classify.addHTMLHeaders(sDocText);
            }
            PrintWriter fout = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), true));
            fout.println(sDocText);
            fout.close();

            HashTree htResultSet = API.PostExecute(f, false);

            if (!htResultSet.containsKey("CLASSIFICATIONRESULTSET")) {
                api.Log.Log("ClassificationResultSet tag does not exist.");
                throw new Exception("Sorry, this document did not classify into any topics in the system.");
            }

            htResults = (HashTree) htResultSet.get("CLASSIFICATIONRESULTSET");
            Enumeration eC = htResults.keys();

            // Loop through and store the classification results
            while (eC.hasMoreElements()) {
                String sKey = (String) eC.nextElement();
                if (sKey.startsWith("NODE")) {
                    HashTree htNode = (HashTree) htResults.get(sKey);
                    float fScore = new Float((String) htNode.get("SCORE1")).floatValue();
                    if (fScore > 50.0) {
                        String sNodeID = (String) htNode.get("NODEID");
                        String sNodeTitle = (String) htNode.get("NODETITLE");
                        //Log.Log("Classified: NodeID ("+sNodeID+") Title ("+sNodeTitle+")");

                        // Call API function to insert these "unverified" results into the database
                        htArguments = new HashTree(props);
                        htArguments.put("NodeID", sNodeID);
                        htArguments.put("UserID", sUserID);
                        htArguments.put("Status", "0");
                        htArguments.put("Level", "-1");

                        API = new InvokeAPI("people.TSSetExpert", htArguments);
                        API.Execute(false, false);
                    }
                }
            }

            // Call API function to select expert topics from the database
            htArguments = new HashTree(props);
            htArguments.put("UserID", sUserID);
            htArguments.put("Level", "-1");

            API = new InvokeAPI("people.TSGetExpert", htArguments);
            htResults = API.Execute(false, false);

            if (!htResults.containsKey("NODES")) {
                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Suggest Expert Areas");
                Document.WriteTemplate(out, "header-admin.tpl");
                Document.WriteTemplate(out, "people-no-results.tpl");
                Document.WriteFooter(out);
                return;
            }

            HashTree htNodes = (HashTree) htResults.get("NODES");
            Enumeration eNodes = htNodes.elements();

            // Call API to get User information for display
            htArguments = new HashTree(props);
            htArguments.put("UserID", sUserID);

            API = new InvokeAPI("tsuser.TSListUsers", htArguments);
            htResults = API.Execute(false, false);

            if (!htResults.containsKey("SUBSCRIBERS")) {
                throw new Exception("System Error: That subscriber was not found.");
            }

            HashTree htUsers = (HashTree) htResults.get("SUBSCRIBERS");
            HashTree htUser = (HashTree) htUsers.get("SUBSCRIBER");

            String sTabList = People.Expert.BuildTabList("Unverified", sUserID);

            HTMLDocument Document = new HTMLDocument();
            Document.AddVariable("Title", "Suggest Expert Areas");
            Document.WriteTemplate(out, "header-admin.tpl");

            Document.AddVariable("Tab", "Unverified");
            Document.AddVariable("TabList", sTabList);
            Document.SetHash(htUser);
            Document.WriteTemplate(out, "people-result-start.tpl");

            while (eNodes.hasMoreElements()) {
                HashTree htNode = (HashTree) eNodes.nextElement();
                String sThisLevel = (String) htNode.get("LEVEL");

                Document.SetHash(htNode);
                Document.AddVariable("NODETREE", Taxonomy.DisplayResults.BuildNodeTree(out, (String) htNode.get("NODEID"), props));
                Document.AddVariable("CHECKED" + sThisLevel, "checked");

                Document.WriteTemplate(out, "people-result.tpl");
            }

            Document.WriteTemplate(out, "people-result-end.tpl");
            Document.WriteFooter(out);
        }

    }

}
