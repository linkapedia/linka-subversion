package People;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;
import com.indraweb.execution.Session;

public class Expert
{
	public static String BuildTabList (String sState, String sUserID) {

		String sReturn 
			= "<table bgcolor=black border=0 cellpadding=0 cellspacing=0 width=700>\n"+
			  "<tr><td><table border=0 cellpadding=2 cellspacing=1 width=700>\n"+
			  "<tr align=center bgcolor=336666>\n";
		
		if (sState.equals("Unverified")) {
			sReturn = sReturn + "<td class=genrewb width=20%><font size=-1 face=arial>"+
					  "<div class='genrewb'>Unverified</div></font></td>";
		} else {
			sReturn = sReturn + "<td class=genrewb width=20%><font size=-1 face=arial>"+
					  "<div class='genrewb'><a class='genrewb' href='/servlet/Main?UserID="+
					  sUserID+"&template=People.Expert&Tab=Unverified'>Unverified</a></div></font></td>";
		}

		if (sState.equals("Expert")) {
			sReturn = sReturn + "<td class=genrewb width=20%><font size=-1 face=arial>"+
					  "<div class='genrewb'>Expert Areas</div></font></td>";
		} else {
			sReturn = sReturn + "<td class=genrewb width=20%><font size=-1 face=arial>"+
					  "<div class='genrewb'><a class='genrewb' href='/servlet/Main?UserID="+
					  sUserID+"&template=People.Expert&Tab=Expert'>Expert Areas</a></div></font></td>";
		}

		if (sState.equals("Exception")) {
			sReturn = sReturn + "<td class=genrewb width=20%><font size=-1 face=arial>"+
					  "<div class='genrewb'>Exceptions</div></font></td>";
		} else {
			sReturn = sReturn + "<td class=genrewb width=20%><font size=-1 face=arial>"+
					  "<div class='genrewb'><a class='genrewb' href='/servlet/Main?UserID="+
					  sUserID+"&template=People.Expert&Tab=Exception'>Exceptions</a></div></font></td>";
		}
			  
		sReturn = sReturn + "\n</tr></table></td></tr></table>";
		
		return sReturn;
	}
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String ID = (String) props.get("ID");	// ID from the cookie
		String sUserID = (String) props.get ("UserID", ID);	
		String sTab = (String) props.get ("Tab", "Unverified");	
		String submit = (String) props.get ("submit");	

		// If user is submitting results, take care of them here
		if (submit != null) { 
			Enumeration eProps = props.keys();
			
			while (eProps.hasMoreElements()) {
				String sKey = (String) eProps.nextElement();
				String sValue = (String) props.get(sKey);
				
				if ((!sKey.equals("template")) && (!sKey.equals("submit")) && (!sKey.equals("tab")) && 
					(!sKey.equals("skey")) && (!sKey.equals("OPTION_CORPUS_LIST")) && 
					(!sKey.equals("userid")) && (!sKey.equals("id")) && (!sKey.equals("whichdb"))) {

					// Call API to update user expertise information
					sKey = sKey.substring(0, sKey.length()-6);
					HashTree htArguments = new HashTree(props);
					htArguments.put("NodeID", sKey);
					htArguments.put("UserID", sUserID);
					htArguments.put("Status", "1");
					htArguments.put("Level", sValue);
				
					InvokeAPI API = new InvokeAPI ("people.TSSetExpert", htArguments);
					HashTree htResults = API.Execute(false, false);
					
					if (htResults.containsKey("SESSIONEXPIRED")) {		
						Server.Login.handleTSapiRequest(props, out, req, res);
						return;
					}
				}
			}
			
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Expert Areas");
			Document.WriteSuccess(out, "You alterations have been completed successfully."+
									   "<P><a href='/servlet/Main?template=People.Expert&UserID="+sUserID+
									   "&tab=Expert'>Click here</a> to view your Expert settings.");
		} else { try {
			if (sUserID == null) { throw new Exception("System Error: User information not supplied."); }
			
			// Call API to get User information for display
			HashTree htArguments = new HashTree(props);
			htArguments.put("UserID", sUserID);
				
			InvokeAPI API = new InvokeAPI ("tsuser.TSListUsers", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}

			if (!htResults.containsKey("SUBSCRIBERS")) {
				throw new Exception("System Error: That subscriber was not found.");
			}
			
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Expert Areas");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "users/header-admin-expert.tpl");
			
			HashTree htUsers = (HashTree) htResults.get("SUBSCRIBERS");
			HashTree htUser = (HashTree) htUsers.get("SUBSCRIBER");
			Document.SetHash(htUser);
			
			// Call API function to select expert topics from the database
			htArguments = new HashTree(props);
			htArguments.put("UserID", sUserID);
			if (sTab.equals("Unverified")) { htArguments.put("Level", "-1"); }
			if (sTab.equals("Exception")) { htArguments.put("Level", "0"); } 
			else { htArguments.put("NotLevel", "0"); }
				
			API = new InvokeAPI ("people.TSGetExpert", htArguments);
			htResults = API.Execute(false, false);

			String sTabList = BuildTabList(sTab, sUserID);			
			Document.AddVariable("Tab", sTab);
			Document.AddVariable("TabList", sTabList);
			
			if (!htResults.containsKey("NODES")) {
				Document.WriteTemplate(out, "people-no-results.tpl");
				Document.WriteFooter(out);
				return;
			}
			
			HashTree htNodes = (HashTree) htResults.get("NODES");
			Enumeration eNodes = htNodes.elements();
			
			Document.SetHash(htUser);

            if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none"))
                Document.AddVariable("NAME", "Generic User");

			Document.WriteTemplate(out, "people-result-start.tpl");

			while (eNodes.hasMoreElements()) {
				HashTree htNode = (HashTree) eNodes.nextElement();
				String sThisLevel = (String) htNode.get("LEVEL");
				
				Document.SetHash(htNode);
				Document.AddVariable("NODETREE", Taxonomy.DisplayResults.BuildNodeTree(out, (String) htNode.get("NODEID"), props));
				Document.AddVariable("CHECKED"+sThisLevel, "checked");
				
				Document.WriteTemplate(out, "people-result.tpl");
				
				// now clear out checkbox
				Document.AddVariable("CHECKED"+sThisLevel, "");
			}
			
			Document.WriteTemplate(out, "people-result-end.tpl");
			Document.WriteFooter(out);
		} catch (Exception e) {
			String sErrorMsg = e.getMessage();
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Expert Areas");
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}}
	
	}
}
