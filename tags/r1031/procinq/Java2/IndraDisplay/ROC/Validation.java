package ROC;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class Validation
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);	
		String sNodeID = (String) props.get ("NodeID");
		String sScore = (String) props.get ("Score", "FTIMESC");
		String submit = (String) props.get ("submitit");
		
		if (submit != null) {
			HashTree htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			InvokeAPI API = new InvokeAPI ("tsdocument.TSInvalidateDocuments", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			// loop through our incoming arguments and find the documents
			//  that were graded as "hits"
			Enumeration e = req.getParameterNames();
			while ( e.hasMoreElements() )	{
				String sParmName = (String) e.nextElement();
				
				if (sParmName.startsWith("DocID")) {
					sParmName = sParmName.substring(5, sParmName.length());	
					
					// Store affirmative into nodedocument
					htArguments = new HashTree(props);
					htArguments.put("NodeID", sNodeID);
					htArguments.put("DocumentID", sParmName);
					API = new InvokeAPI ("tsdocument.TSValidateDocument", htArguments);
					htResults = API.Execute(false, false);
					
					/*
					if (!htResults.containsKey("SUCCESS")) {
						HTMLDocument Document = new HTMLDocument();
						String sSuccess = "Sorry, the system was unable to validate these documents."+
										  "<br>This is a system error.  Please contact indraweb with "+
										  "the error code "+sParmName+".<P>"+
										  "<a href='/servlet/Main?template=Taxonomy.FrontPage'>Click here"+
										  " to return to the main menu</a>";
						Log.LogError("Error in document validation, attempting to call: \r\n");
						Log.LogError("tsdocument.TSValidateDocument&NodeID="+sNodeID+"&DocumentID="+sParmName+"\r\n");
						Document.WriteSuccess(out, sSuccess);
						return;	
					}
					*/
				}
			} 	
			
			// node is complete, remove it from the validation list
			htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			API = new InvokeAPI ("tsnode.TSRemoveNodeValidation", htArguments);
			htResults = API.Execute(false, false);
			
			sNodeID = null; // reset the node identifier 
		}
		
		if (sNodeID == null) {
			// Get the next topic to be evaluated
			HashTree htArguments = new HashTree(props);
			htArguments.put("CorpusID", sCorpusID);
			InvokeAPI API = new InvokeAPI ("tsnode.TSGetNextValidation", htArguments);
			HashTree htResults = API.Execute(false, false);
		
			if (!htResults.containsKey("NODE")) {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "There are no more documents to validate.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.FrontPage'>Click here"+
								  " to return to the main menu</a> or "+
								  "<a href='/servlet/Main?template=ROC.Analysis&CorpusID="+sCorpusID+"'>click here to"+
								  " output the results of your analysis to file.";
			
				Document.WriteSuccess(out, sSuccess);
				return;
			}
		
			HashTree htNode = (HashTree) htResults.get("NODE");
			sNodeID = (String) htNode.get("NODEID");
			String sCount = (String) htNode.get("DOCUMENTCOUNT");
		}
		
		HashTree htArguments = new HashTree(props);
		htArguments.put("NodeID", sNodeID);
		InvokeAPI API = new InvokeAPI ("tsnode.TSGetNodeProps", htArguments);
		HashTree htResults = API.Execute(false, false);
		
		HashTree htNode = (HashTree) htResults.get("NODE");
		String sNodeTitle = (String) htNode.get("NODETITLE");
	
		// Get every Nth document for this validation topic 
		htArguments = new HashTree(props);
		htArguments.put("NodeID", sNodeID);
		API = new InvokeAPI ("tsdocument.TSGetROCDocument", htArguments);
		htResults = API.Execute(false, false);

		if (!htResults.containsKey("DOCUMENTS")) {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("NodeID", sNodeID);
			Document.AddVariable("Title", sNodeTitle+": ROC Document Validation");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			
			Document.WriteTemplate(out, "header-nosearch.tpl");
			Document.WriteTemplate(out, "ROC/validation-head.tpl");
		
			out.println("No documents have been registered for this topic!  It cannot be used.");
			
			Document.WriteTemplate(out, "ROC/validation-foot.tpl");
			Document.WriteFooter(out);				
			return;
		}
		
		// Write Yes/No Templates
		HTMLDocument Document = new HTMLDocument();
		Document.AddVariable("NodeID", sNodeID);
		Document.AddVariable("Title", sNodeTitle+": ROC Document Validation");
		Document.AddVariable("OPTION_CORPUS_LIST", req);
		Document.WriteTemplate(out, "header-nosearch.tpl");
	
		Document.AddVariable("NodeTitle", sNodeTitle);
		Document.AddVariable("CorpusID", sCorpusID);

		if (sScore.equals("FTIMESC")) { Document.AddVariable("FCD", "SELECTED"); }
		if (sScore.equals("SCORE4")) { Document.AddVariable("SCORE4D", "SELECTED"); }
		if (sScore.equals("SCORE5")) { Document.AddVariable("SCORE5D", "SELECTED"); }
		
		Document.WriteTemplate(out, "ROC/validation-head.tpl");

		HashTree htDocuments = (HashTree) htResults.get("DOCUMENTS");
		Vector vN = Sort(htDocuments, sScore);
		Enumeration eD = vN.elements();
		
		while (eD.hasMoreElements()) {
			HashTree htDocument = (HashTree) eD.nextElement();
			Document.SetHash(htDocument);
			Document.AddVariable("SCORE", (String) htDocument.get(sScore));
			
			Document.WriteTemplate(out, "ROC/validation.tpl");
		}
		
		Document.WriteTemplate(out, "ROC/validation-foot.tpl");
		Document.WriteFooter(out);				
	}

	// Add two vectors together, into a destination Vector
	private static void addVect( Vector dest, Vector left, Vector right ) {
		
		int i;
		   
		dest.removeAllElements();                     // reset destination
		   
		for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
		for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
		   
	}
	
	private static void qs(Vector v, String Score) {
   
		Vector vl = new Vector();                      // Left and right sides
		Vector vr = new Vector();
		HashTree el;
		float key;                                    // key for splitting
      
		if (v.size() < 2) return;                      // 0 or 1= sorted
      
		HashTree htResult = (HashTree) v.elementAt(0);
		key = new Float((String) htResult.get(Score)).floatValue();  
	      
	    // Start at element 1
	    for (int i=1; i < v.size(); i++) {
	       el = (HashTree) v.elementAt(i);
		   if (new Float((String) el.get(Score)).floatValue() < key) 
			   vr.addElement(el); // Add to right
	       else vl.addElement(el);                     // Else add to left
	    }
      
	    qs(vl, Score);                                        // Recursive call left
		qs(vr, Score);                                        //    "        "  right
		vl.addElement(v.elementAt(0));
 
		addVect(v, vl, vr);
	}
	
	public static Vector Sort(HashTree ht, String Score) {
		Vector vNodeDocuments = new Vector();
		Enumeration e = ht.elements();
			
		// Fill the vector with all the results
		while (e.hasMoreElements()) {
			HashTree htResult = (HashTree) e.nextElement();
			vNodeDocuments.addElement(htResult);
		}
			
		// Sort this vector
		qs(vNodeDocuments, Score);
		
		return vNodeDocuments;
	}
}
