package Idrac;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.HashTree;
import com.iw.system.InvokeAPI;

public class KeywordBuilder
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			String sQuery = (String) props.get ("S", false);
			String rowmax = (String) props.get ("R", "10");
			String sort = (String) props.get ("O", "ASC");
			String start = (String) props.get ("start", "1");
            String notidrac = (String) props.get ("notidrac");

            // "corpora" is a multiple select list.  it can not, therefore, be accessed by the
            //  usual props.get command

            String[] corpora;
            corpora = req.getParameterValues("Corpora");
            Vector vCorpora = new Vector(); Hashtable htCorp = new Hashtable();
            if (corpora != null) {
                for (int i=0; i<corpora.length; i++) {
                    String sParmVal = (String) corpora[i];
                    htCorp.put(sParmVal, sParmVal);
                    vCorpora.addElement(sParmVal);
                }
            }

            out.println("<!-- start: "+start+" -->");
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("START", start);
			Document.AddVariable("ROWMAX", rowmax);

            if (new Integer(start).intValue() < 1) {
                Document.AddVariable("START", "1");
            }

            HashTree htArguments = new HashTree(props);
            InvokeAPI API = new InvokeAPI ("tscorpus.TSListCorpora", htArguments);
            HashTree htResults = API.Execute(false, false);
            String sOPTION_CORPUS_LIST = "";

            if (htResults.containsKey("CORPORA")) {
                HashTree htCorpora = (HashTree) htResults.get("CORPORA");
                Vector v = htCorpora.Sort("CORPUS_NAME");

                Enumeration e2 = v.elements();
                while (e2.hasMoreElements()) {
                    HashTree htCorpus = (HashTree) e2.nextElement();
                    if (new Integer((String) htCorpus.get("CORPUSACTIVE")).intValue() == 1) {
                        String CorpusID = (String) htCorpus.get("CORPUSID");
                        String CorpusName = (String) htCorpus.get("CORPUS_NAME");
                        if (CorpusName.length() > 25) { CorpusName = CorpusName.substring(0,22)+"..."; }
                        if (htCorp.containsKey(CorpusID)) {
                            sOPTION_CORPUS_LIST=sOPTION_CORPUS_LIST+"<option selected value="+CorpusID+"> "+CorpusName;
                        } else { sOPTION_CORPUS_LIST=sOPTION_CORPUS_LIST+"<option value="+CorpusID+"> "+CorpusName; }
                    }
                }
                Document.AddVariable("CORPORA", sOPTION_CORPUS_LIST);
            }

 			if (sQuery == null) {
				Document.WriteTemplate(out, "search/keyword-list/kwdIdx.tpl");
				Document.WriteTemplate(out, "search/keyword-list/keyword-begin.tpl");
			} else {
                Document.AddVariable("SEARCH", sQuery);
				String country = (String) props.get ("country", "ALL");
				String countrytext = (String) props.get ("countrytext", "All Regions");

				if (props.get("submit") == null) {
					String NextPrev = (String) props.get("NEXT");
					if (NextPrev.equals("1")) {
						start = ""+(new Integer(start).intValue()+new Integer(rowmax).intValue());
					} else {
						start = ""+(new Integer(start).intValue()-new Integer(rowmax).intValue());
					}
					Document.AddVariable("START", start);
                    if (new Integer(start).intValue() < 1) {
                        Document.AddVariable("START", "1");
                    }
                }
				String query = "SELECT <NODE> WHERE ("+
							   Idrac.Search.BuildQuery(sQuery, "NODETITLE")+") ";
				boolean bUsedAnd = false; boolean bUsedAnd2 = false;
				Document.WriteTemplate(out, "search/keyword-list/kwdIdx.tpl");

                // Find out which corpora were selected and add to query
                Enumeration eV = vCorpora.elements();
                while (eV.hasMoreElements()) {
                    String sCorpusID = (String) eV.nextElement();
                    if (!bUsedAnd) { bUsedAnd = true; query = query + " and ("; }
                    if (bUsedAnd2) { query = query + " or "; } else { bUsedAnd2 = true; }
					query = query + "CORPUSID = '"+sCorpusID+"' ";
                } if (vCorpora.size() > 0) { query = query + ") "; }

				// add country to CQL if available
				if (!country.toLowerCase().equals("all") && (notidrac == null)) {
					query = query + " and (COUNTRY = '"+country+"')";
				}

				Document.AddVariable("COUNTRY", countrytext);

				query = query + " ORDER BY NODETITLE "+sort;
				out.println("<!-- CQL: "+query+" -->");

				htArguments = new HashTree(props);
				sQuery = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sQuery,"=","%3D"));
				htArguments.put("query", query);
				htArguments.put("rowmax", rowmax);
				htArguments.put("start", start);
				API = new InvokeAPI ("tscql.TSCql", htArguments);
				htResults = API.Execute(false, false, false);

				// CATEGORY RESULTS
				if (!htResults.containsKey("NODES")) {
					Document.WriteTemplate(out, "search/keyword-list/no-results-found.tpl");
					out.println("<!-- no category hits found for: "+query+" --> ");
				} else {
					// Process NODE RESULTS
					HashTree htNodes = (HashTree) htResults.get("NODES");
                    Vector vNodes = htNodes.Sort("NODETITLE");
					Enumeration eN = vNodes.elements();

					Document.AddVariable("NUMRESULTS", (String) htResults.get("NUMRESULTS"));
					int iEND = ((new Integer(start).intValue()+
		 		 	 	 	 	new Integer(rowmax).intValue()) - 1);
					if (iEND > new Integer((String) htResults.get("NUMRESULTS")).intValue()) {
						iEND = new Integer((String) htResults.get("NUMRESULTS")).intValue();
					}
                    if (iEND < 1) { iEND = new Integer(rowmax).intValue(); }


					// NEXT - PREV
					if (new Integer(start).intValue() > 1) {
						Document.AddVariable("PREVIOUS", "<a href='JavaScript:PrevPage();'>Previous</a>");
					} else { Document.AddVariable("PREVIOUS", "Previous"); }
					if (iEND < new Integer ((String) htResults.get("NUMRESULTS")).intValue()) {
						Document.AddVariable("NEXT", "<a href='JavaScript:NextPage();'>Next</a>");
					} else { Document.AddVariable("NEXT", "Next"); }

					Document.AddVariable("END", ""+iEND);

					Document.WriteTemplate(out, "search/keyword-list/keyword-and-or.tpl");

					int loop = new Integer(start).intValue() - 1;
                    if (loop < 0) { loop = 0; }
					while (eN.hasMoreElements()) {
						loop++; Document.AddVariable("LOOP", loop+"");
						HashTree htNode = (HashTree) eN.nextElement();

						String sParentID = (String) htNode.get("PARENTID");
						String sParentTitle = "Root";

						if (!sParentID.equals("-1")) {
							htArguments = new HashTree(props);
							htArguments.put("NodeID", sParentID);
							API = new InvokeAPI ("tsnode.TSGetNodeProps", htArguments);
							HashTree htNodeResults = API.Execute(false, false);

							if (htNodeResults.containsKey("NODE")) {
								sParentTitle = (String) ((HashTree) htNodeResults.get("NODE")).get("NODETITLE");
							}
						}

						Document.AddVariable("PARENT", sParentTitle);
						Document.SetHash(htNode);
						Document.WriteTemplate(out, "search/keyword-list/keyword-result.tpl");
					}
					Document.WriteTemplate(out, "search/keyword-list/keyword-results-end.tpl");
				}

			}
			Document.WriteTemplate(out, "search/keyword-list/keyword-end.tpl");
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}

	}
}
