/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Jan 23, 2003
 * Time: 6:42:40 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.metasearch;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.util.Vector;
import java.util.Hashtable;

public class Data_SearchengineCallout {


    private static Hashtable htIntegerSEidToDataOnOneSE;
    private static int[] iArrSEIDs;
    /*
            Create table CALLOUTMETASEARCH (
            SEID NUMBER(4) NOT NULL UNIQUE,
            NAME VARCHAR2(30) NOT NULL UNIQUE,
            SHORTNAME VARCHAR2(3) NOT NULL UNIQUE,
            CLASSPATH VARCHAR2(256) NOT NULL UNIQUE

        Create table CORPUSSearchEngine(
            SEID NUMBER(4) NOT NULL UNIQUE,
            CORPUSID CORPUS(9) NOT NULL UNIQUE,
    */

    // TABLE read MS callouts from DB :
    // create table calloutMetasearch
    // NAME 30 chars
    // SHORTNAME
    // CLASSPATH - see len of others 256 ?
    // MAX WANTED

    // TABLE CORPUSSEARCHENGINE
    // corpusid
    //


    public static synchronized String getSEShortName (int iSearchEngineID ) throws Exception
    {
        DataOnOneSE d = (DataOnOneSE) htIntegerSEidToDataOnOneSE.get ( new Integer ( iSearchEngineID ) );
        if ( d == null )
              d = d;
        return d.sSHORTNAME;
    }

    public static synchronized String getClassPath (int iSearchEngineID ) throws Exception
    {
        DataOnOneSE d = (DataOnOneSE) htIntegerSEidToDataOnOneSE.get (new Integer ( iSearchEngineID ));
        return d.sCLASSPATH;
    }

    public static synchronized int getMaxPerSearchThisSE (int iSearchEngineID ) throws Exception
    {
        DataOnOneSE d = (DataOnOneSE) htIntegerSEidToDataOnOneSE.get (new Integer ( iSearchEngineID ));
        return d.iMaxPerSearchThisSE;
    }

    public static synchronized int getNumResultsOnPage (int iSearchEngineID ) throws Exception
    {
        DataOnOneSE d = (DataOnOneSE) htIntegerSEidToDataOnOneSE.get (new Integer ( iSearchEngineID ));
        return d.iNumResultsPerPage;
    }

    public static synchronized int[] getSearchEngineIDs()
    {
        return iArrSEIDs;
    }

    public static boolean bNeedCacheBeFilled = true;
    public static synchronized void fillOrRefillCache ( Connection dbc ) throws Exception
    {
        if ( bNeedCacheBeFilled )
        {
            htIntegerSEidToDataOnOneSE = new Hashtable();
            iArrSEIDs = null;

            Vector vISearchEngineIDs = new Vector();

            String sSQL = "select SEID, NAMESHORT, CLASSPATH, NUMPERPAGE, NUMWANTEDPERSEARCH from " +
                    //" MSSEARCHENGINES  C1, CorpusSearchEngine C2 where " +
                    " MSSEARCHENGINES where " +
                    //" c1.SEID = c2.SEID(+) " +
                    " ACTIVE = 1  "  +
                    // " and c1.AllCorpora = 1  "  +
                        " order by SEID ";

            //System.out.println( " sSQL [" + sSQL + "]" );
            Statement stmt = null;
            ResultSet rs = null;
            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery ( sSQL );
                while (rs.next())
                {

                    int iSEID = rs.getInt(1);
                    String sSHORTNAME = rs.getString(2);
                    String sCLASSPATH = rs.getString(3);
                    int iNumPerPage = rs.getInt(4);
                    int iNumWantedPerSearch = rs.getInt(5);

                    Integer ISEID = new Integer (iSEID);
                    htIntegerSEidToDataOnOneSE.put (ISEID,
                            new DataOnOneSE ( iSEID,
                                    sSHORTNAME,
                                    sCLASSPATH,
                                    iNumPerPage,
                                    iNumWantedPerSearch ));
                    vISearchEngineIDs.addElement(ISEID);
                }
            } finally {
                if ( rs != null )
                    rs.close();
                stmt.close();
            }
            iArrSEIDs = com.indraweb.utils.sets.UtilSets.convertVectorTo_intArray(vISearchEngineIDs);
            bNeedCacheBeFilled = false;
        }
    }

    private static class DataOnOneSE {
        int iSEID;
        String sSHORTNAME;
        String sCLASSPATH;
        int iNumResultsPerPage;
        int iMaxPerSearchThisSE;

        public DataOnOneSE(
                int iSEID_,
                String sSHORTNAME_,
                String sCLASSPATH_,
                int iNumResultsPerPage_ ,
                int iMaxPerSearchThisSE_)
        {
            iSEID = iSEID_;
            sSHORTNAME = sSHORTNAME_;
            sCLASSPATH = sCLASSPATH_;
            iNumResultsPerPage = iNumResultsPerPage_;
            iMaxPerSearchThisSE = iMaxPerSearchThisSE_;
        }

    }
}



