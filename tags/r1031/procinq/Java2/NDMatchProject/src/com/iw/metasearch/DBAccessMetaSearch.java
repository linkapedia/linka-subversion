package com.iw.metasearch;

import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.execution.Session;
import com.indraweb.util.Log;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilProfiling;
import com.indraweb.util.clsUtils;
import java.io.File;
import java.util.*;

public class DBAccessMetaSearch {

    // **************************************************************************************
    // Recursive the tree: given a Node Id, get all associated search categories.   If none,
    // extract search categories from parent.   Repeat this process.  If none of this Node's
    // ancestors have search categories, default to the web search engines
    //
    private static Hashtable htgetCategoriesfromDb = new Hashtable(); // save results

    public static Vector getCategoriesfromDb(long NodeId, long MetaChannelId, java.sql.Connection dbc)
            throws Exception // **************************************************************************************
    {
        String sql = null;
        long count = 0;
        long NNodeId = NodeId;
        Vector vReturn = null;
        String htKey = NodeId + ":";
        vReturn = (Vector) htgetCategoriesfromDb.get(htKey);

        if (vReturn == null) // if not yet cached
        {
            try {
                // First: Find the NodeId in the ancestory containing NodeGroup results.
                while ((NNodeId != -1) && (count == 0)) {
                    sql = "select count(*) from NodeGroup where NodeId = " + NNodeId;
                    count = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbc);

                    if (count == 0) {
                        sql = "select parentid from node where NodeId = " + NNodeId;
                        NNodeId = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbc);
                    }

                }

            } catch (Exception e) {
                Log.FatalError("error in ExecuteQueryAgainstDBlong sql [" + sql + "]", e);
            }
            // If NNodeId is -1, we walked to the top of the hierarchy and did not find any

            // associated engines.   In that case, return all engines associated with category id 1

            // (representing a search of the basic search engines).

            if (NNodeId == -1) {
                return new Vector();
            }

            // Return the channels
            sql = "select distinct(C.ischannel) from Channel C, CategoryChannel CC, NodeGroup N, SearchCategory S";
            sql = sql + " where N.GroupId = CC.SearchCategoryId and CC.Channelid = C.Channelid";
            sql = sql + " and S.SearchCategoryId = CC.SearchCategoryId and S.MetaChannelId = " + MetaChannelId;
            sql = sql + " and N.NodeId = " + NNodeId;

            vReturn = JDBCIndra_Connection.executeQueryAgainstDBVector(sql, dbc);

            htgetCategoriesfromDb.put(htKey, vReturn);

        }
        return vReturn;
    }
    // ***********************************************************************************
    // Given an Engine Name, get the channel identifier from the database
    private static Hashtable htgetChannelfromEngineNameDb = null; // save results
    private static final Object oSynch_initChannelFileFromDB = new Object();

    public static void initChannelFileFromDB(java.sql.Connection dbc) // **************************************************************************************
    {
        boolean bForceCacheRefresh = false;
        synchronized (oSynch_initChannelFileFromDB) {
            String sDBFile = Session.sIndraHome + "/" + "ChannelfromEngineNameDb.txt";
            File f = new File(sDBFile);
            long lFileModTime = f.lastModified();
            double fileageInHours = UtilProfiling.convertMSToHours(System.currentTimeMillis() - lFileModTime);

            // recreate file if necessary
            if (!UtilFile.bFileExists(sDBFile) || fileageInHours > 24) {
                bForceCacheRefresh = true;
                String sql = null;
                try {
                    sql = "select Ischannel, Channelid from Channel ";
                    Vector vDBResult = JDBCIndra_Connection.executeDBQueryVecOfVecsReturned(sql, dbc, -1);
                    if (vDBResult == null || vDBResult.size() < 2) {
                        Log.FatalError("can not initialize chennel DB file");
                    }
                    Vector v0StrISChannelNames = (Vector) vDBResult.elementAt(0);
                    Vector v1BigDecISChannelIDs = (Vector) vDBResult.elementAt(1);
                    UtilFile.addLineToFileKill(sDBFile, "# file from select Ischannel, Channelid from Channel on [" + new Date() + "]\r\n");

                    for (int iDBIndex = 0; iDBIndex < v0StrISChannelNames.size(); iDBIndex++) {
                        String s0_ChannelName = (String) v0StrISChannelNames.elementAt(iDBIndex);
                        long l0_ChannelID = ((java.math.BigDecimal) v1BigDecISChannelIDs.elementAt(iDBIndex)).longValue();
                        UtilFile.addLineToFile(sDBFile, s0_ChannelName + "\t" + l0_ChannelID + "\r\n");
                    }
                    Log.log("wrote new channel file [" + sDBFile + "]\r\n");
                } catch (Exception e) {
                    Log.FatalError("error in ExecuteQueryAgainstDBStr sql [" + sql + "]", e);
                }
            }
            // load file into HT
            if (htgetChannelfromEngineNameDb == null || bForceCacheRefresh) {
                htgetChannelfromEngineNameDb = new Hashtable();
                Enumeration e = UtilFile.enumFileLines(sDBFile);
                while (e.hasMoreElements()) {
                    String sLine = ((String) e.nextElement()).trim();
                    if (!sLine.startsWith("#")) {
                        StringTokenizer st = new StringTokenizer(sLine, "\t");

                        String sKey = st.nextToken();
                        String sValue = st.nextToken();
                        htgetChannelfromEngineNameDb.put(sKey, sValue);
                    }
                }
            }  // if mem cache needs to be laoded
        } // synch
    }

    public static String getChannelfromEngineNameDb(String sEngineId) {
        if (htgetChannelfromEngineNameDb == null) {
            Log.FatalError("ht not initialized for getChannelfromEngineNameDb(), call initChannelFileFromDB() first \r\n");
        }

        String sChannel = (String) htgetChannelfromEngineNameDb.get(sEngineId);
        if (sChannel == null) {
            for (int i = 0; i < 10; i++) {
                clsUtils.pause_this_many_milliseconds((i + 2) * 1000);
                sChannel = (String) htgetChannelfromEngineNameDb.get(sEngineId);
                if (sChannel != null) {
                    break;
                }
            }
            if (sChannel == null) {
                Log.FatalError("FINAL : no channel ID for engine [" + sEngineId + "]\r\n");
            }
        }
        //Log.FatalError ();
        return sChannel;
    }
}
