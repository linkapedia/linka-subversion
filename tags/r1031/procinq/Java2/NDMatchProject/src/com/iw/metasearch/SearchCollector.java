/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Jan 23, 2003
 * Time: 5:50:23 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.metasearch;

import com.indraweb.util.Log;
import com.indraweb.webelements.IndraURL;

import java.lang.reflect.Method;
import java.util.Hashtable;

import api.tsmetasearchnode.Data_MSAndScoreResult_acrossSEs;

public class SearchCollector {

    private static Hashtable ht_sClasspathToMethod = new Hashtable();
    private static Hashtable ht_sClasspathToClassParmsArr = new Hashtable();
    private static Hashtable ht_sClasspathToClass = new Hashtable();

    private static Integer ISynch_SearchEngineCacheNew = new Integer(1);;


    /**
     * go thru the set of db entries for which classifiers use which external scoring functions
     *
     * caches methods
     * serial at this point
     *
     * hbk 2002 12 09
     */
    public static void searchCollect(
            int iSEID,
            com.iw.scoring.NodeForScore nfs,
            api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQForMS
/*
            java.io.PrintWriter out,
            boolean bStemOn,
            boolean bThesaurusOn,
            boolean bWantPhrasing
*/
            ) throws Exception {
        String sClasspath = (String) Data_SearchengineCallout.getClassPath(iSEID);
        Method methThisClasspath = (Method) ht_sClasspathToMethod.get(sClasspath);
        Class[] classArrParmsThisClasspath = (Class[]) ht_sClasspathToClassParmsArr.get(sClasspath);
        Class classThisClasspath = (Class) ht_sClasspathToClass.get(sClasspath);

        if (methThisClasspath == null)
        { // cache all we can inside here re: class, methods, parm arrays
            // get class
            // get method array
            Class c1 = Class.forName("java.lang.Integer");
            Class c2 = Class.forName("com.iw.scoring.NodeForScore");
            Class c3 = Class.forName("api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode");
            Class c4 = Class.forName("com.indraweb.util.IntContainer");

            classArrParmsThisClasspath = new Class[4];
            classArrParmsThisClasspath[0] = c1;
            classArrParmsThisClasspath[1] = c2;
            classArrParmsThisClasspath[2] = c3;
            classArrParmsThisClasspath[3] = c4;

            // create the class against which to invoke the API method call
            try {

                classThisClasspath = Class.forName(sClasspath);

                // find the method in that class to invoke
                long lStart = System.currentTimeMillis();
                methThisClasspath = classThisClasspath.getMethod("doSearch", classArrParmsThisClasspath);
                ht_sClasspathToMethod.put(sClasspath, methThisClasspath);
                ht_sClasspathToClass.put(sClasspath, classThisClasspath);
                ht_sClasspathToClassParmsArr.put(sClasspath, classArrParmsThisClasspath);
            } catch (Exception e) {
                throw e;
            }
        } // if ( methThisClasspath == null )
        // make the call finally
        // build the objects to pass in to the invoke
        com.indraweb.util.IntContainer intContainerNumIURLsThisSE = new com.indraweb.util.IntContainer();
        Object[] oArr = new Object[classArrParmsThisClasspath.length];
        int iO = 0;
        oArr[iO++] = new Integer(iSEID);
        oArr[iO++] = nfs;
        oArr[iO++] = workQForMS;
        oArr[iO++] = intContainerNumIURLsThisSE;

        //Log.logClearcr(" invoking search callout nodeid [" + nfs.getNodeID() + "] iSEID [" + iSEID + "] name [" + Data_SearchengineCallout.getSEShortName(iSEID) + "]");

        //api.util.UtilReflection.printObjectArrayClassNames ( oArr );
        //api.util.UtilReflection.printClassNameMethodsAndSigs ( classThisClasspath , "doSearch" );

        methThisClasspath.invoke(classThisClasspath, oArr );

        //Log.logClearcr(" completed search iSEID [" + iSEID + "] name [" +
         //       Data_SearchengineCallout.getSEShortName(iSEID) + "] # [" + intContainerNumIURLsThisSE.getiVal() + "]");
        if ( intContainerNumIURLsThisSE.getiVal() == 0 )
        {
/*
            public IndraURL(String sURL_, String title_, String date_, String summary_,
                            int iSEid_, int sequenceNum_)
*/
            IndraURL iURLDummy = new IndraURL ( "ZEROFOUNDMARKER", "","","",iSEID, -1);
            workQForMS.getData_MSAndScoreResult().recordMSSearchHits (iURLDummy);
        }


    }
}






/*

package com.iw.metasearch;

import java.lang.reflect.Method;
import java.util.Hashtable;

public class SearchCollector {

    private static Hashtable ht_sClasspathToMethod = new Hashtable();
    private static Hashtable ht_sClasspathToClassParmsArr = new Hashtable();
    private static Hashtable ht_sClasspathToClass = new Hashtable();

    private static Integer ISynch_SearchEngineCacheNew = new Integer(1);;


     * go thru the set of db entries for which classifiers use which external scoring functions
     *
     * caches methods
     * serial at this point
     *
     * hbk 2002 12 09
    public static void searchCollect(
            int iSEID,
            com.iw.scoring.NodeForScore nfs,
            com.iw.threadmgr.WorkQManager workQ

            ) throws Exception {
        String sClasspath = (String) Data_SearchengineCallout.getClassPath(iSEID);
        Method methThisClasspath = (Method) ht_sClasspathToMethod.get(sClasspath);
        Class[] classArrParmsThisClasspath = (Class[]) ht_sClasspathToClassParmsArr.get(sClasspath);
        Class classThisClasspath = (Class) ht_sClasspathToClass.get(sClasspath);

        if (methThisClasspath == null) { // cache all we can inside here re: class, methods, parm arrays
            // get class
            // get method array
            Class c1 = Class.forName("java.lang.Integer");
            Class c2 = Class.forName("com.iw.scoring.NodeForScore");
            Class c3 = Class.forName("com.iw.threadmgr.WorkQManager");

            classArrParmsThisClasspath = new Class[3];
            classArrParmsThisClasspath[0] = c1;
            classArrParmsThisClasspath[1] = c2;
            classArrParmsThisClasspath[2] = c3;

            // create the class against which to invoke the API method call
            try {

                classThisClasspath = Class.forName(sClasspath);

                // find the method in that class to invoke
                long lStart = System.currentTimeMillis();
                methThisClasspath = classThisClasspath.getMethod("genScores", classArrParmsThisClasspath);
                ht_sClasspathToMethod.put(sClasspath, methThisClasspath);
                ht_sClasspathToClass.put(sClasspath, classThisClasspath);
                ht_sClasspathToClassParmsArr.put(sClasspath, classArrParmsThisClasspath);
            } catch (Exception e) {
                throw e;
            }
        } // if ( methThisClasspath == null )
        // make the call finally
        // build the objects to pass in to the invoke
        Object[] oArr = new Object[classArrParmsThisClasspath.length];
        int iO = 0;
        oArr[iO++] = new Integer(iSEID);
        oArr[iO++] = nfs;
        oArr[iO++] = workQ;

        methThisClasspath.invoke(classThisClasspath, oArr);

    }
}
*/