/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Jan 14, 2003
 * Time: 6:16:15 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.nodefeed.helper;

import java.util.*;
import java.sql.*;

import com.javaexchange.dbConnectionBroker.DbConnectionBroker;

public class DBConnectionJX
{
    public static Hashtable htDbConnectionBrokers = new Hashtable();
    private static Object oSynchCountConnections = new Object();
    private static int iCountConnectionGets = 0;
    private static int iCountConnectionFree = 0;


    public static Connection getConnection(String sDB_OracleJDBC,
                                           String sDB_user,
                                           String sDB_pass
                                           )
            throws Exception
    {


/*
        synchronized (oSynchCountConnections)
        {
            iCountConnectionGets++;
            System.out.println("Freed DBconn from sDB_OracleJDBC ["+sDB_OracleJDBC+"]" +
                    " gets [" + iCountConnectionGets + "]" +
                    " frees [" + iCountConnectionFree + "]" +
                    " out [" + (iCountConnectionGets - iCountConnectionFree) + "]");
        }
*/

        DbConnectionBroker dbcbToUse = (DbConnectionBroker) htDbConnectionBrokers.get(sDB_OracleJDBC);
        if (dbcbToUse == null)
        {
            synchronized (htDbConnectionBrokers)
            {
                dbcbToUse = (DbConnectionBroker) htDbConnectionBrokers.get(sDB_OracleJDBC);
                if (dbcbToUse == null)
                {
                    // register driver
                    System.out.println("@@@ com.iw.nodefeed.helper.DBConnectionJX creating a new connection pool sDB_OracleJDBC  [" + sDB_OracleJDBC + "]");
                    java.sql.DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
                    try
                    {

                        dbcbToUse = new DbConnectionBroker(
                                "oracle.jdbc.driver.OracleDriver",
                                sDB_OracleJDBC,
                                sDB_user,
                                sDB_pass,
                                1, // min conns
                                200, // max cnns
                                "/temp/DBConnectionPool.log",
                                1 //maxConnTime: Time in days between connection resets. (Reset does a basic cleanup)
                        );
                        htDbConnectionBrokers.put(sDB_OracleJDBC, dbcbToUse);
                    } catch (Exception e)
                    {
                        String sStackTrace = com.indraweb.util.Log.stackTraceToString(e);
                        System.out.println("Error in DBConnectionJX.java\r\n" +
                                e.getMessage() + "\r\n" + sStackTrace);
                        throw e;
                    }
                } // if still null within synch
            }
        }

        Connection dbcRtn = dbcbToUse.getConnection();
        return dbcRtn;
    }	 // ection getDBConnection()

    // put connection back in the pool for use
    public static Connection freeConnection(Connection dbc, String sWhichDB)
            throws Exception
    {
        DbConnectionBroker dbcb = (DbConnectionBroker) htDbConnectionBrokers.get(sWhichDB);
        if (dbcb == null)
            throw new Exception("internal program error no such DB connection pool [" + sWhichDB + "]");
        dbcb.freeConnection(dbc);
/*
        synchronized (oSynchCountConnections)
        {
            iCountConnectionFree++;
            System.out.println("Freed DBconn from sWhichDB ["+sWhichDB+"]" +
                    " gets [" + iCountConnectionGets + "]" +
                    " frees [" + iCountConnectionFree + "]" +
                    " out [" + (iCountConnectionGets - iCountConnectionFree) + "]");
        }
*/
        return null;

    }

    public static void destroy(String sWhichDB)
            throws Exception
    {
        DbConnectionBroker dbcb = (DbConnectionBroker) htDbConnectionBrokers.get(sWhichDB);
        if (dbcb == null)
            throw new Exception("internal program error no such DB connection pool [" + sWhichDB + "]");
        dbcb.destroy();
    }

}
