package com.iw.nodefeed.helper;

import java.util.*;
import java.io.*;
import java.net.*;

public class HashTree extends Hashtable {
	private HashTree htParent;

	// Accessor and set methods for new Hashtable attribute, htParent..
	public HashTree GetParent() { return htParent; }
	public void SetParent(HashTree htParent) {
		this.htParent = htParent;	
	}
	
	// Another constructor
	public HashTree () {}
	public HashTree (String SessionKey) {
		if (SessionKey != null) { this.put("SKEY", SessionKey); }
	}

/*
    // incomplete
    public String toString ()
    {

        Enumeration e = this.keys();
        int i = 1;
        while ( e.hasMoreElements())
        {
            Object oKey = e.nextElement();
            Object oVal = this.get ( oKey );
            System.out.println ( "("+i+")["+ oKey+"]:["+oVal+"]" );
            i++;
        }
    }
*/
}
