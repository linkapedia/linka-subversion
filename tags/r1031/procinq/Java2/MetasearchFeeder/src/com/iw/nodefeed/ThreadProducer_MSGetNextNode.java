/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Jan 14, 2003
 * Time: 3:56:11 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.nodefeed;

import com.indraweb.util.UtilProfiling;
import com.indraweb.util.Log;
import com.iw.threadmgr.ThreadGenericProdCons;
import com.iw.threadmgr.WorkQManager;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.Types;
import java.util.Hashtable;
import java.util.HashSet;

public class ThreadProducer_MSGetNextNode extends ThreadGenericProdCons {
    public ThreadProducer_MSGetNextNode(String sDesc_,
                                     WorkQManager producerConsumerWorkQGeneric_,
                                     String sDB_OracleJDBC_,
                                     String sDB_user_,
                                     String sDB_pass_
                                     ) {
        super(sDesc_, producerConsumerWorkQGeneric_, sDB_OracleJDBC_, sDB_user_, sDB_pass_);
    }


    private static Object oSynch_iFirstNode = new Object();
    private static HashSet hsNodesDone = new HashSet();
    private static final int iStartNode = 2521;
    private static int iCurNode = iStartNode;
    private synchronized int getNextNode_test()
    {
        synchronized ( oSynch_iFirstNode )
        {
            for ( int i = 0; true ; i++ )
            {
                iCurNode = iCurNode + i;
                try
                {
					throw new Exception ("code used to say, but since not used and causing build issue ... HBK 6/2006 : Data_NodeIDsTo_NodeForScore.getInstance()");
                    //if ( Data_NodeIDsTo_NodeForScore.getInstance().getNodeForScore(iCurNode, null, false) != null
                    //        && !hsNodesDone.contains ( new Integer ( iCurNode ) ) )
                    //    break;
                    // else node not found try next one in case a skip nodeid

                }
                catch ( Throwable e )
                {
                    Log.FatalError("fatal error in getNextNode_test", e);
                    break;
                }
            }
            hsNodesDone.add ( new Integer ( iCurNode ));
        }
        //System.out.println("getNextNode_test delivering node [" + iCurNode + "]");
        return iCurNode;
    }

    public void run() {
        try {
            while (bStopLooping == false) {
                //    while ( com.indraweb.util.UtilFile.bFileExists( "c:/temp/stopp.hk") )
                //        com.indraweb.util.clsUtils.pause_this_many_milliseconds(1000);
                try {
                    Connection dbc = com.iw.nodefeed.helper.DBConnectionJX.getConnection(sDB_OracleJDBC,
                            sDB_user, sDB_pass);
                    WorkItemNodeToCrawl workItem = null;
                    try {
                        workItem =
                            //new WorkItemNodeToCrawl(getNextNode());  // hbk control uncomment for prod 2003 02 12
                            //new WorkItemNodeToCrawl( getNextNode_test() );
                            new WorkItemNodeToCrawl( getNextNode(sDesc, dbc) );
                    } finally {
                        com.iw.nodefeed.helper.DBConnectionJX.freeConnection(dbc, sDB_OracleJDBC);
                    }

                    workQ.addWorkItem(workItem, sDesc);
                    iCountProducedOrConsumed++;
                    System.out.println("P. [" + sDesc + "] produced # [" + iCountProducedOrConsumed + "] node [" + workItem.getNodeID() + "]" + workQ.getQsize());
                } catch (Exception e) {
                    System.out.println("error 3 in 'ThreadProducer_MSGetNextNode.run()'" + e.getMessage());
                    e.printStackTrace();
                }
            }
            System.out.println("done producer thread [" + sDesc + "] bStopLooping [" + bStopLooping + "]");
        } catch (Exception e) {
            System.out.println("error 4 in 'ThreadProducer_MSGetNextNode.run()'" + e.getMessage());
        } finally {
            System.out.println("Exiting ThreadProducer_MSGetNextNode added this thread [" + sDesc + "]");
        }
    }

    public static int getNextNode (
            String sMachineThreadInfo,
            Connection dbc
    ) throws Exception {
/*
CREATE OR REPLACE PROCEDURE GetNextNode (
        nodeid_out OUT Node.NodeId%TYPE,
        nodetitle_out OUT Node.NodeTitle%TYPE,
        nodesize_out OUT Node.NodeSize%TYPE,
        MachineNameIn IN VARCHAR2,
        corpusid_in IN NUMBER)
*/


/*
        int i = 1;
        if ( i == 1 ) // true hbk control 2003 03 04 to run a single node
        {
            System.out.println ( "returning fixed Node from getNextNode " );
            //return 1087258;
            return 1087265  ;
        }
*/




        long lStartGetNext = System.currentTimeMillis();
        Statement stmt = null;
        CallableStatement cstmt = null;
        int iNodeID = -1;
        try {
            cstmt = dbc.prepareCall("begin GetNextNode (:1, :2, :3, :4, :5) ; end;");
            cstmt.registerOutParameter(1, Types.NUMERIC);    // NUMERIC is preferred over INTEGER with some databases
            cstmt.registerOutParameter(2, Types.VARCHAR);
            cstmt.registerOutParameter(3, Types.NUMERIC);
            cstmt.setString(4, sMachineThreadInfo);
            cstmt.setInt(5, -1);  // dummy - not used anymore
            long lTimeStart = System.currentTimeMillis();
            //System.out.println("entering getNextNode\r\n");
            cstmt.executeUpdate();
            iNodeID = (int) cstmt.getLong(1);

            String sNodeTitle_ThisNoAncestry_noDelimiters = cstmt.getString(2);
            int iNodeSize = (int) cstmt.getLong(3);
            //System.out.println("time in getNextNode (sec) [" + UtilProfiling.elapsedTimeSeconds(lTimeStart) +
              //      "] nodeid [" + iNodeID +
                //    "] title [" + sNodeTitle_ThisNoAncestry_noDelimiters +
                  //  "] nodesize [" + iNodeSize + "]");
        } catch (Exception e) {
            String sStackTrace = Log.stackTraceToString(e);
            if ( sStackTrace.indexOf("ORA-01403: no data found") > 0 ) {
                return 0 ; // no data
            } else {
                e.printStackTrace();
                return -1 ; // error condition
            }
        } finally {
            cstmt.close();
            long lDur = System.currentTimeMillis() - lStartGetNext;
            if ( lDur > 1500 )
                System.out.println(">>> long getNextNode (> 1500) ms [" + lStartGetNext + "]");
        }
        return iNodeID;
    }  // getNextNode


}
