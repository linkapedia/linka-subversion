import java.util.*;
import java.io.*;
import java.net.*;
import java.util.zip.*;
import java.sql.*;

import java.util.Iterator;
import oracle.jdbc.driver.OracleConnection;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ExecutionContext;

// definition of an iterator object to retrieve document identifiers when appropriate
#sql iterator getDocID_
   (String DocumentID);

public class Synchronize {
	
   public static void Synchronize (String ZipPath)
	   throws Exception { 
/* 
	Synchronize

	Synchronize is a java stored procedure invoked through oracle.   Given a directory,
	this procedure will retrieve all zip files, read from a stream and execute SQL that 
	has been specifically preprocessed for this program.  
	   
	This procedure is specifically designed to enhance the overall speed of bulk classification
	by taking database inserts and deletes off-line. 
	   
	The ZIP files are zipped partial SQL created by ITSAPI classifiers as output from 
	batch mode classification runs.   It is FTP'd to the target directory automatically.

	Input arguments:       
	   ZipPath (string)		-	The user must provide the location where the SQL zip files are stored.

	Synchronize work flow:

    1.  Look inside the ZipPath directory and build a vector of pathnames to each zip file.
    2.  Loop through each zip file, read the input stream and execute the DELETE statements ONLY.
	3.	Drop all indicies on the NodeDocument table - step 4 involves inserting potentially millions of
	    rows into the NodeDocument table so these indicies are removed for inefficiencies.
	4.  Loop through each zip file once again, this time executing all SQL, either inserts or updates.
	5.  Rebuild the indicies on NodeDocument, and recompute statistics to find optimal execution paths.
	6.	Finally, delete all zip files that have been processed.

*/

	// define the execution context and the database connection object, all inherent to this procedure
	ExecutionContext ec = DefaultContext.getDefaultContext().getExecutionContext();
    Connection conn = DefaultContext.getDefaultContext().getConnection();

	// truncate the log table each time this procedure is run
	#sql [ec] { delete from FCRAWL_LOG };
	#sql [ec] { commit };	

	// grant permission to each zip file, this is necessary for sqlj to open these files later
	Statement stmt = null; ResultSet rs = null;
		 
	try {
	    stmt = conn.createStatement();
		rs = stmt.executeQuery("call dbms_java.grant_permission('PUBLIC','java.io.FilePermission', '"+ZipPath+"', 'write,delete')");
	} catch (Exception e) {
		#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Could not grant permission to '||:ZipPath) };
	    #sql [ec] { commit };
		return;
	} finally {
		rs.close(); stmt.close(); rs = null; stmt = null;
	}

	// Step 1: Look in the ZIPPATH directory and fill VECTOR with ZIP FILE PATHS
	Vector vZipFiles = GetZipPaths(ZipPath);
	if (vZipFiles.size() < 1) { // return if no zip files found
		#sql [ec] { insert into FCRAWL_LOG (LOG) values ('No zip files found in directory: '||:ZipPath) };
	    #sql [ec] { commit };

		return;	
	}

	// Document maps from URL to ID are stored in this hashtable.  
	Hashtable htDocuments = new Hashtable();	
	int iCounter = 0;

	long lStartDelete = System.currentTimeMillis();

	// Step 2: Loop through each file, read the contents and run only the delete statements
	try {
	
	Enumeration eV = vZipFiles.elements();
	while (eV.hasMoreElements()) {
		String sPath = (String) eV.nextElement();
		File file = new File(sPath);	

		#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Attempting to open: '||:sPath) };
		#sql [ec] { commit };
		
		if (file.exists()) {
			FileInputStream fis = new FileInputStream(file);
			ZipInputStream zin = new ZipInputStream(fis);
			ZipEntry ze = zin.getNextEntry();

			BufferedReader buf = new BufferedReader(new InputStreamReader(zin));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				// process hacked sql statements, but only deletes.   example:
				// NDDEL   \\207.103.213.109\c drive\trec\Fr94\03\FR940318_0\FR940318-0-00013.HTM  delete from nodedocument where documentid = #IW_DOCID# and nodeid in ( select nodeid from node where corpusid = 5)
				Vector v = Split(sData, "\t"); 
				String sOperation = (String) v.elementAt(0);
				if (v.size() < 3) {
					#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Extraneous line found: '||:sData) };	
				} else if (sOperation.equals("NDDEL")) { 
					// note: in this stage we only process delete statements (NDDEL operation)
					String sDocumentID = ""; String sDocURL = (String) v.elementAt(1);
					String sSQL = (String) v.elementAt(2); 

					// retrieve the document identifier from the database - it wouldn't exist 
					// yet in the htDocuments has table
					getDocID_ getDocID;
					#sql getDocID = { select DocumentID from Document where DocURL = :sDocURL }; 
					
					if (getDocID.next()) { 
						sDocumentID = getDocID.DocumentID(); 
						htDocuments.put(sDocURL, sDocumentID); // add to hashtable for future reference
						sSQL = StringReplace(sSQL, "#IW_DOCID#", sDocumentID); // replace variables
						iCounter++; // increment counter
    					
					    // create a JDBCStatement object to execute a dynamic query
						try {
							stmt = conn.createStatement(); rs = stmt.executeQuery(sSQL);
						} catch (Exception e) {
							String error = e.getMessage()+" "+sSQL+" line: 131";;
							#sql [ec] { insert into FCRAWL_LOG (LOG) values (:error) };
							#sql [ec] { commit };		
						} finally {
							if (rs != null) { rs.close(); rs = null; }
							if (stmt != null) { stmt.close(); stmt = null; }
						}
						
						// commit our transactions every 100 statements
						if ((iCounter % 100) == 0) { 
							long lTEnd = System.currentTimeMillis() - lStartDelete;
							#sql [ec] { insert into FCRAWL_LOG (LOG) values (:iCounter||' statements executed in '||:lTEnd||' ms.') };
							#sql [ec] { commit };
						}
					}					
					getDocID.close(); // make sure to close statements or cursors will be exhausted
				}
			}
			fis.close();
			zin.close();
		} else {
			// put a warning in the log file if the file could not be found.  why would this ever happen?
			#sql [ec] { insert into FCRAWL_LOG (LOG) values ('File does not exist: '||:sPath) };
		    #sql [ec] { commit };
		}
    }
	
	} catch (Exception e) { // if error reading log and die
		ByteArrayOutputStream bs = new ByteArrayOutputStream(); 
	    e.printStackTrace(new PrintStream(bs));
		String error = bs.toString(); 
		#sql [ec] { insert into FCRAWL_LOG (LOG) values (:error) };
		#sql [ec] { commit };
		return;
	}
	
	long lEndDelete = System.currentTimeMillis() - lStartDelete;

    #sql [ec] { insert into FCRAWL_LOG (LOG) values (:iCounter||' deleted in '||:lEndDelete||' ms.') };
	#sql [ec] { commit };

	// Step 3: Drop the indicies on the NodeDocument table
	// drop index DOCONNODEDOC;
	// drop index NOTIFICATION;
	// drop index NODEDOCUMENTNODEID;
	// alter table NODEDOCUMENT drop PRIMARY KEY CASCADE;

	long lStartdrop = System.currentTimeMillis();
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Dropping indicies from NODEDOCUMENT table.') };
	#sql [ec] { commit };

	#sql [ec] { drop index DOCONNODEDOC };
	#sql [ec] { drop index NOTIFICATION };
	#sql [ec] { drop index NODEDOCUMENTNODEID };
	#sql [ec] { alter table NODEDOCUMENT drop PRIMARY KEY CASCADE };
		 
	long lEnddrop = System.currentTimeMillis() - lStartdrop;

	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Indicies dropped in '||:lEnddrop||' ms.') };
	#sql [ec] { commit };	

	long lStart = System.currentTimeMillis();
	   
	// Step 4: Walk through the rest of the SQL statements and execute them
	Enumeration eV2 = vZipFiles.elements();

	try {
	while (eV2.hasMoreElements()) { 
		String sPath = (String) eV2.nextElement();
		File file = new File(sPath); 

		if (file.exists()) { 
			FileInputStream fis = new FileInputStream(file); 
			ZipInputStream zin = new ZipInputStream(fis);
			ZipEntry ze = zin.getNextEntry(); 

			BufferedReader buf = new BufferedReader(new InputStreamReader(zin)); 
			String sData = new String(); 
			
			while ((sData = buf.readLine()) != null) { 
				// process more hacked sql statement, examples:
				// DOCUPD  \\207.103.213.109\c drive\trec\Fr94\03\FR940318_0\FR940318-0-00013.HTM  update document set DOCTITLE =  'Title not set', GENREID =  '121', DATELASTFOUND = sysdate, REPOSITORYID = 8 where  documentid  = #IW_DOCID#
				// DOCINS  \\207.103.213.109\c drive\trec\Fr94\03\FR940318_0\FR940318-0-00013.HTM  insert into document (DOCUMENTID,GENREID,REPOSITORYID,DOCTITLE,DOCURL,DATELASTFOUND) values (#IW_DOCID#,121,8,'Title not set','\\207.103.213.109\c drive\trec\Fr94\03\FR940318_0\FR940318-0-00013.HTM',sysdate)
				// NDINS   \\207.103.213.109\c drive\trec\Fr94\03\FR940318_0\FR940318-0-00013.HTM  insert into NODEDOCUMENT  (NODEID,DOCUMENTID,DOCUMENTTYPE,SCORE1,SCORE2,SCORE3,SCORE4,SCORE5,SCORE6,DOCSUMMARY,DATESCORED) values (23373,#IW_DOCID#,1,50.0,0.0,0.3510545679220378,1.1155734047300312,0.15384615384615385,0.22818546914932455,'Cary ',sysdate)
				Vector v = Split(sData, "\t"); 
	   
				String sOperation = (String) v.elementAt(0);
				if (v.size() < 3) { 
					#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Extraneous line found: '||:sData) };
				}  
				if ((!sOperation.equals("NDDEL")) && (v.size() > 2)) {
					String sDocumentID = ""; String sDocURL = (String) v.elementAt(1); 
					String sSQL = (String) v.elementAt(2); 
				
					// we act a bit differently depending upon the executing statement.
					// -- if it's a DOCINS but the documentid already exists in the hash table, ignore it
			 	    if ((sOperation.equals("DOCINS")) && (htDocuments.containsKey(sDocURL))) {} 
					else { 
						// try to get document identifier from hash table, if possible
						if (htDocuments.containsKey(sDocURL)) { sDocumentID = (String) htDocuments.get(sDocURL); }
					    else { // otherwise select it out of the database
							getDocID_ getDocID; 
							#sql getDocID = { select DocumentID from Document where DocURL = :sDocURL }; 
						    if (getDocID.next()) { sDocumentID = getDocID.DocumentID(); }
						    if (!sDocumentID.equals("")) { htDocuments.put(sDocURL, sDocumentID); }
							getDocID.close();
						}
	   
						// okay, if we still do not have a DOCID and this is a DOCINS, get it from DUAL
						if ((!htDocuments.containsKey(sDocURL)) && (sOperation.equals("DOCINS"))) { 
							getDocID_ getDocFromDual; 
							#sql getDocFromDual = { select document_seq.nextval DocumentID from dual }; 
							getDocFromDual.next(); sDocumentID = getDocFromDual.DocumentID();
							htDocuments.put(sDocURL, sDocumentID);
							getDocFromDual.close();
						}

						// if we have a document ID, replace and insert SQL.. if we do NOT, then 
						// some unexpected error has occured.
					    if (!sDocumentID.equals("")) { 
							sSQL = StringReplace(sSQL, "#IW_DOCID#", sDocumentID); iCounter++;
    
						    try {
								// create a JDBCStatement object to execute a dynamic query
								stmt = conn.createStatement(); 
								rs = stmt.executeQuery(sSQL); 
						    } catch (Exception e) {
								String error = e.getMessage()+" "+sSQL+" line: 257";
								#sql [ec] { insert into FCRAWL_LOG (LOG) values (:error||' (sql: '||:sSQL||')') };
								#sql [ec] { commit };
							} finally { 
							   if (rs != null) { rs.close(); rs = null; }
							   if (stmt != null) { stmt.close(); stmt = null; }
							}

							// commit after every 100 transactions
							if ((iCounter % 100) == 0) {  
								long lTEnd = System.currentTimeMillis() - lStart; 
								#sql [ec] { insert into FCRAWL_LOG (LOG) values (:iCounter||' statements executed in '||:lTEnd||' ms.') };
								#sql [ec] { commit };
							}
					     } else {
							#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Aborted: '||:sSQL) }; 
						 }
					}
				}
			}
		fis.close(); 
		zin.close(); 
		}
	}
	} catch (Exception e) { // if error reading log and die
		ByteArrayOutputStream bs = new ByteArrayOutputStream(); 
	    e.printStackTrace(new PrintStream(bs));
	   String error = "Fatal Error "+bs.toString()+" Counter: "+iCounter; 
		#sql [ec] { insert into FCRAWL_LOG (LOG) values (:error) };
		#sql [ec] { commit };
		return;
	}
	long lEnd = System.currentTimeMillis() - lStart;

    #sql [ec] { insert into FCRAWL_LOG (LOG) values (:iCounter||' statements executed in '||:lEnd||' ms.') };
    #sql [ec] { commit };
	   
    // Step 5: Rebuild the indicies using the sqlj stored procedure RebuildIndexes ..
	long lStartadd = System.currentTimeMillis();
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Recreating indicies from NODEDOCUMENT table.') };
    #sql [ec] { commit };

	#sql [ec] { alter table NODEDOCUMENT add PRIMARY KEY (NODEID,DOCUMENTID) };
		 
	long lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();

	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Primary key index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

	#sql [ec] { create index DOCONNODEDOC on NODEDOCUMENT (DOCUMENTID) TABLESPACE DDATA };

	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();

	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('DOCONNODEDOC index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };
		 
	#sql [ec] { create index NODEDOCUMENTNODEID on NODEDOCUMENT (NODEID) TABLESPACE DDATA };

	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();

	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('NODEDOCUMENTNODEID index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

	#sql [ec] { create index NOTIFICATION on NODEDOCUMENT (DATESCORED) TABLESPACE DDATA };
		 
	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();
	
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('NOTIFICATION index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

    try {
		stmt = conn.createStatement(); 
		rs = stmt.executeQuery("analyze table document estimate statistics"); 
    } catch (Exception e) {
		String error = e.getMessage()+" line: 336";
		#sql [ec] { insert into FCRAWL_LOG (LOG) values (:error) };
		#sql [ec] { commit };
	} finally { 
	   if (rs != null) { rs.close(); rs = null; }
	   if (stmt != null) { stmt.close(); stmt = null; }
	}
		 
	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();
	
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Estimated statistics on document table in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

    try {
		stmt = conn.createStatement(); 
		rs = stmt.executeQuery("analyze table nodedocument estimate statistics"); 
    } catch (Exception e) {
		String error = e.getMessage()+" line: 354";
		#sql [ec] { insert into FCRAWL_LOG (LOG) values (:error) };
		#sql [ec] { commit };
	} finally { 
	   if (rs != null) { rs.close(); rs = null; }
	   if (stmt != null) { stmt.close(); stmt = null; }
	}

	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();
	
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Estimated statistics on node document table in '||:lEndadd||' ms.') };
    #sql [ec] { commit };
	   
    // Step 6: Finally! Remove files that were processed

	Enumeration eV3 = vZipFiles.elements();

	while (eV3.hasMoreElements()) {
		String sPath = (String) eV2.nextElement();
		File file = new File(sPath);	

	   if (file.exists()) { file.delete(); }
   }
  }

	// StringReplace: given a string, replace REPLACE with WITH globally throughout
	public static String StringReplace (String s, String replace, String with) {
		int dontReplaceInsideReplacedStringCounter = 0;
		try  {
			while (true) 
			{
				// don't recurse infinitely
				String testStr = s.substring( dontReplaceInsideReplacedStringCounter ) ;
				int replaceableLoc = testStr.indexOf ( replace );
				if ( replaceableLoc >= 0 ) 	
				{	
					String x1 = s.substring ( 0, replaceableLoc + dontReplaceInsideReplacedStringCounter );
					String x2 = s.substring ( dontReplaceInsideReplacedStringCounter + replaceableLoc + replace.length());
					s =  x1 + with + x2;
					dontReplaceInsideReplacedStringCounter += (replaceableLoc + with.length()  );
				} 
				else { return s; } 
			} 
		} 
		catch ( Exception e ) { return null; } 
	} 

	// As per the PERL function, split a string into a vector of strings given a delimiter
	public static Vector Split (String s, String splitterDelimiters ) {
		Vector v = new Vector();
		StringTokenizer st = new StringTokenizer ( s, splitterDelimiters );
		
		String ss = null;
		while ( st.hasMoreElements() )
		{
			ss = (String) st.nextElement();	
			v.addElement( ss );	
		} 
		return v;
	}

	// Look in the zip directory given and build a vector of strings containing absolute paths to zip files
	public static Vector GetZipPaths(String sLocation) throws Exception {
		// initialize return vector
		Vector v = new Vector();
		Connection conn = DefaultContext.getDefaultContext().getConnection();
		ExecutionContext ec = DefaultContext.getDefaultContext().getExecutionContext();
		
		// initialize directory information
		File fDirectory = new File(sLocation);
		ZipFileFilter zff = new ZipFileFilter();
		String FilePaths[] = fDirectory.list(zff);

		// loop through each file in this directory
		if (FilePaths != null) {
			for (int i=0; i<FilePaths.length; i++) {
				File f = new File(sLocation+"\\"+FilePaths[i]);
				String path = f.getAbsolutePath();
				if (f.exists()) { v.addElement(path); }

				try {
				    Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("call dbms_java.grant_permission('PUBLIC','java.io.FilePermission', '"+path+"', 'write,delete')");
					rs.close(); stmt.close();
				} catch (Exception e) {
					#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Could not grant permission to '||:path) };
				    #sql [ec] { commit };
				}
			}
		}
			
		return v;
	}
	// simple file filter to look for zip files only in a given directory
	public static class ZipFileFilter implements FilenameFilter {
	 public boolean accept (File dir, String name) {
	     if (name.toLowerCase().endsWith ("zip")) return true;
	     else return false;
	 }}

}
