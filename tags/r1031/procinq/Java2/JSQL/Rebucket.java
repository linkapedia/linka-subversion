import java.util.*;
import java.util.Iterator;
import oracle.jdbc.driver.OracleConnection;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ExecutionContext;

#sql iterator getROC_
   (double ROCF1, double ROCF2, double ROCF3, 
    double ROCC1, double ROCC2, double ROCC3);
#sql iterator getScores_
   (int nodeid, int documentid, double score5, double score6);

public class Rebucket {
   public static void Rebucket(int iCorpusID) throws Exception { 
	/* 
	Rebucketing: alter SCORE1 for each ROW in the NODEDOCUMENT table given a CORPUSID
	  1) select rocc1, rocc2, rocc3, rocf1, rocf2, rocf3 from corpus 
	     where corpusid = ##CORPUSID##
	  2) select score5, score6, nodeid, documentid from nodedocument where nodeid in 
	    (select nodeid from node where corpusid = ##CORPUSID##)
	  3) do the calculations ..
	  4) for each row: update score1 set score1 = XXX where nodeid = ##NID## and corpusid = ##CID##
	  5) commit work;
	*/

	ExecutionContext ec = DefaultContext.getDefaultContext().getExecutionContext();

	// 1) select rocc1, rocc2, rocc3, rocf1, rocf2, rocf3 from corpus 
	// where corpusid = ##CORPUSID##
	double dROCF1 = 0.0; double dROCF2 = 0.0; double dROCF3 = 0.0;
	double dROCC1 = 0.0; double dROCC2 = 0.0; double dROCC3 = 0.0;

	getROC_ getROC;

	#sql getROC = { select ROCF1, ROCF2, ROCF3, ROCC1, ROCC2, ROCC3 from Corpus 
	                  where corpusid = :iCorpusID };

	if (getROC.next()) {
	   dROCF1 = getROC.ROCF1(); dROCF2 = getROC.ROCF2(); dROCF3 = getROC.ROCF3();
	   dROCC1 = getROC.ROCC1(); dROCC2 = getROC.ROCC2(); dROCC3 = getROC.ROCC3();
	};
	getROC.close();

	// 2) select score5, score6, nodeid, documentid from nodedocument where nodeid in 
	//    (select nodeid from node where corpusid = ##CORPUSID##)		

	getScores_ getScores;

	#sql getScores = { select nodeid, documentid, score5, score6 from NodeDocument 
		                  where nodeid in (select nodeid from node where corpusid = :iCorpusID) };

	while (getScores.next()) {
	   int iNodeID = getScores.nodeid();
	   int iDocID = getScores.documentid();
           double c = getScores.score5();
	   double f = getScores.score6();

	   // 3) do the calculations ..
	   double dScore = 0.0;

	   if (dROCC3 > 10000) {
		if (dROCC3 == 10100) { // frequency
		   if (f > dROCF1) { dScore = 50; }
		   if (f > dROCF2) { dScore = 75; }
		   if (f > dROCF3) { dScore = 100; }
		}
		else if (dROCC3 == 10101) { // coverage
		   if (c > dROCF1) { dScore = 50; }
		   if (c > dROCF2) { dScore = 75; }
		   if (c > dROCF3) { dScore = 100; }
		}
		else { // frequency * coverage
		   if ((f*c) > dROCF1) { dScore = 50; }
		   if ((f*c) > dROCF2) { dScore = 75; }
		   if ((f*c) > dROCF3) { dScore = 100; }
		}
	   } else {
		if ( f > dROCF3 && c > dROCC2 ) { dScore = 100; }
		else if ( f > dROCF2 && f <= dROCF3 && c > dROCC1 && c <= dROCC2 ) { dScore = 75; } 
		else if ( f > dROCF1 && f <= dROCF2 && c > dROCC2 ) { dScore = 50; } 
		else if ( f > 0 && f <= dROCF2 && c > dROCC1 && c <= dROCC2 ) { dScore = 50; } 
		else if ( f > dROCF3 && c > dROCC1 && c <= dROCC2 ) { dScore = 75; } 
		else if ( f > dROCF2 && f <= dROCF3 && c > dROCC2 ) { dScore = 75; } 
		else if ( f > 0	&& f <= dROCF1 && c > dROCC2 && c <= dROCC3 ) { dScore = 0; } 
		else if ( f > 0	&& f <= dROCF1 && c > dROCC3 ) { dScore = 50; } 
           }

 	   // 4) for each row: update score1 set score1 = XXX where nodeid = ##NID## and corpusid = ##CID##
		
	   #sql [ec] { update NodeDocument set Score1 = :dScore where 
		       NodeID = :iNodeID and DocumentID = :iDocID };

	   #sql [ec] { commit };
	}

	getScores.close();
   }
}