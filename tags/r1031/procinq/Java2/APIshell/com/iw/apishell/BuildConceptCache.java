package com.iw.apishell;

import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

public class BuildConceptCache {
	public static void main(String args[]) {
        if (args.length < 6) { System.out.println("Usage: BuildConceptCache.jar -username user -password pass -api api"); return; }

        String SKEY = "";

        String API = "127.0.0.1";
        String Username = "mpuscar";
        String Password = "racer9";

        if (args.length > 1) { Username = (String) args[1]; }
        if (args.length > 3) { Password = (String) args[3]; }
        if (args.length > 5) { API = (String) args[5]; }

        API = "http://"+API+"/";

        ITS its = new ITS();
        its.SetAPI(API);

		try {
            // LOGIN routine takes a USERID and a PASsWORD
            User u = its.Login(Username, Password);

			// Login successful.  Get the session key and put into args for future arguments
			SKEY = u.getSessionKey();
            its.SetSessionKey(SKEY);

			System.out.println("Login successful.  Your session key is "+SKEY);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("Error: Login failed."); return;
        }

        try {
            long lStart = System.currentTimeMillis();
            if (!its.rebuildConceptCache()) { throw new Exception("Concept cache rebuild failed."); }
            long lEnd = System.currentTimeMillis() - lStart;

            System.out.println("Concept cache rebuild completed in "+lEnd+" ms.");
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("Error: Index rebuild failed."); return;
        }
    }
}
