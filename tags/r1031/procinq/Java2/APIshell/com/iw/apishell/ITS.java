package com.iw.apishell;

import java.util.*;
import java.io.*;
import java.net.URLEncoder;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

public class ITS {
    private String SKEY = "NONE";
    private String API = "http://woti.tzo.com:8888/itsapi/ts?fn=";

    public ITS () {}
	public ITS (String SessionKey, String API) {
		if (SessionKey != null) { this.SKEY = SessionKey; }
        if (API != null) { this.API = API+"itsapi/ts?fn="; }
	}

    // Accessor functions
    public void SetSessionKey (String SessionKey) { if (SessionKey != null) { this.SKEY = SessionKey; }}
    public void SetAPI (String API) { if (API != null) { this.API = API+"itsapi/ts?fn="; }}
    public String getSessionKey () { return SKEY; }
    public String getAPI() { return API; }

    public HashTree getArguments() {
        HashTree ht = new HashTree();
        ht.put("api", API);
        ht.put("SKEY", SKEY);

        return ht;
    }

    // ITS Login
    // Arguments: Username, Password
    // Returns: User object
    public User Login (String Username, String Password) throws Exception {
        // LOGIN routine takes a USERID and a PASsWORD
		Hashtable htArgs = new Hashtable();
		htArgs.put("UserID", Username); // example: "sn=cifaadmin,ou=users,dc=cifanet";
		htArgs.put("Password", Password); // example: racer9
        htArgs.put("api", API);

        try {
            // Invoke the ITSAPI now.
            InvokeAPI API = new InvokeAPI ("security.TSLogin", htArgs);
            HashTree htResults = API.Execute(false, false);

            // If there is no "subscriber" tag, an error has occured
            if (!htResults.containsKey("SUBSCRIBER")) {
                throw new Exception("You have specified an invalid username, password combination.");
            }

            // Get user hash tree
            HashTree htUser = (HashTree) htResults.get("SUBSCRIBER");

            if (!htUser.containsKey("KEY")) {
                throw new Exception("You have specified an invalid username, password combination.");
            }

            return new User(htUser);
        } catch (Exception e) { throw e; }
    }

    // Rebuild full text indexes
    public boolean rebuildIndexes (boolean bBuild) throws Exception {
        //System.out.println("mode 1");
        try {
            HashTree htArguments = getArguments();
            if (bBuild) htArguments.put("Build", "true");
            InvokeAPI API = new InvokeAPI ("tsother.TSRebuildIndexes", htArguments);
            HashTree htResults = API.Execute(false, false);
        } catch (Exception e) { e.printStackTrace(System.out); return false; }

        return true;
    }

    // Rebuild a specific full text index
    public boolean rebuildIndexes (String Index) throws Exception {
        //System.out.println("mode 2");
        try {
            HashTree htArguments = getArguments();
            htArguments.put("Index", Index);
            InvokeAPI API = new InvokeAPI ("tsother.TSRebuildIndexes", htArguments);
            HashTree htResults = API.Execute(false, false);
        } catch (Exception e) { e.printStackTrace(System.out); return false; }

        return true;
    }

    // Build a specific full text index from scratch
    public boolean rebuildIndexes (String Index, String Table, String Field) throws Exception {
        //System.out.println("mode 3");
        try {
            HashTree htArguments = getArguments();
            htArguments.put("Index", Index);
            htArguments.put("Table", Table);
            htArguments.put("Field", Field);
            htArguments.put("Build", "true");
            InvokeAPI API = new InvokeAPI ("tsother.TSRebuildIndexes", htArguments);
            HashTree htResults = API.Execute(false, false);
        } catch (Exception e) { e.printStackTrace(System.out); return false; }

        return true;
    }

    public boolean rebuildConceptCache() throws Exception {
        try {
            HashTree htArguments = getArguments();
            InvokeAPI API = new InvokeAPI ("tsother.TSRebuildConceptCache", htArguments);
            HashTree htResults = API.Execute(false, false);
        } catch (Exception e) { e.printStackTrace(System.out); return false; }

        return true;
    }

    public void synchronizeBatchRun() throws Exception { synchronizeBatchRun("RDATA"); }
    public void synchronizeBatchRun (String Tablespace)
    throws Exception {
 		Hashtable htArgs = getArguments();
		htArgs.put("Tablespace", Tablespace);

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSLoadBatchRun", htArgs);
            HashTree htResults = API.Execute(false, false);
        } catch (Exception e) { throw e; }
    }

    // GetArgHash takes command line arguments and parses them into a Hash structure
	public static HashTree getArgHash ( String[] args) {
		HashTree htHash = new HashTree();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}

}
