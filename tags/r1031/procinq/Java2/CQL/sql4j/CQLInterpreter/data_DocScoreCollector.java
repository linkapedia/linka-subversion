/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 28, 2002
 * Time: 10:55:29 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import java.util.Vector;
import java.util.Hashtable;

public class data_DocScoreCollector {

    public Vector vdataDetailRecord_DocScores= new Vector();
    public Hashtable htdataDetailRecord_DocScores= new Hashtable();

    void add
             (
            int iDOCUMENTID,
            int iNodeID,
            int iGENREID,
            String   sDOCTITLE,
            String   sDOCURL,
            String   sDOCUMENTSUMMARY,
            double dIWScore1,
            double dIWScore5,
            double dIWScore6,
            double dORacleScore,
            int iSourceMode
            )
            throws Exception
    {
        dataDetailRecord_DocScore dataDetailRecord_DocScore_new = new dataDetailRecord_DocScore
           (
                iDOCUMENTID,
                   iNodeID,
                iGENREID,
                sDOCTITLE,
                sDOCURL,
                sDOCUMENTSUMMARY,
                dIWScore1,
                dIWScore5,
                dIWScore6,
                dORacleScore,
                (dIWScore5 * dIWScore6),
                   iSourceMode
           );
        htdataDetailRecord_DocScores.put ( ""+iDOCUMENTID+ ":"+iNodeID, dataDetailRecord_DocScore_new );
        vdataDetailRecord_DocScores.addElement(dataDetailRecord_DocScore_new );

    }

}
