/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 28, 2002
 * Time: 6:15:37 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import com.indraweb.execution.Session;
import com.indraweb.util.sort.UtilMergeSort;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class CQLXMLGenMode2 {

    public static void genCombinedOutput(
            CQLCombinedDataFT cds,
            int iStartRow_1_based,
            int iRowMax,
            PrintWriter out,
            Connection dbc,
            int iNumTopRecs,
            boolean bVerbose)
            throws Exception {
        int iMaxRecordCount = Session.cfg.getPropInt("MaxRecordCount");
        if (iRowMax > iMaxRecordCount) {
            iMaxRecordCount = iRowMax;
        } // added by MAP 2/21/03 to supercede hard limits

        // *****************************
        // FIRST POPULATE IW DATA
        // *****************************
        if (cds.data_DocScoreCollectorIWPiece == null) {
            cds.populateData_IW(iStartRow_1_based, iRowMax, out, dbc, bVerbose);
        } else {
            throw new Exception("unknown state 1");
        }
        // *****************************
        // THEN COLLECT ORACLE (FT) DATA
        // *****************************
        if (cds.data_DocScoreCollector_FT == null) {
            if (!cds.bMode_InCombinedFulltextNull) {
                cds.populateData_Oracle(iStartRow_1_based, iRowMax, out, dbc, bVerbose);
            }
        } else {
            throw new Exception("unknown state 2");
        }

        data_DocScoreCollector Dcdscombined = new data_DocScoreCollector();
        Hashtable htdataDetailRecord_DocScore_Oracle = null;
        if (!cds.bMode_InCombinedFulltextNull) {
            htdataDetailRecord_DocScore_Oracle = cds.data_DocScoreCollectorIWPiece.htdataDetailRecord_DocScores;
        }
        // GO THRU IW-PRESENT RECORDS - SEE IF IN ORACLE
        Vector vNewMerged_dataDetailRecord_DocScore = new Vector();
        Vector vTopic_dataDetailRecord_DocScore = cds.data_DocScoreCollectorIWPiece.vdataDetailRecord_DocScores;
        //com.indraweb.util.Log.logClear("vTopic_dataDetailRecord_DocScore.size():"+ vTopic_dataDetailRecord_DocScore.size()+ "\r\n");
        Enumeration e1TopicHits = vTopic_dataDetailRecord_DocScore.elements();

        while (e1TopicHits.hasMoreElements()) {
            dataDetailRecord_DocScore ddrds_IW = (dataDetailRecord_DocScore) e1TopicHits.nextElement();
            int iDocID = ddrds_IW.iDOCUMENTID;
            dataDetailRecord_DocScore ddrds_ORACLE = null;
            if (!cds.bMode_InCombinedFulltextNull) {
                ddrds_ORACLE =
                        (dataDetailRecord_DocScore) htdataDetailRecord_DocScore_Oracle.get(iDocID + ":-1");
            }
            /////////
            /////////  1 IF IN IW/CQL ONLY
            /////////
            // this will always be true in
            if (ddrds_ORACLE == null) // not in oracle - in IW only
            {
                vNewMerged_dataDetailRecord_DocScore.addElement(
                        new dataDetailRecord_DocScore(
                        ddrds_IW.iDOCUMENTID,
                        ddrds_IW.iNodeID,
                        ddrds_IW.iGENREID,
                        ddrds_IW.sDOCTITLE,
                        ddrds_IW.sDOCURL,
                        ddrds_IW.sDOCUMENTSUMMARY,
                        ddrds_IW.dIWScore1,
                        ddrds_IW.dIWScore5,
                        ddrds_IW.dIWScore6,
                        ddrds_IW.dORacleScore,
                        ddrds_IW.dIWScore6 * ddrds_IW.dIWScore5 * cds.dScoreFloorFTScore,
                        dataDetailRecord_DocScore.iSOURCEMODE_IW_ONLY));
                //   com.indraweb.util.Log.logClear("source  doc [" + ddrds_IW.iDOCUMENTID + "] not in oracle - in IW only\r\n");

            } /////////
            /////////  2 IF IN IW/CQL AND FT
            /////////
            else {
                double dIWScoreAggMinToUse = ddrds_IW.dIWScore6 * ddrds_IW.dIWScore5;
                if (dIWScoreAggMinToUse < cds.dScoreFloorFtimesC) {
                    dIWScoreAggMinToUse = cds.dScoreFloorFtimesC;
                }
                //dIWScoreAggMin = 0.1;

                vNewMerged_dataDetailRecord_DocScore.addElement(
                        new dataDetailRecord_DocScore(
                        ddrds_IW.iDOCUMENTID,
                        ddrds_IW.iNodeID,
                        ddrds_IW.iGENREID,
                        ddrds_IW.sDOCTITLE,
                        ddrds_IW.sDOCURL,
                        ddrds_IW.sDOCUMENTSUMMARY,
                        ddrds_IW.dIWScore1,
                        ddrds_IW.dIWScore5,
                        ddrds_IW.dIWScore6,
                        ddrds_ORACLE.dORacleScore,
                        dIWScoreAggMinToUse * ddrds_ORACLE.dORacleScore,
                        dataDetailRecord_DocScore.iSOURCEMODE_IW_AND_ORACLE));
                //   com.indraweb.util.Log.logClear("source  doc [" + ddrds_IW.iDOCUMENTID + "] IW AND oracle - in IW only\r\n");

            }
        } // while

        // THEN GO THRU ALL ORACLE ONLY RECORDS - see if hit before already in IW sweep-lookup
        // GO THRU IW-PRESENT RECORDS - SEE IF IN ORACLE
        if (!cds.bMode_InCombinedFulltextNull) {
            Enumeration e2_FTRecords = cds.data_DocScoreCollector_FT.vdataDetailRecord_DocScores.elements();
            while (e2_FTRecords.hasMoreElements()) {
                dataDetailRecord_DocScore ddrds_ORACLE = (dataDetailRecord_DocScore) e2_FTRecords.nextElement();
                String sBeenVisitedKey = "" + ddrds_ORACLE.iDOCUMENTID + ":" + ddrds_ORACLE.iNodeID;
                /////////
                /////////  3 IF IN FT ONLY
                /////////
                vNewMerged_dataDetailRecord_DocScore.addElement(
                        new dataDetailRecord_DocScore(
                        ddrds_ORACLE.iDOCUMENTID,
                        ddrds_ORACLE.iNodeID,
                        ddrds_ORACLE.iGENREID,
                        ddrds_ORACLE.sDOCTITLE,
                        ddrds_ORACLE.sDOCURL,
                        ddrds_ORACLE.sDOCUMENTSUMMARY,
                        ddrds_ORACLE.dIWScore1,
                        ddrds_ORACLE.dIWScore5,
                        ddrds_ORACLE.dIWScore6,
                        ddrds_ORACLE.dORacleScore,
                        ddrds_ORACLE.dORacleScore * cds.dScoreFloorFtimesC,
                        dataDetailRecord_DocScore.iSOURCEMODE_ORACLE_ONLY));
                //com.indraweb.util.Log.logClear("FT ONLY [" + ddrds_ORACLE.iDOCUMENTID + "] \r\n");
            }  // while oracle elements
        } // if not in combine fulltext null mode
        /////////
        /////////
        // NOW SORT ALL
        /////////
        /////////
        //QSortVector.qs(vNewMerged_dataDetailRecord_DocScore, 0);
        // use mergesort instead as QA degenerates on a sorted set
        sql4j.CQLInterpreter.Comparator_dataDetailRecord icomp = new sql4j.CQLInterpreter.Comparator_dataDetailRecord();
        UtilMergeSort.sort(vNewMerged_dataDetailRecord_DocScore, icomp);


        // STEP 3 ***************************************
        // get final list
        Hashtable htdata_docIDSet_To_DetailRecord_DocScore_finalX = new Hashtable();
        int iNumRecssOutputted = 0;

        int iNumRecords_iSOURCEMODE_IW_ONLY = 0;
        int iNumRecords_iSOURCEMODE_ORACLE_ONLY = 0;
        int iNumRecords_iSOURCEMODE_IW_AND_ORACLE = 0;

        int iNumRecsSkippedAsDups = 0;
        int iStartcount = vNewMerged_dataDetailRecord_DocScore.size();
        for (int i = iStartcount - 1;
                i >= 0 && (iNumTopRecs < 0 || iNumRecssOutputted < iNumTopRecs)
                && iNumRecssOutputted < iMaxRecordCount; i--) {
            if (iNumRecssOutputted == 0) {
                out.println("<DOCUMENTS>");
            }

            dataDetailRecord_DocScore ddrds_COMBINED =
                    (dataDetailRecord_DocScore) vNewMerged_dataDetailRecord_DocScore.elementAt(i);
            // see if that docid outputted already
//            com.indraweb.util.Log.logClear("docid out [" +  ddrds_COMBINED.iDOCUMENTID + "]\r\n");
            Object test = htdata_docIDSet_To_DetailRecord_DocScore_finalX.get(new Integer(ddrds_COMBINED.iDOCUMENTID));
            if (test == null) {
                //throw new Exception ("second time for key " + ddrds_COMBINED.iDOCUMENTID );
                htdata_docIDSet_To_DetailRecord_DocScore_finalX.put(
                        new Integer(ddrds_COMBINED.iDOCUMENTID),
                        ddrds_COMBINED);

                StringBuilder sbOut = new StringBuilder();

                sbOut.append(" <DOCUMENT>\r\n");
                sbOut.append("   <DOCUMENTID>").append(ddrds_COMBINED.iDOCUMENTID).append("</DOCUMENTID>\r\n");
                sbOut.append("   <DOCTITLE><![CDATA[").append(ddrds_COMBINED.sDOCTITLE).append("]]></DOCTITLE>\r\n");
                sbOut.append("   <DOCURL>").append(ddrds_COMBINED.sDOCURL).append("</DOCURL>\r\n");
                sbOut.append("   <DOCSUMMARY><![CDATA[" + "none" + "]]></DOCSUMMARY>\r\n");
                sbOut.append("   <NODEID>").append(ddrds_COMBINED.iNodeID).append("</NODEID>\r\n");
                sbOut.append("   <GENREID>").append(ddrds_COMBINED.iGENREID).append("</GENREID>\r\n");
                sbOut.append("   <SCORE1>").append(f(ddrds_COMBINED.dIWScore1)).append("</SCORE1>\r\n");
                sbOut.append("   <SCORE5>").append(f(ddrds_COMBINED.dIWScore5)).append("</SCORE5>\r\n");
                sbOut.append("   <SCORE6>").append(f(ddrds_COMBINED.dIWScore6)).append("</SCORE6>\r\n");
                sbOut.append("   <SCOREFT>").append(f(ddrds_COMBINED.dORacleScore)).append("</SCOREFT>\r\n");
                sbOut.append("   <SCOREAGG>").append(f(ddrds_COMBINED.dAggScore)).append("</SCOREAGG>\r\n");
                sbOut.append("   <SOURCES>").append(ddrds_COMBINED.iSourceMode).append("</SOURCES>\r\n");
                sbOut.append("</DOCUMENT>");
                out.println(sbOut.toString());

                if (ddrds_COMBINED.iSourceMode == dataDetailRecord_DocScore.iSOURCEMODE_IW_ONLY) {
                    iNumRecords_iSOURCEMODE_IW_ONLY++;
                } else if (ddrds_COMBINED.iSourceMode == dataDetailRecord_DocScore.iSOURCEMODE_ORACLE_ONLY) {
                    iNumRecords_iSOURCEMODE_ORACLE_ONLY++;
                } else if (ddrds_COMBINED.iSourceMode == dataDetailRecord_DocScore.iSOURCEMODE_IW_AND_ORACLE) {
                    iNumRecords_iSOURCEMODE_IW_AND_ORACLE++;
                } else {
                    throw new Exception("invalid source mode [" + ddrds_COMBINED.iSourceMode + "]");
                }



                if (cds.iMode == 2)// trec mode only
                {
                    if ((iNumRecssOutputted + 1) <= 100) {
                        addTrecSummaryOutputLine("/temp/IndraTrec3b2.txt",
                                cds.iQid,
                                ddrds_COMBINED.iDOCUMENTID,
                                iNumRecssOutputted + 1,
                                ddrds_COMBINED.dAggScore,
                                dbc);
                    }

                    StringBuilder sbSQLInsert = new StringBuilder("insert into treccombine ");   // trecqrel key search aid
                    sbSQLInsert.append("(xactid, FTfloor, IWFloor, questionid, counter, DOCUMENTID, "
                            + " nodeid, score1, score5, "
                            + " score6, scoreft, scoreagg, sourcemode) ");
                    sbSQLInsert.append("values ('").append(cds.iTransactionID).append("','").append(cds.dScoreFloorFtimesC).append("','").append(cds.dScoreFloorFTScore).append("','").append(cds.iQid).append("','").append(iNumRecssOutputted).append("','").append(ddrds_COMBINED.iDOCUMENTID).append("','").append(ddrds_COMBINED.iNodeID).append("','").append(f(ddrds_COMBINED.dIWScore1)).append("','").append(f(ddrds_COMBINED.dIWScore5)).append("','").append(f(ddrds_COMBINED.dIWScore6)).append("','").append(f(ddrds_COMBINED.dORacleScore)).append("','").append(f(ddrds_COMBINED.dAggScore)).append("','").append(ddrds_COMBINED.iSourceMode).append(
                            "')");


                    cds.vSSQLInsertsForCombinedTable.addElement(sbSQLInsert.toString());
                } // if trec mode 2
                //com.indraweb.util.Log.logClear("COMBINED DATA OUT [" +  sbOut.toString()+ "]\r\n");
                // table
                iNumRecssOutputted++;
            } else {
                iNumRecsSkippedAsDups++;
            }
        }
        if (iNumRecssOutputted > 0) {
            out.println("</DOCUMENTS>");
        }

        com.indraweb.util.Log.logClearcr(
                "Combined summary "
                + " iStartcount [" + iStartcount + "] "
                + " iNumRecssOutputted [" + iNumRecssOutputted + "] "
                + " iNumTopicRecs [" + iNumRecords_iSOURCEMODE_IW_ONLY + "] "
                + " iNumFTRecs [" + iNumRecords_iSOURCEMODE_ORACLE_ONLY + "] "
                + " iNumRecsSkippedAsDups  [" + iNumRecsSkippedAsDups + "] "
                + " iNumBoth [" + iNumRecords_iSOURCEMODE_IW_AND_ORACLE + "] "
                + "");

        out.println("<NUMRESULTS>" + iNumRecssOutputted + "</NUMRESULTS>");

    }

    static String f(double d) {
        return com.indraweb.util.UtilStrings.numFormatDouble(d, 3);
    }

    private static void addTrecSummaryOutputLine(
            String sFileName,
            int iQid,
            int iDocID,
            int iIndex,
            double dScore,
            Connection dbc) throws Exception // 403	FR940525-1-00062	1	2.85
    {
        String sDocURL = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBStr("select docurl from document where documentid = " + iDocID, dbc);
        int iStartSubIndex = sDocURL.lastIndexOf("\\");
        int iEndSubIndex = sDocURL.lastIndexOf(".");
        if (iStartSubIndex > 0 && iEndSubIndex > iStartSubIndex) // if parsable
        {
            String sURLPieceDB = sDocURL.substring(iStartSubIndex + 1, iEndSubIndex);

            com.indraweb.util.UtilFile.addLineToFile(sFileName,
                    iQid + "\t" + sURLPieceDB + "\t" + iIndex + "\t"
                    + com.indraweb.util.UtilStrings.numFormatDouble(dScore, 3) + "\r\n");
        } else {
            throw new Exception("can't parse URL : " + sDocURL);
        }


    }
}
