
/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Sep 6, 2002
 * Time: 11:31:32 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package sql4j.CQLInterpreter;

import java.util.*;

import sql4j.parser.*;

import sql4j.schema.*;
import sql4j.util.*;


public class CQLInterpreterUtils
{

    public static Columns getIDCols (Columns columns)
            throws Exception
    {

        Columns colsUSER_SEL_COLS_IDS = new Columns ();
        Enumeration eCols = columns.elements ();

        // if an ID col - include it and its source
        // else whatever table ... anycol.DOC table -> thattable.DOCID

        int i = 0;
        HashSet hsIDColNamesCoveredAlready = new HashSet ();
        while (eCols.hasMoreElements ())
        {
            Column col = (Column) eCols.nextElement ();
            if (col.getTableName ().equals (( "NODE" )))
                colsUSER_SEL_COLS_IDS.addUnique (new Column (col.getTableName ().toString () , "NODEID"));
            else if (col.getTableName ().equals (( "DOCUMENT" )))
                colsUSER_SEL_COLS_IDS.addUnique (new Column (col.getTableName ().toString () , "DOCUMENTID"));
            else if (col.getTableName ().equals (( "NODEDOCUMENT" )))
            {
                colsUSER_SEL_COLS_IDS.addUnique (new Column (col.getTableName ().toString () , "DOCUMENTID"));
                colsUSER_SEL_COLS_IDS.addUnique (new Column (col.getTableName ().toString () , "NODEID"));
            }
            else if (col.getTableName ().equals (( "CORPUS" )))
                colsUSER_SEL_COLS_IDS.addUnique (new Column (col.getTableName ().toString () , "CORPUSID"));
        }

        return colsUSER_SEL_COLS_IDS;
    }

    public static Tables getExtendToIncludeJoinTabs (Tables tablesIn, boolean bInSecMode)
            throws Exception
    {
        StringSet ssetTables = tablesIn.toSet ();
        //api.Log.Log ("ssetTables in to getExtendToIncludeJoinTabs [" + ssetTables  + "]" );
        if ( ssetTables.contains("UNKNOWNTABLE"))
            throw new Exception("in getExtendToIncludeJoinTabs ssetTables.contains(UNKNOWNTABLE)");
        // table repeats ok - it's a set
        if (ssetTables.contains ("NODE") &&
                ( ssetTables.contains ("DOCUMENT")  ))
            ssetTables.add ("NODEDOCUMENT");

        if (ssetTables.contains ("SIGNATURE") &&
                ( ssetTables.contains ("DOCUMENT")  ))
            ssetTables.add ("NODEDOCUMENT");

        if (ssetTables.contains ("NODEDOCUMENT") &&
                !ssetTables.contains ("NODE")  &&
                bInSecMode )
            ssetTables.add ("NODE"); // will need corpusid for security



        if (ssetTables.contains ("CORPUS") &&
                ( ssetTables.contains ("DOCUMENT") ))
        {
            ssetTables.add ("NODE");
            ssetTables.add ("NODEDOCUMENT");
        }

        if ( ssetTables.contains ("DOCUMENT") &&
                ssetTables.contains ("NODE") &&
                !ssetTables.contains ("NODEDOCUMENT"))
        {
            ssetTables.add("NODEDOCUMENT");
        }

        if ( ssetTables.contains ("DOCUMENT") &&
                ssetTables.contains ("CORPUS")
               )
        {
            ssetTables.add("NODEDOCUMENT");
        }

        if ( ssetTables.contains ("DOCUMENT") &&
                ssetTables.contains ("CORPUS") &&
                !ssetTables.contains ("NODE"))
        {
            ssetTables.add("NODE");
        }

        //api.Log.Log ("ssetTables in to getExtendToIncludeJoinTabs [" + ssetTables  + "]" );
        return new Tables (ssetTables);

    }


    /* originally just returned a string with ANDed table joins...
        now modified to also return a where constrains to be set up for ANDing BY THE CALLER
        with the callers incoming where constraint
        Vector is a String (original return)
        and now includes a wherecondition which is the additional constraints
        added by an optimization step
    */
    public static String buildJoinStrings (Tables tabs_,
                                           boolean bInUserSecSelect,
                                           WhereCondition wc_
                                           ) throws Exception
    {
        //api.Log.Log ("building join strings among tables [" + tabsINOUT.toSet() + "]" );
        StringBuffer sbJoins = new StringBuffer ();
        StringSet ssetTabs = tabs_.toSet ();


        //System.out.println("ssetTabs [" + ssetTabs + "]");

        String sAnd = " ";

        Vector vStrCompPreds_OUT  = new Vector();

        StringBuffer sbDocJoins = new StringBuffer ();

        if (ssetTabs.contains ("NODE") && ssetTabs.contains ("NODEDOCUMENT"))
        {
            //sbJoins.append (sAnd + "NODE.NODEID = NODEDOCUMENT.NODEID ");
            sbJoins.append (sAnd + "NODE.LINKNODEID = NODEDOCUMENT.NODEID ");
            findCompPredsThisTableCol_DeriveCompPredsOnNextTableCol (
                    wc_,  "NODEDOCUMENT.NODEID", "NODE", "LINKNODEID", vStrCompPreds_OUT );
            sAnd = " and ";
        }

        if (ssetTabs.contains ("IDRACNODE") && ssetTabs.contains ("NODE"))
        {
            sbJoins.append (sAnd + " IDRACNODE.NODEID (+) = NODE.NODEID ");
            sAnd = " and ";
        }
        if (ssetTabs.contains ("NODE") && ssetTabs.contains ("SIGNATURE"))
        {
            sbJoins.append (sAnd + " SIGNATURE.NODEID = NODE.LINKNODEID ");
            sAnd = " and ";
        }

        if (ssetTabs.contains ("NODEDOCUMENT") && ssetTabs.contains ("SIGNATURE"))
        {
            sbJoins.append (sAnd + " SIGNATURE.NODEID = NODEDOCUMENT.NODEID ");
            sAnd = " and ";
        }

        // now join the doc equivalence classes with others as needed
/*
        if (sDocJoinTableRoot != null && sDocJoinTableRoot.equals ("DOCUMENT") && ssetTabs.contains ("NODEDOCUMENT"))
        {
            sbJoins.append (sAnd + makeDocJoinTableName (sDocJoinTableRoot) + ".DOCUMENTID = NODEDOCUMENT.DOCUMENTID ");
            sAnd = " and ";
        }
*/

        if (ssetTabs.contains ("NODE") && ssetTabs.contains ("CORPUS"))
        {
            sbJoins.append (sAnd + "NODE.CORPUSID = CORPUS.CORPUSID");
            sAnd = " and ";
        }

        // security
        if ( ssetTabs.contains ("DOCUMENT")  &&
                ssetTabs.contains ("DOCUMENTSECURITY") )
        {
            sbJoins.append (sAnd + "DOCUMENT.DOCUMENTID = DOCUMENTSECURITY.DOCUMENTID");
            sAnd = " and ";
        }
        else if ( ( ssetTabs.contains ("NODEDOCUMENT") && !ssetTabs.contains ("DOCUMENT")) &&
                ssetTabs.contains ("DOCUMENTSECURITY") )
        {
            sbJoins.append (sAnd + "NODEDOCUMENT.DOCUMENTID = DOCUMENTSECURITY.DOCUMENTID");
            sAnd = " and ";
        }

        if (ssetTabs.contains ("NODEDOCUMENT") && ssetTabs.contains ("DOCUMENT"))
        {
            sbJoins.append (sAnd + "NODEDOCUMENT.DOCUMENTID = DOCUMENT.DOCUMENTID");
            findCompPredsThisTableCol_DeriveCompPredsOnNextTableCol (
                    wc_, "DOCUMENT.DOCUMENTID", "NODEDOCUMENT","DOCUMENTID", vStrCompPreds_OUT);
            findCompPredsThisTableCol_DeriveCompPredsOnNextTableCol (
                    wc_, "NODEDOCUMENT.DOCUMENTID", "DOCUMENT","DOCUMENTID", vStrCompPreds_OUT);
            sAnd = " and ";
        }

        // NOW TAKE UP THE VECTOR OF Comp Pred's and create  string for it

        String sReturn = sbDocJoins.toString () + sbJoins.toString () ;
        for ( int i = 0; i < vStrCompPreds_OUT.size(); i++ )
        {
            String sComparisonPred = (String) vStrCompPreds_OUT.elementAt(i);
            sReturn = sReturn  + " and "  + sComparisonPred ;
        }

        //api.Log.Log("buildJoinStrings sReturn [" + sReturn + "]" );

        return sReturn;
    }

    private static String makeDocJoinTableName (String s) throws Exception
    {
        if (s.equals ("DOCUMENT"))
            return s;
        if (s.equals ("NODEDOCUMENT"))
            return s;
        else
            throw new Exception ("invalid doc eq col [" + s + "]");
    }

    /**
     * typical use : Now that we've got join info, resolve cols that come from UNKNOWNTABLE
     * side effect on columns but not on tablesknown
     */
    public static void resolveColumnsUnknown (
                                                Columns columnsToBeResolved,
                                              HashMap hmUnknownCols_StringColToStringSetTables ,
                                              final Tables tablesKnownSoFar)
            throws Exception
    {
        resolveColumnsUnknown (               null,                                   columnsToBeResolved,
                                              hmUnknownCols_StringColToStringSetTables ,
                                              tablesKnownSoFar );

    }
    /**
     * typical use : Now that we've got join info, resolve cols that come from UNKNOWNTABLE
     * side effect on columns but not on tablesknown
     */
    public static void resolveColumnsUnknown (  Columns columnsAlreadyResolvedSelectionSay,
                                                Columns columnsToBeResolved,
                                              HashMap hmUnknownCols_StringColToStringSetTables ,
                                              final Tables tablesKnownSoFar)
            throws Exception
    {
        // has side effect of updating col.tables from unknown to known


        Enumeration enumColumnsToResolve = columnsToBeResolved.elements ();

        //System.out.println("colsUnknown 1 [" + columnsToBeResolved+ "]");

        // for each col see if unknaown
        Tables tablesKnownAccumulator = new Tables ();
        tablesKnownAccumulator.addAll (tablesKnownSoFar);
        if ( columnsAlreadyResolvedSelectionSay != null )
            tablesKnownAccumulator.addAll (columnsAlreadyResolvedSelectionSay.getTables ());
        Tables tabsFromColsIncoming = columnsToBeResolved.getTables ();
        // tabsFromColsIncoming.


        while (enumColumnsToResolve.hasMoreElements ())
        {
            // if unk see if any current tables (e.g., new join tables) cover it
            Column col = (Column) enumColumnsToResolve.nextElement ();
            //api.Log.Log ("testing col [" + col  + "]");
            String sTableName = col.getTableName ().trim().toUpperCase();
            if (sTableName == null )
                api.Log.Log ("sTableName == null for col [" + col.getName()  + "]" );
            if (!sTableName.equals ("UNKNOWNTABLE"))
            {
                tablesKnownAccumulator.add(new Table(sTableName));
            }
            else
            {
                if ( !resolveColsWithPriorCols( columnsAlreadyResolvedSelectionSay, col) )
                {

                    StringSet sSetCandidateTables = (StringSet) hmUnknownCols_StringColToStringSetTables.get (col.getName ().trim());
                    //api.Log.Log ("   col.getName ()) [" + col.getName () + "] sSetCandidateTables [" + sSetCandidateTables + "]");
                    if (sSetCandidateTables == null)
                    {
                        sSetCandidateTables = new StringSet();
                        Tables tablesNeededByOrderCustom = CQLSchema.schema.getTables(col);
                        if ( tablesNeededByOrderCustom == null )
                            throw new Exception ("1 no table for column [" + col.getName () + "]");
                        Vector vTablesNeededByOrderCustom = tablesNeededByOrderCustom.toVector();
                        if ( vTablesNeededByOrderCustom.size() == 0 )
                            throw new Exception ("2 no table for column [" + col.getName () + "]");
                        if ( vTablesNeededByOrderCustom.size() > 1 )
                            throw new Exception ("ambiguous table for column [" + col.getName () + "]");
                        String sTabName = ((Table) vTablesNeededByOrderCustom.elementAt(0)).toString();
                        sSetCandidateTables.add(sTabName);
                        //api.Log.Log ("   sSetCandidateTables [" + sSetCandidateTables + "] " );
                        col.setTableName(sTabName);
                    }

                    StringSet ssIntersect = sSetCandidateTables.intersection (tablesKnownAccumulator.toSet ());

                    //System.out.println("tablesSoFarKnownAccumulator [" + tablesSoFarKnownAccumulator + "]");
                    //System.out.println("ssIntersect [" + ssIntersect + "]");
                    if (ssIntersect.size () > 1)
                    {
                        //api.Log.Log ("two candidates for CQL col interpret ssIntersect [" + ssIntersect + "]");
                    }
                    if (ssIntersect.size () > 0)
                    {
                        String sTable = (String) ssIntersect.elementAt (0);
                        //api.Log.Log ("   setting table for this col [" + sTable + "]" );
                        col.setTableName (sTable); // pick a random
                    }
                }
            }
        } // while ( enum.hasMoreElements())

        Columns colsUnknown = columnsToBeResolved.getUnknownCols ();
        //System.out.println("colsUnknown 2 [" + colsUnknown+ "]");

        if (colsUnknown.size () > 0)
        {
            boolean bCS_FTSCOREBeenSet = false;
            //api.Log.Log ("colsUnknown.size() > 0 after first pass");

            // nodeid documentid
            StringSet ssColsUnknown = colsUnknown.getColNames (false);

            if (ssColsUnknown.contains ("NODEID") && ssColsUnknown.contains ("DOCUMENTID"))
            {
                colsUnknown.getColumn ("UNKNOWNTABLE" , "NODEID").setTableName ("NODEDOCUMENT");
                colsUnknown.getColumn ("UNKNOWNTABLE" , "DOCUMENTID").setTableName ("NODEDOCUMENT");
                bCS_FTSCOREBeenSet = setCS_FTScore (bCS_FTSCOREBeenSet , colsUnknown, "NODEDOCUMENT" );
            }
            else if (ssColsUnknown.contains ("NODEID") && !ssColsUnknown.contains ("DOCUMENTID"))
            {
                colsUnknown.getColumn ("UNKNOWNTABLE" , "NODEID").setTableName ("NODE");
                tablesKnownAccumulator.addAll(new Tables (new Table("NODE")));
                bCS_FTSCOREBeenSet = setCS_FTScore (bCS_FTSCOREBeenSet , colsUnknown, "NODE" );
            }
            else if (!ssColsUnknown.contains ("NODEID") && ssColsUnknown.contains ("DOCUMENTID"))
            {
                colsUnknown.getColumn ("UNKNOWNTABLE" , "DOCUMENTID").setTableName ("DOCUMENT");
                tablesKnownAccumulator.addAll(new Tables (new Table("DOCUMENT")));
                bCS_FTSCOREBeenSet = setCS_FTScore (bCS_FTSCOREBeenSet , colsUnknown, "DOCUMENT" );
            }

            // corpusid
            if (ssColsUnknown.contains ("CORPUSID") && !ssColsUnknown.contains ("NODEID"))
            {
                colsUnknown.getColumn ("UNKNOWNTABLE" , "CORPUSID").setTableName ("CORPUS");
                tablesKnownAccumulator.addAll(new Tables (new Table("CORPUS")));
            }
            else if (ssColsUnknown.contains ("NODEID") && ssColsUnknown.contains ("CORPUSID"))
            {
                colsUnknown.getColumn ("UNKNOWNTABLE" , "CORPUSID").setTableName ("NODE");
                tablesKnownAccumulator.addAll(new Tables (new Table("NODE")));
                bCS_FTSCOREBeenSet = setCS_FTScore (bCS_FTSCOREBeenSet , colsUnknown, "NODE" );
            }
        }

        Columns colsUnkStill = columnsToBeResolved.getUnknownCols ();
        if (colsUnkStill.size () > 0)
            throw new Exception ("cols still unknown table [" + colsUnkStill + "]");
    } // resolve columns unknown

    private static boolean setCS_FTScore(boolean bCS_FTSCOREBeenSet, Columns colsUnknown, String sTableNameForRef)
    {
        if ( !bCS_FTSCOREBeenSet )
        {
            Column col = colsUnknown.getColumn ("UNKNOWNTABLE" , "CS_FTSCORE");

            if ( col != null )
                col.setTableName (sTableNameForRef);
        }
        return true;
    }

    public static String formatSQL (String sql)
    {
        // get rid double spaces
        //sql = sql.replaceAll("\r", " ");
        //sql = sql.replaceAll("\n", " ");

        // get rid double spaces
        //while ( true )
        //{
//            if ( sql.indexOf("  ") >= 0)
//                sql = sql.replaceAll("  ", " ");
//            else
//                break;
//        }

        sql = sql.replaceAll ("depthfromroot" , "xxyyxx");
        sql = sql.replaceAll ("from" , "\r\nfrom\r\n");
        sql = sql.replaceAll (" where " , "\r\n where \r\n");
        sql = sql.replaceAll (" and " , "\r\n and ");
        sql = sql.replaceAll (" or " , "\r\n or ");
        sql = sql.replaceAll ("intersect" , "\r\nintersect\r\n");
        sql = sql.replaceAll (" in " , "\r\n in \r\n");
        sql = sql.replaceAll (" union " , "\r\n union \r\n");
        sql = sql.replaceAll (" minus " , "\r\n minus \r\n");

        sql = sql.replaceAll ("DEPTHFROMROOT" , "XXYYXX");
        sql = sql.replaceAll ("FROM" , "\r\nFROM\r\n");
        sql = sql.replaceAll (" WHERE " , "\r\n WHERE \r\n");
        sql = sql.replaceAll (" AND " , "\r\n AND ");
        sql = sql.replaceAll (" OR " , "\r\n OR ");
        sql = sql.replaceAll ("INTERSECT" , "\r\nINTERSECT\r\n");
        sql = sql.replaceAll (" IN " , "\r\n IN \r\n");
        sql = sql.replaceAll (" UNION " , "\r\n UNION \r\n");
        sql = sql.replaceAll (" MINUS " , "\r\n MINUS \r\n");

        while (true)
        {
            if (sql.indexOf ("  ") >= 0)
                sql = sql.replaceAll ("  " , " ");
            else
                break;
        }
        sql = sql.replaceAll ("XXYYXX","DEPTHFROMROOT" );
        sql = sql.replaceAll ("xxyyxx","depthfromroot" );
        return "\r\n"+sql + "\r\n";
    }
    public static String printTree(
            WhereCondition wc ,
            int iDepth
            // IWFullTextQueryContainer mpqc
            ) throws Exception
    {

        StringBuffer sbCQLSubquery = new StringBuffer ();

        // WALK TREE FOR SUBSUQUERIES
        // start by assuming that doc is in the select list
        // (7)
        if (wc instanceof AtomicWhereCondition)
        {
            System.out.println(iDepth + ". wc() : " + wc.toString() +
                    " \r\nwc.getIfAllUnderAreNodeNegsConnectedByAnd [" + wc.getIfAllUnderAreNodeNegsConnectedByAnd() +
                    "] \r\nwc.getIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree [" + wc.getIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree() +
                    "] \r\nwc.getIfExitsNodeCorpusAtoms_recursive [" + wc.getIfExitsNodeCorpusAtoms_recursive() +
                    "] \r\nwc.getNodeCorpusANDSemanticsHereOrBelow [" + wc.getNodeCorpusANDSemanticsHereOrBelow() +
                    "]" );
        }   // atomic where
        else if (wc instanceof CompoundWhereCondition)
        {
            CompoundWhereCondition cwc = (CompoundWhereCondition) wc;
            //System.out.println(iCallCounter + ":" + iDepth + ":" + sLeftRightOrStart + " CompoundWhereCondition" + "\r\ncwc.getOperator() [" + cwc.getOperator() + "]\r\n" + " toString() [" + cwc.toString() + "]\r\n" );

            String sOp = cwc.getOperator ();
            // if ( sOp.equals("AND") && exists_anyNodeTableSpecific_Or_CorpusConstraintsBelow ( wc ) ) {

            System.out.println(iDepth + ". (");
            printTree ( cwc.getLeft () , iDepth + 1) ;
            System.out.println(iDepth +". cwc op: " +  sOp +
                    " \r\nwc.getIfAllUnderAreNodeNegsConnectedByAnd [" + wc.getIfAllUnderAreNodeNegsConnectedByAnd() +
                    "] \r\nwc.getIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree [" + wc.getIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree() +
                    "] \r\nwc.getIfExitsNodeCorpusAtoms_recursive [" + wc.getIfExitsNodeCorpusAtoms_recursive() +
                    "] \r\nwc.getNodeCorpusANDSemanticsHereOrBelow [" + wc.getNodeCorpusANDSemanticsHereOrBelow() +
                    "]" );
            printTree ( cwc.getRight() , iDepth + 1) ;
            System.out.println(iDepth + ". )");

        } // compound where
        else
            throw new Exception ("BQ wc instance of unknown type");

        //api.Log.Log ("BQ sbCQLSubquery [" + sbCQLSubquery + "]");
        return sbCQLSubquery.toString ();

    }


    // **************************************
    // WHERE CONDITION TESTERS AND APPENDERS
    // **************************************
    private static void findCompPredsThisTableCol_DeriveCompPredsOnNextTableCol (
                WhereCondition wc_,
                String sTableColExisting,
                String sTableToExtendTo,
                String scolToExtendTo,
                Vector vStrCompPredsAsStrings_OUT) throws Exception
    {
        Vector vScalarExpForRecurse = new Vector();
        findScalarEquiExps_OnThisTableCol_recurse(
                vScalarExpForRecurse,
                wc_,
                sTableColExisting);


        for ( int i = 0; i < vScalarExpForRecurse.size(); i++)
        {
            String sEquiExpressionOnNewTableCol = sTableToExtendTo + "." + scolToExtendTo + " = " +
                ((ScalarExp) vScalarExpForRecurse.elementAt(i)).toString();
            //api.Log.Log ("sEquiExpressionOnNewTableCol [" + sEquiExpressionOnNewTableCol + "]" );
            vStrCompPredsAsStrings_OUT.addElement( sEquiExpressionOnNewTableCol );



/*
            wc_ = mergeWhereConditions("AND", wc_,
                new ComparisonPredicate( "=", ) ;
*/
        }
    }

    private static void findScalarEquiExps_OnThisTableCol_recurse (
                Vector vOfScalarExps_OutRecurse,
                WhereCondition wc_,
                String sTableColExisting
                )  throws Exception
    {
        if ( wc_ instanceof AtomicWhereCondition )
        {
            if ( wc_ instanceof ComparisonPredicate )
            {
                ComparisonPredicate cp_wc = ( ComparisonPredicate ) wc_;
                if ( cp_wc.getOperator().equals("=") &&  cp_wc.getRightType().equals("scalar") )
                {
                    String sTabColThisWC = cp_wc.getLeft().toString().toLowerCase();
                    if ( sTabColThisWC.toLowerCase().equals(sTableColExisting.toLowerCase()) )
                    {
                        //api.Log.Log ("adding scalar exp [" + cp_wc.getRight()  + "]" );
                        vOfScalarExps_OutRecurse.addElement ( cp_wc.getRight() );
                    }
                }
            }
        }
        else if ( wc_ instanceof CompoundWhereCondition )
        {
            CompoundWhereCondition cwc = (CompoundWhereCondition) wc_;
            if (cwc.getOperator().equals("AND") )
            {
                findScalarEquiExps_OnThisTableCol_recurse( vOfScalarExps_OutRecurse, cwc.getLeft(), sTableColExisting);
                findScalarEquiExps_OnThisTableCol_recurse( vOfScalarExps_OutRecurse, cwc.getRight(), sTableColExisting);
            }
        }
        else
            throw new Exception ("where condition class not handled [" + wc_.getClass().getName() + "]" );

    }





    private static WhereCondition mergeWhereConditions( String sOp,
                WhereCondition wcL,
                WhereCondition[] awcArr)
    {

        WhereCondition wcRet = wcL;
        for ( int i = 0; i < awcArr.length; i++)
        {
            wcRet = mergeWhereConditions (sOp, wcRet, awcArr[i]);
        }
        return wcRet;
    }

    private static WhereCondition mergeWhereConditions( String sOp,
                WhereCondition wcL,
                WhereCondition wcR)
    {

        if ( wcL == null )
            return wcR;
        else
            return new CompoundWhereCondition (sOp, wcL, wcR);

    }

    private static boolean resolveColsWithPriorCols ( Columns columnsAlreadyResolvedSelectionSay, Column colToBeResolved )
        throws Exception
    {

        boolean bReturnResolved = false;

        //for (int i = 0; i < columnsToBeResolved.toVector().size(); i++)
        {
          //  Column colToResolve = (Column) columnsToBeResolved.toVector().elementAt(i);
            String sTableNameToResolve = colToBeResolved.getTableName();
            String sColNameToResolve = colToBeResolved.getColumnName().toString();
            if ( sTableNameToResolve.equals("UNKNOWNTABLE") && columnsAlreadyResolvedSelectionSay != null )
            {
                for (int j = 0; j < columnsAlreadyResolvedSelectionSay.toVector().size(); j++)
                {
                    Column colThatMayOfferResolution = (Column) columnsAlreadyResolvedSelectionSay.toVector().elementAt(j);
                    String sColnameThatMayOfferResolution = colThatMayOfferResolution.getColumnName().toString();
                    String sTableNameMayOfferResolution = colThatMayOfferResolution.getTableName();
                    if ( sTableNameMayOfferResolution.equals("UNKNOWNTABLE"))
                        throw new Exception ("supposed to be resolved for col name [" + colThatMayOfferResolution.getColumnName() + "]" );
                    if ( sColnameThatMayOfferResolution.equals(sColNameToResolve))
                    {
                        colToBeResolved.setTableName(sTableNameMayOfferResolution);
                        bReturnResolved = true;
                        break;
                    }
                }

            }
        }
        return bReturnResolved;

    }

}
