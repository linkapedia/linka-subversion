
/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 28, 2002
 * Time: 2:33:11 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

    class dataDetailRecord_DocScore
    {

        int iDOCUMENTID = -1;
        int iNodeID = -1;
        int iGENREID = -1;
        String   sDOCTITLE = null;
        String   sDOCURL = null;
        String   sDOCUMENTSUMMARY = null;
        double dIWScore1 = -1;

        double dIWScore5 = -1;
        double dIWScore6 = -1;
        double dORacleScore = -1;
        double dAggScore= -1;
        int iSourceMode = -1;

        public static final int iSOURCEMODE_IW_ONLY = 1;
        public static final int iSOURCEMODE_IW_AND_ORACLE = 2;
        public static final int iSOURCEMODE_ORACLE_ONLY = 3;

        public dataDetailRecord_DocScore (
                int iDOCUMENTID,
                int iNodeID,
                        int iGENREID,
                        String   sDOCTITLE,
                        String   sDOCURL,
                        String   sDOCUMENTSUMMARY,
                     double dIWScore1,
                     double dIWScore5,
                     double dIWScore6,
                     double dORacleScorex,
                     double dAggScore,
                    int iSourceMode
                )
throws Exception
        {
            this.iDOCUMENTID = iDOCUMENTID;
            this.iNodeID = iNodeID;
            this.iGENREID = iGENREID;
            this.sDOCTITLE= sDOCTITLE;
            this.sDOCURL             = sDOCURL             ;
            this.sDOCUMENTSUMMARY               = sDOCUMENTSUMMARY               ;
            this.dIWScore1 =dIWScore1 ;
            this.dIWScore5 =dIWScore5 ;
            this.dIWScore6 =dIWScore6 ;
            this.dORacleScore =dORacleScorex;
            this.dAggScore=dAggScore;
            this.iSourceMode = iSourceMode;

        }
    }
