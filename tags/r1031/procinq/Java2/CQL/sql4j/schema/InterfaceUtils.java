/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Nov 16, 2002
 * Time: 4:00:46 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.schema ;

import sql4j.parser.*;
import sql4j.util.StringSet;

import java.util.Enumeration;

public class InterfaceUtils {
    public static Tables getJoinTabs ( Columns cols ) throws Exception
    {
        Tables tReturn = new Tables ();
        for (Enumeration e = cols.elements(); e.hasMoreElements();)
        {
            Column c = (Column) e.nextElement();
            String sTabName = c.getTableName();
            tReturn.add(new Table (sTabName ));
        }
         return tReturn;
    }





    public static void addTableNamesToColumns(SelectStatement selStat, Schema schema) throws Exception {
        Columns cs = selStat.getAllColumns();
        if (cs == null)
            return;
        //System.out.println ("columns [" + columns  + "]");
//    System.out.println ("tables [" + tables + "]");
        for (Enumeration e = cs.toVector().elements(); e.hasMoreElements();) {
            Column column = (Column) e.nextElement();
            //System.out.println ("column [" + column  + "]");
            if (!column.isVariable()) { //do nothing for variables.
                if (column.getTableName() == null) { // Table name is not in the column class, retrieve that from the schema.
                    // all schema tables?
                    Tables tables1 = schema.getTables(column);
                    StringSet ssetTableSet1 = (tables1 == null) ? null : new StringSet(tables1.toVector());
                    /// query cols
                    StringSet tableSet2 = (selStat.getAllTables() == null) ? null : new StringSet(selStat.getAllTables().toVector());
                    StringSet intersection = (tables1 == null) ? null : ssetTableSet1.intersection(tableSet2);
                    if (intersection == null || intersection.size() == 0) {
                        System.out.println("can't find the table name for the column " + column.toString());
                    } else
                        if (intersection.size() > 1) {
                            throw new SQLException("more than one table associated with a column [" + column + "]");
                        } else
                            if (intersection.size() == 1) {
                                String tableName = (String) intersection.elements().nextElement();
                                column.addTableName(tableName);
                            }
                }
            }
        }
    } // addtablenames

}
