package sql4j.parser;

/**
 * Insert the type's description here.
 * Creation date: (10/31/00 12:17:55 PM)
 * @author:  Jianguo Lu
 **/

import sql4j.schema.*;
import com.indraweb.ir.Thesaurus;

public class LikePredicate extends AtomicWhereCondition
{
    private ScalarExp scalarExp;
    private String label;
    private String pattern;

    /**
     * LikePredicate constructor comment.
     */
    public LikePredicate()
    {
        super();
    }

    public LikePredicate(ScalarExp se, String l, String p)
    {
        scalarExp = se;
        column1 = se.getColumn();
        label = l;
        pattern = p;
    }

    public String getPattern()
    {
        return pattern;
    }

    public String toString()
    {
        return toString(true);
    }
    /**
     * Insert the method's description here.
     * Creation date: (1/30/01 7:10:02 AM)
     * @return java.lang.String
     */
    public String toString(boolean bIncludeGTZeroForContains)
    {
        String sReturn = null;
        try {
            // String sColName = scalarExp.getColumn().getName();
            // System.out.println("scalarExp.toString() [" + scalarExp.toString() + "]" );

            String sNot = "";
            if (label.toLowerCase().trim().startsWith("not"))
                sNot = " not ";
            //assumes that its been initialized already during parsing
            if ( sql4j.CQLInterpreter.CQLSchema.getIsColTextIndexed ( scalarExp.toString(), null ) )
            {
                //System.out.println ("scalarExp.toString() [" + scalarExp.toString()+ "]");
                //if ( pattern.indexOf('%') >= 0)
    //            throw new RuntimeException( "% not supported in full text index search text " +
    //                 " label [" + label + "]  pattern  [" + pattern + "]");

                String sGTZero = "";
                if (bIncludeGTZeroForContains)
                    sGTZero = " > 0 ";
                if (pattern.startsWith("'##NEARASLIKECODE"))
                {
                    pattern = pattern.replaceFirst("'##NEARASLIKECODE", "");
                    sReturn = sNot + " contains (" + scalarExp.toString() + ", 'near " + pattern + ") " + sGTZero;
                    try
                    {
                        sql4j.CQLInterpreter.SyntaxCheck.parenMatch(pattern);

                    } catch (Exception e)
                    {
                        System.out.println("Error in NEAR expression. " + e.getMessage() + " [" + pattern + "]");
                    }
                } else if (pattern.indexOf("%%") >= 0)
                {
                    sReturn = "lower(" + scalarExp.toString() + ") " + label + " " + pattern.replaceAll("%%", "%").toLowerCase();
                } else
                    sReturn = sNot + " contains (" + scalarExp.toString() + ", " + pattern + ") " + sGTZero;
                //System.out.println("sContainsTest  [" + sContainsTest  + "]");
            } // if a full text indexed col
            else if ( (scalarExp.toString().toLowerCase().trim().equals("node.nodeid") ||
                    scalarExp.toString().toLowerCase().trim().equals("nodedocument.nodeid")) &&
                    pattern.startsWith("'##NODEIDUNDERASLIKECODE ") )
            {

                pattern = pattern.replaceFirst("'##NODEIDUNDERASLIKECODE ", "");
                pattern = pattern.replaceFirst("'", "");

                try
                {
                    sql4j.CQLInterpreter.SyntaxCheck.parenMatch(pattern);

                } catch (Exception e)
                {
                    System.out.println("Error in NEAR expression. " + e.getMessage() + " [" + pattern + "]");
                }
    /*
            if ( com.indraweb.util.UtilFile.bFileExists("c:/temp/IndraDebugUseOldNodeUnderMethod.txt") )
            {
                //original way
                sReturn = sNot + " (" +
                   "node.nodeid in (select nodeid from node start with parentid = " + pattern +
                   " connect by prior nodeid = parentid) ) " ;
            }    else {     // including root
    */
    /*
                sReturn = sNot + " (" +
                   "node.nodeid = " + pattern  + " or node.nodeid in (select nodeid from node start with parentid = " + pattern +
                   " connect by prior nodeid = parentid) ) " ;
    */
                // }
    /*
                sReturn = sNot + " (" +
                        " node.nodeid in (select nodeid from node start with nodeid = " + pattern +
                        " connect by prior nodeid = parentid) ) ";
    */
                sReturn = sNot + " (" +
                        " node.nodeid in (select " + pattern + " from dual union select linknodeid from node start with nodeid = " + pattern +
                        " connect by prior linknodeid = parentid) ) ";
            } else
                sReturn = scalarExp.toString() + " " + label + " " + pattern;

        } catch ( Exception e ) {
            api.Log.LogError("error generating LikePredicate as String - suppressed throw", e);
        }
        return sReturn;

    }

    public String getLabel(){
        return label;
    }

    public void setLabel(String sLabel){
        label = sLabel;
    }

/*
    public boolean getIsThereANodeIDUnder_Recursive()
    {

        if ( pattern.startsWith("'##NODEIDUNDER"))
        {
            //api.Log.Log ("TRUE asking Like P where if getIsThereANodeIDUnder_Recursive");
            return true;
        } else {
            //api.Log.Log ("FALSE asking Like P where if getIsThereANodeIDUnder_Recursive");
            return false;
        }
    }
*/

    public boolean isColFTIndexed()            throws Exception
    {
            return sql4j.CQLInterpreter.CQLSchema.getIsColTextIndexed ( scalarExp.toString(), null ) ;
    }

}