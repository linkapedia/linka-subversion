/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Sep 29, 2003
 * Time: 7:50:38 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package sql4j.parser;

public class WhereConditionRoot
{


    boolean bIfExitsNodeCorpusAtoms_recursive = false;
    boolean bIfAllUnderAreNodeNegsConnectedByAnd = false;

    public void setIfExitsNodeCorpusAtoms_recursive ( boolean bMarkIfExitsNodeCorpusAtoms_recursive_ )
    {
        bIfExitsNodeCorpusAtoms_recursive = bMarkIfExitsNodeCorpusAtoms_recursive_;
    }
    public boolean getIfExitsNodeCorpusAtoms_recursive()
    {
        return bIfExitsNodeCorpusAtoms_recursive;
    }

    public void setIfAllUnderAreNodeNegsConnectedByAnd ( boolean bMarkIfAllUnderAreNodeNegsConnectedByAnd_)
    {
        bIfAllUnderAreNodeNegsConnectedByAnd = bMarkIfAllUnderAreNodeNegsConnectedByAnd_;
    }
    public boolean getIfAllUnderAreNodeNegsConnectedByAnd()
    {
        return bIfAllUnderAreNodeNegsConnectedByAnd;
    }

}
