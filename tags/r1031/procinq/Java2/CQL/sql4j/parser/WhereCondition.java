package sql4j.parser;

/**
 * Insert the type's description here.
 * Creation date: (10/20/00 1:19:45 AM)
 * @author: Jianguo Lu
 */

import sql4j.schema.*;

public interface WhereCondition {
	public Columns getColumns() throws Exception;
	public Tables getTables() throws Exception;

    public void setIfExitsNodeCorpusAtoms_recursive ( boolean bMarkIfExitsNodeCorpusAtoms_recursive_)  ;
    public boolean getIfExitsNodeCorpusAtoms_recursive();

    public void setIfAllUnderAreNodeNegsConnectedByAnd (boolean bMarkIfAllUnderAreNodeNegsConnectedByAnd_);
    public boolean getIfAllUnderAreNodeNegsConnectedByAnd ();
    public boolean getIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree ();

    public boolean getNodeCorpusANDSemanticsHereOrBelow();
    //public boolean getIsThereANodeIDUnder_Recursive();
}




