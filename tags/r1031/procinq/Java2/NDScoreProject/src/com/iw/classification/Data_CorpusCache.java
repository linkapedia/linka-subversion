/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Mar 15, 2004
 * Time: 10:01:37 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.classification;

import com.indraweb.util.UtilProfiling;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

public class Data_CorpusCache {

    private static Hashtable htIntCorpusToIntNodeCount = new Hashtable();
    private static boolean bVerbose = false;

    public static synchronized void addCorpusSize(Integer ICorpusID, Integer INodeCount) {
        htIntCorpusToIntNodeCount.put(ICorpusID, INodeCount);
    }

    public static synchronized int getCorpusSize(Integer ICorpusID, Connection dbc) throws Exception {
        Integer ISize = (Integer) htIntCorpusToIntNodeCount.get(ICorpusID);
        if (ISize == null) {
            String sSQL = "select count (*) from node where corpusid = " + ICorpusID;
            int i1Size = (int) com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBLong(sSQL, dbc);
            long lStart = System.currentTimeMillis();
            if (bVerbose) {
                api.Log.Log("Corp [" + ICorpusID + "] size [" + i1Size + "] from db ms [" + UtilProfiling.elapsedTimeMillis(lStart) + "]");
            }
            ISize = new Integer(i1Size);
            htIntCorpusToIntNodeCount.put(ICorpusID, ISize);
        }
        return ISize.intValue();
    }

    public static synchronized void invalidateCache(Integer ICorpusID) {
        htIntCorpusToIntNodeCount.remove(ICorpusID);
    }

    public static int getNodeTotal(HashSet hsCorpora, Connection dbc) throws Exception {
        int iNodeCountTotal = 0;
        Iterator iterICorp = hsCorpora.iterator();
        while (iterICorp.hasNext()) {
            Integer ICorpusID = (Integer) iterICorp.next();
            iNodeCountTotal += getCorpusSize(ICorpusID, dbc);
        }
        return iNodeCountTotal;

    }
}
