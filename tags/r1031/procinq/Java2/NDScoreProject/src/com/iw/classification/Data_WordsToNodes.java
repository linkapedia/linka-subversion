package com.iw.classification;

import com.indraweb.execution.Session;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.Thesaurus;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.util.*;
import com.iw.scoring.NodeForScore;
import java.sql.Connection;
import java.util.*;

/**
 * per corpus save word stat's
 * change history
 * 1) 2004 02 25 - corpora loaded from ht to hs
 */
public class Data_WordsToNodes {

    static Hashtable htWordsToVecOfIntegerNode = new Hashtable();
    //private static Hashtable htAllWords_ForObjectReuse = null;
    static Hashtable htNodeIDToCorpusID = new Hashtable();
    private static Hashtable htCorpusIDToLongTimeLoaded = new Hashtable();
    static boolean bCacheFresh = false;
    static boolean bVectorNodesIsNFS = false;
    // for use at cache refresh :
    private static Vector vCorpInMemOrderedByInsertTime = new Vector();
    private static Vector vIntCorporaInvalidated = new Vector();
    static ReadWriteLockCache rwlockcache = new ReadWriteLockCache();
    public static boolean bWANT_DATA_STRUCT_DETAIL_TO_LOG = false;
    public static String sBACKUPSIGFILE_SUFFIX = "_FOREMPTY_BACKUP";
    public static boolean bCacheBeenInitialiedOnce = false;

    public static synchronized void emptyCache(Connection dbc, SignatureParms sigparms) throws Exception {
        //long lstart = System.currentTimeMillis();
        //api.Log.Log("start empty cache");
        refreshCache(dbc, "", true, sigparms);
        //long ldone= System.currentTimeMillis();
        //api.Log.Log("done empty cache ms [" + (ldone-lstart) + "]");

    }

    public static synchronized void sweepCacheToThisSet(Connection dbc, String sCommaListCorporaThisCall, SignatureParms sigparms) throws Exception {
        refreshCache(dbc, sCommaListCorporaThisCall, true, sigparms);
    }

    public static synchronized void refreshCache(Connection dbc, SignatureParms sigparms) throws Exception {
        refreshCache(dbc, null, false, sigparms);
    }

    public static synchronized void refreshCache(Connection dbc, String sCommaListCorporaThisCall, boolean bSweepFromMemIfNotNeededThisCall, SignatureParms sigparms) throws Exception {
        boolean bVerbose = false;
        boolean bVerbose2 = false;
        if (bVerbose) {
            api.Log.Log(" ------------ PRE REFRESH/UPDATE CACHE list [" + sCommaListCorporaThisCall + "] ------------ ");
        }
        try {   // opbtain write lock and reset variables
            //api.Log.Log ("t[" + Thread.currentThread().getName() + "] hk pre write lock ");
            rwlockcache.writeLock("top of refresh cache");
            {

                Vector vIntCorporaThisCall = null;
                if (sCommaListCorporaThisCall != null) {
                    if (sCommaListCorporaThisCall.trim().equals("")) {
                        vIntCorporaThisCall = new Vector();
                    } else {
                        if (bVerbose) {
                            api.Log.Log("using caller corpus list [" + sCommaListCorporaThisCall + "]");
                        }
                        vIntCorporaThisCall = com.indraweb.util.UtilStrings.splitByStrLen1(sCommaListCorporaThisCall, ",");
                        //convert Strings to Int's
                        for (int i = 0; i < vIntCorporaThisCall.size(); i++) {
                            vIntCorporaThisCall.setElementAt(new Integer((String) vIntCorporaThisCall.elementAt(i)), i);
                        }
                    }
                } else //vIntCorporaThisCall = getVIntCorporaAsPerCfg (dbc);
                {
                    vIntCorporaThisCall = new Vector(vCorpInMemOrderedByInsertTime);
                }

                String sSavePreState = UtilStrings.convertVecToString(vCorpInMemOrderedByInsertTime, ",");
                // convert from vectors to hashsets
                HashSet hsCorpThisCall = new HashSet(vIntCorporaThisCall);
                HashSet hsCorpInMem = new HashSet(vCorpInMemOrderedByInsertTime);
                HashSet hsCorpInvalidated = new HashSet(vIntCorporaInvalidated);

                // build set to be removed from mem not due to (prior) invalidation
                // FIRST GET READY FOR sweep ... CREATE CORPUS FILE COPY


                //HashSet hsInMemAndAreInvalixdatedPre = new HashSet (hsCorpInMem);
                //hsInMemAndAreInvalidatedPre.retainAll (hsCorpInvalidated);

                HashSet hsCorp_WillNeedToBeLoaded = null;
                HashSet hsCorpRemainingUntouchedInMem = null;
                HashSet hsCorp_FinalResult = null;
                HashSet hsCorpInvalidUnionSweepPending = null;
                if (bSweepFromMemIfNotNeededThisCall) {
                    HashSet hsInMemNotNeeded = new HashSet(hsCorpInMem);
                    hsInMemNotNeeded.removeAll(hsCorpThisCall);

                    hsCorpInvalidUnionSweepPending = new HashSet(hsCorpInvalidated);
                    hsCorpInvalidUnionSweepPending.addAll(hsInMemNotNeeded);

                    hsCorpRemainingUntouchedInMem = new HashSet(hsCorpInMem);
                    hsCorpRemainingUntouchedInMem.removeAll(hsCorpInvalidUnionSweepPending);

                    hsCorp_WillNeedToBeLoaded = new HashSet(vIntCorporaThisCall);
                    hsCorp_WillNeedToBeLoaded.removeAll(hsCorpRemainingUntouchedInMem);

                    hsCorp_FinalResult = new HashSet(hsCorpRemainingUntouchedInMem);
                    hsCorp_FinalResult.addAll(hsCorpRemainingUntouchedInMem);
                    hsCorp_FinalResult.addAll(hsCorp_WillNeedToBeLoaded);
                } else // no sweep    // no need to clean
                {


                    hsCorpRemainingUntouchedInMem = new HashSet(hsCorpInMem);
                    hsCorpRemainingUntouchedInMem.removeAll(hsCorpInvalidated);

//                    HashSet hsThisCallnAndNotInMemAndValid = new HashSet (hsCorpThisCall);
//                    hsThisCallAndNotInMemAndValid.removeAll (hsCorpRemainingUntouchedInMem);

                    hsCorp_WillNeedToBeLoaded = new HashSet(hsCorpThisCall);
                    hsCorp_WillNeedToBeLoaded.removeAll(hsCorpRemainingUntouchedInMem);

                    hsCorp_FinalResult = new HashSet(hsCorpRemainingUntouchedInMem);
                    hsCorp_FinalResult.addAll(hsCorpRemainingUntouchedInMem);
                    hsCorp_FinalResult.addAll(hsCorp_WillNeedToBeLoaded);

                    hsCorpInvalidUnionSweepPending = new HashSet(hsCorpInvalidated);
                }


                HashSet hsCorp_InvalidatedButHadNotBeenLoadedSoNeedNoEmptying_delOrphanBackFile = new HashSet(hsCorpInvalidated);
                hsCorp_InvalidatedButHadNotBeenLoadedSoNeedNoEmptying_delOrphanBackFile.removeAll(hsCorpInMem);

                HashSet hsCorpRemoveFromMem = new HashSet(hsCorpInMem);
                hsCorpRemoveFromMem.removeAll(hsCorpRemainingUntouchedInMem);

                if (hsCorpRemoveFromMem.size() > 0) {
                    bCacheFresh = false;
                }

                if (hsCorp_WillNeedToBeLoaded.size() > 0) {
                    bCacheFresh = false;
                }

                api.Log.Log("REFRESH CACHE: pre [" + sSavePreState + "]"
                        + " pre count [" + Data_CorpusCache.getNodeTotal(hsCorpInMem, dbc) + "]"
                        + " " + strHS("current ", hsCorpThisCall)
                        + " # [" + Data_CorpusCache.getNodeTotal(hsCorpThisCall, dbc) + "]"
                        + " " + strHS("to ", hsCorp_FinalResult)
                        + " # [" + Data_CorpusCache.getNodeTotal(hsCorp_FinalResult, dbc) + "]"
                        + " " + strHS("invalid ", hsCorpInvalidated) + " "
                        + " " + strHS("removed ", hsCorpRemoveFromMem)
                        + " # [" + Data_CorpusCache.getNodeTotal(hsCorpRemoveFromMem, dbc) + "]"
                        + " " + strHS("remains ", hsCorpRemainingUntouchedInMem)
                        + " # [" + Data_CorpusCache.getNodeTotal(hsCorpRemainingUntouchedInMem, dbc) + "]"
                        + " " + strHS("load ", hsCorp_WillNeedToBeLoaded) + " "
                        + " # [" + Data_CorpusCache.getNodeTotal(hsCorp_WillNeedToBeLoaded, dbc) + "]");

                int iCountNodes_FinalResult = Data_CorpusCache.getNodeTotal(hsCorp_FinalResult, dbc);

                int iNodeCountTarget = Session.cfg.getPropInt("NodeCacheCountTarget");
                int iNodeCacheCountMax = Session.cfg.getPropInt("NodeCacheCountMax");

                if (iNodeCountTarget > iNodeCacheCountMax) {
                    throw new Exception("invalid config, NodeCacheCountTarget must be < NodeCacheCountMax ");
                }

                if (bVerbose2) {
                    api.Log.Log("cache refresh comparing iCountNodes_FinalResult [" + iCountNodes_FinalResult + "] > iNodeCountTarget [" + iNodeCountTarget + "]");
                }

                if (iCountNodes_FinalResult > iNodeCountTarget) {
                    HashSet hsCorp_ProposeRemoveFromMemForCacheCompress =
                            compressCache(
                            hsCorpThisCall,
                            vCorpInMemOrderedByInsertTime,
                            hsCorp_FinalResult,
                            dbc);

                    hsCorp_FinalResult.removeAll(hsCorp_ProposeRemoveFromMemForCacheCompress);
                    hsCorpRemainingUntouchedInMem.removeAll(hsCorp_ProposeRemoveFromMemForCacheCompress);
                    hsCorp_WillNeedToBeLoaded.removeAll(hsCorp_ProposeRemoveFromMemForCacheCompress);

                    hsCorpRemoveFromMem.addAll(hsCorp_ProposeRemoveFromMemForCacheCompress);
                    hsCorpInvalidUnionSweepPending.addAll(hsCorp_ProposeRemoveFromMemForCacheCompress);

                    /*
                     * if (bVerbose) api.Log.Log ("CACHE POST REFRESH POST COMPRESS");
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorp_FinalResult_savepreCompress " , hsCorp_FinalResult_savepreCompress ));
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorpThisCall" , hsCorpThisCall));
                     *
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorpInMem" , hsCorpInMem));
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorpInvalidated" , hsCorpInvalidated));
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorp_InvalidatedButHadNotBeenLoadedSoNeedNoEmptying_delOrphanBackFile" ,
                     * hsCorp_InvalidatedButHadNotBeenLoadedSoNeedNoEmptying_delOrphanBackFile));
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorpRemoveFromMem" , hsCorpRemoveFromMem));
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorpInvalidUnionSweepPending" , hsCorpInvalidUnionSweepPending));
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorp_WillNeedToBeLoaded" , hsCorp_WillNeedToBeLoaded));
                     * if (bVerbose) api.Log.Log (strHS ("- 2 hsCorpRemainingUntouchedInMem" , hsCorpRemainingUntouchedInMem));
                     *
                     *
                     */


                }  // if cache compress needed


                Session.stopList = new com.indraweb.ir.clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                if (!bCacheFresh) {
                    //api.Log.Log ("@@@ start Node-Sig refreshCache()");

                    boolean bStemOn = Session.cfg.getPropBool("StemOnClassifyNodeDocScoring");
                    //boolean bThesaurusUse = Session.cfg.getPropBool ("cfg_classification_UseThesaurusInScoring");
                    boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");


                    {
                        // first get the thesauri into cache

                        //api.Log.Log ("ccc" + hsToStr(hsCorp_WillNeedToBeLoaded, ",", false));
                        Iterator iterIntCorpora_ToBeLoaded = hsCorp_WillNeedToBeLoaded.iterator();
                        while (iterIntCorpora_ToBeLoaded.hasNext()) {
                            // simply put into cache
                            int iCorpusID = ((Integer) iterIntCorpora_ToBeLoaded.next()).intValue();
                            // Thesaurus.cacheInvalidate (iCorpusID , true);
                            Thesaurus.cacheGetThesaurus(bStemOn,
                                    iCorpusID,
                                    true,
                                    dbc);
                        }
                        //htCorpusIDsCovered_IfWantThesaurus = NodeForScore.getHTCorpusIDsCovered();
                    }

                    // reload all corpora marked old
                    try {

                        if (bCacheBeenInitialiedOnce) {
                            Vector vIntCorporaRemoveFromMem = new Vector(hsCorpRemoveFromMem);
                            vCorpInMemOrderedByInsertTime.removeAll(vIntCorporaRemoveFromMem);
                            // EMPTY
                            Data_NodeIDsTo_NodeForScore.emptyOrFillData(
                                    Session.cfg.getPropBool("StemOnClassifyNodeDocScoring"),
                                    com.indraweb.util.UtilSets.convertVectorTo_intArray(vIntCorporaRemoveFromMem),
                                    dbc,
                                    bAffinityNodeTitlesOnly,
                                    -1,
                                    true, // empty
                                    hsCorpInvalidated,
                                    sigparms, /// no sig parms needed if emptying
                                    new ParseParms(ParseParms.getDelimitersAll_Default(), false, -1) // take defaults - needed for phrase decomp affinity - must be same as empty
                                    );

                            //Data_WordsToNodes.printCacheContents("post empty");

                        }
                        Vector vIntCorpora_ToBeLoadedAfterEmptyPass = new Vector(hsCorp_WillNeedToBeLoaded);
                        // mark all as currently being accessed
                        for (int i = 0; i < vIntCorpora_ToBeLoadedAfterEmptyPass.size(); i++) {
                            Integer ICorpusID = (Integer) vIntCorpora_ToBeLoadedAfterEmptyPass.elementAt(i);
                            htCorpusIDToLongTimeLoaded.put(ICorpusID, new Long(System.currentTimeMillis()));
                        }
                        // add to the list of in mem corpora
                        vCorpInMemOrderedByInsertTime.addAll(vIntCorpora_ToBeLoadedAfterEmptyPass);
                        Data_NodeIDsTo_NodeForScore.emptyOrFillData(
                                Session.cfg.getPropBool("StemOnClassifyNodeDocScoring"),
                                com.indraweb.util.UtilSets.convertVectorTo_intArray(vIntCorpora_ToBeLoadedAfterEmptyPass),
                                dbc,
                                bAffinityNodeTitlesOnly,
                                -1,
                                false, // FILL
                                null, // not needed on a load sweep
                                sigparms, // take all defaults
                                new ParseParms(ParseParms.getDelimitersAll_Default(), false, -1) // take defaults - for title
                                );


                    } catch (Throwable t) {
                        Log.NonFatalError("from throwable in verifyCacheUpToDate", t);
                        throw new Exception("see log for inner throwable stacktrace msg " + t.getMessage());
                    }

                    bCacheFresh = true;
                    vIntCorporaInvalidated.clear();
                }

            }
        } finally {
            //api.Log.Log ("hbk starting GC");
            //Runtime.getRuntime().gc();
            rwlockcache.done("bottom of refreshCache");
            bCacheBeenInitialiedOnce = true;
            if (bVerbose) {
                api.Log.Log(" --- DONE REFRESH/UPDATE CACHE --- #nodes in mem [" + Data_NodeIDsTo_NodeForScore.getNumNodes() + "]");
            }

        }
    } // refreshCache
//
//    static public synchronized int[] getintArrCorporaAsPerCfg (Connection dbc , boolean bCacheReadThrough)  // fn:
//            // ******************************************
//            throws Exception
//    {
//        try
//        {
//            //rwlockcache.readLock(dbc);
//            Vector v = getVIntCorporaAsPerCfg (dbc);
//            return com.indraweb.util.UtilSets.convertVectorTo_intArray (v);
//        } finally
//        {
//            //rwlockcache.done();
//        }
//    }

    /*
     * static public Vector getVIntCorporaAsPerCfg (Connection dbc) // fn:
     * // ******************************************
     * throws Exception
     * {
     * return getVIntCorporaAsPerCfg (dbc);
     * return getVIntCorporaAsPerCfg (dbc, true);
     * }
     */
    //static public Vector getVIntCorporaAsPerCfg (Connection dbc, boolean bObtainReadlock )  // fn:
//    static public synchronized Vector getVIntCorporaAsPerCfg ()  // fn:
//            // ******************************************
//            throws Exception
//    {
//        return getVIntCorporaAsPerCfg (null); // should  be loaded already
//    }
//
//    static public synchronized Vector getVIntCorporaAsPerCfg (Connection dbc)  // fn:
//            // ******************************************
//            throws Exception
//    {
//        // get corpora
//        try
//        {
//            //if ( bObtainReadlock )
//            //  rwlockcache.readLock(dbc, false);
//
//            if (vIntCorporaAsPerCfg == null)
//            {
//
//                //Vector VIntCorpusIDs = null;
//                String sCommaListCorpusIDs = com.indraweb.execution.Session.cfg.getProp ("cfg_classification_CommaDelimCorporaToUse");
//
//                if (com.indraweb.util.UtilFile.bFileExists ("/temp/IndraDebug_CorpusList.txt"))
//                {
//                    Vector vFileLine = com.indraweb.util.UtilFile.getVecOfFileStrings ("/temp/IndraDebug_CorpusList.txt");
//                    sCommaListCorpusIDs = (String) vFileLine.elementAt (0);
//                    Session.cfg.setProp ("cfg_classification_CommaDelimCorporaToUse" , sCommaListCorpusIDs);
//                    api.Log.Log ("CAUTION CORPUS SET OVERRIDE: /temp/IndraDebug_CorpusList.txt file present, OVERRIDING DB CONFIG CORPUS LIST");
//                    //sCommaListCorpusIDs = "100006";
//                }
//
//                sCommaListCorpusIDs = com.indraweb.util.UtilStrings.replaceStrInStr (sCommaListCorpusIDs , " " , "");
//                //com.indraweb.util.Log.log ( "sCommaListCorpusIDs [" + sCommaListCorpusIDs + "]\r\n" ); ;
//
//                if (sCommaListCorpusIDs.trim ().equals ("-1"))
//                {
//                    String sql = "select corpusid from corpus where corpusactive > 0 order by corpusid";
//
//                    Vector vDBres = JDBCIndra_Connection.ExecuteDBQueryVecOfVecsReturned (sql , dbc , -1);
//                    Vector vCorpusIds = (Vector) vDBres.elementAt (0);
//                    StringBuffer sbCorpusListcommas = new StringBuffer ();
//                    for ( int i = 0 ; i < vCorpusIds.size () ; i++ )
//                    {
//                        int icorpusid = ( (java.math.BigDecimal) vCorpusIds.elementAt (i) ).intValue ();
//                        if (i > 0)
//                            sbCorpusListcommas.append (",");
//
//                        sbCorpusListcommas.append ("" + icorpusid);
//                    }
//                    sCommaListCorpusIDs = sbCorpusListcommas.toString ();
//                } // if == -1 and take all corpora
//
//                vIntCorporaAsPerCfg = com.indraweb.util.UtilStrings.splitByStrLen1 (sCommaListCorpusIDs , ",");
//                for ( int i = 0 ; i < vIntCorporaAsPerCfg.size () ; i++ )
//                {
//                    vIntCorporaAsPerCfg.setElementAt (new Integer ((String) vIntCorporaAsPerCfg.elementAt (i)) , i);
//
//                }
//            }
//            return vIntCorporaAsPerCfg;
//        } finally
//        {
//            //if ( bObtainReadlock )
//            //  rwlockcache.done();
//        }
//    }
    private static final Object oSynchCacheInit = new Object();
    static int iCallCount = 0;

    //private static int iNodeClassifyCountAccumTotal = 0;
// ******************************************
    public static synchronized void accumulateNFS_ContainingWords(
            Enumeration enumDocWords, // fn:
            HashSet hsNodeIDsOrNodeForScoresThatContainWord_outparm,
            Hashtable htCorporaToConsider,
            IntContainer intContainer_NumNodes_outParmAccounting,
            Connection dbc,
            boolean bThesaurusUse,
            boolean bStemOn,
            boolean bProp_sweepCache,
            SignatureParms sigparms)
            // ******************************************
            throws Throwable {
        iCallCount++;
        boolean bVerbose = false;


        try {
            DoRefreshIfNeeded(htCorporaToConsider, bProp_sweepCache, dbc, sigparms);

            intContainer_NumNodes_outParmAccounting.setiVal(Data_NodeIDsTo_NodeForScore.getNumNodes());
            //if ( false && bWANT_DATA_STRUCT_DETAIL_TO_LOG )

            int j = -1;
            //Data_WordsToNodes.printDataStructWordsToNodes("xx", bStemOn);
            while (enumDocWords.hasMoreElements()) {
                j++;
                String sDocWord = (String) enumDocWords.nextElement();
                Vector vOfNodeIDsWithThisWordAsaSig = (Vector) htWordsToVecOfIntegerNode.get(sDocWord);
//                if ( bVerbose )
//                {
                //api.Log.Log ("A. accumulate nodes checking wd [" + sDocWord + "]"); // hbk control comment for production
//                }

                if (vOfNodeIDsWithThisWordAsaSig != null) {

//                    if (bVerbose)
                    //api.Log.Log (j + "B. accumulate FOUND a vec with [" + vOfNodeIDsWithThisWordAsaSig.size () +
                    //        "] nodes for wd [" + sDocWord + "]"); // hbk control comment for production

                    Enumeration enumNodesThisDocWord = vOfNodeIDsWithThisWordAsaSig.elements();
                    int i = 0;
                    while (enumNodesThisDocWord.hasMoreElements()) {
                        i++;
                        // hbk 2004 03 08

                        if (bVectorNodesIsNFS) {
                            //Integer ICorpusID = (Integer) htNodeIDToCorpusID.get (new Integer ((int) nfs.getNodeID()));
                            NodeForScore nfs = (NodeForScore) enumNodesThisDocWord.nextElement();
                            if (nfs.getCorpusID() < 0) {
                                throw new Exception("why nfs.getCorpusID() < 0");
                            }
                            //if ( ICorpusID == null )
                            //    api.Log.LogError("ICorpusID == null  for node [" + nfs.getNodeID() + "]" );
                            if (htCorporaToConsider == null || htCorporaToConsider.get(new Integer(nfs.getCorpusID())) != null) {
                                //if ( true && bWANT_DATA_STRUCT_DETAIL_TO_LOG )
                                //if ( INodeID.intValue() == 3328 )

                                if (bVerbose) {
                                    api.Log.Log("C. " + i + ". wd [" + sDocWord + "] matched node [" + nfs.getNodeID() + "] "); // hbk control comment for production
                                }
                                hsNodeIDsOrNodeForScoresThatContainWord_outparm.add(
                                        Data_NodeIDsTo_NodeForScore.getNodeForScore(new Integer((int) nfs.getNodeID()), true));

                            }
                        } else {
                            Integer INodeID = (Integer) enumNodesThisDocWord.nextElement();
                            Integer ICorpusID = (Integer) htNodeIDToCorpusID.get(INodeID);
                            if (ICorpusID == null) {
                                throw new Exception("someone dumped node to corpus ICorpusID == null ");
                            }
                            if (htCorporaToConsider == null || htCorporaToConsider.get(ICorpusID) != null) {
                                //if ( true && bWANT_DATA_STRUCT_DETAIL_TO_LOG )
                                //if ( INodeID.intValue() == 3328 )
                                //api.Log.Log ("C. " + i + ". wd [" + sWord + "] matched node [" + INodeID + "] "); // hbk control comment for production

                                hsNodeIDsOrNodeForScoresThatContainWord_outparm.add(
                                        Data_NodeIDsTo_NodeForScore.getNodeForScore(
                                        INodeID, true));

                            }
                        }

                    }
                } //else
                //api.Log.Log (j + ". accumulate found no vec for wd [" + sWord + "]"); // hbk control comment for production

                //com.indraweb.util.Log.logClear ("done looking for accum wd [" + sWord + "] set size [" + htNodeIDsThatContainWord_self.size() + "]"); // hbk control comment for production
            }
        } finally {
            //iNodeClassifyCountAccumTotal += htNodeIDToCorpusID.size();

            /*
             * api.Log.Log ("hbk done accum htNodeIDToCorpusID.size() ["
             * + htNodeIDToCorpusID.size() + "] iNodeClassifyCountAccumTotal [" + iNodeClassifyCountAccumTotal + "]" );
             */
            //api.Log.Log ("at end accumulateNFS_ContainingWords [" +
            //      hsNodeIDsOrNodeForScoresThatContainWord_outparm.size() + "]" );
        }
    }

    // fn: ******************************************
    //private static Integer IDebug = new Integer (0);
    //private static int iNodesCompleted = 0;
    static int fillData_WordsToNodeAffinities_ForANodeSet(
            HashSet hsNodeIDs,
            boolean bTitlesOnlyForAffinity,
            boolean bIncludeParentNFSTerms,
            boolean bTitlesOnlyForParentAffinity,
            boolean bStemOn,
            String sPhraseToWordSplitterDelims)
            throws Throwable {
        api.Log.Log("filling affinity struct for [" + hsNodeIDs.size() + "] nodes bTitlesOnlyForAffinity [" + bTitlesOnlyForAffinity + "]");
        int iNumAffinityTermsAdded = 0;
        String sDelimitersText = ParseParms.getDelimitersAll_Default();

//Log.log ("/temp/IndraControlNoParentAffinity.txt not present, using parent affinity");
        Iterator iterNodeIDs = hsNodeIDs.iterator();
        int iLoop = -1;
        while (iterNodeIDs.hasNext()) {
            iLoop++;
            Integer INodeID = ((Integer) iterNodeIDs.next());
            NodeForScore nfs = Data_NodeIDsTo_NodeForScore.getNodeForScore(INodeID, true);
            HashSet hsAffinityTermsBase = nfs.getAffinityTerms(
                    bTitlesOnlyForAffinity, bStemOn, sPhraseToWordSplitterDelims);

            //api.Log.Log ("affinity terms this node [" + INodeID + "] [" + UtilSets.hsToStr(hsAffinityTermsBase)+ "]" );
            iNumAffinityTermsAdded += hsAffinityTermsBase.size();
            //api.Log.Log ("iLoop  [" + iLoop  + "] INodeID [" + INodeID + "] iNumAffinityTermsAdded [" + iNumAffinityTermsAdded +"] hsAffinityTermsBase [" + UtilSets.hsToStr(hsAffinityTermsBase,",",false) + "]" );
            fillData_AffinityWordsToVecNodeIDs(INodeID, hsAffinityTermsBase);


            if (bIncludeParentNFSTerms) {
                if (nfs.getParentNodeID() > 0) {
                    Integer INodeIDParent = new Integer((int) nfs.getParentNodeID());
                    NodeForScore nfsParent = Data_NodeIDsTo_NodeForScore.getNodeForScore(INodeIDParent, true);
                    HashSet hsAffinityTermsParent = nfsParent.getAffinityTerms(
                            bTitlesOnlyForAffinity, bStemOn, sPhraseToWordSplitterDelims);
                    iNumAffinityTermsAdded += hsAffinityTermsParent.size();
                    //api.Log.Log ("iLoop  [" + iLoop  + "] INodeID [" + INodeID + "] iNumAffinityTermsAdded [" + iNumAffinityTermsAdded +"] hsAffinityTermsParent [" + UtilSets.hsToStr(hsAffinityTermsParent,",",false) + "]" );
                    fillData_AffinityWordsToVecNodeIDs(INodeID, hsAffinityTermsParent);
                }
// 2. For each word in the parent's node
            } // if parentID > 0
        } // 2004 while enum all nodes in mem

        return iNumAffinityTermsAdded;
    }

    private static void fillData_AffinityWordsToVecNodeIDs(Integer INodeID,
            HashSet hsStrAffinityTerms) {

        Iterator iterAffinityWords = hsStrAffinityTerms.iterator();
        while (iterAffinityWords.hasNext()) {
            String sAffinityWord = (String) iterAffinityWords.next();
            Vector vIntegerNodesForThisWord_pre = (Vector) htWordsToVecOfIntegerNode.get(sAffinityWord);
            if (vIntegerNodesForThisWord_pre == null) {
                vIntegerNodesForThisWord_pre = new Vector();
                htWordsToVecOfIntegerNode.put(sAffinityWord, vIntegerNodesForThisWord_pre);
            }
            //api.Log.Log ("node [" + INodeID + "] filling affinity term [" + sAffinityWord + "]" );
            vIntegerNodesForThisWord_pre.addElement(INodeID);
        }
    }

    static void emptyData_AffinityWordsToVecNFS(Integer INodeID, HashSet hsStrAffinityTerms) throws Exception {

        //api.Log.Log ("in emptyData_AffinityWordsToVecNFS INodeID [" + INodeID + "] ");
        Iterator iterAffinityWords = hsStrAffinityTerms.iterator();
        while (iterAffinityWords.hasNext()) {
            String sAffWord = (String) iterAffinityWords.next();
            //api.Log.Log ("remove affinity term for node [" + sAffWord + "] affinity term [" + sAffWord + "]" );
            Vector vIntegerNodesForThisWord = (Vector) htWordsToVecOfIntegerNode.get(sAffWord);
            if (vIntegerNodesForThisWord == null) {
                throw new Exception("affinity not there sAffWord [" + sAffWord + "] to node [" + INodeID + "]");
            }

            vIntegerNodesForThisWord.remove(INodeID);

            if (vIntegerNodesForThisWord.isEmpty()) {
                htWordsToVecOfIntegerNode.remove(sAffWord);
            }
        }
    }

// ******************************************
    public static synchronized void invalidateNodeSigCache_byCorpus(String sReason, int iCorpusID) throws Exception {
        invalidateNodeSigCache_byCorpus(sReason, iCorpusID, true);
        invalidateNodeSigCache_byCorpus(sReason, iCorpusID, false);

    }

    public static synchronized void invalidateNodeSigCache_byCorpus(String sReason, int iCorpusID, boolean bStem) throws Exception {
        //boolean bVerbose = false;
        api.Log.Log("in sig cache invalidate, reason  [" + sReason + "] iCorpusID [" + iCorpusID + "]");
        String sfnameSigWordCountsFile = SignatureFile.getFileName(iCorpusID, bStem);
        {
            try {
                rwlockcache.writeLock("top invalidateNodeSigCache_byCorpus ");
                // Thesaurus.cacheInvalidate (iCorpusID , true);
                Data_CorpusCache.invalidateCache(new Integer(iCorpusID));
                if (UtilFile.bFileExists(sfnameSigWordCountsFile)) {
                    bCacheFresh = false;
                    vIntCorporaInvalidated.addElement(new Integer(iCorpusID));
                    /*
                     * if (UtilFile.bFileExists (sfnameSigWordCountsFile_newName))
                     * {
                     * UtilFile.deleteFile (sfnameSigWordCountsFile_newName);
                     * }
                     */
                    UtilFile.deleteFile(sfnameSigWordCountsFile);
                    if (UtilFile.bFileExists(sfnameSigWordCountsFile)) {
                        throw new Exception("unable to remove sig file [" + sfnameSigWordCountsFile + "]");
                    }
                    /*
                     * if (bVerbose)
                     * api.Log.Log ("completed file rename for corpus [" + iCorpusID + "]");
                     */

// this step is formally needed but for speed we can estimate corpora do not change size
//Data_CorpusCache.invalidateCache(new Integer (iCorpusID) );
                }
                //else
                //  api.Log.Log ("no file to invalidate / remove for corpus [" + iCorpusID + "]");
            } finally {
                rwlockcache.done("bottom of invalidateNodeSigCache_byCorpus ");
            }
        }
    }

    /*
     * public static void invalidateNodeSigCache_byNode (int iNodeID) throws Exception
     * {
     * rwlockcache.writeLock();
     * bCacheFresh = false;
     * rwlockcache.done();
     * }
     */
    public static synchronized void vTruncate(Vector v, int iFinalSize) {
        for (int i = v.size() - 1; i >= iFinalSize; i--) {
            v.removeElementAt(i);
        }
    }

    public static Hashtable gethtNodeIDToCorpusID() throws Exception {
        try {
//rwlockcache.readLock(null);
            return htNodeIDToCorpusID;
        } finally {
//rwlockcache.done();
        }
    }

    private static String strHS(String sDesc, HashSet hs) {
        return sDesc + hsToStr(hs, ",", false);
    }

    public static void DoRefreshIfNeeded(Hashtable htCorporaToConsider, boolean bProp_sweepCache, Connection dbc, SignatureParms sigparms) throws Exception {

        rwlockcache.readLock("top of DoRefreshIfNeeded");
        synchronized (oSynchCacheInit) //
        {
// reset ordering to order of use
            HashSet hsCorpInMemAndFreshOverlap = new HashSet(vCorpInMemOrderedByInsertTime);
            hsCorpInMemAndFreshOverlap.removeAll(vIntCorporaInvalidated);

            String sInMemOrdered_PRE = UtilStrings.convertVecToString(vCorpInMemOrderedByInsertTime, ",");
            HashSet hsCorporaToConsider = UtilSets.convertHTKeysToHashSet(htCorporaToConsider);

            Vector vCorporaToConsider = UtilSets.convertHTKeysToVector(htCorporaToConsider);
            HashSet hsMissing = new HashSet(vCorporaToConsider);
            hsMissing.removeAll(hsCorpInMemAndFreshOverlap);

// if (bVerbose1) api.Log.Log ("hsmissing  [" + hsToStr(hsMissing, ",", false)+ "]");
            if (hsMissing.size() > 0) {
                try {
                    rwlockcache.done("within DoRefreshIfNeeded descend for corpora");
                    refreshCache(dbc, UtilStrings.convertVecToString(vCorporaToConsider, ","), bProp_sweepCache, sigparms);
                } finally {
                    rwlockcache.readLock("end within DoRefreshIfNeeded descend for corpora");
                }
            }
            vCorpInMemOrderedByInsertTime.removeAll(hsCorporaToConsider);
            vCorpInMemOrderedByInsertTime.addAll(hsCorporaToConsider);
            String sInMemOrdered_POST = UtilStrings.convertVecToString(vCorpInMemOrderedByInsertTime, ",");
            if (hsMissing.size() > 0) {
                api.Log.Log(
                        "CACHE: in mem [" + sInMemOrdered_PRE
                        + "] invalid [" + UtilStrings.convertVecToString(vIntCorporaInvalidated, ",")
                        + "] needed [" + UtilStrings.convertVecToString(vCorporaToConsider, ",")
                        + "] to load [" + hsToStr(hsMissing, ",", false)
                        + "] result [" + sInMemOrdered_POST
                        + "]");
            }
        }

    }

    private static HashSet compressCache(
            HashSet hsCorpThisCall_,
            Vector vCorpInMemOrderedByInsertTime_,
            HashSet hsCorp_FinalResult_,
            Connection dbc) throws Exception {

        boolean bVerbose = false;

        String sCommaCorporaCachePinned = Session.cfg.getProp("CorporaCachePinned");
        HashSet hsCorporaCachePinned = null;
        if (!sCommaCorporaCachePinned.trim().equals("")) {
            hsCorporaCachePinned = new HashSet();
            String[] sArrCorporaCachePinned = sCommaCorporaCachePinned.split(",");
            if (sArrCorporaCachePinned.length > 0) {
                if (!sArrCorporaCachePinned[0].trim().equals("")) {
                    for (int i = 0; i < sArrCorporaCachePinned.length; i++) {
                        hsCorporaCachePinned.add(new Integer(sArrCorporaCachePinned[i]));
                    }
                }
            }
        }

        int iNodeCountFromCallerCorpusSet = Data_CorpusCache.getNodeTotal(hsCorp_FinalResult_, dbc);
        HashSet hsCorp_FinalResult_PostCompress = new HashSet(hsCorp_FinalResult_);
        int iCurrentProposedCount_Removed = -1;
        HashSet hsCorp_inMemPreThisRefresh = new HashSet(vCorpInMemOrderedByInsertTime_);
        HashSet hsCorp_CandidatesForRemoval = new HashSet(vCorpInMemOrderedByInsertTime_);
        hsCorp_CandidatesForRemoval.removeAll(hsCorpThisCall_);
        if (hsCorporaCachePinned != null) {
            hsCorp_CandidatesForRemoval.removeAll(hsCorporaCachePinned);
        }

        /*
         * if ( bVerbose) api.Log.Log ("hsCorp_CandidatesForRemoval set [" + hsToStr ( hsCorp_CandidatesForRemoval, ",", false ) + "]" );
         * if ( bVerbose) api.Log.Log ("hsCorp_CandidatesForRemoval count [" + Data_CorpusCache.getNodeTotal(hsCorp_CandidatesForRemoval, dbc) + "]" );
         */
        HashSet hsCorp_ProposeRemovalFromMemForCacheCompress = new HashSet();
        Vector vProposeRemovalFromMemForCacheCompress = new Vector();

        int iNodeCountTarget = Session.cfg.getPropInt("NodeCacheCountTarget");
        int iNodeCountMax = Session.cfg.getPropInt("NodeCacheCountMax");

        boolean bSuccessfullyCompressedCached = false; // pessimistic default
        int iCurrentProposedCount = Data_CorpusCache.getNodeTotal(hsCorp_FinalResult_, dbc);

        for (int iCorpusIndex = 0; iCorpusIndex < vCorpInMemOrderedByInsertTime_.size(); iCorpusIndex++) {
            Integer ICorpusID = (Integer) vCorpInMemOrderedByInsertTime_.elementAt(iCorpusIndex);

            if (hsCorp_CandidatesForRemoval.contains(ICorpusID)) {
                int iCountDecreaseFromDeletingThisCorpus = Data_CorpusCache.getCorpusSize(ICorpusID, dbc);
                int iCountFinalAfterRemoved = iCurrentProposedCount - iCountDecreaseFromDeletingThisCorpus;

                hsCorp_ProposeRemovalFromMemForCacheCompress.add(ICorpusID);
                vProposeRemovalFromMemForCacheCompress.addElement("R" + ICorpusID);


                if (iCountFinalAfterRemoved < iNodeCountTarget) {
                    bSuccessfullyCompressedCached = true;
                    break;
                }

            } else {
                if (bVerbose) {
                    api.Log.Log("iCorpusIndex [" + iCorpusIndex + "] NOT candidate - not checking cache removal impact corpus [" + ICorpusID + "]");
                }
                vProposeRemovalFromMemForCacheCompress.addElement("K" + ICorpusID);


            }

            iCurrentProposedCount_Removed = Data_CorpusCache.getNodeTotal(hsCorp_ProposeRemovalFromMemForCacheCompress, dbc);
            iCurrentProposedCount = iNodeCountFromCallerCorpusSet - iCurrentProposedCount_Removed;
        }

        if (iCurrentProposedCount <= iNodeCountMax) {
            bSuccessfullyCompressedCached = true;
        }

        if (!bSuccessfullyCompressedCached) {
            int iNeeded = Data_CorpusCache.getNodeTotal(hsCorpThisCall_, dbc);
            String sErr = "EXCEEDED CACHE: max [" + iNodeCountMax + "] "
                    + " corpora needed [" + hsToStr(hsCorpThisCall_, ",", false) + "] "
                    + " # [" + iNeeded + "] "
                    + " short by # [" + (iNeeded - iNodeCountMax) + "] ";

            api.Log.Log("sErr  [" + sErr + "]");

            throw new Exception(sErr);
        }

        hsCorp_FinalResult_PostCompress.removeAll(hsCorp_ProposeRemovalFromMemForCacheCompress);
        if (hsCorp_ProposeRemovalFromMemForCacheCompress.size() > 0) {
            api.Log.Log("COMPRESS CACHE: "
                    + " tgt [" + iNodeCountTarget + "] "
                    + " max [" + iNodeCountMax + "] "
                    + " refresh recommendation [" + hsToStr(hsCorp_FinalResult_, ",", false) + "] "
                    + " keep remove in eval order [" + UtilStrings.convertVecToString(vProposeRemovalFromMemForCacheCompress, ",") + "] "
                    + " # [" + Data_CorpusCache.getNodeTotal(hsCorp_FinalResult_, dbc) + "] "
                    + " in mem pre refresh in order [" + UtilStrings.convertVecToString(vCorpInMemOrderedByInsertTime_, ",") + "] "
                    + " # [" + Data_CorpusCache.getNodeTotal(hsCorp_inMemPreThisRefresh, dbc) + "] "
                    + " current [" + hsToStr(hsCorpThisCall_, ",", false) + "] "
                    + " # [" + Data_CorpusCache.getNodeTotal(hsCorpThisCall_, dbc) + "] "
                    + " remove [" + hsToStr(hsCorp_ProposeRemovalFromMemForCacheCompress, ",", false) + "] "
                    + " # [" + Data_CorpusCache.getNodeTotal(hsCorp_ProposeRemovalFromMemForCacheCompress, dbc) + "] "
                    + " post [" + hsToStr(hsCorp_FinalResult_PostCompress, ",", false) + "] "
                    + " # [" + Data_CorpusCache.getNodeTotal(hsCorp_FinalResult_PostCompress, dbc) + "] ");
        } else {
            api.Log.Log("COMPRESS CACHE: removes none");
        }


        return hsCorp_ProposeRemovalFromMemForCacheCompress;

    } // compressCache

    public static synchronized void printDataStructWordsToNodes(String sDesc, boolean bStem) throws Throwable {
        //boolean bVerbose = false;
        {
            api.Log.Log("in printDataStructWordsToNodes count words [" + Data_WordsToNodes.htWordsToVecOfIntegerNode.size() + "]");

// 1 WORDS -> NODEID
            Enumeration e = Data_WordsToNodes.htWordsToVecOfIntegerNode.keys();
            int i = 0;
            while (e.hasMoreElements()) {
                String sWord = (String) e.nextElement();
                Vector vNodeIDs = (Vector) Data_WordsToNodes.htWordsToVecOfIntegerNode.get(sWord);
                api.Log.Log("AA. Word " + i + ". from TERMS->vNodeIDs. sWord [" + sWord + "]->" + UtilSets.vToStr(vNodeIDs) + ";");
                i++;
            }

// 2 NODEID -> NFS
            Hashtable htNodeIDToNodeForScore = Data_NodeIDsTo_NodeForScore.getMainHT_htNodeIDToNodeForScore();
            Enumeration e2 = htNodeIDToNodeForScore.keys();
            int j = 0;
            while (e2.hasMoreElements()) {
                Integer INodeID = (Integer) e2.nextElement();
                NodeForScore nfs = (NodeForScore) htNodeIDToNodeForScore.get(INodeID);
                boolean bNFSFound = (nfs != null);
                api.Log.Log("BB. Node " + j + ". from N->NFS, nodeid:" + INodeID + " bNFSFound [" + bNFSFound + "] nfs.sarr ["
                        + UtilStrings.convertSArrToString(nfs.getSarr(bStem, true, true), ",") + "]");

                j++;
            }

// 3 NODEID -> Corpusid
//Hashtable hsNodeIDToCorpusID = Data_NodeIDsTo_NodeForScore.getInstance().getMainHT_htNodeIDToNodeForScore();
            Enumeration e3 = Data_WordsToNodes.htNodeIDToCorpusID.keys();
            int k = 0;
            while (e3.hasMoreElements()) {
                Integer INodeID = (Integer) e3.nextElement();
                Integer ICorpusID = (Integer) Data_WordsToNodes.htNodeIDToCorpusID.get(INodeID);
                boolean bCorpusFound = (ICorpusID != null);
                api.Log.Log("CC. Node " + k + ". from N->C, nodeid:" + INodeID + " Found [" + bCorpusFound + "] ICorpusID [" + ICorpusID + "]");

                k++;
            }
            api.Log.Log("END DATA STRUCT DUMP [" + sDesc + "] WORDS TO NODES ******************************** ");
        }
    }

    public static String hsToStr(HashSet hs, String sDelim, boolean bClean) {
        Iterator I = hs.iterator();
        StringBuilder sb = new StringBuilder();
        int iCount = 0;
        while (I.hasNext()) {
            if (iCount > 0) {
                sb.append(sDelim);
            }
            sb.append(I.next().toString());
            iCount++;

        }
        if (bClean) {
            return sb.toString();
        } else {
            return "HS [" + iCount + "] <" + sb.toString() + ">";
        }
    }
}
