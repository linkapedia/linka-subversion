package com.iw.classification;

import com.indraweb.ir.ParseParms;
import com.indraweb.signatures.FillDataFromDBWriteFileAsWell;
import com.indraweb.signatures.FillDataFromFile;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.signatures.WriteFileFromAllCorpusNSFInMem;
import com.indraweb.util.UtilSets;
import com.iw.scoring.NodeForScore;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * per corpus save word stat's
 *
 */
public class Data_NodeIDsTo_NodeForScore {

    private static Hashtable htNodeIDToNodeForScore = new Hashtable();
    private static Hashtable htCorpusIDToHSNodeIDs = new Hashtable();

    public static Hashtable getMainHT_htNodeIDToNodeForScore() {
        return htNodeIDToNodeForScore;
    }

    static synchronized int emptyOrFillData(boolean bStemOn, // fn:
            int[] iArrCorpusIDs,
            Connection dbc,
            boolean bTitlesOnlyForAffinity,
            int iMaxNumNodesWanted,
            boolean bEmptyingIfTrue,
            HashSet hsCorpEmptyingDueToAnInvalidate,
            SignatureParms sigparms,
            ParseParms parseparms)
            throws Throwable {
        boolean bSigsIndludeParentTitleAtall = (sigparms.getNumParentTitleTermsAsSigs() >= 1);
        if (!bEmptyingIfTrue) // FILLING
        {
            //String sArrCorpusIDsComma = UtilSets.convertiArrayToStringWithDelims (iArrCorpusIDs , ",");
            for (int iCorpusIndex = 0; iCorpusIndex < iArrCorpusIDs.length; iCorpusIndex++) {
                int iCorpusID = iArrCorpusIDs[iCorpusIndex];
                // api.Log.Log ( "start corpus fill [" + iCorpusID + "]" );

                // build N -> NFS, corpus -> hsnodes, and nodeid -> corpusid
                int iNumNodes = fillaCorpus_createsFileIfNotThere(iCorpusID, dbc, bStemOn, sigparms, parseparms);
                //api.Log.Log ( "done corpus fill node portion 1/2 [" + iCorpusID + "] # nodes [" + iNumNodes + "]" );

                // now build words
                HashSet hsNodeIdsThisCorpus = (HashSet) htCorpusIDToHSNodeIDs.get(new Integer(iCorpusID));
                if (hsNodeIdsThisCorpus != null) {
                    // FILLING
                    int iNumAffinityTerms = Data_WordsToNodes.fillData_WordsToNodeAffinities_ForANodeSet(
                            hsNodeIdsThisCorpus,
                            bTitlesOnlyForAffinity,
                            bSigsIndludeParentTitleAtall,
                            true, // bTitlesOnlyForParentAffinity,
                            bStemOn,
                            parseparms.getDelimiters());
                    api.Log.Log("done fill words corpus [" + iCorpusID + "] iNumAffinityTerms [" + iNumAffinityTerms + "]");
                } else {
                    api.Log.Log("WARNING: no nodes with sigs this corpus 2/2 [" + iCorpusID + "]");
                }
            }
            // debug

        } else {
            // EMPTYING
            // for each corpus to empty out
            for (int iCorpusIndex = 0; iCorpusIndex < iArrCorpusIDs.length; iCorpusIndex++) {
                int iCorpusID = iArrCorpusIDs[iCorpusIndex];

                // get the set of node id's
                HashSet hsNodeIDsToRemove = (HashSet) htCorpusIDToHSNodeIDs.get(new Integer(iCorpusID));
                Iterator iterNodeIdsToRemove = hsNodeIDsToRemove.iterator();

                while (iterNodeIdsToRemove.hasNext()) {
                    Integer INodeIDToRemove = (Integer) iterNodeIdsToRemove.next();
                    //api.Log.Log ("start remove node from mem corpus [" + iCorpusID + "] htCorpusIDToHSNodeIDs.size() [" + htCorpusIDToHSNodeIDs.size() + "] hsNodeIDsToRemove.size() [" + hsNodeIDsToRemove.size() + "] nodeid [" + INodeIDToRemove + "]" );
                    // for each nodeid - empty the word struct and parent words too
                    NodeForScore nfsToRemove = (NodeForScore) htNodeIDToNodeForScore.get(INodeIDToRemove);
                    if (nfsToRemove == null) {

                        api.Log.Log("missing nfs to remove [" + INodeIDToRemove + "]");
                        throw new Exception("missing nfs to remove [" + INodeIDToRemove + "]");
                    }
                    HashSet hsAffinityTerms = null;
                    hsAffinityTerms = nfsToRemove.getAffinityTerms(bTitlesOnlyForAffinity, bStemOn, null);// nfs must already have it 

                    Data_WordsToNodes.emptyData_AffinityWordsToVecNFS(INodeIDToRemove, hsAffinityTerms);

                    if (bSigsIndludeParentTitleAtall) {
                        NodeForScore nfsParentToRemove = (NodeForScore) htNodeIDToNodeForScore.get(
                                new Integer((int) nfsToRemove.getParentNodeID()));

                        if (nfsParentToRemove != null) {
                            HashSet hsAffinityTermsParent = nfsParentToRemove.getAffinityTerms(
                                    bTitlesOnlyForAffinity,
                                    bStemOn,
                                    parseparms.getDelimiters());

                            Data_WordsToNodes.emptyData_AffinityWordsToVecNFS(INodeIDToRemove, hsAffinityTermsParent);
                        }
                    } // if parent affinity also

                    htNodeIDToNodeForScore.remove(INodeIDToRemove);

                }  // for each node to remove

                htCorpusIDToHSNodeIDs.remove(new Integer(iCorpusID));
            }
        }
        return -1;
    } // static synchronized int emptyOrFillData ( boolean bStemOn, // fn:

    static synchronized public int fillaCorpus_createsFileIfNotThere(
            int iCorpusID,
            Connection dbc,
            boolean bStem,
            SignatureParms sigparms,
            ParseParms parseparms)
            throws Exception {

        int iNumNodesReturn = -1;
        if (SignatureFile.bSigFileExists(iCorpusID, bStem)) {
            api.Log.Log("start fill from file [" + iCorpusID + "]");
            iNumNodesReturn = FillDataFromFile.fillData_FromFile( // FROM FILE
                    iCorpusID,
                    bStem);
        } else {
            api.Log.Log("start fill from db [" + iCorpusID + "]");
            iNumNodesReturn = FillDataFromDBWriteFileAsWell.fillFromDB_writeSigFile // FROM DB
                    (
                    iCorpusID,
                    bStem,
                    dbc,
                    sigparms,
                    parseparms);

            // CREATE FILE FROM ALL NFS IN MEM THIS CORPUS
            WriteFileFromAllCorpusNSFInMem.writeSigFileFromNFSHashInMem(iCorpusID,
                    bStem, htCorpusIDToHSNodeIDs, htNodeIDToNodeForScore);
        }

        return iNumNodesReturn;
    }	// static private void createNFSHash_was_createFile ( int iCorpusID, String sfnameSigWordCountsFile, Connection dbc )

    public static void loadDataStructIndexingData_oneNFS(NodeForScore nfs) throws Exception {
        Integer INodeID = new Integer((int) nfs.getNodeID());
        htNodeIDToNodeForScore.put(INodeID, nfs);
        Integer ICorpusID = new Integer(nfs.getCorpusID());
        HashSet hsNodeIDsThisCorpus = (HashSet) htCorpusIDToHSNodeIDs.get(ICorpusID);
        if (hsNodeIDsThisCorpus == null) {
            hsNodeIDsThisCorpus = new HashSet();
            htCorpusIDToHSNodeIDs.put(ICorpusID, hsNodeIDsThisCorpus);
        }

        hsNodeIDsThisCorpus.add(INodeID);
        htNodeIDToNodeForScore.put(INodeID, nfs);
        Data_WordsToNodes.htNodeIDToCorpusID.put(INodeID, ICorpusID);


    }

    public static int getNumNodes() throws Exception {
        return htNodeIDToNodeForScore.size();
    }

    public static void removeNode(int iNodeID, boolean bMustBeThere) throws Throwable {
        Object o = htNodeIDToNodeForScore.remove(new Integer(iNodeID));
        if (bMustBeThere && o == null) {
            throw new Exception("removeNodeForScore missing nodeid [" + iNodeID + "]");
        }
    }

    public void addNodeForScore(int iNodeID, NodeForScore nfs) throws Throwable {
        htNodeIDToNodeForScore.put(new Integer(iNodeID), nfs);
    }

    public static NodeForScore getNodeForScore(int iNodeID, boolean bMustBeThere) throws Throwable {
        return getNodeForScore(new Integer(iNodeID), bMustBeThere);
    }

    public static NodeForScore getNodeForScore(Integer INodeID, boolean bMustBeThere) throws Throwable {
        NodeForScore nfsReturn = null;
        nfsReturn = (NodeForScore) htNodeIDToNodeForScore.get(INodeID);
        if (nfsReturn == null) {
            throw new Exception("ERROR: nodeid missing or removed from cache, nodeid [" + INodeID + "]");
        }
        htNodeIDToNodeForScore.put(INodeID, nfsReturn);
        return nfsReturn;
    }

    public static synchronized void printDataStructNodeToNFS(boolean bStemOn, String sDesc, boolean bIncludeWordsStruct) {

        try {
            api.Log.Log("START DATA STRUCT DUMP ALL NODES sDesc [" + sDesc + "] ******************************** ");
            {
                Enumeration e = Data_NodeIDsTo_NodeForScore.getMainHT_htNodeIDToNodeForScore().elements();
                api.Log.Log("# NODES in mem [" + Data_NodeIDsTo_NodeForScore.getMainHT_htNodeIDToNodeForScore().size() + "]");
                api.Log.Log("# htCorpusIDToHSNodeIDs.keys  [" + UtilSets.htKeysToStr(htCorpusIDToHSNodeIDs) + "]");
                api.Log.Log(" NODES *****");
                api.Log.Log(" END NODES *****");
            }
            api.Log.Log("END DATA STRUCT DUMP ALL NODES ******************************** ");

            if (bIncludeWordsStruct) {
                Data_WordsToNodes.printDataStructWordsToNodes(sDesc, bStemOn);
            }
        } catch (Throwable t) {
        }
    }
}
