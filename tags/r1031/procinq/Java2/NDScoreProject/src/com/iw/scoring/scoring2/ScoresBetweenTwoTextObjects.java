package com.iw.scoring.scoring2;

import com.indraweb.encyclopedia.DocForScore;
import com.iw.scoring.NodeForScore;
import com.indraweb.encyclopedia.TextObject;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.log4j.Logger;

/**
 * ScoresBetweenTwoTextObjects is the container object for two text objects
 * (to be scored against one another) and for the set of scores and score
 * values as accumulated over possibly multiple calls to external classifiers.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 */
public class ScoresBetweenTwoTextObjects {
    private static final Logger log = Logger.getLogger(ScoresBetweenTwoTextObjects.class);
    public Hashtable htScoresStrClassifierNameToDScore = new Hashtable();
    public TextObject to1;
    public TextObject to2;
    public com.iw.scoring.scoring2.ScoreExplainData scoreExplainData = null;

    /**
     * Constructor to create this object for passing into the external classifiers
     * @param textObject1  NodeForScore object
     * @param textObject2  DocForScore object
     * @param textObject2  boolean indicating if score explain data is to be collected
     */
    public ScoresBetweenTwoTextObjects(TextObject textObject1, TextObject textObject2, boolean bScoreExplain) {
        this.to1 = textObject1;
        this.to2 = textObject2;
        if (bScoreExplain) {
            scoreExplainData = new ScoreExplainData();
        }
    }

    /**
     * Puts the classifier name and score value in the HashTable htScoresStrClassifierNameToDScore
     * @param sClassifierName the name of the classifier such as "f*c"
     * @param dScoreValue the numeric score value
     */
    public void putScore(String sClassifierName, double dScoreValue) {
        log.debug("putScore(String, double)");
        htScoresStrClassifierNameToDScore.put(sClassifierName, new Double(dScoreValue));
    }

    /**
     * Returns the score associated with the classifier name
     * @param sClassifierName the name of the classifier such as "f*c"
     * @return score value
     */
    public Double getScore(String sClassifierName) {
        Double dScore = (Double) htScoresStrClassifierNameToDScore.get(sClassifierName);
        if (dScore == null) {
            String sSetOfStrClassifierOrScoreName = getClassifierNameSet();
            api.Log.LogError("WARNING: no score as requested for sClassifierOrScoreName [" + sClassifierName + "] "
                    + " nodeid [" + ((NodeForScore) to1).getNodeID() + "].  Classifier names that are present however [" + sSetOfStrClassifierOrScoreName + "]");
        }
        return dScore;
    }

    /**
     * Method to obtain the classifier name as "f*c"
     * @return classifier name 
     */
    public String getClassifierNameSet() {
        Vector vStrClassifierOrScoreName = com.indraweb.util.UtilSets.convertHTKeysToVector(htScoresStrClassifierNameToDScore);
        String sSetOfStrClassifierOrScoreName = com.indraweb.util.UtilStrings.convertVecToString(vStrClassifierOrScoreName, ",");
        return sSetOfStrClassifierOrScoreName;
    }

    /**
     * Returns the first text object (typically NodeForScore)
     * @return text object NodeForScore
     */
    public TextObject getTextObject1() {
        return to1;
    }

    /**
     * Sets the first text object (typically NodeForScore)
     * @param textObject
     */
    public void setTextObject1(TextObject textObject) {
        to1 = textObject;
    }

    /**
     * Returns the second text object (typically DocForScore)
     * @return text object DocForScore
     */
    public TextObject getTextObject2() {
        return to2;
    }

    /**
     * Sets the second text object (typically DocForScore)
     * @param textObject
     */
    public void setTextObject2(TextObject textObject) {
        to2 = textObject;
    }

    public void printScores() throws Exception {
        if (!(to1 instanceof NodeForScore && to2 instanceof DocForScore)) {
            throw new Exception("invalid object types in cprintScores");
        }

        NodeForScore nfs = (NodeForScore) to1;
        DocForScore dfs = (DocForScore) to2;
        int iNodeID = (int) nfs.getNodeID();
        int iCorpusID = nfs.getCorpusID();
        String sDocURL = dfs.getURL();
        Enumeration e = htScoresStrClassifierNameToDScore.keys();
        while (e.hasMoreElements()) {
            String sClassifierName = (String) e.nextElement();
            Double DScore = (Double) htScoresStrClassifierNameToDScore.get(sClassifierName);
            api.Log.Log("sbtto.print classifier [\t" + sClassifierName
                    + "\t] score [\t" + DScore + "\t] corpus:node:doc [\t" + iCorpusID + "\t:" + iNodeID + ":" + sDocURL + "\t] ");
        }
    }

    /**
     * As soon as text objects are no longer needed (Eg score completed) call this to release
     */
    public void releaseTextObjects() {
        to1 = null;
        to2 = null;
    }
}
