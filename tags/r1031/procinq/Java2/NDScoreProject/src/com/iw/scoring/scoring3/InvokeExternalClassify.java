/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Dec 7, 2002
 * Time: 6:35:19 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.scoring.scoring3;

import api.tsclassify.ROCHelper.DataCache_Classifier;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.iw.scoring.scoring2.ScoresBetweenTwoTextObjects;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

/**
 * InvokeExternalClassify is the common callout point for all external classifiers. Caches are maintained of external classes, class arrays, and methods as an optimization.
 *
 * @author Henry Kon
 * @version %I%, %G%
 * @since 1.0
 */
public class InvokeExternalClassify {

    private static Hashtable htCache_sClasspathToMethod = new Hashtable();
    private static Hashtable htCache_sClasspathToClassParmsArr = new Hashtable();
    private static Hashtable htCache_sClasspathToClass = new Hashtable();
    private static int PARMARRLEN = 6; // # objects in arg list to callout

    /**
     * Iterates through all classifier classpaths and calls out to each using reflection. <p> The ScoresBetweenTwoTextObjects object will contain both TextObject objects (typically NodeForScore and
     * DocForScore) before the call and the HashTable htScoresStrClassifierNameToDScorewithin the ScoresBetweenTwoTextObjects will be empty. <p> The called routine will populate the hashtable in
     * ScoresBetweenTwoTextObjects with (String) classifier/score name (e.g., "f*c"), (Double) scorevalue pairs. <p>
     *
     * @param sbtto the container holding the two text objects and ultimately the score results
     * @param dbc database connection
     * @param bScoreExplain indicates if score explain
     * @param bStemOn true means stem terms before scoring
     * @param bThesauruson true means use the thesaurus feature in scoring
     */
    //static int iCallCnt = 0;
    public static ExplainDisplayDataInstanceTABLE scoreCollect(
            ScoresBetweenTwoTextObjects sbtto,
            java.sql.Connection dbc,
            boolean bScoreExplain,
            java.io.PrintWriter out,
            boolean bStemOn,
            boolean bThesaurusOn) throws Throwable {

        ExplainDisplayDataInstanceTABLE eddiTABLE_ROC = null;
        Iterator it_sUniqueClasspaths = DataCache_Classifier.gethsAllStrClassifierPaths(dbc).iterator();
        if (bScoreExplain) {
            // do ROC classifiers etc.
            eddiTABLE_ROC = fillExplainDataTableForTransmitToGUI(sbtto, dbc);
        }

        int iNumPaths = 0;
        // for each unique classifier callout
        while (it_sUniqueClasspaths.hasNext()) {
            iNumPaths++;
            String sNextclasspath = (String) it_sUniqueClasspaths.next();
            Method methThisClasspath = (Method) htCache_sClasspathToMethod.get(sNextclasspath);
            Class classThisClasspath = (Class) htCache_sClasspathToClass.get(sNextclasspath);

            if (methThisClasspath == null) { // cache all we can inside here re: class, methods, parm arrays
                Class[] classArrParmsThisClasspath = (Class[]) htCache_sClasspathToClassParmsArr.get(sNextclasspath);
                // get method array
                Class c1 = Class.forName("com.iw.scoring.scoring2.ScoresBetweenTwoTextObjects");
                //Class c2 = Class.forName ("java.sql.Connection");
                Class c2 = Class.forName("java.lang.Boolean");
                Class c3 = Class.forName("java.io.PrintWriter");
                Class c4 = Class.forName("java.lang.Boolean");
                Class c5 = Class.forName("java.lang.Boolean");
                Class connection = Class.forName("java.sql.Connection");

                classArrParmsThisClasspath = new Class[PARMARRLEN];
                classArrParmsThisClasspath[0] = c1;
                classArrParmsThisClasspath[1] = c2;
                classArrParmsThisClasspath[2] = c3;
                classArrParmsThisClasspath[3] = c4;
                classArrParmsThisClasspath[4] = c5;
                classArrParmsThisClasspath[5] = connection;


                // create the class against which to invoke the API method call
                try {
                    // *****************
                    // GET CLASS
                    // *****************
                    classThisClasspath = Class.forName(sNextclasspath);

                    // *****************
                    // GET METHOD
                    // *****************
                    methThisClasspath = classThisClasspath.getMethod("genScores", classArrParmsThisClasspath);
                    htCache_sClasspathToMethod.put(sNextclasspath, methThisClasspath);
                    htCache_sClasspathToClass.put(sNextclasspath, classThisClasspath);
                    htCache_sClasspathToClassParmsArr.put(sNextclasspath, classArrParmsThisClasspath);
                } catch (Throwable t) {
                    throw t;
                }
            } // if ( methThisClasspath == null )
            // make the call finally
            // build the objects to pass in to the invoke
            Object[] oArr = new Object[PARMARRLEN];
            int iO = 0;
            oArr[iO++] = sbtto;
            // speed optimization potential opportunity! :
            // set these as statics only once in the score explain class being invoked
            oArr[iO++] = bScoreExplain;
            oArr[iO++] = out;
            oArr[iO++] = bStemOn;
            oArr[iO++] = bThesaurusOn;
            oArr[iO++] = dbc;

            // Log.logClearcr(" invoking (1-base index) external score callout [" + iNumPaths + "]");
            methThisClasspath.invoke(classThisClasspath, oArr);
            if (iNumPaths == 0) {
                throw new Exception("no external scoring classpaths defined");
            }
            //api.Log.Log ("numpaths [" + iNumPaths + "]" );
        }

        return eddiTABLE_ROC;
    }

    /**
     * keeps an in mem list of external classifiers to run against
     */
    static class ClassifierCache {
        // hash table classfiier path eg com.indraweb.extclass1 to hash set eg {1,3,4)

        private Hashtable ht_sClassifierPath_hsOfStrClassifierIDs = null;
        private HashSet hsClassifierPaths = null; // for top loop of this class' user
        private Hashtable ht_sClassifierID_sColName = null;

        public synchronized HashSet getClasspaths() {
            return hsClassifierPaths;
        }

        public synchronized HashSet getIDsForClasspath(String sClasspath) {
            return (HashSet) ht_sClassifierPath_hsOfStrClassifierIDs.get(sClasspath);
        }

        public synchronized String getColName(int iClassifierID) {
            return (String) ht_sClassifierID_sColName.get("" + iClassifierID);
        }
    }

    public static void invalidateCache()
            throws Exception {
        htCache_sClasspathToMethod = new Hashtable();
        htCache_sClasspathToClassParmsArr = new Hashtable();
        htCache_sClasspathToClass = new Hashtable();

    }

    private static ExplainDisplayDataInstanceTABLE fillExplainDataTableForTransmitToGUI(
            ScoresBetweenTwoTextObjects sbtto,
            Connection dbc) throws Exception {
        com.iw.scoring.NodeForScore nfs = (com.iw.scoring.NodeForScore) sbtto.to1;

        Vector vOArrRowExplain_ROCClassifier = new Vector();
        String[] sArrColNames_Tab1_TopAllStatsNonTerm = {"Name", "Value"};
        ExplainDisplayDataInstanceTABLE eddiTABLE_ROC = new ExplainDisplayDataInstanceTABLE(
                "External Classify Invoker", // source
                "ROC Classifiers", // tab name
                ExplainDisplayDataContainer.sExplainDataRenderType_TABLE,
                "Star counts, ROC Classifiers, Cutoffs", // Description
                "Star counts, ROC Classifiers, Cutoffs", // tooltip
                sArrColNames_Tab1_TopAllStatsNonTerm,
                vOArrRowExplain_ROCClassifier,
                null);

        sbtto.scoreExplainData.addExplainDisplayDataInstance(eddiTABLE_ROC);

        // corpusid and rocsetting id this node
        String sROCSETTINGID = null;
        {
            String sSQL = "SELECT corpus_name from corpus where corpusid = " + nfs.getCorpusID();
            String sCorpusName = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBStr(sSQL, dbc);
            eddiTABLE_ROC.addVectorRow("Corpus name (id)", sCorpusName + " (" + nfs.getCorpusID() + ")");

            String sSQL1 = "SELECT ROCSETTINGID from corpus where corpusid = " + nfs.getCorpusID();
            sROCSETTINGID = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBStr(sSQL1, dbc);
            eddiTABLE_ROC.addVectorRow("ROC SettingID", sROCSETTINGID);
        }


        // starcounts, cutoffs,
        {

            String sSQL = "SELECT STARCOUNT, COSTRATIOFPTOFN, CUTOFFSCOREGTE, "
                    + " c.CLASSIFIERID, SCORE1VAL, TRUEPOS, FALSEPOS, COLNAME, CLASSIFIERNAME "
                    + " FROM ROCBucketLocalClassifier r, Classifier c"
                    + " WHERE ROCSETTINGID = " + sROCSETTINGID
                    + " and r.classifierID =  c.classifierID  "
                    + " order by STARCOUNT ASC";
            //api.Log.Log("sSQL  [" + sSQL + "]" );

            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);

            // Need to get total number of results for calculating percentages
            int iRowNum = 0;
            while (rs.next()) {
                iRowNum++;
                int iSTARCOUNT = rs.getInt(1);
                float fCOSTRATIOFPTOFN = rs.getFloat(2);
                float fCUTOFFSCOREGTE = rs.getFloat(3);
                int iCLASSIFIERID = rs.getInt(4);
                float fSCORE1VAL = rs.getInt(5);
                float fTRUEPOS = rs.getInt(6);
                float fFALSEPOS = rs.getInt(7);
                String sCOLNAME = rs.getString(8);
                String sCLASSIFIERNAME = rs.getString(9);

                eddiTABLE_ROC.addVectorRow(
                        "Star count [" + iSTARCOUNT + "]",
                        "Classifier [" + sCLASSIFIERNAME + "] min score [" + com.indraweb.util.UtilStrings.numFormatDouble((double) fCUTOFFSCOREGTE, 2) + "]");

                //vDataROCBucketLocalClassifierThisROCSetting.addElement(new DataCache_RocBucketLocalClassifier(
                //      IROCSETTINGID.intValue() , iSTARCOUNT, fCOSTRATIOFPTOFN,
                //      fCUTOFFSCOREGTE, iCLASSIFIERID, sCLASSIFIERNAME, fRATIOTNTOTP, fSCORE1VAL, fTRUEPOS, fFALSEPOS, sCOLNAME));
                //htStrClassifierNamesToColNames.put (sCLASSIFIERNAME, sCOLNAME );
                //htStrColNamesToStrClassifierNames.put (sCOLNAME,sCLASSIFIERNAME );
            }

            rs.close();
            stmt.close();
            //htIntegerROCSettingID_toVDataROCBucketLocalClassifier.put (
            //       new Integer (iROCSettingID), vDataROCBucketLocalClassifierThisROCSetting );
        }// for all ROCSettingIDs
        return eddiTABLE_ROC;
    }
}
