package com.enterprisedt.net.ftp;

import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.enterprisedt.net.ftp.FTPConnectMode;
import java.io.IOException;


public class FTPGetRemoteFileList {

    public static String[] ls (
            String host,
            String user,
            String password,
            String sRemoteDir

    ) throws Exception {


        System.out.println("in HBK control code ");
        // connect and test supplying port no.
        FTPClient ftp = new FTPClient(host, 21);
        //ftp.login(user, password);
        //ftp.quit();

        // connect again
        ftp = new FTPClient(host);

        // switch on debug of responses
        ftp.debugResponses(true);

        ftp.login(user, password);

        // change dir
        ftp.chdir(sRemoteDir);

        // test that dir() works in full mode
        String[] listings = ftp.dir(".", true);
        //for (int i = 0; i < listings.length; i++)
         //   System.out.println(listings[i]);

        // try system()
        //System.out.println(ftp.system());

        // try pwd()
        //System.out.println(ftp.pwd());

        ftp.quit();
        return listings;
    }


    /**
     *  Basic usage statement
     */
    public static void usage() {

        System.out.println("Usage: ");
        System.out.println("com.enterprisedt.net.ftp.FTPClientTest " +
                           "remotehost user password filename directory " +
                           "(ascii|binary) (active|pasv)");
    }

}
















