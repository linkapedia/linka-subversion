/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 22, 2002
 * Time: 7:55:16 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.analysis.trec;

public class TrecCompareCounters {

    public int icountIWHitsthisQ = -1;
    public int ivINodeIDssize= -1;
    public int icounttrecHitsthisQ = -1;
    public String sNodeIDList = null;
    public int iNumFalseRecords= -1;
    public int icountIWHitsthisQCountNodeRepeats = -1;

    public TrecCompareCounters  (
        int icountIWHitsthisQ,
        int ivINodeIDssize,
        int icounttrecHitsthisQ,
        String sNodeIDList,
        int iNumFalseRecords ,
        int icountIWHitsthisQCountNodeRepeats
           )
    {
        this.icountIWHitsthisQ = icountIWHitsthisQ;
        this.ivINodeIDssize= ivINodeIDssize;
        this.icounttrecHitsthisQ = icounttrecHitsthisQ ;
        this.sNodeIDList = sNodeIDList ;
        this.iNumFalseRecords= iNumFalseRecords;
        this.icountIWHitsthisQCountNodeRepeats = icountIWHitsthisQCountNodeRepeats;
    }


    public String toString()
    {
        return ("*** " +
                "]\r\n # TREC hits [" + icounttrecHitsthisQ+
                "]\r\n # IW hits thisQ [" + icountIWHitsthisQ+
                "]\r\n # IW Hits thisQ Count Node Repeats [" +  icountIWHitsthisQCountNodeRepeats  +
                "]\r\n # unique false pos or false neg docIDs [" + iNumFalseRecords +
                "]\r\n # NodeIDs [" + ivINodeIDssize  + "] NoideIDList [" + sNodeIDList +
                "]\r\n"
        );

    }


}
