/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 20, 2002
 * Time: 7:30:34 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.analysis.trec;

import java.sql.*;
import java.util.Vector;

import com.indraweb.util.*;

public class TrecIwCompareReport {

    //public static int[] iArrQIDs = {403};
    public static int[] iArrQIDs = {403,406,411,413,416,420,429,434,435,441,444,447,449,450};
    //public static int[] iArrQIDs = {447};                           \
    public static void outputRecallNumbers ( Connection dbc )
        throws Exception
    {
        com.indraweb.util.UtilFile.addLineToFileKill("c:/t3.txt", "RECALL REPORT\r\n" );
        com.indraweb.util.UtilFile.addLineToFile("c:/t3.txt", "QId\tQDesc\tDocumentid\tDoctitle\tDdocurl\t" +
                "Nodeid\tNodeTitle\tCorpusid\tScore1\r\n" );
            // loop thru qIDs

        for ( int iQidIdx = 0; iQidIdx < iArrQIDs.length; iQidIdx++ ) // loop thru Q id's
        {

            int iNumDataRowsMissingThisQ = 0;
            int iQidActual = iArrQIDs[iQidIdx];
            if (iQidActual == 4031) iQidActual = 403; // special case for one node
            // ******** WHICH DOCS DID ONE MISS AND NOT THE OTHER
            String sSQLDocsfoundbyTrecAndNotIW =
                    " select distinct (documentid)  from trecqrel tqr , trecqnode  tqn " +
                    //  " select documentid  from trecqrel tqr , trecqnode  tqn " +
                " where qestionid = " + iQidActual +
                " and tqn.questionid = tqr.qestionid  " +
                " and tqn.version = 2 " +
                " and status = 1 " +
                " minus " +
                " select d.documentid  " +
                " from trecqnode tqn, nodedocument nd, document d, node n " +
                " where " +
                //" docurl like '\\\\66.134.131.60\\60gb db drive\\trec\\tars\\%' " +
                " tqn.version = 2 " +
                " and tqn.nodeid = n.nodeid " +
                " and n.corpusid = 100040 " +
                " and n.nodeid = nd.nodeid "  +
                " and d.documentid = nd.documentid " +
                " and questionid = " + iArrQIDs[iQidIdx] ;


                //" select nd.documentid from trecqnode tqn, nodedocument nd, document d, node n " +
                //" where tqn.nodeid = nd.nodeid " +
                //" and d.documentid = nd.documentid " +
                //" and n.nodeid = nd.nodeid and n.corpusid = 100040 " +
                //" and docurl like '\\\\66.134.131.60\\60gb db drive\\trec\\tars\\%' " +
                //" and tqn.version = 2 " +
            //" and questionid = " + iArrQIDs[iQidIdx] +
                //")";


            com.indraweb.util.Log.logClearcr("\r\n" + sSQLDocsfoundbyTrecAndNotIW +"\r\n");
            int i4 = 0;


            java.sql.Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQLDocsfoundbyTrecAndNotIW);
            try {
                int i3 = 0;
                while ( rs.next() )
                {

                    int iDocumentid = rs.getInt(1);
                    // ASSUME IN THERE :
                    if ( iDocumentid   > 0 )
                    {
                        iNumDataRowsMissingThisQ++;
                        boolean bFoundandPrintedFullRecord = false;
                        if ( true )
                        {
                            String sSQLData =
                                " select qestionid, qdesc, d.documentid, doctitle, docurl, " +
                                " n.nodeid, n.nodetitle, corpusid, score1 " +
                                " From document d, nodedocument nd, node n , trecqrel tqr , trecqnode  tqn " +
                                " where n.nodeid = nd.nodeid " +
                                " and nd.documentid = d.documentid " +
                                " and tqn.nodeid = nd.nodeid " +
                                " and tqn.version = 2 " +
                                " and tqn.questionid = " + iArrQIDs[iQidIdx] +
                                " and tqn.questionid = tqr.qestionid  " +
                                " and n.corpusid = 100040 " +
                                " and (d.documentid) = " + iDocumentid ;

                            com.indraweb.util.Log.logClearcr("\r\nsSQLData :" + sSQLData +"\r\n");
                            java.sql.Statement stmt2 = dbc.createStatement();
                            ResultSet rs2 = stmt2.executeQuery(sSQLData );
                            try {
                                while ( rs2.next() )
                                {
                                    int iColCount = 1;

                                    int iQid = rs2.getInt(iColCount++);
                                    String sQDesc = rs2.getString(iColCount++);
                                    int iDocumentid2 = rs2.getInt(iColCount++);
                                    String sDoctitle = rs2.getString(iColCount++);
                                    String sDdocurl = rs2.getString(iColCount++);
                                    int iNodeid = rs2.getInt(iColCount++);
                                    String sNodeTitle= rs2.getString(iColCount++);
                                    int iCorpusid = rs2.getInt(iColCount++);
                                    double dScore1 = rs2.getDouble(iColCount++);
                                    com.indraweb.util.UtilFile.addLineToFile("c:/t3.txt",
                                            iQidIdx + "###" +
                                        iQid+ "\t" +
                                        sQDesc+ "\t" +
                                        iDocumentid2+ "\t" +
                                        sDoctitle+ "\t" +
                                        sDdocurl+ "\t" +
                                        iNodeid + "\t" +
                                        sNodeTitle+ "\t" +
                                        iCorpusid+ "\t" +
                                        dScore1 + "\r\n"
                                    );
                                    bFoundandPrintedFullRecord = true;
                                } // while ( rs2.next() )

                            } finally {
                                stmt2.close();
                                rs2.close();
                            }
                        }
                        if ( true && !bFoundandPrintedFullRecord )
                        {
                            String sSQLData =
                                " select d.documentid, doctitle, docurl " +
                                " " +
                                " From document d" +
                                " where d.documentid = " + iDocumentid ;

                            com.indraweb.util.Log.logClearcr("\r\nsSQLData :" + sSQLData +"\r\n");
                            java.sql.Statement stmt2 = dbc.createStatement();
                            ResultSet rs2 = stmt2.executeQuery(sSQLData );
                            try {
                                while ( rs2.next() )
                                {
                                    int iColCount = 1;

                                    int iQid = iArrQIDs[iQidIdx];
                                    String sQDesc = getQDescFromQid ( iQid, dbc);
                                    int iDocumentid2 = rs2.getInt(iColCount++);
                                    String sDoctitle = rs2.getString(iColCount++);
                                    String sDdocurl = rs2.getString(iColCount++);
                                    int iNodeid = -1;
                                    String sNodeTitle= "NA";
                                    int iCorpusid = -1;
                                    double dScore1 = -1;
                                    com.indraweb.util.UtilFile.addLineToFile("c:/t3.txt",
                                            iQidIdx + "###" +
                                        iQid+ "\t" +
                                        sQDesc+ "\t" +
                                        iDocumentid2+ "\t" +
                                        sDoctitle+ "\t" +
                                        sDdocurl+ "\t" +
                                        iNodeid + "\t" +
                                        sNodeTitle+ "\t" +
                                        iCorpusid+ "\t" +
                                        dScore1 + "\r\n"
                                    );
                                } // while ( rs2.next() )

                            } finally {
                                stmt2.close();
                                rs2.close();
                            }
                        }
                    } // if docid > 0


                }
            }
            finally {
                rs.close();
                stmt.close();
            }

            printsupplementalStats ( iArrQIDs[iQidIdx ], iNumDataRowsMissingThisQ, dbc);

        }  // for
    }

    //java.util.Hashtable htCache
    private static String getQDescFromQid  ( int iQid, Connection dbc )
    throws Exception
    {
        String sSQL = "select qdesc from trecqnode where questionid = " + iQid + " and version = 2";
        String s = com.indraweb.database.JDBCIndra_Connection.ExecuteQueryAgainstDBStr(sSQL, dbc);
        return s;
    }


    //java.util.Hashtable htCache
    private static void printsupplementalStats  ( int iQid, int iNumdataRowsMissing, Connection dbc )
    throws Exception
    {

        String sSQLGetNodesForQ = "select nodeid from trecqnode where questionid = " + iQid +
            " and version = 2" ;
        Vector vINodeIDs = com.indraweb.database.JDBCIndra_Connection.ExecuteQueryAgainstDBVector(sSQLGetNodesForQ, dbc);
        StringBuffer  sbNoideIDList = new StringBuffer();
        for ( int ix = 0; ix < vINodeIDs.size(); ix++ )
        {
            if ( ix > 0 )
                sbNoideIDList.append(",");
            sbNoideIDList.append(vINodeIDs.elementAt(ix));
        }
        ///Log.logClearcr("***** node list [" + sbNoideIDList + "]");
        //UtilSets.convertVectorTo_intArray(vINodeIDs);

        // GET # DOcs by TREc
        String sSQL = "select count (*) from trecqrel where status = 1 and qestionid = " +
            iQid ;
        long lcounttrecHitsthisQ = com.indraweb.database.JDBCIndra_Connection.ExecuteQueryAgainstDBlong(sSQL, dbc);
        // GET # DOcs by IW
        sSQL = "select count (*) from ( select distinct (documentid) from nodedocument nd where nodeid in (" + sbNoideIDList + "))";
        long lcountIWHitsthisQ = com.indraweb.database.JDBCIndra_Connection.ExecuteQueryAgainstDBlong(sSQL, dbc);
        com.indraweb.util.UtilFile.addLineToFile("c:/t3.txt",
                "*** " +
                  "Q [" + iQid +
                "]\r\n #trec hits [" + lcounttrecHitsthisQ +
                "]\r\n #IW hits [" + lcountIWHitsthisQ +
                "]\r\n #NodeIDs [" + vINodeIDs.size()  + "] NoideIDList [" + sbNoideIDList +
                "]\r\n #false neg [" + iNumdataRowsMissing +
                "]\r\n"      );
    }

}

/*
TrecPS = trec positive set =
 select documentid from trecqrel where qestionid = 403 and status = 1;    (21)

TNS = trec negative set =
 select documentid from trecqrel where qestionid = 403 and status = 0;    (1025)

IWPS = indraweb positive set =
 select d.documentid from trecqnode tqn, nodedocument nd, document d where tqn.nodeid = nd.nodeid and  d.documentid = nd.documentid and docurl like '\\66.134.131.60\60gb db drive\trec\tars\%' and questionid = 403 and nd.score1 > 0
*/