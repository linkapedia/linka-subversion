/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 19, 2002
 * Time: 8:01:06 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.analysis.trec;

import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilStrings;

import java.util.Vector;

public class TrecQrelLoader {

    public static Vector getvTrecQrels() throws Exception
    {
        Vector vReturn_TrecTopicsFromQrel = new Vector();

        //String sFileName_qrel8_answerkey = "C:/ausr/hkon/IndraWeb/technology/trec anal/qrel8.txt";
        String sFileName_qrel8_answerkey = "C:/ausr/hkon/IndraWeb/technology/trec anal/qrel9_hk.txt";
        Vector vFileStrings = UtilFile.getVecOfFileStrings(sFileName_qrel8_answerkey);
        int iLastSize = 0;
        int icountlast = 0;
        System.out.println("vsize " + vFileStrings.size() );
        // LOAD FILE LINES

        // walk thru vec of file strings
        for ( int i = 0; i < vFileStrings.size(); i++ )
        {
            if ( i % 100 == 0 )  System.out.println(" i [" + i + "] ");
            String sLine =(String)  vFileStrings.elementAt(i);
            // get data from each file
            Vector vLineSplitUp = UtilStrings.splitByStrLen1(sLine, " ");
            Integer IDataQID = new Integer ( (String) vLineSplitUp.elementAt(0));
            String sDataTopicURLSubString = (String) vLineSplitUp.elementAt(2);
            Integer IDataHit01 = new Integer ( (String) vLineSplitUp.elementAt(3));
            // TEST THAT NOTHING BUT
            int iCount = vLineSplitUp.size();
            if ( iCount != icountlast ) {
                // create vec of file strings
                if ( icountlast != -1 )
                    throw new Exception("iCount != icountlast i [" + i + "] iCount  [" + iCount  + "] icountlast  [" + icountlast  + "] " );
                icountlast = iCount;
            }
            vReturn_TrecTopicsFromQrel.addElement ( new TQRelContent ( IDataQID.intValue() ));
        }
        return vReturn_TrecTopicsFromQrel;
    }  // getvTrecTopicsFrom Qrel
}
