package com.iw.system;

import java.util.*;
import java.util.List;
import java.io.*;
import java.net.URLEncoder;
import java.awt.*;
import java.awt.datatransfer.*;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * The ProcinQ object defines both a user session and contains methods used to interface with the ProcinQ API.  Each of
 * these methods simply abstract another layer onto the raw InvokeAPI class.
 *
 * A pointer to the ProcinQ object should be kept persistently as it is the container for both the user's session
 *  key and the location of the API server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@return An ProcinQ object.
 */

public class ProcinQ implements Serializable {
    private String SKEY = "NONE";
    private String API = "http://localhost/";
    private String domainController = null;

    // object constructors
    public ProcinQ() {}

    public ProcinQ(String SessionKey, String API) {
        if (SessionKey != null) this.SKEY = SessionKey;
        if (API != null) this.API = "http://"+ API + "/itsapi/ts?fn=";
    }

    // accessor functions
    public void setSessionKey(String SessionKey) {
        if (SessionKey != null) this.SKEY = SessionKey;
    }

    public void setAPI(String API) {
        if (API != null) this.API = "http://"+ API + "/itsapi/ts?fn=";
    }

    public String getSessionKey() { return SKEY; }

    public String getAPI() { return API; }

    public HashTree getArguments() {
        HashTree ht = new HashTree();
        ht.put("api", API);
        ht.put("SKEY", SKEY);

        return ht;
    }

    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeAPI(sAPIcall, htArgs, true); }
    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs, boolean bReplaceQuotes) throws Exception {
        return InvokeAPI(sAPIcall, htArgs, bReplaceQuotes, false);
    }
    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs, boolean bReplaceQuotes, boolean bDebug)
            throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        HashTree ht = null;
        try { ht = API.Execute(bDebug, false, bReplaceQuotes); }
        catch (Exception e) { throw e; }

        return ht;
    }

    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeDomAPI(sAPIcall, htArgs, false); }
    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try { d = API.dExecute(bDebug, false); }
        catch (Exception e) { throw e; }

        return d;
    }

    public org.dom4j.Document InvokeDomPostAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeDomPostAPI(sAPIcall, htArgs, false); }
    public org.dom4j.Document InvokeDomPostAPI(String sAPIcall, Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try { d = API.dExecutePost(bDebug, false); }
        catch (Exception e) { throw e; }

        return d;
    }

    // Saffron functionality: log into server, post a document, get back concepts
    // required:
    //   security.TSLogin
    //   tscorpus.TSListCorpora
    //   tsclassify.TSClassifyDoc
    //   tsnode.TSGetNodeProps
    //   tsnode.TSGetNodeTree
    //   tscql.TSCql

    /* ****************************************************************************** */
    //   security.TSLogin
    public void Login(String Username, String Password) throws Exception {
        // LOGIN routine takes a USERID and a PASsWORD
        Hashtable htArgs = new Hashtable();
        htArgs.put("UserID", Username); // example: "sn=cifaadmin,ou=users,dc=cifanet";
        htArgs.put("Password", Password); // example: racer9
        htArgs.put("api", API);

        try {
            // Invoke the ProcinQAPI now.
            HashTree htResults = InvokeAPI("security.TSLogin", htArgs);

            // If there is no "subscriber" tag, an error has occured
            if (!htResults.containsKey("SUBSCRIBER")) {
                throw new Exception("Invalid username, password combination.");
            }

            // Get user hash tree
            HashTree htUser = (HashTree) htResults.get("SUBSCRIBER");

            if (!htUser.containsKey("KEY")) {
                throw new Exception("Invalid username, password combination.");
            }

            setSessionKey((String) htUser.get("KEY"));
        } catch (Exception e) {
            throw e;
        }
    }

    //   tscorpus.TSListCorpora
    public Vector getCorpora() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSListCorpora", htArgs);

            if (!htResults.containsKey("CORPORA")) {
                System.out.println("Sorry, no corpora elements found.");
                throw new Exception("There are no taxonomies loaded in this system.");
            }

            HashTree htCorpora = (HashTree) htResults.get("CORPORA");

            Enumeration eC = htCorpora.elements();
            while (eC.hasMoreElements()) {
                HashTree htCorpus = (HashTree) eC.nextElement();
                Corpus c = new Corpus(htCorpus);

                v.add(c); // add this node to the vector
            }

            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    //   tsnode.TSGetNodeProps
    public Node getNodeProps(String NodeID) throws Exception {
        String sCQL = "SELECT <NODE> WHERE NODEID = "+NodeID;

        Vector vNode = CQL(sCQL);
        if (vNode.size() == 0) { throw new Exception("NODE ID " + NodeID + " does not exist."); }
        else { return (Node) vNode.elementAt(0); }
    }

    //   tsnode.TSGetNodeTree
    public Vector getNodeTree(String NodeID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        Vector v = new Vector(); // return struct

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSGetNodeTree", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) return new Vector();

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                Node n = new Node(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    // tscql.TSCql
    public Vector CQL(String Query) throws Exception {
        return CQL(Query, 1, 500);
    }

    public Vector CQL(String Query, int Start, int RowMax) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("query", Query);
        htArgs.put("start", "" + Start);
        htArgs.put("rowmax", "" + RowMax);

        Vector v = new Vector();
        //System.out.println("CQL: "+Query);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscql.TSCql", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            // return different structures depending upon NODE or DOCUMENT
            if (Query.toUpperCase().indexOf("<NODE>") != -1) {
                Element eNodes = elemRoot.element("NODES");
                if (eNodes == null) return new Vector();

                Iterator i = eNodes.elements().iterator();
                while (i.hasNext()) {
                    Element eNode = (Element) i.next();

                    Iterator i2 = eNode.elements().iterator();
                    Node n = new Node(i2);

                    v.add(n);
                }
            } else if (Query.toUpperCase().indexOf("<NODEDOCUMENT>") != -1) {
                Element eNodeDocs = elemRoot.element("NODEDOCUMENTS");
                if (eNodeDocs == null) return new Vector();

                Iterator i = eNodeDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eNodeDoc = (Element) i.next();

                    Iterator i2 = eNodeDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            } else {
                throw new Exception("The CQL selection object was invalid.");
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    // classify: using the HTTP POST method
    public Vector classify (File f, boolean bPost, String corporaList) throws Exception {
        return classify(f, bPost, corporaList, 3); }
    public Vector classify (File f, boolean bPost, String corporaList, int parentTitleTermsAsSigs) throws Exception {
        Vector v = new Vector(); boolean bUsingTempFile = false;

        Hashtable htArgs = getArguments();
        htArgs.put("explainscores", "false");
        if (bPost) { htArgs.put("post", "true"); }
        else { htArgs.put("post", "false"); }

        htArgs.put("DocTitle", "None");
        htArgs.put("Corpora", corporaList);
        htArgs.put("NumParentTitleTermsAsSigs", ""+parentTitleTermsAsSigs);

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);
            org.dom4j.Document doc = API.dExecute(f);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eDocs == null) throw new ClassifyError();

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                if (eDoc.getQName().getName().equals("NODE")) {
                    Iterator i2 = eDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            }
        } catch (SessionExpired e) { throw e;
        } catch (ClassifyError e) { throw e;
        } catch (Exception e) { throw e; }

        return v;
    }
}
