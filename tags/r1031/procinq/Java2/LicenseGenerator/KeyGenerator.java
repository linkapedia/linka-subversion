// KeyGenerator

import java.math.BigInteger;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.*;
import java.util.*;
import java.io.*;

public class KeyGenerator {
    public static final int INGESTION = 0;
    public static final int PEOPLE_SEARCH = 1;
    public static final int THESAURUS = 2;
    public static final int NARRATIVE = 3;

    public static final int NUM_OPTIONS = 4;

    public static void main(String args[]) throws Exception {
        String bitmap = args[0];

        if ((bitmap == null) || (!isBinary(bitmap))) {
            System.out.println("Usage: KeyGenerator <binary string>\n");
            return;
        }

        if (bitmap.length() < NUM_OPTIONS) {
            System.out.println("There are "+NUM_OPTIONS+" components in the system.  Please try again.");
            return;
        }

        if (bitmap.toCharArray()[INGESTION] == '1') System.out.println("Ingestion .. enabled");
        else System.out.println("Ingestion .. disabled");

        if (bitmap.toCharArray()[PEOPLE_SEARCH] == '1') System.out.println("People Search .. enabled");
        else System.out.println("People Search .. disabled");

        if (bitmap.toCharArray()[THESAURUS] == '1') System.out.println("Thesaurus .. enabled");
        else System.out.println("Thesaurus .. disabled");

        if (bitmap.toCharArray()[NARRATIVE] == '1') System.out.println("Narrative .. enabled");
        else System.out.println("Narrative .. disabled");

        String hexmap = bitmapToHexmap(bitmap);
        //System.out.println("hexmap: "+hexmap);

        String license = hexmapToLicense(hexmap);
        System.out.println("\nLicense Key: "+license);

        /*
        System.out.println("--------------------------");

        hexmap = licenseToHexmap(license);
        System.out.println("hexmap: "+hexmap);

        if (isHexmapStable(hexmap)) System.out.println("Hexmap is stable.");
        else System.out.println("Hexmap is UNSTABLE");

        bitmap = hexmapToBitmap(hexmap);
        System.out.println("bitmap: "+bitmap);
        */

    }

    // 8-bytes Salt
    private static byte[] salt = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
        (byte) 0x56, (byte) 0x34, (byte) 0xE3, (byte) 0x03
    };

    private static final char[] pass = "AB8391D01EF0192A".toCharArray();

    // decrypt license key into HEXMAP
    public static String licenseToHexmap(String licenseKey) throws Exception {
        PBEParameterSpec ps = new javax.crypto.spec.PBEParameterSpec(salt, 20);
        SecretKeyFactory kf = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey k = kf.generateSecret(new javax.crypto.spec.PBEKeySpec(pass));

        Cipher decryptCipher = Cipher.getInstance("PBEWithMD5AndDES/CBC/PKCS5Padding");
        decryptCipher.init(Cipher.DECRYPT_MODE, k, ps);

        return decrypt(licenseKey, decryptCipher);
    }

    // verify HEXMAP contains acceptable sequence
    public static boolean isHexmapStable(String hexMap) {
        if ((hexMap.startsWith("DEED")) && (hexMap.endsWith("ACE"))) return true;
        return false;
    }

    // convert HEXMAP to bitmap
    public static String hexmapToBitmap(String hexMap) {
        BigInteger bi = new BigInteger(hexMap.substring(4, hexMap.length() - 3), 16);
        return bi.toString(2);
    }

    // create new License object from bitmap
    /*
    public static License createLicense (String bitMap) {
        return "";
    } */

    // convert BITMAP to HEXMAP (sequence added)
    public static String bitmapToHexmap(String bitMap) {
        BigInteger bi = new BigInteger(bitMap, 2);
        return new String("DEED" + bi.toString(16) + "ACE").toUpperCase();
    }

    // encrypt HEXMAP into license key
    public static String hexmapToLicense(String hexMap) throws Exception {
        PBEParameterSpec ps = new javax.crypto.spec.PBEParameterSpec(salt, 20);
        SecretKeyFactory kf = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey k = kf.generateSecret(new javax.crypto.spec.PBEKeySpec(pass));

        Cipher ecipher = Cipher.getInstance("PBEWithMD5AndDES/CBC/PKCS5Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, k, ps);

        return encrypt(hexMap, ecipher);
    }

    public static String encrypt(String str, Cipher ecipher) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return new sun.misc.BASE64Encoder().encode(enc);

        } catch (BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public static String decrypt(String str, Cipher dcipher) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");

        } catch (BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
        }
        return null;
    }

    private static boolean isBinary(String s) {
        int pos = 0;
        char c;

        while (pos < s.length()) {
            c = s.charAt(pos);
            if ((c != '0') && (c != '1')) return false;
            pos++;
        }

        return true;
    }
}