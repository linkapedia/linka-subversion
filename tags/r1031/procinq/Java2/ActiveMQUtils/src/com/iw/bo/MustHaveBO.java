package com.iw.bo;

import com.iw.bo.interfaces.IMustHaveBO;
import com.iw.db.dao.MustHaveDAO;
import com.iw.db.dao.interfaces.IMustHaveDAO;
import com.yuxipacific.services.cache.CacheHandler;
import com.yuxipacific.services.cache.CacheObject;
import java.sql.Connection;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Class to handle all the business logic of the Must Haves
 *
 * @author Alex
 */
public class MustHaveBO implements IMustHaveBO {

    private static final Logger log = Logger.getLogger(MustHaveBO.class);
    private IMustHaveDAO mustHaveDAO = null;
    private static final String MUSTHAVE_CACHE_IDENTIFIER = "musthaves";
    private static CacheObject mustHaveCache = null;

    static {
        mustHaveCache = CacheHandler.getCachedObjectHandler(MUSTHAVE_CACHE_IDENTIFIER);
    }

    public MustHaveBO(Connection conn) {
        this.mustHaveDAO = new MustHaveDAO(conn);

    }

    private MustHaveBO() {
    }

    /**
     * Method to return the MustHaves Associated with a Node!
     *
     * @param nodeID Node Identifier
     * @return {@code String} A must have list comma separated of the given node.
     * @throws Exception
     */
    @Override
    public String getMustHavesByNodeID(Integer nodeID) throws Exception {
        log.debug("getMustHavesByNodeID(Integer)");
        String _tmpMustHaveList;
        try {
            _tmpMustHaveList = (String) mustHaveCache.getObject(nodeID);
            if (_tmpMustHaveList == null) {
                _tmpMustHaveList = getMustHaveDAO().getMustHavesByNodeID(nodeID);
                if (_tmpMustHaveList != null && !_tmpMustHaveList.isEmpty()) {
                    mustHaveCache.setObject(nodeID, _tmpMustHaveList);
                } else {
                    log.warn("There is no must have for node id: " + nodeID + ". Setting empty string as default.");
                    mustHaveCache.setObject(nodeID, "");
                }
            }
        } catch (Exception ex) {
            log.error("An exception ocurred: ", ex);
            throw ex;
        }
        return _tmpMustHaveList;
    }

    /**
     * Method to return all the MustHaves Associated with a Node within the given Corpus!
     *
     * @param corpusID Taxonomy/Corpus Identifier.
     * @return  {@code Map<Integer, MustHave>} A map containing the must haves associated with each node of the given corpus ID. <p><b>Note:</b>The Key of the map is the node ID, the Value is a comma
     * separated list of must haves.</p>
     * @see java.util.Map
     * @throws Exception
     */
    @Override
    public Map<Integer, String> getMustHavesByCorpusID(Integer corpusID) throws Exception {
        log.debug("getMustHavesByCorpusID(Integer)");
        Map<Integer, String> _tmpMustHaveList;

        try {
            _tmpMustHaveList = getMustHaveDAO().getMustHavesByCorpusID(corpusID);
            for (Integer nodeID : _tmpMustHaveList.keySet()) {
                mustHaveCache.setObject(nodeID, _tmpMustHaveList.get(nodeID));
            }
        } catch (Exception ex) {
            log.error("An exception ocurred: ", ex);
            throw ex;
        }
        return _tmpMustHaveList;
    }

    public IMustHaveDAO getMustHaveDAO() {
        return mustHaveDAO;
    }

    public void setMustHaveDAO(IMustHaveDAO mustHaveDAO) {
        this.mustHaveDAO = mustHaveDAO;
    }
}