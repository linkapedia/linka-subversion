package com.iw.bo.interfaces;

import com.iw.db.beans.Terms;
import java.util.List;

/**
 *
 * @author Alex
 */
public interface ICorpusBO {

    public String getAffinityTermsByCorpusID(Integer corpusID) throws Exception;

    public List<Terms> getAllEnabledAffinityTerms() throws Exception;
}
