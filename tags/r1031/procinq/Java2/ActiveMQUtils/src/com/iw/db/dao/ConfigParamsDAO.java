package com.iw.db.dao;

import com.iw.db.ConnectionFactory;
import com.iw.db.beans.ConfigParam;
import com.iw.db.dao.interfaces.IConfigParamsDAO;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class ConfigParamsDAO implements IConfigParamsDAO {

    private static final Logger log = Logger.getLogger(ConfigParam.class);

    @Override
    public List<ConfigParam> getConfigParamsAsList() throws SQLException, Exception {
	log.debug("getConfigParamsAsList()");
	ConfigParam configParam;
	String sQuery = "SELECT PARAMNAME, PARAMVALUE, PARAMTYPE, PARAMDESC FROM CONFIGPARAMS ORDER BY PARAMNAME";
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	List<ConfigParam> configParamsList = new ArrayList<ConfigParam>();
	try {
	    conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sQuery);
	    while (rs.next()) {
		configParam = new ConfigParam();
		configParam.setName(rs.getString("PARAMNAME"));
		configParam.setValue(rs.getString("PARAMVALUE"));
		configParam.setType(rs.getInt("PARAMTYPE"));
		configParam.setDesc(rs.getString("PARAMDESC"));
		configParamsList.add(configParam);
	    }
	    rs.close();
	    rs = null;
	    stmt.close();
	    stmt = null;
	    conn.close();
	    conn = null;
	} catch (SQLException e) {
	    log.error("An sql exception ocurred: " + e.getMessage());
	    throw e;
	} catch (Exception e) {
	    log.error("An exception ocurred: " + e.getMessage());
	    throw e;
	} finally {
	    if (rs != null) {
		rs.close();
		rs = null;
	    }
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	    if (conn != null) {
		conn.close();
		conn = null;
	    }
	}
	return configParamsList;
    }

    @Override
    public Map<String, String> getConfigParamsAsMap() throws SQLException, Exception {
	log.debug("getConfigParamsAsMap()");
	String sQuery = "SELECT PARAMNAME, PARAMVALUE FROM CONFIGPARAMS ORDER BY PARAMNAME";
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	Map<String, String> configParamsList = new HashMap<String, String>();
	try {
	    conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sQuery);
	    while (rs.next()) {
		configParamsList.put(rs.getString("PARAMNAME"), rs.getString("PARAMVALUE"));
	    }
	    rs.close();
	    rs = null;
	    stmt.close();
	    stmt = null;
	    conn.close();
	    conn = null;
	} catch (SQLException e) {
	    log.error("An sql exception ocurred: " + e.getMessage());
	    throw e;
	} catch (Exception e) {
	    log.error("An exception ocurred: " + e.getMessage());
	    throw e;
	} finally {
	    if (rs != null) {
		rs.close();
		rs = null;
	    }
	    if (stmt != null) {
		stmt.close();
		stmt = null;
	    }
	    if (conn != null) {
		conn.close();
		conn = null;
	    }
	}
	return configParamsList;
    }
}
