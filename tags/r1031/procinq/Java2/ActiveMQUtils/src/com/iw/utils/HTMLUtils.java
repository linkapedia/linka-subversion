package com.iw.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
class HTMLUtils {

    private static final Logger log = Logger.getLogger(HTMLUtils.class);

    public static String getDocTitleFromHTMLFile(File file) {
        log.debug("getDocTitleFromHTMLFile()");
        StringBuilder html = new StringBuilder();
        String docTitle = null;
        try {
            //use buffering, reading one line at a time
            //FileReader always assumes default encoding is OK!
            BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
            try {
                String line; //not declared within while loop
                /*
                 * readLine is a bit quirky : it returns the content of a line MINUS the newline. it returns null only for the END of the stream. it returns an empty String if two newlines appear in a
                 * row.
                 */
                while ((line = input.readLine()) != null) {
                    html.append(line);
                    html.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }

            String content = html.toString().replaceAll("\\s+", " ");
            Pattern p = Pattern.compile("<title>(.*?)</title>", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(content);
            while (m.find() == true) {
                docTitle = m.group(1);
            }

            //If the doc title was not found in the title area of the HTML (no title tag found)
            //We search for the first H1 tag in the document (SEO requires this tag).
            if (docTitle == null || "".equals(docTitle)) {
                content = html.toString().replaceAll("\\s+", " ");
                p = Pattern.compile("<h1>(.*?)</h1>", Pattern.CASE_INSENSITIVE);
                m = p.matcher(content);
                while (m.find() == true) {
                    docTitle = m.group(1);
                }
            }
        } catch (IOException ex) {
            log.error("Error in getDocTitleFromHTMLFile().", ex);
        }
        return docTitle;
    }
}
