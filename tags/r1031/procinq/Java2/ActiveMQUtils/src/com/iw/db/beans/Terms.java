package com.iw.db.beans;

import java.io.Serializable;

/**
 *
 * @author Alex
 */
public class Terms implements Serializable {

    /**
     * Taxonomy ID
     */
    private Integer corpusID;
    /**
     * List of comma separated values.
     */
    private String terms;

    public Integer getCorpusID() {
        return corpusID;
    }

    public void setCorpusID(Integer corpusID) {
        this.corpusID = corpusID;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @Override
    public String toString() {
        return "Terms{" + "corpusID=" + corpusID + ", terms=" + terms + '}';
    }
}
