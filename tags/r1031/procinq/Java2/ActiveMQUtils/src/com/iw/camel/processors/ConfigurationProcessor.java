package com.iw.camel.processors;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

import com.iw.activemq.beans.ConfigurationPackage;
import com.iw.activemq.procinq.QueueProducer;
import com.iw.application.context.AppServletContextListener;
import com.iw.rest.beans.Classifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationProcessor implements Processor {

    private static final transient Logger log = LoggerFactory.getLogger(ConfigurationProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("ConfigurationProcessor::process(Exchange)");
        Message message = exchange.getIn();
        Object configurationObject = message.getBody();
        if(configurationObject instanceof ConfigurationPackage) {
            ConfigurationPackage confPkg = (ConfigurationPackage) configurationObject;
            String destination = (confPkg.getDestination() != null) ? confPkg.getDestination() : "ALL";
            Classifier classifier = Classifier.getLocalInstance();
            log.info("ConfigMessage: " + confPkg.getTask() + " " + destination);
            if (confPkg.getTask().equals("stop")) {
                //Validate if the message is for this system.
                if (destination.equals("ALL") || destination.equals(classifier.getIpAddress())) {
                    CamelContext camelCtx = AppServletContextListener.getCamelContext();
                    // remove myself from the in flight registry so we can stop this route without trouble
                    camelCtx.getInflightRepository().remove(exchange);
                    // stop this route
                    camelCtx.stopRoute("ClassificationQueue");
                }
            } else if (confPkg.getTask().equals("start")) {
                //Validate if the message is for this system.
                if (destination.equals("ALL") || destination.equals(classifier.getIpAddress())) {
                    CamelContext camelCtx = AppServletContextListener.getCamelContext();
                    // start this route
                    camelCtx.startRoute("ClassificationQueue");
                }
            } else if (confPkg.getTask().equals("register")) {
                QueueProducer.registerClassifier(classifier);
            }  else if (confPkg.getTask().equals("refresh-classifiers-list")) {
                //This is run by each classifier that gets the message.
                log.info("Updating classifier"+classifier.getId()+"["+classifier.getIpAddress()+"]");
                QueueProducer.registerClassifier(classifier);
            }
        }
    }
}
