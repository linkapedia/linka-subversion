package com.iw.bo;

import com.iw.bo.interfaces.ICorpusBO;
import com.iw.db.beans.Terms;
import com.iw.db.dao.CorpusDAO;
import com.iw.db.dao.interfaces.ICorpusDAO;
import java.sql.Connection;
import java.util.List;
import org.apache.jcs.access.exception.InvalidArgumentException;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class CorpusBO implements ICorpusBO {

    private static final Logger log = Logger.getLogger(CorpusBO.class);
    private ICorpusDAO corpusDAO = null;

    public CorpusBO(Connection conn) {
        corpusDAO = new CorpusDAO(conn);
    }

    private CorpusBO() {
    }

    @Override
    public String getAffinityTermsByCorpusID(Integer corpusID) throws Exception {
        log.debug("getAffinityTermsByCorpusID(Integer)");
        if (corpusID == null || corpusID == 0) {
            log.error("No valid argument found");
            throw new InvalidArgumentException("You must provide a valid id");
        }
        return getCorpusDAO().getAffinityTermsByCorpusID(corpusID);
    }

    @Override
    public List<Terms> getAllEnabledAffinityTerms() throws Exception {
        log.debug("getAllEnabledAffinityTerms()");
        return getCorpusDAO().getAllEnabledAffinityTerms();
    }

    public ICorpusDAO getCorpusDAO() {
        return corpusDAO;
    }

    public void setCorpusDAO(ICorpusDAO corpusDAO) {
        this.corpusDAO = corpusDAO;
    }
}
