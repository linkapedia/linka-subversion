package com.iw.db.beans;

import java.io.Serializable;

/**
 *
 * @author Alex
 */
public class ConfigParam implements Serializable{

    private String name;
    private String value;
    private String desc;
    private int type;

    public ConfigParam() {
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
