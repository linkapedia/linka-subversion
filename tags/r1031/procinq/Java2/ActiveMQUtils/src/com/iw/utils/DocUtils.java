package com.iw.utils;

import com.iw.beans.ClassificationDocument;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class DocUtils {

    private static final Logger log = Logger.getLogger(DocUtils.class);

    /**
     * 
     * @param classDoc
     * @see {@link ClassificationDocument}
     * @return 
     */
    public static String getDocTitleFromFile(ClassificationDocument classDoc) {
        log.debug("getDocTitleFromFile(ClassificationDocument)");
        String docTitle = null;
        try {
            if (classDoc.getFileType().equals(ClassificationDocument.HTML_TYPE)) {
                docTitle = HTMLUtils.getDocTitleFromHTMLFile(classDoc.getInternalFile());
            } else if (classDoc.getFileType().equals(ClassificationDocument.PDF_TYPE)) {
                docTitle = PDFUtils.getDocTitleFromPDF(classDoc.getInternalFile());
            } else if (classDoc.getFileType().equals(ClassificationDocument.WORD_TYPE) || classDoc.getFileType().equals(ClassificationDocument.WORD_2010_TYPE)) {
                docTitle = MSWordUtils.getDocTitleFromWord(classDoc.getInternalFile());
            }
        } catch (Exception e) {
            log.error("An exception occured in getting the title of PDF Document.", e);
        }
        return docTitle;
    }
}
