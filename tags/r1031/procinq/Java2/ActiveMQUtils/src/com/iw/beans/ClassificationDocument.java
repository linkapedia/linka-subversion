package com.iw.beans;

import java.io.File;

/**
 *
 * @author Alex
 */
public class ClassificationDocument {

    private File internalFile;
    private String fileName;
    private String fileType;
    private String fileRemoteURI;
    private String fileWebCacheURI;
    public static final String HTML_TYPE = "html";
    public static final String PDF_TYPE = "pdf";
    public static final String WORD_TYPE = "doc";
    public static final String WORD_2010_TYPE = "docx";

    public ClassificationDocument() {
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileRemoteURI() {
        return fileRemoteURI;
    }

    public void setFileRemoteURI(String fileRemoteURI) {
        this.fileRemoteURI = fileRemoteURI;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileWebCacheURI() {
        return fileWebCacheURI;
    }

    public void setFileWebCacheURI(String fileWebCacheURI) {
        this.fileWebCacheURI = fileWebCacheURI;
    }

    public File getInternalFile() {
        return internalFile;
    }

    public void setInternalFile(File internalFile) {
        this.internalFile = internalFile;
        if (internalFile != null) {
            setFileWebCacheURI(internalFile.getAbsolutePath());
        }
    }

    @Override
    public String toString() {
        return "ClassificationDocument{" + "fileName=" + fileName + ", fileType=" + fileType + ", fileRemoteURI=" + fileRemoteURI + '}';
    }
}
