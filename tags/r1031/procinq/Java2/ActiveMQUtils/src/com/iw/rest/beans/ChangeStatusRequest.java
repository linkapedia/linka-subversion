package com.iw.rest.beans;

import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class ChangeStatusRequest {

    private static final Logger log = Logger.getLogger(ChangeStatusRequest.class);
    private Task[] task;

    public Task[] getTask() {
        return task;
    }

    public void setTask(Task[] task) {
        this.task = task;
    }
}
