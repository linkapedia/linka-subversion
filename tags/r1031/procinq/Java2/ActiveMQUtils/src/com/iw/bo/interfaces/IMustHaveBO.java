package com.iw.bo.interfaces;

import java.util.Map;

/**
 *
 * @author Alex
 */
public interface IMustHaveBO {

    /**
     * Method to return the MustHaves Associated with a Node!
     *
     * @param nodeID Node Identifier
     * @return {@code String} A must have list comma separated of the given
     * node.
     * @throws Exception
     */
    public String getMustHavesByNodeID(Integer nodeID) throws Exception;

    /**
     * Method to return all the MustHaves Associated with a Node within the
     * given Corpus!
     *
     * @param corpusID Taxonomy/Corpus Identifier.
     * @return  {@code Map<Integer, MustHave>} A map containing the must haves
     * associated with each node of the given corpus ID. <p><b>Note:</b>The Key
     * of the map is the node ID, the Value is a comma separated list of must
     * haves.</p>
     * @see java.util.Map
     * @throws Exception
     */
    public Map<Integer, String> getMustHavesByCorpusID(Integer corpusID) throws Exception;
}
