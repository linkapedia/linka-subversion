create table DescriptionRecord (
  drID Number(9) NOT NULL,
  Name Varchar2(256),
  DateCreated DATE, DateRevised Date, DateEstablished Date,
  ActiveMeshYearList Number(4) DEFAULT 2004,
  HistoryNote Varchar2(4000),
  OnlineNote Varchar2(4000),
  PublicMeshNote Varchar2(4000)) TABLESPACE RDATA;
alter table descriptionrecord add primary key (drID);

create table AllowableQualifier (
   aqID Number(9) NOT NULL,
   UI Varchar2(256),
   Name Varchar2(256));
alter table allowablequalifier add primary key (aqID);

create table PreviousIndexing (
   piID Number(9) NOT NULL,
   Name Varchar2(256));
alter table previousindexing add primary key (piID);

create table treeNumbers (
   trID Number(9) NOT NULL,
   trNum Varchar2(256));
alter table treenumbers add primary key (trid);

create table DescriptorRecordAllowableQual (
   drID Number(9) NOT NULL,
   aqID Number(9) NOT NULL);
alter table descriptorrecordallowablequal add foreign key (drID) references DescriptionRecord (drID) on delete cascade;
alter table descriptorrecordallowablequal add foreign key (aqID) references AllowableQualifier (aqID) on delete cascade;

create table DescriptorRecordPreviousIndex (
   drID Number(9) NOT NULL,
   piID Number(9) NOT NULL);
alter table descriptorrecordpreviousindex add foreign key (drID) references DescriptionRecord (drID) on delete cascade;
alter table descriptorrecordpreviousindex add foreign key (piID) references PreviousIndexing (piID) on delete cascade;

create table DescriptorRecordTreeNumber (
   drID Number(9) NOT NULL,
   trID Number(9) NOT NULL) tablespace RDATA;
alter table descriptorrecordtreenumber add foreign key (drID) references DescriptionRecord (drID) on delete cascade;
alter table descriptorrecordtreenumber add foreign key (trID) references TreeNumbers (trID) on delete cascade;

create table Concept (
   cID Number(9) NOT NULL,
   PreferredConcept Varchar2(256),
   Name Varchar2(256),
   UMLSUI Varchar2(256),
   CASN1Name varchar2(256),
   RegistryNumber varchar2(256),
   ScopeNote varchar2(4000)) TABLESPACE RDATA;
alter table Concept add primary key (cID);

create table DescriptorRecordConcept (
    drID Number(9) NOT NULL,
    cID Number(9) NOT NULL) TABLESPACE RDATA;
alter table descriptorrecordconcept add foreign key (drID) references DescriptionRecord (drID) on delete cascade;
alter table descriptorrecordconcept add foreign key (cID) references Concept (cID) on delete cascade;

create table ConceptRelationship (
   cID Number(9) NOT NULL,
   c2ID Number(9) NOT NULL,
   Type Varchar2(4) NOT NULL) TABLESPACE RDATA;
alter table conceptrelationship add foreign key (cID) references Concept (cID) on delete cascade;
alter table conceptrelationship add foreign key (c2ID) references Concept (cID) on delete cascade;

create table SemanticTypes (
   stID Number(9) NOT NULL,
   UI Varchar2(256),
   Name Varchar2(256)) TABLESPACE RDATA;
alter table SemanticTypes add primary key (stID);

create table PharmcologicalActions (
   paID Number(9) NOT NULL,
   UI Varchar2(256),
   Name Varchar2(256)) TABLESPACE RDATA;
alter table PharmcologicalActions add primary key (paID);

create table Terms (
   tID Number(9) NOT NULL,
   UI Varchar2(256),
   Name Varchar2(256),
   DateCreated Date) TABLESPACE RDATA;
alter table Terms add primary key (tID);

create table ConceptSemanticTypes (
   cID Number(9) NOT NULL,
   stID Number(9) NOT NULL) TABLESPACE RDATA;
alter table conceptsemantictypes add foreign key (cID) references Concept (cID) on delete cascade;
alter table conceptsemantictypes add foreign key (stID) references SemanticTypes (stID) on delete cascade;

create table ConceptPharmcologicalAction (
   cID Number(9) NOT NULL,
   paID Number(9) NOT NULL) TABLESPACE RDATA;
alter table conceptpharmcologicalaction add foreign key (cID) references Concept (cID) on delete cascade;
alter table conceptpharmcologicalaction add foreign key (paID) references PharmcologicalActions (paID) on delete cascade;

create table ConceptTerm (
   cID Number(9) NOT NULL,
   tID Number(9) NOT NULL) TABLESPACE RDATA;
alter table conceptterm add foreign key (cID) references Concept (cID) on delete cascade;
alter table conceptterm add foreign key (tID) references Terms (tID) on delete cascade;

COMMIT WORK;

create sequence DescriptionRecordSeq start with 1;
create sequence AllowableQualifierSeq start with 1;
create sequence PreviousIndexingSeq start with 1;
create sequence TreeNumbersSeq start with 1;
create sequence ConceptSeq start with 1;
create sequence SemanticTypesSeq start with 1;
create sequence PharmcologicalActionsSeq start with 1;
create sequence TermsSeq start with 1;

COMMIT WORK;

create trigger DescriptionRecord_trigger before insert on DescriptionRecord for each row when (new.drID is null)
  begin select DescriptionRecordSeq.nextval into :new.drID from dual;
  end;
/

create trigger AllowableQualifier_trigger before insert on AllowableQualifier for each row when (new.aqrID is null)
  begin select AllowableQualifierSeq.nextval into :new.aqID from dual;
  end;
/

create trigger PreviousIndexing_trigger before insert on PreviousIndexing for each row when (new.piID is null)
  begin select PreviousIndexingSeq.nextval into :new.piID from dual;
  end;
/

create trigger TreeNumbers_trigger before insert on TreeNumbers for each row when (new.trID is null)
  begin select TreeNumbersSeq.nextval into :new.trID from dual;
  end;
/

create trigger Concept_trigger before insert on Concept for each row when (new.cID is null)
  begin select ConceptSeq.nextval into :new.cID from dual;
  end;
/

create trigger SemanticTypes_trigger before insert on SemanticTypes for each row when (new.stID is null)
  begin select SemanticTypesSeq.nextval into :new.stID from dual;
  end;
/

create trigger PharmcologicalActions_trigger before insert on PharmcologicalActions for each row when (new.paID is null)
  begin select PharmcologicalActionsSeq.nextval into :new.paID from dual;
  end;
/

create trigger Terms_trigger before insert on Terms for each row when (new.tID is null)
  begin select TermsSeq.nextval into :new.tID from dual;
  end;
/

COMMIT WORK;
