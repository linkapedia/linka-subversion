#!/usr/bin/perl
#
# modify-node.cgi
# Written by: Michael A. Puscar

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;

if ((!defined($query->param('nid'))) ||
    (!defined($query->param('cname'))) ||
    (!defined($query->param('cid')))) {
    print "Location: /seditor/missing-info.html\n\n";
    exit(1);
}

local ($cid) = $query->param('cid');
local ($nid) = $query->param('nid');
local ($cname) = $query->param('cname');
local ($list) = $query->param('OldStuff_lst');

if (defined($query->param('Revert'))) {
    print "Location: engines.cgi?cid=$cid&nid=$nid&cname=$cname\n\n"; exit(1);
}

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:gaea", "sbooks", "racer9");

$sth = $dbh->prepare("delete from NodeGroup where NodeId = $nid") || die "Cannot insert group: $!\n";
$sth->execute() || die "Cannot remove group: $!\n";
$sth->finish;

@categories = split(/\,/, $list);
foreach $catid (@categories) {
    if (($catid ne ".none") && ($nid != -1)) {
       $sth = $dbh->prepare("insert into NodeGroup values ($nid, $catid)") || die "Cannot insert group: $!\n";
       $sth->execute() || die "Cannot insert group: (inserting $nid, $catid) $!\n";
       $sth->finish;
    }
}

print "Location: engines.cgi?cid=$cid&nid=$nid&cname=$cname\n\n";
$dbh->disconnect;

1;
