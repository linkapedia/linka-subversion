#!/usr/bin/perl
#
# show-blacklist.cgi
# Written by: Michael A. Puscar
# - show all global blacklists

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local (%cookies) = getCookies();

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");

if (defined($query->param('submit'))) { 
    local (@kill_list);

    # Get targets for deletion
    for ($i = 1; $i <= $query->param('count'); $i++) {
	my ($id) = 'id'.$i;
	if (defined($query->param($id))) { push (@kill_list, $query->param($id)); }
    }
    
    # KILL targets
    foreach $kill (@kill_list) { 
	my ($q) = "DELETE FROM BlackList WHERE BlackListId = $kill";
	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish; undef $q; undef $sth;

	my ($q) = "INSERT INTO EditorLog (EditorId, ActionCode, Action_String, Action_Date) ";
	$q = $q."values (".$cookies{'EDITORID'}.", 6, 'Delete black list entry', SYSDATE)";
	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish; undef $q;
    }
    print "Location: /seditor/blacklist-done.html\n\n";

    $dbh->disconnect();

} else {
    my ($q) = "SELECT B.BlackListId, C.ClientName, B.URLChunk, E.FirstName, E.LastName, B.DateCreated FROM BlackList B, Client C, Editor E where B.ClientId = C.ClientId AND E.EditorId = B.EditorId AND B.NodeId IS NULL AND C.CorpusId IN (SELECT CorpusId FROM Corpus WHERE PublisherId = ".$cookies{'PUBLISHERID'}.") order by B.DateCreated desc";
    my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
    $sth->execute() || warn "Fatal database error: $!\n";
    
    print "Content-type: text/html\n\n";
    &print_head();
    print "<form method=GET action='show-blacklist.cgi'>\n";
    print "<table width=91% border=0 bgcolor=eeeeee>\n";
    print "<tr><td colspan=6><input type='submit' name='submit' value='UPDATE BLACK LIST'><br>&nbsp;</td></tr>\n";
    print "<tr><td align=center> Delete? </td> <td align=center> Corpus </td><td align=center> Black List</td><td align=center> Editor</td><td align=center> Date</td></tr>\n";
    local ($loop)= 0; while (my ($bid, $name, $word, $first, $last, $date) = $sth->fetchrow_array) {
	$loop++; print "<tr><td align=center> <input type='checkbox' name='id$loop' value='$bid'> </td>\n";
	print "<td> $name </td><td> $word </td><td> $first $last </td> <td> $date </td></tr>\n";
    }
    print "<input type='hidden' name='count' value='$loop'>\n";
    print "<tr><td colspan=6> &nbsp; <BR><input type='submit' name='submit' value='UPDATE BLACK LIST'></td></tr>\n";
    print "</table></form>\n";
    &print_foot();

    $sth->finish;
}
$dbh->disconnect;

1;

