#!/usr/bin/perl
#
# auth.cgi
# Written by: Michael A. Puscar
# Check user against our authentication database.
# If the user is valid, write user information into the cookie file.

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local ($USERNAME, $PASSWORD) = "";
local (%cookies) = getCookies();

if (defined($cookies{'USERNAME'})) { $USERNAME = $cookies{'USERNAME'}; }
if (defined($cookies{'PASSWORD'})) { $PASSWORD = $cookies{'PASSWORD'}; }
if (defined($query->param('USERNAME'))) { $USERNAME = $query->param('USERNAME'); }
if (defined($query->param('PASSWORD'))) { $PASSWORD = $query->param('PASSWORD'); }

# Were username and password information even sent?
if (($USERNAME eq "") || ($PASSWORD eq "")) { 
    print "Location: /seditor/bad-username.html\n\n";
    exit(1);
}

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
my ($gui) = &get_user_info($USERNAME, $PASSWORD);
my ($firstname, $lastname, $accesslevel, $editorid, $pubid) = $gui->fetchrow_array;
$gui->finish();

if (!defined($firstname)) { 
   print "Location: /seditor/bad-username.html\n\n";
}

local ($name) = $firstname." ".$lastname;
local ($cid);

if (defined($cookies{'CORPUSID'})) { $cid = $cookies{'CORPUSID'}; }
else { 
   my ($sth) = &get_corpus($pubid);
   ($cid, $cname) = $sth->fetchrow_array;
}
$dbh->disconnect;

print "Content-type: text/html\n\n";
print "<HTML><HEAD><SCRIPT LANGUAGE='JavaScript' SRC='cookies.js'></SCRIPT>\n";
print "<SCRIPT LANGUAGE='JavaScript'>\n";
print "  SetCookie ('USERNAME','".$USERNAME."');\n";
print "  SetCookie ('PASSWORD','".$PASSWORD."');\n";
print "  SetCookie ('FNAME','".$name."');\n";
print "  SetCookie ('EDITORID','".$editorid."');\n";
print "  SetCookie ('PUBLISHERID','".$pubid."');\n";
print "  SetCookie ('CORPUSID','".$cid."');\n";
print "  SetCookie ('ACCESS',".$accesslevel.");\n";
print "</SCRIPT></HEAD>\n";

&print_head();
print "<font face=\"Arial\" size=+1>Logging <b><u>$USERNAME</u></b> into the system, PLEASE WAIT...</font>\n";
print "<SCRIPT>function redirect() { window.location='main.cgi'; }  setTimeout(\"redirect();\", 5000)</SCRIPT>\n";
&print_foot();

1;


