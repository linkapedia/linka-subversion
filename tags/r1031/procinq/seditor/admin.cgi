#!/usr/bin/perl
#
# admin.cgi
# Written by: Michael A. Puscar
#
# Functions: 
# 	dbl	Add / remove a domain from the black list
#	cpw	Change password
#	vea	View editor activity log
#	anu	Manage users

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

# Read cookies
local (%cookies);
@tmparr = split (/; /,$ENV{'HTTP_COOKIE'}); 
foreach(@tmparr){ ($name, $value) = split (/=/,$_); $cookies{$name} = $value; }

local ($query) = new CGI;

if ($query->param('func') eq "dbl") { &display_black_list(); exit(1); }
if ($query->param('func') eq "cpw") { 
   if ($cookies{'ACCESS'} == 9) { print "Location: change-pw-admin.html\n\n"; }
   print "Location: change-pw.html\n\n"; exit(1);
}
if ($query->param('func') eq "anu") { 
   if ($cookies{'ACCESS'} == 9) { print "Location: modify-admin.html\n\n"; }
   print "Location: modify.html\n\n"; }
if ($query->param('func') eq "vea") { 
   $user = ""; if (defined($query->param('user'))) { $user = $query->param('user'); }
   $date = ""; if (defined($query->param('date'))) { $date = $query->param('date'); }
   &view_activity($date, $user); exit(1);
}

exit(1);
