#!/usr/bin/perl
#
# show-blacklist.cgi
# Written by: Michael A. Puscar
# - show all global blacklists

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local (%cookies) = getCookies();

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
local $clientname = $query->param('clientname');
$clientname =~ s/\ /\%20/gi;

my ($q) = "SELECT B.BlackListId, C.ClientName, B.URLChunk, E.FirstName, E.LastName, B.DateCreated FROM BlackList B, Client C, Editor E where B.ClientId = C.ClientId AND E.EditorId = B.EditorId AND B.NodeId = ".$query->param('nid')." AND C.CorpusId IN (SELECT CorpusId FROM Corpus WHERE PublisherId = ".$cookies{'PUBLISHERID'}.") order by B.DateCreated desc";
my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
$sth->execute() || warn "Fatal database error: $!\n";
    
    print "Content-type: text/html\n\n";
    &print_head();
    print "<form method=GET action='show-blacklist.cgi'>\n";
    print "[<a href='GetNode.cgi?nid=".$query->param('nid')."&cid=".$query->param('cid')."&hid=".$query->param('hid')."&clid=".$query->param('clid')."&clientname=".$clientname."'>Return to the Node Menu</a>]<p>\n";
    print "<table width=95% border=0 bgcolor=eeeeee>\n";
    print "<tr><td colspan=6><input type='submit' name='submit' value='UPDATE BLACK LIST'><br>&nbsp;</td></tr>\n";
    print "<tr><td align=center><b><font size=-1>Delete?</font></b></td> <td align=center><b><font size=-1>Corpus</font></b></td><td align=center><b><font size=-1>Black List</font></b></td><td align=center><b><font size=-1>Editor</font></b></td><td align=center><b><font size=-1>Date</font></b></td></tr>\n";
    local ($loop)= 0; while (my ($bid, $name, $word, $first, $last, $date) = $sth->fetchrow_array) {
	$loop++; print "<tr><td align=center> <input type='checkbox' name='id$loop' value='$bid'> </td>\n";
	print "<td align=center> <font size=-1>$name</font> </td><td align=center><font size=-1>$word</font> </td><td align=center><font size=-1>$first $last</font> </td> <td align=center> <font size=-1>$date</font> </td></tr>\n";
    }
    print "<input type='hidden' name='count' value='$loop'>\n";
    print "<tr><td colspan=6> &nbsp; <BR><input type='submit' name='submit' value='UPDATE BLACK LIST'></td></tr>\n";
    print "</table></form>\n";
    &print_foot();

    $sth->finish;
$dbh->disconnect;

1;

