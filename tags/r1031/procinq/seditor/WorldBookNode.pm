package WorldBookNode;

#
# Each node has the following properties: 
#
#    NODE ID:	Nid as assigned by the database
#    CHILDREN:  An array of node id's representing children of this node
#    TITLE:     The node title
#    URL:	Relative URL where this node can be found  
#    PARENT:	Node id for the current node's parent
#

sub new { bless {}, __PACKAGE__ } 

sub Nid {
    my $self = shift;
    $self->{Nid} = shift if @_;
    return $self->{Nid};
}

sub Children {
    my $self = shift;
    $self->{Children} = shift if @_;
    return $self->{Children};
}

sub URL {
    my $self = shift;
    $self->{URL} = shift if @_;
    return $self->{URL};
}

sub Title {
    my $self = shift;
    $self->{Title} = shift if @_;
    return $self->{Title};
}

sub Pid {
    my $self = shift;
    $self->{Pid} = shift if @_;
    return $self->{Pid};
}

1;
