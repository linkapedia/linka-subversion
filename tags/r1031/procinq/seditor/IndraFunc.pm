sub children {
 my $q = "select nodeid, nodetitle from node where parentid = ". $query->param('node');

 my $sth = $dbh->prepare($q) or return undef;
 $sth->execute() or return undef;

 return ($sth, $sth->rows);
}

sub get_corpus {
 my $pubid = $_[0];

 my $q = "select corpusid, corpus_name from corpus where publisherid = $pubid";

 my $sth = $dbh->prepare($q) or return undef;
 $sth->execute() or return undef;

 return ($sth);
}

sub get_user_info {
 my ($USERNAME, $PASSWORD) = @_;

 my $q = "select firstname, lastname, accesslevel, editorid, publisherid from editor";
 $q = $q . " where username = Upper('" . $USERNAME;
 $q = $q . "') and password = Upper('" . $PASSWORD;
 $q = $q . "')";

 my $sth = $dbh->prepare($q) or return undef;
 $sth->execute() or return undef;

 return ($sth, $sth->rows);
}

sub display_black_list {

   local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");

   # User attempted to add or remove a domain from the black list 
   if (defined($query->param('submit'))) {
      my @names = $query->param; $eid = $cookies{'EDITORID'};
      foreach $name (@names) {
	 if (($name ne "new") && ($name ne "func") && ($name ne "submit")) {
	    &remove_black_list($name, $eid); } 
      }
 
      if ($query->param('new') ne "") { &add_black_list($query->param('new'), $eid); }
   }
 
   my $q = "select d.domainname, d.datecreated, e.firstname, e.lastname";
   $q = $q . " from DomainBlackList d, Editor e where e.editorid = d.editorid";

   my $sth = $dbh->prepare($q) or print "Location: error.html\n\n";
   $sth->execute() or print "Location: error.html\n\n";

   print "Content-type: text/html\n\n";
   &w_file("templates/head.tpl"); 
   &w_file("templates/dblhead.tpl");

   while ( ($dname, $dcreated, $fname, $lname) = $sth->fetchrow_array) {
      print "<tr><td>$dname</td> <td>$fname $lname</td> <td>$dcreated</td> <td align=center><input type='checkbox' name='$dname'></tr>\n";
   }

   &w_file("templates/dblfoot.tpl");
   &w_file("templates/foot.tpl");

   $sth->finish();
   $dbh->disconnect();
   return 1;
}

# Write from file to standard out (HTTP)
sub w_file { my ($fn) = @_; open (FILE, $fn); while (<FILE>) { print $_; } close(FILE); }

# Add a new domain to the domain black list, then write to the log table
sub add_black_list { 
   my ($domain, $eid) = @_;

   # Insert into the domain black list table
   my $q = "insert into DomainBlackList (editorid, domainname, datecreated) ";
   $q = $q . "values ($eid, '$domain', SYSDATE)";

   my $sth = $dbh->prepare($q) or print "Location: error.html\n\n";
   $sth->execute() or print "Location: error.html\n\n";

   # Insert into the log table
   &log($eid, 1, "Add $domain");
   return 1;
}

# EDITORID                                  NOT NULL NUMBER(4)
# ACTIONCODE                                NOT NULL NUMBER(2)
# ACTION_STRING                                      VARCHAR2(30)
# NODEID                                             NUMBER(6)
# ACTIONDATE                                NOT NULL DATE
sub log {
   my ($eid, $actioncode, $action_string) = @_;

   # insert into the log table
   my $q = "insert into EditorLog (editorid, actioncode, action_string, actiondate) ";
   $q = $q . "values ($eid, $actioncode, '$action_string', SYSDATE)";

   my $sth = $dbh->prepare($q) or print "Location: error.html\n\n";
   $sth->execute() or print "Location: error.html\n\n";
  
   return 1; 
}

sub remove_black_list {
   my ($domain, $eid) = @_;

   # Insert into the domain black list table
   my $q = "delete from DomainBlackList where domainname = '$domain'";

   my $sth = $dbh->prepare($q) or print "Location: error.html\n\n";
   $sth->execute() or print "Location: error.html\n\n";

   # Insert into the log table
   &log($eid, 1, "Deleted $domain");
   return 1;
}

# EDITORID                                  NOT NULL NUMBER(4)
# ACTIONCODE                                NOT NULL NUMBER(2)
# ACTION_STRING                                      VARCHAR2(30)
# NODEID                                             NUMBER(6)
# ACTIONDATE                                NOT NULL DATE
sub view_activity {
   my ($date, $user) = @_;
   if ($date eq "") { $date = 7; }
  
   local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");

   # Select and display the activity log from the database
   my $q = "select e.firstname, e.lastname, v.action_string, v.actiondate";
   $q = $q . " from editor e, editorlog v where e.editorid = v.editorid and";
   $q = $q . " trunc(v.actiondate) > trunc(SYSDATE-$date)";
   if ($user ne "") { $q = $q . " and e.username = UPPER('$user')"; }
   $q = $q . " order by v.actiondate";

   my $sth = $dbh->prepare($q) or print "Location: error.html\n\n";
   $sth->execute() or print "Location: error.html\n\n";
   
   print "Content-type: text/html\n\n";
   &w_file("templates/head.tpl"); 
   &w_file("templates/vahead.tpl");
   while ( ($fname, $lname, $action, $adate) = $sth->fetchrow_array) {
      print "<tr><td>$lname, $fname</td> <td>$action</td> <td>$adate</td></tr>\n";
   }
   &w_file("templates/vafoot.tpl");
   &w_file("templates/foot.tpl");
   $sth->finish(); $dbh->disconnect();
  
   return 1; 
}

sub change_password {
   return 1;
}

sub add_user {
   return 1;
}

sub delete_user {
   return 1;
}

sub remove_user {
   return 1;
}

sub getCookies {
    my(%cookies);

    foreach (split (/; /,$ENV{'HTTP_COOKIE'})){
        my($key) = split(/=/, $_);

        $cookies{$key} = substr($_, index($_, "=")+1);
    } 
    return(%cookies);
}

sub print_template {
   # Print out the template
    open (FILE, "templates/$_[0]");  my @lines = <FILE>; close(FILE);
    foreach $line (@lines) { $line =~ s/##(.*)##/${$1}/gi; print $line; }
}

sub print_head_restricted {
   local ($corpus) = $_[0];

   # Print out the template
    open (FILE, "templates/head-restricted.tpl");  my @lines = <FILE>; close(FILE);
    foreach $line (@lines) { $line =~ s/##(.*)##/${$1}/gi; print $line; }
}

sub print_head_with_cookies {
   local ($corpus) = $_[0];

   # Print out the template
    open (FILE, "templates/head-with-cookies.tpl");  my @lines = <FILE>; close(FILE);
    foreach $line (@lines) { $line =~ s/##(.*)##/${$1}/gi; print $line; }
}

sub print_head {
   # Print out the header
    open (FILE, "templates/head.tpl");
    while (<FILE>) { print; }
    close (FILE);
}

sub print_foot {
    # Print out the footer
    open (FILE, "templates/foot.tpl");
    while (<FILE>) { print; }
    close (FILE);
}

1;

