#!/usr/bin/perl
#
# insert.cgi
# Written by: Michael A. Puscar
# - Allow insertion of editorial links

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local (%cookies) = getCookies();
local ($results_per_page) = 100;

# Were username and password information even sent?
if ((!defined($query->param('hid'))) || 
    (!defined($query->param('clid'))) ||
    (!defined($query->param('clientname'))) ||
    (!defined($query->param('nid'))) ||
    (!defined($query->param('cid')))) {
    print "Location: /seditor/missing-info.html\n\n";
    exit(1);
}

local ($hid) = $query->param('hid');
local ($cid) = $query->param('cid');
local ($nid) = $query->param('nid');
local ($clid) = $query->param('clid');
local ($clientname) = $query->param('clientname');

# Only display 100 results per page.   If not specified, assume page 1.
local ($page);  if (defined($query->param('page'))) { $page = $query->param('page'); }
else { $page = 1; }

local ($ARGUMENTS) = "hid=$hid&cid=$cid&nid=$nid&clid=$clid&clientname=$clientname&page=$page";

# If defined, then user requested insert of some links
if (defined($query->param('submit'))) { 
    # Connect to the LINUX Oracle database
    local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
    local ($url) = $query->param('url');

    if ($url !~ /http/i) { $url = "http://".$url; }

    # CREATE OR REPLACE PROCEDURE InsertDocument (
    #         NodeIdIn IN Number,
    #         DocTitleIn IN Long,
    #         DocURLIn IN Long,
    #         DocumentSumIn IN Long,
    #         ChannelidIn IN Long,
    #         ScoreOneIn IN Long,
    #         ScoreTwoIn IN Long,
    #         ScoreThreeIn IN Long,
    #         MachineNameIn IN Long,
    #         DocumentsIn IN Number)
    my $insert_document = $dbh->prepare(q{
        BEGIN
            InsertDocument(
                      NODEIDIN => :nid,
                      DOCTITLEIN => :title,
                      DOCURLIN => :url,
                      DOCUMENTSUMIN => :sum,
                      CHANNELIDIN => :channel,
                      SCOREONEIN => :scoreone,
                      SCORETWOIN => :scoretwo,
                      SCORETHREEIN => :scorethree,
                      MACHINENAMEIN => :machinename,
                      DOCUMENTSIN => :documents
                      );
        END;
    });

    $insert_document->bind_param(":nid", $query->param('nid'));
    $insert_document->bind_param(":title", $query->param('title'));
    $insert_document->bind_param(":url", $url);
    $insert_document->bind_param(":sum", $query->param('summary'));
    $insert_document->bind_param(":channel", 100);
    $insert_document->bind_param(":scoreone", 101);
    $insert_document->bind_param(":scoretwo", 101);
    $insert_document->bind_param(":scorethree", 101);
    $insert_document->bind_param(":machinename", "Editor: ".$cookies{'EDITORID'});
    $insert_document->bind_param(":documents", 1);
    $insert_document->execute || die ("DB insert failure: ".$insert_document->err." -- ".$dbh->errstr);
#    $insert_document->execute || &raise_db_error($insert_document->err, $dbh->errstr);
    
    $insert_document->finish;
    $dbh->disconnect;

    print "Location: /seditor/blacklist-done.html\n\n";
}

print "Content-type: text/html\n\n";
&print_head();
print "<form action='insert.cgi' method='get'>\n";
print "<input type='hidden' name='hid' value=$hid>";
print "<input type='hidden' name='cid' value=$cid>";
print "<input type='hidden' name='nid' value=$nid>";
print "<input type='hidden' name='clid' value=$clid>";
print "<input type='hidden' name='clientname' value='$clientname'>";
print "<input type='hidden' name='page' value=$page>\n";
&print_template("insert.tpl");
&print_foot();

1;
