Your submissions have been successfully written into corpus normal form.   
An archive file has been created on your behalf.
<P>
You can validate the corpus normal form by clicking the button below.   Validation will
check for unusually short section files, titles longer than 150 characters, titles containing 
non-alphanumeric characters, and blank titles.
<P>
To continue, please click the "<b>validate</b>" button below.
<P>
<form method=post action=validate.cgi>
<input type=text name=destination value='##goto##' size=20>
<input type="submit" name="validate" value="Validate CNF">
</form>
</blockquote>
<table width=765><tr><td>
<div align="center"><p align=center><font face="Arial, Helvetica, sans-serif"> 2002 IndraWeb Ltd.  All rights reserved<br>
  <a class=footer href="http://www.surfablebooks.com/about_us.htm">About Us</a>&nbsp; |&nbsp; <a 
class=footer href="http://www.surfablebooks.com/contact_us.htm">Contact Us</a>&nbsp; 
  |&nbsp; <a class=footer href="http://www.surfablebooks.com/jobs.htm">Jobs</a>&nbsp; 
  |&nbsp; <a class=footer href="http://www.surfablebooks.com/privacy_policy.htm">Privacy 
  Policy</a>&nbsp; |&nbsp; <a class=footer 
href="http://www.surfablebooks.com/terms%20of%20service">Terms of Service</a>&nbsp; 
  |&nbsp; <a class=footer href="http://www.surfablebooks.com/help">Help</a></font></p>
</td></tr></table>
</body>
</html>
