import java.util.*;
import java.io.*;
import java.net.*;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

// InvokeAPI is a standardized JAVA wrapper used to invoke the Indraweb ITS API
//
// Example usage:
//
//  InvokeAPI API = new InvokeAPI ("security.TSLogin", htArgs);
//	HashTree htResults = API.Execute();

public class InvokeAPI {
	private String sTaxonomyServer;
	private String sAPIcall;	
	private Hashtable htArguments;
	private String SessionKey = null;
	
	// Accessor methods to private variables
	public String GetTaxonomyServer() { return sTaxonomyServer; }
	public String GetAPIcall() { return sAPIcall; }
	public String GetSessionKey() { return SessionKey; }
	public Hashtable GetAPIarguments() { return htArguments; }
	
	// Object constructors
	public InvokeAPI (String sAPIcall, Hashtable htArguments, String sTaxonomyServer) {
		this.sAPIcall = sAPIcall;
		this.sTaxonomyServer = sTaxonomyServer;
		this.htArguments = htArguments;
		
		if (htArguments.containsKey("SKEY")) { this.SessionKey = (String) htArguments.get("SKEY"); }
	}
	public InvokeAPI (String sAPIcall, Hashtable htArguments) {
		
		this.sAPIcall = sAPIcall;
		this.htArguments = htArguments;
		this.sTaxonomyServer = (String) htArguments.get("api");

		if (htArguments.containsKey("SKEY")) { this.SessionKey = (String) htArguments.get("SKEY"); }
	}

    // This is an internal function -- if a tag name already exists, append to it
	private String CreateName (Hashtable htHash, String sName) {
		// If the tag already exists, append to it and make it unique
		if (htHash.containsKey(sName)) {
			sName = sName + "a";
			sName = CreateName(htHash, sName);
		} 
		
		return sName;
	}
	
	// Execute the API call.   Returns success 0 for success, -1 for failure.
	public HashTree Execute () { return Execute(false, false); }
	public HashTree Execute (boolean bDebug, boolean bUseProxy) {
		String sURL = sTaxonomyServer+sAPIcall;
		Enumeration e = htArguments.keys();

		HashTree htOriginal = new HashTree();
		htOriginal.SetParent(null);
		HashTree htObject = htOriginal;
			
		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);
			
			if (!sKey.equals("api")) {
				sURL = sURL+"&"+sKey+"="+sValue; 
			
				// Remove any apostraphies (because they are illegal) and change spaces into + signs
                sURL = sURL.replace(' ', '+'); sURL = removeChar(sURL, '\'');
			}
		}
		
		// Make a socket connection to the server
        HttpURLConnection httpCon = null;
		try {
			long lStart = System.currentTimeMillis();

            URL myURL = null;

            if (!bUseProxy) { myURL = new URL(sURL); }
            else {
                myURL = new URL("http",         // protocol,
                                  "172.16.101.110",  // host name or IP of proxy server to use
                                  8080,             // proxy port or �1 to indicate the default port for the protocol
                                  sURL);  // the original URL, specified in the �file� parameter
            }

			if (bDebug) { System.out.println("API call: "+sURL); }
			httpCon = (HttpURLConnection) myURL.openConnection();

			if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Http error: " + httpCon.getResponseMessage());
				throw new Exception("Http error: " + httpCon.getResponseMessage());
			}

			InputStream is = httpCon.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();

			while ((sData = buf.readLine()) != null) {
				if (bDebug) { System.out.println(sData); }
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");

				String sTag1;

				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }

				String sTag2 = "";

				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) &&
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
 					 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						sTag1 = new String (CreateName(htObject, sTag1));
						//out.println("<BR>Begin object: "+sTag1);

						if (sTag1.equals("TS_ERROR")) {
							httpCon.disconnect();
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							httpCon.disconnect();
							return htOriginal;
						}

						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1,htObject);

					}

					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2);
						htObject = (HashTree) htObject.GetParent();
					}

					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
  						String sTag = new String (getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						//out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

						// If the variable value contains a "CDATA", eliminate it here
                        if (sTag.indexOf("<![CDATA") != -1) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}

						htObject.put(sTag1, sTag);
					}
				}
			}

			httpCon.disconnect(); httpCon = null;

			return htObject;
		}
		catch (Exception except) { except.printStackTrace(System.out); return null; }
        finally { if (httpCon != null) { httpCon.disconnect(); }}
	}

    // ******** Bring results back as vector instead of hash tree
    public Vector vExecute() { return vExecute (false, false); }
    public Vector vExecute(boolean bDebug, boolean bUseProxy) {
        String sURL = sTaxonomyServer + sAPIcall;
        Enumeration e = htArguments.keys();

        VectorTree vOriginal = new VectorTree();
        vOriginal.SetParent(null);
        VectorTree vObject = vOriginal;

		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);

			if (!sKey.equals("api")) {
				sURL = sURL+"&"+sKey+"="+sValue;

				// Remove any apostraphies (because they are illegal) and change spaces into + signs
                sURL = sURL.replace(' ', '+'); sURL = removeChar(sURL, '\'');
			}
		}

        // Make a socket connection to the server
        HttpURLConnection httpCon = null;
		try {
			long lStart = System.currentTimeMillis();

            URL myURL = null;

            if (!bUseProxy) { myURL = new URL(sURL); }
            else {
                myURL = new URL("http",         // protocol,
                                  "172.16.101.110",  // host name or IP of proxy server to use
                                  8080,             // proxy port or �1 to indicate the default port for the protocol
                                  sURL);  // the original URL, specified in the �file� parameter
            }

			if (bDebug) { System.out.println("API call: "+sURL); }
			httpCon = (HttpURLConnection) myURL.openConnection();

			if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Http error: " + httpCon.getResponseMessage());
				throw new Exception("Http error: " + httpCon.getResponseMessage());
			}

			InputStream is = httpCon.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();

            while ((sData = buf.readLine()) != null) {
				if (bDebug) { System.out.println(sData); }
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = new String("");
                }

                Integer iVariance = new Integer(0);

                // HACK! HACK! Ignore lines that start with..
                if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) &&
                        (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
                        (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
                        (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

                    // If s1Tag is populated but s2Tag is not, start the object
                    if ((!sTag1.equals("")) && (sTag2.equals(""))) {

                        if (sTag1.equals("TS_ERROR")) {
                            httpCon.disconnect();
                            return vOriginal;
                        }
                        if (sTag1.equals("INTERNALSTACKTRACE")) {
                            httpCon.disconnect();
                            return vOriginal;
                        }

                        VectorTree vOldObject = vObject;
                        vObject = new VectorTree();
                        vObject.SetParent(vOldObject);
                        vOldObject.addElement(vObject);

                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if ((!sTag2.equals("")) && (sTag1.equals(""))) {
                        vObject = (VectorTree) vObject.GetParent();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                        String sTag = new String (getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));

                        // If the variable value contains a "CDATA", eliminate it here
                        if (sTag.indexOf("<![CDATA") != -1) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}

                        vObject.addElement(sTag);
                    }
                }
            }

            httpCon.disconnect();
            long lStop = System.currentTimeMillis() - lStart;
            //System.out.println("<!-- Timing: " + lStop + " ms -->");

            return vObject;
        } catch (Exception except) {
            return null;
        }
    }


    // get locations of strings
 	public static int getLocationOfNthOfThese (String s, String s1, int index1 ) {
		int currentPointer = -1; int incrementAmount = 0;

		for (int i = 0; i < index1; i++ ) {
			String curString = s.substring( currentPointer + 1 );
			incrementAmount = curString.indexOf ( s1 );

			if ( incrementAmount == -1 ) { return -1; }
			else { currentPointer += incrementAmount + 1; }
		}
		return currentPointer;
	}

    public static String removeChar(String s, char c) {
       String r = "";
       for (int i = 0; i < s.length(); i ++) { if (s.charAt(i) != c) r += s.charAt(i); }
       return r;
    }

    // string parsing routine
 	public static String getStringBetweenThisAndThat (String s, String s1, String s2) {
        int index1 = 1; int index2 = 1;
		String returnStr = null;

		int startLoc = 0;
		if ( index1 > 0 ) {
			startLoc = getLocationOfNthOfThese (s, s1, index1 ) + 1 ;
			if (startLoc == -1) { return ""; }
		}

		if ( index2 > 0 ) {
			int endLoc = 0;
			int totalLen = startLoc+s1.length();
			if (totalLen == -1) { return ""; }

			endLoc = getLocationOfNthOfThese (s, s2, index2 ) ;
			if (endLoc == -1) { returnStr = s.substring ( startLoc + s1.length()-1 ); }
			else {
				try { returnStr = s.substring ( startLoc + s1.length()-1, endLoc ); }
				catch ( Exception e ) { returnStr = "";	}
			}
		} else { returnStr = s.substring ( startLoc+1, s1.length()); }

		return returnStr;
	}
}