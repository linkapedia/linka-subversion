/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import java.util.Vector;

public class Node {
    // User attributes
    private String NodeID;
    private String CorpusID;
    private String NodeTitle;
    private String ParentID;
    private String NIWP;
    private String Depth;
    private String DateScanned;
    private String DateUpdated;
    private String NodeStatus;

    // constructor(s)
    public Node () {}
    public Node (HashTree ht) {
        NodeID = (String) ht.get("NODEID");
        CorpusID = (String) ht.get("CORPUSID");
        NodeTitle = (String) ht.get("NODETITLE");
        ParentID = (String) ht.get("PARENTID");
        NIWP = (String) ht.get("INDEXWITHINPARENT");
        Depth = (String) ht.get("DEPTHFROMROOT");
        DateScanned = (String) ht.get("DATESCANNED");
        DateUpdated = (String) ht.get("DATEUPDATED");
        NodeStatus = (String) ht.get("NODESTATUS");
    }
    public Node (Vector v) {
        NodeID = (String) v.elementAt(0);
        CorpusID = (String) v.elementAt(1);
        ParentID = (String) v.elementAt(3);
        NodeTitle = (String) v.elementAt(2);
        NIWP = (String) v.elementAt(4);
        Depth = (String) v.elementAt(5);

        if (v.size() > 6) { DateScanned = (String) v.elementAt(6); }
        if (v.size() > 7) { DateUpdated = (String) v.elementAt(7); }
        if (v.size() > 8) { NodeStatus = (String) v.elementAt(8); }
    }

    // accessor functions
    public String getID() { return NodeID; }
    public String getCorpusID() { return CorpusID; }
    public String getTitle() { return NodeTitle; }
    public String getParent() { return ParentID; }
    public String getIndex() { return NIWP; }
    public String getDepth() { return Depth; }
    public String getDateScanned() { return DateScanned; }
    public String getDateUpdated() { return DateUpdated; }
    public String getNodeStatus() { return NodeStatus; }

    public void setID(String Node) { NodeID = Node; }
    public void setCorpusID(String Corpus) { CorpusID = Corpus; }
    public void setTitle(String Title) { NodeTitle = Title; }
    public void setParent(String Parent) { ParentID = Parent; }
    public void setIndex(String Index) { NIWP = Index; }
    public void setDepth(String DepthLevel) { Depth = DepthLevel; }
    public void setDateScanned(String Date) { DateScanned = Date; }
    public void setDateUpdated(String Date) { DateUpdated = Date; }
    public void setNodeStatus(String Status) { NodeStatus = Status; }
}
