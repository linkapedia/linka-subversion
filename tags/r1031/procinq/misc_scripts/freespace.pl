#!/usr/bin/perl

use Net::SMTP;
use DBI;
$ENV{'ORACLE_HOME'} = '/opt/oracle';
local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9");

local ($sth) = $dbh->prepare("select substr(to_char(sum(bytes)/1024/1024, '999,999,999.99'), 1, 15)  \"TOT MBYTES\", substr(to_char(sum(bytes)/1024/1024/1024, '999,999.99'), 1, 11) \"TOT GBYTES\" from sys.dba_data_files");
$sth->execute || die "database error $!\n";

($mb, $gb) = $sth->fetchrow_array();
$gb =~ s/\ //gi;

if (int($gb) < 25) {
    print "Only $gb gigabytes remaining, sending out a warning e-mail.\n";

    $smtp = Net::SMTP->new('smtp.netreach.net');
    
    $smtp->mail('info\@indraweb.com');
    $smtp->to('hkon\@indraweb.com');
    
    $smtp->data();
    $smtp->datasend("To: hkon\@indraweb.com\n");
    $smtp->datasend("Subject: [athena] DB warning!\n");
    $smtp->datasend("\n");
    $smtp->datasend("This is an automated message from the Indraweb monitor.\n");
    $smtp->datasend("\n");
    $smtp->datasend("There is only $gb gigabytes of allocated disk space left on athena. \n");
    $smtp->datasend("This is likely to cause problems in the near future.\n");
    $smtp->datasend("\n");    
    $smtp->datasend("===========================================\n");
    $smtp->datasend("NetReach : 215-283-2300 extension 400 \n");
    $smtp->datasend("MP home 610-265-8038, cell 610-322-3132, mpuscar\@hotmail.com\n");
    $smtp->datasend("HK home 781-861-8202, cell 781-405-4265\n");
    $smtp->datasend("\n");
    $smtp->dataend();
} else { print "We're okay.  Still $gb gigabytes remaining.\n"; }

if (int($gb) < 25) {
    $smtp = Net::SMTP->new('smtp.netreach.net');
    
    $smtp->mail('info\@indraweb.com');
    $smtp->to('mpuscar\@indraweb.com');
    
    $smtp->data();
    $smtp->datasend("To: mpuscar\@indraweb.com\n");
    $smtp->datasend("Subject: [athena] DB warning!\n");
    $smtp->datasend("\n");
    $smtp->datasend("This is an automated message from the Indraweb monitor.\n");
    $smtp->datasend("\n");
    $smtp->datasend("There is only $gb gigabytes of allocated disk space left on athena. \n");
    $smtp->datasend("This is likely to cause problems in the near future.\n");
    $smtp->datasend("\n");    
    $smtp->datasend("===========================================\n");
    $smtp->datasend("NetReach : 215-283-2300 extension 400 \n");
    $smtp->datasend("MP home 610-265-8038, cell 610-322-3132, mpuscar\@hotmail.com\n");
    $smtp->datasend("HK home 781-861-8202, cell 781-405-4265\n");
    $smtp->datasend("\n");
    $smtp->dataend();
}

$sth->finish;
$dbh->disconnect;

1;

