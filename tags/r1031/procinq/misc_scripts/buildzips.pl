#!/usr/bin/perl

require LWP::Parallel::UserAgent;
use HTTP::Request;
use DBI;
use LWP::UserAgent;

# First we must LOG IN
local $ua = LWP::UserAgent->new;
local $request = HTTP::Request->new("GET", "http://66.134.131.36/servlet/ts?fn=security.TSLogin&UserID=sn=mpuscar,ou=users,dc=indraweb,dc=com&Password=racer9");
local $response = $ua->request($request) || die "Cannot get response.";

$response->content() =~ /<KEY>(.*)<\/KEY>/gi;
local ($skey) = $1;

print "Session Key: $skey\n\n";
if (!defined($skey)) { die "Could not log into the system.\n"; }

# Then for each node/genre that has changed, create the zip file

local ($dbh) = DBI->connect("dbi:Oracle:gaea", "sbooks", "racer9");
local ($q) = "select corpusid from corpus order by corpusid asc"; print "$q\n";
local ($c) = $dbh->prepare($q) || die "Could not execute select from corpus table.\n";
$c->execute || die "Could not execute select from corpus table.\n";

while (local ($cid) = $c->fetchrow_array()) {
    print "Generating zip files for corpus $cid ..\n";
    local ($q) = "select genreid from genre"; print "$q\n";
    local ($sth) = $dbh->prepare($q) || die "Could not execute select from genre table.\n";
    $sth->execute || die "Could not execute select from genre table.\n";

    local (@GenreArr);
    while (local $genreid = $sth->fetchrow_array()) { if ($genreid != 1) { push (@GenreArr, $genreid); }}
    $sth->finish(); undef $sth;

    $q = "select nodeid, nodetitle from node where corpusid = $cid and nodeid in (select distinct(nodeid) from nodedocument)"; print "$q\n";
    local ($sth) = $dbh->prepare($q) || die "Could not execute select from node table.\n";
    $sth->execute || die "Could not execute select from node table.\n";
    
    while (($id, $title) = $sth->fetchrow_array()) {
	my ($url) = "http://66.134.131.36/servlet/ts?fn=tsapisvr.TSGetNodeDocuments&whichdb=SVR&NodeID=".$id."&SKEY=".$skey;
	print "Creating zip file for Node: $title ($id) ($url)\n";
	undef $ua; local ($ua) = LWP::UserAgent->new;
	undef $request; local $request = HTTP::Request->new("GET", $url);
	undef $response; local $response = $ua->request($request) || die "Cannot get response.";
	if ($response->content() =~ /TS_ERROR/i) { warn "Error: ".$response->content()."\n\n"; }
    }

    $sth->finish();
    undef $sth;

    # For each genre generate a cache file for each node
    foreach $genreid (@GenreArr) {
	$q = "select nodeid, nodetitle from node where corpusid = $cid and nodeid in (select nodeid from nodedocument where documentid in (select documentid from document where genreid = $genreid))";
        print "$q\n";
	local ($sth) = $dbh->prepare($q) || die "Could not execute select from node table.\n";
	$sth->execute || die "Could not execute select from node table.\n";
	
	while (($id, $title) = $sth->fetchrow_array()) {
	    my ($url) = "http://66.134.131.36/servlet/ts?fn=tsapisvr.TSGetNodeDocuments&whichdb=SVR&FolderID=$genreid&NodeID=".$id."&SKEY=".$skey;
	    print "Creating zip file for Node: $title ($id) ($url)\n";
	    undef $ua; local ($ua) = LWP::UserAgent->new;
	    undef $request; local $request = HTTP::Request->new("GET", $url);
	    undef $response; local $response = $ua->request($request) || die "Cannot get response.";
	    if ($response->content() =~ /TS_ERROR/i) { warn "Error: ".$response->content()."\n\n"; }
	}
	
	$sth->finish();
	undef $sth;
    }
}

$dbh->disconnect();

1;
