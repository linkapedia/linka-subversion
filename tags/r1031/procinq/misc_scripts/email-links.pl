#!C:\perl\bin\perl
#
# Send e-mail to users tracking nodes with new
# documents found during the past X hours

# Load necessary libraries and functions
use DBI;
use Net::SMTP;
use Getopt::Long;
use LWP::UserAgent;

require "NodeDocumentObject.pm";
require "UserObject.pm";

# Define default values
local ($days) = 1;
local ($dbname) = "client";
local ($uname) = "sbooks";
local ($password) = "racer9";
local ($api) = "http://66.134.131.36:8105/servlet/ts?fn=";
local ($server) = "http://66.134.131.36:8105/cache/";
local ($emailserver) = "smtp.covadmail.net";
local ($days) = 1;

# Load configuration parameters
&GetOptions ("days=i" => \$days,
	     "username=s" => \$username,
	     "password=s" => \$password,
	     "api=s" => \$api,
	     "emailserver=s" => \$emailserver,
	     "server=s" => \$server);

# Global variables, database connections, etc.
local (%iNodeIdHash); local (@iNodeIdArr); local ($oUser) = ""; local ($output) = "";
local ($title, $url, $score);

# Step 1: Establish a session key with the client ITS API
local $ua = LWP::UserAgent->new;
local $loginurl = $api."security.TSLogin&UserID=$username&Password=$password"; 
local $request = HTTP::Request->new("GET", $loginurl);
local $response = $ua->request($request) || die "Cannot get response for login request.";

$response->content() =~ /<KEY>(.*)<\/KEY>/gi;
local ($skey) = $1;

if (!defined($skey)) { die "Could not log into the system: $loginurl.\n"; }
print "Session established.  Session key is $skey\n";

# Step 2: Determine date of last run for this interval.  If never been run, only
#         retrieve documents within the time period specified.
$ua = LWP::UserAgent->new;
local $url = $api."tsother.TSGetCurrentDate&SKEY=".$skey;
$request = HTTP::Request->new("GET", $url);
$response = $ua->request($request) || die "Cannot get response from the API.";

$response->content() =~ /<DATE>(.*)<\/DATE>/gi;

local ($current) = $1;
local ($date) = "";
local ($exists) = 1;

open (DATE, "$days.txt");
@lines = <DATE>; close(DATE); 
$date = $lines[0]; 
chomp($date);

if ($date eq "") {
	$ua = LWP::UserAgent->new;
	$url = $api."tsother.TSGetCurrentDate&Offset=".$days."&SKEY=".$skey;
	$request = HTTP::Request->new("GET", $url);
	$response = $ua->request($request) || die "Cannot get response from the API.";
	
	$response->content() =~ /<DATE>(.*)<\/DATE>/gi;

	$date = $1;
}

print "Notification last run: $date\n";

# Step 3: Load all templates into memory
print "Loading templates into memory..\n";

open (STEMPLATE, "templates/semail.tpl");
local (@stemplate) = <STEMPLATE>; close(STEMPLATE);

open (RSTEMPLATE, "templates/sresult.tpl");
local (@rstemplate) = <RSTEMPLATE>; close(RSTEMPLATE);

open (RTEMPLATE, "templates/result.tpl");
local (@rtemplate) = <RTEMPLATE>; close(RTEMPLATE);

open (CTEMPLATE, "templates/cresult.tpl");
local (@ctemplate) = <CTEMPLATE>; close(CTEMPLATE);

open (RETEMPLATE, "templates/eresult.tpl");
local (@retemplate) = <RETEMPLATE>; close(RETEMPLATE);

open (ETEMPLATE, "templates/eemail.tpl");
local (@etemplate) = <ETEMPLATE>; close(ETEMPLATE);

# Step 4: Load all nodes from the database that have changed (new
#  documents were added) since "$date" and are tracked by a 
#  user in our system.   NOTE: though this uses the API, the API function
#  is required to query both the client and server databases.   
#
#  This API function will determine the complete list of nodes that have changed
#   on either the server or client databases, then find all nodes being tracked
#   by the user set.   Only nodes that have changed and are being tracked will be used.
print "Retrieving notification list.  This will take several minutes...\n";

$ua = LWP::UserAgent->new;
$url = $date;
$url = $api."tsnode.TSGetNodeNotificationList&DATE=".$url;
$url = $url."&SKEY=".$skey;
$request = HTTP::Request->new("GET", $url);
$response = $ua->request($request) || die "Cannot get response from the API: ".$url;

# Store response in local array
local (@lines) = split(/\n/, $response->content());
foreach $line (@lines) {
   if ($line =~ /<NODEID>(.*)<\/NODEID>/i) { push (@iNodeIdArr, $1); }
} undef @lines;

if (scalar(@iNodeIdArr) == 0) { die "No topics found to process.\n"; }

print "Found ".scalar(@iNodeIdArr)." topics to process.\n";

# Step 5: Build a cache for each node with new results
foreach $nodeid (@iNodeIdArr) {
    local (@nodedocarr);
    $ua = LWP::UserAgent->new;
    $url = $api."tsdocument.TSGetNodeDocument&ScoreThreshold=-0.5&NodeID=".$nodeid."&DATE=".$date."&SKEY=".$skey;
    $request = HTTP::Request->new("GET", $url);
    $response = $ua->request($request) || die "Cannot get response from the API: ".$url;
    
    print "Creating cache file: $nodeid.cache\n";

    local (@lines) = split(/\n/, $response->content());

    # create nodedocument objects in memory
    local ($ndo);
    foreach $line (@lines) {
	if ($line =~ /<NODEDOCUMENT>/i) { $ndo = NodeDocumentObject->new; }

	if ($line =~ /<NODEID>(.*)<\/NODEID>/i) { $ndo->{Nid} = $1; }
	if ($line =~ /<DOCUMENTID>(.*)<\/DOCUMENTID>/i) { $ndo->{Did} = $1; }
	if ($line =~ /<FOLDERID>(.*)<\/FOLDERID>/i) { $ndo->{Fid} = $1; }
	if ($line =~ /<DOCTITLE><\!\[CDATA\[(.*)\]\]><\/DOCTITLE>/i) { $ndo->{Dtitle} = $1; }
	if ($line =~ /<DOCURL><\!\[CDATA\[(.*)\]\]><\/DOCURL>/i) { $ndo->{DURL} = $1; }
	if ($line =~ /<DOCSUMMARY><\!\[CDATA\[(.*)\]\]><\/DOCSUMMARY>/i) { $ndo->{Dsum} = $1; }
	if ($line =~ /<SCORE1>(.*)<\/SCORE1>/i) { $ndo->{Score1} = $1; }
	if ($line =~ /<SCORE2>(.*)<\/SCORE2>/i) { $ndo->{Score2} = $1; }
	if ($line =~ /<SCORE3>(.*)<\/SCORE3>/i) { $ndo->{Score3} = $1; }
	if ($line =~ /<SCORE4>(.*)<\/SCORE4>/i) { $ndo->{Score4} = $1; }
	if ($line =~ /<SCORE5>(.*)<\/SCORE5>/i) { $ndo->{Score5} = $1; }
	if ($line =~ /<SCORE6>(.*)<\/SCORE6>/i) { $ndo->{Score6} = $1; }
	if ($line =~ /<DATESCORED>(.*)<\/DATESCORED>/i) { $ndo->{Dscored} = $1; }

	if ($line =~ /<\/NODEDOCUMENT>/i) { push(@nodedocarr, $ndo); }
    }

    open (CACHE, ">cache/$nodeid.cache");
    local ($loop) = 0; 

    foreach $ndo (@nodedocarr) {
	$title = $ndo->Dtitle;
	$url = $ndo->DURL;
	$score = $ndo->Score1;
	$docsum = $ndo->Dsum;

	$loop++; if ($loop < 75) {
	undef $stars; local ($stars) = "";
	if ($score > 49) { $stars = "<img src=\"http://www.surfablebooks.com/images/star.gif\">"; }
	if ($score > 74) { $stars = $stars . "<img src=\"http://www.surfablebooks.com/images/star.gif\">"; }
	if ($score > 99) { $stars = $stars . "<img src=\"http://www.surfablebooks.com/images/star.gif\">"; }
	foreach $line (@ctemplate) { 
	    my $linex = $line;
	    $linex =~ s/##(.*)##/${$1}/gi; 
	    print CACHE $linex; 
	}}
        undef $ndo;
     } $loop = 0;

    close(CACHE); undef @lines;
}

undef @nodedocarr;

# Step 4a: Extract user information for users tracking a node with
#  new documents loaded during the past $hours hours
print "Extracting user information..\n";

local ($emailstatus) = 1;
if ($days > 1) { $emailstatus = 2; }
if ($days > 7) { $emailstatus = 3; }

$ua = LWP::UserAgent->new;
$url = $api."tsuser.TSGetUsersNodesTracked&EmailStatus=".$emailstatus."&SKEY=".$skey;
$request = HTTP::Request->new("GET", $url);
$response = $ua->request($request) || die "Cannot get response from the API: ".$url;
    
local (@lines) = split(/\n/, $response->content());
local (@users);

# create nodedocument objects in memory
local ($user);
foreach $line (@lines) {
    if ($line =~ /<USERNODE>/i) { $user = UserObject->new; }

    if ($line =~ /<ID>(.*)<\/ID>/i) { $user->{Uid} = $1; }
    if ($line =~ /<NODETITLE>(.*)<\/NODETITLE>/i) { $user->{Ntitle} = $1; }
    if ($line =~ /<NODEID>(.*)<\/NODEID>/i) { $user->{Nid} = $1; }
    if ($line =~ /<EMAIL>(.*)<\/EMAIL>/i) { $user->{Email} = $1; }
    if ($line =~ /<NAME>(.*)<\/NAME>/i) { $user->{Name} = $1; }

    if ($line =~ /<\/USERNODE>/i) { push(@users, $user); }
}

# Step 4b: Loop through each user (along with their nodes) and 
#  send out an e-mail to each user

local ($oUser) = "";
local ($uid, $nid, $ntitle, $email, $firstname, $eemail);
foreach $user (@users) {
    $uid = $user->Uid;
    $nid = $user->Nid;
    $ntitle = $user->Ntitle;
    $email = $user->Email;
    $firstname = $user->Name;

    if (-e "cache/$nid.cache") {

    # If oUser is "", we are processing the very first user/node
    if ($oUser eq "") {
	# print start e-mail template
	foreach $line (@stemplate) { 
	    my $linex = $line;
	    $linex =~ s/##(.*)##/${$1}/gi; 
	    $output = $output . $linex; 
	} $oUser = $uid; $output=$output."\n";
    }
    
    # if UID and OUSER are not equal, send out e-mail for previous user
    elsif ($uid ne $oUser) {
	# print end e-mail template
	foreach $line (@etemplate) { 
	    my $linex = $line;
	    $linex =~ s/##(.*)##/${$1}/gi; 
	    $output = $output . $linex; 
	} 
	if ($eemail ne "null") {
	    print "Sending e-mail to $eemail \n";
	
	    $smtp = Net::SMTP->new($emailserver);	
	    $smtp->mail('update\@indraweb.com');
	    $smtp->to($eemail);
    
	    $smtp->data();
	    $smtp->datasend("To: $eemail\n");
            $smtp->datasend("Content-Type: text/html\n");
            $smtp->datasend("Content-Transfer-Encoding: 7bit\n");
	    $smtp->datasend("Subject: ITS Notification\n");
	    $smtp->datasend("\n");
	    $smtp->datasend($output);
	    $smtp->datasend("\n\n");
	    $smtp->dataend();
         }

        $output = ""; $oUser = $uid;
  
	# print start e-mail template for next user
	foreach $line (@stemplate) { 
	    my $linex = $line;
	    $linex =~ s/##(.*)##/${$1}/gi; 
	    $output = $output . $linex; 
        }
    }

    # Print start results template
    foreach $line (@rstemplate) { 
	my $linex = $line;
	$linex =~ s/##(.*)##/${$1}/gi; 
	$output = $output . $linex; 
    }

    # Get result information for this node from the cache
    open (FILE, "cache/$nid.cache"); 
    while ($line = <FILE>) { $output = $output . $line; }
    close(FILE);

    $eemail = $email;
    }
}

# Send out the final e-mail
if ($oUser ne "") {
    # print end e-mail template
    foreach $line (@etemplate) { 
	my $linex = $line;
	$linex =~ s/##(.*)##/${$1}/gi; 
	$output = $output . $linex; 
    } 
    print "Sending e-mail to $eemail \n";
    
    if ($eemail ne "null") {
        $smtp = Net::SMTP->new($emailserver);	
        $smtp->mail('update\@indraweb.com');
        $smtp->to($eemail);
    
        $smtp->data();
        $smtp->datasend("To: $eemail\n");
        $smtp->datasend("Content-Type: text/html\n");
        $smtp->datasend("Content-Transfer-Encoding: 7bit\n");
        $smtp->datasend("Subject: ITS Notification\n");
        $smtp->datasend("\n");
        $smtp->datasend($output);
        $smtp->datasend("\n");
        $smtp->dataend();
    }
}

# free memory, etc.
open (DATE, ">$days.txt");
print DATE $current."\n";
close(DATE);

1;
 
