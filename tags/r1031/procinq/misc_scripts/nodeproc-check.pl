#!/usr/bin/perl

#SQL> describe machinelog;
# Name                                      Null?    Type
# ----------------------------------------- -------- ----------------------------
# MACHINENAME                               NOT NULL VARCHAR2(30)
# CONFIGID                                  NOT NULL NUMBER(4)
# LASTACCESS                                NOT NULL DATE

use Net::SMTP;
use DBI;
$ENV{'ORACLE_HOME'} = '/opt/oracle';
local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9");

local ($sth) = $dbh->prepare("select machinename, (sysdate-lastaccess) from machinelog");
$sth->execute || die "database error $!\n";

local ($bugstring) = "";
while (($mname, $datediff) = $sth->fetchrow_array()) { 
   if (($datediff*60*24) > 20.0) {
      print "Nothing found for $mname in ".int(60*24*$datediff)." minutes\n";
      $bugstring = $bugstring . "Nothing found for $mname in ".int(60*24*$datediff)." minutes\n";
   }
}

$sth->finish();
$dbh->disconnect();

if ($bugstring ne "") {
    $smtp = Net::SMTP->new('smtp.netreach.net');
    
    $smtp->mail('info\@indraweb.com');
    $smtp->to('hkon\@indraweb.com');
    
    $smtp->data();
    $smtp->datasend("To: hkon\@indraweb.com\n");
    $smtp->datasend("Subject: [General] Activity warning!\n");
    $smtp->datasend("\n");
    $smtp->datasend("This is an automated message from the Indraweb monitor.\n");
    $smtp->datasend("\n");
    $smtp->datasend("The following machines have been idle for an extended \n");
    $smtp->datasend("period of time on a single node.\n");
    $smtp->datasend("\n");    
    $smtp->datasend("$bugstring\n");    
    $smtp->datasend("\n");    
    $smtp->datasend("===========================================\n");
    $smtp->datasend("NetReach : 215-283-2300 extension 400 \n");
    $smtp->datasend("MP home 610-265-8038, cell 610-322-3132, mpuscar\@hotmail.com\n");
    $smtp->datasend("HK home 781-861-8202, cell 781-405-4265\n");
    $smtp->datasend("\n");
    $smtp->dataend();
}

if ($bugstring ne "") {
    $smtp = Net::SMTP->new('smtp.netreach.net');
    
    $smtp->mail('info\@indraweb.com');
    $smtp->to('mpuscar\@indraweb.com');

    $smtp->data();
    $smtp->datasend("To: hkon\@indraweb.com\n");
    $smtp->datasend("Subject: [General] Activity warning!\n");
    $smtp->datasend("\n");
    $smtp->datasend("This is an automated message from the Indraweb monitor.\n");
    $smtp->datasend("\n");
    $smtp->datasend("The following machines have been idle for an extended \n");
    $smtp->datasend("period of time on a single node.\n");
    $smtp->datasend("\n");    
    $smtp->datasend("$bugstring\n");    
    $smtp->datasend("\n");    
    $smtp->datasend("===========================================\n");
    $smtp->datasend("NetReach : 215-283-2300 extension 400 \n");
    $smtp->datasend("MP home 610-265-8038, cell 610-322-3132, mpuscar\@hotmail.com\n");
    $smtp->datasend("HK home 781-861-8202, cell 781-405-4265\n");
    $smtp->datasend("\n");
    $smtp->dataend();
}

$sth->finish;
$dbh->disconnect;

1;

