#!/usr/bin/perl
#
# Output data from the database in Excel format for ROC data analysis
use DBI;
use Getopt::Long;

# Get corpus identifier
local ($dbname) = "mitre";
local ($corpusid) = 0;

&GetOptions ("corpusid=i" => \$corpusid,
	     "dbname=s" => \$dbname);

if ($corpusid == 0) { &die_error("Sorry you must specify a corpus identifier.\n"); }

# Connect to the database
local ($dbh) = DBI->connect("dbi:Oracle:$dbname", "sbooks", "racer9");

# Get columns to print in excel
local ($q) = "SELECT NODEDOCUMENT.NODEID, NODE.NODETITLE, NODE.NODESIZE, NODEDOCUMENT.DOCUMENTID, NODEDOCUMENT.SCORE1, NODEDOCUMENT.SCORE2, NODEDOCUMENT.SCORE4, NODEDOCUMENT.DATESCORED, BLACKLIST.URLCHUNK, BLACKLIST.NODEID, BLACKLIST.DATECREATED, DOCUMENT.DOCTITLE, DOCUMENT.DOCURL FROM SBOOKS.NODE, SBOOKS.NODEDOCUMENT, SBOOKS.DOCUMENT, SBOOKS.BLACKLIST WHERE node.nodeid = nodedocument.nodeid and document.documentid = nodedocument.documentid and blacklist.urlchunk (+) = document.docurl  and node.nodeid in ( select node.nodeid from node where corpusid = $corpusid) ORDER BY NODEDOCUMENT.NODEID, NODEDOCUMENT.SCORE4";
local ($sth) = $dbh->prepare($q) || &die_error("Could not get fields from the database.");
$sth->execute || &die_error("Could not get fields from the database.");

print "Node ID\t";
print "Node Title\t";
print "Node Size\t";
print "Document ID\t";
print "Score 1\t";
print "Score 2\t";
print "Score 4\t";
print "Date Scored\t";
print "Blacklist URL\t";
print "Blacklist Node\t";
print "Blacklist Date\t";
print "Document Title\t";
print "Document URL\t\n";

while (($nid, $ntit, $size, $did, $s1, $s2, $s4, $date, $burl, $bnid, $bdate, $dtitle, $durl) = $sth->fetchrow_array()) {
    print "$nid\t";
    print "$ntit\t";
    print "$size\t";
    print "$did\t";
    print "$s1\t";
    print "$s2\t";
    print "$s4\t";
    print "$date\t";
    print "$burl\t";
    print "$bnid\t";
    print "$bdate\t";
    print "$dtitle\t";
    print "$durl\t\n";
}

print "\n";

$sth->finish();
$dbh->disconnect();
exit(1);

sub die_error {
    my ($message) = $_[0];

    print "Usage: gb.pl -corpusid <corpusid>\n";
    exit(0);
}
