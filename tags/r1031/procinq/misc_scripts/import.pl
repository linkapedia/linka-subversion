#!/usr/bin/perl

use DBI;
use Getopt::Long;

local ($table) = "";
local ($filename) = "";

&GetOptions ("table=s" => \$table, 
	     "file=s" => \$filename);

if ($table eq "") { die "Error: use the -table option to specify the table name.  Goodbye!\n"; }
if ($filename eq "") { die "Error: use the -file option to specify the import file name.  Goodbye!\n"; }

open (FILE, "$filename"); @lines = <FILE>; close(FILE);

local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9");
chomp($lines[0]);
local (@columns) = split(/\t/, $lines[0]);

foreach $line (@lines) {
    chomp($line);
    if ($line ne $lines[0]) {
	@fields = split(/\t/, $line);
	my $query = "INSERT INTO $table (";
	foreach $column (@columns) { $query = $query . "$column,"; } chop($query);
	$query = $query . ") VALUES (";
	foreach $field (@fields) { $field =~ s/\'//gi; $query = $query . "'$field',"; } chop($query);
	$query = $query . ")";

	print $query."\n";

	undef $sth; local $sth = $dbh->prepare($query) || die "Error in query: $!\n";
	$sth->execute() || die "Error in query: $!\n";
	$sth->finish();
    }
}

$dbh->disconnect();
