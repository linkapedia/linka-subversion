#!/usr/bin/perl
#
# n2h2check.pl
# 
# Sweep the DOCUMENT table each hour for documents with a FilterStatus
# of 2 (not checked by N2H2).   Check these documents with an HTTP
# HEAD request through the N2H2 proxy and flag the document in the
# database as appropriate.

$| = 1; # Flush STDOUT

use Getopt::Long;
use DBI;
use LWP::Parallel::UserAgent;
use URI;

# Get a persistent connection to the database.
local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9");

local $sth = $dbh->prepare("select count(*) from Document where FilterStatus = 2");
$sth->execute;
my $count = $sth->fetchrow_array();
$sth->finish; undef $sth;

if ($count == 0) { print "No unfiltered documents to processes.   Goodbye!\n"; exit(1); }

local $sth = $dbh->prepare("select distinct(docurl) from Document where FilterStatus = 2");
$sth->execute;

## Don't register more than 20k requests at a time, we'll exhause memory!
local ($loop) = 0;

while ($loop < 500) {
    undef $ua;

    $ua = LWP::Parallel::UserAgent->new();
    $ua->proxy(http  => 'http://indrawebprod14.netreach.net:3343/');
    $ua->redirect (1); # prevents automatic following of redirects
    $ua->max_hosts(20); # sets maximum number of locations accessed in paral
    $ua->max_req  (5); # sets maximum number of parallel requests per host
    $ua->timeout (10);
    local $type = "HEAD";    

    print "Documents selected.   Registering requests...\n";
    while ($loop < 500) { 
	$docurl = $sth->fetchrow_array() || &run_complete();
	$loop++;
	my $request = HTTP::Request->new($type);
	my $url = URI->new($docurl);
	$request->url($url);
	$request->header('url' => $docurl); 
	$ua->register ($request);
    }
    if ($loop == 500) { $loop = 0; }
    print "Running parallel user agent ...\n";
    my $entries = $ua->wait(10);
}

$sth->finish; 
$dbh->disconnect;
print "Done.\n";

1;

sub run_complete {
   print "Restarting run: ".$dbh->errstr."\n";
   $sth->finish; 
   $sth = $dbh->prepare("select docurl from Document where FilterStatus = 2");
   $sth->execute || die "Final error: ".$dbh->errstr."\n";

   $docurl = $sth->fetchrow_array();
}

sub LWP::Parallel::UserAgent::on_return {
    my $self = shift;
    my ($request, $response, $entry) = @_;
    
    my ($url) = $response->request->header('url'); 
    $url =~ s/\'//gi;

    # 200 OK
    # Cache-Control: max-age=28800
    # Date: Mon, 18 Jun 2001 19:54:15 GMT
    # Pragma: BESSBLOCK
    # Age: 5462
    # Server: Apache/1.3.12 (Unix)
    # Content-Type: text/html
    # Expires: 0
    # Client-Date: Mon, 18 Jun 2001 21:23:30 GMT
    # Client-Peer: 207.29.192.124:3343
    # Proxy-Connection: close
    # Title: Bess Can't Go There
    # X-Cache: HIT from apollo
   
    my $sRes = $response->as_string;
    if ($sRes =~ /BESSBLOCK/i) {
	print "BLOCKED: $url\n"; 

	undef $tsth; my $tsth = $dbh->prepare("UPDATE Document SET FilterStatus = 1 where DocUrl = '$url'") || die "Query failed: $!\n";
	$tsth->execute() || die "Query failed: $!\n";
	$tsth->finish;	
    }
    else { 
        print $response->code.": $url\n"; 
	undef $tsth; my $tsth = $dbh->prepare("UPDATE Document SET FilterStatus = 0 where DocUrl = '$url'") || die "Query failed: $!\n";
	$tsth->execute() || die "Query failed: $!\n";
	$tsth->finish;	
    }
}
$dbh->disconnect();

sub stop {
    $sth->finish;
    $dbh->disconnect();
    exit(1);
}

1;

