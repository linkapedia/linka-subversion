#!/usr/bin/perl

use Net::SMTP;
use DBI;
$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9") || &dberror();
$dbh->disconnect;

sub dberror {
    print "The database is down!\n";
    $smtp = Net::SMTP->new('smtp.netreach.net');
    
    $smtp->mail('info\@indraweb.com');
    $smtp->to('hkon\@indraweb.com');
    
    $smtp->data();
    $smtp->datasend("To: hkon\@indraweb.com\n");
    $smtp->datasend("Subject: [athena] DB down!\n");
    $smtp->datasend("\n");
    $smtp->datasend("This is an automated message from the Indraweb monitor.\n");
    $smtp->datasend("\n");
    $smtp->datasend("The monitoring tool is unable to forge a connection \n");
    $smtp->datasend("with the production database.   This usually means \n");
    $smtp->datasend("that the database is experiencing an outage. \n");
    $smtp->datasend("\n");    
    $smtp->datasend("===========================================\n");
    $smtp->datasend("NetReach : 215-283-2300 extension 400 \n");
    $smtp->datasend("MP home 610-265-8038, cell 610-322-3132, mpuscar\@hotmail.com\n");
    $smtp->datasend("HK home 781-861-8202, cell 781-405-4265\n");
    $smtp->datasend("\n");
    $smtp->dataend();

    undef $smtp;
    $smtp = Net::SMTP->new('smtp.netreach.net');
    
    $smtp->mail('info\@indraweb.com');
    $smtp->to('mpuscar\@indraweb.com');
    
    $smtp->data();
    $smtp->datasend("To: mpuscar\@indraweb.com\n");
    $smtp->datasend("Subject: [athena] DB down!\n");
    $smtp->datasend("\n");
    $smtp->datasend("This is an automated message from the Indraweb monitor.\n");
    $smtp->datasend("\n");
    $smtp->datasend("The monitoring tool is unable to forge a connection \n");
    $smtp->datasend("with the production database.   This usually means \n");
    $smtp->datasend("that the database is experiencing an outage. \n");
    $smtp->datasend("===========================================\n");
    $smtp->datasend("NetReach : 215-283-2300 extension 400 \n");
    $smtp->datasend("MP home 610-265-8038, cell 610-322-3132, mpuscar\@hotmail.com\n");
    $smtp->datasend("HK home 781-861-8202, cell 781-405-4265\n");
    $smtp->datasend("\n");
    $smtp->datasend("-monitoring tool\n");
    $smtp->dataend();
}


1;

