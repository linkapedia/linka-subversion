create sequence filter_seq start with 1;

CREATE OR REPLACE PROCEDURE FILTER (
   DOCUMENTID_IN IN Number,
   QUERYID_OUT OUT Number)
AS
BEGIN
DECLARE
  query number(12);
  v_rid rowid;
  begin
    select rowid into v_rid from document where documentid = DOCUMENTID_IN;
    select filter_seq.nextval into query from dual;
    Ctx_Doc.Set_Key_Type ( Ctx_Doc.Type_Rowid );
    Ctx_Doc.Filter
      (
      index_name   => 'DocFullText',
      textkey      => v_rid,
      restab       => 'filtertab',
      query_id     => query,
      plaintext    => true
      );

    QUERYID_OUT := query;
  end;
  commit work;
END FILTER;
/
commit;
