connect sbooks/slaanesh@idrac
--
-- Tables to be replicated
--
-- May need to grant create table to the master schema.
grant create table to sbooks;
create or replace view mastertables as select * from user_tables where table_name not in ('NOTIFICATION', 'CONFIGPARAMS') and table_name not like '%$%'
--
-- Assumes a minimal database has been created
--
--
-- Procedures to create snapshots
--
@IndrawebProcedures.txt
begin
  CreateSnapshotLogs;
end;
/
--
-- Create the link to the master database on the slave.
--
connect sbooks/idrac@intra01
-- May need to grant create table to the master schema.
grant create table to sbooks;
grant create snapshot to sbooks;
create database link idrac connect to sbooks identified by slaanesh using 'idrac';
--
-- Create view to say what tables are to be replicated.
create or replace view mastertables as select * from mastertables@idrac
--
-- Procedures to create snapshots
--
@IndrawebProcedures.txt
begin
  BuildSnapshots('idrac');
end;
/
--
-- Re-create all indexes.
--
@Indexes.sql
