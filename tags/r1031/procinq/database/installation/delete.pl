#!/usr/bin/perl

use DBI;

local ($dbh) = DBI->connect("dbi:Oracle:hestia", "sbooks", "racer9");
$dbh->{AutoCommit} = 0;

local ($q) = "select nodeid from node where corpusid in (2200028, 2200029)";
local ($sth) = $dbh->prepare($q); 
$sth->execute();

local ($loop) = 0;
while ($nid = $sth->fetchrow_array()) { 
   $q = "delete from node where nodeid = $nid"; print ".";
   $loop++; 

   local ($sth2) = $dbh->prepare($q); 
   $sth2->execute(); $sth2->finish();
   if (($loop % 500) == 0) { print "\n$loop finished.\n"; $dbh->commit; }
}

print "\n$loop finished.\n"; $dbh->commit; $sth->finish();

local ($q) = "delete from corpus where corpusid in (2200028, 2200029)";
local ($sth) = $dbh->prepare($q); 
$sth->execute();

$dbh->commit;

$sth->finish();
$dbh->disconnect();
