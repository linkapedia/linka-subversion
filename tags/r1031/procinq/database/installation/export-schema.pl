#!/usr/bin/perl

use DBI;
use Getopt::Long;

local ($filename) = "";

&GetOptions ("filename=s" => \$filename);

if ($filename eq "") { die "Usage: export-schema.pl -filename <schema>\n"; }
if (!-e $filename) { die "Fatal Error! File: $filename could not be found.\n"; }

# open the existing schema file that was generated from TOAD
open (TOAD, "$filename"); @lines = <TOAD>; close(TOAD);
open (TOAD, ">>$filename"); # open the schema file to append

local ($dbh) = DBI->connect("dbi:Oracle:medusa", "sbooks", "racer9");

print "Creating sequences.."; 

local ($q) = "select sequence_name from user_sequences";
local ($sth) = $dbh->prepare($q); 
$sth->execute();

print TOAD "\n";

local ($loop) = 0;
while ($sname = $sth->fetchrow_array()) { 
   print TOAD "create sequence $sname start with 1;\n";
   print ".";
} print "\n";
$sth->finish();
$dbh->disconnect();

print "Appending fields...";

local ($table) = "NONE";
foreach $line (@lines) {
   if ($line =~ /CREATE TABLE (.*?) \(/) { $table = $1; print TOAD "\n"; }
   if ($line =~ /^\s+(.*?)NUMBER(.*)/) { 
	$col = $1; $etc = $2; chop($etc); if ($etc =~ /\,$/) { chop($etc); }
	print TOAD "ALTER TABLE $table ADD ($col NUMBER $etc);\n"; 
	print TOAD "ALTER TABLE $table MODIFY ($col NUMBER $etc);\n"; print "."; 
   }
   if ($line =~ /^\s+(.*?)VARCHAR2(.*)/) { 
	$col = $1; $etc = $2; chop($etc); if ($etc =~ /\,$/) { chop($etc); }
	print TOAD "ALTER TABLE $table ADD ($col VARCHAR2 $etc);\n"; 
	print TOAD "ALTER TABLE $table MODIFY ($col VARCHAR2 $etc);\n"; print "."; 
   }
} print "\n\n";

print TOAD "\nCOMMIT WORK;\n";
close (TOAD);

print "Done!\n"; 
1;


