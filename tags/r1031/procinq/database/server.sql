Create table CrawlerConfig (
	ConfigId Number(4,0) NOT NULL,
	ConfigParam Varchar2(50),
	ConfigValue Varchar2(200),
	OrderIndex Number(4,0))  ;

Create table MachineLog (
	MachineName Varchar2(30) NOT NULL,
	ConfigId Number(4,0) NOT NULL,
	LastAccess Date NOT NULL)  ;

Create table Channel (
	ChannelId Number(4,0) NOT NULL,
	ChannelName Varchar2(100),
	ISChannel Varchar2(100))  ;

Create table Genre (
	GenreId Number(4,0) NOT NULL UNIQUE ,
	GenreName Varchar2(20),
	GenreStatus Number(1,0) Default 1,
	ClientId Number(5,0) NOT NULL)  ;

Create table MetaChannel (
	MetaChannelId Number(4,0) NOT NULL,
	MetaChannelName Varchar2(20))  ;

Create table Publisher (
	PublisherId Number(4,0) NOT NULL UNIQUE ,
	Publisher Varchar2(50),
	PublisherUrl Varchar2(256))  ;

Create table Client (
	ClientId Number(5,0) NOT NULL,
	ClientName Varchar2(20))  ;

Create table Corpus (
	CorpusId Number(9,0) NOT NULL,
	PublisherId Number(4,0) NOT NULL,
	ClientId Number(5,0) NOT NULL,
	CorpusShortName Varchar2(50) NOT NULL,
	Corpus_Name Varchar2(256) NOT NULL,
	CorpusDesc Varchar2(256),
	CorpusImageIds Varchar2(256),
	CorpusPath Varchar2(256),
	ROCF1 Real Default 0.0,
	ROCF2 Real Default 0.0,
	ROCF3 Real Default 0.0,
	ROCC1 Real Default 0.0,
	ROCC2 Real Default 0.0,
	ROCC3 Real Default 0.0,
	CorpusActive Number(3,0) Default 1,
	CorpusRunFrequency Number(3,0) Default 90,
	CorpusLastRun Date Default SYSDATE)  ;

Create table Document (
	DocumentId Number(9,0) NOT NULL UNIQUE ,
	GenreId Number(4,0) NOT NULL,
	DocTitle Varchar2(256),
	DocUrl Varchar2(256),
	DocumentSummary Varchar2(4000),
	FilterStatus Number(1,0) Default 1,
	TroubleStatus Number(1,0) Default 0)  ;

Create table FileNode (
	FileId Number(9,0) NOT NULL,
	NodeId Number(12,0) NOT NULL,
	Section Number(7,0))  ;

Create table Node (
	NodeId Number(12,0) NOT NULL,
	CorpusId Number(9,0) NOT NULL,
	ClientId Number(5,0) NOT NULL,
	NodeTitle Varchar2(256),
	NodeSize Number(9,0),
	ParentId Number(9,0),
	ParentRel Number(3,0),
	NodeIndexWithinParent Number(3,0),
	DepthFromRoot Number(3,0),
	DateScanned Date,
	DateUpdated Date)  ;

Create table SearchCategory (
	SearchCategoryId Number(6,0) NOT NULL,
	MetaChannelId Number(4,0) NOT NULL,
	Title Varchar2(250),
	ParentId Number(6,0))  ;

Create table CategoryChannel (
	SearchCategoryId Number(6,0) NOT NULL,
	ChannelId Number(4,0) NOT NULL,
	ClientId Number(5,0) NOT NULL)  ;

Create table Checkout (
	NodeId Number(12,0) NOT NULL,
	CorpusId Number(9,0) NOT NULL,
	Hostname Varchar2(200),
	CheckoutDate Date)  ;

Create table CorpusFile (
	CorpusId Number(9,0) NOT NULL,
	FileId Number(9,0) NOT NULL,
	Path Varchar2(256))  ;

Create table CorpusGenre (
	GenreId Number(4,0) NOT NULL,
	CorpusId Number(9,0) NOT NULL)  ;

Create table CoverIdentifiers (
	CoverId Varchar2(10) NOT NULL UNIQUE ,
	CorpusId Number(9,0) NOT NULL,
	Status Number(1,0) Default 1)  ;

Create table DocumentChangeLog (
	DocumentId Number(9,0) NOT NULL,
	NodeId Number(12,0) NOT NULL,
	OldScore Real,
	NewScore Real)  ;

Create table DocumentChannel (
	DocumentId Number(9,0) NOT NULL,
	ChannelId Number(4,0) NOT NULL,
	NodeId Number(12,0) NOT NULL)  ;

Create table EditorLog (
	LogId Number(8,0) NOT NULL,
	NodeId Number(12,0) NOT NULL,
	ActionCode Number(2,0),
	ActionString Varchar2(30),
	ActionDate Date)  ;

Create table GenreDetectUrl (
	GenreId Number(4,0) NOT NULL,
	GenreUrl Varchar2(256) NOT NULL,
	GenreDetectMethod Number(2,0) NOT NULL)  ;

Create table NodeChannel (
	NodeId Number(12,0) NOT NULL,
	ChannelId Number(4,0) NOT NULL)  ;

Create table NodeDocument (
	NodeId Number(12,0) NOT NULL,
	DocumentId Number(9,0) NOT NULL,
	DocumentType Number(2,0),
	Score1 Real,
	Score2 Real,
	Score3 Real,
	Score4 Real,
	Score5 Real,
	Score6 Real,
	DocSummary Varchar2(1000),
	DateScored Date)  ;

Create table NodeGroup (
	NodeId Number(12,0) NOT NULL,
	GroupId Number(6,0) NOT NULL)  ;

Create table Signature (
	SignatureWord Varchar2(30),
	SignatureOccurences Number(5,0),
	NodeId Number(12,0) NOT NULL)  ;

Create table StatLog (
	EventCode Number(3,0) NOT NULL,
	NodeId Number(12,0) NOT NULL,
	EventTime Date,
	EventDesc Varchar2(50),
	Number1 Real,
	Number2 Real,
	Number3 Real,
	Number4 Real)  ;

Create table UpdateLog (
	UpdateLogId Number(13,0) NOT NULL,
	NodeId Number(12,0) NOT NULL,
	ActionCode Number(3,0),
	Hostname Varchar2(50),
	UpdateDate Date)  ;



Alter table CrawlerConfig add primary key (ConfigId);
Alter table MachineLog add primary key (ConfigId);
Alter table NodeUrlQueue add primary key (NodeId,ChannelId);
Alter table Channel add primary key (ChannelId);
Alter table Genre add primary key (GenreId);
Alter table MetaChannel add primary key (MetaChannelId);
Alter table Publisher add primary key (PublisherId);
Alter table Client add primary key (ClientId);
Alter table Corpus add primary key (CorpusId);
Alter table Document add primary key (DocumentId);
Alter table FileNode add primary key (FileId);
Alter table Node add primary key (NodeId);
Alter table SearchCategory add primary key (SearchCategoryId);
Alter table CategoryChannel add primary key (SearchCategoryId,ChannelId,ClientId);
Alter table Checkout add primary key (NodeId,CorpusId);
Alter table CorpusFile add primary key (CorpusId,FileId);
Alter table CorpusGenre add primary key (GenreId,CorpusId);
Alter table CoverIdentifiers add primary key (CoverId);
Alter table DocumentChangeLog add primary key (DocumentId,NodeId);
Alter table DocumentChannel add primary key (DocumentId,ChannelId,NodeId);
Alter table EditorLog add primary key (LogId);
Alter table GenreDetectUrl add primary key (GenreId);
Alter table NodeChannel add primary key (NodeId,ChannelId);
Alter table NodeDocument add primary key (NodeId,DocumentId);
Alter table NodeGroup add primary key (NodeId,GroupId);
Alter table Signature add primary key (NodeId);
Alter table StatLog add primary key (EventCode);
Alter table UpdateLog add primary key (UpdateLogId);

/* Update trigger for Channel */

Create Trigger tu_Channel after update   
of ChannelId
on  Channel 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child NodeChannel update when parent Channel changed */
     if (:old_upd.ChannelId != :new_upd.ChannelId)  then
     	begin
     	update NodeChannel
     	set 	ChannelId = :new_upd.ChannelId
     	where 	NodeChannel.ChannelId = :old_upd.ChannelId ;
     	end;
     end if;
     
     /* cascade child CategoryChannel update when parent Channel changed */
     if (:old_upd.ChannelId != :new_upd.ChannelId)  then
     	begin
     	update CategoryChannel
     	set 	ChannelId = :new_upd.ChannelId
     	where 	CategoryChannel.ChannelId = :old_upd.ChannelId ;
     	end;
     end if;
     
     /* cascade child DocumentChannel update when parent Channel changed */
     if (:old_upd.ChannelId != :new_upd.ChannelId)  then
     	begin
     	update DocumentChannel
     	set 	ChannelId = :new_upd.ChannelId
     	where 	DocumentChannel.ChannelId = :old_upd.ChannelId ;
     	end;
     end if;
     
     /* cascade child NodeUrlQueue update when parent Channel changed */
     if (:old_upd.ChannelId != :new_upd.ChannelId)  then
     	begin
     	update NodeUrlQueue
     	set 	ChannelId = :new_upd.ChannelId
     	where 	NodeUrlQueue.ChannelId = :old_upd.ChannelId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Genre */

Create Trigger tu_Genre after update   
of GenreId,ClientId
on  Genre 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Document update when parent Genre changed */
     if (:old_upd.GenreId != :new_upd.GenreId)  then
     	begin
     	update Document
     	set 	GenreId = :new_upd.GenreId
     	where 	Document.GenreId = :old_upd.GenreId ;
     	end;
     end if;
     
     /* cascade child GenreDetectUrl update when parent Genre changed */
     if (:old_upd.GenreId != :new_upd.GenreId)  then
     	begin
     	update GenreDetectUrl
     	set 	GenreId = :new_upd.GenreId
     	where 	GenreDetectUrl.GenreId = :old_upd.GenreId ;
     	end;
     end if;
     
     /* cascade child CorpusGenre update when parent Genre changed */
     if (:old_upd.GenreId != :new_upd.GenreId)  then
     	begin
     	update CorpusGenre
     	set 	GenreId = :new_upd.GenreId
     	where 	CorpusGenre.GenreId = :old_upd.GenreId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for MetaChannel */

Create Trigger tu_MetaChannel after update   
of MetaChannelId
on  MetaChannel 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child SearchCategory update when parent MetaChannel changed */
     if (:old_upd.MetaChannelId != :new_upd.MetaChannelId)  then
     	begin
     	update SearchCategory
     	set 	MetaChannelId = :new_upd.MetaChannelId
     	where 	SearchCategory.MetaChannelId = :old_upd.MetaChannelId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Publisher */

Create Trigger tu_Publisher after update   
of PublisherId
on  Publisher 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Corpus update when parent Publisher changed */
     if (:old_upd.PublisherId != :new_upd.PublisherId)  then
     	begin
     	update Corpus
     	set 	PublisherId = :new_upd.PublisherId
     	where 	Corpus.PublisherId = :old_upd.PublisherId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Client */

Create Trigger tu_Client after update   
of ClientId
on  Client 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Node update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update Node
     	set 	ClientId = :new_upd.ClientId
     	where 	Node.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
     /* cascade child CategoryChannel update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update CategoryChannel
     	set 	ClientId = :new_upd.ClientId
     	where 	CategoryChannel.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
     /* cascade child Genre update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update Genre
     	set 	ClientId = :new_upd.ClientId
     	where 	Genre.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
     /* cascade child Corpus update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update Corpus
     	set 	ClientId = :new_upd.ClientId
     	where 	Corpus.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Corpus */

Create Trigger tu_Corpus after update   
of CorpusId,PublisherId,ClientId
on  Corpus 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Node update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update Node
     	set 	CorpusId = :new_upd.CorpusId
     	where 	Node.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child CoverIdentifiers update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update CoverIdentifiers
     	set 	CorpusId = :new_upd.CorpusId
     	where 	CoverIdentifiers.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child Checkout update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update Checkout
     	set 	CorpusId = :new_upd.CorpusId
     	where 	Checkout.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child CorpusFile update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update CorpusFile
     	set 	CorpusId = :new_upd.CorpusId
     	where 	CorpusFile.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child CorpusGenre update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update CorpusGenre
     	set 	CorpusId = :new_upd.CorpusId
     	where 	CorpusGenre.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Document */

Create Trigger tu_Document after update   
of DocumentId,GenreId
on  Document 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child NodeDocument update when parent Document changed */
     if (:old_upd.DocumentId != :new_upd.DocumentId)  then
     	begin
     	update NodeDocument
     	set 	DocumentId = :new_upd.DocumentId
     	where 	NodeDocument.DocumentId = :old_upd.DocumentId ;
     	end;
     end if;
     
     /* cascade child DocumentChangeLog update when parent Document changed */
     if (:old_upd.DocumentId != :new_upd.DocumentId)  then
     	begin
     	update DocumentChangeLog
     	set 	DocumentId = :new_upd.DocumentId
     	where 	DocumentChangeLog.DocumentId = :old_upd.DocumentId ;
     	end;
     end if;
     
     /* cascade child DocumentChannel update when parent Document changed */
     if (:old_upd.DocumentId != :new_upd.DocumentId)  then
     	begin
     	update DocumentChannel
     	set 	DocumentId = :new_upd.DocumentId
     	where 	DocumentChannel.DocumentId = :old_upd.DocumentId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for FileNode */

Create Trigger tu_FileNode after update   
of FileId,NodeId
on  FileNode 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child CorpusFile update when parent FileNode changed */
     if (:old_upd.FileId != :new_upd.FileId)  then
     	begin
     	update CorpusFile
     	set 	FileId = :new_upd.FileId
     	where 	CorpusFile.FileId = :old_upd.FileId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Node */

Create Trigger tu_Node after update   
of NodeId,CorpusId,ClientId
on  Node 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Signature update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update Signature
     	set 	NodeId = :new_upd.NodeId
     	where 	Signature.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child StatLog update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update StatLog
     	set 	NodeId = :new_upd.NodeId
     	where 	StatLog.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child NodeChannel update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update NodeChannel
     	set 	NodeId = :new_upd.NodeId
     	where 	NodeChannel.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child NodeGroup update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update NodeGroup
     	set 	NodeId = :new_upd.NodeId
     	where 	NodeGroup.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child NodeDocument update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update NodeDocument
     	set 	NodeId = :new_upd.NodeId
     	where 	NodeDocument.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child UpdateLog update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update UpdateLog
     	set 	NodeId = :new_upd.NodeId
     	where 	UpdateLog.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child FileNode update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update FileNode
     	set 	NodeId = :new_upd.NodeId
     	where 	FileNode.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child EditorLog update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update EditorLog
     	set 	NodeId = :new_upd.NodeId
     	where 	EditorLog.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child Checkout update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update Checkout
     	set 	NodeId = :new_upd.NodeId
     	where 	Checkout.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child DocumentChangeLog update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update DocumentChangeLog
     	set 	NodeId = :new_upd.NodeId
     	where 	DocumentChangeLog.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child DocumentChannel update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update DocumentChannel
     	set 	NodeId = :new_upd.NodeId
     	where 	DocumentChannel.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
end;
/
/* Update trigger for SearchCategory */

Create Trigger tu_SearchCategory after update   
of SearchCategoryId,MetaChannelId
on  SearchCategory 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child NodeGroup update when parent SearchCategory changed */
     if (:old_upd.SearchCategoryId != :new_upd.SearchCategoryId)  then
     	begin
     	update NodeGroup
     	set 	GroupId = :new_upd.SearchCategoryId
     	where 	NodeGroup.GroupId = :old_upd.SearchCategoryId ;
     	end;
     end if;
     
     /* cascade child CategoryChannel update when parent SearchCategory changed */
     if (:old_upd.SearchCategoryId != :new_upd.SearchCategoryId)  then
     	begin
     	update CategoryChannel
     	set 	SearchCategoryId = :new_upd.SearchCategoryId
     	where 	CategoryChannel.SearchCategoryId = :old_upd.SearchCategoryId ;
     	end;
     end if;
     
      
     
end;
/












