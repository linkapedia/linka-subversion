REM Sample script to create database - Page 33 - 36
REM * ***********************************************************
REM * Script to create DB1 instance with db_block_size = 8192
REM *
REM *  Created:   Dave Kreines - 10/18/98
REM *
REM *
REM * ***********************************************************
REM * Start the instance (ORACLE_SID must be set to <athena>).
REM *
connect internal
startup nomount pfile=/home/oracle/OraHome1/dbs/initathena.ora

REM * Create the <athena> database.
REM *
create database "athena"
    maxinstances 2
    maxlogfiles  32
    maxdatafiles 1000
    character set "US7ASCII"
datafile '/RAID01/athena/system01.dbf' size 50M
logfile  '/home/oracle/OraHome1/athena/log01.log' size 512M,
         '/home/oracle/OraHome1/athena/log02.log' size 512M,
         '/home/oracle/OraHome1/athena/log03.log' size 512M,
         '/home/oracle/OraHome1/athena/log04.log' size 512M,
         '/home/oracle/OraHome1/athena/log05.log' size 512M,
         '/home/oracle/OraHome1/athena/log06.log' size 512M,
         '/home/oracle/OraHome1/athena/log07.log' size 512M,
         '/home/oracle/OraHome1/athena/log08.log' size 512M,
         '/home/oracle/OraHome1/athena/log09.log' size 512M,
         '/home/oracle/OraHome1/athena/log10.log' size 512M

REM * Now perform all commands necessary to create
REM * the final database after the CREATE DATABASE command has
REM * succeeded.


REM * install data dictionary:
@/home/oracle/OraHome1/rdbms/admin/catalog.sql

REM * install procedural components:
@/home/oracle/OraHome1/rdbms/admin/catproc.sql


REM * Create additional rollback segment in SYSTEM since
REM * at least one non-system rollback segment is required
REM * before creating a tablespace.
REM *
create rollback segment SYSROLL tablespace system
storage (initial 25K next 25K minextents 2 maxextents 99);

REM * Put SYSROLL online without shutting
REM * down and restarting the database.
REM *
alter rollback segment SYSROLL online;

REM * Create a tablespace for rollback segments.
REM *
create tablespace ROLLBACK1
 datafile '/RAID01/athena/rbs01.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);

create tablespace ROLLBACK2
 datafile '/RAID01/athena/rbs02.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
create tablespace ROLLBACK3
 datafile '/RAID01/athena/rbs03.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
create tablespace ROLLBACK4
 datafile '/RAID01/athena/rbs04.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
create tablespace ROLLBACK5
 datafile '/RAID01/athena/rbs05.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
create tablespace ROLLBACK6
 datafile '/RAID01/athena/rbs06.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
create tablespace ROLLBACK7
 datafile '/RAID01/athena/rbs07.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
create tablespace ROLLBACK8
 datafile '/RAID01/athena/rbs08.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
create tablespace ROLLBACK9
 datafile '/RAID01/athena/rbs09.dbf' size 512M
 default storage (
  initial      128M
  next         128M
  pctincrease  0
  minextents   2
);
REM * Create the "real" rollback segments.
REM *
create rollback segment RBS01 tablespace ROLLBACK1
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS02 tablespace ROLLBACK2
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS03 tablespace ROLLBACK3
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS04 tablespace ROLLBACK4
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS05 tablespace ROLLBACK5
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS06 tablespace ROLLBACK6
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS07 tablespace ROLLBACK7
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS08 tablespace ROLLBACK8
storage (initial 128M next 128M minextents 2 optimal 350M);
create rollback segment RBS09 tablespace ROLLBACK9
storage (initial 128M next 128M minextents 2 optimal 350M);

REM * Use ALTER ROLLBACK SEGMENT ONLINE to put rollback segments online
REM * without shutting down and restarting the database.
REM *
alter rollback segment RBS01 online;
alter rollback segment RBS02 online;
alter rollback segment RBS03 online;
alter rollback segment RBS04 online;
alter rollback segment RBS05 online;
alter rollback segment RBS06 online;
alter rollback segment RBS07 online;
alter rollback segment RBS08 online;
alter rollback segment RBS09 online;

REM * Since we've created and brought online 10 more rollback segments,
REM * we no longer need the rollback segment in the SYSTEM tablespace.
REM * We could delete it, but we will leave it here in case we need it
REM * in the future.
alter rollback segment SYSROLL offline;

REM * Create a tablespace for temporary segments.
create tablespace TEMP
 datafile '/RAID01/athena/temp01.dbf' size 1280M
 default storage (
 initial     64M
 next        64M
 maxextents  UNLIMITED
 pctincrease 0
);

REM * Create a tablespace for database tools.
REM *
create tablespace TOOLS
 datafile '/RAID01/athena/tools01.dbf' size 25M
 default storage (
 initial     50K
 next        50K
 maxextents  UNLIMITED
 pctincrease 0
);

REM * Create tablespaces for user activity.
REM *
create tablespace DDATA
datafile '/RAID01/athena/ddata01.dbf' size 2000M,
'/RAID01/athena/ddata02.dbf' size 2000M,
'/RAID01/athena/ddata03.dbf' size 2000M,
'/RAID01/athena/ddata04.dbf' size 2000M,
'/RAID01/athena/ddata05.dbf' size 2000M,
'/RAID01/athena/ddata06.dbf' size 2000M,
'/RAID01/athena/ddata07.dbf' size 2000M,
'/RAID01/athena/ddata08.dbf' size 2000M,
'/RAID01/athena/ddata09.dbf' size 2000M,
'/RAID01/athena/ddata10.dbf' size 2000M,
'/RAID01/athena/ddata11.dbf' size 2000M,
'/RAID01/athena/ddata12.dbf' size 2000M,
'/RAID01/athena/ddata13.dbf' size 2000M,
'/RAID01/athena/ddata14.dbf' size 2000M,
'/RAID01/athena/ddata15.dbf' size 2000M,
'/RAID01/athena/ddata16.dbf' size 2000M,
'/RAID01/athena/ddata17.dbf' size 2000M,
'/RAID01/athena/ddata18.dbf' size 2000M,
'/RAID01/athena/ddata19.dbf' size 2000M,
'/RAID01/athena/ddata20.dbf' size 2000M,
'/RAID01/athena/ddata21.dbf' size 2000M,
'/RAID01/athena/ddata22.dbf' size 2000M,
'/RAID01/athena/ddata23.dbf' size 2000M,
'/RAID01/athena/ddata24.dbf' size 2000M,
'/RAID01/athena/ddata25.dbf' size 2000M,
'/RAID01/athena/ddata26.dbf' size 2000M,
'/RAID01/athena/ddata27.dbf' size 2000M,
'/RAID01/athena/ddata28.dbf' size 2000M,
'/RAID01/athena/ddata29.dbf' size 2000M,
'/RAID01/athena/ddata30.dbf' size 2000M,
'/RAID01/athena/ddata31.dbf' size 2000M,
'/RAID01/athena/ddata32.dbf' size 2000M,
'/RAID01/athena/ddata33.dbf' size 2000M,
'/RAID01/athena/ddata34.dbf' size 2000M,
'/RAID01/athena/ddata35.dbf' size 2000M,
'/RAID01/athena/ddata36.dbf' size 2000M,
'/RAID01/athena/ddata37.dbf' size 2000M,
'/RAID01/athena/ddata38.dbf' size 2000M,
'/RAID01/athena/ddata39.dbf' size 2000M,
'/RAID01/athena/ddata40.dbf' size 2000M,
'/RAID01/athena/ddata41.dbf' size 2000M,
'/RAID01/athena/ddata42.dbf' size 2000M,
'/RAID01/athena/ddata43.dbf' size 2000M,
'/RAID01/athena/ddata44.dbf' size 2000M,
'/RAID01/athena/ddata45.dbf' size 2000M,
'/RAID01/athena/ddata46.dbf' size 2000M,
'/RAID01/athena/ddata47.dbf' size 2000M,
'/RAID01/athena/ddata48.dbf' size 2000M,
'/RAID01/athena/ddata49.dbf' size 2000M,
'/RAID01/athena/ddata50.dbf' size 2000M
 default storage (
   initial     250K
   next        250K
   maxextents  UNLIMITED
   pctincrease 0
);

create tablespace RDATA
 datafile '/RAID01/athena/rdata01.dbf' size 1538M
 default storage (
   initial     250K
   next        250K
   maxextents  UNLIMITED
   pctincrease 0
);

REM * Create tablespaces for indexes.
REM *
create tablespace RINDEX
 datafile '/RAID01/athena/rindex01.dbf' size 512M
 default storage (
   initial     250K
   next        250K
   maxextents  UNLIMITED
   pctincrease 0
);

create tablespace DINDEX
datafile '/RAID01/athena/dindex01.dbf' size 2000M,
'/RAID01/athena/dindex02.dbf' size 2000M, 
'/RAID01/athena/dindex03.dbf' size 2000M, 
'/RAID01/athena/dindex04.dbf' size 2000M, 
'/RAID01/athena/dindex05.dbf' size 2000M, 
'/RAID01/athena/dindex06.dbf' size 2000M, 
'/RAID01/athena/dindex07.dbf' size 2000M,
'/RAID01/athena/dindex08.dbf' size 2000M, 
'/RAID01/athena/dindex09.dbf' size 2000M, 
'/RAID01/athena/dindex10.dbf' size 2000M
 default storage (
   initial     250K
   next        250K
   maxextents  UNLIMITED
   pctincrease 0
);

REM * Alter SYS and SYSTEM users, because Oracle will make SYSTEM
REM * the default and temporary tablespace by default, and we don't
REM * want that.
REM *
alter user sys temporary tablespace TEMP;
alter user system default tablespace TOOLS temporary tablespace TEMP;

REM * Now run the Oracle-supplied scripts we need for this DB
REM *
@/home/oracle/OraHome1/rdbms/admin/catexp.sql
@/home/oracle/OraHome1/rdbms/admin/dbmspool.sql
@/home/oracle/OraHome1/rdbms/admin/prvtpool.plb

REM * Now run the Oracle-supplied script to create the DBA views
REM * for the SYSTEM account.  Change to SYSTEM first.
REM *
connect system/manager
@/home/oracle/OraHome1/rdbms/admin/catdbsyn.sql

REM * All done, so close the log file and exit.
REM *
exit
