// Login
//
// Log into the ProcinQ Server

import com.iw.system.*;
import com.iw.tools.*;

import java.util.*;
import java.io.*;

public class Test {
    public static void main(String args[]) throws Exception {

        String sBibliography = "bibliography";
        if (sBibliography.indexOf('&') != -1) {
            sBibliography.replaceAll("&", "&'||'");
        }
        String sFullText = "filepath";
        String sLanguage = "language";
        String sCountry = "country";
        String sIdracNumber = "idracnumber";
        String sOutdated = "0";
        String sForm = "0";
        String sGroupDN = "group";

        Calendar now = Calendar.getInstance();
        String sDate = "";
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;
        int day = now.get(Calendar.DAY_OF_MONTH);

        // get today's date
        if (month > 9) {
            sDate = "" + month;
        } else {
            sDate = "0" + month;
        }
        if (day > 9) {
            sDate = sDate + "/" + day;
        } else {
            sDate = sDate + "/0" + day;
        }
        sDate = sDate + "/" + year;

        String sAdoption = sDate;
        String sPublication = sDate;
        String sEntry = sDate;
        String sRevision = sDate;
        String sSystem = sDate;

        String sDocumentID = "13";

        String sSQL = "update document set bibliography = ?, language = '"+sLanguage+"', country = '"+sCountry+"', "+
                       "idracnumber = '"+sIdracNumber+"', adoptiondate = to_date('"+sAdoption+"', 'MM/DD/YY'), "+
                       "publicationdate = to_date('"+sPublication+"', 'MM/DD/YY'), entrydate = to_date('"+sEntry+"', 'MM/DD/YY'), "+
                       "revisiondate = to_date('"+sRevision+"', 'MM/DD/YY'), systemdate = to_date('"+sSystem+"', 'MM/DD/YY'), "+
                       "outdated = "+sOutdated+", form = "+sForm+") where DocumentID = "+sDocumentID;
                System.out.println("idracdocument output: "+sSQL);
    }
}