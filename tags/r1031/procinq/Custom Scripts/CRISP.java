import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class CRISP {
    public static Hashtable nodes = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        File f = new File("C:/Documents and Settings/indraweb/projects/perlscripts/crisp/crisp.out");

        String sCorpusName = "CRISP";
        System.out.println("Corpus: "+sCorpusName);

        c = server.getCorpora("1778");
        Vector v = server.CQL("SELECT <NODE> WHERE CORPUSID = 1778", 1, 100000);

        System.out.println("Found "+v.size()+" total nodes in corpus DTIC.");
        for (int i = 0; i < v.size(); i++) {
            Node n = (Node) v.elementAt(i);
            nodes.put(n.get("NODETITLE").trim(), n);
        }

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null; String text = ""; Node n = null;
        boolean bBroader = true; int broader = 0; int narrower = 0; int newstuff = 0;
        try {
            while ( (record=dis.readLine()) != null ) {
                record = record.replaceAll("\\"+(char)42, "");

                if (!record.trim().equals("")) {
                    if (record.trim().startsWith("BT ")) {
                        if (nodes.containsKey(record.trim().substring(3))) {
                            Node n3 = (Node) nodes.get(record.trim().substring(3));
                            if (n3 != null && bBroader) {
                                int depth = Integer.parseInt(n3.get("DEPTHFROMROOT"));

                                n.set("PARENTID", n3.get("NODEID"));
                                n.set("DEPTHFROMROOT", (depth-1)+"");

                                System.out.println("Modify node "+n.get("NODETITLE")+" to parent "+n3.get("NODETITLE"));
                                boolean b = server.editNodeProps(n);
                                if (!b) System.err.println("MODIFICATION FAILED!!! :(");
                                else bBroader = false;
                            }

                            broader++;
                        }
                    } else if (record.trim().startsWith("NT ")) {
                        if (nodes.containsKey(record.trim().substring(3))) {
                            Node n3 = (Node) nodes.get(record.trim().substring(3));
                            if (n3 != null) {
                                int depth = Integer.parseInt(n3.get("DEPTHFROMROOT"));

                                n3.set("PARENTID", n.get("NODEID"));
                                n3.set("DEPTHFROMROOT", (depth+1)+"");

                                System.out.println("Modify node "+n3.get("NODETITLE")+" to parent "+n.get("NODETITLE"));
                                boolean b = server.editNodeProps(n3);
                                if (!b) System.err.println("MODIFICATION FAILED!!! :(");
                            }

                            narrower++;
                        }
                    } else if (record.trim().startsWith("use ")) {

                    } else if (record.trim().startsWith("UF ")) {

                    } else if (record.substring(0, 1).equals(" ")) {

                    } else {
                        System.out.println("-------------------------------------------------");
                        text = "";
                        n = null;
                        bBroader = true;

                        System.out.println("Term: "+record.trim());
                        n = (Node) nodes.get(record.trim());
                        n.set("NODETITLE", n.get("NODETITLE").trim());
                        if (n == null) {
                            System.err.println("FATAL ERROR!! CANNOT FIND "+record.trim());

                            Node parent = (Node) nodes.get(record.trim().substring(0, 1).toUpperCase());
                            n = addNode(record.trim(), record.trim(), parent.get("NODEID"), 2);
                            nodes.put(record.trim(), n); newstuff++;
                        }
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }

        System.out.println("FYI: There were "+narrower+" narrower terms total and "+broader+" broader terms total and new terms "+newstuff+" total.");
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}