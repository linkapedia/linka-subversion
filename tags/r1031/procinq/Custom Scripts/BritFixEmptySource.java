import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.xml.sax.InputSource;

public class BritFixEmptySource {
    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    private static Hashtable nodes = new Hashtable();

    public static void main(String args[]) throws Exception {
        String _start = "C:/Documents and Settings/indraweb/Desktop/TAX-DEV/student/ebi/xml_articles/";

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        System.out.println("Loading nodes in corpus 2309...");
        c = server.getCorpora("2309");
        Node root = server.getCorpusRoot(c.getID());

        for (int loop = 1; loop < 33000; loop = loop + 1000) {
            System.out.println("selecting from "+loop+" to "+(loop+999));
            Vector vNodes = server.CQL("select <node> where corpusid = 2309", loop, (loop+999));

            for (int i = 0; i < vNodes.size(); i++) {
                Node n = (Node) vNodes.elementAt(i);
                String desc = (String) n.get("NODEDESC");

                try {
                    int britnum = Integer.parseInt(n.get("NODEDESC").trim());
                    if (britnum != -1) {
                        britnum = britnum - 9000000;
                        Vector v = new Vector();
                        if (nodes.containsKey(britnum+"")) { v = (Vector) nodes.get(britnum+""); }
                        v.add(n);
                        nodes.put(britnum+"", v);
                    }
                } catch (Exception e) { }
            }
        }

        System.out.println("Size: "+nodes.size());

        System.out.println("Getting all files in the directory ...");

        // step 2: loop for every file in the brit art directory and assign node information
        File fDirectory = new File(_start);
        String FilePaths[] = fDirectory.list();

        System.out.println("Looping...");

        for (int j = 0; j < FilePaths.length; j++) {
            File f = new File(_start + FilePaths[j]);

            if (FilePaths[j].endsWith(".xml")) {
                FileInputStream fis = new FileInputStream(f);
                SAXReader xmlReader = new SAXReader(false);
                xmlReader.setValidation(false);

                InputSource ins = new InputSource(fis);
                ins.setEncoding("UTF-8");
                ins.setSystemId(f.toURL().toString());

                org.dom4j.Document doc = xmlReader.read(ins);

                Element eArticle = doc.getRootElement();
                Element eTitle = eArticle.element("title");
                String ID = eArticle.attribute("artclid").getValue();

                System.out.println("Reading: "+eTitle.getStringValue()+" (ID: "+ID+")");
                if (nodes.containsKey(ID.trim())) {
                    Vector v = (Vector) nodes.get(ID.trim());

                    StringBuffer sb = new StringBuffer();

                    if (eArticle.element("firstpar") != null)
                        sb.append(eArticle.element("firstpar").getStringValue());

                    List source = eArticle.elements("p");
                    if (source != null) {
                        Iterator i = source.iterator();

                        while (i.hasNext()) {
                            Element p = (Element) i.next();
                            sb.append(p.getStringValue()+"\r\n");
                        }
                    }

                    for (int lup = 0; lup < v.size(); lup++) {
                        Node node = (Node) v.elementAt(lup);

                        try {
                            System.out.println("Set source: "+node.get("NODETITLE")+" ID: "+node.get("NODEID")+" size: "+sb.length());
                            server.setNodeSource(node, sb.toString());
                        }
                        catch (Exception ex) {
                            System.err.println("Could not set source for node "+node.get("NODETITLE")+" length: "+sb.toString().length());
                            ex.printStackTrace(System.err);
                        }
                    }
                }
            }
        }
    }
}
