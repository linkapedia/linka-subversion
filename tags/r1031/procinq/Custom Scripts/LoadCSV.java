import com.iw.system.*;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class LoadCSV {
    public static Hashtable htArguments = new Hashtable(); // input arguments
    public static Hashtable nodes = new Hashtable(); // nodes already inserted

    private static ProcinQ server = new ProcinQ(); // server/API instance

    public static void main(String args[]) throws Exception {
        // Step 1: read input arguments
        htArguments = getArgHash(args);

        String sFilename = (String) htArguments.get("filename");
        String sCorpusName = (String) htArguments.get("corpusname");

        // Step 2. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 3. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        // Step 4. Create the corpus and root node
        Corpus c = server.addCorpus(sCorpusName, sCorpusName);
        Node root = server.getCorpusRoot(c.getID());

        // Step 5. Read CSV File and create nodes
        File f = new File(sFilename);
        //DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF8"));

        String record = null;
        try {
            while ((record = br.readLine()) != null) {
                String[] line = record.replaceAll("\"", "").split(",");
                String nodedepth1 = line[0];
                String nodedepth2 = line[1];

                if (!nodes.containsKey(nodedepth1)) nodes.put(nodedepth1, addNode(c, nodedepth1, root.get("NODEID"), 1));
                Node parent = (Node) nodes.get(nodedepth1);

                addNode(c, nodedepth2, parent.get("NODEID"), 2);
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            e.printStackTrace(System.err);
            return;
        }

    }

    public static Node addNode(Corpus c, String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    // get argument list from command line
   public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}
