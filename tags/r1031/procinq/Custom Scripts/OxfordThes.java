import com.iw.system.*;

import java.util.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.xml.sax.InputSource;

public class OxfordThes {

    public static com.iw.system.Thesaurus t = null;
    public static ProcinQ server = null;

    public static void main(String args[]) throws Exception {

        // Step 1. Initialize the server object
        server = new ProcinQ();
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("its", "its");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        // create thesaurus
        t = server.createThesaurus("Oxford Internet");

        // now loop through each item in the directory
        String dirpath = "C:/Documents and Settings/indraweb/Desktop/TAX-DEV/oxford/Internet/";
        File dir = new File(dirpath);
        String[] children = dir.list();  int loop = 0;
        for (int k=0; k < children.length; k++) {
            loop++;
            File f = new File(dirpath+children[k]);
            if (f.getAbsolutePath().endsWith(".xml")) {
                FileInputStream fis = new FileInputStream(f);
                SAXReader xmlReader = new SAXReader(false);
                xmlReader.setValidation(false);

                InputSource ins = new InputSource(fis);
                ins.setEncoding("UTF-8");
                ins.setSystemId(f.toURL().toString());

                org.dom4j.Document doc = xmlReader.read(ins);

                Element eArticle = doc.getRootElement();
                List list = eArticle.elements("letter");
                Iterator i = list.iterator();

                while (i.hasNext()) {
                    Element letter = (Element) i.next();
                    List list2 = letter.elements("e");
                    Iterator i2 = list2.iterator();

                    while (i2.hasNext()) {
                        Element e = (Element) i2.next();
                        Element sg = e.element("sg");
                        String base = e.element("hg").element("hw").getText();

                        System.out.println("*** WORD: "+base);
                        getRelationships(sg, base);
                        System.out.println(" ");
                    }
                }

            }
        }
    }

    public static void getRelationships (Element e, String word) {
        Vector v = new Vector();
        if (e == null) return;

        List l = e.elements();
        Iterator i = l.iterator();

        while (i.hasNext()) {
            Element e2 = (Element) i.next();

            /*
                (a) if there is a <exp>, make it a synonym
                (b) if there is a <emb>, make it a related term
                (c) if there is a <xr>, make it a related term
                (d) if there is a <work>, make it a narrower term
                (e) if there is a <var>, make it a synonym
            */
            if (e2.getName().toLowerCase().equals("exp")) {
                System.out.println("<exp> "+word+" is a SYNONYM of "+e2.getText());
                addRelationship("1", word, e2.getText());
                addRelationship("1", e2.getText(), word);
            } else if (e2.getName().toLowerCase().equals("emb")) {
                System.out.println("<emb> "+word+" is a RELATED TERM of "+e2.getText());
                addRelationship("4", word, e2.getText());
            } else if (e2.getName().toLowerCase().equals("xr")) {
                System.out.println("<xr> "+word+" is a RELATED TERM of "+e2.getText());
                addRelationship("4", word, e2.getText());
            } else if (e2.getName().toLowerCase().equals("work")) {
                System.out.println("<work> "+word+" is a NARROWER of "+e2.getText());
                addRelationship("3", word, e2.getText());
                addRelationship("2", e2.getText(), word);
            } else if (e2.getName().toLowerCase().equals("var")) {
                System.out.println("<var> "+word+" is a SYNONYM of "+e2.getText());
                addRelationship("1", word, e2.getText());
                addRelationship("1", e2.getText(), word);
            }

            getRelationships(e2, word);
        }
    }

    private static void addRelationship(String relationship, String base, String predicate) {
        System.out.println("Relationship: " + relationship + " Word: " + base + " Predicate: "+predicate);

        try { server.addRelationship(t.getID(), base, predicate, relationship); }
        catch (Exception e) {
            System.err.println("*** "+base+" "+relationship+" "+predicate);
            e.printStackTrace(System.err);
        }
    }
}