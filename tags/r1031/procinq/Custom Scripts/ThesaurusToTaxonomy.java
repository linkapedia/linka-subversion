// BritArt
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class ThesaurusToTaxonomy {
    public static Hashtable htArguments = new Hashtable();
    public static Hashtable htNodes = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;
    private static Node root = null;

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("127.0.0.1");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("demo", "demo");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        if (!htArguments.containsKey("thesaurus")) { System.err.println("Required: -thesaurus <thesaurusid>"); return; }

        // create the corpus based upon the thesaurus name
        String sCorpusName = (String) htArguments.get("corpus");
        c = server.addCorpus(sCorpusName, sCorpusName);
        root = server.getCorpusRoot(c.getID());

        // create an alphabetical hierarchy
        System.out.println("Creating alphabetical hierarchy ...");
        createAlphabet();

        // select: thesaurusword, relationship, thesaurusword ...
        System.out.println("Get relationship set ...");
        Vector v = server.getThesaurusRelats((String) htArguments.get("thesaurus"));

        System.out.println("Loop through each term ...");

        // loop through each term.   If relationship is NT, create
        for (int i = 0; i < v.size(); i++) {
            HashTree ht = (HashTree) v.elementAt(i);

            if (((String) ht.get("RELATIONSHIP")).equals("3")) {
                String s1 = (String) ht.get("WORD");
                String s2 = (String) ht.get("PREDICATE");

                if (!htNodes.containsKey(s1.toLowerCase())) {
                    Node parent = (Node) htNodes.get(s1.toLowerCase().substring(0, 1));
                    if (parent == null) parent = (Node) htNodes.get("other");
                    htNodes.put(s1.toLowerCase(), addNode(s1, parent.get("NODEID"), 2));
                }

                Node n = (Node) htNodes.get(s1.toLowerCase());
                htNodes.put(s2.toLowerCase(), addNode(s2, n.get("NODEID"), 3));
            }
        }

    }

    public static void createAlphabet() {
        String s[] = { "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","Other" };

        for (int i = 0; i < s.length; i++) {
            Node n = addNode(s[i], root.get("NODEID"), 1);
            htNodes.put(s[i].toLowerCase(), n);
        }

    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static Node addNode (String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", printCapitalized(sNodeTitle));
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth+"");
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try { n = server.addNode(n); }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); e.printStackTrace(System.err); }

        return n;

    }

    public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}