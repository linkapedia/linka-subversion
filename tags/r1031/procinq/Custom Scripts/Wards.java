import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class Wards {
    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;
    private static Hashtable nodes = new Hashtable();

    // nodes
    public static Node company = null;
    public static Node variants = null;
    public static Node domains = null;
    public static Node brands = null;

    public static Node subsidiaries = null;
    public static Node products = null;
    public static Node ticker = null;
    public static Node SIC = null;

    public static Node NAISC = null;
    public static Node employee = null;

    public static Node acquisition = null;
    public static Node merger = null;
    public static Node nchange = null;
    public static Node dbas = null;
    public static Node lname = null;
    public static Node iname = null;
    public static Node pname = null;

    public static Hashtable htBrands = new Hashtable();
    public static Hashtable htProducts = new Hashtable();

    public static boolean bNextemployee = false;

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        File f = new File("C:/Documents and Settings/indraweb/projects/perlscripts/Wards/birwards.txt");

        String sCorpusName = "Wards";
        System.out.println("Corpus: "+sCorpusName);

        c = server.addCorpus(sCorpusName, sCorpusName);
        Node root = server.getCorpusRoot(c.getID());

        nodes.put("A", addNode("A", "A", root.get("NODEID"), 1));
        nodes.put("B", addNode("B", "B", root.get("NODEID"), 1));
        nodes.put("C", addNode("C", "C", root.get("NODEID"), 1));
        nodes.put("D", addNode("D", "D", root.get("NODEID"), 1));
        nodes.put("E", addNode("E", "E", root.get("NODEID"), 1));
        nodes.put("F", addNode("F", "F", root.get("NODEID"), 1));
        nodes.put("G", addNode("G", "G", root.get("NODEID"), 1));
        nodes.put("H", addNode("H", "H", root.get("NODEID"), 1));
        nodes.put("I", addNode("I", "I", root.get("NODEID"), 1));
        nodes.put("J", addNode("J", "J", root.get("NODEID"), 1));
        nodes.put("K", addNode("K", "K", root.get("NODEID"), 1));
        nodes.put("L", addNode("L", "L", root.get("NODEID"), 1));
        nodes.put("M", addNode("M", "M", root.get("NODEID"), 1));
        nodes.put("N", addNode("N", "N", root.get("NODEID"), 1));
        nodes.put("O", addNode("O", "O", root.get("NODEID"), 1));
        nodes.put("P", addNode("P", "P", root.get("NODEID"), 1));
        nodes.put("Q", addNode("Q", "Q", root.get("NODEID"), 1));
        nodes.put("R", addNode("R", "R", root.get("NODEID"), 1));
        nodes.put("S", addNode("S", "S", root.get("NODEID"), 1));
        nodes.put("T", addNode("T", "T", root.get("NODEID"), 1));
        nodes.put("U", addNode("U", "U", root.get("NODEID"), 1));
        nodes.put("V", addNode("V", "V", root.get("NODEID"), 1));
        nodes.put("W", addNode("W", "W", root.get("NODEID"), 1));
        nodes.put("X", addNode("X", "X", root.get("NODEID"), 1));
        nodes.put("Y", addNode("Y", "Y", root.get("NODEID"), 1));
        nodes.put("Z", addNode("Z", "Z", root.get("NODEID"), 1));
        nodes.put("0", addNode("Other", "Other non alphabetic", root.get("NODEID"), 1));

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;

        Vector allcodes = new Vector();
        allcodes.add("CO");
        allcodes.add("VN");
        allcodes.add("EL");
        allcodes.add("UR");
        allcodes.add("BT");
        allcodes.add("BN");
        allcodes.add("PT");
        allcodes.add("PR");
        allcodes.add("TS");
        allcodes.add("EX");
        allcodes.add("PS");
        allcodes.add("SS");
        allcodes.add("NC");
        allcodes.add("NS");
        allcodes.add("N0");
        allcodes.add("N1");
        allcodes.add("N2");

        boolean bNextvariant = false; String sNextvariant = "";
        boolean bNextproduct = false; String sNextproduct = "";
        boolean bNextbrand = false; String sNextbrand = "";

        try {
            while ( (record=dis.readLine()) != null ) {
                String code = record.substring(0, 2);
                String text = record.substring(3);

                if (bNextvariant) {
                    if (text.startsWith("_Acquisition")) { addNode(sNextvariant, text, acquisition.get("NODEID"), 5); }
                    else if (text.startsWith("_Merger")) { addNode(sNextvariant, text, merger.get("NODEID"), 5); }
                    else if (text.startsWith("_Name Change")) { addNode(sNextvariant, text, nchange.get("NODEID"), 5); }
                    else if (text.startsWith("_Does Business as Name")) { addNode(sNextvariant, text, dbas.get("NODEID"), 5); }
                    else if (text.startsWith("_Legal Name")) { addNode(sNextvariant, text, lname.get("NODEID"), 5); }
                    else if (text.startsWith("_Inverted Name")) { addNode(sNextvariant, text, iname.get("NODEID"), 5); }
                    else if (text.startsWith("_Predicast Name")) { addNode(sNextvariant, text, pname.get("NODEID"), 5); }

                    bNextvariant = false; sNextvariant = "";
                }

                if (bNextbrand) {
                    Node brandtype = null;
                    if (htBrands.containsKey(text)) { brandtype = (Node) htBrands.get(text); }
                    else { brandtype = addNode(sNextbrand, text, brands.get("NODEID"), 4); htBrands.put(text, brandtype); }

                    addNode(sNextbrand, text, brandtype.get("NODEID"), 5);

                    bNextbrand = false; sNextbrand = "";
                }

                if (bNextproduct) {
                    Node producttype = null;
                    if (htProducts.containsKey(text)) { producttype = (Node) htProducts.get(text); }
                    else { producttype = addNode(sNextproduct, text, products.get("NODEID"), 4); htProducts.put(text, producttype); }

                    addNode(sNextproduct, text, producttype.get("NODEID"), 5);

                    bNextproduct = false; sNextproduct = "";
                }

                int index = allcodes.indexOf(code.toUpperCase());

                switch (index) {
                    case 0:  addCompany(text); break;
                    case 1:  sNextvariant = text; bNextvariant = true; break;
                    case 2:  addNode(text, text, domains.get("NODEID"), 4); break;
                    case 3:  addNode(text, text, domains.get("NODEID"), 4); break;
                    case 4:  break;
                    case 5:  sNextbrand = text; bNextbrand = true; break;
                    case 6:  break;
                    case 7:  sNextproduct = text; bNextproduct = true; break;
                    case 8:  addNode(text, text, ticker.get("NODEID"), 4); break;
                    case 9:  addNode(text, text, ticker.get("NODEID"), 4); break;
                    case 10:  addNode(text, text, SIC.get("NODEID"), 4); break;
                    case 11:  addNode(text, text, SIC.get("NODEID"), 4); break;
                    case 12:  addNode(text, text, NAISC.get("NODEID"), 4); break;
                    case 13:  addNode(text, text, NAISC.get("NODEID"), 4); break;
                    case 14:  addEmployee(text); break;
                    case 15:  addEmployee(text); break;
                    case 16:  addEmployee(text); break;
                    default: break;
                }

            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }
    }

    public static void addEmployee(String text) {
        if (bNextemployee) { addNode(text, text, employee.get("NODEID"), 4); bNextemployee = false; }
        else { bNextemployee = true; }
    }

    public static void addCompany(String Name) {
        Node parent = (Node) nodes.get(Name.substring(0, 1).toUpperCase());
        if (parent == null) parent = (Node) nodes.get("0");
        company = addNode(Name, Name, parent.get("NODEID"), 2);

        variants = addNode("Variant Names", "Variant Names", company.get("NODEID"), 3);
        domains = addNode("Web Domain", "Web Domain", company.get("NODEID"), 3);
        brands = addNode("Brands", "Brands", company.get("NODEID"), 3);

        subsidiaries = addNode("Subsidiaries", "Subsidiaries", company.get("NODEID"), 3);
        products = addNode("Products", "Products", company.get("NODEID"), 3);
        ticker = addNode("Ticker", "Ticker", company.get("NODEID"), 3);

        employee = addNode("Key Employees", "Key Employees", company.get("NODEID"), 3);
        SIC = addNode("SIC", "SIC", company.get("NODEID"), 3);
        NAISC = addNode("NAISC", "NAISC", company.get("NODEID"), 3);

        acquisition = addNode("Acquisition", "Variant Type", variants.get("NODEID"), 4);
        merger = addNode("Merger", "Merger", variants.get("NODEID"), 4);
        nchange = addNode("Name Change", "Name Change", variants.get("NODEID"), 4);
        dbas = addNode("Does Business As", "Does Business As", variants.get("NODEID"), 4);
        lname = addNode("Legal Name", "Legal Name", variants.get("NODEID"), 4);
        iname = addNode("Inverted Name", "Inverted Name", variants.get("NODEID"), 4);
        pname = addNode("Predicast Name", "Predicast Name", variants.get("NODEID"), 4);

        htProducts.clear(); htBrands.clear();
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}