// GetCorpora
//
// Log into the ProcinQ server and list all taxonomies in the system

import com.iw.system.*;
import com.iw.tools.*;

import java.util.*;
import java.io.*;

public class GetCorpora {
    public static Hashtable htArguments = new Hashtable(); // hashtable arguments

    public static void main(String args[]) throws Exception {
        System.out.println("This program will get the list of available taxonomies and print the results.\n");
        htArguments = com.iw.tools.GetArguments.getArgHash(args);

        if (!htArguments.containsKey("server")) {
            System.err.println("Usage: GetCorpora -server <API>"); return;
        }

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArguments.get("server"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        System.out.println(" ");

        Vector taxonomyList = server.getCorpora();
        for (int i = 0; i < taxonomyList.size(); i++) {
            Corpus c = (Corpus) taxonomyList.elementAt(i);
            System.out.println("Taxonomy Name: "+c.getName()+" ID: "+c.getID());
        }

   }
}