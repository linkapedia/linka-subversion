import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class Tageo {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    private static String[] sTitles = new String[10];
    private static StringBuffer[] sbData = new StringBuffer[10];
    private static Node[] nodes = new Node[10];
    public static int level = -1;

    public static boolean go = false;

    public static void main(String args[]) throws Exception {
        File dir = new File("C:/Documents and Settings/indraweb/projects/perlscripts/cities");
        String files[] = dir.list();

        System.out.println("Files: "+files.length);

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        for (int i = 0; i < files.length; i++) {
            File f = new File("C:/Documents and Settings/indraweb/projects/perlscripts/cities/"+files[i]);
            if (files[i].endsWith("out")) {
                System.out.println("Build from: " + f.getAbsolutePath());
                createTaxonomy(f);
            }
        }
    }

    public static void createTaxonomy(File f) throws Exception {
        String sCorpusName = "(COTW) "+f.getName().substring(0, f.getName().length() - 4);;
        System.out.println("Corpus: "+sCorpusName);

        if (sCorpusName.equals("(COTW) Netherlands")) { go = true; return; }
        if (!go) return;

        c = server.addCorpus(sCorpusName, sCorpusName);
        Node root = server.getCorpusRoot(c.getID());

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));
        Node hierarchy[] = new Node[4];
        hierarchy[0] = root;

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) {
                String args[] = record.split("\t");
                String URL = args[0];
                String title = args[1];
                int depth = Integer.parseInt(args[2]);

                hierarchy [depth] = addNode(title, URL, hierarchy[depth-1].get("NODEID"), depth);
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}