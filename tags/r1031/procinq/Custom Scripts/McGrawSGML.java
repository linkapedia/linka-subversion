import com.iw.system.*;

import java.util.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.xml.sax.InputSource;

public class McGrawSGML {

    public static Hashtable htNodes = new Hashtable();
    public static Corpus c = null;
    public static ProcinQ server = null;

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server = new ProcinQ();
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("its", "its");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        //c = server.addCorpus("Dictionary of Scientific and Technical Terms", "Dictionary of Scientific and Technical Terms");

        c = server.getCorpora("2297");
        Node root = server.getCorpusRoot(c.getID());

        Vector nodez = server.CQL("SELECT <NODE> where CORPUSID = 2297 AND DEPTHFROMROOT = 1", 1, 30000);
        for (int i = 0; i < nodez.size(); i++) {
            Node n = (Node) nodez.elementAt(i);
            htNodes.put(n.get("NODETITLE"), n);
        }

        // now loop through each item in the
        File f = new File("C:/Documents and Settings/indraweb/Desktop/TAX-DEV/Dictionary of Scientific and Technical Terms/DSTT6SGML.xml");
        FileInputStream fis = new FileInputStream(f);
        SAXReader xmlReader = new SAXReader(false);
        xmlReader.setValidation(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        System.out.println("Reading document from stream...");
        org.dom4j.Document doc = xmlReader.read(ins);
        System.out.println("Done.");

        Element eTopics = doc.getRootElement();

        List list = eTopics.elements("ENTRY");
        Iterator i = list.iterator();
        boolean insert = false;

        while (i.hasNext()) {
            Element eEntry = (Element) i.next();
            Element eTerm = (Element) eEntry.element("TERM");
            Element eBody = (Element) eEntry.element("BODY");
            System.out.println("Term: " + eTerm.getText());

            if (insert) {
                List cats = eBody.elements("CATGRP");
                Iterator j = cats.iterator();

                while (j.hasNext()) {
                    Element eCatGrp = (Element) j.next();
                    Element eCat = (Element) eCatGrp.element("CAT");
                    Element eDef = (Element) eCatGrp.element("DEF");
                    Element eXref = (Element) eCatGrp.element("XREFGRP");

                    if (eXref != null) eXref = (Element) eXref.element("XREF");

                    System.out.print("Category: " + eCat.getText());
                    Node category = null;
                    if (htNodes.containsKey(eCat.getText()))
                        category = (Node) htNodes.get(eCat.getText());
                    else
                        category = addNode(eCat.getText(), root.get("NODEID"), 1, null);

                    htNodes.put(eCat.getText(), category);

                    Node term = addNode(eTerm.getText(), category.get("NODEID"), 2, null);
                    String source = "";

                    if (eDef != null) {
                        source = eDef.getText();
                        try {
                            server.setNodeSource(term, source);
                        } catch (Exception e) {
                            System.err.println("Could not set the source for this node.");
                        }
                    }
                    if (eXref != null) {
                        source = eXref.getText();
                        try {
                            server.setNodeSource(term, source);
                            server.addMustHaveTerm(term, source);
                        } catch (Exception e) {
                            System.err.println("Could not set the source for this node.");
                        }
                    }

                    System.out.println("Source: " + source);
                }

                System.out.println(" ");
            }

            if (eTerm.getText().equals("Testacellidae")) insert = true;
        }
    }

    public static Node addShellNode(NodeInsert ni) {
        return addNode(ni.name, ni.parent, ni.depth, null);
    }

    public static Node addNode(NodeInsert ni) {
        return addNode(ni.name, ni.parent, ni.depth, ni.link);
    }

    public static Node addNode(String sNodeTitle, String sParent, int Depth, String link) {
        if (sNodeTitle.trim().equals(""))
            System.out.println("Blank Title");

        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        if (link != null) n.set("LINKNODEID", link);

        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            if (link == null) n = server.addNode(n); else n = server.addLinkNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;
    }

    // copy this node recursively
    public static Node copyNode(NodeInsert ni) {
        Node n = null;

        System.out.println("Copy node: " + ni.name + " Parent: " + ni.parent);
        try {
            n = server.copyNode(ni);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            System.err.println("Could not copy node: " + ni.name);
        }

        return n;

    }
}