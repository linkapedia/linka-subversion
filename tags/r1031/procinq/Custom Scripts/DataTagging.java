// DataTagging
//
// Given a directory of files, classify them against the given corpora and write the results into the file

import com.iw.system.*;

import java.util.*;
import java.io.*;

public class DataTagging {
    private static String inDir = "";
    private static String outDir = "";

    public static Hashtable htArguments = new Hashtable(); // hashtable arguments

    public static void main(String args[]) throws Exception {
        System.out.println("This program will demonstrate a data tagging example in the ProcinQ Server.\n");

        if ((!htArguments.containsKey("base")) || (!htArguments.containsKey("server"))) {
            System.err.println("Usage: DataTagging -server <API> -base <DIR>"); return;
        }

        inDir = (String) htArguments.get("base")+"/classification/data-tagging/in/";
        outDir = (String) htArguments.get("base")+"/classification/data-tagging/out/";

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArguments.get("server"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        // Step 3. Read all the files from the directory INDIR
        System.out.println("**** RETRIEVING THE LIST OF FILES TO CLASSIFY ****");
        System.out.println(" ");

        File fDirectory = new File(inDir);
		String FilePaths[] = fDirectory.list();

        System.out.println("**** BEGIN CLASSIFICATION  ****");
        System.out.println(" ");

        for (int i = 0; i < FilePaths.length; i++) {
            File f = new File(inDir+"/"+FilePaths[i]);

            Vector classifyResults = new Vector(); // classification results will be stored here
            StringBuffer sb = new StringBuffer(); // data from the file will be stored here

            // Step 4. Classify each file in the directory
            System.out.println("Classify: "+f.getAbsolutePath());
            DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

            String record = null;
            try {
                while ( (record=dis.readLine()) != null ) { sb.append(record); }
            } catch (IOException e) {
                System.err.println("There was an error while reading the file "+f.getAbsolutePath());
                return;
            }

            // Classify the document.  The server expects a string and a corpusID list.  If more than one taxonomy
            // is specified, the corpusIDs in the second argument should be separated by commas with no spaces.
            try { classifyResults = server.classify(sb.toString(), "58,70,71,72"); }
            catch (SessionExpired se) {
                System.err.println("Session expired."); return; }
            catch (ClassifyError ce) {
                System.err.println("Classification failed.");
                ce.printStackTrace(System.err); return;
            }
            // Step 5. If a document classified into 1 or more topics, write the file to OUTDIR
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outDir+"/"+FilePaths[i])));
            if (classifyResults.size() > 0) {
                System.out.println("Success: "+f.getAbsolutePath());

                try {

                    out.println("<TOPICRESULTS>");

                    for (int j = 0; j < classifyResults.size(); j++) {
                        NodeDocument nd = (NodeDocument) classifyResults.elementAt(j);

                        // Note: the attributes of the ND object are dynamic.  See API for a base set of attributes.
                        // Additional attributes can be added simply by adding fields to the database table.   For this
                        // example, only the titles of the topic hits are printed.
                        System.out.println("   Result: "+nd.get("NODETITLE")+" Score: "+nd.get("SCORE1"));

                        // finally, this function will give you the context of the concepts that were returned
                        Vector nodeTree = server.getNodeTree(nd.get("NODEID"));

                        out.print("   <TOPICRESULT>");
                        for (int k = nodeTree.size()-1; k > 0; k--) {
                            if (k != nodeTree.size()-1) out.print(" > ");

                            Node n = (Node) nodeTree.elementAt(k);
                            out.print(n.get("NODETITLE"));
                        }

                        out.println(" > "+nd.get("NODETITLE")+"</TOPICRESULT>");
                    }

                    out.println("</TOPICRESULTS>");

                } catch (IOException ioe) {
                    System.err.println("Error: I/O exception writing to: "+outDir+"/"+FilePaths[i]);

                    throw ioe;
                }
            }
            BufferedReader in = new BufferedReader(new FileReader(f));

            String line;
            while ((line = in.readLine()) != null) { out.println(line); }

            out.flush(); out.close();

        }

        System.out.println("DataTagging complete.\n");
   }
}