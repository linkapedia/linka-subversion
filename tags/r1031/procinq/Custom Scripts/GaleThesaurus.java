// Thesauruss
//
// This file contains a series of Thesauruss that may be used to access the ProcinQ Server API.

import com.iw.system.*;

import java.util.*;
import java.io.*;

public class GaleThesaurus {

    public static com.iw.system.Thesaurus t = null;

    public static void main(String args[]) throws Exception {
        System.out.println("Import Gale Thesaurus..\n");

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI("192.168.0.223:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("schering", "schering");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        System.out.println("**** Thesaurus ingestion ****");
        File f = new File("thes2.txt");
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;
        String relationship = "";
        String CT = "";

        t = server.createThesaurus("Gale Drugs");

        // for each record,
        try {
            while ((record = dis.readLine()) != null) {
                if ((!record.trim().equals("")) && (record.trim().length() > 3)) {
                    if (!record.substring(1, 4).trim().equals("")) {
                        relationship = record.substring(1, 4).trim();
                    } else { relationship = ""; }
                    if (!record.substring(0, 1).equals(" ")) {
                        CT = record;
                    } else {
                        if ((relationship.equals("BT")) || (relationship.equals("UF")) || (relationship.equals("NT")))
                            addRelationship(server, relationship, CT, record.substring(4));
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file " + f.getAbsolutePath());
            return;
        }
    }

    private static void addRelationship(ProcinQ server, String relat, String base, String predicate) {
        System.out.println("Relationship: " + relat + " Word: " + base + " Predicate: "+predicate);

        String relationship = "0";
        if (relat.equals("UF")) { relationship = "1"; }
        else if (relat.equals("BT")) { relationship = "2"; }
        else if (relat.equals("NT")) { relationship = "3"; }

        try { server.addRelationship(t.getID(), base, predicate, relationship); }
        catch (Exception e) { e.printStackTrace(System.err); }
    }
}