// BritArt
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.xml.sax.InputSource;

public class BritStudentNew {
    public static Hashtable nodes = new Hashtable();
    public static Hashtable unassigned = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;
    private static boolean insert = false;

    public static void main(String args[]) throws Exception {
        String _start = "C:/Documents and Settings/indraweb/Desktop/TAX-DEV/student/ebi/xml_articles/";

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        System.out.println("Loading nodes in corpus 2309...");
        c = server.getCorpora("2309");
        Node root = server.getCorpusRoot(c.getID());

        Vector vNodes = server.CQL("select <node> where corpusid = 2309", 1, 5000);
        for (int i = 0; i < vNodes.size(); i++) {
            Node n = (Node) vNodes.elementAt(i);
            int nodedesc = 9000000;
            try {
                nodedesc = Integer.parseInt(n.get("NODEDESC"));
            } catch (Exception e) {
            }
            nodedesc = nodedesc - 9000000;

            nodes.put(nodedesc + "", n);
        }
        vNodes = null;

        vNodes = server.CQL("select <node> where parentid = 17090237");
        for (int i = 0; i < vNodes.size(); i++) {
            Node n = (Node) vNodes.elementAt(i);
            unassigned.put(n.get("NODETITLE"), n);
        }
        vNodes = null;

        System.out.println("Getting all files in the directory ...");

        // step 2: loop for every file in the brit art directory and assign node information
        File fDirectory = new File(_start);
        String FilePaths[] = fDirectory.list();
        int notfound = 0;
        int found = 0;

        System.out.println("Looping...");

        for (int j = 0; j < FilePaths.length; j++) {
            File f = new File(_start + FilePaths[j]);

            if (FilePaths[j].endsWith(".xml")) {
                FileInputStream fis = new FileInputStream(f);
                SAXReader xmlReader = new SAXReader(false);
                xmlReader.setValidation(false);

                InputSource ins = new InputSource(fis);
                ins.setEncoding("UTF-8");
                ins.setSystemId(f.toURL().toString());

                org.dom4j.Document doc = xmlReader.read(ins);

                Element eArticle = doc.getRootElement();
                Element eTitle = eArticle.element("title");
                String ID = eArticle.attribute("artclid").getValue();
                String title = eTitle.getStringValue();

                if (insert) {
                    Node n = (Node) nodes.get(ID.trim());

                    if (n == null) {
                        notfound++;
                        Node parent = (Node) unassigned.get("Other");
                        if (unassigned.containsKey(title.substring(0, 1).toUpperCase())) {
                            parent = (Node) unassigned.get(title.substring(0, 1).toUpperCase());
                        }

                        n = addNode(title, parent.get("NODEID"), "9" + ID, 3);
                    } else { System.out.println("Old node: "+n.get("NODETITLE")+" Parent: "+n.get("PARENTID")+" Depth: "+n.get("DEPTHFROMROOT")); }

                    process(n, eArticle, server);

                }
                if (title.equals("Hodgkin, Dorothy Crowfoot")) insert = true;
            }

        }

        /*
        c = server.addCorpus("Britannica Student (10/2/2006)", "Britannica Student (10/2/2006)");
        Node root = server.getCorpusRoot(c.getID());
        root.set("DEPTHFROMROOT", "0");
        nodes.put("0", root);

        // step 1, create the category hierarchy
        String record = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("C:/Documents and Settings/indraweb/Desktop/TAX-DEV/student/brit.txt"));
            while ((record = br.readLine()) != null) {
                // ex Node: 0 Parent: -1 URL: http://www.britannica.com/ebi/subject Title: Britannica Student Encyclopedia Article:
                // ex Node: 2209 Parent: 0 URL: http://www.britannica.com/ebi/subject?id=2209&subject=Sports Title: Sports Article: None
                // ex Node: 300061 Parent: 2209 URL: None Title: Zaharias, Babe Article: /ebi/article?eu=300061
                Pattern p = Pattern.compile("Node: (.*?) Parent: (.*?) Title: (.*?) Article: (.*)", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(record); boolean bResult = m.find();
                if (bResult) {
                    Node parent = (Node) nodes.get(m.group(2));
                    int depth = Integer.parseInt(parent.get("DEPTHFROMROOT"));
                    Node n = addNode(m.group(3), parent.get("NODEID"), m.group(4), (depth+1));
                    nodes.put(m.group(1), n);

                    System.out.print("Node: "+m.group(1));
                    System.out.print(" Parent: "+m.group(2));
                    System.out.print(" Title: "+m.group(3));
                    System.out.println(" Article: "+m.group(4));
                } else {
                    System.err.println("NO MATCH FOR: "+record);
                }
            }

            br.close();
        } catch (IOException e) {
            System.err.println("IO Exception.");
            e.printStackTrace(System.err); return;
        }
        */

    }

    public static String getSource(Element e) {
        StringBuffer sb = new StringBuffer("");

        List source = e.elements("p");
        if (source != null) {
            Iterator i = source.iterator();

            while (i.hasNext()) {
                Element p = (Element) i.next();
                sb.append(p.getStringValue()+"\r\n");
            }
        }

        return sb.toString();
    }

    public static void process (Node node, Element e, ProcinQ its) {
        // now add source for this node
        StringBuffer sb = new StringBuffer();

        if (e.element("firstpar") != null)
            sb.append(e.element("firstpar").getStringValue());

        sb.append(getSource(e));
        try { server.setNodeSource(node, sb.toString()); }
        catch (Exception ex) {
            System.err.println("Could not set source for node "+node.get("NODETITLE")+" length: "+sb.toString().length());
            ex.printStackTrace(System.err);
        }
        sb = null;

        List h1 = e.elements("h1"); if (h1 != null) processInternal(h1, node, its);
        List h2 = e.elements("h2"); if (h2 != null) processInternal(h2, node, its);
        List h3 = e.elements("h3"); if (h3 != null) processInternal(h3, node, its);
        List h4 = e.elements("h4"); if (h4 != null) processInternal(h4, node, its);
        List h5 = e.elements("h5"); if (h5 != null) processInternal(h5, node, its);
        List h6 = e.elements("h6"); if (h6 != null) processInternal(h6, node, its);
    }

    public static void processInternal (List h1, Node parent, ProcinQ its) {
        int depth = Integer.parseInt(parent.get("DEPTHFROMROOT"));

        if (h1 != null) {
            Iterator i1 = h1.iterator();
            while (i1.hasNext()) {
                Element eh = (Element) i1.next();
                Element eTitle = eh.element("headtitle");
                String ID = "9"+eh.attribute("tocid").getValue();
                String title = eTitle.getStringValue();

                Node n = addNode(title, parent.get("NODEID"), ID, (depth+1));

                process(n, eh, its);
            }
        }
    }

    public static Node addNode(String sNodeTitle, String sParent, String sNodeDesc, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent + " Depth: "+Depth);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}