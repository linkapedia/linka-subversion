/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Dec 29, 2005
 * Time: 11:24:11 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

import com.iw.system.Node;
import com.iw.system.ProcinQ;
import com.iw.system.Corpus;

import java.io.*;
import java.net.*;
import java.util.Hashtable;
import java.util.regex.*;

public class Europa {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    public static void main(String args[]) throws Exception {

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        c = server.addCorpus("Europa Glossary", "Europa");
        Node root = server.getCorpusRoot(c.getID());

        Node nodes[] = new Node[3];
        nodes[0] = root; int level = 1;

        File f = new File("C:/Documents and Settings/indraweb/projects/perlscripts/europa/europa.txt");
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;
        try {
            while ((record = dis.readLine()) != null) {
                Pattern p = Pattern.compile("<A NAME=\"(.*?)\">", Pattern.CASE_INSENSITIVE);
                Pattern p2 = Pattern.compile("<A HREF=\"(.*?)\">(.*?)</A>", Pattern.CASE_INSENSITIVE);

                Matcher m = p.matcher(record); boolean bResult = m.find();
                if (bResult) {
                    nodes[1] = addNode(m.group(1), m.group(1), nodes[0].get("NODEID"), 1, c);
                    System.out.println("Title: "+m.group(1)+" Depth: 1");
                }

                Matcher m2 = p2.matcher(record); boolean bResult2 = m2.find();
                if (bResult2) {
                    nodes[2] = addNode(m2.group(2), m2.group(2), nodes[1].get("NODEID"), 2, c);
                    System.out.println("Title: "+m2.group(2)+" URL: "+m2.group(1)+" Depth: 2");
                    crawlURL(m2.group(1), server, nodes[2]);
                }
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file " + f.getAbsolutePath());
            return;
        }

    }

    public static void crawlURL (String URL, ProcinQ server, Node n) {
        URL = "http://europa.eu.int/scadplus/glossary/"+URL;

        System.out.println("Opening .. "+URL);

        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");
        boolean bPDFDocument = false;
        org.dom4j.Document doc = null;
        boolean read = true;

        try {
            URL myURL = new URL(URL);

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            DataInputStream dis = new DataInputStream(new BufferedInputStream(httpCon.getInputStream()));

            String record = null; StringBuffer sb = new StringBuffer(); boolean bWrite = false;
            try {
                while ((record = dis.readLine()) != null) {
                    if (record.startsWith("<P><B>"))bWrite = true;
                    if ((record.startsWith("<HR>")) || (record.startsWith("<P>See:</P>"))) bWrite = false;
                    if (bWrite) sb.append(record);
                }
            } catch (IOException e) {
                System.err.println("There was an error while reading the URL " + URL);
                return;
            }

            String s = sb.toString();
            s = s.replaceAll("<(.*?)>", " ");

            System.out.println(s);
            server.setNodeSource(n, s);
        } catch (Exception e) { e.printStackTrace(System.err); return; }
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth, Corpus c) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }
}
