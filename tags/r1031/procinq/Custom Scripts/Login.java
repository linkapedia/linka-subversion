// Login
//
// Log into the ProcinQ Server

import com.iw.system.*;
import com.iw.tools.*;

import java.util.*;
import java.io.*;

public class Login {
    public static Hashtable htArguments = new Hashtable(); // hashtable arguments

    public static void main(String args[]) throws Exception {
        System.out.println("This program will create a session on the ProcinQ server.\n");
        htArguments = com.iw.tools.GetArguments.getArgHash(args);

        if (!htArguments.containsKey("server")) {
            System.err.println("Usage: Login -server <API>"); return;
        }

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArguments.get("server"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");
   }
}