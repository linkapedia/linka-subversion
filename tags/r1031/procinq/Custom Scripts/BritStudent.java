// Gale
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.net.*;

import org.xml.sax.InputSource;
import org.dom4j.io.SAXReader;
import org.dom4j.Element;

public class BritStudent {
    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;
    private static Hashtable ht = new Hashtable();

    public static void main(String args[]) throws Exception {

        // Step 1. Initialize the server object
        server.setAPI("192.168.0.223:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        String sCorpusName = "Britannica Student";
        System.out.println("Corpus: "+sCorpusName);

        c = server.addCorpus(sCorpusName, sCorpusName);
        Node root = server.getCorpusRoot(c.getID());
        String start = "C:/Documents and Settings/indraweb/projects/brit/student";

        recurse(start, 1, root);
    }

    public static void recurse(String _start, int depth, Node n) throws Exception {
        File fDirectory = new File(_start);
		String FilePaths[] = fDirectory.list();

        if (FilePaths != null) {
            for (int i = 0; i < FilePaths.length; i++) {
                if (FilePaths[i].endsWith(".txt")) {
                    // do nothing
                } else if (FilePaths[i].endsWith(".html")) {
                    // this is a source document, load it, load url and process
                    String source = HTMLtoTXT(new File(_start + "/" + FilePaths[i]));

                    server.setNodeSource(n, source);

                } else {
                    // this is a directory, create the node
                    Node n2 = addNode(FilePaths[i], n.get("NODEID"), depth);
                    recurse((_start + "/" + FilePaths[i]), (depth+1), n2);
                }
            }
        }
    }

    public static String HTMLtoTXT(File HTMLfile) throws Exception {
        File tempFile = new File("temp.txt");
        if (tempFile.exists()) tempFile.delete();

        FileReader in = new FileReader(HTMLfile);

        HTMLtoTXT parser = new HTMLtoTXT();
        parser.parse(in);
        in.close();

        String s = parser.getText(); StringBuffer sb = new StringBuffer();
        char[] all = s.toCharArray();

        for (int i = 0; i < all.length; i++) {
            if ((((int) all[i]) > 122) || (((int) all[i]) < 10))
            { all[i] = ' '; }
            sb.append(all[i]);
        }

        if (sb.toString().indexOf("var mm") > -1)
            return sb.toString().substring(0, sb.toString().indexOf("var mm"));

        return sb.toString();
    }


    public static void writeSource(String _url, Node _n) throws Exception {
        _n.set("NODEDESC", _url);
        server.editNodeProps(_n);

        if (true) return;

        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");

        try {
            URL myURL = new URL(_url);

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true);
            httpCon.setDoOutput(true);
            httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening " + myURL.getUserInfo() + " : " + httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }
        } catch (Exception e) {
        }
    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh) && !(prevCh != (char) 39))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static Node addNode (String sNodeTitle, String sParent, int Depth) throws Exception {
        return addNode(sNodeTitle, sParent, Depth, null); }
    public static Node addNode (String sNodeTitle, String sParent, int Depth, Node link) throws Exception {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth+"");
        if (link != null) n.set("LINKNODEID", link.get("NODEID"));
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try {
            if (link != null) { n = server.addLinkNode(n); }
            else { n = server.addNode(n); }}
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); throw e; }

        return n;

    }
}