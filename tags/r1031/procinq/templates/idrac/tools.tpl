<SCRIPT>
function Purge() {
    if (confirm ('All objects marked for deletion will be unrecoverable after a system purge.  Continue?')) {
       window.location='/servlet/Main?template=Server.Purge';
    }
}
</SCRIPT>
  <BLOCKQUOTE>
    <p><B>Tools</B></p>
    <table width="76%" border="0">
      <tr> 
        <td width="18%" valign="top"><b><a href="javascript:Purge();">Purge Objects</a></b></td>
        <td width="73%" valign="top">Purge objects allows you to permanently delete 
          objects from the system. Some object like Corpora are not completely 
          deleted by the API, but rather marked for deletion. The Purge Objects 
          function completely removes them from the database to free up the space.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Server.Classify"><b>Classify</b></a></td>
        <td width="73%" valign="top">Select a document from a local server to classify into one or
	   more corpora.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Server.CorpusIngest"><b>Ingest Corpus</b></a></td>
        <td width="73%" valign="top">Ingest a corpus that exists in corpus normal form.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.GetRelatedNodes"><b>Related Nodes</b></a></td>
        <td width="73%" valign="top">Find all topics on this server containing a specific document.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.Synch">Synchronize with Knowledge Harvester</a></b></td>
        <td width="73%" valign="top">If you are a subscriber to the Knowledge 
          Harvester and are creating your own corpora or a modifying corpora, 
          then after a session of modifying nodes you need to communicate these 
          changes to the central knowledge harvester database. This allows the 
          Knowledge Harvester to look for information on the new nodes.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
    </table>
    </BLOCKQUOTE>