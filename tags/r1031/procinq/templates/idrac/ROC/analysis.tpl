<blockquote>
<table width=700><tr><td>

<b><u>Instructions</b></u>: This procedure will output data files 
corresponding to document analysis work already completed.   Note,
if you have not yet done document analysis, or if your document 
analysis work is incomplete for this corpus, the data files that 
are generated may not be accurate.
<P>
Data files output from this procedure should be run through the ROC
analysis tool provided separately.   The resulting ROC curves can then
be input directly into the system using the <b>Edit Corpus</b> functionality.
<P>
Please enter the directory where the data files will be written below.
<br>
<form method=get action=/servlet/Main>
<input type="hidden" name="template" value="ROC.Analysis">
<input type="hidden" name="CorpusID" value="##CORPUSID##">
<input type="text" name="DirectoryPath" size=60 maxlength=300>
<input type="submit" name="submit" value="Submit">
</form>
</td></tr></table>
</blockquote>

