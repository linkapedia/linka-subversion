<BLOCKQUOTE>
    <TABLE cellPadding=2 width="78%" border=0>
      <TBODY> 
      <TR> 
        <TD vAlign=top width="50%"> 
          <P><B>Node Help:</B></P>
          <P>This screen allows you to manipulate the nodes that are in the system. 
            Each node has the following properties:</P>
          <table width="80%" border="1">
            <tr> 
              <td>Parent Tree</td>
              <td>This is not editable. It shows the hierarchy between the current 
                node and the top of the corpus</td>
            </tr>
            <tr> 
              <td>Node Title</td>
              <td>This is the name of the node in the gui of the system.</td>
            </tr>
            <tr> 
              <td>Signatures</td>
              <td>This is a list of the concept signatures in the sytem. Leaf 
                nodes are typically the only nodes with signatures in the system</td>
            </tr>
            <tr> 
              <td>Subtopics</td>
              <td>This is an ordered list of subtopics for the current node. Leaf 
                nodes do not have subtopics</td>
            </tr>
          </table>
          <p><b>Edit Signatures:</b></p>
          <p>This function allows you to manipulate the signatures for a given 
            node. Each node has 0 to n signature terms associated with it. Signatures 
            are words or phrases that help to distiguish one node from another. 
            If you are installing a corpus from one of our topic map files, each 
            leaf node has a collection of signature terms.</p>
          <p><b>Move up / Move Down:</b></p>
          <p>This function allows you to change the order of children nodes for 
            the selected node.</p>
          <p><b>Add Node:</b></p>
          <p><b>Move Tree:</b></p>
          <p>This function allows you to move whole section of the node tree.</p>
          <p>This function allows you to create a new node in the system. This 
            node will be a child of the current node.</p>
          <p><b>Explore Node:</b></p>
          <p>This function allows you to drill down in the hierarchy of the node 
            tree. The current node will change to the node selected in the child 
            node list.</p>
          <p><b>Save Node:</b></p>
          <p>This function saves the current node properties including the order 
            of the node's children.</p>
          <p><b>Delete Node:</b></p>
          <p>This function deletes the selected child node from the system.</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          </TD>
      </TR>
      <TR>
        <TD vAlign=top width="50%">&nbsp;</TD>
      </TR>
      </TBODY>
    </TABLE>
  </BLOCKQUOTE>
