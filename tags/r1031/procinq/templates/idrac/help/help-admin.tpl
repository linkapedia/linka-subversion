<blockquote>
  <TABLE cellPadding=2 width="700" border=0>
    <TBODY> 
    <TR> 
      <TD vAlign=top width="50%">
      <p><b>Administration Help:</b></p>
      <p>This screen is the administration section of the Taxonomy Server. This allows you to change properties of the server, groups, folders, thesauri and corpora. You may also perform system maintenance functions here like purging deleted objects, synchronizing nodes for knowledge harvesting.</p>
        </TD>
    </TR>
    </TBODY> 
  </TABLE>
</blockquote>