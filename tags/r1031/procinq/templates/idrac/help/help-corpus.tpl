<BLOCKQUOTE>
    <TABLE cellPadding=2 width="78%" border=0>
      <TBODY> 
      <TR> 
        <TD vAlign=top width="50%"> 
          <P><B>Corpus Help:</B></P>
          <P>This screen allows you to manipulate the corpora that are in the 
            system. The screen shows the available corpora that are in the system. 
            From this screen you may perform the following functions:</P>
          <P><b>Browse a selected corpus:</b></P>
          <P>By selecting a corpus in the list and hitting browse, you will be 
            taken into a screen that shows you the node hierarchy of the corpus. 
            From here you may edit, add and delete nodes from the system as well 
            as manipulate concept sinatures.</P>
          <P><b>Add a new corpus:</b></P>
          <p>This function allows you to create a corpus from scratch. You can 
            create and name a corpus and start creating nodes underneath it. This 
            is different from &quot;Import a Corpus from a file&quot;</p>
          <p><b>Import a corpus from a file:</b></p>
          <p>This function allows you to import a corpus from a topic map file. 
            Topic maps are provided by the software vendor and include entire 
            corpora, concept signatures, and links to the knowledge harvester. 
          </p>
          <p><b>Privledges:</b></p>
          <p>This allows you to set security for a given corpus. Security is controlled 
            by groups. Each corpus has a list of groups that is allowed to access 
            and manipulate the corpus. For each group / corpus combination - the 
            following rights are:</p>
          <table width="80%" border="1">
            <tr> 
              <td>Read</td>
              <td>Minimum security -- this allows the user to search through the 
                corpus and see results</td>
            </tr>
            <tr> 
              <td>Write</td>
              <td>This level of security allows the user to change aspects of 
                the corpus itself ( channels, properties, security )</td>
            </tr>
            <tr> 
              <td>None</td>
              <td>The user will not be able to see the corpus</td>
            </tr>
          </table>
          <p><b>Edit the selected corpus</b></p>
          <p>This function allows you to change basic properties of the corpus. 
            These properties are:</p>
          <table width="80%" border="1">
            <tr> 
              <td>Taxonomy Name</td>
              <td>The name of the taxonomy or corpus. This is used in the gui 
                for selecting the corpus to search through</td>
            </tr>
            <tr> 
              <td>Taxonomy Description</td>
              <td>This is the long name of the copus / taxonomy. </td>
            </tr>
            <tr> 
              <td>Taxonomy Available</td>
              <td>This determines weather this taxonomy / corpus is displayed 
                in the gui of the system. It is typically false for taxonomies 
                that are under development.</td>
            </tr>
            <tr> 
              <td>Folder Selection</td>
              <td>This allows you to choose which folders are visible in the gui 
                for a given corpus.</td>
            </tr>
          </table>
          <p><b>Delete the selected corpus</b></p>
          <p>This function allows you to remove the corpus from the sytem. Since 
            this is a large transaction, the corpus is only marked for deletion 
            and may be recovered by the system administrator prior to running 
            purge objects.</p>
          <p>&nbsp;</p>
          </TD>
      </TR>
      <TR>
        <TD vAlign=top width="50%">&nbsp;</TD>
      </TR>
      </TBODY>
    </TABLE>
  </BLOCKQUOTE>
