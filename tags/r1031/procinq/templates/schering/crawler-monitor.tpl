  <BLOCKQUOTE>
    <table width="76%" border="0">
      <br>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=NodesRun24Hrs"><b>Nodes 24hrs</b></a></td>
        <td width="73%" valign="top">List the nodes, corpora, and crawler for the last day. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <br>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=NodesPerMachineLast24Hours"><b>Nodes per mach. 24hrs</b></a></td>
        <td width="73%" valign="top">Count the nodes processed per machine and completion code in the last 24 hours. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <p>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=NodesRun1Hr"><b>Nodes 1hr</b></a></td>
        <td width="73%" valign="top">List the nodes, corpora, and crawler for the last hour. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <p>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=NodesPerMachineLast1Hr"><b>Nodes per mach. 1hr</b></a></td>
        <td width="73%" valign="top">Count the nodes processed per machine and completion code in the last hour.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <p>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=ClearData"><b>Clear data</b></a></td>
        <td width="73%" valign="top">Delete crawler process history data from the database.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <p>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=ReloadNCQData"><b>Reload Q</b></a></td>
        <td width="73%" valign="top">Reload a pre-assigned group of corpora into the node crawl queue for processing.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <p>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=ListCrawlers"><b>List Crawlers</b></a></td>
        <td width="73%" valign="top">List the set of machines, active status, and threads per machine.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <p>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.monitor.ViewProcessDataFromSQL&query=ListSearchEngineSources"><b>List Sources</b></a></td>
        <td width="73%" valign="top">List the set of search engines and their active status.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
    </table>
    </BLOCKQUOTE>