<blockquote>
<table width=700><tr><td>
<b><u>Instructions:</u></b> This feature allows you to add the topic <b>##NODETITLE##</b>
to the ROC topic validation list.  Documents from this topic list will then be 
user validated in order to adjust the ROC cut off points.
<P>
To continue submission of this topic to the ROC topic validation list, please enter
the number of documents from this topic to be processed at validation time.
<br>
<form action="/servlet/Main">
<input type="hidden" name="template" value="ROC.Chooser">
<input type="hidden" name="NodeID" value="##NODEID##">
Number of Documents: <input type="text" name="Count" value="100" size=5>
<input type="submit" name="submit" value="Continue">
</form>
</td></tr></table>
</blockquote>
