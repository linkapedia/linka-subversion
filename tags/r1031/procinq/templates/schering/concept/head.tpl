<script>
var curRow = null;
var oldColor = "#FFFFFF";
var documentID = null;

function highlight(e, newRow, docID){
  if (newRow == curRow) { return; }

  oldColor = newRow.style.backgroundColor;
  documentID = docID;
  newRow.style.backgroundColor="#99BBBB"; newRow.focus(); 
  if (curRow != null) { curRow.style.backgroundColor = oldColor; curRow.focus(); }

  curRow = newRow;
}

function GenerateReport() {
  if (documentID == null) { alert('Please select a narrative before clicking the report link.'); return; }
  var url = "/servlet/Main?template=ConceptAlerts.Report&did="+documentID;
  if (document.menu.genre.options[document.menu.genre.selectedIndex].value != -1) { 
	url = url + "&genreid="+document.menu.genre.options[document.menu.genre.selectedIndex].value; 
  } 

  window.location = url;
}

function NewConcept() {
  window.location = "/servlet/Main?template=ConceptAlerts.NewNarrative";
}

function EditConcept() {
  if (documentID == null) { alert('Please select a narrative before clicking the edit link.'); return; }
  window.location = "/servlet/Main?template=ConceptAlerts.NewNarrative&did="+documentID;
}

function RemoveConcept() {
  if (documentID == null) { alert('Please select a narrative before clicking the delete link.'); return; }
  var ww = 600, wh = 450;
  var wPos = 0;
  var lPos = 0;

  var url = "/servlet/Main?template=ConceptAlerts.RemoveNarrative&did="+documentID;

  if(screen) {
     wPos = (screen.width - ww)/2;
     lPos = (screen.height - wh)/2;
  }
  if (!window.SEwin) {
     SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  } else {
     if (SEwin.closed) {
        SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  }}
  SEwin.focus();  
}
</script>

<blockquote>

<table border=0 width=350 cellpadding=0 cellspacing=0><tr><td>
<form name="menu">
<font size=-1 face="Arial, sans-serif"><a href="javascript:NewConcept();">New</a> &nbsp;
              <a href="javascript:EditConcept();">Edit</a> &nbsp;
              <a href="javascript:RemoveConcept();">Delete</a> &nbsp;
              <u>Reports</u> &nbsp;
</td><td valign=top>
		  <select name="genre">
			<option value="-1">All</option>
			##GENRES##
		  </select>
		  <input type="button" name="Go" Value="Go" onClick="javascript:GenerateReport();">
</font>
</td></tr></table>
</form>
<hr width=650>

<table border=2 width=650 cellpadding=0 cellspacing=0><tr><td>
<table border=0 cellpadding=2 cellspacing=0 width=650>
<tr><td colspan=2 bgcolor="329806" align=center
        valign=top style="font-size:12pt; color: #FFFFFF; font-family: Arial, sans-serif; font-weight: bold">
    Concept Alerts</td></tr>
