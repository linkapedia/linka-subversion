<html>
<head>
<title>##TITLE##</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="data/shared-style.css" type="text/css">

<link rel="stylesheet" href="data/style2.css" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="52">
    <tr><td><img src="/servlet/images/spacer.jpg" width="10" height="10"></td></tr>
    <tr>
      <td valign="top"><img src="/servlet/images/spacer.jpg" width="10" height="10"></td>
      <td width="85%" valign="top" align="left">

			 <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100">
            <img src="/servlet/images/spacer.jpg" width="100" height="1">
          </td>
          <td class="bigtitle" width="95%">
				    <font class="infotitlecolor" size=+1><b>ProcinQ CQL Results</b>&nbsp;</font>
          </td>
          <td width="100"><img src="/servlet/images/spacer.jpg" width="100" height="1"></td>
        </tr>
        <tr><td>
          <img src="/servlet/images/spacer.jpg" width="1" height="1">
        </td></tr>
        </tr>
        <tr valign="top">
          <td width="100">
            <img src="/servlet/images/spacer.jpg" width="100" height="1">
          </td>
          <td class="textresult" width="95%">

				  <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="reportsubtitle" width="33%">
								     <b>User Name:</b>
								</td>
                <td class="white" width="67%">
								  <font class="infocolor">
								    SCHERING
                  </font>
								</td>
              </tr>

       
       
       
              <tr>
                <td width="33%" class="reportsubtitle">
								     <b>Query:</b>
								</td>
                <td width="67%" class="white">
								 <font class="infocolor">
								     ##CQL##
								 </font>
                </td>
              </tr>

       
       
       
              <tr>
                <td width="33%" class="reportsubtitle">
								    <b>Customer:</b>
								</td>
                <td width="67%" class="white">
								 <font class="infocolor">
								 Schering AG
                 </font>
								</td>
              </tr>
       

              <tr>
                <td width="33%"><img src="/servlet/images/spacer.jpg" width="10" height="10"></td>
                <td width="67%"><img src="/servlet/images/spacer.jpg" width="10" height="10"></td>
              </tr>
            </table>
          </td>
          <td width="100" align="center">
            <img src="/servlet/images/itslogo3.gif" border=1>
          </td>
        </tr>
      </table>
    </td>
    <td width="17%"><img src="/servlet/images/spacer.jpg" width="10" height="10"></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="1%"><img src="/servlet/images/spacer.jpg" width="1" height="20"></td>
    <td colspan="3">
      <div align="left">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="btblack" width="25%">
						    <font class="infocolor"><b>Results</b></font>
						</td>
            <td width="75%" align="right">&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="4">
	</td></tr>

