<!-- script to activate queries on questions -->
<script LANGUAGE="JAVASCRIPT">
function question1() {
   var si = document.forms[1].genre1.selectedIndex;

   var query = "SELECT <DOCUMENT> WHERE GENREID = "+ document.forms[1].genre1.options[si].value+" AND "+
	"FULLTEXT LIKE '"+document.forms[1].text1.value+"'";
   window.location = '/servlet/Main?template=Taxonomy.CQL&query='+escape(query);
}

function question2() {
   var query = "SELECT <NODEDOCUMENT> WHERE NODETITLE LIKE '"+ document.forms[1].text2.value+"' AND "+
	"FULLTEXT LIKE '"+document.forms[1].text3.value+"'";
   window.location = '/servlet/Main?template=Taxonomy.CQL&query='+escape(query);
}

function question3() {
   var query = "SELECT <DOCUMENT> WHERE NODETITLE = '"+ document.forms[1].text4.value+"' AND "+
	"NODETITLE = '"+document.forms[1].text5.value+"'";
   window.location = '/servlet/Main?template=Taxonomy.CQL&query='+escape(query);
}

function question4() {
   var si = document.forms[1].genre2.selectedIndex;

   var query = "SELECT <NODEDOCUMENT> WHERE GENREID = "+ document.forms[1].genre2.options[si].value+" AND "+
	"DOCTITLE LIKE '"+document.forms[1].text6.value+"'";
   window.location = '/servlet/Main?template=Taxonomy.CQL&query='+escape(query);
}

function question5() {
   var si = document.forms[1].genre3.selectedIndex;
   var query = "SELECT <DOCUMENT> WHERE GENREID = "+ document.forms[1].genre2.options[si].value+" AND "+
	"DATELASTFOUND > '"+ document.forms[1].text7.value+"' AND "+
	"DATELASTFOUND < '"+document.forms[1].text8.value+"'";
   window.location = '/servlet/Main?template=Taxonomy.CQL&query='+escape(query);
}

function question6() {
   var query = "SELECT <DOCUMENT> WHERE FULLTEXT LIKE '"+document.forms[1].text9.value+"' AND "+
      "DATELASTFOUND > '"+ document.forms[1].text10.value+"' AND "+
	"DATELASTFOUND < '"+document.forms[1].text11.value+"'";
   window.location = '/servlet/Main?template=Taxonomy.CQL&query='+escape(query);
}

</script>

<form>
<table align=left width=700><tr><td>

<!-- Queries -->
<table align=right width=399 bgcolor="#F0F0F0" border=1 bordercolor=000000><tr><td>
<table width=399 align="left" cellspacing=0>
<tr height=20><td bgcolor="#6987DF" colspan=2>
&nbsp; <b><font face="Helvetica,Arial" size=3 color="White">Query Builder</font></b></td>
<td align=right bgcolor="#6987DF"> &nbsp; &nbsp;</td></tr>
<tr height=30 valign=middle><td>&nbsp;</td>
<!-- question #1 -->
<td><font face="Helvetica,Arial" size=2> 
Retrieve documents from the 
<select name="genre1">
<option value=8 selected>PubMed
<option value=2>CBER
<option value=3>CDER
<option value=4>EMEA
<option value=5>CDC
<option value=6>IDRAC
<option value=9>Factiva
</select> 
folder that mention the term 
<input name="text1" type="text" size="9" value="Betaseron">.</font>
<br>
<hr>
</td>
<td valign=top>&nbsp; <input type="button" value="Run" onClick='javascript:question1()'> &nbsp; </font></td></tr>
<!-- question #2 -->
<tr height=30><td>&nbsp;</td>
<td><font face="Helvetica,Arial" size=2> 
Retrieve documents about 
<input name="text2" type="text" size="9" value="contraceptive"> that mention the term 
<input name="text3" type="text" size="9" value="Drospirenone">.</font>
<hr>
</td>
<td valign=top>&nbsp; <input type="button" value="Run" onClick='javascript:question2()'> &nbsp; </font></td></tr>
<!-- question #3 -->
<tr height=30><td>&nbsp;</td>
<td><font face="Helvetica,Arial" size=2> 
Retrieve documents about both
<input name="text4" type="text" size="9" value="Magnetic"> and 
<input name="text5" type="text" size="9" value="Gadolinium">.</font>
<hr>
</td>
<td valign=top>&nbsp; <input type="button" value="Run" onClick='javascript:question3()'> &nbsp; </font></td></tr>
<!-- question #4 -->
<tr height=30><td>&nbsp;</td>
<td><font face="Helvetica,Arial" size=2> 
Retrieve documents from the 
<select name="genre2">
<option value=4 selected>EMEA
<option value=8>PubMed
<option value=2>CBER
<option value=3>CDER
<option value=5>CDC
<option value=6>IDRAC
<option value=9>Factiva
</select> 
folder with the term    
<input name="text6" type="text" size="9" value="blood"> 
in the title.
<hr>
</td>
<td valign=top>&nbsp; <input type="button" value="Run" onClick='javascript:question4()'> &nbsp; </font></td></tr>
<!-- question #5 -->
<tr height=30><td>&nbsp;</td>
<td><font face="Helvetica,Arial" size=2> 
Retrieve documents from the 
<select name="genre3">
<option value=4>EMEA
<option value=8>PubMed
<option value=2>CBER
<option value=3>CDER
<option value=5>CDC
<option value=6 SELECTED>IDRAC
<option value=9>Factiva
</select> 
folder found between 
<input name="text7" type="text" size="9" value=""> 
and 
<input name="text8" type="text" size="9" value="">.
<hr>
</td>
<td valign=top>&nbsp; <input type="button" value="Run" onClick='javascript:question5()'> &nbsp; </font></td></tr>
<!-- question #6 -->
<tr height=30><td>&nbsp;</td>
<td><font face="Helvetica,Arial" size=2> 
Retrieve documents that mention the term 
<input name="text9" type="text" size="9" value="Betaseron">
found between 
<input name="text10" type="text" size="9" value=""> 
and 
<input name="text11" type="text" size="9" value="">.
</td>
<td valign=top>&nbsp; <input type="button" value="Run" onClick='javascript:question6()'> &nbsp; </font></td></tr>
</table>
</td></tr></table>
</form>

<!-- 25-MAY-05 -->
<script>
var today = new Date();
var m = today.getMonth() + 1;
var day = today.getDate();
var year = today.getYear();
var s = "-";
var mmm = 
( 1==m)?'Jan':( 2==m)?'Feb':(3==m)?'Mar':
( 4==m)?'Apr':( 5==m)?'May':(6==m)?'Jun':
( 7==m)?'Jul':( 8==m)?'Aug':(9==m)?'Sep':
(10==m)?'Oct':(11==m)?'Nov':'Dec';

document.forms[1].text8.value = day + s + mmm + s + year;
document.forms[1].text11.value = day + s + mmm + s + year;

today.setDate(today.getDate() - 14);
m = today.getMonth() + 1;
day = today.getDate();
year = today.getYear();
mmm = 
( 1==m)?'Jan':( 2==m)?'Feb':(3==m)?'Mar':
( 4==m)?'Apr':( 5==m)?'May':(6==m)?'Jun':
( 7==m)?'Jul':( 8==m)?'Aug':(9==m)?'Sep':
(10==m)?'Oct':(11==m)?'Nov':'Dec';

document.forms[1].text7.value = day + s + mmm + s + year;
document.forms[1].text10.value = day + s + mmm + s + year;

</script>

<!-- Products -->
<table width=253 bgcolor="#F0F0F0" border=1 bordercolor=000000><tr><td>
<table width=253 cellspacing=0>
<tr height=20><td bgcolor="#6987DF" colspan=2>
&nbsp; <b><font face="Helvetica,Arial" size=3 color="White">##TYPE##</font></b></td>
<td align=right bgcolor="#6987DF"> &nbsp; &nbsp;</td></tr>