<BLOCKQUOTE>
    <TABLE cellPadding=2 width="78%" border=0>
      <TBODY> 
      <TR> 
        <TD vAlign=top width="50%"> 
          <P><B>Node Help:</B></P>
          <P>This screen allows you to manipulate the nodes that are in the system. 
            Each node has the following properties:</P>
          <table width="80%" border="1">
            <tr> 
              <td>Parent Tree</td>
              <td>This is not editable. It shows the hierarchy between the current 
                node and the top of the taxonomy.</td>
            </tr>
            <tr> 
              <td>Node Title</td>
              <td>This is the name of the node in the gui of the system.</td>
            </tr>
            <tr> 
              <td>Signatures</td>
              <td>This is a list of the concept signatures in the sytem. Leaf 
                nodes are typically the only nodes with signatures in the system</td>
            </tr>
            <tr> 
              <td>Subtopics</td>
              <td>This is an ordered list of subtopics for the current node. Leaf 
                nodes do not have subtopics</td>
            </tr>
          </table>
          </TD>
      </TR>
      <TR>
        <TD vAlign=top width="50%">&nbsp;</TD>
      </TR>
      </TBODY>
    </TABLE>
  </BLOCKQUOTE>
