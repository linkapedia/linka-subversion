<SCRIPT LANGUAGE="JAVASCRIPT"> 
function editsigs() {
  var ww = 350, wh = 285;
  var wPos = 0;
  var lPos = 0;

  var title = document.fm.NodeTitle.value;
  var url = "/servlet/Main?template=Taxonomy.EditSigs&NodeID=##NODEID##&NodeTitle="+title;

  if(screen) {
  wPos = (screen.width - ww)/2;
  lPos = (screen.height - wh)/2;
  }
  if (!window.SEwin) {
  // Not defined
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  } else {
  // defined
  if (SEwin.closed) {
  // closed, open a new one
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  }}
  SEwin.focus();
}

function Remove() {
   var col = "Children";
   var sl = document.fm[col].selectedIndex;

   if (sl == -1) { return; }

     if (document.fm[col].options[sl].value!='none') {
     if (confirm("You are about to delete the selected topic and all of its children, Continue?.")) {
     if (confirm("Deleted topics may never be recoverable, are you sure you want to continue?")) {
         window.location = "/servlet/Main?template=Taxonomy.AdminBrowse&delete.x=yes&NodeID="+document.fm[col].options[sl].value;
}}}}

function Add() {
    window.location="/servlet/Main?template=Taxonomy.AddNode&NodeID=##NODEID##";
}

function Save() {
   var col = "Children";
   var ChildOrder = "";

   if (document.fm[col] != null) {
     var ArrLen = document.fm[col].options.length;   
      for (i=0; i<ArrLen; i++) {
       if (i != 0) { ChildOrder = ChildOrder+','; }
       ChildOrder = ChildOrder + document.fm[col].options[i].value+'('+i+')';
   }}
   if (document.fm.CorpusID != null) { 
   window.location = "/servlet/Main?template=##TEMPLATE##"+
		"&save.x=yes&NodeID=##NODEID##"+
                                "&CorpusID="+document.fm.CorpusID.value+
		"&NodeTitle="+document.fm.NodeTitle.value+"&Signatures="+document.fm.Signatures.value+
		"&ChildOrder="+ChildOrder;
   } else {
   window.location = "/servlet/Main?template=##TEMPLATE##"+
		"&save.x=yes&NodeID=##NODEID##"+
		"&NodeTitle="+document.fm.NodeTitle.value+"&Signatures="+document.fm.Signatures.value+
		"&ChildOrder="+ChildOrder;
   }
}

function Up() {
   var col = "Children";
   var sl = document.fm[col].selectedIndex;

   if (sl == -1) { return; }

     if (document.fm[col].options[sl].value!='none') {
          if (sl > 0) {
	oText = document.fm[col].options[sl-1].text;
      	oValue = document.fm[col].options[sl-1].value;
      	document.fm[col].options[sl-1].text = document.fm[col].options[sl].text;
      	document.fm[col].options[sl-1].value = document.fm[col].options[sl].value;
	document.fm[col].options[sl].text = oText;
	document.fm[col].options[sl].value = oValue;
	document.fm[col].selectedIndex = sl-1;
          }
    }
}

function Down() {
   var col = "Children";
   var sl = document.fm[col].selectedIndex;

   if (sl == -1) { return; }

     if (document.fm[col].options[sl].value!='none') {
          if (sl+1 < document.fm[col].options.length) {
	oText = document.fm[col].options[sl+1].text;
      	oValue = document.fm[col].options[sl+1].value;
      	document.fm[col].options[sl+1].text = document.fm[col].options[sl].text;
      	document.fm[col].options[sl+1].value = document.fm[col].options[sl].value;
	document.fm[col].options[sl].text = oText;
	document.fm[col].options[sl].value = oValue;
	document.fm[col].selectedIndex = sl+1;
          }
    }
}

function Explore() {
   var col = "Children";
   var sl = document.fm[col].selectedIndex;

   if (sl == -1) { return; }

     if (document.fm[col].options[sl].value!='none') {
         window.location = "/servlet/Main?template=Taxonomy.AdminBrowse&NodeID="+document.fm[col].options[sl].value;
    }
}


function Move() {
   var col = "Children";
   var sl = document.fm[col].selectedIndex;

   if (sl != -1) {
         window.location = "/servlet/Main?template=Taxonomy.MoveNodes&NodeIDA="+document.fm[col].options[sl].value+"&NodeIDB="+document.fm[col].options[sl].value;
   }
}

function makeList(col) 
{
  val = "";
  for (j=0; j<document.fm[col].length; j++) {
    if (val > "") { val += ","; }
    if (document.fm[col].options[j].value > "") val += document.fm[col].options[j].value;
  } 
  return val;
}

function sub_layout() {
  document.fm["OldStuff_lst"].value = makeList("OldStuff"); 
  document.fm.submit();
}
</script>

<blockquote>
<form name="fm" method=post action="/servlet/Main">
<input type=hidden name=NodeID value=##NODEID##>
##Corpus##
<input type="hidden" name="template" value="##TEMPLATE##">
<table width=700><tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Parent Tree:</b></font></td>
                 <td>##NODETREE##</td></tr>
            <tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Node Title:</b></font></td>
                 <td ><input type='text' name='NodeTitle' value='##NODETITLE##' size=50 maxlength = 255></td></tr>
            <tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Signatures:</b></font></td>
                  <td><a href="javascript:editsigs();"><font size=-2>Edit Signatures</font></a><br>
                  <textarea name="Signatures" cols="70" rows="3" READONLY>##SIGNATURELIST##</textarea></td></tr>
           </table>
