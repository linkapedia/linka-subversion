  <BLOCKQUOTE>
    <table width="650" border="0">
      <tr>
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.CorpusAdminister"><b>ROC Settings</b></a></td>
        <td width="73%" valign="top">Taxonomy ROC Settings allows you to add, edit, delete and
          manage ROC settings for each taxonomy in the system. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr>
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Taxonomy.Repository">Repositories</a></b></td>
        <td width="73%" valign="top">Add, remove, and manage system repositories.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr>
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Taxonomy.Classifier">Classifiers</a></b></td>
        <td width="73%" valign="top">Add, remove, and manage various classification engines within the ITS.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr>
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.SystemParam">System Parameters</a></b></td>
        <td width="73%" valign="top">Allows the Administrator the ability to change
          various system parameters such as database location and Knowledge Harvester
          information <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
    </table>
    </BLOCKQUOTE>