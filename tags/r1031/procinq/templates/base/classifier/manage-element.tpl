<!-- manage-element.tpl -->
<SCRIPT>
function Delete##CLASSIFIERID## () {
   if (confirm("If you delete this classifier, all document scoring for this classifier will also be erased.  Are you sure?")) {
	window.location = "/servlet/Main?template=Taxonomy.DeleteClassifier&CID=##CLASSIFIERID##";
   }
}
</SCRIPT>
<tr><td>##CLASSIFIERNAME##</td>
      <td align=center>##COLNAME##</td>
      <td align=center>##CLASSPATH##</td>
      <td align=center>##SORTORDER##</td>
      <td align=center>##ACTIVE##</td>
      <td align=center><a href="/servlet/Main?template=Taxonomy.CreateClassifier&CID=##CLASSIFIERID##">
	<img src="/servlet/images/edit.gif" border=0></a></td>
      <td align=center><a href="javascript:Delete##ClassifierID##();">
	<img src="/servlet/images/delete.gif" border=0></a></td>
</tr>