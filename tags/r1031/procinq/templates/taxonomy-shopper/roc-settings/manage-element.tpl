<!-- manage-element.tpl -->
<SCRIPT>
function Delete##CLASSIFIERID## () {
   if (confirm("You are about to remove a local classifier used to score documents against this taxonomy.  Are you sure?")) {
      myWin = window.open( "/servlet/Main?template=Taxonomy.DeleteLocalClassifier&RID=##ROCSETTINGID##&star=##STARCOUNT##",
	"RemoveIt", "menubar=no,personalbar=no,resizable=yes,scrollbars=no,width=650,height=180" );
      myWin.moveTo(0,0);
      myWin.focus();
   }
}
</SCRIPT>

<tr><td>##CLASSIFIERNAME##</td>
      <td align=center>##STARCOUNT##</td>
      <td align=center>##COSTRATIOFPTOFN##</td>
      <td align=center>##CUTOFFSCOREGTE##</td>
      <td align=center><a href="/servlet/Main?template=Taxonomy.CreateLocalClassifier&RID=##ROCSETTINGID##&star=##STARCOUNT##">
	<img src="/servlet/images/edit.gif" border=0></a></td>
      <td align=center><a href="javascript:Delete##ClassifierID##();">
	<img src="/servlet/images/delete.gif" border=0></a></td>
</tr>