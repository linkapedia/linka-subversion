<blockquote>
  <TABLE cellPadding=2 width="700" border=0>
    <TBODY>
    <TR>
      <TD vAlign=top width="50%">
      <p>Welcome to the ProcinQ Taxonomy Server. The Taxonomy Server allows you to organize information inside and
      outside your company with trusted taxonomies. The server provides access to searching, adding and modifying
      these taxonomies as well as retrieving and manipulating information that is catagorized into them.</p>
      <p><b>Help for Home:</b></p>
      <p>Clicking Home will take you to the front page of the Taxonomy Server user interface. From here you can select
      any of the available taxonomies to begin browsing or execute a search.</p>
      <p><b>Help for Prefs:</b></p>
      <p>Clicking on prefs will take you to your user preference screen. At this screen you can change aspects of your user profile.</p>
      <p><b>Help for Administer:</b></p>
      <p>With the appropriate privledges in the system, administer allows you change properties of the server. </b></p>
      <p><b>Help for Login:</b></p>
      <p>In order for you to access the Taxonomy server, it is first necessary to login to the server. You should have
      been given and user id and a password from the system administrator.</p>
      <p><b>Help for Logout:</b></p>
      <p>This will log you out of the taxonomy server and end you session.</p>
        </TD>
    </TR>
    </TBODY>
  </TABLE>
</blockquote>