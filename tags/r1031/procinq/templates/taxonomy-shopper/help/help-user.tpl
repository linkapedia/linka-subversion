<BLOCKQUOTE>
  <TABLE cellPadding=2 width="78%" border=0>
    <TBODY>
    <TR>
      <TD vAlign=top width="50%">
          <P><B>User Preference Help:</B></P>
          <P>This screen allows you to manipulate the users preferences.
          <P>Users have the following attributes:</P>
          <table width="80%" border="1">
              <td>Scoring Threshold</td>
              <td>A user setting for determining the filtering level for the user. 
                This can be edited only, 3 stars, 2 stars, 1 star or everything</td>
            <tr> 
              <td>User status</td>
              <td>True means that the user is active in the system</td>
            </tr>
            <tr> 
              <td>Alert status</td>
              <td>Frequenct of email alerts for the user. This can be off for 
                no alerts, daily, weekly or monthly</td>
            </tr>
            <tr> 
              <td>Results per page</td>
              <td>The number of results per page the user wants for pages of links</td>
            </tr>
            <tr>
              <td>Information Alerts</td>
              <td>This lists all of the topics that the user has signed up for 
                information alerts on</td>
            </tr>
          </table>
          <P>&nbsp;</P>
          <P><b>Watched topics</b></P>
          <p>This area lets the user change any of their watched topics in the system</p>
        </TD></TR></TBODY></TABLE></BLOCKQUOTE>
