<script language="JavaScript">
<!--
var myWin;
function MM_openBrWindow(theURL,winName,features) { //v2.0
  if (myWin && (!myWin.closed))
    myWin.focus();
  else
  {
    myWin = window.open(theURL,winName,features);
    myWin.moveTo(0,0);
  }
}
//-->
</script>
<p>
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
    <td width="50"><img src="/servlet/images/idrac/s.gif" width=50 height=50></td>
    <td width="660"><b><font face="Arial,Helvetica" size=4>Advanced Search</font></b></td>
    <td width="50"><img src="/servlet/images/idrac/s.gif" width=50 height=50></td>
  </tr>
  <tr valign="top">
    <td width="50">&nbsp;</td>
    <td width="660" align="left">
      <p>This Search menu will allow you to easily retrieve documents within the system.
        Use either Full Text or Topic criteria or both in the same search. You
        can also search full text in the title of the documents.</p>
      <div align="center">
        <table width="580" border="0" bordercolor=000000 cellspacing="3" cellpadding="3" bgcolor=669999>
		<form name="forme" method=get action="/servlet/Main">
		<input type="hidden" name="template" value="Taxonomy.Search">
		<input type="hidden" name="advanced" value="true">
          <tr>
              <td valign="top">
                <table width="580" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><font color=white>Corpora(s):</td>
                    <td><font color=white>
                      <select name="corpusid" onFocus="status='/gif/mcsupd.gif'">
                        <option value="All" SELECTED>All Taxonomies
				##OPTION_CORPUS_LIST##
                      </select> and
                    </td>
		    <td align=right>
                     <input type="submit" name="Search" value="Search">
                      <img src="/servlet/images/idrac/s.gif" width="10" height="22">
		    </td>
                  </tr>
                  <tr>
                    <td><font color=white>Full text (and):</td>
                    <td colspan = 2><font color=white>
                      <input type="text" size="50" name="fulltext"> and
                    </td>
                  </tr>
                  <tr>
                    <td><font color=white>Document title:</td>
                    <td colspan = 2> <font color=white>
                      <input type="text" size="50" name="title"> and
                    </td>
                  </tr>
                  <tr>
                    <td><font color=white>Topic Title:</td>
                    <td colspan = 2> <font color=white>
                      <input type="text" size="28" name="keywords">
			    <input type="button" name="PermutIndex" value="Topic Index" onClick="MM_openBrWindow('/servlet/Main?template=Idrac.KeywordBuilder','idracKwsWin','scrollbars=yes,width=630,height=420')">  or
			  </td>
                  </tr>
                  <tr>
                    <td colspan=3>&nbsp;<br>&nbsp;<br>&nbsp; </td>
                  </tr>
                  <tr>
                  <tr>
                    <td><font color=white>Full text (or):</td>
                    <td colspan = 2>
                      <input type="text" size="50" name="fulltextor">
                    </td>
                  </tr>
                </table>
                <p>&nbsp;</p>
                </td>
          </tr>
		</form>
        </table>
      </div>

