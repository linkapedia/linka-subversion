<blockquote><font face="Arial">
<b>Note</b>: After the thesaurus is created, you may add thesaurus terms and relationships using the <i>Browse</i> option from the Thesaurus menu.
<P>
<form method=get action="/servlet/Main">
<input type="hidden" name="template" value="Taxonomy.CreateThesaurus">
<b>Thesaurus Name: </b>&nbsp; <input type="text" name="ThesaurusName" value="" maxlength=20 maxsize=20>
&nbsp; <input type="submit" name="submit" value="Create">
</form>
</font>
</blockquote>