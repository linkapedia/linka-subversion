<SCRIPT LANGUAGE="JAVASCRIPT"> 
function AddA() {
    window.location="/servlet/Main?template=Taxonomy.AddNode&NodeID=##NODEID##";
}

function DownA() {
   var o_col = "ChildrenA";
   var d_col = "ChildrenB";
   var d_sl = document.fm[d_col].length;

   alert("Your selections have been moved.  They will appear at the bottom of the second list box.");
   for (i=0; i < document.fm[o_col].options.length; i++) {
      if (document.fm[o_col].options[i].selected) {
         oText = document.fm[o_col].options[i].text;
         oValue = document.fm[o_col].options[i].value;
         document.fm[o_col].options[i] = null;
         document.fm[d_col].options[d_sl] = new Option (oText, oValue, false, true);
	 d_sl = d_sl + 1; i = i - 1;
      } 
   }     
}

function ExploreA() {
   var col = "ChildrenA";
   var sl = document.fm[col].selectedIndex;

   if (sl != -1) {
        window.location = "/servlet/Main?template=Taxonomy.MoveNodes&NodeIDB="+document.fm.NodeIDB.value+"&NodeIDA="+document.fm[col].options[sl].value;
   } else { alert("You must first highlight the selection to explore in the drop down list."); }
}

function ExploreLinkA(NodeID) {
   window.location = "/servlet/Main?template=Taxonomy.MoveNodes&NodeIDB="+document.fm.NodeIDB.value+"&NodeIDA="+NodeID;
}

</script>

<blockquote>
<form name="fm" method=post action="/servlet/Main">
<input type=hidden name=NodeIDA value=##NODEID##>
<input type="hidden" name="template" value="Taxonomy.MoveNodes">
<table width=650>
  <tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Parent Tree:</b></font></td>
                 <td>##NODETREE##</td></tr>
  <tr><td width=100><font face="Arial, Helvetica, sans-serif" size=-1><b>Node Title:</b></font></td>
       <td> <a href="/servlet/Main?template=Taxonomy.AdminBrowse&NodeID=##NODEID##">
	    ##NODETITLE##</a> </td></tr>
</table>
