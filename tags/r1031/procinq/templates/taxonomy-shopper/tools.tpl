<SCRIPT>
function Purge() {
    if (confirm ('All objects marked for deletion will be unrecoverable after a system purge.  Continue?')) {
       window.location='/servlet/Main?template=Server.Purge';
    }
}
</SCRIPT>
  <BLOCKQUOTE>
    <p><B>Tools</B></p>
    <table width="650" border="0">
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Server.Classify"><b>Classify</b></a></td>
        <td width="73%" valign="top">Select a document from a local server to classify into one or
	   more taxonomies.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr>
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.CQL"><b>Custom CQL Query</b></a></td>
        <td width="73%" valign="top">Search and retrieve data using Intellisophic's <i><u>Concept Query Language</u></i>.<br>
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
	<!--
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=ConceptAlerts.Alerts"><b>Concept Alerts</b></a></td>
        <td width="73%" valign="top">Create, manage and execute narratives using the ITS <i><u>Concept Alert System</u></i>.<br>
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr> 
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.SearchSignatures"><b>Documents by Signature</b></a></td>
        <td width="73%" valign="top">Search and retrieve documents that contain a single word in the node concept signature</u></i>.<br>
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.SimilarDocuments">
	<b>Get Similar Documents</b></a></td>
        <td width="73%" valign="top">Find similar documents based on the classification of a given document</u></i>.<br>
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
	-->
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.GetRelatedNodes"><b>Related Nodes</b></a></td>
        <td width="73%" valign="top">Find all topics on this server containing a specific document.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
    </table>
    </BLOCKQUOTE>