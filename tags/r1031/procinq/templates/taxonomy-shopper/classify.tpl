<blockquote>
<FORM name=import action=/servlet/Main method=post encType=multipart/form-data>
<INPUT type=hidden value=Server.Classify name=template>
<INPUT type=hidden value=true name=ShowScores>
<INPUT type=hidden value=false name=post>
<b><u>Classify a Document</u></b> 
<p>
<b>Instructions:</b> Copy and paste text from a document into the space provided 
below and click the <b>CLASSIFY</b> button.  You must select a subject area to 
provide the classification engine with the proper context.
<p>
<table border=0 cellpadding=2 cellspacing=2>
<tr><td><select name="subjectarea">
<option value=-1>-- Please select --
##SUBJECTAREA##
</select></td></tr>
<tr><td><textarea name="doctext" rows=10 cols=60>Insert a text or HTML document here.</textarea></td></tr>
</table>
<P>
<input type="submit" name="SUBMIT" value="CLASSIFY">
</form>
