<HTML>
	<HEAD>
		<title>Saved Query / Alert</title>
		<style type="text/css">
			<!--
				H5 { font-size:9pt; font-family:verdana; font-style:bold; }
			-->
		</style>
		<script LANGUAGE="JavaScript"><!--
				function CloseWindow() { window.close(); }
				function RunIt() {
				    if (document.SaveSearch.Name.value == '') {
						alert("Sorry, you must first enter a name for your alert.");
						return false;
				    }
				return true;
				}				
				
			//--></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form name="SaveSearch" action="/servlet/Main" method="post" onSubmit="return RunIt()">
			<input type="hidden" name="template" value="Taxonomy.AddAlert">
			<input type="hidden" name="CQL" value="##QUERY##">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
  				<tr>
					<td valign="middle">
						<h5>Query Saved As: &nbsp;</h5>
					</td>
					<td valign="top">
						<input type="text" name="Name" size="20" maxlength="60" TABINDEX="1" style="font-size:9pt; font-family:verdana; font-style:bold; width:270px; height:25px;">
					</td>
					<td valign="top" align="right">
						<input type="submit" TABINDEX="6" name="Save" value="Save"> &nbsp; <input type="button" TABINDEX="7" name="Close" onclick="javascript:CloseWindow();" value="Cancel">
					</td>
				</tr>
				<tr>
					<td>
						<h5>Notes (optional): &nbsp;</h5>
					</td>
					<td valign="top" colspan="2"><textarea TABINDEX="4" name="Notes" rows="2" cols="40" style="font-size:9pt; font-family:verdana; font-style:bold; width:420px; height:60px;"></textarea>
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td valign="bottom">
						<h5>E-mail Alert Frequency: 
						</h5>
					</td>
					<td valign="top" colspan="2"><select TABINDEX="5" name="RunFreq" style="font-size:9pt; font-family:verdana; font-style:bold;">
							<option value="-1" selected>Never 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
							<option value="7">Every Week 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
							<option value="14">Every 2 Weeks 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
							<option value="21">Every 3 Weeks 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
							<option value="28">Every 4 Weeks 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
						</select>
					</td>
				</tr>
			</table>
		</form>
		<script> document.SaveSearch.Name.focus(); </script>
	</body>
</HTML>
