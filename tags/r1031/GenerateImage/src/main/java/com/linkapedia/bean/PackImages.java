/**
 * @author: Andres R
 * bean to transport image info
 */
package com.linkapedia.bean;

import java.awt.image.BufferedImage;

public class PackImages {

	private String id;
	private BufferedImage image;
	private BufferedImage imageThumnail;
	private BufferedImage compositeImage;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public BufferedImage getImageThumnail() {
		return imageThumnail;
	}
	public void setImageThumnail(BufferedImage imegeThumnail) {
		this.imageThumnail = imegeThumnail;
	}
	public BufferedImage getCompositeImage() {
		return compositeImage;
	}
	public void setCompositeImage(BufferedImage compositeImage) {
		this.compositeImage = compositeImage;
	}
	
	
}
