package com.yuxipacific.documents.network;

import com.yuxipacific.documents.storage.exceptions.download.DeniedDownloadException;
import com.yuxipacific.documents.storage.exceptions.download.FileSizeExceededException;
import java.io.FileNotFoundException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 * Class to handle the requests to Internet to download any file.
 * <p/>
 * @author Xander Kno
 */
public class FileDownloader {

    private static final Logger log = Logger.getLogger(FileDownloader.class);
    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("system/config");
    private static final String DEFAULT_DESTINATION;

    static {
        DEFAULT_DESTINATION = systemConfig.getString("system.download.dst.location");
    }

    /**
     * Method to request the download of a file using the proxy
     * <p/>
     * @param url Internet location of the file to be downloaded.
     * @return Local path to the file.
     */
    public static String download(URL url) throws FileNotFoundException, FileSizeExceededException, DeniedDownloadException, SocketTimeoutException {
        log.debug("downloadFile(URL)");
        return DownloadProxy.downloadFile(url, DEFAULT_DESTINATION);
    }

    /**
     * Method to request the download of a file using the proxy
     * <p/>
     * @param source Internet location of the file to be downloaded.
     * @param destination Destination folder for the file.
     * @return Local path to the file.
     */
    public static String download(URL source, String destination) throws FileNotFoundException, FileSizeExceededException, DeniedDownloadException, SocketTimeoutException {
        log.debug("downloadFile(URL, Long)");
        return DownloadProxy.downloadFile(source, destination);
    }
}
