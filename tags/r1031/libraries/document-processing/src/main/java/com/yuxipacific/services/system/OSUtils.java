package com.yuxipacific.services.system;

import java.io.IOException;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class OSUtils {

    private static final Logger log = Logger.getLogger(OSUtils.class);

    public static void shutdownHost() {
    }

    public static void restartHost() {
        log.info("Going to restart host in +2 min");
        Runtime run = Runtime.getRuntime();
        //The best possible I found is to construct a command which you want to execute  
        //as a string and use that in exec. If the batch file takes command line arguments  
        //the command can be constructed a array of strings and pass the array as input to  
        //the exec method. The command can also be passed externally as input to the method.  

        Process p = null;
        String cmd = "shutdown -r +2 Classifier Refresh";
        try {
            p = run.exec(cmd);
            p.getErrorStream();
            p.waitFor();
        } catch (InterruptedException ex) {
            log.error("An exception ocurred while restarting the host.");
        } catch (IOException e) {
            log.error("An exception ocurred while restarting the host.");
        } finally {
            p.destroy();
        }
    }
}
