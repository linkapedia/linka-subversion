package com.yuxipacific.documents.storage.exceptions.parser;

/**
 *
 * @author Xander Kno
 */
public class NoParseableDocumentException extends Exception {

    /**
     * Creates a new instance of
     * <code>NoParseableDocumentException</code> without detail message.
     */
    public NoParseableDocumentException() {
    }

    /**
     * Constructs an instance of
     * <code>NoParseableDocumentException</code> with the specified detail message.
     * <p/>
     * @param msg the detail message.
     */
    public NoParseableDocumentException(String msg) {
        super(msg);
    }

    public NoParseableDocumentException(Throwable cause) {
        super(cause);
    }
}
