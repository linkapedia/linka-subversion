package com.yuxipacific.documents.storage.exceptions.download;

/**
 *
 * @author Xander Kno
 */
public class FileSizeExceededException extends Exception {

    public FileSizeExceededException(String message) {
        super(message);
    }

    public FileSizeExceededException() {
    }
}
