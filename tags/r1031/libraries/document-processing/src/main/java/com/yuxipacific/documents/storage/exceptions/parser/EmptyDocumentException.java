package com.yuxipacific.documents.storage.exceptions.parser;

/**
 *
 * @author Xander Kno
 */
public class EmptyDocumentException extends Exception {

    /**
     * Creates a new instance of
     * <code>EmptyDocumentException</code> without detail message.
     */
    public EmptyDocumentException() {
    }

    /**
     * Constructs an instance of
     * <code>EmptyDocumentException</code> with the specified detail message.
     * <p/>
     * @param msg the detail message.
     */
    public EmptyDocumentException(String msg) {
        super(msg);
    }
}
