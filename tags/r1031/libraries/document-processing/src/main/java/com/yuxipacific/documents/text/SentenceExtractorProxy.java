package com.yuxipacific.documents.text;

import edu.washington.cs.knowitall.nlp.ChunkedSentence;
import edu.washington.cs.knowitall.nlp.ChunkedSentenceReader;
import edu.washington.cs.knowitall.util.DefaultObjects;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class SentenceExtractorProxy {

    private static final Logger log = Logger.getLogger(SentenceExtractorProxy.class);


    /**
     * Method to handle the Sentence Extraction from a given text.
     * @param content File content to extract the sentence from.
     * @return Sentences extracted from the given text.
     * @throws Exception 
     */
    public static String extractFrom(String content) throws Exception {
        log.debug("extractFrom(String)");
        if (content == null || content.isEmpty()) {
            log.error("Cannot execute the extraction without content.");
            return null;
        }
        StringBuilder fileExtractedContent = new StringBuilder();
        try {
            InputStream in = new ByteArrayInputStream(content.getBytes("UTF-8"));
            InputStreamReader isr = new InputStreamReader(in);

            log.debug("Initializing NLP tools...");
            ChunkedSentenceReader sentReader = DefaultObjects.getDefaultSentenceReader(isr);
            log.debug("Done.");

            try {
                String sentString;
                for (ChunkedSentence sent : sentReader.getSentences()) {
                    sentString = sent.getTokensAsString();
                    if (sentString != null) {
                        fileExtractedContent.append(sentString).append("\n");
                    }
                }
            } catch (StringIndexOutOfBoundsException e) {
                log.error("An exception has ocurred: The end of the content has been reached.");
            }
        } catch (Exception e) {
            log.error("An exception has ocurred: ", e);
        }
        return fileExtractedContent.toString();
    }
}
