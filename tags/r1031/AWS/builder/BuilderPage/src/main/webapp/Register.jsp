<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<f:view>
    <html>
        <head>
            <title>Register User</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="css/external.css">
            <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" charset="utf-8">
            </script>
        </head>
        <body>

            <div id="div_box_img">
                <img id="fbProfilePic" src="" />
            </div> 

            <h:form  id="frmLogin"  styleClass="box login">
                <fieldset class="boxBody">
                    <legend align="right">Create Your Account On Linkapedia</legend>
                    <label>User Mail</label>
                    <h:inputText value="#{UserBean.email}" id="usermail" 
                                 requiredMessage="Please enter your mail" 
                                 validatorMessage="Invalid e-mail address format"
                                 required="true">   
                        <f:validateRegex pattern="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                    </h:inputText>

                    <label>First Name</label>
                    <h:inputText value="#{UserBean.firstName}" id="firstname"
                                 required="true" requiredMessage="Please enter your first name"
                                 validatorMessage="user name must be at least 3 characters">
                        <f:validateLength minimum="3" />
                    </h:inputText>

                    <label>Last Name</label>
                    <h:inputText value="#{UserBean.lastName}" id="lastname"
                                 required="true" requiredMessage="Please enter your last name"
                                 validatorMessage="user name must be at least 3 characters">
                        <f:validateLength minimum="3" />
                    </h:inputText>
                    <label>Password</label>
                    <h:inputSecret value="#{UserBean.password}" id="password" 
                                   required ="true" requiredMessage="Please enter your password"
                                   validatorMessage="password must be at least 3 characters">
                        <f:validateLength minimum="3" />
                    </h:inputSecret>

                    <h:inputHidden value="#{UserBean.fbId}" id="fbId" 
                                   required ="false"  
                                   >
                    </h:inputHidden>

                    <h:inputHidden value="#{UserBean.fbToken}" id="fbToken" 
                                   required ="false"  
                                   >
                    </h:inputHidden>

                </fieldset>
                <footer>
                    <h:commandButton value="Register" action="#{UserBean.save}" styleClass="btnLogin"  />
                    <h:messages style="color:red;"  />
                </footer>

            </h:form>

            <%
       if (request.getParameter("fbid")!=null)
                           {                             
            %>
            <script>
                document.getElementById('frmLogin:usermail').value = '<% out.print(request.getParameter("email")); %>';
                document.getElementById('frmLogin:firstname').value = '<% out.print(request.getParameter("name")); %>';
                document.getElementById('frmLogin:lastname').value = '<% out.print(request.getParameter("lastname")); %>';
                document.getElementById('frmLogin:fbId').value = '<% out.print(request.getParameter("fbid")); %>';
                document.getElementById('frmLogin:fbToken').value = '<% out.print(request.getParameter("fbtoken")); %>';

            </script>       
            <%
               }; 
            %>

            <script>
                if (document.getElementById('frmLogin:fbId').value != "") {
                    $("#fbProfilePic").attr('src', "http://graph.facebook.com/" + document.getElementById('frmLogin:fbId').value + "/picture?type=large");
                    $("#div_box_img").show();
                }
            </script>


        </body>
    </html>

</f:view>