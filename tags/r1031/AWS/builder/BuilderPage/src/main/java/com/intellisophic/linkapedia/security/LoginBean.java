package com.intellisophic.linkapedia.security;

import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.io.IOException;
import javax.faces.context.FacesContext;

/**
 *
 * @author Bancolombia
 */
public class LoginBean {

    /**
     * Creates a new instance of LoginBean
     */
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LoginBean.class);
    private String login;
    private String password;
    private FacesContext context;

    public LoginBean() throws IOException {

        context = FacesContext.getCurrentInstance();
        Object o = context.getExternalContext().getSessionMap().get("terms");
        if (o == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/index.jsp");
        }

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String logout() {
        try {
            String tinyNodeUrl = UtilEnviroment.getContext() + "/home";
            FacesContext context = FacesContext.getCurrentInstance();
            User user = (User) context.getExternalContext().getSessionMap().get("user");

            //The user is logged in.
            //HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
            // session.invalidate();
            context.getExternalContext().getSessionMap().remove("user");
            DataHandlerFacade.deleteCacheUser(user);
            FacesContext.getCurrentInstance().getExternalContext().redirect(tinyNodeUrl);
            return "success";

        } catch (Exception ex) {
            log.error("Error getting user", ex);
        }
        return "success";
    }
}
