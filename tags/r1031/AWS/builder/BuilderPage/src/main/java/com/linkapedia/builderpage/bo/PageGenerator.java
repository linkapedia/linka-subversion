package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.FollowedUser;
import com.intellisophic.linkapedia.api.beans.FollowingInterestCollection;
import com.intellisophic.linkapedia.api.beans.FollowingUser;
import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollectionElement;
import com.intellisophic.linkapedia.api.beans.InterestCollectionFollowed;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.NodeUser;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.api.beans.UserFeed;
import com.intellisophic.linkapedia.api.beans.UserNode;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.beans.DigestInfoObject;
import com.linkapedia.builderpage.bo.beans.InfoVersionObject;
import com.linkapedia.builderpage.bo.beans.LinkObject;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import com.linkapedia.builderpage.bo.models.DefaultPageGenerator;
import com.linkapedia.builderpage.bo.temp.HomeLink;
import com.linkapedia.builderpage.util.DigestUtil;
import com.linkapedia.builderpage.util.BuilderUtil;
import com.linkapedia.builderpage.util.ManageResourcesUtils;
import com.linkapedia.builderpage.util.SentenceUtil;
import com.linkapedia.builderpage.util.StringUtil;
import com.linkapedia.builderpage.util.TreeMenuUtils;
import com.linkapedia.builderpage.util.UtilEnviroment;
import com.sun.jersey.core.util.Base64;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

/**
 * class to generate the page
 *
 * @author andres
 */
public class PageGenerator extends DefaultPageGenerator {

    private static final Logger log = Logger.getLogger(PageGenerator.class);

    public PageGenerator(NodeStatic node, User user, String templateName) {
        //init parameters
        super(node, user, templateName);
    }

    /**
     * generate a link node page put the variables in the context
     *
     * @param documents
     * @return
     */
    @Override
    public boolean generateLinksNodePage(List<LinkObject> links) {
        log.debug("generateLinksNodePage(List<NodeDocument>)");
        String templateStyle = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.templatestyle.name");

        if (templateStyle.equals("wiki")) {
            return generateLinksNodePageWiki(links);
        }

        int maxNumberOfCharactersInDescription = 80;
        links = BuilderUtil.removeLinkObjectWithoutDigest(links);
        int totalLinks = links.size();
        int totalUsersFollowingTopic = 0;
        List<NodeUser> nodeUsers = new ArrayList<NodeUser>();
        List<NodeUser> randomNodeUsers = new ArrayList<NodeUser>();
        boolean userAlreadyFollowingTopic = false;
        int maxNumberOfUsersToShowFollowingTopic = 10;
        int init = 10; // init value to infinite scroll
        int nObjects = 5; //Objects number to return

        try {
            maxNumberOfCharactersInDescription = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.collection.small.maxDescriptionCharacters.digest"));
        } catch (NumberFormatException ex) {
            log.info("error parsiong max number of characters to show, Default maxNumberOfCharactersInDescription: " + maxNumberOfCharactersInDescription);
        }

        try {
            maxNumberOfUsersToShowFollowingTopic = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facepile.users.link.showing"));
            init = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.link.init"));
            nObjects = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.link.nobjects"));
        } catch (NumberFormatException e) {
            log.info("error parsing max number to show. Default maxNumberOfUsersToShowFollowingTopic=10", e);
        }
        init = (init < links.size() ? init : links.size());
        links = links.subList(0, init);

        BuilderUtil.calculateColumns(links);
        BuilderUtil.calculateLinksImageSrc(links);
        BuilderUtil.calculateTitle(links, 5);

        String timeJoinNode = "";
        long timeStampJoinNode = 0;
        try {
            nodeUsers = DataHandlerFacade.getNodeUsersByNodeId(node.getId());

            for (NodeUser n : nodeUsers) {
                if (n.getUserId().equals(user.getUserId())) {
                    timeStampJoinNode = n.getTimeStamp();
                    userAlreadyFollowingTopic = true;
                    break;
                }
            }
            totalUsersFollowingTopic = nodeUsers.size();
            Collections.shuffle(nodeUsers);
            if (nodeUsers.size() <= maxNumberOfUsersToShowFollowingTopic) {

                randomNodeUsers = nodeUsers;
            } else {
                randomNodeUsers = nodeUsers.subList(0, maxNumberOfUsersToShowFollowingTopic);
            }

        } catch (Exception ex) {
            log.info("error getting the NodeUser List from node: " + node.getId(), ex);
        }

        //time last join node
        if (timeStampJoinNode != 0) {
            Date date = new Date(timeStampJoinNode);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
            timeJoinNode = simpleDateFormat.format(date);
        }



        //get collection small view
        List<InterestCollection> collectionsRelated = null;
        try {
            collectionsRelated = DataHandlerFacade.getInterestCollectionByNodeId(node.getId());
        } catch (Exception ex) {
            log.error("Error getting collections from nodeId" + node.getId(), ex);
            collectionsRelated = new ArrayList<InterestCollection>();
        }
        if (collectionsRelated != null) {
            Collections.shuffle(collectionsRelated);
        } else {
            collectionsRelated = new ArrayList<InterestCollection>();
        }

        for (int pos = 0; pos < collectionsRelated.size(); pos++) {
            collectionsRelated.get(pos).setInterestCollectionDescription(StringUtil.shrinkString(collectionsRelated.get(pos).getInterestCollectionDescription(), maxNumberOfCharactersInDescription, " ..."));
        }

        List<String> searchs = new ArrayList<String>();
        List<String> replacements = new ArrayList<String>();
        String digest = "";

        searchs.add(",");
        searchs.add(";");
        searchs.add("!");
        searchs.add("\"");
        searchs.add(".");

        replacements.add(", ");
        replacements.add("; ");
        replacements.add("! ");
        replacements.add("\"");
        replacements.add(". ");

        for (int i = 0; i < links.size(); i++) {
            digest = links.get(i).getDigest();
            links.get(i).setDigest(DigestUtil.FilterDigestSpaces(digest, searchs, replacements));
        }

        //get the first 6 to show in slide
        int initCollections = 6;
        initCollections = (initCollections < collectionsRelated.size() ? initCollections : collectionsRelated.size());
        collectionsRelated = collectionsRelated.subList(0, initCollections);
        context.put("links", links);
        context.put("totalLinks", totalLinks);
        context.put("infiniteScrollInit", init);
        context.put("infiniteScrollNobjects", nObjects);
        context.put("totalUsersFollowingTopic", totalUsersFollowingTopic);
        context.put("maxNumberOfUsersToShowFollowingTopic", maxNumberOfUsersToShowFollowingTopic);
        context.put("randomNodeUsers", randomNodeUsers);
        context.put("userAlreadyFollowingTopic", userAlreadyFollowingTopic);
        context.put("collectionsRelated", collectionsRelated);
        context.put("messageLinksNotFound", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.notdocuments"));
        context.put("timeJoinNode", timeJoinNode);
        return true;

    }

    //Generate page link from google
    public boolean generateLinksNodePageWiki(List<LinkObject> links) {
        log.debug("generateLinksNodePage(List<NodeDocument>)");
        int maxNumberOfCharactersInDescription = 80;
        links = BuilderUtil.removeLinkObjectWithoutDigest(links);
        int totalLinks = links.size();
        int totalUsersFollowingTopic = 0;
        List<NodeUser> nodeUsers = new ArrayList<NodeUser>();
        List<NodeUser> randomNodeUsers = new ArrayList<NodeUser>();
        boolean userAlreadyFollowingTopic = false;
        int maxNumberOfUsersToShowFollowingTopic = 10;
        int init = 10; // init value to infinite scroll
        int nObjects = 5; //Objects number to return

        //bread crumbs----------------------------------------------------------
        String nodeId = node.getId();
        String lastbreadcrumbswithchilds = "null";
        List<BreadCrumbs> breadCrumbs = new ArrayList<BreadCrumbs>();
        List<BreadCrumbs> reverseList = new ArrayList<BreadCrumbs>();

        NodeStatic temp = null;
        BreadCrumbs tempB = null;

        try {
            temp = DataHandlerFacade.getNode(nodeId);
            tempB = new BreadCrumbs(temp);
            tempB.setChilds(TreeMenuUtils.getChildren(temp, temp.getId()));
            breadCrumbs.add(tempB);

            while (!TreeMenuUtils.isRootNode(temp) && !TreeMenuUtils.isTaxonomyRootNode(temp)) {
                nodeId = temp.getParentID();
                temp = DataHandlerFacade.getNode(nodeId);
                if (temp != null) {
                    tempB = new BreadCrumbs(temp);
                    tempB.setChilds(TreeMenuUtils.getChildren(temp, temp.getId()));
                    breadCrumbs.add(tempB);
                }
            }

            int size = breadCrumbs.size();
            for (int i = size; i > 0; i--) {
                reverseList.add(breadCrumbs.get(i - 1));
                if (breadCrumbs.get(i - 1).getChilds() != null) {
                    if (breadCrumbs.get(i - 1).getTotalChilds() > 0) {
                        lastbreadcrumbswithchilds = breadCrumbs.get(i - 1).getParent().getId();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        //----------------------------------------------------------------------

        try {
            maxNumberOfCharactersInDescription = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.collection.small.maxDescriptionCharacters.digest"));
        } catch (NumberFormatException ex) {
            log.info("error parsiong max number of characters to show, Default maxNumberOfCharactersInDescription: " + maxNumberOfCharactersInDescription);
        }

        try {
            maxNumberOfUsersToShowFollowingTopic = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facepile.users.link.showing"));
            init = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.link.init"));
            nObjects = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.link.nobjects"));
        } catch (NumberFormatException e) {
            log.info("error parsing max number to show. Default maxNumberOfUsersToShowFollowingTopic=10", e);
        }
        init = (init < links.size() ? init : links.size());
        links = links.subList(0, init);

        BuilderUtil.calculateColumns(links);
        BuilderUtil.calculateLinksImageSrc(links);
        BuilderUtil.calculateTitle(links, 5);

        String timeJoinNode = "";
        long timeStampJoinNode = 0;
        try {
            nodeUsers = DataHandlerFacade.getNodeUsersByNodeId(node.getId());

            for (NodeUser n : nodeUsers) {
                if (n.getUserId().equals(user.getUserId())) {
                    timeStampJoinNode = n.getTimeStamp();
                    userAlreadyFollowingTopic = true;
                    break;
                }
            }
            totalUsersFollowingTopic = nodeUsers.size();
            Collections.shuffle(nodeUsers);
            if (nodeUsers.size() <= maxNumberOfUsersToShowFollowingTopic) {

                randomNodeUsers = nodeUsers;
            } else {
                randomNodeUsers = nodeUsers.subList(0, maxNumberOfUsersToShowFollowingTopic);
            }

        } catch (Exception ex) {
            log.info("error getting the NodeUser List from node: " + node.getId(), ex);
        }

        //time last join node
        if (timeStampJoinNode != 0) {
            Date date = new Date(timeStampJoinNode);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
            timeJoinNode = simpleDateFormat.format(date);
        }



        //get collection small view
        List<InterestCollection> collectionsRelated = null;
        try {
            collectionsRelated = DataHandlerFacade.getInterestCollectionByNodeId(node.getId());
        } catch (Exception ex) {
            log.error("Error getting collections from nodeId" + node.getId(), ex);
            collectionsRelated = new ArrayList<InterestCollection>();
        }
        if (collectionsRelated != null) {
            Collections.shuffle(collectionsRelated);
        } else {
            collectionsRelated = new ArrayList<InterestCollection>();
        }

        for (int pos = 0; pos < collectionsRelated.size(); pos++) {
            collectionsRelated.get(pos).setInterestCollectionDescription(StringUtil.shrinkString(collectionsRelated.get(pos).getInterestCollectionDescription(), maxNumberOfCharactersInDescription, " ..."));
        }

        List<String> searchs = new ArrayList<String>();
        List<String> replacements = new ArrayList<String>();
        String digest = "";

        searchs.add(",");
        searchs.add(";");
        searchs.add("!");
        searchs.add("\"");
        searchs.add(".");

        replacements.add(", ");
        replacements.add("; ");
        replacements.add("! ");
        replacements.add("\"");
        replacements.add(". ");

        for (int i = 0; i < links.size(); i++) {
            digest = links.get(i).getDigest();
            links.get(i).setDigest(DigestUtil.FilterDigestSpaces(digest, searchs, replacements));
        }

        //get the first 6 to show in slide
        int initCollections = 6;
        initCollections = (initCollections < collectionsRelated.size() ? initCollections : collectionsRelated.size());
        collectionsRelated = collectionsRelated.subList(0, initCollections);
        context.put("links", links);
        context.put("totalLinks", totalLinks);
        context.put("infiniteScrollInit", init);
        context.put("infiniteScrollNobjects", nObjects);
        context.put("totalUsersFollowingTopic", totalUsersFollowingTopic);
        context.put("maxNumberOfUsersToShowFollowingTopic", maxNumberOfUsersToShowFollowingTopic);
        context.put("randomNodeUsers", randomNodeUsers);
        context.put("userAlreadyFollowingTopic", userAlreadyFollowingTopic);
        context.put("collectionsRelated", collectionsRelated);
        context.put("messageLinksNotFound", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.notdocuments"));
        context.put("timeJoinNode", timeJoinNode);
        context.put("breadcrumbs", reverseList);
        context.put("lastbreadcrumbswithchilds", lastbreadcrumbswithchilds);

        return true;
    }

    /**
     * generate a bridge node page put the variables in the context
     *
     * @return
     */
    @Override
    public boolean generateBridgeNodePage() {
        log.debug("generateBridgeNodePage");
        String templateStyle = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.templatestyle.name");

        if (templateStyle.equals("wiki")) {
            return generateBridgeNodePageWiki();
        }

        int init = 12; // init value to infinite scroll
        int nObjects = 8; //Objects number to return  

        if (treeGenerator.isFather()) {
            List<NodeDataObject> childrenToshow = level3;
            BuilderUtil.setBridgeNodeAttributes(childrenToshow);
            childrenToshow = BuilderUtil.setDeafultDescription(childrenToshow, null, true);
            try {
                init = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.node.init"));
                nObjects = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.node.nobjects"));
            } catch (NumberFormatException e) {
                log.info("error parsing infinite scroll node data: Default init=12, nObjects=8", e);
            }

            if (childrenToshow.size() >= init) {
                childrenToshow = childrenToshow.subList(0, init);
            } else {
                init = childrenToshow.size();
            }

            BuilderUtil.calculateNodesImageSrc(childrenToshow, this.node.getTaxonomyID());

            context.put("children", childrenToshow);

        } else {
            context.put("emptyNodeMessage", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.empty.bridgenode"));
        }
        context.put("infiniteScrollInit", init);
        context.put("infiniteScrollNobjects", nObjects);

        return true;
    }

    public boolean generateBridgeNodePageWiki() {
        log.debug("generateBridgeNodePage");
        int init = 12; // init value to infinite scroll
        int nObjects = 8; //Objects number to return  

        //bread crumbs----------------------------------------------------------
        String nodeId = node.getId();
        String lastbreadcrumbswithchilds = "null";
        List<BreadCrumbs> breadCrumbs = new ArrayList<BreadCrumbs>();
        List<BreadCrumbs> reverseList = new ArrayList<BreadCrumbs>();

        NodeStatic temp = null;
        BreadCrumbs tempB = null;

        try {
            temp = DataHandlerFacade.getNode(nodeId);
            tempB = new BreadCrumbs(temp);
            tempB.setChilds(TreeMenuUtils.getChildren(temp, temp.getId()));
            breadCrumbs.add(tempB);

            while (!TreeMenuUtils.isRootNode(temp) && !TreeMenuUtils.isTaxonomyRootNode(temp)) {
                nodeId = temp.getParentID();
                temp = DataHandlerFacade.getNode(nodeId);
                if (temp != null) {
                    tempB = new BreadCrumbs(temp);
                    tempB.setChilds(TreeMenuUtils.getChildren(temp, temp.getId()));
                    breadCrumbs.add(tempB);
                }
            }

            int size = breadCrumbs.size();
            for (int i = size; i > 0; i--) {
                reverseList.add(breadCrumbs.get(i - 1));
                if (breadCrumbs.get(i - 1).getChilds() != null) {
                    if (breadCrumbs.get(i - 1).getTotalChilds() > 0) {                        
                        lastbreadcrumbswithchilds = breadCrumbs.get(i - 1).getParent().getId();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        //----------------------------------------------------------------------

        if (treeGenerator.isFather()) {
            List<NodeDataObject> childrenToshow = level3;
            BuilderUtil.setBridgeNodeAttributes(childrenToshow);
            childrenToshow = BuilderUtil.setDeafultDescription(childrenToshow, null, true);
            try {
                init = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.node.init"));
                nObjects = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.infinite.scroll.node.nobjects"));
            } catch (NumberFormatException e) {
                log.info("error parsing infinite scroll node data: Default init=12, nObjects=8", e);
            }

            if (childrenToshow.size() >= init) {
                childrenToshow = childrenToshow.subList(0, init);
            } else {
                init = childrenToshow.size();
            }

            BuilderUtil.calculateNodesImageSrc(childrenToshow, this.node.getTaxonomyID());

            context.put("children", childrenToshow);

        } else {
            context.put("emptyNodeMessage", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.empty.bridgenode"));
        }
        context.put("infiniteScrollInit", init);
        context.put("infiniteScrollNobjects", nObjects);
        context.put("breadcrumbs", reverseList);
        context.put("lastbreadcrumbswithchilds", lastbreadcrumbswithchilds);

        return true;
    }

    /**
     * generate the digest info page put the variables in the context TODO: bad
     * code please not check the title in nodedocument
     *
     * @return
     */
    @Override
    public boolean generateDigestPage(DigestInfoObject digestInfo) {
        log.debug("generateDigestPage(NodeStatic node,String docId)");
        String templateStyle = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.templatestyle.name");

        if (templateStyle.equals("wiki")) {
            return generateDigestPageWiki(digestInfo);
        }

        int maxNumberOfCharactersInDescription = 80;
        InfoVersionObject infoToReturn = SentenceUtil.getInfoDocuments(node, digestInfo.getDocId());
        context.put("digestObject", infoToReturn);
        context.put("docId", digestInfo.getDocId());

        try {
            maxNumberOfCharactersInDescription = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.collection.small.maxDescriptionCharacters.digest"));
        } catch (NumberFormatException ex) {
            log.info("error parsiong max number of characters to show, Default maxNumberOfCharactersInDescription: " + maxNumberOfCharactersInDescription, ex);
        }

        NodeDocument nodeDoc = null;
        try {
            nodeDoc = DataHandlerFacade.getNodeDocument(node.getId(), digestInfo.getDocId());
        } catch (Exception e) {
            log.error("Error getting node document");
            nodeDoc = new NodeDocument();
        }

        if (infoToReturn != null) {
            context.put("urlImage", BuilderUtil.calculateLinksImageSrc(digestInfo.getDocId()));
            context.put("urlSource", infoToReturn.getUrl());
            context.put("title", nodeDoc.getDocTitle());
            //another parameters
            int totalUsersFollowingTopic = 0;
            int maxNumberOfUsersToShowFollowingTopic = 10;
            boolean userAlreadyFollowingTopic = false;
            List<NodeUser> nodeUsers = new ArrayList<NodeUser>();

            try {
                nodeUsers = DataHandlerFacade.getNodeUsersByNodeId(node.getId());
            } catch (Exception ex) {
                log.error("Error getting users in a node", ex);
            }

            String timeJoinNode = "";
            long timeStampJoinNode = 0;
            for (NodeUser n : nodeUsers) {
                if (n.getUserId().equals(user.getUserId())) {
                    timeStampJoinNode = n.getTimeStamp();
                    userAlreadyFollowingTopic = true;
                    break;
                }
            }

            //time last join node
            if (timeStampJoinNode != 0) {
                Date date = new Date(timeStampJoinNode);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
                timeJoinNode = simpleDateFormat.format(date);
            }

            List<NodeUser> randomNodeUsers = new ArrayList<NodeUser>();

            if (nodeUsers == null) {
                return false;
            }
            totalUsersFollowingTopic = nodeUsers.size();
            Collections.shuffle(nodeUsers);
            if (nodeUsers.size() <= maxNumberOfUsersToShowFollowingTopic) {
                randomNodeUsers = nodeUsers;
            } else {
                randomNodeUsers = nodeUsers.subList(0, maxNumberOfUsersToShowFollowingTopic);
            }

            //get collection small view
            List<InterestCollection> collectionsRelated = null;
            try {
                collectionsRelated = DataHandlerFacade.getInterestCollectionByNodeId(node.getId());
            } catch (Exception ex) {
                log.error("Error getting collections from nodeId" + node.getId(), ex);
                collectionsRelated = new ArrayList<InterestCollection>();
            }
            if (collectionsRelated != null) {
                Collections.shuffle(collectionsRelated);
            } else {
                collectionsRelated = new ArrayList<InterestCollection>();
            }

            //get the first 6 to show in slide
            int initCollections = 6;
            initCollections = (initCollections < collectionsRelated.size() ? initCollections : collectionsRelated.size());
            collectionsRelated = collectionsRelated.subList(0, initCollections);

            for (int pos = 0; pos < collectionsRelated.size(); pos++) {
                collectionsRelated.get(pos).setInterestCollectionDescription(StringUtil.shrinkString(collectionsRelated.get(pos).getInterestCollectionDescription(), maxNumberOfCharactersInDescription, " ..."));
            }

            totalUsersFollowingTopic = nodeUsers.size();

            /* DISQUS */
            try {
                String disqusKey = UtilEnviroment.getDisqusKey();
                String disqusSecret = UtilEnviroment.getDisqusSecret();


                JSONObject jSONObject = new JSONObject();
                jSONObject.put("id", user.getUserId());
                jSONObject.put("username", user.getName());
                jSONObject.put("email", user.getEmail());
                jSONObject.put("avatar", "http://graph.facebook.com/" + user.getFacebookCredentials().getProperty("fbId") + "/picture?type=small");
                jSONObject.put("url", UtilEnviroment.getContext() + "/ProfileCreation?userId=" + user.getUserId());
                Date date = new Date();
                Long timeStamp = date.getTime();

                timeStamp = timeStamp / 1000L; //divide by 1000 to remove the milliseconds

                String jsonString = jSONObject.toString();

                byte[] message = Base64.encode(jsonString);
                String messageWithTimeStamp = new String(message);

                messageWithTimeStamp += " " + String.valueOf(timeStamp);

                String disqusMessage = new String(message);

                byte[] keyBytes = disqusSecret.getBytes();
                SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
                // Get an hmac_sha1 Mac instance and initialize with the signing key
                Mac mac = Mac.getInstance("HmacSHA1");
                mac.init(signingKey);
                // Compute the hmac on input data bytes
                byte[] rawHmac = mac.doFinal(messageWithTimeStamp.getBytes());
                // Convert raw bytes to Hex
                byte[] hexBytes = new Hex().encode(rawHmac);
                //  Covert array of Hex bytes to a String
                String hmac = new String(hexBytes, "UTF-8");

                context.put("disqusPublicKey", disqusKey);
                context.put("disqusTimeStamp", timeStamp);
                context.put("disqusMessage", disqusMessage);
                context.put("disqusHMAC", hmac);

            } catch (Exception ex) {
                log.error("Exception setting the disqus Single Sign On, message: " + ex.getMessage());
            }
            context.put("totalUsersFollowingTopic", totalUsersFollowingTopic);
            context.put("randomNodeUsers", randomNodeUsers);
            context.put("userAlreadyFollowingTopic", userAlreadyFollowingTopic);
            context.put("collectionsRelated", collectionsRelated);
            context.put("timeJoinNode", timeJoinNode);

            return true;
        } else {
            return false;
        }
    }

    public boolean generateDigestPageWiki(DigestInfoObject digestInfo) {
        log.debug("generateDigestPage(NodeStatic node,String docId)");

        int maxNumberOfCharactersInDescription = 80;
        InfoVersionObject infoToReturn = SentenceUtil.getInfoDocuments(node, digestInfo.getDocId());
        
        infoToReturn.setDocDigest(infoToReturn.getDocDigest().replaceAll("<br>", ""));
        
        context.put("digestObject", infoToReturn);
        context.put("docId", digestInfo.getDocId());
        DocumentNode doc = null;

        //bread crumbs----------------------------------------------------------
        String nodeId = digestInfo.getNodeId();
        String lastbreadcrumbswithchilds = "null";
        List<BreadCrumbs> breadCrumbs = new ArrayList<BreadCrumbs>();
        List<BreadCrumbs> reverseList = new ArrayList<BreadCrumbs>();

        NodeStatic temp = null;
        BreadCrumbs tempB = null;

        try {
            temp = DataHandlerFacade.getNode(nodeId);
            tempB = new BreadCrumbs(temp);
            tempB.setChilds(TreeMenuUtils.getChildren(temp, temp.getId()));
            breadCrumbs.add(tempB);

            while (!TreeMenuUtils.isRootNode(temp) && !TreeMenuUtils.isTaxonomyRootNode(temp)) {
                nodeId = temp.getParentID();
                temp = DataHandlerFacade.getNode(nodeId);
                if (temp != null) {
                    tempB = new BreadCrumbs(temp);
                    tempB.setChilds(TreeMenuUtils.getChildren(temp, temp.getId()));
                    breadCrumbs.add(tempB);
                }
            }

            int size = breadCrumbs.size();
            for (int i = size; i > 0; i--) {
                reverseList.add(breadCrumbs.get(i - 1));
                if (breadCrumbs.get(i - 1).getChilds() != null) {
                    if (breadCrumbs.get(i - 1).getTotalChilds() > 0) {
                        lastbreadcrumbswithchilds = breadCrumbs.get(i - 1).getParent().getId();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        //----------------------------------------------------------------------

        try {
            maxNumberOfCharactersInDescription = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.collection.small.maxDescriptionCharacters.digest"));
        } catch (NumberFormatException ex) {
            log.info("error parsiong max number of characters to show, Default maxNumberOfCharactersInDescription: " + maxNumberOfCharactersInDescription, ex);
        }
        if (infoToReturn != null) {
            context.put("urlImage", BuilderUtil.calculateLinksImageSrc(digestInfo.getDocId()));
            context.put("urlSource", infoToReturn.getUrl());
            context.put("title", infoToReturn.getTitle());
            //another parameters
            int totalUsersFollowingTopic = 0;
            int maxNumberOfUsersToShowFollowingTopic = 10;
            boolean userAlreadyFollowingTopic = false;
            List<NodeUser> nodeUsers = new ArrayList<NodeUser>();

            try {
                nodeUsers = DataHandlerFacade.getNodeUsersByNodeId(node.getId());
            } catch (Exception ex) {
                log.error("Error getting users in a node", ex);
            }

            String timeJoinNode = "";
            long timeStampJoinNode = 0;
            for (NodeUser n : nodeUsers) {
                if (n.getUserId().equals(user.getUserId())) {
                    timeStampJoinNode = n.getTimeStamp();
                    userAlreadyFollowingTopic = true;
                    break;
                }
            }

            //time last join node
            if (timeStampJoinNode != 0) {
                Date date = new Date(timeStampJoinNode);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
                timeJoinNode = simpleDateFormat.format(date);
            }

            List<NodeUser> randomNodeUsers = new ArrayList<NodeUser>();

            if (nodeUsers == null) {
                return false;
            }
            totalUsersFollowingTopic = nodeUsers.size();
            Collections.shuffle(nodeUsers);
            if (nodeUsers.size() <= maxNumberOfUsersToShowFollowingTopic) {
                randomNodeUsers = nodeUsers;
            } else {
                randomNodeUsers = nodeUsers.subList(0, maxNumberOfUsersToShowFollowingTopic);
            }

            //get collection small view
            List<InterestCollection> collectionsRelated = null;
            try {
                collectionsRelated = DataHandlerFacade.getInterestCollectionByNodeId(node.getId());
            } catch (Exception ex) {
                log.error("Error getting collections from nodeId" + node.getId(), ex);
                collectionsRelated = new ArrayList<InterestCollection>();
            }
            if (collectionsRelated != null) {
                Collections.shuffle(collectionsRelated);
            } else {
                collectionsRelated = new ArrayList<InterestCollection>();
            }

            //get the first 6 to show in slide
            int initCollections = 6;
            initCollections = (initCollections < collectionsRelated.size() ? initCollections : collectionsRelated.size());
            collectionsRelated = collectionsRelated.subList(0, initCollections);

            for (int pos = 0; pos < collectionsRelated.size(); pos++) {
                collectionsRelated.get(pos).setInterestCollectionDescription(StringUtil.shrinkString(collectionsRelated.get(pos).getInterestCollectionDescription(), maxNumberOfCharactersInDescription, " ..."));
            }

            totalUsersFollowingTopic = nodeUsers.size();

            /* DISQUS */
            try {
                String disqusKey = UtilEnviroment.getDisqusKey();
                String disqusSecret = UtilEnviroment.getDisqusSecret();


                JSONObject jSONObject = new JSONObject();
                jSONObject.put("id", user.getUserId());
                jSONObject.put("username", user.getName());
                jSONObject.put("email", user.getEmail());
                jSONObject.put("avatar", "http://graph.facebook.com/" + user.getFacebookCredentials().getProperty("fbId") + "/picture?type=small");
                jSONObject.put("url", UtilEnviroment.getContext() + "/ProfileCreation?userId=" + user.getUserId());
                Date date = new Date();
                Long timeStamp = date.getTime();

                timeStamp = timeStamp / 1000L; //divide by 1000 to remove the milliseconds

                String jsonString = jSONObject.toString();

                byte[] message = Base64.encode(jsonString);
                String messageWithTimeStamp = new String(message);

                messageWithTimeStamp += " " + String.valueOf(timeStamp);

                String disqusMessage = new String(message);

                byte[] keyBytes = disqusSecret.getBytes();
                SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
                // Get an hmac_sha1 Mac instance and initialize with the signing key
                Mac mac = Mac.getInstance("HmacSHA1");
                mac.init(signingKey);
                // Compute the hmac on input data bytes
                byte[] rawHmac = mac.doFinal(messageWithTimeStamp.getBytes());
                // Convert raw bytes to Hex
                byte[] hexBytes = new Hex().encode(rawHmac);
                //  Covert array of Hex bytes to a String
                String hmac = new String(hexBytes, "UTF-8");

                context.put("disqusPublicKey", disqusKey);
                context.put("disqusTimeStamp", timeStamp);
                context.put("disqusMessage", disqusMessage);
                context.put("disqusHMAC", hmac);

            } catch (Exception ex) {
                log.error("Exception setting the disqus Single Sign On, message: " + ex.getMessage());
            }
            context.put("totalUsersFollowingTopic", totalUsersFollowingTopic);
            context.put("randomNodeUsers", randomNodeUsers);
            context.put("userAlreadyFollowingTopic", userAlreadyFollowingTopic);
            context.put("collectionsRelated", collectionsRelated);
            context.put("timeJoinNode", timeJoinNode);
            context.put("breadcrumbs", reverseList);
            context.put("lastbreadcrumbswithchilds", lastbreadcrumbswithchilds);

            return true;
        } else {
            return false;
        }
    }

    /**
     * generate the profile page put the variables in the context
     *
     * @return
     */
    @Override
    public boolean generateProfilePage(User userToView, Map<String, Object> parameters) {
        if (userToView == null) {
            log.error("Error generateProfilePage: the userToView is null");
            return false;
        }
        List<UserNode> listUserNode = new ArrayList<UserNode>();
        List<FollowedUser> listFollowedUser = new ArrayList<FollowedUser>();
        List<FollowingUser> listFollowingUser = new ArrayList<FollowingUser>();
        Boolean userAlreadyFollowing = false;
        int maxNumberOfUsersFollowingToShow = 25;
        int maxNumberOfFollowersToShow = 10;

        int init = 10; // init value to infinite scroll
        int nObjects = 5; //Objects number to return 

        int maxNumberOfCharactersInDescription = 200;

        try {
            init = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.feed.number.init"));
            nObjects = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.feed.number.nobjects"));
        } catch (NumberFormatException e) {
            log.info("error parsing infinite scroll node data: Default init=12, nObjects=8", e);
        }

        try {
            maxNumberOfUsersFollowingToShow = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facepile.users.following.profile"));
            maxNumberOfFollowersToShow = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facepile.users.follower.profile"));
        } catch (NumberFormatException e) {
            log.info("error parsing number of users to show. Default maxNumberOfUsersToShow: 10", e);
        }

        try {
            maxNumberOfCharactersInDescription = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.collection.large.maxDescriptionCharacters.profile"));
        } catch (NumberFormatException ex) {
            log.info("error parsiong max number of characters to show, Default maxNumberOfCharactersInDescription: " + maxNumberOfCharactersInDescription);
        }

        //get the nodes by the user
        try {
            listUserNode = DataHandlerFacade.getUserNodesByUserId(userToView.getUserId());
        } catch (Exception ex) {
            log.error("Error getting nodes by userToView", ex);
        }

        //get followers
        try {
            listFollowedUser = DataHandlerFacade.getFollowerUsersByFollowedUserId(userToView.getUserId());
        } catch (Exception ex) {
            log.error("Error getting nodes by userToView", ex);
        }

        // get following users
        try {
            listFollowingUser = DataHandlerFacade.getFollowingUsersByFollowerUserId(userToView.getUserId());
        } catch (Exception ex) {
            log.error("Error getting nodes by userToView", ex);
        }

        //ramdom and the first data in the vector
        if (listFollowingUser.size() > maxNumberOfUsersFollowingToShow) {
            Collections.shuffle(listFollowingUser);
            listFollowingUser = listFollowingUser.subList(0, maxNumberOfUsersFollowingToShow);
        }

        if (listFollowedUser.size() > maxNumberOfFollowersToShow) {
            Collections.shuffle(listFollowedUser);
            listFollowedUser = listFollowedUser.subList(0, maxNumberOfFollowersToShow);
        }
        long timeStampUserStartFollowing = 0;
        String timeUserStartFollowing = "";
        for (FollowedUser followedUser : listFollowedUser) {
            if (followedUser.getFollowingUserId().equals(user.getUserId())) {
                timeStampUserStartFollowing = followedUser.getTimeStamp();
                userAlreadyFollowing = true;
                break;
            }
        }
        if (timeStampUserStartFollowing != 0) {
            Date date = new Date(timeStampUserStartFollowing);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
            timeUserStartFollowing = simpleDateFormat.format(date);
        }

        List<UserFeed> userFeeds = new ArrayList<UserFeed>();
        try {
            //TODO: get the events log
            userFeeds = DataHandlerFacade.getUserFeed(userToView.getUserId());

            if (userFeeds.size() < init) {
                init = userFeeds.size();
            }
            userFeeds = userFeeds.subList(0, init);


        } catch (Exception ex) {
            log.error("Error getting userFeeds", ex);
        }

        //get follows collections
        List<InterestCollection> collectionfollows = null;
        try {
            collectionfollows = DataHandlerFacade.getInterestCollectionFollowedByUserId(userToView.getUserId());
        } catch (Exception ex) {
            log.error("Error getting collection follow by user", ex);
        }
        List<InterestCollection> collectionfollowsWithoutMe = new ArrayList<InterestCollection>();
        if (collectionfollows != null) {
            for (InterestCollection interest : collectionfollows) {
                if (!interest.getOwnerId().equals(userToView.getUserId())) {
                    collectionfollowsWithoutMe.add(interest);
                }
            }
            Collections.shuffle(collectionfollowsWithoutMe);
        }

        //get the first 6 to show in slide
        int initCollections = 6;
        initCollections = (initCollections < collectionfollowsWithoutMe.size() ? initCollections : collectionfollowsWithoutMe.size());
        collectionfollowsWithoutMe = collectionfollowsWithoutMe.subList(0, initCollections);

        //get my collections
        List<InterestCollection> mycollections = new ArrayList<InterestCollection>();
        List<String> searchs = new ArrayList<String>();
        List<String> replacements = new ArrayList<String>();
        int interescollectionelements = 0;
        String textDigest = "";

        searchs.add(",");
        searchs.add(";");
        searchs.add("!");
        searchs.add("\"");
        searchs.add(".");

        replacements.add(", ");
        replacements.add("; ");
        replacements.add("! ");
        replacements.add("\"");
        replacements.add(". ");

        try {
            mycollections = DataHandlerFacade.getInterestCollectionOwnedByUserId(userToView.getUserId(), true);

            for (int pos = 0; pos < mycollections.size(); pos++) {
                interescollectionelements = mycollections.get(pos).getInterestCollectionElements().size();

                for (int i = 0; i < interescollectionelements; i++) {
                    textDigest = mycollections.get(pos).getInterestCollectionElements().get(i).getDigestText();
                    mycollections.get(pos).getInterestCollectionElements().get(i).setDigestText(DigestUtil.FilterDigestSpaces(textDigest, searchs, replacements));
                }

                mycollections.get(pos).setInterestCollectionDescription(StringUtil.shrinkString(mycollections.get(pos).getInterestCollectionDescription(), maxNumberOfCharactersInDescription, " ..."));
            }


        } catch (Exception ex) {
            log.error("Error getting collection follow by user", ex);
            mycollections = new ArrayList<InterestCollection>();
        }


        //put the variables in the context
        context.put("userToView", userToView);
        context.put("listUserNode", listUserNode);
        context.put("listFollowedUser", listFollowedUser);
        context.put("listFollowing", listFollowingUser);
        context.put("userAlreadyFollowing", userAlreadyFollowing);
        context.put("maxNumberOfUsersToShow", maxNumberOfUsersFollowingToShow);
        context.put("userFeeds", userFeeds);
        context.put("init", init);
        context.put("nObjects", nObjects);
        context.put("collectionfollows", collectionfollowsWithoutMe);
        context.put("mycollections", mycollections);
        context.put("timeUserStartFollowing", timeUserStartFollowing);

        //parameters
        if (parameters.get("taxonomyName") != null) {
            context.put("taxonomyName", parameters.get("taxonomyName"));
        }
        if (parameters.get("taxonomyId") != null) {
            context.put("taxonomyId", parameters.get("taxonomyId"));
        }
        context.put("timeStampServer", new Date().getTime());
        return true;
    }

    @Override
    public boolean generateHomePage(Map<String, Object> parameters) {
        String templateStyle = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.templatestyle.name");

        if (templateStyle.equals("wiki")) {
            return generateHomePageWiki(parameters);
        }

        List<HomeLink> links = HomeLink.getHomeLinks();
        //parameters
        if (parameters.get("taxonomyName") != null) {
            context.put("taxonomyName", parameters.get("taxonomyName"));
        }
        if (parameters.get("taxonomyId") != null) {
            context.put("taxonomyId", parameters.get("taxonomyId"));
        }

        List<String> searchs = new ArrayList<String>();
        List<String> replacements = new ArrayList<String>();
        String digest = "";

        searchs.add(",");
        searchs.add(";");
        searchs.add("!");
        searchs.add("\"");
        searchs.add(".");

        replacements.add(", ");
        replacements.add("; ");
        replacements.add("! ");
        replacements.add("\"");
        replacements.add(". ");

        for (int i = 0; i < links.size(); i++) {
            digest = links.get(i).getDigest();
            links.get(i).setDigest(DigestUtil.FilterDigestSpaces(digest, searchs, replacements));
        }

        context.put("links", links);
        return true;
    }

    public boolean generateHomePageWiki(Map<String, Object> parameters) {
        //parameters
        if (parameters.get("taxonomyName") != null) {
            context.put("taxonomyName", parameters.get("taxonomyName"));
        }
        if (parameters.get("taxonomyId") != null) {
            context.put("taxonomyId", parameters.get("taxonomyId"));
        }

        return true;
    }

    /**
     * generate the Collection page
     *
     * @return
     */
    @Override
    public boolean generateCollectionPage(InterestCollection interestCollection) {
        int maxNumberOfUsersToShowFollowingCollection;
        int maxNumberOfRelatedCollectionsToShow;
        int maxNumberOfCharactersInDescription = 200;
        User userOwner = null;
        int followersCount = 0;
        List<FollowingInterestCollection> followersOfCollection = null;
        boolean userArleadyFollowingCollection = false;

        try {

            followersCount = DataHandlerFacade.getFollowingInterestCollectionCount(interestCollection.getInterestCollectionId());
            maxNumberOfUsersToShowFollowingCollection = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facepile.collection.following", "10"));
            maxNumberOfRelatedCollectionsToShow = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facepile.collection.related", "10"));
            userOwner = DataHandlerFacade.getUser(interestCollection.getOwnerId());
            followersOfCollection = DataHandlerFacade.getFollowingInterestCollectionByCollectionId(interestCollection.getInterestCollectionId());

            if (followersOfCollection.size() > maxNumberOfUsersToShowFollowingCollection) {
                Collections.shuffle(followersOfCollection);
                followersOfCollection = followersOfCollection.subList(0, maxNumberOfUsersToShowFollowingCollection);
            }

            if (user.getUserId() != null) {
                List<InterestCollection> interestCollectionsFollowedByUser = DataHandlerFacade.getInterestCollectionFollowedByUserId(user.getUserId());
                for (InterestCollection ic : interestCollectionsFollowedByUser) {
                    if (ic.getInterestCollectionId().equals(interestCollection.getInterestCollectionId())) {
                        userArleadyFollowingCollection = true;
                        InterestCollectionFollowed icf = DataHandlerFacade.getInterestCollectionFollowedByUserIdAndCollectionId(user.getUserId(), interestCollection.getInterestCollectionId());
                        String joinAgo = "";
                        if (icf != null) {
                            Date date = new Date(icf.getTimeStamp());
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
                            joinAgo = simpleDateFormat.format(date);
                        }
                        context.put("followDate", joinAgo);
                        break;
                    }
                }
            }

            Map relatedNodes = new HashMap<String, String>();

            for (InterestCollectionElement ice : interestCollection.getInterestCollectionElements()) {
                if (ice.getNodeId() != null) {
                    relatedNodes.put(ice.getNodeId(), ice.getNodeName());
                }
            }

            Map relatedCollections = new HashMap<String, InterestCollection>();

            try {
                maxNumberOfCharactersInDescription = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.collection.large.maxDescriptionCharacters.profile"));
            } catch (NumberFormatException ex) {
                log.info("error parsiong max number of characters to show, Default maxNumberOfCharactersInDescription: " + maxNumberOfCharactersInDescription);
            }

            Iterator it = relatedNodes.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                if (pairs.getKey() != null) {
                    for (InterestCollection ic : DataHandlerFacade.getInterestCollectionByNodeId(pairs.getKey().toString())) {
                        if (!ic.getInterestCollectionId().equals(interestCollection.getInterestCollectionId())) {
                            if (!ic.getIsPrivate()) {
                                ic.setInterestCollectionDescription(StringUtil.shrinkString(ic.getInterestCollectionDescription(), maxNumberOfCharactersInDescription, "..."));
                                relatedCollections.put(ic.getInterestCollectionId(), ic);
                            }
                        }
                    }

                }
                // it.remove(); // avoids a ConcurrentModificationException
            }

            int size = interestCollection.getInterestCollectionElements().size();
            String digestText = "";
            List<String> searchs = new ArrayList<String>();
            List<String> replacements = new ArrayList<String>();
            searchs.add(",");
            searchs.add(";");
            searchs.add("!");
            searchs.add("\"");
            searchs.add(".");

            replacements.add(", ");
            replacements.add("; ");
            replacements.add("! ");
            replacements.add("\"");
            replacements.add(". ");

            for (int i = 0; i < size; i++) {
                digestText = interestCollection.getInterestCollectionElements().get(i).getDigestText();
                interestCollection.getInterestCollectionElements().get(i).setDigestText(DigestUtil.FilterDigestSpaces(digestText, searchs, replacements));
            }

            context.put("interestCollection", interestCollection);
            context.put("userOwner", userOwner);
            context.put("followersCount", followersCount);
            context.put("followersOfCollection", followersOfCollection);
            context.put("userArleadyFollowingCollection", userArleadyFollowingCollection);
            context.put("maxNumberOfUsersToShowFollowingCollection", maxNumberOfUsersToShowFollowingCollection);
            context.put("maxNumberOfRelatedCollectionsToShow", maxNumberOfRelatedCollectionsToShow);
            context.put("relatedNodes", relatedNodes);
            context.put("relatedCollections", relatedCollections);

        } catch (Exception ex) {
            log.error("Exception in generateCollectionPage :" + ex.getMessage());
        }
        return true;
    }

    /**
     * generate the application pages - about / privacy
     *
     * @return
     */
    @Override
    public boolean generateApplicationPage() {
        return true;
    }
}
