package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.beans.DigestInfoObject;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import com.linkapedia.builderpage.bo.comparators.NodeDataComparator;
import com.linkapedia.builderpage.bo.models.IBuilderPage;
import com.linkapedia.builderpage.util.TreeMenuUtils;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * controller of the SERVLETS
 *
 * @author andres
 */
public class BuilderPageController implements IBuilderPage {

    private static final Logger log = Logger.getLogger(BuilderPageController.class);
    private BuildPageBo builderPageBo; //implementation of the logic

    public BuilderPageController(BuildPageBo builderPageBo) {
        this.builderPageBo = builderPageBo;
    }

    /**
     * validate the data to build the node page (bridge and link node)
     *
     * @param nodeId
     * @param user
     * @return
     */
    public boolean createNodePage(String nodeId, User user) {
        log.debug("BuilderPageValidator:createNodePage");
        NodeStatic nodeStatic = null;
        try {
            nodeStatic = DataHandlerFacade.getNode(nodeId);
        } catch (Exception e) {
            log.error("Error getting NodeStatic", e);
        }
        if (nodeStatic != null) {
            return this.builderPageBo.createNodePage(nodeStatic, user);
        } else {
            log.error("nodeId: " + nodeId + " not found in DynamoDB");
            return false;
        }
    }

    /**
     * validate the data to build the digest page
     *
     * @param digestInfo
     * @param user
     * @return
     */
    public boolean createDigestPage(DigestInfoObject digestInfo, User user) {
        if (digestInfo == null) {
            log.error("Parameter digestInfo is missing");
            return false;
        }
        log.debug("BuilderPageValidator:createDigestPage");
        NodeStatic nodeStatic = null;
        try {
            nodeStatic = DataHandlerFacade.getNode(digestInfo.getNodeId());
        } catch (Exception e) {
            log.error("Error getting NodeStatic", e);
        }
        if (nodeStatic != null) {
            return this.builderPageBo.createDigestPage(nodeStatic, digestInfo, user);
        } else {
            return false;
        }
    }

    /**
     * validate the data to build the profile page
     *
     * @param user
     * @return
     */
    public boolean createProfilePage(User currentuser, User user, Map<String, Object> parameters) {
        log.debug("BuilderPageValidator:createProfilePage");
        if (user == null) {
            log.info("Missing parameters in createProfilePage");
            return false;
        }
        NodeStatic nodeStatic = null;
        try {
            nodeStatic = DataHandlerFacade.getNode(UtilEnviroment.getRootIdByEnv());
        } catch (Exception e) {
            log.error("Error getting NodeStatic", e);
        }
        return builderPageBo.createProfilePage(nodeStatic, currentuser, user, parameters);
    }

    /**
     * validate the data to build the home page
     *
     * @param user
     * @return
     */
    public boolean createHomePage(User user, Map<String, Object> parameters) {
        log.debug("BuilderPageValidator:createHomePage");
        NodeStatic nodeStatic = null;
        try {
            nodeStatic = DataHandlerFacade.getNode(UtilEnviroment.getRootIdByEnv());
        } catch (Exception e) {
            log.error("Error getting NodeStatic", e);
        }
        if (nodeStatic != null) {
            try {
                //get the first child hack home page: order here to show the first child selected
                LinkedList<NodeDataObject> childrenNodes = TreeMenuUtils.getList(nodeStatic.getChildrenIDs(), nodeStatic.getChildrenNames(), nodeStatic.getChildrenIndexWithinParent());//sons
                NodeDataComparator com = new NodeDataComparator();
                if (childrenNodes != null) {
                    Collections.sort(childrenNodes, com);
                }
                nodeStatic = DataHandlerFacade.getNode(childrenNodes.get(0).getNodeId());
            } catch (Exception ex) {
                log.error("Error getting nodestatic in home creation: ", ex);
            }
            return this.builderPageBo.createHomePage(nodeStatic, user, parameters);
        } else {
            return false;
        }
    }

    /**
     * validate the data to build the collection page
     *
     * @param string
     * @param user
     * @return
     */
    public boolean createCollectionPage(String collectionId, User user) {
        if (collectionId == null) {
            log.error("Parameter collectionId is missing");
            return false;
        }

        log.debug("BuilderPageValidator:createCollectionPage");
        InterestCollection interestCollection = null;
        try {
            interestCollection = DataHandlerFacade.getInterestCollection(collectionId, true);
        } catch (Exception e) {
            log.error("Error getting Interest Collection", e);
        }
        NodeStatic nodeStatic = null;
        try {
            nodeStatic = DataHandlerFacade.getNode(UtilEnviroment.getRootIdByEnv());
        } catch (Exception e) {
            log.error("Error getting NodeStatic", e);
        }
        if (interestCollection != null) {
            return this.builderPageBo.createCollectionPage(nodeStatic, interestCollection, user);
        } else {
            return false;
        }
    }

    /**
     * validate the data to build application page
     *
     * @param user
     * @return
     */
    public boolean createApplicationPage(User user, Map<String, Object> parameters) {
        if (!"about".equals(parameters.get("pageType")) && (!"privacy".equals(parameters.get("pageType")))) {
            log.error("Unsopported pageType: " + parameters.get("pageType"));
            return false;
        }
        NodeStatic nodeStatic = null;
        try {
            nodeStatic = DataHandlerFacade.getNode(UtilEnviroment.getRootIdByEnv());
        } catch (Exception e) {
            log.error("Error getting NodeStatic", e);
        }
        return this.builderPageBo.createApplicationPage(nodeStatic, user, parameters);
    }
}
