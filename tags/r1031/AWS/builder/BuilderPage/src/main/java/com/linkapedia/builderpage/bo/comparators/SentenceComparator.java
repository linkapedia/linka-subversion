package com.linkapedia.builderpage.bo.comparators;

import com.linkapedia.core.DocumentSentence;
import java.util.Comparator;

/**
 *
 * @author andres
 */
public class SentenceComparator implements Comparator<DocumentSentence> {

    @Override
    public int compare(DocumentSentence o1, DocumentSentence o2) {
        double totalScore1 = o1.getScoreByNodeTitle() + o1.getScoreByMustHaves() + o1.getScoreBySignatures();
        double totalScore2 = o2.getScoreByNodeTitle() + o2.getScoreByMustHaves() + o2.getScoreBySignatures();
        return (totalScore1 < totalScore2) ? 1 : (totalScore1 > totalScore2) ? -1 : 0;
    }
}
