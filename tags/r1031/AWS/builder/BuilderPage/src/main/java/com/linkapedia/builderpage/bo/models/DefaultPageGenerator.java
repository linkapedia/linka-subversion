package com.linkapedia.builderpage.bo.models;

import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.Taxonomy;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.TreeMenuGenerator;
import com.linkapedia.builderpage.bo.beans.DigestInfoObject;
import com.linkapedia.builderpage.bo.beans.LinkObject;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import com.linkapedia.builderpage.bo.template.engine.VelocityCreator;
import com.linkapedia.builderpage.util.BuilderUtil;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

/**
 *
 * @author andres
 */
public abstract class DefaultPageGenerator {

    private static final Logger log = Logger.getLogger(DefaultPageGenerator.class);
    protected LinkedList<NodeDataObject> level1;
    protected LinkedList<NodeDataObject> level2;
    protected LinkedList<NodeDataObject> level3;
    protected TreeMenuGenerator treeGenerator;
    protected NodeStatic node;
    protected User user;
    //template fields
    protected VelocityContext context;
    protected Template template;

    /**
     * constructor
     *
     * @param node
     * @param user
     * @param templateName
     */
    public DefaultPageGenerator(NodeStatic node, User user, String templateName) {
        this.node = node;
        this.user = user;
        createTemplate(templateName);
    }

    /**
     * configure the template
     *
     * @param t
     */
    private void createTemplate(String t) {
        //init the template engine
        try {
            //configure the template
            template = VelocityCreator.getTemplate(t);
            context = VelocityCreator.getContext(true);
            if (template == null || context == null) {
                log.error("Error configuring the template");
            }
        } catch (Exception ex) {
            log.error("Error configuring the template", ex);
        }
    }

    /**
     * parameters for the MENU
     */
    public void putMenuObjects() {
        if (this.node == null) {
            log.error("DefaultPageGenerator: putMenuObjects -> node is null");
            return;
        }
        treeGenerator = new TreeMenuGenerator(this.node);
        treeGenerator.buildMenu();
        this.level1 = treeGenerator.getLevel(TreeMenuGenerator.TREE_LEVEL1);
        this.level2 = treeGenerator.getLevel(TreeMenuGenerator.TREE_LEVEL2);
        this.level3 = treeGenerator.getLevel(TreeMenuGenerator.TREE_LEVEL3);
        //parameters for the tree
        context.put("level1", level1);
        context.put("level2", level2);
        context.put("level3", level3);

        level1 = BuilderUtil.setNameEncode(level1);
        level2 = BuilderUtil.setNameEncode(level2);
        level3 = BuilderUtil.setNameEncode(level3);


        context.put("hasChild", treeGenerator.isFather());
        context.put("showLevel1", treeGenerator.isRoot());
    }

    /**
     * PageNode parameters
     */
    public void putPageNodeObjects() {
        if (this.node == null) {
            log.error("DefaultPageGenerator: putPageNodeObjects -> node is null");
            return;
        }
        Taxonomy tax;
        try {
            tax = DataHandlerFacade.getTaxonomy(node.getTaxonomyID());
        } catch (Exception ex) {
            log.error("Error getting the taxonomy object", ex);
            return;
        }
        //if is  nodeRoot show taxonomy description else show node description
        if (node.getId().equals(tax.getRootNodeID())) {
            context.put("nodeDesc", tax.getDescription());
        } else {
            String nodeDescription = node.getDescription();
            if (nodeDescription == null) {
                nodeDescription = "";
            } else {
                nodeDescription = nodeDescription.trim();
                if (!"".equals(nodeDescription)) {
                    nodeDescription = nodeDescription + " ...";
                } else {
                    nodeDescription = "";
                }
            }
            context.put("nodeDesc", nodeDescription);
        }
        context.put("node", this.node);
        context.put("taxonomy", tax);
    }

    /**
     * others parameters
     */
    public void putGeneralObjects() {
        if (this.user == null) {
            log.error("DefaultPageGenerator: putGeneralObjects -> user is null");
            return;
        }
        context.put("user", user);
        //facebook parameters
        context.put("clientId", UtilEnviroment.getFaceBookClientId());
        //environment parameter
        context.put("environment", UtilEnviroment.getEnv());
        if (UtilEnviroment.isBuddhism() || UtilEnviroment.isWine()) {
            context.put("searchQuery2", false);
        } else {
            context.put("searchQuery2", true);
        }
        context.put("context", UtilEnviroment.getContext());
        context.put("kissMetricId", UtilEnviroment.getKissmetricId());
    }

    /**
     * merge the context and the template
     *
     * @return
     */
    public String mergeContext() {
        StringWriter sw = new StringWriter();
        try {
            template.merge(context, sw);
            //stringPage contain the page with the data
            return sw.toString();
        } catch (ResourceNotFoundException ex) {
            log.error("Error Creating page: ResourceNotFoundException", ex);
        } catch (ParseErrorException ex) {
            log.error("Error Creating page: ParseErrorException", ex);
        } catch (MethodInvocationException ex) {
            log.error("Error Creating page: MethodInvocationException", ex);
        } catch (IOException ex) {
            log.error("Error Creating page: IOException", ex);
        } catch (Exception ex) {
            log.error("Error Creating page: Exception", ex);
        } finally {
            if (sw != null) {
                try {
                    sw.close();
                } catch (IOException ex) {
                    log.error("Error closing StringWriter", ex);
                }
            }
        }
        return "";
    }

    //abstract methods
    public abstract boolean generateProfilePage(User user, Map<String, Object> parameters);

    public abstract boolean generateDigestPage(DigestInfoObject digestInfo);        

    public abstract boolean generateBridgeNodePage();

    public abstract boolean generateLinksNodePage(List<LinkObject> documents);        

    public abstract boolean generateHomePage(Map<String, Object> parameters);

    public abstract boolean generateCollectionPage(InterestCollection interestCollection);

    public abstract boolean generateApplicationPage();
}
