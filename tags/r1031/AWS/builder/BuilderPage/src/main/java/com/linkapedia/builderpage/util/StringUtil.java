package com.linkapedia.builderpage.util;

/**
 *
 * @author andres
 */
public class StringUtil {

    public static String sanitizeString(String line) {
        String normalized_string = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(line);//unesscape html characters
        if (normalized_string != null) {
            normalized_string = normalized_string.trim();
            normalized_string = normalized_string.replaceAll("\\'", "");
            normalized_string = normalized_string.replaceAll("\\\"", "");
        }
        return normalized_string;
    }

    public static String reverseString(String value) {
        StringBuilder corpusIdReverse = new StringBuilder();
        corpusIdReverse.append(value);
        corpusIdReverse = corpusIdReverse.reverse();
        return corpusIdReverse.toString();
    }

    //returns a text with a maxNumberOfChars but not cutting a word in the middle
    //optional you can pass chars to add at the end of the cutted text, this would be included in the maxNumberOfChars
    public static String shrinkString(String text, int maxNumberOfChars, String charsToAddAtTheEnd) {
        if (text == null) {
            return null;
        }
        int initTextSize = text.length();
        int charsToAddSize = 0;

        if (charsToAddAtTheEnd != null) {
            charsToAddSize = charsToAddAtTheEnd.length();
        }

        if (initTextSize <= maxNumberOfChars) {
            return text;
        }

        text = text.substring(0, maxNumberOfChars);
        int lastIndexOfSpace = text.lastIndexOf(" ");
        int finalIndexToCut = lastIndexOfSpace;


        while (text.length() > 0) {
            text = text.substring(0, finalIndexToCut);
            if (finalIndexToCut + charsToAddSize <= maxNumberOfChars) {
                if (charsToAddAtTheEnd != null) {
                    text = text + charsToAddAtTheEnd;
                }
                return text;
            }
            lastIndexOfSpace = text.lastIndexOf(" ");
            finalIndexToCut = lastIndexOfSpace;
        }

        return null;
    }
}
