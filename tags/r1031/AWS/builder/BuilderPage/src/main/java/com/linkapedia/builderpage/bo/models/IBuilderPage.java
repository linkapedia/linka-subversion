package com.linkapedia.builderpage.bo.models;

import com.intellisophic.linkapedia.api.beans.User;
import com.linkapedia.builderpage.bo.beans.DigestInfoObject;
import java.util.Map;

/**
 *
 * @author andres
 */
public interface IBuilderPage {

    public boolean createNodePage(String nodeId, User user);

    public boolean createDigestPage(DigestInfoObject digestInfo, User user);

    public boolean createProfilePage(User currentuserUser, User user, Map<String, Object> parameters);

    public boolean createHomePage(User user, Map<String, Object> parameters);

    public boolean createApplicationPage(User user, Map<String, Object> parameters);

    public boolean createCollectionPage(String collectionId, User user);
}
