package com.intellisophic.linkapedia.security;

import com.linkapedia.builderpage.util.UtilEnviroment;
import java.io.IOException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Bancolombia
 */
public class IndexBean {

    /**
     * Creates a new instance of LoginBean
     */
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(IndexBean.class);
    private String login;
    private String password;
    FacesContext context;

    public IndexBean() throws IOException {
        log.debug("call index.jsp");
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String singin() {
        try {
            boolean termsAccepted = false;
            String tinyNodeUrl = UtilEnviroment.getContext() + "/home";
            context = FacesContext.getCurrentInstance();
            if (UtilEnviroment.auth(getLogin(), getPassword())) {
                termsAccepted = true;
                context.getExternalContext().getSessionMap().put("terms", termsAccepted);
                context.getExternalContext().redirect(tinyNodeUrl);
                return "success";
            } else {
                context.addMessage("Error", new FacesMessage("The credential are not valid"));
                return "error";
            }
        } catch (Exception ex) {
            log.error("Error getting user", ex);
        }
        return "success";
    }
}
