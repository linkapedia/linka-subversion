package com.linkapedia.builderpage.bo.beans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juanidrobo
 */
public class PaginatorLinkObject {

    private List<LinkObject> linkObjectList = new ArrayList<LinkObject>();
    private Integer nextIndexObject;
    private boolean end;

    public Integer getNextIndexObject() {
        return nextIndexObject;
    }

    public void setNextIndexObject(Integer nextIndexObject) {
        this.nextIndexObject = nextIndexObject;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public List<LinkObject> getLinkObjectList() {
        return linkObjectList;
    }

    public void setLinkObjectList(List<LinkObject> linkObjectList) {
        this.linkObjectList = linkObjectList;
    }
}
