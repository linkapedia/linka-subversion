package com.linkapedia.builderpage.bo.template.engine;

import com.linkapedia.builderpage.util.SEOUtils;
import org.apache.velocity.tools.config.DefaultKey;

/**
 *
 * @author andres
 */
@DefaultKey("EscapeSeo")
public class EscapeTools {

    public String escape(String text) {
        if(text==null){
            return "";
        }
        return SEOUtils.encodeFileName(text);
    }

    public String scapeHtml(String html) {
        if(html==null){
            return "";
        }
        html = html.replaceAll("\\<.*?>", "");
        html = html.replaceAll("&nbsp;", "");
        html = html.replaceAll("&amp;", "");
        return html;
    }
}
