package com.linkapedia.builderpage.util;

import java.util.regex.Pattern;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class SEOUtils {

    private static final Logger log = Logger.getLogger(SEOUtils.class);

    //Input: hola    % & casa's ?
    //Output: hola-casas
    public static String encodeFileName(String fileNameToEncode) {
        log.debug("encodeFileName(String)");
        if (fileNameToEncode != null) {
            fileNameToEncode = StringEscapeUtils.unescapeXml(fileNameToEncode);
            fileNameToEncode = fileNameToEncode.toLowerCase();
            fileNameToEncode = fileNameToEncode.trim();
            fileNameToEncode = fileNameToEncode.replaceAll("-", "");
            fileNameToEncode = fileNameToEncode.replaceAll("\\s{2,}", " ");
            fileNameToEncode = fileNameToEncode.replaceAll(" ", "-");

            String result = new String();
            for (char c : fileNameToEncode.toCharArray()) {
                if (Pattern.matches("[a-zA-z0-9-]", String.valueOf(c))) {
                    result = result.concat(String.valueOf(c));

                }
            }

            return result;

        }
        return fileNameToEncode;
    }
}
