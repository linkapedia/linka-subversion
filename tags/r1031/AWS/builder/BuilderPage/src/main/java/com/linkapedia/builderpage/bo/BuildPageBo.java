package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.beans.DigestInfoObject;
import com.linkapedia.builderpage.bo.beans.LinkObject;
import com.linkapedia.builderpage.util.BuilderUtil;
import com.linkapedia.builderpage.util.ManageResourcesUtils;
import java.util.*;
import org.apache.log4j.Logger;

/**
 * logic to create the string(HTML) of the page
 * <p/>
 * @author andres
 */
public class BuildPageBo extends Observable {

    private static final Logger log = Logger.getLogger(BuildPageBo.class);
    private String stringPage;

    /**
     * create the bridge or link node
     *
     * @param node
     * @param user
     * @return
     */
    public boolean createNodePage(NodeStatic node, User user) {
        log.debug("createPageByNodeId Velocity template engine, creating page for:" + node.getId());
        //validate if is a bridge node page or link page
        List<NodeDocument> documents = DataHandlerFacade.getNodeDocumentsByNodeId(String.valueOf(node.getId()));
        BuilderUtil.sorterDocuments(documents);
        if (documents.size() > 250) {
            documents = documents.subList(0, 250);
        }
        List<LinkObject> links = BuilderUtil.getLinks(documents, node);
        links = BuilderUtil.removeLinkObjectWithoutDigest(links);

        boolean generatePageResult = false;
        PageGenerator pg = null;
        String children = node.getChildrenIDs();
        if ((links == null || links.isEmpty()) || (node.isBridgeNode())) {
            if (children == null || children.isEmpty()) {
                //link page
                pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.linknodepage.template.name"));
                pg.putMenuObjects();
                pg.putPageNodeObjects();
                pg.putGeneralObjects();
                generatePageResult = pg.generateLinksNodePage(links);
            } else {
                //bridge page
                pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.bridgenodepage.template.name"));
                pg.putMenuObjects();
                pg.putPageNodeObjects();
                pg.putGeneralObjects();
                generatePageResult = pg.generateBridgeNodePage();
            }
        } else {
            pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.linknodepage.template.name"));
            pg.putMenuObjects();
            pg.putPageNodeObjects();
            pg.putGeneralObjects();
            generatePageResult = pg.generateLinksNodePage(links);
        }

        if (generatePageResult) {
            this.stringPage = pg.mergeContext();
            notifyObeservers();
        } else {
            log.error("Error creating the page");
            return false;
        }
        return true;
    }

    /**
     * create a digest page
     *
     * @param node
     * @param digestInfo
     * @param user
     * @return
     */
    public boolean createDigestPage(NodeStatic node, DigestInfoObject digestInfo, User user) {
        boolean generatePageResult = false;
        PageGenerator pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.digestnodepage.template.name"));
        pg.putMenuObjects();
        pg.putPageNodeObjects();
        pg.putGeneralObjects();
        generatePageResult = pg.generateDigestPage(digestInfo);
        if (generatePageResult) {
            this.stringPage = pg.mergeContext();
            notifyObeservers();
        } else {
            log.error("Error creating the page");
            return false;
        }
        return true;

    }

    /**
     * create a profile page
     *
     * @param user
     * @return
     */
    public boolean createProfilePage(NodeStatic node, User currentUser, User user, Map<String, Object> parameters) {
        PageGenerator pg = new PageGenerator(node, currentUser, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.profile.template.name"));
        pg.putGeneralObjects();
        pg.putPageNodeObjects();
        boolean generatePageResult = pg.generateProfilePage(user, parameters);
        if (generatePageResult) {
            this.stringPage = pg.mergeContext();
            notifyObeservers();
        } else {
            log.error("Error creating the page");
            return false;
        }
        return true;
    }

    /**
     * create a home page
     *
     * @return
     */
    public boolean createHomePage(NodeStatic node, User user, Map<String, Object> parameters) {
        boolean generatePageResult = false;
        PageGenerator pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.home.template.name"));
        pg.putMenuObjects();
        pg.putPageNodeObjects();
        pg.putGeneralObjects();
        generatePageResult = pg.generateHomePage(parameters);
        if (generatePageResult) {
            this.stringPage = pg.mergeContext();
            notifyObeservers();
        } else {
            log.error("Error creating the page");
            return false;
        }
        return true;

    }

    /**
     * create a Collection page
     *
     * @return
     */
    public boolean createCollectionPage(NodeStatic node, InterestCollection interestCollection, User user) {
        boolean generatePageResult = false;
        PageGenerator pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.collection.template.name"));
        pg.putGeneralObjects();
        pg.putPageNodeObjects();
        generatePageResult = pg.generateCollectionPage(interestCollection);
        if (generatePageResult) {
            this.stringPage = pg.mergeContext();
            notifyObeservers();
        } else {
            log.error("Error creating the page");
            return false;
        }
        return true;

    }

    /**
     * create a application page
     *
     * @return
     */
    public boolean createApplicationPage(NodeStatic node, User user, Map<String, Object> parameters) {
        PageGenerator pg = null;
        if ("about".equals(parameters.get("pageType"))) {
            pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.about.name"));
        } else if ("privacy".equals(parameters.get("pageType"))) {
            pg = new PageGenerator(node, user, ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.privacy.name"));
        } else {
            log.error("Unsopported pageType: " + parameters.get("pageType"));
            return false;
        }
        pg.putPageNodeObjects();
        pg.putGeneralObjects();
        boolean generatePageResult = pg.generateApplicationPage();
        if (generatePageResult) {
            this.stringPage = pg.mergeContext();
            notifyObeservers();
        } else {
            log.error("Error creating the page");
            return false;
        }
        return true;
    }

    /**
     * notify the observers to paint the page
     */
    private void notifyObeservers() {
        setChanged();
        notifyObservers();
    }

    /**
     * this method is for the observer
     *
     * @return the string of the page
     */
    public String getPageString() {
        return stringPage;
    }
}
