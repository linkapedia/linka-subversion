package com.linkapedia.builderpage.bo.beans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juanidrobo
 */
public class PaginatorNodeObject {

    private List<NodeDataObject> nodeObjectList = new ArrayList<NodeDataObject>();
    private Integer nextIndexObject;
    private boolean end;
    private String corpusIdReverse;

    public String getCorpusIdReverse() {
        return corpusIdReverse;
    }

    public void setCorpusIdReverse(String corpusIdReverse) {
        this.corpusIdReverse = corpusIdReverse;
    }

    public Integer getNextIndexObject() {
        return nextIndexObject;
    }

    public void setNextIndexObject(Integer nextIndexObject) {
        this.nextIndexObject = nextIndexObject;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public List<NodeDataObject> getNodeObjectList() {
        return nodeObjectList;
    }

    public void setNodeObjectList(NodeDataObject node) {
        nodeObjectList.add(node);
    }

    public void setObjectList(List<NodeDataObject> nodeDataObjectList) {
        nodeObjectList = nodeDataObjectList;
    }
}
