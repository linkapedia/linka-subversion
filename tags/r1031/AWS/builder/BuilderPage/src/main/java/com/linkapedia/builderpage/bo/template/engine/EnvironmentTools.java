package com.linkapedia.builderpage.bo.template.engine;

import com.linkapedia.builderpage.util.UtilEnviroment;
import org.apache.velocity.tools.config.DefaultKey;

/**
 *
 * @author andres
 */
@DefaultKey("EnvironmentTools")
public class EnvironmentTools {

    /**
     * get the name to the correct environment
     *
     * @return
     */
    public static String getNameCalidoso() {
        return UtilEnviroment.getNameCalidoso();
    }
}
