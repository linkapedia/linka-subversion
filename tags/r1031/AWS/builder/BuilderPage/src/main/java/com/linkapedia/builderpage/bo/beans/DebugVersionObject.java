package com.linkapedia.builderpage.bo.beans;

import java.io.Serializable;

/**
 * object for sentence logic
 *
 * @author andres
 */
public class DebugVersionObject implements Serializable {

    private String sentence;
    private double scoreByNodeTitle;
    private double scoreByMustHaves;
    private double scoreBySignatures;
    //temp to show in the template to test algorithm
    private String signatures;
    private String mustHaves;

    public double getScoreByMustHaves() {
        return scoreByMustHaves;
    }

    public void setScoreByMustHaves(double scoreByMustHaves) {
        this.scoreByMustHaves = scoreByMustHaves;
    }

    public double getScoreByNodeTitle() {
        return scoreByNodeTitle;
    }

    public void setScoreByNodeTitle(double scoreByNodeTitle) {
        this.scoreByNodeTitle = scoreByNodeTitle;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public double getScoreBySignatures() {
        return scoreBySignatures;
    }

    public void setScoreBySignatures(double scoreBySignatures) {
        this.scoreBySignatures = scoreBySignatures;
    }

    public String getMustHaves() {
        return mustHaves;
    }

    public void setMustHaves(String mustHaves) {
        this.mustHaves = mustHaves;
    }

    public String getSignatures() {
        return signatures;
    }

    public void setSignatures(String signatures) {
        this.signatures = signatures;
    }
}
