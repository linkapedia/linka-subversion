/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.builderpage.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author alexander
 */
public class DigestUtil {

    public static String FilterDigestSpaces(String digest, List<String> search, List<String> replacements) {
        try {
            int size = search.size();
            int i;
            if (search.size() != replacements.size()) {
                return "null";
            }

            if (digest != null) {
                for (i = 0; i < size; i++) {
                    if (search.get(i).equals("\"")) {
                        String textTemp = "";
                        Pattern pattern = Pattern.compile("( )*(?:\\\".*?\\\")( )*");

                        Matcher matcher = pattern.matcher(digest);

                        while (matcher.find()) {
                            textTemp = " " + RegexExtraSpaces(matcher.group(), "\"", "\"") + " ";
                            digest = digest.replaceAll(matcher.group(), textTemp);
                        }
                    } else {
                        digest = RegexExtraSpaces(digest, search.get(i), replacements.get(i));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        return digest;
    }

    private static String RegexExtraSpaces(String digest, String search, String replacement) {
        return digest.replaceAll("( )*\\" + search + "( )*", replacement);
    }
}
