package com.linkapedia.builderpage.bo.template.engine;

import com.linkapedia.builderpage.util.UtilEnviroment;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.ToolManager;

/**
 * factory of the Template, get the context
 *
 * @author andres
 */
public class VelocityCreator {

    private static final Logger log = Logger.getLogger(VelocityCreator.class);
    private static Map<String, Template> templateMaps;

    static {
        templateMaps = new HashMap<String, Template>();
    }

    public VelocityCreator() {
    }

    /**
     * save the templates in memory to not create more
     *
     * @param templateName
     * @return
     */
    public static Template getTemplate(String templateName) {
        log.debug("VelocityEngine:getEngine");
        Template template = templateMaps.get(templateName);
        if (template != null) {
            return template;
        }
        //create the engine in the factory
        VelocityEngine engine;
        try {
            engine = createEngine();
            template = engine.getTemplate(templateName);
            if (template != null) {
                templateMaps.put(templateName, template);
            }
        } catch (Exception ex) {
            log.error("Error creating the template", ex);
        }

        return template;
    }

    /**
     * create a context in each call
     *
     * @param useVelocityTool
     * @return
     */
    public static VelocityContext getContext(boolean useVelocityTool) {
        VelocityContext context = null;
        if (useVelocityTool) {
            ToolManager velocityToolManager = new ToolManager();
            velocityToolManager.configure(VelocityCreator.class.getClassLoader().getResource("system/velocity-tools.xml").getPath());
            context = new VelocityContext(velocityToolManager.createContext());
        } else {
            context = new VelocityContext();

        }
        return context;
    }

    /**
     * get velocity engine
     *
     * @return VelocityCreator: with all parameters configured
     * @throws Exception
     */
    private static VelocityEngine createEngine() throws Exception {
        Properties p = new Properties();
        VelocityEngine engine;
        p.setProperty("resource.loader", "file");
        p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
        String path = VelocityCreator.class.getClassLoader().getResource("system/templates/" + UtilEnviroment.getEnv()).getPath();
        p.setProperty("file.resource.loader.path", path);
        p.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
        //Charset properties
        p.setProperty("input.encoding", "UTF-8");
        p.setProperty("output.encoding", "UTF-8");
        engine = new VelocityEngine();
        engine.init(p);
        return engine;
    }
}
