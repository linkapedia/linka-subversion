package com.linkapedia.builderpage.bo.comparators;

import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import java.util.Comparator;

/**
 *
 * @author Bancolombia
 */
public class NodeDataComparator implements Comparator<NodeDataObject> {

    @Override
    public int compare(NodeDataObject o1, NodeDataObject o2) {
        return (o1.getPosition() < o2.getPosition()) ? -1 : (o1.getPosition() > o2.getPosition()) ? 1 : 0;
    }
}
