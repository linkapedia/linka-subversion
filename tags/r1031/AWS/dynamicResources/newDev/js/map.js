function Map(){
	var Map = [];
	this.put = function(key, object){
		if(this.validateKey(key) == null){			
			return null;
		}
		if(Map.length == 0){						
			Map = [
				{
					key : key,
					array : [object]
				}
			];
		}else{
			var array = this.getItem(key);
			if(array.length == 0){
				Map.push({
					key : key,
					array : [object]
				});
			}else{
				array[0].array.push(object);
			}
		}
	}
	
	this.getItem = function(key){
		if(this.validateKey(key) == null){			
			return null;
		}else{
			var array = [];
			var size = Map.length;			
			for(i = 0; i < size ; i++){
				if(Map[i].key === key){
					array.push(Map[i]);
					break;
				}
			}
			return array;
		}
	}
	
	this.getMap = function(){
		return Map;
	}
	
	this.validateKey = function(key){
		if(key == null || key == undefined){
			console.log("Error: key can not be null or undefined");
			return null;
		}else{
			return key;
		}
	}	
}
