
$(document).ready(function() {
getReady(1);
getReady(2);
});

function getReady(num){
  $("#saveButton"+num).click(function() {
        saveCollection(num);
        hideOptions(num);
        setBehavior(num);
    });
    
    $("#editButton"+num).click(function() {
        showOptions(num);
        clearBehavior(num);
    });

    $("#cancelButton"+num).click(function() {
        hideOptions(num);
        setBehavior(num);
       $("#editText"+num).val($("#interestCollectionText"+num+" span").text()); 
    });
    
    $("#editText"+num).val($("#interestCollectionText"+num+" span").text());
    setBehavior(num);
}


function setBehavior(num){
    $("#edit"+num).hover( 
        function () {
            $("#editButton"+num).show();
        }, 
        function () {
            $("#editButton"+num).hide();
        }
        )
            
    
}
function clearBehavior(num)
{
    $("#edit"+num).unbind();
}
function showOptions(num){
    $("#editButton"+num).hide();
    $("#interestCollectionText"+num+" span").hide();
    $("#saveButton"+num).show();
    $("#cancelButton"+num).show();
    $("#editText"+num).text($("#interestCollectionText"+num+" span").text()); 
    $("#editText"+num).show();
    
}
function hideOptions(num){
    
    $("#saveButton"+num).hide();
    $("#cancelButton"+num).hide();
    $("#editText"+num).hide();
    $("#interestCollectionText"+num+" span").show();
    setBehavior(num);
}

function saveCollection(num){

 COLLECTIONAPI.updateCollection();
}

function loadCollectionPage(){
    $('img').each(function() {
        if((typeof this.naturalWidth != "undefined" && this.naturalWidth == 0 ) || this.readyState == 'uninitialized' ) {
             var parent = $(this).parent();
            if(parent.hasClass("box")){
                parent.remove();
            }
        }
    });
    var container = $('#digests');
    container.imagesLoaded(function() {
       container.masonry();
    }); 
}

function managerErrorImage(image,url){
    image.onerror = "";
    $(image).attr('src',url);
}

