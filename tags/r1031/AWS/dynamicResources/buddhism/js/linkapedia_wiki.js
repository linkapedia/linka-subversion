
var instructional = jQuery(' <div id="instructional_2"> <div id="item1">Login </div><div id="item2">Topic Search </div><div id="item3">Browse</div><div id="item4">Explore </div> </div><div id="instructional"></div>');
function load() {
 //menu
    window.api = $("#slider1").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider2").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider3").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
}
function loadLinkPage(nodeId, infiniteScrollInit, infiniteScrollNobjects)
{		
    infiniteLinkScroll(nodeId,infiniteScrollInit,infiniteScrollNobjects);

	_kmq.push(['record', 'Link Page']);  //KISSMETRIC EVENT
	
    $(".titleNodeLinkpage").click(function(e){
		_kmq.push(['record', 'Link Page One Click']);  //KISSMETRIC EVENT
	});
}

function loadBridgeNodePage()
{	
    //masonry
    var container = $('#bridgeNodeContent');
    clipTextBridgeNode();
    
    _kmq.push(['record', 'Bridge Node Page']);  //KISSMETRIC EVENT
    $(".titleNodeLinkpage").click(function(e){
		_kmq.push(['record', 'Link Page One Click']);  //KISSMETRIC EVENT
	});
}

function loadHomePage( )
{
    //masonry
    var container = $('#digests');
    //hide the container of portlets
    $('#digests').css({
        opacity : 0
    });
    _kmq.push(['record', 'Viewed Homepage']);
}

function loadCollectionPage(){
    var container = $('#digests');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}

function loadProfilePage(){
    var container = $('#collectionsUser');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}

function changeSourceMetaImg(page){
	var exp = "";
	var imgHtml = null;
	if(page == "bridgepage"){
		exp="#bridgeNodeContent > div:first > img";
	}else if(page == "linkpage"){
		exp = "#digests > div:first .linkImage";	
	}
	imgHtml = $(exp);
	if(imgHtml.length > 0){
		$("meta[property='og:image']").attr("content",imgHtml.attr("src"));	
	}else{
		$("meta[property='og:image']").attr("content","https://s3.amazonaws.com/resourcesweb/newdev/images/no_image.png");	
	}
}

function loadDigestPage(){		
	$("#seeOriginalSource").click(function(){
		_kmq.push(['record', 'See Original Source']);  //KISSMETRIC EVENT
	});
		
		$("#seeOriginalSource").prepend("<br />");
		$("#seeOriginalSource").css("position","absolute");	
		$("#seeOriginalSource").css("left","379px");				
		$(".digest_text").append("<br /><br />");

		_kmq.push(['record', 'Digest Page View']);  //KISSMETRIC EVENT

	
	instructional = jQuery(' <div id="instructional_2"> <div id="item1">Login </div><div id="item2">Topic Search </div><div id="item5_browse">Browse</div><div id="item6_save">Save </div> <div id="item7_discuss">Discuss </div> </div><div id="instructional"></div>');
}

function overlay(){
	if(!localStorage.getItem("overlay")){		
		instructional.appendTo(document.body);
		
		$('#instructional').click( function() {
			localStorage.setItem("overlay","true");
			$('#instructional_2').hide();
			$('#instructional').fadeOut();
		});

		$('#instructional_2').click( function() {
			localStorage.setItem("overlay","true");
			$('#instructional_2').hide();
			$('#instructional').fadeOut();
		});
	}
}

function showRelated(link){		
	showRelatedById($(link).attr("id"));
}

function hiddenRelated(link){
$(link).siblings("a").css("display","block");
$(link).css("display","none");	
$(".listrelated").css("display","none");
}

function showRelatedById(id){
	$(".hide3").css("display","none");
	$(".show3").css("display","block");	
	$(".listrelated").css("display","none");
	
	$(".showRelated_"+id).css("display","none");
	$(".showRelated_"+id).siblings(".hide3").css("display","block");
	
	$("#related_"+$(".showRelated_"+id).attr("id")).css("display","list-item");				
	
	if(id != "null"){
		$("#related_"+id).css("display","list-item");
	}	
}

function clipTextBridgeNode(){
	var collectionBridgeNodeText = $(".bridge_text");
	var sub = 320;
	$.each(collectionBridgeNodeText, function(index, element){
		if($(element).text().length > sub){
			$(element).text(clipText($(element).text(),sub)+"...");
		}else {
			$(element).text($(element).text()+"...");
		}		
	});
}

function clipText(text, textlengthclip){
	if(text){
		if(text.length > textlengthclip){
			return text.substring(0,textlengthclip)
		}else {
			return text;
		}
	}
	return "";
}
