// created by Juan Idrobo

var PAGE_EVENTS = function() {

    maxNumberOfUsersToShowFollowingTopic = 10;//default
    maxNumberOfUsersToShowFollowingUser = 10;//default
    lock = false;
    return{
        setMaxNumberOfUsersToShowFollowingTopic: function(maxNumber) {
            maxNumberOfUsersToShowFollowingTopic = maxNumber;
        },
        setMaxNumberOfUsersToShowFollowingUser: function(maxNumber) {
            maxNumberOfUsersToShowFollowingUser = maxNumber;
        },
        joinNode: function(nodeId) {
            if (!lock) {
                lock = true;
                $.ajax({
                    url: CONTEXT_LINKAPEDIA+'/rest/page-events/joinNode/' + nodeId,
                    success: function(response) {
                        if (JSON.parse(response)) {
                              _kmq.push(['record', 'JOIN NODE']);  //KISSMETRIC EVENT
                            
                            $('#join_unjoin_topic').removeClass();
				    $('#join_unjoin_topic').addClass('unjoinNode');
				    $('#timeJoinNode').text('Joined today');
                            $('#join_unjoin_topic').attr("onclick", "PAGE_EVENTS.unjoinNode(\"" + nodeId + "\")");
				    $('#join_unjoin_topic').text('(Leave)');

                            var count = $('#members_count').text();

                            if (count == 0)
                            {
                                $('#members').show();
                                $('#no_following_topic').hide();
                                $('#users_followig_topic').show();


                            }
                            count++;
                            $('#members_count').text(count);
                            $('#total_users_following_topic').text(count);

                            if ($('#topic_followers .user_following').size() < maxNumberOfUsersToShowFollowingTopic)
                            {
                                $('.user_itself_following').show();
                            }
                        }
                        lock = false;
                    },
                    error: function(response) {
                        lock = false;
                    }


                });
            }

        },
        unjoinNode: function(nodeId) {
            if (!lock) {
                lock = true;
                $.ajax({
                    url: CONTEXT_LINKAPEDIA+'/rest/page-events/unjoinNode/' + nodeId,
                    success: function(response) {
                        if (JSON.parse(response)) {
                            _kmq.push(['record', 'UNJOIN NODE']);  //KISSMETRIC EVENT
                            
                            $('#join_unjoin_topic').removeClass();
				    $('#join_unjoin_topic').addClass('join_topic');
				    $('#timeJoinNode').text('');

                            $('#join_unjoin_topic').attr("onclick", "PAGE_EVENTS.joinNode(\"" + nodeId + "\")");
				    $('#join_unjoin_topic').text('Join topic');

                            var count = $('#members_count').text();
                            count--;
                            $('#members_count').text(count);
                            $('#total_users_following_topic').text(count);
                            if (count == 0)
                            {
                                $('#members').hide();
                                $('#users_followig_topic').hide();
                                $('#no_following_topic').show();

                            }

                            $('.user_itself_following').hide();
                        }
                        lock = false;
                    },
                    error: function(response) {
                        lock = false;
                    }

                });
            }

        },
        followUser: function(userId) {
            if (!lock) {
                lock = true;
                $.ajax({
                    url: CONTEXT_LINKAPEDIA+'/rest/page-events/followUser/' + userId,
                    success: function(response) {
                        if (JSON.parse(response)) {
                            _kmq.push(['record', 'FOLLOW USER']);  //KISSMETRIC EVENT

                            $('#follow_unfollow_user').removeClass();
				    $('#follow_unfollow_user').addClass('unfollow_user');
				    $('#followDate').text('Following today');
                            
                            $('#follow_unfollow_user').attr("onclick", "PAGE_EVENTS.unFollowUser(\"" + userId + "\")");
                            $('#follow_unfollow_user').text("(Leave)");
                            var count = $('#followers_count').text();
                            count++;
                            $('#followers_count').text(count);

                            if ($('#user_followers .user_following').size() < maxNumberOfUsersToShowFollowingUser)
                            {
                                $('.user_itself_following').show();
                            }
                        }
                        lock = false;
                    },
                    error: function(response) {
                        lock = false;
                    }


                });
            }

        },
        unFollowUser: function(userId) {
            if (!lock) {
                lock = true;
                $.ajax({
                    url: CONTEXT_LINKAPEDIA+'/rest/page-events/unFollowUser/' + userId,
                    success: function(response) {
                        if (JSON.parse(response)) {
                            _kmq.push(['record', 'UNFOLLOW USER']);  //KISSMETRIC EVENT

                            $('#follow_unfollow_user').removeClass();
				    $('#follow_unfollow_user').addClass('follow_user');
				    $('#followDate').text('');
                            
                            $('#follow_unfollow_user').attr("onclick", "PAGE_EVENTS.followUser(\"" + userId + "\")");
                            $('#follow_unfollow_user').text("Follow");
                            var count = $('#followers_count').text();
                            count--;
                            $('#followers_count').text(count);

                            $('.user_itself_following').hide();

                        }
                        lock = false;
                    },
                    error: function(response) {
                        lock = false;
                    }


                });
            }

        },
        LoginFacebook: function(token, next){
			$.ajax({
				url: CONTEXT_LINKAPEDIA+"/FacebookLoginClient?tokenFb="+token+"&next="+next,
				dataType: "JSON",
				success: function(response){
					if(response.login == "loginWithFacebook"){
						_kmq.push(['record', 'loginWithFacebook']);  //KISSMETRIC EVENT
					}else if(response.login == "createAccount"){
						_kmq.push(['record', 'createAccount']);  //KISSMETRIC EVENT
					}
					
					if(response.url){
						var currentPage = $("#containTopMenu").attr("currentpage");
						
						if(currentPage == "homepage"){
							window.location = response.url;
						}else{
							location.reload();
						}
					}
				}
			});
		},
		playVideoHome: function(){
			var iframe = $('.video > iframe')[0],
			player = $f(iframe);
			
			player.addEvent('ready', function() {
				player.addEvent('play', function(){
					_kmq.push(['record', 'Video']);  //KISSMETRIC EVENT
				});
			});
		}
    };
}();







