/*
 *Andres restrepo
 *
 *This file contain all logic to get sentenses
 */

 var call = "SentenceExtracted";
 function getDebugVersion(corpusId,nodeId,fileName){
    //call ajax to get result
    $.ajax({
        url: call,
        dataType: "jsonp",
        data: "corpusId="+corpusId+"&nodeId="+nodeId+"&fileName="+fileName+"&version=1",
        success: function( data ) {
            var status = data.status;
            var message = data.message;
            var sentences = data.sentences;
            var string = "";
            if(status == 0){
                string = "<div><b>"+message+"</b></div>";
            }else{
            //parse and return data
            string = "<div style='padding:10px;'>";
            for (i = 0; i < sentences.length; i++){
                var sentence=sentences[i].sentence;
                var scoreNodeTitle = sentences[i].scoreNodeTitle;
                var scoreMH = sentences[i].scoreMH;
                var scoreSig = sentences[i].scoreSig;
                var signatures = sentences[i].signatures;
                var musthaves = sentences[i].musthaves;
                    //build html <div> and append
                    string+="<br>";
                    string+="<p>"+sentence+"<br>";
                    string+="sscore1(Node title):&nbsp;"+scoreNodeTitle+"<br>";
                    string+="score2(Musthaves):&nbsp;"+scoreMH+"<br>";
                    string+="score3(signatures):&nbsp;"+scoreSig+"<br>";
                    var total = scoreNodeTitle + scoreMH + scoreSig;
                    string+="Total:&nbsp;"+total+"<br>";
                    string+=" Musthaves:&nbsp;"+musthaves+"<br>";
                    string+=" Signatures:&nbsp;"+signatures+"<br>";
                    string+="</p><br>"; 
                }
                string+="</div>";
            }


            $('#dialogDebugSentences').children("div").remove();
            $('#dialogDebugSentences').append(string);
            $('#dialogDebugSentences').dialog( "open" );

        },beforeSend: function(){
            $("#loadingAjax").dialog( "open" );
        },complete: function(){
            $("#loadingAjax").dialog( "close" );       
        },error: function(){
            console.log("Error getting result debug version");
            $("#loadingAjax").dialog( "close" );  
        }
    });
}

function getInfoVersion(corpusId,nodeId,fileName,title,link,imageNumber){

    $.ajax({
        url: call,
        dataType: "jsonp",
        data: "corpusId="+corpusId+"&nodeId="+nodeId+"&fileName="+fileName+"&version=2",
        success: function( data ) {
            var docDigest=data.docDigest;
            var nodes = data.nodes;
            var idImage ="porletImage_"+imageNumber;
            //build html <div> and append

            var string = "<div id=\"dinamicInfoPage\" style='padding:10px;'>";
            string+="<a class=\"backButtonInfoPage\" href=\"JavaScript:void(0);\" onclick=\"$.fancybox.close();\">&#60;Back</a>";
            string+="<a style=\"margin-left: 800px;\" href=\"JavaScript:void(0);\" onclick=\"$.fancybox.close();\"><img src=\"https://s3.amazonaws.com/resourcesweb/dev/site-resources/images/close.gif\"></a></br>";
            string+="<span class=\"titlesInfoPage\">"+title+"</span><br>";
            string+="<a class=\"linkInfoPage\" href=\"#\">save</a>&nbsp;|&nbsp;";
            string+="<a class=\"linkInfoPage\" href=\""+link+"\" target=\"_blank\">see original source</a>";

            <!-- AddThis Button BEGIN -->
            string+="<div style=\"display: inline-block;\" class=\"addthis_toolbox addthis_default_style\">";
            string+="<a href=\"JavaScript:void(0);\" class=\"addthis_button_facebook\" style=\"cursor:pointer\"></a>";
            string+="<a href=\"JavaScript:void(0);\" class=\"addthis_button_twitter\" style=\"cursor:pointer\"></a>";
            string+="<a href=\"JavaScript:void(0);\" class=\"addthis_button_email\" style=\"cursor:pointer\"></a>";
            string+="<a href=\"JavaScript:void(0);\" class=\"addthis_button_pinterest_share\" style=\"cursor:pointer\"></a>";
            string+="<a href=\"JavaScript:void(0);\" class=\"addthis_button_compact\"></a>";
            string+="<a href=\"JavaScript:void(0);\" class=\"addthis_counter addthis_bubble_style\"></a>";
            string+="</div></br></br>";
            <!-- AddThis Button END -->

            string+="<div>";
            string+="<div id=\"digestInfo\" style=\"width:62%; float:left;\">";
            string+=docDigest;
            string+="<br>";
            string+="<a class=\"linkInfoPage\" href=\"#\">save</a>&nbsp;|&nbsp;";
            string+="<a class=\"linkInfoPage\" href=\""+link+"\" target=\"_blank\">see original source</a>";
            string+="</div>";
            string+="<div id=\"imageArea\" style=\"float:left;width: 241px;padding-left: 30px;\">";
            string+="<img src="+$("#"+idImage).attr("src")+" WIDTH=240 HEIGHT=160 /><br><br>";
            string+="<span class=\"titlesInfoPage\">Topics found:</span><br>";
            string+="<div>";
            if (nodes!=undefined){
                //Related nodes
                for (i = 0; i < nodes.length; i++){
                    if(i>0){
                        string+="&nbsp;|&nbsp;";
                    }
                    string+="<a class=\"linkInfoPage\" href='?nodeId="+nodes[i].id+"'>"+nodes[i].title+"</a>";
                }
            }
            string+="</div>";
            string+="</div>";
            string+="<div style=\"clear:both;\"</div><br>";
            string+="</div>";
            string+="</div>";

            $('#dinamicInfoPage').remove();
            $('#infoPage').prepend(string);

            //load addthis
            var script = 'http://s7.addthis.com/js/300/addthis_widget.js?domready=1#pubid=ra-5072f6c670b8ba6e';
            if (window.addthis){
                window.addthis = null;
            }
            $.getScript( script );

            //load disqus -- the correct thread based in the document md5
            var url = window.location.href;
            url = replaceAll(url,"#","");
            loadDisqus($(this),fileName,url+"#!"+fileName);
            $('#infoPageView').click();


        },beforeSend: function(){
            $("#loadingAjax").dialog( "open" );
        },complete: function(){
            $("#loadingAjax").dialog( "close" );  
        }
    });
}

    function replaceAll(txt, replace, with_this) {
      return txt.replace(new RegExp(replace, 'g'),with_this);
    } 

    var disqus_shortname = 'devbudd';
    var disqus_identifier; //made of post id and guid
    var disqus_url; //post permalink
    var disqus_developer;
    function loadDisqus(source, identifier, url) {

        if (window.DISQUS) {

           $('#disqus_thread').insertAfter(source); //append the HTML after the link

           //if Disqus exists, call it's reset method with new parameters
           DISQUS.reset({
              reload: true,
              config: function () {
              this.page.identifier = identifier;
              this.page.url = url;
              }
           });

        } else {

           //insert a wrapper in HTML after the relevant "show comments" link
           $('#disqus_thread').insertAfter(source);
           disqus_identifier = identifier; //set the identifier argument
           disqus_url = url; //set the permalink argument
           disqus_developer = '1'; //remove this variable in production

           //append the Disqus embed script to HTML
           var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
           dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
           $('head').append(dsq);

        }
    }

