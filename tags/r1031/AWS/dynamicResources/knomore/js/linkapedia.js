function load() {
 //menu
    window.api = $("#slider1").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider2").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider3").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
}
function loadLinkPage(nodeId, infiniteScrollInit, infiniteScrollNobjects )
{
    //masonry
    var container = $('#digests');
    //hide the container of portlets
    $('#digests').css({
        opacity : 0
    });
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }
        });
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            if(!isiPad){
                $('.fluid .box.col2').css("min-width","430px");
                $('.fluid .box.col1').css("min-width","200px");
            }
        infiniteLinkScroll(nodeId,infiniteScrollInit,infiniteScrollNobjects);
        //show the porlets slowly
        $('#digests').animate({
            opacity : 1
        }, {
            duration : 2000
        });
    });


}

function loadBridgeNodePage()
{
    //masonry
    var container = $('#bridgeNodeContent');
    //hide the container of portlets
    container.css({
        opacity : 0
    });
    container.imagesLoaded(function() {
        
        container.masonry({
            itemSelector : '.boxBridgeNode',
           /* columnWidth : function(containerWidth) {

                return containerWidth / 4   ;
   
            }
            */
           columnWidth:70
        });

        //show the porlets slowly
        container.animate({
            opacity : 1
        }, {
            duration : 2000
        });
    });
    
    
    bridgeNodeEffect(); //bridgeNode.js
}

function loadHomePage( )
{
    //masonry
    var container = $('#digests');
    //hide the container of portlets
    $('#digests').css({
        opacity : 0
    });
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }
        });
        //show the porlets slowly
        $('#digests').animate({
            opacity : 1
        }, {
            duration : 2000
        });
    });
}

function loadCollectionPage(){
    var container = $('#digests');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}

function loadProfilePage(){
    var container = $('#collectionsUser');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}
