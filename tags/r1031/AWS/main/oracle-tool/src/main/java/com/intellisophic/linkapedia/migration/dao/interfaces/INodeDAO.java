package com.intellisophic.linkapedia.migration.dao.interfaces;

import com.intellisophic.linkapedia.migration.bo.models.Node;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Xander Kno
 */
public interface INodeDAO {

    public boolean createNode(Node node) throws SQLException, Exception;

    public Node getNode(Long nodeId) throws SQLException, Exception;

    public boolean updateNode(Node node) throws SQLException, Exception;

    public boolean deleteNode(Long nodeId) throws SQLException, Exception;

    //Lists
    public List<Node> getNodesByCorpusID(Long corpusId) throws SQLException, Exception;

    public List<Node> getNodesByParentID(Long parentID) throws SQLException, Exception;
    
    public List<Long> getChildrenByNodeId(Long nodeId) throws SQLException, Exception;
    
    public List<Node> getNodesByNodeId (Long nodeId,Boolean isRecursive) throws SQLException, Exception;
}
