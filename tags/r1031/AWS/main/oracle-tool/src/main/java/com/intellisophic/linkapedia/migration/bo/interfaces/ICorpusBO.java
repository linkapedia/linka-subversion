package com.intellisophic.linkapedia.migration.bo.interfaces;

import com.intellisophic.linkapedia.migration.bo.models.Corpus;
import java.util.List;

/**
 *
 * @author Xander Kno
 */
public interface ICorpusBO {

    public Corpus getCorpus(Long corpusID) throws Exception;

    public List<Corpus> getAllCorpus() throws Exception;
}
