package com.intellisophic.linkapedia.migration.dao.interfaces;

import com.intellisophic.linkapedia.migration.bo.models.Corpus;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Xander Kno
 */
public interface ICorpusDAO {

    public Corpus getCorpus(Long corpusID) throws SQLException, Exception;

    public List<Corpus> getAllCorpus() throws SQLException, Exception;
}
