package com.intellisophic.linkapedia.migration.bo.models;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Xander Kno
 */
public class Node implements Serializable {

    private Long id;
    private Long corpusId;
    private String title;
    private Integer size;
    private Long parentId;
    private Integer indexWithinParent;
    private Integer depthFromRoot;
    private Date scanned;
    private Date updated;
    private Integer status;
    private String lTitle;
    private Date lastSync;
    private String description;
    private Long linkNodeId;
    private Boolean listNode;
    private Integer type;
    private Boolean bridgeNode;
    //attribute to store node aditional info.
    private Map<String, String> extraValues;
    private String path;

    public Boolean getBridgeNode() {
        return bridgeNode;
    }

    public void setBridgeNode(Boolean bridgeNode) {
        this.bridgeNode = bridgeNode;
    }

    public Long getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(Long corpusId) {
        this.corpusId = corpusId;
    }

    public Integer getDepthFromRoot() {
        return depthFromRoot;
    }

    public void setDepthFromRoot(Integer depthFromRoot) {
        this.depthFromRoot = depthFromRoot;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndexWithinParent() {
        return indexWithinParent;
    }

    public void setIndexWithinParent(Integer indexWithinParent) {
        this.indexWithinParent = indexWithinParent;
    }

    public String getlTitle() {
        return lTitle;
    }

    public void setlTitle(String lTitle) {
        this.lTitle = lTitle;
    }

    public Date getLastSync() {
        return lastSync;
    }

    public void setLastSync(Date lastSync) {
        this.lastSync = lastSync;
    }

    public Long getLinkNodeId() {
        return linkNodeId;
    }

    public void setLinkNodeId(Long linkNodeId) {
        this.linkNodeId = linkNodeId;
    }

    public Boolean getListNode() {
        return listNode;
    }

    public void setListNode(Boolean listNode) {
        this.listNode = listNode;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Date getScanned() {
        return scanned;
    }

    public void setScanned(Date scanned) {
        this.scanned = scanned;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Map<String, String> getExtraValues() {
        if (extraValues == null) {
            extraValues = new HashMap<String, String>();
        }
        return extraValues;
    }

    public void setExtraValues(Map<String, String> extraValues) {
        this.extraValues = extraValues;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Node{" + "id=" + id + ", corpusId=" + corpusId + ", title=" + title + ", parentId=" + parentId + ", status=" + status + ", description=" + description + '}';
    }
}
