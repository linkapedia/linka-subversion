package com.intellisophic.linkapedia.migration.dao.interfaces;

import java.sql.SQLException;

/**
 *
 * @author Xander Kno
 */
public interface IMustHaveDAO {

    public String getMustHavesByNodeID(Long nodeID) throws SQLException, Exception;
}
