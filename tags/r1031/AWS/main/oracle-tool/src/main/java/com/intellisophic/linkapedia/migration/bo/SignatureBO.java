package com.intellisophic.linkapedia.migration.bo;

import com.intellisophic.linkapedia.migration.bo.interfaces.ISignatureBO;
import com.intellisophic.linkapedia.migration.dao.SignatureDAO;
import com.intellisophic.linkapedia.migration.dao.interfaces.ISignatureDAO;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class SignatureBO implements ISignatureBO {

    private static final Logger log = Logger.getLogger(SignatureBO.class);
    private ISignatureDAO signatureDAO;

    public SignatureBO() {
        this.signatureDAO = new SignatureDAO();
    }

    public Map<String, Long> getSignatureByNodeID(Long nodeID) throws Exception {
        return getSignatureDAO().getSignatureByNodeID(nodeID);
    }

    public ISignatureDAO getSignatureDAO() {
        return signatureDAO;
    }

    public void setSignatureDAO(ISignatureDAO signatureDAO) {
        this.signatureDAO = signatureDAO;
    }
}
