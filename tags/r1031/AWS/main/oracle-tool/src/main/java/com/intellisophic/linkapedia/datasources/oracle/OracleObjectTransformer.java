package com.intellisophic.linkapedia.datasources.oracle;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.Taxonomy;
import com.intellisophic.linkapedia.api.beans.enums.Status;
import com.intellisophic.linkapedia.generic.utils.NumericUtils;
import com.intellisophic.linkapedia.migration.bo.models.Corpus;
import com.intellisophic.linkapedia.migration.bo.models.Node;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class OracleObjectTransformer {

    private static final Logger log = Logger.getLogger(OracleObjectTransformer.class);

    public static Taxonomy transform(Corpus corpus) {
        log.debug("transform(Corpus)");
        Taxonomy taxonomy = null;
        String[] terms = null;
        if (corpus == null) {
            return null;
        }
        try {
            taxonomy = new Taxonomy();
            taxonomy.setActive(corpus.isActive());
            taxonomy.setAffinity(corpus.isAffinity());
            taxonomy.setDescription(corpus.getDescription());
            taxonomy.setID(NumericUtils.revertLongAsString(corpus.getID()));
            taxonomy.setLastRun(corpus.getLastRun());
            taxonomy.setLastSync(corpus.getLastSync());
            taxonomy.setName(corpus.getName());
            taxonomy.setROCC1(corpus.getRocc1());
            taxonomy.setROCC2(corpus.getRocc2());
            taxonomy.setROCC3(corpus.getRocc3());
            taxonomy.setROCF1(corpus.getRocf1());
            taxonomy.setROCF2(corpus.getRocf2());
            taxonomy.setROCF3(corpus.getRocf3());
            taxonomy.setROCSettingID(corpus.getRocSettingID());
            taxonomy.setRunFrecuency(corpus.getRunFrecuency());
            taxonomy.setStatus(Status.PRODUCTION);
            if (corpus.getTerms() != null) {
                terms = corpus.getTerms().split("\\,");
            }
            taxonomy.setTerms(terms);
            taxonomy.setRootNodeID(NumericUtils.revertLongAsString(corpus.getRootNodeID()));
        } catch (Exception e) {
            log.error("An exception has ocurred on transform(Corpus)", e);
        }
        return taxonomy;
    }

    public static NodeStatic transform(Node node) {
        log.debug("transform(Node)");
        NodeStatic nodeStatic = null;
        if (node == null) {
            return null;
        }
        try {
            nodeStatic = new NodeStatic();
            nodeStatic.setId(NumericUtils.revertLongAsString(node.getId()));
            nodeStatic.setTitle(node.getTitle());
            nodeStatic.setDescription(node.getDescription());
            nodeStatic.setBridgeNode(node.getBridgeNode());
            nodeStatic.setDepthFromRoot(node.getDepthFromRoot());
            nodeStatic.setIndexWithinParent(node.getIndexWithinParent());
            nodeStatic.setLinkNodeID(NumericUtils.revertLongAsString(node.getLinkNodeId()));
            nodeStatic.setListNode(node.getListNode());
            nodeStatic.setModificationDate(node.getUpdated());
            nodeStatic.setParentID(NumericUtils.revertLongAsString(node.getParentId()));
            nodeStatic.setSize(node.getSize());
            nodeStatic.setType(node.getType());
            //Process path
            nodeStatic.setPath(node.getPath());

            //process extra params
            Map<String, String> extraValues = node.getExtraValues();
            for (String property : extraValues.keySet()) {
                if (property.equals("parentName")) {
                    nodeStatic.setParentName(extraValues.get(property));
                } else if (property.equals("parentIDs")) {
                    nodeStatic.setParentIDs(extraValues.get(property));
                } else if (property.equals("parentIndexWithinParent")) {
                    nodeStatic.setParentIndexWithinParent(extraValues.get(property));
                } else if (property.equals("parentNames")) {
                    nodeStatic.setParentNames(extraValues.get(property));
                } else if (property.equals("childrenIDs")) {
                    nodeStatic.setChildrenIDs(extraValues.get(property));
                } else if (property.equals("childrenIndexWithinParent")) {
                    nodeStatic.setChildrenIndexWithinParent(extraValues.get(property));
                } else if (property.equals("childrenNames")) {
                    nodeStatic.setChildrenNames(extraValues.get(property));
                } else if (property.equals("musthaves")) {
                    nodeStatic.setMusthaves(extraValues.get(property));
                } else if (property.equals("siblingIDs")) {
                    nodeStatic.setSiblingIDs(extraValues.get(property));
                } else if (property.equals("siblingNames")) {
                    nodeStatic.setSiblingNames(extraValues.get(property));
                } else if (property.equals("siblingIndexWithinParent")) {
                    nodeStatic.setSiblingIndexWithinParent(extraValues.get(property));
                } else if (property.equals("grandParentIDs")) {
                    nodeStatic.setGrandParentIDs(extraValues.get(property));
                } else if (property.equals("grandParentNames")) {
                    nodeStatic.setGrandParentNames(extraValues.get(property));
                } else if (property.equals("grandParentID")) {
                    nodeStatic.setGrandParentID(extraValues.get(property));
                } else if (property.equals("grandParentName")) {
                    nodeStatic.setGrandParentName(extraValues.get(property));
                } else if (property.equals("grandParentIndexWithinParent")) {
                    nodeStatic.setGrandParentIndexWithinParent(extraValues.get(property));
                } else if (property.equals("signatures")) {
                    nodeStatic.setSignatures(extraValues.get(property));
                } else if (property.equals("signatureWeights")) {
                    nodeStatic.setSignatureWeights(extraValues.get(property));
                } else if (property.equals("taxonomyID")) {
                    nodeStatic.setTaxonomyID(extraValues.get(property));
                } else if (property.equals("taxonomyName")) {
                    nodeStatic.setTaxonomyName(extraValues.get(property));
                }
            }
        } catch (Exception e) {
            log.error("An exception has ocurred on transform(Node)", e);
        }
        return nodeStatic;
    }
}
