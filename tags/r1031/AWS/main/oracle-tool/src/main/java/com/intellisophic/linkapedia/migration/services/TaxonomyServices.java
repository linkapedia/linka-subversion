package com.intellisophic.linkapedia.migration.services;

import com.intellisophic.linkapedia.amazon.services.cloudsearch.bo.facade.CloudSearchServices;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBWrapperAbs;
import com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.DynamoDBHelper;
import com.intellisophic.linkapedia.api.beans.CloudSearchInfo;
import com.intellisophic.linkapedia.api.beans.NodeLink;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.Taxonomy;
import com.intellisophic.linkapedia.datasources.cloudsearch.CloudSearchObjectTransformer;
import com.intellisophic.linkapedia.datasources.oracle.OracleObjectTransformer;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.intellisophic.linkapedia.generic.utils.NumericUtils;
import com.intellisophic.linkapedia.migration.bo.models.Corpus;
import com.intellisophic.linkapedia.migration.bo.models.Node;
import com.intellisophic.linkapedia.migration.facade.TaxonomyFacade;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class TaxonomyServices {

    private static final Logger log = Logger.getLogger(TaxonomyServices.class);
    private TaxonomyFacade taxonomyFacade;
    //send batches of objects in CloudSearch
    private static final int DEFAULT_LIMIT_CLOUD_SEARCH = 5000;

    public TaxonomyServices() {
        taxonomyFacade = new TaxonomyFacade();
    }

    /**
     *
     * @param nodeId
     */
    public List<Long> populateCloudSearch(Long nodeId) {
        return populateCloudSearch(nodeId, DEFAULT_LIMIT_CLOUD_SEARCH);
    }

    /**
     * save all children in cloudSearch using the data of dynamo this include a
     * nodeId
     *
     * @param nodeId
     * @param limit
     * @return list of nodes with error
     *
     */
    public List<Long> populateCloudSearch(Long nodeId, int limit) {
        List<Long> nodesIds = null;
        List<Long> nodesWithError = null;
        try {
            nodesIds = taxonomyFacade.getChildrenByNodeId(nodeId);
        } catch (Exception ex) {
            log.error("Error getting the children for one node", ex);
        }

        //by each nodeId we should to create a information to send to cloudSeacrh
        //I use limit parameter to have a control in the memory
        List<CloudSearchInfo> data = new ArrayList<CloudSearchInfo>();
        nodesWithError = new ArrayList<Long>();
        CloudSearchInfo cloudSearchInfo;
        String path;
        int controlLimit = 1;
        int controlEnd = 0;
        DynamoDBWrapperAbs.env = "prod";//get data from production table: this is work arroud I am sorry
        for (Long id : nodesIds) {
            controlEnd++;
            NodeStatic node;
            try {
                node = DataHandlerFacade.getNode(NumericUtils.revertLongAsString(id));
            } catch (Exception e) {
                log.error("Error getting NodeStatic ", e);
                nodesWithError.add(id);
                continue;
            }
            if (node != null) {
                //create object of cloudsearchinfo
                cloudSearchInfo = new CloudSearchInfo();
                cloudSearchInfo.setCorpusId(node.getTaxonomyID());
                cloudSearchInfo.setCorpusName(node.getTaxonomyName());
                cloudSearchInfo.setNodeId(node.getId());
                cloudSearchInfo.setNodeName(node.getTitle());
                cloudSearchInfo.setNodeDesc(node.getDescription());
                path = node.getTaxonomyName();
                path = (node.getGrandParentName() == null ? path : path + "/" + node.getGrandParentName());
                path = (node.getParentName() == null ? path : path + "/" + node.getParentName());
                cloudSearchInfo.setNodePath(path);
                cloudSearchInfo.setNodePublic(true);

                data.add(cloudSearchInfo);

                log.debug("data size:" + data.size());

                //verify if I should insert the data
                if (controlLimit >= limit || controlEnd == nodesIds.size()) {
                    //send data
                    CloudSearchServices.updateNodes(data);
                    controlLimit = 1;
                    data.clear();
                    continue;
                }
            } else {
                log.info("Error getting NodeStatic: " + id);
                nodesWithError.add(id);
            }
            controlLimit++;
        }

        return nodesWithError;

    }

    public void migrateNodesFromOracleToDynamoDB(Long corpusID) {
        log.debug("migrateNodesFromOracleToDynamoDB(Long)");
        List<Node> nodesFromOracle = null;
        NodeStatic tmpNodeSt = null;
        List<CloudSearchInfo> batchInfo = new ArrayList<CloudSearchInfo>(10);
        DynamoDBWrapperAbs.env = "prod";//save in production table: this is work arroud I am sorry
        try {
            nodesFromOracle = getTaxonomyFacade().getNodesByCorpusId(corpusID, true);
            if (nodesFromOracle != null && !nodesFromOracle.isEmpty()) {
                log.debug("Got " + nodesFromOracle.size() + " records from Oracle");
                log.debug("Starting object convertion.");
                int cont = 1;
                for (Node node : nodesFromOracle) {
                    try {
                        //Transform object retrieved from Oracle
                        tmpNodeSt = OracleObjectTransformer.transform(node);
                        //Save the objects.
                        DataHandlerFacade.saveNode(tmpNodeSt);
                        //Send the object to Cloudsearch for storage.
                        if (batchInfo.isEmpty() || batchInfo.size() <= 10) {
                            batchInfo.add(CloudSearchObjectTransformer.transform(tmpNodeSt));
                        } else if (batchInfo.size() > 10) {
                            log.debug("Going to add the batch to CloudSearch API");
                            batchInfo.add(CloudSearchObjectTransformer.transform(tmpNodeSt));
                            CloudSearchServices.updateNodes(batchInfo);
                            batchInfo.clear();
                        }
                    } catch (Exception e) {
                        log.error("An exception ocurred while saving the object on dynamo/cloudsearch.", e);
                    }
                    log.debug("Node saved in dynamo/cloudSearch count: " + cont);
                    cont++;
                }
                if (batchInfo.size() > 0) {
                    CloudSearchServices.updateNodes(batchInfo);
                    batchInfo.clear();
                }
            } else {
                log.warn("No nodes found for corpusid {" + corpusID + "}");
            }
        } catch (Exception e) {
            log.error("An exception has ocurred on migrateNodesFromOracleToDynamoDB(Long)", e);
        }
    }

    public void migrateTaxonomyFromOracleToDynamoDB() {
        log.debug("migrateTaxonomyFromOracleToDynamoDB(Long)");
        List<Corpus> corpusFromOracle = null;
        Taxonomy tmpTaxonomy = null;
        DynamoDBWrapperAbs.env = "prod";//get data from production table: this is work arroud I am sorry
        try {
            corpusFromOracle = getTaxonomyFacade().getAllCorpus();
            if (corpusFromOracle != null && !corpusFromOracle.isEmpty()) {
                log.info("Got " + corpusFromOracle.size() + " records from Oracle");
                log.info("Starting object convertion.");
                for (Corpus corpus : corpusFromOracle) {
                    //Transform object retrieved from Oracle
                    tmpTaxonomy = OracleObjectTransformer.transform(corpus);
                    //Save the objects.
                    DataHandlerFacade.saveTaxonomy(tmpTaxonomy);
                }
            } else {
                log.warn("No taxonomies found");
            }
        } catch (Exception e) {
            log.error("An exception has ocurred on migrateTaxonomyFromOracleToDynamoDB(Long)", e);
        }
    }

    public void migrateLinkNodesFromOracleToDynamoDB(Long nodeId, Boolean isRecursive) {
        if (nodeId != 18974913) {
            log.info("For now just ***Knomor*** nodeId: 18974913 works!");
            return;
        }

        List<Node> linkNodesFromOracle = null;
        List<Long> children = null;
        NodeStatic nodeStatic = null;
        NodeLink nodeLink = null;
        Boolean isForAmazon = true;

        List<CloudSearchInfo> batchInfo = new ArrayList<CloudSearchInfo>(10);
        String tableName = "NodeLink";
        DynamoDBWrapperAbs.env = "prod";//get data from production table: this is work arroud I am sorry
        try {
            //linkNodesFromOracle = getTaxonomyFacade().getNodesByCorpusId(corpusID, true);
            linkNodesFromOracle = getTaxonomyFacade().getNodesByNodeId(nodeId, isForAmazon, isRecursive);

            if (linkNodesFromOracle != null && !linkNodesFromOracle.isEmpty()) {

                //create table if it doesn exist in AWS
                if (!DynamoDBHelper.tableExist(tableName)) {
                    Class<?> clazz = Class.forName("com.intellisophic.linkapedia.api.beans." + tableName);
                    DynamoDBHelper.createTableFromClass(clazz);
                }

                for (Node node : linkNodesFromOracle) {
                    nodeStatic = OracleObjectTransformer.transform(node);

                    //if parentId is the Id of the RootNode Taxonomy set ParentId -2 to recognize that the parent is the root of MAIN TAXONOMY
                    if (nodeId.equals(node.getParentId())) {
                        nodeStatic.setParentID("-2");
                    }
                    //if LinkNodeId equals to NodeId means that is a Static Node and should be in NodeStatic
                    if (nodeStatic.getId().equals(nodeStatic.getLinkNodeID())) {
                        nodeStatic.setBridgeNode(true);
                        DataHandlerFacade.saveNode(nodeStatic);
                        //Send the object to Cloudsearch for storage.
                        if (batchInfo.isEmpty() || batchInfo.size() <= 10) {
                            batchInfo.add(CloudSearchObjectTransformer.transform(nodeStatic));
                        } else if (batchInfo.size() > 10) {
                            log.debug("Going to add the batch to CloudSearch API");
                            batchInfo.add(CloudSearchObjectTransformer.transform(nodeStatic));
                            CloudSearchServices.updateNodes(batchInfo);
                            batchInfo.clear();
                        }
                    } //else the node is a LinkNode that points to another StaticNode 
                    else {
                        nodeLink = new NodeLink();
                        nodeLink.setNodeLinkId(nodeStatic.getId());
                        nodeLink.setNodeStaticLinkId(nodeStatic.getLinkNodeID());
                        nodeLink.setNodeParentStaticId(nodeStatic.getParentID());
                        nodeLink.setTitle(nodeStatic.getTitle());
                        DataHandlerFacade.saveNodeLink(nodeLink);
                    }
                }

                if (batchInfo.size() > 0) {
                    CloudSearchServices.updateNodes(batchInfo);
                    batchInfo.clear();
                }

            } else {
                //log.warn("No nodes found for corpusid {" + corpusID + "}");
            }
        } catch (Exception ex) {
            log.error("An exception has ocurred on migrateLinkNodesFromOracleToDynamoDB", ex);
        }

    }

    public TaxonomyFacade getTaxonomyFacade() {
        return taxonomyFacade;
    }

    public void setTaxonomyFacade(TaxonomyFacade taxonomyFacade) {
        this.taxonomyFacade = taxonomyFacade;
    }
}
