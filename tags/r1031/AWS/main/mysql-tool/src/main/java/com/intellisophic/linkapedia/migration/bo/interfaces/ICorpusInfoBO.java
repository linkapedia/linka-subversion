package com.intellisophic.linkapedia.migration.bo.interfaces;

import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import java.util.List;

/**
 *
 * @author Xander Kno
 */
public interface ICorpusInfoBO {

    public List<CorpusInfo> getCorpusInfoByNodeID(Long corpusID, Long nodeID) throws Exception;

    public List<CorpusInfo> getAllCorpusInfo(Long corpusID) throws Exception;
}
