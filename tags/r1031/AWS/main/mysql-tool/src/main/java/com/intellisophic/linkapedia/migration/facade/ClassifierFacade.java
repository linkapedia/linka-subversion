package com.intellisophic.linkapedia.migration.facade;

import com.intellisophic.linkapedia.migration.bo.CorpusInfoBO;
import com.intellisophic.linkapedia.migration.bo.interfaces.ICorpusInfoBO;
import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class ClassifierFacade {

    private static final Logger log = LoggerFactory.getLogger(ClassifierFacade.class);
    private ICorpusInfoBO corpusInfoBO;

    public List<CorpusInfo> getCorpusInfoByNodeID(Long corpusID, Long nodeID) throws Exception {
        log.debug("getCorpusInfoByNodeID(Long, Long)");
        return getCorpusInfoBO().getCorpusInfoByNodeID(corpusID, nodeID);
    }

    public List<CorpusInfo> getAllCorpusInfo(Long corpusID) throws Exception {
        log.debug("getAllCorpusInfo(Long)");
        return getCorpusInfoBO().getAllCorpusInfo(corpusID);
    }

    public ClassifierFacade() {
        this.corpusInfoBO = new CorpusInfoBO();
    }

    public ClassifierFacade(ICorpusInfoBO corpusInfoBO) {
        this.corpusInfoBO = corpusInfoBO;
    }

    public ICorpusInfoBO getCorpusInfoBO() {
        return corpusInfoBO;
    }

    public void setCorpusInfoBO(ICorpusInfoBO corpusInfoBO) {
        this.corpusInfoBO = corpusInfoBO;
    }
}
