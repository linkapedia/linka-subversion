package com.intellisophic.linkapedia.migration.dao.interfaces;

import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Xander Kno
 */
public interface ICorpusInfoDAO {

    public List<CorpusInfo> getCorpusInfoByNodeID(Long corpusID, Long nodeID) throws SQLException, Exception;

    public List<CorpusInfo> getAllCorpusInfo(Long corpusID) throws SQLException, Exception;
}
