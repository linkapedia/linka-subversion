/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.exceptions;

/**
 *
 * @author juanidrobo
 */
public class ObjectNotFoundException extends Exception {

    public ObjectNotFoundException() {
    }
    public ObjectNotFoundException(String message) {
        super(message);
    }
}
