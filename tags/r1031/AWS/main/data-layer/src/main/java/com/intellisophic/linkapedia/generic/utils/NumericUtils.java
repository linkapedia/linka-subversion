package com.intellisophic.linkapedia.generic.utils;

import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class NumericUtils {

    private static final Logger log = Logger.getLogger(NumericUtils.class);

    /**
     * Method to create a Long number reverting the order of the numbers of the given number
     * <b>Example: </b> Given the number 12345L this function will return 54321L.
     * <p/>
     * @param number
     * @return number inverted, in this case 54321L.
     */
    public static Long revertLong(Long number) {
        String inverseIDStr = "";
        Long reverseID = null;
        if (number == null) {
            return null;
        }
        try {
            String idStr = number.toString();
            for (int i = idStr.length() - 1; i >= 0; i--) {
                char character = idStr.charAt(i);
                inverseIDStr += character;
            }
            reverseID = Long.parseLong(inverseIDStr);
        } catch (Exception ex) {
            log.error("An exception has ocurred.", ex);
        }
        return reverseID;
    }

    /**
     * Method to create a String containing the number reverting the order of the numbers of the given number
     * <b>Example: </b> Given the number 12345L this function will return 54321L.
     * <p/>
     * @param number
     * @return number inverted, in this case 54321L.
     */
    public static String revertLongAsString(Long number) {
        String inverseIDStr = "";
        if (number == null) {
            return null;
        }
        if (number < 0L) {
            return String.valueOf(number);
        }
        try {
            String idStr = number.toString();
            for (int i = idStr.length() - 1; i >= 0; i--) {
                char character = idStr.charAt(i);
                inverseIDStr += character;
            }
        } catch (Exception ex) {
            log.error("An exception has ocurred.", ex);
        }
        return inverseIDStr;
    }
}
