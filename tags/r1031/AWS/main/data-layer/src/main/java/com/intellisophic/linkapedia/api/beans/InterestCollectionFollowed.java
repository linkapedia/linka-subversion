package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "InterestCollectionFollowed", hashKey = "userId", hashKeyType = "S", rangeKey = "interestCollectionId", rangeKeyType = "S")
@DynamoDBTable(tableName = "InterestCollectionFollowed")
public class InterestCollectionFollowed implements Serializable {

    private String userId;
    private String interestCollectionId;
    private Long timeStamp;
    //This is not going to Dynamo!
    private String interestCollectionName;

    @DynamoDBHashKey(attributeName = "userId")
    public String getUserId() {
        return userId;
    }

    @DynamoDBRangeKey(attributeName = "interestCollectionId")
    public String getInterestCollectionId() {
        return interestCollectionId;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public Long getTimeStamp() {
        return timeStamp;
    }

    @DynamoDBIgnore
    public String getInterestCollectionName() {
        return interestCollectionName;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setInterestCollectionId(String interestCollectionId) {
        this.interestCollectionId = interestCollectionId;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setInterestCollectionName(String interestCollectionName) {
        this.interestCollectionName = interestCollectionName;
    }
}
