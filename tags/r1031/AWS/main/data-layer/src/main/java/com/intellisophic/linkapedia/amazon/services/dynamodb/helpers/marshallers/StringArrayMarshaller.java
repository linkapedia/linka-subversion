package com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshaller;
import com.intellisophic.linkapedia.generic.utils.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class StringArrayMarshaller implements DynamoDBMarshaller<String[]> {

    private static final Logger log = Logger.getLogger(StringArrayMarshaller.class);
    private static final String AMAZON_FIELD_SEPARATOR = "||";
    private static final String AMAZON_FIELD_SEPARATOR_REGEX = "\\|\\|";

    @Override
    public String marshall(String[] values) {
        log.debug("marshall(String[])");
        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            sb.append(value).append(AMAZON_FIELD_SEPARATOR);
        }
        return StringUtils.rCharTrim(sb, AMAZON_FIELD_SEPARATOR);
    }

    @Override
    public String[] unmarshall(Class<String[]> clazz, String obj) {
        log.debug("unmarshall(Class<String[]>, String)");
        return obj.split(AMAZON_FIELD_SEPARATOR_REGEX);
    }
}
