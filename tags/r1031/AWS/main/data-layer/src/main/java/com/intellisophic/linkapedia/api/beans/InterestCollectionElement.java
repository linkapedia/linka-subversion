/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "InterestCollectionElement", hashKey = "interestCollectionId", hashKeyType = "S", rangeKey = "docId", rangeKeyType = "S")
@DynamoDBTable(tableName = "InterestCollectionElement")
public class InterestCollectionElement implements Serializable {

    private String interestCollectionId;
    private String docId;
    private String nodeId;
    private String nodeName;
    private String docTitle;
    private String digestText;
    private int order;
    private String userReason;
    private long timeStamp;
    //This is not going to Dynamo!
    private String interestCollectionName;

    @DynamoDBHashKey(attributeName = "interestCollectionId")
    public String getInterestCollectionId() {
        return interestCollectionId;
    }

    @DynamoDBRangeKey(attributeName = "docId")
    public String getDocId() {
        return docId;
    }

    @DynamoDBAttribute(attributeName = "nodeId")
    public String getNodeId() {
        return nodeId;
    }

    @DynamoDBAttribute(attributeName = "nodeName")
    public String getNodeName() {
        return nodeName;
    }

    @DynamoDBAttribute(attributeName = "docTitle")
    public String getDocTitle() {
        return docTitle;
    }

    @DynamoDBAttribute(attributeName = "digestText")
    public String getDigestText() {
        return digestText;
    }

    @DynamoDBAttribute(attributeName = "order")
    public int getOrder() {
        return order;
    }

    @DynamoDBAttribute(attributeName = "userReason")
    public String getUserReason() {
        return userReason;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public long getTimeStamp() {
        return timeStamp;
    }

    public String getInterestCollectionName() {
        return interestCollectionName;
    }

    public void setInterestCollectionId(String interestCollectionId) {
        this.interestCollectionId = interestCollectionId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public void setDocTitle(String docTitle) {
        this.docTitle = docTitle;
    }

    public void setDigestText(String digestText) {
        this.digestText = digestText;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setUserReason(String userReason) {
        this.userReason = userReason;
    }

    public void setInterestCollectionName(String interestCollectionName) {
        this.interestCollectionName = interestCollectionName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
