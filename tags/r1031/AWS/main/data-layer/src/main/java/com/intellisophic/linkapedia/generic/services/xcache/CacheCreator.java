package com.intellisophic.linkapedia.generic.services.xcache;

/**
 *
 * @author juanidrobo
 */
public abstract class CacheCreator {
  
    
    public ICache create(String cacheId) {
        return factoryMethod(cacheId);
    }
     
    protected abstract ICache factoryMethod(String cacheId);
}
