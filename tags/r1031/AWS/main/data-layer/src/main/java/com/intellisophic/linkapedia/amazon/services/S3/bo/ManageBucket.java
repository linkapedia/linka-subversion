package com.intellisophic.linkapedia.amazon.services.S3.bo;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.intellisophic.linkapedia.amazon.services.security.credentials.CredentialHandler;
import java.io.InputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 *
 */
public class ManageBucket extends CredentialHandler {

    private static final Logger log = Logger.getLogger(ManageBucket.class);
    private AmazonS3 s3Client;
    private String name;

    public ManageBucket(String bucketName) {
        this.name = bucketName;
        //my configuration
        ClientConfiguration clienConf = new ClientConfiguration();
        //set 60 min in the conecction timeout
        clienConf.setSocketTimeout(3600000);
        clienConf.setProtocol(Protocol.HTTP);
        clienConf.setConnectionTimeout(3600000);
        this.s3Client = new AmazonS3Client(getCredentials(), clienConf);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to save data on the Bucket
     *
     * @param key
     * @param data
     * @param meta
     * @return
     */
    public boolean saveData(String key, InputStream data, ObjectMetadata meta) {
        log.debug("Class ManageBucket: saveData(String key, InputStream data,"
                + "ObjectMetadata meta)");
        try {
            getClient().putObject(new PutObjectRequest(name, key, data, meta));
            log.info("Save data key -> " + key + " Ok!");
        } catch (AmazonServiceException es) {
            log.error("Caught an AmazonServiceException, which means your "
                    + "request made it to Amazon S3, but was rejected "
                    + "with an error response for some reason.");
            log.error("Error Message:    " + es.getMessage());
            log.error("HTTP Status Code: " + es.getStatusCode());
            log.error("AWS Error Code:   " + es.getErrorCode());
            log.error("Error Type:       " + es.getErrorType());
            log.error("Request ID:       " + es.getRequestId());
            return false;
        } catch (AmazonClientException ec) {
            log.error("Caught an AmazonClientException, which means the "
                    + "client encountered a serious internal problem while trying "
                    + "to communicate with S3, such as not being able to access"
                    + " the network.");
            log.error("Error Message: " + ec.getMessage());
            return false;
        }
        return true;
    }

    /**
     *
     * @param key
     * @param data
     * @return
     * @throws Exception
     */
    public boolean saveData(String key, InputStream data) {
        return saveData(key, data, new ObjectMetadata());
    }

    /**
     * move a key to another bucket
     *
     * @param keySource
     * @param bucketDestination
     * @param keyDestination
     * @return
     * @throws Exception
     */
    public boolean copyData(String keySource, ManageBucket bucketDestination, String keyDestination) throws Exception {
        log.debug("copyData(String, ManageBucket, String)");

        CopyObjectRequest copyObjRequest = new CopyObjectRequest(name, keySource, bucketDestination.getName(), keyDestination);
        log.info("Copying object. into :" + bucketDestination.getName() + "/" + keyDestination);
        try {
            getClient().copyObject(copyObjRequest);
        } catch (AmazonServiceException es) {
            log.error("Caught an AmazonServiceException, which means your "
                    + "request made it to Amazon S3, but was rejected "
                    + "with an error response for some reason.");
            log.error("Error Message:    " + es.getMessage());
            log.error("HTTP Status Code: " + es.getStatusCode());
            log.error("AWS Error Code:   " + es.getErrorCode());
            log.error("Error Type:       " + es.getErrorType());
            log.error("Request ID:       " + es.getRequestId());
        } catch (AmazonClientException ec) {
            log.error("Caught an AmazonClientException, which means the "
                    + "client encountered a serious internal problem while trying "
                    + "to communicate with S3, such as not being able to access"
                    + " the network.");
            log.error("Error Message: " + ec.getMessage());
        }
        return true;
    }

    /**
     * list files in a bucket and a prefix
     * set null in the optional parameters if you not want to use this
     * <p/>
     * @param prefix
     * @param delimiter
     * @param maxkeys
     * @param marker
     * @return
     */
    public ObjectListing listFiles(String prefix, String delimiter, Integer maxkeys, String marker) {
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(this.getName());

        //Validate parameters
        if (prefix != null) {
            listObjectsRequest.withPrefix(prefix);
        }
        if (delimiter != null) {
            listObjectsRequest.withDelimiter(delimiter);
        }
        if (maxkeys != null) {
            listObjectsRequest.withMaxKeys(maxkeys);
        }
        if (marker != null) {
            listObjectsRequest.withMarker(marker);
        }

        //send the request
        ObjectListing objectListing = new ObjectListing();

        try {
            objectListing = getClient().listObjects(listObjectsRequest);
        } catch (AmazonServiceException es) {
            log.error("Caught an AmazonServiceException, which means your "
                    + "request made it to Amazon S3, but was rejected "
                    + "with an error response for some reason.");
            log.error("Error Message:    " + es.getMessage());
            log.error("HTTP Status Code: " + es.getStatusCode());
            log.error("AWS Error Code:   " + es.getErrorCode());
            log.error("Error Type:       " + es.getErrorType());
            log.error("Request ID:       " + es.getRequestId());
        } catch (AmazonClientException ec) {
            log.error("Caught an AmazonClientException, which means the "
                    + "client encountered a serious internal problem while trying "
                    + "to communicate with S3, such as not being able to access"
                    + " the network.");
            log.error("Error Message: " + ec.getMessage());
        }
        return objectListing;

    }

    /**
     * Get the data as InputStream
     * <p/>
     * @param key Object identifier inside the bucket.
     * @return
     * @throws Exception
     * @see java.io.InputStream
     */
    public InputStream getData(String key) throws Exception {
        log.debug("getData(String)");
        S3Object object = getClient().getObject(new GetObjectRequest(getName(), key));
        if (object != null) {
            return object.getObjectContent();
        } else {
            throw new Exception("Sorry!, The key is not found.");
        }
    }

    private AmazonS3 getClient() {
        return s3Client;
    }
}
