package com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshaller;
import com.intellisophic.linkapedia.generic.utils.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class LongArrayMarshaller implements DynamoDBMarshaller<Long[]> {

    private static final Logger log = Logger.getLogger(LongArrayMarshaller.class);
    private static final String AMAZON_FIELD_SEPARATOR = "||";
    private static final String AMAZON_FIELD_SEPARATOR_REGEX = "\\|\\|";

    @Override
    public String marshall(Long[] values) {
        log.debug("marshall(Long[])");
        StringBuilder sb = new StringBuilder();
        for (Long value : values) {
            sb.append(value).append(AMAZON_FIELD_SEPARATOR);
        }
        return StringUtils.rCharTrim(sb, AMAZON_FIELD_SEPARATOR);
    }

    @Override
    public Long[] unmarshall(Class<Long[]> clazz, String obj) {
        log.debug("unmarshall(Class<Long[]>, String)");
        String[] values = obj.split(AMAZON_FIELD_SEPARATOR_REGEX);
        Long[] returnValues = new Long[values.length];
        for (int i = 0; i < returnValues.length; i++) {
            try {
                returnValues[i] = Long.valueOf(values[i]);
            } catch (NumberFormatException numberFormatException) {
                log.warn("No valid format for long value {" + values[i] + "}");
                returnValues[i] = -1L;
            }
        }
        return returnValues;
    }
}
