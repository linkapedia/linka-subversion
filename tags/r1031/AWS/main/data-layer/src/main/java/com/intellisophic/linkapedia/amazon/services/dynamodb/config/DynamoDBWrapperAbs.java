package com.intellisophic.linkapedia.amazon.services.dynamodb.config;

import com.amazonaws.services.dynamodb.AmazonDynamoDB;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMappingException;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedScanList;
import java.util.List;

/**
 *
 * @author juanidrobo
 */
public class DynamoDBWrapperAbs extends DynamoDBMapper {

    public static String env = "newdev"; //please not change this variable
    DynamoDBMapperConfig dBMapperConfig = null;

    public DynamoDBWrapperAbs(AmazonDynamoDB dynamoDB) {
        super(dynamoDB);
    }

    public DynamoDBWrapperAbs(AmazonDynamoDB dynamoDB, DynamoDBMapperConfig config) {
        super(dynamoDB, config);
    }

    @Override
    public <T extends Object> void save(T object) {
        super.save(object, this.getDBMapperConfig(object.getClass()));
    }

    @Override
    public <T extends Object> T load(Class<T> clazz, Object hashKey) {

        return super.load(clazz, hashKey, this.getDBMapperConfig(clazz));
    }

    @Override
    public <T extends Object> T load(Class<T> clazz, Object hashKey, Object rangeKey) {
        return super.load(clazz, hashKey, rangeKey, this.getDBMapperConfig(clazz));
    }

    @Override
    public void delete(Object object) {

        super.delete(object, this.getDBMapperConfig(object.getClass()));
    }

    @Override
    public <T extends Object> PaginatedQueryList<T> query(Class<T> clazz, DynamoDBQueryExpression queryExpression) {
        return super.query(clazz, queryExpression, this.getDBMapperConfig(clazz));
    }

    @Override
    public <T extends Object> PaginatedScanList<T> scan(Class<T> clazz, DynamoDBScanExpression scanExpression) {

        return super.scan(clazz, scanExpression, this.getDBMapperConfig(clazz));
    }

    @Override
    public <T extends Object> PaginatedScanList<T> scan(Class<T> clazz, DynamoDBScanExpression scanExpression, DynamoDBMapperConfig config) {
        return super.scan(clazz, scanExpression, this.getDBMapperConfig(clazz));
    }

    @Override
    public void batchSave(List<? extends Object> objectsToSave) {
        super.batchSave(objectsToSave,this.getDBMapperConfig(objectsToSave.get(0).getClass()));
    }
    
    @Override
    public void batchSave(Object[] objectsToSave) {
    super.batchSave(objectsToSave,this.getDBMapperConfig(objectsToSave[0].getClass()));
    }
    
    
    private DynamoDBMapperConfig getDBMapperConfig(Class<?> o) {

        String dataTableName = "";
        if (o.isAnnotationPresent(DynamoDBTable.class)) {
            DynamoDBTable dBTable = o.getAnnotation(DynamoDBTable.class);
            dataTableName = dBTable.tableName();
        } else {
            throw new DynamoDBMappingException("Annotation TableName not found");
        }


        if (DynamoDBWrapperAbs.env.equals("newdev") || DynamoDBWrapperAbs.env.equals("local")) {
            dataTableName = "dev_" + dataTableName;
        }


        dBMapperConfig = new DynamoDBMapperConfig(new DynamoDBMapperConfig.TableNameOverride(dataTableName));
        return dBMapperConfig;
    }
}
