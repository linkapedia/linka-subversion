package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;


/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "UserNode", hashKey = "userId", hashKeyType = "S", rangeKey = "nodeId", rangeKeyType = "S")
@DynamoDBTable(tableName = "UserNode")
public class UserNode implements Serializable {

    /**
     * unique/key/userId
     */
    private String userId;
    private String nodeId;
    private String nodeTitle;
    private Long timeStamp;

    @DynamoDBHashKey(attributeName = "userId")
    public String getUserId() {
        return userId;
    }

    @DynamoDBRangeKey(attributeName = "nodeId")
    public String getNodeId() {
        return nodeId;
    }

    @DynamoDBAttribute(attributeName = "nodeTitle")
    public String getNodeTitle() {
        return nodeTitle;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
