package com.intellisophic.linkapedia.api.beans.enums;

/**
 *
 * @author Xander Kno
 */
public enum UserRole {

    ADMIN("admin"), REGISTERED("registered"), GUEST("guest");
    private String userRole;

    UserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRole() {
        return this.userRole;
    }

    @Override
    public String toString() {
        return "UserRole{" + "userRole=" + userRole + '}';
    }
}
