/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.exceptions;

/**
 *
 * @author juanidrobo
 */
public class ObjectAlreadyExistsException extends Exception {

    public ObjectAlreadyExistsException() {
    }
    public ObjectAlreadyExistsException(String message) {
        super(message);
    }
}
