package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Xander Kno
 */
@AmazonDynamoDB(tableName = "DocumentNode", hashKey = "docID", hashKeyType = "S", rangeKey = "nodeID", rangeKeyType = "S")
@DynamoDBTable(tableName = "DocumentNode")
public class DocumentNode implements Serializable {

    /**
     * MD5 Hash generated based on the doc URL.
     */
    private String docID;
    private String nodeID;
    private String title;
    private String URL;
    private String nodeName;
    private Float score01;
    private Float score02;
    private Float score03;
    private Float score04;
    private Float score05;
    private Float score06;
    private Float score07;
    private Float score08;
    private Float score09;
    private Float score10;
    private Float score11;
    private Float score12;
    private Float pagerank;
    private Date lastFound;
    private boolean modified;
    private long timeStamp;

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public void setNodeID(String nodeID) {
        this.nodeID = nodeID;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public void setScore01(Float score01) {
        this.score01 = score01;
    }

    public void setScore02(Float score02) {
        this.score02 = score02;
    }

    public void setScore03(Float score03) {
        this.score03 = score03;
    }

    public void setScore04(Float score04) {
        this.score04 = score04;
    }

    public void setScore05(Float score05) {
        this.score05 = score05;
    }

    public void setScore06(Float score06) {
        this.score06 = score06;
    }

    public void setScore07(Float score07) {
        this.score07 = score07;
    }

    public void setScore08(Float score08) {
        this.score08 = score08;
    }

    public void setScore09(Float score09) {
        this.score09 = score09;
    }

    public void setScore10(Float score10) {
        this.score10 = score10;
    }

    public void setScore11(Float score11) {
        this.score11 = score11;
    }

    public void setScore12(Float score12) {
        this.score12 = score12;
    }

    public void setPagerank(Float pagerank) {
        this.pagerank = pagerank;
    }

    public void setLastFound(Date lastFound) {
        this.lastFound = lastFound;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @DynamoDBHashKey(attributeName = "docID")
    public String getDocID() {
        return docID;
    }

    @DynamoDBRangeKey(attributeName = "nodeID")
    public String getNodeID() {
        return nodeID;
    }

    @DynamoDBAttribute(attributeName = "title")
    public String getTitle() {
        return title;
    }

    @DynamoDBAttribute(attributeName = "url")
    public String getURL() {
        return URL;
    }

    @DynamoDBAttribute(attributeName = "nodeName")
    public String getNodeName() {
        return nodeName;
    }

    @DynamoDBAttribute(attributeName = "score01")
    public Float getScore01() {
        return score01;
    }

    @DynamoDBAttribute(attributeName = "score02")
    public Float getScore02() {
        return score02;
    }

    @DynamoDBAttribute(attributeName = "score03")
    public Float getScore03() {
        return score03;
    }

    @DynamoDBAttribute(attributeName = "score04")
    public Float getScore04() {
        return score04;
    }

    @DynamoDBAttribute(attributeName = "score05")
    public Float getScore05() {
        return score05;
    }

    @DynamoDBAttribute(attributeName = "score06")
    public Float getScore06() {
        return score06;
    }

    @DynamoDBAttribute(attributeName = "score07")
    public Float getScore07() {
        return score07;
    }

    @DynamoDBAttribute(attributeName = "score08")
    public Float getScore08() {
        return score08;
    }

    @DynamoDBAttribute(attributeName = "score09")
    public Float getScore09() {
        return score09;
    }

    @DynamoDBAttribute(attributeName = "score10")
    public Float getScore10() {
        return score10;
    }

    @DynamoDBAttribute(attributeName = "score11")
    public Float getScore11() {
        return score11;
    }

    @DynamoDBAttribute(attributeName = "score12")
    public Float getScore12() {
        return score12;
    }

    @DynamoDBAttribute(attributeName = "pagerank")
    public Float getPagerank() {
        return pagerank;
    }

    @DynamoDBAttribute(attributeName = "lastFound")
    public Date getLastFound() {
        return lastFound;
    }

    @DynamoDBIgnore
    public boolean isModified() {
        return modified;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DocumentNode{" + "docID=").append(docID).append(", nodeID=").append(nodeID).append(", title=").append(title).append(", URL=").append(URL).append(", nodeName=").append(nodeName).append(", score01=").append(score01).append(", score02=").append(score02).append(", pagerank=").append(pagerank).append(", modified=").append(modified).append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.docID != null ? this.docID.hashCode() : 0);
        hash = 79 * hash + (this.nodeID != null ? this.nodeID.hashCode() : 0);
        hash = 79 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 79 * hash + (this.URL != null ? this.URL.hashCode() : 0);
        hash = 79 * hash + (this.nodeName != null ? this.nodeName.hashCode() : 0);
        hash = 79 * hash + (this.score01 != null ? this.score01.hashCode() : 0);
        hash = 79 * hash + (this.score02 != null ? this.score02.hashCode() : 0);
        hash = 79 * hash + (this.pagerank != null ? this.pagerank.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DocumentNode other = (DocumentNode) obj;
        if ((this.docID == null) ? (other.docID != null) : !this.docID.equals(other.docID)) {
            return false;
        }
        if (this.nodeID != other.nodeID && (this.nodeID == null || !this.nodeID.equals(other.nodeID))) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.URL == null) ? (other.URL != null) : !this.URL.equals(other.URL)) {
            return false;
        }
        if ((this.nodeName == null) ? (other.nodeName != null) : !this.nodeName.equals(other.nodeName)) {
            return false;
        }
        if (this.score01 != other.score01 && (this.score01 == null || !this.score01.equals(other.score01))) {
            return false;
        }
        if (this.score02 != other.score02 && (this.score02 == null || !this.score02.equals(other.score02))) {
            return false;
        }
        if (this.pagerank != other.pagerank && (this.pagerank == null || !this.pagerank.equals(other.pagerank))) {
            return false;
        }
        return true;
    }
}
