package com.intellisophic.linkapedia.amazon.services.emr.helpers;

import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsResult;
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
import com.amazonaws.services.elasticmapreduce.model.PlacementType;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;
import com.amazonaws.services.elasticmapreduce.util.StepFactory;
import com.intellisophic.linkapedia.amazon.services.emr.config.AmazonEMRClientAbs;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class MapReduceHelper extends AmazonEMRClientAbs {

    private static final Logger log = LoggerFactory.getLogger(MapReduceHelper.class);
    private static final String EXECUTION_COUNTER = "27";
    private static final String INSTANCE_TYPE = "c1.medium";

    public static void main(String[] args) {
        AmazonElasticMapReduce service = getAmazonEMRClient();
        JobFlowInstancesConfig conf = new JobFlowInstancesConfig();
        conf.setHadoopVersion("1.0.3");
        conf.setEc2KeyName("hadoopkey");
        conf.setInstanceCount(1);
        conf.setKeepJobFlowAliveWhenNoSteps(false);
        conf.setMasterInstanceType(INSTANCE_TYPE);
        conf.setPlacement(new PlacementType("us-east-1a"));
        conf.setSlaveInstanceType(INSTANCE_TYPE);

        String jobFlowName = "ImageJob" + EXECUTION_COUNTER;
        log.debug(jobFlowName);

        List arguments = new LinkedList();
        arguments.add("s3n://hadoopfiles/input/369854_375434_a_11.80");
        arguments.add("s3n://hadoopfiles/outputs/" + jobFlowName + "/" + EXECUTION_COUNTER + "/ProcessImages/");

        HadoopJarStepConfig jarsetup = new HadoopJarStepConfig()
                .withArgs(arguments)
                .withJar("s3n://hadoopfiles/WordCount.jar")
                .withMainClass("com.intellisophic.linkapedia.amazon.services.emr.imagejob.ImageJob");

        //Debugging
        StepFactory stepFactory = new StepFactory();

        StepConfig enableDebugging = new StepConfig()
                .withName("Enable Debugging")
                .withActionOnFailure("TERMINATE_JOB_FLOW")
                .withHadoopJarStep(stepFactory.newEnableDebuggingStep());

        StepConfig imageProcess = new StepConfig()
                .withName("Process Images")
                .withActionOnFailure("CANCEL_AND_WAIT")
                .withHadoopJarStep(jarsetup);

        RunJobFlowRequest request = new RunJobFlowRequest()
                .withInstances(conf).withLogUri("s3n://hadoopflowlogs/imagejob/" + EXECUTION_COUNTER)
                .withName(jobFlowName)
                .withSteps(enableDebugging, imageProcess);

        String jobFlowID = invokeRunJobFlow(service, request);
        DescribeJobFlowsResult describeRs = getJobFlowDetails(service, jobFlowID);
        System.out.println("JobFlow: " + describeRs);
    }

    /**
     * Run Job Flow request sample
     * <p/>
     * @param service instance of AmazonElasticMapReduce service
     * @param request Action to invoke
     */
    public static String invokeRunJobFlow(AmazonElasticMapReduce service, RunJobFlowRequest request) {
        RunJobFlowResult result = service.runJobFlow(request);
        return result.getJobFlowId();
    }

    public static DescribeJobFlowsResult getJobFlowDetails(AmazonElasticMapReduce service, String jobID) {
        DescribeJobFlowsRequest request = new DescribeJobFlowsRequest()
                .withJobFlowIds(jobID);
        return service.describeJobFlows(request);
    }
}
