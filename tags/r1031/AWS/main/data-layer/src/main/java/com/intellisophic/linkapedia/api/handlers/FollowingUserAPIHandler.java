package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.FollowingUser;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class FollowingUserAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(FollowingUserAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(FollowingUserAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<FollowingUser> getFollowingUsersByFollowerUserId(String followerUserId) throws Exception {
        log.info("getFollowingUsersByFollowerUserId - followerUserId: " + followerUserId);
        List<FollowingUser> returnList = new ArrayList<FollowingUser>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(followerUserId);
            if (cacheObj != null) {
                return (List<FollowingUser>) cacheObj;
            } else {
                PaginatedQueryList<FollowingUser> result = getDynamoDBMapper().query(
                        FollowingUser.class, new DynamoDBQueryExpression(new AttributeValue(followerUserId)));

                for (FollowingUser followingUser : result) {
                    if (followingUser != null) {
                        returnList.add(followingUser);
                    }
                }
                cache.set(followerUserId, returnList, ONE_HOUR);
            }
        } catch (Exception ex) {
            log.error("Exception in getFollowingUsersByFollowerUserId - followerUserId: " + followerUserId + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;

    }

    public static int getFollowingsCount(String followerUserId) throws Exception {

        log.info("getFollowingsCount - followerUserId: " + followerUserId);

        List<FollowingUser> followingUsersList = new ArrayList<FollowingUser>();
        Object cacheObj = null;
        int count;

        try {
            cacheObj = cache.get(followerUserId);
            if (cacheObj != null) {
                followingUsersList = (List<FollowingUser>) cacheObj;

            } else {
                followingUsersList = getFollowingUsersByFollowerUserId(followerUserId);
            }
            count = followingUsersList.size();
        } catch (Exception ex) {
            log.error("Exception in getFollowingsCount - followerUserId: " + followerUserId + " - " + ex.getMessage());
            throw ex;
        }

        return count;
    }

    //this method use the events table
    public static void saveFollowingUser(FollowingUser followingUser) throws Exception {
        log.info("saveFollowingUser - FollowingUser");
        try {
            getDynamoDBMapper().save(followingUser);
            //event
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(followingUser.getFollowingUserId());
            userEvents.registerEvent(followingUser, UserFeedAPIHandler.FOLLOW);
            cache.delete(followingUser.getFollowingUserId()); //delete from cache to ensure the data consistence
        } catch (Exception ex) {
            log.error("Exception in saveFollowingUser: " + ex.getMessage());
            throw ex;
        }
    }

    //this method use the events table
    public static void deleteFollowingUser(FollowingUser followingUser) throws Exception {
        log.info("deleteFollowingUser - FollowingUser");
        try {
            getDynamoDBMapper().delete(followingUser);
            //event
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(followingUser.getFollowingUserId());
            userEvents.registerEvent(followingUser, UserFeedAPIHandler.UNFOLLOW);
            cache.delete(followingUser.getFollowingUserId());
        } catch (Exception ex) {
            log.error("Exception in deleteFollowingUser: " + ex.getMessage());
            throw ex;
        }

    }
}
