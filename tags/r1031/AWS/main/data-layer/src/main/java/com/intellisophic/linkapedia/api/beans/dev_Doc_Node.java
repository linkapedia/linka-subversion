/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;
import java.nio.ByteBuffer;



/**
 *
 * @author Sahar Ebadi
 */
@AmazonDynamoDB(tableName = "dev_Doc_Node", hashKey = "docId", hashKeyType = "S")
@DynamoDBTable(tableName = "dev_Doc_Node")
public class dev_Doc_Node implements Serializable {

    private String docId;

    /**
     * MD5 Hash of the docURL.
     */
    private ByteBuffer nodeList;


    public void setDocId(String docId) {
        this.docId = docId;
    }

    public void setNodeList(ByteBuffer nodeList) throws Exception {
        //ByteBuffer nodeListBytBuff =  compressString(nodeList);
        this.nodeList = nodeList;
    }


    @DynamoDBHashKey(attributeName = "docId")
    public String getDocId() {
        return docId;
    }

    @DynamoDBAttribute(attributeName = "nodeList")
    public ByteBuffer getNodeList(){
        return nodeList;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Doc_Node{" + "docId=").append(docId).append(", nodeList=").append(nodeList).append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.docId != null ? this.docId.hashCode() : 0);
        hash = 29 * hash + (this.nodeList != null ? this.nodeList.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final dev_Doc_Node other = (dev_Doc_Node) obj;
        if (this.docId != other.docId && (this.docId == null || !this.docId.equals(other.docId))) {
            return false;
        }
        if ((this.nodeList == null) ? (other.nodeList != null) : !this.nodeList.equals(other.nodeList)) {
            return false;
        }
        return true;
    }

}
