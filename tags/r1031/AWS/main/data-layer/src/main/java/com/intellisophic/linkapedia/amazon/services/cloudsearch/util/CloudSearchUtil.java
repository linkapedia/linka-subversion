package com.intellisophic.linkapedia.amazon.services.cloudsearch.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 * Util to access to cloud search
 *
 * @author andres
 *
 */
public class CloudSearchUtil {

    private static final Logger LOG = Logger.getLogger(CloudSearchUtil.class);
    private static final Properties prop = new Properties();

    static {
        try {
            prop.load(CloudSearchUtil.class.getClass().getResourceAsStream("/system/config.properties"));
        } catch (Exception e) {
            LOG.error("Error getting properties", e);
        }

    }

    /**
     * get timestamp from a web service
     *
     * @return
     * @throws IOException
     */
    public static long getTimestamp() throws IOException, Exception {
        if (prop == null || prop.isEmpty()) {
            throw new Exception("Error, properties file is not loaded");
        }
        String urlString = prop.getProperty("org.linkapedia.cloudsearch.webservice.time");
        URL url = null;
        HttpURLConnection con = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            url = new URL(urlString);
            con = (HttpURLConnection) (url.openConnection());

            con.setDoInput(true);
            con.connect();

            isr = new InputStreamReader(con.getInputStream());
            br = new BufferedReader(isr);
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            // parse json
            JSONObject jObject = JSONObject.fromObject(sb.toString());
            String data = jObject.getJSONObject("Result").getString("Timestamp");
            return Long.parseLong(data);
        } finally {
            // close resources
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException e) {
                    LOG.error("Error closing InputStremReader", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    LOG.error("Error closing BufferedReader", e);
                }
            }
            if (con != null) {
                con.disconnect();
            }
        }

    }

    /**
     * get url ton save documents from a properties file
     *
     * @return
     */
    public static String getURL() {
        String url = prop.getProperty("org.linkapedia.cloudsearch.batches.api");
        return url;
    }

    /**
     * show message from the api cloud search
     *
     * @param json
     * @return
     */
    public static String parseResponse(String json) {
        JSONObject data = null;
        StringBuilder sb = new StringBuilder();
        try {
            data = JSONObject.fromObject(json);
            String status = data.getString("status");
            // parse message ok
            if (status.equalsIgnoreCase("success")) {
                sb.append("Success!").append("\n");
            } // parse message wit errors
            else if (status.equalsIgnoreCase("error")) {
                sb.append("Error!").append("\n");
                JSONArray jarr = data.getJSONArray("errors");
                if (jarr != null) {
                    int index = 0;
                    //show each error message.
                    for (Object obj : jarr) {
                        JSONObject oAux = (JSONObject) obj;
                        sb.append("message ").append(index).append(" :");
                        sb.append(oAux.getString("message")).append("\n");
                        index++;
                    }
                }
            }
            // show add and delete objects
            sb.append("adds: ").append(data.getString("adds")).append("\n");
            sb.append("deletes: ").append(data.getString("deletes")).append("\n");

            return sb.toString();
        } catch (Exception e) {
            LOG.error("Error parsing response from Cloud Search API", e);
        }
        return json;
    }
}
