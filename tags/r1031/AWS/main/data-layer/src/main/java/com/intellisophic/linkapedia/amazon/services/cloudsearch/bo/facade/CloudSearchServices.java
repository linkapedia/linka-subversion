package com.intellisophic.linkapedia.amazon.services.cloudsearch.bo.facade;

import com.intellisophic.linkapedia.amazon.services.cloudsearch.bo.BatchesObject;
import com.intellisophic.linkapedia.amazon.services.cloudsearch.bo.CloudSearchObjectAWS;
import com.intellisophic.linkapedia.amazon.services.cloudsearch.util.CloudSearchUtil;
import com.intellisophic.linkapedia.api.beans.CloudSearchInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

/**
 * manage services CloudSearch update, delete
 *
 * @author andres
 *
 */
public class CloudSearchServices {

    private static final Logger LOG = Logger.getLogger(CloudSearchServices.class);
    private static final String ATTION_ADD = "add";
    private static final String ACTION_DELETE = "delete";

    /**
     * update only one node
     *
     * @param data
     */
    public static void updateNode(CloudSearchInfo data) {
        List<CloudSearchInfo> aux = new ArrayList<CloudSearchInfo>();
        aux.add(data);
        updateNodes(aux);
    }

    /**
     * add several document into CloudSearch
     *
     * @param data
     * @return a report with the results
     */
    public static void updateNodes(List<CloudSearchInfo> data) {
        // validate parameter
        if (data == null) {
            LOG.error("data can't be null");
            return;
        }
        // version for this batches
        long timeVersion;
        try {
            timeVersion = CloudSearchUtil.getTimestamp();
        } catch (IOException e1) {
            LOG.error("IOException getting TimeStamp", e1);
            return;
        } catch (Exception e) {
            LOG.error("General Exception getting TimeStamp", e);
            return;
        }
        // setting the file
        BatchesObject batches = new BatchesObject();
        int index = 0;
        while (index < data.size()) {
            //get current data
            CloudSearchInfo currentData = data.get(index);
            //build the correct document for send to cloud search.
            CloudSearchObjectAWS cloudAWS = new CloudSearchObjectAWS(currentData);
            // set header parameter
            cloudAWS.setHeadParameter(ATTION_ADD,
                    String.valueOf(currentData.getNodeId()), timeVersion);
            int status = batches.addObject(cloudAWS);
            // validate the data
            if (status == BatchesObject.STATUS_BIG_FILE) {
                index++;
                continue;
            }
            if (status == BatchesObject.STATUS_OK) {
                index++;
                if (index == data.size()) {
                    sendData(batches);
                    break;
                }
                continue;
            }
            if (status == BatchesObject.STATUS_BIG_BATCHES) {
                //send the data
                sendData(batches);
                batches.reset();
            }
        }
        // show nodes with error
        List<String> nodesWithError = BatchesObject.getErrorNodes();

        if (nodesWithError == null || nodesWithError.isEmpty()) {
            LOG.info("Nodes with error: 0");
        } else {
            LOG.info("Nodes with error: " + nodesWithError.size());
            for (String id : nodesWithError) {
                LOG.info("ID: " + id);
            }
        }
    }

     /**
     * delete one node in cloud search
     * @param data 
     */
    public static void deleteNode(CloudSearchInfo data) {
        throw new UnsupportedOperationException("Implement me please!");
    }
    
    /**
     * delete a group nodes
     * @param data 
     */
    public static void deleteNodes(List<CloudSearchInfo> data) {
        throw new UnsupportedOperationException("Implement me please!");
    }

    /**
     * delete all domain
     * @param domainName 
     */
    public static void deleteAll(String domainName) {
         throw new UnsupportedOperationException("Implement me please!");
    }

    /**
     * send documents to cloud search
     * <p/>
     * @param batches
     */
    private static void sendData(BatchesObject batches) {
        HttpClient client = new DefaultHttpClient();
        BufferedReader br = null;
        HttpPost method = new HttpPost(CloudSearchUtil.getURL());
        method.addHeader("Content-Type", "application/json");
        HttpResponse res = null;
        StringEntity documentSdf = null;
        HttpEntity entity = null;
        StringBuilder jsonResponse = new StringBuilder();

        try {
            // add the batches document in the request
            documentSdf = new StringEntity(batches.toString(),
                    Charset.forName("UTF-8"));
            method.setEntity(documentSdf);
            res = client.execute(method);

            LOG.info("Info server cloud search: " + res.getStatusLine());

            entity = res.getEntity();
            if (entity == null) {
                LOG.error("Can't access to the resource");
            } else {
                // read the result
                br = new BufferedReader(new InputStreamReader(
                        entity.getContent()));
                String line = "";
                jsonResponse.setLength(0);
                while (((line = br.readLine()) != null)) {
                    jsonResponse.append(line);
                }
                LOG.info("Result: "
                        + CloudSearchUtil.parseResponse(jsonResponse.toString()));
            }
        } catch (Exception e) {
            LOG.error("Error sending the document: ", e);
        } finally {
            //release all resources
            method.releaseConnection();
            if (br != null) {
                try {
                    br.close();
                } catch (Exception fe) {
                    LOG.error("Error closing buffered reader", fe);
                }
            }
        }
    }
}
