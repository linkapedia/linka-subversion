package com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshaller;
import java.util.Properties;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class PropertiesMarshaller implements DynamoDBMarshaller<Properties> {

    private static final Logger log = LoggerFactory.getLogger(PropertiesMarshaller.class);

    @Override
    public String marshall(Properties properties) {
        log.debug("marshall(Properties)");
        /*StringBuilder sb = new StringBuilder("[{");
        for (Object object : properties.keySet()) {
            sb.append((String) object).append(":\"").append(properties.getProperty((String) object)).append("\",");
        }
        sb.append("}]");
        sb.replace(sb.indexOf(",}]"), sb.indexOf(",}]") + 3, "}]");
        return sb.toString();*/
        JSONArray jsonObj = JSONArray.fromObject(properties);
        return jsonObj.toString();
    }

    /*@Override
    public Properties unmarshall(Class<Properties> clazz, String obj) {
        log.debug("unmarshall(Class<Properties>, String)");
        if (obj == null || obj.trim().isEmpty()) {
            return new Properties();
        } else {
            Properties props = new Properties();
            obj = obj.replaceAll("\\[\\{", "");
            obj = obj.replaceAll("\\}\\]", "");
            String[] values = obj.split("\\,");
            String value = null;
            String[] keyValue = null;
            for (int i = 0; i < values.length; i++) {
                value = values[i];
                keyValue = value.split("\\:");
                props.put(keyValue[0], keyValue[1].substring(1, keyValue[1].length() - 1));
            }
            return props;
        }
    }*/
    
        @Override
    public Properties unmarshall(Class<Properties> clazz, String obj) {
        log.debug("unmarshall(Class<Properties>, String)");    
        JSONArray jsonObj = JSONArray.fromObject(obj);
        return (Properties)JSONObject.toBean(jsonObj.getJSONObject(0), Properties.class);
    }
}
