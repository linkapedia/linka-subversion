package com.intellisophic.linkapedia.amazon.services.dynamodb.annotations;

import java.lang.annotation.*;

/**
 * Annotation created to handle the Amazon storage options for a table and make
 * an association with the class.
 * <p/>
 * @author Xander Kno
 * @version 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AmazonDynamoDB {

    /**
     * Dynamo table name
     * <p/>
     * @return table name
     */
    String tableName();

    /**
     * Dynamo table "primary key"
     * <p/>
     * @return table primary key
     */
    String hashKey();

    /**
     * Data type for the hashKey of the table
     * <p/>
     * @return {'S':String, 'N':Number}
     */
    String hashKeyType();

    /**
     * Dynamo table "range key"
     * <p/>
     * @return table range key
     */
    String rangeKey() default "";

    /**
     * Data type for the rangeKey of the table
     * <p/>
     * @return {'S':String, 'N':Number}
     */
    String rangeKeyType() default "";

    /**
     * Read throughput provisioned capacity.
     * <p/>
     * @return read capacity
     */
    long readCapacity() default 10L;

    /**
     * Write throughput provisioned capacity.
     * <p/>
     * @return write capacity
     */
    long writeCapacity() default 10L;
}
