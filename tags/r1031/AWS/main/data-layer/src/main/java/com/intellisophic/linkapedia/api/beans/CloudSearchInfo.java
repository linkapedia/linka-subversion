package com.intellisophic.linkapedia.api.beans;

/**
 * data to comunicate with the api
 * <p/>
 * @author andres
 *
 */
public class CloudSearchInfo {

    //attr protected head sdf
    protected String type;
    protected String id;
    protected long version;
    private String lang;
    //fields
    private String nodeId;
    private String nodeName;
    private String nodeDesc;
    private String nodePath;
    private String corpusId;
    private String corpusName;
    private boolean nodePublic;

    /**
     * populate fields in the constructor
     * <p/>
     * @param lang: example "en"
     * @param nodeId
     * @param nodeName
     * @param nodePath "path complete of the node"
     * @param corpusId
     * @param corpusName
     * @param nodePublic
     */
    public CloudSearchInfo(String lang, String nodeId, String nodeName,
            String nodeDesc, String nodePath, String corpusId, String corpusName,
            boolean nodePublic) {
        super();
        this.lang = lang;
        this.nodeId = nodeId;
        this.nodeName = nodeName;
        this.nodeDesc = nodeDesc;
        this.nodePath = nodePath;
        this.corpusId = corpusId;
        this.corpusName = corpusName;
        this.nodePublic = nodePublic;
    }

    public CloudSearchInfo() {
        this.lang = "EN";
    }

    protected String getType() {
        return type;
    }

    protected void setType(String type) {
        this.type = type;
    }

    protected String getId() {
        return id;
    }

    protected void setId(String id) {
        this.id = id;
    }

    protected long getVersion() {
        return version;
    }

    protected void setVersion(long version) {
        this.version = version;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeDesc() {
        return nodeDesc;
    }

    public void setNodeDesc(String nodeDesc) {
        this.nodeDesc = nodeDesc;
    }

    public String getNodePath() {
        return nodePath;
    }

    public void setNodePath(String nodePath) {
        this.nodePath = nodePath;
    }

    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }

    public String getCorpusName() {
        return corpusName;
    }

    public void setCorpusName(String corpusName) {
        this.corpusName = corpusName;
    }

    public boolean isNodePublic() {
        return nodePublic;
    }

    public void setNodePublic(boolean nodePublic) {
        this.nodePublic = nodePublic;
    }
}
