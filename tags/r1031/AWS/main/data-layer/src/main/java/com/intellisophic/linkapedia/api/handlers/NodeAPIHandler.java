package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMapperConfig;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class NodeAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(NodeAPIHandler.class);
    // private static final String NODE_CACHE_IDENTIFIER = "nodes";
    // private static CacheObject nodeCache = initializeCache();
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(NodeAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;
     

    public static NodeStatic getNode(String nodeID) throws IllegalArgumentException {
        log.debug("getNode(Long)");
        NodeStatic node = null;
        if (nodeID == null) {
            throw new IllegalArgumentException("The node id can not be null");
        }
        node = (NodeStatic) cache.get(nodeID);
        if (node != null) {
            return node;
        } else {
            node = getDynamoDBMapper().load(NodeStatic.class, nodeID);
            if (node != null) {
                node.setModified(false);
                cache.set(nodeID, node, ONE_HOUR);
            } else {
                log.warn("No node found for nodeID {" + nodeID + "}");
            }
            return node;
        }
    }

    public static void saveNode(NodeStatic node) throws IllegalArgumentException {
        log.debug("saveNode(NodeStatic)");
        NodeStatic cacheNode = null;
        if (node == null) {
            throw new IllegalArgumentException("The node to be saved can not be null");
        }
        cacheNode = (NodeStatic) cache.get(node.getId());
        if (cacheNode != null && (node.isModified() || !cacheNode.equals(node))) {
            //The node is a previosly saved object so we need to saved to store the updates it contains.
            getDynamoDBMapper().save(node);
            //We need to change it status so we dont save it twice.
            node.setModified(false);
            cache.set(node.getId(), node, ONE_HOUR);
        } else if (cacheNode == null) {
            // The node is a new node so we need to store it in the db.
            getDynamoDBMapper().save(node);
           /* DynamoDBMapperConfig dynamoDBMapperConfig;
           dynamoDBMapperConfig = new DynamoDBMapperConfig(new DynamoDBMapperConfig.TableNameOverride("fdfgfd"));
            getDynamoDBMapper().save(node, dynamoDBMapperConfig);*/
            //We need to change it status so we dont save it twice.
            node.setModified(false);
            cache.set(node.getId(), node, ONE_HOUR);
        } else {
            log.debug("The node is in a non-modified state so it won't be saved again.");
        }
    }

    public static void deleteNode(NodeStatic node) throws IllegalArgumentException {
        log.debug("deleteNode(NodeStatic)");
        if (node == null) {
            throw new IllegalArgumentException("The node to be deleted can not be null");
        }
        getDynamoDBMapper().delete(node);
        cache.delete(node.getId());
        node = null;
    }
}
