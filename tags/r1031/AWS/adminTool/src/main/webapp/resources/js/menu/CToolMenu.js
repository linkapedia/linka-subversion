/**
 * @author Andres Restrepo
 * menu of the web page
 */

var CToolMenu = kendo.Class.extend({
    load : function() {
        //load windows
        var win80legs = new WIN80Legs('window80Legs', '80-legs');
        win80legs.createWindow();
       
        //register the events
        $("#search80LegsBtn").click(function() {
            var text = $('#searchtxt').val();
            text = $.trim(text);
            var url;
            if (text != "") {
                url = "services/s3/80legs/file-prefix?callback=?";
                url = url + "&prefix=" + text;
                win80legs.loadContent(url);
            }

        });
        $("#uploadFilesBtn").click(function() {
            win80legs.uploadFilesSelected();
        });
                
        $("#uploadAllFilesBtn").click(function() {
            alert("Working in this");
        });
                
        $("#nextPaginationBtn").click(function(){
            //call the next pages
            win80legs.nextPagination();
        });
                
        $("#loadAgainBtn").click(function(){
            win80legs.loadContent("services/s3/80legs?callback=?");
        });

        //event of the menu
        function onSelect(e) {
            var element = $(e.item).children(".k-link").text()
            element = $.trim(element)
            //open window
            //manage the events by each item
            if (element == "80-legs") {
                //load the content
                win80legs.show(win80legs.getWindow());
                win80legs.loadContent("services/s3/80legs?callback=?");
                //clear input
                $('#searchtxt').val("");
                               
            }
        }

        $("#menu").kendoMenu({
            select : onSelect
        });
    }
});

