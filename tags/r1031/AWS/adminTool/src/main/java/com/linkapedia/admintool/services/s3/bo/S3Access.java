package com.linkapedia.admintool.services.s3.bo;

import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.intellisophic.linkapedia.amazon.services.S3.bo.ManageBucket;
import com.linkapedia.admintool.services.s3.bo.beans.Object80Legs;
import com.linkapedia.admintool.services.s3.bo.beans.Transport80Legs;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class S3Access {

    private static final Logger log = Logger.getLogger(S3Access.class);
    private static ManageBucket mbOut;
    private static ManageBucket mbIn;
    private static final String PREFIX_OUT = "staged/";
    private static final String PREFIX_IN = "staged/";

    static {
        mbOut = new ManageBucket("files80legs"); //point output of files
        mbIn = new ManageBucket("80legs");  //entry point of files
    }

    /**
     * save file with the selected files name into s3
     *
     * @param data
     * @return
     */
    public static boolean saveFile(List<String> data) {
        log.debug("S3Access: saveFile(List)");
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentType("text/plain");

        //write file txt with the name files
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(output);
        //String fileName = UUID.randomUUID().toString() + System.nanoTime();
        String fileName = "file";
        for (String d : data) {
            ps.append(d);
            ps.append("\n");
        }
        ps.flush();
        InputStream bai = new ByteArrayInputStream(output.toByteArray());
        boolean result = mbOut.saveData(PREFIX_OUT + fileName, bai, meta);
        ps.close();
        return result;
    }

    /**
     * get Files from a bucket
     *
     * @param prefix
     * @param delimiter
     * @param maxkeys
     * @param marker
     * @return
     */
    public static Transport80Legs getFiles(String prefix, String delimiter, Integer maxkeys, String marker) {
        log.debug("S3Access: getFiles(String, String, Integer, String)");

        ObjectListing listFiles = mbIn.listFiles(PREFIX_IN + prefix, delimiter, maxkeys, marker);

        List<Object80Legs> listInfo80Legs = new ArrayList<Object80Legs>();

        Transport80Legs trasnsport80Legs = new Transport80Legs();

        //set header in my json object
        trasnsport80Legs.setTruncated(listFiles.isTruncated());
        trasnsport80Legs.setMarker(listFiles.getNextMarker());

        Object80Legs info80Legs = null;

        String auxKey = "";
        for (S3ObjectSummary su : listFiles.getObjectSummaries()) {
            //populate objject with the keys
            info80Legs = new Object80Legs();
            auxKey = su.getKey();
            //get the file name
            auxKey = auxKey.substring(auxKey.lastIndexOf("/") + 1);
            info80Legs.setName(auxKey);
            //transform the bytes in megabytes
            info80Legs.setSize(su.getSize() / (1024 * 1024));
            listInfo80Legs.add(info80Legs);
        }
        //FIXME: remove the first element root folder. we need to found a good way to do this in the query
        if (!listInfo80Legs.isEmpty()) {
            if (listInfo80Legs.get(0).getName().equals("")) {
                listInfo80Legs.remove(0);
            }
        }
        trasnsport80Legs.setKeys(listInfo80Legs);
        return trasnsport80Legs;
    }
}
