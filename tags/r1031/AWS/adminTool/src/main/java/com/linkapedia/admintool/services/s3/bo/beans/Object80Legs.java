package com.linkapedia.admintool.services.s3.bo.beans;

/**
 * base information to show in the web page
 * @author andres
 */
public class Object80Legs {

    private String name;
    private Long size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}