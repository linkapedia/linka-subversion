package com.linkapedia.admintool.services.s3.rest;

import com.linkapedia.admintool.services.s3.bo.Facade;
import com.linkapedia.admintool.services.s3.bo.beans.SelectedFiles;
import com.linkapedia.admintool.services.s3.bo.beans.Transport80Legs;
import com.sun.jersey.api.json.JSONWithPadding;
import java.security.InvalidParameterException;
import javax.ws.rs.*;
import org.apache.log4j.Logger;

/**
 * entry point
 *
 * @author andres
 */
@Path("/s3")
public class Router {

    private static final Logger log = Logger.getLogger(Router.class);
    private static final String CALLBACK_FUNCTION = "callback";

    /**
     * search 80 legs file by filename
     *
     * @param callback
     * @param fileName
     * @return
     */
    @GET
    @Path("/80legs/file-prefix")
    @Produces("application/x-javascript") //use x-javascript by the jsonp
    public JSONWithPadding getFiles(@QueryParam(CALLBACK_FUNCTION) @DefaultValue("jsoncallback") String callback, @QueryParam("prefix") String prefix) {
        log.debug("Router: getFiles(prefix)");
        Transport80Legs files = Facade.getFiles(prefix);
        return new JSONWithPadding(files, callback);
    }

    /**
     * get all 80 legs files
     *
     * @param callback
     * @return
     */
    @GET
    @Path("/80legs")
    @Produces("application/x-javascript") //use x-javascript by the jsonp
    public JSONWithPadding getFiles(@QueryParam(CALLBACK_FUNCTION) @DefaultValue("jsoncallback") String callback) {
        log.debug("Router: getFiles()");
        Transport80Legs files = Facade.getFiles();
        return new JSONWithPadding(files, callback);
    }

    /**
     * Next page from the files
     *
     * @param callback
     * @param marker
     * @return
     */
    @GET
    @Path("/80legs/nextPage")
    @Produces("application/x-javascript") //use x-javascript by the jsonp
    public JSONWithPadding getNextPageFiles(@QueryParam(CALLBACK_FUNCTION) @DefaultValue("jsoncallback") String callback, @QueryParam("marker") String marker) {
        log.debug("Router: getNextPageFiles(marker)");
        Transport80Legs files = Facade.getNextPage(marker);
        return new JSONWithPadding(files, callback);
    }

    /**
     * process selected files
     *
     * @param data
     * @return
     */
    @POST
    @Path("/process")
    @Consumes("application/json")
    @Produces("application/json")
    public void process(SelectedFiles data) {
        log.debug("Router: process(SelectedFiles)");
        try {
            Facade.process(data);
        } catch (InvalidParameterException ex) {
            log.error("Error in the process", ex);
        }

    }
}
