/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.format;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public class DocumentNodeToDocNodeMapper implements iDynamoMapper {

    private iMapToDocNode mapper;

    public DocumentNodeToDocNodeMapper(iMapToDocNode mapper) {

        this.mapper = mapper;
    }

    public List<Doc_Node> getDocNodes(List<DocumentNode> documentNodes) {
        List<Doc_Node> docNodes = new ArrayList<Doc_Node>();
        boolean sortOrder = true;
        String previousId;
        Doc_Node doc;
        Map<String, Float> unsortMap = new HashMap<String, Float>();
        try {
            if (!documentNodes.isEmpty()) {
                previousId = documentNodes.get(0).getDocID();
                for (int counter = 0; counter < documentNodes.size(); counter++) {
                    System.out.println("****Mapping document node:" + documentNodes.get(counter).getDocID() + "****");
                    if (previousId.equalsIgnoreCase(documentNodes.get(counter).getDocID())) {
                        unsortMap.put(documentNodes.get(counter).getNodeID(), documentNodes.get(counter).getScore11());
                    } else {
                        if (!unsortMap.isEmpty()) {
                            doc = mapper.formatDocNode(unsortMap, previousId, sortOrder, documentNodes.get(counter - 1));
                            docNodes.add(doc);
                        }
                        previousId = documentNodes.get(counter).getDocID();
                        unsortMap.clear();
                        unsortMap.put(documentNodes.get(counter).getNodeID(), documentNodes.get(counter).getScore11());
                    }
                    if (documentNodes.size() - 1 == counter) {
                        doc = mapper.formatDocNode(unsortMap, previousId, sortOrder, documentNodes.get(counter));
                        docNodes.add(doc);
                    }
                    System.out.println("+++++++++++++Items processed: " + counter + " of:" + documentNodes.size() + "+++++++++++++++++");
                }
            }

        } catch (Exception ex) {
            System.out.println("Exception mapping: " + ex.getMessage());
        }

        return docNodes;

    }

    public List<DocumentNode> getDocumentNodes(List<NodeDocument> nodeDocuments) {
        Date dateTime = new Date();
        List<DocumentNode> documentNodes = new ArrayList<DocumentNode>();
        for (NodeDocument nodeDocument : nodeDocuments) {
            DocumentNode documentNode = new DocumentNode();
            documentNode.setDocID(nodeDocument.getDocID());
            documentNode.setNodeID(nodeDocument.getNodeID());
            documentNode.setPagerank(nodeDocument.getPagerank());
            documentNode.setScore01(nodeDocument.getScore01());
            documentNode.setScore02(nodeDocument.getScore02());
            documentNode.setScore03(nodeDocument.getScore03());
            documentNode.setScore04(nodeDocument.getScore04());
            documentNode.setScore05(nodeDocument.getScore05());
            documentNode.setScore06(nodeDocument.getScore06());
            documentNode.setScore07(nodeDocument.getScore07());
            documentNode.setScore08(nodeDocument.getScore08());
            documentNode.setScore09(nodeDocument.getScore09());
            documentNode.setScore10(nodeDocument.getScore10());
            documentNode.setScore11(nodeDocument.getScore11());
            documentNode.setScore12(nodeDocument.getScore12());
            if (nodeDocument.getTimeStamp() == 0) {
                documentNode.setTimeStamp(dateTime.getTime());
            } else {

                documentNode.setTimeStamp(nodeDocument.getTimeStamp());
            }
            documentNode.setTitle(nodeDocument.getDocTitle());
            documentNode.setURL(nodeDocument.getDocURL());

            documentNodes.add(documentNode);

        }

        return documentNodes;
    }

    public DocumentNode getDocumentNode(NodeDocument nodeDocument, long timeStamp) {

        DocumentNode documentNode = new DocumentNode();
        documentNode.setDocID(nodeDocument.getDocID());
        documentNode.setNodeID(nodeDocument.getNodeID());
        documentNode.setPagerank(nodeDocument.getPagerank());
        documentNode.setScore01(nodeDocument.getScore01());
        documentNode.setScore02(nodeDocument.getScore02());
        documentNode.setScore03(nodeDocument.getScore03());
        documentNode.setScore04(nodeDocument.getScore04());
        documentNode.setScore05(nodeDocument.getScore05());
        documentNode.setScore06(nodeDocument.getScore06());
        documentNode.setScore07(nodeDocument.getScore07());
        documentNode.setScore08(nodeDocument.getScore08());
        documentNode.setScore09(nodeDocument.getScore09());
        documentNode.setScore10(nodeDocument.getScore10());
        documentNode.setScore11(nodeDocument.getScore11());
        documentNode.setScore12(nodeDocument.getScore12());
        if (nodeDocument.getTimeStamp() == 0) {
            documentNode.setTimeStamp(timeStamp);
        } else {

            documentNode.setTimeStamp(nodeDocument.getTimeStamp());
        }
        documentNode.setTitle(nodeDocument.getDocTitle());
        documentNode.setURL(nodeDocument.getDocURL());
        return documentNode;

    }
}
