/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.exec;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.linkapedia.dynamo.mapper.data.DataUtility;
import com.linkapedia.dynamo.mapper.data.IDataOperations;
import com.linkapedia.dynamo.mapper.format.DocumentNodeToDocNodeMapper;
import com.linkapedia.dynamo.mapper.format.iDynamoMapper;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author andres
 */
public class RunMapperToDocNode {

    public static void main(String args[]) {
        System.out.println("****************Getting the context****************");
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        iDynamoMapper mapper = (DocumentNodeToDocNodeMapper) context.getBean("docNodeMaper");
        IDataOperations dataUtil = (DataUtility) context.getBean("dataUtility");
        try {
            System.out.println("****************Getting the document nodes****************");
            List<DocumentNode> documentNodes = dataUtil.getDataFromDocumentNode();
            System.out.println("+++++++++++++Number of Document nodes + " + documentNodes.size());
            System.out.println("****************Mapping the tables****************");
            List<Doc_Node> docsToStore = mapper.getDocNodes(documentNodes);
            System.out.println("****************Saving the data****************");
            dataUtil.saveDataDoc_Node(docsToStore);
            System.out.println("****************Done****************");

        } catch (Exception ex) {
            System.out.println("Exception:" + ex);
        }
    }
}
