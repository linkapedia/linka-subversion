/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.utils;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 *
 * @author andres
 */
public interface iTextOperations {

    ByteBuffer compressString(String input) throws IOException;

    String decompressString(ByteBuffer input) throws IOException;
}
