/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.data;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import java.util.List;

/**
 *
 * @author andres
 */
public interface IDataOperations {

    List<DocumentNode> getDataFromDocumentNode() throws Exception;

    void saveDataDoc_Node(List<Doc_Node> nodesToInsert);

    List<NodeDocument> getDataFromNodeDocument() throws Exception;

    void saveDataDocumentNodes(List<DocumentNode> nodesToInsert);
    void saveDataDocumentNode(DocumentNode nodeToInsert);
}
