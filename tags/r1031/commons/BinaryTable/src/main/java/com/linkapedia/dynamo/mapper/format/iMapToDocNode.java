/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.format;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import java.util.Map;

/**
 *
 * @author andres
 */
public interface iMapToDocNode {
    
    Doc_Node formatDocNode(Map<String, Float> unsortMap,String previousId,boolean sortDirection, DocumentNode node);
}
