function init(){
				
				$("#datepicker").datepicker({
					onSelect:updateDate,
					showOn: 'button', 
					buttonImage: './images/date.gif', 
					buttonImageOnly: true});
				//$("datepicker").setDateFormat('ymd','-');
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);
				//Add to meals dialog
				$("#dialog").dialog({
                    autoOpen: false,
					modal: true,
					width:'300px'
                });
				$("#recipeCardBody").dialog({
					autoOpen:false,
					modal:true,
					width:"474px"
				});
				
                //$("a.add").click(addToMeals());
				$("#addToMeals").click(submitRecipe);
				
				//Confirm overwriting meals dialog
				$("#dialogOverwriting").dialog({
                    autoOpen: false,
					modal: true,
					width:'300'
                });				
				$("#overwritingYes").click(overwritingYes);
				$("#overwritingNo").click(overwritingNo);
				$("#overwritingSnack").click(overwritingSnack);
				
				//setWait();
				//set foodAllergies's style
				$("#foodAllergies").dialog({
					autoOpen:false,
					modal:true,
					width:'550px'
				});
				//set the SaveAllergy's event
				$("#SaveAllergy").click(saveAllergy);
				//set the foodAllergies's event
				$("a.allergies").click(foodAllergies);
				// Set food journal initiation data.
				getFoodJournal(new Date());
				
				// Search the food journal data on the day.
				
				//getUserDayData(gNowDate);
				
				//Review week journal
				$("#review_week").dialog({
					autoOpen: false,
					modal: true,
					width:744
				});		
				
}

/**
 * Get Sunday in a week.
 * @param {Date} datDay either date in a week 
 * @return (Date) Sunday
 */
function getSundayInWeek(datDay) {
	var iDay = datDay.getDay();
	var sunday = new Date(datDay.getTime() - iDay * 24 * 60 * 60 * 1000);
	
	return sunday;
}

/**
 * Get SaturDay in a week.
 * @param {Date} datDay either date in a week
 * @return (Date) SaturDay
 */
function getSaturdayInWeek(datDay) {
	var iDay = datDay.getDay();
	var saturday = new Date(datDay.getTime() + (6-iDay) * 24 * 60 * 60 * 1000);
	return saturday;
}

/**
 * Add the days to Date.
 * @param (Date) Date
 * @param (int) days
 */
function addDays(dDay,iDays) {
	return new Date(dDay.getTime() + iDays * 24 * 60 * 60 * 1000);
}

function convertStringDateToDate(sDate) {
	return new Date(Date.parse(sDate));
}

function getDateAsString(aDate){
return	(aDate.getMonth() + 1) + "/" + aDate.getDate() + "/" + aDate.getFullYear();
}

/**
 * Format date function
 * @param {Object} format
 *   var myDate = new Date();
 *   alert(myDate.format('M jS, Y')); 
 */
Date.prototype.format = function(format) {
	var returnStr = '';
	var replace = Date.replaceChars;
	for (var i = 0; i < format.length; i++) {
		var curChar = format.charAt(i);
		if (replace[curChar]) {
			returnStr += replace[curChar].call(this);
		} else {
			returnStr += curChar;
		}
	}
	return returnStr;
};
Date.replaceChars = {
	shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
	longMonths: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	longDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	
	// Day
	d: function() { return (this.getDate() < 10 ? '0' : '') + this.getDate(); },
	D: function() { return Date.replaceChars.shortDays[this.getDay()]; },
	j: function() { return this.getDate(); },
	l: function() { return Date.replaceChars.longDays[this.getDay()]; },
	N: function() { return this.getDay() + 1; },
	S: function() { return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th'))); },
	w: function() { return this.getDay(); },
	z: function() { return "Not Yet Supported"; },
	// Week
	W: function() { return "Not Yet Supported"; },
	// Month
	F: function() { return Date.replaceChars.longMonths[this.getMonth()]; },
	m: function() { return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1); },
	M: function() { return Date.replaceChars.shortMonths[this.getMonth()]; },
	n: function() { return this.getMonth() + 1; },
	t: function() { return "Not Yet Supported"; },
	// Year
	L: function() { return (((this.getFullYear()%4==0)&&(this.getFullYear()%100 != 0)) || (this.getFullYear()%400==0)) ? '1' : '0'; },
	o: function() { return "Not Supported"; },
	Y: function() { return this.getFullYear(); },
	y: function() { return ('' + this.getFullYear()).substr(2); },
	// Time
	a: function() { return this.getHours() < 12 ? 'am' : 'pm'; },
	A: function() { return this.getHours() < 12 ? 'AM' : 'PM'; },
	B: function() { return "Not Yet Supported"; },
	g: function() { return this.getHours() % 12 || 12; },
	G: function() { return this.getHours(); },
	h: function() { return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12); },
	H: function() { return (this.getHours() < 10 ? '0' : '') + this.getHours(); },
	i: function() { return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes(); },
	s: function() { return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds(); },
	// Timezone
	e: function() { return "Not Yet Supported"; },
	I: function() { return "Not Supported"; },
	O: function() { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00'; },
	P: function() { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':' + (Math.abs(this.getTimezoneOffset() % 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() % 60)); },
	T: function() { var m = this.getMonth(); this.setMonth(0); var result = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1'); this.setMonth(m); return result;},
	Z: function() { return -this.getTimezoneOffset() * 60; },
	// Full Date/Time
	c: function() { return this.format("Y-m-d") + "T" + this.format("H:i:sP"); },
	r: function() { return this.toString(); },
	U: function() { return this.getTime() / 1000; }
};

/**
 * View page control.
 */
function viewControlPage(recordCount, pageNumber, currentPage){

    var startPage = 1;
    var endPage = 1;
    var retHTML = "";
    var pageCount = 0;
    var viewPageControl = document.getElementById("pageControl");
    if (recordCount % pageNumber == 0) {
        pageCount = parseInt(recordCount / pageNumber);
    }
    else {
        pageCount = parseInt(recordCount / pageNumber) + 1;
    }
    
    if (currentPage > 3) {
        startPage = currentPage - 3;
    }
    else {
        startPage = 1;
    }
    
    if ((pageCount - currentPage) > 3) {
        endPage = currentPage + 3;
    }
    else {
        endPage = pageCount;
    }
    
    retHTML = "<ul>";
    
    if (currentPage != 1 && pageCount != 0) {
        retHTML = retHTML + "<li  class='next nextnow'><a onclick='getControlPageHTML(";
        retHTML = retHTML + recordCount + "," + pageNumber + "," + (currentPage - 1) + ")' class='next_pre'>&lt;&lt;Previous</a></li>";
    }
    else {
        retHTML = retHTML + "<li class='next nextnow' style='background-color:#C0C0C0;border:1px solid #ccc;color:666'>&lt;&lt;Previous</li>";
    }
    
    for (i = startPage; i <= endPage; i++) {
        if (i == currentPage) {
            retHTML = retHTML + "<li style='background-color:#FF8855;' class='now'><a onclick='getControlPageHTML(";
            retHTML = retHTML + recordCount + "," + pageNumber + "," + i + ");'>";
            retHTML = retHTML + "<font color='white'>" + i + "</font></a></li>";
        }
        else {
            retHTML = retHTML + "<li><a onclick='getControlPageHTML(";
            retHTML = retHTML + recordCount + "," + pageNumber + "," + i + ");'>" + i + "</a></li>";
        }
    }
    if (currentPage != pageCount && pageCount != 0) {
        retHTML = retHTML + "<li class='next' ><a onclick='getControlPageHTML(";
        retHTML = retHTML + recordCount + "," + pageNumber + "," + (currentPage + 1) + ")' class='next_pre'>Next&gt;&gt;</a></li>";
    }
    else {
        retHTML = retHTML + "<li class='next' style='background-color:#C0C0C0;border:1px solid #ccc;color:666'>Next&gt;&gt;</li>";
    }
    retHTML = retHTML + "</ul>";
    
    viewPageControl.innerHTML = retHTML;
    
}

/**
 * Reset page control, if then link button press from page control.
 */
function getControlPageHTML(recordCount, pageNumber, currentPage){

    document.getElementById("currentPage").value = currentPage;
	if ($("title").text()==("My Favorites" ) || $("title").html()==("My Favorites" )  ) {
		getFavoriteRecipes();
		return;
	}
    getRecipes();
}

function isDate(dateStr) {

var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
var matchArray = dateStr.match(datePat); // is the format ok?

if (matchArray == null) {
alert("Please enter date as either mm/dd/yyyy or mm-dd-yyyy.");
return false;
}

month = matchArray[1]; // p@rse date into variables
day = matchArray[3];
year = matchArray[5];

if (month < 1 || month > 12) { // check month range
alert("Month must be between 1 and 12.");
return false;
}

if (day < 1 || day > 31) {
alert("Day must be between 1 and 31.");
return false;
}

if ((month==4 || month==6 || month==9 || month==11) && day==31) {
alert("Month "+month+" doesn`t have 31 days!")
return false;
}

if (month == 2) { // check for february 29th
var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
if (day > 29 || (day==29 && !isleap)) {
alert("February " + year + " doesn`t have " + day + " days!");
return false;
}
}
return true; // date is valid
}