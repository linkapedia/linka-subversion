package com.linkipedia.dao;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import com.linkipedia.constant.Constants;
import com.linkipedia.crawl.ClientBase;
import com.linkipedia.crawl.ClientBaseImpl;
import com.linkipedia.crawl.ContentParse2GoogleCrawlResult;
import com.linkipedia.crawl.Runner;
import com.linkipedia.crawl.UrlCrawlResult;
import com.linkipedia.entity.Concepts;
import com.linkipedia.entity.DomainBean;
import com.linkipedia.entity.GoogleCrawlResult;
import com.linkipedia.entity.MustHave;
import com.linkipedia.entity.Signatures;
import com.linkipedia.entity.Source;
import com.linkipedia.util.PropertiestUtils;

/**
 * @author Administrator
 * @version 1.0
 */
public class SearchDocumentDao extends Dao {

	private static Logger logger = Logger.getLogger(SearchDocumentDao.class);
	private Map<String, Integer> domainMap = null;

	/**
	 * Execute SQL and get the Models,put the models to List
	 * 
	 * @param String
	 *            the SQL
	 * @return List <Model> the collections contains Models
	 */
	public boolean executeUpdateSQL(String sql) {
		Statement stmt = null;
		ResultSet rs = null;
		Connection connection = null;
		int ret;
		try {
			connection = getConn();
			stmt = connection.createStatement();
			stmt.executeUpdate(sql);
			// ret = stmt.getUpdateCount();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(rs, stmt, connection);
		}
		return true;
	}

	/**
	 * @deprecated
	 * @param sql
	 * @return
	 */
	public Object getDomainIdFromSql(String sql) {
		Statement stmt = null;
		ResultSet rs = null;
		Connection connection = null;
		Object ret = 0;
		try {
			connection = getConn();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs != null) {
				while (rs.next()) {
					ret = rs.getObject(1);
					break;
				}
			}
			// ret = stmt.getUpdateCount();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rs, stmt, connection);
		}
		return ret;
	}

	 public List<Concepts> getAllConcepts() {
	 List<Concepts> list = null;
	 Connection connection = null;
	 Statement st = null;
	 ResultSet rs = null;
	 try {
	 connection = getConn();
	 st = connection.createStatement();
	 rs =
	 st.executeQuery("select CORPUSID,NODEID,NODETITLE,NODESIZE,PARENTID,NODEINDEXWITHINPARENT,DEPTHFROMROOT,CRAWLSTATUS from t_metadata where "+Constants.CORPUSID);
	 list = new ArrayList<Concepts>();
	 while (rs.next()) {
	 Concepts c = new Concepts();
	 c.setCorpusId(rs.getInt(1));
	 c.setNodeId(rs.getInt(2));
	 c.setNodeTitle(rs.getString(3));
	 c.setNodeSize(rs.getInt(4));
	 c.setParenentId(rs.getInt(5));
	 c.setNodeIndexWithInParent(rs.getInt(6));
	 c.setDepthFromRoot(rs.getInt(7));
	 c.setCrawlStatus(rs.getInt(8));
	 list.add(c);
	 }
	 } catch (Exception e) {
	 e.printStackTrace();
	 } finally {
	 close(rs, st, connection);
	 }
	 return list;
	 }

	public List<Concepts> getConcepts() {
		List<Concepts> list = new ArrayList<Concepts>();
		Connection conn = this.getConn();
		try {
			Statement top = conn
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
			ResultSet topSet = top
					.executeQuery("select CORPUSID,NODEID,NODETITLE,NODESIZE,PARENTID,NODEINDEXWITHINPARENT,DEPTHFROMROOT,CRAWLSTATUS from t_metadata where depthfromroot = 0 and "+Constants.CORPUSID+" order by NODEINDEXWITHINPARENT");
			while (topSet.next()) {
				Concepts c = new Concepts();
				c.setCorpusId(topSet.getInt(1));
				int nodeid = topSet.getInt(2);
				c.setNodeId(nodeid);
				c.setNodeTitle(topSet.getString(3));
				c.setNodeSize(topSet.getInt(4));
				c.setParenentId(topSet.getInt(5));
				c.setNodeIndexWithInParent(topSet.getInt(6));
				c.setDepthFromRoot(topSet.getInt(7));
				c.setCrawlStatus(topSet.getInt(8));
				list.add(c);
				if (logger.isInfoEnabled())
					logger
							.info("Get the first Concetps from t_metadata,the node id is:"
									+ nodeid);
				Statement biglinkst = conn.createStatement(
						ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
				ResultSet biglinks = biglinkst
						.executeQuery("select CORPUSID,NODEID,NODETITLE,NODESIZE,PARENTID,NODEINDEXWITHINPARENT,DEPTHFROMROOT,CRAWLSTATUS from t_metadata where depthfromroot = 1 and PARENTID="
								+ nodeid + " and " + Constants.CORPUSID
								+ " order by NODEINDEXWITHINPARENT");
				//+ " order by NODEINDEXWITHINPARENT limit 5");
				while (biglinks.next()) {
					Concepts c1 = new Concepts();
					c.setCorpusId(biglinks.getInt(1));
					int nodeid1 = biglinks.getInt(2);
					c1.setNodeId(nodeid1);
					c1.setNodeTitle(biglinks.getString(3));
					c1.setNodeSize(biglinks.getInt(4));
					c1.setParenentId(biglinks.getInt(5));
					c1.setNodeIndexWithInParent(biglinks.getInt(6));
					c1.setDepthFromRoot(biglinks.getInt(7));
					c1.setCrawlStatus(biglinks.getInt(8));
					list.add(c1);
					if (logger.isInfoEnabled())
						logger
								.info("Get the big Concetps from t_metadata,the parentid is:"
										+ nodeid);
					Statement smalllinkst = conn.createStatement(
							ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
//					ResultSet smalllinks = smalllinkst
//							.executeQuery("select CORPUSID,NODEID,NODETITLE,NODESIZE,PARENTID,NODEINDEXWITHINPARENT,DEPTHFROMROOT from t_metadata where depthfromroot = 2 and PARENTID="
//									+ nodeid1
//									+ " order by NODEINDEXWITHINPARENT");
					ResultSet smalllinks = smalllinkst
					.executeQuery("select t.CORPUSID,t.NODEID,t.NODETITLE,t.NODESIZE,t.PARENTID,t.NODEINDEXWITHINPARENT,t.DEPTHFROMROOT,t.CRAWLSTATUS from t_metadata t where t.depthfromroot = 2 and t.PARENTID="
							+nodeid1 + " and t." + Constants.CORPUSID
							+" and exists(select * from (select min(t2.nodeid)as nodeid from t_metadata t2 where t2."+Constants.CORPUSID+" group by t2.nodetitle)temp where t.nodeid=temp.nodeid) order by t.NODEINDEXWITHINPARENT");
			
					while (smalllinks.next()) {
						Concepts c2 = new Concepts();
						c.setCorpusId(smalllinks.getInt(1));
						int nodeid2 = smalllinks.getInt(2);
						c2.setNodeId(nodeid2);
						c2.setNodeTitle(smalllinks.getString(3));
						c2.setNodeSize(smalllinks.getInt(4));
						c2.setParenentId(smalllinks.getInt(5));
						c2.setNodeIndexWithInParent(smalllinks.getInt(6));
						c2.setDepthFromRoot(smalllinks.getInt(7));
						c2.setCrawlStatus(smalllinks.getInt(8));
						list.add(c2);
						if (logger.isInfoEnabled())
							logger.info("Get the small Concetps from t_metadata,the parentid is:"
											+ nodeid1);
						Statement thirdlinkst = conn.createStatement(
								ResultSet.TYPE_SCROLL_SENSITIVE,
								ResultSet.CONCUR_READ_ONLY);
						ResultSet thirdlinksts = thirdlinkst
						.executeQuery("select t.CORPUSID,t.NODEID,t.NODETITLE,t.NODESIZE,t.PARENTID,t.NODEINDEXWITHINPARENT,t.DEPTHFROMROOT,t.CRAWLSTATUS from t_metadata t where t.depthfromroot = 3 and t.PARENTID="
								+nodeid2 + " and t." + Constants.CORPUSID
								+" and exists(select * from (select min(t2.nodeid)as nodeid from t_metadata t2  where t2."+Constants.CORPUSID+" group by t2.nodetitle)temp where t.nodeid=temp.nodeid) order by t.NODEINDEXWITHINPARENT");
				
						while (thirdlinksts.next()) {
							Concepts c3 = new Concepts();
							c.setCorpusId(smalllinks.getInt(1));
							int nodeid3 = smalllinks.getInt(2);
							c3.setNodeId(nodeid3);
							c3.setNodeTitle(smalllinks.getString(3));
							c3.setNodeSize(smalllinks.getInt(4));
							c3.setParenentId(smalllinks.getInt(5));
							c3.setNodeIndexWithInParent(smalllinks.getInt(6));
							c3.setDepthFromRoot(smalllinks.getInt(7));
							c3.setCrawlStatus(smalllinks.getInt(8));
							list.add(c3);
							if (logger.isInfoEnabled())
								logger.info("Get the small Concetps from t_metadata,the parentid is:"
												+ nodeid2);
	
					}
				}
			}
			}
			} catch (SQLException e) {
			e.printStackTrace();
			logger.error("GetConcepts occur error:" + e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	
	
	public List<Concepts> getConceptsTemp() {
		List<Concepts> list = new ArrayList<Concepts>();
		Connection conn = this.getConn();
		try {
			Statement top = conn
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
			ResultSet topSet = top
					.executeQuery("select CORPUSID,NODEID,NODETITLE,NODESIZE,PARENTID,NODEINDEXWITHINPARENT,DEPTHFROMROOT from t_metadata m where depthfromroot = 3 and "
								+Constants.CORPUSID+" and exists (select d.nodeid from t_document d where d.nodeid=m.parentid) and not exists(select d.nodeid from t_document d where d.nodeid=m.nodeid) order by NODEID");
			while (topSet.next()) {
				Concepts c = new Concepts();
				c.setCorpusId(topSet.getInt(1));
				int nodeid = topSet.getInt(2);
				System.out.println(nodeid+"  ===nodeid");
				c.setNodeId(nodeid);
				c.setNodeTitle(topSet.getString(3));
				c.setNodeSize(topSet.getInt(4));
				c.setParenentId(topSet.getInt(5));
				c.setNodeIndexWithInParent(topSet.getInt(6));
				c.setDepthFromRoot(topSet.getInt(7));
				list.add(c);
				if (logger.isInfoEnabled())
					logger
							.info("Get the first Concetps from t_metadata,the node id is:"
									+ nodeid);
			}
			} catch (SQLException e) {
			e.printStackTrace();
			logger.error("GetConcepts occur error:" + e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public Concepts getConceptById(int nodeId) {
		Concepts c = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st
					.executeQuery("select CORPUSID,NODEID,NODETITLE,NODESIZE,PARENTID,NODEINDEXWITHINPARENT,DEPTHFROMROOT from t_metadata where NODEID="
							+ nodeId + " and " + Constants.CORPUSID);
			while (rs.next()) {
				c = new Concepts();
				c.setCorpusId(rs.getInt(1));
				c.setNodeId(rs.getInt(2));
				c.setNodeTitle(rs.getString(3));
				c.setNodeSize(rs.getInt(4));
				c.setParenentId(rs.getInt(5));
				c.setNodeIndexWithInParent(rs.getInt(6));
				c.setDepthFromRoot(rs.getInt(7));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return c;
	}

	public List<MustHave> getMustHavesById(int nodeId) {
		List<MustHave> list = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st
					.executeQuery("select CORPUSID,NODETITLE,MUSTWORD from t_musthave where NODEID="
							+ nodeId +" and " + Constants.CORPUSID);
			list = new ArrayList<MustHave>();
			while (rs.next()) {
				MustHave m = new MustHave();
				m.setCorpusId(rs.getInt(1));
				m.setNodeId(nodeId);
				m.setNodeTitle(rs.getString(2));
				m.setMustWord(rs.getString(3));
				list.add(m);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}

	public List<Signatures> getSignaturesById(int nodeId) {
		List<Signatures> list = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st
					.executeQuery("select CORPUSID,NODETITLE,SIGNATUREWORD,SIGNATUREOCCURENCES from t_signatures where NODEID="
							+ nodeId + " and " + Constants.CORPUSID);
			list = new ArrayList<Signatures>();
			while (rs.next()) {
				Signatures s = new Signatures();
				s.setCorpusId(rs.getInt(1));
				s.setNodeId(nodeId);
				s.setNodeTitle(rs.getString(2));
				s.setSingatureWord(rs.getString(3));
				s.setSignatureOccurences(rs.getInt(4));
				list.add(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}
	
	/**
	 * Ticket #21 Change META DESCRIPTION
	 */
	public List<Source> getSourceById(int nodeId) {
		List<Source> list = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st
					.executeQuery("select PARENTID,NODETITLE,NODESOURCE,CORPUSID,CORPUS_NAME from t_source where NODEID="
							+ nodeId + " and " + Constants.CORPUSID);
			list = new ArrayList<Source>();
			while (rs.next()) {
				Source s = new Source();
				s.setNodeId(nodeId);
				s.setParentId(rs.getInt("PARENTID"));
				s.setNodeSource(rs.getString("NODESOURCE"));
				s.setNodeTitle(rs.getString("NODETITLE"));
				s.setCorpusId(rs.getInt("CORPUSID"));
				s.setCorpusName(rs.getString("CORPUS_NAME"));
				list.add(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}
	
	public List<GoogleCrawlResult> getGoogleCrawlResultsById(int nodeId) {
		List<GoogleCrawlResult> list = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st.executeQuery("select doc.URL,doc.KEYWORD,doc.TITLE,doc.SNIPPET,doc.SCORE,doc.DOMAIN_ID,domain.DOMAIN_NAME from t_document doc inner join t_domain domain on doc.DOMAIN_ID=domain.DOMAIN_ID where doc.NODEID="
							+ nodeId + " order by doc.DOMAIN_ID");
			list = new ArrayList<GoogleCrawlResult>();
			while (rs.next()) {
				//Ticket #20 Ensure results contain the word "coffee"
				String title = rs.getString(3);
				String snippet = rs.getString(4);
				if(containCoffee(title)||containCoffee(snippet)){
					GoogleCrawlResult g = new GoogleCrawlResult();
					g.setNodeid(nodeId);
					g.setUrl(rs.getString(1));
					g.setKeyWord(rs.getString(2));
					g.setTitle(title);
					g.setSnippet(snippet);
					g.setScore(rs.getInt(5));
					g.setDomainId(rs.getInt("DOMAIN_ID"));
					g.setDomain(rs.getString("DOMAIN_NAME"));
					list.add(g);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}
	
	/**
	 * Ticket #20 Ensure results contain the word "coffee"
	 */
	private boolean containCoffee(String source){
		if(source != null){
			String lowwerSource = source.toLowerCase();
			if(lowwerSource.contains(Constants.WORD_COFFEA)||lowwerSource.contains(Constants.WORD_COFFEE)){
				return true;
			}
		}
		return false;
	}
	
	public List<GoogleCrawlResult> getNoscoreGoogleCrawlResults() {
		List<GoogleCrawlResult> list = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			//TODO
			rs = st.executeQuery("select doc.NODEID,doc.URL,doc.KEYWORD from t_document doc where doc.DOMAIN_ID !=5 and doc.FLAG=0");
			//rs = st.executeQuery("select doc.NODEID,doc.URL,doc.KEYWORD from t_document doc where doc.DOMAIN_ID !=5 and doc.SCORE=0 AND doc.NODEID in (select sign.NODEID from t_signatures sign)");
			list = new ArrayList<GoogleCrawlResult>();
			while (rs.next()) {
				GoogleCrawlResult g = new GoogleCrawlResult();
				g.setNodeid(rs.getInt(1));
				g.setUrl(rs.getString(2));
				g.setKeyWord(rs.getString(3));
//				g.setTitle(rs.getString(3));
//				g.setSnippet(rs.getString(4));
//				g.setScore(rs.getInt(5));
//				g.setDomainId(rs.getInt("DOMAIN_ID"));
//				g.setDomain(rs.getString("DOMAIN_NAME"));
				list.add(g);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}
	
	

	/**
	 * @deprecated
	 * @param nodeId
	 * @return list of search result lists grouped by domain
	 */
	public List<List<GoogleCrawlResult>> getResultGroupByNodeId(int nodeId) {
		List<GoogleCrawlResult> result = getGoogleCrawlResultsById(nodeId);
		List<List<GoogleCrawlResult>> groups = new ArrayList<List<GoogleCrawlResult>>();
		if (result.size() == 0)
			return groups;

		List<GoogleCrawlResult> group = new ArrayList<GoogleCrawlResult>();
		String currentDomain = result.get(0).getDomain();
		for (GoogleCrawlResult doc : result) {
			group.add(doc);
			if (!doc.getDomain().equals(currentDomain)) {
				groups.add(group);
				group = new ArrayList<GoogleCrawlResult>();
				currentDomain = doc.getDomain();
			}
		}

		return groups;

	}

	/**
	 * 
	 * @param nodeId
	 * @return Map of search result lists grouped by domain
	 */
	public Map<String, List<GoogleCrawlResult>> getResultGroups(int nodeId) {
		List<GoogleCrawlResult> result = getGoogleCrawlResultsById(nodeId);
		/**
		 * Ticket #21 Change META DESCRIPTION
		 */
		Map<String, List<GoogleCrawlResult>> groups = new LinkedHashMap<String, List<GoogleCrawlResult>>();
		if (result.size() == 0)
			return groups;
		for (GoogleCrawlResult doc : result) {
			String domain = String.valueOf(doc.getDomainId());
			if (!groups.containsKey(domain))
				groups.put(domain, new ArrayList<GoogleCrawlResult>());
			groups.get(domain).add(doc);

		}
		return groups;
	}

	public Map<String, Integer> getInverseDomains() {
		List<DomainBean> domains = getDomains();
		Map<String, Integer> domianMap = new HashMap<String, Integer>();
		for (DomainBean d : domains) {
			domianMap.put(d.getDomain_name(), d.getDomain_id());
		}
		return domianMap;
	}

	public Integer getDomainId(String domainName) {
		if (domainMap == null)
			domainMap = this.getInverseDomains();
		return domainMap.get(domainName);
	}

	public boolean batchInsertResult(List<GoogleCrawlResult> list) {
		boolean success = false;
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		java.util.Date now = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2010-09-06
		// 09:22:58
		String sdate = sdf.format(now).toString();

		if (list != null && list.size() > 0) {
			try {
				connection = getConn();
				connection.setAutoCommit(false);
				ps = connection
						.prepareStatement("INSERT INTO `t_document`(NODEID,URL,KEYWORD,TITLE,SNIPPET,LOCATION,SCORE,CREATED_DATE,MODIFIED_DATE,DOMAIN_ID) values(?,?,?,?,?,?,?,?,?,?)");
				for (GoogleCrawlResult result : list) {
					ps.setInt(1, result.getNodeid());
					ps.setString(2, result.getUrl());
					ps.setString(3, result.getKeyWord());
					ps.setString(4, result.getTitle());
					ps.setString(5, result.getSnippet());
					ps.setString(6, result.getFileLocation());
					ps.setInt(7, result.getScore());
					ps.setObject(8, sdate);
					ps.setObject(9, sdate);
					int domain_id = (Integer) this.getDomainId(result.getDomain());
					ps.setInt(10, domain_id);
					ps.addBatch();
				}
				int[] results = ps.executeBatch();
				connection.commit();
				success = true;
			} catch (SQLException e) {
				e.printStackTrace();
				try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			} finally {
				close(rs, ps, connection);
			}
		}
		return success;
	}

	/*
	 * public List<Signatures> getSignatures(int corpusId){ List<Signatures>
	 * list = null; Connection connection = null; Statement st = null; ResultSet
	 * rs = null; try { connection = getConnection(); st =
	 * connection.createStatement(); rs =st.executeQuery(
	 * "select NODEID,NODETITLE,SIGNATUREWORD,SIGNATUREOCCURENCES from t_signatures where CORPUSID="
	 * +corpusId); list = new ArrayList<Signatures>(); while(rs.next()){
	 * Signatures s = new Signatures(); s.setNodeId(rs.getInt(1));
	 * s.setNodeTitle(rs.getString(2)); s.setSingatureWord(rs.getString(3));
	 * s.setSignatureOccurences(rs.getInt(4)); list.add(s); } } catch (Exception
	 * e) { e.printStackTrace(); }finally{ close(rs,st,connection); } return
	 * list; }
	 */

	public Map<String, String> getUrlAndKeyword() {
		Map<String, String> map = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		// TODO

		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st.executeQuery("select URL,KEYWORD from t_document");
			map = new HashMap<String, String>();
			while (rs.next()) {
				map.put(rs.getString(1), rs.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return map;
	}

	public boolean updateScore(String url, int score) {
		boolean success = false;
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		java.util.Date now = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2010-09-06
		// 09:22:58
		String sdate = sdf.format(now).toString();
		try {
			connection = getConn();
			connection.setAutoCommit(false);
			ps = connection
					.prepareStatement("UPDATE `t_document` SET SCORE=?,MODIFIED_DATE=?,FLAG=? WHERE URL=?");
			ps.setInt(1, score);
			ps.setObject(2, sdate);
			ps.setInt(3, score==0 ? 2:1);//flag to 1 means update the socre,flag to 2 means can't get the content of the url but had scanned this record.
			ps.setString(4, url);
			ps.executeUpdate();
			connection.commit();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			close(rs, ps, connection);
		}

		return success;
	}

	public boolean updateScore(Map<String, Integer> map) {
		boolean success = false;
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		java.util.Date now = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2010-09-06
		// 09:22:58
		String sdate = sdf.format(now).toString();

		if (map != null && map.size() > 0) {
			try {
				connection = getConn();
				connection.setAutoCommit(false);
				// UPDATE `t_document` SET SCORE=2000,LOCATION='OOOOOOO' WHERE
				// URL="http://www.youtube.com/watch?v=nxZZSWxdQMs"
				ps = connection
						.prepareStatement("UPDATE `t_document` SET SCORE=?,MODIFIED_DATE=? WHERE URL=?");
				for (Entry<String, Integer> entry : map.entrySet()) {
					ps.setInt(1, entry.getValue());
					ps.setObject(2, sdate);
					ps.setString(3, entry.getKey());
					ps.addBatch();
				}
				int[] results = ps.executeBatch();
				connection.commit();
				success = true;
			} catch (SQLException e) {
				e.printStackTrace();
				try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			} finally {
				close(rs, ps, connection);
			}
		}
		return success;
	}

	
	public List<DomainBean> getDomainsWoDotNet() {

		List<DomainBean> list = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st.executeQuery("select * from t_domain where DOMAIN_ID>1 order by DOMAIN_ID");
			list = new ArrayList<DomainBean>();
			while (rs.next()) {
				DomainBean g = new DomainBean(rs.getInt("DOMAIN_ID"), rs
						.getString("DOMAIN_NAME"));
				list.add(g);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}
	
	public List<DomainBean> getDomains() {

		List<DomainBean> list = null;
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st.executeQuery("select * from t_domain order by DOMAIN_ID");
			list = new ArrayList<DomainBean>();
			while (rs.next()) {
				DomainBean g = new DomainBean(rs.getInt("DOMAIN_ID"), rs
						.getString("DOMAIN_NAME"));
				list.add(g);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}

	public static void main(String[] args) throws Exception {
		java.util.Date now = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2010-09-06
		// 09:22:58
		String sdate = sdf.format(now).toString();
		Properties porps = PropertiestUtils.loadConfigFile();

		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(porps.getProperty(Constants.JDBC_DRIVER));
		dataSource.setUrl(porps.getProperty(Constants.JDBC_URL));
		dataSource.setUsername(porps.getProperty(Constants.JDBC_USER));
		dataSource.setPassword(porps.getProperty(Constants.JDBC_PASSWORD));
		dataSource.setInitialSize(1);
		SearchDocumentDao dao = new SearchDocumentDao();
		ClientBase cb = new ClientBaseImpl();
		List<Concepts> list = dao.getConcepts();

		for (Concepts concept : list) {
			int nodeId = concept.getNodeId();
			String nodeTitle = concept.getNodeTitle();
			List<MustHave> mustHaveList = dao.getMustHavesById(nodeId);
			List<String> keywordList = new ArrayList<String>();

			if (mustHaveList != null && mustHaveList.size() > 0) {
				for (int i = 0; i < mustHaveList.size(); i++) {
					MustHave mustHave = mustHaveList.get(i);
					String mustWord = mustHave.getMustWord();
					if (mustWord != null && mustWord.trim().length() > 0) {
						if (!nodeTitle.equalsIgnoreCase(mustWord)) {
							keywordList.add(nodeTitle + " " + mustWord);
						} else {
							keywordList.add(nodeTitle);
						}

					}
				}
			} else {
				keywordList.add(nodeTitle);
			}

			if (keywordList.size() > 0) {

				ContentParse2GoogleCrawlResult c = new ContentParse2GoogleCrawlResult();

				for (int j = 0; j < keywordList.size(); j++) {
					// System.out.println(keywordList.get(j) + "  keyword");
					List<GoogleCrawlResult> videolist = c
							.parseVedioContentToResult(keywordList.get(j), 80,
									"youtube.com", 0, true, nodeId);

					dao.batchInsertResult(videolist);
				}

				for (int j = 0; j < keywordList.size(); j++) {
					String[] domain = { ".org", ".edu", ".net", ".gov" };
					for (String str : domain) {
						List<GoogleCrawlResult> list_ = c.parseContentToResult(
								keywordList.get(j), 80, str, 0, false, nodeId);
						dao.batchInsertResult(list_);
					}
				}
			}
		}
	}

	public boolean updatsCrawlStatus(int nodeId, int i) {
		boolean successful = false;
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			connection.setAutoCommit(false);
			ps = connection.prepareStatement("UPDATE `t_metadata` SET CRAWLSTATUS=? WHERE NODEID=? and " +Constants.CORPUSID);
			ps.setInt(1, i);
			ps.setObject(2, nodeId);
			ps.executeUpdate();
			connection.commit();
			successful = true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			close(rs, ps, connection);
		}
		return successful;
	}
}
