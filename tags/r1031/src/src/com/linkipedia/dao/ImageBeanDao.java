package com.linkipedia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.linkipedia.entity.ImageBean;

public class ImageBeanDao extends Dao {

	public List<ImageBean> getImageBeansById(int nodeId) {
		List<ImageBean> list = new ArrayList<ImageBean>();
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st.executeQuery("select LINK1,LINK2 from t_imagelink where NODEID="
							+ nodeId);
			while (rs.next()) {
				ImageBean image = new ImageBean();
				image.setNodeId(nodeId);
				image.setImageLink1(rs.getString(1));
				image.setImageLink2(rs.getString(2));
				list.add(image);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
		return list;
	}
	
	public boolean insertImageBean(ImageBean imageBean){
		boolean success = false;
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
			try {
				connection = getConn();
				connection.setAutoCommit(false);
				ps = connection.prepareStatement("INSERT INTO `t_imagelink`(NODEID,LINK1,LINK2) values(?,?,?)");
				ps.setInt(1, imageBean.getNodeId());
				ps.setString(2, imageBean.getImageLink1());
				ps.setString(3, imageBean.getImageLink2());
				ps.executeUpdate();
				connection.commit();
				success = true;
			} catch (SQLException e) {
				e.printStackTrace();
				try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			} finally {
				close(rs, ps, connection);
			}
		return success;
	}
}
