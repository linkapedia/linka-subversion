package com.linkipedia.entity;

public class Signatures {
	private int corpusId;
	private int nodeId;
	private String nodeTitle;
	private String singatureWord;
	private int signatureOccurences;
	public int getCorpusId() {
		return corpusId;
	}
	public void setCorpusId(int corpusId) {
		this.corpusId = corpusId;
	}
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeTitle() {
		return nodeTitle;
	}
	public void setNodeTitle(String nodeTitle) {
		this.nodeTitle = nodeTitle;
	}
	public String getSingatureWord() {
		return singatureWord;
	}
	public void setSingatureWord(String singatureWord) {
		this.singatureWord = singatureWord;
	}
	public int getSignatureOccurences() {
		return signatureOccurences;
	}
	public void setSignatureOccurences(int signatureOccurences) {
		this.signatureOccurences = signatureOccurences;
	}
	public Signatures(int corpusId, int nodeId, String nodeTitle,
			String singatureWord, int signatureOccurences) {
		this.corpusId = corpusId;
		this.nodeId = nodeId;
		this.nodeTitle = nodeTitle;
		this.singatureWord = singatureWord;
		this.signatureOccurences = signatureOccurences;
	}

	public Signatures() {

	}

	
	

}
