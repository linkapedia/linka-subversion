package com.linkipedia.entity;
/**
 * Ticket #21 Change META DESCRIPTION
 */
public class Source {
	private int parentId;
	private int nodeId;
	private String nodeTitle;
	private String nodeSource;
	private int corpusId;
	private String corpusName;
	
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeTitle() {
		return nodeTitle;
	}
	public void setNodeTitle(String nodeTitle) {
		this.nodeTitle = nodeTitle;
	}
	public String getNodeSource() {
		return nodeSource;
	}
	public void setNodeSource(String nodeSource) {
		this.nodeSource = nodeSource;
	}
	public int getCorpusId() {
		return corpusId;
	}
	public void setCorpusId(int corpusId) {
		this.corpusId = corpusId;
	}
	public String getCorpusName() {
		return corpusName;
	}
	public void setCorpusName(String corpusName) {
		this.corpusName = corpusName;
	}
	public Source(int parentId, int nodeId, String nodeTitle,
			String nodeSource, int corpusId, String corpusName) {
		super();
		this.parentId = parentId;
		this.nodeId = nodeId;
		this.nodeTitle = nodeTitle;
		this.nodeSource = nodeSource;
		this.corpusId = corpusId;
		this.corpusName = corpusName;
	}
	public Source() {
		
	}
	
}
