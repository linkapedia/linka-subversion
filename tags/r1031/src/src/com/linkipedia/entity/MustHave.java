package com.linkipedia.entity;

public class MustHave {
	private int corpusId;
	private int nodeId;
	private String nodeTitle;
	private String mustWord;
	public int getCorpusId() {
		return corpusId;
	}
	
	public MustHave(int corpusId, int nodeId, String nodeTitle, String mustWord) {
		this.corpusId = corpusId;
		this.nodeId = nodeId;
		this.nodeTitle = nodeTitle;
		this.mustWord = mustWord;
	}
	
	public MustHave() {
	}

	public void setCorpusId(int corpusId) {
		this.corpusId = corpusId;
	}
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeTitle() {
		return nodeTitle;
	}
	public void setNodeTitle(String nodeTitle) {
		this.nodeTitle = nodeTitle;
	}
	public String getMustWord() {
		return mustWord;
	}
	public void setMustWord(String mustWord) {
		this.mustWord = mustWord;
	}
	
	

}
