package com.linkipedia.entity;

public class GoogleCrawlResult {
	//URL, title, snippet, score and file location of the full text. 
	private int nodeid;
	private String keyWord;
	private String url;
	private String title;
	private String snippet;
	private String fileLocation;
	private String fullText;
	private int score;
	private String domain;
	private int domainId;
	
	
	public int getNodeid() {
		return nodeid;
	}

	public void setNodeid(int nodeid) {
		this.nodeid = nodeid;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public GoogleCrawlResult() {
	}
	
	public GoogleCrawlResult(int nodeid,String url, String title, String snippet,
			String fileLocation, String fullText,int socre,String keyWord,String domain) {
		this.nodeid = nodeid;
		this.url = url;
		this.title = title;
		this.snippet = snippet;
		this.fileLocation = fileLocation;
		this.fullText = fullText;
		this.score = socre;
		this.keyWord = keyWord;
		this.domain = domain;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSnippet() {
		return snippet;
	}
	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public String getFullText() {
		return fullText;
	}
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return the domainId
	 */
	public int getDomainId() {
		return domainId;
	}

	/**
	 * @param domainId the domainId to set
	 */
	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}
	
	

}
