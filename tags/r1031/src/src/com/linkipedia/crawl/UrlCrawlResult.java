package com.linkipedia.crawl;

import java.util.List;

import com.linkipedia.dao.Dao;
import com.linkipedia.dao.SearchDocumentDao;
import com.linkipedia.entity.Signatures;

public class UrlCrawlResult extends Dao implements Runnable {

	private String url;
	private SearchDocumentDao dao;
	private int nodeId;

	public UrlCrawlResult(int nodeId, String url, SearchDocumentDao dao) {
		this.nodeId = nodeId;
		this.url = url;
		this.dao = dao;
	}

	public SearchDocumentDao getDao() {
		return dao;
	}

	public void setDao(SearchDocumentDao dao) {
		this.dao = dao;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public UrlCrawlResult() {
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void run() {

		ClientBaseImpl cb = new ClientBaseImpl();
		String url_ = this.url;
		url_ = url_.replace("&quot;", "\"").replace("&amp;", "&").replace(
				"&lt;", "<").replace("&gt;", ">").replace("&reg;", "®")
				.replace("&acute;", "´").replace("&plusmn;", "±");

		List<Signatures> signaturesList = dao.getSignaturesById(this.nodeId);
		int score = 0;
		boolean hadScan = false;
		if ( signaturesList != null && signaturesList.size() > 0) {
			String content = null;
			try {
				content = cb.doRequestFromUrl(url_);
			} catch (Exception e) {
				hadScan = true;
				//e.printStackTrace();
			} finally{
				if (hadScan)
					dao.updateScore(this.url, 0);
			}
			if (content != null && content.length() > 0) {
				for (Signatures signature : signaturesList) {
					String keyword_ = signature.getSingatureWord();
					String[] temp = content.toLowerCase().split(keyword_);
					if (temp.length > 1)
						score = score + (temp.length - 1);
				}
			}
		}
		if (score > 0) {
			dao.updateScore(this.url, score);
		}
	}
}
