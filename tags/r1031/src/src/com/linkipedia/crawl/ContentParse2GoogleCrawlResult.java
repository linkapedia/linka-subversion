package com.linkipedia.crawl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import com.linkipedia.entity.GoogleCrawlResult;
import com.linkipedia.util.LinkiPediaUtils;

public class ContentParse2GoogleCrawlResult {

	/**
	 * @param args
	 * @throws Exception
	 */

	private String keyword;
	private int numberPerPage;
	private String siteDomian;
	private int pageStart;
	private String titleAndHrefReg = "<a\\s*href=\"([^\"]+)\"\\s*target=_blank\\s*class=l\\s*[^>]*>(.*)</a></h3>";
	private String vediaTitleAndHrefReg = "<h3\\s* class=r><a\\s*href=\"([^\"]+)\"\\s*target=_blank\\s*class=l\\s*[^>]*>(.*)</a></h3>";
	private String snippetReg = "<div\\s* class=\"s\">(.*)<br>";
	private String vediaSnippetReg = "<br>(.*)<br><cite>";
	private int perListSize = 50;
	// TODO ,must delete list,this just for test
	List<GoogleCrawlResult> list = new ArrayList<GoogleCrawlResult>();

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getNumberPerPage() {
		return numberPerPage;
	}

	public void setNumberPerPage(int numberPerPage) {
		this.numberPerPage = numberPerPage;
	}

	public String getSiteDomian() {
		return siteDomian;
	}

	public void setSiteDomian(String siteDomian) {
		this.siteDomian = siteDomian;
	}

	public int getPageStart() {
		return pageStart;
	}

	public void setPageStart(int pageStart) {
		this.pageStart = pageStart;
	}

	public ContentParse2GoogleCrawlResult() {
	}

//	public ContentParse2GoogleCrawlResult(String keyword, int numberPerPage,
//			String siteDomian) {
//		this.keyword = keyword;
//		this.numberPerPage = numberPerPage;
//		this.siteDomian = siteDomian;
//	}
//
//	public ContentParse2GoogleCrawlResult(String keyword, int numberPerPage,
//			String siteDomian, int pageStart) {
//		this.keyword = keyword;
//		this.numberPerPage = numberPerPage;
//		this.siteDomian = siteDomian;
//		this.pageStart = pageStart;
//	}

	private static MatchResult getMatchGroup(String input, String patternStr) {
		try {
			MatchResult result = null;
			PatternCompiler compiler = new Perl5Compiler();
			Pattern pattern = compiler.compile(patternStr);
			PatternMatcher matcher = new Perl5Matcher();
			if (matcher.contains(input, pattern)) {
				result = matcher.getMatch();
			}
			if (result != null)
				return result;
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<GoogleCrawlResult> parseContentToResult(String keyword,int numberPerPage,String siteDomian,int start,boolean isVedio) throws ClientProtocolException, IOException {
		List<GoogleCrawlResult> list = new ArrayList<GoogleCrawlResult>();
		ClientBase client = new ClientBaseImpl();
		
		String content = client.getContentFromUrl(LinkiPediaUtils.formatGooelKeyword(keyword), numberPerPage, siteDomian, start, isVedio);
		String[] contents = content.split("<h3 class=\"r\">");
		for (int i = 1; i < contents.length; i++) {
			if (list.size() < perListSize) {
				MatchResult matchResult = getMatchGroup(contents[i], titleAndHrefReg);
				GoogleCrawlResult result = null;
				if (matchResult != null) {
					result = new GoogleCrawlResult();
					result.setUrl(matchResult.group(1));
					result.setTitle(LinkiPediaUtils.formatGoogleContent(matchResult.group(2)));
					result.setDomain(siteDomian);
					result.setKeyWord(keyword);
				}
				String temp_ = contents[i].replace("\n", " ")
						.replace("\r", " ");
				MatchResult matchResult_ = getMatchGroup(temp_, snippetReg);
				if (matchResult_ != null && result != null) {
					result.setSnippet(LinkiPediaUtils.formatGoogleContent(matchResult_.group(1)));
				}
				if(result != null)
					list.add(result);
			} else {
				break;
			}
		}
		return list;
	}
	
	public List<GoogleCrawlResult> parseContentToResult(String keyword,int numberPerPage,String siteDomian,int start,boolean isVedio,int nodeId) throws ClientProtocolException, IOException {
		List<GoogleCrawlResult> list = new ArrayList<GoogleCrawlResult>();
		ClientBase client = new ClientBaseImpl();
		
		String content = client.getContentFromUrl(LinkiPediaUtils.formatGooelKeyword(keyword), numberPerPage, siteDomian, start, isVedio);
		String[] contents = content.split("<h3 class=\"r\">");
		for (int i = 1; i < contents.length; i++) {
			if (list.size() < perListSize) {
				MatchResult matchResult = getMatchGroup(contents[i], titleAndHrefReg);
				GoogleCrawlResult result = null;
				if (matchResult != null) {
					result = new GoogleCrawlResult();
					result.setNodeid(nodeId);
					result.setDomain(siteDomian);
					result.setKeyWord(keyword);
					result.setUrl(matchResult.group(1));
					result.setTitle(LinkiPediaUtils.formatGoogleContent(matchResult.group(2)));
				}
				String temp_ = contents[i].replace("\n", " ")
						.replace("\r", " ");
				MatchResult matchResult_ = getMatchGroup(temp_, snippetReg);
				if (matchResult_ != null && result != null) {
					result.setSnippet(LinkiPediaUtils.formatGoogleContent(matchResult_.group(1)));
				}
				if(result != null)
					list.add(result);
			} else {
				break;
			}
		}
		return list;
	}

	public List<GoogleCrawlResult> parseVedioContentToResult(String keyword,int numberPerPage,String siteDomian,int start,boolean isVedio) throws ClientProtocolException, IOException {
		List<GoogleCrawlResult> list = new ArrayList<GoogleCrawlResult>();
		ClientBase client = new ClientBaseImpl();
		String content = client.getContentFromUrl(LinkiPediaUtils.formatGooelKeyword(keyword), numberPerPage, siteDomian, start, isVedio);
		String[] contents = content.split("<li class=\"g videobox s\" style=\"position:relative\">");
		for (int i = 1; i < contents.length; i++) {
			if (list.size() < perListSize) {
				MatchResult matchResult = getMatchGroup(contents[i],vediaTitleAndHrefReg);
				GoogleCrawlResult result = null;
				if (matchResult != null) {
					result = new GoogleCrawlResult();
					result.setUrl(matchResult.group(1));
					result.setTitle(LinkiPediaUtils.formatGoogleContent(matchResult.group(2)));
					result.setKeyWord(keyword);
					result.setDomain(siteDomian);
				}
				String temp_ = contents[i].replace("\n", " ").replace("\r", " ");
				MatchResult matchResult_ = getMatchGroup(temp_, vediaSnippetReg);
				if (matchResult_ != null && result != null) {
					result.setSnippet(LinkiPediaUtils.formatGoogleContent(matchResult_.group(1)));
				}
				if(result != null)
					list.add(result);
			} else {
				break;
			}
		}
		return list;
	}
	
	public List<GoogleCrawlResult> parseVedioContentToResult(String keyword,int numberPerPage,String siteDomian,int start,boolean isVedio,int nodeId) throws ClientProtocolException, IOException {
		List<GoogleCrawlResult> list = new ArrayList<GoogleCrawlResult>();
		ClientBase client = new ClientBaseImpl();
		String content = client.getContentFromUrl(LinkiPediaUtils.formatGooelKeyword(keyword), numberPerPage, siteDomian, start, isVedio);
		String[] contents = content.split("<li class=\"g videobox s\" style=\"position:relative\">");
		for (int i = 1; i < contents.length; i++) {
			if (list.size() < perListSize) {
				MatchResult matchResult = getMatchGroup(contents[i],vediaTitleAndHrefReg);
				GoogleCrawlResult result = null;
				if (matchResult != null) {
					result = new GoogleCrawlResult();
					result.setKeyWord(keyword);
					result.setNodeid(nodeId);
					result.setDomain(siteDomian);
					result.setUrl(matchResult.group(1));
					result.setTitle(LinkiPediaUtils.formatGoogleContent(matchResult.group(2)));
				}
				String temp_ = contents[i].replace("\n", " ").replace("\r", " ");
				MatchResult matchResult_ = getMatchGroup(temp_, vediaSnippetReg);
				if (matchResult_ != null) {
					result.setSnippet(LinkiPediaUtils.formatGoogleContent(matchResult_.group(1)));
				}
				if(result != null)
					list.add(result);
			} else {
				break;
			}
		}
		return list;
	}



}
