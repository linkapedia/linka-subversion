package com.linkipedia.constant;


public class Constants {
	/** The JDBC driver name.*/
	public final static String JDBC_DRIVER = "JDBC_DRIVER";
	/** The DB url.*/
	public final static String JDBC_URL = "JDBC_URL";
	/** The DB user name.*/
	public final static String JDBC_USER = "JDBC_USER";
	/** The DB password.*/
	public final static String JDBC_PASSWORD = "JDBC_PASSWORD";
		
	//Timer, unit is hour
	public final static String TIMER_INTERVAL = "TIMER_INTERVAL";
	
	public final static String FREEMARKER_TEMPLATE ="FREEMARKER_TEMPLATE";
	
	public final static String FREEMARKER_GENERATEFILEPATH ="FREEMARKER_GENERATEFILEPATH";
	
	public final static String HTML_TEMPLATE = "HTML_TEMPLATE";
	
	public final static String HTML_LEFT_TEMPLATE = "HTML_LEFT_TEMPLATE";
	
	public final static String HTML_TEMPLATE_JSTREE = "HTML_TEMPLATE_JSTREE";
	

	//Ticket #20 Ensure results contain the word "coffee"
	public final static String WORD_COFFEE = "coffee";
	
	public final static String WORD_COFFEA = "coffea";
	
	//the version of csv
	public final static String CORPUSID = "CORPUSID = 2670";
	
}
