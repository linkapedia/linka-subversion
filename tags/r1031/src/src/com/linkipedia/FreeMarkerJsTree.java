package com.linkipedia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.linkipedia.constant.Constants;
import com.linkipedia.dao.LinkDao;
import com.linkipedia.dao.SearchDocumentDao;
import com.linkipedia.entity.Concepts;
import com.linkipedia.entity.DomainBean;
import com.linkipedia.entity.GoogleCrawlResult;
import com.linkipedia.entity.Link;
import com.linkipedia.entity.Signatures;
import com.linkipedia.entity.Source;
import com.linkipedia.util.PropertiestUtils;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreeMarkerJsTree

{

	private static freemarker.template.Configuration cfg;
	private static Logger logger = Logger.getLogger(FreeMarkerJsTree.class);
	private static Properties props = null;
	private static String filePath;

	private void init() throws Exception {
		try {
			props = PropertiestUtils.loadConfigFile();
			cfg = new freemarker.template.Configuration();
			// 设置FreeMarker的模版文件位置

			cfg.setDirectoryForTemplateLoading(new File(props
					.getProperty(Constants.FREEMARKER_TEMPLATE)));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			filePath = props.getProperty(Constants.FREEMARKER_GENERATEFILEPATH);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void process_resultpages(Map globalRoot, List<Link> links,
			Template t) throws Exception {

		for (Link link : links) {
			FileOutputStream fos = new FileOutputStream(filePath
					+ String.valueOf(link.getNodeid()) + ".html");
			System.out.println("Creating " + link.getLink());

			Writer out = new OutputStreamWriter(fos, "UTF-8");

			globalRoot.put("link", link);
			//ticket #17 'Modify title, but limit title to 70 characters'
			globalRoot.put("title", concatenateAllParentsTitle(link));
			globalRoot.put("header", link.getNodetitle());
			Concepts concept = new Concepts(link.getNodeid());
			// description
			List<Signatures> signatures = concept.getSignatures();
			StringBuilder sb = new StringBuilder();
			for (Signatures sig : signatures) {
				sb.append(sig.getNodeTitle());
				sb.append(",");
			}
			String description = "";
			if (sb.length() > 1)
				description = sb.substring(1, sb.length() - 1);
			
			
			//Ticket #18 Change meta keywords to add signatures
			globalRoot.put("keywords", link.getNodetitle()+","+description);
			
			// search results
			Map<String, List<GoogleCrawlResult>> results = concept.getResults();
			globalRoot.put("results", results);

			/**
			 * Ticket #21 Change META DESCRIPTION
			 */
			List<Source> source = concept.getSource();
			StringBuilder sb_Source = new StringBuilder();
			for (Source soc : source) {
				String nodeSource = soc.getNodeSource();
				if(!(nodeSource.isEmpty() || " ".equals(nodeSource) )){
					sb_Source.append(nodeSource);
					sb_Source.append(",");
				}
			}
			String description_ = "";
			if (sb_Source.length() > 1){
				description_ = sb_Source.substring(1, sb_Source.length() - 1);
				if(description_.length() > 160){
					description_ = description_.substring(1, 160);
				}
			}else{
				for(Iterator<Map.Entry<String,List<GoogleCrawlResult>>> it = results.entrySet().iterator();it.hasNext();){
					List<GoogleCrawlResult> list = it.next().getValue();
					if(list.size() >= 1){
						description_ = list.get(0).getTitle();
						break;
					}
				}
			}
			globalRoot.put("description", description_);
			
			
			t.process(globalRoot, out);
			if (link.getChildren() != null && link.getChildren().size() > 0)
				process_resultpages(globalRoot, link.getChildren(), t);
		}

	}
	/**
	 * @author Kevin
	 * @based on ticket #17 'Modify title, but limit title to 70 characters'
	 * @param title
	 * @return
	 */
	private String concatenateAllParentsTitle(Link link){
		String title = getParentsTitle(link);
		if(title.length()>70){
			String dots = "...";
			title = title.substring(0, 70-dots.length())+dots;
		}
		return title;
	}
	private String getParentsTitle(Link link){
		String title = "";
		Link parent = link.getParent();
		if( parent != null){
			title = getParentsTitle(parent)+" "+link.getNodetitle();
		}else{
			title = link.getNodetitle();
		}
		return title;
	}
	
	private void process_homepage(Map globalRoot, List<Link> links, Template t)
			throws Exception {

		FileOutputStream fos = new FileOutputStream(filePath + "index.html");
		Writer out = new OutputStreamWriter(fos, "UTF-8");

		if (logger.isInfoEnabled())
			logger.info("Creating Homepage.");

		globalRoot.put("title", "Linkipedia Home");
		globalRoot.put("header", "Linkipedia Home");
		globalRoot.put("keywords", "Linkipedia");
		globalRoot.put("description", "Linkipedia Home");

		t.process(globalRoot, out);

		if (logger.isInfoEnabled())
			logger.info("Homepage created.");
	}

	public void process() throws Exception, IOException {
		if (logger.isInfoEnabled())
			logger.info("Process will generate html files!");

		init();
		Template t = cfg.getTemplate(props
				.getProperty(Constants.HTML_TEMPLATE_JSTREE));

		Map globalRoot = new HashMap();
		List<Link> links = Link.getLinksGraph();
		globalRoot.put("links", links);

		String json4tree = getJson(links);
		globalRoot.put("json4tree", json4tree);

		List<DomainBean> domains = DomainBean.getDomainsWoDotNet();
		globalRoot.put("domainbeanlist", domains);

		globalRoot.put("homepage", "true");
		process_homepage(globalRoot, links, t);

		globalRoot.put("homepage", "false");
		process_resultpages(globalRoot, links, t);
		if (logger.isInfoEnabled())
			logger.info("Generate html files done!");
	}

	private void processLeft(Map globalRoot, List<Link> links, Template t)
			throws TemplateException, IOException {
		FileOutputStream fos = new FileOutputStream(filePath + "left.html");
		System.out.println("Creating left content.");

		Writer out = new OutputStreamWriter(fos, "UTF-8");
		t.process(globalRoot, out);

	}

	private String getLeft() throws IOException {
		FileReader f = new FileReader(filePath + "left.html");
		BufferedReader r = new BufferedReader(f);
		StringBuilder sb = new StringBuilder();
		String temp;
		while ((temp = r.readLine()) != null) {
			sb.append(temp);
		}
		return sb.toString();
	}

	private String getJson(List<Link> links) {

		StringBuilder linksJson = new StringBuilder();
		linksJson.append("[");
		for (Link link : links) {
			linksJson.append(getJSon(link));
			linksJson.append(",");
		}
		linksJson.delete(linksJson.length() - 1, linksJson.length());
		linksJson.append("]");
		
		//Ticket#19 Create a Site Map
		generateSiteMapText(links);
		
		return linksJson.toString();

	}
	
	private String getJSon(Link link) {
		List<Link> children = link.getChildren();
		String title = getShortString(link.getNodetitle(), link
				.getDepthfromroot());
		String hint;
		if (title.equals(link.getNodetitle()))
			hint = "";
		else
			hint = ",title:\" " + link.getNodetitle() + "\"";

		if (children == null || children.size() == 0) {
			return "{data:{title:\"" + title + "\",attr:{href:\""
					+ link.getLink() + "\"" + hint + "}}}";

		} else {
			StringBuilder childrenJson = new StringBuilder();
			for (Link child : children) {
				childrenJson.append(getJSon(child));
				childrenJson.append(",");
			}
			childrenJson.delete(childrenJson.length() - 1, childrenJson
					.length());
			return "{data:{title:\"" + title + "\",attr:{href:\""
					+ link.getLink() + "\"" + hint + "}},children:["
					+ childrenJson.toString() + "]}";
		}
	}

	private String getShortString(String string, int level) {
		return getShortString(string, 35, level);
	}

	private String getShortString(String string, int length, int level) {
		length = (length - level * 2);
		String dots = "...";
		length = length - dots.length();
		if (string.length() > length)
			return string.substring(0, length) + dots;
		else
			return string;
	}
	/**
	 * Ticket#19 Create a Site Map
	 */
	private void generateSiteMapText(List<Link> links){
		String string = generateLinks(links);
	    try{
	    	FileWriter   fw=new   FileWriter( "./siteMap.txt ",true);
	    	BufferedWriter   bw=new   BufferedWriter(fw);
	    	bw.write(string);
	    	bw.close();
	    	fw.close();
	    }catch(IOException   e){
	    		 
	    } 
	}
	
	private String generateLinks(List<Link> links){
		StringBuffer linkString = new StringBuffer("");
		if (links != null && links.size() > 0) {
			linkString.append("<ul>");
			for(Link link : links){
				linkString.append(geerateOneLink(link));
			}
			linkString.append("</ul>");
		}
		return linkString.toString();
	}
	private String geerateOneLink(Link link){
		StringBuffer oneLink = new StringBuffer();
		oneLink.append("<li>");
		oneLink.append("<a href=\""+link.getLink()+"\" >");
		oneLink.append(link.getNodetitle().replaceAll("&", "&amp;"));
		oneLink.append("</a>");
		List<Link> children = link.getChildren();
		if(children != null && children.size() > 0){
			oneLink.append(generateLinks(children));
		}
		oneLink.append("</li>");
		return oneLink.toString();
	}
	
	
	public static void main(String[] args) throws Exception {
		new FreeMarkerJsTree().process();
	}

}
