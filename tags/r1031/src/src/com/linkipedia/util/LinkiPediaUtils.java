package com.linkipedia.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;


public class LinkiPediaUtils {

	public static List<String> tokenizeStringToList(String str, String delimiters, 
			boolean trimToken) {
		if (str == null || str.length() == 0)
			return new ArrayList<String>(0);
		StringTokenizer tokenizer = new StringTokenizer(str, delimiters, false);
		int count = tokenizer.countTokens();
		List<String> values = new ArrayList<String>(count);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (trimToken)
				token = token.trim();
			values.add(token);
		}
		return values;
	}

	public static String formatGoogleContent(String Content) {
		  if (Content != null && Content.length() > 0 ) {
			  GoogleFormatter wtf = new GoogleFormatter(Content);
			  return wtf.format().toString();  
		  }
		  return "";		  
	  }
	
	
	//FIXME,if the title is Company Coffee Inc. must parse to Company+Coffee+Inc.
	public static String formatGooelKeyword(String title){
		return title.replace(" ", "+").replace("'", "%27").replace("&", "%26").replace("\"", "%22");	
	}
	
	public static int getScore(String content,String keyWord){
		String[] a = content.split(keyWord);
		int score = a.length;
		a = null;
		return score;	
	}
	
	public static int searchStringInArray(String[] strArray, String str) {
		boolean ret = false;
		for (int i = 0; i < strArray.length; ++i) {
			if (strArray[i] == null) {
				ret = (strArray[i] == str);
			}
			else if (str == null) {
				ret = (strArray[i] == str);
			}
			else {
				ret = strArray.equals(str);
			}
			if (ret)
				return i;
			
		}
		return -1;
	}
}


class GoogleFormatter {
	private Stack<Tag> pairStack = new Stack<Tag>();
	private StringBuilder text = null;
	
	public GoogleFormatter(String pText) {
		StringBuilder sb = new StringBuilder(pText.length());
		sb.append(pText);
		this.text = sb;
	}
	
	/**
	 * @param startIndex
	 * @param continuousChar
	 * @return
	 */
	private int countContinuousChar(int startIndex, int endIndex, char continuousChar) {
		int textLastIndex = this.text.length() - 1;
		if (endIndex > textLastIndex)
			endIndex = textLastIndex;
		char currentChar;
		int count = 0;
		for (int i = startIndex; i <= endIndex; ++i) {
			currentChar = this.text.charAt(i);
			if (currentChar == continuousChar) {
				++count;
			}
			else {
				break;
			}
		}
		return count;
	}
	private int indexOf(int startIndex, int endIndex, String target) {
		if (target == null || target.length() <= 0) {
			throw new IllegalStateException("Target can not be empty.");
		}
		int textLastIndex = this.text.length() - 1;
		if (startIndex > textLastIndex)
			return -1;
		if (endIndex > textLastIndex)
			endIndex = textLastIndex;
		
		char firstTargetChar = target.charAt(0);
		for (int i = startIndex; i <= endIndex; ++i) {
			char srcCurrentChar = this.text.charAt(i);
			if (srcCurrentChar == firstTargetChar) {
				boolean match = true;
				for (int j = 1; j < target.length(); ++j) {
					char nextTargetChar = target.charAt(j);
					char nextSrcChar = this.text.charAt(i + j);
					if (nextTargetChar != nextSrcChar) {
						match = false;
						break;
					}
				}
				if (match) {
					return i;
				}
			}
		}
		return -1;
	}
	private int indexOf(int startIndex, String target) {
		return (startIndex > this.text.length() - 1) 
		    ? -1 : this.text.indexOf(target, startIndex);
	}
	private boolean startWith(int startIndex, String target) {
		int size = this.text.length();
		int restSrcSize = size - 1 - startIndex + 1;
		if (restSrcSize < target.length())
			return false;
		boolean match = true;
		for (int offset = 0; offset < target.length(); ++offset) {
			if (this.text.charAt(startIndex + offset) != target.charAt(offset)) {
				match = false;
				break;
			}
		}
		return match;
	}
	
	public String format() {
		this.handleLines();
		this._format();
		return this.trimNullChar();
	}
	private String trimNullChar() {
		StringBuilder sb = new StringBuilder(this.text.length());
		int size = this.text.length();
		char currentChar;
		for (int i = 0; i < size; ++i) {
			currentChar = this.text.charAt(i);
			if (currentChar != '\0')
				sb.append(currentChar);
		}
		this.text = null;
		return sb.toString();
	}
	
	private void _format() {
		int _currentIndex = 0;
		char _currentChar;
		while (_currentIndex < this.text.length()) {
			_currentChar = this.text.charAt(_currentIndex);
			if (_currentChar == '<') {
				_currentIndex = this._handleTag(_currentIndex);
			}
			else {
				++_currentIndex;
			}
		}
		//Only trim the unbalanced left tags.
		if (this.pairStack.size() > 0) {
			for (Tag leftTag : this.pairStack) {
				this.ignoreText(leftTag.startIndex, leftTag.endIndex);
			}
		}
	}
	
	
	/**
	 * Find the relevant right tag, the limitation is the embed level must be one.  
	 * @param currentText Current text.
	 * @param currentEndTagIndex The current index of right tag
	 * @param leftTag The left tag name.
	 * @param rightTag The right tag name.
	 * @return The relevant right tag index.
	 */
	private int findRelevantRightTag(String currentText, int currentRightTagIndex, String leftTag, String rightTag) {
		if (currentText == null || currentText.length() <= 0) {
			return currentRightTagIndex;
		}
		int internalLeftIndex = currentText.indexOf(leftTag);
		if (internalLeftIndex >= 0) {
			int nextRightLinkIndex = this.indexOf(currentRightTagIndex+2, rightTag);
			//No more right link.
			if (nextRightLinkIndex < 0) {
				return currentRightTagIndex;
			}
			//Another right link is available. 
			else {
				//Link text is empty.
				if (currentRightTagIndex+2 == nextRightLinkIndex) {
					return nextRightLinkIndex;
				}
				String nextLinkText = this.text.substring(currentRightTagIndex+2, nextRightLinkIndex);
				return findRelevantRightTag(nextLinkText, nextRightLinkIndex, leftTag, rightTag);
			}
		}
		else {
			return currentRightTagIndex;
		}
	}
	private int _handleTag(int startIndex) {
		//Trim the comment tag first.
		if (this.startWith(startIndex+1, "!--")) {
			 int rightCommentIndex = this.indexOf(startIndex+4, "-->");
			 if (rightCommentIndex == -1) {
				 return startIndex + 1;
			 }
			 else {
				 this.ignoreText(startIndex, rightCommentIndex+2);
				 return rightCommentIndex + 3;
			 }
		}
		
		int rightIndex = this.indexOf(startIndex+1, ">");
		//text.indexOf(">", startIndex);
		int distance = rightIndex - startIndex;
		//Found relevant '>'
		if (distance > 1) {
			Tag tmpTag = new Tag(startIndex, rightIndex, text.substring(startIndex+1, rightIndex));
			return this._handleTag(tmpTag);
		}
		else if (distance == 1) {
			this.ignoreText(startIndex, rightIndex);
			return rightIndex + 1;
		}
		//Not found
		else {
			return startIndex + 1;
		}
	}
	
	
	private int _handleTag(Tag tmpTag) {
	    if (tmpTag.name.length() <= 0) {
	    	this.ignoreText(tmpTag.startIndex, tmpTag.endIndex);
	    	return tmpTag.endIndex + 1;
	    }
//NOTICE, DO NOT DELETE IT.
//		if (tmpTag.isCommentTag()) {
//			this.ignoreText(tmpTag.startIndex, tmpTag.endIndex);
//			return tmpTag.endIndex + 1;
//		}
		//Left tag <..>
		else if (tmpTag.isLeftTag()) {
			//Reserve the nowiki tags.
			if (tmpTag.isNoWikiTag()) {
				String rightTagName = "</" + tmpTag.name + '>';
				int rightIndex = this.text.indexOf(rightTagName, tmpTag.endIndex+1);
				if (rightIndex >= 0) {
					int rightEndIndex = rightIndex+rightTagName.length()-1;
					this.ignoreText(tmpTag.startIndex, tmpTag.endIndex);
					this.ignoreText(rightIndex, rightEndIndex);
					return rightEndIndex + 1;
				}
				//Relevant nowiki tag not found.
				else {
					this.ignoreText(tmpTag.startIndex, tmpTag.endIndex);
					return tmpTag.endIndex + 1;
				}
			}
			else {
			    pairStack.push(tmpTag);
			    return tmpTag.endIndex + 1;
			}
		}
		//Ignore the whole tag like <br />
		else if (tmpTag.isWholeTag()) {
			this.ignoreText(tmpTag.startIndex, tmpTag.endIndex);
			return tmpTag.endIndex + 1;
		}
		//Right tag </..>
		else {
			Tag leftTag = this.deepSearchRelevantLeftTag(tmpTag);
			//Trim current right tag
			if (leftTag == null) {
				this.ignoreText(tmpTag.startIndex, tmpTag.endIndex);
			}
			else if (leftTag.isRefTag()) {
				this.ignoreText(leftTag.startIndex, tmpTag.endIndex);
			}
			else {
				this.ignoreText(leftTag.startIndex, leftTag.endIndex);
				this.ignoreText(tmpTag.startIndex, tmpTag.endIndex);
				//FIXME we should transfer the text between the two tags.
			}
			return tmpTag.endIndex + 1;
		}
	}
	
	private Tag deepSearchRelevantLeftTag(Tag rightTag) {
		int size = this.pairStack.size();
		Tag currentLeftTag = null;
		//Tag matchTag = null;
		int matchIndex = -1;
		for (int i = size-1; i >=0; --i) {
			currentLeftTag = this.pairStack.elementAt(i);
			if (rightTag.isRelevantLeftTag(currentLeftTag)) {
				//matchTag =  currentLeftTag;
				matchIndex = i;
				break;
			}
		}
		//No left tag's found, delete the right tag.
		if (matchIndex == -1) {
			this.ignoreText(rightTag.startIndex, rightTag.endIndex);
			return null;
		}
		for (int i = matchIndex + 1; i < size; ++i) {
			Tag discardLeftTag = this.pairStack.elementAt(i);
			this.ignoreText(discardLeftTag.startIndex, discardLeftTag.endIndex);
			//FIXME, we should transfer the text between currentLeftTag.endIndex to rightTag
			this.pairStack.setElementAt(null, i);
		}
		Iterator<Tag> iter =  this.pairStack.iterator();
		while (iter.hasNext()) {
			Tag tmpTag = iter.next();
			if (tmpTag == null)
				iter.remove();
		}
		return this.pairStack.pop();//Notice, pop it.
		
	}

	/**
	 * Trim the list characters, retrieve the first non-list-character index.
	 * @param startIndex of text
	 * @param endIndex of text(including)
	 * @return retrieve the first non-list-character index.
	 */
	private int trimHeadingChars(int startIndex, int endIndex, String specifiedChars) {
		char currentChar;
		for (int i = startIndex; i <= endIndex; ++i) {
			currentChar = this.text.charAt(i);
			if (specifiedChars.indexOf(currentChar) >= 0) {
				this.text.setCharAt(i, '\0');
			} else {
				return i;
			}
		}
		return endIndex + 1;//Notice, it excesses the endIndex.
	}
	/**
	 * Trim the heading sector, =xxx=, return the first nonEqualCharacter index.
	 * @param startIndex
	 * @param endIndex
	 * @return The first nonEqualCharacter index.
	 */
	private void trimHeadingSector(int startIndex, int endIndex) {
		int count = this.countContinuousChar(startIndex, endIndex, '=');
		if (count <= 1) {
			return;
		}
		String rightStr = this.text.substring(startIndex, startIndex+count);
		int rightIndex = this.indexOf(startIndex+count, endIndex, rightStr);
		if (rightIndex > startIndex) {
			this.ignoreText(startIndex, startIndex+count-1);
			this.ignoreText(rightIndex, rightIndex+count-1);
		}
	}
	private void trimHeadingContinuousChar(int startIndex, int endIndex, 
			char specifiedChar, int continuousTime) {
		int count = this.countContinuousChar(startIndex, endIndex, specifiedChar);
		if (count >= continuousTime) {
			this.ignoreText(startIndex, startIndex+continuousTime-1);
		}
	}
	
	/**
	 * First we should trim the line heading to make it easy to transfer the wiki text.
	 */
	private void handleLines() {
		int _previousLineIndex = -1;
		int _currentIndex = 0;
		char currentChar;
		int textSize = this.text.length();
		while (_currentIndex < textSize) {
			currentChar = this.text.charAt(_currentIndex);
			if (currentChar == '\r' || currentChar == '\n') {
				this.handleLine(_previousLineIndex+1, _currentIndex-1);
				_previousLineIndex = _currentIndex;
				this.text.setCharAt(_currentIndex, '\0');
			}
			else if (_currentIndex == textSize -1) {
				this.handleLine(_previousLineIndex+1, _currentIndex);
				_previousLineIndex = _currentIndex;
			}
			++_currentIndex;
		}
	}
	private void handleLine(int startIndex, int endIndex) {
		if (startIndex >= endIndex)
			return;
		char currentChar = this.text.charAt(startIndex);
		if (currentChar == '=') {
			this.trimHeadingSector(startIndex, endIndex);
		}
		else if (currentChar == '-') {
			this.trimHeadingContinuousChar(startIndex, endIndex, '-', 4);
		}
		else if ("*#:;".indexOf(currentChar) >= 0) {
			this.trimHeadingChars(startIndex, endIndex, "*#:;");
		}
	}

	private void ignoreText(int startIndex, int endIndex) {
		for (int i = startIndex; i <= endIndex; ++i) {
			this.text.setCharAt(i, '\0');
		}
	}
}
class Tag {
	public String name = null;
	//-1 is left tag, 0 is whole tag, 1 is right tag.  
	public short tagType = 0;
	public int startIndex = 0;
	public int endIndex = 0;
	public Tag(int pStartIndex, int pEndIndex, String pName) {
		if (pName.charAt(0) == '/') {
			this.tagType = 1;
			this.name = pName.substring(1);
		}
		else if (pName.charAt(pName.length()-1) == '/') {
			this.tagType = 0;
			this.name = pName.substring(0, pName.length()-1);
		}
		else {
			this.tagType = -1;
			this.name = pName;
			//if (!this.isCommentTag())//NOTICE, do not delete it.
			trimTagAttributes();
		}
		this.startIndex = pStartIndex;
		this.endIndex = pEndIndex;
		this.name = this.name.trim();
	}
	private void trimTagAttributes() {
		int index = this.name.indexOf(' ');
		if (index > 0) {
			this.name = this.name.substring(0, index);
		}
	}
//NOTICE, do not delete it.
//	public boolean isCommentTag() {
//		return this.name.startsWith("!--") && this.name.endsWith("--");
//	}
	public boolean isRefTag() {
		return "ref".equalsIgnoreCase(this.name);
	}
	public boolean isLeftTag() {
		return tagType == -1;
	}
	public boolean isWholeTag() {
		return tagType == 0;
	}
	public boolean isRightTag() {
		return tagType == 1;
	}
	public boolean isRelevantLeftTag(Tag leftTag) {
		if (!leftTag.isLeftTag()) 
			return false;
		//FIXME, add more validation later.
		return this.name.equalsIgnoreCase(leftTag.name);
	}
	public boolean isNoWikiTag() {
		return "nowiki".equalsIgnoreCase(this.name) || 
		       "pre".equalsIgnoreCase(this.name) ||
		       "source".equalsIgnoreCase(this.name);
	}
	

}




