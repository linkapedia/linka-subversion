<#list links as biglink>			
	<li>
		<a title="${biglink.nodetitle}" href="${biglink.link}"><#if biglink.nodetitle?length gt 30>${biglink.nodetitle?substring(0,30)}...<#else>${biglink.nodetitle}</#if></a>	
		<ul>
			<#if biglink.children?exists> 
			<#list biglink.children as smalllink>						
		 	<li>
		 		 <a  title="${smalllink.nodetitle}" href="${smalllink.link}"><#if smalllink.nodetitle?length gt 25>${smalllink.nodetitle?substring(0,25)}...<#else>${smalllink.nodetitle}</#if></a>			 		
			 		 <#if smalllink.children?exists>
			 		 <#list smalllink.children as littlelink>	
			 		 <ul>
			 		 	<li>					 		 
			 		 	<a  title="${littlelink.nodetitle}" href="${littlelink.link}" ><#if littlelink.nodetitle?length gt 30>${littlelink.nodetitle?substring(0,30)}...<#else>${littlelink.nodetitle}</#if></a>
			 		 	</li>						    	
			    	</ul>
			    	</#list>
			    	</#if>
			</li>    							    
		    </#list>
		    </#if>
	    </ul>
    </li>			    				
</#list>