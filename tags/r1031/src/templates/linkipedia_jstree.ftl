<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${title}</title>
	<link href="css/common.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="js/_lib/jquery.js"></script>
	<script type="text/javascript" src="js/_lib/jquery.cookie.js"></script>
	<script type="text/javascript" src="js/_lib/jquery.hotkeys.js"></script>
	<script type="text/javascript" src="js/jquery.jstree.js"></script>
	
	<!--[if IE 6]>
		<script type="text/javascript" src="css/iepng.js" ></script>
		<script type="text/javascript">
		DD_belatedPNG.fix('div, ul, img, li, input');
		</script>
	<![endif]-->
	<meta name="Description" content="${description}" />
	<meta name="Keywords" content="${keywords}" />
	
	<script type="text/javascript">
	//<![CDATA[ 
		
		$(function(){
			showTab(2);	
			$("#tree").jstree({
			json_data:{data:${json4tree}},
			themes : {  
				"theme" : "classic",  
				"dots" : true,  
				"icons" : false 	
			},  							
			plugins : [ "themes", "json_data" ]	
			});
			//$("div.content_left a[href$='" + getFileName() + "']").addClass("now");
		});

		

		function getFileName(){
			 var url = window.location.href;   
			  fileName = url.split("//")[1].split("/");			
			  //file = fileName[fileName.length-1].split(".")[0]; 
			  return fileName;
			}

		function showTab(tab){
			var allTabs=$("div#results div.tab");
			allTabs.hide();
			$(".link3 a").removeClass("now");
			var tabdiv=$("div#results div#tab" + tab);
			//if (tabdiv==null)
			//	alert("No result in the domain.");
			tabdiv.show();
			$(".link3 a#dommainlink" + tab).addClass("now");
		}
		//]]> 
	</script>

</head>

<body>
<div id="container">

	<div id="header">    <div id="logo2"><a href="http://www.konamountaincoffee.com"><img src="images/logo2.png" alt="logo" class="float_r"/></a></div>			
		<div id="link_right">
		  <div class="link2">
				<script type="text/javascript">
				//<![CDATA[ 
					function shs_click(type) {
						u=location.href;
						t=document.title;
						switch (type){
						case "fb":
							url = 'http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t);
							break;
						case "tw":
							url ='http://twitter.com/share?text=Check+out+'+encodeURIComponent(u)+'&t='+encodeURIComponent(t);
							break;
						case "dg":
							url = 'http://digg.com/submit?url='+encodeURIComponent(u)+'&t='+encodeURIComponent(t);
							break;
						} 
						window.open(url,'sharer','toolbar=0,status=0,width=626,height=436');
						return false;
					}
					//]]> 
				</script>
				<a onclick="event.cancelBubble=true; return shs_click('fb');" target="_blank" href="http://www.facebook.com/share.php?u=www.linkapedia.com"><img src="images/fackbook.png" alt="facebook" width="32" height="32" /></a>
				<a onclick="event.cancelBubble=true; return shs_click('tw');" target="_blank" href="http://twitter.com/home?status=Check+out+http://www.linkapedia.com"><img src="images/twitter.png" alt="twitter" width="32" height="32" /></a>
				<a onclick="event.cancelBubble=true; return shs_click('dg');" target="_blank" href="http://digg.com/submit?url=http://www.linkapedia.com"><img src="images/digg.png" alt="digg" width="32" height="32" /></a>
			</div>
			<div class="link3">
					<ul>
						<#list domainbeanlist?sort_by("domain_id") as domain>
						<li class="domainbutton"><a id="dommainlink${domain.domain_id}" href="#" onclick="showTab(${domain.domain_id})">${domain.domain_name}</a></li>
						<#if (domain_index lt domainbeanlist?size-1) >  
							<li><img src="images/line1.png" alt="no picture"/></li>
						</#if>
						</#list>
					</ul>
			</div>
		</div>
		<div id="logo"><a href="#"><img src="images/logo1.png" alt="logo"/></a></div>
	</div>
	
	<div id="content">
		<div class="location">
		<#--assign count=0-->
		<#if homepage=="true">
			
			<#else>
			<#list link.path as crumb>
				<a href="${crumb.link}">${crumb.nodetitle}</a>
				<#--assign count=count+1--> 
				<#if (crumb_index lt link.path?size-1) > -> </#if>
			</#list>	
		 </#if>
		
		</div>
		<div class="content_left">
			<div id="tree"></div>
		</div>
		<div class="content_right">
			<h1>${header}</h1>
			<table>
			  <tr>
			    <td width="219"><img src="images/pic1.gif" alt="no picture"/></td>
			    <td width="193"><img src="images/pic2.gif" alt="no picture"/></td>
			    <td width="43">&nbsp;</td>
			  </tr>
			  <tr>
			    <td align="center">Dark roasted coffee beans</td>
			    <td align="center">Light roasted coffee beans</td>
			    <td>&nbsp;</td>
			  </tr>
			</table>
			
			<div id="results">	
				<#if homepage=="true">
					<p><object><h3>Welcome to linkapedia!</h3></object>Please click the links to view search results.</p>
					<#else>
					<#--assign domain_ids=results?keys?sort-->		
					<#--list domain_ids as domain_id-->	
					
					<#list domainbeanlist?sort_by("domain_id") as domain>
						<div class="tab" id="tab${domain.domain_id}">
							<#if results?keys?seq_contains(domain.domain_id?c)>							
								<#list results[domain.domain_id?c] as snippet>
									<p><object><h3><a href="${snippet.url}">"${snippet.title}"</a></h3></object>"${snippet.snippet!''}"</p>
								</#list>
								<#else>
								<p><object><h3>No result found for this domain.</h3></object></p>
							</#if>
						</div>			
					</#list>	
				
				</#if>		
			</div>			
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="footer">© 2010 Linkapedia. All rights reserved&nbsp;&nbsp;&nbsp;<a href="sitemap.html">Site Map</a></div>
</div>
</body>
</html>
