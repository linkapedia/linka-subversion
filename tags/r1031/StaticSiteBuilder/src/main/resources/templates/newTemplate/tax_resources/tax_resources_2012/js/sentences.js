/*
 *Andres restrepo
 *
 *This file contain all logic to get sentenses
*/
function getSentences(urlServer,urlLink,nodeId,fileMd5){
    //call ajax to get result
    $.ajax({
        url: urlServer,
        dataType: "jsonp",
        data: "nodeId="+nodeId+"&fileName="+fileMd5+"&sentenceType="+1,
        success: function( data ) {
            //parse and return data
	    var string = "<div style='padding:10px; background-color:black;opacity:0.9'>";
	    string+="<a href=\"#\" onclick=\"getSentencesAll('"+urlServer+"','"+nodeId+"','"+fileMd5+"');\" > Debug sentences</a>";

            for (i = 0; i < data.length; i++){
                var sentence=data[i].sentence;
                //build html <div> and append
		string+="<br>";
		string+="<p>"+sentence+"</p><br>";
            }
            string+="<a href='"+urlLink+"' target='_blank'>Go to page</a>";
	    string+="</div>";
	    $.colorbox({html:string,
			width:"50%", 
            		maxHeight:"80%", 
            		fixed:true,
            		overlayClose:false
	    });	    
        }
    });
}
	
function getSentencesAll(urlServer,nodeId,fileMd5){
    //call ajax to get result
    $.colorbox.close();
    $.ajax({
        url: urlServer,
        dataType: "jsonp",
        data: "nodeId="+nodeId+"&fileName="+fileMd5+"&sentenceType="+2,
        success: function( data ) {
            //parse and return data
	    var string = "<div style='padding:10px;'>";
            for (i = 0; i < data.length; i++){
                var sentence=data[i].sentence;
		var scoreNodeTitle = data[i].scoreNodeTitle;
		var scoreMH = data[i].scoreMH;
		var scoreSig = data[i].scoreSig;
		var signatures = data[i].signatures;
		var musthaves = data[i].musthaves;
                //build html <div> and append
		string+="<br>";
		string+="<p>"+sentence+"<br>";
		string+="sscore1(Node title):&nbsp;"+scoreNodeTitle+"<br>";
		string+="score2(Musthaves):&nbsp;"+scoreMH+"<br>";
		string+="score3(signatures):&nbsp;"+scoreSig+"<br>";
		var total = scoreNodeTitle + scoreMH + scoreSig;
		string+="Total:&nbsp;"+total+"<br>";
		string+=" Musthaves:&nbsp;"+musthaves+"<br>";
		string+=" Signatures:&nbsp;"+signatures+"<br>";
		string+="</p><br>"; 
			
            }
	    string+="</div>";
	    $('#dialogSentences').children("div").remove();
	    $('#dialogSentences').append(string);
	    $('#dialogSentences').dialog( "open" );
        }
    });
}	
