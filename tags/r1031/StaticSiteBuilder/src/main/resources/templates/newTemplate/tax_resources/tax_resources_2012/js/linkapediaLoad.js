/*
 *Andres restrepo
 *
 *This file contain all configuration to plugins jquery in the pages
*/
function load(){
    //init tree menu carrousel
    // initialize scrollable
    $(".scrollable").scrollable();
    
    //load jmasonry...
    var container = $('#container');
    container.imagesLoaded( function(){
        container.masonry({
            itemSelector : '.box'
        });
    });
    $( "#dialog" ).dialog({
        autoOpen: false,
        show: "blind",
        hide: "explode",
        resizable: false,
        modal: true
    });

    $( "#dialogSentences" ).dialog({
        autoOpen: false,
        show: "blind",
        hide: "explode",
        resizable: false,
	maxWidth:500,
	maxHeight:500,
	height: 500,
	width:500,
        modal: true
    });

    $(".cInterest").click(function() {
        $( "#dialog" ).dialog( "open" );
        return false;
    });
    
    var $container = $('#container');
    $container.infinitescroll({
        navSelector  : '#page-nav',    // selector for the paged navigation 
        nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
        itemSelector : '.box',     // selector for all items you'll retrieve
        localMode:true,
        animate:true,
        loading: {
            finishedMsg: 'No more pages to load.',
            img: 'http://i.imgur.com/6RMhx.gif'
        }
    },
    // trigger Masonry as a callback
    function( newElements ) {
        // hide new items while they are loading
        var $newElems = $( newElements ).css({
            opacity: 0
        });
        // ensure that images load before adding to masonry layout
        $newElems.imagesLoaded(function(){
            // show elems now they're read
            $newElems.animate({
                opacity: 1
            });
            $container.masonry( 'appended', $newElems, true );
        });
    }
    ); 
    $('#container').css('visibility','visible');

}


