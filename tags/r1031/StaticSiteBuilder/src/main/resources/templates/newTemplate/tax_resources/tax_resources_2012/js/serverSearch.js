/*
 *Andres restrepo
 *
 *logic to use the search call database ajax
*/
var METHOD_AJAX = 'getNodes';
function configureSearch(host,siteName,taxonomy){
    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function( ul, items ) {
            var self = this,
            currentCategory = "";
            $.each( items, function( index, item ) {
                if ( item.category != currentCategory ) {
                    ul.append( "<li class='ui-autocomplete-category'><center>" + item.category + "</center></li>" );
                    currentCategory = item.category;
                }
                self._renderItem( ul, item );
            });
        },
        _renderItem : function( ul, item ) {
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<img width= 38px height= 38px class='iconSearch' src=\"/"+siteName+"/"+item.category+"/images/thumbnail/"+item.nodeid+".jpg\" onerror=\"this.src='/"+siteName+"/site_resources_2012/images/notImage.jpg';\" class='ui-state-default'>")
            .append( "<a class=\"itemSearch\"'><div class=\"search-nodeTitle\"> <b><i>" + item.node + "</i></b></div>" + "<div class=\"search-url\">"+item.uri + ".</div><div class=\"search-summary\"> ... </div></a>" )
            .appendTo( ul );
        }
		
    });

    var url = host+METHOD_AJAX;
    $( "#searchtxt" ).catcomplete({
        delay: 500,
        minLength: 3,
        source: function( request, response ) {
            $.ajax({
                url: url,
                dataType: "jsonp",
                data: 'text='+$("#searchtxt").val(),
                success: function( data ) {
                    //parse and return data
                    var d =[];
                    for (i = 0; i < data.length; i++){
                        var tax=data[i].taxonomy;
                        var node=data[i].node;
                        var uri =data[i].URI;
                        var nodeid = data[i].nodeid;
                        var uriAux = uri.substr(0,uri.lastIndexOf("/"));
                        uriAux = replaceAll(uriAux,"/","->");
                        d.push({
                            node:node,
                            nodeid:nodeid,
                            uri:uriAux,
                            uri1:uri,
                            category:tax
                        });
                    }
                    d=d.sort(compare);
                    console.log(d);
                    response(d);
                }
            });
        },
        focus: function( event, ui ) {
            $( "#searchtxt" ).val( ui.item.node );
            return false;
        },
        select: function( event, ui ) {
            $( "#searchtxt" ).val( ui.item.label );
            alert("Go to "+ui.item.uri1);
            window.location.href="/"+siteName+ui.item.uri1;
            return false;
        }
    });
    
    // Order by taxonomy
    function compare(a,b) {
        if (a.category < b.category )
            return -1;
        if (a.category  > b.category )
            return 1;
        return 0;
    }
    
    function replaceAll( text, s, r ){
        while (text.toString().indexOf(s) != -1)
            text = text.toString().replace(s,r);
        return text;
    }
}

