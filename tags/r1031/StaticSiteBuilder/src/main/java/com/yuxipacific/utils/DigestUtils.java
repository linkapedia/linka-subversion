package com.yuxipacific.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class DigestUtils {

    private static final Logger log = Logger.getLogger(DigestUtils.class);

    /**
     *
     * @param text
     * @return
     */
    public static String getMD5(String text) {
        log.debug("getMD5(String)");
        StringBuilder sb = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());
            byte byteData[] = md.digest();
            sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            log.error("The Algorithm that you are trying to use does not exist.", e);
        } catch (Exception e) {
            log.error("An exception has ocurred.", e);
        }
        return (sb == null) ? "" : sb.toString();
    }
}
