package com.yuxipacific.activemq.beans;

import com.intellisophic.builder.business.beans.PageObject;
import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.activemq.ActiveMQConnection;
import org.apache.log4j.Logger;

/**
 * Class to handle all the communication with the Queue.
 *
 * @author Alex
 */
public class QueueProducer {

    private static Logger log = Logger.getLogger(QueueProducer.class);

    /**
     * Method to send the messages to the classification queue.
     *
     * @param pageObject Package that contains the information of the job to be
     * classified.
     * @throws IllegalArgumentException
     * @throws JMSException
     * @throws NamingException
     */
    public static void sendMessage(PageObject pageObject) throws IllegalArgumentException, JMSException, NamingException {
        sendMessage(pageObject, false);
    }

    /**
     * Method to send the messages to the classification queue.
     *
     * @param pageObject Package that contains the information of the job to be
     * classified.
     * @param allowEmptyData boolean that allows the user to send an empty page
     * object not null.
     * @throws IllegalArgumentException
     * @throws JMSException
     * @throws NamingException
     */
    public static void sendMessage(PageObject pageObject, boolean allowEmptyData) throws IllegalArgumentException, JMSException, NamingException {
        log.debug("sendMessage(PageObject)");
        if (pageObject == null) {
            log.error("Error in sendMessage(PageObject) No Package provided to send to the queue.");
            throw new IllegalArgumentException("No Package provided to send to the queue");
        }
        if (!allowEmptyData && pageObject.isEmpty()) {
            log.error("Error in sendMessage(PageObject) Package data can not be empty or null.");
            throw new IllegalArgumentException("Package data can not be null.");
        }
        ActiveMQConnection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            InitialContext initCtx = new InitialContext();
            Context envContext = (Context) initCtx.lookup("java:comp/env");
            ConnectionFactory connectionFactory = (ConnectionFactory) envContext.lookup("jms/ConnectionFactory");
            connection = (ActiveMQConnection) connectionFactory.createConnection();
            connection.start();
            //Non transacted session.
            session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            producer = session.createProducer((Destination) envContext.lookup("jms/queue/PageCreationQueue"));
            //Disable persistence for this queue.
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            Message objMessage = session.createObjectMessage(pageObject);
            producer.send(objMessage);
        } catch (JMSException exception) {
            log.error("Error in sendMessage(PageObject)", exception);
            throw exception;
        } catch (NamingException exception) {
            log.error("Error in sendMessage(PageObject)", exception);
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (session != null) {
                session.close();
            }
            if (producer != null) {
                producer.close();
            }
        }
    }
}