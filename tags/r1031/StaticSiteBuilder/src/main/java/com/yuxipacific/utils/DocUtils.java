/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yuxipacific.utils;

import com.intellisophic.builder.business.beans.SignatureObject;
import com.intellisophic.builder.view.beans.SummaryObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class DocUtils {

    private static final Logger LOG = Logger.getLogger(DocUtils.class);
    private static final int DEFAULT_WORD_COUNT = 10;
    private static String summaryExcludedFiles = null;
    private static final ResourceBundle systemResource = ResourceBundle.getBundle("system/config");
    private static final int POINT_BY_NODE_TITLE = 50;
    private static final int POINT_BY_MUTHAVES = 30;
    private static final int POINT_BY_SIGNATURES = 10;

    static {
        summaryExcludedFiles = systemResource.getString("builder.excludefiles.extensions");
    }

    /**
     * Method to retrieve a phrase from a document that contains the give must
     * have.
     *
     * @param file File that represents the file to be processed.
     * @param mustHave String that represents a must have to be included in the
     * phrase
     * @param wordCount Number of words to be included.
     * @return A phrase containing the must have.
     */
    public static String getDocPhrase(String docContent, String mimeType, String mustHave, int wordCount) {
        LOG.debug("getDocPhrase(String, String, String, Int)");
        StringBuilder phrase = null;
        int wordIndex = 0;
        int firstPointIndex = -1;
        int lastPointIndex = -1;
        String[] excludeFileExtension;
        boolean ignoreFile = false;
        if (docContent != null) {
            //Set the lower case for the content to be able of compare.
            docContent = docContent.toLowerCase();

            if (mustHave != null) {
                //Set the lower case for the must have to be able of compare.
                mustHave = mustHave.toLowerCase();
            }

            //We need to avoid the search of must haves on a excluded file.
            //We need to check if we should calculate the summary for the document.
            if (summaryExcludedFiles != null) {
                excludeFileExtension = summaryExcludedFiles.split(",");
                //We need to check if we should calculate the summary for the document.
                for (String extension : excludeFileExtension) {
                    if (mimeType.contains(extension)) {
                        ignoreFile = true;
                        //Don't calculate the summary, intead replace the summary with a dummy summary.
                        phrase = new StringBuilder(systemResource.getString("builder.excludefiles." + extension + ".summary"));
                        break;
                    }
                }
            } else {
                LOG.info("No file extensions to ignore.");
            }

            if (!ignoreFile) {
                //find if we have the word in any part of the document.
                if (docContent != null && docContent.contains(mustHave)) {
                    String[] words = docContent.split(" ");
                    //Remove garbage from the words. (".",",")
                    words = StringUtils.sanitizeWords(words);
                    //We need to separate the musthaves in a array to search by parts.
                    String[] mustHaves = mustHave.split(" ");
                    for (int i = 0; i < words.length; i++) {
                        //find the index of the first part of the must have
                        if (words[i].equalsIgnoreCase(mustHaves[0])) {
                            //Find if the subsecuent parts are the other parts of the must have.
                            boolean isFullMustHave = true;
                            for (int j = 0; j < mustHaves.length; j++) {
                                isFullMustHave = isFullMustHave && words[i + j].equalsIgnoreCase(mustHaves[j]);
                            }
                            if (isFullMustHave) {
                                wordIndex = i;
                                break;
                            }
                        }
                    }

                    //Lets find a point before the musthave
                    for (int j = wordIndex; j > 0; j--) {
                        if (words[j].contains(".")) {
                            firstPointIndex = j + 1;
                            break;
                        }
                    }
                    //Lets find a point after the must have
                    for (int j = wordIndex; j < words.length; j++) {
                        if (words[j].contains(".")) {
                            lastPointIndex = j;
                            break;
                        }
                    }

                    //Now that we have the index we have to extract the words in a real phrase
                    if (firstPointIndex < 0 && wordIndex - wordCount > 0) {
                        //If no point was found, we use the wordCount.
                        firstPointIndex = (int) wordIndex - wordCount;
                    }
                    if (lastPointIndex < 0 && wordIndex + wordCount < words.length) {
                        //If no point was found, we use the wordCount.
                        lastPointIndex = (int) wordIndex + wordCount;
                    }

                    //If we found the first and the last point, but the range is to large.
                    if (firstPointIndex < wordIndex - (wordCount * 2)) {
                        firstPointIndex = (int) wordIndex - wordCount;
                    }

                    if (lastPointIndex > wordIndex + (wordCount * 2)) {
                        lastPointIndex = (int) wordIndex + wordCount;
                    }
                    if (firstPointIndex < 0) {
                        firstPointIndex = 0;
                    }
                    if (lastPointIndex > words.length) {
                        lastPointIndex = words.length;
                    }

                    //Extract the text.
                    phrase = new StringBuilder();
                    for (int j = firstPointIndex; j <= lastPointIndex; j++) {
                        phrase.append(words[j].trim()).append(" ");
                    }
                }
            }
        }
        return (phrase != null) ? phrase.toString() : null;
    }

    /**
     *
     * @param file
     * @param content
     * @param musthaves
     * @return
     */
    public static List<SummaryObject> getScores(File file, String nodeTitle, String[] musthaves, List<SignatureObject> signatures) throws Exception {
        LOG.info("TEST: get score to sentences");
        List<String> sentences = new ArrayList<String>();
        List<SummaryObject> listToReturn = new ArrayList<SummaryObject>();
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String line = "";
            //add sentences into array
            while ((line = br.readLine()) != null) {
                sentences.add(line);
            }

            //build score
            int numNodeTitle = 0;
            int numMusthaves = 0;
            int numSignatures = 0;
            SummaryObject summaryObject = null;
            Pattern p = null;
            Matcher ma = null;
            for (String sentence : sentences) {
                numNodeTitle = 0;
                numMusthaves = 0;
                numSignatures = 0;
                if (nodeTitle != null) {
                    p = Pattern.compile(nodeTitle, Pattern.CASE_INSENSITIVE);
                    ma = p.matcher(sentence);
                    //node title score
                    while (ma.find()) {
                        numNodeTitle++;
                    }
                }
                //must haves score
                if (musthaves != null) {
                    for (int i = 0; i < musthaves.length; i++) {
                        if (!musthaves[i].equalsIgnoreCase(nodeTitle)) {
                            p = Pattern.compile(musthaves[i], Pattern.CASE_INSENSITIVE);
                            ma = p.matcher(sentence);
                            while (ma.find()) {
                                numMusthaves++;
                            }
                        }
                    }
                }
                //signatures score
                if (signatures != null) {
                    for (SignatureObject sObj : signatures) {
                        String word = sObj.getWord();
                        if (word != null) {
                            p = Pattern.compile(word, Pattern.CASE_INSENSITIVE);
                            ma = p.matcher(sentence);
                            while (ma.find()) {
                                numSignatures++;
                            }
                        }
                    }
                }

                //calculate score
                summaryObject = new SummaryObject();
                summaryObject.setSentence(sentence);
                summaryObject.setScoreByNodeTitle(numNodeTitle * POINT_BY_NODE_TITLE);
                summaryObject.setScoreByMustHaves(numMusthaves * POINT_BY_MUTHAVES);
                summaryObject.setScoreBySignatures(numSignatures * POINT_BY_SIGNATURES);

                //todo remove this in production
                if (musthaves != null) {
                    summaryObject.setMustHaves(org.apache.commons.lang.StringUtils.join(musthaves, ","));
                }
                if (signatures != null) {
                    StringBuilder sb = new StringBuilder();
                    for (SignatureObject sObj : signatures) {
                        sb.append(sObj.getWord());
                        sb.append(",");
                    }
                    summaryObject.setSignatures(sb.toString());
                }
                listToReturn.add(summaryObject);
            }
        } finally {
            try {
                if (null != br) {
                    br.close();
                }
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e) {
                LOG.error("Error closing resources");
            }
        }
        return listToReturn;

    }

    /**
     * Method to retrieve a phrase from a document that contains the give must
     * have.
     *
     * @param file File that represents the file to be processed.
     * @param content Content of the file passed to avoid cache.
     * @param mustHave String that represents a must have to be included in the
     * phrase
     * @return A phrase containing the must have.
     */
    public static String getDocPhrase(File file, String content, String mustHave) {
        return getDocPhrase(content, FileUtils.getFileMimeType(file), mustHave, DEFAULT_WORD_COUNT);
    }
}
