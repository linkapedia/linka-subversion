package com.intellisophic.builder.data;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class MySQLHandler {

    private static final Logger log = Logger.getLogger(MySQLHandler.class);
    private static final ResourceBundle resource = ResourceBundle.getBundle("database/config");

    /**
     * Method that allows the user to check if the table for this corpusID
     * already Exists
     *
     * @param corpusID String containing the corpusID (Taxonomy ID)
     * @return
     */
    public static boolean tableExists(String ipAdress, Long corpusID) throws CommunicationsException, Exception {
        log.debug("tableExists(String, String)");
        Connection conn = null;
        ResultSet rs = null;
        String tableName;
        boolean tableExists = false;
        try {
            conn = MySQLConnectionFactory.createConnectionByIP(ipAdress);
            rs = getTables(conn, new String[]{"TABLE"});
            while (rs.next()) {
                tableName = rs.getString("TABLE_NAME");
                if (tableName.equalsIgnoreCase(resource.getString("database.table.prefix") + corpusID)) {
                    tableExists = true;
                }
            }
            rs.close();
            rs = null;
            conn.close();
            conn = null;
        } catch (CommunicationsException e) {
            log.error("Error in tableExists(String, String). " + e.getLocalizedMessage());
            tableExists = false;
            throw e;
        } catch (Exception e) {
            log.error("Error in tableExists(String, String). " + e.getLocalizedMessage());
            tableExists = false;
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException exception) {
                log.error("Error in tableExists(String, String) {Closing Resources}", exception);
            }
        }
        return tableExists;
    }

    /**
     * 
     * @param conn
     * @param types
     * @return 
     */
    private static ResultSet getTables(Connection conn, String[] types) {
        log.debug("getMetadata(Connection,String[])");
        DatabaseMetaData metadata;
        ResultSet rs = null;
        try {
            metadata = conn.getMetaData();
            rs = metadata.getTables(null, null, null, types);
        } catch (Exception e) {
            log.error("Error in getMetadata(Connection,String[])", e);
        }
        return rs;
    }
}