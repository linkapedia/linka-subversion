package com.intellisophic.builder.business;

import com.intellisophic.builder.business.beans.*;
import com.intellisophic.builder.business.context.AppServletContextListener;
import com.intellisophic.builder.business.interfaces.INodeBO;
import com.intellisophic.builder.business.threads.CallableProcess;
import com.intellisophic.builder.data.ClassifierDAO;
import com.intellisophic.builder.data.MySQLHandler;
import com.intellisophic.builder.data.NodeDAO;
import com.intellisophic.builder.data.interfaces.IClassifierDAO;
import com.intellisophic.builder.data.interfaces.INodeDAO;
import com.intellisophic.builder.data.pool.JDBCConnectionPool;
import com.intellisophic.builder.services.exceptions.DataNotFoundException;
import com.intellisophic.builder.services.mongo.MongoHandler;
import com.intellisophic.builder.view.beans.ViewNode;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.yuxipacific.activemq.beans.QueueProducer;
import com.yuxipacific.utils.FileUtils;
import com.yuxipacific.utils.StringUtils;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.util.*;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.jms.JMSException;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

/**
 * Class to handle the request for the builder.
 *
 * @version 1.1
 * @author Alex
 */
public class BuilderProcessor {

    private static final Logger log = Logger.getLogger(BuilderProcessor.class);
    private static final ResourceBundle databaseConfig = ResourceBundle.getBundle("database/config");
    private static final ResourceBundle RB = ResourceBundle.getBundle("system/config");
    private static Map<String, String> failedWork = null;
    private static BuilderProcessor builderProcessor = null;
    private INodeBO nodeBO = null;
    private static Map<String, JDBCConnectionPool> connectionPoolByIp = new HashMap<String, JDBCConnectionPool>();

    private BuilderProcessor() {
        failedWork = new HashMap<String, String>();
        nodeBO = new NodeBO();
    }

    /**
     *
     * @return BuilderProcessor
     * @see BuilderProcessor
     */
    public static BuilderProcessor getInstance() {
        if (builderProcessor == null) {
            builderProcessor = new BuilderProcessor();
        }
        return builderProcessor;
    }

    /**
     *  FIXME : we have a error in the synchronization when is executing a big taxonomy. 
     * @param corpusID
     * @param ipAddresses
     */
    public void processJobs(BuildRequest buildRequest) throws CommunicationsException, DataNotFoundException, Exception {
        log.debug("processJobs(BuildRequest)");
        List<Node> nodeList;
        List<String> _relevantIpAddress = new ArrayList<String>(buildRequest.getIpAddress().length);
        //We need to start thread for the diferent mysql databases for each IpAddress and NodeID
        //New approach to the page per node schema. 2012/01/24
        ExecutorService threadPool = Executors.newFixedThreadPool(5);
        CompletionService<Void> cservice = new ExecutorCompletionService<Void>(threadPool);

        try {
            String[] ipAddresses = buildRequest.getIpAddress();
            long corpusID = buildRequest.getCorpusID();
            //We need to discard those classifiers that dont contain information about the taxonomy.
            for (int i = 0; i < ipAddresses.length; i++) {
                String ipAddress = ipAddresses[i];
                try {
                    if (MySQLHandler.tableExists(ipAddress, corpusID)) {
                        log.info("Classifier data found in " + ipAddress + " for taxonomy {" + corpusID + "}");
                        _relevantIpAddress.add(ipAddress);
                    } else {
                        log.warn("Classifier data not found in " + ipAddress + " for taxonomy {" + corpusID + "}");
                    }
                } catch (CommunicationsException e) {
                    log.error("Classifier data not found. Classifier DB does not exist on host {" + ipAddress + "}");
                }
            }

            //If we got at least one classifier with data.
            if (_relevantIpAddress != null && _relevantIpAddress.size() > 0) {

                //We should create the connection pools for the classifiers.
                String dbURL;
                JDBCConnectionPool connectionPool;
                for (String ipAddress : _relevantIpAddress) {
                    dbURL = "jdbc:mysql://" + ipAddress + "/" + databaseConfig.getString("database.name");
                    connectionPool = new JDBCConnectionPool(databaseConfig.getString("database.driver"), dbURL, databaseConfig.getString("database.username"), databaseConfig.getString("database.password"));
                    connectionPoolByIp.put(ipAddress, connectionPool);
                }

                //We need to get all the nodes of the given taxonomy to know which nodes retrieve
                //so we initialize the NodeCache which returns the nodes loaded.
                nodeList = AppServletContextListener.getNodeCache().initialize(corpusID);

                //Load the musthaves for the taxonomy (CorpusID).
                AppServletContextListener.getMustHaveCache().initialize(corpusID);

                //Load the signatures for the taxonomy (CorpudID)
                AppServletContextListener.getSignatureCache().initialize(corpusID);

                //Load the xml menu using the data from all the classifiers.
                
                getTaxonomyMenuByCorpusID(corpusID, _relevantIpAddress);

                //Search for each node in all the classifiers.
                String[] relevantIpAddress = _relevantIpAddress.toArray(new String[0]);
                CallableProcess task;
                for (Node node : nodeList) {
                    task = new CallableProcess();
                    task.setCorpusID(corpusID);
                    task.setIpAddresses(relevantIpAddress);
                    task.setNodeID(node.getId());
                    cservice.submit(task);
                }

                Void result;
                for (int i = 0; i < nodeList.size(); i++) {
                    result = cservice.take().get();
                }
            } else {
                log.warn("This taxonomy {" + corpusID + "} has not been classified.");
                throw new DataNotFoundException("This taxonomy {" + corpusID + "} has not been classified.");
            }
        } catch (DataNotFoundException e) {
            log.error("Error in processJobs." + e.getLocalizedMessage());
            throw e;
        } catch (Exception e) {
            log.error("Error in processJobs.", e);
            throw e;
        } finally {
            threadPool.shutdown();
        }
    }

    /**
     *
     * @param classifierIP
     * @param reason
     */
    public static synchronized void reportFailedWork(String classifierIP, String reason) {
        log.debug("reportFailedWork(String,String)");
        if (BuilderProcessor.failedWork.containsKey(classifierIP)) {
            String previousReason = BuilderProcessor.failedWork.get(classifierIP);
            reason += previousReason;
        }
        log.info("Getting report for classifier: " + classifierIP);
        BuilderProcessor.failedWork.put(classifierIP, reason);
    }

    /**
     *
     * @param corpusID
     * @param viewNodeList
     * @return
     */
    public String getNodesAsXMLTreeMenu(List<ViewNode> viewNodeList) {
        log.debug("getNodesAsXMLTreeMenu()");
        StringBuilder sb = new StringBuilder();
        Map<Long, Node> parentList = new HashMap<Long, Node>();
        Node _tmpNode;
        try {
            sb.append("<root>");
            for (ViewNode viewNode : viewNodeList) {
                //Make the current node the parent.
                sb.append(viewNode.asXML());
                _tmpNode = AppServletContextListener.getNodeCache().getNodeByNodeID(viewNode.getNodeId());
                parentList.putAll(getParentsForNode(_tmpNode));
            }
            parentList = pruneDuplicatedNodes(parentList, viewNodeList);
            for (Node node : parentList.values()) {
                sb.append(node.asXML());
            }
            sb.append("</root>");
        } catch (Exception e) {
            log.error("Error in getNodesAsXMLTreeMenu().", e);
        }
        return sb.toString();
    }

    /**
     *
     * @param parents
     * @param viewNodeList
     * @return
     */
    public Map<Long, Node> pruneDuplicatedNodes(Map<Long, Node> parents, List<ViewNode> viewNodeList) {
        log.debug("pruneDuplicatedNodes(Map,List)");
        for (ViewNode viewNode : viewNodeList) {
            if (parents.containsKey(viewNode.getNodeId())) {
                parents.remove(viewNode.getNodeId());
            }
        }
        return parents;
    }

    public INodeBO getNodeBO() {
        return nodeBO;
    }

    public void setNodeBO(INodeBO nodeBO) {
        this.nodeBO = nodeBO;
    }

    /**
     *
     * @param currentNode
     * @return
     */
    private static Map<Long, Node> getParentsForNode(Node currentNode) {
        log.debug("getParentsForNode(Node)");
        Node parentNode = currentNode;
        Map<Long, Node> parentTillTheTop = new HashMap<Long, Node>();
        Long parentId;
        do {
            //Search the parent of the current node.
            if (parentNode != null) {
                parentId = parentNode.getParent().getId();
                parentTillTheTop.put(parentNode.getId(), parentNode);
            } else {
                parentId = -1L;
            }
            parentNode = AppServletContextListener.getNodeCache().getNodeByNodeID(parentNode.getParent().getId());
        } while (parentId != -1L);
        return parentTillTheTop;
    }

    /**
     * Method to get the menu information for all the nodes
     *
     * @param corpusID Taxonomy Identifier
     * @param ipAddresses IP Address of the Machines that contains relevant
     * information about the corpusID.
     * @throws Exception
     */
    private static void getTaxonomyMenuByCorpusID(Long corpusID, List<String> ipAddresses) throws Exception {
        log.debug("getTaxonomyMenuByCorpusID(Long, List<String>)");
        INodeDAO nodeDAO = new NodeDAO();
        IClassifierDAO classifierDAO;
        List<Node> nodeList;
        List<Long> nodeIDList;
        Map<Long, Long> _tmpNodeIDList = null;
        StringBuilder sb = new StringBuilder();
        try {
            //We need to retrieve the list of nodes classified by each classifier.
            JDBCConnectionPool connectionPool;
            Connection conn;
            for (String ipAddress : ipAddresses) {
                connectionPool = connectionPoolByIp.get(ipAddress);
                conn = connectionPool.checkOut();
                classifierDAO = new ClassifierDAO(conn);
                nodeIDList = classifierDAO.getClassifiedNodesByCorpusID(corpusID);
                if (nodeIDList != null) {
                    if (_tmpNodeIDList == null) {
                        _tmpNodeIDList = new HashMap<Long, Long>();
                    }
                    for (Long nodeID : nodeIDList) {
                        _tmpNodeIDList.put(nodeID, nodeID);
                    }
                } else {
                    log.warn("Data not found in host {" + ipAddress + "} for corpusID {" + corpusID + "}");
                }
                connectionPool.checkIn(conn);
            }

            //Get the nodes from the oracle db
            nodeList = nodeDAO.getNodesByCorpusID(corpusID);
            List<Node> nodesToPrune = new ArrayList<Node>();
            //Prune duplicated values from Oracle Node list using the result from the classifiers.
            for (Node node : nodeList) {
                if (!_tmpNodeIDList.containsKey(node.getId())) {
                    nodesToPrune.add(node);
                }
            }

            //Actually remove the items.
            nodeList.removeAll(nodesToPrune);

            //We need to add the parents of the remaining nodes.
            Map<Long, Node> missingNodes = new HashMap<Long, Node>();
            for (Node node : nodeList) {
                missingNodes.putAll(getParentsForNode(node));
            }

            Map<Long, Node> nodesToQueue = new HashMap<Long, Node>(missingNodes);
            //Fusion the parents with the nodes.
            for (Node node : nodeList) {
                if (missingNodes.containsKey(node.getId())) {
                    //Remove the node that are not in the classifier but it needs to be generated.
                    //Link nodes.
                    nodesToQueue.remove(node.getId());
                }
                missingNodes.put(node.getId(), node);
            }

            //Final Node List
            nodeList = new ArrayList(missingNodes.values());
            String tax = "";
            MongoObject mo;
            //Now nodeList only contains the nodes classified.
            if (nodeList != null && !nodeList.isEmpty()) {
                //Start to generate the xml for the menu.
                sb.append("<root>");
                //StringBuilder jsonString = new StringBuilder("[");
                tax = AppServletContextListener.getNodeCache().getRootNodeByCorpusID(corpusID).getTitle();
                List<MongoObject> listMongo = new ArrayList<MongoObject>();
                for (Node node : nodeList) {
                    node.setPathToParent(getPathToRootForNode(node));
                    sb.append(node.asXML());
                    //use to send using driver mongodb******************************
                    mo = new MongoObject();
                    mo.setTaxonomy(StringUtils.encodeFileName(tax));
                    mo.setNodeid(String.valueOf(node.getId()));
                    mo.setNode(node.getTitle());
                    mo.setUri(node.getPathToParent() + ".html");
                    listMongo.add(mo);

                }
                sb.append("</root>");
                //process to mongodb to this taxonomy

                MongoHandler.writeDataIntoMongoDBDriver(listMongo, StringUtils.encodeFileName(tax));
                MongoHandler.deleteDataImages(String.valueOf(corpusID));

            }
            //TODO send images folder into web server

            //----------------------------------
            //TODO create static information to interest circle
            try {
                createClassStatic();
            } catch (Exception e) {
                log.error("Error create interest circle class");
            }
            //----------------------------------



            //Set the menu for page creation access.
            //Load the xml menu using the data from all the classifiers.       
            
            String strTaxonomyMenu = sb.toString();
            if (log.isDebugEnabled()) {
                log.debug("Going to write to disk the menu generate for CorpusID{" + corpusID + "}");
                FileUtils.writeFile("generatedmenu_" + corpusID + ".xml", strTaxonomyMenu);
            }

            //Convert menu to XML Objects
            log.debug("Going to create the XML Object to store the menu");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document doc = saxBuilder.build(new ByteArrayInputStream(strTaxonomyMenu.getBytes()));
            
            //Set the menu where the consumer can use it
            TreeMenuBO.setFullTreeMenu(doc);


            //Send the bridge nodes to the queue.
            PageObject _tmpPageObject;
            for (Node node : nodesToQueue.values()) {
                log.debug("Node {" + node.getId() + "} ready for queue and page creation");
                _tmpPageObject = new PageObject();
                _tmpPageObject.setCorpusID(corpusID);
                _tmpPageObject.setNodeId(node.getId());
                _tmpPageObject.setTitle(node.getTitle());
                _tmpPageObject.setNodes(new ArrayList<CorpusObject>());
                log.debug("Sending " + _tmpPageObject + " to the queue for creation.");
                try {
                    QueueProducer.sendMessage(_tmpPageObject, true);
                } catch (IllegalArgumentException e) {
                    log.error("Error in processJobs, IllegalArgumentException. ", e);
                } catch (JMSException e) {
                    log.error("Error in processJobs, JMSException. ", e);
                } catch (NamingException e) {
                    log.error("Error in processJobs, NamingException. ", e);
                }
            }
        } catch (Exception e) {
            log.error("An exception ocurred. ", e);
            throw e;
        }
    }

    private static void createClassStatic() throws IOException, URISyntaxException {
        // get book
        InputStream is = null;
        List<String> list = new ArrayList<String>();
        Properties prop = new Properties();
        prop.load(BuilderProcessor.class.getClassLoader().getResourceAsStream("system/config.properties"));
        URL url = BuilderProcessor.class.getClassLoader().getResource("interest_circles.xlsx");
        File f = new File(url.toURI());
        try {
            is = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            System.err.println("Not found file: " + f.getAbsolutePath());
            return;
        }
        XSSFWorkbook book = new XSSFWorkbook(is);

        XSSFSheet sheet = book.getSheetAt(0);
        XSSFRow row = null;
        XSSFCell cell = null;
        InterestCircleInfo icInfo = null;
        for (int i = 3, z = sheet.getLastRowNum(); i < z; i++) {
            row = sheet.getRow(i);
            if (row != null) {
                icInfo = new InterestCircleInfo();
                for (int ii = 0, zz = row.getLastCellNum(); ii < zz; ii++) {
                    cell = row.getCell(ii);
                    if (cell != null) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        list.add(String.valueOf(cell.getStringCellValue()));
                    }
                }
                try {
                    if (!list.isEmpty()) {
                        icInfo.setCorpusId(list.get(0));
                        icInfo.setTaxonomyName(list.get(1));
                        icInfo.setInterestCircleName(list.get(2));
                        icInfo.setAuthor(list.get(3));
                        icInfo.setImageName(list.get(4));
                        icInfo.setFollowers(list.get(5));
                    }
                } catch (IndexOutOfBoundsException e) {
                    log.error("some parameters not found");
                } finally {
                    InterestCircleInfo.addElement(icInfo);
                    list.clear();
                }
            }
        }
    }

    /**
     * Method to get the path {URL} from the given node till the top of the
     * tree.
     *
     * @param currentNode Node to find the path to.
     * @return path separated by "/" from the root to the currentNode. Optimized
     * to use StringBuilder (03/29/2012)
     */
    private static String getPathToRootForNode(Node currentNode) {
        log.debug("getPathToRootForNode(Node)");
        StringBuilder strPathToRoot;
        if (currentNode == null) {
            return "";
        }
        strPathToRoot = new StringBuilder();
        //Make the current node the parent.
        Node parentNode = currentNode;
        Long parentId;

        log.debug("Going to get the hierarchy for node {" + currentNode.getId() + "}");

        strPathToRoot.insert(0, StringUtils.encodeFileName(parentNode.getTitle())).insert(0, "/");
        parentId = parentNode.getParent().getId();
        parentNode = AppServletContextListener.getNodeCache().getNodeByNodeID(parentId);

        while (parentNode != null) {
            strPathToRoot.insert(0, StringUtils.encodeFileName(parentNode.getTitle())).insert(0, "/");
            parentId = parentNode.getParent().getId();
            parentNode = AppServletContextListener.getNodeCache().getNodeByNodeID(parentId);

            if (parentNode != null) {
                log.debug("Parent found: " + parentNode.getId());
            } else {
                log.debug("Parent not found: This is the root node");
            }
        }
        return strPathToRoot.toString();
    }

    public static Map<String, JDBCConnectionPool> getConnectionPoolByIp() {
        return connectionPoolByIp;
    }

    public static void setConnectionPoolByIp(Map<String, JDBCConnectionPool> connectionPoolByIp) {
        BuilderProcessor.connectionPoolByIp = connectionPoolByIp;
    }
}
