package com.intellisophic.builder.business.beans;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Alex
 */
public class PageObject implements Serializable {

    /**
     * List of nodes containing the information to generate the page.
     */
    private List<CorpusObject> nodes;
    /**
     * Must Haves related to the node.
     */
    private String mustHaves;
    /**
     * Page Title: Extracted from the Node Title.
     */
    private String title;
    /**
     * Node ID related to the page.
     */
    private Long nodeId;
    /**
     * Taxonomy corpus identifier (this is the root node).
     */
    private Long corpusID;

    /**
     * Default constructor.
     */
    public PageObject() {
        this.mustHaves = null;
        this.title = null;
    }

    /**
     * Method to return the must haves of the page as a comma separated list.
     *
     * @return
     */
    public String getMustHaves() {
        return mustHaves;
    }

    public void setMustHaves(String mustHaves) {
        this.mustHaves = mustHaves;
    }

    public List<CorpusObject> getNodes() {
        return nodes;
    }

    public void setNodes(List<CorpusObject> nodes) {
        this.nodes = nodes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String nodeTitle) {
        this.title = nodeTitle;
    }

    public boolean isEmpty() {
        if (this.nodes != null) {
            return this.nodes.isEmpty();
        } else {
            return true;
        }
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getCorpusID() {
        return corpusID;
    }

    public void setCorpusID(Long corpusID) {
        this.corpusID = corpusID;
    }

    @Override
    public String toString() {
        return "PageObject{nodeId=" + nodeId + ", corpusID=" + corpusID + '}';
    }
    
    
}
