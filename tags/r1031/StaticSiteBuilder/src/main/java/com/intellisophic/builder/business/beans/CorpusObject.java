package com.intellisophic.builder.business.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Class to handle the results from each classifier db.
 *
 * @author Alex
 */
public class CorpusObject implements Serializable {

    private Long nodeID;
    private String URI;
    /**
     * The path to the downloaded file (download done by the classifier).
     */
    private String webCacheURI;
    private String docTitle;
    private String summary;
    private Date timestamp;
    private float score01;
    private float score02;
    private float score03;
    private float score04;
    private float score05;
    private float score06;
    private float score07;
    private float score08;
    private float score09;
    private float score10;
    private float score11;
    private float score12;
    /**
     * New classifier plug in score.
     *
     * @since 03/08/2012
     */
    private float pagerank;

    public CorpusObject() {
        this.nodeID = Long.MIN_VALUE;
        this.URI = null;
        this.docTitle = "";
        this.summary = "";
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public String getDocTitle() {
        return docTitle;
    }

    public void setDocTitle(String docTitle) {
        this.docTitle = docTitle;
    }

    public Long getNodeID() {
        return nodeID;
    }

    public void setNodeID(Long nodeID) {
        this.nodeID = nodeID;
    }

    public float getScore01() {
        return score01;
    }

    public void setScore01(float score01) {
        this.score01 = score01;
    }

    public float getScore02() {
        return score02;
    }

    public void setScore02(float score02) {
        this.score02 = score02;
    }

    public float getScore03() {
        return score03;
    }

    public void setScore03(float score03) {
        this.score03 = score03;
    }

    public float getScore04() {
        return score04;
    }

    public void setScore04(float score04) {
        this.score04 = score04;
    }

    public float getScore05() {
        return score05;
    }

    public void setScore05(float score05) {
        this.score05 = score05;
    }

    public float getScore06() {
        return score06;
    }

    public void setScore06(float score06) {
        this.score06 = score06;
    }

    public float getScore07() {
        return score07;
    }

    public void setScore07(float score07) {
        this.score07 = score07;
    }

    public float getScore08() {
        return score08;
    }

    public void setScore08(float score08) {
        this.score08 = score08;
    }

    public float getScore09() {
        return score09;
    }

    public void setScore09(float score09) {
        this.score09 = score09;
    }

    public float getScore10() {
        return score10;
    }

    public void setScore10(float score10) {
        this.score10 = score10;
    }

    public float getScore11() {
        return score11;
    }

    public void setScore11(float score11) {
        this.score11 = score11;
    }

    public float getScore12() {
        return score12;
    }

    public void setScore12(float score12) {
        this.score12 = score12;
    }

    public float getPagerank() {
        return pagerank;
    }

    public void setPagerank(float pagerank) {
        this.pagerank = pagerank;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getWebCacheURI() {
        return webCacheURI;
    }

    public void setWebCacheURI(String webCacheURI) {
        this.webCacheURI = webCacheURI;
    }
}
