package com.intellisophic.builder.services.rest;

import com.intellisophic.builder.business.BuilderProcessor;
import com.intellisophic.builder.business.beans.BuildRequest;
import com.intellisophic.builder.services.exceptions.DataNotFoundException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
@Path("/builder")
public class BuilderResource {

    private static transient final Logger log = Logger.getLogger(BuilderResource.class);
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String startBuilderProcess(JAXBElement<BuildRequest> request) {
        log.debug("startBuilderProcess()");

        StringBuilder sb;
        String statusMessage = "failed";
        try {
            BuildRequest buildRequest = request.getValue();
            if (buildRequest != null && buildRequest.getCorpusID() > 0 && buildRequest.getIpAddress() != null && buildRequest.getIpAddress().length > 0) {
                log.info("Build Static Site for Taxonomy: " + buildRequest.getCorpusID());
                log.info("Resource <CorpusID>: " + buildRequest.getCorpusID());
                String[] ipAddresses = buildRequest.getIpAddress();
                for (String ipAddress : ipAddresses) {
                    log.info("Going to use the classifier on Ip Address: " + ipAddress);
                }
                BuilderProcessor builderProcessor = BuilderProcessor.getInstance();
                builderProcessor.processJobs(buildRequest);
                statusMessage = "success";
            }
        } catch (DataNotFoundException e) {
            log.error("Error in startBuilderProcess(). ", e);
            statusMessage = e.getLocalizedMessage();
        } catch (Exception e) {
            log.error("Error in startBuilderProcess()", e);
        }
        sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append("<response>");
        sb.append(statusMessage);
        sb.append("</response>");
        return sb.toString();
    }
}