package com.intellisophic.builder.services.cache;

import com.intellisophic.builder.data.MustHavesDAO;
import com.intellisophic.builder.data.interfaces.IMustHaveDAO;
import com.intellisophic.builder.services.cache.jcs.JCSMustHaveCache;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class MustHaveCache {

    private static final Logger log = Logger.getLogger(MustHaveCache.class);
    private JCSMustHaveCache mustHaveCache = null;
    private boolean isFirstRun = true;

    /**
     * Class default constructor
     */
    public MustHaveCache() {
        mustHaveCache = new JCSMustHaveCache();
    }

    /**
     * Method to load the must haves from the Oracle DB into the Builder.
     *
     * @param corpusID
     * @return Map<Long,String>
     */
    public Map<Long, String> initialize(Long corpusID) {
        log.debug("initialize(Long)");
        IMustHaveDAO mustHaveDAO;
        Map<Long, String> mustHaveItems = null;
        //Check if the object is already in the cache.
        if (isFirstRun) {
            isFirstRun = false;
            log.info("Going to populate/refresh musthave cache for corpusID: " + corpusID);
            mustHaveDAO = new MustHavesDAO();
            try {
                mustHaveItems = mustHaveDAO.getMustHavesByCorpusID(corpusID);
                if (mustHaveItems != null && !mustHaveItems.isEmpty()) {
                    Set<Long> nodeIDs = mustHaveItems.keySet();
                    for (Long nodeID : nodeIDs) {
                        //Fill the cache with data.
                        mustHaveCache.addMustHave(nodeID, mustHaveItems.get(nodeID));
                    }
                }
            } catch (Exception e) {
                log.error("Error in initialize(Long).", e);
            }
        } else {
            log.info("No Need to refresh the cache, already in the latest version");
        }
        return mustHaveItems;
    }

    /**
     * Method to get the must haves of a given node associated with a nodeID.
     *
     * @param nodeID Node Identifier.
     * @return Must haves for the given node.
     */
    public String getMustHaveByNodeID(Long nodeID) {
        log.debug("getMustHaveByNodeID(Long)");
        return mustHaveCache.getMustHave(nodeID);
    }
}
