package com.intellisophic.builder.business.beans.comparators;

import com.intellisophic.builder.business.beans.CorpusObject;
import java.util.Comparator;

/**
 *
 * @author Xander Kno
 */
public class CorpusObjectComparator implements Comparator<CorpusObject> {

    private final int LESSTHAN = -1;
    private final int EQUAL = 0;
    private final int GREATERTHAN = 1;

    @Override
    public int compare(CorpusObject corpusObject1, CorpusObject corpusObject2) {
        float score1_1 = corpusObject1.getScore01();
        float score1_2 = corpusObject2.getScore01();
        float pagerank_1 = corpusObject1.getPagerank();
        float pagerank_2 = corpusObject2.getPagerank();
        if (score1_1 < score1_2) {
            return GREATERTHAN;
        } else if (score1_1 > score1_2) {
            return LESSTHAN;
        } else {
            if (pagerank_1 < pagerank_2) {
                return GREATERTHAN;
            } else if (pagerank_1 > pagerank_2) {
                return LESSTHAN;
            } else {
                return EQUAL;
            }
        }
    }
}
