package com.intellisophic.builder.services.cache;

import com.intellisophic.builder.business.NodeBO;
import com.intellisophic.builder.business.beans.Node;
import com.intellisophic.builder.business.interfaces.INodeBO;
import com.intellisophic.builder.services.cache.jcs.JCSNodeCache;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Class to handle the cache for the nodes of the Taxonomy. This cache will be populated when we got a request for a new corpusID.
 *
 * @author Alex
 * @version 2.0
 */
public class NodeCache {
    
    private static final Logger log = Logger.getLogger(NodeCache.class);
    private JCSNodeCache nodeCache = null;
    private INodeBO _nodeBO = null;
    private Map<Long, Long> nodesAlreadyCached = new HashMap<Long, Long>();

    /**
     * Constructor
     */
    public NodeCache() {
        nodeCache = new JCSNodeCache();
    }

    /**
     * Method to initialize the cache when a new CorpusID is received.
     *
     * @param corpusID
     */
    public List<Node> initialize(Long corpusID) {
        log.debug("initialize(Long)");
        List<Node> nodeList = null;
        try {
            //Check if we dont have the nodes for this corpus in the cache.
            if (!getNodesAlreadyCached().containsKey(corpusID)) {
                //Load the Nodes from the database.
                log.info("Going to add the Taxonomy with ID: " + corpusID + " to the NodeCache.");
                nodeList = getNodeBO().getNodesByCorpusID(corpusID);
                if (nodeList != null) {
                    log.info("Found " + nodeList.size() + " Nodes");
                    nodeCache.addNodes(nodeList);
                    for (Node node : nodeList) {
                        if (node != null && node.getParent() != null && node.getParent().getId() == -1L) {
                            log.info("Found root node for Taxonomy with corpusid = " + corpusID);
                            nodeCache.addRootNode(node);
                            break;
                        }
                    }
                } else {
                    log.warn("No nodes found for this corpus id {" + corpusID + "}");
                }
                getNodesAlreadyCached().put(corpusID, corpusID);
            } else {
                log.info("No need to query the db, the taxonomy with ID: " + corpusID + " is already in memory");
            }
        } catch (Exception e) {
            log.error("A exception ocurred while filling NodeCache.", e);
        }
        return nodeList;
    }

    /**
     * Method to return a Node using the IDs.
     *
     * @param nodeId Node ID
     * @return Node
     * @see {@code Node}
     */
    public Node getNodeByNodeID(Long nodeId) {
        log.debug("getNodeByNodeID(String)");
        return nodeCache.getNode(nodeId);
    }

    /**
     * Method to obtain the Root node of a given taxonomy.
     *
     * @param corpusID Taxonomy ID.
     * @return Node Taxonomy root node | null if not found.
     * @see {@code Node}
     */
    public Node getRootNodeByCorpusID(Long corpusID) {
        log.debug("getRootNodeByCorpusID(Long)");
        Node node = null;
        try {
            node = nodeCache.getRootNode(corpusID);
            if (null == node) {
                node = getNodeBO().getRootNodeByCorpusID(corpusID);
                nodeCache.addRootNode(node);
            }
        } catch (Exception ex) {
            log.error("An exception ocurred.", ex);
        }
        return node;
    }
    
    public void addRootNode(Node parentNode) {
        log.debug("addRootNode(Node)");
        nodeCache.addRootNode(parentNode);
    }

    /**
     * Method to return a {@code NodeBO} instance.
     *
     * @return {@code NodeBO}
     * @see NodeBO
     */
    private INodeBO getNodeBO() {
        if (_nodeBO == null) {
            _nodeBO = new NodeBO();
        }
        return _nodeBO;
    }
    
    public Map<Long, Long> getNodesAlreadyCached() {
        return nodesAlreadyCached;
    }
    
    public void setNodesAlreadyCached(Map<Long, Long> nodesAlreadyCached) {
        this.nodesAlreadyCached = nodesAlreadyCached;
    }
}
