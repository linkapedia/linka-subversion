package com.intellisophic.builder.services.mongo;

import com.intellisophic.builder.business.beans.MongoObject;
import com.intellisophic.builder.view.beans.ViewLink;
import com.intellisophic.builder.view.beans.ViewNode;
import com.intellisophic.builder.view.templates.PackImageProcess;
import com.mongodb.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 * connect to server node.js and put data writeDataIntoMongoDB(String) Note: to
 * use interface node.js you put correct key Note: interface node.js not good
 * charset encode connect to server mongodb and put
 * datawriteDataIntoMongoDBDriver(List<MongoObject>)
 *
 * @author andres
 */
public class MongoHandler {

    private static final ResourceBundle systemResource = ResourceBundle.getBundle("system/config");
    private static final Logger log = Logger.getLogger(MongoHandler.class);
    private static final String remoteMethod = "saveDocuments?";
    private static final String params[] = {"key=andres89"};

    /**
     * @param data
     * @throws MalformedURLException
     * @throws IOException
     */
    public static void writeDataIntoMongoDB(String data) throws MalformedURLException, IOException {
        log.debug("writeDataIntoMongoDB(String)");
        //build params
        OutputStreamWriter wr = null;
        BufferedReader br = null;
        try {
            StringBuilder paramsBuild = new StringBuilder("");
            for (int i = 0; i < params.length; i++) {
                paramsBuild.append(params[i]).append("&");
            }
            String parameters = paramsBuild.toString().substring(0, paramsBuild.toString().length() - 1);
            //build url
            URL url = new URL(systemResource.getString("nodejs.server") + remoteMethod + parameters);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setRequestProperty("Accept-Charset", "utf-8");
            con.setRequestProperty("Accept-Language", "en");
            con.setRequestProperty("Content-type", "application/json");

            //send data
            wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(data);
            wr.flush();
            wr.close();
            wr = null;
            // proccess response
            br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                log.debug(line);
            }
            br.close();
            br = null;
        } catch (Exception e) {
            log.error("An exception ocurred sending data to MongoDB.", e);
        } finally {
            try {
                if (wr != null) {
                    wr.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                log.error("An exception ocurred while closing resources.", e);
            }
        }
    }

    /**
     *
     * @param listMongo
     * @throws UnknownHostException
     */
    public static void writeDataIntoMongoDBDriver(List<MongoObject> listMongo, String taxonomy) {
        log.debug("mongodb: put data into mongo");
        Mongo mongoConn = null;
        DBCursor cur = null;
        try {
            mongoConn = new Mongo(systemResource.getString("mongo.host"), Integer.parseInt(systemResource.getString("mongo.port")));
            DB database = mongoConn.getDB("searchTest");
            DBCollection coll = database.getCollection("search");
            DBObject dbo;
            log.debug("Going to send " + listMongo.size() + " objects to the mongodb.");
            int i = 1;
            BasicDBObject document = new BasicDBObject();
            document.put("taxonomy", taxonomy);
            log.info("Delete data from mongodb: taxonomy," + taxonomy);
            coll.remove(document);
            for (MongoObject mo : listMongo) {
                dbo = new BasicDBObject();
                dbo.put("taxonomy", mo.getTaxonomy());
                dbo.put("nodeid", mo.getNodeid());
                dbo.put("node", mo.getNode());
                dbo.put("URI", mo.getUri());
                coll.insert(dbo);
                log.debug("Data for mongodb: " + i);
                i++;
            }
            log.debug("mongodb: process ok");
        } catch (Exception e) {
            log.error("An exception has ocurred. mongo insert method", e);
        } finally {
            if (cur != null) {
                log.debug("mongodb: close cursor");
                cur.close();
            }
            if (mongoConn != null) {
                log.debug("mongodb: close connection");
                mongoConn.close();
            }
        }

    }

    public static void deleteDataImages(String corpusId){
        log.debug("mongodb: delete data images info");
        Mongo mongoConn = null;
        try {
            mongoConn = new Mongo(systemResource.getString("mongo.host"), Integer.parseInt(systemResource.getString("mongo.port")));
            DB database = mongoConn.getDB("searchTest");
            DBCollection coll = database.getCollection("images");
            BasicDBObject document = new BasicDBObject();
            document.put("corpusId", corpusId);
            log.info("Delete data from mongodb: corpusId: " + corpusId);
            coll.remove(document);
            log.info("Delete data ok!");
        } catch (Exception e) {
            log.error("Error delete data from images.", e);
        } finally {
            if (mongoConn != null) {
                log.debug("mongodb: close connection");
                mongoConn.close();
            }
        }
    }

    public static void saveDataImages(List<ViewLink> links, ViewNode node) {
        log.debug("mongodb: put data into mongo");
        Mongo mongoConn = null;
        try {
            mongoConn = new Mongo(systemResource.getString("mongo.host"), Integer.parseInt(systemResource.getString("mongo.port")));
            DB database = mongoConn.getDB("searchTest");
            DBCollection coll = database.getCollection("images");
            DBObject dbo;
            DBObject dboList;
            int i = 0;
            dbo = new BasicDBObject();
            dbo.put("nodeId",String.valueOf(node.getNodeId()));
            dbo.put("corpusId",String.valueOf(node.getRootNode().getCorpusID()));
            //build the links
            BasicDBList dbl = new BasicDBList();
            for (ViewLink vl : links) {
                dboList = new BasicDBObject();
                dboList.put("nameMd5", vl.getLinkMd5());
                dboList.put("URI", vl.getUri());
                dboList.put("Path", vl.getPathFile());
                dbl.add(dboList);
                log.debug("inserting object image(MongoDB): " + i);
                i++;
            }
            dbo.put("links", dbl);
            coll.insert(dbo, WriteConcern.SAFE);
            log.debug("mongodb: process ok");
        } catch (Exception e) {
            log.error("An exception has ocurred. mongo insert method", e);
        } finally {
            if (mongoConn != null) {
                log.debug("mongodb: close connection");
                mongoConn.close();
            }
        }
    }
}
