package com.intellisophic.builder.services.ftp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos
 */
public class FtpSync extends TimerTask {

    private static final Logger LOG = Logger.getLogger(FtpSync.class);
    private static final ResourceBundle RB = ResourceBundle.getBundle("system/config");
    private static final String ZIPOUT;
    private static final String FTPTEMP;
    private ZipOutputStream zipBackBuffer;
    private ByteArrayOutputStream baoBuffer;
    private List<File> fileAppends;
    private String taxonomy;
    private Random random;

    static {
        ZIPOUT = RB.getString("zip.pages.output.final");
        FTPTEMP = RB.getString("ftp.sync.tempDir");
    }

    public FtpSync() {
        fileAppends = new ArrayList<File>();
        this.baoBuffer = new ByteArrayOutputStream();
        this.zipBackBuffer = new ZipOutputStream(this.baoBuffer);
        this.random = new Random(System.currentTimeMillis());
    }

    public void setTaxonomy(String taxonomy) {
        if (taxonomy == null) {
            return;
        }
        this.taxonomy = taxonomy;
    }

    public String getTaxonomy() {
        return this.taxonomy;
    }

    public synchronized void addNewEntry(File file, byte[] data) throws IOException {
        if (file == null) {
            LOG.info("Try to put null file in zip file");
            return;
        }
        LOG.info(this);
        LOG.info("new Entry File to Zip For Sync:\t" + file.getAbsolutePath());
        this.fileAppends.add(file);
        String path = file.getPath();
        ZipEntry zipEntry = new ZipEntry(path);
        this.zipBackBuffer.putNextEntry(zipEntry);
        this.zipBackBuffer.write(data);
        this.zipBackBuffer.closeEntry();
    }

    @Override
    public void run() {
        LOG.info(this);
        LOG.info("File For Sync Size:\t" + this.fileAppends.size());
        final List<File> fileAux = new ArrayList<File>(fileAppends);
        synchronized (this) {
            if (fileAux.isEmpty()) {
                return;
            }
            try {
                zipBackBuffer.flush();
                zipBackBuffer.close();
                byte[] data = this.baoBuffer.toByteArray();
                this.baoBuffer.reset();
                this.zipBackBuffer = new ZipOutputStream(baoBuffer);
                File file = new File(FTPTEMP + System.currentTimeMillis() + random.nextLong() + ".zip");
                FileOutputStream fao = new FileOutputStream(file);
                fao.write(data);
                fao.flush();
                fao.close();
                file.renameTo(new File(new File(ZIPOUT), file.getName()));
                fileAppends.clear();
            } catch (Exception ex) {
                LOG.info(ex);
            }
        }
    }

    @Override
    public String toString() {
        return "Sync Taxonomy: " + taxonomy;
    }
}
