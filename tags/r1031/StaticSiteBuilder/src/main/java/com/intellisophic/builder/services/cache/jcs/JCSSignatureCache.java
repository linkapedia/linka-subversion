package com.intellisophic.builder.services.cache.jcs;

import com.intellisophic.builder.business.beans.SignatureObject;
import java.util.List;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.access.exception.InvalidArgumentException;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class JCSSignatureCache {

    private static final Logger log = Logger.getLogger(JCSSignatureCache.class);
    private JCS cache = null;
    private static final String SIGNATURE_CACHE_IDENTIFIER = "signatureCache";

    /**
     * Class contructor used to initialize the cache.
     */
    public JCSSignatureCache() {
        log.debug("Signature Cache Constructor.");
        try {
            // Load the cache
            cache = JCS.getInstance(SIGNATURE_CACHE_IDENTIFIER);
            log.info("Signature Cache Initialized.");
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method that allows to add new Signature in the cache.
     *
     * @param nodeID Node Identifier.
     * @param sObjList List to signatures
     */
    public void addSignature(Long nodeID, List<SignatureObject> sObjList) {
        log.debug("addSignature(Long, String)");
        try {
            if ((nodeID != null) && (sObjList != null && !sObjList.isEmpty())) {
                cache.put(nodeID, sObjList);
            } else {
                throw new InvalidArgumentException("The NodeID is null or the list is null");
            }
        } catch (InvalidArgumentException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        } catch (CacheException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        }
    }

    /** 
     * Method to return the signatures for a given nodeID.
     *
     * @param nodeID Node Identifier.
     * @return Signatures of the given node, null if not found.
     */
    public List<SignatureObject> getSignatures(Long nodeID) {
        log.debug("getSignatures(Long)");
        return (List<SignatureObject>) cache.get(nodeID);
    }

    /**
     * Method to remove a Signatures from the cache for given node.
     *
     * @param nodeID Node Identifier.
     */
    public void removeSignatures(Long nodeID) {
        log.debug("getSignatures(Long)");
        try {
            cache.remove(nodeID);
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method to close the Cache when the application shutdown.
     */
    public static void shutdown() {
        CompositeCacheManager compositeCacheManager = CompositeCacheManager.getInstance();
        compositeCacheManager.release();
    }
}
