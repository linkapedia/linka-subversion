package org.linkapedia.images.bo;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.linkapedia.images.util.ImageCreation;
import org.linkapedia.images.util.ManageResourcesUtil;

import com.sun.image.codec.jpeg.ImageFormatException;

public class BuilderImage {

    private static final Logger log = Logger.getLogger(BuilderImage.class);
    private static final int DEFAULT_BOXH = 100;
    private static final int DEFAULT_BOXW = 100;
    public static final String SITE_IMAGE = "siteimage";
    public static final String DIGEST_IMAGE = "digestimage";
    public static final String MINI_IMAGE = "miniimage";
    public static final String NOCROP_IMAGE = "nocropimage";
    private BufferedImage image;
    private String type;

    public BuilderImage(BufferedImage image, String type) {
        this.image = image;
        this.type = type;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BufferedImage build() {
        if (!validate()) {
            return null;
        }
        int boxh;
        int boxw;
        boxh = getBoxH();
        boxw = getBoxW();
        return build(boxw, boxh);
    }

    public BufferedImage build(int boxw, int boxh) {
        if (!validate()) {
            return null;
        }
        if (this.type.equals(SITE_IMAGE)) {
            return buildSiteImage(boxw, boxh);
        } else if (this.type.equals(DIGEST_IMAGE)) {
            return buildDigestImage(boxw, boxh);
        } else if (this.type.equals(MINI_IMAGE)) {
            return buildMiniImage(boxw, boxh);
        } else if (this.type.equals(NOCROP_IMAGE)) {
            return buildNoCropImage(boxw, boxh);
        } else {
            log.error("Type unsupported");
            return null;
        }
    }

    private BufferedImage buildSiteImage(int boxw, int boxh) {
        log.debug("BuilderImage: buildSiteImage(int,int)");

        BufferedImage background = ImageCreation.scaleImage(image, boxh, boxw,
                false);
        background = ImageCreation.createLayerTransparent(background);

        BufferedImage mini = null;
        if ((this.image.getHeight() <= boxh) && (this.image.getWidth() <= boxw)) {
            mini = this.image;
        } else {
            mini = ImageCreation.scaleImage(image, boxh, boxw, true);
        }
        background = ImageCreation.createCompositeImage(background, mini,
                mini.getHeight(), mini.getWidth(),
                (boxw - mini.getWidth()) / 2, (boxh - mini.getHeight()) / 2);
        try {
            background = ImageCreation
                    .convertToJPG(background, new Float(0.25));
        } catch (ImageFormatException e) {
            log.error("Error building jpg image: ", e);
        } catch (IOException e) {
            log.error("Error building jpg image: ", e);
        }
        return background;
    }

    public BufferedImage buildDigestImage(int boxw, int boxh) {
        log.debug("BuilderImage: buildDigestImage(int,int)");
        BufferedImage dest = this.image;

        int width = dest.getWidth();
        int height = dest.getHeight();
        try {
            if (width > boxw && height > boxh) {
                if (width - boxw >= height - boxh) {
                    //scale height keeping aspect ratio
                    dest = ImageCreation.scaleImage(dest, boxh, width, true);
                } else {
                    //scale width keeping aspect ratio
                    dest = ImageCreation.scaleImage(dest, height, boxw, true);
                }
                width = dest.getWidth();
                height = dest.getHeight();

                if (width > boxw && height > boxh) {
                    // crop middle
                    dest = dest.getSubimage((width - boxw) / 2, (height - boxh) / 2,
                            boxw, boxh);
                }
                return dest;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Error creating image digest", e);
            return null;
        }


    }

    public BufferedImage buildNoCropImage(int boxw, int boxh) {
        log.debug("BuilderImage: buildNoCropImage(int,int)");
        BufferedImage dest = this.image;

        int width = dest.getWidth();
        int height = dest.getHeight();

        try {
            if (width > boxw && height > boxh) {
                if (width - boxw >= height - boxh) {
                    //scale height keeping aspect ratio
                    dest = ImageCreation.scaleImage(dest, boxh, width, true);
                } else {
                    //scale width keeping aspect ratio
                    dest = ImageCreation.scaleImage(dest, height, boxw, true);
                }
                width = dest.getWidth();
                height = dest.getHeight();

                if (width > boxw && height > boxh) {
                    // crop middle
                    dest = dest.getSubimage((width - boxw) / 2, (height - boxh) / 2,
                            boxw, boxh);
                }
                return dest;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Error creating image digest", e);
            return null;
        }
    }

    public boolean checkNoCropImage(BufferedImage image, int boxw, int boxh) {
        int width = image.getWidth();
        int height = image.getHeight();

        if (width > boxw && height > boxh) {
            return true;
        }
        return false;
    }

    public BufferedImage buildMiniImage(int boxw, int boxh) {
        log.debug("BuilderImage: buildMiniImage(int,int)");
        BufferedImage dest = this.image;

        dest = ImageCreation.scaleImage(dest, boxh, boxh, true);

        return dest;

    }

    /**
     * utilities
     */
    private Integer getBoxH() {
        Integer boxh = DEFAULT_BOXH;
        try {
            boxh = Integer.parseInt(ManageResourcesUtil.getProperties()
                    .getProperty("org.linkapedia.images." + this.type + ".boxh",
                    String.valueOf(DEFAULT_BOXH)));
        } catch (NumberFormatException e) {
            log.error("Error getting parameter BOXH from properties ", e);
        }
        return boxh;
    }

    private int getBoxW() {
        Integer boxw = DEFAULT_BOXW;
        try {
            boxw = Integer.parseInt(ManageResourcesUtil.getProperties()
                    .getProperty("org.linkapedia.images." + this.type + ".boxw",
                    String.valueOf(DEFAULT_BOXW)));
        } catch (NumberFormatException e) {
            log.error("Error getting parameter BOXH from properties ", e);
        }
        return boxw;
    }

    private boolean validate() {
        if (this.image == null) {
            log.error("Please set a image, can't be null");
            return false;
        }
        return true;
    }
}