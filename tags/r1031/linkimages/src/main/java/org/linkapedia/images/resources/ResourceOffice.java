package org.linkapedia.images.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.poi.hslf.HSLFSlideShow;
import org.apache.poi.hslf.usermodel.PictureData;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.wrapper.PackImages;

/**
 * @author andres
 * @verion 0.4 BETA
 * 
 * get images from office documents
 */
public class ResourceOffice {

	private static final Logger LOG = Logger.getLogger(ResourceOffice.class);

	
	/**
	 * get images .docx
	 * 
	 * @param is
	 * @param pf
	 * @return
	 * @throws Exception
	 */
	public static PackImages getImagesDocX(InputStream is, int max)
			throws Exception {
		LOG.debug("getImagesDocX(InputStream, PreFilter)");
		if (is == null) {
			throw new InvalidParameterException("Some parameters are NULL");
		}
		long n = System.currentTimeMillis();
		int num = Math.abs(max);
		XWPFDocument doc = null;
		List<XWPFPictureData> images = null;
		PackImages list = null;
		doc = new XWPFDocument(is);
		images = doc.getAllPackagePictures();
		BufferedImage bi = null;
		list = new PackImages();
		ImageInfo ii = null;
		int i = 1;
		for (XWPFPictureData pic : images) {
			try {
				ii = new ImageInfo();
				bi = ImageIO.read(new ByteArrayInputStream(pic.getData()));
				ii.setId(UUID.randomUUID().toString() + System.nanoTime());
				ii.setImage(bi);
				list.add(ii);
				LOG.info("get image from docx ok!");
			} catch (Exception e) {
				LOG.error("get image from docx error! " + e.getMessage());
			}
			// get a limit number images
			if (num != 0) {
				if (i == num)
					break;
			}
			i++;
		}
		LOG.info("TIME IN SECONDS (DOCX IMAGES): " + (System.currentTimeMillis() - n) / 1000);
		return list;
	}

	/**
	 * get images .doc
	 * 
	 * @param is
	 * @param pf
	 * @return
	 * @throws Exception
	 */
	public static PackImages getImagesDoc(InputStream is, int max)
			throws Exception {
		LOG.debug("getImagesDoc(InputStream, PreFilter)");
		if (is == null) {
			throw new InvalidParameterException("Some parameters are NULL");
		}
		long n = System.currentTimeMillis();
		int num = Math.abs(max);
		HWPFDocument doc = null;
		List<Picture> images = null;
		PackImages list = null;

		doc = new HWPFDocument(is);
		images = doc.getPicturesTable().getAllPictures();
		BufferedImage bi = null;
		list = new PackImages();
		ImageInfo ii = null;
		int i = 1;
		for (Picture pic : images) {
			try {
				ii = new ImageInfo();
				bi = ImageIO.read(new ByteArrayInputStream(pic.getContent()));
				ii.setId(UUID.randomUUID().toString() + System.nanoTime());
				ii.setImage(bi);
				list.add(ii);
				LOG.info("get image from doc ok! ");
			} catch (Exception e) {
				LOG.error("get image from doc error! " + e.getMessage());
			}
			// get a limit number images
			if (num != 0) {
				if (i == num)
					break;
			}
			i++;
		}
		LOG.info("TIME IN SECONDS (DOC IMAGES): " + (System.currentTimeMillis() - n) / 1000);
		return list;

	}

	/**
	 * get images .ppt
	 * 
	 * @param is
	 * @param pf
	 * @return
	 * @throws Exception
	 */
	public static PackImages getImagesPpt(InputStream is, int max)
			throws Exception {
		LOG.debug("getImagesPpt(InputStream, PreFilter)");
		if (is == null) {
			throw new InvalidParameterException("Some parameters are NULL");
		}
		long n = System.currentTimeMillis();
		int num = Math.abs(max);
		SlideShow ppt = null;
		PictureData[] images = null;
		PackImages list = null;

		ppt = new SlideShow(new HSLFSlideShow(is));
		images = ppt.getPictureData();
		BufferedImage bi = null;
		list = new PackImages();
		ImageInfo ii = null;
		int i = 1;
		for (int k = 0; k < images.length; k++) {
			try {
				ii = new ImageInfo();
				bi = ImageIO
						.read(new ByteArrayInputStream(images[k].getData()));
				ii.setId(UUID.randomUUID().toString() + System.nanoTime());
				ii.setImage(bi);
				list.add(ii);
				LOG.info("get image from ppt ok!");
			} catch (Exception e) {
				LOG.error("get image from ppt error! " + e.getMessage());
			}
			// get a limit number images
			if (num != 0) {
				if (i == num)
					break;
			}
			i++;
		}
		LOG.info("TIME IN SECONDS (PPT IMAGES): " + (System.currentTimeMillis() - n) / 1000);
		return list;
	}

	/**
	 * get images to .pptx
	 * @param is
	 * @param pf
	 * @return
	 * @throws Exception
	 */
	public static PackImages getImagesPptX(InputStream is, int max)
			throws Exception {
		LOG.debug("getImagesPptX(InputStream, PreFilter)");
		if (is == null) {
			throw new InvalidParameterException("Some parameters are NULL");
		}
		long n = System.currentTimeMillis();
		int num = Math.abs(max);
		XMLSlideShow ppt = null;
		List<XSLFPictureData> images = null;
		PackImages list = null;

		ppt = new XMLSlideShow(is);
		images = ppt.getAllPictures();
		BufferedImage bi = null;
		list = new PackImages();
		ImageInfo ii = null;
		int i = 1;
		for (XSLFPictureData pic : images) {
			try {
				ii = new ImageInfo();
				bi = ImageIO.read(new ByteArrayInputStream(pic.getData()));
				ii.setId(UUID.randomUUID().toString() + System.nanoTime());
				ii.setImage(bi);
				list.add(ii);
				LOG.info("get image from pptx ok!");
			} catch (Exception e) {
				LOG.error("get image from pptx error! " + e.getMessage());
			}
			// get a limit number images
			if (num != 0) {
				if (i == num)
					break;
			}
			i++;
		}
		LOG.info("TIME IN SECONDS (PPTX IMAGES): " + (System.currentTimeMillis() - n) / 1000);
		return list;

	}

	/**
	 * get inputstream from a url to use the other methods
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static InputStream getInputStrem(URL url) throws Exception {
		LOG.debug("getInputStrem(URL)");
		InputStream is = null;
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			is = con.getInputStream();
			return is;
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}

	}

	/**
	 * get inputstream from a file to use the other methods
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static InputStream getInputStrem(File file) throws Exception {
		LOG.debug("getInputStrem(File)");
		FileInputStream fis = new FileInputStream(file);
		return fis;
	}

}