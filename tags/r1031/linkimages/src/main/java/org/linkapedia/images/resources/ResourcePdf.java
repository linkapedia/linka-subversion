package org.linkapedia.images.resources;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.linkapedia.images.beans.ImageInfo;

/**
 * @author andres
 * @verion 0.3 BETA
 * 
 *         get image to pdf documents
 */
public class ResourcePdf {

	private static final Logger LOG = Logger.getLogger(ResourcePdf.class);

	/**
	 * create image to first page pdf
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static ImageInfo getImage(URL url) throws Exception {
		LOG.debug("getImagePDF(URL)");
		PDDocument doc = PDDocument.load(url);
		return getImageDoc(doc);
	}

	/**
	 * create image to first page pdf
	 * 
	 * @param is
	 * @return
	 * @throws Exception
	 */
	public static ImageInfo getImage(InputStream is) throws Exception {
		LOG.debug("getImagePDF(InputStream)");
		PDDocument doc = PDDocument.load(is);
		return getImageDoc(doc);
	}

	/**
	 * create image to first page pdf
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static ImageInfo getImage(File file) throws Exception {
		LOG.debug("getImagePDF(File)");
		PDDocument doc = PDDocument.load(file);
		return getImageDoc(doc);
	}

	/**
	 * get image from a document
	 * 
	 * @param doc
	 * @return
	 * @throws Exception
	 */
	private static ImageInfo getImageDoc(PDDocument doc) throws Exception {
		LOG.debug("getImageDoc(PDDocument)");
		long n = System.currentTimeMillis();
		PDPage page = new PDPage();
		page = (PDPage) doc.getDocumentCatalog().getAllPages().get(0);
		BufferedImage bi;
		bi = page.convertToImage();
		ImageInfo ii = new ImageInfo();
		// set a unique id to each image
		ii.setId(UUID.randomUUID().toString() + System.nanoTime());
		ii.setImage(bi);
		LOG.info("TIME IN SECONDS (PDF IMAGE): "
				+ (System.currentTimeMillis() - n) / 1000);
		return ii;
	}
}