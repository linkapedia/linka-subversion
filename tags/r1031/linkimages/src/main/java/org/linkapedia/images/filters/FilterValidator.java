package org.linkapedia.images.filters;

import java.awt.image.BufferedImage;

import org.apache.log4j.Logger;

public class FilterValidator {

	private static final Logger log = Logger.getLogger(ImageSelection.class);

	private FilterImages filter;
	private BufferedImage image;

	public FilterValidator(FilterImages filter) {
		this.filter = filter;
	}

	/**
	 * verify if pass all conditions
	 * 
	 * @return
	 */
	public boolean filter(BufferedImage image) {
		if (image == null) {
			log.error("Error filter: image is null");
			return false;
		}
		this.image = image;
		return bySize() && byRatio();
	}

	/**
	 * filter by size
	 * 
	 * @return
	 */
	private boolean bySize() {
		int height = this.image.getHeight();
		int width = this.image.getWidth();
		boolean maxHeight = (filter.getMax_height() == 0 ? true
				: (height > filter.getMax_height() ? false : true));
		boolean minHeight = (filter.getMin_height() == 0 ? true
				: (height < filter.getMin_height() ? false : true));
		boolean maxWidth = (filter.getMax_width() == 0 ? true : (width > filter
				.getMax_width() ? false : true));
		boolean minWidth = (filter.getMin_width() == 0 ? true : (width < filter
				.getMin_width() ? false : true));
		// compare if the image pass filter by size
		if (maxHeight && minHeight && maxWidth && minWidth) {
			return true;
		}
		return false;
	}

	/**
	 * filer by ratio
	 * 
	 * @return
	 */
	private boolean byRatio() {
		int height = this.image.getHeight();
		int width = this.image.getWidth();
		double div = (double) width / (double) height;
		double divRatio = 0;
		double divRatio1 = 0;
		boolean ratioCompare = (filter.getRatiox() != 0 && filter.getRatioy() != 0);
		if (ratioCompare) {
			divRatio = filter.getRatiox() / filter.getRatioy();
			divRatio1 = filter.getRatioy() / filter.getRatiox();
			if ((div >= divRatio1) && (div <= divRatio)) {
				return true;
			}
		}
		return false;
	}
}
