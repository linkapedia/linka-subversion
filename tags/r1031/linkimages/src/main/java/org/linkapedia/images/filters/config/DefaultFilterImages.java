package org.linkapedia.images.filters.config;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.linkapedia.images.filters.FilterImages;
import org.linkapedia.images.util.ManageResourcesUtil;

public class DefaultFilterImages extends FilterImages {

	private static final Logger log = Logger
			.getLogger(DefaultFilterImages.class);

	public DefaultFilterImages() {
		Properties prop = ManageResourcesUtil.getProperties();
		if (prop == null) {
			log.error("Error creating DefaultFilterImages, properties not load");
			return;
		}
		// set the parameters with the properties

		double ratiox;
		double ratioy;

		int max_height;
		int min_height;
		int max_width;
		int min_width;

		try {
			ratiox = Double.parseDouble(prop
					.getProperty("org.linkapedia.images.default.ratiox"));
			setRatiox(ratiox);
			ratioy = Double.parseDouble(prop
					.getProperty("org.linkapedia.images.default.ratioy"));
			setRatioy(ratioy);
			max_height = Integer.parseInt(prop
					.getProperty("org.linkapedia.images.default.max_height"));
			setMax_height(max_height);
			min_height = Integer.parseInt(prop
					.getProperty("org.linkapedia.images.default.min_height"));
			setMax_width(min_height);
			max_width = Integer.parseInt(prop
					.getProperty("org.linkapedia.images.default.max_width"));
			setMax_width(max_width);
			min_width = Integer.parseInt(prop
					.getProperty("org.linkapedia.images.default.min_width"));
			setMin_width(min_width);
		} catch (NumberFormatException e) {
			log.error("Error setting DefaultFilterImages", e);
		}catch(Exception e){
			log.error("Error not found the property");
		}

	}

}