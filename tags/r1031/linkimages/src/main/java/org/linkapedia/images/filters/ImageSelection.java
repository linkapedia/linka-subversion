package org.linkapedia.images.filters;

import java.awt.image.BufferedImage;
import java.security.InvalidParameterException;
import org.apache.log4j.Logger;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.wrapper.PackImages;

/**
 * @author andres
 * @verion 0.4 BETA
 * 
 * some method to manage filters
 */
public class ImageSelection {

	private static final Logger LOG = Logger.getLogger(ImageSelection.class);

	/**
	 * get a better Buffered apply FilterImages
	 * 
	 * @param list
	 * @param filter
	 * @return
	 */
	public static ImageInfo getBetterImageFilter(PackImages list, FilterImages filter)
			throws Exception {
		LOG.debug("getBetterImageFilter(PackImages list, FilterImages filter)");
		if (list == null || filter == null) {
			throw new InvalidParameterException("List or filter = NULL");
		}
		int height = 0;
		int width = 0;
		double divRatio = 0;
		double divRatio1 = 0;
		PackImages bettersImages = new PackImages();
		boolean ratioCompare = (filter.getRatiox() != 0 && filter
				.getRatioy() != 0);
		if (ratioCompare) {
			divRatio = filter.getRatiox() / filter.getRatioy();
			divRatio1 = filter.getRatioy() / filter.getRatiox();
		}
		BufferedImage image = null;
		for (ImageInfo ii : list) {
			try {
				image = ii.getImage();
				if (image == null)
					continue;
				height = image.getHeight();
				width = image.getWidth();
				double div = (double) width / (double) height;
				boolean maxHeight = (filter.getMax_height() == 0 ? true
						: (height > filter.getMax_height() ? false : true));
				boolean minHeight = (filter.getMin_height() == 0 ? true
						: (height < filter.getMin_height() ? false : true));
				boolean maxWidth = (filter.getMax_width() == 0 ? true
						: (width > filter.getMax_width() ? false : true));
				boolean minWidth = (filter.getMin_width() == 0 ? true
						: (width < filter.getMin_width() ? false : true));
				// compare if the image pass filter by size
				if (maxHeight && minHeight && maxWidth && minWidth) {
					// compare the better image ratio
					if (ratioCompare) {
						if ((div >= divRatio1) && (div <= divRatio)) {
							bettersImages.add(ii);
						}
					} else {
						bettersImages.add(ii);
					}
				}
			} catch (Exception e) {
				LOG.error("Error getting image: " + e.getMessage());
			}
		}
		return getLargestImage(bettersImages);
	}

	/**
	 * get the image more big
	 * 
	 * @param pi
	 * @return
	 */
	private static ImageInfo getLargestImage(PackImages pi) {
		LOG.info("--Get largest image from the list--");
		ImageInfo better = null;
		boolean init = true;
		int sum = 0;
		int may = 0;
		for (ImageInfo ii : pi) {
			sum = ii.getImage().getHeight() + ii.getImage().getWidth();
			if (init) {
				better = ii;
				may = sum;
				init = false;
			} else {
				if (sum > may) {
					better = ii;
					may = sum;
				}
			}
		}
		return better;
	}

}