package org.linkapedia.images.wrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.linkapedia.images.beans.ImageInfo;

/**
 * @author andres
 * @verion 0.1 BETA
 * 
 *         manage a list<ImageInfo> wrapper
 */

public class PackImages implements List<ImageInfo>, Cloneable {

	private List<ImageInfo> elements;

	public PackImages() {
		this.elements = new ArrayList<ImageInfo>();
	}

	public PackImages(Collection<ImageInfo> elements) {
		this.elements = new ArrayList<ImageInfo>(elements);
	}

	public PackImages(List<ImageInfo> elements) {
		this.elements = elements;
	}

	public PackImages(ImageInfo... elements) {
		this(Arrays.asList(elements));
	}

	@Override
	public PackImages clone() {
		List<ImageInfo> contents = new ArrayList<ImageInfo>();
		for (ImageInfo e : elements)
			contents.add(e.clone());
		return new PackImages(contents);
	}

	// list methods
	public int size() {
		return elements.size();
	}

	public boolean isEmpty() {
		return elements.isEmpty();
	}

	public boolean contains(Object o) {
		return elements.contains(o);
	}

	public Iterator<ImageInfo> iterator() {
		return elements.iterator();
	}

	public Object[] toArray() {
		return elements.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return elements.toArray(a);
	}

	public boolean add(ImageInfo e) {
		return elements.add(e);
	}

	public boolean remove(Object o) {
		return elements.remove(o);
	}

	public boolean containsAll(Collection<?> c) {
		return elements.containsAll(c);
	}

	public boolean addAll(Collection<? extends ImageInfo> c) {
		return elements.addAll(c);
	}

	public boolean addAll(int index, Collection<? extends ImageInfo> c) {
		return elements.addAll(c);
	}

	public boolean removeAll(Collection<?> c) {
		return elements.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return elements.retainAll(c);
	}

	public void clear() {
		elements.clear();
	}

	public ImageInfo get(int index) {
		return elements.get(index);
	}

	public ImageInfo remove(int index) {
		return elements.remove(index);
	}

	public int indexOf(Object o) {
		return elements.indexOf(o);
	}

	public int lastIndexOf(Object o) {
		return elements.lastIndexOf(o);
	}

	public ListIterator<ImageInfo> listIterator() {
		return elements.listIterator();
	}

	public ListIterator<ImageInfo> listIterator(int index) {
		return elements.listIterator(index);
	}

	public List<ImageInfo> subList(int fromIndex, int toIndex) {
		return elements.subList(fromIndex, toIndex);
	}

	public ImageInfo set(int index, ImageInfo element) {
		return elements.set(index, element);
	}

	public void add(int index, ImageInfo element) {
		elements.add(index, element);
	}
}