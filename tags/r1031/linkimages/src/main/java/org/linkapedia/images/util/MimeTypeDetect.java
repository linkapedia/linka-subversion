/**
 * @author andres
 * @verion 0.3 BETA
 * 
 * File type detector
 */
package org.linkapedia.images.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.apache.tika.Tika;

/**
 * 
 * FIXME: detect type from a url not a file in this moment only detect the type
 * for a file
 * 
 */
public class MimeTypeDetect {

	private static final Logger LOG = Logger.getLogger(MimeTypeDetect.class);

	// files supported
	private final String TYPE_PDF = "application/pdf";
	private final String TYPE_DOC = "application/msword";
	private final String TYPE_DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
	private final String TYPE_PPT = "application/vnd.ms-powerpoint";
	private final String TYPE_PPTX = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
	private final String UNKNOWN_TYPE = "unknown";

	private String type;

	public MimeTypeDetect(byte[] b) throws IOException {
		InputStream is = new ByteArrayInputStream(b);
		try {
			processStream(is);
		} finally {
			is.close();
		}
	}

	public MimeTypeDetect(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		try {
			processStream(is);
		} finally {
			is.close();
		}
	}

	public MimeTypeDetect(InputStream is) {
		processStream(is);
	}

	private void processStream(InputStream is) {
		LOG.debug("MimeTypeDetect: getMimeType(InputStream)");
		Tika tika = new Tika();
		String type = "";
		try {
			type = tika.detect(is);
			LOG.info("InputStream Type: " + type);
		} catch (Exception e) {
			LOG.error("I can't get MimeType");
		}
		this.type = type;
	}

	public String getType() {
		if (this.type.equals(TYPE_PDF)) {
			this.type = "PDF";
		} else if (this.type.equals(TYPE_DOC)) {
			this.type = "DOC";
		} else if (this.type.equals(TYPE_DOCX)) {
			this.type = "DOCX";
		} else if (this.type.equals(TYPE_PPT)) {
			this.type = "PPT";
		} else if (this.type.equals(TYPE_PPTX)) {
			this.type = "PPTX";
		} else {
			this.type = UNKNOWN_TYPE;
		}
		return type;
	}
}