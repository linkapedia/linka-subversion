/**
 * @author andres
 * @verion 0.1 BETA
 */
package org.linkapedia.images.filters;

public class FilterImages implements Cloneable {

	private double ratiox = 0;
	private double ratioy = 0;
	private int max_height = 0;
	private int min_height = 0;
	private int max_width = 0;
	private int min_width = 0;

	public double getRatiox() {
		return ratiox;
	}

	public void setRatiox(double ratiox) {
		this.ratiox = ratiox;
	}

	public double getRatioy() {
		return ratioy;
	}

	public void setRatioy(double ratioy) {
		this.ratioy = ratioy;
	}

	public int getMax_height() {
		return max_height;
	}

	public void setMax_height(int max_height) {
		this.max_height = max_height;
	}

	public int getMin_height() {
		return min_height;
	}

	public void setMin_height(int min_height) {
		this.min_height = min_height;
	}

	public int getMax_width() {
		return max_width;
	}

	public void setMax_width(int max_width) {
		this.max_width = max_width;
	}

	public int getMin_width() {
		return min_width;
	}

	public void setMin_width(int min_width) {
		this.min_width = min_width;
	}

}