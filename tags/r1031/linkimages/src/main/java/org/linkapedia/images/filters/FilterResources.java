/**
 * @author andres
 * @verion 0.1 BETA
 */
package org.linkapedia.images.filters;

import java.util.ArrayList;
import java.util.List;

public class FilterResources {

	// num max of images to return
	private int num = 0;
	// these words must be within <img ...
	private List<String> wordsIn = new ArrayList<String>();
	// these words should not be within <img ...
	private List<String> wordsOut = new ArrayList<String>();

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public List<String> getWordsIn() {
		return wordsIn;
	}

	public void setWordsIn(List<String> wordsIn) {
		this.wordsIn = wordsIn;
	}

	public List<String> getWordsOut() {
		return wordsOut;
	}

	public void setWordsOut(List<String> wordsOut) {
		this.wordsOut = wordsOut;
	}

}