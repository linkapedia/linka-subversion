# there is not a lot of brains in the job ticket parser, so be exact
# use protected memory
ProtectMem=0
DebugTxt=0
# folder where we have the files we want to parse
Sentences=/home/norb/Documents/Docs/sentences
# output folder
Output=/home/norb/Documents/Docs/sentences/output
# data files
UnfilteredNodes=/home/norb/Documents/Docs/Input/unfiltered_nodes.data
Words=/home/norb/Documents/Docs/Input/words.data
State=/home/norb/Documents/Docs/Input/state.aho
ListNode=/home/norb/Documents/Docs/Input/list_node.data
NodeMustHave=/home/norb/Documents/Docs/Input/node_musthave.data
NodeSigVec=/home/norb/Documents/Docs/Input/node_sig_vector.data
	