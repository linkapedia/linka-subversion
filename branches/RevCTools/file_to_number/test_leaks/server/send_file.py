#!/usr/bin/python
import socket
import struct
import sys
TITLE_PACKET=1
DOCUMENT_PACKET=2
END_PACKET=3
def read_file(file_name):
	fd=open(file_name,"r")
	data=fd.readlines()
	fd.close()
	return data
def send_file_new(lines,sock):
	line=lines[0]
	size=len(line)
	sock.send(struct.pack("IB3s",size,TITLE_PACKET,"car"))
	sock.send(line)
	for line in lines[1:]:
		size=len(line)
		sock.send(struct.pack("IB3s",size,DOCUMENT_PACKET,"car"))
		sock.send(line)
	sock.send(struct.pack("IB3s",0,END_PACKET,"car"))
def send_file_old(lines,sock):
	for line in lines:
		sock.send(line)
	sock.shutdown(socket.SHUT_WR)
def read_socket(sock):
	fd=sock.makefile()
	data=fd.read()
	print data
	sock.close()
	fd.close()
def send_file(file_name,host,port,old):
	try:
		sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		sock.connect((host,port))
		data=read_file(file_name)
		if(old):
			send_file_old(data,sock)
		else:
			send_file_new(data,sock)
		read_socket(sock)
	except Exception as e:
		print "Error Send File %s"%file_name
		sock.close()
def main(file_name,host,port,old=False):
	send_file(file_name,host,port,old)
if __name__=='__main__':
	if(len(sys.argv)<4):
		print "please input all commands for example"
		print "./send_file localhost 5000 test_sample.txt"
	elif(len(sys.argv)==4):
		main(sys.argv[3],sys.argv[1],int(sys.argv[2]))
	else:
		main(sys.argv[3],sys.argv[1],int(sys.argv[2]),True)
