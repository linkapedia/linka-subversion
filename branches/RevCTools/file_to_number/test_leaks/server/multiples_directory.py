#!/usr/bin/python
import sys
import send_directory
import threading
import os
def send_multiple_directory(host,port,directory):
	thread_list=[]
	for dir_path,dir_names,file_names in os.walk(directory):
		for dir_name in dir_names:
			print "init work in directory %s"%directory+dir_name
			thread_work=threading.Thread(group=None,target=send_directory.send_directory,name=None,args=(host,port,directory+dir_name+"/"),kwargs={})
			thread_work.start()
			thread_list.append(thread_work)
		break
if __name__=='__main__':
	if(len(sys.argv)<4):
		print "please input all option example ./multiples_directory.py localhost 5000 ~/test_data/"
	else:
		send_multiple_directory(sys.argv[1],int(sys.argv[2]),sys.argv[3])

