#!/usr/bin/python
import re
import sys
def main():
	if(len(sys.argv)<5):
		print "quantity arguments invalid"
		print "example ./buildsmallphrase.py words.txt phrases.txt document.txt new_phrases_file.txt"
		return
	word_file_name=sys.argv[1]
	phrase_file_name=sys.argv[2]
	document_file_name=sys.argv[3]
	new_phrase_file_name=sys.argv[4]
	print "Maping Word File "+word_file_name
	map_word=build_map_word_from_word_file(word_file_name)
	print "find word in document "+document_file_name
	words=find_match_words_from_document_file(document_file_name,map_word)
	print "build new phrase file "+new_phrase_file_name
	build_phrase_from_words_include_in_phrases_file(phrase_file_name,words,new_phrase_file_name)
def build_phrase_from_words_include_in_phrases_file(phrase_file_name,words,new_phrase_file_name):
	phrase_file=open(phrase_file_name)
	phrases=[]
	for line in phrase_file.readlines():
		phrase=line.rstrip().split(",")
		if(is_word_in_phrase(phrase,words)):
			phrases.append(tuple(phrase))
	phrase_file.close()
	save_phrase_file_from_phrases(new_phrase_file_name,set(phrases))
def save_phrase_file_from_phrases(phrase_file,phrases):
	phrase_file=open(phrase_file,"w")
	for phrase in phrases:
		phrase_file.write(",".join(phrase))
		phrase_file.write("\r\n")
	phrase_file.close()
def is_word_in_phrase(words_phrase,words):	
	for word_phrase in words_phrase:
		if word_phrase in words:			
			return True
	return False
def find_match_words_from_document_file(document_file_name,map_word):
	fd=open(document_file_name)
	regex_words=re.compile(r"\b\w*\b")
	words_found=[]
	for line in fd.readlines():
		line=line.rstrip()
		words=regex_words.findall(line)
		words_found=words_found+find_match_word(words,map_word)
	fd.close()	
	return set(words_found)
def find_match_word(words,map_word):
	words_found=[]
	for word in words:		
		if word in map_word:
			words_found.append(map_word[word])
	return words_found
def build_map_word_from_word_file(word_file_name):
	fd=open(word_file_name)
	map_word={}
	for line in fd.readlines():
		word_data=line.rstrip().split(",")
		if(len(word_data)!=2):
			continue
		map_word[word_data[1]]=word_data[0]
	fd.close()
	return map_word
if __name__=="__main__":
	main()
