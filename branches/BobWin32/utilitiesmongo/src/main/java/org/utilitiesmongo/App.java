package org.utilitiesmongo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.utilitiesmongo.bean.PackMongo;
import org.utilitiesmongo.util.ParameterUtil;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

/**
 * Start
 * 
 */
public class App {
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(App.class);

	public static void main(String[] args) {
		LOG.info("Start!");
		Properties prop = null;
		String taxonomy = null;
		String path = null;
		boolean help = false;
		if (args.length == 0) {
			LOG.error("missing parameters");
			return;
		}
		prop = new Properties();
		try {
			prop.load(App.class.getClassLoader().getResourceAsStream(
					"system/config.properties"));
			help = parseOptions(prop, args);
			taxonomy = prop.getProperty("TAXONOMY");
			path = prop.getProperty("PATH");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (help) {
			printHelp();
			return;
		}
		if ((taxonomy == null) || (path == null)) {
			LOG.error("missing parameters");
			return;
		}

		SAXBuilder sb = null;
		Document doc = null;
		List<PackMongo> listP = null;
		try {
			sb = new SAXBuilder();
			doc = sb.build(new File(path));
			listP = createPackMongo(doc, taxonomy);
		} catch (JDOMException e) {
			LOG.error("JDOMException: " + e.getMessage());
		} catch (IOException e) {
			LOG.error("IOException: " + e.getMessage());
		}
		insertDataInMongo(prop, listP);
		LOG.info("FINISHED!");
	}

	private static void printHelp() {
		LOG.info("-t: Taxonomy name: folder name in the web site");
		LOG.info("-dfile: Path xml file");
	}

	private static boolean parseOptions(Properties properties, String[] args) {
		String option = null;
		int i = 0;
		while (i < args.length) {
			option = ParameterUtil.OPTIONMAPPING.get(args[i]);
			if (option != null) {
				if (option.equals("HELP")) {
					return true;
				}
				if (args[i].startsWith("--")) {
					properties.put(option, option);
					i++;
				} else if ((i + 1) < args.length) {
					properties.put(option, args[i + 1]);
					i += 2;
				} else {
					LOG.error("Please specify param " + args[i]);
					return false;
				}
			} else {
				LOG.error("paremeters not found:" + args[i]);
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private static List<PackMongo> createPackMongo(Document doc, String taxonomy) {
		List<PackMongo> list = new ArrayList<PackMongo>();
		Element e = doc.getRootElement();
		List<Element> elements = e.getChildren();
		String nodeId = null;
		String nodeTitle = null;
		String path = null;
		PackMongo pack = null;
		for (Element el : elements) {
			pack = new PackMongo();
			nodeId = el.getAttributeValue("id");
			nodeTitle = el.getChild("content").getChildText("name");
			path = el.getChild("content").getChild("name")
					.getAttributeValue("href");
			pack.setNodeid(nodeId);
			pack.setNodeTitle(nodeTitle);
			pack.setPath(path);
			pack.setTaxonomy(taxonomy);
			list.add(pack);
		}
		return list;

	}

	private static void insertDataInMongo(Properties prop, List<PackMongo> listP) {
		LOG.debug("mongodb: put data into mongo");
		try {
			Mongo mongoConn = new Mongo(
					prop.getProperty(ParameterUtil.MONGO_HOST),
					Integer.parseInt(prop.getProperty(ParameterUtil.MONGO_PORT)));
			DB database = mongoConn.getDB("searchTest");
			DBCollection coll = database.getCollection("search");
			List<DBObject> list = new ArrayList<DBObject>();
			DBCursor cur;
			DBObject dbo;
			LOG.debug("Going to send " + listP.size()
					+ " objects to the queue.");
			for (PackMongo mo : listP) {
				dbo = new BasicDBObject();
				dbo.put("taxonomy", mo.getTaxonomy());
				dbo.put("nodeid", mo.getNodeid());
				dbo.put("node", mo.getNodeTitle());
				dbo.put("URI", mo.getPath());
				cur = coll.find(dbo);
				if (!cur.hasNext()) {
					list.add(dbo);
				}
			}
			coll.insert(list);
			LOG.debug("mongodb: data saved ok");
			if (mongoConn != null) {
				LOG.debug("mongodb: close connection");
				mongoConn.close();
			}
		} catch (Exception e) {
			LOG.error("An exception has ocurred. "+ e.getMessage());
		}
		
	}
}
