/*
Created			2/23/2001
Modified		2/23/2001
Project		
Model		
Company		
Author		
Version		
Database		Oracle 8 
*/

Create table Signature (
	NodeId Number(9,0) NOT NULL,
	SignatureWord Varchar2(30),
	SignatureOccurences Number(5,0)
)  ;

Create table Document (
	DocumentId Number(7,0) NOT NULL UNIQUE ,
	DocTitle Varchar2(256),
	DocURL Varchar2(256) NOT NULL UNIQUE ,
	DocumentSummary Varchar2(4000),
	DateLastFound Date,
Primary Key (DocumentId)
)  ;

Create table NodeDocument (
	NodeId Number(9,0) NOT NULL,
	DocumentId Number(7,0) NOT NULL,
	DocumentType Number(2,0),
	Score1 Real,
	Score2 Real,
	Score3 Real,
	DateScored Date,
Primary Key (NodeId,DocumentId)
)  ;

Create table DocumentChannel (
	DocumentId Number(7,0) NOT NULL,
	Channelid Number(4,0) NOT NULL,
	NodeId Number(9,0) NOT NULL
)  ;

Create table Channel (
	Channelid Number(4,0) NOT NULL UNIQUE ,
	ChannelName Varchar2(30),
Primary Key (Channelid)
)  ;

Create table Book (
	BookId Number(4,0) NOT NULL,
	Channelid Number(4,0) NOT NULL,
	ChannelBookId Number(4,0),
	PublisherName Varchar2(50),
	AuthorName Varchar2(50),
	BookTitle Varchar2(50),
	ISBN Varchar2(15),
	SubTitle Varchar2(500),
	Synopsis Varchar2(4000),
	Copyright Varchar2(256),
	Category Varchar2(50),
	Biography Long,
Primary Key (BookId,Channelid)
)  ;

Create table UpdateLog (
	UpdateLogId Number(13,0) NOT NULL UNIQUE ,
	NodeId Number(9,0),
	ActionCode Number(3,0) NOT NULL,
	HostName Varchar2(20) NOT NULL,
	UpdateDate Date NOT NULL,
Primary Key (UpdateLogId)
)  ;

Create table Node (
	NodeId Number(9,0) NOT NULL,
	CorpusId Number(4,0) NOT NULL,
	NodeTitle Varchar2(256),
	NodeSize Number(9,0),
	DateScanned Date,
	DateUpdated Date,
Primary Key (NodeId)
)  ;

Create table FileNode (
	NodeId Number(9,0) NOT NULL,
	FileId Number(9,0) NOT NULL,
	Section Number(7,0),
Primary Key (NodeId,FileId)
)  ;

Create table NodeBook (
	NodeId Number(9,0) NOT NULL,
	Channelid Number(4,0) NOT NULL,
	BookId Number(4,0) NOT NULL,
	Score1 Real,
	Score2 Real,
	Score3 Real,
	DateScored Date,
Primary Key (NodeId,Channelid,BookId)
)  ;

Create table CorpusFile (
	FileId Number(9,0) NOT NULL,
	CorpusId Number(4,0) NOT NULL,
	Path Varchar2(256),
Primary Key (FileId)
)  ;

Create table BlackList (
	BlackListId Number(4,0) NOT NULL,
	ClientId Number(4,0) NOT NULL,
	CorpusId Number(4,0) NOT NULL,
	EditorId Number(4,0) NOT NULL,
	URLChunk Varchar2(256) NOT NULL,
	NodeId Number(9,0),
	DateCreated Date,
Primary Key (BlackListId,ClientId,CorpusId,EditorId)
)  ;

Create table Hierarchy (
	HierarchyId Number(4,0) NOT NULL,
	CorpusId Number(4,0) NOT NULL,
	NodeId Number(9,0) NOT NULL,
	ParentId Number(9,0),
	ParentRel Number(3,0),
	NodeIndexWithinParent Number(3,0)
)  ;

Create table Client (
	ClientId Number(4,0) NOT NULL,
	CorpusId Number(4,0) NOT NULL,
	HierarchyId Number(4,0) NOT NULL,
	ClientName Varchar2(30),
Primary Key (ClientId)
)  ;

Create table Editor (
	EditorId Number(4,0) NOT NULL,
	PublisherId Number(4,0) NOT NULL,
	Username Varchar2(15) NOT NULL UNIQUE ,
	Password Varchar2(10) NOT NULL,
	FirstName Varchar2(10),
	LastName Varchar2(15),
	Email Varchar2(25),
	AccessLevel Number(1,0) Default 1 NOT NULL,
	DateCreated Date NOT NULL,
Primary Key (EditorId)
)  ;

Create table Publisher (
	PublisherId Number(4,0) NOT NULL UNIQUE ,
	Publisher Varchar2(50),
	Contact Varchar2(50),
	Address Varchar2(256),
	Phone Varchar2(15),
	Email Varchar2(50),
	PublisherURL Varchar2(256),
Primary Key (PublisherId)
)  ;

Create table Corpus (
	CorpusId Number(4,0) NOT NULL UNIQUE ,
	PublisherId Number(4,0) NOT NULL,
	Version Varchar2(50),
	ISBN Varchar2(15),
	CorpusShortName Varchar2(50),
	Corpus_Name Varchar2(256),
	PubCorpURL Varchar2(256),
	CorpusDesc Varchar2(256),
	LicenseId Number(4,0),
	CorpusImageIds Varchar2(256),
	CopyrightDate Date,
	Date_Last_Copyright_Notice Date,
	Date_Last_Update Date,
	AuthorId Number(4,0),
	CopyrightNotice Varchar2(256),
	CorpusActive Number(3,0),
Primary Key (CorpusId)
)  ;

Create table EditorLog (
	LogId Number(8,0) NOT NULL,
	EditorId Number(4,0) NOT NULL,
	ActionCode Number(2,0) NOT NULL,
	Action_String Varchar2(30),
	NodeId Number(6,0),
	ActionDate Date NOT NULL,
Primary Key (LogId)
)  ;

Create table Checkout (
	NodeId Number(9,0) NOT NULL UNIQUE,
	CorpusId Number(4,0),
	Hostname Varchar2(20),
	Checkoutdate Date
);

Create Index Checko ON Checkout (NodeId);
Create Index DataMineELog  ON EditorLog (ActionCode,Action_String,ActionDate,NodeId)  ;
Create Index HI on Hierarchy (HierarchyId, NodeId);
Create Index NSIG on Signature (NodeId);

alter table Book add   foreign key(Channelid) references Channel (Channelid)  on delete cascade;
alter table NodeBook add   foreign key(BookId,Channelid) references Book (BookId,Channelid)  on delete cascade;
alter table NodeBook add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table UpdateLog add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table FileNode add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table Signature add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table NodeDocument add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table DocumentChannel add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table Hierarchy add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table BlackList add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
alter table BlackList add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
alter table FileNode add   foreign key(FileId) references CorpusFile (FileId)  on delete cascade;
alter table Client add   foreign key(HierarchyId) references Hierarchy (HierarchyId)  on delete cascade;
alter table Client add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
alter table EditorLog add   foreign key(EditorId) references Editor (EditorId)  on delete cascade;
alter table Editor add   foreign key(PublisherId) references Publisher (PublisherId)  on delete cascade;
alter table Corpus add   foreign key(PublisherId) references Publisher (PublisherId)  on delete cascade;
alter table Node add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
alter table CorpusFile add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
alter table Hierarchy add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;

/* Update trigger for Channel */

CREATE Trigger tu_Channel after update   
of Channelid
on  Channel 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Book update when parent Channel changed */
     if (:old_upd.Channelid != :new_upd.Channelid)  then
     	begin
     	update Book
     	set 	Channelid = :new_upd.Channelid
     	where 	Book.Channelid = :old_upd.Channelid ;
     	end;
     end if;
end;
/
/* Update trigger for Book */

CREATE Trigger tu_Book after update   
of BookId,Channelid
on  Book 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child NodeBook update when parent Book changed */
     if (:old_upd.Channelid != :new_upd.Channelid OR 
        	:old_upd.BookId != :new_upd.BookId)  then
     	begin
     	update NodeBook
     	set 	Channelid = :new_upd.Channelid,
          	BookId = :new_upd.BookId
     	where 	NodeBook.Channelid = :old_upd.Channelid AND 
            		NodeBook.BookId = :old_upd.BookId ;
     	end;
     end if;
end;
/
/* Update trigger for Node */

CREATE Trigger tu_Node after update   
of NodeId,CorpusId
on  Node 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child NodeBook update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update NodeBook
     	set 	NodeId = :new_upd.NodeId
     	where 	NodeBook.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child UpdateLog update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update UpdateLog
     	set 	NodeId = :new_upd.NodeId
     	where 	UpdateLog.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child FileNode update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update FileNode
     	set 	NodeId = :new_upd.NodeId
     	where 	FileNode.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child Signature update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update Signature
     	set 	NodeId = :new_upd.NodeId
     	where 	Signature.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child NodeDocument update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update NodeDocument
     	set 	NodeId = :new_upd.NodeId
     	where 	NodeDocument.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child DocumentChannel update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update DocumentChannel
     	set 	NodeId = :new_upd.NodeId
     	where 	DocumentChannel.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child Hierarchy update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update Hierarchy
     	set 	NodeId = :new_upd.NodeId
     	where 	Hierarchy.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child BlackList update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update BlackList
     	set 	NodeId = :new_upd.NodeId
     	where 	BlackList.NodeId = :old_upd.NodeId ;
     	end;
     end if;
end;
/
/* Update trigger for CorpusFile */

CREATE Trigger tu_CorpusFile after update   
of FileId,CorpusId
on  CorpusFile 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child FileNode update when parent CorpusFile changed */
     if (:old_upd.FileId != :new_upd.FileId)  then
     	begin
     	update FileNode
     	set 	FileId = :new_upd.FileId
     	where 	FileNode.FileId = :old_upd.FileId ;
     	end;
     end if;
end;
/
/* Update trigger for Hierarchy */

CREATE Trigger tu_Hierarchy after update   
of HierarchyId,CorpusId,NodeId
on  Hierarchy 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Client update when parent Hierarchy changed */
     if (:old_upd.HierarchyId != :new_upd.HierarchyId)  then
     	begin
     	update Client
     	set 	HierarchyId = :new_upd.HierarchyId
     	where 	Client.HierarchyId = :old_upd.HierarchyId ;
     	end;
     end if;
end;
/
/* Update trigger for Editor */

CREATE Trigger tu_Editor after update   
of EditorId,PublisherId
on  Editor 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child EditorLog update when parent Editor changed */
     if (:old_upd.EditorId != :new_upd.EditorId)  then
     	begin
     	update EditorLog
     	set 	EditorId = :new_upd.EditorId
     	where 	EditorLog.EditorId = :old_upd.EditorId ;
     	end;
     end if;
end;
/
/* Update trigger for Publisher */

CREATE Trigger tu_Publisher after update   
of PublisherId
on  Publisher 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Editor update when parent Publisher changed */
     if (:old_upd.PublisherId != :new_upd.PublisherId)  then
     	begin
     	update Editor
     	set 	PublisherId = :new_upd.PublisherId
     	where 	Editor.PublisherId = :old_upd.PublisherId ;
     	end;
     end if;
     
     /* cascade child Corpus update when parent Publisher changed */
     if (:old_upd.PublisherId != :new_upd.PublisherId)  then
     	begin
     	update Corpus
     	set 	PublisherId = :new_upd.PublisherId
     	where 	Corpus.PublisherId = :old_upd.PublisherId ;
     	end;
     end if;
end;
/
/* Update trigger for Corpus */

CREATE Trigger tu_Corpus after update   
of CorpusId,PublisherId
on  Corpus 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Node update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update Node
     	set 	CorpusId = :new_upd.CorpusId
     	where 	Node.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child CorpusFile update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update CorpusFile
     	set 	CorpusId = :new_upd.CorpusId
     	where 	CorpusFile.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child Hierarchy update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update Hierarchy
     	set 	CorpusId = :new_upd.CorpusId
     	where 	Hierarchy.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
end;
/

create sequence document_seq start with 1;

create trigger doc_trigger before insert on document for each row when (new.documentid is null)
begin select document_seq.nextval into :new.documentid from dual;
end;
/ 
create sequence ulog_seq start with 1;

create trigger ulog_trigger before insert on updatelog for each row when (new.updatelogid is null)
begin select ulog_seq.nextval into :new.updatelogid from dual;
end;
/
create sequence blacklist_seq start with 1;

create trigger bl_trigger before insert on blacklist for each row when (new.blacklistid is null)
begin select blacklist_seq.nextval into :new.blacklistid from dual;
end;
/
create sequence editor_seq start with 1;

create trigger ed_trigger before insert on editor for each row when (new.editorid is null)
begin select editor_seq.nextval into :new.editorid from dual;
end;
/
create sequence elog_seq start with 1;

create trigger elog_trigger before insert on editorlog for each row when (new.logid is null)
begin select elog_seq.nextval into :new.logid from dual;
end;
/
