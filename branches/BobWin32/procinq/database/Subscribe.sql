CREATE OR REPLACE PROCEDURE Subscribe (
	EMAIL_IN IN Varchar2, -- Email address
	PASSWORD_IN IN Varchar2, -- User Password	
	FIRSTNAME_IN IN Varchar2, -- First Name
	LASTNAME_IN IN Varchar2, -- Last Name
	STREET_IN IN Varchar2, -- Street Address
	CITY_IN IN Varchar2, -- City
	STATE_IN IN Varchar2, -- State or Provience
	ZIP_IN IN Varchar2, -- Zip or Postal code
	COUNTRY_IN IN Varchar2, -- Country
	CORPUSID_IN IN Number, -- Corpus ID subscribing to
	SPAM_IN IN Number, -- Does this user want SPAM?  0 or 1
	COVERCODE_IN IN Varchar2 -- Unique cover code mandatory for free trial
	)
AS
BEGIN 
DECLARE
   UserId NUMBER(9);
   Tday NUMBER(4);
   Eday NUMBER(4);
   CName VARCHAR2(50);
   Expire_Date DATE;
   CoverStatus NUMBER(1);
BEGIN
   CoverStatus := 0;
   IF (COVERCODE_IN = 'NULL') THEN
      CoverStatus := 1;
   ELSE
      SELECT Status INTO CoverStatus FROM CoverIdentifiers WHERE CoverId = Upper(COVERCODE_IN);
      IF (CoverStatus != 1) THEN
         Raise_Application_Error(-20003, 'Cover Identifier already in use.');
      END IF;
   END IF;

   SELECT sub_seq.nextval INTO UserId FROM dual;
   SELECT trial_days, expire_days, corpusshortname INTO Tday, Eday, CName FROM corpus WHERE corpusid = CORPUSID_IN;

   -- If there is no trial period, go straight into billing period
   IF (COVERCODE_IN = 'NULL') OR (Tday = 0) THEN 
      Expire_Date := SYSDATE + Eday;
   ELSE 
      Expire_Date := SYSDATE + Tday;
   END IF;

   INSERT INTO Subscriber (USERID, EMAIL, PASSWORD, FIRSTNAME, LASTNAME, STREET, CITY, STATE, ZIPCODE, COUNTRY, USERSTATUS, DEFAULTCORPUSNAME, USERWANTSSPAM) VALUES (UserId, Upper(EMAIL_IN), PASSWORD_IN, FIRSTNAME_IN, LASTNAME_IN, STREET_IN, CITY_IN, STATE_IN, ZIP_IN, COUNTRY_IN, 1, CName, SPAM_IN);
   INSERT INTO SubscriberCorpus VALUES (UserId, CORPUSID_IN, SYSDATE, Expire_Date);

   IF (COVERCODE_IN = 'NULL') THEN
      CoverStatus := 1;
   ELSE
      UPDATE CoverIdentifiers SET Status = 0 WHERE CoverId = COVERCODE_IN;
   END IF;

EXCEPTION 
    WHEN NO_DATA_FOUND THEN
	 Raise_Application_Error(-20002, 'Invalid Cover Identifier was provided.');
    WHEN DUP_VAL_ON_INDEX THEN
	 Raise_Application_Error(-20001, 'E-mail address already exists.');
END;

COMMIT WORK;
END Subscribe;
/
