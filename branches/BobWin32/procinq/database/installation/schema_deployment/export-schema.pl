#!/usr/bin/perl

#
# NOTES for exporting from TOAD
#
# uncheck: Include DROP statement in script
# uncheck: Include table space info
# uncheck: Separate Check constraints
# uncheck: Separate Unique Constraints
# check: Primary Key with Indexes Script
# check: Include Triggers
# check: Include SQL*Plus / chars

use DBI;
use Getopt::Long;

local ($filename) = "";

&GetOptions ("filename=s" => \$filename);

if ($filename eq "") { die "Usage: export-schema.pl -filename <schema>\n"; }
if (!-e $filename) { die "Fatal Error! File: $filename could not be found.\n"; }

# open the existing schema file that was generated from TOAD
open (TOAD, "$filename"); @lines = <TOAD>; close(TOAD);
open (TOAD, ">>$filename"); # open the schema file to append

local ($dbh) = DBI->connect("dbi:Oracle:pandora", "sbooks", "racer9");

print "Creating sequences.."; 

local ($q) = "select sequence_name from user_sequences";
local ($sth) = $dbh->prepare($q); 
$sth->execute();

print TOAD "\n";

local ($loop) = 0;
while ($sname = $sth->fetchrow_array()) { 
   print TOAD "create sequence $sname start with 2;\n";
   print ".";
} print "\n";
$sth->finish();

print "Creating configuration parameters.."; 

$q = "select paramname, paramvalue, paramtype from configparams";
local ($sth) = $dbh->prepare($q); 
$sth->execute();

print TOAD "\n";

local ($loop) = 0;
while (($name, $val, $type) = $sth->fetchrow_array()) { 
   print TOAD "insert into configparams values ('$name', '$val', $type);\n";
   print ".";
} print "\n";
$sth->finish();

print "Creating stored procedures.."; 

$q = "select p.object_Name, s.text from user_source s, user_procedures p where p.object_name = s.name order by p.object_name, s.line asc";
local ($sth) = $dbh->prepare($q); 
$sth->execute();

local ($loop) = 0; local ($former) = "";
while (($name, $text) = $sth->fetchrow_array()) { 
   if (($former ne $name) && ($former ne "")) { print TOAD "\n/\n"; }
   if ($former ne $name) { print TOAD "\n"; $former = $name; }
   if ($text =~ /^PROCEDURE/) { $text = "CREATE OR REPLACE $text"; }
   print TOAD "$text";
   print ".";
} print "\n";
$sth->finish();

print TOAD "\n/\n";

print "Creating default data rows.."; 

$q = "select repositorytypeid, repositorytypename, repositoryclassname from repositorytype";
local ($sth) = $dbh->prepare($q); 
$sth->execute();

local ($loop) = 0; local ($former) = "";
while (($id, $name, $class) = $sth->fetchrow_array()) { 
   print TOAD "insert into repositorytype values ($id, '$name', '$class');\n";
   print ".";
} print "\n";
$sth->finish();

print TOAD "\n";

$dbh->disconnect();

print TOAD "insert into genre (genreid, genrename, genrestatus) values (1, 'Commercial Web', 1);\n";
print TOAD "insert into genre values (12, 'People', 1);\n";
print TOAD "insert into repository (repositoryid, repositoryname, repositorytypeid) values (1, 'ROC Analysis', 1);\n";
print TOAD "insert into repository (repositoryid, repositoryname, repositorytypeid) values (99999, 'Internal Use', 8);\n";

print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (3001, 'StarCountInt', 'SCORE1', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1);\n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (1, 'Freq', 'SCORE4', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (3, 'Cov', 'SCORE5', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (4, 'FreqScaled', 'SCORE6', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (27, 'f*c', 'SCORE2', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (3002, 'StarCount', 'SCORE3', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (28, 'f*c^', 'SCORE7', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (29, 'f*c^20', 'SCORE8', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (30, 'f*c^15', 'SCORE9', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (31, 'fb*c', 'SCORE10', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (33, 'fb*c^20', 'SCORE11', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
print TOAD "INSERT INTO CLASSIFIER ( CLASSIFIERID, CLASSIFIERNAME, COLNAME, CLASSPATH, SORTORDER, ACTIVE ) VALUES (34, 'fb*c^15', 'SCORE12', 'com.indraweb.externalclassifier1.ScoreStyle1', 0, 1); \n";
commit;

print TOAD "insert into security values (0, 'Everyone');\n";

print TOAD "insert into rocsetting values (1, 50);\n";
print TOAD "insert into rocbucketlocalclassifier values (1, 1, .2, .3, 30, 50, 0, .25);\n";
print TOAD "insert into rocbucketlocalclassifier values (1, 2, 1, .6667, 30, 75, 0, .25);\n";
print TOAD "insert into rocbucketlocalclassifier values (1, 3, 5, 1.063, 30, 100, 0, .25);\n";

print TOAD "insert into corpus (corpusid, corpus_name, corpusdesc, corpusactive, rocsettingid) values (1, 'Example Taxonomy', 'Example', 1, 1);\n";
print TOAD "insert into node values (1, 1, 'Example Root Topic', 100, -1, 1, 0, sysdate, sysdate, 1, null, sysdate, null, 1);\n";
print TOAD "insert into subjectarea values (1, 'Examples', 1);\n";
print TOAD "insert into corpussubject values(1, 1);\n";

print TOAD "drop index NodeNodeTitle;\n";
print TOAD "create index NodeNodeTitle on Node(NodeTitle) indextype is ctxsys.context;\n\n";

print TOAD "drop index SignatureSignatureWord;\n";
print TOAD "create index SignatureSignatureWord on Signature (SignatureWord) indextype is ctxsys.context;\n\n";

print TOAD "drop index DocumentDocTitle;\n";
print TOAD "create index DocumentDocTitle on Document(DocTitle) indextype is ctxsys.context;\n\n";

print TOAD "drop index DocumentDocumentSummary;\n";
print TOAD "create index DocumentDocumentSummary on Document(DocumentSummary) indextype is ctxsys.context;\n\n";

print TOAD "drop index DocFulltext;\n";
print TOAD "create index DocFullText on Document(FullText)\n";
print TOAD "indextype is ctxsys.context parameters \n";
print TOAD "('datastore COMMON_DIR storage index_location')\n";
print TOAD "/\n";

print TOAD "COMMIT WORK;\n\n";


print "Appending fields...";

local ($table) = "NONE";
foreach $line (@lines) {
   if ($line =~ /CREATE TABLE (.*?) \(/) { 
	$table = $1; print TOAD "\n"; 
	print TOAD "ALTER TABLE $table MOVE TABLESPACE DDATA;\n";
   }
   if ($line =~ /^\s+(.*?)NUMBER(.*)/) { 
	$col = $1; $etc = $2; chop($etc); if ($etc =~ /\,$/) { chop($etc); }
	print TOAD "ALTER TABLE $table ADD ($col NUMBER $etc);\n"; 
	print TOAD "ALTER TABLE $table MODIFY ($col NUMBER $etc);\n"; print "."; 
   }
   if ($line =~ /^\s+(.*?)VARCHAR2(.*)/) { 
	$col = $1; $etc = $2; chop($etc); if ($etc =~ /\,$/) { chop($etc); }
	print TOAD "ALTER TABLE $table ADD ($col VARCHAR2 $etc);\n"; 
	print TOAD "ALTER TABLE $table MODIFY ($col VARCHAR2 $etc);\n"; print "."; 
   }
} print "\n\n";

print TOAD "\nCOMMIT WORK;\n";
close (TOAD);

print "Done!\n"; 
1;


