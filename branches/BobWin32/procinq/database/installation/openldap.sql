/* update the configuration parameters */
update configparams set paramvalue = 'DC=examplenet' where paramname = 'DC';
update configparams set paramvalue = '127.0.0.1' where paramname = 'LdapHost';
update configparams set paramvalue = '389' where paramname = 'LdapPort';
update configparams set paramvalue = 'cn=administrator,dc=examplenet' where paramname = 'LdapUsername';
update configparams set paramvalue = 'example' where paramname = 'LdapPassword';
update configparams set paramvalue = 'cn=IndraAdministrators' where paramname = 'AdminGroup';
update configparams set paramvalue = 'sn=##USERNAME##,ou=users,dc=examplenet' where paramname = 'LoginName';
commit;
