
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ref.DefaultContext;

import java.util.Vector;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.io.*;

// HBK 2003 03 28
// Reads Thesaurus content from Indraweb tables for inclusion into Oracle CTX index thesaurus
// Oracle Java program
// DROP FROM ORACLE JAVA POOL : dropjava -verbose -user sbooks/racer9@client c:\temp\SynchThesaurus.java
// ADD  TO ORACLE JAVA POOL : loadjava -verbose -user sbooks/racer9@client -resolve c:\temp\SynchThesaurus.java

// STEP 1
// compile the java on the db machine
// c:\oracle\ora81\sqlj\lib>dropjava -verbose -user sbooks/racer9@client c:\temp\SynchThesaurus.java
// c:\oracle\ora81\sqlj\lib>loadjava -verbose -user sbooks/racer9@client -resolve c:\temp\SynchThesaurus.java

/*  steps 2,3 can be run in sqlplus together then 4
drop procedure SynchThesaurus;
CREATE OR REPLACE PROCEDURE SynchThesaurus (sDBName VARCHAR2, sUserDB VARCHAR2, sPassDB VARCHAR2) AS LANGUAGE JAVA NAME 'SynchThesaurus.SynchThesaurus (java.lang.String, java.lang.String, java.lang.String)';

// STEP 4 (really to be run from code but as a test ...
commit;
exec synchthesaurus ('client','sbooks','racer9');
*/

// to see thesaurus content :
/*
VARIABLE exp2 varchar2(4000);
begin
  :exp2 := ctx_thes.syn('henry2','indrahk');
end;
.
run;
PRINT :exp2;
*/

public class SynchThesaurus
{

    static String sFilePathLogSynchThes = ""; //eg "c:\\temp\\LogSynchThes.txt";
    static String sFolderOracleBin = null; // "eg C:\\oracle\\ora81\\bin";
    static String sOracleVersion = null; // "eg 8.1.7";
    static String sFilePathCtxLoadExe = null; // sFolderNameOracleBin  + "\\ctxload";

    public static void SynchThesaurus(String sDBName,
                                      String sUserDB,
                                      String sPassDB
                                      ) throws Exception
    {
        synchronized (sFilePathLogSynchThes)
        {
            try
            {
                ExecutionContext ec = DefaultContext.getDefaultContext().getExecutionContext();
                Connection dbc = DefaultContext.getDefaultContext().getConnection();

                // insert into configparams values ( 'FilePathLogSynchThes', 'c:/temp/LogSynchThes.txt', 2);
                sFilePathLogSynchThes = ExecuteQueryAgainstDBStr("select paramvalue from configparams where " +
                        " paramname = 'FilePathLogSynchThes' ", dbc); // "c:/temp/LogSynchThes.txt"
                log("***** START SynchThesaurus script " + new java.util.Date());
                // insert into configparams values ( 'OracleBinFolder', 'C:/oracle/ora81/bin', 2);
                sFolderOracleBin = ExecuteQueryAgainstDBStr("select paramvalue from configparams where " +
                        " paramname = 'OracleBinFolder' ", dbc); // "C:\\oracle\\ora81\\bin"
                sFilePathCtxLoadExe = sFolderOracleBin + "/ctxload";
                // insert into configparams values ( 'OracleVersion', '8.1', 2);
                sOracleVersion = ExecuteQueryAgainstDBStr("select paramvalue from configparams where " +
                        " paramname = 'OracleVersion' ", dbc); // ""


                // GRANT LOG FILE PERMISSIONS
                Statement stmt = dbc.createStatement();
                ResultSet rs = stmt.executeQuery("call dbms_java.grant_permission('PUBLIC','java.io.FilePermission', '" + sFilePathLogSynchThes + "', 'write,delete')");
                rs.close();
                stmt.close();


                // GRANT PERMISSIONS ON CTXLOAD.EXE
                Statement stmt4 = dbc.createStatement();
                ResultSet rs4 = stmt4.executeQuery("call dbms_java.grant_permission('PUBLIC','java.io.FilePermission', '" + sFilePathCtxLoadExe + "', 'read,execute,write')");
                rs4.close();
                stmt4.close();

                stmt = null;
                rs = null;
                int iNumThesaurusUpdated = createFilesAndCreateThesPerFile(
                        stmt, dbc, rs,
                        sDBName,
                        sUserDB,
                        sPassDB
                );

            } catch (Exception e)
            {
                logError("Error - synch thesaurus failed, see synch thesaurus log for details [" +
                        e.getMessage() + "]", e);

            }
        }  // synchronized
    }

    private static int createFilesAndCreateThesPerFile(
            Statement stmt,
            Connection dbc,
            ResultSet rs,
            String sDBName,
            String sUserDB,
            String sPassDB
            ) throws Exception
    {
        int iNumThesaurusUpdated = 0;
        try
        {
            String SQL = "select t.THESAURUSID, tw1.THESAURUSWORD, tw2.THESAURUSWORD " +
                    " from thesaurus t, thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2  " +
                    " where t.thesaurusID = tr.thesaurusID and tr.WORDANCHOR = tw1.WORDID and tr.WORDRELATED = tw2.WORDID " +
                    " order by THESAURUSID DESC, tw1.THESAURUSWORD";

            stmt = dbc.createStatement();

            rs = stmt.executeQuery(SQL);
            String sTIDCurrent = null;
            PrintWriter file = null;

            String sFileNameClosing = null;
            String sFileNameOpening = null;

            String sPriorAnchorWord = "";
            String sTIDNew = null;
            int iloop = 0;
            while (rs.next())
            {
                sTIDNew = rs.getString(1);
                String sW1 = rs.getString(2);
                String sW2 = rs.getString(3);
                String sLineOpenWord = null;
                String sLine2WordSyns = null;

                // HANDLE NEXT THESAURUS
                if (!sTIDNew.equals(sTIDCurrent))
                {
                    iloop++;
                    log("\r\n\r\n [" + iloop + "] ******** NEXT THESAURUS PRIOR [" + sTIDCurrent + "] NEW [" + sTIDNew + "] ");
                    // close file fom if this
                    if (file != null)
                    {
                        String sThesNameCurent = getThesName(sTIDCurrent, dbc);
                        sFileNameClosing = "c:/temp/Thes_" + sTIDCurrent + ".txt";
                        file.close();
                        log("complete thesaurus file create [" + sTIDCurrent + "]");

                        iNumThesaurusUpdated++;
                        insertThesToOracle(sTIDCurrent,
                                sThesNameCurent,
                                sDBName,
                                sUserDB,
                                sPassDB,
                                sOracleVersion,
                                dbc
                        );
                        iNumThesaurusUpdated++;

                        file = null;
                    }
                    // delete from oracle any existing version of upcoming thesaurus
                    deleteThesaurus(getThesName(sTIDNew, dbc), dbc);

                    sFileNameOpening = "c:/temp/Thes_" + sTIDNew.replace(' ', '_') + ".txt";
                    // GET PERMISSION TO WRITE THES FILE EXPORT
                    Statement stmt2 = dbc.createStatement();
                    ResultSet rs2 = stmt2.executeQuery("call dbms_java.grant_permission('PUBLIC','java.io.FilePermission', '" + sFileNameOpening + "', 'write,delete')");
                    rs2.close();
                    stmt2.close();

                    file = new PrintWriter(new BufferedWriter(new FileWriter(sFileNameOpening)));
                    log("started thesaurus file create [" + sFileNameOpening + "]");

                    sTIDCurrent = sTIDNew;
                }
                // HANDLE NEXT ANCHOR WORD
                if (sW1.equals(sPriorAnchorWord))
                {
                    sLineOpenWord = "";
                } else
                {
                    sLineOpenWord = "\r\n" + sW1.trim() + "\r\n";
                    sPriorAnchorWord = sW1;
                }
                sLine2WordSyns = "   SYN " + sW2 + "\r\n";
                file.write(sLineOpenWord + sLine2WordSyns);
            }

            if (file != null)
            {
                file.close();
                log("complete thesaurus file create [" + sFileNameOpening + "]");

                iNumThesaurusUpdated++;
                insertThesToOracle(sTIDNew,
                        getThesName(sTIDNew, dbc),
                        sDBName,
                        sUserDB,
                        sPassDB,
                        sOracleVersion,
                        dbc
                );

            }

        } catch (Exception e)
        {
            logError("Fatal error reading contents of zip file!", e);
            throw e;
        } finally
        {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        return iNumThesaurusUpdated;

    }

    private static String getThesName(String sTID, Connection dbc) throws Exception
    {
        String sSQL = "select thesaurusname from thesaurus where thesaurusid = " + sTID;
        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery(sSQL);
        String sThesName = null;
        if (rs.next())
            sThesName = rs.getString(1);
        rs.close();
        return sThesName;
    }

    private static void deleteThesaurus(String sTName, Connection dbc) throws Exception
    {
        Statement stmt = null;
        CallableStatement cstmt = null;
        try
        {
            cstmt = dbc.prepareCall("begin ctx_thes.drop_thesaurus(:1) ; end;");
            cstmt.setString(1, sTName);
            cstmt.executeUpdate();
        } catch (Exception e)
        {
            log("Error in deleteThesaurus [" + e.getMessage() + "]");
        } finally
        {
            cstmt.close();
        }
    }

    private static void insertThesToOracle(String sThesID,
                                           String sThesName,
                                           String sDBName,
                                           String sUserDB,
                                           String sPassDB,
                                           String sOracleVersion,
                                           Connection dbc
                                           ) throws Exception
    {
        // LOAD ORACLE WITH THIS ONE THESAURUS AT A TIME - synchrronous exec call
        try
        {
            // oracle 8 syntax differs from 9
            // 8 ctxload -user sbooks/racer9@client -thes -name "IDRAC Thesaurus" -file c:/tmp/Thes_1.txt
            // 9 ctxload -user sbooks/racer9        -thes -name "idrac" -file C:/temp/Thes_1.txt
            String sDBSectionForCtxLoad = "";
            if (sOracleVersion.startsWith("8"))
                sDBSectionForCtxLoad = "@" + sDBName;

            String sExec = sFilePathCtxLoadExe + " -user " +
                    sUserDB + "/" + sPassDB + sDBSectionForCtxLoad +
                    " -thes -name \"" + sThesName + "\" -file " + "c:/temp/Thes_" + sThesID + ".txt";
            log("starting thesaurus import [" + sExec + "]");
            Process pro = Runtime.getRuntime().exec(sExec);
            InputStream error = pro.getErrorStream();
            InputStream output = pro.getInputStream();
            Thread err = new Thread(new OutErrReader("Error stream", error));
            Thread out = new Thread(new OutErrReader("Output stream", output));
            out.start();
            err.start();
            pro.waitFor();
            out.join();
            err.join();

            if (pro.exitValue() == 0)
                log("completed theaurus import command [" + sExec + "]");
            else
            {
                log("completed with failure theaurus import command [" + sExec + "]");
                throw new Exception("failed theaurus import command [" + sExec + "]");
            }
        } catch (Exception e)
        {
            logError("completed with exception failure in thesaurus load thesaurus [" + sThesName + "]", e);
        }
    }

    public static class OutErrReader implements Runnable
    {
        InputStream is;
        String sStreamName = null;

        public OutErrReader(String sStreamName, InputStream is)
        {
            this.is = is;
            this.sStreamName = sStreamName;
        }

        public void run()
        {
            try
            {
                BufferedReader in = new BufferedReader(new InputStreamReader(is));
                String temp = null;
                while ((temp = in.readLine()) != null)
                {
                    log("Stream [" + sStreamName + "] " + temp);
                }
                is.close();
            } catch (Exception e)
            {
                try
                {
                    logError("Stream [" + sStreamName + "] error in threaded stream reader [" + sStreamName + "]", e);
                } catch (Exception e2)
                {
                    // nothing to do here - can't throw exception from a run
                }
            }
        }
    }

    public static synchronized void log(String lineToadd) throws Exception
    {
        PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(sFilePathLogSynchThes, true)));
        file.write(new java.util.Date() + ":" + lineToadd + "\r\n");
        file.close();
    }

    public static synchronized void logError(String lineToadd, Throwable e) throws Exception
    {
        log("ERROR: " + lineToadd + " stack [" + stackTraceToString(e) + "]");
    }

    public static String stackTraceToString(Throwable e) throws Exception
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return "StackTRACE " + sw.toString() + " ";
    }

    public static String ExecuteQueryAgainstDBStr(String aSQL, Connection dbc)
            throws Exception
    {

        // ----------------------------------------------------------

        if (dbc.isClosed())
            log("jdbcindra_connection hbk hbk 1 dbc closed [" + dbc + "]");
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(aSQL);

            int iNumFound = 0;

            Vector vecSourceURLs = new Vector();
            String sReturnVal = null;
            int i = 0;
            while (rs.next())
            {
                //MessageBox(0, "in FillListviewWithWebInfoForDocAndNode looping",         "This message box from Java", 0);
                sReturnVal = rs.getString(1);
                i++;
            }
            return sReturnVal;
        } catch (Exception ex)
        {
            logError("Exception in SynchThesaurus.ExecuteQueryAgainstDBStr sql [" + aSQL + "]", ex);
        } finally
        {
            if (stmt != null)
                stmt.close();
            if (rs != null)
                rs.close();
        }
        return null;

    }	 // runQueryAgainstDV ... ()

}

