alter table conceptcache add (indexwithinnode number(2));
alter table conceptalertcache modify (count float);

-- (1) select all nodes associated with this document and put them into the conceptcache table
-- (2) select all documents from the set of nodes in the cache table 
--
-- create global temporary table ConceptCache (documentid number(9), nodeid number(9)) on commit delete rows;
CREATE OR REPLACE PROCEDURE GetSimilarDocs (documentid_in IN Document.DocumentId%TYPE)
IS 
BEGIN
  delete from conceptalertcache where conceptid = documentid_in;
  commit;

  declare cursor c1 is select 'insert into conceptcache select documentid, nodeid, '||rtrim(to_char(indexwithinnode))||' from nodedocument where nodeid = ' || rtrim(to_char(nodeid)) as theline from nodedocument where documentid = documentid_in;
  begin
  for c1_rec in c1 loop
    execute immediate c1_rec.theline;
    -- dbms_output.put_line(c1_rec.theline);
  end loop; 
  insert into conceptalertcache select documentid_in, documentid, sum(indexwithinnode) from conceptcache group by documentid;
  commit work;
  delete from conceptcache;
  commit work;
  end;
END GetSimilarDocs;
/
show errors;

CREATE OR REPLACE PROCEDURE GetDocsBySignature (sigterm_in IN Signature.SignatureWord%TYPE)
IS 
BEGIN
  declare cursor c1 is select 'insert into conceptcache select documentid, nodeid from nodedocument where nodeid = ' || rtrim(to_char(nodeid)) as theline from nodedocument where nodeid in (select distinct(nodeid) from signature where contains(signatureword, sigterm_in)>0);
  begin
  for c1_rec in c1 loop
    execute immediate c1_rec.theline;
    -- dbms_output.put_line(c1_rec.theline);
  end loop; 
  end;
END GetDocsBySignature;
/
show errors;

set serveroutput on;
BEGIN
  declare cursor c1 is select 'insert into conceptalertcache select 328031, documentid, nodeid from nodedocument where nodeid = ' || rtrim(to_char(nodeid)) as theline from nodedocument where documentid = 328031;
  begin
  for c1_rec in c1 loop
    -- execute immediate c1_rec.theline;
    dbms_output.put_line(c1_rec.theline);
  end loop; 
  end;
END;
