/*
Created		9/28/2001
Modified	5/1/2002
Project		
Model		
Company		
Author		
Version		
Database		Oracle 8 
*/


Create table Signature (
	NodeId Number(12,0) NOT NULL,
	SignatureWord Varchar2(30),
	SignatureOccurences Number(5,0))  ;

Create table Channel (
	ChannelId Number(4,0) NOT NULL,
	ChannelName Varchar2(100),
	ISChannel Varchar2(100))  ;

Create table UpdateLog (
	UpdateLogId Number(13,0) NOT NULL,
	NodeId Number(12,0) NOT NULL,
	ActionCode Number(3,0),
	Hostname Varchar2(50),
	UpdateDate Date)  ;

Create table Genre (
	GenreId Number(4,0) NOT NULL UNIQUE ,
	ClientId Number(5,0) NOT NULL,
	GenreName Varchar2(20) UNIQUE,
	GenreStatus Number(1,0) Default 1)  ;

Create table CorpusGenre (
	GenreId Number(4,0) NOT NULL,
	CorpusId Number(9,0) NOT NULL)  ;

Create table Corpus (
	CorpusId Number(9,0) NOT NULL,
	ClientId Number(5,0) NOT NULL,
	Corpus_Name Varchar2(256) NOT NULL,
	CorpusDesc Varchar2(256),
	CorpusImageIds Varchar2(256),
	ROCF1 Real Default 0.0,
	ROCF2 Real Default 0.0,
	ROCF3 Real Default 0.0,
	ROCC1 Real Default 0.0,
	ROCC2 Real Default 0.0,
	ROCC3 Real Default 0.0,
	CorpusActive Number(3,0) Default 1,
	CorpusRunFrequency Number(3,0) Default 90,
	CorpusLastRun Date Default SYSDATE,
	CorpusLastSync Date Default SYSDATE)  ;

Create table NodeDocument (
	NodeId Number(12,0) NOT NULL,
	DocumentId Number(9,0) NOT NULL,
	DocumentType Number(2,0),
	Score1 Real,
	Score2 Real,
	Score3 Real,
	Score4 Real,
	Score5 Real,
	Score6 Real,
	DocSummary Varchar2(1000),
	DateScored Date)  ;

Create table Node (
	NodeId Number(12,0) NOT NULL,
	CorpusId Number(9,0) NOT NULL,
	ClientId Number(5,0) NOT NULL,
	NodeTitle Varchar2(256),
	NodeSize Number(9,0),
	ParentId Number(9,0),
	NodeIndexWithinParent Number(3,0),
	DepthFromRoot Number(3,0),
	DateScanned Date,
	DateUpdated Date, 
        NodeStatus Number(1) DEFAULT 1,
	NodeLTitle Varchar2(256)
	NodeLastSync Date Default sysdate);

Create table Client (
	ClientId Number(5,0) NOT NULL,
	ClientName Varchar2(20),
	DomainComponent varchar2(100),
	Password varchar2(15),
	LdapHost varchar2(20));

Create table DocumentChannel (
	DocumentId Number(9,0) NOT NULL,
	ChannelId Number(4,0) NOT NULL,
	NodeId Number(12,0) NOT NULL)  ;

Create table NodeChannel (
	NodeId Number(12,0) NOT NULL,
	ChannelId Number(4,0) NOT NULL)  ;

Create table Document (
	DocumentId Number(9,0) NOT NULL UNIQUE ,
	GenreId Number(4,0) NOT NULL,
	DocTitle Varchar2(256),
	DocUrl Varchar2(256) UNIQUE,
	DocumentSummary Varchar2(4000),
	FilterStatus Number(1,0) Default 1,
	TroubleStatus Number(1,0) Default 0,
	DateLastFound Date Default SYSDATE,
	RepositoryID Number(4) Default 1)  ;

Create table Blacklist (
        BlackListId Number(4,0) NOT NULL UNIQUE ,
        CorpusId Number(9,0) NOT NULL,
        NodeId Number(12,0),
        UrlChunk Varchar2(256),
        DateCreated Date)  ;	

Create table Expert (
	UserId Number(9,0) NOT NULL,
        NodeId Number(12,0) NOT NULL,
	Status Number(2,0) NOT NULL,
	ExpertLevel Number(2,0) NOT NULL);

/* Tables used for managing thesauri */

Create table Thesaurus (
	ThesaurusId Number(5,0) NOT NULL,
	ThesaurusName Varchar2(30));

Create table ThesaurusWord (
	WordId Number(12,0) NOT NULL,
	ThesaurusWord Varchar2(60) NOT NULL UNIQUE);

Create table ThesaurusRelations (
	ThesaurusId Number(5,0) NOT NULL,
	WordAnchor Number(12,0) NOT NULL,
	WordRelated Number(12,0) NOT NULL,
	Relationship Number(2,0) NOT NULL);

Create table CorpusThesaurus (
	CorpusId Number(9,0) NOT NULL,
	ThesaurusId Number(5,0) NOT NULL);

/* Tables used for adding custom attributes to document table */

Create table CustomDocumentAttributes (
	AttributeId Number(6,0) NOT NULL,
	AttributeName Varchar2(50) NOT NULL);

Create table CustomDocument (
	DocumentId Number(9,0) NOT NULL,
	AttributeId Number(6,0) NOT NULL,
	AttributeVal Varchar2(4000));

/* Tables used for managing repositories */

Create table Repository (
	RepositoryId Number(5,0) NOT NULL,
	RepositoryTypeId Number(5,0) NOT NULL,
	RepositoryName Varchar2(256) NOT NULL,
	RepositoryLoc Varchar2(500),
	Username Varchar2(50),
	Password Varchar2(50),
	Fulltext Number(1) DEFAULT 1);

Create table RepositoryType (
	RepositoryTypeId Number(5,0) NOT NULL,
	RepositoryTypeName Varchar2(256), 
	RepositoryClassName Varchar2(50) NOT NULL);

Create table WatchFolder (
	RepositoryId Number(5,0) NOT NULL,
	Path Varchar2(4000) NOT NULL,
	Recurse Number(1) DEFAULT 0,
	DateLastCapture Date DEFAULT SYSDATE);

Create table DocumentSecurity (
	DocumentId Number(9,0) NOT NULL,
	SecurityKey Varchar2(500));

/* Table used for managing server configuration */
Create table ConfigParams (
	ParamName Varchar2(50) NOT NULL,
	ParamValue Varchar2(256) DEFAULT '',
	ParamType Number(1) DEFAULT 0);

/* Tables used for storing nuggets and snippets of documents */
Create table Nugget (
	NuggetId Number(9,0) NOT NULL,
	Nugget Varchar2(4000));

Create table Snippet (
	SnippetId Number(9,0) NOT NULL,
	NuggetId Number(9,0) NOT NULL,
	Snippet Varchar2(4000));

Create table NodeDocumentSnippet (
	NodeId Number(12,0) NOT NULL,
	DocumentId Number(9,0) NOT NULL,
	SnippetId Number(9,0) NOT NULL);

/* Table added for storage of topic validation list */
Create table NodeValidation (
	NodeId Number(12,0) NOT NULL,
	DocumentCount Number(5,0) DEFAULT 0);

/* ROC table */
Create table ROCValidation (
	NodeId Number(12,0) NOT NULL,
	DocumentId Number(9,0) NOT NULL,
	ROCValidation Number(1,0) Default 0);

/* ------------- revisions for schering 1/15/05 -- */
create table GroupNotification (AlertID Number(12) NOT NULL, GroupDN Varchar2(300)) tablespace ddata;
alter table GroupNotification add foreign key (AlertID) references Notification (AlertID) on delete cascade;

/* ------------- since major revisions 6/1/04 -- */
insert into repositorytype values (11, 'Ultrasearch', 'Ultrasearch');

alter table node drop column clientid;
alter table genre drop column clientid;
alter table corpus drop column clientid;
drop table client;

create unique index ConfigParamsParamName on ConfigParams (ParamName) tablespace DDATA;

Create table NodeData (
	NodeId Number(12,0) NOT NULL,
	NodeSource CLOB) TABLESPACE DDATA;
alter table NodeData add foreign key (NodeID) references Node (NodeID) on delete cascade;

/* ------------- since migration script created 3/1/04 -- */
insert into classifier  (CLASSIFIERID,CLASSIFIERNAME,COLNAME,CLASSPATH,SORTORDER,ACTIVE ) values ( 31,'fb*c'   ,'SCORE10','com.indraweb.externalclassifier1.ScoreStyle1',0,1);
insert into classifier  (CLASSIFIERID,CLASSIFIERNAME,COLNAME,CLASSPATH,SORTORDER,ACTIVE ) values ( 33,'fb*c^20','SCORE11','com.indraweb.externalclassifier1.ScoreStyle1',0,1);
insert into classifier  (CLASSIFIERID,CLASSIFIERNAME,COLNAME,CLASSPATH,SORTORDER,ACTIVE ) values ( 34,'fb*c^15','SCORE12','com.indraweb.externalclassifier1.ScoreStyle1',0,1);

alter table document modify (troublestatus number(3));
create index nodedepthfromroot on node (depthfromroot) tablespace RDATA;
create index SignatureSignatureWord on Signature(SignatureWord) indextype is ctxsys.context;
create global temporary table ConceptCache (documentid number(9), nodeid number(9)) on commit delete rows;

create table conceptalertcache (conceptid number(9), documentid number(9), count number(9)) tablespace DDATA;
alter table conceptalertcache add foreign key (ConceptId) references Document (DocumentId) on delete cascade;
alter table conceptalertcache add foreign key (DocumentId) references Document (DocumentId) on delete cascade;

alter table document add (DocumentSecurity varchar2(4000) default '1');


create sequence security_seq start with 1;

/* each new security must have a security id */
create trigger security_trigger before insert on security for each row when (new.securityid is null)
  begin select security_seq.nextval into :new.securityid from dual;
  end;
/

create or replace trigger doc_trigger before insert on document for each row when (new.documentid is null)
  begin select doc_seq.nextval into :new.documentid from dual;
  end;
/

/* ------------- since last DFI delivery 10/22/03+ --- */
alter table document add (DocSize number(9));
alter table node add (NodeDesc varchar2(4000));
alter table document modify (repositoryid number(10));
insert into repositorytype values (8, 'Internal Document Abstracts', 'Filesystem');
insert into repository values (9999, 8, 'Concept Alerts', null, null, null, 1);

/* -- link node changes 12/30/03 -- */
alter table node add (LinkNodeID Number(12));
commit;

update node set linknodeid = nodeid;
commit;

create or replace trigger linknode_trigger before insert on node for each row when (new.linknodeid is null)
  begin :new.linknodeid := :new.nodeid;
  end;
/

/* each new corpus must have an rocsetting id */
create or replace trigger corpusroc_trigger before insert on corpus for each row when (new.rocsettingid is null)
  begin :new.rocsettingid := 1;
  end;
/

/* ------------- since last DFI delivery 10/1/03 --- */
/* Email alerts table */
Create table Notification (
	AlertId Number(12,0) NOT NULL,
	AlertName Varchar2(100) NOT NULL,
	UserDN Varchar2(100),
	Email Varchar2(100),
	CQL Varchar2(100),
	RunFrequency Number(3),
	LastRunDate Date);

/* staging tables */
create table StageDocCorpusDelete (
    DocumentID Number(9),
    CorpusID Number(9),
    StagingDate Date default SYSDATE);

create table StageNodeDocInsert (
    NodeID Number(12),
    DocumentID Number(9),
    DocumentType Number(2),
    Score1 Float, Score2 Float, Score3 Float, Score4 Float, Score5 Float, Score6 Float,
    Score7 Float, Score8 Float, Score9 Float, Score10 Float, Score11 Float, Score12 Float,
    DocSummary Varchar2(1000), DateScored Date, IndexWithinNode Number(20),
    CorpusID Number(9), StagingDate Date Default SYSDATE);

/* changes for new batch processing */
create table NodeDocumentLocked (Machine varchar2(256), DateLocked Date default SYSDATE);

alter table nodedocument add (edited number(1) default 0);
alter table document modify (updateddate date default sysdate);
update document set updateddate = datelastfound where updateddate is null;
alter table document modify (docurl varchar2(512));

/* new indicies for speed */
create index StagingDateStage1 on StageNodeDocInsert(StagingDate) tablespace RDATA;
create index StagingDateStage2 on StageDocCorpusDelete(StagingDate) tablespace RDATA;
create index NodeDocumentEdited on NodeDocument(Edited) tablespace RDATA;
create index DocumentUpdatedDate on Document(UpdatedDate) tablespace RDATA;
create index NodeDocumentScore1 on NodeDocument (Score1) tablespace RDATA;
create index documentreviewstatus on document (reviewstatus) tablespace DDATA;
analyze table document compute statistics;

/* ROC changes */
alter table corpus add (rocsettingid number(3) default 1);
update corpus set rocsettingid = 1;

create table ROCSetting (ROCSETTINGID NUMBER(4) NOT NULL, RATIOTNTOTP FLOAT NOT NULL) TABLESPACE DDATA;

create table ROCBucketLocalClassifier (
  ROCSETTINGID NUMBER(4) NOT NULL, STARCOUNT NUMBER(2) NOT NULL,
  COSTRATIOFPTOFN FLOAT NOT NULL, CUTOFFSCOREGTE FLOAT NOT NULL,
  CLASSIFIERID NUMBER(4) NOT NULL, SCORE1VAL FLOAT,
  TRUEPOS FLOAT, FALSEPOS FLOAT
) TABLESPACE DDATA;

create table classifier (
  CLASSIFIERID NUMBER(4) NOT NULL, CLASSIFIERNAME VARCHAR2(20) NOT NULL,
  COLNAME VARCHAR2(20) NOT NULL, CLASSPATH VARCHAR2(256) NOT NULL, SORTORDER NUMBER(1),
  ACTIVE NUMBER(1) DEFAULT 1
) TABLESPACE DDATA;

alter table classifier add primary key (classifierid);
create unique index ClassifierIDNameColName on Classifier (ClassifierID, ClassifierName, ColName);

alter table rocsetting add primary key (rocsettingid);
alter table rocbucketlocalclassifier add primary key (rocsettingid, starcount);

create sequence rocsetting_seq start with 100;

update rocbucketlocalclassifier set classifierid = 27, cutoffscoregte = 0.3 where rocsettingid = 1 and starcount = 1; 
update rocbucketlocalclassifier set classifierid = 27, cutoffscoregte = 0.6667 where rocsettingid = 1 and starcount = 2; 
update rocbucketlocalclassifier set classifierid = 27, cutoffscoregte = 1.063 where rocsettingid = 1 and starcount = 3;

alter table document add (lang varchar2(10) default 'english', fmt varchar2(10) default 'text');

/* Subject area */

create table SubjectArea (subjectareaid number(9), subjectname varchar2(200), subjectareastatus number(1) default 1);
create table CorpusSubject (corpusid number(9), subjectareaid number(9));
alter table corpussubject add foreign key (CorpusID) references Corpus (CorpusID) on delete cascade;
alter table SubjectArea add primary key (SubjectAreaId);
alter table corpussubject add foreign key (SubjectAreaID) references SubjectArea (SubjectAreaID) on delete cascade;
create sequence subjectarea_seq start with 1;
create trigger subjectarea_trigger before insert on subjectarea for each row when (new.subjectareaid is null)
  begin select subjectarea_seq.nextval into :new.subjectareaid from dual;
  end;
/

insert into repositorytype values (10, 'WOTI Index', 'WOTI');

Alter table Notification add primary key (AlertId);
create sequence alert_seq start with 1;
create trigger alert_trigger before insert on notification for each row when (new.alertid is null)
begin select alert_seq.nextval into :new.alertid from dual;
end;
/

/* new create index command */
create index DocFullText on Document(FullText) indextype is ctxsys.context
  parameters ('datastore ctxsys.file_datastore 
  filter ctxsys.inso_filter 
  format column fmt
  lexer global_lexer
  language column lang');

/* ************************** END OF DFI CHANGES ****************** */

Alter table ROCValidation add primary key (NodeId, DocumentId);

Alter table ROCValidation add foreign key (NodeId) references Node (NodeId) on delete cascade;
Alter table ROCValidation add foreign key (DocumentId) references Document (DocumentId) on delete cascade;
Alter table NodeValidation add foreign key (NodeId) references Node (NodeId) on delete cascade;

Alter table Nugget add primary key (NuggetId);
Alter table Snippet add primary key (SnippetId, NuggetId);
Alter table NodeDocumentSnippet add primary key (NodeId, DocumentId, SnippetId);

Alter table Snippet add foreign key (NuggetId) references Nugget (NuggetId) on delete cascade;
Alter table NodeDocumentSnippet add foreign key (NodeId) references Node (NodeId) on delete cascade;
Alter table NodeDocumentSnippet add foreign key (DocumentId) references Document (DocumentId) on delete cascade;

Alter table Repository add primary key (RepositoryId);
Alter table RepositoryType add primary key (RepositoryTypeId);
Alter table Repository add foreign key (RepositoryTypeId) references RepositoryType (RepositoryTypeId) on delete cascade;
Alter table WatchFolder add foreign key (RepositoryId) references Repository (RepositoryId) on delete cascade;
Alter table DocumentSecurity add foreign key (DocumentId) references Document (DocumentId) on delete cascade;

Alter table CustomDocumentAttributes add primary key (AttributeId);
Alter table CustomDocument add foreign key (DocumentId) references Document (DocumentId) on delete cascade;
Alter table CustomDocument add foreign key (AttributeId) references CustomDocumentAttributes (AttributeId) on delete cascade;

Alter table Document add foreign key (RepositoryId) references Repository (RepositoryId) on delete cascade;

Alter table Thesaurus add primary key (ThesaurusId);
Alter table ThesaurusWord add primary key (WordId);
Alter table CorpusThesaurus add primary key (CorpusId, ThesaurusId);
Alter table CorpusThesaurus add foreign key(ThesaurusId) references Thesaurus (ThesaurusId) on delete cascade;
Alter table CorpusThesaurus add foreign key(CorpusId) references Corpus (CorpusId) on delete cascade;
Alter table ThesaurusRelations add foreign key(ThesaurusId) references Thesaurus (ThesaurusId) on delete cascade;
Alter table ThesaurusRelations add foreign key(WordAnchor) references ThesaurusWord (WordId) on delete cascade;
Alter table ThesaurusRelations add foreign key(WordRelated) references ThesaurusWord (WordId) on delete cascade;

create sequence snippet_seq start with 1;
create sequence nugget_seq start with 1;

/* ROC Indexes */
create index ROC on Node (CorpusID);
create index ROC2 on NodeDocument (NodeID);
create index ROCv on NodeDocument (ROCValidation);

create index ThresIn on ThesaurusRelations (WordAnchor, Relationship);
create unique index TR on thesaurusrelations (thesaurusid, wordanchor, wordrelated);
create index Din on NodeDocument (DocumentID, NodeID, Score1) tablespace ddata;

create sequence thes_seq start with 1;
create trigger thes_trigger before insert on thesaurus for each row when (new.thesaurusid is null)
begin select thes_seq.nextval into :new.thesaurusid from dual;
end;
/
create sequence thesword_seq start with 1;
create trigger thesword_trigger before insert on thesaurusword for each row when (new.wordid is null)
begin select thesword_seq.nextval into :new.wordid from dual;
end;
/

/* These triggers maintain referential integrity among search tables */
create or replace trigger NodeSearch
  after insert or update on Node
  for each row 
  begin
     update Node set NodeLTitle = lower(' '||:new.nodetitle||' ') where nodeid = :new.nodeid; 
  end;
/

Alter table Signature add primary key (NodeId);
Alter table Channel add primary key (ChannelId);
Alter table UpdateLog add primary key (UpdateLogId);
Alter table Genre add primary key (GenreId);
Alter table CorpusGenre add primary key (GenreId,CorpusId);
Alter table Corpus add primary key (CorpusId);
Alter table NodeDocument add primary key (NodeId,DocumentId);
Alter table Node add primary key (NodeId);
Alter table Client add primary key (ClientId);
Alter table DocumentChannel add primary key (DocumentId,ChannelId,NodeId);
Alter table NodeChannel add primary key (NodeId,ChannelId);
Alter table Document add primary key (DocumentId);

Alter table Expert add foreign key(NodeId) references Node (NodeId) on delete cascade;

Alter table CorpusAccess add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
Alter table NodeChannel add   foreign key(ChannelId) references Channel (ChannelId)  on delete cascade;
Alter table DocumentChannel add   foreign key(ChannelId) references Channel (ChannelId)  on delete cascade;
Alter table DocumentChannel add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table CorpusGenre add   foreign key(GenreId) references Genre (GenreId)  on delete cascade;
Alter table Node add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
Alter table CorpusGenre add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
Alter table Signature add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table NodeChannel add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table NodeDocument add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table NodeDocument add   foreign key(DocumentId) references Document (DocumentId)  on delete cascade;
Alter table UpdateLog add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table EditorLog add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table SubscriberNode add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table Blacklist add   foreign key(CorpusId) references Corpus (CorpusId)  on delete cascade;
Alter table Blacklist add   foreign key(NodeId) references Node (NodeId)  on delete cascade;
Alter table Node add   foreign key(ClientId) references Client (ClientId)  on delete cascade;
Alter table Genre add   foreign key(ClientId) references Client (ClientId)  on delete cascade;
Alter table Corpus add   foreign key(ClientId) references Client (ClientId)  on delete cascade;
Alter table DocumentChannel add   foreign key(DocumentId) references Document (DocumentId)  on delete cascade;

create view NodeDocumentSignature as
    select D.DocumentID, D.DocTitle, D.DocURL, S.SignatureWord, S.SIGNATUREOCCURENCES
    from Document D, NodeDocument ND, Signature S
    where ND.NodeID = S.NodeID and D.DocumentID = ND.DocumentID;

create index DocumentIDTitle on Document (DocumentID, DocTitle) tablespace RDATA;

create unique index SigInd on Signature (NodeID, SignatureWord, SignatureOccurences);

create sequence groupaccess_seq start with 1;

create trigger ga_trigger before insert on groupaccess for each row when (new.groupaccessid is null)
begin select groupaccess_seq.nextval into :new.groupaccessid from dual;
end;
/

create sequence blacklist_seq start with 1;

create trigger bl_trigger before insert on blacklist for each row when (new.blacklistid is null)
begin select blacklist_seq.nextval into :new.blacklistid from dual;
end;
/

create sequence subscriber_seq start with 1;

create trigger sb_trigger before insert on subscriber for each row when (new.userid is null)
begin select subscriber_seq.nextval into :new.userid from dual;
end;
/

create sequence genre_seq start with 15;

create trigger genre_trigger before insert on genre for each row when (new.genreid is null)
begin select genre_seq.nextval into :new.genreid from dual;
end;
/

create sequence corpus_seq start with 1;

create trigger corp_trigger before insert on corpus for each row when (new.corpusid is null)
begin select corpus_seq.nextval into :new.corpusid from dual;
end;
/

/****** TRIGGERS FOR SERVER DATABASE ******/
create trigger channel_trigger before insert on channel for each row when (new.channelid is null)
begin select channel_seq.nextval into :new.channelid from dual;
end;
/

create trigger searchcategory_trigger before insert on searchcategory for each row when (new.searchcategoryid is null)
begin select searchcategory_seq.nextval into :new.searchcategoryid from dual;
end;
/

create trigger msmetachannel_trigger before insert on msmetachannel for each row when (new.msid is null)
begin select msmetachannel_seq.nextval into :new.msid from dual;
end;
/

/* Update trigger for Channel */

Create Trigger tu_Channel after update   
of ChannelId
on  Channel 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child NodeChannel update when parent Channel changed */
     if (:old_upd.ChannelId != :new_upd.ChannelId)  then
     	begin
     	update NodeChannel
     	set 	ChannelId = :new_upd.ChannelId
     	where 	NodeChannel.ChannelId = :old_upd.ChannelId ;
     	end;
     end if;
     
     /* cascade child DocumentChannel update when parent Channel changed */
     if (:old_upd.ChannelId != :new_upd.ChannelId)  then
     	begin
     	update DocumentChannel
     	set 	ChannelId = :new_upd.ChannelId
     	where 	DocumentChannel.ChannelId = :old_upd.ChannelId ;
     	end;
     end if;
     
      
     
end;
/
/* Delete trigger for Genre */

Create Trigger tu_deletegenre before delete
on Genre
for each row
begin
      update Document set GenreId = 0 where Document.GenreID = :old.Genreid;
end;
/

/* Update trigger for Genre */

Create Trigger tu_Genre after update   
of GenreId,ClientId
on  Genre 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Document update when parent Genre changed */
     if (:old_upd.GenreId != :new_upd.GenreId)  then
     	begin
     	update Document
     	set 	GenreId = :new_upd.GenreId
     	where 	Document.GenreId = :old_upd.GenreId ;
     	end;
     end if;
     
     /* cascade child CorpusGenre update when parent Genre changed */
     if (:old_upd.GenreId != :new_upd.GenreId)  then
     	begin
     	update CorpusGenre
     	set 	GenreId = :new_upd.GenreId
     	where 	CorpusGenre.GenreId = :old_upd.GenreId ;
     	end;
     end if;
     
end;
/
/* Update trigger for Corpus */

Create Trigger tu_Corpus after update   
of CorpusId,ClientId
on  Corpus 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Node update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update Node
     	set 	CorpusId = :new_upd.CorpusId
     	where 	Node.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child CorpusGenre update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update CorpusGenre
     	set 	CorpusId = :new_upd.CorpusId
     	where 	CorpusGenre.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
     /* cascade child CorpusAccess update when parent Corpus changed */
     if (:old_upd.CorpusId != :new_upd.CorpusId)  then
     	begin
     	update CorpusAccess
     	set 	CorpusId = :new_upd.CorpusId
     	where 	CorpusAccess.CorpusId = :old_upd.CorpusId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Node */

Create Trigger tu_Node after update   
of NodeId,CorpusId,ClientId
on  Node 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Signature update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update Signature
     	set 	NodeId = :new_upd.NodeId
     	where 	Signature.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child NodeChannel update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update NodeChannel
     	set 	NodeId = :new_upd.NodeId
     	where 	NodeChannel.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child NodeDocument update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update NodeDocument
     	set 	NodeId = :new_upd.NodeId
     	where 	NodeDocument.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child UpdateLog update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update UpdateLog
     	set 	NodeId = :new_upd.NodeId
     	where 	UpdateLog.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child EditorLog update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update EditorLog
     	set 	NodeId = :new_upd.NodeId
     	where 	EditorLog.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child SubscriberNode update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update SubscriberNode
     	set 	NodeId = :new_upd.NodeId
     	where 	SubscriberNode.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child DocumentChannel update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update DocumentChannel
     	set 	NodeId = :new_upd.NodeId
     	where 	DocumentChannel.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
     /* cascade child Blacklist update when parent Node changed */
     if (:old_upd.NodeId != :new_upd.NodeId)  then
     	begin
     	update Blacklist
     	set 	NodeId = :new_upd.NodeId
     	where 	Blacklist.NodeId = :old_upd.NodeId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Client */

Create Trigger tu_Client after update   
of ClientId
on  Client 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child Node update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update Node
     	set 	ClientId = :new_upd.ClientId
     	where 	Node.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
     /* cascade child Blacklist update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update Blacklist
     	set 	ClientId = :new_upd.ClientId
     	where 	Blacklist.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
     /* cascade child Genre update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update Genre
     	set 	ClientId = :new_upd.ClientId
     	where 	Genre.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
     /* cascade child Corpus update when parent Client changed */
     if (:old_upd.ClientId != :new_upd.ClientId)  then
     	begin
     	update Corpus
     	set 	ClientId = :new_upd.ClientId
     	where 	Corpus.ClientId = :old_upd.ClientId ;
     	end;
     end if;
     
      
     
end;
/
/* Update trigger for Document */

Create Trigger tu_Document after update   
of DocumentId,GenreId
on  Document 
referencing new as new_upd old as old_upd for each row
declare numrows integer;
begin
     /* cascade child NodeDocument update when parent Document changed */
     if (:old_upd.DocumentId != :new_upd.DocumentId)  then
     	begin
     	update NodeDocument
     	set 	DocumentId = :new_upd.DocumentId
     	where 	NodeDocument.DocumentId = :old_upd.DocumentId ;
     	end;
     end if;
     
     /* cascade child DocumentChannel update when parent Document changed */
     if (:old_upd.DocumentId != :new_upd.DocumentId)  then
     	begin
     	update DocumentChannel
     	set 	DocumentId = :new_upd.DocumentId
     	where 	DocumentChannel.DocumentId = :old_upd.DocumentId ;
     	end;
     end if;
     
      
     
end;
/

