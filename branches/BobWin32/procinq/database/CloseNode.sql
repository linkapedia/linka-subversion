/* The CloseNode procedure is invoked by a client machine when it */
/* has completed processing of a given node.   This procedure     */
/* will update the "datescanned" and "dateupdated" columns of the */
/* Node table and put an entry into the UpdateLog to indicate     */
/* that the node has been closed.                                 */
/*                                                                */
/* Also do cleanup work: Reset any nodes checked out longer than  */
/* 3 hours..                                                      */
CREATE OR REPLACE PROCEDURE CloseNode (
	NodeIdIn IN Node.NodeId%TYPE,
	MachineNameIn IN Varchar2)
IS
BEGIN
DELETE FROM NodeCrawlQueue WHERE nodeid = NodeIdIn;
UPDATE node SET DateUpdated = SYSDATE WHERE nodeid = NodeIdIn;
UPDATE node SET DateScanned = SYSDATE WHERE nodeid = NodeIdIn;
COMMIT WORK;

-- Now flag old nodes that have been checked out more than 3 hours
UPDATE NodeCrawlQueue SET Status = 3 WHERE Status = 2 AND NodeID in 
   (SELECT NodeID FROM Node WHERE (to_char(SYSDATE, 'yyyymmddhh24') - to_char(datescanned, 'yyyymmddhh24')) > 1);
COMMIT WORK;

-- If there are no more nodes to run in this corpus, remove it from the priority queue
DELETE FROM CorpusPriority WHERE corpusid NOT IN 
   (SELECT DISTINCT(corpusid) FROM NodeCrawlQueue WHERE Status IN (1,3));
COMMIT WORK;

END CloseNode;
/
