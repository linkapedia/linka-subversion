CREATE OR REPLACE PROCEDURE GetTextStream (documentid_in IN Document.DocumentId%TYPE, words OUT CLOB)
IS 
BEGIN
DECLARE
  ri rowid;

  BEGIN
  SELECT rowid INTO ri FROM DOCUMENT WHERE documentid = documentid_in;
  ctx_doc.set_key_type('ROWID');
     ctx_doc.filter(
         index_name => 'DocFullText',
         textkey    => ri,
         restab     => words,
         plaintext  => TRUE);
  END;
COMMIT WORK;
END GetTextStream;
/
show errors;
