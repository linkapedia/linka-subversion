The MESH hierarchy was created by doing the following:

1) Creating a custom MeSH database schema conforming to the standard attributes (mesh.sql)
2) Parsing the latest desc2004.xml and inserting data into the schema (mesh.pl)
3) Crawl the MeSH web site to extract hierarchy information (Spider.java, Hyperlink.java)
4) Extract the hierarchy information found and insert as nodes in the database (create-mesh.pl)
5) Finally, take information from the schema and add the node source to existing nodes (relats.pl)

