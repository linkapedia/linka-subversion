#!C:\PERL\BIN\PERL

use DBI;

local (%concept_hash); 
local (%node_conversion);
local (%node_title);

local ($dbh) = DBI->connect("dbi:Oracle:dallas", "sbooks", "racer9") || die "db connect failed: $!\n";

# load all the top level nodes
local ($q) = "select c.cid, d.name from descriptionrecord d, concept c, descriptorrecordconcept dc ".
             "where dc.drid = d.drid and dc.cid = c.cid and lower(c.name) = lower(d.name)";
local ($sth) = $dbh->prepare($q) || die "query failed: $q\n";
$sth->execute() || die "query failed: $q\n";

local ($loop) = 481128;

while (($id, $name) = $sth->fetchrow_array()) {
   $name =~ s/\b(\w)/\U$1/g; # capitalize the first letter of every word

   $loop++; 
   $node_conversion{$id} = $loop;   
   $node_title{$loop} = $name;  
   $concept_hash{$loop} = "481128"; 
}

$sth->finish(); undef $sth;

print "\n".($loop-481128)." top level relationships created.\n";

$q = "select c1.cid, c1.name, c2.cid, c2.name from concept c1, concept c2, conceptrelationship c ".
     "where c1.cid = c.cid and c2.cid = c.c2id and c.type = 'NRW'";
local ($sth) = $dbh->prepare($q) || die "query failed: $q\n";
$sth->execute() || die "query failed: $q\n";

local ($dups) = 0;

while (($id, $name, $tid, $tname) = $sth->fetchrow_array()) {
   $name =~ s/\b(\w)/\U$1/g; # capitalize the first letter of every word
   $tname =~ s/\b(\w)/\U$1/g; # capitalize the first letter of every word

   if (defined($node_conversion{$id})) { $dups++; }
   else {
      $loop++; 

      local ($id1) = $loop;
      local ($id2) = "";

      if (!defined($node_conversion{$tid})) {
         $loop++; $id2 = $loop;

         $node_conversion{$tid} = $id2;   
         $node_title{$id2} = $tname;  
         $concept_hash{$id2} = $id1; 
      } 

      $node_conversion{$id} = $id1;   
      $node_title{$id1} = $name;  
   }
}

print "\n".($loop-481128)." total relationships created.\n";
print "$dups duplicate relationships ignored.\n\n";

foreach $id (keys %node_conversion) {
   my ($nid) = $node_conversion{$id};

   print "cid: $id nid: $nid name: ".$node_title{$nid}." parent: ".$concept_hash{$nid}."\n";
}

$dbh->disconnect();

1;
