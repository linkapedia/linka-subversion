#!C:\PERL\BIN\PERL

use DBI;

open (MESH, "desc2004.xml");
local ($dbh) = DBI->connect("dbi:Oracle:dallas", "sbooks", "racer9") || die "db connect failed: $!\n";

# local variables
local ($DescriptorUI, $DescriptorName, $DateCreated, $DateRevised, $DateEstablished);
local ($ActiveMeshYearList, $QualifierUI, $QualifierName, $HistoryNote, $OnlineNote, $MeshNote);

local ($DnameForthcoming) = 0; local ($DcreatedForthcoming) = 0; local ($DrevisedForthcoming) = 0;
local ($DestablishedForthcoming) = 0; local ($DactiveForthcoming) = 0; local ($activeDescID) = -1;

local ($QnameForthcoming) = 0;
local ($CnameForthcoming) = 0;
local ($TnameForthcoming) = 0;

local ($ConceptUI, $ConceptName, $ConceptUML, $ConceptCASN, $ConceptReg, $ScopeNote, $ConceptRelat);
local ($conceptID) = -1;

local ($SemanticUI); 

local ($TermUI, $TermName);

# hash tables
local (%qualifiers); local (%treenumbers); local (%previousindexing); local (%concepts);
local (%semantics); local (%pharma); local (%terms);

# to do at the end
local (@to_do);

$loop = 0;
while (<MESH>) {
   $line = $_; 

   # Descriptor Records
   if ($line =~ /<DescriptorUI>(.*?)<\/DescriptorUI>/) { $DescriptorUI = $1; }
   if ($line =~ /<DescriptorName>/) { $DnameForthcoming = 1; }
   if ($line =~ /<String>(.*?)<\/String>/) {
      if ($DnameForthcoming == 1) { $DnameForthcoming = 0; $DescriptorName = $1; }
      if ($QnameForthcoming == 1) { $QnameForthcoming = 0; $QualifierName = $1; }
      if ($CnameForthcoming == 1) { $CnameForthcoming = 0; $ConceptName = $1; }
      if ($TnameForthcoming == 1) { $TnameForthcoming = 0; $TermName = $1; }
   }
   if ($line =~ /<DateCreated>/) { $DcreatedForthcoming = 1; }
   if ($line =~ /<DateRevised>/) { $DrevisedForthcoming = 1; }
   if ($line =~ /<DateEstablished>/) { $DestablishedForthcoming = 1; }
   if ($line =~ /<ActiveMeSHYearList>/) { $DactiveForthcoming = 1; }
   if ($line =~ /<Year>(.*?)<\/Year>/) { 
      if ($DcreatedForthcoming == 1) { $DateCreated = $1; }
      if ($DrevisedForthcoming == 1) { $DateRevised = $1; }
      if ($DestablishedForthcoming == 1) { $DateEstablished = $1; }
      if ($DactiveForthcoming == 1) { $DactiveForthcoming = 0; $ActiveMeshYearList = $1; }
   } 
   if ($line =~ /<Month>(.*?)<\/Month>/) { 
      if ($DcreatedForthcoming == 1) { $DateCreated = $DateCreated."-".$1; }
      if ($DrevisedForthcoming == 1) { $DateRevised = $DateRevised."-".$1; }
      if ($DestablishedForthcoming == 1) { $DateEstablished = $DateEstablished."-".$1; }
   }
   if ($line =~ /<Day>(.*?)<\/Day>/) {
      if ($DcreatedForthcoming == 1) { $DcreatedForthcoming = 0; $DateCreated = $DateCreated."-".$1; }
      if ($DrevisedForthcoming == 1) { $DrevisedForthcoming = 0; $DateRevised = $DateRevised."-".$1; }
      if ($DestablishedForthcoming == 1) { $DestablishedForthcoming = 0; $DateEstablished = $DateEstablished."-".$1; }
   }
   
   if ($line =~ /<HistoryNote>(.*)/) { $HistoryNote = $1; chomp($HistoryNote); }
   if ($line =~ /<OnlineNote>(.*)/) { $OnlineNote = $1; chomp($OnlineNote); }
   if ($line =~ /<PublicMeSHNote>(.*)/) { $MeshNote = $1; chomp($MeshNote); }

   # Signifies the end of a Descriptor Record
   if ($line =~ /<\/ActiveMeSHYearList>/) { $activeDescID = &writeDESC(); }

   # Allowable Qualifier
   if ($line =~ /<QualifierUI>(.*?)<\/QualifierUI>/) { $QualifierUI = $1; }
   if ($line =~ /<QualifierName>/) { $QnameForthcoming = 1; }
   
   # Signifies the end of a Qualifier
   if ($line =~ /<\/AllowableQualifier>/) { &writeQUAL($activeDescID); }

   # Previous Indexing List
   if ($line =~ /<PreviousIndexing>(.*?)<\/PreviousIndexing>/) { &writePREV($activeDescID, $1); }

   # TreeNumberList 
   if ($line =~ /<TreeNumber>(.*?)<\/TreeNumber>/) { &writeTREE($activeDescID, $1); }

   # Concepts!
   if ($line =~ /<ConceptUI>(.*?)<\/ConceptUI>/) { $ConceptUI = $1; }
   if ($line =~ /<ConceptName>/) { $CnameForthcoming = 1; }
   if ($line =~ /<ConceptUMLSUI>(.*?)<\/ConceptUMLSUI>/) { $ConceptUML = $1; }
   if ($line =~ /<CASN1Name>(.*?)<\/<CASN1Name>/) { $ConceptCASN = $1; }
   if ($line =~ /<RegistryNumber>(.*?)<\/RegistryNumber>/) { $ConceptReg = $1; }
   if ($line =~ /<ScopeNote>(.*)/) { $ScopeNote = $1; }
   if ($line =~ /<SemanticTypeList>/) { $conceptID = &writeCONC($activeDescID); } 
   if ($line =~ /<ConceptRelation RelationName=\"(.*?)\"/) { $ConceptRelat = $1; }

   # Semantic Type
   if ($line =~ /<SemanticTypeUI>(.*?)<\/SemanticTypeUI>/) { $SemanticUI = $1; }
   if ($line =~ /<SemanticTypeName>(.*?)<\/SemanticTypeName>/) { &writeSEM($conceptID, $1); }

   # Pharmacological Action
   if ($line =~ /<\/PharmacologicalAction>/) { &writePHARM($conceptID); }
   
   # Concept Relations
   if ($line =~ /<Concept.UI>(.*?)<\/Concept.UI>/) { &writeCONCRELAT($conceptID, $1); } 

   # Term 
   if ($line =~ /<TermUI>(.*?)<\/TermUI>/) { $TermUI = $1; $TnameForthcoming = 1; } 
   if ($line =~ /<\/Term>/) { &writeTERM($conceptID); }

   # End of Record (sew up relationships)
   if ($line =~ /<\/DescriptorRecord>/) { 
      &finishAndDiscard($activeDescID, $conceptID); $loop++;
    
      $activeDescID = ""; $conceptID = "";
   }
} 

print "\nFinishing out concept relationships on the TO_DO list ..\n";
foreach $query (@to_do) {
   print ".";
   $sth = $dbh->prepare($q) || die "query failed: $q\n";
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth; 
}

$dbh->disconnect();
close(MESH); 

sub writeDESC {
   print "Descriptor Record: $DescriptorName\n";

   $q = "select DescriptionRecordSeq.nextval from dual";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   my ($dID) = $sth->fetchrow_array(); $sth->finish(); undef $sth;

   $DescriptorName = &clean($DescriptorName);

   $q = "insert into DescriptionRecord (drID, Name, DateCreated, ActiveMeshYearList) ".
        "values ($dID, '$DescriptorName', to_date('$DateCreated', 'YYYY-MM-DD'), $ActiveMeshYearList)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

   $DescriptorUI = ""; $DescriptorName = ""; $DateCreated = ""; 
   $DateRevised = ""; $DateEstablished = ""; $ActiveMeshYearList = "";

   return $dID;
}

sub writeCONC {
   my ($dID) = $_[0];
   local ($cID) = "";

   print "  Concept Record: $ConceptName ($ConceptUML)\n";
 
   if (defined($concepts{$ConceptUI})) { $cID = $concepts{$ConceptUI}; }
   else {
      $q = "select ConceptSeq.nextval from dual";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $cID = $sth->fetchrow_array(); $sth->finish(); undef $sth;

      $concepts{$ConceptUI} = $cID;

      $ConceptName = &clean($ConceptName);
      $ScopeNote = &clean($ScopeNote);

      $q = "insert into Concept (cID, Name, UMLSUI, CASN1NAME, RegistryNumber, ScopeNote) ".
           "values ($cID, '$ConceptName', '$ConceptUML', '$ConceptCASN', '$ConceptReg', '$ScopeNote')";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sth->finish(); undef $sth;
   }

   $q = "insert into DescriptorRecordConcept ".
        "values ($dID, $cID)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

   $ConceptName = ""; $ConceptUML = ""; $ConceptCASN = ""; $ConceptReg = ""; $ScopeNote = "";
   
   return $cID;
}

sub writeCONCRELAT {
   my ($cID) = $_[0];
   my ($cUI) = $_[1];

   $q = "insert into ConceptRelationship ".
        "values ($cID, ".$concepts{$cUI}.", '$ConceptRelat')";

   if (!defined($concepts{$cUI})) { push(@to_do, $q); return; }

   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;   
}

sub writeTERM {
   my ($cID) = $_[0]; 
 
   local ($tID) = "";

   print "  Term Name: $TermName\n";

   if (defined($terms{$TermUI})) { $tID = $terms{$TermUI}; }
   else {   
      $q = "select TermsSeq.nextval from dual";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $tID = $sth->fetchrow_array(); $sth->finish(); undef $sth;

      $terms{$TermUI} = $tID;

      $TermName = &clean($TermName);

      $q = "insert into Terms (tID, UI, Name) ".
           "values ($tID, '$TermUI', '$TermName')";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sth->finish(); undef $sth;
   }

   $q = "insert into ConceptTerm ".
        "values ($cID, $tID)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

   $TermUI = ""; $TermName = ""; $DateCreated = ""; 
}

sub writeQUAL {
   my ($dID) = $_[0]; 
   local ($qID) = "";

   print "  Allowable Qualifier: $QualifierName\n";

   if (defined($qualifiers{$QualifierUI})) { $qID = $qualifiers{$QualifierUI}; }
   else {   
      $q = "select AllowableQualifierSeq.nextval from dual";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $qID = $sth->fetchrow_array(); $sth->finish(); undef $sth;

      $qualifiers{$QualifierUI} = $qID;

      $QualifierName = &clean($QualifierName);

      $q = "insert into AllowableQualifier (aqID, UI, Name) ".
           "values ($qID, '$QualifierUI', '$QualifierName')";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sth->finish(); undef $sth;
   }

   $q = "insert into DescriptorRecordAllowableQual ".
        "values ($dID, $qID)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

   $QualifierUI = ""; $QualifierName = ""; 
}

sub writePREV {
   my ($dID) = $_[0]; 
   my ($name) = $_[1];

   local ($piID) = "";

   print "  Previous Indexing: $name\n";

   if (defined($previousindexing{$name})) { $piID = $previousindexing{$name}; }
   else {
      $q = "select PreviousIndexingSeq.nextval from dual";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $piID = $sth->fetchrow_array(); $sth->finish(); undef $sth;

      $previousindexing{$name} = $piID;

      $name = &clean($name);

      $q = "insert into PreviousIndexing (piID, Name) ".
           "values ($piID, '$name')";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sth->finish(); undef $sth;
   }

   $q = "insert into DescriptorRecordPreviousIndex ".
        "values ($dID, $piID)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;
}

sub writePHARM {
   my ($cID) = $_[0]; 
 
   local ($pid) = "";

   print "  Pharma Action: $DescriptorName\n";

   if (defined($pharma{$DescriptorUI})) { $pID = $pharma{$DescriptorUI}; }
   else {   
      $q = "select PharmcologicalActionsSeq.nextval from dual";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $pID = $sth->fetchrow_array(); $sth->finish(); undef $sth;

      $pharma{$DescriptorUI} = $pID;

      $DescriptorName = &clean($DescriptorName);

      $q = "insert into PharmcologicalActions (paID, UI, Name) ".
           "values ($pID, '$DescriptorUI', '$DescriptorName')";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sth->finish(); undef $sth;
   }

   $q = "insert into ConceptPharmcologicalAction ".
        "values ($cID, $pID)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

   $DescriptorUI = ""; $DescriptorName = ""; 
}

sub writeTREE {
   my ($dID) = $_[0]; 
   my ($name) = $_[1];

   local ($tID);

   print "  Tree Number: $name\n";

   if (defined($treenumbers{$name})) { $tID = $treenumbers{$name}; }
   else {
      $q = "select TreeNumbersSeq.nextval from dual";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $tID = $sth->fetchrow_array(); $sth->finish(); undef $sth;

      $treenumbers{$name} = $tID;

      $name = &clean($name);

      $q = "insert into TreeNumbers (trID, TrNum) ".
           "values ($tID, '$name')";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sth->finish(); undef $sth;
   }

   $q = "insert into DescriptorRecordTreeNumber ".
        "values ($dID, $tID)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

}

sub writeSEM {
   my ($cID) = $_[0]; 
   my ($name) = $_[1];
 
   local ($sID) = "";

   print "  Semantic Type Name: $name\n";

   if (defined($semantics{$SemanticUI})) { $sID = $semantics{$SemanticUI}; }
   else {   
      $q = "select SemanticTypesSeq.nextval from dual";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sID = $sth->fetchrow_array(); $sth->finish(); undef $sth;

      $semantics{$SemanticUI} = $sID;

      $name = &clean($name);

      $q = "insert into SemanticTypes (stID, UI, Name) ".
           "values ($sID, '$SemanticUI', '$name')";
      $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
      $sth->execute || die "query failed: $q\n"; 
      $sth->finish(); undef $sth;
   }

   $q = "insert into ConceptSemanticTypes ".
        "values ($cID, $sID)";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

   $SemanticUI = "";
}

sub finishAndDiscard {
   my ($dID) = $_[0]; 
   my ($cID) = $_[1];

   $HistoryNote = &clean($HistoryNote);
   $OnlineNote = &clean($OnlineNote);
   $MeshNote = &clean($MeshNote);

   $q = "update DescriptionRecord set HistoryNote = '$HistoryNote',  ".
                                      "OnlineNote = '$OnlineNote', ".
				      "PublicMeshNote = '$MeshNote' ".
        "where drID = $dID";
   $sth = $dbh->prepare($q) || die "query failed: $q\n"; 
   $sth->execute || die "query failed: $q\n"; 
   $sth->finish(); undef $sth;

   $HistoryNote = "";
   $OnlineNote = "";
   $MeshNote = "";

   print "\n";
}

sub clean { 
   my ($word) = $_[0];

   $word =~ s/\&amp\;/\&/gi;
   $word =~ s/\'/\'\'/gi;
   $word =~ s/\&/\&\'\|\|\'/gi;

   return $word;
}

1;
