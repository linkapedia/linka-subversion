#!C:\PERL\BIN\PERL

use DBI;

local (%concept_hash); 
local (%node_conversion);
local (%node_title);

local ($dbh) = DBI->connect("dbi:Oracle:content", "sbooks", "racer9") || die "db connect failed: $!\n";

# load all the top level nodes
local ($q) = "select nodeid, lower(nodetitle) from node where corpusid = 47";
local ($sth) = $dbh->prepare($q) || die "query failed: $q\n";
$sth->execute() || die "query failed: $q\n";

while (($id, $title) = $sth->fetchrow_array()) {
   print "Node ID: $id Title: $title\n";
   
   local ($nodesource) = "";

   $title =~ s/\'/\'\'/gi;
   $title =~ s/\&/\&\'\|\|\'/gi;

   $q = "select cid, scopenote from concept where lower(name) = '$title'";
   local ($sth2) = $dbh->prepare($q) || die "query failed: $q\n";
   $sth2->execute() || die "query failed: $q\n";

   local ($cid, $note) = $sth2->fetchrow_array(); $sth2->finish(); undef $sth2;
   $nodesource = $nodesource . $note . "\n";

   $q = "select p.name from pharmcologicalActions p, ConceptPharmcologicalAction cpa ".
        "where p.paid = cpa.paid and cpa.cid = $cid";
   local ($sth2) = $dbh->prepare($q) || die "query failed: $q\n";
   $sth2->execute() || die "query failed: $q\n";

   while ($paname = $sth2->fetchrow_array()) { $nodesource = $nodesource . $paname . " "; }
   $sth2->finish(); undef $sth2;

   $q = "select t.name from Terms t, ConceptTerm cpa ".
        "where t.tid = cpa.tid and cpa.cid = $cid";
   local ($sth2) = $dbh->prepare($q) || die "query failed: $q\n";
   $sth2->execute() || die "query failed: $q\n";

   while ($term = $sth2->fetchrow_array()) { $nodesource = $nodesource . $term . " "; }
   $sth2->finish(); undef $sth2;
   $nodesource = $nodesource . "\n";

   $q = "select s.name from SemanticTypes s, ConceptSemanticTypes cpa ".
        "where s.stid = cpa.stid and cpa.cid = $cid";
   local ($sth2) = $dbh->prepare($q) || die "query failed: $q\n";
   $sth2->execute() || die "query failed: $q\n";

   while ($stname = $sth2->fetchrow_array()) { $nodesource = $nodesource . $stname . " "; }
   $sth2->finish(); undef $sth2;

   print "$nodesource\n\n";
}


$sth->finish();
$dbh->disconnect();

1;
