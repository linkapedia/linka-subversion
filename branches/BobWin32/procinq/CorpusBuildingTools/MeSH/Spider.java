import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;
import java.text.SimpleDateFormat;

public class Spider
{
    private static Hashtable htVisited = new Hashtable();
    private static int NextID = 481129;
    private static PrintWriter dataout;

    public static Vector[] v = new Vector[10];

    // main function
    public static void main(String[] args) {
        // add all the level 1's to the mix first
        v[0] = new Vector();

        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/A", "Anatomy", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/B", "Organisms", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/C", "Diseases", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/D", "Chemicals and Drugs", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/F", "Psychiatry and Psychology", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/G", "Biological Sciences", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/H", "Physical Sciences", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/I", "Anthropology, Education, Sociology and Social Phenomena", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/J", "Technology and Food and Beverages", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/K", "Humanities", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/L", "Information Science", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/M", "Persons", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/N", "Health Care", NextID++, 1));
        v[0].add(new Hyperlink("http://fred.hmc.psu.edu/ds/retrieve/fred/meshdescriptor/Z", "Geographic Locations", NextID++, 1));


        try { dataout = new PrintWriter(new FileWriter("C:/temp/mesh.sql")); }
        catch (Exception e) { e.printStackTrace(System.out); return; }

        // now crawl each of the level 1's, add all of them, then add the level 2's
        for (int i = 0; i < 9; i++) {
            if ((v[i] != null) && (v[i].size() > 0)) { process(i); }
            else { System.out.println("Cannot process level "+i+" .. no results?"); }
        }

        dataout.close();
    }

    private static void process (int level) {
        v[level+1] = new Vector();

        for (int i = 0; i < v[level].size(); i++) {
            Hyperlink link = (Hyperlink) v[level].elementAt(i);
            String data = getData(link.getURL());
            //data = data.replaceAll("\\r\\n", " ");
            //data = data.replaceAll("\\n", " ");

            dataout.println(link.toString());
            System.out.println(link.toString());

            int start = data.lastIndexOf("Child Terms");
            int end = data.lastIndexOf("See Also");

            if (start > end) {
                try {
                    PrintWriter out = new PrintWriter(new FileWriter("C:/temp/tmp.txt"));
                    out.println(data);
                    out.close();
                } catch (Exception e) { e.printStackTrace(System.err); }
                System.out.println("Start position is greater than end position?? Term: "+link.getTitle());
            }

            if ((start == -1) || (end == -1)) {
                try {
                    PrintWriter out = new PrintWriter(new FileWriter("C:/temp/tmp.txt"));
                    out.println(data);
                    out.close();
                } catch (Exception e) { e.printStackTrace(System.err); }
                System.out.println("Can't find the terms?? WTF?? Term: "+link.getTitle());

            }

            try { data = data.substring(start, end); }
            catch (Exception e) {
                e.printStackTrace(System.err);
                return;
            }

            if (data.length() > 0) {
                Vector vChildren = extractHyperTextLinks(data, link);

                for (int j = 0; j < vChildren.size(); j++) {
                    Hyperlink child = (Hyperlink) vChildren.elementAt(j);
                    //System.out.println("  .. found link: "+child.getURL());
                    child.addParent(link.getID());
                    v[level+1].add(child);
                }
            }
        }

        System.out.println("\nLevel "+(level+1)+" size is: "+v[level+1].size());
        System.out.println("Finished processing level "+(level+1)+"\n");
    }

    // given a URL, retrieve the HTML and return it
    private static String getData (String URL) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");

        try {
            URL myURL = new URL(URL);
            httpCon = (HttpURLConnection) myURL.openConnection();
            //httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            DataInputStream dis = new DataInputStream(new BufferedInputStream(is));

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "html";

            // NOTE
            if ((httpCon.getContentType().equals("text/html"))) {
                String inputLine; String s = "";
                while ((inputLine = dis.readLine()) != null) { s = s + inputLine; }

                long lEnd = System.currentTimeMillis() - lStart;

                return s;
            } else { return "-1"; }

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms. : "+e.toString());
            e.printStackTrace(System.out);
            return "";
        }
        finally { httpCon.disconnect(); }
    }

    // extract all HTML hyperlinks from a given document and return in a vector
    public static Vector extractHyperTextLinks(String page, Hyperlink h) {
        int lastPosition = 0;           // position of "http:" substring in page
        Vector vHyperlinks = new Vector();
        String rootURL = h.getURL();

        Pattern p = Pattern.compile("\\s*href\\s*=\\s*\"*?([^>\" ]+)\"*?.*?>{1}?(.*?)(</a>{1}?)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(page); boolean bResult = m.find();

        while (bResult) {
            String sURL = m.group(1);
            String sTitle = m.group(2);

            if ((!sURL.toLowerCase().startsWith("http")) && (!sURL.startsWith("/"))) {
                if ((getDirectory(rootURL)).endsWith("/")) {
                    sURL = getDirectory(rootURL)+sURL;
                } else { sURL = getDirectory(rootURL)+"/"+sURL; }
            }
            if (sURL.startsWith("/")) {
                if ((getBase(rootURL)).endsWith("/")) {
                    sURL = getBaseWithoutSlash(rootURL)+sURL;
                } else { sURL = getBase(rootURL)+sURL; }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf("#");
            if (iPound != -1) { sURL = sURL.substring(0, iPound); }

            try {
                URI u = new URI(sURL);
                u = u.normalize();
                sURL = u.toString();
            } catch (Exception e) {
                System.out.println("Could not normalize URL: "+sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll(new Character((char)0).toString(), "");

             if (sURL.indexOf("meshdescriptor") != -1)
                 vHyperlinks.add(new Hyperlink(sURL, sTitle, NextID++, h.getLevel()+1));
            bResult = m.find();
        }
        return vHyperlinks;
    }

    // return the URL directory that this file was found in on the web server
    public static String getDirectory (String URL) {
        while (URL.endsWith("/")) { return URL; }
        int index = URL.lastIndexOf('/');

        return URL.substring(0, index);
    }
    // return the base URL (host)
    public static String getBase (String URL) {
        int index = URL.indexOf('/', 7);

        return URL.substring(0, index);
    }

    public static String getBaseWithoutSlash (String URL) {
        int index = URL.indexOf('/', 7);

        return URL.substring(0, index-1);
    }

    // mark or check if page has already been spidered
    public static void markVisited (String URL) { htVisited.put(URL.toLowerCase(), "1"); }
    public static boolean alreadyVisited (String URL) { if (htVisited.containsKey(URL.toLowerCase())) { return true; } return false; }

    // look at the document extension, make sure it's not an image file
    public static boolean isImage (String URL) {
        if (URL.toLowerCase().endsWith(".jpg")) { return true; }
        if (URL.toLowerCase().endsWith(".gif")) { return true; }
        if (URL.toLowerCase().endsWith(".jpeg")) { return true; }
        if (URL.toLowerCase().endsWith(".bmp")) { return true; }
        if (URL.toLowerCase().endsWith(".wav")) { return true; }
        if (URL.toLowerCase().endsWith(".css")) { return true; }
        if (URL.toLowerCase().endsWith(".xml")) { return true; }
        if (URL.indexOf("javascript") != -1) { return true; }
        if (URL.indexOf("mailto") != -1) { return true; }
        if (URL.indexOf("..") != -1) { System.out.println("Found URL: "+URL+" has relative paths, discarding.."); return true; }
        return false;
    }

    // HEAD request the URL and return the last modified date, if applicable, style: 1976-01-05 13:50:08
    public String doHeadRequest (String URL) {
        HttpURLConnection httpCon = null;

        try {
            URL myURL = new URL(URL);
            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestMethod("HEAD");

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            long changed = httpCon.getLastModified();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmss");
            return sdf.format(new Date(changed));

        } catch (Exception e) {
            System.out.println("HTTP HEAD request failure: "+e.toString());
            return "";
        }
        finally { httpCon.disconnect(); }
    }
}
