import java.text.DecimalFormat;
import java.util.Vector;

public class Hyperlink {
    private String URL;
    private String Title;
    private int ID;
    private int Parent = -1;
    private int Level;

    public Hyperlink() {}
    public Hyperlink (String URL, String Title, int ID, int Level) {
        this.URL = URL; this.Title = Title; this.ID = ID; this.Level = Level; }

    public String getURL() { return URL; }
    public String getTitle() { return Title; }
    public int getID() { return ID; }
    public int getLevel() { return Level; }
    public int getParent() { return Parent; }

    public void setURL(String URL) { this.URL = URL; }
    public void setTitle(String Title) { this.Title = Title; }
    public void setLevel(int Level) { this.Level = Level; }
    public void setID(int ID) { this.ID = ID; }

    public void addParent(int ID) { this.Parent = ID; }

    public String toString() {
        return ID+","+Title+","+Parent+","+Level;
    }
}
