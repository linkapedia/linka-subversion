#!/usr/bin/perl
#
# ODP.PL
#
# This program processes the Open Directory RDF format
# and creates the file structure needed in order to import
# it into a corpus.
#
# After the RDF file is processed, web sites are processed
# in order and appended to the filesystem for signature
# generation.
#
# - Written by Michael A. Puscar, 5/8/2001
#
# Parameters:
#    - name             Name of the top level category
#    - regex            Regex pattern, usually the top category

use LWP::UserAgent;		# Spidering agent
use Getopt::Long; 		# Input arguments

# Default values for input parameters
local ($name) = "Open Directory: Arts";
local ($regex) = "Top/Arts/Celebrities";

# Input arguments
&GetOptions ("name=s" => \$name,
             "regex=s" => \$regex);

# These are my global variables (please, no mean words)
local (%node_hash); local ($node); local (@urls);
local ($open) = 0; local ($title); local ($parent); local ($niwp);

# RDF file to be processed
open (FILE, "content.rdf.u8");

# Looking for only four items: 
#   -- open topic tag
#   -- cat id
#   -- close topic tag
#   -- links for an open topic

# Node_Hash contains the nodes with Title, Filename pairs
# Set the parent now.
$slash = "/";
$node_hash{$name} = $slash;

# Begin processing the file
while ($line = <FILE>) { 
    # If the line contains /TOPIC, close the topic.
    if ($line =~ /<\/Topic>/) { $open = 0; }
    
    # Extract the catalog identifier
    if (($line =~ /<catid>(.*)<\/catid>/) && ($open == 1)) {
        undef $parent; my $parent = "";
	# Extract the title
	if ($title =~ /(.*)\/(.*)/) { 
	    $parent = $1; 
	} else { $parent = $name; }
	undef $pfile; my $pfile = $node_hash{$parent};
	
	# Build filename path
	$niwp = 1;
	undef $niwp_string; my $niwp_string = sprintf ("%3.3d", $niwp);
	$slash = $pfile.$niwp_string."/";
	while (-e "/tmp/".$slash) {
	    $niwp++;
	    undef $niwp_string; my $niwp_string = sprintf ("%3.3d", $niwp);
	    $slash = $pfile.$niwp_string."/";
	}

	# Store in the hash table	
	$node_hash{$title} = $slash;
	my $filename = "/tmp/".$slash."000.html";
        mkdir ("/tmp/$slash") || warn "Error creating /tmp/".$slash.": $!\n";
	undef $tmp; local $tmp = $title;
	if ($tmp =~ /(.*)\/(.*)/i) { $tmp = $2; }
	$tmp =~ s/_/\ /gi;
	open(FILE2, ">>$filename"); print FILE2 "<HTML><TITLE>$tmp</TITLE>\n"; close(FILE2);
    }
    # Open the topic if new topic
    if ($line =~ /<Topic R\:id=\"(.*)\">/i) {
	$title = $1; if ($title =~ /$regex/i) { $open = 1; }
    }
    # This is the denotation for a link.   Register it for later processing.
    if (($line =~ /<link r\:resource=\"(.*)\"/) && ($open == 1)) { 
	&register_url($1, "/tmp/".$node_hash{$title});
    }
}
close (FILE);
undef %node_hash; undef $slash; undef $open; undef $title; undef $parent; undef $niwp;

print "Initial filesystem stored.\n";

# Push a URL/filename combination onto the array stack (queue) for later processing
sub register_url {
    my ($url, $filename) = @_;
    open(REGISTER, ">>data.txt"); print REGISTER "$url||$filename\n"; close(REGISTER);
    print "$url ($filename)\n";
    
    return 1;
}

1;

