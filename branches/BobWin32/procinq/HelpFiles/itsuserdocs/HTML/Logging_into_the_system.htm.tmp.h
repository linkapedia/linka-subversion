<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<META NAME="GENERATOR" CONTENT="Solutionsoft HelpBreeze JavaHelp Edition">
<TITLE>Logging into the system</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF=../javahelp.css>
</HEAD>
<BODY BGCOLOR=#ffffff>

<H1>Logging into the system</H1>
<P>After starting the Editor's desktop client, you will see 
a login screen like:</P>
<P>&nbsp;</P>
<P><IMG height=210 hspace=0 src="../Images/loginscreen.gif" width=350 
border=0></P>
<P><STRONG>Procedure heading</STRONG> 
 </P>
<OL>
    <LI>step 1 
    <LI>step 2 
    <LI>step 3</LI></OL>
</BODY>
</HTML>