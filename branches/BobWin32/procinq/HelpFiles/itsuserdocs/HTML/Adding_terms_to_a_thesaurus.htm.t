<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<META NAME="GENERATOR" CONTENT="Solutionsoft HelpBreeze JavaHelp Edition">
<TITLE>Adding terms to a thesaurus</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF=../javahelp.css>
</HEAD>
<BODY BGCOLOR=#ffffff>
<H1>Adding terms to a thesaurus</H1>
<P>Upon opening a thesaurus, you will see the followign screen:</P>
<P>&nbsp;</P>
<P><IMG height=459 hspace=0 src="../Images/emptythes.GIF" width=759 
border=0></P>
<P>&nbsp;</P>
<P>For our first thesaurus entry we will create a similar 
term relationship between the words "sofa" and "couch"</P>
<P>In the <A href="ISO_Thesuarus_Model.htm">ISO 2788</A> 
thesaurus specification, every thesarus relationship starts with an anchor 
term.&nbsp;&nbsp; For our example, we will add the anchor term of 
"sofa"&nbsp;&nbsp; First we select the add icon over the anchor column.</P>
<P><IMG height=37 hspace=0 src="../Images/anchorclose.GIF" 
width=156 border=0></P>
<P>The add icon for each column is : <IMG height=22 
hspace=0 src="../Images/anchoradd.GIF" width=26 border=0></P>
<P>Selecting this icon will bring up the followign 
dialog:</P>
<P><IMG height=180 hspace=0 
src="../Images/anchorcreate.GIF" width=379 border=0></P>
<P>&nbsp;</P>
<P>In this dialog, we want to have both the anchor term and 
the related term.&nbsp; In this case, the anchor term is couch and the related 
term is sofa.&nbsp;&nbsp; We will also keep the relationship set at "similar 
term"</P>
<P>&nbsp;</P>
<P>Now, lets assume that I want to add another similar term 
to couch, in addition to sofa, I also want to have the term "futon".&nbsp; In 
this case, we have already created the anchor term.&nbsp; First we make sure 
that the term "couch" is selected, then we choose which relationship we want to 
add.&nbsp; We have the choice between similar term, broader term or narrower 
term.&nbsp;&nbsp;&nbsp; For this example we are going to create "futon" as a 
similar term to "couch"&nbsp; to do this, we are going to select add : <IMG 
height=22 hspace=0 src="../Images/anchoradd.GIF" width=26 border=0>&nbsp; from 
the similar term column on the screen.</P>
<P>&nbsp;</P>
<P><IMG height=65 hspace=0 src="../Images/thessimadd.GIF" 
width=196 border=0></P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
</BODY>
</HTML>