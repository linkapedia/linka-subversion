#!/usr/bin/perl

#
# Build out a static hierarchy based upon our Nodes.
# Algorithm will process breadth first.
#
# Step 1:
# Connect to the Oracle database and select all rows in
#  the node table.   Load each node into a hash table
#  that can be referenced by node id (create WBN objects).
#
# Step 2:
# Recursively sift through each node in the hierarchy, 
#  creating the HTML for each node and all of it's children.
#
# Parameters:
#    - tree             Root directory to build the tree
#    - hid              Hierarchy identifier to use
#    - corpus           Corpus identifier to use
#    - client           Client identifier to use
#    - results          Number of results per page (default is 10)
#    - low		Low node for a node range (optional)
#    - high		High node for a node range (optional)
#    - debug		Debug mode (0 is off, 1 is on)
#    - help me		Display help message
#
# Written by Michael A. Puscar from 2/16/2001 -> 3/18/2001
########################################################
#
use DBI;
use Benchmark;
use Getopt::Long;
use URI::URL;
require "WorldBookNode.pm";

# Signal switches catch interrupts
$SIG{INT} = \&stop;
$SIG{QUIT} = \&stop;
$SIG{STOP} = \&stop;
$SIG{HUP} = \&stop;
$SIG{TERM} = \&stop;

# set default values
local ($tree) = "/src/web/html/";
local ($hid) = 2;
local ($corpus) = 5;
local ($client) = 1;
local ($results) = 10;
local ($lownode) = 0;
local ($highnode) = 0;
local ($debug) = 0;
local ($help) = "";
local ($overflow);

&GetOptions ("hid=i" => \$hid,
	     "corpus=i" => \$corpus,
	     "client=i" => \$client,
	     "results=i" => \$results,
	     "high=i" => \$highnode,
	     "low=i" => \$lownode,
	     "debug=i" => \$debug,
	     "help=s" => \$help,
             "tree=s" => \$tree);

if ($help ne "") { 
    print "Usage: ./static_pages.pl [-tree][-hid][-corpus][-client][-results][-low][-high][-debug][-help me]\n";
    print "(all parameters are optional)\n";
    print "    - hid              Hierarchy identifier to use\n";
    print "    - corpus           Corpus identifier to use\n";
    print "    - client           Client identifier to use\n";
    print "    - tree             Root directory to build the tree (default: /src/web/html)\n";
    print "    - results          Number of results per page (default: 10)\n";
    print "    - low              Low node for a node range (optional)\n";
    print "    - high             High node for a node range (optional)\n";
    print "    - debug            Debug mode (0 is off, 1 is on)\n";
    print "    - help me          Display this message\n";
    exit(1);
}

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
$dbh->{AutoCommit} = 0;
local ($t0, $t1);

# Step 1: Create the object hierarchy in memory
local (%wbn_hash);
local (%templates);
local (%blacklist);

local($sth) = &get_corpus_info();
local ($corpus_short, $corpus_long, $fee, $image_ids) = $sth->fetchrow_array;
$sth->finish();

# Create the top level category (node id: -1)
local ($wbn) = WorldBookNode->new;
$wbn->{Nid} = -1;
$wbn->{Pid} = -1;
$wbn->{URL} = "/1.htm";
$wbn->{Title} = $corpus_long;
$wbn->{Children} = "";
$wbn->{NIWP} = 1;

$wbn_hash{$wbn->Nid} = $wbn;

print "Selecting node data from the database ...\n";
if ($debug) { $t0 = new Benchmark; }

# Select all nodes from the table (close query as quickly as possible)
local($sth) = &select_all_nodes(); 
if ($debug) {   
    $t1 = new Benchmark;
    $td = timediff($t1, $t0);
    print "completed in: ",timestr($td),"\n";
}

print "Loading data structures into memory...\n";
if ($debug) { $t0 = new Benchmark; }
while (($Nid, $Pid, $Title, $NIWP) = $sth->fetchrow_array) {
    # Create the node in memory
    $wbn = WorldBookNode->new;
    $wbn->{Nid} = $Nid;
    $wbn->{Pid} = $Pid;
    $wbn->{Title} = $Title;
    $wbn->{NIWP} = $NIWP;
    $wbn->{Children} = "";
    
    $wbn_hash{$Nid} = $wbn;
    undef $wbn;
}

undef $sth; undef $wbn;

# This loop builds the parent-child relationship in memory
foreach $key (sort { $b <=> $a } keys %wbn_hash) {
    $wbn = $wbn_hash{$key};
    $Nid = $wbn->Nid; $Pid = $wbn->Pid; $Title = $wbn->Title;
    
    # Recursive: find parent, set children ids, build URL
    $GPid = $Pid; $URL = &Bescape($Nid)."/1.htm"; $gpn = $wbn;
    while ($GPid != -1) {
	($GPid, $URL, $gpn) = &set_children($gpn, $URL);
    }
    $wbn->{URL} = "/$corpus_short/$URL";
    
    $wbn_hash{$Nid} = $wbn;
    undef $wbn;
}
if ($debug) { 
    $t1 = new Benchmark;
    $td = timediff($t1, $t0);
    print "completed in: ",timestr($td),"\n";
}

# Step 2: Load in templates and scoring and create HTML
if ($debug) { $t0 = new Benchmark; }
print "Loading HTML templates into memory...\n";
&load_template("templates/template.htm");
&load_template("templates/noreschild.htm");
&load_template("templates/no_results.htm");
&load_template("templates/randc.htm");
&load_template("templates/result.htm");

if ($debug) { 
    $t1 = new Benchmark;
    $td = timediff($t1, $t0);
    print "completed in: ",timestr($td),"\n";
}

# Step 3: Load black list for this corpus hierarchy
my ($sth) = &get_blacklist($corpus, $client);
while (my ($nid, $word) = $sth->fetchrow_array) {
    # If $nid is empty then black list word applies to ALL
    if ($nid eq "") { $nid = "all"; }

    if (!defined($blacklist{$nid})) { $blacklist{$nid} = $word; }
    else { $blacklist{$nid} = $blacklist{$nid} . "||" . $word; }
}
$sth->finish;

# Step 4: Open a cursor to the database to get all the results
print "Getting results from the database...\n";
if ($debug) { $t0 = new Benchmark; }

local ($iwd) = &get_iw_documents();
if ($debug) {  
    $t1 = new Benchmark;
    $td = timediff($t1, $t0);
    print "completed in: ",timestr($td),"\n";
} 

# (building tree)
print "Building tree ... \n";

foreach $key (sort { $b <=> $a } keys %wbn_hash) {
    $wbn = $wbn_hash{$key};
    # Create this node.   Use a specific range if specified.
    if (($lownode != 0) && ($highnode != 0)) {
        if (($wbn->Nid >= $lownode) && ($wbn->Nid <= $highnode)) {
            &create_node ($wbn);
        }}
    else { &create_node ($wbn); }
    undef $wbn;
}

print "\n";
stop;

# STOP: (if sig interrupt is called)
sub stop {
    undef %wbn_hash;
    $dbh->disconnect;
    undef $dbh;
    exit(1);
}

# Load all templates (recursive)
sub load_template {
    my ($filename) = @_;
    
    open (TEMPLATE, "$filename") || die "Error: template file not found, $filename\n";
    my (@lines) = <TEMPLATE>; close(TEMPLATE);
    $templates{$filename} = [ @lines ];
    
    foreach $line (@lines) { if ($line =~ /<!--INCLTEM=\"(.*)\"--\>/) { &load_template($1); }}

    1;
}

# Get Full Title (recursive)
sub get_full_title {
    my ($wbn) = @_;
    my ($full_title) = "";
    
    if ($wbn->Pid != -1) {
	$full_title = &get_full_title($wbn_hash{$wbn->Pid});
    }
    
    return $full_title . " > " . $wbn->Title;
}

# Get Category With Links (recursive)
#  this is a variable used in the templates
sub get_category_with_links {
    my ($wbn, $depth) = @_;
    my ($href_string,$dots) = "";
    my ($i) = 0;
    
    for ($i=1; $i<=$depth; $i++) { $dots = $dots . "../"; }    
    
    if ($wbn->Pid != -1) {
	($href_string,$depth) = &get_category_with_links($wbn_hash{$wbn->Pid},($depth+1));
    }
    
    my ($rval)=$href_string . " > " . "<a href='".$dots."1.htm'>".$wbn->Title."</a> ";
    return ($rval, $depth);
}

# Create this node (on disk) 
sub create_node {
    my $wbn = $_[0];
    
    # Node creation piece.
    $path = $tree . $wbn->URL; 
    if (!-e $path) { # Directory does not exist.   Create it.
	my $dir = $path; $dir =~ s/1.htm//gi; 
	&make_directory($dir); 
	&create_web_page($wbn, $dir, scalar(@children));
    } 
}

# Create the web page.   To do so we need:
#  1) Load the results for this category
#  2) Create one or many files from the template
sub create_web_page {
    local ($wbn, $page, $children) = @_;
    local ($loop, $rows);

    # these are our local variables -- there are many of them
    local ($node_template) = ""; local ($SIGNATURES) = "";
    local ($nid, $doct, $docu, $docs, $chan, $dtyp, $sco1, $sco2, $sco3);
    local ($onid, $odoct, $odocu, $odocs, $ochan, $odtyp, $osco1, $osco2, $osco3);
    $nid = 0; $rows = 0; undef @res_arr; 

    local (@res_arr);

    # temporary variables -- hold the key to looking one step back
    undef ($tnid); undef ($tdoct); undef ($tdocu); undef ($tdocs); undef ($tchan);
    undef ($tdtyp); undef ($tsco1); undef ($tsco2); undef ($tsco3);
    local ($tnid, $tdoct, $tdocu, $tdocs, $tchan, $tdtyp, $tsco1, $tsco2, $tsco3);

    # Each node will usually have at least one result of overflow due to the fact that we always
    # have to select an extra result out to know that we are beyond our group.   If that is the case, 
    # store this extra result in a special variable
    if ($overflow ne "") { 
	($onid, $odoct, $odocu, $odocs, $ochan, $odtyp, $osco1, $osco2, $osco3)= split(/\|\|/,$overflow);
	if ($onid == $wbn->Nid) { 
	    if (&url_contain_blacklist($onid, $odocu) == 0) {
		push (@res_arr, $wbn->Nid."||$odoct||$odocu||$odocs||$ochan||$odtyp||$osco1||$osco2||$osco3"); 
		$tnid = $onid; $tdoct = $odoct; $tdocu = $odocu; $tchan = $ochan; $tdtyp = $odtyp; 
		$tsco1 = $osco1; $tsco2 = $osco2; $tsco3 = $osco3; $tdocs = $odocs;  }
	    $overflow = "";
	}
    } 
    
    # If the overflow variable contains a result with an NID less than our current node, then
    # there are no results here, do not try to fetch another row until it is => current.
    if (($overflow eq "") || ($onid >= $wbn->Nid)) {

	# Loop through our cursor and grab all the results associated with this node.
	if (defined($iwd)) {
	    while (($nid >= $wbn->Nid) || ($nid == 0)) {
		($nid, $doct, $docu, $docs, $chan, $dtyp, $sco1, $sco2, $sco3) = $iwd->fetchrow();
		if (!defined($nid)) { $nid = -1; } # indicates that we have completed fetching rows 
		if ($nid == $wbn->Nid) { 
		    if (&url_contain_blacklist($nid, $docu) == 0) {
			if (($nid == $tnid) && ($docu eq $tdocu)) {
			    # found a duplicate, now pop it off our list
			    pop (@res_arr);
			    if ($chan !~ /$tchan/i) { $chan = $chan.", ".$tchan; }
			} # or if document summaries are equal, eliminate the duplicate
			elsif (($nid == $tnid) && ($docs eq $tdocs)) { pop (@res_arr); }
			
			# Push this result onto the result array and place in temporary storage
			push (@res_arr, $wbn->Nid."||$doct||$docu||$docs||$chan||$dtyp||$sco1||$sco2||$sco3"); 
			$tnid = $nid; $tdoct = $doct; $tdocu = $docu; $tchan = $chan; $tdtyp = $dtyp; 
			$tsco1 = $sco1; $tsco2 = $sco2; $tsco3 = $sco3; $tdocs = $docs;
		    }}
	    }}
	
	# If IWD cursor is finished, sew it up
	if ($nid == -1) { $iwd->finish(); undef $iwd; }   

	# If there is overflow from a node less than our node, put it on the overflow stack	
	if (($nid != 0) && ($nid < $wbn->Nid)) { 
	    $overflow = $nid."||$doct||$docu||$docs||$chan||$dtyp||$sco1||$sco2||$sco3"; 
	}
    }
	
    # Number of rows can be indicated by checking size of the array
    $rows = scalar(@res_arr);

    # Get the signatures for this category -- requires another select :(
    $sig = &get_signatures($wbn->Nid);
    while (my $signature = $sig->fetchrow_array) { $SIGNATURES = $SIGNATURES.$signature." "; }
    $sig->finish();

    # Remove "titleRecord" and replace with the actual title
    $SIGNATURES =~ s/titleRecord//gi;
    $SIGNATURES = $wbn->Title ." ". $SIGNATURES;

    @children = split(',', $wbn->Children);
    $childnum = scalar(@children);
    
    # Node Types: 
    #   -- Node has no children and no results. (no_results template)
    #   -- Node has results but no children. (template template)
    #   -- Node has children but no results. (noreschild template)
    #   -- Node has both children and results. (randc template)
    if (($rows > 0) && ($childnum > 0)) { $node_template = "templates/randc.htm"; }
    elsif ($rows > 0) { $node_template = "templates/template.htm"; }
    elsif ($childnum > 0) { $node_template = "templates/noreschild.htm"; }
    else { $node_template = "templates/no_results.htm"; }
    
    print "Node: ".$wbn->Nid." Rows: ".$rows." Children: ".$childnum." Template: ".$node_template."\n";

    if ($debug) {  
	print "Creating actual web pages for Node ".$wbn->Nid." ...";
	$t0 = new Benchmark;
    }

    # Need to print a page even if rows and children are 0
    if ($rows == 0) { 
	my ($thispage) = $page . "1.htm";
        open (FILE, ">$thispage");
        print FILE "<!-- Web page generated automatically by the Indraweb page builder-->\n";
        &print_template($wbn, $node_template, $thispage, 1, $rows);
        close (FILE);
    } else {
    for ($loop = 1; $loop <= 1+(($rows-1) / $results); $loop++) { 
	my ($thispage) = $page . $loop . ".htm";
	open (FILE, ">$thispage"); 
	print FILE "<!-- Web page generated automatically by the Indraweb page builder-->\n";
	&print_template($wbn, $node_template, $thispage, $loop, $rows);
	close (FILE);
    }}

    if ($debug) {     
	$t1 = new Benchmark;
	$td = timediff($t1, $t0);
	print ".. finished in: ",timestr($td),"\n";
    }
}

# Subroutine to determine whether a given URL (as an input argument) contains one of the blacklist words.
sub url_contain_blacklist {
    my ($nid, $url) = @_;
    local ($blackstring);

    if ($blacklist{'all'} ne "") { $blackstring = $blacklist{'all'}."||".$blacklist{$nid}; }
    else { $blackstring = $blacklist{'all'}.$blacklist{$nid}; }

    my (@words) = split('\|\|', $blackstring);
    foreach $word (@words) { print "Checking if $url contains $word\n"; if ($url =~ /$word/i) { print "Yes!  It does!\n"; return 1; }}

    return 0;
}

# This procedure will build the previous/next HTML bar
sub build_prev_next {
    my ($ct, $row) = @_;
    my ($next) = $ct+1;
    my ($prev) = $ct-1;
    my ($i);
    
    my ($pn) = "<p align=center><font face=Arial, Helvetica, sans-serif>\n";
    if ($prev!=0) { $pn = $pn . "<a href='".$prev.".htm'>[Prev]</a> "; }
    else { $pn = $pn . "[Prev] "; }
    for ($i=1; $i<=((($row-1)/$results)+1); $i++) { 
	if ($i == $ct) { $pn = $pn."$i "; }
	else { $pn = $pn."<a href='".$i.".htm'>$i</a> "; }}
    if ($ct<(($row-1)/$results)) { 
	$pn = $pn . "<a href='".$next.".htm'>[Next]</a>"; }
    else { $pn = $pn . "[Next] "; }
    
    return $pn;
}

# Read in a template and replace variables accordingly.   This
#  procedure also handles the result list creation.
sub print_template {
    local ($wbn, $tem, $page, $counter, $rows) = @_;
    local ($LOOPI);
    
    # This next block of variables are loaded and replaced in the templates
    local ($CATEGORY_HIERARCHY,$depth) = &get_category_with_links($wbn,0);
    local ($PREVNEXT) = &build_prev_next($counter, $rows);
    local ($START) = ($counter*$results)-($results-1);
    local ($END) = ($counter*$results);
    if ($END > $rows) { $END = $rows; }
    if ($END == 0) { $END = 1; } if ($START == 0) { $START = 1; }
    $CATEGORY_HIERARCHY = "<a href='/$corpus_short/1.htm'>".$corpus_long."</a>" . $CATEGORY_HIERARCHY;
    local($NODEID) = $wbn->Nid;
    
    # Get documents associated with this node.
    foreach $line (@ {$templates{$tem}} ) {
	undef $linex; $linex = $line;
	if ($linex =~ /<!--INCLTEM=\"(.*)\"--\>/) { 
	    &print_template($wbn,$1,$page,$counter, $rows); }
	else {
	    $INDRATITLE = $wbn->Title;
	    $TITLEX = &get_full_title($wbn);
	    $TITLEX = $corpus_long.$TITLEX;
	    $linex =~ s/##(.*)##/${$1}/gi;
	    print FILE $linex; }
    }
    
    # If this is a directory template, treat it a bit differently.
    # Loop through each of the children and create a link to them.
    #  -- note, there is hard coded HTML in here.   I really hate to do this.
    #     do you have a better idea?   
    
    if ($tem =~ /noreschild-head/i) {
	@children = split(',', $wbn->Children);
	for (my $j = 0; $j < scalar(@children); $j++) { $children[$j] = $wbn_hash{$children[$j]}; }	
	@children = sort by_index @children;

	print FILE "<table cellpadding=2 width='78%' border=0><tbody><tr><td valign=top width='50%'><ul>\n";
 	my ($length_children) = scalar(@children);
	
	# Create two columns of children.   This should probably be in a template, but (see above comments)
	for (my $i=0; $i<($length_children / 2); $i++) {
	    my $child_wbn = @children[$i];
	    my ($this_url) = &build_url($child_wbn->URL, "");
	    print FILE "<li><font face='Arial, Helvetica, sans-serif'><font size='-1'>\n";
	    #print FILE "<a href='".$this_url."'>".$child_wbn->Title."</a></font>\n";

	    # If the FEE for this corpus is not 0, it is a SUBSCRIBERS ONLY Corpus
	    #  Javascript checking will ensure that the user is authorized.
	    if ($fee == 0) { 
		print FILE "<script> document.write('<a href=\"".$this_url."\">".&MakeJsReadable($child_wbn->Title)."</a></font>'); </SCRIPT>\n";
	    } else {
		print FILE "<SCRIPT>\n";
		print FILE "if (corpus == 1) { \n";
		print FILE "document.write('<a href=\"".$this_url."\">".&MakeJsReadable($child_wbn->Title)."</a></font>');\n";
		print FILE "} else { document.write('<a href=\"/login.cgi?CORPUS=$corpus\">".&MakeJsReadable($child_wbn->Title)."</a></font>'); } \n";
		print FILE "</SCRIPT>\n";
	    }
	    undef $child_wbn;
	}
	
	print FILE "</td><td valign=top width='50%'><ul>\n";

	local ($tmpi);
	if (($length_children % 2) == 1) { $tmpi = ($length_children / 2) + 0.5; }
	else { $tmpi = $length_children / 2; }

	for (my $i=$tmpi; $i< $length_children; $i++) {
	    my $child_wbn = @children[$i];
	    my ($this_url) = &build_url($child_wbn->URL, "");
	    print FILE "<li><font face='Arial, Helvetica, sans-serif'><font size='-1'>\n";
#	    print FILE "<a href='".$this_url."'>".$child_wbn->Title."</a></font>\n";

	    # If the FEE for this corpus is not 0, it is a SUBSCRIBERS ONLY Corpus
	    #  Javascript checking will ensure that the user is authorized.
	    if ($fee == 0) { 
		print FILE "<script> document.write('<a href=\"".$this_url."\">".&MakeJsReadable($child_wbn->Title)."</a></font>'); </SCRIPT>\n";
	    } else {
		print FILE "<SCRIPT>\n";
		print FILE "if (corpus == 1) { \n";
		print FILE "document.write('<a href=\"".$this_url."\">".&MakeJsReadable($child_wbn->Title)."</a></font>');\n";
		print FILE "} else { document.write('<a href=\"/login.cgi?CORPUS=$corpus\">".&MakeJsReadable($child_wbn->Title)."</a></font>'); } \n";
		print FILE "</SCRIPT>\n";
	    }

	    undef $child_wbn;
	}
	
	print FILE "</td></tr></tbody></table>\n"; 
    }
    
    # If this is the results template, treat it a bit differently.
    if ($tem =~ /results.htm/i) {
	for ($LOOPI = (($counter*$results)-($results-1)); $LOOPI <= ($counter*$results); $LOOPI++) {
	    if (defined(@res_arr[$LOOPI-1])) {
    		# SELECT nodeid doctitle docurl documentsummary channelname documenttype score1 score2 score3
		($NODEID, $DOCTIT, $DOCURL, $DOCSUM, $SEARCHENGINE, $TYPE, $SCOREA, $SCOREB, $SCOREC) 
		   = split(/\|\|/,@res_arr[$LOOPI-1]);
		local $DOCURL2 = &build_url($DOCURL,"");
		local $STARS = "<IMG height=14 src=\"/images/star.gif\" width=14>\n";
		if ($SCOREA > 74) { $STARS .= "<IMG height=14 src=\"/images/star.gif\" width=14>\n"; }
		if ($SCOREA > 99) { $STARS .= "<IMG height=14 src=\"/images/star.gif\" width=14>\n"; }
		$STARS = "<NOBR>".$STARS;
		foreach $line (@ {$templates{'templates/result.htm'}} ) {
		    undef $linex; $linex = $line;
		    $linex =~ s/##(.*)##/${$1}/gi;
		    print FILE $linex; }
	    }}
    }
}

# Sort routine: Sort nodes numerically
sub by_node {
   $a->Nid <=> $b->Nid || 0;
}

# Sort routine: Sort scores 1, 2, and 3 (not currently used)
sub by_score {
   my (@score1) = split (/\|\|/, $a);
   my (@score2) = split (/\|\|/, $b);

   my ($compare1) = (1000 * $score1[6]) + (10 * $score1[7]) + (1 * $score1[8]);
   my ($compare2) = (1000 * $score2[6]) + (10 * $score2[7]) + (1 * $score2[8]);

   $compare2 <=> $compare1;
}

# Sort routine: Sort nodes by their index within parent
sub by_index {
   my ($first_index) = $a->NIWP;
   my ($secon_index) = $b->NIWP;

   $first_index <=> $secon_index || 0;
}

# Sort routine: Sort nodes by their title
sub by_name {
   my ($first_name) = $a->Title;
   my ($secon_name) = $b->Title;

   $first_name =~ tr/A-Z/a-z/;
   $secon_name =~ tr/A-Z/a-z/;

   $first_name cmp $secon_name || 0;
}

# Create a node directory if it doesn't exist
sub make_directory { 
    my ($directory) = @_;
    #print ".";
    
    $directory =~ s/\/\///gi; undef $error;
    $rc = mkdir($directory); $error = $!;
    if (($error eq "No such file or directory") && ($rc == 0)) {
	my $parent = $directory;
	chomp($parent); 
	$parent =~ s/(.*)\/(.*)\//$1\//gi;
	&make_directory($parent);
	mkdir($directory);
    }
    
}

# Internal procedure to set the children of the WBN object
sub set_children {
    my ($wbn, $URL) = @_;
    my ($has_children) = 0;
    
    # Get the node's parent and add child, if necessary
    $pwbn = $wbn_hash{$wbn->Pid};
    if (!defined($pwbn)) { 
        print "fatal error! no hash entry for " . $l_wbn->Pid . "\n";
	$dbh->disconnect; exit(1); }
    if ($pwbn->Children eq "") { 
	$pwbn->{Children} = $wbn->Nid;
    } else {
	foreach $listelement (map { split ',' } $pwbn->{Children}) {
	    if ($listelement eq $wbn->Nid) { $has_children = 1; } }
	if ($has_children == 0) { 
	    $pwbn->{Children} = $pwbn->Children . "," . $wbn->Nid;
	}
    }
    
    $URL = &Bescape($pwbn->Nid)."/".$URL;
    return ($pwbn->Pid, $URL, $pwbn);
}

# select all signatures associated with the node NODE_ID
sub get_signatures {
    my $SNode = $_[0];
    
    my $sig = $dbh->prepare("SELECT SignatureWord FROM Signature WHERE NodeId = $SNode");
    $sig->execute();
    return $sig;
}

# select all documents and scores associated with this node
sub get_iw_documents {
    my $more_sql = "";
    
    if (($highnode != 0) && ($lownode != 0)) { $more_sql = "and nd.nodeid <= $highnode and nd.nodeid >= $lownode"; }   

    my $sth2 = $dbh->prepare("SET TRANSACTION READ ONLY") || die "Failed: ".$DBI::errstr; 
    my $rc2 = $sth2->execute() || die "Failed: ".$DBI::errstr;
    
    my $iwd = $dbh->prepare("SELECT nd.nodeid, d.doctitle, d.docurl, d.documentsummary, c.channelname, nd.documenttype, score1, score2, score3 from hierarchy h, document d, nodedocument nd, documentchannel dc, channel c WHERE h.nodeid = nd.nodeid and h.corpusid = $corpus and h.parentrel = 1 and h.hierarchyid = $hid and d.documentid = nd.documentid and d.documentid = dc.documentid and c.channelid = dc.channelid and nd.nodeid = dc.nodeid and score1 > 0 $more_sql order by nd.nodeid desc, score1 DESC, score2 DESC, score3 DESC") || die "Cannot execute document selection: $!\n";
    $iwd->execute() || die "Cannot execute document selection: $!\n";
    
    return $iwd;
}

# select all nodes from the database
sub select_all_nodes {
    my $sth = $dbh->prepare("select N.NodeID, H.ParentID, N.NodeTitle, H.NodeIndexWithinParent from Node N, Hierarchy H where H.HierarchyID = $hid and H.CorpusId = $corpus and N.NodeId = H.NodeId order by N.NodeId desc");
    
    $sth->execute();
    return $sth;
}

# get all the corpus information
sub get_corpus_info {
    my $sth = $dbh->prepare("select corpusshortname, corpus_name, fee, corpusimageids from corpus where corpusid = $corpus");
    
    $sth->execute();
    return $sth;
}

sub get_blacklist {
   my ($corpus, $client) = @_;
   my $sth = $dbh->prepare("SELECT NodeId, URLChunk FROM BlackList WHERE ClientId = $client and CorpusId = $corpus");
   $sth->execute();
   return $sth;
}

# Escape function for building the directories
sub Bescape {
    my ( $string ) = @_;
    $string =~ s/\//-/g; $string =~ s/\.//g; $string =~ s/\'//g;
    $string;
}

sub MakeJsReadable {
    my ( $string ) = @_;
    $string =~ s/\'//g;
    $string;
}

# Escape function for building the HTML
sub Hescape {
    my ( $string ) = @_;
    $string =~ s/\ /%20/g; $string =~ s/\&/%26/g;
    $string;
}

# Regenerate the entire URL
sub build_url {
    my ($partial, $model) = @_;
    my $url = new URI::URL($partial, $model);
    my $build = $url->abs->as_string;

    return $build;
}

1;
