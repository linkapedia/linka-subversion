<HTML><HEAD>
<TITLE>
S-Book Editor Administration Center
</TITLE>
<link rel="stylesheet" href="/images/surf.css">
</HEAD>
<BODY bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 height="78">
  <TBODY>
  <TR>
    <TD width="69" bgcolor="#336666" height="50">&nbsp;&nbsp;<img src="/demo/images/logo.gif" width=
"52" height="55" align="absmiddle"></TD>
    <TD width="850" bgcolor="#336666" height="50">
      <div align="right">
        <p><font face="Arial, Helvetica, sans-serif"><b><font color="#FFFFFF" size="4">Document Comm
ent Page &nbsp; &nbsp;</font></b></font></p>
        <p><font color=white>
           [<a class="demolink" href="/az/">Home</a>]
           [<a class="demolink" href="/az/index.html">Logout</a>]
           [<a class="demolink" href="/az/index.html#help">Help</a>]</font>
      </div>
    </TD>
  </TR>
  </TBODY>
</TABLE>
<BR><blockquote>
<font size=+1 face="Arial"><b>Add New Black List Entry</b></font>
<P>
<u><b>Instructions</b></u>: 
This form will allow you to black list all results found
that contain a given string in the url.   If you would
like to blacklist a specific URL, you can copy and paste
the URL directly into the space provided below.  To black
list all results from a specific domain, type the domain
name only.   Do not include <b>http://</b> as a black list
word.
<P>
Additions to the black list will affect <u>all</u> corpuses
for this publisher.   Please note that all entries are 
case sensitive.
<P>
Your entry will not take effect until the next 
build cycle.
<P>
##BLACKLISTFORM##
<P align=center><FONT face="Arial, Helvetica, sans-serif">� 2001 IndraWeb
      Ltd. All rights reserved<BR><A class=footer
      href="/about_us.htm">About Us</A>&nbsp; |&nbsp; <A
      class=footer href="/contact_us.htm">Contact
      Us</A>&nbsp; |&nbsp; <A class=footer
      href="/jobs.htm">Jobs</A>&nbsp; |&nbsp; <A
      class=footer href="/privacy_policy.htm">Privacy
      Policy</A>&nbsp; |&nbsp; <A class=footer
      href="/terms%20of%20service">Terms of
      Service</A>&nbsp; |&nbsp; <A class=footer
      href="/help">Help</A></FONT></P>
      <P align=center> </P></FONT>
</blockquote>
</BODY>
</HTML>
