#!/usr/bin/perl
#
# engines.cgi
# Written by: Michael A. Puscar
# - Display hierarchy information and allow users to choose search engines
#
# Need to populate: CORPUS, CLIENT_NAME, NODE_TITLE, RESULTS for template

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;

# Were username and password information even sent?
if (!defined($query->param('group'))) {
    &print_error ("A serious error has occured.\n");
    exit(1);
}

local ($group) = $query->param('group');

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:gaea", "sbooks", "racer9");

$sth = $dbh->prepare("SELECT C.ChannelName from Channel C, CategoryChannel CC where CC.SearchCategoryId = $group and C.ChannelId = CC.ChannelId") || die "Cannot execute node title selection: $!\n";
$sth->execute() || die "Fatal database error: $!\n";

print "Content-type: text/html\n\n";
print "<HTML><BODY>\n";
print "<TABLE cellSpacing=0 cellPadding=0 width=\"100%\" border=0 height=\"50\">\n";
print "  <TBODY>\n";
print "  <TR>\n";
print "    <TD align=left width=\"919\" bgcolor=\"#003264\" height=\"30\"><img src=\"http://66.134.131.36/images/logo_topnav.gif\" \n";
print "border=1 align=\"absmiddle\"></TD>\n";
print "  </TR>\n";
print "  </TBODY>\n";
print "</TABLE>\n";
print "<BR><blockquote>\n";
print "<font size=-1 face='Arial'><b>This group contains: </b>\n<P>";
print "<UL>\n";
while ($title = $sth->fetchrow_array()) { print "<LI> $title\n"; }
print "</UL></font>\n";
$sth->finish;
$dbh->disconnect;

1;
