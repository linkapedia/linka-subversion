#!C:\perl\bin\perl.exe

#
# toc.cgi
#
# generate a table of contents listings from a .sav file

use CGI;

local ($query) = new CGI;
local ($savefile) = $query->param('savefile') || "";

if ($savefile eq "") { &write_error ("You must specify a save file to print the table of contents.", 1); }
open (FILE, "$savefile") || &write_error ("The save file you entered, $savefile, is not valid."); 
local (@lines) = <FILE>; close(FILE);

&write_header();
&write_template("header.tpl");

foreach $line (@lines) {
   chomp($line);
   local ($title, $depth) = split(/\|\|/, $line);

   if ($depth != 0) {
      print "<BR> ";
      if ($depth == 1) { print "<b>"; }
      for ($i = 0; $i < $depth; $i++) { print "&nbsp; &nbsp; "; }
      print "$title ";
      if ($depth == 1) { print "</b>"; }
      print "($depth)\n";
   }
   undef $title; undef $depth;
}

&write_template("footer-valid.tpl");

sub write_header {
    print "Content-type: text/html\n\n";
}

sub write_template {
    my $template = $_[0];
    open (FILE, "$template"); my (@linez) = <FILE>; close(FILE);
    foreach $linex (@linez) {
	$linex =~ s/##(.*)##/${$1}/gi;
	print $linex;
    }
}

sub write_error {
    my ($message) = $_[0];
    my ($header) = $_[1];

    if (defined($header)) { &write_header(); }

    print "<b>Error:</b> $message\n";
    exit(1);
}

1;