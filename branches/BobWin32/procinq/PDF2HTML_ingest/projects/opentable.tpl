<table width=65%><td><td>
<u><b>Instructions:</b></u> Assign the section type to each of the following sections.   Each section number specifies the section's level within the tree.   E designates an invalid section that is marked or deletion.
<P>
<u><b>Note:</b></u> Because some books capitalize topic names, section names have been altered so that the first initial of each word is capialized and the rest are lower case.   Please inspect each section name to ensure the capitalization is correct.
</td></tr></table>
<P>
<form action="index.cgi" method=post>
<table border=0 bgcolor=FFFFFF width=70% bordercolor=000000>
<tr><td align=center><b><font size=+2>Section Name</font></b></td>
    <td align=center><b>0</b></td>
    <td align=center><b>1</b></td>
    <td align=center><b>2</b></td>
    <td align=center><b>3</b></td>
    <td align=center><b>4</b></td>
    <td align=center><b>5</b></td>
    <td align=center><b>6</b></td>
    <td align=center><b>7</b></td>
    <td align=center><b>8</b></td>
    <td align=center><b>E</b></td>
</tr>
