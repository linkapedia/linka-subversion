import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

public class RebuildIndexes {
	public static void main(String args[]) {

        //if (args.length < 1) { System.out.println("Usage: RebuildIndexes.jar -username user -password pass -api api"); return; }

        String SKEY = "";

        String API = "http://woti.tzo.com:8888/";
        String Username = "sn=cifaadmin,ou=users,dc=cifanet";
        String Password = "racer9";

        if (args.length > 1) { Username = (String) args[1]; }
        if (args.length > 3) { Password = (String) args[3]; }
        if (args.length > 5) { API = (String) args[5]; }

        ITS its = new ITS();
        its.SetAPI(API);

		try {
            // LOGIN routine takes a USERID and a PASsWORD
            User u = its.Login(Username, Password);

			// Login successful.  Get the session key and put into args for future arguments
			SKEY = u.getSessionKey();
            its.SetSessionKey(SKEY);

			System.out.println("Login successful.  Your session key is "+SKEY);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("Error: Login failed."); return;
        }

        try {
            long lStart = System.currentTimeMillis();
            if (!its.rebuildIndexes()) { throw new Exception("Index rebuild failed."); }
            long lEnd = System.currentTimeMillis() - lStart;

            System.out.println("Indexes rebuild completed in "+lEnd+" ms.");
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("Error: Index rebuild failed."); return;
        }
    }
}
