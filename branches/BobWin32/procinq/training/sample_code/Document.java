/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import java.util.Vector;
import java.net.URLEncoder;

public class Document {
    // User attributes
    private String DocumentID = "null";
    private String DocURL = "null";
    private String GenreID = "null";
    private String DocTitle = "null";
    private String Abstract = "null";
    private String Visible = "null";
    private String DateLastFound = "null";

    // new DFI fields
    private String Short = "null";
    private String Author = "null";
    private String Source = "null";
    private String Citation = "null";
    private String SortDate = "null";
    private String ShowDate = "null";
    private String SubmittedBy = "null";
    private String ReviewDate = "null";
    private String ReviewedBy = "null";
    private String ReviewStatus = "null";
    private String Comments = "null";
    private String UpdatedDate = "null";
    private String UpdatedBy = "null";


    // constructor(s)
    public Document (HashTree ht) {
        DocumentID = (String) ht.get("DOCUMENTID");
        GenreID = (String) ht.get("GENREID");
        DocTitle = (String) ht.get("DOCTITLE");
        Abstract = (String) ht.get("DOCUMENTSUMMARY");
        DocURL = (String) ht.get("DOCURL");

        if (ht.containsKey("VISIBLE")) { Visible = (String) ht.get("VISIBLE"); }
        if (ht.containsKey("SHORT")) { Short = (String) ht.get("SHORT"); }
        if (ht.containsKey("AUTHOR")) { Author = (String) ht.get("AUTHOR"); }
        if (ht.containsKey("SOURCE")) { Source = (String) ht.get("SOURCE"); }
        if (ht.containsKey("CITATION")) { Citation = (String) ht.get("CITATION"); }
        if (ht.containsKey("SUBMITTEDBY")) { SubmittedBy = (String) ht.get("SUBMITTEDBY"); }
        if (ht.containsKey("REVIEWEDBY")) { ReviewedBy = (String) ht.get("REVIEWEDBY"); }
        if (ht.containsKey("REVIEWSTATUS")) { ReviewStatus = (String) ht.get("REVIEWSTATUS"); }
        if (ht.containsKey("COMMENTS")) { Comments = (String) ht.get("COMMENTS"); }
        if (ht.containsKey("UPDATEDBY")) { UpdatedBy = (String) ht.get("UPDATEDBY"); }

        if (ht.containsKey("REVIEWDATE")) {
            ReviewDate = (String) ht.get("REVIEWDATE");
            if (ReviewDate.length() > 19) { ReviewDate = ReviewDate.substring(0, 18); }
        }
        if (ht.containsKey("UPDATEDDATE")) {
            UpdatedDate = (String) ht.get("UPDATEDDATE");
            if (UpdatedDate.length() > 19) { UpdatedDate = UpdatedDate.substring(0, 18); }
        }
        if (ht.containsKey("SORTDATE")) {
            SortDate = (String) ht.get("SORTDATE");
            if (SortDate.length() > 19) { SortDate = SortDate.substring(0, 18); }
        }
        if (ht.containsKey("SHOWDATE")) {
            ShowDate = (String) ht.get("SHOWDATE");
            if (ShowDate.length() > 19) { ShowDate = ShowDate.substring(0, 18); }
        }
    }

    public Document (Vector v) {
        DocumentID = (String) v.elementAt(0);
        GenreID = (String) v.elementAt(1);
        DocTitle = (String) v.elementAt(2);
        DocURL = (String) v.elementAt(3);
        Abstract = (String) v.elementAt(4);

        // the remaining fields are optional
        if (v.size() > 6) { Visible = (String) v.elementAt(6); }
        if (v.size() > 7) { Short = (String) v.elementAt(7); }
        if (v.size() > 8) { Author = (String) v.elementAt(8); }
        if (v.size() > 9) { Source = (String) v.elementAt(9); }
        if (v.size() > 10) { Citation = (String) v.elementAt(10); }
        if (v.size() > 13) { SubmittedBy = (String) v.elementAt(13); }
        if (v.size() > 15) { ReviewedBy = (String) v.elementAt(15); }
        if (v.size() > 16) { ReviewStatus = (String) v.elementAt(16); }
        if (v.size() > 17) { Comments = (String) v.elementAt(17); }
        if (v.size() > 19) { UpdatedBy = (String) v.elementAt(19); }

        if (v.size() > 14) {
            ReviewDate = (String) v.elementAt(14);
            if (ReviewDate.length() > 19) { ReviewDate = ReviewDate.substring(0, 19); }
        }
        if (v.size() > 11) {
            SortDate = (String) v.elementAt(11);
            if (SortDate.length() > 19) { SortDate = SortDate.substring(0, 19); }
        }
        if (v.size() > 18) {
            UpdatedDate = (String) v.elementAt(18);
            if (UpdatedDate.length() > 19) { UpdatedDate = UpdatedDate.substring(0, 19); }
        }
        if (v.size() > 12) {
            ShowDate = (String) v.elementAt(12);
            if (ShowDate.length() > 19) { ShowDate = ShowDate.substring(0, 19); }
        }
        DateLastFound = (String) v.elementAt(5);
        if (DateLastFound.length() > 19) { DateLastFound = DateLastFound.substring(0, 19); }

    }

    public static String FixDate (String Date) {
        if (Date.charAt(2) != '-') {
            // change from: 2003-07-24+07:55:35 to 24-07-2003+07:55:35
            return Date.substring(8, 10)+"-"+Date.substring(5, 7)+"-"+Date.substring(0, 4)+Date.substring(10);
        } else { return Date; }
    }

    // accessor functions
    public String getID() { return DocumentID; }
    public String getGenreID() { return GenreID; }
    public String getTitle() { return DocTitle; }
    public String getAbstract() { return Abstract; }
    public String getDocURL() { return DocURL; }
    public String getDateLastFound() { return DateLastFound; }

    // new DFI extended schema accessor functions
    public String getVisible() { return Visible; }
    public String getAuthor() { return Author; }
    public String getSource() { return Source; }
    public String getCitation() { return Citation; }
    public String getSortDate() { return SortDate; }
    public String getShowDate() { return ShowDate; }
    public String getSubmittedBy() { return SubmittedBy; }
    public String getReviewDate() { return ReviewDate; }
    public String getReviewedBy() { return ReviewedBy; }
    public String getReviewStatus() { return ReviewStatus; }
    public String getComments() { return Comments; }
    public String getUpdatedDate() { return UpdatedDate; }
    public String getUpdatedBy() { return UpdatedBy; }

    // accessor functions with encoding
    public String getEncodedURL() { return URLEncoder.encode(DocURL); }
    public String getEncodedTitle() { return URLEncoder.encode(DocTitle); }

    // update functions
    public void setID(String DocumentID) { this.DocumentID = DocumentID; }
    public void setGenreID(String GenreID) { this.GenreID = GenreID; }
    public void setTitle(String Title) { this.DocTitle = Title; }
    public void setAbstract(String Abstract) { this.Abstract = Abstract; }
    public void setDocURL(String URL) { this.DocURL = URL; }
    public void setVisible(String Visible) { this.Visible = Visible; }
    public void setAuthor(String Author) { this.Author = Author; }
    public void setSource(String Source) { this.Source = Source; }
    public void setCitation(String Citation) { this.Citation = Citation; }
    public void setSubmittedBy(String Submitted) { this.SubmittedBy = Submitted; }
    public void setReviewedBy(String Reviewed) { this.ReviewedBy = Reviewed; }
    public void setReviewStatus(String Status) { this.ReviewStatus = Status; }
    public void setComments(String Comments) { this.Comments = Comments; }
    public void setUpdatedBy(String Updated) { this.UpdatedBy = Updated; }
    public void setShowDate(String Date) { this.ShowDate = Date; }

    public void setSortDate(String Date) {
        if ((!Date.equals("null")) & (Date.length() < 11)) { Date = Date + " 12:00:00"; }
        this.SortDate = Date;
    }
    public void setUpdatedDate(String Date) {
        if ((!Date.equals("null")) & (Date.length() < 11)) { Date = Date + " 12:00:00"; }
        this.UpdatedDate = Date;
    }
    public void setReviewDate(String Date) {
        if ((!Date.equals("null")) & (Date.length() < 11)) { Date = Date + " 12:00:00"; }
        this.ReviewDate = Date;
    }
    public void setDateLastFound(String Date) {
        if ((!Date.equals("null")) & (Date.length() < 11)) { Date = Date + " 12:00:00"; }
        this.DateLastFound = Date;
    }
}
