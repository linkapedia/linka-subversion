import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

/*
 * Read in a delimited metadata file and updated the database via the API
 *
 * Written By: Michael A. Puscar, Indraweb
 * Date: Jul 24, 2003
 * Time: 11:35:47 AM
 */

public class LoadMetaData {
    public static void ReadFile (File f, ITS its, Hashtable GenreList)
	throws Exception {
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(f);
			BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
			String sData = new String();

            int iLoop = 0;
			while ((sData = buf.readLine()) != null) { iLoop++;
                String fields[] = sData.split("\t");
                try {
                    // 0:  GenreName
                    // 1:  Title
                    // 2:  Author
                    // 3:  Source
                    // 4:  SortDate
                    // 5:  DocURL
                    // 6:  FilePath
                    // 7:  ReviewStatus

                    Document d = its.getDocPropsByURL((String) fields[6]);

                    if (GenreList.containsKey(((String) fields[0]).toLowerCase())) {
                        d.setGenreID((String) GenreList.get(((String) fields[0]).toLowerCase()));
                    }
                    d.setTitle(fields[1]);
                    d.setAuthor(fields[2]);
                    d.setSource(fields[3]);
                    d.setSortDate(fields[4]);
                    d.setDocURL(fields[5]);
                    d.setReviewStatus(fields[7]);

                    its.editDocument(d);
                    System.out.println("Old: "+(String)fields[1]+" New : "+d.getTitle()+" (updated successfully)");

                } catch (Exception e) {
                    System.out.println("Nothing found for: "+(String) fields[6]);
                    //e.printStackTrace(System.out);
                }
			}
		} catch (Exception e) { e.printStackTrace(System.out); return;
		} finally { fis.close(); }
	}

	public static void main(String args[]) {

        String API = "http://68.163.92.229:8888/";
        if (args.length < 1) { System.out.println("Usage: LoadData.java <filename> <api location>"); }
        if (args.length == 2) { API = args[1]; }

		try {
            // LOGIN routine takes a USERID and a PASsWORD
            ITS its = new ITS();
            its.SetAPI(API);

            // LOGIN
            User u = its.Login("sn=cifaadmin,ou=users,dc=cifanet", "racer9");

			// Login successful.  Get the session key and put into args for future arguments
			String SKEY = u.getSessionKey();
            its.SetSessionKey(SKEY);

			System.out.println("Login successful.  Your session key is "+SKEY);

            System.out.println("Building the genre hash..");
            Hashtable htGenres = its.getFolders();

            System.out.println("Loading "+args[0]+", please wait.");

            File f = new File(args[0]);
            if (!f.exists()) { throw new Exception("File does not exist."); }
            ReadFile(f, its, htGenres);

		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage());
            e.printStackTrace(System.out);
		}
    }
}
