import java.util.*;
import java.io.*;
import java.net.URLEncoder;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

public class ITS {
    private String SKEY = "NONE";
    private String API = "http://woti.tzo.com:8888/itsapi/ts?fn=";

    public ITS () {}
	public ITS (String SessionKey, String API) {
		if (SessionKey != null) { this.SKEY = SessionKey; }
        if (API != null) { this.API = API+"itsapi/ts?fn="; }
	}

    // Accessor functions
    public void SetSessionKey (String SessionKey) { if (SessionKey != null) { this.SKEY = SessionKey; }}
    public void SetAPI (String API) { if (API != null) { this.API = API+"itsapi/ts?fn="; }}
    public String getSessionKey () { return SKEY; }
    public String getAPI() { return API; }

    public HashTree getArguments() {
        HashTree ht = new HashTree();
        ht.put("api", API);
        ht.put("SKEY", SKEY);

        return ht;
    }

    // ITS Login
    // Arguments: Username, Password
    // Returns: User object
    public User Login (String Username, String Password) throws Exception {
        // LOGIN routine takes a USERID and a PASsWORD
		Hashtable htArgs = new Hashtable();
		htArgs.put("UserID", Username); // example: "sn=cifaadmin,ou=users,dc=cifanet";
		htArgs.put("Password", Password); // example: racer9
        htArgs.put("api", API);

        try {
            // Invoke the ITSAPI now.
            InvokeAPI API = new InvokeAPI ("security.TSLogin", htArgs);
            HashTree htResults = API.Execute(false, false);

            // If there is no "subscriber" tag, an error has occured
            if (!htResults.containsKey("SUBSCRIBER")) {
                throw new Exception("You have specified an invalid username, password combination.");
            }

            // Get user hash tree
            HashTree htUser = (HashTree) htResults.get("SUBSCRIBER");

            if (!htUser.containsKey("KEY")) {
                throw new Exception("You have specified an invalid username, password combination.");
            }

            return new User(htUser);
        } catch (Exception e) { throw e; }
    }

    public Hashtable getFolders () throws Exception {
		Hashtable htArgs = getArguments();
        Hashtable ht = new Hashtable();

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsgenre.TSListFolders", htArgs);
            HashTree htResults = API.Execute(false, true);

            if (!htResults.containsKey("GENRES")) {
                throw new Exception("Fatal error, this system does not have any genres.");
            }

            HashTree htGenres = (HashTree) htResults.get("GENRES");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                ht.put(((String) htGenre.get("GENRENAME")).toLowerCase(), (String) htGenre.get("GENREID"));
            }

            return ht;
        } catch (Exception e) { throw e; }
    }

	// Get the root node of a corpus
	// Arguments: CorpusID
	// Returns: NODE object
	public Node getCorpusRoot (String CorpusID) throws Exception {
		Hashtable htArgs = getArguments();
		htArgs.put("CorpusID", CorpusID);

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tscorpus.TSGetCorpusRoot", htArgs);
            HashTree htResults = API.Execute(false, true);

            if (!htResults.containsKey("NODES")) {
                throw new Exception("Corpus ID "+CorpusID+" does not have a root node.");
            }

            HashTree htNodes = (HashTree) htResults.get("NODES");
            HashTree htNode = (HashTree) htNodes.get("NODE");

            return new Node(htNode);

        } catch (Exception e) { throw e; }
    }

	public Vector getCorpora () throws Exception {
		Hashtable htArgs = getArguments();
        Vector v = new Vector();

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tscorpus.TSListCorpora", htArgs);
            HashTree htResults = API.Execute(false, true);

            if (!htResults.containsKey("CORPORA")) {
                throw new Exception("There are no taxonomies loaded in this system.");
            }

            HashTree htCorpora = (HashTree) htResults.get("CORPORA");
            Enumeration eC = htCorpora.elements();
            while (eC.hasMoreElements()) {
                HashTree htCorpus = (HashTree) eC.nextElement();
                Corpus c = new Corpus(htCorpus);

                v.add(c);
            }
            return v;

        } catch (Exception e) { throw e; }
    }

    // Given a NODEID, get the node properties
    // Arguments: NodeID
    // Returns: NODE object
    public Node getNodeProps (String NodeID) throws Exception {
 		Hashtable htArgs = getArguments();
		htArgs.put("NodeID", NodeID);

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsnode.TSGetNodeProps", htArgs);
            HashTree htResults = API.Execute(false, true);

            if (!htResults.containsKey("NODE")) {
                throw new Exception("NODE ID "+NodeID+" does not exist.");
            }

            HashTree htNode = (HashTree) htResults.get("NODE");

            return new Node(htNode);

        } catch (Exception e) { throw e; }
    }

    public Vector getNodeTree (String NodeID) throws Exception {
		Hashtable htArgs = getArguments();
		htArgs.put("NodeID", NodeID);

        Vector v = new Vector(); // return struct

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsnode.TSGetNodeTree", htArgs);
            Vector vResults = API.vExecute(false, true);

            if (vResults.size() == 0) { throw new Exception("Could not find a node tree for nodeID "+NodeID+"."); }

            Vector vNodes = (Vector) vResults.elementAt(0);

            Enumeration eN = vNodes.elements();
            while (eN.hasMoreElements()) {
                Vector vNode = (Vector) eN.nextElement();
                Node n = new Node(vNode);

                v.add(n); // add this node document to the vector
            }

            return v;
        } catch (Exception e) { throw e; }
    }

    public Vector getRelatedNodes (String DocumentID, String DocURL)
    throws Exception { return getRelatedNodes(DocumentID, DocURL, "50.0"); }
    public Vector getRelatedNodes (String DocumentID, String DocURL, String ScoreThreshold) throws Exception {
		Hashtable htArgs = getArguments();
		htArgs.put("DocumentID", DocumentID);
        htArgs.put("DocURL", DocURL);
        htArgs.put("Type", "0");
        htArgs.put("ScoreThreshold", ScoreThreshold);

        Vector v = new Vector(); // return struct

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsdocument.TSGetRelatedNodes", htArgs);
            Vector vResults = API.vExecute(false, true);

            if (vResults.size() == 0) { throw new Exception("There has been a system error.  Please see the error logs for more details."); }

            Vector vNodes = (Vector) vResults.elementAt(0);

            Enumeration eN = vNodes.elements();
            while (eN.hasMoreElements()) {
                Object o = (Object) eN.nextElement();

                try {
                    Vector vNode = (Vector) o;
                    NodeDocument n = new NodeDocument(vNode);

                    v.add(n); // add this node document to the vector
                } catch (Exception e) { }
            }

            return v;
        } catch (Exception e) { throw e; }
    }

    public Document getDocProps (String DocumentID) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCUMENTID = "+DocumentID);
        if (v.size() == 0) { throw new Exception("No documents found."); }
        return (Document) v.elementAt(0);
    }

    public Document getDocPropsByURL (String DocURL) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCURL = '"+DocURL+"'");
        if (v.size() == 0) { throw new Exception("No documents found."); }
        return (Document) v.elementAt(0);
    }

    public Document getDocPropsLikeURL (String DocURL) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCURL LIKE '%%"+DocURL+"%%'");
        if (v.size() == 0) { throw new Exception("No documents found."); }
        return (Document) v.elementAt(0);
    }

    public Document addDocument (String DocumentURL, Hashtable NameValPairs) throws Exception {
 		Hashtable htArgs = getArguments();
		htArgs.put("DocURL", DocumentURL);

        Enumeration eNV = NameValPairs.keys();
        while (eNV.hasMoreElements()) {
            String sKey = (String) eNV.nextElement();
            String sVal = (String) NameValPairs.get(sKey);

            htArgs.put(sKey, sVal);
        }

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsdocument.TSAddDocument", htArgs);
            HashTree htResults = API.Execute(false, true);

            if (!htResults.containsKey("DOCUMENTID")) {
                throw new Exception("Document addition failed.");
            }
            return (Document) getDocProps((String) htResults.get("DOCUMENTID"));

        } catch (Exception e) { throw e; }
    }

    public Document editDocument (Document d) throws Exception {
        Hashtable NameValPairs = new Hashtable();
        if ((!d.getGenreID().equals("null")) && (!d.getGenreID().equals(""))) {
            NameValPairs.put("genreid", d.getGenreID()); }
        if ((!d.getTitle().equals("null")) && (!d.getTitle().equals(""))){
            NameValPairs.put("doctitle", URLEncoder.encode(d.getTitle())); }
        if ((!d.getAbstract().equals("null")) && (!d.getAbstract().equals(""))){
            NameValPairs.put("documentsummary", URLEncoder.encode(d.getAbstract())); }
        if ((!d.getDocURL().equals("null")) && (!d.getDocURL().equals(""))){
            NameValPairs.put("docurl", URLEncoder.encode(d.getDocURL())); }
        if ((!d.getDateLastFound().equals("null")) && (!d.getDateLastFound().equals(""))){
            NameValPairs.put("datelastfound", d.FixDate(d.getDateLastFound())); }
        if ((!d.getVisible().equals("null")) && (!d.getVisible().equals(""))){
            NameValPairs.put("visible", d.getVisible()); }
        if ((!d.getAuthor().equals("null")) && (!d.getAuthor().equals(""))){
            NameValPairs.put("author", URLEncoder.encode(d.getAuthor())); }
        if ((!d.getCitation().equals("null")) && (!d.getCitation().equals(""))){
            NameValPairs.put("citation", URLEncoder.encode(d.getCitation())); }
        if ((!d.getSource().equals("null")) && (!d.getSource().equals(""))){
            NameValPairs.put("source", URLEncoder.encode(d.getSource())); }
        if ((!d.getSortDate().equals("null")) && (!d.getSortDate().equals(""))){
            NameValPairs.put("sortdate", d.FixDate(d.getSortDate())); }
        if ((!d.getShowDate().equals("null")) && (!d.getShowDate().equals(""))){
            NameValPairs.put("showdate", d.FixDate(d.getShowDate())); }
        if ((!d.getSubmittedBy().equals("null")) && (!d.getSubmittedBy().equals(""))){
            NameValPairs.put("submittedby", URLEncoder.encode(d.getSubmittedBy())); }
        if ((!d.getReviewDate().equals("null")) && (!d.getReviewDate().equals(""))){
            NameValPairs.put("reviewdate", d.FixDate(d.getReviewDate())); }
        if ((!d.getReviewedBy().equals("null")) && (!d.getReviewedBy().equals(""))){
            NameValPairs.put("reviewedby", URLEncoder.encode(d.getReviewedBy())); }
        if ((!d.getReviewStatus().equals("null")) && (!d.getReviewStatus().equals(""))){
            NameValPairs.put("reviewstatus", d.getReviewStatus()); }
        if ((!d.getComments().equals("null")) && (!d.getComments().equals(""))){
            NameValPairs.put("comments", URLEncoder.encode(d.getComments())); }
        if ((!d.getUpdatedDate().equals("null")) && (!d.getUpdatedDate().equals(""))){
            NameValPairs.put("updateddate", d.FixDate(d.getUpdatedDate())); }
        if ((!d.getUpdatedBy().equals("null")) && (!d.getUpdatedBy().equals(""))){
            NameValPairs.put("updatedby", URLEncoder.encode(d.getUpdatedBy())); }

        return editDocument(d.getID(), NameValPairs);
    }
    public Document editDocument (String DocumentID, Hashtable NameValPairs) throws Exception {
 		Hashtable htArgs = getArguments();
		htArgs.put("DocumentID", DocumentID);

        Enumeration eNV = NameValPairs.keys();
        while (eNV.hasMoreElements()) {
            String sKey = (String) eNV.nextElement();
            String sVal = (String) NameValPairs.get(sKey);

            htArgs.put(sKey, sVal);
        }

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsdocument.TSEditDocument", htArgs);
            HashTree htResults = API.Execute(false, true);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Document modification failed.");
            }
            return (Document) getDocProps(DocumentID);

        } catch (Exception e) { throw e; }
    }

    public void editNodeDocument (String DocumentID, String NodeID, String DocURL, String DocTitle, String DocSummary, String Score)
    throws Exception {
 		Hashtable htArgs = getArguments();
		htArgs.put("DocumentID", DocumentID);
        htArgs.put("NodeID", NodeID);
        htArgs.put("Score1", Score);
        htArgs.put("DocTitle", URLEncoder.encode(DocTitle));
        htArgs.put("DocURL", URLEncoder.encode(DocURL));
        htArgs.put("DocSummary", URLEncoder.encode(DocSummary));

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsdocument.TSEditDocProps", htArgs);
            HashTree htResults = API.Execute(false, true);
        } catch (Exception e) { throw e; }
    }

    public void deleteNodeDocument (String DocumentID, String NodeID)
    throws Exception {
 		Hashtable htArgs = getArguments();
		htArgs.put("DocumentID", DocumentID);
        htArgs.put("NodeID", NodeID);

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tsnode.TSRemoveNodeDocument", htArgs);
            HashTree htResults = API.Execute(false, true);
        } catch (Exception e) { throw e; }
    }

    public Vector CQL (String Query) throws Exception {
       return CQL(Query, 1, 500);
    }
    public Vector CQL (String Query, int Start, int RowMax) throws Exception {
 		Hashtable htArgs = getArguments();
		htArgs.put("query", URLEncoder.encode(Query));
        htArgs.put("start", ""+Start);
        htArgs.put("rowmax", ""+RowMax);

        Vector v = new Vector();

		// invoke the API
		try {
            InvokeAPI API = new InvokeAPI("tscql.TSCql", htArgs);
            Vector vResults = API.vExecute(false, true);

            // check returned values
            if (vResults.size() == 0) { throw new Exception ("CQL parsing exception."); }
            if (vResults.size() == 1) { return v; } // no results were found

            // return different structures depending upon NODE or DOCUMENT
            if (Query.toUpperCase().indexOf("<NODE>") != -1) {
                // Process NODE RESULTS
                Vector vNodes = (Vector) vResults.elementAt(0);

                Enumeration eN = vNodes.elements();
                while (eN.hasMoreElements()) {
                    Vector vNode = (Vector) eN.nextElement();
                    Node n = new Node(vNode);

                    v.add(n); // add this node to the vector
                }
            } else {
                Vector vDocs = (Vector) vResults.elementAt(0);

                Enumeration eD = vDocs.elements();
                while (eD.hasMoreElements()) {
                    Vector vDocument = (Vector) eD.nextElement();
                    Document d = new Document(vDocument);

                    v.add(d); // add this document to the vector
               }
            }

            return v;
        } catch (Exception e) { throw e; }
    }

    // Rebuild full text indexes
    public boolean rebuildIndexes () throws Exception {
        try {
            HashTree htArguments = getArguments();
            InvokeAPI API = new InvokeAPI ("tsother.TSRebuildIndexes", htArguments);
            HashTree htResults = API.Execute(false, false);
        } catch (Exception e) { e.printStackTrace(System.out); return false; }

        return true;
    }

}
