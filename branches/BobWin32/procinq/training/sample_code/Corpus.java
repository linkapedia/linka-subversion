/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 24, 2003
 * Time: 1:35:47 PM
 */

/*
  <CORPUSID>10000002</CORPUSID>
  <CLIENTID>100</CLIENTID>
  <CORPUS_NAME>Biological agents</CORPUS_NAME>
  <CORPUSDESC>null</CORPUSDESC>
  <ROCF1>0.0</ROCF1>
  <ROCF2>0.0</ROCF2>
  <ROCF3>0.0</ROCF3>
  <ROCC1>0.0</ROCC1>
  <ROCC2>0.0</ROCC2>
  <ROCC3>0.0</ROCC3>
  <CORPUSACTIVE>1</CORPUSACTIVE>
 */
import java.util.Vector;

public class Corpus {
    // User attributes
    private String CorpusID;
    private String CorpusName;
    private String CorpusDesc;
    private String CorpusActive;

    // constructor(s)
    public Corpus (HashTree ht) {
        CorpusID = (String) ht.get("CORPUSID");
        CorpusName = (String) ht.get("CORPUS_NAME");
        System.out.println("CorpusID: "+CorpusID);
        if (ht.containsKey("CORPUSDESC")) {
            CorpusDesc = (String) ht.get("CORPUSDESC"); }
        if (ht.containsKey("CORPUSACTIVE")) {
            CorpusActive = (String) ht.get("CORPUSACTIVE"); }
    }

    public Corpus (Vector v) {
        CorpusID = (String) v.elementAt(0);
        CorpusName = (String) v.elementAt(2);
        CorpusDesc = (String) v.elementAt(3);
        CorpusActive = (String) v.elementAt(10);
    }

    // accessor functions
    public String getID() { return CorpusID; }
    public String getName() { return CorpusName; }
    public String getDescription() { return CorpusDesc; }
    public String getActive() { return CorpusActive; }

    public void setID(String ID) { CorpusID = ID; }
    public void setName(String Name) { CorpusName = Name; }
    public void setDescription(String Description) { CorpusDesc = Description; }
    public void setActive(String Active) { CorpusActive = Active; }
}
