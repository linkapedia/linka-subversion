#!C:\PERL\BIN\PERL

use DBI; 

open (FILE, "synonymCountry.txt"); @lines = <FILE>; close(FILE);
local ($dbh) = DBI->connect("dbi:Oracle:dallas", "sbooks", "racer9");

local ($new) = 0; local ($old) = 0;
local (%wordlist);

print "Loading thesaurus words...";
&loadThesaurusWords();
print "done.\n";

foreach $line (@lines) {
   chomp($line);
   @words = split(/\t/, $line);

   local ($curword) = &cleanWord($words[1]); 
   local ($synword) = &cleanWord($words[2]);

   local ($wordida) = &getOrInsertWord($curword); 
   local ($wordidb) = &getOrInsertWord($synword); 

   # insert the relationship
   print "Inserting relationships $wordida to $wordidb.\n";
   $q = "insert into thesaurusrelations values (1, $wordida, $wordidb, 1)";
   $sth = $dbh->prepare($q) || warn "failure: $q\n";
   $sth->execute() || warn "failure: $q\n";
   $sth->finish(); 
}

print "\nTotal: ".($new+$old)." New: $new Existing: $old\n";

$dbh->disconnect();

1;

sub loadThesaurusWords {
   $q = "select wordid, thesaurusword from thesaurusword";
   $sth = $dbh->prepare($q) || die "failure: $q\n";
   $sth->execute() || die "failure: $q\n";
   while (my($wid, $word) = $sth->fetchrow_array()) { $wordlist{&cleanWord($word)} = $wid; }
   $sth->finish(); 
}

sub getOrInsertWord {
   my ($cword) = $_[0];
   my ($wordid) = "";

   if (!defined($wordlist{$cword})) {
     $q = "insert into thesaurusword (thesaurusword) values ('$cword')";
     $sth = $dbh->prepare($q) || die "failure: $q\n";
     $sth->execute() || die "failure: $q\n";
     $sth->finish(); 

     $q = "select wordid from thesaurusword where thesaurusword = '$cword'";
     $sth = $dbh->prepare($q) || die "failure: $q\n";
     $sth->execute() || die "failure: $q\n";
     $wordid = $sth->fetchrow_array(); $wordlist{$curword} = $wordid;
     $sth->finish(); $new++;
     $wordlist{$cword} = $wordid;
   } else { $wordid = $wordlist{$cword}; $old++; }

   print "Word: ".$words[1]." ID: ".$wordid."\n";

   return $wordid;
}

sub cleanWord {
   my ($cword) = $_[0];

   $cword =~ s/\'/\'\'/gi;
   $cword =~ s/\"//gi;
   $cword =~ tr/[A-Z]/[a-z]/;

   return $cword;
}
