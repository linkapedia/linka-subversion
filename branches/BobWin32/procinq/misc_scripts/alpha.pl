#!C:\PERL\BIN\PERL

use DBI;
use Getopt::Long;

$| = 1;

local ($cid) = 0;
&GetOptions ('corpus=s' => \$cid);

if ($cid == 0) { die "Usage: alpha.pl -corpus <corpusid>\n"; }

local ($dbh) = DBI->connect("dbi:Oracle:client", "sbooks", "racer9");
local ($q) = "select max(nodeid) from node";
local ($sth) = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
local ($nid) = $sth->fetchrow_array();
$sth->finish();

$q = "select clientid from corpus where corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
local ($clid) = $sth->fetchrow_array();
$sth->finish();

$q = "select nodeid from node where corpusid = $cid and parentid = -1";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
local ($pid) = $sth->fetchrow_array();
$sth->finish();

print "Alphabetize has begun.  Corpus: $cid Parent: $pid Client: $clid\n";

## 
# Create 27 nodes, A-Z plus Other and assign parent nodes

# A
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'A', $pid, 1, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'A%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "A..";

# B
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'B', $pid, 2, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'B%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "B..";

# C
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'C', $pid, 3, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'C%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "C..";

# D
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'D', $pid, 4, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'D%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "D..";

# E
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'E', $pid, 5, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'E%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "E..";

# F
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'F', $pid, 6, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'F%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "F..";

# G
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'G', $pid, 7, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'G%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "G..\n";

# H
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'H', $pid, 8, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'H%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "H..";

# I
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'I', $pid, 9, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'I%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "I..";

# J
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'J', $pid, 10, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'J%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "J..";

# K
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'K', $pid, 11, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'K%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "K..";

# L
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'L', $pid, 12, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'L%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "L..";

# M
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'M', $pid, 13, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'M%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "M..";

# N
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'N', $pid, 14, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'N%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "N..\n";

# O
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'O', $pid, 15, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'O%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "O..";

# P
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'P', $pid, 16, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'P%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "P..";

# Q
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'Q', $pid, 17, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'Q%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "Q..";

# R
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'R', $pid, 18, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'R%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "R..";

# S
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'S', $pid, 19, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'S%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "S..";

# T
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'T', $pid, 20, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'T%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "T..";

# U
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'U', $pid, 21, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'U%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "U..\n";

# V
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'V', $pid, 22, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'V%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "V..";

# W
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'W', $pid, 23, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'W%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "W..";

# X
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'X', $pid, 24, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'X%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "X..";

# Y
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'Y', $pid, 25, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'Y%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "Y..";

# Z
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'Z', $pid, 26, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and upper(nodetitle) like 'Z%' and corpusid = $cid";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "Z..";

# oTHERS
$nid++;
$q = "insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot) ".
     "values ($nid, $cid, $clid, 'Others', $pid, 27, 2)";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
$q = "update node set parentid = $nid where parentid = $pid and nodeid != $nid and corpusid = $cid ".
     "and (nodeid < ".($nid-27)." or nodeid > ".($nid).")";
$sth = $dbh->prepare($q) || die "Could not execute query: $q\n";
$sth->execute() || die "Could not execute query: $q\n";
$sth->finish();
print "Others..\n\n";

print "Done.\n";

$dbh->disconnect();




