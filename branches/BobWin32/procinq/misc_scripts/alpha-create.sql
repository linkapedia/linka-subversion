insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (1, 30, 22, 'Concept/Theme', -1, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (2, 31, 22, 'Reference Text', -1, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (3, 32, 22, 'Company/Authority', -1, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (4, 33, 22, 'Product/Brand', -1, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (5, 34, 22, 'Therap. Class/Disease', -1, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (6, 35, 22, 'Company', -1, 1, 1);
commit;

insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (14, 30, 22, 'A', 1, 1, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (15, 30, 22, 'B', 1, 2, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (16, 30, 22, 'C', 1, 3, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (17, 30, 22, 'D', 1, 4, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (18, 30, 22, 'E', 1, 5, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (19, 30, 22, 'F', 1, 6, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (20, 30, 22, 'G', 1, 7, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (21, 30, 22, 'H', 1, 8, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (22, 30, 22, 'I', 1, 9, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (23, 30, 22, 'J', 1, 10, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (24, 30, 22, 'K', 1, 11, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (25, 30, 22, 'L', 1, 12, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (26, 30, 22, 'M', 1, 13, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (27, 30, 22, 'N', 1, 14, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (28, 30, 22, 'O', 1, 15, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (29, 30, 22, 'P', 1, 16, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (30, 30, 22, 'Q', 1, 17, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (31, 30, 22, 'R', 1, 18, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (32, 30, 22, 'S', 1, 19, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (33, 30, 22, 'T', 1, 20, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (34, 30, 22, 'U', 1, 21, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (35, 30, 22, 'V', 1, 22, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (36, 30, 22, 'W', 1, 23, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (37, 30, 22, 'X', 1, 24, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (38, 30, 22, 'Y', 1, 25, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (39, 30, 22, 'Z', 1, 26, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (40, 30, 22, '1,2,3...', 1, -30, 2);
commit;

insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (41, 31, 22, 'A', 2, 1, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (42, 31, 22, 'B', 2, 2, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (43, 31, 22, 'C', 2, 3, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (44, 31, 22, 'D', 2, 4, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (45, 31, 22, 'E', 2, 5, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (46, 31, 22, 'F', 2, 6, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (47, 31, 22, 'G', 2, 7, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (48, 31, 22, 'H', 2, 8, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (49, 31, 22, 'I', 2, 9, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (50, 31, 22, 'J', 2, 10, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (51, 31, 22, 'K', 2, 11, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (52, 31, 22, 'L', 2, 12, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (53, 31, 22, 'M', 2, 13, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (54, 31, 22, 'N', 2, 14, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (55, 31, 22, 'O', 2, 15, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (56, 31, 22, 'P', 2, 16, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (57, 31, 22, 'Q', 2, 17, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (58, 31, 22, 'R', 2, 18, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (59, 31, 22, 'S', 2, 19, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (60, 31, 22, 'T', 2, 20, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (61, 31, 22, 'U', 2, 21, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (62, 31, 22, 'V', 2, 22, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (63, 31, 22, 'W', 2, 23, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (64, 31, 22, 'X', 2, 24, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (65, 31, 22, 'Y', 2, 25, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (66, 31, 22, 'Z', 2, 26, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (67, 31, 22, '1,2,3...', 2, -30, 2);
commit;

insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (68, 32, 22, 'A', 3, 1, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (69, 32, 22, 'B', 3, 2, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (70, 32, 22, 'C', 3, 3, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (71, 32, 22, 'D', 3, 4, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (72, 32, 22, 'E', 3, 5, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (73, 32, 22, 'F', 3, 6, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (74, 32, 22, 'G', 3, 7, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (75, 32, 22, 'H', 3, 8, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (76, 32, 22, 'I', 3, 9, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (77, 32, 22, 'J', 3, 10, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (78, 32, 22, 'K', 3, 11, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (79, 32, 22, 'L', 3, 12, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (80, 32, 22, 'M', 3, 13, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (81, 32, 22, 'N', 3, 14, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (82, 32, 22, 'O', 3, 15, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (83, 32, 22, 'P', 3, 16, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (84, 32, 22, 'Q', 3, 17, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (85, 32, 22, 'R', 3, 18, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (86, 32, 22, 'S', 3, 19, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (87, 32, 22, 'T', 3, 20, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (88, 32, 22, 'U', 3, 21, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (89, 32, 22, 'V', 3, 22, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (90, 32, 22, 'W', 3, 23, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (91, 32, 22, 'X', 3, 24, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (92, 32, 22, 'Y', 3, 25, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (93, 32, 22, 'Z', 3, 26, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (94, 32, 22, '1,2,3...', 3, -30, 2);
commit;

insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (95, 33, 22, 'A', 4, 1, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (96, 33, 22, 'B', 4, 2, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (97, 33, 22, 'C', 4, 3, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (98, 33, 22, 'D', 4, 4, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (99, 33, 22, 'E', 4, 5, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (100, 33, 22, 'F', 4, 6, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (101, 33, 22, 'G', 4, 7, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (102, 33, 22, 'H', 4, 8, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (103, 33, 22, 'I', 4, 9, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (104, 33, 22, 'J', 4, 10, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (105, 33, 22, 'K', 4, 11, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (106, 33, 22, 'L', 4, 12, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (107, 33, 22, 'M', 4, 13, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (108, 33, 22, 'N', 4, 14, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (109, 33, 22, 'O', 4, 15, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (110, 33, 22, 'P', 4, 16, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (111, 33, 22, 'Q', 4, 17, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (112, 33, 22, 'R', 4, 18, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (113, 33, 22, 'S', 4, 19, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (114, 33, 22, 'T', 4, 20, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (115, 33, 22, 'U', 4, 21, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (116, 33, 22, 'V', 4, 22, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (117, 33, 22, 'W', 4, 23, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (118, 33, 22, 'X', 4, 24, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (119, 33, 22, 'Y', 4, 25, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (120, 33, 22, 'Z', 4, 26, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (121, 33, 22, '1,2,3...', 4, -30, 2);
commit;

insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (122, 34, 22, 'A', 5, 1, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (123, 34, 22, 'B', 5, 2, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (124, 34, 22, 'C', 5, 3, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (125, 34, 22, 'D', 5, 4, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (126, 34, 22, 'E', 5, 5, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (127, 34, 22, 'F', 5, 6, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (128, 34, 22, 'G', 5, 7, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (129, 34, 22, 'H', 5, 8, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (130, 34, 22, 'I', 5, 9, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (131, 34, 22, 'J', 5, 10, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (132, 34, 22, 'K', 5, 11, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (133, 34, 22, 'L', 5, 12, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (134, 34, 22, 'M', 5, 13, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (135, 34, 22, 'N', 5, 14, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (136, 34, 22, 'O', 5, 15, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (137, 34, 22, 'P', 5, 16, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (138, 34, 22, 'Q', 5, 17, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (139, 34, 22, 'R', 5, 18, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (140, 34, 22, 'S', 5, 19, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (141, 34, 22, 'T', 5, 20, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (142, 34, 22, 'U', 5, 21, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (143, 34, 22, 'V', 5, 22, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (144, 34, 22, 'W', 5, 23, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (145, 34, 22, 'X', 5, 24, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (146, 34, 22, 'Y', 5, 25, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (147, 34, 22, 'Z', 5, 26, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (148, 34, 22, '1,2,3...', 5, -30, 2);
commit;

insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (149, 35, 22, 'A', 6, 1, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (150, 35, 22, 'B', 6, 2, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (151, 35, 22, 'C', 6, 3, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (152, 35, 22, 'D', 6, 4, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (153, 35, 22, 'E', 6, 5, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (154, 35, 22, 'F', 6, 6, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (155, 35, 22, 'G', 6, 7, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (156, 35, 22, 'H', 6, 8, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (157, 35, 22, 'I', 6, 9, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (158, 35, 22, 'J', 6, 10, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (159, 35, 22, 'K', 6, 11, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (160, 35, 22, 'L', 6, 12, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (161, 35, 22, 'M', 6, 13, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (162, 35, 22, 'N', 6, 14, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (163, 35, 22, 'O', 6, 15, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (164, 35, 22, 'P', 6, 16, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (165, 35, 22, 'Q', 6, 17, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (166, 35, 22, 'R', 6, 18, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (167, 35, 22, 'S', 6, 19, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (168, 35, 22, 'T', 6, 20, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (169, 35, 22, 'U', 6, 21, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (170, 35, 22, 'V', 6, 22, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (171, 35, 22, 'W', 6, 23, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (172, 35, 22, 'X', 6, 24, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (173, 35, 22, 'Y', 6, 25, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (174, 35, 22, 'Z', 6, 26, 2);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (175, 35, 22, '1,2,3...', 6, -30, 2);
commit;

insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (7, 30, 22, 'PENDING TOPIC', 29, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (8, 31, 22, 'PENDING TOPIC', 56, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (9, 32, 22, 'PENDING TOPIC', 83, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (10, 33, 22, 'PENDING TOPIC', 110, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (11, 34, 22, 'PENDING TOPIC', 137, 1, 1);
insert into node (nodeid, corpusid, clientid, nodetitle, parentid, nodeindexwithinparent, depthfromroot)
  values (12, 35, 22, 'PENDING TOPIC', 164, 1, 1);
commit;

--
-- RUN J.ZADROGA'S IMPORT SCRIPT PLUS 'COMPANY' TAX IMPORT SCRIPTS BEFORE EXECUTING NEXT SET OF SQL --
--

update node set parentid = 6 where parentid = 2205999;
commit;

update node set parentid = 14 where corpusid = 30 and lower(nodetitle) like 'a%' and parentid = 7 and nodetitle != 'A';
update node set parentid = 15 where corpusid = 30 and lower(nodetitle) like 'b%' and parentid = 7 and nodetitle != 'B';
update node set parentid = 16 where corpusid = 30 and lower(nodetitle) like 'c%' and parentid = 7 and nodetitle != 'C';
update node set parentid = 17 where corpusid = 30 and lower(nodetitle) like 'd%' and parentid = 7 and nodetitle != 'D';
update node set parentid = 18 where corpusid = 30 and lower(nodetitle) like 'e%' and parentid = 7 and nodetitle != 'E';
update node set parentid = 19 where corpusid = 30 and lower(nodetitle) like 'f%' and parentid = 7 and nodetitle != 'F';
update node set parentid = 20 where corpusid = 30 and lower(nodetitle) like 'g%' and parentid = 7 and nodetitle != 'G';
update node set parentid = 21 where corpusid = 30 and lower(nodetitle) like 'h%' and parentid = 7 and nodetitle != 'H';
update node set parentid = 22 where corpusid = 30 and lower(nodetitle) like 'i%' and parentid = 7 and nodetitle != 'I';
update node set parentid = 23 where corpusid = 30 and lower(nodetitle) like 'j%' and parentid = 7 and nodetitle != 'J';
update node set parentid = 24 where corpusid = 30 and lower(nodetitle) like 'k%' and parentid = 7 and nodetitle != 'K';
update node set parentid = 25 where corpusid = 30 and lower(nodetitle) like 'l%' and parentid = 7 and nodetitle != 'L';
update node set parentid = 26 where corpusid = 30 and lower(nodetitle) like 'm%' and parentid = 7 and nodetitle != 'M';
update node set parentid = 27 where corpusid = 30 and lower(nodetitle) like 'n%' and parentid = 7 and nodetitle != 'N';
update node set parentid = 28 where corpusid = 30 and lower(nodetitle) like 'o%' and parentid = 7 and nodetitle != 'O';
update node set parentid = 29 where corpusid = 30 and lower(nodetitle) like 'p%' and parentid = 7 and nodetitle != 'P';
update node set parentid = 30 where corpusid = 30 and lower(nodetitle) like 'q%' and parentid = 7 and nodetitle != 'Q';
update node set parentid = 31 where corpusid = 30 and lower(nodetitle) like 'r%' and parentid = 7 and nodetitle != 'R';
update node set parentid = 32 where corpusid = 30 and lower(nodetitle) like 's%' and parentid = 7 and nodetitle != 'S';
update node set parentid = 33 where corpusid = 30 and lower(nodetitle) like 't%' and parentid = 7 and nodetitle != 'T';
update node set parentid = 34 where corpusid = 30 and lower(nodetitle) like 'u%' and parentid = 7 and nodetitle != 'U';
update node set parentid = 35 where corpusid = 30 and lower(nodetitle) like 'v%' and parentid = 7 and nodetitle != 'V';
update node set parentid = 36 where corpusid = 30 and lower(nodetitle) like 'w%' and parentid = 7 and nodetitle != 'W';
update node set parentid = 37 where corpusid = 30 and lower(nodetitle) like 'x%' and parentid = 7 and nodetitle != 'X';
update node set parentid = 38 where corpusid = 30 and lower(nodetitle) like 'y%' and parentid = 7 and nodetitle != 'Y';
update node set parentid = 39 where corpusid = 30 and lower(nodetitle) like 'z%' and parentid = 7 and nodetitle != 'Z';
update node set parentid = 40 where corpusid = 30 and parentid = 7 and nodetitle != '1,2,3...' 
and nodetitle != 'A' and nodetitle != 'K' and nodetitle != 'R' and nodetitle != 'D' and 
nodetitle != 'E' and nodetitle != 'L' and nodetitle != 'S' and nodetitle != 'Y' and 
nodetitle != 'F' and nodetitle != 'M' and nodetitle != 'T' and nodetitle != 'Z' and 
nodetitle != 'G' and nodetitle != 'N' and nodetitle != 'U' and nodetitle != 'B' and 
nodetitle != 'H' and nodetitle != 'O' and nodetitle != 'V' and nodetitle != 'C' and 
nodetitle != 'I' and nodetitle != 'P' and nodetitle != 'W' and
nodetitle != 'J' and nodetitle != 'Q' and nodetitle != 'X';

update node set parentid = 41 where corpusid = 31 and lower(nodetitle) like 'a%' and parentid = 8 and nodetitle != 'A';
update node set parentid = 42 where corpusid = 31 and lower(nodetitle) like 'b%' and parentid = 8 and nodetitle != 'B';
update node set parentid = 43 where corpusid = 31 and lower(nodetitle) like 'c%' and parentid = 8 and nodetitle != 'C';
update node set parentid = 44 where corpusid = 31 and lower(nodetitle) like 'd%' and parentid = 8 and nodetitle != 'D';
update node set parentid = 45 where corpusid = 31 and lower(nodetitle) like 'e%' and parentid = 8 and nodetitle != 'E';
update node set parentid = 46 where corpusid = 31 and lower(nodetitle) like 'f%' and parentid = 8 and nodetitle != 'F';
update node set parentid = 47 where corpusid = 31 and lower(nodetitle) like 'g%' and parentid = 8 and nodetitle != 'G';
update node set parentid = 48 where corpusid = 31 and lower(nodetitle) like 'h%' and parentid = 8 and nodetitle != 'H';
update node set parentid = 49 where corpusid = 31 and lower(nodetitle) like 'i%' and parentid = 8 and nodetitle != 'I';
update node set parentid = 50 where corpusid = 31 and lower(nodetitle) like 'j%' and parentid = 8 and nodetitle != 'J';
update node set parentid = 51 where corpusid = 31 and lower(nodetitle) like 'k%' and parentid = 8 and nodetitle != 'K';
update node set parentid = 52 where corpusid = 31 and lower(nodetitle) like 'l%' and parentid = 8 and nodetitle != 'L';
update node set parentid = 53 where corpusid = 31 and lower(nodetitle) like 'm%' and parentid = 8 and nodetitle != 'M';
update node set parentid = 54 where corpusid = 31 and lower(nodetitle) like 'n%' and parentid = 8 and nodetitle != 'N';
update node set parentid = 55 where corpusid = 31 and lower(nodetitle) like 'o%' and parentid = 8 and nodetitle != 'O';
update node set parentid = 56 where corpusid = 31 and lower(nodetitle) like 'p%' and parentid = 8 and nodetitle != 'P';
update node set parentid = 57 where corpusid = 31 and lower(nodetitle) like 'q%' and parentid = 8 and nodetitle != 'Q';
update node set parentid = 58 where corpusid = 31 and lower(nodetitle) like 'r%' and parentid = 8 and nodetitle != 'R';
update node set parentid = 59 where corpusid = 31 and lower(nodetitle) like 's%' and parentid = 8 and nodetitle != 'S';
update node set parentid = 60 where corpusid = 31 and lower(nodetitle) like 't%' and parentid = 8 and nodetitle != 'T';
update node set parentid = 61 where corpusid = 31 and lower(nodetitle) like 'u%' and parentid = 8 and nodetitle != 'U';
update node set parentid = 62 where corpusid = 31 and lower(nodetitle) like 'v%' and parentid = 8 and nodetitle != 'V';
update node set parentid = 63 where corpusid = 31 and lower(nodetitle) like 'w%' and parentid = 8 and nodetitle != 'W';
update node set parentid = 64 where corpusid = 31 and lower(nodetitle) like 'x%' and parentid = 8 and nodetitle != 'X';
update node set parentid = 65 where corpusid = 31 and lower(nodetitle) like 'y%' and parentid = 8 and nodetitle != 'Y';
update node set parentid = 66 where corpusid = 31 and lower(nodetitle) like 'z%' and parentid = 8 and nodetitle != 'Z';
update node set parentid = 67 where corpusid = 31 and parentid = 8 and nodetitle != '1,2,3...' 
and nodetitle != 'A' and nodetitle != 'K' and nodetitle != 'R' and nodetitle != 'D' and 
nodetitle != 'E' and nodetitle != 'L' and nodetitle != 'S' and nodetitle != 'Y' and 
nodetitle != 'F' and nodetitle != 'M' and nodetitle != 'T' and nodetitle != 'Z' and 
nodetitle != 'G' and nodetitle != 'N' and nodetitle != 'U' and nodetitle != 'B' and 
nodetitle != 'H' and nodetitle != 'O' and nodetitle != 'V' and nodetitle != 'C' and 
nodetitle != 'I' and nodetitle != 'P' and nodetitle != 'W' and
nodetitle != 'J' and nodetitle != 'Q' and nodetitle != 'X';

update node set parentid = 68 where corpusid = 32 and lower(nodetitle) like 'a%' and parentid = 9 and nodetitle != 'A';
update node set parentid = 69 where corpusid = 32 and lower(nodetitle) like 'b%' and parentid = 9 and nodetitle != 'B';
update node set parentid = 70 where corpusid = 32 and lower(nodetitle) like 'c%' and parentid = 9 and nodetitle != 'C';
update node set parentid = 71 where corpusid = 32 and lower(nodetitle) like 'd%' and parentid = 9 and nodetitle != 'D';
update node set parentid = 72 where corpusid = 32 and lower(nodetitle) like 'e%' and parentid = 9 and nodetitle != 'E';
update node set parentid = 73 where corpusid = 32 and lower(nodetitle) like 'f%' and parentid = 9 and nodetitle != 'F';
update node set parentid = 74 where corpusid = 32 and lower(nodetitle) like 'g%' and parentid = 9 and nodetitle != 'G';
update node set parentid = 75 where corpusid = 32 and lower(nodetitle) like 'h%' and parentid = 9 and nodetitle != 'H';
update node set parentid = 76 where corpusid = 32 and lower(nodetitle) like 'i%' and parentid = 9 and nodetitle != 'I';
update node set parentid = 77 where corpusid = 32 and lower(nodetitle) like 'j%' and parentid = 9 and nodetitle != 'J';
update node set parentid = 78 where corpusid = 32 and lower(nodetitle) like 'k%' and parentid = 9 and nodetitle != 'K';
update node set parentid = 79 where corpusid = 32 and lower(nodetitle) like 'l%' and parentid = 9 and nodetitle != 'L';
update node set parentid = 80 where corpusid = 32 and lower(nodetitle) like 'm%' and parentid = 9 and nodetitle != 'M';
update node set parentid = 81 where corpusid = 32 and lower(nodetitle) like 'n%' and parentid = 9 and nodetitle != 'N';
update node set parentid = 82 where corpusid = 32 and lower(nodetitle) like 'o%' and parentid = 9 and nodetitle != 'O';
update node set parentid = 83 where corpusid = 32 and lower(nodetitle) like 'p%' and parentid = 9 and nodetitle != 'P';
update node set parentid = 84 where corpusid = 32 and lower(nodetitle) like 'q%' and parentid = 9 and nodetitle != 'Q';
update node set parentid = 85 where corpusid = 32 and lower(nodetitle) like 'r%' and parentid = 9 and nodetitle != 'R';
update node set parentid = 86 where corpusid = 32 and lower(nodetitle) like 's%' and parentid = 9 and nodetitle != 'S';
update node set parentid = 87 where corpusid = 32 and lower(nodetitle) like 't%' and parentid = 9 and nodetitle != 'T';
update node set parentid = 88 where corpusid = 32 and lower(nodetitle) like 'u%' and parentid = 9 and nodetitle != 'U';
update node set parentid = 89 where corpusid = 32 and lower(nodetitle) like 'v%' and parentid = 9 and nodetitle != 'V';
update node set parentid = 90 where corpusid = 32 and lower(nodetitle) like 'w%' and parentid = 9 and nodetitle != 'W';
update node set parentid = 91 where corpusid = 32 and lower(nodetitle) like 'x%' and parentid = 9 and nodetitle != 'X';
update node set parentid = 92 where corpusid = 32 and lower(nodetitle) like 'y%' and parentid = 9 and nodetitle != 'Y';
update node set parentid = 93 where corpusid = 32 and lower(nodetitle) like 'z%' and parentid = 9 and nodetitle != 'Z';
update node set parentid = 94 where corpusid = 32 and parentid = 9 and nodetitle != '1,2,3...' 
and nodetitle != 'A' and nodetitle != 'K' and nodetitle != 'R' and nodetitle != 'D' and 
nodetitle != 'E' and nodetitle != 'L' and nodetitle != 'S' and nodetitle != 'Y' and 
nodetitle != 'F' and nodetitle != 'M' and nodetitle != 'T' and nodetitle != 'Z' and 
nodetitle != 'G' and nodetitle != 'N' and nodetitle != 'U' and nodetitle != 'B' and 
nodetitle != 'H' and nodetitle != 'O' and nodetitle != 'V' and nodetitle != 'C' and 
nodetitle != 'I' and nodetitle != 'P' and nodetitle != 'W' and
nodetitle != 'J' and nodetitle != 'Q' and nodetitle != 'X';

update node set parentid = 95 where corpusid = 33 and lower(nodetitle) like 'a%' and parentid = 10 and nodetitle != 'A';
update node set parentid = 96 where corpusid = 33 and lower(nodetitle) like 'b%' and parentid = 10 and nodetitle != 'B';
update node set parentid = 97 where corpusid = 33 and lower(nodetitle) like 'c%' and parentid = 10 and nodetitle != 'C';
update node set parentid = 98 where corpusid = 33 and lower(nodetitle) like 'd%' and parentid = 10 and nodetitle != 'D';
update node set parentid = 99 where corpusid = 33 and lower(nodetitle) like 'e%' and parentid = 10 and nodetitle != 'E';
update node set parentid = 100 where corpusid = 33 and lower(nodetitle) like 'f%' and parentid = 10 and nodetitle != 'F';
update node set parentid = 101 where corpusid = 33 and lower(nodetitle) like 'g%' and parentid = 10 and nodetitle != 'G';
update node set parentid = 102 where corpusid = 33 and lower(nodetitle) like 'h%' and parentid = 10 and nodetitle != 'H';
update node set parentid = 103 where corpusid = 33 and lower(nodetitle) like 'i%' and parentid = 10 and nodetitle != 'I';
update node set parentid = 104 where corpusid = 33 and lower(nodetitle) like 'j%' and parentid = 10 and nodetitle != 'J';
update node set parentid = 105 where corpusid = 33 and lower(nodetitle) like 'k%' and parentid = 10 and nodetitle != 'K';
update node set parentid = 106 where corpusid = 33 and lower(nodetitle) like 'l%' and parentid = 10 and nodetitle != 'L';
update node set parentid = 107 where corpusid = 33 and lower(nodetitle) like 'm%' and parentid = 10 and nodetitle != 'M';
update node set parentid = 108 where corpusid = 33 and lower(nodetitle) like 'n%' and parentid = 10 and nodetitle != 'N';
update node set parentid = 109 where corpusid = 33 and lower(nodetitle) like 'o%' and parentid = 10 and nodetitle != 'O';
update node set parentid = 110 where corpusid = 33 and lower(nodetitle) like 'p%' and parentid = 10 and nodetitle != 'P';
update node set parentid = 111 where corpusid = 33 and lower(nodetitle) like 'q%' and parentid = 10 and nodetitle != 'Q';
update node set parentid = 112 where corpusid = 33 and lower(nodetitle) like 'r%' and parentid = 10 and nodetitle != 'R';
update node set parentid = 113 where corpusid = 33 and lower(nodetitle) like 's%' and parentid = 10 and nodetitle != 'S';
update node set parentid = 114 where corpusid = 33 and lower(nodetitle) like 't%' and parentid = 10 and nodetitle != 'T';
update node set parentid = 115 where corpusid = 33 and lower(nodetitle) like 'u%' and parentid = 10 and nodetitle != 'U';
update node set parentid = 116 where corpusid = 33 and lower(nodetitle) like 'v%' and parentid = 10 and nodetitle != 'V';
update node set parentid = 117 where corpusid = 33 and lower(nodetitle) like 'w%' and parentid = 10 and nodetitle != 'W';
update node set parentid = 118 where corpusid = 33 and lower(nodetitle) like 'x%' and parentid = 10 and nodetitle != 'X';
update node set parentid = 119 where corpusid = 33 and lower(nodetitle) like 'y%' and parentid = 10 and nodetitle != 'Y';
update node set parentid = 120 where corpusid = 33 and lower(nodetitle) like 'z%' and parentid = 10 and nodetitle != 'Z';
update node set parentid = 121 where corpusid = 33 and parentid = 10 and nodetitle != '1,2,3...' 
and nodetitle != 'A' and nodetitle != 'K' and nodetitle != 'R' and nodetitle != 'D' and 
nodetitle != 'E' and nodetitle != 'L' and nodetitle != 'S' and nodetitle != 'Y' and 
nodetitle != 'F' and nodetitle != 'M' and nodetitle != 'T' and nodetitle != 'Z' and 
nodetitle != 'G' and nodetitle != 'N' and nodetitle != 'U' and nodetitle != 'B' and 
nodetitle != 'H' and nodetitle != 'O' and nodetitle != 'V' and nodetitle != 'C' and 
nodetitle != 'I' and nodetitle != 'P' and nodetitle != 'W' and
nodetitle != 'J' and nodetitle != 'Q' and nodetitle != 'X';

update node set parentid = 122 where corpusid = 34 and lower(nodetitle) like 'a%' and parentid = 11 and nodetitle != 'A';
update node set parentid = 123 where corpusid = 34 and lower(nodetitle) like 'b%' and parentid = 11 and nodetitle != 'B';
update node set parentid = 124 where corpusid = 34 and lower(nodetitle) like 'c%' and parentid = 11 and nodetitle != 'C';
update node set parentid = 125 where corpusid = 34 and lower(nodetitle) like 'd%' and parentid = 11 and nodetitle != 'D';
update node set parentid = 126 where corpusid = 34 and lower(nodetitle) like 'e%' and parentid = 11 and nodetitle != 'E';
update node set parentid = 127 where corpusid = 34 and lower(nodetitle) like 'f%' and parentid = 11 and nodetitle != 'F';
update node set parentid = 128 where corpusid = 34 and lower(nodetitle) like 'g%' and parentid = 11 and nodetitle != 'G';
update node set parentid = 129 where corpusid = 34 and lower(nodetitle) like 'h%' and parentid = 11 and nodetitle != 'H';
update node set parentid = 130 where corpusid = 34 and lower(nodetitle) like 'i%' and parentid = 11 and nodetitle != 'I';
update node set parentid = 131 where corpusid = 34 and lower(nodetitle) like 'j%' and parentid = 11 and nodetitle != 'J';
update node set parentid = 132 where corpusid = 34 and lower(nodetitle) like 'k%' and parentid = 11 and nodetitle != 'K';
update node set parentid = 133 where corpusid = 34 and lower(nodetitle) like 'l%' and parentid = 11 and nodetitle != 'L';
update node set parentid = 134 where corpusid = 34 and lower(nodetitle) like 'm%' and parentid = 11 and nodetitle != 'M';
update node set parentid = 135 where corpusid = 34 and lower(nodetitle) like 'n%' and parentid = 11 and nodetitle != 'N';
update node set parentid = 136 where corpusid = 34 and lower(nodetitle) like 'o%' and parentid = 11 and nodetitle != 'O';
update node set parentid = 137 where corpusid = 34 and lower(nodetitle) like 'p%' and parentid = 11 and nodetitle != 'P';
update node set parentid = 138 where corpusid = 34 and lower(nodetitle) like 'q%' and parentid = 11 and nodetitle != 'Q';
update node set parentid = 139 where corpusid = 34 and lower(nodetitle) like 'r%' and parentid = 11 and nodetitle != 'R';
update node set parentid = 140 where corpusid = 34 and lower(nodetitle) like 's%' and parentid = 11 and nodetitle != 'S';
update node set parentid = 141 where corpusid = 34 and lower(nodetitle) like 't%' and parentid = 11 and nodetitle != 'T';
update node set parentid = 142 where corpusid = 34 and lower(nodetitle) like 'u%' and parentid = 11 and nodetitle != 'U';
update node set parentid = 143 where corpusid = 34 and lower(nodetitle) like 'v%' and parentid = 11 and nodetitle != 'V';
update node set parentid = 144 where corpusid = 34 and lower(nodetitle) like 'w%' and parentid = 11 and nodetitle != 'W';
update node set parentid = 145 where corpusid = 34 and lower(nodetitle) like 'x%' and parentid = 11 and nodetitle != 'X';
update node set parentid = 146 where corpusid = 34 and lower(nodetitle) like 'y%' and parentid = 11 and nodetitle != 'Y';
update node set parentid = 147 where corpusid = 34 and lower(nodetitle) like 'z%' and parentid = 11 and nodetitle != 'Z';
update node set parentid = 148 where corpusid = 34 and parentid = 11 and nodetitle != '1,2,3...' 
and nodetitle != 'A' and nodetitle != 'K' and nodetitle != 'R' and nodetitle != 'D' and 
nodetitle != 'E' and nodetitle != 'L' and nodetitle != 'S' and nodetitle != 'Y' and 
nodetitle != 'F' and nodetitle != 'M' and nodetitle != 'T' and nodetitle != 'Z' and 
nodetitle != 'G' and nodetitle != 'N' and nodetitle != 'U' and nodetitle != 'B' and 
nodetitle != 'H' and nodetitle != 'O' and nodetitle != 'V' and nodetitle != 'C' and 
nodetitle != 'I' and nodetitle != 'P' and nodetitle != 'W' and
nodetitle != 'J' and nodetitle != 'Q' and nodetitle != 'X';
commit;

update node set parentid = 149 where corpusid = 35 and lower(nodetitle) like 'a%' and parentid = 6 and nodetitle != 'A';
update node set parentid = 150 where corpusid = 35 and lower(nodetitle) like 'b%' and parentid = 6 and nodetitle != 'B';
update node set parentid = 151 where corpusid = 35 and lower(nodetitle) like 'c%' and parentid = 6 and nodetitle != 'C';
update node set parentid = 152 where corpusid = 35 and lower(nodetitle) like 'd%' and parentid = 6 and nodetitle != 'D';
update node set parentid = 153 where corpusid = 35 and lower(nodetitle) like 'e%' and parentid = 6 and nodetitle != 'E';
update node set parentid = 154 where corpusid = 35 and lower(nodetitle) like 'f%' and parentid = 6 and nodetitle != 'F';
update node set parentid = 155 where corpusid = 35 and lower(nodetitle) like 'g%' and parentid = 6 and nodetitle != 'G';
update node set parentid = 156 where corpusid = 35 and lower(nodetitle) like 'h%' and parentid = 6 and nodetitle != 'H';
update node set parentid = 157 where corpusid = 35 and lower(nodetitle) like 'i%' and parentid = 6 and nodetitle != 'I';
update node set parentid = 157 where corpusid = 35 and lower(nodetitle) like 'j%' and parentid = 6 and nodetitle != 'J';
update node set parentid = 158 where corpusid = 35 and lower(nodetitle) like 'k%' and parentid = 6 and nodetitle != 'K';
update node set parentid = 159 where corpusid = 35 and lower(nodetitle) like 'l%' and parentid = 6 and nodetitle != 'L';
update node set parentid = 160 where corpusid = 35 and lower(nodetitle) like 'm%' and parentid = 6 and nodetitle != 'M';
update node set parentid = 161 where corpusid = 35 and lower(nodetitle) like 'n%' and parentid = 6 and nodetitle != 'N';
update node set parentid = 162 where corpusid = 35 and lower(nodetitle) like 'o%' and parentid = 6 and nodetitle != 'O';
update node set parentid = 163 where corpusid = 35 and lower(nodetitle) like 'p%' and parentid = 6 and nodetitle != 'P';
update node set parentid = 164 where corpusid = 35 and lower(nodetitle) like 'q%' and parentid = 6 and nodetitle != 'Q';
update node set parentid = 165 where corpusid = 35 and lower(nodetitle) like 'r%' and parentid = 6 and nodetitle != 'R';
update node set parentid = 166 where corpusid = 35 and lower(nodetitle) like 's%' and parentid = 6 and nodetitle != 'S';
update node set parentid = 167 where corpusid = 35 and lower(nodetitle) like 't%' and parentid = 6 and nodetitle != 'T';
update node set parentid = 168 where corpusid = 35 and lower(nodetitle) like 'u%' and parentid = 6 and nodetitle != 'U';
update node set parentid = 169 where corpusid = 35 and lower(nodetitle) like 'v%' and parentid = 6 and nodetitle != 'V';
update node set parentid = 170 where corpusid = 35 and lower(nodetitle) like 'w%' and parentid = 6 and nodetitle != 'W';
update node set parentid = 171 where corpusid = 35 and lower(nodetitle) like 'x%' and parentid = 6 and nodetitle != 'X';
update node set parentid = 172 where corpusid = 35 and lower(nodetitle) like 'y%' and parentid = 6 and nodetitle != 'Y';
update node set parentid = 173 where corpusid = 35 and lower(nodetitle) like 'z%' and parentid = 6 and nodetitle != 'Z';
update node set parentid = 174 where corpusid = 35 and parentid = 6 and nodetitle != '1,2,3...' 
and nodetitle != 'A' and nodetitle != 'K' and nodetitle != 'R' and nodetitle != 'D' and 
nodetitle != 'E' and nodetitle != 'L' and nodetitle != 'S' and nodetitle != 'Y' and 
nodetitle != 'F' and nodetitle != 'M' and nodetitle != 'T' and nodetitle != 'Z' and 
nodetitle != 'G' and nodetitle != 'N' and nodetitle != 'U' and nodetitle != 'B' and 
nodetitle != 'H' and nodetitle != 'O' and nodetitle != 'V' and nodetitle != 'C' and 
nodetitle != 'I' and nodetitle != 'P' and nodetitle != 'W' and
nodetitle != 'J' and nodetitle != 'Q' and nodetitle != 'X';
commit;

