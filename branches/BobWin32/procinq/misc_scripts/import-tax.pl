#!/usr/bin/perl

use Getopt::Long;
use DBI;
require "Node.pm";

open (FILE, "Tax.txt"); local (@lines) = <FILE>; close(FILE);
local (@currArr);
local (%nodes);

foreach $line (@lines) {
   chomp($line);
   local (@elements) = split(/\t/, $line);

   local ($loop) = 0;

   foreach $element (@elements) {
	if ($element ne "") { 
	   local (@tmpArr); 
	   for ($i = 0; $i < $loop; $i++) { push(@tmpArr, $currArr[$i]); }
	   @currArr = @tmpArr; undef @tmpArr;
	   push (@currArr, $element);

	   if (($loop > 0) && (($loop % 2) == 1)) {
		#$element =~ s/(\w+)/\u\L$1/g;
		$element =~ s/\"//gi;
		local ($node) = Node->new;
		$node->{Nid} = "".$currArr[$loop-1];
		$node->{Ntitle} = $element;
		$node->{Ndepth} = "".(2+(($loop+1) / 2));
		if ($loop == 1) { $node->{Nparent} = "2205999"; }
		else { $node->{Nparent} = "".$currArr[$loop-3]; }
		$node->{NIWP} = "".getNIWP($node->Nparent);

		$nodes{$currArr[$loop-1]} = $node; undef $node;
	   }
	}
	$loop++;
   }
}

local ($dbh) = DBI->connect("dbi:Oracle:client", "sbooks", "racer9");
local (%inserted); $inserted{"2205999"} = 1;

foreach $nodeid (sort {$a<=>$b} keys %nodes) {
   &insert_node($nodeid);
}

$dbh->disconnect();

sub getNIWP {
   my ($pid) = $_[0];
   local ($count) = 0;

   foreach $nodeid (keys %nodes) {
      my ($node) = $nodes{$nodeid};
      if ($node->Nparent eq $pid) { $count++; }
   }

   return $count;
}

sub insert_node {
   my ($nodeid) = $_[0];
   local ($node) = $nodes{$nodeid};
   local ($parent) = $node->Nparent;

   print "(insert node called with node ($nodeid) parent ($parent))\n";
   if ($parent ne "2205999") { $parent = $parent+2205999; }

   if (defined($inserted{$nodeid})) { return; }
   if (!defined($inserted{$node->Nparent})) { &insert_node($node->Nparent); }
   local ($title) = $node->Ntitle;
   $title =~ s/\'/\'\'/gi;

   local ($q) =
      "insert into node values (".(2205999+$nodeid).",35,22,'".$title."', 0, ".$parent.
      ",".$node->NIWP.",".$node->Ndepth.",sysdate,sysdate,1,null,sysdate)";
   local ($sth) = $dbh->prepare($q) || die "died: $q\n";
   $sth->execute || die "died: $q\n";
   $sth->finish();
   $inserted{$nodeid} = 1;

   print "Node: ".(2205999+$nodeid)." Title: ".$node->Ntitle." Parent: ".$parent." NIWP: ".$node->NIWP." Depth: ".$node->Ndepth."\n";
}

1;
