#!/usr/bin/perl

use DBI;
$ENV{'ORACLE_HOME'} = '/opt/oracle';
local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9");

local ($sth) = $dbh->prepare("select nodeid from nodedocument where score1 > 10.0 and nodeid > 29999 and nodeid < 40000 and documentid in (select documentid from documentchannel where channelid > 3 and channelid < 9000) having count(*) > 3 group by nodeid");
$sth->execute || die "database error $!\n";

local (%nodehash);

while ($nodeid = $sth->fetchrow_array()) { $nodehash{$nodeid} = 1; }
$sth->finish;

undef $sth; $sth = $dbh->prepare("select nodeid from hierarchy where parentid = -1 and corpusid = 6");
$sth->execute || die "database error $!\n";
while ($nodeid = $sth->fetchrow_array()) { $nodehash{$nodeid} = 1; }
$sth->finish;

local ($i) = 0;
for ($loop = 30000; $loop <= 33737; $loop++) {
    if ($nodehash{$loop} != 1) { 
	print "Resetting node $loop\n";
	$i++;
	undef $sth; 
	$sth = $dbh->prepare("update node set datescanned = trunc(SYSDATE-2500) where nodeid = $loop");
	$sth->execute || die "database error $!\n";
	$sth->finish;
	$sth = $dbh->prepare("update node set dateupdated = trunc(SYSDATE-2500) where nodeid = $loop");
	$sth->execute || die "database error $!\n";
	$sth->finish;
    }
}

print "\n$i nodes reset.\n\n";
$dbh->disconnect;

1;
