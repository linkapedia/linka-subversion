<script language="JavaScript">
<!--
var myWin;
function MM_openBrWindow(theURL,winName,features) { //v2.0
  if (myWin && (!myWin.closed))
    myWin.focus();
  else
  {
    myWin = window.open(theURL,winName,features);
    myWin.moveTo(0,0);
  }
}
//-->
</script>
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
  	<td width="50"><img src="/images/idrac/s.gif" width=50 height=50></td>
    <td width="660" align="right"><img src="/images/idrac/T_advsearch.gif" width="391" height="19"></td>
    <td width="50"><img src="/images/idrac/s.gif" width=50 height=50></td>
  </tr>
  <tr valign="top">
    <td width="50">&nbsp;</td>
    <td width="660" align="left"> 
      <p>This Search menu will allow you to easily retrieve documents within IDRAC. 
        Use either Full Text or Keyword criteria or both in the same search. You 
        can also search full text in the IDRAC title of the documents. If you 
        need more help, consult the <a href="deadlink.html">tips menu</a>.</p>
      <div align="center">
        <table width="580" border="0" cellspacing="0" cellpadding="0">
		<form name="forme" method=get action="/servlet/Main">
		<input type="hidden" name="template" value="Idrac.Search">
		<input type="hidden" name="advanced" value="true">
          <tr> 
              <td valign="top"> 
                <table width="580" border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td>Region(s):</td>
                    <td> 
                      <select name="country" onFocus="status='/gif/mcsupd.gif'">
                        <option value="MY">My Regions
                        <option value="All" SELECTED>All Regions
                        <option value="AR">Argentina 
                        <option value="AT">Austria 
                        <option value="BE">Belgium 
                        <option value="BR">Brazil 
                        <option value="CA">Canada 
                        <option value="CZ">Czech Republic 
                        <option value="DK">Denmark 
                        <option value="EU">European Union 
                        <option value="FI">Finland 
                        <option value="FR">France 
                        <option value="DE">Germany 
                        <option value="GR">Greece 
                        <option value="HU">Hungary 
                        <option value="INT">International 
                        <option value="IR">Ireland 
                        <option value="IT">Italy 
                        <option value="JP">Japan 
                        <option value="NL">Netherlands 
                        <option value="PL">Poland 
                        <option value="PT">Portugal 
                        <option value="RU">Russian Fed. 
                        <option value="SK">Slovakia 
                        <option value="SP">Spain 
                        <option value="SE">Sweden 
                        <option value="CH">Switzerland 
                        <option value="UK">United Kingdom 
                        <option value="US">USA 
				<option value="OTH">Other Regions
                      </select>
                    </td>
                  </tr>
                  <tr> 
                    <td>Full text:</td>
                    <td> 
                      <input type="text" size="50" name="fulltext">
                    </td>
                  </tr>
                  <tr> 
                    <td>IDRAC title:</td>
                    <td> 
                      <input type="text" size="50" name="idractitle">
                    </td>
                  </tr>
                  <tr> 
                    <td>Bibliography:</td>
                    <td> 
                      <input type="text" size="50" name="bibliography">
                    </td>
                  </tr>
                  <tr> 
                    <td>Abstract:</td>
                    <td> 
                      <input type="text" size="50" name="abstract">
                    </td>
                  </tr>
                  <tr> 
                    <td>Keywords:</td>
                    <td> 
                      <input type="text" size="28" name="keywords">
			    <input type="button" name="PermutIndex" value="Keyword Index" onClick="MM_openBrWindow('/servlet/Main?template=Idrac.KeywordBuilder','idracKwsWin','scrollbars=yes,width=630,height=420')">
			  </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>
                      <input type="reset" name="Clear" value="Clear">
                      <input type="submit" name="Search" value="Submit">
                      <img src="/images/idrac/s.gif" width="10" height="22"> </td>
                  </tr>
                </table>
                <p>&nbsp;</p>
                </td>
          </tr>
		</form>
        </table>
      </div>

