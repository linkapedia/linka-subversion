<BLOCKQUOTE>
    <TABLE cellPadding=2 width="78%" border=0>
      <TBODY> 
      <TR> 
        <TD vAlign=top width="50%"> 
          <P><b>System Parameters Help:</b></P>
          <P>This screen allows you to change system parameters. You will need to restart the server before the parameters take affect.</P>
        
<style><!--
.Normal
	{font-size:12.0pt;
	font-family:"Times New Roman";}
-->
</style>
</head>
<table border=1 cellspacing=0 cellpadding=0>
  <thead> 
  <tr> 
    <td width=247 valign=top bgcolor="#FFFFCA" class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>Attribute Name</span></p>
    </td>
    <td width=169 valign=top bgcolor="#FFFFCA" class="Normal"> 
      <p>Description</p>
    </td>
    <td width=174 valign=top bgcolor="#FFFFCA" class="Normal"> 
      <p>Recommended Setting</p>
    </td>
  </tr>
  </thead> 
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>cfg_classification_CommaDelimCorporaToUse:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>This is a comma delimited list of taxonomies to use in the server.� This 
        should be �1 to use all taxonomies.� You should only change this if you 
        are testing with a reduced set of taxonomies.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>-1</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>NumBytesOfWebTopicsToTake:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>Limit on the amount of text to use for a web based document. Default 
        is 21000 or 21K.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>21000</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>SigWordNotStartWithDefault:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>Used for signature generation, Words that start with the following characters 
        will not be considered for signatures</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>'_-0123456789]</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>SigGenRangeSize:</span></p>
    </td>
    <td width=169 valign=top class="Normal">&nbsp; </td>
    <td width=174 valign=top class="Normal"> 
      <p>250</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>StemOnClassifyNodeDocScoring:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>Set to true if you want to use word stemming for matching words and concepts 
        during classify.� Recommended setting is false.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>false</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>cfg_classification_UseThesaurusInScoring:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>If you are using a thesaurus, set to true, otherwise, set to false.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>true</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapPort</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>The port id of the LDAP server you are using for authentication on the 
        server.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>389</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapPassword</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>The password for the LDAP administrator.� This is used by the taxonomy 
        server for changes to the LDAP database</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>ClientID:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>This is the ID of your database instance.� This is assigned by Indraweb</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>IncludeNumbersFromDoc:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>Set to true if you want numbers in a document to contribute to scoring.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>false</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>TaxServer:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>The HTTP address of the Central Indraweb server for knowledge harvesting 
        results.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>http://66.134.131.36</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>DC:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>The context of the LDAP instance you are using for authentication.� </p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>In the Indraweb domain, this is dc=indraweb,dc=com</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>FULLTEXT</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>Location of full text indexing application.</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapHost:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>The IP address of the LDAP instance you are using for security in the 
        system</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapUsername:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>The username of the LDAP user the ITS server is logging in as.� This 
        needs to be a user with the ability to change attributes in the LDAP domain.</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>DocumentParseDelimiters:</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>A list of characters that the system will act as delimiters between two 
        words.� Note that �-� is not in this list. </p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>?!/, &amp;+.:;#()*</p>
    </td>
  </tr>
  <tr> 
    <td width=247 valign=top class="Normal"> 
      <p><span style='font-size:8.0pt;font-family:Arial'>AdminGroup</span></p>
    </td>
    <td width=169 valign=top class="Normal"> 
      <p>The context of the Admin group in the LDAP domain.� Members of this group 
        will have admin privledges inside of the taxonomy server.</p>
    </td>
    <td width=174 valign=top class="Normal"> 
      <p>cn=Administrators</p>
    </td>
  </tr>
</table>
          <p></p>
          <p></p>
          <p></p>
          <p>&nbsp;</p>
          </TD>
      </TR>
      <TR>
        <TD vAlign=top width="50%">&nbsp;</TD>
      </TR>
      </TBODY>
    </TABLE>
  </BLOCKQUOTE>
