<body onLoad="totdinit()" BGCOLOR="FFFFFF" background="/gif/blshade.gif">
<script Language="JavaScript" Src="totd.js">
</script>
<table width="760" border="0">
  <tr> 
    <td>
<p align="right"><img src="/images/idrac/bannhelp.gif" width="456" height="40"> </p>
<p align="left">&nbsp;</p>
<table border="0">
  <tr>
    <td align="left" width="300"><BLOCKQUOTE>
    <P><A href="http://66.134.131.35/servlet/helpbis.htm">Contents</A><span class="nav">&nbsp;&nbsp;&nbsp;&nbsp; 
	<A href="http://66.134.131.35/servlet/glossar.htm"> Index</A></P>
            </BLOCKQUOTE></td>
      </tr>
</table>
<blockquote> 
  <p align="left" width="160"><font color="#000080" class=bodycopy face="Arial">IDRAC, the world's 
    most comprehensive regulatory affairs database, offers a wealth of information 
    both on CD-ROM and Online. IDRAC contains everything you need to know about 
    legal and administrative affairs and quality, safety and efficacy regulations 
    for Europe, the US, CEE countries, Japan, Canada, Brazil, Argentina and Mexico, 
    Australia and the South East Asia Mini-Modules covering Hong Kong, Malaysia 
    and Singapore. The database also provides clear step-by-step instructions 
    to help you navigate the drug application process via the Table of Contents. 
    The Help menu will provide you with two options <em>Contents</em> and <em>Index 
    </em>where you can obtain information regarding searching, troubleshooting, 
    the structure of the database, and technical information. </font></p>
</blockquote>
<p><br>
</p>
    </td>
  </tr>
</table>
<DIV align=center>


</body>