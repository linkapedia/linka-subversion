<BLOCKQUOTE>
    <TABLE cellPadding=2 width="78%" border=0>
      <TBODY> 
      <TR> 
        <TD vAlign=top width="50%"> 
          <P><B>Groups Help:</B></P>
          <P>This screen allows you to manipulate the groups that are in the system. 
            Security in the system is based on the groups concept. Each user is 
            in 1 or more groups. Each corpus grants different security levels 
            based on groups.<B>:</B></P>
          <P>The first column has a list of groups in the system. For each group 
            you can change the name, edit the users in the group, or delete it. 
            To confirm changes you need to save the group. The last group on this 
            list always reads (New Group). By changing this name and saving it, 
            you will create a new group in the system.</P>
          <p>To change the users in a group, select edit group. This takes you 
            to a screen that lists the current users in the group and a list of 
            the remaining active users in the system. To add a person to the group, 
            select them in the &quot;Current Users&quot; section and hit the arrow 
            button. To remove a person from a group, select them in the User in 
            Group list and hit the arrow button under the list. Hit save to confirm 
            these changes.</p>
          <p>&nbsp;</p>
        </TD>
      </TR>
      <TR>
        <TD vAlign=top width="50%">&nbsp;</TD>
      </TR>
      </TBODY>
    </TABLE>
  </BLOCKQUOTE>
