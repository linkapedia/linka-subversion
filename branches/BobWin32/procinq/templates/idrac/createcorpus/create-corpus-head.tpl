<blockquote>
<form method=post action="/servlet/Main">
<input type="hidden" name="CorpusID"  value="##CorpusID##">
<input type="hidden" name="template" value="##formpost##">

<table>
<tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Taxonomy Name:</b></font></td>
<td colspan=5><input type="text" name="CorpusName" size=50 maxlength=255 value="##CORPUS_NAME##"></td></tr>
<tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Taxonomy Description:</b></font></td>
<td colspan=5><textarea name="CorpusDesc" cols="50" rows="3">##CORPUSDESC##</textarea></td></tr>
<tr><td colspan=6><hr></td></tr>
<tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>ROC Frequency (1) </b></font></td>
<td><input type="text" name="ROCF1" size=5 maxlength=5 value="##ROCF1##"></td>
<td><font face="Arial, Helvetica, sans-serif" size=-1><b>ROC Frequency (2) </b></font></td>
<td><input type="text" name="ROCF2" size=5 maxlength=5 value="##ROCF2##"></td>
<td><font face="Arial, Helvetica, sans-serif" size=-1><b>ROC Frequency (3) </b></font></td>
<td><input type="text" name="ROCF3" size=5 maxlength=5 value="##ROCF3##"></td></tr>
<tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>ROC Coverage (1) </b></font></td>
<td><input type="text" name="ROCC1" size=5 maxlength=5 value="##ROCC1##"></td>
<td><font face="Arial, Helvetica, sans-serif" size=-1><b>ROC Coverage (2) </b></font></td>
<td><input type="text" name="ROCC2" size=5 maxlength=5 value="##ROCC2##"></td>
<td><font face="Arial, Helvetica, sans-serif" size=-1><b>ROC Coverage (3) </b></font></td>
<td><input type="text" name="ROCC3" size=5 maxlength=5 value="##ROCC3##"></td></tr>
<tr><td colspan=6><hr></td></tr>
<tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Taxonomy Available:</b></font></td>
<td colspan=5><input type="checkbox" name="Active" ##ACTIVE##></td></tr>
<tr><td colspan=6><font face="Arial, Helvetica, sans-serif" size=-1><b><BR>Folder Selection:</b></font>
</td></tr>
</table>
    <table width="70%" border="0">
      <tr> 
        <td width="23%" height="24">&nbsp;</td>
        <td colspan="2" bgcolor="#336666" height="24">
          <div align="left"><font face="Arial, Helvetica, sans-serif" color="#FFFFFF"><b>&nbsp; Available 
            Folders for this Taxonomy </b></font></div>
        </td>
