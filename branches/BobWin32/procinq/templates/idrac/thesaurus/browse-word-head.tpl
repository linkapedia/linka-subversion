<html>
<head>
<title> ##TITLE## </title>
<link rel="STYLESHEET" type="text/css" href="/images/espsstyle.css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#dddddd">
<table cellpadding="0" cellspacing="0" border="0" width="150">
<tr>
<td valign="middle" align="center" width="115"><img src="/images/itslogo3.gif" alt="Indraweb Logo" border="1"></td>
<td width="150">

<table cellpadding="0" cellspacing="0" border="0" width="440">
<tr>
<td bgcolor="#003264" align="right" width="150" height=62>

<table cellspacing="0" cellpadding="0" border="0">
<tr><td><img src="/images/spacer_blue.gif" width=1 height=44 alt="" border="0"></td>
<td nowrap class="whitecopy">&nbsp;&nbsp;</td>
<td> &nbsp; 
</td>
<td> &nbsp; </td>
</tr>
</table>

</td>
<td bgcolor="#6a82a3" align="center" width="55">
<!-- img src="/images/button_findsol.gif" width=50 height=44 alt="" border="0" -->
</td>
</tr>
</table>
</td>
</tr>
<tr><td colspan="2" bgcolor="#000000"><img src="/images/spacer_black.gif" width=150 height=1 alt="" border="0"></td></tr>
</table>

<SCRIPT>
function Popup(WordID, ThesaurusID) {
  var ww = 575, wh = 385;
  var wPos = 0;
  var lPos = 0;

  var url = "/servlet/Main?template=Taxonomy.GetWordProps&WordID="+WordID+"&ThesaurusID="+ThesaurusID;

  if(screen) {
  wPos = (screen.width - ww)/2 + WordID/100;
  lPos = (screen.height - wh)/2;
  SEwin = window.open(url, "win"+WordID, 'toolbar=0,directories=0,status=0,scrollbars=1,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  SEwin.focus();
   }
}

function Remove(ThesaurusID, AnchorID, ObjectID, Rel) {
     if (confirm("You are about to delete the selected relationship, continue?")) {
         window.location = "/servlet/Main?template=Taxonomy.GetWordProps&Action=Delete&ThesaurusID="+ThesaurusID+"&WordID="+AnchorID+"&ObjectID="+ObjectID+"&Rel="+Rel;
    }
}
</SCRIPT>
<blockquote>
<form method=post action="/servlet/Main">
<b>Anchor Term:</b> <input type="text" name="AnchorWord" value="##AnchorName##" size=40>
<P>
<input type="hidden" name="template" value="Taxonomy.GetWordProps">
<input type="hidden" name="ThesaurusID" value="##THESAURUSID##">
<input type="hidden" name="WordID" value="##WORDID##">
<input type="hidden" name="WordName" value="##ANCHORNAME##">
<table width=65% valign=middle>
##HEADER##