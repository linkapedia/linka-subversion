<blockquote>
<table width=80%><tr><td>
<b>Instructions</b> Each thesaurus may be used by one or many corpora.  You may alter the relationships to the thesaurus that you selected on the previous screen.   After you have finished making your changes, please click the <i>Submit Changes</i> button.
</td></tr></table>
<P>
<form method=get action="/servlet/Main">
<input type="hidden" name="template" value="Taxonomy.EditThesaurusCorpus">
<table width=75%>
