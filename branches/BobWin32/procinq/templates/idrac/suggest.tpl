<blockquote>
<table width=700><tr><td>
<b><u>Instructions</u></b>: This feature will suggest areas of expertise for a given individual user.   Please select the user below along with up to 3 sample documents that would best characterize the user's knowledge area.  
<P>
<form method="POST" ENCTYPE="multipart/form-data" action="/servlet/Main">
<input type="hidden" name="template" value="People.Suggest">
<input type="hidden" name="users" value="##SELECTUSERS##">
<table width=50%>
<tr><td colspan=2><b><font face="Arial">Document 1</font></b>: &nbsp;  &nbsp; &nbsp; <input type="file" class="Wf" name="doc1" id="attFbutton"> </td></tr>
<tr><td colspan=2><b><font face="Arial">Document 2</font></b>: &nbsp;  &nbsp; &nbsp; <input type="file" class="Wf" name="doc2" id="attFbutton"> </td></tr>
<tr><td colspan=2><b><font face="Arial">Document 3</font></b>: &nbsp;  &nbsp;  &nbsp; <input type="file" class="Wf" name="doc3" id="attFbutton"> </td></tr>
<tr><td colspan=2> &nbsp; <BR><input type="submit" name="submit" value="Suggest"> </td></tr>
</table>
</form>
</td></tr></table>
</blockquote>
  