<SCRIPT LANGUAGE="JAVASCRIPT"> 

function Remove() {
   var col = "corpus";
   var sl = document.fm[col].selectedIndex;

     if (document.fm[col].options[sl].value!='none') {
     if (confirm("You are about to delete the selected corpus and all of its topics, continue?")) {
     if (confirm("Deleted corpora may never be recoverable, are you sure you want to continue?")) {
         window.location = "/servlet/Main?template=Taxonomy.DeleteCorpus&CorpusID="+document.fm[col].options[sl].value;
}}}}

function Explore() {
   var col = "corpus";
   var sl = document.fm[col].selectedIndex;

     if (document.fm[col].options[sl].value!='none') {
         window.location = "/servlet/Main?template=Taxonomy.AdminBrowse&CorpusID="+document.fm[col].options[sl].value;
    }
}

function Edit() {
   var col = "corpus";
   var sl = document.fm[col].selectedIndex;

     if (document.fm[col].options[sl].value!='none') {
         window.location = "/servlet/Main?template=Taxonomy.EditCorpus&CorpusID="+document.fm[col].options[sl].value;
    }
}

function Security() {
   var col = "corpus";
   var sl = document.fm[col].selectedIndex;

     if (document.fm[col].options[sl].value!='none') {
         window.location = "/servlet/Main?template=Taxonomy.ViewCorpusSecurity&CorpusID="+document.fm[col].options[sl].value;
    }
}

function Add() {
    window.location = "/servlet/Main?template=Taxonomy.CreateCorpus";
}

</SCRIPT>

    <blockquote>
    <u><b>Available Corpora</b></u><p>
<form name="fm">
<table cellpadding=2 width=65% border=0>
  <tbody>
  <tr>
    <td valign=top width="55%">
    <select name="corpus" size=10>
