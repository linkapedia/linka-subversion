  <BLOCKQUOTE>
    <p><B>Administration Options</B></p>
    <table width="76%" border="0">
<!--
      <tr> 
        <td width="18%" height="19" valign="top"><a href="/servlet/Main?template=Taxonomy.AdministerUsers"><b>Users</b></a> </td>
        <td width="73%" height="19" valign="top"> 
          <p>Users allows you to add, edit, delete and manage users of the system<br>
          </p>
        </td>
        <td width="9%" height="19">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.ViewGroup"><b>Access Privledges</b></a></td>
        <td width="73%" valign="top">Groups allows you to manage access permissions in the system.  Groups are made of one or more users. 
You may assign corpus permissions to each group.   These permissions will then flow through to the users within each group.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
-->
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.CorpusAdminister"><b>Corpus</b></a></td>
        <td width="73%" valign="top">Corpus allows you to add, edit, delete and 
          manage corpora in the system. Corpora may either be created manually 
          with the system, or imported from XTM Topic Map Files.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.ViewGenre"><b>Folder</b></a></td>
        <td width="73%" valign="top">Folder allows you to add, edit, delete and 
          manage folders in the system. Folders allow you to organize content 
          into categories making it easier for a user to select content from specific 
          sources. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Taxonomy.ViewThesauri"><b>Thesaurus</b></a></td>
        <td width="73%" valign="top"> Thesaurus allows you to add, edit, delete, and 
          manage thesauri in the system.  Thesauri may also be assigned or reassigned to
          one or several corpora. 
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
<!--      <tr> 
        <td width="18%" valign="top"><b>Channel</b></td>
        <td width="73%" valign="top">Channel allows you to add, edit, delete and 
          manage channels. Channels represent different systems that documents 
          are coming from. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
-->
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Taxonomy.Repository">Repositories</a></b></td>
        <td width="73%" valign="top">Add, remove, and manage system repositories.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Taxonomy.Tools">Tools</a></b></td>
        <td width="73%" valign="top">A collection of utilities for system maintenance, classification
	   of documents, and retrieving related topics.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.SystemParam">System Parameters</a></b></td>
        <td width="73%" valign="top">Allows the Administrator the ability to change 
          various system parameters such as database location and Knowledge Harvester 
          information <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <!-- tr>
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.ManageSessions">Manage Sessions</a></b></td>
        <td width="73%" valign="top">Lets the System Administrator see who is 
          logged on and delete dead sessions from the system<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr -->
    </table>
    <!-- LI> Channel --></BLOCKQUOTE>