
  <BLOCKQUOTE> 
    <table width="70%" border="0">
      <tr>
        <td><b>Manage Sessions </b>-- Lists all current sessions in the system. 
          You may remove a session that you think is dead. All user sessions automatically 
          are dropped after 1 hour of inactivity.</td>
      </tr>
    </table>
    <br>
    <br>
    <table width="56%" border="0">
      <tr>
        <td width="31%"><b>Session Name</b></td>
        <td width="24%"><b>Logged In For:</b></td>
        <td width="45%" align="right"><b>Remove Session</b></td>
      </tr>
      <tr>
        <td width="31%">admin@indraweb.com</td>
        <td width="24%">2:37</td>
        <td width="45%" align="right"><img src="/images/delete.gif" alt="Delete Session" width="16" height="18">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>
    </table>
    <!-- LI> Channel --></BLOCKQUOTE>