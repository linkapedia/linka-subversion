  <BLOCKQUOTE>
    <table width="650" border="0">
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.ClassifyPost">Classify (post)</a></b></td>
        <td width="73%" valign="top">As classify document (above), except this tool will post a document from the filesystem 
	  into the ITS database.
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
          <br>
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.CreateNodeSigsFromDoc">Create node from a Document</a></b></td> 
        <td width="73%" valign="top">Creates a node and signatures from a sample document.
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><b><a href="javascript:Purge();">Purge Objects</a></b></td>
        <td width="73%" valign="top">Purge objects allows you to permanently delete 
          objects from the system. Some object like Taxonomies are not completely
          deleted by the API, but rather marked for deletion. The Purge Objects 
          function completely removes them from the database to free up the space.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Server.CorpusIngest"><b>Ingest Taxonomy</b></a></td>
        <td width="73%" valign="top">Ingest a taxonomy that exists in corpus normal form.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
	<!--
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.RebuildIndexes">Rebuild Full Text Indexes</a></b></td>
        <td width="73%" valign="top">Use this option periodically to rebuild the full text document indexes.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
	-->
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.Review">Review Changes</a></b></td>
        <td width="73%" valign="top">Review documents that have been human edited with subsequent change requests
	  from the system during batch classification. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
	<!--
      <tr> 
        <td width="18%" valign="top"><b><a href="/servlet/Main?template=Server.Synch&tablespace=DDATA">Synchronize Batch Classification</a></b></td>
        <td width="73%" valign="top">Synchronize the production system to include documents waiting in the
	pending queue.  Documents awaiting synchronization were typically gathered during an off-line batch
	classification step.

	Please note that, during the synchronization process, document metadata
	within the ITS will be marked as READ-ONLY.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
	-->
    </table>
    </BLOCKQUOTE>