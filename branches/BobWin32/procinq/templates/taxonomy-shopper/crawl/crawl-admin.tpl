  <BLOCKQUOTE>
    <table width="76%" border="0">
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Admin?template=Crawl.CrawlStatus"><b>Monitor Harvesters</b></a></td>
        <td width="73%" valign="top">Monitor gives you a window into the current run status of the knowledge
	    harvesting system. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Admin?template=Crawl.MonitorEngines"><b>Monitor Engines</b></a></td>
        <td width="73%" valign="top">Monitor the status of the metasearch engines. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Admin?template=Crawl.MonitorCorpota"><b>Monitor Corpora</b></a></td>
        <td width="73%" valign="top">Monitor the status of the available corpora. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Admin?template=Crawl.ManageHarvesters"><b>Manage Harvesters</b></a></td>
        <td width="73%" valign="top">Manage individual harvesters, including thread counts and run time options. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Admin?template=Crawl.SearchPlugin"><b>Search Plug-in Setup</b></a></td>
        <td width="73%" valign="top">Add, remove or manage web harvesting modules.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Admin?template=Crawl.CorpusPriority"><b>Schedule</b></a></td>
        <td width="73%" valign="top">View and change the priority queue for the knowledge harvesting system.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <!--
      <tr> 
        <td width="18%" valign="top"><a href="http://www.surfablebooks.com/seditor/main.cgi"><b>Assign Sources</b></a></td>
        <td width="73%" valign="top">Choose from over one thousand sources and assign to specific corpora for 
	    knowledge harvesting as appropriate. 
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      -->
    </table>
    </BLOCKQUOTE>