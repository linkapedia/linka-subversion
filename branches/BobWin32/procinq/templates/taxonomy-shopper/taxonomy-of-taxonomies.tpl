<html>
<head>
<title> Available Taxonomies </title>
<link rel="STYLESHEET" type="text/css" href="/servlet/images/espsstyle.css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" background="/servlet/images/auto_bg.gif">
<table cellpadding="0" cellspacing="0" border="0" width="765">
<tr>
<td valign="middle" align="center" width="95"><img src="/servlet/images/itslogo3.gif" alt="Intellisophic Logo" border="0"></td>
<td width="700">

<table cellpadding="0" cellspacing="0" border="0" width="700">
<tr><td height="1"> <img width=1 height=1 src="/servlet/images/blank.gif"> </td></tr>
<tr>
<td bgcolor="#C68B12" align="right" width="600">

<FORM ACTION="/servlet/Main" METHOD="POST">
<table cellspacing="0" cellpadding="0" border="0">
<tr><td><img src="/servlet/images/spacer_tan.gif" width=1 height=44 alt="" border="0"></td>
<td nowrap class="whitecopy">&nbsp;&nbsp;</td>
<td> &nbsp;
</td>
<td> &nbsp; </td>
</tr>
</table>

</td>
<td bgcolor="#333399" align="center" width="150">
<!-- img src="/servlet/images/button_findsol.gif" width=110 height=44 alt="" border="0" -->
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
   <tr>
   <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
   </tr>
   </table>

</td>
</tr>
<tr><td colspan="2" bgcolor="#000000"><img src="/servlet/images/spacer_black.gif" width=765 height=1 alt="" border="0"></td></tr>
</table>

<!-- child head -->
<DIV align=center>
</DIV><BR><FONT face="Arial, Helvetica, sans-serif">
&nbsp;&nbsp;
Taxonomy Server: Main Menu
</FONT>
<DIV align=left>
<TABLE border=0 width=760 cellspacing=0 cellpadding=3>
<TR bgColor=#333399 borderColor=#996600>
<TD align=left height=23><FONT face="Arial, Helvetica, sans-serif" color=white>
<B>&nbsp;&nbsp; Available Taxonomies </td>
<td align=right>
<A class="genrewb" HREF="/servlet/Main?template=Taxonomy.FrontPage">Home</a> |
<A class="genrewb" HREF="/servlet/Main?template=Server.Logout">Logout</a> |
<A class="genrewb" HREF="/servlet/Main?template=Taxonomy.Help">Help</a>
&nbsp; &nbsp;
</B></FONT></TD></TR></TBODY></TABLE>

<TABLE border=0 cellPadding=0 cellSpacing=0 width=700>
  <TR>
    <td height=10 valign=bottom> <B>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <FONT face="Helvetica,Arial" size=-1>Taxonomy Search:</font></B>
<INPUT NAME="template" TYPE=HIDDEN  Value="Taxonomy.FindCorpora">
<INPUT NAME="CorpusID" TYPE=HIDDEN  value="101">
&nbsp; &nbsp; <INPUT maxLength=100 name=Keywords size=40>
      &nbsp; <INPUT name=Search type=submit value=Go>
</td></tr>
</table>
</FORM>
<style>
.menulines{
	border:2.5px solid #F0F0F0;
        FONT-WEIGHT: none; FONT-SIZE: 12pt; FONT-FAMILY: Arial, Helvetica, sans-serif
}

.corpora{
	border:2.5px solid #F0F0F0;
        FONT-WEIGHT: none; FONT-SIZE: 10pt; FONT-FAMILY: Arial, Helvetica, sans-serif
}

.corpora a{
	border:2.5px solid #F0F0F0;
        FONT-WEIGHT: none; FONT-SIZE: 10pt; FONT-FAMILY: Arial, Helvetica, sans-serif
}

.menulines a{
	text-decoration:none;
	color:black;
}
</style>

<script>
function over_effect(e,state){
  if (document.all)
    source4=event.srcElement
  else if (document.getElementById)
    source4=e.target
  
  if (source4.className=="menulines") {
    source4.style.borderStyle=state
    if (e.type == 'mousedown') { setLayer('taxonomies', source4.name); }
    //alert(e.type);
  } else {
    while(source4.tagName!="TABLE"){
      source4=document.getElementById? source4.parentNode : source4.parentElement
      if (source4.className=="menulines")
        source4.style.borderStyle=state
    }
  }
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function setLayer(objName,newText) { //v3.0
  if ((obj=MM_findObj(objName))!=null) with (obj)
    if (navigator.appName=='Netscape') {document.write(unescape(newText)); document.close();}
    else innerHTML = unescape(newText);
}

</script>
<blockquote>
<table border="0" width="650" cellspacing="0" cellpadding="0" style="background-color:#F0F0F0">
<tr><td width="100%" bgcolor="#333399" colspan=2><font face="Arial" size="3" color="white">&nbsp; <b>Subject Area Selection</b></font></td></tr>
<tr><td width="100%" bgcolor="#F0F0F0"> &nbsp; </td></tr>
<tr><td bgcolor="#F0F0F0" valign="top">
   <table border="0" valign="top" width="225" cellspacing="0" cellpadding="0" onMouseover="over_effect(event,'outset')" 
    onMouseout="over_effect(event,'solid')" onMousedown="over_effect(event,'inset')" onMouseup="over_effect(event,'outset')" 
    style="background-color:#F0F0F0">
   <tr><td class="menulines" name="<UL> <LI> <a class='corpora' href='/servlet/Main?template=Taxonomy.DisplayResults&CorpusID=101'>A Taxonomy of Taxonomies</a> <br> &nbsp; </UL>">&nbsp; 
	Taxonomy Listing</td></tr>
   </table>
   </td>
  
   <td bgcolor="#F0F0F0" valign="top">
   <table border="0" width="425" cellspacing="0" cellpadding="0" style="background-color:#F0F0F0" valign="top">
   <tr><td>
	<div id="taxonomies">

	</div>
   </td></tr></table>
   </td>
</tr></table>
</table>
</blockquote>
<p>
<table width=765><tr><td>
<div align="center"><p align=center><font face="Arial, Helvetica, sans-serif">&copy; 2004 Intellisophic, Inc.  All rights reserved<br>
  <a class=footer href="http://www.Intellisophic.com/about_us.php">About Us</a>&nbsp; |&nbsp; <a
class=footer href="http://www.Intellisophic.com/contact_us.php">Contact Us</a>&nbsp;
  |&nbsp; <a class=footer href="/servlet/Main?template=Taxonomy.Help">Help</a></font></p>
</td></tr></table>
</body>
</html>