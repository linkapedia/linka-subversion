<blockquote>
Please choose a help topic that best matches your request.
<P>
<UL>
<LI> <a href="/servlet/Main?template=Taxonomy.Help&helptext=help-home"> Intellisophic Taxonomy Server </a>
<LI> <a href="/servlet/Main?template=Taxonomy.Help&helptext=help-admin"> General Administration </a>
<LI> <a href="/servlet/Main?template=Taxonomy.Help&helptext=help-system"> System Parameters </a>
<LI> <a href="/servlet/Main?template=Taxonomy.Help&helptext=help-corpus"> Taxonomy Browsing</a>
<LI> <a href="/servlet/Main?template=Taxonomy.Help&helptext=help-node"> Topic Browsing</a>
<LI> <a href="/servlet/Main?template=Taxonomy.Help&helptext=help-user"> Manipulating User Preferences</a>
</UL>
</blockquote>