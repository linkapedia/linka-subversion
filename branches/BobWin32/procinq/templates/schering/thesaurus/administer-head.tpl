<SCRIPT LANGUAGE="JAVASCRIPT"> 

function Remove() {
   var col = "thesaurus";
   var sl = document.fm[col].selectedIndex;

     if (document.fm[col].options[sl].value!='none') {
     if (confirm("You are about to delete the selected thesaurus and all of its relationships, continue?")) {
     if (confirm("Deleted thesauri may never be recoverable, are you sure you want to continue?")) {
         window.location = "/servlet/Main?template=Taxonomy.DeleteThesaurus&ThesaurusID="+document.fm[col].options[sl].value;
}}}}

function Explore() {
   var col = "thesaurus";
   var sl = document.fm[col].selectedIndex;

     if (document.fm[col].options[sl].value!='none') {
         window.location = "/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID="+document.fm[col].options[sl].value;
    }
}

function Edit() {
   var col = "thesaurus";
   var sl = document.fm[col].selectedIndex;

     if (document.fm[col].options[sl].value!='none') {
         window.location = "/servlet/Main?template=Taxonomy.EditThesaurusCorpus&ThesaurusID="+document.fm[col].options[sl].value;
    }
}

function Add() {
    window.location = "/servlet/Main?template=Taxonomy.CreateThesaurus";
}

</SCRIPT>

<blockquote>
<u><b>Available Thesauri</b></u><p>
<form name="fm">
<table cellpadding=2 width=65% border=0>
  <tbody>
  <tr>
    <td valign=top width="55%">
    <select name="thesaurus" size=10>
