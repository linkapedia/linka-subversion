<SCRIPT>
// Create two lists to send to MoveNodes.   They are nodes underneath parent A, and
//  nodes underneath parent B.   Also send along parent IDs for A and B.
function Save() {
   var acol = "ChildrenA";
   var bcol = "ChildrenB";

   var ChildListA = "";
   var ChildListB = "";

   if (document.fm[acol] != null) {
     var ArrLen = document.fm[acol].options.length;   
     for (i=0; i<ArrLen; i++) {
        if (i != 0) { ChildListA = ChildListA+','; }
        ChildListA = ChildListA + document.fm[acol].options[i].value;
   }}

   if (document.fm[bcol] != null) {
     var ArrLen = document.fm[bcol].options.length;   
     for (i=0; i<ArrLen; i++) {
        if (i != 0) { ChildListB = ChildListB+','; }
        ChildListB = ChildListB + document.fm[bcol].options[i].value;
   }}

   document.fm.ListA.value = ChildListA;
   document.fm.ListB.value = ChildListB;
   document.fm.submit()
}
</SCRIPT>

<table width=700><tr><td align=right>
<input type="button" name="save" value=" Save " onClick="javascript:Save();">
<input type="submit" name="clear" Value=" Clear ">
<input type="hidden" name="ListA" value="">
<input type="hidden" name="ListB" value="">
</td></tr></table>
</form>
</blockquote>
