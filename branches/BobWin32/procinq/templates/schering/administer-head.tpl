<SCRIPT LANGUAGE="JAVASCRIPT">

function ROC() {
   var col = "corpus";
   var sl = document.fm[col].selectedIndex;

   if (sl != -1 && document.fm[col].options[sl].value!='none') {
         window.location = "/servlet/Main?template=Taxonomy.EditROCSettings&CID="+document.fm[col].options[sl].value;
   }
}
</SCRIPT>
    <blockquote>
<form name="fm">
<table cellpadding=2 width=65% border=0>
  <tbody>
  <tr><td colspan=2>
    This tool allows you to manage Receiver Operating Characteristics (ROC) for any of the taxonomies that exist
    within the system.  To proceed, please select the taxonomy to manage and click "<b>CONTINUE</b>".
    <p>
    <u><b>Available Taxonomies</b></u><p>
  </td></tr>
  <tr>
    <td valign=top>
    <select name="corpus" size=12>
