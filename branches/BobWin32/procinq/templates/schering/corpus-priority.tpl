<SCRIPT LANGUAGE="JAVASCRIPT">
function Remove() {
   var col = "Current";
   var col2 = "Others";
   var sl = document.fm[col].selectedIndex;

   if (sl == -1) { return; }

     if (document.fm[col].options[sl].value!='none') {
     if (confirm("You are about to remove the selected taxonomy and all topics from the run queue, Continue?")) {
     if (confirm("Removed topics will be re-run if added to the queue again at a later date, are you sure you want to continue?")) {
	   var d_sl = document.fm[col2].length;

         for (i=0; i < document.fm[col].options.length; i++) {
            if (document.fm[col].options[i].selected) {
               oText = document.fm[col].options[i].text;
         	   oValue = document.fm[col].options[i].value;
         	   document.fm[col].options[i] = null;
         	   document.fm[col2].options[d_sl] = new Option (oText, oValue, false, true);
	 	   d_sl = d_sl + 1; i = i - 1;
            }
         }
     }
}}}

function Add() {
   var col = "Current";
   var col2 = "Others";
   var sl = document.fm[col2].selectedIndex;

   if (sl == -1) { return; }

   var d_sl = document.fm[col].length;

   for (i=0; i < document.fm[col2].options.length; i++) {
      if (document.fm[col2].options[i].selected) {
         oText = document.fm[col2].options[i].text;
   	   oValue = document.fm[col2].options[i].value;
    	   document.fm[col2].options[i] = null;
    	   document.fm[col].options[d_sl] = new Option (oText, oValue, false, true);
	   d_sl = d_sl + 1; i = i - 1;
      }
   }
}

function Up(col) {
   var col = "Current";
   var sl = document.fm[col].selectedIndex;

   if (sl == -1) { return; }

     if (document.fm[col].options[sl].value!='none') {
          if (sl > 0) {
		oText = document.fm[col].options[sl-1].text;
      	oValue = document.fm[col].options[sl-1].value;
      	document.fm[col].options[sl-1].text = document.fm[col].options[sl].text;
      	document.fm[col].options[sl-1].value = document.fm[col].options[sl].value;
		document.fm[col].options[sl].text = oText;
		document.fm[col].options[sl].value = oValue;
		document.fm[col].selectedIndex = sl-1;
          }
     }
}

function Down() {
   var col = "Current";
   var sl = document.fm[col].selectedIndex;

   if (sl == -1) { return; }

     if (document.fm[col].options[sl].value!='none') {
          if (sl+1 < document.fm[col].options.length) {
	oText = document.fm[col].options[sl+1].text;
      	oValue = document.fm[col].options[sl+1].value;
      	document.fm[col].options[sl+1].text = document.fm[col].options[sl].text;
      	document.fm[col].options[sl+1].value = document.fm[col].options[sl].value;
	document.fm[col].options[sl].text = oText;
	document.fm[col].options[sl].value = oValue;
	document.fm[col].selectedIndex = sl+1;
          }
    }
}

function makeList(col)
{
  var val = "";
  for (j=0; j<document.fm[col].length; j++) {
    if (val > "") { val += ","; }
    if (document.fm[col].options[j].value > "") val += document.fm[col].options[j].value+":"+(1+j);
  }
  return val;
}

function sub_layout() {
  var rval = makeList("Current");
  window.location = '/servlet/Main?template=Crawl.CorpusPriority&current_list='+rval;
}
</script>

<blockquote>
<form name="fm" method=post action="/servlet/Main">
<input type="hidden" name="template" value="Crawl.CorpusPriority">
<table>
<tr><td> &nbsp; </td>
<td>
   <table width=700 height=100><tr><td>
   <font face="Arial, Helvetica, sans-serif" size=-1>
   <b>Taxonomy Priorities: &nbsp; &nbsp;</b></font></td><td>
   <SELECT name="Current" size=5>
   ##EXISTING##
   </SELECT>
   </td><td valign=center>
      <table><tr>
	<td><a href="javascript:Up()"><img src="/servlet/images/up.gif" alt="Move taxonomy up in order" border=0></a></td>
	<td>&nbsp; The UP arrow increases run priority.</td></tr>
	<tr><td><a href="javascript:Down()"><img src="/servlet/images/down.gif" alt="Move taxonomy down in order" border=0></a></td>
	<td>&nbsp; The DOWN arrow decreases run priority.</td></tr>
	<tr><td><a href='javascript:Remove()'><img src="/servlet/images/delete.gif" alt="Delete topic" border=0></a></td>
	<td>&nbsp; Use the DELETE icon to remove run priority.</td></tr></table>
   </td></tr></table>
</td></tr>
<tr><td> &nbsp; </td>
<td>
   <table width=700 height=100><tr><td>
   <font face="Arial, Helvetica, sans-serif" size=-1>
   <b>Other Taxonomies: &nbsp; &nbsp;</b></font></td><td>
   <SELECT name="Others" size=5>
   ##OTHERS##
   </SELECT>
   </td><td valign=center>
      <table><tr>
	<td><a href="javascript:Add()"><img src="/servlet/images/up.gif" alt="Move taxonomy up into priority list" border=0></a></td>
	<td>&nbsp; Use the UP arrow to add to run priority.</td></tr></table>
   </td></tr></table>
</td></tr></table>

<table width=700><tr><td align=right>
<input type="button" value="Save Changes" name="submit" onClick="javascript:sub_layout();">
</td></tr></table>

</form>
<P>
</blockquote>