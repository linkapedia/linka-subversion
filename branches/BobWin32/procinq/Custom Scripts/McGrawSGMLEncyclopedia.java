import com.iw.system.*;

import java.util.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.xml.sax.InputSource;

public class McGrawSGMLEncyclopedia {

    public static Hashtable htNodes = new Hashtable();
    public static Corpus c = null;
    public static ProcinQ server = null;

    public static void main(String args[]) throws Exception {

        // Step 1. Initialize the server object
        server = new ProcinQ();
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("its", "its");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        //
        c = server.addCorpus("Encyclopedia of Science and Technology (x3)", "Encyclopedia of Science and Technology (x3)");
        Node root = server.getCorpusRoot(c.getID());

        // now loop through each item in the directory
        String dirpath = "C:/Documents and Settings/indraweb/Desktop/TAX-DEV/Encyclopedia of Science and Technology/";
        File dir = new File(dirpath);
        String[] children = dir.list();  int loop = 0;
        for (int k=0; k < children.length; k++) {
            loop++;
            if (loop > 250) {
                System.out.println("Hard exit after 250 topics.");
                return;
            }
            File f = new File(dirpath+children[k]);
            if (f.getAbsolutePath().endsWith(".xml")) {
                FileInputStream fis = new FileInputStream(f);
                SAXReader xmlReader = new SAXReader(false);
                xmlReader.setValidation(false);

                InputSource ins = new InputSource(fis);
                ins.setEncoding("UTF-8");
                ins.setSystemId(f.toURL().toString());

                org.dom4j.Document doc = xmlReader.read(ins);

                Element eArticle = doc.getRootElement();
                Element eTerm = (Element) eArticle.element("ATL");
                Element eText = (Element) eArticle.element("ATEXT");

                Vector musthaveterms = getParentTopics(eArticle);

                Node n = addNodeRecur(eText, server, eTerm.getText(), root, 1);
                for (int i = 0; i < musthaveterms.size(); i++) {
                    server.addMustHaveTerm(n, (String) musthaveterms.elementAt(i));
                }
            }
        }
    }

    public static Vector getParentTopics (Element e) {
        Vector v = new Vector();

        List l = e.elements();
        Iterator i = l.iterator();

        while (i.hasNext()) {
            Element e2 = (Element) i.next();

            if (e2.getName().toLowerCase().equals("xref")) {
                Node n = null;
                String title = e2.getText().trim();
                title = title.replaceAll("\n", " ");
                v.add(title);
            } else {
                Vector v2 = getParentTopics(e2);
                if (v2.size() > 0) {
                    for (int j = 0; j < v2.size(); j++) {
                        v.add(v2.elementAt(j));
                    }
                }
            }
        }

        return v;
    }

    public static Node addNodeRecur (Element e, ProcinQ server, String title, Node parent, int depth) {
        List psource = e.elements("P");
        Iterator l = psource.iterator();

        if (title == null) {
            Element eTitle = (Element) e.element("ST");
            title = eTitle.getText();
        } title = title.replaceAll("\n", " ");

        String source = "";
        while (l.hasNext()) {
            Element eSource = (Element) l.next();
            source = source + " " + eSource.getText();
        }
        //System.out.println("Title: "+title.trim()+" .. "+e.getName());
        //System.out.println("Source: "+source.trim()+"\n");

        Node n = addNode(title.trim(), parent.get("NODEID"), depth, null);
        try { server.setNodeSource(n, source.trim()); }
        catch (Exception ex) { ex.printStackTrace(System.err); }

        depth++;

        List cats = e.elements("SEC2");
        Iterator j = cats.iterator();

        while (j.hasNext()) {
            Element sec = (Element) j.next();
            addNodeRecur(sec, server, null, n, depth);
        }

        cats = e.elements("SEC3");
        j = cats.iterator();

        while (j.hasNext()) {
            Element sec = (Element) j.next();
            addNodeRecur(sec, server, null, n, depth);
        }

        return n;
    }

    public static Node addNode(String sNodeTitle, String sParent, int Depth, String link) {
        if (sNodeTitle.trim().equals(""))
            System.out.println("Blank Title");

        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        if (link != null) n.set("LINKNODEID", link);

        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            if (link == null) n = server.addNode(n); else n = server.addLinkNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;
    }
}