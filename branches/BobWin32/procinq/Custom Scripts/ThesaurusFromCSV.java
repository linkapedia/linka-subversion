// Thesauruss
//
// This file contains a series of Thesauruss that may be used to access the ProcinQ Server API.

import com.iw.system.*;

import java.util.*;
import java.io.*;

public class ThesaurusFromCSV {

    public static com.iw.system.Thesaurus t = null;
    public static Hashtable htArguments = new Hashtable();

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);
        System.out.println("Import ISO Thesaurus..\n");

        if (!htArguments.containsKey("thesaurus")) { System.err.println("Required: -thesaurus <thesaurusname>"); return; }
        if (!htArguments.containsKey("filename")) { System.err.println("Required: -filename <filename>"); return; }

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("its", "its");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        System.out.println("**** Thesaurus ingestion ****");
        File f = new File((String)htArguments.get("filename"));
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;

        t = server.createThesaurus((String) htArguments.get("thesaurus"));
        int loop = 0;

        // "term_id","term","used_from_id","used_from_term"
        // 16,"Science history",126182,"History of science"
        try {
            while ((record = dis.readLine()) != null) {
                loop++;
                if (loop > 1) {
                    record = record.replaceAll("\"", "");
                    String[] columns = record.split(",");
                    addRelationship(server, "UF", columns[1], columns[3]);
                }
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file " + f.getAbsolutePath());
            return;
        }
    }

    private static void addRelationship(ProcinQ server, String relat, String base, String predicate) {
        System.out.println("Relationship: " + relat + " Word: " + base + " Predicate: "+predicate);

        String relationship = "0";
        if (relat.equals("UF")) { relationship = "1"; }
        else if (relat.equals("BT")) { relationship = "2"; }
        else if (relat.equals("NT")) { relationship = "3"; }

        try { server.addRelationship(t.getID(), base, predicate, relationship); }
        catch (Exception e) { e.printStackTrace(System.err); }
    }

    public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}