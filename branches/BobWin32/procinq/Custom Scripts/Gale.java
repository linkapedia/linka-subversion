// Gale
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class Gale {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    private static String lastElement = "";

    private static String[] sTitles = new String[10];
    private static StringBuffer[] sbData = new StringBuffer[10];
    private static Node[] nodes = new Node[10];
    public static int level = -1;

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        System.out.println("Spidering article directory ...");

        File fDirectory = new File((String) htArguments.get("file"));
        if (!fDirectory.exists()) {
            System.err.println("File: " + (String) htArguments.get("file") + " does not exist.");
            return;
        }

        String sCorpusName = (String) htArguments.get("corpus");
        c = server.addCorpus(sCorpusName, sCorpusName);
        Node root = server.getCorpusRoot(c.getID());

        sTitles[0] = root.get("NODETITLE");
        nodes[0] = root;

        String FilePaths[] = fDirectory.list();
        System.out.println("Selecting all articles.. " + FilePaths.length + " total articles found.");

        if (FilePaths != null) {
            for (int i = 0; i < FilePaths.length; i++) {
                File f = new File((String) htArguments.get("file") + "/" + FilePaths[i]);
                if (f.getName().startsWith("eai")) {
                    System.out.println(" ");
                    System.out.println("File: "+f.getAbsolutePath());

                    FileInputStream fis = new FileInputStream(f);
                    //String sLast = root.get("NODEID");

                    SAXReader xmlReader = new SAXReader(false);

                    InputSource ins = new InputSource(fis);
                    ins.setEncoding("UTF-8");
                    ins.setSystemId(f.toURL().toString());

                    org.dom4j.Document doc = xmlReader.read(ins);

                    Element eArticle = doc.getRootElement();
                    if (eArticle == null) {
                        System.err.println("No article tag for file: " + f.getAbsolutePath());
                        return;
                    }
                    lastElement = "";

                    Element eHead = eArticle.element("head");
                    Element eBody = eArticle.element("body");
                    if (eHead != null) {
                        Element eTitle = eHead.element("title");

                        System.out.println("File Title: "+eTitle.getText().trim());

                        Iterator it = eBody.elements("div").iterator();
                        while (it.hasNext()) {
                            // recurse through the DIV tags
                            Element eDiv = (Element) it.next();

                            if ((eDiv.attribute("type") != null) && (((Attribute) eDiv.attribute("type")).getData().equals("article")))
                                recurseElement (eDiv);
                        }
                    }
                    fis.close();
                }
            }
        }
    }

    public static void recurseElement(Element e) {
        List list = e.elements();
        if (list.size() == 0) return;

        Iterator it2 = list.iterator();

        while (it2.hasNext()) {
            Element e2 = (Element) it2.next();

            if ((e2.getQualifiedName().startsWith("h")) && (!e2.getQualifiedName().equals("hr"))) {
                if (level >= 0) {
                    // write node source from previous section
                    try { if (sbData[level] != null) server.setNodeSource(nodes[level], sbData[level].toString()); }
                    catch (Exception ex) { ex.printStackTrace(System.out); }
                }
                level = Integer.parseInt(e2.getQualifiedName().substring(1,2));
                for (int i = level+1; i < 10; i++) { nodes[i] = null; }

                sbData[level] = null;

                String sTitle = getSource(e2).trim();
                sTitles[level] = printCapitalized(sTitle);

                for (int i = 0; i <= level; i++) {
                    if (sTitles[i] != null) {
                        if (i > 0) System.out.print(" -> ");
                        else System.out.print("Title: ");
                        System.out.print(sTitles[i]);
                    }
                }
                System.out.println(" ");
                lastElement = e2.getQualifiedName();

                Node parent = null;
                int i = 1;

                while (parent == null) { parent = nodes[level-i]; i++; }

                nodes[level] = addNode(sTitles[level], parent.get("NODEID"), level);

            } else if ((e2.getQualifiedName().equals("ul")) && (level >= 0)) {
                Iterator it3 = e2.elements().iterator();

                while (it3.hasNext()) {
                    Element e3 = (Element) it3.next();
                    if (e3.getQualifiedName().equals("li")) {
                        if (sbData[level] == null) sbData[level] = new StringBuffer();

                        String sData = getSource(e3).trim();
                        if (!(sData.equals("")) && !(sData.equals("\n"))) sbData[level].append(sData.trim());
                    }
                }

            }

            else if ((e2.getQualifiedName().equals("p")) && (level >= 0)) {

                // if the parent of this P is an H2, and it contains a B child, add this child
                // as another topic.
                if ((e2.elements().size() > 0) && lastElement.equals("h2")) {
                    Iterator it3 = e2.elements().iterator();

                    while (it3.hasNext()) {
                        Element e3 = (Element) it3.next();
                        if (e3.getQualifiedName().equals("b")) {

                            // write node source from previous section
                            try { if (sbData[level] != null) server.setNodeSource(nodes[level], sbData[level].toString()); }
                            catch (Exception ex) { ex.printStackTrace(System.out); }

                            level = 3;
                            for (int i = level+1; i < 10; i++) { nodes[i] = null; }

                            sbData[level] = null;

                            String sTitle = getSource(e3).trim();
                            if (sTitle.endsWith(".")) { sTitle = sTitle.substring(0, sTitle.length()-1); }
                            sTitles[level] = printCapitalized(sTitle);

                            for (int i = 0; i <= level; i++) {
                                if (sTitles[i] != null) {
                                    if (i > 0) System.out.print(" -> ");
                                    else System.out.print("Title: ");
                                    System.out.print(sTitles[i]);
                                }
                            }
                            System.out.println(" ");

                            Node parent = null;
                            int i = 1;

                            while (parent == null) { parent = nodes[level-i]; i++; }

                            nodes[level] = addNode(sTitles[level], parent.get("NODEID"), level);
                        }
                    }
                }

                if (sbData[level] == null) sbData[level] = new StringBuffer();

                String sData = getSource(e2).trim();
                if (!(sData.equals("")) && !(sData.equals("\n"))) sbData[level].append(sData.trim());

            } else {
                if ((e2.attribute("type") != null) && (((Attribute) e2.attribute("type")).getData().equals("article")))
                    recurseElement (e2);
            }
        }
    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh) && !(prevCh != (char) 39))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static String getSource (Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) { sb.append(((Text) obj).getText()); }
            else if (obj instanceof Element) {
                if (((Element) obj).attribute("char") != null)
                    sb.append(((Element) obj).attribute("char").getText());
                else if (((Element) obj).getQualifiedName().equals("br")) { sb.append(" "); }
                else sb.append(getSource((Element) obj));
            }
        }

        return sb.toString();
    }

    public static Node addNode (String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth+"");
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try { n = server.addNode(n); }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); e.printStackTrace(System.err); }

        return n;
    }

    public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}