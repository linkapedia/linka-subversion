import com.iw.system.*;

import java.util.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.xml.sax.InputSource;

public class Raston {

    public static Hashtable htArguments = new Hashtable();
    public static Hashtable htNodes = new Hashtable();
    public static Hashtable htFound = new Hashtable();
    public static Corpus c = null;
    public static ProcinQ server = null;

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server = new ProcinQ();
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("its", "its");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        System.out.println("Loading topics into memory...");
        try {
            Vector nodes = server.CQL("select <node> where corpusid = 2237 and depthfromroot = 1", 1, 5000);
            for (int i = 0; i < nodes.size(); i++) {
                Node n = (Node) nodes.elementAt(i);
                //System.out.println("Adding: ###"+n.get("NODETITLE").toLowerCase().trim()+"###");
                htNodes.put(n.get("NODETITLE").toLowerCase().trim(), n);
            }
            System.out.println("Topic load successful, " + nodes.size() + " topics loaded.");
        } catch (Exception e) {
            System.err.println("Topic load failed.");
            return;
        }

        c = server.addCorpus("Ralston2 (MPTEST)", "Ralston2 (MPTEST)");
        Node root = server.getCorpusRoot(c.getID());
        String[] parents = new String[6];

        // now loop through each item in the
        File f = new File("C:/Documents and Settings/indraweb/Desktop/TAX-DEV/ralson/fulltext/topics.txt");
        FileInputStream fis = new FileInputStream(f);
        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);
        Element eTopics = doc.getRootElement();

        List list = eTopics.elements("topic");
        Iterator i = list.iterator();
        int previouslevel = -1;
        NodeInsert ni = null;

        while (i.hasNext()) {
            Element eto = (Element) i.next();
            String name = eto.element("name").getText();
            int level = Integer.parseInt(eto.element("level").getText());
            boolean upper = false;

            if (name.toUpperCase().equals(name)) upper = true;
            else if (level == 2) level++;

            /**
             * - if the new topic is a child of the previous, and the previous was an article,
             *   then create the previous as a shell and the new as a child of the shell (copy)
             * - if the new topic is a higher depth level than the previous, copyNode on the previous
             * - if the new topic is equal or lower depth level then the previous, copy node underneath previous
             */
            if (ni != null) {
                if ((previouslevel < level) && (ni.link != null)) { // copy and move on
                    // this is a middle layer node with a source, create as both a shell and a child
                    if (ni.name.trim().equals(""))
                        System.out.println("No name!");

                    Node pn = addNode(ni.name, ni.parent, ni.depth, null);
                    ni.parent = pn.get("NODEID");
                    parents[previouslevel] = pn.get("NODEID");
                    Node pn2 = copyNode(ni);
                    htFound.put(pn2.get("NODETITLE").toLowerCase(), pn);
                } else {
                    if (ni.name.trim().equals(""))
                        System.out.println("No name!");

                    Node pn = null;
                    if (ni.link != null) pn = copyNode(ni);
                    else pn = addNode(ni);

                    parents[previouslevel] = pn.get("NODEID");
                    htFound.put(pn.get("NODETITLE").toLowerCase(), pn);
                }
            }

            // *********************** LEVEL 1/0 ************************* //
            if (level == 1) {
                ni = new NodeInsert(name, root.get("NODEID"), 1, null, upper);
                previouslevel = level;
            } else if (level == 2) {
                ni = new NodeInsert(name, parents[1], 2, null, upper);
                previouslevel = level;
            } else if (htNodes.containsKey(name.toLowerCase().trim())) {
                Node n = (Node) htNodes.get(name.toLowerCase().trim());
                ni = new NodeInsert(name, parents[level-1], level, n.get("NODEID"), upper);
                previouslevel = level;
            } else { System.out.println("##### NODE NOT FOUND:"+name); ni = null; }

        }

        Node orphans = addNode("ORPHANS", root.get("NODEID"), 1, null);

        Enumeration eH = htNodes.keys();
        while (eH.hasMoreElements()) {
            String s = (String) eH.nextElement();
            if (!htFound.containsKey(s.toLowerCase().trim())) {
                Node n = (Node) htNodes.get(s.toLowerCase().trim());
                addNode(s, orphans.get("NODEID"), 2, n.get("NODEID"));
            }
        }

    }

    public static Node addShellNode (NodeInsert ni) {
        return addNode(ni.name, ni.parent, ni.depth, null);
    }
    public static Node addNode (NodeInsert ni) {
        return addNode(ni.name, ni.parent, ni.depth, ni.link);
    }
    public static Node addNode (String sNodeTitle, String sParent, int Depth, String link) {
        if (sNodeTitle.trim().equals(""))
            System.out.println("Blank Title");

        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth+"");
        if (link != null) n.set("LINKNODEID", link);

        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try { if (link == null) n = server.addNode(n); else n = server.addLinkNode(n); }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); e.printStackTrace(System.err); }

        return n;
    }

    // copy this node recursively
    public static Node copyNode (NodeInsert ni) {
        Node n = null;

        System.out.println("Copy node: "+ni.name+" Parent: "+ni.parent);
        try { n = server.copyNode(ni); }
        catch (Exception e) {
            e.printStackTrace(System.err);
            System.err.println("Could not copy node: "+ni.name);
        }

        return n;

    }
}