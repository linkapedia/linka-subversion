// Gale
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.net.*;

import org.xml.sax.InputSource;
import org.dom4j.io.SAXReader;
import org.dom4j.Element;

public class WikiArticles {
    public static boolean go = false;

    private static ProcinQ server = new ProcinQ();
    private static Vector Nodes = new Vector();

    public static void main(String args[]) throws Exception {

        // Step 1. Initialize the server object
        server.setAPI("192.168.0.223:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        //File f = new File("C:/wiki/file.html");
        //FileInputStream fis = new FileInputStream(f);
        //String sLast = root.get("NODEID");
        System.out.println("Loading all nodes into hash ...");
        Nodes = server.CQL("SELECT <NODE> WHERE CORPUSID = 108 AND NODEID = LINKNODEID");
        for (int i = 0; i < Nodes.size(); i++) {
            Node n = (Node) Nodes.elementAt(i);
            String URL = "http://en.wikipedia.org/wiki/Category:"+n.get("NODETITLE").replaceAll(" ","_");
            System.out.println("Opening .. "+URL);

            HttpURLConnection httpCon = null;
            StringBuffer returnString = new StringBuffer("");
            boolean bPDFDocument = false;
            org.dom4j.Document doc = null;
            boolean read = true;

            try {
                URL myURL = new URL(URL);

                httpCon = (HttpURLConnection) myURL.openConnection();
                httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
                httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
                //httpCon.setRequestMethod("POST");
                httpCon.setDefaultUseCaches(false);

                if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
                }

                InputStream is = httpCon.getInputStream();
                SAXReader xmlReader = new SAXReader(false);

                InputSource ins = new InputSource(is);
                ins.setEncoding("UTF-8");
                ins.setSystemId(URL);

                doc = xmlReader.read(ins);
            } catch (Exception e) {
                read = false;
                System.out.println("** FLAG FOR LATER: "+URL);
            }
            if (read == true) {
                Element eArticle = doc.getRootElement();
                Element eBody = eArticle.element("body");

                go = false;
                recurseElement(eBody, n);
            } read = true;
        }
    }

    public static void recurseElement(Element e, Node n) {
        List list = e.elements();
        if (list.size() == 0) return;

        Iterator it2 = list.iterator();

        while (it2.hasNext()) {
            Element e2 = (Element) it2.next();
            //for (int i = 0; i < level; i++) { System.out.print("  "); }
            /*
                11597672 Nature
                11399920 Mathematics
                11399921 Science
                11399922 Society
                11399923 Technology
                11399916 Culture
                11399917 Geography
                11399918 History
                11399919 Personal Life
                11399915 Wikipedia (Full)
            */
            if (e2.getQualifiedName().equals("ul")) go = true;

            if ((e2.getQualifiedName().equals("a")) && (e2.attribute("title") != null) &&
                    ((e2.attribute("title")).getText().startsWith("Category:"))) {
                if (go) {
                    System.out.println(e2.getText()+" type: "+e2.attribute("title").getText());
                    Node child = null;

                    try { child = addNode(e2.getText(), n.get("NODEID"), Integer.parseInt(n.get("DEPTHFROMROOT"))+1); }
                    catch (Exception ex) { }
                } //else { System.out.println("NO! "+e2.getText()+" type: "+e2.attribute("title").getText()); }
            }

            if (e2.getText().startsWith("Articles in category")) go = false;
            recurseElement(e2, n);
        }

    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh) && !(prevCh != (char) 39))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static Node addNode (String sNodeTitle, String sParent, int Depth) throws Exception {
        return addNode(sNodeTitle, sParent, Depth, null); }
    public static Node addNode (String sNodeTitle, String sParent, int Depth, Node link) throws Exception {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", "108");
        n.set("DEPTHFROMROOT", Depth+"");
        if (link != null) n.set("LINKNODEID", link.get("NODEID"));
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try {
            if (link != null) { n = server.addLinkNode(n); }
            else { n = server.addNode(n); }}
        catch (DataIntegrityViolation dive) {
            System.err.println("Data integrity violation adding "+sNodeTitle+" underneath "+sParent+" .."); throw dive; }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); throw e; }

        return n;

    }
}