// BritArt
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class FixOtherCities {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    private static String[] sTitles = new String[10];
    private static StringBuffer[] sbData = new StringBuffer[10];
    public static int level = -1;

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("192.168.0.223:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("demo", "demo");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        System.out.println("Spidering article directory ...");

        File fDirectory = new File((String) htArguments.get("file"));
        if (!fDirectory.exists()) {
            System.err.println("File: " + (String) htArguments.get("file") + " does not exist.");
            return;
        }

        c = server.getCorpora("903");
        Node root = server.getCorpusRoot(c.getID());

        sTitles[0] = root.get("NODETITLE");

        String FilePaths[] = fDirectory.list();
        System.out.println("Selecting all articles.. " + FilePaths.length + " total articles found.");

        if (FilePaths != null) {
            for (int i = 0; i < FilePaths.length; i++) {
                File f = new File((String) htArguments.get("file") + "/" + FilePaths[i]);
                if (f.getName().startsWith("ciow")) {
                    System.out.println(" ");
                    System.out.println("File: "+f.getAbsolutePath());

                    FileInputStream fis = new FileInputStream(f);
                    //String sLast = root.get("NODEID");

                    SAXReader xmlReader = new SAXReader(false);

                    InputSource ins = new InputSource(fis);
                    ins.setEncoding("UTF-8");
                    ins.setSystemId(f.toURL().toString());

                    org.dom4j.Document doc = xmlReader.read(ins);

                    Element eArticle = doc.getRootElement();
                    if (eArticle == null) {
                        System.err.println("No article tag for file: " + f.getAbsolutePath());
                        return;
                    }

                    Element eHead = eArticle.element("head");
                    Element eBody = eArticle.element("body");
                    Element eTitle = eHead.element("title");

                    //System.out.println("File Title: "+eTitle.getText().trim());

                    Iterator it = eBody.elements("div").iterator();
                    while (it.hasNext()) {
                        // recurse through the DIV tags
                        Element eDiv = (Element) it.next();

                        if ((eDiv.attribute("type") != null) && (((Attribute) eDiv.attribute("type")).getData().equals("article")))
                            recurseElement (eDiv);
                    }

                    fis.close();
                }
            }
        }
    }

    public static void recurseElement(Element e) {
        List list = e.elements();
        if (list.size() == 0) return;

        Iterator it2 = list.iterator();

        Node parent = null; Node n = null;

        while (it2.hasNext()) {
            Element e2 = (Element) it2.next();

            if ((e2.getQualifiedName().startsWith("h")) && (!e2.getQualifiedName().equals("hr"))) {
                level = Integer.parseInt(e2.getQualifiedName().substring(1,2));

                sbData[level] = null;

                String sTitle = e2.getText().trim();
                if (sTitle.equals("")) { sTitle = getSource(e2); }
                sTitles[level] = printCapitalized(sTitle);

                for (int i = 0; i <= level; i++) {
                    if (sTitles[i] != null) {
                        if (i > 0) System.out.print(" -> ");
                        else System.out.print("Title: ");
                        System.out.print(sTitles[i]);
                    }
                } System.out.println(" ");

                if (sTitles[level].equals("LOCAL HOLIDAYS")) {
                    try {
                        Vector v = server.CQL("SELECT <NODE> WHERE CORPUSID = 903 AND NODETITLE LIKE '"+sTitles[level-1].replaceAll("'", "''")+"'");
                        if (v.size() > 0) {
                            parent = (Node) v.elementAt(0);
                            v = server.CQL("SELECT <NODE> WHERE NODETITLE = '"+sTitles[level]+"' AND PARENTID = "+parent.get("NODEID"));
                            n = (Node) v.elementAt(0);
                            System.out.println("Node: "+n.get("NODETITLE")+" ("+n.get("NODEID")+") Parent: "+
                                parent.get("NODETITLE")+" ("+parent.get("NODEID")+")");

                            //try { if (sbData[level] != null) server.setNodeSource((Node) v.elementAt(0), sbData[level].toString()); }
                            //catch (Exception ex) { ex.printStackTrace(System.out); }

                        } else { System.out.println("Found nothing for: SELECT <NODE> WHERE CORPUSID = 903 AND NODETITLE LIKE '"+sTitles[level-1].replaceAll("'", "''")+"'"); }
                    } catch (Exception ex) {
                        System.out.println("ERROR PROCESSING: "+sTitles[level-1]+" -> "+sTitles[level]);
                        n = null; parent = null;
                    }
                } else { parent = null; n = null; }

            } else if ((e2.getQualifiedName().equals("p")) && (level >= 0)) {
                if (n != null) { addNode(getSource(e2), n.get("NODEID"), 4); }
            } else {
                if ((e2.attribute("type") != null) && (((Attribute) e2.attribute("type")).getData().equals("article")))
                    recurseElement (e2);
            }
        }
    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static String getSource (Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) { sb.append(((Text) obj).getText()); }
            else if (obj instanceof Element) {
                if (((Element) obj).attribute("char") != null)
                    sb.append(((Element) obj).attribute("char").getText());
                else sb.append(getSource((Element) obj));
            }
        }

        return sb.toString();
    }

    public static Node addNode (String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth+"");
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try { n = server.addNode(n); }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); e.printStackTrace(System.err); }

        return n;

    }

    public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}