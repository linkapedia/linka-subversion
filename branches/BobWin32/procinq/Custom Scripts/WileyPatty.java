import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class WileyPatty {
    public static Hashtable htArguments = new Hashtable();
    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;
    private static Hashtable ht = null;
    private static Hashtable todo = new Hashtable();

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        c = server.addCorpus("Wiley Patty Toxicology 5/30/2006", "Wiley Patty Toxicology 5/30/2006");
        Node root = server.getCorpusRoot(c.getID());

        // *****************************************************
        // first, create the top level categories
        System.out.println("************************************************************");
        System.out.println("************************************************************");
        System.out.println("WRITING THE TOP LEVEL OF THE TAXONOMY");
        System.out.println("************************************************************");
        System.out.println("************************************************************");

        File f = new File("C:/Documents and Settings/indraweb/Desktop/wiley/cache/toplevel.xml");
        FileInputStream fis = new FileInputStream(f);

        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);

        Element eCats = doc.getRootElement();
        List titles = eCats.elements("title");
        Node parent = null;

        for (int i = 0; i < titles.size(); i++) {
            Element eTitle = (Element) titles.get(i);
            String sTitle = eTitle.attribute("id").getValue().toString();
            parent = addNode(sTitle, sTitle, root.get("NODEID"), 1);
            System.out.println("Creating: "+sTitle+" (level 1)");

            List subtitles = eTitle.elements("subtitle");

            for (int j = 0; j < subtitles.size(); j++) {
                Element eSub = (Element) subtitles.get(j);
                String sSub = eSub.attribute("id").getValue().toString();
                String sUrl = eSub.attribute("url").getValue().toString();
                Node n = addNode(sSub, sSub, parent.get("NODEID"), 2);

                todo.put(sUrl, n);

                System.out.println("Creating: "+sSub+" (level 2)");
            }
        }

        // *****************************************************
        // second, layer in the sub categories
        System.out.println("************************************************************");
        System.out.println("************************************************************");
        System.out.println("WRITING THE INDIVIDUAL SECTIONS");
        System.out.println("************************************************************");
        System.out.println("************************************************************");

        Enumeration eH = todo.keys();
        while (eH.hasMoreElements()) {
            String toxcode = (String) eH.nextElement();
            Node parentNode = (Node) todo.get(toxcode);

            boolean readCache = true; int loop = 0;
            while (readCache) {
                loop++;
                System.out.println("***********************************************************");
                System.out.println("Reading "+toxcode+"-sect"+loop+" from the cache.");

                File f2 = new File("C:/Documents and Settings/indraweb/Desktop/wiley/xml/"+toxcode+"-sect"+loop+".html.xml");
                if (!f2.exists()) { System.out.println(f2.getAbsolutePath()+" does not exist, resetting..."); readCache = false; }
                else {
                    FileInputStream fis2 = new FileInputStream(f2);

                    SAXReader xmlReader2 = new SAXReader(false);

                    InputSource ins2 = new InputSource(fis2);
                    ins2.setEncoding("UTF-8");
                    ins2.setSystemId(f2.toURL().toString());

                    try {
                        org.dom4j.Document doc2 = xmlReader2.read(ins2);

                        Element eTop = doc2.getRootElement();
                        Element eChap = eTop.element("chapter");
                        String chapTitle = eChap.attribute("title").getValue().toString().trim();

                        Node[] nodes = new Node[10];

                        nodes[0] = parentNode;
                        nodes[1] = addNode(chapTitle, chapTitle+" "+toxcode+"-sect"+loop, parentNode.get("NODEID"), 3);
                        server.setNodeSource(nodes[1], eChap.getText());
                        System.out.println("Creating: **chapter** "+chapTitle+" (level 1)");

                        List sections = eTop.elements("section");
                        for (int k = 0; k < sections.size(); k++) {
                            Element eSec = (Element) sections.get(k);
                            String sectTitle = eSec.attribute("title").getValue().toString().trim();
                            String slevel = eSec.attribute("level").getValue().toString().trim();
                            String body = eSec.getText();
                            int level = Integer.parseInt(slevel);

                            if (sectTitle.trim().equals("Synonyms:")) {
                                int z = 1;
                                Node par = nodes[level-z];
                                while ((par == null) && (level-z > 0)) {
                                    z++; par = nodes[level-z];
                                }
                                body = server.getNodeSource(par.get("NODEID"))+" "+body;
                                System.out.println(" .. now length "+body.length());
                                server.setNodeSource(par, body);
                                /*
                                String[] subnodes = body.trim().split(",");
                                for (int m = 0; m < subnodes.length; m++) {
                                    System.out.println("Creating: "+subnodes[m].trim()+" (syonym) (level "+level+")");
                                    addNode(subnodes[m].trim()+" (synonym)", nodes[level-1].get("NODEID"), level);
                                } */
                            } else if (sectTitle.trim().equals("")) {
                                try {
                                    System.out.print("Encountered empty title ("+sectTitle.trim()+")");
                                    int z = 1;
                                    Node par = nodes[level-z];
                                    while ((par == null) && (level-z > 0)) {
                                        z++; par = nodes[level-z];
                                    }
                                    body = server.getNodeSource(par.get("NODEID"))+" "+body;
                                    System.out.println(" .. now length "+body.length());
                                    server.setNodeSource(par, body);
                                } catch (Exception e) { System.err.println("Node source update failed, probably because of level."); }

                            } else {
                                System.out.println("Creating: "+sectTitle+" (level "+level+")");
                                try {
                                    int z = 1;
                                    Node par = nodes[level-z];
                                    while ((par == null) && (level-z > 0)) {
                                        z++; par = nodes[level-z];
                                    }

                                    nodes[level] = addNode(sectTitle, sectTitle+" "+toxcode+"-sect"+loop, par.get("NODEID"), level);
                                    server.setNodeSource(nodes[level], body);
                                } catch (Exception e) { System.err.println("Node create failed, probably because of level."); }
                            }
                        }
                    }  catch (Exception e) {
                        System.err.println("**** Parsing error **** "); readCache = false;
                    }
                }
            }
        }
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        //System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}

/*
        // *****************************************************
        // second, layer in the sub categories
        System.out.println("************************************************************");
        System.out.println("************************************************************");
        System.out.println("WRITING THE INDIVIDUAL SECTIONS");
        System.out.println("************************************************************");
        System.out.println("************************************************************");

        Enumeration eH = todo.keys();
        while (eH.hasMoreElements()) {
            String toxcode = (String) eH.nextElement();
            Node parentNode = (Node) todo.get(toxcode);

            boolean readCache = true; int loop = 1;
            while (readCache) {
                System.out.println("Reading "+toxcode+" from the cache.");

                File f2 = new File("C:/Documents and Settings/indraweb/Desktop/wiley/cache/"+toxcode+"-sect"+loop+".html");
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f2), "UTF8"));
                String line; int recordLevel = 0; boolean pause = false;

                StringBuffer majorSection = new StringBuffer(""); Node majorNode = null;
                StringBuffer minorSection = new StringBuffer(""); Node minorNode = null;
                StringBuffer tier3Section = new StringBuffer(""); Node tier3Node = null;
                StringBuffer tier4Section = new StringBuffer(""); Node tier4Node = null;
                StringBuffer tier5Section = new StringBuffer(""); Node tier5Node = null;

                while ((line = br.readLine()) != null) {
                    // check title
                    Pattern p = Pattern.compile("<TITLE>(.*?)</TITLE>", Pattern.CASE_INSENSITIVE);
                    Matcher m = p.matcher(line); boolean isTitle = m.find();

                    if (isTitle) {
                        System.out.println("Creating: "+m.group(1)+" (level 3) from "+line);
                        majorNode = addNode(m.group(1), parentNode.get("NODEID"), 3);
                    }

                    // we turn off the recorder for table
                    Pattern p3 = Pattern.compile("(.*?)<table ", Pattern.CASE_INSENSITIVE);
                    Matcher m3 = p3.matcher(line); boolean isStartTable = m3.find();

                    if (isStartTable) { pause = true; }

                    Pattern p4 = Pattern.compile("</table>", Pattern.CASE_INSENSITIVE);
                    Matcher m4 = p4.matcher(line); boolean isEndTable = m4.find();

                    if (isEndTable) { pause = false; }

                    // check section
                    Pattern p2 = Pattern.compile("<span class=\"sect1\">(.*?)", Pattern.CASE_INSENSITIVE);
                    Matcher m2 = p2.matcher(line); boolean isSect1 = m2.find();

                    if (isSect1) {
                        majorSection = new StringBuffer(m2.group(1)); recordLevel = 1;
                    }

                    Pattern p5 = Pattern.compile("<div class=\"sect2\">(.*?)</div>", Pattern.CASE_INSENSITIVE);
                    Matcher m5 = p5.matcher(line); boolean isSect2 = m5.find();

                    if (isSect2) {
                        String secTit = m5.group(1);
                        if (recordLevel > 1) {
                            for (int k = recordLevel; k >= 2; k--) {
                                if (k == 2) { server.setNodeSource(minorNode, minorSection.toString().replaceAll("<(.*?)>", " ")); }
                                if (k == 3) { server.setNodeSource(tier3Node, tier3Section.toString().replaceAll("<(.*?)>", " ")); }
                                if (k == 4) { server.setNodeSource(tier4Node, tier4Section.toString().replaceAll("<(.*?)>", " ")); }
                                if (k == 5) { server.setNodeSource(tier5Node, tier5Section.toString().replaceAll("<(.*?)>", " ")); }
                            }
                        }
                        System.out.println("Creating: "+secTit.replaceAll("<(.*?)>", "")+" (level 4) from "+line);
                        minorNode = addNode(secTit.replaceAll("<(.*?)>", ""), majorNode.get("NODEID"), 4); recordLevel = 2;
                    }

                    Pattern p6 = Pattern.compile("<div class=\"sect3\">(.*?)</div>", Pattern.CASE_INSENSITIVE);
                    Matcher m6 = p6.matcher(line); boolean isSect3 = m6.find();

                    if (isSect3) {
                        String secTit = m6.group(1);
                        if (recordLevel > 2) {
                            for (int k = recordLevel; k >= 3; k--) {
                                if (k == 3) { server.setNodeSource(tier3Node, tier3Section.toString().replaceAll("<(.*?)>", " ")); }
                                if (k == 4) { server.setNodeSource(tier4Node, tier4Section.toString().replaceAll("<(.*?)>", " ")); }
                                if (k == 5) { server.setNodeSource(tier5Node, tier5Section.toString().replaceAll("<(.*?)>", " ")); }
                            }
                        }
                        System.out.println("Creating: "+secTit.replaceAll("<(.*?)>", "")+" (level 5) from "+line);
                        tier3Node = addNode(secTit.replaceAll("<(.*?)>", ""), minorNode.get("NODEID"), 5); recordLevel = 3;
                    }

                    Pattern p7 = Pattern.compile("<div class=\"sect4\">(.*?)</div>", Pattern.CASE_INSENSITIVE);
                    Matcher m7 = p7.matcher(line); boolean isSect4 = m7.find();

                    if (isSect4) {
                        String secTit = m7.group(1);
                        if (recordLevel > 3) {
                            for (int k = recordLevel; k >= 4; k--) {
                                if (k == 4) { server.setNodeSource(tier4Node, tier4Section.toString().replaceAll("<(.*?)>", " ")); }
                                if (k == 5) { server.setNodeSource(tier5Node, tier5Section.toString().replaceAll("<(.*?)>", " ")); }
                            }
                        }
                        System.out.println("Creating: "+secTit.replaceAll("<(.*?)>", "")+" (level 6) from "+line);
                        tier4Node = addNode(secTit.replaceAll("<(.*?)>", ""), tier3Node.get("NODEID"), 6); recordLevel = 4;
                    }

                    Pattern p8 = Pattern.compile("<div class=\"sect5\">(.*?)</div>", Pattern.CASE_INSENSITIVE);
                    Matcher m8 = p8.matcher(line); boolean isSect5 = m8.find();

                    if (isSect5) {
                        String secTit = m8.group(1);
                        if (recordLevel > 4) {
                            server.setNodeSource(tier5Node, tier5Section.toString().replaceAll("<(.*?)>", " "));
                        }
                        System.out.println("Creating: "+secTit.replaceAll("<(.*?)>", "")+" (level 7) from "+line);
                        tier5Node = addNode(secTit.replaceAll("<(.*?)>", ""), tier4Node.get("NODEID"), 7); recordLevel = 5;
                    }

                    if ((recordLevel > 0) && (!pause)) {
                        switch (recordLevel) {
                            case 1: majorSection.append(line); break;
                            case 2: minorSection.append(line); break;
                            case 3: tier3Section.append(line); break;
                            case 4: tier4Section.append(line); break;
                            case 5: tier5Section.append(line); break;
                            default: System.err.println("Warning! Section "+recordLevel+" could not be processed.");
                        }
                    }
                }

                if ((majorSection != null) && (majorNode != null))
                    server.setNodeSource(majorNode, majorSection.toString().replaceAll("<(.*?)>", " "));

                br.close();

                loop++; f2 = new File("C:/Documents and Settings/indraweb/Desktop/wiley/cache/"+toxcode+"-sect"+loop+".html");
                if (!f2.exists()) readCache = false;
            }

        }
*/

