import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class Elsevier {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    private static String[] sTitles = new String[10];
    private static StringBuffer[] sbData = new StringBuffer[10];
    private static Node[] nodes = new Node[10];
    public static int level = -1;

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        System.out.println("Reading master XML file ...");

        // "" 1 to 87

        for (int i = 1; i < 88; i++) {
            String s = i+"";
            if (i < 10) { s = "0"+s; }

            File f = new File("C:/Documents and Settings/indraweb/projects/taxonomies/else/braunwalds/body/0479c0"+s+"/0479c0"+s+".sgm");
            System.out.println("File: " + f.getAbsolutePath());

            FileInputStream fis = new FileInputStream(f);

            SAXReader xmlReader = new SAXReader(false);

            InputSource ins = new InputSource(fis);
            ins.setEncoding("UTF-8");
            ins.setSystemId(f.toURL().toString());

            org.dom4j.Document doc = xmlReader.read(ins);
            Element opr = doc.getRootElement();

            String sCorpusName = (String) htArguments.get("corpus");
            fis.close();
        }
    }

    public static void recurseElement(Element e) {
        List list = e.elements("sg");
        if (list.size() == 0) return;

        level++;

        Iterator it2 = list.iterator();

        while (it2.hasNext()) {
            Element eSG = (Element) it2.next();

            if (eSG.element("se1") != null) {
                List list2 = eSG.element("se1").elements();

                Vector vH1 = new Vector(); int min = 0;

                if (list2.size() > 0) {
                    Iterator it3 = list2.iterator();

                    while (it3.hasNext()) {
                        Element eH1 = (Element) it3.next();
                        if (eH1.getQualifiedName().toLowerCase().startsWith("h"))
                            vH1.add(eH1);
                    }
                }

                for (int loop = 0; loop < vH1.size(); loop++) {
                    Element eH = (Element) vH1.elementAt(loop);
                    Element next = null;
                    if ((loop+1) < vH1.size()) { next = (Element) vH1.elementAt(loop+1); }

                    int incl = new Integer(eH.getQualifiedName().substring(1, 2)).intValue() - 1;
                    level = level + incl;

                    sTitles[level] = getSource(eH);

                    for (int i = 0; i <= level; i++) {
                        if (i > 0) System.out.print(" -> ");
                        System.out.print(sTitles[i]);
                    }
                    System.out.println(" ");

                    // add to database
                    nodes[level] = addNode(sTitles[level], nodes[level-1].get("NODEID"), level+1);

                    // write source
                    // discard everything before the h1 in the source, and everything after a second h1 or h2
                    String source = getSource(eSG.element("se1"));
                    source = source.substring(min);
                    min = source.indexOf(getSource(eH));
                    source = source.substring(min);

                    if (next != null) {
                        int index2 = source.indexOf(getSource(next));
                        source = source.substring(0, index2);
                    }

                    try { server.setNodeSource(nodes[level], source); }
                    catch (Exception ex) { System.err.println("Could not add source for: "+nodes[level].get("NODETITLE")); }

                    level = level - incl;
                }
            }

            if (eSG.element("ssect") != null) {
                sTitles[level] = getSource(eSG.element("ssect").element("shg"));

                for (int i = 0; i <= level; i++) {
                    if (i > 0) System.out.print(" -> ");
                    System.out.print(sTitles[i]);
                }

                System.out.println(" ");

                // add to database
                nodes[level] = addNode(sTitles[level], nodes[level-1].get("NODEID"), level+1);
                if (eSG.element("ssect").element("sg").element("se1") != null) {
                    try { server.setNodeSource(nodes[level], getSource(eSG.element("ssect").element("sg").element("se1"))); }
                    catch (Exception ex) { System.err.println("Could not add source for: "+nodes[level].get("NODETITLE")); }
                }

                recurseElement(eSG.element("ssect"));
            }
        }

        level--;
    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh) && !(prevCh != (char) 39))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static String getSource(Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) {
                sb.append(((Text) obj).getText());
            } else if (obj instanceof Element) {
                if (((Element) obj).attribute("char") != null)
                    sb.append(((Element) obj).attribute("char").getText());
                else
                    sb.append(getSource((Element) obj));
            }
        }

        return sb.toString();
    }

    public static Node addNode(String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}