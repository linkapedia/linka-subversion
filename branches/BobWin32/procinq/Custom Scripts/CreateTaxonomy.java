// CreateTaxonomy
//
// Log into the ProcinQ Server, read through a file and create a taxonomy with two topics

import com.iw.system.*;
import com.iw.tools.*;

import java.util.*;
import java.io.*;

public class CreateTaxonomy {
    public static Hashtable htArguments = new Hashtable(); // hashtable arguments

    public static void main(String args[]) throws Exception {
        System.out.println("This program will create a session on the ProcinQ server.\n");
        htArguments = com.iw.tools.GetArguments.getArgHash(args);

        if (!htArguments.containsKey("server")) {
            System.err.println("Usage: Login -server <API>"); return;
        }

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArguments.get("server"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        // create a new taxonomy - this will also create the root node
        Corpus c = server.addCorpus("NewTaxonomy");

        // retrieve the root node from this taxonomy
        Node root = server.getCorpusRoot(c.getID());

        // create a child node underneath the root
        Node child1 = addNode("Child 1", root.get("NODEID"), 1, 1, c, server);

        // create a second child underneath the root
        Node child2 = addNode("Child 2", root.get("NODEID"), 1, 2, c, server);
   }

    public static Node addNode (String sNodeTitle, String sParent, int Depth, int Index, Corpus c, ProcinQ server) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth+"");
        n.set("NODEINDEXWITHINPARENT", Index+"");
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try { n = server.addNode(n); }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); e.printStackTrace(System.err); }

        return n;
    }
}