// GetNodes
//
// Given a corpus, log into the ProcinQ server and list all nodes in that corpus.

import com.iw.system.*;
import com.iw.tools.*;

import java.util.*;
import java.io.*;

public class GetNodes {
    public static Hashtable htArguments = new Hashtable(); // hashtable arguments

    public static void main(String args[]) throws Exception {
        System.out.println("This program will get all topics in a given corpus.\n");
        htArguments = com.iw.tools.GetArguments.getArgHash(args);

        if ((!htArguments.containsKey("corpus")) || (!htArguments.containsKey("server"))) {
            System.err.println("Usage: GetNodes -server <API> -corpus <corpusid>"); return;
        }

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArguments.get("server"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        System.out.println(" ");

        // Note: Only the first argument to this call (the query) is required.  The second argument is the location
        // to begin and the third argument is the number of results (defaults are 1 and 500, respectively)
        Vector topics = server.CQL("SELECT <NODE> WHERE CORPUSID = "+(String) htArguments.get("corpus"), 1, 20);
        for (int i = 0; i < topics.size(); i++) {
            Node n2 = (Node) topics.elementAt(i);

            Vector nodeTree = server.getNodeTree(n2.get("NODEID"));

            for (int j = nodeTree.size()-1; j > 0; j--) {
                if (j == nodeTree.size()-1) System.out.print("   ");
                else System.out.print(" > ");

                Node n = (Node) nodeTree.elementAt(j);
                System.out.print(n.get("NODETITLE"));
            }
            System.out.println(" ");
        }

   }
}