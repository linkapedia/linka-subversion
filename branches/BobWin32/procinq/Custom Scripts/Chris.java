import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class Chris {
    public static Hashtable nodes = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    public static boolean go = false;

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        File f = new File("C:/Documents and Settings/indraweb/projects/perlscripts/chris/codes.out");
        createTaxonomy(f);
    }

    public static void createTaxonomy(File f) throws Exception {
        String sCorpusName = "CHRIS";
        System.out.println("Corpus: "+sCorpusName);

        c = server.addCorpus(sCorpusName, sCorpusName);
        Node root = server.getCorpusRoot(c.getID());

        nodes.put("A", addNode("A", "A", root.get("NODEID"), 1));
        nodes.put("B", addNode("B", "B", root.get("NODEID"), 1));
        nodes.put("C", addNode("C", "C", root.get("NODEID"), 1));
        nodes.put("D", addNode("D", "D", root.get("NODEID"), 1));
        nodes.put("E", addNode("E", "E", root.get("NODEID"), 1));
        nodes.put("F", addNode("F", "F", root.get("NODEID"), 1));
        nodes.put("G", addNode("G", "G", root.get("NODEID"), 1));
        nodes.put("H", addNode("H", "H", root.get("NODEID"), 1));
        nodes.put("I", addNode("I", "I", root.get("NODEID"), 1));
        nodes.put("J", addNode("J", "J", root.get("NODEID"), 1));
        nodes.put("K", addNode("K", "K", root.get("NODEID"), 1));
        nodes.put("L", addNode("L", "L", root.get("NODEID"), 1));
        nodes.put("M", addNode("M", "M", root.get("NODEID"), 1));
        nodes.put("N", addNode("N", "N", root.get("NODEID"), 1));
        nodes.put("O", addNode("O", "O", root.get("NODEID"), 1));
        nodes.put("P", addNode("P", "P", root.get("NODEID"), 1));
        nodes.put("Q", addNode("Q", "Q", root.get("NODEID"), 1));
        nodes.put("R", addNode("R", "R", root.get("NODEID"), 1));
        nodes.put("S", addNode("S", "S", root.get("NODEID"), 1));
        nodes.put("T", addNode("T", "T", root.get("NODEID"), 1));
        nodes.put("U", addNode("U", "U", root.get("NODEID"), 1));
        nodes.put("V", addNode("V", "V", root.get("NODEID"), 1));
        nodes.put("W", addNode("W", "W", root.get("NODEID"), 1));
        nodes.put("X", addNode("X", "X", root.get("NODEID"), 1));
        nodes.put("Y", addNode("Y", "Y", root.get("NODEID"), 1));
        nodes.put("Z", addNode("Z", "Z", root.get("NODEID"), 1));
        nodes.put("0", addNode("Misc", "Misc", root.get("NODEID"), 1));

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null; String abbrev = null; String title = null; Node n = null;
        try {
            while ( (record=dis.readLine()) != null ) {
                String args[] = record.split("\t");

                if (abbrev == null) {
                    abbrev = args[0];
                    title = args[1];

                    Node parent = (Node) nodes.get(title.substring(0, 1));
                    if (parent == null) parent = (Node) nodes.get("0");

                    n = addNode(title, record, parent.get("NODEID"), 2);
                } else {
                    Vector v = new Vector();
                    v.add(new Signature(abbrev, 10.0));

                    for (int i = 0; i < args.length; i++) {
                        v.add(new Signature(args[i], 10.0));
                    }

                    server.saveSignatures(n.get("NODEID"), v, false);

                    abbrev = null; title = null; n = null;
                }
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}