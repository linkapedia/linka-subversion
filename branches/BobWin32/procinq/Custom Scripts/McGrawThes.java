import com.iw.system.*;

import java.util.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.xml.sax.InputSource;

public class McGrawThes {

    public static com.iw.system.Thesaurus t = null;
    public static ProcinQ server = null;

    public static void main(String args[]) throws Exception {

        // Step 1. Initialize the server object
        server = new ProcinQ();
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("its", "its");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        // create thesaurus
        t = server.createThesaurus("Thesaurus (McGraw ESaT)");

        // now loop through each item in the directory
        String dirpath = "C:/Documents and Settings/indraweb/Desktop/TAX-DEV/Encyclopedia of Science and Technology/";
        File dir = new File(dirpath);
        String[] children = dir.list();  int loop = 0;
        for (int k=0; k < children.length; k++) {
            loop++;
            File f = new File(dirpath+children[k]);
            if (f.getAbsolutePath().endsWith(".xml")) {
                FileInputStream fis = new FileInputStream(f);
                SAXReader xmlReader = new SAXReader(false);
                xmlReader.setValidation(false);

                InputSource ins = new InputSource(fis);
                ins.setEncoding("UTF-8");
                ins.setSystemId(f.toURL().toString());

                org.dom4j.Document doc = xmlReader.read(ins);

                Element eArticle = doc.getRootElement();
                Element eTerm = (Element) eArticle.element("ATL");
                Element eText = (Element) eArticle.element("ATEXT");

                Vector xref = getParentTopics(eArticle);

                for (int i = 0; i < xref.size(); i++) {
                    addRelationship("1", eTerm.getText(), (String) xref.elementAt(i));
                }
            }
        }
    }

    private static void addRelationship(String relat, String base, String predicate) {
        System.out.println("Relationship: " + relat + " Word: " + base + " Predicate: "+predicate);

        String relationship = "0";
        if (relat.equals("UF")) { relationship = "1"; }
        else if (relat.equals("BT")) { relationship = "2"; }
        else if (relat.equals("NT")) { relationship = "3"; }

        try { server.addRelationship(t.getID(), base, predicate, relationship); }
        catch (Exception e) { e.printStackTrace(System.err); }
    }

    public static Vector getParentTopics (Element e) {
        Vector v = new Vector();

        List l = e.elements();
        Iterator i = l.iterator();

        while (i.hasNext()) {
            Element e2 = (Element) i.next();

            if (e2.getName().toLowerCase().equals("xref")) {
                Node n = null;
                String title = e2.getText().trim();
                title = title.replaceAll("\n", " ");
                v.add(title);
            } else {
                Vector v2 = getParentTopics(e2);
                if (v2.size() > 0) {
                    for (int j = 0; j < v2.size(); j++) {
                        v.add(v2.elementAt(j));
                    }
                }
            }
        }

        return v;
    }
}