
time /T
rem goto :BUILDALL_STEP9_create_insaller_out_dirs

:BUILDALL_STEP1 build itsapi
call build itsapi
echo "BUILDALL_STEP1 build itsapi"
rem pause

if not exist ""C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar"" ECHO "missing "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar""
if not exist ""C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar"" pause


:BUILDALL_STEP2 build itsclassifier
call build itsclassifier
echo "BUILDALL_STEP1 build itsclassifier"
rem pause

:BUILDALL_STEP3 build javadoc 
call cd1
cd "C:\Documents and Settings\Indraweb\buildprocess\javadocrun"
call javadocs_gen.bat

xcopy "C:\Documents and Settings\Indraweb\buildprocess\javadocoutput" "C:\Program Files\ITS\javadoc" /s
copy "C:\Documents and Settings\Indraweb\buildprocess\javadocinput\index.html" "C:\Program Files\ITS\javadoc" 


:BUILDALL_STEP9_create_insaller_out_dirs 

rmdir /S /Q     "C:\Program Files\install4j\iwprojects\Installer Build"
if exist "C:\Program Files\install4j\iwprojects\Installer Build" ECHO "ERROR removing C:\Program Files\install4j\iwprojects\Installer Build"
if exist "C:\Program Files\install4j\iwprojects\Installer Build" pause
mkdir "C:\Program Files\install4j\iwprojects\Installer Build"

:BUILDALL_STEP10 create installer for ProcinQserver
ss get "$/BuildProcesses/install4j/ProcinQSERVER.install4j" -GL"C:\Program Files\install4j\iwprojects" -R  -O- -Yhkon,racer9
"C:\Program Files\install4j\bin\install4jc.exe" -v "C:\Program Files\install4j\iwprojects\ProcinQSERVER.install4j"

:BUILDALL_STEP11 create installer for ProcinQedit 
ss get "$/BuildProcesses/install4j/ProcinQEDIT.install4j" -GL"C:\Program Files\install4j\iwprojects" -R  -O- -Yhkon,racer9
"C:\Program Files\install4j\bin\install4jc.exe" -v "C:\Program Files\install4j\iwprojects\ProcinQEDIT.install4j"
	
:BUILDALL_STEP12 create installer for ProcinQclassifier
echo BUILDALL_STEP12 create installer for ProcinQclassifier
ss get "$/BuildProcesses/install4j/ProcinQCLASSIFIER.install4j" -GL"C:\Program Files\install4j\iwprojects" -R  -O- -Yhkon,racer9
"C:\Program Files\install4j\bin\install4jc.exe" -v "C:\Program Files\install4j\iwprojects\ProcinQCLASSIFIER.install4j"


call cd1

:BUILDALL_STEP20 copycdms

copycdms

rem call build itsharvester
rem call build itsharvesterclient

time /T
echo COMPLETE 
pause
