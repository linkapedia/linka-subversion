set BUILDPROCESS=C:\Documents and Settings\Indraweb\buildprocess

echo Procinq_STEP0 - delete target cd image folders to start
rmdir /S /Q     \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage
if exist \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage"
if exist \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage pause
rem repeat for itsclassifier
rmdir /S /Q     \\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier
if exist \\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier"
if exist \\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier pause



echo Procinq_STEP1 - war files
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files"
copy /Y "C:\Program Files\ITS\WARS\itsapi.war"                 "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files"
copy /Y "C:\Program Files\ITS\WARS\servlet.war"                 "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files"

echo Procinq_STEP2 - jar files
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files"


rem  1 root / ldap / open ldap - those 4 files 
rem  2 ad exe file

echo Procinq_STEP5 - ad
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\ad"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\ad" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\ad"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\ad" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\ad"
rem repeat for itsalclassifier 
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\ad"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\ad" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\ad"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\ad" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\ad"

echo Procinq_STEP6 - ldap
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\openldap"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\openldap" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\openldap"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\openldap" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\OpenLDAP"
rem repeat for itsalclassifier 
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\openldap"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\openldap" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\openldap"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\openldap" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\OpenLDAP"


echo Procinq_STEP7 - COPY internal JAR FILES
copy /Y "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar"                 "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsapi-svr.jar"
if not exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsapi-svr.jar" ECHO "missing \\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsapi-svr.jar"
if not exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsapi-svr.jar" pause

copy /Y "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet\WEB-INF\lib\itsapi-ui.jar"                 "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\servlet.jar"
if not exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\servlet.jar" echo "missing \\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\servlet.jar"
if not exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\servlet.jar" pause

copy /Y "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsclassifier\WEB-INF\lib\itsclassifier-svr.jar"                 "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsclassifier-svr.jar"
if not exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsclassifier-svr.jar" echo "missing \\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsclassifier-svr.jar"
if not exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\jar files\itsclassifier-svr.jar" pause

echo Procinq_STEP7.5 copy cdimage AD files
copy "C:\Documents and Settings\Indraweb\buildprocess\ADirectory\*.ldif" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\ad"  /Y
copy "C:\Documents and Settings\Indraweb\buildprocess\ADirectory\*.exe" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\ad"  /Y
rem repeat for itsalclassifier 
copy "C:\Documents and Settings\Indraweb\buildprocess\ADirectory\*.ldif" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\ad"  /Y
copy "C:\Documents and Settings\Indraweb\buildprocess\ADirectory\*.exe" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\ad"  /Y

echo Procinq_STEP7.75 copy cdimage openldap files 
copy "C:\Documents and Settings\Indraweb\buildprocess\OpenLDAP\*.schema" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\openldap"  /Y
copy "C:\Documents and Settings\Indraweb\buildprocess\OpenLDAP\*.conf" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\openldap"  /Y
copy "C:\Documents and Settings\Indraweb\buildprocess\OpenLDAP\*.ldif" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\openldap"  /Y
rem repeat for itsalclassifier 
copy "C:\Documents and Settings\Indraweb\buildprocess\OpenLDAP\*.schema" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\openldap"  /Y
copy "C:\Documents and Settings\Indraweb\buildprocess\OpenLDAP\*.conf" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\openldap"  /Y
copy "C:\Documents and Settings\Indraweb\buildprocess\OpenLDAP\*.ldif" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\openldap"  /Y


echo Procinq_STEP8 - cdimages - copy installer files 
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer"
rem repeat for itsalclassifier 
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\installer"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\installer" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\installer" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\installer"

copy "C:\Program Files\install4j\iwprojects\Installer Build\ProcinqEDIT_*.*" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer" 
copy "C:\Program Files\install4j\iwprojects\Installer Build\Procinq_*.*" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer" 
copy "C:\Program Files\install4j\iwprojects\Installer Build\ProcinqClassifier_*.*" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier\installer" 

echo Procinq_STEP9 - copy internal gui - itsadmin
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\bin"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\bin" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\internal\bin"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\bin" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\bin\itsadmin"
xcopy "C:\Program Files\ITS\bin\itsadmin" "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\bin\itsadmin"   /Y /S /Q


echo Procinq_STEP9.5 copy db scripts to cdimage\database
rmdir /S /Q     "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database" ECHO "ERROR removing \\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database"
if exist "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database" pause
mkdir "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database"
copy "C:\Documents and Settings\Indraweb\buildprocess\database\installation\*.sql"  "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database"
copy "C:\Documents and Settings\Indraweb\buildprocess\database\installation\*.dmp"  "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database"
copy "C:\Documents and Settings\Indraweb\buildprocess\database\installation\*.java"  "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\database"


rem Procinq_STEP10 get installer guide 
copy "C:\Documents and Settings\Indraweb\buildprocess\doc\Installation Guide.pdf" "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage"


echo Procinq_STEP15 - copy jar files to c:\ftpserver for linux pickup
cd "c:\ftpserver"
del  /F /Q  .
copy /Y "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi\WEB-INF\lib\itsapi-svr.jar"                 .
copy /Y "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet\WEB-INF\lib\itsapi-ui.jar"                 .
copy /Y C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsclassifier\WEB-INF\lib\itsclassifier-svr.jar   .

echo Procinq_STEP16 - copy itsadmin files to c:\ftpserver for whomever to pickup
xcopy "C:\Program Files\ITS\bin\itsadmin" .  /Y /S /Q

echo Procinq_STEP17 get tomcat
copy \\192.168.0.10\indraweb\applications\jakarta-tomcat-5.0.18\jakarta-tomcat-5.0.18.exe   "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage" 
copy \\192.168.0.10\indraweb\applications\jakarta-tomcat-5.0.18\jakarta-tomcat-5.0.18.exe   "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimageclassifier" 


del /F "c:\ftpserver\ProcinqEDIT_windows_1_2.exe"
copy "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer\ProcinqEDIT_windows_1_2.exe" "c:\ftpserver\Procinq_windows_1_2.exe"
del /F "c:\ftpserver\Procinq_windows_3_0.exe"
copy "\\192.168.0.10\indraweb\Indraweb Deliverables\cdimage\installer\Procinq_windows_3_0.exe" "c:\ftpserver\Procinq_windows_3_0.exe"
del /F "c:\ftpserver\itsapi.war"
copy "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files\itsapi.war" "c:\ftpserver\itsapi.war"
del /F "c:\ftpserver\servlet.war"
copy "\\192.168.0.10\indraweb\Indraweb Deliverables\internal\war files\servlet.war" "c:\ftpserver\servlet.war"


cd1
echo done copycdms.bat
