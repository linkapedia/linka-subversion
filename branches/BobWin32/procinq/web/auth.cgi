#!/usr/bin/perl
#
# auth.cgi
# Written by: Michael A. Puscar
# Check user against our authentication database.
# If the user is valid, write user information into the cookie file.

use CGI;
use DBI;
use HTTP::Cookies;

$ENV{'ORACLE_HOME'} = "/opt/oracle";
local ($query) = new CGI;

# Were username and password information even sent?
if ((!defined($query->param('EMAIL'))) ||
    (!defined($query->param('PASSWORD'))) ||
    (!defined($query->param('LOGIN')))) {
    print "Location: /errors/missing-info.html\n\n";
    exit(1);
}

local ($CORPUS_SHORT) = $query->param('CORPUS_SHORT');
local ($CORPUS) = $query->param('CORPUS');
local ($CORPUS_NAME) = $query->param('CORPUS_NAME');

# Connect to the LINUX Oracle database and get this user's information
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");

my ($sth) = &validate_user();
my ($uid, $fname, $dcorpus) = $sth->fetchrow_array; $sth->finish();
if ($uid eq "") { &raise_error("Sorry, the e-mail address or password that you entered is not valid.\n"); }

# Sometimes an existing user subscribers to another SBook.  Catch that case here.
if (defined($query->param('EXISTS'))) { 
   # CREATE OR REPLACE PROCEDURE SubscribeExistingUser (
   #     USERID_IN IN Number, -- User identifier
   #     CORPUS_IN IN Number -- Corpus subscribing to..
   my $subscribe_user = $dbh->prepare(q{
        BEGIN
            SubscribeExistingUser(
                      USERID_IN => :user,
                      CORPUSID_IN => :corp
                      );
        END;
    });

   $subscribe_user->bind_param(":user", $uid);
   $subscribe_user->bind_param(":corp", $CORPUS);
   $subscribe_user->execute() || &raise_db_error($subscribe_user->err, $dbh->errstr);
   $subscribe_user->finish; undef $sth;
}

if (&does_user_have_this_corpus($uid, $CORPUS) == 0) { 
   print "Location: /register.cgi?type=new&corpus=$CORPUS&uid=$uid\n\n";
   exit(1);
}

$sth = &get_user_corpus($uid);

print "Content-type: text/html\n\n";
print "<HTML><HEAD><SCRIPT LANGUAGE='JavaScript' SRC='javascript/cookies.js'></SCRIPT>\n";
print "<SCRIPT LANGUAGE='JavaScript'>\n";

print "  SetCookie ('USERID', $uid, 365);\n";
print "  SetCookie ('NAME','".$fname."', 365);\n";

my ($csl, $csh, $exp_days);
my ($authorized) = 0;

while (($csl, $csh, $exp_days) = $sth->fetchrow_array) {
    print "  SetCookie ('$csh', 1, $exp_days);\n";    
    if ($csh eq $CORPUS_SHORT) { $authorized = 1; }
}
$sth->finish();
$dbh->disconnect();

if ($authorized == 0) { $CORPUS_SHORT = $csh; $CORPUS_NAME = $csl; }
print "</SCRIPT>\n";

open (FILE, "templates/login-welcome.tpl"); @lines = <FILE>; close(FILE);
foreach $line (@lines) {
    $line =~ s/##(.*)##/${$1}/gi;
    print $line;
}

1;

sub raise_error {
    my ($error_msg) = $_[0];

    print "Content-type: text/html\n\n";
    open (FILE, "templates/head.tpl"); while (<FILE>) { print; } close(FILE);
    print "$error_msg Please click your <b>back</b> button to return to the previous page.\n";
    open (FILE, "templates/foot.tpl"); while (<FILE>) { print; } close(FILE);
    exit(1);
}

sub validate_user {
    my $q;
    $q = "SELECT UserId, FirstName, DefaultCorpusName FROM Subscriber WHERE Email = UPPER('".$query->param('EMAIL')."') and Password = UPPER('".$query->param('PASSWORD')."')";
    
    my $sth = $dbh->prepare($q) or return undef;
    $sth->execute() or return undef;
    
    return ($sth);
}

# does_user_have_this_corpus($uid, $CORPUS)
sub does_user_have_this_corpus {
    my ($uid, $corpus) = @_;

    my $q = "SELECT CorpusId FROM SubscriberCorpus WHERE UserId = $uid AND CorpusId = $corpus";
    my $sth = $dbh->prepare($q) or return undef;
    $sth->execute() or return undef;

    my ($cid) = $sth->fetchrow_array;

    if ($cid == $corpus) { return 1; } else { return 0; }
}

sub get_user_corpus {
    my ($uid) = $_[0];

    my $q = "SELECT C.Corpus_Name, C.CorpusShortName, (S.ExpirationDate - SYSDATE) FROM SubscriberCorpus S, Corpus C WHERE S.UserId = $uid";
    my $sth = $dbh->prepare($q) or return undef;
    $sth->execute() or return undef;

    return ($sth);
}

sub raise_db_error {
    my ($err_num, $err_str) = @_;

    if ($err_num == 20002) {
        &raise_error("Sorry, the identifier that you entered is not valid.  Your unique, 10 digit identifier is required in order to obtain the free trial.   It is located on the inside cover of the book.\n"); }
    if ($err_num == 20003) {
        &raise_error("Sorry, that identifier is already in use.   Each identifier may only be used once to gain a free trial subscription.   If you find this message to be an error, please contact our <a href='mailto:info\@indraweb.com'>customer service</a> department with the details of this transaction.   If you purchased this book used, you can <a href='/register.cgi?corpus=$corpus&type=new'>register without a free trial</a>.\n");
    }
    
    &raise_error("Sorry, there has been an unexpected error processing your subscription.   It will not be processed at this time.   The system administrator has been notified of the problem.   Please try your request again later.  <!-- Num: $err_num Str: $err_str -->\n");
    exit(1);
}

1;
