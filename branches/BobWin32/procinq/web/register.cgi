#!/usr/bin/perl
#
# register.cgi
# Written by: Michael A. Puscar
# Register the user as an S-Book subscriber

use CGI;
use DBI;
use Net::SMTP;

$ENV{'ORACLE_HOME'} = "/opt/oracle";
local ($query) = new CGI;

# If CORPUS is not defined, flag an error
if (!defined($query->param('corpus'))) {
    print "Location: /index.html\n\n"; 
}
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
local ($FIRSTNAME, $LASTNAME, $EMAIL, $STREET, $CITY, $STATE, $ZIPCODE);

# If this user has already subscribed to our service, load their information
if (defined($query->param('uid'))) { 
    my ($uid) = $query->param('uid');
    $q = "select EMAIL, FIRSTNAME, LASTNAME, STREET, CITY, STATE, ZIPCODE from Subscriber where userid = $uid";
    my $sth = $dbh->prepare($q) || die "Cannot load user information: $!\n";
    $sth->execute() || die "Cannot load user information: $!\n";  
    ($EMAIL, $FIRSTNAME, $LASTNAME, $STREET, $CITY, $STATE, $ZIPCODE) = $sth->fetchrow_array;
    $sth->finish;
}

# Requesting subscription to an additional S-book, but already in our system..
if (defined($query->param('exists'))) {
   local ($corpus) = $query->param('corpus');
   local ($email) = $query->param('email');
   local ($corpus_short) = $query->param('corpus_short');
   local ($corpus_name) = $query->param('corpus_name');

   open (FILE, "templates/email-exists.tpl"); @lines = <FILE>; close(FILE);

   print "Content-type: text/html\n\n";
   foreach $line (@lines) { 
	$line =~ s/##(.*)##/${$1}/gi;
	print $line;
   }

   $dbh->disconnect();   
   exit(1);
}

# Request load of the registration page if no submission
if (!defined($query->param('SUBMIT'))) { 
    local ($CORPUS) = $query->param('corpus');

    # For display purposes need: Corpus Desc, Number of Nodes
    local ($CORPUS_SHORT, $CORPUS_NAME, $TRIAL, $TRIAL_TEXT, $EXPIR_TEXT, $FEE, $COUNT) = &get_corpus_info($CORPUS);
    
    # If FEE is 0, user should not need to log in for this service!
    if ($FEE == 0) { print "Location: /$CORPUS_SHORT/1.htm\n\n"; }
    
    print "Content-type: text/html\n\n";
    my (@lines);

    if ($query->param('type') eq "activate") {    
 	open (FILE, "templates/register.tpl"); @lines = <FILE>; close(FILE);
    } elsif ($query->param('type') eq "new") {
	open (FILE, "templates/register-new.tpl"); @lines = <FILE>; close(FILE);
    }
    
    foreach $line (@lines) { 
	$line =~ s/##(.*)##/${$1}/gi;
	print $line;
    }

# User is submitting the form -- call our stored procedure and send to thank you page
} else {
    local ($corpus) = $query->param('corpus');
    local ($CORPUS_NAME) = $query->param('CORPUS_NAME');
    local ($CORPUS_SHORT) = $query->param('CORPUS_SHORT');
    local ($EMAIL) = $query->param('email');
    local ($SPAM);
    
    if (defined($query->param('spam'))) { $SPAM = 1; } else { $SPAM = 0; }
    
    # STEP 1: FIELD CHECKING
    # Possible error conditions: 
    
    #  A) A required field was left empty
    if (($query->param('firstname') eq "") ||
	($query->param('lastname') eq "") || ($query->param('street') eq "") ||
	($query->param('city') eq "") || ($query->param('state') eq "") ||
	($query->param('zip') eq "") || ($query->param('email') eq "") ||
	($query->param('Country') eq "")) {
	&raise_error("Sorry, one or more of the fields on the registration page was left blank. \n");
    }
    
    #  B) E-mail address must be valid (contain an @ sign, period, and no spaces)
    if (($query->param('email') !~ /\@/) || ($query->param('email') !~ /\./) || 
	($query->param('email') =~ /\ /)) { 
	&raise_error("The e-mail address that you entered is not valid.   Note that your password will be send to you via e-mail, so you must enter a valid e-mail address.\n");
    }
    
    #  C) User must check the terms and conditions box
    if (!defined($query->param('Terms'))) { 
	&raise_error("Sorry, you did not check the terms and conditions agreement box.  In order to subscribe to our service you will need to agree to the terms and conditions of the web site.\n");
    }
    
    #  D) User did not enter their credit card
    if ($query->param('creditnum') eq "") { 
	&raise_error("You must enter your credit card number in order to subscribe to this S-Book.\n");
    }

    #  E) If user is requesting a free trial, ensure cover code is supplied
    if ((defined($query->param('username'))) && ($query->param('username') eq "")) {
	&raise_error("You did not enter the 10 digit identifier.   This identifier is necessary to take advantage of the free trial offer.   If you did not purchase the book, or purchased it used, you can <a href='/register.cgi?corpus=$corpus&type=new'>register without the identifer here</a>.\n"); }

    local ($COVER);
    if (defined($query->param('username'))) { $COVER = $query->param('username'); }
    else { $COVER = "NULL"; }
    
    # STEP 2: GENERATE RANDOM PASSWORD
    my @a=(0..9,A..Z);
    local $randompass = join '', map { $a[int rand @a] } (0..7);
    
    # STEP 3: CALL STORED PROCEDURE
    my $subscribe_user = $dbh->prepare(q{
	BEGIN
	    Subscribe(
		      EMAIL_IN => :email,
		      PASSWORD_IN => :password,
		      FIRSTNAME_IN => :fname,
		      LASTNAME_IN => :lname,
		      STREET_IN => :street,
		      CITY_IN => :city,
		      STATE_IN => :state,
		      ZIP_IN => :zip,
		      COUNTRY_IN => :country,
		      CORPUSID_IN => :cid,
		      SPAM_IN => :spam,
		      COVERCODE_IN => :code
		      );
	END;
    });
    
    $subscribe_user->bind_param(":email", $query->param('email')); 
    $subscribe_user->bind_param(":password", $randompass);
    $subscribe_user->bind_param(":fname", $query->param('firstname'));
    $subscribe_user->bind_param(":lname", $query->param('lastname'));
    $subscribe_user->bind_param(":street", $query->param('street'));
    $subscribe_user->bind_param(":city", $query->param('city'));
    $subscribe_user->bind_param(":state", $query->param('state'));
    $subscribe_user->bind_param(":zip", $query->param('zip'));
    $subscribe_user->bind_param(":country", $query->param('Country'));
    $subscribe_user->bind_param(":cid", $query->param('corpus'));
    $subscribe_user->bind_param(":spam", $SPAM);
    $subscribe_user->bind_param(":code", $COVER);
    $subscribe_user->execute || &raise_db_error($subscribe_user->err, $dbh->errstr);
 
    $dbh->disconnect();
 
    # STEP 4: GENERATE OUTPUT
    print "Content-type: text/html\n\n";
    print "<HTML><HEAD><!-- COVER: ".$COVER." -->\n";

    # STEP 5: SEND WELCOME E-MAIL
    # Send an e-mail out to the user with their username and password
    $smtp = Net::SMTP->new('smtp.netreach.net');
    
    $smtp->mail('info@indraweb.com');
    $smtp->to($EMAIL);
    
    $smtp->data();
    $smtp->datasend("To: $EMAIL\n");
    $smtp->datasend("Subject: Welcome to Surfable Books!\n");
    $smtp->datasend("\n");
    $smtp->datasend($query->param('firstname').",\n\n");
    $smtp->datasend("Thank you for subscribing to the $CORPUS_NAME\n");
    $smtp->datasend("S-Book.  Your S-Book subscription will provide you with \n");
    $smtp->datasend("pages of additional research and detail in this subject area. \n\n");
    $smtp->datasend("Your password to use the web site is: $randompass\n");
    $smtp->datasend("You can change your password at the Surfable Book web site, go\n");
    $smtp->datasend("to http://207.29.192.131/change-pw.html\n\n");
    $smtp->datasend("Please write your password down, you will need it to access your\n");
    $smtp->datasend("S-Book subscription.\n\nThank you, and enjoy your new surfable book.\n\n");
    $smtp->datasend("The Surfable Books Staff\n");
    $smtp->dataend();
    
    open (FILE, "templates/register-thanks.tpl"); @lines = <FILE>; close(FILE);
    foreach $line (@lines) { 
	$line =~ s/##(.*)##/${$1}/gi;
	print $line;
    }
}

sub raise_db_error {
    my ($err_num, $err_str) = @_;

    # Check for exception conditions 
    # ERROR: 20001 is when the user enters an e-mail address already in our system
    if ($err_num == 20001) { 
        local ($CORPUS_SHORT, $CORPUS_NAME, $TRIAL, $TRIAL_TEXT, $EXPIR_TEXT, $FEE, $COUNT) = &get_corpus_info($corpus);
	print "Location: /register.cgi?exists=1&corpus=$corpus&corpus_short=$CORPUS_SHORT&corpus_name=$CORPUS_NAME&email=".$query->param('email')."\n\n";
        exit(1);
    }
    if ($err_num == 20002) {
	&raise_error("Sorry, the identifier that you entered is not valid.  Your unique, 10 digit identifier is required in order to obtain the free trial.   It is located on the inside cover of the book.\n"); }
    if ($err_num == 20003) {
	&raise_error("Sorry, that identifier is already in use.   Each identifier may only be used once to gain a free trial subscription.   If you find this message to be an error, please contact our <a href='mailto:info\@indraweb.com'>customer service</a> department with the details of this transaction.   If you purchased this book used, you can <a href='/register.cgi?corpus=$corpus&type=new'>register without a free trial</a>.\n");
    }

    &raise_error("Sorry, there has been an unexpected error processing your subscription.   It will not be processed at this time.   The system administrator has been notified of the problem.   Please try your request again later.  <!-- Num: $err_num Str: $err_str -->\n");
    exit(1);
}

sub get_corpus_info {
    my $CORPUS = $_[0];

    my $q = "SELECT CorpusShortName, Corpus_Name, Trial_Days, Trial_Days_Text, Expire_Days_Text, Fee FROM corpus WHERE CorpusId = $CORPUS";
    my $sth = $dbh->prepare($q) or return undef;
    $sth->execute() or return undef;
    my ($CS, $CN, $TD, $TDT, $ED, $FEE) = $sth->fetchrow_array; $sth->finish(); undef $sth;
 
    my $q = "SELECT count(*) FROM Node WHERE CorpusId = $CORPUS";
    my $sth = $dbh->prepare($q) or return undef;
    $sth->execute() or return undef;
    my $CO = $sth->fetchrow_array; $sth->finish(); undef $sth;
   
    return ($CS, $CN, $TD, $TDT, $ED, $FEE, $CO);
}
 
sub raise_error {
    my ($error_msg) = $_[0];

    print "Content-type: text/html\n\n";
    open (FILE, "templates/head.tpl"); while (<FILE>) { print; } close(FILE);
    print "$error_msg Please click your <b>back</b> button to return to the previous page.\n";
    open (FILE, "templates/foot.tpl"); while (<FILE>) { print; } close(FILE);
    exit(1);
}
  
1;
