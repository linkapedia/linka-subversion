<LINK href="/images/surf.css" rel=stylesheet>
<TITLE>Introduction: ##CORPUS_NAME## S Book</TITLE>
</HEAD>
<BODY bgColor=#ffffff leftMargin=0 topMargin=0 marginheight="0" marginwidth="0">
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD width=149><A href="http://www.surfablebooks.com/"><IMG 
      alt="HOMEPAGE - The Surfable Book Project" border=0 height=62 
      src="/images/surfable_header1_small.gif" width=149></A></TD>
    <TD width=573><IMG alt="The Surfable Book Project" height=62 
      src="/images/surfable_header2_small.gif" width=573></TD>
    <TD width="100%"><IMG height=62 
      src="/images/surfable_header3_small.gif" 
width="100%"></TD></TR></TBODY></TABLE>
<BR><blockquote>
<FONT size=+1 face="Arial, Helvetica, sans-serif">
<b>Thank you for subscribing!</b></FONT><P>
<FONT face="Arial, Helvetica, sans-serif">
Thank you for subscribing to the ##CORPUS_NAME## Surfable
Book.   In order to verify the information that you submitted, 
a confirmation e-mail has been sent to the following 
e-mail address: ##EMAIL##.  
<P>
That e-mail contains the password that
you will use to access your account.   Please retrieve the
password from your e-mail box and enter it into the box below.
<P>
<FORM NAME="Login" ACTION="auth.cgi" METHOD="POST">
<INPUT TYPE="HIDDEN" NAME="LOGIN" VALUE="YES">
<input type="hidden" name="CORPUS_NAME" value="##CORPUS_NAME##"> 
<input type="hidden" name="CORPUS_SHORT" value="##CORPUS_SHORT##"> 
      <table border=0 cellspacing=0 cellpadding=2 width=100%>
        <tr>
          <td width=40% align="right">
        <font face="arial,helvetica,sans-serif" size=-1><b>E-MAIL ADDRESS:</font></td>
          <td width=60%><font>
            <INPUT TYPE="TEXT" NAME="EMAIL" SIZE=24 MAXLENGTH=50 value="##EMAIL##"></font><br>
            <img border=0 src="/images/spacer.gif" width=11 height=6><br>
          </td>
        </tr>
        <tr>
          <td width=40% align="right">
        <font face="arial,helvetica,sans-serif" size=-1><b>PASSWORD:</font></td>
          <td width=60%><font>
            <INPUT TYPE="PASSWORD" NAME="PASSWORD" SIZE=24 MAXLENGTH=10 value=""></font><br>
          </td>
        </tr>
      </table>
      <center>
      <br>
      <INPUT TYPE="SUBMIT" NAME="SUBMIT" VALUE="LOGIN"><br></center></form>
</FONT><BR>
<P align=center><FONT face="Arial, Helvetica, sans-serif">� 2001 IndraWeb
      Ltd. All rights reserved<BR><A class=footer
      href="/about_us.htm">About Us</A>&nbsp; |&nbsp; <A
      class=footer href="/contact_us.htm">Contact
      Us</A>&nbsp; |&nbsp; <A class=footer
      href="/jobs.htm">Jobs</A>&nbsp; |&nbsp; <A
      class=footer href="/privacy_policy.htm">Privacy
      Policy</A>&nbsp; |&nbsp; <A class=footer
      href="/terms%20of%20service">Terms of
      Service</A>&nbsp; |&nbsp; <A class=footer
      href="/help">Help</A></FONT></P>
      <P align=center> </P></FONT>
</blockquote>
</BODY>
</HTML>
