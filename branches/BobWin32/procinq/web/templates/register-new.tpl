<HTML><HEAD>
<TITLE>
##CORPUS_NAME## S-Book Registration
</TITLE>
<script language="Javascript">
function openWin(filename) {
var NewWin = window.open(filename,'MoreDetails','toolbar=no,status=no,width=580,height=455');
}
function openWin2(filename) {
var NewWin = window.open(filename,'Terms');
}
</script>
<SCRIPT LANGUAGE="JavaScript" SRC="/javascript/cookies.js"> </SCRIPT>
<SCRIPT> RedirectIfCookie(); </SCRIPT> 
<LINK href="/images/surf.css" rel=stylesheet>
<style type="text/css"><!--a:hover { color:#0000ff; text-decoration:underline; }a:link { color:#0000ff; text-decoration:underline; }a:visited { color:#0000ff; text-decoration:underline; }.getheading { font-family:arial,helvetica,verdana; font-weight:bold; font-size:12px; color:#0000ff; }.gettext { font-family:arial,helvetica,verdana; font-weight:normal; font-size:10px; color:#000000; }.redheading { font-family:arial,helvetica,verdana; font-weight:bold; font-size:16px; color:#cc3300; }.red { color:#cc3300; }.bkheading { font-family:arial,helvetica,verdana; font-weight:bold; font-size:15px; color:#000000; }.text { font-family:arial,helvetica,verdana; font-weight:normal; font-size:13px; color:#000000; }.smtext { font-family:arial,helvetica,verdana; font-weight:normal; font-size:11px; color:#000000; }.monospace { font-family:Courier,monospace; font-size:10px; }.background { background-image:url('./s-default/art/typist.jpg'); background-repeat:no-repeat; }-->
</style>
</HEAD>
<BODY bgColor=#ffffff leftMargin=0 topMargin=0 marginheight="0" marginwidth="0">
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD width=149><A href="http://www.surfablebooks.com/"><IMG 
      alt="HOMEPAGE - The Surfable Book Project" border=0 height=62 
      src="/images/surfable_header1_small.gif" width=149></A></TD>
    <TD width=573><IMG alt="The Surfable Book Project" height=62 
      src="/images/surfable_header2_small.gif" width=573></TD>
    <TD width="100%"><IMG height=62 
      src="/images/surfable_header3_small.gif" 
width="100%"></TD></TR></TBODY></TABLE>
<BR><blockquote>
<form action="register.cgi" method=post>
<input type="hidden" name="corpus" value="##CORPUS##">
<input type="hidden" name="CORPUS_NAME" value="##CORPUS_NAME##">
<input type="hidden" name="CORPUS_SHORT" value="##CORPUS_SHORT##">
<font size=-1 face="Arial">
Thank you for your interest in the S-book edition of <u>##CORPUS_NAME##</u>.   
This S-Book contains ##COUNT## fully surfable topics and sub topics.   Enjoy your blah blah hey what does this look like, a Holiday Inn?   Mike is having problems with marketing copy let's do this piece later.
</font><p>
<table border=0 cellpadding=0 cellspacing=0>
<tr><td><b><font color=black face="Arial" size=-2>First Name:</font></td>
<td><input type="text" name="firstname" value="##FIRSTNAME##" size=15 maxlength=30> &nbsp;</td>
<td><b><font color=black face="Arial" size=-2>Last Name:</font></td>
<td><input type="text" name="lastname" value="##LASTNAME##" size=30 maxlength=30> &nbsp;</td></tr>
<tr><td><b><font color=black face="Arial" size=-2>Street Address:</font></td>
<td><input type="text" name="street" value="##STREET##" size=30 maxlength=30> &nbsp;</td>
<td><b><font color=black face="Arial" size=-2>City:</font></td>
<td><input type="text" name="city" value="##CITY##" size=30 maxlength=30></td></tr>
<tr><td><b><font color=black face="Arial" size=-2>State (abbrev.):</font></td>
<td><input type="text" name="state" value="##STATE##" size=5 maxlength=2></td>
<td><b><font color=black face="Arial" size=-2>Zip Code:</font></td>
<td><input type="text" name="zip" value="##ZIPCODE##" size=15 maxlength=11></td></tr>
<tr><td><b><font color=black face="Arial" size=-2>E-mail Address:</font></td>
<td><input type="text" name="email" value="##EMAIL##" size=30 maxlength=30></td>
<td><b><font color=black face="Arial" size=-2>Country:</font></td>
<td><select name="Country"><option selected value="USA">United States<option value="CANADA">Canada</select></td></tr></table>
<font color=black face="Arial" size=-1>
<!--- <BR><input type="checkbox" name="TIME" value="good"> 
Try 4 risk free issues of <b>Time Magazine</b>. (<a href="javascript:openWin('offers/time.htm')">more details</a>) --->
<BR><input type="checkbox" name="spam" value="good" checked> 
Yes! Please notify me periodically with other special offers targeted to my interests.
<P>
<input type="checkbox" name="Terms" value="good"> 
I accept the <a href="javascript:openWin2('/terms%20of%20service.htm')">Terms and Conditions</a> of this service.
</font>
<P>
<p class="notation"> * Your credit card will be charged $##FEE## for a 
##EXPIR_TEXT## subscription.  That is only a fraction of the book price!
Please read the <a href="/terms%20of%20service.htm">Terms & Conditions</a> 
for more information.
<p><span class="bkheading">Credit Card Information</span><br>
Please be sure that your name and street address correspond with 
the name and address of your credit card. 
<P><img src="/images/cc-logo.gif">
<BR>
<table border=0 cellpadding=0 cellspacing=0>
<tr><td><b><font color=black face="Arial" size=-2>Credit Card Number:</font></td>
<td colspan=3><input type=text name="creditnum" size=16 maxlength=16> &nbsp;</td>
</tr>
<tr><td><b><font color=black face="Arial" size=-2>Credit Card Type:</font></td>
<td>
<select name="credittype">
<option> Visa
<option> Master Card
<option> Discover
<option> American Express
</select>&nbsp; &nbsp; &nbsp;
</td>
<td><b><font color=black face="Arial" size=-2>Expiration Date:</font></td>
<td>
<select name="exp_month">
<option> 01
<option> 02
<option> 03
<option> 04
<option> 05
<option> 06
<option> 07
<option> 08
<option> 09
<option> 10
<option> 11
<option> 12
</select>
<select name="exp_year">
<option> 2001
<option> 2002
<option> 2003
<option> 2004
<option> 2005
<option> 2006
<option> 2007
<option> 2008
<option> 2009
<option> 2010
</select>
</td></tr></table>
<P>
<input type="submit" name="SUBMIT" value="SUBMIT REGISTRATION">
</form>
<BR>
<P align=center><FONT face="Arial, Helvetica, sans-serif">� 2001 IndraWeb
      Ltd. All rights reserved<BR><A class=footer
      href="/about_us.htm">About Us</A>&nbsp; |&nbsp; <A
      class=footer href="/contact_us.htm">Contact
      Us</A>&nbsp; |&nbsp; <A class=footer
      href="/jobs.htm">Jobs</A>&nbsp; |&nbsp; <A
      class=footer href="/privacy_policy.htm">Privacy
      Policy</A>&nbsp; |&nbsp; <A class=footer
      href="/terms%20of%20service">Terms of
      Service</A>&nbsp; |&nbsp; <A class=footer
      href="/help">Help</A></FONT></P>
      <P align=center> </P></FONT>
</blockquote>
</BODY>
</HTML>
