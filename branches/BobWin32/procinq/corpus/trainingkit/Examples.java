// Examples
//
// This file contains a series of examples that may be used to access the ProcinQ Server API.

import com.iw.system.*;

import java.util.*;
import java.io.*;

public class Examples {
    public static void main(String args[]) throws Exception {
        System.out.println("This program will test commonly used API functions in the ProcinQ Server.\n");

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI("66.134.131.36");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        // EXAMPLES
        //
        // Listed below are a series of example calls into the ProcinQ API.

        // **** BEGIN EXAMPLE 1 ****
        // Example #1: Classify a document against the Worldbook (49) taxonomy and print the results
        // API calls used in this example: tsclassify.TSClassifyDoc, tsnode.TSGetNodeTree
        // Note: the client is responsible for converting the file into text format
        System.out.println("**** EXAMPLE #1: Classify a document and print the results ****");
        System.out.println(" ");
        File f = new File("sample-file.txt");
        Vector classifyResults = new Vector(); // classification results will be stored here
        StringBuffer sb = new StringBuffer(); // data from the file will be stored here

        System.out.println("Begin classify: "+f.getAbsolutePath());
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { sb.append(record); }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }

        // Classify the document.  The server expects a string and a corpusID list.  If more than one taxonomy
        // is specified, the corpusIDs in the second argument should be separated by commas with no spaces.
        try { classifyResults = server.classify(sb.toString(), "49"); }
        catch (SessionExpired se) {
            System.err.println("Session expired."); return; }
        catch (ClassifyError ce) {
            System.err.println("Classification failed.");
            ce.printStackTrace(System.err); return;
        }
        if (classifyResults.size() == 0) { System.err.println("Classification returned no results."); return; }
        System.out.println("Classification complete.\n");

        // Loop through the classification results and print detailed concept information
        for (int i = 0; i < classifyResults.size(); i++) {
            NodeDocument nd = (NodeDocument) classifyResults.elementAt(i);

            // Note: the attributes of the ND object are dynamic.  See API for a base set of attributes.
            // Additional attributes can be added simply by adding fields to the database table.   For this
            // example, only the titles of the topic hits are printed.
            System.out.println("Result: "+nd.get("NODETITLE")+" Score: "+nd.get("SCORE1"));

            // finally, this function will give you the context of the concepts that were returned
            Vector nodeTree = server.getNodeTree(nd.get("NODEID"));

            for (int j = nodeTree.size()-1; j > 0; j--) {
                if (j == nodeTree.size()-1) System.out.print("   ");
                else System.out.print(" > ");

                Node n = (Node) nodeTree.elementAt(j);
                System.out.print(n.get("NODETITLE"));
            }

            System.out.println("\n");
        }
        // **** END EXAMPLE 1 ***

        // **** BEGIN EXAMPLE 2 ****
        // Example #2: Get the list of taxonomies in the database and print them to standard out
        // API calls used in this example: tscorpus.TSListCorpora
        // Note: only taxonomies that this user has access to read will be returned from this function

        System.out.println(" ");
        System.out.println("**** EXAMPLE #2: Get the list of available taxonomies and print the results ****");
        System.out.println(" ");

        Vector taxonomyList = server.getCorpora();
        for (int i = 0; i < taxonomyList.size(); i++) {
            Corpus c = (Corpus) taxonomyList.elementAt(i);
            System.out.println("Taxonomy Name: "+c.getName()+" ID: "+c.getID());
        }

        // **** END EXAMPLE 2 ***

        // **** BEGIN EXAMPLE 3 ****
        // Example #3: Run a sample CQL call, in this case, return all topics in the Worldbook taxonomy with
        //   the word "Medicine" in the title.
        // API calls used in this example: tscql.TSCql, tsnode.TSGetNodeTree

        System.out.println(" ");
        System.out.println("**** EXAMPLE #3: Get all topics in Worldbook with the word Medicine in the title ****");
        System.out.println(" ");

        // Note: Only the first argument to this call (the query) is required.  The second argument is the location
        // to begin and the third argument is the number of results (defaults are 1 and 500, respectively)
        Vector topics = server.CQL("SELECT <NODE> WHERE NODETITLE LIKE 'Medicine' AND CORPUSID = 49", 1, 20);
        for (int i = 0; i < topics.size(); i++) {
            Node n2 = (Node) topics.elementAt(i);

            Vector nodeTree = server.getNodeTree(n2.get("NODEID"));

            for (int j = nodeTree.size()-1; j > 0; j--) {
                if (j == nodeTree.size()-1) System.out.print("   ");
                else System.out.print(" > ");

                Node n = (Node) nodeTree.elementAt(j);
                System.out.print(n.get("NODETITLE"));
            }
            System.out.println(" ");
        }

        // **** END EXAMPLE 3 ***

        // **** BEGIN EXAMPLE 4 ****
        // Example #4: select all topic children whose parentid = 570320 (Limits on Damage Liabilities) and,
        //    for each of these topics, print some topic information and the corresponding list of signature words
        // API calls used in this example: tscql.TSCql, tsnode.TSGetNodeProps and tsnode.TSGetNodeSignatures

        System.out.println(" ");
        System.out.println("**** EXAMPLE #4: Get topic children of node 570320, print names and signature words. ****");
        System.out.println(" ");

        topics = server.CQL("SELECT <NODE> WHERE PARENTID = 570320 ORDER BY NODEINDEXWITHINPARENT ASC");
        System.out.println("Topic 570320 has "+topics.size()+" child topics.\n");

        for (int i = 0; i < topics.size(); i++) {
            Node n = (Node) topics.elementAt(i);

            System.out.println("Topic Name: "+n.get("NODETITLE")+" Depth: "+n.get("DEPTHFROMROOT")+
                    " INDEX WITHIN PARENT: "+n.get("NODEINDEXWITHINPARENT"));

            Vector signatures = server.getNodeSignatures(n);

            for (int j = 0; j < signatures.size(); j++) {
                Signature s = (Signature) signatures.elementAt(j);
                System.out.println("   Signature: "+s.getWord()+" Weight: "+s.getWeight());
            }

            System.out.println(" ");
        }

        System.out.println(" ");

        System.out.println("\n *** END OF EXAMPLES ***");
   }
}