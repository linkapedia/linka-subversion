package com.iw.system;

import java.util.Vector;
import java.sql.ResultSet;
import java.io.PrintWriter;

public class Thesaurus {
    // User attributes
    private String ThesaurusID;
    private String ThesaurusName;
    private String DisplayName;

    // constructor(s)
    public Thesaurus(HashTree ht) {
        ThesaurusID = (String) ht.get("THESAURUSID");
        ThesaurusName = (String) ht.get("THESAURUSNAME");
        DisplayName = ThesaurusName + "    ";
    }

    public Thesaurus(Vector v) {
        ThesaurusID = (String) v.elementAt(0);
        ThesaurusName = (String) v.elementAt(1);
        DisplayName = ThesaurusName + "    ";
    }

    public Thesaurus(ResultSet rs) throws Exception {
        ThesaurusID = rs.getString(1);
        ThesaurusName = rs.getString(2);
    }

    // emit proper xml for this class
    public void emitXML(PrintWriter out) {
        emitXML(out, false);
    }

    public void emitXML(PrintWriter out, boolean WithWrap) {
        if (WithWrap) {
            out.println("<THESAURUS>");
        }
        out.println("   <THESAURUSID>" + ThesaurusID + "</THESAURUSID>");
        out.println("   <THESAURUSNAME>" + ThesaurusName + "</THESAURUSNAME>");
        if (WithWrap) {
            out.println("</THESAURUS>");
        }
    }

    // accessor functions
    public String getID() {
        return ThesaurusID;
    }

    public String getName() {
        return ThesaurusName;
    }

    public void setID(String ID) {
        ThesaurusID = ID;
    }

    public void setName(String Name) {
        ThesaurusName = Name;
    }

    public String toString() {
        return DisplayName;
    }
}
