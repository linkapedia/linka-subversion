// MainTest

import com.iw.system.*;

import java.util.*;
import java.io.*;

public class MainTest {
    public static void main(String args[]) throws Exception {
        System.out.println("This program will test the API functions used for the Saffron-Indraweb integration.\n");

        // for this test, specify a text file to represent a piece of unstructured data
        File f = new File("C:\\data\\nutrition.txt");
        Vector classifyResults = new Vector();
        StringBuffer sb = new StringBuffer();

        // Step 1. Initialize
        ProcinQ server = new ProcinQ();
        server.setAPI("66.134.131.36");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("saffrondemo", "saffrondemo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        // Step 3. Classify a piece of unstructured data against the database.
        //   The first parameter is the plain text String containing the unstructured data.
        //   The second parameter represents the list of taxonomies (by ID) that the data is to
        //       be classified against.  Multiple corpora should be separated by commas.  To
        //       retrieve the list of available corpora, use the server.getCorpora() wrapper function.
        //   The third parameter (optional) is the document title.  This will assist in classification.
        //   The fourth parameter (optional) is true only if node-doc gists are requested (default is false)
        System.out.println("Attempting to classify: "+f.getAbsolutePath());
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { sb.append(record); }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }

        try { classifyResults = server.classify(sb.toString(), "32,41,9,55,11,18,57,49,12,27,44"); }
        catch (SessionExpired se) {
            System.err.println("Session expired."); return; }
        catch (ClassifyError ce) {
            System.err.println("Classification failed.");
            ce.printStackTrace(System.err); return;
        }
        if (classifyResults.size() == 0) { System.err.println("Classification returned no results."); return; }
        System.out.println("Classification complete.\n");

        // Step 4. Loop through the classification results and print detailed concept information
        for (int i = 0; i < classifyResults.size(); i++) {
            NodeDocument nd = (NodeDocument) classifyResults.elementAt(i);

            // customer note: attributes of the ND object are dynamic.  See API for a base set of attributes.
            //                additional attributes can be added simply by adding fields to the database table.
            System.out.println("Result: "+nd.get("NODETITLE"));

            // finally, this function will give you the context of the concepts that were returned
            Vector nodeTree = server.getNodeTree(nd.get("NODEID"));

            for (int j = nodeTree.size()-1; j > 0; j--) {
                if (j == nodeTree.size()-1) System.out.print("   ");
                else System.out.print(" > ");

                Node n = (Node) nodeTree.elementAt(j);
                System.out.print(n.get("NODETITLE"));
            }

            System.out.println("\n");
        }
    }
}