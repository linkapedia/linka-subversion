// MainTest

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;
import java.net.*;

public class Chinese {

    public static void main(String args[]) throws Exception {
        System.out.println("This program will convert the Clarke corpus into Chinese.\n");

        // Step 1. Initialize
        ProcinQ server = new ProcinQ();
        server.setAPI("192.168.0.223");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = 48 AND PARENTID > 0", 1, 5000);

        for (int i = 0; i < vNodes.size(); i++) {
            Node n = (Node) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);

            System.out.println("** OLD ** NodeID: "+n.get("NODEID")+" Title: "+n.get("NODETITLE")+" Parent: "+n.get("PARENTID")+" Sigs: "+vSignatures.size());

            int nodeID = Integer.parseInt(n.get("NODEID"));
            int parentID = Integer.parseInt(n.get("PARENTID"));

            String translate = n.get("NODETITLE");
            if (true) translate = translate(n.get("NODETITLE"));

            // alter this node to the new specifications
            n.set("CORPUSID", "63");
            n.set("NODEID", (nodeID + 75000) + "");
            n.set("LINKNODEID", (nodeID + 75000) + "");
            n.set("PARENTID", (parentID + 75000) + "");
            n.set("NODETITLE", translate);

            if (parentID == 533246) {
                n.set("PARENTID", "608658");
            }

            System.out.println("** NEW ** NodeID: " + n.get("NODEID") + " Title: " + translate + " Parent: " + n.get("PARENTID"));
            server.addNode(n);

            // now translate each signature
            for (int j = 0; j < vSignatures.size(); j++) {
                Signature s = (Signature) vSignatures.elementAt(j);
                System.out.println("  ** OLD SIGNATURE ** " + s.getWord());
                s.setWord(translate(s.getWord()));
                System.out.println("  ** NEW SIGNATURE ** " + s.getWord());
                vSignatures.set(j, s);
            }

            if (vSignatures.size() > 0) server.saveSignatures(n.get("NODEID"), vSignatures, false);

            System.out.println(" ");
        }
    }

    public static String translate (String nodeTitle) {
        long lStart = System.currentTimeMillis();

        // Make a socket connection to the server
        HttpURLConnection httpURLConn = null;
        DataOutputStream outStream;
        DataInputStream inStream;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        FileInputStream fileInputStream = null;

        try {
            URL theURL = getURLPost();

            httpURLConn = (HttpURLConnection) theURL.openConnection();
            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setRequestProperty("Content-Encoding","UTF-8");
            httpURLConn.setRequestProperty("Accept-Charset","UTF-8");
            httpURLConn.setDoInput(true);
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setDefaultUseCaches(false);
            httpURLConn.setAllowUserInteraction(false);

            httpURLConn.setDefaultUseCaches(false);
            OutputStream oStream = httpURLConn.getOutputStream();

            int loop = 0;

            Hashtable htArguments = new Hashtable();
            htArguments.put("q", nodeTitle);
            htArguments.put("kls", "0");
            htArguments.put("ienc", "utf8");
            htArguments.put("doit", "done");
            htArguments.put("intl", "1");
            htArguments.put("tt", "urltext");
            htArguments.put("trtext", nodeTitle);
            //htArguments.put("lp", "en_zt"); // CHINESE
            //htArguments.put("lp", "en_ko");   // KOREAN
            htArguments.put("lp", "en_ru"); // RUSSIAN
            htArguments.put("Submit", "Translate");

            Enumeration e = htArguments.keys();

            // write POST arguments in call to the taxonomy server
            while (e.hasMoreElements()) {
                String sKey = (String) e.nextElement();
                String sValue = (String) htArguments.get(sKey);

                loop++;
                String parameterString = sKey + "=" + sValue;
                if (loop > 0) parameterString = "&"+parameterString;
                oStream.write(parameterString.getBytes("UTF-8"));
                oStream.flush();
            }

            oStream.flush();
            oStream.close();

            // *** READ THE RESPONSE ***
            String document = streamToString(new DataInputStream(httpURLConn.getInputStream()));

            //OutputStreamWriter out2 = new OutputStreamWriter(new FileOutputStream("C:\\temp\\wacky.html", false), "UTF-8");
            //out2.write(document); out2.close();

            Pattern r = Pattern.compile("<td bgcolor=white class=s><div style=padding:10px;>(.*?)</div></td>", Pattern.CASE_INSENSITIVE);
            Matcher m = r.matcher(document);
            if (m.find()) {
                String s = m.group(1).toString();
                return s;
            }

            System.out.println("** ERROR ** could not find suitable replacement for "+nodeTitle);

            return nodeTitle;

        } catch (Exception except) {
            except.printStackTrace();
        } finally {
            if (httpURLConn != null) { httpURLConn.disconnect(); httpURLConn = null; }
        }
        return nodeTitle;
    }

    // This should really use a StringWriter or something similar and
    // simpler to capture the response message.  But for now,
    // something quick and dirty.
    public static String streamToString(DataInputStream in) throws IOException {
        InputStreamReader isr = new InputStreamReader(in, "UTF-8");
        StringBuffer sb = new StringBuffer();
        int i = 0;

        i = isr.read();

        while (i >= 0) {
            sb = sb.append((char) i);
            i = isr.read();
        }

        return sb.toString();
    }

    private static URL getURLPost () throws Exception {
        String sURL = "http://babelfish.altavista.com/tr";
        URL myURL = new URL(sURL);

        return myURL;

    }
}