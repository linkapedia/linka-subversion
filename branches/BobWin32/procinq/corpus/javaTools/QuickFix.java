// MainTest

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

public class QuickFix {
    public static Vector files = new Vector();

    public static void main(String args[]) throws Exception {
        System.out.println("This program will fix the Benders configuration.\n");

        // for this test, specify a text file to represent a piece of unstructured data
        Vector classifyResults = new Vector();
        Hashtable htArgs = getArgHash(args);

        if ((!htArgs.containsKey("api")) || (!htArgs.containsKey("location"))) {
            System.err.println("Usage: QuickFix -api <server> -location <location>"); return;
        }

        // Step 1. Initialize
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArgs.get("api"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        for (int i = 498; i < 523; i++) {
            File f = new File("C:/data/benders/benders/001/003/"+i);

            int depth = 2;
            String nodeTitle = parseTitle(f);

            Node n = createNode("3121", "100", depth+"", nodeTitle, nodeTitle, "4");
            n = server.addNode(n);

            try {
                String s = getCNFData(f);
                // no longer used if (s.length() > 200)
                server.setNodeSource(n, s);
            } catch (Exception e) { e.printStackTrace(System.out); }

            System.out.println("Title: "+nodeTitle+" File: "+f.getAbsolutePath()+" Node ID: "+n.get("NODEID"));
        }
    }

    public static void getFiles(String Location, String FilePaths[]) {
        for (int i = 0; i < FilePaths.length; i++) {
            File f = new File(Location + "/" + FilePaths[i]);

            // if this is a directory and recurse is true, continue collecting
            if (f.isDirectory()) {
                String FilePaths2[] = f.list();

                if (FilePaths2 != null) {
                    getFiles(Location + "/" + FilePaths[i], FilePaths2);
                }
            }

            if (!f.isDirectory() && (!f.getName().equals(".file"))) {
                files.add(f);
            }
        }
    }

	// GetArgHash takes command line arguments and parses them into a Hash structure
	public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}

    private static class ClassifierThread extends Thread {
        ProcinQ server = null;

        public ClassifierThread(ProcinQ server) {
            super();
            this.server = server;
        }

        public void run() {
            System.out.println("Entering the RUN method..");

            Vector classifyResults = new Vector();
            File f = null;

            while (true) {
                synchronized (files) {
                    if (files.size() < 1) return;

                    f = (File) files.remove(0);
                }

                try {
                    classifyResults = server.classify(f, false, "5,16,17,18,21,22,100003,100013,100045,200004");
                } catch (SessionExpired se) {
                    System.err.println("Session expired.");
                    return;
                } catch (ClassifyError ce) {
                    System.err.println("Classification failed.");
                    ce.printStackTrace(System.err);
                    return;
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    return;
                }

                if (classifyResults.size() == 0) {
                    System.out.println("File (" + f.getAbsolutePath() + " returned no results.");
                } else {
                    System.out.println("File (" + f.getAbsolutePath() + " returned " + classifyResults.size() + " results.");
                }
            }
        }
    }

    // Given a directory in CNF, grab the 000.html file from underneath and parse out the title
    public static String parseTitle(File f) throws Exception {
        File CNFfile = new File(f.getAbsolutePath()+"/000.html");
        boolean b = false;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(CNFfile);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            Pattern r = Pattern.compile("<TITLE>(.*)</TITLE>", Pattern.CASE_INSENSITIVE);
            Pattern r2 = Pattern.compile("<TITLE>", Pattern.CASE_INSENSITIVE);
            Matcher m = null; Matcher m2 = null;
            while ((sData = buf.readLine()) != null) {
                if (b) return sData;

                m = r.matcher(sData); m2 = r2.matcher(sData);
                if (m.find()) return (String) m.group(1).toString();
                if (m2.find()) b = true;
            }

            System.err.println("Warning: No topic title found for "+CNFfile.getAbsolutePath());

            return "Untitled Topic";
        } catch (Exception e) { throw e; }
        finally { fis.close(); fis = null; }
    }

    // get a stream of text from a file.  if this document is not .txt, filter it first.
    public static String getCNFData(File f) throws Exception {
        boolean bUsingTempFile = false;
        File tempFile = new File(f.getAbsolutePath()+"/000.html");

         // if the file is of type text, it needs an HTML wrapper before being posted
         try { tempFile = HTMLtoTXT(tempFile); }
         catch (Exception e) { e.printStackTrace(System.out); return ""; }

        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(tempFile);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData +"\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (fis != null) { fis.close(); fis.close(); }
        }

        return sOut;
    }

    public static File HTMLtoTXT(File HTMLfile) throws Exception {
        File tempFile = new File("temp.txt");
        FileReader in = new FileReader(HTMLfile);

        HTMLtoTEXT parser = new HTMLtoTEXT();
        parser.parse(in);
        in.close();

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.print(parser.getText());
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }

        return tempFile;
    }

    public static Node createNode(String ParentID, String NodeSize, String DepthFromRoot,
                                         String NodeDesc, String NodeTitle, String CorpusID) {
        Node n = new Node();

        n.set("PARENTID", ParentID);
        n.set("NODESIZE", NodeSize);
        n.set("DEPTHFROMROOT", DepthFromRoot);
        n.set("NODEDESC", NodeDesc);
        n.set("NODETITLE", NodeTitle);
        n.set("CORPUSID", CorpusID);

        return n;
    }

    public static Node createNode(Corpus c) {
        Node n = new Node();

        n.set("PARENTID", "-1");
        n.set("NODESIZE", "100");
        n.set("DEPTHFROMROOT", "0");
        n.set("NODEDESC", c.getName());
        n.set("NODETITLE", c.getName());
        n.set("CORPUSID", c.getID());

        return n;
    }
}