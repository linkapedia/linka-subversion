// MainTest

import com.iw.system.*;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.net.*;

public class FixGSM {
    public static void main(String args[]) throws Exception {
        //System.out.println("This program will create a topic map in Inxight's expected format.\n");

        // Step 1. Initialize
        ProcinQ server = new ProcinQ();
        server.setAPI("127.0.0.1");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        for (int j = 19000; j < 30000; j = j + 500) {

            Vector vDocuments = server.CQL("SELECT <DOCUMENT> WHERE TROUBLESTATUS = 1", j, j+500);

            // for each document, load the real document from disk and parse out the date and title
            for (int i = 0; i < vDocuments.size(); i++) {
                boolean bNewTitle = false; boolean bNewDate = false;

                Document d = (Document) vDocuments.elementAt(i);
                String filepath = (String) d.get("FULLTEXT");
                //filepath = "Z:\\"+filepath.substring(17);

                File f = new File(filepath);
                if (!f.exists()) System.out.println(filepath+" : filename does not exist!");

                /* ******************************** read in file information */
                FileInputStream fis = null;
                StringBuffer sb = new StringBuffer();
                try {
                    fis = new FileInputStream(f);
                    BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
                    String sData = new String();

                    while ((sData = buf.readLine()) != null) { sb.append(sData.replaceAll("\n", "")); }
                } catch (Exception e) {
                    System.out.println("Could not open file: "+f.getAbsolutePath());
                } finally {
                    if (fis != null) { fis.close(); fis.close(); fis = null; }
                }
                /* ********************************************************** */

                Pattern r = Pattern.compile("<TITLE>(.*?)</TITLE>", Pattern.CASE_INSENSITIVE);
                Matcher m = r.matcher(sb.toString());

                if (m.find()) { d.set("DOCTITLE", m.group(1).toString()); bNewTitle = true; }
                //else System.out.println(filepath+" : could not extract a title.");

                Pattern r2 = Pattern.compile("([0-1][0-9]-[0-3][0-9]-[0-2][0-9][0-9][0-9])", Pattern.CASE_INSENSITIVE);
                Matcher m2 = r2.matcher(sb.toString());

                if (m2.find()) {
                    d.set("DATELASTFOUND", m2.group(1).toString()); bNewDate = true;
                    //System.out.println(filepath+" : found date! "+d.get("DATELASTFOUND"));
                }
                else {

                    r2 = Pattern.compile("([0-1][0-9]-[0-3][0-9]-[0-9][0-9])", Pattern.CASE_INSENSITIVE);
                    m2 = r2.matcher(sb.toString());

                    if (m2.find()) {
                        d.set("DATELASTFOUND", m2.group(1).toString()); bNewDate = true;
                        //System.out.println(filepath+" : found date! "+d.get("DATELASTFOUND"));
                    }
                }

                if (bNewTitle && bNewDate) server.editDocument(d, fixString(d.get("DOCTITLE")), d.get("DATELASTFOUND"));
                else if (bNewTitle) server.editDocument(d, fixString(d.get("DOCTITLE")));

                if ((i % 25) == 0) System.out.println((i+j)+" documents processed of 27666.");

                r = null; m = null; r2 = null; m2 = null;
                d = null; filepath = null; sb = null; f = null;
            }

        }

    }

    public static String fixString (String s) {
        s = s.replaceAll("'", "''");
        //s = s.replaceAll("&", "&'||'");

        return s;
    }
}