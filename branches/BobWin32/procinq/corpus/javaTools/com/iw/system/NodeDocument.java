package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import org.dom4j.Element;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Iterator;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class NodeDocument extends Node implements Comparable {
    // Combined attributes
    public Hashtable htnd = new Hashtable();

    // constructor(s)
    public NodeDocument(HashTree ht) {
        htnd = new Hashtable();

        Enumeration eN = ht.keys();
        while (eN.hasMoreElements()) {
            String key = (String) eN.nextElement();
            String val = (String) ht.get(key);

            htnd.put(key, val);
        }
    }

    public NodeDocument (Iterator i) {
        if (htnd == null) htnd = new Hashtable();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            try { htnd.put(e.getName(), e.getText()); }
            catch (Exception ex) { ex.printStackTrace(System.out); }
        }
    }

    public NodeDocument(ResultSet rs) throws Exception {
        ResultSetMetaData rsmd = rs.getMetaData();

        for (int i = 1; i <= rsmd.getColumnCount(); i++)
            if (rs.getString(i) != null)
                htnd.put(rsmd.getColumnName(i), rs.getString(i));
            else htnd.put(rsmd.getColumnName(i), "");    }

    public boolean emitXML(PrintWriter out, boolean WithDocWrap) {
        boolean bAuthorized = true;

        if (bAuthorized) {
            if (WithDocWrap) { out.println("<NODEDOCUMENT>"); }

            if ( htnd != null ) {
                Enumeration enumCustomColnames = htnd.keys();
                while ( enumCustomColnames.hasMoreElements() )
                {
                    String sColName = (String) enumCustomColnames.nextElement();
                    try { out.println("   <"+sColName+"><![CDATA[" + new String(get(sColName).getBytes("UTF-8"))+ "]]></"+sColName+">"); }
                    catch (Exception e) { e.printStackTrace(System.out); }
                }
            }

            if (WithDocWrap) { out.println("</NODEDOCUMENT>"); }

            return true;
        }
        return false;
    }

    // accessor function -- 5/13/2004 only one accessor function required
    public String get(String fieldName) {
        String s = (String) htnd.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }
        return s.replaceAll(" <br> ", "\n");
    }
    public void set(String fieldName, String fieldValue) { htnd.put(fieldName, fieldValue); }

    // accessor functions with encoding
    public String getEncodedField(String fieldName) throws Exception {
        return URLEncoder.encode(get(fieldName), "UTF-8"); }

    public String getDecodedField(String fieldName) throws Exception {
        return URLDecoder.decode(get(fieldName), "UTF-8"); }

    public String toString() {
        String s = get("NODETITLE"); if (s == null) return super.toString();
        return s;
    }
    public int compareTo(Object o) {
        String s = get("NODETITLE"); if (s == null) return super.toString().compareTo(o.toString());
        return s.compareTo(o.toString());
    }
}
