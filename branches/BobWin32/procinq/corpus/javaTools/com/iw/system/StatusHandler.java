package com.iw.system;

import java.io.IOException;

import org.xml.sax.*;
import org.dom4j.*;

public class StatusHandler
        implements ElementHandler {

    public final static int SESSION_EXPIRED = 1;
    public final static int DATA_ERROR = 2;

    public int Status = -1;

    public StatusHandler () { super(); }

    public void onStart(ElementPath ep) {
    }

    public void onEnd(ElementPath ep) {
        if (ep.getPath().endsWith("SESSIONEXPIRED")) Status = SESSION_EXPIRED;
        if (ep.getPath().endsWith("MISSINGSKEY")) Status = SESSION_EXPIRED;
        if (ep.getPath().endsWith("DATAERROR")) Status = DATA_ERROR;
    }

}

