// MainTest

import com.iw.system.*;

import java.util.*;
import java.io.*;
import java.net.*;

public class XtmOut {
    public static int corpusID = 41;
    public static Node rootNode = null;
    public static Hashtable commonWords = new Hashtable();

    public static void main(String args[]) throws Exception {
        //System.out.println("This program will create a topic map in Inxight's expected format.\n");

        // Step 1. Initialize
        ProcinQ server = new ProcinQ();
        server.setAPI("192.168.0.223");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file commonwords.txt");
            return;
        }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        FileOutputStream fos = new FileOutputStream(new File("prolif-wmd.xtm"));
        Writer out = new OutputStreamWriter(fos, "UTF8");

        System.out.println("Writing out the header ...");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<topicMap id=\"prolifwmd\" xmlns=\"http://www.topicmaps.org/xtm/1.0/\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n");

        System.out.println("Selecting topics within the taxonomy ...");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" ORDER BY NODEID ASC", 1, 100000);

        System.out.println("Writing out the base objects ... ");

        out.write("<topic id=\"child\">\n");
        out.write("   <baseName id=\"child\">\n");
        out.write("      <baseNameString>Child</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"parent\">\n");
        out.write("   <baseName id=\"parent\">\n");
        out.write("      <baseNameString>Parent</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"child-of\">\n");
        out.write("   <baseName id=\"child-of5\">\n");
        out.write("      <baseNameString>Has Child</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("   <baseName id=\"parent-scope\">\n");
        out.write("      <scope>\n");
        out.write("         <topicRef xlink:href=\"#parent\" />\n");
        out.write("      </scope>\n");
        out.write("      <baseNameString>Has Child</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>");

        Hashtable ht = new Hashtable();

        System.out.println("Writing out the topics ... ");

        for (int i = 0; i < vNodes.size(); i++) {
            // *** WRITE NODE INFORMATION HERE
            Node n = (Node) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);

            writeNode(n, signatures, out);
            ht.put(n.get("NODEID"), n.get("NODETITLE"));
            System.out.print(".");
        }

        System.out.println(" ");

        for (int i = 0; i < vNodes.size(); i++) {
            // *** WRITE NODE RELATIONSHIP INFORMATION HERE
            Node n = (Node) vNodes.elementAt(i);
            out.write("<association id=\""+n.get("NODEID")+"-"+n.get("PARENTID")+"\">\n");
            out.write("   <instanceOf>\n");
            out.write("      <topicRef xlink:href=\"#child-of\" />\n");
            out.write("   </instanceOf>\n");
            out.write("   <member>\n");
            out.write("      <roleSpec>\n");
            out.write("         <topicRef xlink:href=\"#child\" />\n");
            out.write("      </roleSpec>\n");
            out.write("      <topicRef xlink:href=\"#"+generateID(n)+"\" />\n");
            out.write("   </member>\n");
            out.write("   <member>\n");
            out.write("      <roleSpec>\n");
            out.write("         <topicRef xlink:href=\"#parent\" />\n");
            out.write("      </roleSpec>\n");
            out.write("      <topicRef xlink:href=\"#"+generateID(n.get("PARENTID"), (String) ht.get(n.get("PARENTID")))+"\" />\n");
            out.write("   </member>\n");
            out.write("</association>\n");

            System.out.print(".");
        }

        System.out.println(" ");
        out.write("</topicMap>\n");

        out.close();
    }

    public static void writeNode(Node n, Signatures s, Writer out) throws IOException {
        out.write("<topic id=\""+generateID(n)+"\">\n");
        out.write("   <instanceOf><topicRef xlink:href=\"#node\" /></instanceOf>\n");
        out.write("   <baseName> <baseNameString>"+n.get("NODETITLE")+"</baseNameString> </baseName>\n");
        out.write("   <occurrence>\n");
        out.write("      <resourceData id=\"NodeSize\">"+n.get("NODESIZE")+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("   <occurrence>\n");
        out.write("      <resourceData id=\"ParentId\">"+n.get("PARENTID")+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("   <occurrence>\n");
        out.write("      <resourceData id=\"NodeIndexWithinParent\">"+n.get("NODEINDEXWITHINPARENT")+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("   <occurrence>\n");
        out.write("      <resourceData id=\"DepthFromRoot\">"+n.get("DEPTHFROMROOT")+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("   <occurrence>\n");
        out.write("      <resourceData id=\"Signatures\">"+s.getTopicMapString()+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("</topic>\n");
    }

    public static String generateID(Node n) {
        return generateID((String) n.get("NODEID"), (String) n.get("NODETITLE"));
    }
    public static String generateID(String NodeID, String NodeTitle) {
        StringBuffer sb = new StringBuffer(NodeTitle+"-"+NodeID);

        for (int i = 0; i < sb.length(); i++) {
            //System.out.println("i: "+i+" (of "+sb.length()+")");
            if (!Character.isLetterOrDigit(sb.charAt(i)))
                sb = sb.replace(i, i+1, "-");
        }

        return sb.toString();
    }
}