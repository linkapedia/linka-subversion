import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class Gale
{
    public static int nodeID = -1;
    public static int rootID = -1;
    public static int corpusID = -1;

    public static Hashtable htAlpha = new Hashtable();
    public static Hashtable htArguments = new Hashtable();

    // main function
    public static void main(String[] args) {
        htArguments = getArgHash(args);

        if ((!htArguments.containsKey("corpusid")) ||
            (!htArguments.containsKey("nodeid")) ||
            (!htArguments.containsKey("rootid")) ||
            (!htArguments.containsKey("file"))) {

            System.err.println("Usage: Gale -file filename -corpusid corpus -nodeid node -rootid root");
            return;
        }

        nodeID = new Integer((String) htArguments.get("nodeid")).intValue();
        rootID = new Integer((String) htArguments.get("rootid")).intValue();
        corpusID = new Integer((String) htArguments.get("corpusid")).intValue();

        // load in the comma separated file
        File f = new File((String) htArguments.get("file"));
        if (!f.exists()) { System.err.println("File: "+(String)htArguments.get("file")+" does not exist."); return; }

        FileInputStream fis = null;

        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        try {
            Connection connection = DriverManager.getConnection(
                        "jdbc:oracle:thin:@192.168.0.223:1521:content",
                        "sbooks", // ## fill in User here
                        "racer9" // ## fill in Password here
                );

            createAlphabeticHierarchy(connection);

            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            // read through the text file
            // $$ designates a new record
            // TE designates a depth level 2 topic
            // TX <b>(.*?)</b> designates a depth level 3 topic
            // TX <LI>(.*?) or TX <UL><LI>(.*?) designates a depth level 4 topic

            boolean bTX1 = false; boolean bTX2 = false; boolean bTX0 = false; int niwp = 0;
            String sTX1 = ""; String sTX2 = ""; String sTX0 = "";
            String pTX1 = ""; String pTX2 = ""; String pTX3 = "";

            int totalRecords = 0;

            buf.readLine();
            while ((sData = buf.readLine()) != null) {
                if (sData.startsWith("$$")) { // ***************** END OF RECORD
                    totalRecords++; System.out.println("Writing record number "+totalRecords);

                    if (bTX2) {
                        FixBlanks.insertClobToNodeData(Long.parseLong(pTX3), stringHTMLtoText(sTX2), connection);
                        sTX2 = ""; pTX3 = ""; bTX2 = false;
                    }

                    // if a TX1 is open, write out node source and close it
                    if (bTX1) {
                        FixBlanks.insertClobToNodeData(Long.parseLong(pTX2), stringHTMLtoText(sTX1), connection);
                        sTX1 = ""; pTX2 = ""; bTX1 = false;
                    }
                } else if (sData.startsWith("TI")) { // ***************** CREATE RECORD LEVEL 2
                    // if a TX2 is open, write out node source and close it
                    if (bTX2) {
                        FixBlanks.insertClobToNodeData(Long.parseLong(pTX3), stringHTMLtoText(sTX2), connection);
                        sTX2 = ""; pTX3 = ""; bTX2 = false;
                    }

                    // if a TX1 is open, write out node source and close it
                    if (bTX1) {
                        FixBlanks.insertClobToNodeData(Long.parseLong(pTX2), stringHTMLtoText(sTX1), connection);
                        sTX1 = ""; pTX2 = ""; bTX1 = false;
                    }

                    if (!sTX0.equals("")) {
                        FixBlanks.insertClobToNodeData(Long.parseLong(pTX1), stringHTMLtoText(sTX0), connection);
                        sTX0 = ""; sTX1 = ""; pTX2 = "";
                    }

                    String[] fields = new String[5]; String sText = "";

                    Pattern r = Pattern.compile("TI (.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    if (m.find()) sText = sText + m.group(1).toString();

                    if (!sText.equals("")) {
                        nodeID++; niwp++; String parentID = (String) htAlpha.get("Others");
                        if (htAlpha.containsKey(sText.substring(0, 1).toUpperCase()))
                            parentID = (String) htAlpha.get(sText.substring(0, 1).toUpperCase());

                        fields[0] = nodeID+"";
                        fields[1] = parentID;
                        fields[2] = "2";
                        fields[3] = niwp+"";
                        fields[4] = sText;
                        insertNode(fields, connection);

                        pTX1 = nodeID+""; System.out.println("   Section title: "+sText);
                    } else { System.out.println("Error: Could not extract TE node title for "+sData); }

                } else if (sData.toUpperCase().startsWith("TX <B>")) { // ***************** CREATE RECORD LEVEL 3
                    // if a TX1 is open, write out node source and close it
                    if (bTX1) {
                        FixBlanks.insertClobToNodeData(Long.parseLong(pTX2), stringHTMLtoText(sTX1), connection);
                        sTX1 = ""; pTX2 = ""; bTX1 = false;
                    }
                    // if a TX2 is open, write out node source and close it
                    if (bTX2) {
                        FixBlanks.insertClobToNodeData(Long.parseLong(pTX3), stringHTMLtoText(sTX2), connection);
                        sTX2 = ""; pTX3 = ""; bTX2 = false;
                    }

                    String[] fields = new String[5]; String sText = "";

                    Pattern r = Pattern.compile("TX <B.(.*)</B>", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    if (m.find()) sText = sText + m.group(1).toString();

                    if (!sText.equals("")) {
                        nodeID++; niwp++;

                        fields[0] = nodeID+"";
                        fields[1] = pTX1;
                        fields[2] = "3";
                        fields[3] = niwp+"";
                        fields[4] = sText;
                        insertNode(fields, connection);

                        pTX2 = nodeID+""; bTX1 = true; System.out.println("      SubSection title: "+sText);
                    } else { System.out.println("Error: Could not extract TX <B> node title for "+sData); }

                } else if (bTX2) {
                    if (sData.length() > 2) sTX2 = sTX2 +" "+ sData.substring(3);
                } else if (bTX1) {
                    if (sData.length() > 2) sTX1 = sTX1 +" "+ sData.substring(3);
                } else if (bTX0 && (sData.toUpperCase().startsWith("   "))) {
                    if (sData.length() > 2) sTX0 = sTX0 +" "+ sData.substring(3);
                } else if (sData.toUpperCase().startsWith("TX ")) {
                    if (sData.length() > 2) sTX0 = sTX0 +" "+ sData.substring(3);
                    bTX0 = true;
                } else { bTX0 = false; }

                //FixBlanks.insertClobToNodeData(Long.parseLong(fields[0]), stringHTMLtoText(getTop(data)), connection);
            }

            fis.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace(System.out); return;
        }

        fis = null;
    }

    // create A-Z plus Others
    private static void createAlphabeticHierarchy(Connection connection) {
        String[] fields = new String[5];

        int niwp = 1;

        // insert A through Z
        for (int i = 65; i < 91; i++) {
            nodeID++;

            fields[0] = nodeID+"";
            fields[1] = rootID+"";
            fields[2] = "1";
            fields[3] = niwp+"";
            fields[4] = ((char)i)+"";
            try { insertNode(fields, connection); }
            catch (Exception e) { System.out.println("Error inserting nodeID "+nodeID+" title: "+fields[4]); }

            htAlpha.put(fields[4], nodeID+"");
        }

        nodeID++;

        // insert other
        fields[0] = nodeID+"";
        fields[1] = rootID+"";
        fields[2] = "1";
        fields[3] = niwp+"";
        fields[4] = "Others";
        try { insertNode(fields, connection); }
        catch (Exception e) { System.out.println("Error inserting nodeID "+nodeID+" title: "+fields[4]); }

        htAlpha.put(fields[4], nodeID+"");
    }

    private static String stringHTMLtoText(String s) {
        HTMLtoTEXT parser = new HTMLtoTEXT();

        try { parser.parse(new StringReader(s)); }
        catch (Exception e) {
            //e.printStackTrace(System.out);
            String t = s.replaceAll("<(.*?)>", " ");
            return t;
        }
        return parser.getText();
    }

    private static void insertNode (String[] fields, Connection connection) throws Exception {
        Statement stmt = null;

        try {
            String query = "insert into node values ("+fields[0]+","+corpusID+",'"+fields[4].replaceAll("'", "''")+"',100,"+
                fields[1]+","+fields[3]+","+fields[2]+",sysdate,sysdate,1,null,sysdate,'"+fields[4].replaceAll("'", "''")+"',"+
                fields[0]+")";

            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally { stmt.close(); stmt = null; }
    }


    public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}
