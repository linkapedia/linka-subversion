import java.io.*;
import java.util.*;
import java.sql.*;
import java.security.*;

import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;

public class SSLtest
{
	// Object constructor -- draw defaults from configs
	public static void main(String[] args) {
        Hashtable hashtableEnvironment = new Hashtable();
        hashtableEnvironment.put(
                Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory"
        );

        String keystore = "C:/test/schering.keystore";
        System.setProperty("javax.net.ssl.trustStore", keystore);
        System.setProperty("javax.net.ssl.keyStore", keystore);
        System.setProperty("javax.net.ssl.keyStorePassword", "Initial1");

        hashtableEnvironment.put(
                Context.SECURITY_PROTOCOL,
                "ssl"
        );

        hashtableEnvironment.put(Context.SECURITY_AUTHENTICATION, "simple");
        hashtableEnvironment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        hashtableEnvironment.put(Context.PROVIDER_URL, "ldap://149.234.5.162:50001/");
        hashtableEnvironment.put(Context.SECURITY_PRINCIPAL, "CN=_BEAskOnceBind,OU=AskOnce,OU=Applications,DC=Schering,DC=Net");
        hashtableEnvironment.put(Context.SECURITY_CREDENTIALS, "Initial1");

        try {
            Context ct = new InitialContext(hashtableEnvironment);
            ct.close();
            DirContext ctx = new InitialDirContext(hashtableEnvironment);

            SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.OBJECT_SCOPE);

            String DN = "CN=_BEAskOnceBind,OU=AskOnce,OU=Applications,DC=Schering,DC=Net";
 			NamingEnumeration sre = ctx.search(DN, "(objectClass=Group)", constraints);

 			SearchResult sr = null;
            // if SRE is null, no results were found
            try { sr = (SearchResult) sre.next(); }
            catch (Exception e) { System.out.println("No groups found for user: "+DN); throw new Exception(e); }

			Attributes attrs = sr.getAttributes();

		    NamingEnumeration ae = attrs.getAll() ;

			while (ae.hasMore()) {
				Attribute attr = (Attribute)ae.next();
				if (attr.getID().equals("memberOf")) {
					NamingEnumeration ex = attr.getAll();
					while (ex.hasMore()) {
						String sDN = (String) ex.next();
                        System.out.println("Group: "+sDN);
					}
				}
			}
			ctx.close();

		} catch (Exception e) { e.printStackTrace(System.err); return; }
    }
}

