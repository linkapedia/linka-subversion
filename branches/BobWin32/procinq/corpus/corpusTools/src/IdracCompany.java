import com.iw.system.Node;

import java.sql.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

public class IdracCompany {

    public static Hashtable ht = new Hashtable();
    public static int nodeID = 2399853;

    public static void main(String[] args) {

        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        // load benchmark of the data from the Idrac database
        try {
            System.out.println("Opening database connection.");

            // open connection to database
            Connection connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@212.121.162.179:1521:idrac",
                    "sbooks", // ## fill in User here
                    "cardi" // ## fill in Password here
            );

            String query = "SELECT NODEID, NODETITLE FROM NODE WHERE CORPUSID = 37";
            Statement stmt = null; ResultSet rs = null;

            System.out.println("Selecting information from IDRAC database.");
            try {
                // execute query
                stmt = connection.createStatement();
                rs = stmt.executeQuery(query);

                while (rs.next()) {
                    Node n = new Node(rs);
                    ht.put(n.get("NODETITLE").toString().toUpperCase(), n);
                }
            } catch (java.sql.SQLException e) { throw e; }
            finally { rs.close(); stmt.close(); }

            // track current and existing parents
            Node[] hierarchy = new Node[6];
            int[] niwp = new int[6];
            for (int i = 0; i < niwp.length; i++) { niwp[i] = 0; }

            // load snapshot of the data from spreadsheet to compare and contrast
            System.out.println("Contrasting with spreadsheet information.\n");
            try {
                File f = new File("C:/data/TaxoCompany.txt");
                FileInputStream fis = null;

                fis = new FileInputStream(f);
                BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
                String sData = new String();

                buf.readLine();
                while ((sData = buf.readLine()) != null) {
                    String[] nodedata = sData.split("\t");
                    Node n;

                    for (int i = 0; i < nodedata.length; i++) {
                        System.out.println("... test: "+i);
                        if (!nodedata[i].equals("")) {
                            if (ht.containsKey(nodedata[i].toUpperCase())) {
                                n = (Node) ht.get(nodedata[i].toUpperCase());

                                if (i == 0) { n.set("PARENTID", "6"); }
                                else { n.set("PARENTID", ((Node) hierarchy[i-1]).get("NODEID")); }
                                n.set("DEPTHFROMROOT", (i+1)+"");
                                n.set("NODEINDEXWITHINPARENT", niwp[i]+"");

                                // update node object in the database here
                                System.out.println("Found: "+nodedata[i]+" (ID: "+n.get("NODEID")+", PAR: "+n.get("PARENTID")+", "+
                                   "DEPTH: "+n.get("DEPTHFROMROOT")+", NIWP: "+n.get("NODEINDEXWITHINPARENT")+")");
                            }
                            else {
                                // create node
                                n = new Node(); nodeID++; niwp[i]++;

                                // fill out the node object
                                n.set("NODETITLE", nodedata[i]);
                                n.set("NODEID", nodeID+"");
                                if (i == 0) { n.set("PARENTID", "6"); }
                                else { n.set("PARENTID", ((Node) hierarchy[i-1]).get("NODEID")); }
                                n.set("CLIENTID", "22");
                                n.set("NODESIZE", "100");
                                n.set("DEPTHFROMROOT", (i+1)+"");
                                n.set("NODEINDEXWITHINPARENT", niwp[i]+"");

                                ht.put(nodedata[i].toUpperCase(), n);
                                System.out.println("New: "+nodedata[i]+" (ID: "+n.get("NODEID")+", PAR: "+n.get("PARENTID")+", "+
                                   "DEPTH: "+n.get("DEPTHFROMROOT")+", NIWP: "+n.get("NODEINDEXWITHINPARENT")+")");
                            }
                            hierarchy[i] = n;
                            for (int j = i+1; j < niwp.length; j++) { niwp[j] = 0; }
                        }
                    }
                }
            } catch (Exception e) { System.err.println(e); }

            connection.close();

        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }
    }
}