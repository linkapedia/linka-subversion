import com.iw.system.Node;

import java.sql.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

public class IdracCompany2 {

    public static Hashtable ht = new Hashtable();
    public static int nodeID = 1000;
    public static String corpusID = "37";

    public static void main(String[] args) {

        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        // load benchmark of the data from the Idrac database
        try {
            System.out.println("Opening database connection.");

            // open connection to database
            Connection connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@212.121.162.179:1521:idrac",
                    "sbooks", // ## fill in User here
                    "cardi" // ## fill in Password here
            );

            /*
            Connection connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@192.168.0.223:1521:content",
                    "sbooks", // ## fill in User here
                    "racer9" // ## fill in Password here
            ); */

            // track current and existing parents
            Node[] hierarchy = new Node[6];
            int[] niwp = new int[6];
            for (int i = 0; i < niwp.length; i++) { niwp[i] = 0; }

            // load snapshot of the data from spreadsheet to compare and contrast
            System.out.println("Loading spreadsheet information.\n");
            try {
                File f = new File("C:/data/TaxoCompany.txt");
                FileInputStream fis = null;

                fis = new FileInputStream(f);
                BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
                String sData = new String();

                buf.readLine();
                while ((sData = buf.readLine()) != null) {
                    String[] nodedata = sData.split("\t");
                    Node n;

                    for (int i = 0; i < nodedata.length; i++) {
                        if (!nodedata[i].equals("")) {
                            if (ht.containsKey(nodedata[i].toUpperCase())) {
                                n = (Node) ht.get(nodedata[i].toUpperCase());

                                if (i == 0) { n.set("PARENTID", "1000"); }
                                else { n.set("PARENTID", ((Node) hierarchy[i-1]).get("NODEID")); }
                                n.set("DEPTHFROMROOT", (i+1)+"");
                                n.set("CORPUSID", corpusID);
                                n.set("NODEINDEXWITHINPARENT", niwp[i]+"");

                                // update node object in the database here
                                System.out.println("Found: "+nodedata[i]+" (ID: "+n.get("NODEID")+", PAR: "+n.get("PARENTID")+", "+
                                   "DEPTH: "+n.get("DEPTHFROMROOT")+", NIWP: "+n.get("NODEINDEXWITHINPARENT")+")");
                                insertNode(connection, n);

                            }
                            else {
                                // create node
                                n = new Node(); nodeID++; niwp[i]++;

                                // fill out the node object
                                n.set("NODETITLE", nodedata[i]);
                                n.set("NODEID", nodeID+"");
                                if (i == 0) { n.set("PARENTID", "1000"); }
                                else { n.set("PARENTID", ((Node) hierarchy[i-1]).get("NODEID")); }
                                n.set("CLIENTID", "22");
                                n.set("NODESIZE", "100");
                                n.set("CORPUSID", corpusID);
                                n.set("DEPTHFROMROOT", (i+1)+"");
                                n.set("NODEINDEXWITHINPARENT", niwp[i]+"");

                                ht.put(nodedata[i].toUpperCase(), n);
                                System.out.println("New: "+nodedata[i]+" (ID: "+n.get("NODEID")+", PAR: "+n.get("PARENTID")+", "+
                                   "DEPTH: "+n.get("DEPTHFROMROOT")+", NIWP: "+n.get("NODEINDEXWITHINPARENT")+")");
                                insertNode(connection, n);
                            }
                            hierarchy[i] = n;
                            for (int j = i+1; j < niwp.length; j++) { niwp[j] = 0; }
                        }
                    }
                }
            } catch (Exception e) { throw e; }

            connection.close();

        } catch (Exception e) {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
    }

    public static void insertNode(Connection connection, Node n) {
        String sNodeTitle = n.get("NODETITLE");
        sNodeTitle = sNodeTitle.replaceAll("'", "''");

        String query = "insert into node values ("+n.get("NODEID")+", "+corpusID+", 22, '"+sNodeTitle+"', 100, "+
             n.get("PARENTID")+", "+n.get("NODEINDEXWITHINPARENT")+", "+n.get("DEPTHFROMROOT")+","+
             "sysdate, sysdate, 1, null, sysdate)";
        System.out.println(query);

        Statement stmt = null;

        try {
            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            stmt = null;
        }
    }
}