#!/usr/bin/perl

## corpusin.pl
#
# Given a filename, input an XML version of the corpus 
#  using the Topic Maps XTM DTD

use DBI;
use Getopt::Long;

# Signal switches catch interrupts
#
# If one of these occur, make sure to erase all inserts made into the database
$SIG{INT} = \&cleanup;
$SIG{QUIT} = \&cleanup;
$SIG{STOP} = \&cleanup;
$SIG{HUP} = \&cleanup;
$SIG{TERM} = \&cleanup;

local ($filename) = "corpusout.xml";
local ($clientid) = 0;

$ENV{'PATH'} = "C:/Perl/bin/;C:/Oracle/Ora92/bin/";
$ENV{'ORACLE_HOME'} = "C:/Oracle/Ora92";

&GetOptions ("filename=s" => \$filename);

open (FILE, "$filename") || warn "Filename: $filename not found.\n";
local (@lines) = <FILE>; close(FILE);

local ($dbh) = DBI->connect("dbi:Oracle:perseus", "sbooks", "racer9");
#$dbh->{AutoCommit} = 0;

local ($cid, $cname, $cdesc, $r1, $r2, $r3, $r4, $r5, $r6);
local ($nid, $ntitle, $nsize, $clid, $pid, $niwp, $depth);
local ($signatures, $sigword, $sigfreq, $corpus);
local ($savedcorpus) = -1;
local ($nextline) = 0;

$nid = -1; $corpus = -1;
$cid = -1; $cdesc = "";

# Loop through each line to extract the XML data
foreach $line (@lines) {
    #######################
    #
    # Corpus related regex
    #
    #######################

    if ($nextline == 1) { 
	if ($line !~ /resourceData/) { 
	    $cdesc = $cdesc . $line; 
	} else { $nextline = 0; }
    }
    # id="300012" xmln
    if ($line =~ /<topicMap id=(.*?)>/) { $cid = $1; $savedcorpus = $cid; }
    if (($cid != -1) && ($line =~ /String>(.*)<\/baseNameString/))
    {
	$cname = $1;
    }

    if ($line =~ /<resourceData id=\"ROCF1\">(.*)<\/resourceData>/) { $r1 = $1; }
    if ($line =~ /<resourceData id=\"ROCF2\">(.*)<\/resourceData>/) { $r2 = $1; }
    if ($line =~ /<resourceData id=\"ROCF3\">(.*)<\/resourceData>/) { $r3 = $1; }
    if ($line =~ /<resourceData id=\"ROCC1\">(.*)<\/resourceData>/) { $r4 = $1; }
    if ($line =~ /<resourceData id=\"ROCC2\">(.*)<\/resourceData>/) { $r5 = $1; }
    if ($line =~ /<resourceData id=\"ROCC3\">(.*)<\/resourceData>/) { $r6 = $1; }
    if ($line =~ /<resourceData id=\"CorpusDesc\">/) { $nextline = 1; }
    
    if (($cid != -1) && ($line =~ /<\/occurence>/)) { 
	# Insert corpus into the database
	$cdesc =~ s/\n//gi; $cdesc =~ s/\'//gi;
	print "Inserting Corpus ID: $cid Name: $cname R1: $r1 R2: $r2 R3: $r3 R4: $r4 R5: $r5 R6: $r6 DESC: $cdesc \n";
	my ($q)="insert into corpus (corpusid, clientid, corpus_name, rocf1, rocf2, rocf3, rocc1, rocc2, rocc3, corpusdesc, corpusactive, corpusrunfrequency, corpuslastrun) values ($cid, 0, '$cname', $r1, $r2, $r3, $r4, $r5, $r6, '$cdesc', 1, 90, SYSDATE)";
	my ($sth) = $dbh->prepare($q) || die "Error: Cannot insert corpus, $q, $!\n";
        $sth->execute() || die "Error: Cannot insert corpus, $q, $!\n";
        $sth->finish();
        $cid = -1;
    } 

    #######################
    #
    # Node and Signature related regex
    #
    #######################

    if ($line =~ /<topic id=(.*?)>/) { $nid = $1; $nid =~ s/\"//gi; }
    if (($nid != -1) && ($line =~ /String>(.*)<\/baseNameString/))
    {
	$ntitle = $1;
    }
    if ($line =~ /<instanceOf>(.*)<\/instanceOf>/) { $corpus = $1; }
    if ($line =~ /<resourceData id=\"ClientId\">(.*)<\/resourceData>/) { $clid = $1; }
    if ($line =~ /<resourceData id=\"NodeSize\">(.*)<\/resourceData>/) { $nsize = $1; }
    if ($line =~ /<resourceData id=\"ParentId\">(.*)<\/resourceData>/) { $pid = $1; }
    if ($line =~ /<resourceData id=\"NodeIndexWithinRoot\">(.*)<\/resourceData>/) { $niwp = $1; }
    if ($line =~ /<resourceData id=\"DepthFromRoot\">(.*)<\/resourceData>/) { $depth = $1; }
    if ($line =~ /<resourceData id=\"Signatures\">(.*)<\/resourceData>/) {
	$signatures = $1; 
	if ($signatures =~ /CDATA\[(.*)\]/) { $signatures = $1; }
	$signatures =~ s/\]//gi;

	$ntitle =~ s/\'//gi;
	# Insert node into the database
        print "Inserting Node ID: $nid Title: $ntitle Corpus: $corpus Client: $clid Size: $nsize Parent: $pid Index: $niwp Depth: $depth\n";
	my ($q)="insert into node (nodeid, corpusid, clientid, nodetitle, nodesize, parentid, nodeindexwithinparent, depthfromroot) values ($nid, $corpus, $clid, '$ntitle', $nsize, $pid, $niwp, $depth)";
	my ($sth) = $dbh->prepare($q) || die "Error: Cannot insert node, $q, $!\n";
        $sth->execute() || die "Error: Cannot insert node, $q, $!\n";
        $sth->finish();
        my (@sigs) = split(/\,/,$signatures);
	foreach $sig (@sigs) {
	   ($sigword, $sigfreq) = split(/\|\|/, $sig);
	   # Insert sig into the database
	   $sigword =~ s/\'//gi;
	   $sigfreq =~ s/\'//gi;
	   if (($sigfreq ne "") && ($sigword ne "")) {
           print "Signature: Node: $nid Word: $sigword Freq: $sigfreq\n";
	   my ($q)="insert into signature (nodeid, signatureword, SIGNATUREOCCURENCES) values ($nid, '$sigword', $sigfreq)";
           my ($sth) = $dbh->prepare($q) || warn "Error: Cannot insert signature, $q, $!\n";
           $sth->execute() || warn "Error: Cannot insert signature, $q, $!\n";
           $sth->finish(); }
	}
	$nid = -1;
    }

}
$dbh->disconnect();
exit(1);

sub cleanup {
    print "Cleanup signal received, cleaning up the mess..\n";

    if ($savedcorpus != -1) {
	print "Deleting obsolete signature information..\n";
	local ($sth) = $dbh->prepare("delete from signature where nodeid in (select nodeid from node where corpusid = $savedcorpus)");
	$sth->execute || warn "Cannot cleanup signature information.\n";
	$sth->finish;
	print "Deleting obsolete topic information..\n";
        ($sth) = $dbh->prepare("delete from node where corpusid = $savedcorpus");
	$sth->execute || warn "Cannot cleanup node information.\n";
	$sth->finish;
	print "Deleting obsolete corpus information..\n";
	local ($sth) = $dbh->prepare("delete from corpus where corpusid = $savedcorpus");
	$sth->execute || warn "Cannot cleanup signature information.\n";
	$sth->finish;
    }

    print "Done.\n";
    $dbh->disconnect();
    exit(1);
}

sub die_error {
    my ($err_string) = $_[0];

    warn $err_string;
}

1;
