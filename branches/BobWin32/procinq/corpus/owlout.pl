#!/usr/bin/perl

## corpusout.pl
#
# Given a corpus identifier and filename, output an XML version of the corpus 
#  using the the OWL specification

use DBI;
use Getopt::Long;

local ($corpus) = "";
local ($filename) = "owlout.xtm";

&GetOptions ("corpus=i" => \$corpus,
	     "filename=s" => \$filename);

if ($corpus == "") { die "owlout.pl -corpus <corpus> -filename <filename>\n"; }

print "Writing schema information..\n";

open (TEMPLATE, "procinq.owl"); local (@template_lines) = <TEMPLATE>; close(TEMPLATE);
open (FILE, ">$filename");

foreach $line (@template_lines) { print FILE $line; } 

undef @template_lines;

local ($dbh) = DBI->connect("dbi:Oracle:medusa", "sbooks", "racer9");

print "Selecting corpus information from the database..\n";
local ($q) = "select corpusid, corpus_name, corpusdesc, corpusactive from corpus where corpusid = $corpus";
local ($sth) = $dbh->prepare($q) || &die_error ("Died during corpus selection: $q.\n");
$sth->execute || &die_error ("Died during corpus selection: $q.\n");
local ($cid, $cname, $cdesc, $active) = $sth->fetchrow_array();
$sth->finish();

print "Printing corpus information to XML file..\n";
print FILE "\n<Corpus rdf:ID=\"$cid\">\n";
print FILE "   <title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">$cname</title>\n";
print FILE "   <description rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">$cdesc</description>\n";
print FILE "   <active rdf:datatype=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\">$active</active>\n";
print FILE "</Corpus>\n\n";

print "Selecting node and signature information from the database..\n";
$q = "select nodeid, linknodeid, nodetitle, nodedesc, nodesize, parentid, ".
      "nodeindexwithinparent, depthfromroot, nodestatus from node where corpusid = $cid";
$sth = $dbh->prepare($q) || &die_error ("Died during node selection: $q.\n");
$sth->execute || &die_error ("Died during node selection: $q.\n");

print "Printing node and signature information to FILE...\n";
while (($nid, $linkid, $ntitle, $ndesc, $nsize, $pid, $niwp, $depth, $nstatus) = $sth->fetchrow_array()) {
    print FILE "<Node rdf:ID=\"$nid\">\n";
    print FILE "   <title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">$ntitle</title>\n";
    print FILE "   <description rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">$ndesc</description>\n";
    print FILE "   <size rdf:datatype=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\">$nsize</size>\n";
    print FILE "   <indexwithinparent rdf:datatype=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\">$niwp</indexwithinparent>\n";
    print FILE "   <depth rdf:datatype=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\">$depth</depth>\n";

    print FILE "   <hasCorpus rdf:resource=\"#$cid\" /> \n";

    if ($pid != -1) { print FILE "   <hasParent rdf:resource=\"#$pid\" />\n"; }
    if ($nid != $linkid) { print FILE "   <hasLink rdf:resource=\"#$linkid\" />\n" }

    if ($nstatus != 1) { 
       print FILE "   <active rdf:datatype=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\">0</active>\n";
    } else {
       print FILE "   <active rdf:datatype=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\">1</active>\n";
    }
    print FILE "</Node>\n\n";

    my ($sigsth) = $dbh->prepare("select signatureword, signatureoccurences from signature where nodeid = $nid") || &die_error("Died during signature selection: $q\n");
    $sigsth->execute || &die_error("Died during signature selection: $q\n");
    
    local ($signatures) = "";
    while (my($sigword, $sigocc) = $sigsth->fetchrow_array()) {
        print FILE "<Signature rdf:ID=\"".$sigword.$sigocc."\">\n";
        print FILE "   <title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">$sigword</title>\n";
        print FILE "   <frequency rdf:datatype=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\">$sigocc</frequency>\n";
	print FILE "   <hasParent rdf:resource=\"#$nid\" />\n";
        print FILE "</Signature>\n\n";
    } $sigsth->finish();
}

print FILE "</rdf:RDF>\n";
$sth->finish();


sub die_error {
    my ($err_string) = $_[0];

    close(FILE);
    die $err_string;
}

1;
