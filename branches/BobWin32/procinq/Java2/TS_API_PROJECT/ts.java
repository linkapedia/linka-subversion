
import api.*;
import api.util.VeritableOutputStream;
import com.indraweb.execution.Session;
import com.indraweb.ir.clsStemAndStopList;
import com.iw.license.IndraLicense;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import java.io.*;
import java.sql.Connection;
import java.util.*;
import java.util.zip.GZIPInputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 * Main Servlet class for IndraWeb Taxonomy Server API All rights reserved. (c)IndraWeb.com,Inc. 2001-2002
 *
 * @authors hkon, mpuscar
 *
 * @param req Contains the name-value pairs: fn=functionname parm1=parameterValue1 parm2=parameterValue2 ...
 *
 * @param res response is written as XML to the output stream of res
 *
 * @return xml via output stream
 */
public class ts extends HttpServlet {
    // This is a "convenience method" so, after overriding it there is no need
    // to call the super()
    // This function takes the place of the initializeSession method in the
    // engine project.

    private static final long serialVersionUID = 1497246913121144197L;
    private static int icallctr_ts = 0;
    private static final Logger log = Logger.getLogger(ts.class);

    @Override
    public void init() throws ServletException {
        log.debug("Init Method from TS Servlet");
        // If the session is already initialized, return.

        if (com.indraweb.execution.Session.GetbInitedSession()) {
            return;
        }

        // Load environment from JRUN config application variables first
        Enumeration<String> eParams = getServletContext().getInitParameterNames();
        Map<String, String> htprops = new HashMap<String, String>();

        while (eParams.hasMoreElements()) {
            String sParamName = eParams.nextElement();
            String sParamValu = (String) getServletContext().getInitParameter(sParamName);
            htprops.put(sParamName, sParamValu);
        }
        Session.sIndraHome = (String) getServletContext().getInitParameter("IndraHome");
        log.info("ts::init() Set Session.sIndraHome to [" + Session.sIndraHome + "]");
        Session.cfg = new com.indraweb.execution.ConfigProperties(htprops);

        // added by MAP 6/24/04, create license object and store in session
        String licenseKey = Session.cfg.getProp("licenseKey", false, "0");
        log.info("License key (" + licenseKey + ") initializing...");

        if (licenseKey.equals("0")) {
            log.warn("Warning! No license key was specified, starting server in evaluation mode.");
        } else {
            try {
                licenseKey = com.indraweb.utils.license.LicenseUtils.licenseToBitmap(licenseKey);
            } catch (Exception e) {
                log.error("Fatal error! Invalid license key specified.", e);
                throw new ServletException("Fatal error! Invalid license key specified: " + licenseKey);
            }
        }
        Session.license = new IndraLicense(licenseKey);
        Session.SetbInitedSession(true);
        log.info("TS SERVER INITED");
    }

    public void doSerialize(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        log.debug("TS::doSerialize(HttpServletRequest, HttpServletResponse)");
        PrintWriter out = getPrintWriter(res);
        SerialProps sDataP;

        try {
            ObjectInputStream objin = new ObjectInputStream(req.getInputStream());
            Object o = objin.readObject();
            sDataP = (SerialProps) o;
            File f = sDataP.GetFile();
            if (f.exists()) {
                f.delete();
            }
            String sFileName = Session.cfg.getProp("ImportDir") + "/" + f.getName();
            FileOutputStream fos = new FileOutputStream(sFileName);
            fos.write(sDataP.bArr);
            fos.flush();
            fos.close();
            sDataP.Props.put("URL", "file:///" + sFileName);
            doGet(req, res, sDataP.Props);

        } catch (Exception e) {
            log.error("An exception has ocurred while serializing data.", e);
            TSException tse = new TSException(api.emitxml.EmitGenXML_ErrorInfo.ERR_TS_ERROR, e.getMessage());
            api.emitxml.EmitGenXML_ErrorInfo.emitException("", tse, out);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("TS::doPost(HttpServletRequest, HttpServletResponse)");
        icallctr_ts++;
        if (request.getContentType().equals("application/x-java-serialized-object")) {
            doSerialize(request, response);
            return;
        }

        if (!request.getContentType().substring(0, 19).equals("multipart/form-data")) {
            log.debug("GET Request Detected");
            doGet(request, response);
        } else {
            log.debug("POST Request Detected");
            MultipartParser mpr = new MultipartParser(request, 1000000000);
            Part p = null;
            String sFilename = null;
            api.APIProps apiprops = new api.APIProps();

            try {
                // get indrahome from jrun config
                String sIndraHome = Session.sIndraHome;
                if (sIndraHome == null) {
                    throw new Exception("IndraHomeFolder not specified in jrun config");
                }

                // loop through each part that was sent
                //FileMetadata is to manage image
                ArrayList<FileMetadata> listFilesMeta = new ArrayList<FileMetadata>();
                FileMetadata fileMeta;
                Map<String, InputStream> fileMap = new HashMap<String, InputStream>();
                String partData[] = null;
                boolean flagManagerImages = false;
                boolean flagFileGz = false;
                while ((p = mpr.readNextPart()) != null) {
                    String sParmName = p.getName();
                    String sParmValue = "";
                    if (p.isParam()) {
                        ParamPart pp = (ParamPart) p;
                        if (pp.getName().equalsIgnoreCase("infoData")) {
                            log.debug("Upload Image Found");
                            log.debug("infoData " + pp.getStringValue());
                            partData = pp.getStringValue().split("!");
                            flagManagerImages = true;
                        }
                        if (pp.getName().equalsIgnoreCase("getFileGz")) {
                            flagFileGz = true;
                        }
                        sParmValue = pp.getStringValue();
                    }
                    if (p.isFile()) {
                        log.debug("File Detected on POST");
                        FilePart fp = (FilePart) p;

                        // Write content type and file name into props
                        apiprops.put("Content-type", fp.getContentType());
                        sParmValue = fp.getFileName();

                        if (fp.getFileName() != null) {
                            // 10/17/2004 - do not write input stream to a file, keep it in memory instead
                            InputStream is = fp.getInputStream();
                            if (flagFileGz) {
                                log.debug("FILE GZIP read GZIPInputStream");
                                is = new GZIPInputStream(is);
                            }

                            ByteArrayOutputStream bao = new ByteArrayOutputStream();
                            int b = 0;
                            byte data[] = new byte[1024];
                            while ((b = is.read(data, 0, data.length)) != -1) {
                                bao.write(data, 0, b);
                            }

                            is.close();

//                            log.debug("SB length before: " + bao.size());
                            StringBuilder sb = removeIllegalCharacters(bao.toString());
//                            log.debug("SB length after: " + sb.length());
                            log.debug("FileName: " + fp.getFileName() + "/ FilePath: " + fp.getFilePath());
                            if (fp.getFileName().endsWith(".txt")) {
                                log.debug("Upload txt File");
                                apiprops.put("inputstream", "<HTML>\n<HEAD><TITLE>" + fp.getFileName() + "</TITLE>\n</HEAD>" + "\n<BODY>\n" + sb.toString() + "\n</BODY>\n</HTML>");
                            } else if (flagManagerImages) {
                                log.debug("Upload Image");
                                ByteArrayInputStream bis = new ByteArrayInputStream(bao.toByteArray());
                                log.debug("Put Image Into map " + fp.getName() + " Size File " + bis.available());
                                fileMap.put(fp.getName(), bis);
                            } else if (flagFileGz) {
                                log.debug("FILE GZIP set inputstream");
                                //apiprops.put("inputstream", bao.toByteArray());
                                apiprops.put("inputstream", bao);
                                log.debug("Bao Value" + apiprops.get("inputstream"));
                            } else {
                                log.debug("Uploading Binary FIle (No including images)");
                                apiprops.put("inputstream", sb.toString());
                            }
                            apiprops.put("fileposted", "true");
                        }
                        apiprops.put("URL", fp.getFilePath());
                    }

                    //Reading remaining parameters.
                    if (sParmValue != null) {
                        apiprops.put(sParmName, sParmValue);
                    }
                }

                if (flagManagerImages) {
                    log.debug("FileMap Map Size " + partData.length);
                    log.debug("FileMap Building from infoData");
                    String data[] = null;
                    int b = 0;
                    byte barray[] = new byte[1024];
                    ByteArrayOutputStream bao = null;
                    InputStream is = null;
                    for (int i = 0; i < partData.length; i++) {
                        data = partData[i].split(";");
                        log.debug("row from the infoData " + partData[i].toString());
                        if (data.length != 4) {
                            continue;
                        }
                        fileMeta = new FileMetadata();
                        fileMeta.setId(data[0]);
                        fileMeta.setDescription(data[1]);
                        fileMeta.setNodeId(data[2]);
                        fileMeta.setUri(data[3]);
                        b = 0;
                        bao = new ByteArrayOutputStream();
                        is = fileMap.get(data[0]);
                        if (is == null) {
                            continue;
                        }
                        while ((b = is.read(barray, 0, 1024)) != -1) {
                            bao.write(barray, 0, b);
                        }
                        fileMeta.setImage(bao.toByteArray());
                        listFilesMeta.add(fileMeta);
                    }
                    log.debug("FileMap List Size " + listFilesMeta.size());
                    apiprops.put("inputstream", listFilesMeta);
                }
                doGet(request, response, apiprops);
            } // If an error occurs attempting to invoke the class, or it does not
            // exist, print the main page
            catch (Exception e) {
                api.Log.LogFatal("Failure in receiving MIME attached HTTP call.", e);
                Log.LogError("caught within ts.java.doPost(), re-throwing", e);
                api.emitxml.EmitGenXML_ErrorInfo.emitException("caught within ts.java.doPost(), re-throwing", e, System.err);
                throw new ServletException("General exception in ts.java Post see api.log ", e);
            } finally { // cleanup extraneous files
                if (sFilename != null) {
                    File f = new File(sFilename);
                    if (f.exists() && !com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebugLeaveClassifyTempFiles.txt")) {
                        f.delete();
                    } else {
                        log.warn("file not existing to delete or IndraDebugLeaveClassifyTempFiles.txt switch set not to [" + sFilename + "]");
                    }
                }
            }
        }
        // UtilFile.addLineToFile("/temp/temp.txt",
        // "exiting ts doPost outer  b[" + icallctr_ts + "]\r\n");
    }

    // *******************************************
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        log.debug("TS::doGet(Request,Response)");
        icallctr_ts++;
        Properties props = new Properties();

        api.APIProps apiprops = new api.APIProps(props);
        doGet(req, res, apiprops);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res, api.APIProps apiprops) throws ServletException, IOException {
        log.debug("doGet(Request,Response,Props)");
        doGet(req, res, apiprops, false);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res, api.APIProps apiprops, boolean testcase) throws ServletException, IOException {
        log.debug("doGet(Request,Response,Props,TestCase)");
        Connection dbc = null;
        String sWhichDB = "API";
        PrintWriter out = null;
        try {
            if (req.getParameter("DTAR") != null) {
                log.debug("erase output");
                res.setContentType("application/x-gzip");
                out = new PrintWriter(new ByteArrayOutputStream());

            } else {
                res.setContentType("text/xml; charset=UTF-8");
                out = res.getWriter();
            }

            @SuppressWarnings("unchecked")
            Enumeration<String> enumeration = req.getParameterNames();
            while (enumeration.hasMoreElements()) {
                String sParmName = (String) enumeration.nextElement();
                String sISO = req.getParameter(sParmName);
                byte bytes[] = sISO.getBytes();
                String sParmValue = (new String(bytes, "UTF-8"));
                apiprops.put(sParmName, sParmValue);
            }

            try {
                sWhichDB = (String) apiprops.get("whichdb", "API");
            } catch (Exception e) {
                log.warn("USING API AS ERROR DEFAULT");
                sWhichDB = "API";
            }

            // don't init a dbc for tsmetasearch - dont want to waste them
            // System.out.println("sFn = [" + sFn + "]" );
            // System.out.println("ts getting DBC for sWhichDB  [" + sWhichDB +
            // "]");
            dbc = api.util.APIDBInterface.getConnection(sWhichDB);
            if (dbc == null) {
                throw new Exception("Database connection is NULL DB [" + sWhichDB + "]");
            }

            // instantiate security
            api.security.DocumentSecurity.getSecurityHash(dbc);

            Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));

            // if a proxy has been specified in the configuration, it is set
            // here
            if (Session.cfg.getProp("proxyHost") != null) {
                String sHost = Session.cfg.getProp("proxyHost");
                if (!sHost.toLowerCase().equals("none")) {
                    setProxyProperties(Session.cfg.getProp("proxyHost"), Session.cfg.getProp("proxyPort"));
                    // api.Log.Log("### Note: api will be using a proxy [" +
                    // Session.cfg.getProp("proxyHost") + "]");
                } // else
                // api.Log.Log("### Note: api will not be using a proxy.");
            } // else
            // api.Log.Log("### Note: api will not be using a proxy.");

            APIHandler.doGetMine(req, res, apiprops, out, dbc, true, testcase);
        } catch (Exception e) {
            Log.LogError("caught within ts.java.doGet(), re-throwing", e);
            api.emitxml.EmitGenXML_ErrorInfo.emitException("caught within ts.java.doGet(), re-throwing", e, out);
            throw new ServletException("General exception in ts.java Get see api.log ", e);
        } finally {
            try {
                dbc = api.util.APIDBInterface.freeConnection(dbc);
            } catch (Exception e) {
                api.Log.LogError("error freeing dbc", e);
            }
        }
    } // public void doGet(HttpServletRequ est req,

    @Override
    public void destroy() {
    }

    public StringBuilder removeIllegalCharacters(String s) {
        log.debug("removeIllegalCharacters(String)");
        StringBuilder sb = new StringBuilder(s);
        return removeIllegalCharacters(sb);
    }

    public StringBuilder removeIllegalCharacters(StringBuilder sb) {
        log.debug("removeIllegalCharacters(StringBuilder)");
        for (int i = 0; i < sb.length(); i++) {
            if ((sb.charAt(i) < 32) && (sb.charAt(i) != 13)) {
                sb.deleteCharAt(i);
                --i; // We just shortened this buffer.
            }
        }
        return sb;
    }

    public void setProxyProperties(String proxyHost, String proxyPort) {
        log.debug("TS::setProxyProperties(String,String)");
        System.getProperties().put("proxySet", "true");
        System.getProperties().put("proxyHost", proxyHost);
        System.getProperties().put("proxyPort", proxyPort);
    }

    public PrintWriter getPrintWriter(HttpServletResponse res) throws IOException {
        log.debug("TS::getPrintWriter(Response)");
        return new PrintWriter(new OutputStreamWriter(new VeritableOutputStream(res.getOutputStream()), "UTF-8"));
    }
} // public class ts extends HttpServlet

