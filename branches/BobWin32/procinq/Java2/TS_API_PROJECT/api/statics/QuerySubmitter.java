package api.statics;

import java.sql.*;

public class QuerySubmitter
{

	// ***********************************************
	public static int DBQueryint ( String sql, Connection dbc )
	// ***********************************************
		throws Exception
	{
		int iRtn = -1;
		Statement stmt = dbc.createStatement();	
		ResultSet rs = null;
		rs = stmt.executeQuery (sql);
		while ( rs.next() ) {
			iRtn = rs.getInt(1);
		}
		rs.close();
	    stmt.close();
		return iRtn;
	} 
	
	// ***********************************************
	public static String DBQueryString ( String sql, Connection dbc )
	// ***********************************************
		throws Exception
	{
		String sRtn = null;
		Statement stmt = dbc.createStatement();	
		ResultSet rs = null;
		rs = stmt.executeQuery (sql);
		while ( rs.next() ) {
			sRtn = rs.getString(1);
		}
		rs.close();
	    stmt.close();
		return sRtn;
	} 
}
