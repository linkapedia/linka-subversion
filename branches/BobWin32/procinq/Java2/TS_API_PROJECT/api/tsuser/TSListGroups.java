package api.tsuser;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.util.Properties;

import com.iw.system.User;
import com.iw.system.SecurityGroup;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * List all the LDAP/system that this user has access to see.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  GroupID(optional) The unique LDAP group identifier, if specific group properties are requested.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsuser.TSListGroups&SKEY=-132981656

 *	@return	a series of LDAP group objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <GROUPS>
            <GROUP>
                <GROUPID>CN=PL,CN=Users,DC=indraweb</GROUPID>
                <GROUPNAME>PL</GROUPNAME>
                <GROUPLONGNAME>PL</GROUPLONGNAME>
            </GROUP>
            <GROUP>
                <GROUPID>CN=DnsAdmins,CN=Users,DC=indraweb</GROUPID>
                <GROUPNAME>DnsAdmins</GROUPNAME>
                <GROUPLONGNAME>DNS Administrators Group</GROUPLONGNAME>
            </GROUP>
 ...
        </GROUPS>
    </TSRESULT>  \endverbatim
 */
public class TSListGroups
{
	// TSListGroupMembers (sessionid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sGroupDC = (String) props.get ("GroupID"); // OPTIONAL
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = null;
		try {
            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
			Hashtable htGroups = new Hashtable();
			
			// Get the GROUP LIST from LDAP
			if (sGroupDC == null) {
                if (u.IsMember(out)) { htGroups = lc.GetAllGroups(); }
                else { htGroups = lc.GetGroups(u); }
            } else {
				Group g = lc.GetGroup(sGroupDC); 
				if (g == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
				htGroups.put(g.GetDN(), g);
			}
				
			if (htGroups.size() > 0) { out.println("<GROUPS>"); }
			Enumeration eh = htGroups.elements();
			while (eh.hasMoreElements()) {
				Group g = (Group) eh.nextElement();
				String s = g.GetDN(); String DN = s;
				
				if (!s.endsWith(lc.GetDC())) { DN = DN+","+lc.GetDC(); }

                SecurityGroup sg = api.security.DocumentSecurity.getSecurityByName(s);
                String securityID = "-1";
                if (sg != null) securityID = sg.getID();

				out.println ("   <GROUP> ");
                out.println ("      <SECURITYID>"+securityID+"</SECURITYID>");
				out.println ("      <GROUPID>"+DN+"</GROUPID>");
				out.println ("      <GROUPNAME>"+g.GetGroupName()+"</GROUPNAME>");
                out.println ("      <GROUPLONGNAME>"+g.GetDisplayName()+"</GROUPLONGNAME>");
				out.println ("   </GROUP>");
			}
			if (htGroups.size() > 0) { out.println("</GROUPS>"); }
		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
