package api.tsuser;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;

import com.indraweb.database.*;

/**
 * List the LDAP/system users who are a member of the given group DN
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  GroupID The unique LDAP group identifier.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsuser.TSListGroupMembers&GroupID=CN=Server+Operators,DC=indraweb&SKEY=-132981656

 *	@return	a series of SUBSCRIBER objects, each corresponding to a member of this group
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUBSCRIBERS>
            <SUBSCRIBER>
                <ID>sn=user,ou=users,dc=indraweb,dc=com</ID>
                <EMAIL>testuser@indraweb.com</EMAIL>
                <PASSWORD>mypassword</PASSWORD>
                <NAME>Test User</NAME>
                <EMAILSTATUS>1</EMAILSTATUS>
                <SCORETHRESHOLD>50.0</SCORETHRESHOLD>
                <RESULTSPERPAGE>10</RESULTSPERPAGE>
                <USERSTATUS>1</USERSTATUS>
                <KEY>993135977</KEY>
            </SUBSCRIBER>
 ...
        </SUBSCRIBERS>
    </TSRESULT>  \endverbatim
 */
public class TSListGroupMembers
{
	// TSListGroupMembers (sessionid, group_name)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sGroupDC = (String) props.get ("GroupID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {
			Group g = lc.GetGroup(sGroupDC);
		
			if (g == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			lc.close();
            lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
			Vector vUsers = lc.GetUsers(g);
			if (vUsers.size() == 0) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			Enumeration ev = vUsers.elements();
			if (vUsers.size() > 0) { out.println("<SUBSCRIBERS>"); }
			while (ev.hasMoreElements()) {
				u = (User) ev.nextElement();

				out.println ("   <SUBSCRIBER> ");
				out.println ("      <ID>"+u.GetDN()+","+lc.GetDC()+"</ID> ");
				out.println ("      <EMAIL>"+u.GetEmail()+"</EMAIL>");
				out.println ("      <PASSWORD>"+u.GetPassword()+"</PASSWORD>");
				out.println ("      <NAME>"+u.GetFullname()+"</NAME>");
				out.println ("      <EMAILSTATUS>"+u.GetEmailStatus()+"</EMAILSTATUS>");
				out.println ("      <SCORETHRESHOLD>"+u.GetScoreThreshold()+"</SCORETHRESHOLD>");
				out.println ("      <RESULTSPERPAGE>"+u.GetResultsPerPage()+"</RESULTSPERPAGE>");
				out.println ("      <USERSTATUS>"+u.GetUserStatus()+"</USERSTATUS>");
				out.println ("   </SUBSCRIBER>");
			}
			if (vUsers.size() > 0) { out.println("</SUBSCRIBERS>"); }
		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
