package api.tscorpus;

import api.FileMetadata;
import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import org.apache.log4j.Logger;

/**
 * Add a Image to corpus.
 *
 *	@authors Andres Restrepo, All Rights Reserved.
 *
 *	@return	SUCCESS tag, if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
<TSRESULT>
<SUCCESS>image add ok</SUCCESS>
</TSRESULT>
\endverbatim
 */
public class TSAddImageCorpus {

    // TSAddImageCorpus (imageid,description,url,image(blob),corpusid)
    private static final Logger log = Logger.getLogger(TSAddImageCorpus.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        log.debug("TSAddImageCorpus");
        List<FileMetadata> listFileMetada=(List<FileMetadata>)props.get("inputstream", null);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String sSQL;
        if(listFileMetada.isEmpty())
        {
            log.debug("List FIle is Empty");
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR) , out);
            return;
        }
        String corpusid=listFileMetada.get(0).getNodeId();
        if(corpusid==null){
            log.debug("not corpusid selected");
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR) , out);
            return;
        }
        try {
            if (!u.IsAdmin(Integer.parseInt(corpusid), out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            sSQL = "insert into CORPUSIMAGES(imageid, description, url, corpusid, image) values(SEC_IMAGESCORPUS.nextval, ?, ?, ?, ?)";
            pstmt = dbc.prepareStatement(sSQL);
            for(FileMetadata file:listFileMetada)
            {
                pstmt.setString(1, file.getDescription());
                pstmt.setString(2, file.getUri());
                pstmt.setInt(3, Integer.parseInt(corpusid));
                pstmt.setBinaryStream(4, new ByteArrayInputStream(file.getImage()));
                pstmt.executeUpdate();
            }
            log.debug("upload image file successful");
            out.println("<SUCCESS>images add ok</SUCCESS>");
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
}
