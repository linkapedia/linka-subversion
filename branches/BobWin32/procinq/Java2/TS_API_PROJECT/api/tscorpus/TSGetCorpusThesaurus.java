package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.iw.system.Thesaurus;
import com.indraweb.database.*;

/**
 *  Get all available thesauri in the server that have a relationship with the given corpus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CorpusID    Unique identifier corresponding to a given corpus or taxonomy.
 *
 *  @note    http://itsserver/servlet/ts?fn=tscorpus.TSGetCorpusThesaurus&SKEY=993135977

 *	@return	a series of thesaurus objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <THESAURI>
            <THESAURUS>
                <THESAURUSID>1</THESAURUSID>
                <THESAURUSNAME>IDRAC Thesaurus</THESAURUSNAME>
            </THESAURUS>
            <THESAURUS>
                <THESAURUSID>1121</THESAURUSID>
                <THESAURUSNAME>regress02</THESAURUSNAME>
            </THESAURUS>
            <THESAURUS>
                <THESAURUSID>13360</THESAURUSID>
                <THESAURUSNAME>al Qaeda</THESAURUSNAME>
            </THESAURUS>
 ...
  \endverbatim
 */
public class TSGetCorpusThesaurus
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
        String sCorpusID = (String) props.get("CorpusID", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Hashtable ht = new Hashtable();

        Statement stmt = null; ResultSet rs = null;

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED); }

			String sSQL = " select c.thesaurusid, t.thesaurusname from thesaurus t, corpusthesaurus c "+
                          " where t.thesaurusid = c.thesaurusid and c.corpusid = "+sCorpusID;

			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			while ( rs.next() ) {
                Thesaurus t = new Thesaurus(rs);
                ht.put(t.getID(), t);
            }

            if (ht.size() > 0) {
				out.println ("<THESAURI>");

                Enumeration eT = ht.elements();
                while (eT.hasMoreElements()) {
                    Thesaurus t = (Thesaurus) eT.nextElement();
                    t.emitXML(out, true);
                }

                out.println ("</THESAURI>");
            } //else { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
		}

		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally {
            lc.close();
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
