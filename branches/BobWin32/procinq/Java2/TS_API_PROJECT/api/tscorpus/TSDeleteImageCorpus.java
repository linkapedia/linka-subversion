/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.tscorpus;

import java.io.*;
import java.sql.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;



import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSDeleteImageCorpus {

    private static final Logger log = Logger.getLogger(TSDeleteImageCorpus.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        log.debug("TSDeleteImageCorpus");
        Statement stmt = null;
        String sSQL = "";
        String imageid = (String) props.get("imageid");
        log.debug("IMAGEID, TABLE CORPUSIMAGES: "+imageid);
        if (imageid == null) {
            log.error("Not imageid selected");
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR), out);
            return;
        }
        try {
            stmt = dbc.createStatement();
            sSQL = "DELETE FROM CORPUSIMAGES WHERE IMAGEID=" + imageid;
            stmt.executeUpdate(sSQL);
            log.debug("Delete OK...");
            out.println("<SUCCESS>images delete ok</SUCCESS>");
        } catch (SQLException sqle) {
            log.error("SQLException -> TSDeleteImageCorpus" + sqle.getMessage() + " sql is: " + sSQL);
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR), out);
        } catch (Exception e) {
            log.error("Exception TSDeleteImageCorpus " + e.getMessage());
            EmitGenXML_ErrorInfo.emitException("TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR), out);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }


    }
}
