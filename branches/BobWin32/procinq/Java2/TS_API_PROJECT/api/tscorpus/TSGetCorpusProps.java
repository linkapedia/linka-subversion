package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 *  Display the properties of a given corpus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *
 *  @note    http://itsserver/servlet/ts?fn=tscorpus.TSGetCorpusProps&CorpusID=4&SKEY=993135977

 *	@return	An ITS Corpus object.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CORPUS>
            <CORPUSID>4</CORPUSID>
            <CORPUS_NAME>My First Corpus</CORPUS_NAME>
            <CORPUSDESC>something or other</CORPUSDESC>
            <ROCF1>0.0</ROCF1>
            <ROCF2>0.0</ROCF2>
            <ROCF3>0.0</ROCF3>
            <ROCC1>0.0</ROCC1>
            <ROCC2>0.0</ROCC2>
            <ROCC3>0.0</ROCC3>
            <CORPUSACTIVE>1</CORPUSACTIVE>
        </CORPUS>
    <CLASSLOAD>Fri Oct 26 17:47:46 EDT 2001</CLASSLOAD>
    <CALLCOUNT>49</CALLCOUNT>
    <TIMEOFCALL_MS>50</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSGetCorpusProps
{
	// TSGetCorpusProps (sessionid, corpusid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{

		String iCorpusID = (String) props.get ("CorpusID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {

			if ((!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) &&
                (!u.IsAuthorized(iCorpusID, out))) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " select CorpusID, Corpus_Name, CorpusDesc, "+
						  " Rocf1, Rocf2, Rocf3, Rocc1, Rocc2, Rocc3, CorpusActive"+
						  " from Corpus where CorpusId = "+iCorpusID;
			Statement stmt = dbc.createStatement();
			ResultSet rs = stmt.executeQuery (sSQL);

			/*
			<CORPUS ID=corpusid>
			   <CORPUS_NAME>Corpus Name</CORPUS_NAME>
			   <CORPUSDESC>Corpus Description</CORPUSDESC>
			   <ROCF1>score</ROCF1>
			   <ROCF2>score</ROCF2>
			   <ROCF3>score</ROCF3>
			   <ROCC1>score</ROCC1>
			   <ROCC2>score</ROCC2>
			   <ROCC3>score</ROCC3>
			   <CORPUSACTIVE>Corpus Active Boolean</CORPUSACTIVE>
			</CORPUS>
			*/

			int loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.   To do this, hold off
				//  on writing out the initial stream <CORPERA> until we have found at
				//  least one result.   -MAP 10/14/01
				loop = loop + 1;

				String sCorpusName = rs.getString(2);
				String sCorpusDesc = rs.getString(3);
				float fRocF1 = rs.getFloat(4);
				float fRocF2 = rs.getFloat(5);
				float fRocF3 = rs.getFloat(6);
				float fRocC1 = rs.getFloat(7);
				float fRocC2 = rs.getFloat(8);
				float fRocC3 = rs.getFloat(9);
				int iCorpusActive = rs.getInt(10);

				out.println (" <CORPUS>");
				out.println ("   <CORPUSID>"+iCorpusID+"</CORPUSID>");
				out.println ("   <CORPUS_NAME>"+sCorpusName+"</CORPUS_NAME>");
				out.println ("   <CORPUSDESC>"+sCorpusDesc+"</CORPUSDESC>");
				out.println ("   <ROCF1>"+fRocF1+"</ROCF1>");
				out.println ("   <ROCF2>"+fRocF2+"</ROCF2>");
				out.println ("   <ROCF3>"+fRocF3+"</ROCF3>");
				out.println ("   <ROCC1>"+fRocC1+"</ROCC1>");
				out.println ("   <ROCC2>"+fRocC2+"</ROCC2>");
				out.println ("   <ROCC3>"+fRocC3+"</ROCC3>");
				out.println ("   <CORPUSACTIVE>"+iCorpusActive+"</CORPUSACTIVE>");
				out.println ("</CORPUS>");

			}

			rs.close();
		    stmt.close();


			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Corpus not found</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND);
			}

		}

		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
