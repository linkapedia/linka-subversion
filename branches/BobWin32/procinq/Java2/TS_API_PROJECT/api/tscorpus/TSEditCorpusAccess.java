package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;

import com.iw.system.User;

/**
 *  Grant access permissions for a given corpus in LDAP.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus identifier
 *  @param  GroupID The full LDAP distinguished name corresponding to the group
 *  @param  Access  Access permissions, -1 for none, 0 for read only and 1 for read/write.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tscorpus.TSEditCorpusAccess&CorpusID=100007&SKEY=1634835192&Access=0&GroupID=cn=Tech+Staff,ou=users,dc=indraweb,dc=com

 *	@return	SUCCESS tag if successful
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>Permissions modified successfully</SUCCESS>
    </TSRESULT>
  \endverbatim
 */
public class TSEditCorpusAccess
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sGroupDC = (String) props.get ("GroupID", true);
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sAccess = (String) props.get ("Access", true);
	
		String sKey = (String) props.get("SKEY", true);
		User u = (User) Session.htUsers.get(sKey);

        // if security is turned off, security changes can no longer be made
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            out.println("<SUCCESS>Permissions modified successfully</SUCCESS>");
            return;
        }

        LDAP_Connection lc = null;
		try {
			if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
			Group g = lc.GetGroup(sGroupDC); 
		
			if (g == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			if (sAccess.equals("1")) { g.GrantAdmin(sCorpusID); g.RevokeCorpus(sCorpusID); }
			else if (sAccess.equals("0")) { g.GrantCorpus(sCorpusID); g.RevokeAdmin(sCorpusID); }
			else { g.RevokeAdmin(sCorpusID); g.RevokeCorpus(sCorpusID); }

			if (!g.SaveGroup(out, u.GetDN(), u.GetPassword())) {
				throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_GROUP_ACCESS_CHANGE_FAILURE); 	
			}

            // now recycle group permissions cache
            u.SetGroupPermissions(new Hashtable()); u.GetAuthorizedHash(out);

            out.println("<SUCCESS>Permissions modified successfully</SUCCESS>");

		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
