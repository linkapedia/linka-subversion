package api.tscorpus;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import api.util.S3Storage;
import api.util.S3StorageException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.linkapedia.images.bo.BuilderImage;

/**
 *
 * @author andres
 */
public class TSSaveImagesS3 {

    private static final Logger LOG = Logger.getLogger(TSSaveImagesS3.class);
    private static final String FOLDER_IMAGE = "images";
    private static final String BUCKET_NAME = "taxonomyimages";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        LOG.debug("TSSaveImagesS3 Corpus");
        NodeUtils nu = new NodeUtils(dbc);
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String corpusId = (String) props.get("corpusid");

        if ((corpusId == null)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (corpusId.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorizationByCorpus(Integer.parseInt(corpusId), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        int ncorpusId = 0;
        List<byte[]> list = null;
        try {
            ncorpusId = Integer.parseInt(corpusId);
        } catch (NumberFormatException nfe) {
            LOG.error("error corpusid not is a number", nfe);
            out.println("<RESULT>Invalid corpusId</RESULT>");
            return;
        }
        int imageaCount;
        try {
            list = nu.getImagesByCorpusId(ncorpusId);
        } catch (SQLException e) {
            LOG.error("Error in the consult SQL", e);
            out.println("<RESULT>Error in the sql statement</RESULT>");
            return;
        }

        imageaCount = list.size();
        if (imageaCount >= 5) {
            StringBuilder idSb = new StringBuilder();
            //reverse corpusid
            idSb.append(corpusId);
            idSb = idSb.reverse();
            int cont = 0;
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentType("image/jpeg");
            for (byte[] is : list) {
                BufferedImage image = ImageIO.read(new ByteArrayInputStream(is));
                try {
                    BuilderImage builder = new BuilderImage(image, BuilderImage.SITE_IMAGE);
                    image = builder.build();
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    ImageIO.write(image, "JPG", bao);
                    String key = idSb.toString() + "/" + FOLDER_IMAGE + "/" + idSb.toString() + "_" + cont + ".jpg";
                    S3Storage.storageData(BUCKET_NAME, key, new ByteArrayInputStream(bao.toByteArray()), meta);
 
                    if (cont == 4) {
                        break;
                    }
                    cont++;
                } catch (S3StorageException e) {
                    LOG.error("Error S3StorageException: storageing image ", e);
                    continue;
                } catch (IOException e) {
                    LOG.error("Error IO: tranform image ", e);
                    continue;
                } catch (Exception e) {
                    LOG.error("Exception general in the process", e);
                    continue;
                } finally {
                }
            }
            out.println("<RESULT>images saved -> " + cont++ + "</RESULT>");
        } else {
            out.println("<RESULT> Require 5 images to export</RESULT>");
        }
    }
}
