package api.tscorpus;


import java.io.*;
import java.sql.*;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.execution.Session;
import com.iw.system.User;

/**
 *  Lists the black list terms for a given corpus or node within a corpus.  To get the blacklist for a corpus,
 *  do not specify a node identifier..
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  BlackList_Term  A whole or partial string matching the blacklisted URL
 *  @param  NodeID (optional)   Unique node identifier, used only to restrict blacklisting by topic.
 *
 *  @note  http://itsserver/servlet/ts?fn=tscorpus.TSListBlacklist&CorpusID=5&SKEY=993135977

 *	@return	a series of black list objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <BLACKLIST>
            <BLACKLISTWORD>joes.com</BLACKLISTWORD>
        </BLACKLIST>
    </TSRESULT>
  \endverbatim
 */
public class TSListBlacklist
{
	// TSListBlacklist (sessionid, corpusid, nodeid)
	/*
	SQL> describe blacklist;
	 Name                                      Null?    Type
	 ----------------------------------------- -------- ----------------------------
	 BLACKLISTID                               NOT NULL NUMBER(4)
	 CORPUSID                                  NOT NULL NUMBER(9)
	 NODEID                                    NOT NULL NUMBER(12)
	 URLCHUNK                                           VARCHAR2(256)
	 DATECREATED                                        DATE
	*/
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID");
		// Node ID is optional
		String sNodeID = (String) props.get ("NodeID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {

			if ((!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) &&
                (!u.IsAuthorized(sCorpusID, out))) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			if (sCorpusID == null) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
			}

			String sSQL = " select UrlChunk from Blacklist where "+
						  " CorpusID = "+sCorpusID;
			if (sNodeID != null) { sSQL = sSQL + " or NodeID = "+sNodeID; }
			else { sSQL = sSQL + " and NodeID is NULL"; }
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				if (loop == 1) { out.println ("<BLACKLIST>"); }
				
				String sBlack = rs.getString(1);
		
				out.println ("      <BLACKLISTWORD>"+sBlack+"</BLACKLISTWORD>");
				
			}

			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No blacklist items found </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}

			if (loop > 0) { out.println ("</BLACKLIST>"); }
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
