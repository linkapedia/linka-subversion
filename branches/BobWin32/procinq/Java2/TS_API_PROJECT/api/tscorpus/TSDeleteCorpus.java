package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.DecimalFormat;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Marks a corpus for deletion.  The deletion will only occur after a purge objects directive has been executed.
 *  Until then, the corpus will not be available for any user processing.   If invoked with the "permanently" flag,
 *  the corpus will be removed immediately and will not be recoverable.
 *
 *  If the corpus is marked for permanent deletion, a series of <STATUS> tags will be returned indicating
 *  deletion progress.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  Permanently (optional)  If this parameter is specified, the corpus will be deleted immediately.
 *
 *  @note    http://itsserver/servlet/ts?fn=tscorpus.TSDeleteCorpus&CorpusID=4&SKEY=993135977

 *	@return	SUCCESS tag if the corpus was removed successfully.
 *  \verbatim
 <?xml version="1.0" encoding="UTF-8" ?>
 <TSRESULT>
 <SUCCESS>Corpus was removed successfully.</SUCCESS>
 </TSRESULT>
 \endverbatim
 */
public class TSDeleteCorpus {
    // TSDeleteCorpus (sessionid, corpusid)
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sCorpusID = (String) props.get("CorpusID");
        String sPermanently = (String) props.get("permanently");
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            if (!u.IsAdmin(sCorpusID, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            if (sPermanently == null) changeCorpusStatus(sCorpusID, "-1", dbc);
            else removeCorpus(sCorpusID, out, dbc);

            out.println("<SUCCESS>Corpus was removed successfully.</SUCCESS>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }

    public static void changeCorpusStatus(String sCorpusID, String sStatus, Connection dbc) throws Exception {
        String sSQL;
        Statement stmt = null;;
        try {
            sSQL = "update Corpus set CorpusActive = -1 where CorpusId = " + sCorpusID;

            stmt = dbc.createStatement();

            // If query statement failed, throw an exception
            if (stmt.executeUpdate(sSQL) == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }

    // to remove a corpus and return status, select all of the nodes in the corpus and remove them
    //  one at a time.  this will improve speed as well.
    public static void removeCorpus(String sCorpusID, PrintWriter out, Connection dbc) throws Exception {
        String sSQL;
        Statement stmt = null;
        ResultSet rs = null;
        Vector vNodes = new Vector();

        // get all the node identifiers and put them into
        try {
            sSQL = "select nodeid from node where corpusid = " + sCorpusID;
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) vNodes.add(rs.getString(1));

        } catch (Exception e) { throw e;
        } finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }
        }

        try {
            DecimalFormat twoDigits = new DecimalFormat("0");
            dbc.setAutoCommit(false);

            for (int i = 0; i < vNodes.size(); i++) {

                try {
                    sSQL = "delete from node where nodeid = " + (String) vNodes.elementAt(i);
                    stmt = dbc.createStatement();
                    if (stmt.executeUpdate(sSQL) == 0) {
                        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_USER_NODE_DELETE_FAILURE);
                    }
                } catch (Exception e) { throw e;
                } finally { if (stmt != null) { stmt.close(); stmt = null; } dbc.setAutoCommit(true); }

                long percent = ((i + 1) * 100) / vNodes.size();
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                out.flush();
            }

        } catch (Exception e) { dbc.rollback(); throw e;
        } finally { if (stmt != null) { stmt.close(); stmt = null; } dbc.setAutoCommit(true); }

        try {
            sSQL = "delete from corpus where corpusid = " + sCorpusID;
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) { throw e;
        } finally { if (stmt != null) { stmt.close(); stmt = null; } }
    }

}
