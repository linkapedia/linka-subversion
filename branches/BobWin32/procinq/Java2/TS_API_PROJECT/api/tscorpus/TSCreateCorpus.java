package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;

import com.indraweb.execution.Session;
import api.Log;
import com.iw.system.User;

/**
 *  Creates a new corpus in the system.  This corpus name should be unique to avoid confusion.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus identifier
 *  @param  CorpusName    Corpus display name.
 *  @param  CorpusDesc (optional)   Corpus description.
 *  @param  CorpusActive (optional)   Active state, 1 is active, 0 is inactive.
 *
 *  @note    http://its_server:8100/servlet/ts?fn=tscorpus.TSCreateCorpus&CorpusActive=1&CorpusName=My%20First%20Corpus&CorpusDesc=something%20or%20other&SKEY=993135977

 *	@return	Simple corpus identifier
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CORPUSID>100022</CORPUSID>
        <CLASSLOAD>Fri Oct 26 17:47:46 EDT 2001</CLASSLOAD>
        <CALLCOUNT>38</CALLCOUNT>
        <TIMEOFCALL_MS>10</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSCreateCorpus
{
	// TSDeleteCorpus (sessionid, corpusid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		// Ensure user is a member of the admin group
		if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

		/*
			http://207.103.213.118:8100/servlet/ts?fn=tscorpus.TSCreateCorpus&
				CorpusName=TSCreateCorpus%20Test%20Corpus%20Name&
				CorpusDesc=&
				CorpusPath=pathunknown&
				RocF1=1&
				RocF2=2&
				RocF3=3&
				RocC1=1&
				RocC2=2&
				RocC3=3
			delete from corpus where corpusid = 3;

		// RECALL: Corpus table is different on client and server schema

		SQL> desc corpus;
		 Name                                      Null?    Type
		 ----------------------------------------- -------- ----------------------------
		 CORPUSID                                  NOT NULL NUMBER(9)
		 CORPUS_NAME                               NOT NULL VARCHAR2(256)
		 CORPUSDESC                                         VARCHAR2(256)
		 CORPUSIMAGEIDS                                     VARCHAR2(256)
		 ROCF1                                              NUMBER(63)
		 ROCF2                                              NUMBER(63)
		 ROCF3                                              NUMBER(63)
		 ROCC1                                              NUMBER(63)
		 ROCC2                                              NUMBER(63)
		 ROCC3                                              NUMBER(63)
		 CORPUSACTIVE                                       NUMBER(3)
		 CORPUSRUNFREQUENCY                                 NUMBER(3)
		 CORPUSLASTRUN                                      DATE
		*/
		createCorpus ( props, out, dbc);

	}

	/**
	 * for direct call by TSInstallCorpus, -1 means error in create corpus for internal call
	 */

	static int createCorpus ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		int iCorpusIDReturn = -1;
		try {
			Integer ISemaphore = new Integer(0);
			synchronized (ISemaphore) {

                int iCorpusID = 0;

				//SvrSchema: String sCorpusShortName = (String) props.get ("CorpusShortName");
				String sCorpusName = (String) props.get ("CorpusName", true);
				String sCorpusDesc = (String) props.get ("CorpusDesc", "None");
				String sRocF1 = (String) props.get ("RocF1", "0.0");
				String sRocF2 = (String) props.get ("RocF2", "0.0");
				String sRocF3 = (String) props.get ("RocF3", "0.0");
				String sRocC1 = (String) props.get ("RocC1", "0.0");
				String sRocC2 = (String) props.get ("RocC2", "0.0");
				String sRocC3 = (String) props.get ("RocC3", "0.0");
				String sCorpusActive = (String) props.get ("CorpusActive", "-1");
                String sCorpusID = (String) props.get("CorpusID");

                boolean bIndraweb = Session.cfg.getPropBool("Indraweb", false, "false");

                if (sCorpusID != null) iCorpusID = new Integer(sCorpusID).intValue();
                else iCorpusID = com.indraweb.utils.oracle.UniqueSpecification.getSubsequentCorpusID(dbc, bIndraweb);

				String sWhichDB = (String) props.get ("whichdb", "API");
				String sSQL = null;

				if ( sWhichDB.toLowerCase().equals("api"))
				{

					sSQL = "insert into Corpus " +
							  " ( CorpusID, Corpus_Name, CorpusDesc, RocF1, RocF2, RocF3, RocC1, RocC2, RocC3, CorpusActive ) " +
							  " values (" +
									iCorpusID+"," +
									//SvrSchema: sPublisherID + "," +
									"'"+sCorpusName +"'"+ "," +
									"'"+sCorpusDesc +"'"+ "," +
									//SvrSchema: "'"+sCorpusPath +"'"+ "," +
									sRocF1 + "," +
									sRocF2 + "," +
									sRocF3 + "," +
									sRocC1 + "," +
									sRocC2 + "," +
									sRocC3+  "," +
									sCorpusActive +
							  ")" ;
				}
				else if ( sWhichDB.toLowerCase().equals("svr"))
				{
					String sPublisherID = (String) props.get ("publisherid", true);
					sSQL = "insert into Corpus " +
							  " ( CorpusID, PublisherID, CorpusShortName, Corpus_Name, CorpusDesc, RocF1, RocF2, RocF3, RocC1, RocC2, RocC3, CorpusActive ) " +
							  " values (" +
									iCorpusID+"," +
									sPublisherID + "," +
									"'"+sCorpusName +"'"+ "," +
									"'"+sCorpusName +"'"+ "," +
									"'"+sCorpusDesc +"'"+ "," +
									//SvrSchema: "'"+sCorpusPath +"'"+ "," +
									sRocF1 + "," +
									sRocF2 + "," +
									sRocF3 + "," +
									sRocC1 + "," +
									sRocC2 + "," +
									sRocC3+  "," +
									sCorpusActive +
							  ")" ;

				}
				else
					Log.LogFatal("invalid DB ID [" + sWhichDB+ "]\r\n");



				Statement stmt = null;
				try
				{
					stmt = dbc.createStatement();

					// If query statement failed, throw an exception
					if (stmt.executeUpdate (sSQL) == 0) {
						out.println ( "<DEBUG>The specified corpus does not exist</DEBUG>");
						throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND);
					}
					iCorpusIDReturn = iCorpusID;

				}
				catch ( Exception e )
				{
					throw new TSException (	EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION,
						"e.msg ["	+ e.getMessage() + "] " +
						" SQL ["	+ sSQL + "] " +
						" e.stk ["	+ Log.stackTraceToString(e) + "]" );
				}
				finally
				{
					out.println (EmitValue.genValue ("CORPUSID" , ""+ (iCorpusID) ));
					stmt.close();
				}
			} // synchronized
		}

		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
			iCorpusIDReturn = -1; // error condition
		}

		return iCorpusIDReturn;

	} // static int createCorpus ()
}
