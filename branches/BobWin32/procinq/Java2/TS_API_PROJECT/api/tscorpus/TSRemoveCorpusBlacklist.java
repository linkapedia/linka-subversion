package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove a term from the corpus blacklist.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  BlackList_Term  A whole or partial string matching the blacklisted URL to be removed
 *  @param  NodeID (optional)   Unique node identifier, used only to restrict blacklisting by topic.
 *
 *  @note   http://itsserver/servlet/ts?fn=tscorpus.TSRemoveCorpusBlacklist&CorpusID=5&BlackList_Term=joes.com&SKEY=993135977

 *	@return	an error if unsuccessful
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CLASSLOAD>Fri Oct 26 17:47:46 EDT 2001</CLASSLOAD>
        <CALLCOUNT>38</CALLCOUNT>
        <TIMEOFCALL_MS>10</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSRemoveCorpusBlacklist
{
	// TSRemoveCorpusBlacklist (sessionid, corpusid, blacklist term, nodeid)
	/*
	SQL> describe blacklist;
	 Name                                      Null?    Type
	 ----------------------------------------- -------- ----------------------------
	 BLACKLISTID                               NOT NULL NUMBER(4)
	 CORPUSID                                  NOT NULL NUMBER(9)
	 NODEID                                    NOT NULL NUMBER(12)
	 URLCHUNK                                           VARCHAR2(256)
	 DATECREATED                                        DATE
	*/
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID");
		String sBlack = (String) props.get ("BlackList_Term");
		// Node ID is optional
		String sNodeID = (String) props.get ("NodeID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			if ((sCorpusID == null) || (sBlack == null)) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
			}
			
			String sSQL = " delete from Blacklist where CorpusID = "+sCorpusID+" and "+
						  " UrlChunk = '"+sBlack+"' ";
			if (sNodeID != null) { sSQL = sSQL + " and NodeID = "+sNodeID; }
			
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Attempt to remove new blacklist item failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

		    stmt.close();
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
