package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 *  This function returns the node properties of the root topic of the given corpus
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *
 *  @note    http://itsserver/servlet/ts?fn=tscorpus.TSGetCorpusRoot&CorpusID=2&SKEY=993135977

 *	@return	An ITS Node object.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <NODES>
            <NODE>
                <NODEID>91734</NODEID>
                <CORPUSID>2</CORPUSID>
                <NODETITLE>Wiley InterScience - Kirk-Othmer Encyclopedia of Chemical Technology</NODETITLE>
                <PARENTID>-1</PARENTID>
                <INDEXWITHINPARENT>0</INDEXWITHINPARENT>
                <DEPTHFROMROOT>0</DEPTHFROMROOT>
                <DATESCANNED>10/15/01 05:19</DATESCANNED>
                <DATEUPDATED>10/15/01 05:19</DATEUPDATED>
            </NODE>
        </NODES>
    <CLASSLOAD>Mon Nov 05 19:04:36 EST 2001</CLASSLOAD>
    <CALLCOUNT>103</CALLCOUNT>
    <TIMEOFCALL_MS>100</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSGetCorpusRoot
{
	
	private static int iCallCounter = 0;
	private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

	// TSGetCorpusRoot (sessionid, CorpusID)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		synchronized (ICallCounterSemaphore)
			{ iCallCounter++; }
		out.println ("<CALLCOUNT_FN>" + iCallCounter + "</CALLCOUNT_FN>");
		String sCorpusID = (String) props.get ("CorpusID");
		/* SQL> describe node;
		 Name                                      Null?    Type
		 ----------------------------------------- -------- ----------------------------
		 NODEID                                    NOT NULL NUMBER(12)
		 CORPUSID                                  NOT NULL NUMBER(9)
		 NODETITLE                                          VARCHAR2(256)
		 NODESIZE                                           NUMBER(9)
		 PARENTID                                           NUMBER(9)
		 PARENTREL                                          NUMBER(3)
		 NODEINDEXWITHINPARENT                              NUMBER(3)
		 DEPTHFROMROOT                                      NUMBER(3)
		 DATESCANNED                                        DATE
		 DATEUPDATED                                        DATE 
		*/

		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {

            // security will now be handled by the node emitter
			//if (!u.IsAuthorized(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = Node.getSQL(dbc)+" where parentid = -1 and corpusid = " + sCorpusID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.   To do this, hold off
				//  on writing out the initial stream <NODES> until we have found at 
				//  least one result.   -MAP 10/15/01
				//
				//  Note: There may be more than one root node for a given corpus
				loop = loop + 1;
                Node n = new Node(rs);

				if (loop == 1) { out.println ("<NODES> "); }

                n.emitXML(out, u, true);
			}
		
			rs.close();
		    stmt.close();
			
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No root nodes available for Corpus ID: "+sCorpusID+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROOT_NODES_THIS_CORPUS);
			}

			out.println ("</NODES> ");
		}
		
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
