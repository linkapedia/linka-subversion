package api.tscorpus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.zip.GZIPOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.xeustechnologies.jtar.TarEntry;
import org.xeustechnologies.jtar.TarOutputStream;

/**
 * get images from a corpus.
 *
 *	@authors Andres Restrepo
 *
 *      @return file.tar.gz 
 *      1->file (images information)
 *      2->metadata image1
 *      image1 byte[]
 *      3->metadata image2
 *      image2 byte[]
 *      ...
 *      n->
 */
public class TSGetImagesCorpus {

    private static final Logger log = Logger.getLogger(TSGetImagesCorpus.class);

    public static void handleTSapiRequest(HttpServletRequest req,
                                          HttpServletResponse res,
                                          api.APIProps props,
                                          PrintWriter out,
                                          Connection dbc
                                          ){

        log.debug("TSGetImagesCorpus");
        String corpusid = (String) props.get("corpusid");
        Statement stmt = null;
        ResultSet rs = null;
        String sSql = "SELECT * FROM CORPUSIMAGES WHERE CORPUSID="+corpusid;
        log.debug("Select sql-> "+sSql);
        log.debug("set mime type");
        res.setContentType("application/x-gzip");
        try {
            ByteArrayOutputStream bao=new ByteArrayOutputStream();
            TarOutputStream tao=new TarOutputStream(bao);
            log.debug("Get Outputstream body");
            GZIPOutputStream gout=new GZIPOutputStream(res.getOutputStream());
            log.debug("Create Statement");
            stmt = dbc.createStatement();
            log.debug("Execute Statement");
            rs = stmt.executeQuery(sSql);
            int imageid;
            String Description="";
            String url ="";
            int corpus;
            StringBuilder lineToFile=new StringBuilder();
            byte[] fileImage=null;
            TarEntry tarEntry=null;
            while (rs.next()) {
                imageid=rs.getInt("IMAGEID");
                Description=rs.getString("DESCRIPTION");
                Description = validateString(Description);
                url =rs.getString("URL");
                url=validateString(url);
                corpus=rs.getInt("CORPUSID");
                fileImage=rs.getBytes("IMAGE");
                log.debug("Image File Size "+fileImage.length);
                tarEntry=new TarEntry(new File(""), String.valueOf(imageid));
                log.debug("File Name "+String.valueOf(imageid));
                tarEntry.setName(String.valueOf(imageid));
                log.debug("Set File Size "+fileImage.length);
                tarEntry.setSize(fileImage.length);
                log.debug("Put New Entry ");
                tao.putNextEntry(tarEntry);
                log.debug("Writer Byte In Tar");
                tao.write(fileImage);
                lineToFile.append(imageid);
                lineToFile.append("||");
                lineToFile.append(Description);
                lineToFile.append("||");
                lineToFile.append(url);
                lineToFile.append("||");
                lineToFile.append(corpus);
                lineToFile.append("^");
                
                log.debug("\n lineToFile -> "+lineToFile.toString()+"\n");
                //copy line into directory
            }
            String strData=lineToFile.toString();
            if(strData.endsWith("^"))
            {
                strData=strData.substring(0, strData.lastIndexOf("^"));
            }
            tarEntry=new TarEntry(new File(""), "fileMetaData");
            log.debug("set File Name Meta Data");
            tarEntry.setName("fileMetaData");
            log.debug("set File Size Meta Data");
            tarEntry.setSize(strData.getBytes().length);
            log.debug("set Put Entry  Meta Data");
            tao.putNextEntry(tarEntry);
            log.debug("write Bytes Meta Data");
            tao.write(strData.getBytes());
            log.debug("close Tar");
            tao.close();
            log.debug("GZip Output");
            gout.write(bao.toByteArray());
            log.debug("GZip Flush");
            gout.flush();
            log.debug("GZip Close");
            gout.close();

        } catch (SQLException ex) {
            log.error("SQLException TSGetImageNode: "+ex.getMessage());
        } catch (Exception e) {
            log.error("Exception TSGetImageNode: "+e.getMessage());
        } finally {
            if(rs!=null){
                try {
                    rs.close();
                } catch (SQLException ex) {
                   log.error("error: rs.close() class: TSGetImageNode");
                }
            }
            if(stmt!=null){
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    log.error("error: smtp.close() class: TSGetImageNode");
                }
            }
        }
    }
    private static String validateString(String str){
        str=str.replace("||", "");
        str=str.replace("^", "");
        return str;
    }
}

