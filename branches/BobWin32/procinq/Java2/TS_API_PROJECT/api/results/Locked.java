package api.results;

import java.sql.*;

// LOCKED class
//
// This class is used to govern records in the NodeDocumentLocked table.  All functions
//   are static functions.  The table is locked for Write only.

public class Locked {
    public static boolean NodeDocumentLocked (Connection dbc) throws Exception {
        Statement stmt = null; ResultSet rs = null;
        String sSQL = "select Machine from NodeDocumentLocked";
        int count = 0;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            while ( rs.next() ) { count++; }

            if (count == 0) { return false; }
            return true;

        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
    }

    public static boolean LockNodeDocument (Connection dbc, String MachineName) throws Exception {
        Statement stmt = null;
        String sSQL = "insert into NodeDocumentLocked values ('"+MachineName+"', SYSDATE)";

        try {
			stmt = dbc.createStatement();
			if (stmt.executeUpdate (sSQL) == 0) { return false; }

            return true;
        } catch (Exception e) { throw e; }
        finally {
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
    }

    public static boolean UnlockNodeDocument (Connection dbc) throws Exception {
        Statement stmt = null;
        String sSQL = "delete from NodeDocumentLocked";
        int count = 0;

        try {
			stmt = dbc.createStatement();
			if (stmt.executeUpdate (sSQL) == 0) { return false; }

            return true;
        } catch (Exception e) { throw e; }
        finally {
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
    }

}
