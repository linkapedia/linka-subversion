package api.results;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.indraweb.util.UtilStrings.*;

import com.iw.system.User;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

public class Query
{
	private String sQuery = "";
	private String sTitleQuery = "";
	private String sRelatedQuery = "";
	private String sSignatureQuery = "";
	private String sThesaurusQuery = "";
	private Hashtable htResults;
	boolean Related = false;
	boolean Thesaurus = false;
	boolean bPrintSQL = false;
	
	// Accessor methods to private variables
	public String GetQuery() { return sQuery; }
	public String GetTitleQuery() { return sTitleQuery; }
	public String GetRelatedQuery() { return sRelatedQuery; }
	public String GetSignatureQuery() { return sSignatureQuery; }
	public String GetThesaurusQuery() { return sThesaurusQuery; }
	public Hashtable GetHashResults() { return htResults; }

	public boolean PrintOutSQL() { bPrintSQL = true; return bPrintSQL; }

	// This function is used when there are multiple layers of parenthesis in 
	// the search query.  Append recursive layer of hash terms to current layer
	private void AppendHashtable (Hashtable ht) {
		Enumeration e = ht.keys();
		
		while (e.hasMoreElements()) {
			String sK = (String) e.nextElement();
			NodeResult nV = (NodeResult) ht.get(sK);
			
			htResults.put(sK, nV);
		}			
	}
	
	// Object constructor
	public Query (String Query) { this.sQuery = Query; }

	// Use generated oracle query string to get search results
	//
	// There are three search modes.  Straight title search, related search, and 
	//  thesaurus search.   Title search is always on.   The other queries are optional,
	//  and located in local booleans.   Run each query as necessary and concat. results.  Then
	//  display concat. results at the end of this function.
	public int RunQuery(PrintWriter out, Connection dbc, User u) { return RunQuery(out, dbc, u, 100000); }
	public int RunQuery(PrintWriter out, Connection dbc, User u, int iMaxResults) {
		Hashtable htNodeResults = new Hashtable();
        int loop = 0;

		try {
			long lStart = System.currentTimeMillis();
            Hashtable htGetAuthorizedHash = u.GetAuthorizedHash(out);

            if (!Related) {
                // Title search
                String sSQLtitle = com.iw.system.Node.getSQL(dbc)+ " where NodeStatus != -1 and NodeID in ( "+sTitleQuery+" )";

                if (bPrintSQL) { out.println("<QUERY>Title: "+sSQLtitle+"</QUERY>"); }
                Statement stmttitle = dbc.createStatement();
                ResultSet rstitle = stmttitle.executeQuery (sSQLtitle);

                long lStop = System.currentTimeMillis() - lStart;
                //out.println("<!-- Timing: "+lStop+" ms -->");

                while (rstitle.next()) {
                    com.iw.system.Node n = new com.iw.system.Node(rstitle);
                    if ( htGetAuthorizedHash.get (n.get("CORPUSID")) != null	&& (loop <= iMaxResults )) {
                        loop = loop + 1;
                        if (loop == 1) { out.println ("<NODES> "); }
                        out.println ("   <NODE> ");
                        n.emitXML(out, u, false);
                        out.println ("   <RESULTTYPE>0</RESULTTYPE>");
                        out.println ("   </NODE>");
                        lStop = System.currentTimeMillis() - lStart;
                        //out.println("<!-- Print Results - Timing: "+lStop+" ms -->");
                    }
                }

                rstitle.close();
                stmttitle.close();
            }

			lStart = System.currentTimeMillis();
			if (Related && (loop < iMaxResults)) {
				String sSQL = com.iw.system.Node.getSQL(dbc)+" where (NodeStatus is null or NodeStatus != -1) and NodeID in ( "+sSignatureQuery+" )";

				if (bPrintSQL) { out.println("<QUERY>Signature: "+sSQL+"</QUERY>"); }
				Statement stmt = dbc.createStatement();	
				ResultSet rs = stmt.executeQuery (sSQL);
			
				while (rs.next()) {
                    if (loop < iMaxResults) {
                        loop++;

                        if (loop == 1) out.println("<NODES>");
                        com.iw.system.Node n = new com.iw.system.Node(rs);

                        out.println("  <NODE>");
					    n.emitXML(out, u, false);
                        out.println("     <RESULTTYPE>2</RESULTTYPE>");
                        out.println("  </NODE>");
					}
				}
				rs.close();
			    stmt.close();
			}
			long lStop = System.currentTimeMillis() - lStart;
			//out.println("<!-- Timing: "+lStop+" ms -->");

			if (Thesaurus && (loop < iMaxResults)) {
				String sSQL = com.iw.system.Node.getSQL(dbc)+ " where NodeStatus != -1 and NodeID in ( "+sThesaurusQuery+" )";

				if (bPrintSQL) { out.println("<QUERY>Thesaurus: "+sSQL+"</QUERY>"); }
				Statement stmt = dbc.createStatement();	
				ResultSet rs = stmt.executeQuery (sSQL);
			
				while (rs.next()) {
                    com.iw.system.Node n = new com.iw.system.Node(rs);
					if ( htGetAuthorizedHash.get (n.get("CORPUSID")) != null	&& (loop <= iMaxResults )) {
						loop = loop + 1;
						if (loop == 1) { out.println ("<NODES> "); }
						out.println ("   <NODE> ");
                        n.emitXML(out, u, false);
						out.println ("   <RESULTTYPE>1</RESULTTYPE>");
						out.println ("   </NODE>");
						lStop = System.currentTimeMillis() - lStart;
						//out.println("<!-- Print Results - Timing: "+lStop+" ms -->");	
					}
				}
				rs.close();
			    stmt.close();
			}
			lStart = System.currentTimeMillis();				
		
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Sorry no matches. </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}

			out.println ("</NODES> ");
			lStop = System.currentTimeMillis() - lStart;

			//out.println("<!-- Timing: "+lStop+" ms -->");
		}
		
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );  } 
		catch ( Exception e) { 
			e.printStackTrace(out);
            api.Log.LogError(e);
			EmitGenXML_ErrorInfo.emitException ( "TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION), out );
		}
        return loop;
	}
	
	// Build Oracle Query String for Titles from Boolean Query
	
	// Sample query:
	// (solution+or+liquid)+and+device
	
	public String BuildTitleQuery(String CorpusID) {
		sTitleQuery = " select distinct (nodeid) from node where ";
		
		// Restrict search to a specific corpus ID if one is supplied
		if (!CorpusID.equals("0")) { sTitleQuery = sTitleQuery + "CorpusID = "+CorpusID+" and "; }
		sTitleQuery = sTitleQuery + "(";
		
		char[] cArr = new char[sQuery.length()+1];
		
		sQuery.getChars(0, sQuery.length(), cArr, 0);
		for (int i = 0; i < sQuery.length(); i++) {
			
			// welcome and boolean
			if ((cArr[i] == '(') || (cArr[i] == ')')) { sTitleQuery = sTitleQuery + " "+cArr[i]; }
			else {
				int j = i; // get temporary pointer
				Hashtable ht = GetNextWord(cArr, i, j, sQuery);
				Enumeration eH = ht.keys();
				String sWord = (String) eH.nextElement();
				//api.Log.Log("Found word: "+sWord);
				j = new Integer((String) ht.get(sWord)).intValue();
							  
				if (!sWord.equals("")) {
					if ((sWord.toUpperCase().equals("AND")) || 
						(sWord.toUpperCase().equals("OR")) || 
						(sWord.toUpperCase().equals("NOT"))) { 
						sTitleQuery = sTitleQuery + " "+sWord; 
					}
					else {
						sTitleQuery = sTitleQuery +
									  " (nodeltitle like lower('"+sWord+" %') or "+
									  " nodeltitle like lower('% "+sWord+"') or "+
									  " nodeltitle like lower('% "+sWord+" %') or "+
									  " nodeltitle = lower('"+sWord+"'))";
						i=j+1;
						ht = GetNextWord(cArr, i, i, sQuery);
						eH = ht.keys();
						String sNextWord = (String) eH.nextElement();
						//api.Log.Log("Processed so far: "+sTitleQuery);
						//api.Log.Log(".. and next word is: "+sNextWord);
						
						if (!((sNextWord.toUpperCase().equals("AND")) || 
							(sNextWord.toUpperCase().equals("OR")) || 
							(sNextWord.toUpperCase().equals("NOT")))) { 
							if (cArr[j] == ' ') { sTitleQuery = sTitleQuery + " AND "; }
						}
					}
					i = j;
				}
			}
		}
		
		sTitleQuery = sTitleQuery + ")";
		return sTitleQuery+"";
	}

	// Build Oracle Query String for Related from Boolean Query
	
	// Sample query:
	// (solution+or+liquid)+and+device
	public String BuildRelatedQuery(String CorpusID) {
		Related = true;
		sRelatedQuery = " select distinct (s.nodeid) from ThesaurusWord tw1, ThesaurusWord tw2, "+
		 				  " ThesaurusRelations tr, signature s";
		if (!CorpusID.equals("0")) { sRelatedQuery = sRelatedQuery + ", CorpusThesaurus c"; }
		
		sRelatedQuery = sRelatedQuery + " where tr.Wordanchor = tw1.WordID and tr.WordRelated = tw2.WordID and ";
		
		// Restrict search to a specific corpus ID if one is supplied
		if (!CorpusID.equals("0")) { 
			sRelatedQuery = sRelatedQuery + "c.CorpusID = "+CorpusID+" and C.ThesaurusID = tr.ThesaurusID and ";	
		}

		char[] cArr = new char[sQuery.length()+1];
		
		sQuery.getChars(0, sQuery.length(), cArr, 0);
		for (int i = 0; i < sQuery.length(); i++) {
			if ((cArr[i] == '(') || (cArr[i] == ')')) { sRelatedQuery = sRelatedQuery + " "+cArr[i]; }
			else {
				int j = i; // get temporary pointer
				Hashtable ht = GetNextWord(cArr, i, j, sQuery);
				Enumeration eH = ht.keys();
				String sWord = (String) eH.nextElement();
				//api.Log.Log("Found word: "+sWord);
				j = new Integer((String) ht.get(sWord)).intValue();
				
				if (!sWord.equals("")) {
					if ((sWord.toUpperCase().equals("AND")) || 
						(sWord.toUpperCase().equals("OR")) || 
						(sWord.toUpperCase().equals("NOT"))) { 
						sRelatedQuery = sRelatedQuery + " "+sWord; 
					}
					else {
						sRelatedQuery = sRelatedQuery + " lower(tw1.thesaurusword) = lower('"+sWord+"') and ("+
									  " lower(s.signatureword) = lower(tw2.thesaurusword))";
						i=j+1;
						ht = GetNextWord(cArr, i, i, sQuery);
						eH = ht.keys();
						String sNextWord = (String) eH.nextElement();
						//api.Log.Log("Processed so far: "+sRelatedQuery);
						//api.Log.Log(".. and next word is: "+sNextWord);
						
						if (!((sNextWord.toUpperCase().equals("AND")) || 
							(sNextWord.toUpperCase().equals("OR")) || 
							(sNextWord.toUpperCase().equals("NOT")))) { 
							if (cArr[j] == ' ') { sRelatedQuery = sRelatedQuery + " AND "; }
						}
					}
					i = j;
				}
			}
		}
		
		sSignatureQuery = " select distinct (nodeid) from signature where ";

		// Restrict search to a specific corpus ID if one is supplied
		if (!CorpusID.equals("0") && !CorpusID.equals("")) {
			sSignatureQuery = sSignatureQuery + "nodeid IN (select nodeid from node where "+
							  " CorpusID = "+CorpusID+") and "; 
		}
		sSignatureQuery = sSignatureQuery + "( ";

		cArr = new char[sQuery.length()+1];
		
		sQuery.getChars(0, sQuery.length(), cArr, 0);
		for (int i = 0; i < sQuery.length(); i++) {
			if ((cArr[i] == '(') || (cArr[i] == ')')) { sSignatureQuery = sSignatureQuery + " "+cArr[i]; }
			else {
				int j = i; // get temporary pointer
				Hashtable ht = GetNextWord(cArr, i, j, sQuery);
				Enumeration eH = ht.keys();
				String sWord = (String) eH.nextElement();
				//api.Log.Log("Found word: "+sWord);
				j = new Integer((String) ht.get(sWord)).intValue();
				
				if (!sWord.equals("")) {
					if ((sWord.toUpperCase().equals("AND")) || 
						(sWord.toUpperCase().equals("OR")) || 
						(sWord.toUpperCase().equals("NOT"))) { sSignatureQuery = sSignatureQuery + " "+sWord; }
					else {
						sSignatureQuery = sSignatureQuery + " contains(signatureword, '"+sWord+"')>0 ";
						i=j+1;
						ht = GetNextWord(cArr, i, i, sQuery);
						eH = ht.keys();
						String sNextWord = (String) eH.nextElement();
						//api.Log.Log("Processed so far: "+sSignatureQuery);
						//api.Log.Log(".. and next word is: "+sNextWord);
						
						if (!((sNextWord.toUpperCase().equals("AND")) || 
							(sNextWord.toUpperCase().equals("OR")) || 
							(sNextWord.toUpperCase().equals("NOT")))) { 
							if (cArr[j] == ' ') { sSignatureQuery = sSignatureQuery + " AND "; }
						}
					}
					i = j;
				}
			}
		}

		sSignatureQuery = sSignatureQuery + ")";
		//api.Log.Log("Signature query: "+sSignatureQuery);

		return sRelatedQuery+"";
	}
	
	// Build Oracle Query String for Thesauri from Boolean Query
	
	// Sample query:
	// (solution+or+liquid)+and+device
	
	public String BuildThesaurusQuery(String CorpusID) {
		Thesaurus = true;
		sThesaurusQuery = " select distinct (n.nodeid) from ThesaurusWord tw1, ThesaurusWord tw2, "+
		 				  " ThesaurusRelations tr, node n";
		if (!CorpusID.equals("0")) { sThesaurusQuery = sThesaurusQuery + ", CorpusThesaurus c"; }

		sThesaurusQuery = sThesaurusQuery + " where tr.Wordanchor = tw1.WordID and tr.WordRelated = tw2.WordID and ";

		// Restrict search to a specific corpus ID if one is supplied
		if (!CorpusID.equals("0")) { 
			sThesaurusQuery = sThesaurusQuery + "c.CorpusID = "+CorpusID+" and c.ThesaurusID = tr.ThesaurusID and ";	
		}

		char[] cArr = new char[sQuery.length()+1];
		
		sQuery.getChars(0, sQuery.length(), cArr, 0);
		for (int i = 0; i < sQuery.length(); i++) {
			if ((cArr[i] == '(') || (cArr[i] == ')')) { sThesaurusQuery = sThesaurusQuery + " "+cArr[i]; }
			else {
				int j = i; // get temporary pointer
				Hashtable ht = GetNextWord(cArr, i, j, sQuery);
				Enumeration eH = ht.keys();
				String sWord = (String) eH.nextElement();
				//api.Log.Log("Found word: "+sWord);
				j = new Integer((String) ht.get(sWord)).intValue();
				
				if (!sWord.equals("")) {
					if ((sWord.toUpperCase().equals("AND")) || 
						(sWord.toUpperCase().equals("OR")) || 
						(sWord.toUpperCase().equals("NOT"))) { sThesaurusQuery = sThesaurusQuery + " "+sWord; }
					else {
						sThesaurusQuery = sThesaurusQuery + " tw1.thesaurusword = lower('"+sWord+"') and ("+
									  " n.nodeltitle like   tw2.thesaurusword || ' %' or "+
									  " n.nodeltitle like ' %' || tw2.thesaurusword or "+
									  " n.nodeltitle like ' %' || tw2.thesaurusword || ' %' or "+
									  " n.nodeltitle =      tw2.thesaurusword)";
						i=j+1;
						ht = GetNextWord(cArr, i, i, sQuery);
						eH = ht.keys();
						String sNextWord = (String) eH.nextElement();
						//api.Log.Log("Processed so far: "+sThesaurusQuery);
						//api.Log.Log(".. and next word is: "+sNextWord);
						
						if (!((sNextWord.toUpperCase().equals("AND")) || 
							(sNextWord.toUpperCase().equals("OR")) || 
							(sNextWord.toUpperCase().equals("NOT")))) { 
							if (cArr[j] == ' ') { sThesaurusQuery = sThesaurusQuery + " AND "; }
						}
					}
					i = j;
				}
			}
		}
		
		return sThesaurusQuery+"";
	}
	public Hashtable GetNextWord (char[] cArr, int i, int j, String sQuery) {
		Hashtable ht = new Hashtable();
		String sWord = "";
		
		if (j >= cArr.length) { ht.put("", j+""); return ht; }
		
		if (cArr[j] == '"') {
			j++;
			while ((cArr[j] != '"') && (j < sQuery.length())) { j++; }
			sWord = new String().copyValueOf(cArr, i+1, ((j-i)-1));
		} else {
			while ((cArr[j] != ' ') && (cArr[j] != ')') && (j < sQuery.length())) { j++; }
			sWord = new String().copyValueOf(cArr, i, (j-i));
			if (cArr[j] == ')') { j=j-1; }
		}	

		ht.put(sWord, j+"");
		return ht;
	}
}
