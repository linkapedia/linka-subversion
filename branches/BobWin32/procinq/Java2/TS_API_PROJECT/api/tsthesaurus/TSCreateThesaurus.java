package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 *  Create a new thesaurus in the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusName   Name of the new thesaurus to be created
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSCreateThesaurus&ThesaurusName=Test&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Thesaurus created successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSCreateThesaurus
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sThesaurusName = (String) props.get ("ThesaurusName", true);
		
		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " insert into Thesaurus (ThesaurusName) values ('"+sThesaurusName+"')";
			Statement istmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (istmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Attempt to add new thesaurus failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
		    istmt.close();

			sSQL = " select ThesaurusID from Thesaurus where ThesaurusName = '"+sThesaurusName+"'";
			Statement stmt = dbc.createStatement();
			
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				
				int iThesID = rs.getInt(1);
				
				out.println ("   <THESAURUS> ");
				out.println ("      <THESAURUSID>"+iThesID+"</THESAURUSID>");
				out.println ("      <THESAURUSNAME>"+sThesaurusName+"</THESAURUSNAME>");
				out.println ("   </THESAURUS>");
			}
			rs.close(); 
		    stmt.close();
			out.println("<SUCCESS>Thesaurus created successfully.</SUCCESS>");
			
			if (loop == 0) { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CREATE_GENRE_FAILURE); }
		}
		
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
