package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove an anchor term from the given thesaurus.  Removing this term will remove all associations and relationships.
 *  Note removing an anchor term will automatically purge the signature cache of any corpus using the given thesaurus.
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusID   Unique thesaurus identifier
 *	@param  WordID    Unique thesaurus term identifier.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSRemoveWordAnchor&WordID=410&ThesaurusID=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Anchor term removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveWordAnchor
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sThID = (String) props.get ("ThesaurusID", true);
		String sWordID = (String) props.get ("WordID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Statement stmt = null;

		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            // Ensure user is a member of the admin group
			if (!u.IsMember(out)) {
                out.println("<NOTAUTHORIZED>You are not authorized to use this function</NOTAUTHORIZED>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
			
			String sSQL = " delete from ThesaurusRelations where ThesaurusID = "+sThID+" and "+
                          " (WordAnchor = "+sWordID+" or WordRelated = "+sWordID+")";

            try {
                stmt = dbc.createStatement();

                // If query statement failed, throw an exception
                if (stmt.executeUpdate (sSQL) == 0) {
                    out.println ( "<DEBUG>Delete from ThesaurusRelations table failed</DEBUG>");
                    throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
                }
            } catch (Exception e) { throw e; }
            finally { if (stmt != null) { stmt.close(); stmt = null; }}

			// purge signature cache files
			api.tspurge.TSPurgeSigCache.PurgeThesaurusSignatures(sThID, out, dbc);

			stmt.close();
            out.println("<SUCCESS>Anchor term removed successfully.</SUCCESS>");
		}
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
