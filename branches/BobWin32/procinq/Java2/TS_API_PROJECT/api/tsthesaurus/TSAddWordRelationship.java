package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Add a relationship between two thesaurus terms within a given thesaurus context.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusID   Unique thesaurus identifier
 *  @param  Word1   Anchor term from whom the relationship is defined.
 *  @param  Word2   Object term of the relationship.
 *  @param  Relationship    Relationship type, 1 is a synonym, 2 is a broader term and 3 is a narrower term
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSAddWordRelationship&ThesaurusID=1&Word1=couch&Word2=sofa&Relationship=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Word relationship added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddWordRelationship
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sThID = (String) props.get ("ThesaurusID", true);
		String sWord1 = (String) props.get ("Word1", true);
		String sWord2 = (String) props.get ("Word2", true);
		String sRelationship = (String) props.get ("Relationship", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) {
                out.println("<NOTAUTHORIZED>You are not authorized to use this function</NOTAUTHORIZED>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
				
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            // Get or create the words in our database
			int iWordID1 = CreateThesaurusWord(sWord1, dbc);		
			int iWordID2 = CreateThesaurusWord(sWord2, dbc);		
			
			if ((iWordID1 == 0) || (iWordID2 == 0)) {
				out.println("<DEBUG>Error creating or select word identifiers</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
			
			// Assign the relationship
			String sSQL = " insert into ThesaurusRelations (ThesaurusId, WordAnchor, WordRelated, Relationship) "+
						  " values ("+sThID+","+iWordID1+", "+iWordID2+", "+sRelationship+")";
			Statement stmt = dbc.createStatement();	

			try {
                stmt.executeUpdate (sSQL);
                out.println("<SUCCESS>Word relationship added successfully.</SUCCESS>");
            }
			catch (Exception e) {
				out.println ( "<DEBUG>Relationship already exists ("+sThID+","+iWordID1+", "+iWordID2+", "+sRelationship+") </DEBUG>"); 
				stmt.close();
				sSQL = " update ThesaurusRelations set Relationship = "+sRelationship+" where ThesaurusID"+
					   " = "+sThID+" and WordAnchor = "+iWordID1+" and WordRelated = "+iWordID2;
				stmt = dbc.createStatement();	

				if (stmt.executeUpdate (sSQL) == 0) { 
					out.println("<DEBUG>Update failed as well.   There is an error in database interaction.</DEBUG>");
                    api.Log.LogError("FAILURE! :"+sSQL);
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
				}
			}
			// purge signature cache files
			api.tspurge.TSPurgeSigCache.PurgeThesaurusSignatures(sThID, out, dbc);

		    stmt.close();
			
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
	
	public static int CreateThesaurusWord (String sThesWord, Connection dbc) {
		
		// If the thesaurus word contains or ' characters, escape them out properly
		sThesWord = new String(com.indraweb.util.UtilStrings.replaceStrInStr
			(sThesWord,"'","''"));
		
		// Insert the word into the database.  
		try {
			String sSQL = "insert into ThesaurusWord (ThesaurusWord) values (LOWER('"+sThesWord+"'))";
			Statement stmt = dbc.createStatement();	
			stmt.executeUpdate(sSQL); stmt.close();
		} catch (Exception e) {} // If it fails, assume word already exists

		// Attempt to retrieve WORD IDENTIFIER for this word
		try {
			String sSQL="select WORDID from ThesaurusWord where LOWER(thesaurusword)=LOWER('"+sThesWord+"')";
			Statement stmt = dbc.createStatement();	ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();  
			int iReturn = rs.getInt(1);
			rs.close();
			stmt.close();
			return iReturn;
		} catch (Exception e) { api.Log.LogError("err in select WORDID from ThesaurusWord", e); return -1; }		
	}
}
