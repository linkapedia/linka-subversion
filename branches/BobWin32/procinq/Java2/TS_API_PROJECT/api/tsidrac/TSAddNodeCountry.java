package api.tsidrac;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Adds a country relationship to an existing IDRAC node object.  IDRAC nodes may contain more than one country.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  The unique node identifier of the node relationship being added.
 *  @param  Country (IDRAC attribute) The country relationship that is being added.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsidrac.TSNodeCountry&SKEY=-132981656&NodeID=48413&Country=US

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node country relationship created successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddNodeCountry
{
	// add a node country relationship (idrac only)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
        String sCountry = (String) props.get ("Country", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null; ResultSet rs = null;
		try {
            // check authorization level before completing this irreversable action
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL); rs.next();

			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
            rs = null; stmt = null;

			if ((u == null) || (!u.IsAdmin(iCorpusId, out))) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            sSQL = "insert into IdracNode (NodeID, NodeCountry) "+
                          "values ("+sNodeID+", '"+sCountry+"')";
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Error creating node country relationships.</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
            out.println("<SUCCESS>Node country relationship created successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (rs != null) { rs.close(); }
            if (stmt != null) { stmt.close(); }
        }
	}
}
