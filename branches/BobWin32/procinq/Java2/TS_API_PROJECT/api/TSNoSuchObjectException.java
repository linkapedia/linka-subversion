package api;

import api.emitxml.EmitGenXML_ErrorInfo;

public class TSNoSuchObjectException extends TSException
{
	public TSNoSuchObjectException ( String s )
	{
		super ( EmitGenXML_ErrorInfo.ERR_TS_NO_SUCH_OBJECT, s );
	} 
}
