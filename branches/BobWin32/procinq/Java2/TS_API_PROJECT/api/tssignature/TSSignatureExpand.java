package api.tssignature;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.tsnode.TSGetNodeSigs;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.signatures.SigExpandersTitleThesaurus;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.signatures.SignaturesContainer;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Vector;

/**
 * Get the thesaurus and title expanded version of a sig vector for passing into classify.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param Sigs String sequence indicating counts and terms. Example: "3,term1|||5,term2|||7,term3"
 * @param NodeID Identifies the node for parent and self title get
 * @param CorpusID Identifies the set of thesauri
 *
 * @note http://itsserver/servlet/ts?fn=tsthesaurus.TSSignatureExpand&CorpusID=4&Sigs=3,term1|||5,term2 is phrase||thesterm21||thesterm22 is phrase|||7,term3||thesterm31&SKEY=993135977
 *
 * @return	SUCCESS tag if successful. \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <SUCCESS>Corpus thesaurus relationship added successfully.</SUCCESS> </TSRESULT> \endverbatim
 */
public class TSSignatureExpand {

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        boolean bDebugging = false;
        String sNodeId = (String) props.get("NodeID", true);
        String sCorpusID = (String) props.get("CorpusID", true);
        String sSigs = (String) props.get("Sigs", true);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            if (!bDebugging && !com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            if (!bDebugging && !u.IsAdmin(sCorpusID, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }


            Vector vStrSigWords = new Vector();
            Vector vIntSigCounts = new Vector();

            SignatureParms sigParms = new SignatureParms(props);
            //IntContainer ic_out_titleCount = new IntContainer();
            SigExpandersTitleThesaurus.vecTermsFromString(
                    //sNodeTitle,
                    sSigs,
                    vStrSigWords,
                    vIntSigCounts //ic_out_titleCount
                    );

            //api.Log.Log ("vStrSigWords post title [" +  UtilSets.vToStr(vStrSigWords)+ "]" );
            // add title to words and add also thesaurus terms
            String sNodeTitle = JDBCIndra_Connection.executeQueryAgainstDBStr("select nodetitle from node where nodeid = " + sNodeId, dbc);
            int iNodeIDParent = (int) JDBCIndra_Connection.executeQueryAgainstDBLong("select parentid from node where nodeid = " + sNodeId, dbc);
            if (iNodeIDParent > 0) {
                JDBCIndra_Connection.executeQueryAgainstDBStr("select nodetitle from node where nodeid = " + iNodeIDParent, dbc);
            }

            SignaturesContainer sigCont = SigExpandersTitleThesaurus.getADefaultExpansion(
                    vStrSigWords,
                    vIntSigCounts,
                    sNodeTitle,
                    Integer.parseInt(sCorpusID),
                    iNodeIDParent,
                    dbc,
                    sigParms.iNumParentTitleTermsAsSigs);

            //api.Log.Log ("TSSignatureExpand done [" + StringAndCount.toString(vStrSigWords, vIntSigCounts) + "]" );
            TSGetNodeSigs.emitTermAndThesSigs(
                    sigCont.getVStr(),
                    sigCont.getVInt(),
                    //vArrUnderThesWords,
                    "NA",
                    out,
                    0);


            /*
             * <ERRORTERM ID=errorcode> <ERRORDESC>Error Description</ERRORDESC> </ERRORTERM>
             */

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}
