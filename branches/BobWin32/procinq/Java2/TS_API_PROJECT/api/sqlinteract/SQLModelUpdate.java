package api.sqlinteract;


import java.util.Properties;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.util.UtilStrings;

import api.APIProps;

public class SQLModelUpdate
{
	
	
	public static String genSQLUpdate ( String sTableName, 
										APIProps propsNameValuePairs, 
										String[] sArrFields, 
										String[] sArrFields_DBNames,
										int iNumRequiredFields
											 )
		throws TSException
	{
		
		
		for ( int loop = 0; loop < sArrFields.length; loop++ ) {
			// Throw an exception if ANY of the optional parameters were passed
			if ( loop < iNumRequiredFields ) {
				if ( propsNameValuePairs.get (sArrFields[loop]) == null )
				{
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS, "missing field : " + sArrFields[loop]);
				}
			}				
		}

		// Most of the parameters to this function are OPTIONAL
		// Build a query string based on the optional parameters
		StringBuffer sb = new StringBuffer ();
		sb.append  ("update " + sTableName + " set " );

			
		// add all the field=value pairs
		boolean bAnyFieldsYet = false;
		for ( int loop = iNumRequiredFields; loop < sArrFields.length; loop++ )
		{
			String sValue = (String) propsNameValuePairs.get (sArrFields[loop]);
			if ( sValue != null )
			{
				if ( bAnyFieldsYet )
					sb.append(", ");
				sValue = UtilStrings.replaceStrInStr ( sValue,"'", "''");
				if (!sValue.toUpperCase().equals("SYSDATE")) {
					sb.append ( sArrFields_DBNames[loop] + " = '" + 
								sValue + "'" );
				} else {
					sb.append ( sArrFields_DBNames[loop] + " = " + 
								sValue );
				}
				bAnyFieldsYet = true;
			}
		} 
			
		if ( !bAnyFieldsYet )
		{ 
			// throw new TSException ( EmitGenXML_ErrorInfo.ERR_TS_NO_UPDATE_FIELDS );
			// Do not throw an exception if no fields are to be updated -- this function
			//  is often invoked from within other API functions and we do not want to 
			//  drop into an exception condition for this.   Just return a return code.
			return new String("");
		}
		else
		{
			// Tack on the WHERE clause
			sb.append (" where ");
			
			bAnyFieldsYet = false;
			for ( int loop = 0; loop < iNumRequiredFields; loop++ )
			{
				String sValue = (String) propsNameValuePairs.get (sArrFields[loop]);
				if ( bAnyFieldsYet )
					sb.append(" and ");
				
				sValue = UtilStrings.replaceStrInStr ( sValue,"'", "''");
				sb.append ( sArrFields_DBNames[loop] + " = " + "'"+ 
							 sValue + "'" );
				bAnyFieldsYet = true;
			} 
			
		}
		
		return sb.toString();
	}
}
