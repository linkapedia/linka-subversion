package api.tssubjectarea;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove a subject area, along with all of its corpus relationships, from the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  SubjectAreaID   Unique subject area identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSRemoveSubjectArea&SubjectAreaID=1&SKEY=993135977

 *	@return	SUCCESS tag if relationship was successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Subject area removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveSubjectArea
{
	// TSRemoveFolder (sessionid, folderid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sSubjectAreaID = (String) props.get ("SubjectAreaID", true);

        Statement stmt = null;
		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = "delete from SubjectArea where SubjectAreaID = "+sSubjectAreaID;
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from SubjectArea table failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_GENRE_DELETE_FAILURE);
			}

			out.println("<SUCCESS>Subject area removed successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
