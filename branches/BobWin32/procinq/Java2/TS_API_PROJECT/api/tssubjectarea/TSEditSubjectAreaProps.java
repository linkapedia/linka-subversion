package api.tssubjectarea;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 *  Edit the properties of a given subject area in the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  SubjectAreaID   Unique identifier of the target subject area.
 *	@param  SubjectName (optional)  Name of the subject area to be created.
 *  @param  SubjectAreaStatus (optional)   Initial status of the subject area
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSEditSubjectAreaProps&SubjectName=Test&SubjectAreaID=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Subject Area modified successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditSubjectAreaProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// NOTE : required fields MUST appear first in the list above.
		String sTableName = "subjectarea";
		int iNumRequiredFields = 1;
		String[] sArrFields_URLNameSpace = {
			"SubjectAreaID",		// required
			"SubjectName",
			"SubjectAreaStatus" };

		String[] sArrFields_DBNameSpace = {
			"SubjectAreaID",		// required
			"SubjectName",
			"SubjectAreaStatus" };

		try
		{
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			String sSQL = api.sqlinteract.SQLModelUpdate.genSQLUpdate ( sTableName,
														  props,
														  sArrFields_URLNameSpace,
														  sArrFields_DBNameSpace,
														  iNumRequiredFields
															);

			Statement stmt = null;
			try
			{
				stmt = dbc.createStatement();

				// If query statement failed, throw an exception
				if (stmt.executeUpdate (sSQL) == 0) {
					out.println ( "<DEBUG>The specified record does not exist to update</DEBUG>");
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
				}
				out.println("<SUCCESS>Subject Area modified successfully.</SUCCESS>");
			}
			catch ( Exception e )
			{
				throw new TSException (	EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION,
					"e.msg ["	+ e.getMessage() + "] " +
					" SQL ["	+ sSQL.toString() + "] " +
					" e.stk ["	+ api.Log.stackTraceToString(e) + "]" );
			}
			finally
			{
				out.println (EmitValue.genValue ("DEBUG" , "edit update operation complete"  ));
				stmt.close();
			}
		}
		// Catch exception
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
