package api.version;

import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilArgConfigProp;

import java.util.Vector;
import java.util.Properties;
import java.util.Hashtable;
import java.io.FileInputStream;
/*
   VersionManager.class

   This class is a command line utility too query and update build version numbers for all Indraweb
   builds.

    Example Runs :
        java api.version.VersionManager [-getVersions|-incrementBuild] -version Client -folder


   Optional input arguments:
	   Corpora				-	A comma separated list indicating the corpora to classify
							    against.   Defaults to configuration file if not provided.
	   Genre				-	Specify the genre (folder) identifier for this document set
	   Config				-   Location of configuration file containing API and thread info
       Batch			    -   Run FCrawl in BATCH mode?  Default is false
	   DocSumm				-   Create document summaries ? Default is true
	   ErrorRun				-	Re-run all documents in the problem queue (C:\problem-documents.log)

   Web Crawl specific optional input arguments:
       EndTimes             -   (WebCache only) Specify the maximum amount of time to run for (currently used only by WebCache)
       Restore              -   (WebCache only) Restore state from file
       Interval             -   (Web only) Minimum interval (in seconds) from which to grab files PER PROXY (if applicable)
       ProxyConfig          -   (Web only) Points to a configuration file a series of proxy servers to download
                                documents, used for IP spoofing.
       DocumentID           -   (PubMed only) Specify the document identifier starting point (descending).
       StopID               -   (PubMed only) Specify the document identifier stopping point (descending).

    UPDATES:
        Created 2004 06 23


*/

public class VersionManager {

    public static void Main (String[] sArrArgs) throws Exception
    {
        Hashtable htArgs = UtilArgConfigProp.getArgHash(sArrArgs);

        if ( sArrArgs.length == 0 )
        {
            System.out.println("Indrweb Version Control Utility");
            System.out.println("java api.version.VersionManager " +
                "{-getinfo|-incrementbuild|-incrementversion} "+
                "[-buildtype {client|server}] " +
                "[-versionfile filelocation");
        }





    }

    public String getVersionClient ()  throws Exception
    {
        return null;
    }

    public String getVersionServer ()  throws Exception
    {
        return null;
    }

    public void buildTime_IncrementVersion ( String sHomeFolder, String s ) throws Exception
    {
        Properties props = new Properties();
        props.load(new FileInputStream ( sHomeFolder + "/" + "version.txt" ) );

    }
}
