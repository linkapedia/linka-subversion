package api.tsdictionary;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

public class TSCreateDictionary {
    private static final Logger log = Logger.getLogger(TSCreateDictionary.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("handleTSapiRequest(APIProps, PrintWriter, Connection");
        String[] wordList = null;
        PreparedStatement prest = null;
        ResultSet rs = null;
        int dictionaryID = 0;
        String sDictionaryName = null;
        String sDictionaryWords = null;


        try {
            sDictionaryName = (String) props.get("DictionaryName", true);
            log.debug("Getting dictionary name: " + sDictionaryName);
            sDictionaryWords = (String) props.get("DictionaryWord");
            log.debug("Getting dictionary words: " + sDictionaryWords);
            if (sDictionaryWords != null && !sDictionaryWords.trim().isEmpty()) {
                wordList = sDictionaryWords.split(",");
                log.debug("Getting dictionary word count: " + wordList.length);
            }

            /*
             * Verify the dictionary already in database
             */
            prest = dbc.prepareStatement("SELECT NAME FROM DICTIONARY WHERE NAME=?");
            prest.setString(1, sDictionaryName);
            rs = prest.executeQuery();
            if (!rs.next()) {
                prest = dbc.prepareStatement("INSERT INTO DICTIONARY(NAME) VALUES (?)");
                prest.setString(1, sDictionaryName);
                prest.executeUpdate();
                prest = dbc.prepareStatement("SELECT ID FROM DICTIONARY WHERE NAME=?");
                prest.setString(1, sDictionaryName);
                rs = prest.executeQuery();
                if (rs.next()) {
                    dictionaryID = rs.getInt(1);
                }

                /*
                 * Read data for the ingestion database 
                 */
                for (int i = 0; i < wordList.length; i++) {

                    prest = dbc.prepareStatement("INSERT INTO WORDS(DICTIONARYID,WORD) VALUES (?,?)");
                    prest.setInt(1, dictionaryID);
                    prest.setString(2, wordList[i]);
                    prest.executeQuery();

                }
            }
        } catch (SQLException e) {
            log.error("An exception ocurred trying to handle the request.", e);
        } catch (Exception e) {
            log.error("An exception ocurred trying to handle the request.", e);
        } /*
         * Close connection
         */ finally {

            if (dbc != null) {
                dbc.close();
            }
            if (prest != null) {
                prest.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}