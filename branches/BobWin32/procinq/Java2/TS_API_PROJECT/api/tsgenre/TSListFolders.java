package api.tsgenre;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 * Lists all of the folders for a given corpus.  If no corpora is given, then all of the folders are listed
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CorpusID (optional)    Unique corpus identifier.  This parameter will restrict folders returned by corpus.
 *
 *  @note   http://ITS/servlet/ts?fn=tsgenre.TSListFolders&CorpusID=100004&SKEY=1634835192

 *	@return ITS folder objects.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <GENRES>
            <GENRE>
                <GENREID>41</GENREID>
                <GENRENAME>Reference</GENRENAME>
                <GENRESTATUS>1</GENRESTATUS>
            </GENRE>
            <GENRE>
                <GENREID>42</GENREID>
                <GENRENAME>Explanatory</GENRENAME>
                <GENRESTATUS>1</GENRESTATUS>
            </GENRE>
            <GENRE>
                <GENREID>43</GENREID>
                <GENRENAME>Forms</GENRENAME>
                <GENRESTATUS>1</GENRESTATUS>
            </GENRE>
        </GENRES>
    <CLASSLOAD>Fri Jun 07 13:16:40 EDT 2002</CLASSLOAD>
    <CALLCOUNT>2818</CALLCOUNT>
    <TIMEOFCALL_MS>340</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSListFolders
{
	// TSListFolders (sessionid, corpusid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{

		// Corpus ID is optional
		String sCorpusID = (String) props.get ("CorpusID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			
			if (sCorpusID != null) {
                if ((!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) &&
                    (!u.IsAuthorized(sCorpusID, out))) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            }

			String sSQL = " select GenreID, GenreName, GenreStatus from Genre "; 
			if (sCorpusID != null) { sSQL = sSQL+" where GenreID IN (select GenreID from CorpusGenre "+
											" where CorpusID = "+sCorpusID+")"; }			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				int iGenreID = rs.getInt(1);
				String sGenreName = rs.getString(2);
				int iGenreStatus = rs.getInt(3);

				loop = loop + 1;
				if (loop == 1) { out.println ("<GENRES>"); }
				out.println ("   <GENRE> ");
				out.println ("      <GENREID>"+iGenreID+"</GENREID>");
				out.println ("      <GENRENAME>"+sGenreName+"</GENRENAME>");
				out.println ("      <GENRESTATUS>"+iGenreStatus+"</GENRESTATUS>");
				out.println ("   </GENRE>");
			}

			// If no results found, throw an exception

			rs.close();
		    stmt.close();

			if (loop > 0) {	out.println ("</GENRES>"); }
		}
		
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
