package api.emitxml;

import java.io.*;
import java.util.Vector;
import api.TSException;

public class EmitGenXML_ErrorInfo {

    private static Vector vErrCodeStringPairs = new Vector();
    public static int iErrIndex = 0;
    public static final int ERR_TS_SUCCESS = (new ErrPair(iErrIndex++, "SUCCESS")).getErrIndex();
    public static final int ERR_TS_ERROR = (new ErrPair(iErrIndex++, "General API error")).getErrIndex();
    public static final int ERR_TS_NO_UPDATE_FIELDS = (new ErrPair(iErrIndex++, "No fields provided for update")).getErrIndex();
    public static final int ERR_TS_JAVA_LANG_EXCEPTION = (new ErrPair(iErrIndex++, "java.lang.Exception")).getErrIndex();
    public static final int ERR_TS_GET_CORPUS_ERROR = (new ErrPair(iErrIndex++, "Get corpus error")).getErrIndex();
    public static final int ERR_TS_NO_SUBSCRIBERS_THIS_NODE = (new ErrPair(iErrIndex++, "No subscribers found for this node")).getErrIndex();
    public static final int ERR_TS_NO_NODES_FOUND_THISUSER = (new ErrPair(iErrIndex++, "No nodes found for this user")).getErrIndex();
    public static final int ERR_TS_NO_CORPORA_THIS_DB = (new ErrPair(iErrIndex++, "No corpora were found in this database")).getErrIndex();
    public static final int ERR_TS_CORPUS_NOT_FOUND = (new ErrPair(iErrIndex++, "Corpus not found")).getErrIndex();
    public static final int ERR_TS_NODE_NOT_FOUND = (new ErrPair(iErrIndex++, "Node not found")).getErrIndex();
    public static final int ERR_TS_NO_NODES_THIS_PARENT_NODE = (new ErrPair(iErrIndex++, "No nodes found for this parent node")).getErrIndex();
    public static final int ERR_TS_NO_INFO_THIS_DOC_NODE = (new ErrPair(iErrIndex++, "No information found for this document node combination")).getErrIndex();
    public static final int ERR_TS_NO_SIGS_THIS_NODE = (new ErrPair(iErrIndex++, "No signatures found for this node")).getErrIndex();
    public static final int ERR_TS_NO_ROOT_NODES_THIS_CORPUS = (new ErrPair(iErrIndex++, "No root nodes available for this corpus")).getErrIndex();
    public static final int ERR_TS_NO_NODES_THIS_CORPUS = (new ErrPair(iErrIndex++, "No nodes available for this corpus")).getErrIndex();
    public static final int ERR_TS_NO_MATCHES_ON_SEARCH = (new ErrPair(iErrIndex++, "No nodes matched your search request")).getErrIndex();
    public static final int ERR_TS_NO_SUCH_OBJECT = (new ErrPair(iErrIndex++, "No such API object or method")).getErrIndex();
    public static final int ERR_TS_NO_SUCH_METHOD_ON_KNOWN_OBJECT = (new ErrPair(iErrIndex++, "No such method on this known object")).getErrIndex();
    public static final int ERR_TS_DB_CONNECT_ERROR = (new ErrPair(iErrIndex++, "DB failure")).getErrIndex();
    public static final int ERR_TS_MISSING_PARAMETERS = (new ErrPair(iErrIndex++, "Invalid/Missing parameters")).getErrIndex();
    public static final int ERR_TS_CORPUS_GENRE_FAILURE = (new ErrPair(iErrIndex++, "Insert of Corpus Folder relationship failed")).getErrIndex();
    public static final int ERR_TS_CORPUS_GENRE_DELETE_FAILURE = (new ErrPair(iErrIndex++, "Delete of Corpus Folder relationship failed")).getErrIndex();
    public static final int ERR_TS_GENRE_DELETE_FAILURE = (new ErrPair(iErrIndex++, "Delete of Folder failed")).getErrIndex();
    public static final int ERR_TS_USER_NODE_FAILURE = (new ErrPair(iErrIndex++, "Insert of User Node relationship failed")).getErrIndex();
    public static final int ERR_TS_USER_NODE_DELETE_FAILURE = (new ErrPair(iErrIndex++, "Delete of User Node relationship failed")).getErrIndex();
    public static final int ERR_TS_EDIT_SUBSCRIBER_FAILURE = (new ErrPair(iErrIndex++, "Attempt to change subscriber properties failed")).getErrIndex();
    public static final int ERR_TS_DELETE_DOC_FAILURE = (new ErrPair(iErrIndex++, "Delete of client document failed - does it exist?")).getErrIndex();
    public static final int ERR_TS_ADD_SIGNATURE_FAILURE = (new ErrPair(iErrIndex++, "Attempt to add signature failed")).getErrIndex();
    public static final int ERR_TS_DELETE_SIGNATURE_FAILURE = (new ErrPair(iErrIndex++, "Attempt to remove signature failed")).getErrIndex();
    public static final int ERR_TS_GROUP_ACCESS_CHANGE_FAILURE = (new ErrPair(iErrIndex++, "Attempt to add, change, or remove group access permissions failed")).getErrIndex();
    public static final int ERR_TS_GROUP_ACCESS_USER_FAILURE = (new ErrPair(iErrIndex++, "Attempt to add or remove user to/from group failed")).getErrIndex();
    public static final int ERR_TS_EXCEPTION_PASSED_FROM_INVOKED_METHOD = (new ErrPair(iErrIndex++, "An exception was thrown from the reflection-invoked method")).getErrIndex();
    public static final int ERR_TS_ERROR_IN_DB_INTERACTION = (new ErrPair(iErrIndex++, "Exception during database interaction")).getErrIndex();
    public static final int ERR_TS_CREATE_GROUP_FAILURE = (new ErrPair(iErrIndex++, "Failure in creating group")).getErrIndex();
    public static final int ERR_TS_LIST_GROUP_FAILURE = (new ErrPair(iErrIndex++, "Failure in listing available groups")).getErrIndex();
    public static final int ERR_TS_CREATE_USER_FAILURE = (new ErrPair(iErrIndex++, "Failure in creating user")).getErrIndex();
    public static final int ERR_TS_NO_SUBSCRIBERS_THIS_GROUP = (new ErrPair(iErrIndex++, "No subscribers found for this group")).getErrIndex();
    public static final int ERR_TS_CLIENT_NOT_FOUND = (new ErrPair(iErrIndex++, "Configuration error - client identifier not found")).getErrIndex();
    public static final int ERR_TS_CREATE_GENRE_FAILURE = (new ErrPair(iErrIndex++, "Failure in creating genre")).getErrIndex();
    public static final int ERR_TS_DELETE_USER_FAILURE = (new ErrPair(iErrIndex++, "Failure in deleting user")).getErrIndex();
    public static final int ERR_TS_DELETE_GROUP_FAILURE = (new ErrPair(iErrIndex++, "Failure in deleting group")).getErrIndex();
    public static final int ERR_TS_NO_ROWS_FOUND = (new ErrPair(iErrIndex++, "No rows found")).getErrIndex();
    public static final int ERR_TS_INVOCATION_TARGET_EXCEPTION = (new ErrPair(iErrIndex++, "Target of invocation invalid")).getErrIndex();
    public static final int ERR_TS_OUTSIDE_INVOKED_METHOD = (new ErrPair(iErrIndex++, "Outside of invoked method")).getErrIndex();
    public static final int ERR_TS_BAD_PASSWORD = (new ErrPair(iErrIndex++, "Invalid username or password")).getErrIndex();
    public static final int ERR_TS_SESSION_EXPIRED = (new ErrPair(iErrIndex++, "This session key has expired")).getErrIndex();
    public static final int ERR_TS_OUT_OF_DATE = (new ErrPair(iErrIndex++, "This function is no longer used")).getErrIndex();
    public static final int ERR_TS_NOT_AUTHORIZED = (new ErrPair(iErrIndex++, "You are not authorized to invoke this function")).getErrIndex();
    public static final int ERR_TS_NODERANGE_MAXED_PER_CLIENT = (new ErrPair(iErrIndex++, "There are insufficient unused nodeIDs remaining")).getErrIndex();
    public static final int ERR_TS_NODEDOCUMENT_LOCKED = (new ErrPair(iErrIndex++, "The database is currently locked from write access")).getErrIndex();
    public static final int ERR_TS_DATA_INTEGRITY_VIOLATION = (new ErrPair(iErrIndex++, "Execution forbidden due to data integrity constraints")).getErrIndex();

    public static String getErrText(int iErrIndex) {
        return ((ErrPair) vErrCodeStringPairs.elementAt(iErrIndex)).getErrText();
    }

    private static class ErrPair {

        int iErrIndex = -1;
        String sErrDescriptor = null;

        public ErrPair(int iErrIndex, String sErrDescriptor) {
            this.iErrIndex = iErrIndex;
            this.sErrDescriptor = sErrDescriptor;
            vErrCodeStringPairs.addElement(this);
        }

        int getErrIndex() {
            return iErrIndex;
        }

        String getErrText() {
            return sErrDescriptor;
        }
    }

    public static void emitException(String sAdditionalErrorInfo, Throwable e, PrintWriter out) {
        out.println(getsXML_JavaException(sAdditionalErrorInfo, e));
        return;
    }

    public static void emitException(String sAdditionalErrorInfo, Throwable e, PrintStream out) {
        out.println(getsXML_JavaException(sAdditionalErrorInfo, e));
        return;
    }

    public static String getsXML_JavaException(String sAdditionalErrorInfo, Throwable e) {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<TS_ERROR>\n");
            sb.append("<TS_ERROR_TYPE>" + "JAVA" + "</TS_ERROR_TYPE>\n");
            sb.append("<TS_ERROR_CODE>").append(ERR_TS_JAVA_LANG_EXCEPTION).append("</TS_ERROR_CODE>\n");
            sb.append("<TS_ERROR_CODE_STRING>").append(EmitGenXML_ErrorInfo.getErrText(ERR_TS_JAVA_LANG_EXCEPTION)).append("</TS_ERROR_CODE_STRING>\n");
            sb.append("<TS_ERROR_DESC>").append(sAdditionalErrorInfo).append("</TS_ERROR_DESC>\n");
            sb.append("<TS_EXCEPTION_GETMESSAGE>").append(e.getMessage()).append("</TS_EXCEPTION_GETMESSAGE>\n");
            String sStackTrace = api.Log.stackTraceToString(e);
            sb.append("<TS_EXCEPTION_STACKTRACE><![CDATA[").append(sStackTrace).append("]]></TS_EXCEPTION_STACKTRACE>\n");
            sb.append("</TS_ERROR>");
            return sb.toString();
        } catch (Exception e2) {
            System.out.println("final message : Error [" + e2.getMessage() + "[ writing [" + sb.toString() + "]");
        }
        return null;
    }

    public static void genXML_TestingException(String sAdditionalErrorInfo, Exception e, PrintWriter out)
            throws Exception {
        out.println("<TS_ERROR>\n");
        out.println("<TS_ERROR_TYPE>" + "TESTING" + "</TS_ERROR_TYPE>\n");
        out.println("<TS_ERROR_DESC>" + sAdditionalErrorInfo + "</TS_ERROR_DESC>\n");
        out.println("<TS_EXCEPTION_GETMESSAGE>" + e.getMessage() + "</TS_EXCEPTION_GETMESSAGE>\n");
        String sStackTrace = api.Log.stackTraceToString(e);
        out.println("<TS_EXCEPTION_STACKTRACE><![CDATA[" + sStackTrace + "]]></TS_EXCEPTION_STACKTRACE>\n");
        out.println("</TS_ERROR>");
        throw e;
    }
}
