/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Jul 11, 2006
 * Time: 10:12:24 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Remove the given word (or all words) from the must have table for a given
 * node.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param nodeid The unique node identifier
 * @param term Term to remove (if empty, all terms are removed)
 *
 * @note
 * http://ITSSERVER/itsapi/ts?fn=tsnode.TSRemoveMustHaves&NodeID=2000&SKEY=9919294812
 * <p/>
 * @return	SUCCESS tag, if successful. \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <SUCCESS>Must Have term removed successfully.</SUCCESS>
 * </TSRESULT>
 * \endverbatim
 */
public class TSRemoveMustHaves {

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sTerm = (String) props.get("Term");
        String sLang = (String) props.get("lang");
        String sKey = (String) props.get("SKEY", true);
        String musthaveType = (String) props.get("musthavetype");
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
        ResultSet rs = null;
        try {
            // for authorization purposes, need to know the corpus id of this node
            String sSQL = "select CorpusId from Node where NodeID = " + sNodeID;
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            int iCorpusId = rs.getInt(1);
            rs.close();
            stmt.close();
            rs = null;
            stmt = null;

            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        int update = 0;

        try {
            String musthaveTableName = "";
            if (musthaveType == null || musthaveType.equals("1")) {
                musthaveTableName = "musthave";
            } else {
                musthaveTableName = "nodemusthavegate";
            }
            String sSQL = "delete from " + musthaveTableName + " where nodeID = " + sNodeID;
            if (sTerm != null) {
                sSQL = sSQL + " and MustWord = '" + sTerm + "'";
            }
            if (sLang != null) {
                sSQL = sSQL + " and lang = '" + sLang + "'";
            }
            stmt = dbc.createStatement();

            // If query statement failed, throw an exception
            update = stmt.executeUpdate(sSQL);
            if (update == 0) {
                out.println("<DEBUG>Failed to remove term(s) for nodeID: " + sNodeID + " </DEBUG>");
            }
        } catch (Exception e) {
            out.println("<DEBUG>Failed to remove term(s) for nodeID: " + sNodeID + " </DEBUG>");
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        out.println("<SUCCESS>" + update + " must have words removed successfully.</SUCCESS>");

        try {
            String sSQL = "update Node set NodeLastSync = SYSDATE, dateupdated = sysdate where NodeID = " + sNodeID;
            stmt = dbc.createStatement();
            // If query statement failed, throw an exception
            if (stmt.executeUpdate(sSQL) == 0) {
                out.println("<DEBUG>Failed to update node status.</DEBUG>");
            }
        } catch (Exception tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }
}
