package api.tests;

import junit.framework.TestSuite;
import junit.framework.Test;
import junit.framework.TestResult;

import java.util.Enumeration;
import java.util.Vector;
import java.lang.reflect.Method;

import com.indraweb.util.UtilFile;

public class IndraTestSuite extends TestSuite {
   public IndraTestSuite() {
        super();
    }

    public IndraTestSuite(Class aClass, String s) {
        super(aClass, s);
    }

    public IndraTestSuite(Class aClass) {
        super(aClass);
    }

    public IndraTestSuite(String s) {
        super(s);
    }

    public void addTest(Test test) {
        super.addTest(test);
    }

    public void addTestSuite(Class aClass) {
        super.addTestSuite(aClass);
    }

    public void run(TestResult result) {
        super.run(result);
    }

    public void runTest(Test test, TestResult result) {
        super.runTest(test, result);
    }

}
