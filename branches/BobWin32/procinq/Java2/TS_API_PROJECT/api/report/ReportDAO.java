package api.report;

import com.iw.db.ConnectionFactory;
import com.iw.system.ModelNodeReport;
import com.iw.system.ParamsQueryReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class ReportDAO {

    private static final Logger log = Logger.getLogger(ReportDAO.class);

    public List<ModelNodeReport> getOracleData(String corpusId, ParamsQueryReport parameters) throws ReportDAOException {
        log.debug("ReportDAO: getOracleData(String corpusId)");
        Connection con = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (con == null) {
            throw new ReportDAOException("ReportDAO: Error getting oracle connection");
        }
        ModelNodeReport modelNode = null;
        List<ModelNodeReport> list = null;

        String sql = "SELECT nodeid, nodetitle, sum(case related when 'musthave' then total else 0 end) inmusthave,"
                + "sum(case related when 'images' then total else 0 end) inimages,"
                + "sum(case related when 'signature' then total else 0 end) insignature "
                + "from( SELECT "
                + "node.nodeid,"
                + "node.nodetitle,"
                + "COUNT(nodeimages.nodeid) AS total,"
                + "'images' as related "
                + "FROM node "
                + "inner JOIN nodeimages ON (nodeimages.nodeid = node.nodeid) where node.corpusid=? GROUP BY node.nodeid,node.nodetitle "
                + "union SELECT node.nodeid,node.nodetitle, COUNT(musthave.nodeid) AS total, 'musthave' as related "
                + "FROM node inner JOIN musthave ON (musthave.nodeid = node.nodeid) where node.corpusid=? GROUP BY node.nodeid,node.nodetitle "
                + "union SELECT node.nodeid,node.nodetitle, COUNT(signature.nodeid) AS total, 'signature' as related FROM node "
                + "inner JOIN signature ON (signature.nodeid = node.nodeid) where "
                + "node.corpusid=? GROUP BY node.nodeid,node.nodetitle ) group by nodeid,nodetitle "
                + "having( "
                + "(sum(case related when 'signature' then total else 0 end) < ? or sum(case related when 'signature' then total else 0 end) > ?) or "
                + "(sum(case related when 'musthave' then total else 0 end) = ? or sum(case related when 'musthave' then total else 0 end) > ?) or "
                + "sum(case related when 'images' then total else 0 end) = 0 )";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        list = new ArrayList<ModelNodeReport>();
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(corpusId));
            pstmt.setInt(2, Integer.parseInt(corpusId));
            pstmt.setInt(3, Integer.parseInt(corpusId));
            pstmt.setInt(4, Integer.parseInt(parameters.getLessSignature()));
            pstmt.setInt(5, Integer.parseInt(parameters.getMoreSignature()));
            pstmt.setInt(6, Integer.parseInt(parameters.getLessMusthave()));
            pstmt.setInt(7, Integer.parseInt(parameters.getMoreMusthave()));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String nodeid = String.valueOf(rs.getInt("nodeid"));
                String nodetitle = rs.getString("nodetitle");
                String inmusthave = String.valueOf(rs.getInt("inmusthave"));
                String insignature = String.valueOf(rs.getInt("insignature"));
                String inimages = String.valueOf(rs.getInt("inimages"));
                modelNode = new ModelNodeReport();
                modelNode.setNodeId(nodeid);
                modelNode.setNodeTitle(nodetitle);
                modelNode.setNodeMusthaves(inmusthave);
                modelNode.setNodeSignatures(insignature);
                modelNode.setNodeImages(inimages);
                modelNode.setNodeLinks("-");
                list.add(modelNode);
            }
            return list;
        } catch (SQLException e) {
            throw new ReportDAOException("ReportDAO: Error SQL exception exucuting query", e.getCause());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                throw new ReportDAOException("ReportDAO: Error SQL exception closing resources", e.getCause());
            }
        }
    }

    public Long getLinksDynamo(String nodeId) {
        return null;
    }
}
