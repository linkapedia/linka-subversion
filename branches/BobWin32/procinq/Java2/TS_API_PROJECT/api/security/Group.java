package api.security;

import com.indraweb.execution.Session;

import java.util.*;
import java.io.*;

import javax.naming.*;
import javax.naming.directory.*;

public class Group implements Serializable
{
	private String DN;
	private String sGroupName;
    private String sDisplayName;
	private Hashtable htCorpusAccess = new Hashtable();
	private Hashtable htAdminAccess = new Hashtable();

	// Accessor methods to private variables
	public String GetDN() { return DN; }
	public void SetDN(String DN) { this.DN = DN; }
	
	public String GetGroupName() { return sGroupName; }
	public void SetGroupName(String GroupName) { this.sGroupName = GroupName; }

    public String GetDisplayName() {
        if (sDisplayName == null) { return sGroupName; }
        return sDisplayName;
    }
	public void SetDisplayName(String GroupName) { this.sDisplayName = GroupName; }

	// Object constructors
	public Group () {}
	public Group (String sGroupName) {
        this.sGroupName = sGroupName;
        this.sDisplayName = sGroupName;
    }
    public Group (String sGroupName, String sDisplayName) {
        this.sGroupName = sGroupName;
        this.sDisplayName = sDisplayName;
    }

	public Hashtable GetCorpusAccess() { return htCorpusAccess; }
	public Hashtable GetAdminAccess() { return htAdminAccess; }
	
	// The following methods will grant or revoke the corpus access level of this group
	public void GrantCorpus(String CorpusID) { 
		this.htCorpusAccess.put(CorpusID, "1"); 
		this.htAdminAccess.put(CorpusID, "0");
	}
	public void GrantCorpus(int CorpusID) { 
		this.htCorpusAccess.put(""+CorpusID, "1"); 
		this.htAdminAccess.put(""+CorpusID, "0");
	}
	public void RevokeCorpus(String CorpusID) { htCorpusAccess.put(CorpusID, "0"); }
	public void RevokeCorpus(int CorpusID) { htCorpusAccess.put(""+CorpusID, "0"); }

	// The following methods will grant or revoke administrative access to a given corpus.
	// Note that a user with administrative access also has corpus level access.
	public void GrantAdmin(String CorpusID) { 
		htCorpusAccess.put(CorpusID, "0"); 
		htAdminAccess.put(CorpusID, "1"); 
	}
	public void GrantAdmin(int CorpusID) { 
		htCorpusAccess.put(""+CorpusID, "0"); 
		htAdminAccess.put(""+CorpusID, "1"); 
	}
	public void RevokeAdmin(String CorpusID) { 
		htAdminAccess.put(CorpusID, "0");
	}
	public void RevokeAdmin(int CorpusID) { 
		htAdminAccess.put(""+CorpusID, "0");
	}

	// Check authorization of this group for this corpus
	public boolean IsAuthorized (int CorpusID) { return IsAuthorized(""+CorpusID); }
	public boolean IsAuthorized (String CorpusID) {
		if (CorpusID == null) { return false; }
		if ((!this.htCorpusAccess.containsKey(CorpusID)) &&
			(!this.htAdminAccess.containsKey(CorpusID))) { return false; }

		try {
			String sAccessLevel = (String) this.htCorpusAccess.get(CorpusID);
			if (sAccessLevel.equals("1")) { return true; }
		} catch (Exception e) {}
		
		try {
			String sAccessLevel = (String) this.htAdminAccess.get(CorpusID);
			if (sAccessLevel.equals("1")) { return true; }
		} catch (Exception e) {}

		return false;
	}
	public boolean IsAdmin (int CorpusID) { return IsAdmin(""+CorpusID); }
	public boolean IsAdmin (String CorpusID) {
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) { return true; }

		if (CorpusID == null) { return false; }
		if (!this.htAdminAccess.containsKey(CorpusID)) { return false; }
		String sAccessLevel = (String) this.htAdminAccess.get(CorpusID);
		if (sAccessLevel.equals("1")) { return true; }
		else { return false; }
	}

	// Saving groups is different depending upon which authentication system you use
	public boolean SaveGroup (PrintWriter out, String sUserName, String sPassword) {
		LDAP_Connection lc = new LDAP_Connection(com.indraweb.execution.Session.cfg.getProp("LdapUsername"), 
												 com.indraweb.execution.Session.cfg.getProp("LdapPassword"), out);
        try {
            if (lc.GetAuthentication() == 1) {	return SaveGroupAD(out, sUserName, sPassword, lc); }
            return SaveGroupOpenLDAP(out, sUserName, sPassword, lc);
        } catch (Exception e) { api.Log.LogError(e); }
        finally { lc.close(); }

        return false;
	}
	
	// Save group to LDAP server (Active Directory)
	public boolean SaveGroupAD (PrintWriter out, String sUserName, String sPassword, LDAP_Connection lc) {
		try {
			DirContext ctx = lc.getDirContext();
			String sTempDN = this.DN;
			
			if (sTempDN.toUpperCase().endsWith(lc.GetDC().toUpperCase())) {
				sTempDN = sTempDN.substring(0, sTempDN.length() - (lc.GetDC().length()+1));
			}

			// Specify the changes to make
			int iModificationItems = 4; 
			
			// Get the attribute list -- need a baseline to compare
			Attributes attrs = ctx.getAttributes(sTempDN);
			Attribute aC = attrs.get("corpusAccess");  
			Attribute aA = attrs.get("adminAccess");

			// Create new attributes
			Attribute htN = new BasicAttribute("corpusAccess");
			Attribute htE = new BasicAttribute("adminAccess");

			// Get keys
			Enumeration htNe = this.GetCorpusAccess().keys(); 
			Enumeration htEe = this.GetAdminAccess().keys();

			// initialize various counters; these will decide which AD actions we take later..
			int iCorpusCounter = 0; int iAdminCounter = 0;
			boolean bCorpusEmpty = true; boolean bAdminEmpty = true;

			// Loop through our hashtables and build attribute hashs
			while (htNe.hasMoreElements()) {
				String sCorpusID = (String) htNe.nextElement();
				String sIs = (String) this.GetCorpusAccess().get(sCorpusID);
				if (sIs.equals("1")) { htN.add(sCorpusID); iCorpusCounter++; bCorpusEmpty = false; }
			}	
			while (htEe.hasMoreElements()) {
				String sCorpusID = (String) htEe.nextElement();
				String sIs = (String) this.GetAdminAccess().get(sCorpusID);
				if (sIs.equals("1")) { htE.add(sCorpusID); iAdminCounter++; bAdminEmpty = false; }
			}

			if (aC == null) { iModificationItems = iModificationItems - 1; }
			if (aA == null) { iModificationItems = iModificationItems - 1; }
			if (iCorpusCounter == 0) { iModificationItems = iModificationItems - 1; }
			if (iAdminCounter == 0) { iModificationItems = iModificationItems - 1; }
			
			if (iModificationItems == 0) { return false; }
				 
			ModificationItem[] mods = new ModificationItem[iModificationItems];
			int iModItem = 0;
			
			// Always begin by removing everything
			if (aC != null) {
				mods[iModItem] = new ModificationItem(ctx.REMOVE_ATTRIBUTE, new BasicAttribute("corpusAccess"));
				iModItem++;
			}
			if (aA != null) {
				mods[iModItem] = new ModificationItem(ctx.REMOVE_ATTRIBUTE, new BasicAttribute("adminAccess"));
				iModItem++;
			}
			
			// Create modification items for corpusAccess and adminAcces
			if (iCorpusCounter > 0) { mods[iModItem] = new ModificationItem(ctx.ADD_ATTRIBUTE, htN); iModItem++; }
			if (iAdminCounter > 0) { mods[iModItem] = new ModificationItem(ctx.ADD_ATTRIBUTE, htE); iModItem++; }

			// Now: modify attributes and classes in Active Directory, and
			//  store results in a session variable for caching
			ctx.modifyAttributes(sTempDN, mods);
			com.indraweb.execution.Session.htGroups.put(sTempDN, this);

			return true;
			
		} catch (Exception e) { api.Log.LogError(e); return false; }
	}

	// Save group to LDAP server (open LDAP) 
	public boolean SaveGroupOpenLDAP (PrintWriter out, String sUserName, String sPassword, LDAP_Connection lc) {
		try {
			DirContext ctx = lc.getDirContext();
			String sTempDN = this.DN;
			if (sTempDN.toUpperCase().endsWith(lc.GetDC().toUpperCase())) {
				sTempDN = sTempDN.substring(0, sTempDN.length() - (lc.GetDC().length()+1));
			}

            Attributes attrs = ctx.getAttributes(sTempDN);

			// We always save groups as Indragroup
			Attribute oc = attrs.get("objectClass");
            if (!oc.contains("Indragroup")) { oc.add("Indragroup"); }

			Attribute htN = new BasicAttribute("corpusAccess");
			Enumeration htNe = this.GetCorpusAccess().keys();

			while (htNe.hasMoreElements()) {
				String sNodeID = (String) htNe.nextElement();
				String sIs = (String) this.GetCorpusAccess().get(sNodeID);
				if (sIs.equals("1")) { htN.add(sNodeID); }
			}

			Attribute htE = new BasicAttribute("adminAccess");
			Enumeration htEe = this.GetAdminAccess().keys();
			while (htEe.hasMoreElements()) {
				String sNodeID = (String) htEe.nextElement();
				String sIs = (String) this.GetAdminAccess().get(sNodeID);
				if (sIs.equals("1")) { htE.add(sNodeID); }
			}

			if (this.GetCorpusAccess().size() > 0) { attrs.put(htN); } else { attrs.remove("corpusAccess"); }
			if (this.GetAdminAccess().size() > 0) { attrs.put(htE); } else { attrs.remove("adminAccess"); }
            attrs.put(oc);

            ctx.modifyAttributes(sTempDN, ctx.REPLACE_ATTRIBUTE, attrs);
			com.indraweb.execution.Session.htGroups.put(sTempDN, this);

			return true;
			
		} catch (Exception e) { api.Log.LogError(e); return false; }
	} 
}
