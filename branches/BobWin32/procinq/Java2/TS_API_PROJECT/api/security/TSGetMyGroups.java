package api.security;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import javax.naming.*;
import javax.naming.directory.*;

import com.iw.system.User;

import com.indraweb.execution.Session;
import com.indraweb.database.*;

import java.util.Hashtable;

import java.security.*;

/**
 * This API retrieves the list of LDAP groups that the user is associated with.  If LDAP is not active, return default.
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note   http://ITSSERVER/servlet/ts?fn=security.TSGetMyGroups

 *	@return A list of LDAP groups
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
       <GROUPS>
          <GROUP>
             <GROUPNAME>Default Group</GROUPNAME>
             <DC>cn=Default,dc=None</DC>
          </GROUP>
       </GROUPS>
    </TSRESULT>
  \endverbatim
 */
public class TSGetMyGroups
{
	// Log the user into the system and return a session key
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        try {

			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user exists
			if (u == null) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            //api.Log.Log("AuthenticationSystem: "+ Session.cfg.getProp("AuthenticationSystem").toLowerCase());

            // added 04/15/04 -- if security setting is NONE, then no groups will be found, use a default group
            if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
                out.println("<GROUPS>");
                out.println("   <GROUP>");
                out.println("      <GROUPNAME>Default Group</GROUPNAME>");
                out.println("      <DC>cn=Default,dc=None</DC>");
                out.println("   </GROUP>");
                out.println("</GROUPS>");

                return;
            }

			// Attempt to log this user in
			LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);

            // MODIFIED 12/16: load user variable with group permissions at login time
            lc.Refresh_Connection();
            Hashtable ht = lc.GetGroups(u);

            Enumeration eG = ht.elements(); int loop = 0;
            while (eG.hasMoreElements()) {
                loop++; if (loop == 1) out.println("<GROUPS>");

                Group g = (Group) eG.nextElement();
                out.println("   <GROUP>");
                out.println("      <GROUPNAME>"+g.GetDisplayName()+"</GROUPNAME>");
                out.println("      <DC>"+g.GetDN()+"</DC>");
                out.println("   </GROUP>");
            }

            if (loop == 0) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);

            out.println("</GROUPS>");
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}
