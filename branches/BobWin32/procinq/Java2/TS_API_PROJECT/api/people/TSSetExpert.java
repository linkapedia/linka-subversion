package api.people;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.iw.system.User;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 * This API call has been deprecated.  People search is now stored inside of narratives.
 *
 * @note    This API call has been deprecated.  People search is now stored inside of narratives.
 */
public class TSSetExpert
{
    public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		try { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE); }
		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}
