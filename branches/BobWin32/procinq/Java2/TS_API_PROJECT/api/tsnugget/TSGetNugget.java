package api.tsnugget;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Retrieve nugget data given a unique nugget identifier.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NuggetID  The unique nugget identifier of the nugget data requested.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnugget.TSGetNugget&SKEY=-132981656&NuggetID=13

 *	@return	A NUGGET object, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <NUGGET>
        <NUGGETID>13</NUGGETID>
        <NUGGET>The number of weekly smallpox vaccine adverse events</NUGGET>
      </NUGGET>
   </TSRESULT>
  \endverbatim
 */
public class TSGetNugget {

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNuggetID = (String) props.get ("NuggetID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
		try {
			
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select Nugget from Nugget where NuggetId = "+sNuggetID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
 
			int loop = 0;
			while ( rs.next() ) {
				loop++;
				String sNugget = rs.getString(1);
	
				out.println ("   <NUGGET> ");
				out.println ("   <NUGGETID>"+sNuggetID+"</NUGGETID>");
				out.print   ("   <NUGGET>");
					for (int k = 0; k < sNugget.length(); k++) {
						int ichar = (int) sNugget.charAt(k);
						if ((ichar != 13) && (ichar != 10)) { out.print(sNugget.charAt(k)); }
					}				
				out.println ("   </NUGGET>");
				out.println ("   </NUGGET>");
			}
		
			rs.close();
		    stmt.close();
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No nuggets found for Nugget ID: "+sNuggetID+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
