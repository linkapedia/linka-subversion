package api.tsqa;

import com.indraweb.util.UtilStrings;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//import com.indraweb.analysis.trec.ReportPRSummaryIterative;

//import com.indraweb.trec

	

public class TSQATrec2
{
	private static int iCallCounter = 0;
	private static final Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on
	
	// ***********************************************************
	public static void handleTSapiRequest ( HttpServletRequest req, 
											HttpServletResponse res, 
											api.APIProps props, 
											PrintWriter out, 
											Connection dbc )
	// ***********************************************************
		throws Exception
	{
		try 
		{
			out.println ("<CALLCOUNTER>" + iCallCounter + "</CALLCOUNTER>");out.flush();
			out.println ("<TIME>" + new java.util.Date() + "</TIME>");out.flush();
		    //com.indraweb.util.Log.log("in TSQATrec\r\n");
			synchronized (ICallCounterSemaphore)
				{ iCallCounter++; }

			int iProp_NumTopRecs = props.getint ("NumTopRecs", "10000000" ); 
			
			String sProp_QuestionID = (String) props.get ("QID", true );
			Vector vSQids = null;

			if ( sProp_QuestionID.indexOf(",") > 0 ){
				vSQids = UtilStrings.splitByStrLen1(sProp_QuestionID, ","); 

			}
			else {
				vSQids = new Vector();
				vSQids.addElement(sProp_QuestionID);
			} 
		
			
			//long lStartwholeprogram = System.currentTimeMillis();

			
			out.println ("<QUALITY_REPORT>");
			PRAccumulator pra = new PRAccumulator();
			for ( int iQIndex = 0; iQIndex < vSQids.size(); iQIndex++)
			{
				int iProp_QuestionID = Integer.parseInt ((String) vSQids.elementAt(iQIndex));
				//TrecCompareCounters
				PRPair prp = runSummaryReport_PR (iProp_QuestionID, 
															  iProp_NumTopRecs, 
															  false, 
															  dbc, out );				
				pra.addPR (prp.dP, prp.dR);
			} // for all Q's 
			out.println ("<PAVG>" + pra.getAveragePRecision() + "</PAVG>");out.flush();
			out.println ("<RAVG>" + pra.getAverageRecall() + "</RAVG>");out.flush();
			out.println ("</QUALITY_REPORT>");
			//UtilFile.addLineToFile(sFile , "time total this program [" + UtilProfiling.elapsedTimeMillis(lStartwholeprogram) + "]\r\n" );
			
		}
		catch ( Throwable t )
		{
		    com.indraweb.util.Log.NonFatalError("top level throwable", t );
		}
    }
	
	
// 88888888888888888888888888888888888888
	
	public static PRPair runSummaryReport_PR ( int iQid,
                                             int iTopXCount,
                                             boolean bIncludeScore1Eq0 ,
                                             Connection dbc,
                                             PrintWriter out)
    throws Exception
    {
        // GET IW RESULTS
        Hashtable htIWHitsTopX = getTopXIWDocsThisQ (iQid, iTopXCount,bIncludeScore1Eq0, dbc );
        // GET TREC RESULTS
        Hashtable htTrecHitsTopX = getTrecDocsThisQ(iQid,  dbc );

        // GET PRECISION - COMMON SIZE IW TO TREC
        Enumeration e = htIWHitsTopX.keys() ;
        int iCountCommon = 0;
        while (e.hasMoreElements() )
        {
            Integer IDocID = (Integer ) e.nextElement();
            if ( htTrecHitsTopX.get ( IDocID ) != null  )
                iCountCommon++;
        }

        // GET RECALL - COMMON SIZE IW TO TREC
        Enumeration e2 = htTrecHitsTopX.keys () ;
        int iCountMissing = 0;
        while (e.hasMoreElements() )
        {
            Integer IDocID = (Integer ) e.nextElement();
            if ( htIWHitsTopX.get ( IDocID ) == null )
                iCountMissing++;
        }

        double dPrecision = (double) iCountCommon / (double)htIWHitsTopX.size() ;
        double dRecall = (double) iCountCommon / (double)htTrecHitsTopX.size() ;
		
		PRPair prpreturn = new PRPair();
		prpreturn.dP = dPrecision;
		prpreturn.dR = dRecall;

        out.println("<QID_PR>");
        out.println("<QID>" + iQid + "</QID>") ;
        out.println("<COUNT>" + iTopXCount + "</COUNT>") ;
        out.println("<NODES>" + getNodesForQ(iQid, dbc) + "</NODES>") ;
		
        out.println("<P>" + iCountCommon  + "/" +htIWHitsTopX.size() + " = " + dPrecision + "</P>")  ;
        out.println("<R>" + iCountCommon  + "/" +htTrecHitsTopX.size() + " = " + dRecall + "</R>");
        out.println("</QID_PR>");


        // NOW GET THE SET OF TREC HITS
/*
        // second time around - extra file set MP created for 811 docs :
        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery (sSQL2);
        int iCounter = 0;
        Hashtable htFoundDocURLPiecesDB = new Hashtable();
        int iNumPrintedThisNode  = 0;
        while ( rs.next()  && iNumPrintedThisNode < 50 ) {
            // get FT944-8320 from [\\66.134.131.60\60gb db drive\trec\tars\Ft\FT944\FT944_25\FT944-8320.HTM]
            String sURLDB = rs.getString(1);
            double dScoreAgg = rs.getDouble(2);

            int iStartSubIndex = sURLDB.lastIndexOf("\\");
            int iEndSubIndex = sURLDB.lastIndexOf(".");
            if ( iStartSubIndex > 0 && iEndSubIndex > iStartSubIndex ) // if parsable
            {
                String sURLPieceDB = sURLDB.substring(iStartSubIndex+1, iEndSubIndex);
                String sPReexist = (String) htFoundDocURLPiecesDB.get ( sURLDB );
                if ( sPReexist == null )
                {
                    htFoundDocURLPiecesDB.put ( sURLPieceDB , "dummy");
                    com.indraweb.util.UtilFile.addLineToFile(sFileName,
                            iArrQIDs[i] + "\t" +sURLPieceDB + "\t" +iNumPrintedTotal+ "\t" +
                            com.indraweb.util.UtilStrings.numFormatDouble(dScoreAgg, 2) + "\r\n");
                    iNumPrintedThisNode++;
                    iNumPrintedTotal++;
                }
            } // if parsable
            else {
                com.indraweb.util.Log.logClearcr("ERROR - URL NOT PARSABLE" +
                    " sURLDB [" + sURLDB + "]");
            }
        }
        rs.close();
        stmt.close();
*/
		return prpreturn;
    }

    private static Hashtable getTopXIWDocsThisQ (int iQid,
                                               int iTopXCount,
                                boolean bIncludeScore1Eq0 ,
                                Connection dbc )
    throws Exception
    {
        String sConstraintScore0 = " and score1 > 0 ";
        if ( bIncludeScore1Eq0 )
            sConstraintScore0 = "";
        //select distinct d.documentid from trecqnode tqn, nodedocument nd, document d, node n    where  tqn.version = 2    and tqn.nodeid = n.nodeid    and n.corpusid = 100040    and n.nodeid = nd.nodeid    and nd.score1 > 0    and d.documentid = nd.documentid    and questionid = 447   and (score5*score6)  > .000

        // 401	Q0	FT923-1528	0	3.2633	pir9At0
        //double dCurrentIterateCutoff = 3;
        String sSQL2 = " select d.documentid " +
        " from trecqnode tqn, nodedocument nd, document d, node n    " +
        " where  tqn.version = 2    " +
        " and tqn.nodeid = n.nodeid    " +
        " and n.corpusid = 100040    " +
        " and n.nodeid = nd.nodeid    " +
        sConstraintScore0 +
        " and d.documentid = nd.documentid    " +
        " and questionid = " + iQid +
        " order by (score5*score6) desc";
        //" and (score5*score6)  > " + dCurrentIterateCutoff ;

        //com.indraweb.util.Log.logClear("sSQL2 [" + sSQL2 + "]\r\n");

        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery (sSQL2);
        Hashtable htDocsDesc = new Hashtable();
        while ( rs.next()  && htDocsDesc.size() < iTopXCount ) {
            // get FT944-8320 from [\\66.134.131.60\60gb db drive\trec\tars\Ft\FT944\FT944_25\FT944-8320.HTM]
            int iDocID = rs.getInt(1);
            htDocsDesc.put(new Integer ( iDocID), "dummy");
        }
        return htDocsDesc;

    }
    private static Hashtable getTrecDocsThisQ (int iQid,
                                Connection dbc )
    throws Exception
    {
        double dCurrentIterateCutoff = 3;
        String sSQL2 = "select distinct documentid from trecqrel where status = 1 and " +
                " qestionid = " + iQid;

        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery (sSQL2);
        Hashtable htDocsDesc = new Hashtable();
        while ( rs.next() ) {
            // get FT944-8320 from [\\66.134.131.60\60gb db drive\trec\tars\Ft\FT944\FT944_25\FT944-8320.HTM]
            int iDocID = rs.getInt(1);
            htDocsDesc.put(new Integer ( iDocID), "dummy");
        }

        return htDocsDesc;
    }

	
	
	static String getNodesForQ ( int iQid, Connection dbc )
		throws Exception
	{
		
        String sSQLGetNodesForQ = "select nodeid from trecqnode where questionid = " + iQid +
            " and version = 2" ;
        Vector vINodeIDs = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLGetNodesForQ, dbc);
        StringBuilder  sbNoideIDList = new StringBuilder();
        for ( int ix = 0; ix < vINodeIDs.size(); ix++ )
        {
            if ( ix > 0 )
                sbNoideIDList.append(",");
            sbNoideIDList.append(vINodeIDs.elementAt(ix));
        }
		return sbNoideIDList.toString();
	} 
	

	static class PRPair
	{
		double dP = 0;
		double dR = 0;
	} 

	static class PRAccumulator
	{
		double dPaccumulator = 0;
		double dRaccumulator = 0;
		int iCountInstances = 0;
		
		public void addPR ( double dp, double dr )  		{
			dPaccumulator+= dp;
			dRaccumulator+= dr;
			iCountInstances++;
		} 
		
		double getAveragePRecision () 		{
			return dPaccumulator / (double) iCountInstances;	
		} 
		double getAverageRecall () 		{
			return dRaccumulator / (double) iCountInstances;	
		} 
		
		
		
	} 

}
	
	
	
	
	
	

