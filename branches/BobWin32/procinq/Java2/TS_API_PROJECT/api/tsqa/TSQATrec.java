package api.tsqa;

import com.iw.scoring.scoreaggregation.ScoreAggregation;
import com.iw.scoring.scoring2.*;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilProfiling;
import com.indraweb.util.UtilStrings;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Vector;


public class TSQATrec {
    private static int iCallCounter = 0;
    private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

    // ***********************************************************
    public static void handleTSapiRequest(HttpServletRequest req,
                                          HttpServletResponse res,
                                          api.APIProps props,
                                          PrintWriter out,
                                          Connection dbc)
            // ***********************************************************
            throws Exception {
        try {
            out.println("<CALLCOUNTER>" + iCallCounter + "</CALLCOUNTER>");
            out.flush();
            out.println("<TIME>" + new java.util.Date() + "</TIME>");
            out.flush();
            //com.indraweb.util.Log.log("in TSQATrec\r\n");
            synchronized (ICallCounterSemaphore) {
                iCallCounter++;
            }

            String sFileOriginal = (String) props.get("OUTPUTFILE", true);
            String sREPORTONLY = (String) props.get("REPORTONLY", "BOTH");
            int iProp_NumTopRecs = props.getint("NumTopRecs", "10000000");

            String sProp_QuestionID = (String) props.get("QID", true);
            Vector vSQids = null;

            if (sProp_QuestionID.indexOf(",") > 0) {
                vSQids = UtilStrings.splitByStrLen1(sProp_QuestionID, ",");

            } else {
                vSQids = new Vector();
                vSQids.addElement(sProp_QuestionID);
            }


            //long lStartwholeprogram = System.currentTimeMillis();


            for (int iQIndex = 0; iQIndex < vSQids.size(); iQIndex++) {
                int iProp_QuestionID = Integer.parseInt((String) vSQids.elementAt(iQIndex));
                String sFile = UtilStrings.replaceStrInStr(sFileOriginal, ".", "_" + iProp_QuestionID + ".");
                try {
                    PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(sFile, true)));
                    file.write("test pre delete TSQA");
                    file.close();
                    File f = new File(sFile);
                    f.delete();
                } catch (Exception e) {
                    String sErr = "can't write to or delete file [" + sFile + "]";
                    com.indraweb.util.Log.log("<ERROR>" + sErr + "</ERROR>");
                    out.println("<ERROR>" + "not running Q [" + iProp_QuestionID + "] can't delete file [" + sFile + "]</ERROR>");
                    continue;
                }
                out.println("<QUALITY_REPORT>");

                UtilFile.addLineToFileKill(sFile, "TREC PRECISION AND RECALL REPORT " + new java.util.Date() + "\r\n");

                long lStartThisQLoop = System.currentTimeMillis();
                // PRECISION ***************************************
                // PRECISION ***************************************
                // PRECISION ***************************************
                // PRECISION ***************************************
                boolean bcommondone = false;
                if (!sREPORTONLY.toUpperCase().equals("RECALL")) {
                    long lStartthisQloop_precisionreport = System.currentTimeMillis();
                    UtilFile.addLineToFile(sFile, "QId\tQDesc\tDocumentid\tNodeid\tDoctitle\tDdocurl\tNodeTitle\tCorpusid\tScore1STR\tScore5COV\tScore6FRQ\tScore5*6\r\n");
                    UtilFile.addLineToFile(sFile, "\r\nPRECISION REPORT\r\n>>>\r\n");

                    TrecCompareCounters tcc = TSPrecisionRecallReport.queryAndReport(TSPrecisionRecallReport.iREPORTTYPE_PRECISION,
                            0, iProp_QuestionID, dbc, sFile, iProp_NumTopRecs);

                    tcc.CalculateAndOutputAggregateNumbers(iProp_NumTopRecs, out);

                    out.println("<QID>" + iProp_QuestionID + " : " + tcc.sQtitle + "</QID>");
                    UtilFile.addLineToFile(sFile, tcc.toString());
                    UtilFile.addLineToFile(sFile, "time in precision report this Qid [" + UtilProfiling.elapsedTimeMillis(lStartthisQloop_precisionreport) + "]\r\n");
                    out.println(tcc.toStringShortForXML_common());
                    out.println(tcc.toStringShortForXMLSpecificToReportType());
                    bcommondone = true;

                }

                // RECALL *********************************************
                // RECALL *********************************************
                // RECALL *********************************************
                // RECALL *********************************************
                if (true && !sREPORTONLY.toUpperCase().equals("PRECISION")) {
                    UtilFile.addLineToFile(sFile, "\r\nRECALL REPORT\r\n>>>\r\n");
                    long lStartthisQloop_recallreport = System.currentTimeMillis();
                    TrecCompareCounters tcc = TSPrecisionRecallReport.queryAndReport(
                            TSPrecisionRecallReport.iREPORTTYPE_RECALL, 0, iProp_QuestionID, dbc, sFile, -1);
                    tcc.CalculateAndOutputAggregateNumbers(iProp_NumTopRecs, out);
                    UtilFile.addLineToFile(sFile, tcc.toString());
                    UtilFile.addLineToFile(sFile, "time in recall report this Qid [" + UtilProfiling.elapsedTimeMillis(lStartthisQloop_recallreport) + "]\r\n");

                    if (!bcommondone) {
                        out.println(tcc.toStringShortForXML_common());
                    }
                    out.println(tcc.toStringShortForXMLSpecificToReportType());

                    double dRecall = ((double) tcc.icounttrecHitsthisQ - (double) tcc.iNumDistinctFalseDocs_GT_zero) / (double) tcc.icounttrecHitsthisQ;

                }
                UtilFile.addLineToFile(sFile, "time total this Qid [" + UtilProfiling.elapsedTimeMillis(lStartThisQLoop) + "]\r\n");
                out.println("</QUALITY_REPORT>");
            } // for all Q's
            //UtilFile.addLineToFile(sFile , "time total this program [" + UtilProfiling.elapsedTimeMillis(lStartwholeprogram) + "]\r\n" );

        } catch (Throwable t) {
            com.indraweb.util.Log.NonFatalError("top level throwable", t);
        }
    }


}







