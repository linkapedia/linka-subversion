/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Mar 5, 2004
 * Time: 3:34:17 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package api.tsqa;

import com.iw.classification.Data_WordsToNodes;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilProfiling;
import com.indraweb.util.clsUtils;
import com.indraweb.signatures.SignatureParms;

import java.sql.Connection;
import java.util.Vector;

import api.MainTest;
import api.APIProps;

public class MainTestMultiThreadClassifyCacheExerciser
{
/* setting for test 1
    static boolean bConfig_sweepcache = true;
    static boolean bConfig_invalidate = true;
    static boolean bUseSystemOutIfTrue = true;
    static boolean bEmptyCacheWhenDonePerThread = false;
    static int iNumLoops  = 1;
    static int iNumThreads = 4;
*/
    static boolean bConfig_sweepcache = false;
    static boolean bConfig_invalidate = true;
    static boolean bUseSystemOutIfTrue = true;
    static boolean bEmptyCacheWhenDonePerThread = false;
    static boolean bStopWaitKey = false;
    static int iNumLoops  = -1;
    static int iNumThreads = 4;
    // see SignatureFile.java line 88
/*
    //api.Log.Log ("debug capped iNumNodesEncountered < 1000 ");
    //api.Log.Log ("debug capped iNumNodesEncountered < 1000  ");
    //while (rs.next () && iNumNodesEncountered <= 1000 )
*/
    // see DAta_wordstonodes.java line 557




    public static void mainTest (Connection dbc) throws Throwable
    {





        UtilFile.deleteFile( "C:/Program Files/ITS/CorpusSource/SigWordCountsFile_13.txt");
        UtilFile.deleteFile( "C:/Program Files/ITS/CorpusSource/SigWordCountsFile_18.txt");
        UtilFile.deleteFile( "C:/Program Files/ITS/CorpusSource/SigWordCountsFile_100002.txt");
        UtilFile.deleteFile( "C:/Program Files/ITS/CorpusSource/SigWordCountsFile_100011.txt");


        boolean bRunningSerialEarlyTestSectionThenSystemExiting =
                false;
        if ( bRunningSerialEarlyTestSectionThenSystemExiting  )
        {
            api.Log.Log ("at program start in non threaded model");
            Connection dbcx = api.statics.DBConnectionJX.getConnection ("API", "t1") ;
            SingleThreadTest st1 = null;



            int iLoop = 0;

            //int[] iArrCorpusIDsToRunThru = {13,18,100002,100011};
            int[] iArrCorpusIDsToRunThru = {13,18};           // %%%%%%%%%%%%%%%%%%%%%%%%%%%%

            while ( iLoop < iArrCorpusIDsToRunThru.length && !UtilFile.bFileExists("c:/temp/IndraDebugStopLooping.txt"))
            {
                if (bStopWaitKey) clsUtils.waitForKeyboardInput("1.1 baseline at start iLoop [" + iLoop + "]corpus [" + iArrCorpusIDsToRunThru[iLoop]+ "]" );
                if (bStopWaitKey) clsUtils.waitForKeyboardInput("1.2 baseline at start iLoop [" + iLoop + "]corpus [" + iArrCorpusIDsToRunThru[iLoop]+ "]" );

                st1 = new SingleThreadTest ("1", dbcx, ""+iArrCorpusIDsToRunThru[iLoop],"701663,701664,701665", false);
                st1.run();

                if (bStopWaitKey) clsUtils.waitForKeyboardInput("2.1 baseline at start iLoop [" + iLoop + "]corpus [" + iArrCorpusIDsToRunThru[iLoop]+ "]" );
                if (bStopWaitKey) clsUtils.waitForKeyboardInput("2.2 baseline at start iLoop [" + iLoop + "]corpus [" + iArrCorpusIDsToRunThru[iLoop]+ "]" );
                if (bStopWaitKey) clsUtils.waitForKeyboardInput("2.3 baseline at start iLoop [" + iLoop + "]corpus [" + iArrCorpusIDsToRunThru[iLoop]+ "]" );
                if (bStopWaitKey) clsUtils.waitForKeyboardInput("2.4 baseline at start iLoop [" + iLoop + "]corpus [" + iArrCorpusIDsToRunThru[iLoop]+ "]" );
                //clsUtils.waitForKeyboardInput("2 after classify pre empty iLoop [" + iLoop + "]");
    /*
                st1 = new SingleThreadTest ("1", dbcx, "13","701663,701664,701665", false);
                st1.run();
                st1 = new SingleThreadTest ("1", dbcx, "18","701663,701664,701665", false);
                st1.run();
    */
               /* clsUtils.waitForKeyboardInput("3  " +
                        " after classify but pre empty and GC iLoop last corpus installed [" + iArrCorpusIDsToRunThru[iLoop] +
                        "] Data_WordsToNodes.gethtNodeIDToCorpusID().size() [" + Data_WordsToNodes.gethtNodeIDToCorpusID().size()  +
                        "] Data_NodeIDsTo_NodeForScore.getNumNodes (N->NFS)  [" + Data_NodeIDsTo_NodeForScore.getNumNodes()  +
                        "] before next, iLoop [" + iLoop + "]");
                clsUtils.waitForKeyboardInput("3  " +
                        " after classify but pre empty and GC iLoop last corpus installed [" + iArrCorpusIDsToRunThru[iLoop] +
                        "] Data_WordsToNodes.gethtNodeIDToCorpusID().size() [" + Data_WordsToNodes.gethtNodeIDToCorpusID().size()  +
                        "] Data_NodeIDsTo_NodeForScore.getNumNodes (N->NFS)  [" + Data_NodeIDsTo_NodeForScore.getNumNodes()  +
                        "] before next, iLoop [" + iLoop + "]");*/
                //Data_WordsToNodes.printCacheContents("3 after one corpus [" +
                 //       iArrCorpusIDsToRunThru[iLoop] + "] before next ", dbc);
                //clsUtils.waitForKeyboardInput("4 after classify and empty and GC iLoop [" + iLoop + "]");
                if (bStopWaitKey) clsUtils.waitForKeyboardInput("3 " +
                        " after classify and empty and GC iLoop last corpus installed [" + iArrCorpusIDsToRunThru[iLoop] +
                        "] Data_WordsToNodes.gethtNodeIDToCorpusID().size() [" + Data_WordsToNodes.gethtNodeIDToCorpusID().size()  +
                        "] Data_NodeIDsTo_NodeForScore.getNumNodes (N->NFS)  [" + Data_NodeIDsTo_NodeForScore.getNumNodes()  +
                        "] before next, iLoop [" + iLoop + "]");
                if (bStopWaitKey) clsUtils.waitForKeyboardInput("3 " +
                        " after classify and empty and GC iLoop last corpus installed [" + iArrCorpusIDsToRunThru[iLoop] +
                        "] Data_WordsToNodes.gethtNodeIDToCorpumsID().size() [" + Data_WordsToNodes.gethtNodeIDToCorpusID().size()  +
                        "] Data_NodeIDsTo_NodeForScore.getNumNodes (N->NFS)  [" + Data_NodeIDsTo_NodeForScore.getNumNodes()  +
                        "] before next, iLoop [" + iLoop + "]");
                iLoop++;

            }
            clsUtils.waitForKeyboardInput("AAA1 pre emptycache");
            clsUtils.waitForKeyboardInput("AAA2 pre emptycache");
            clsUtils.waitForKeyboardInput("AAA3 pre emptycache");
            clsUtils.waitForKeyboardInput("AAA4 pre emptycache");
            Data_WordsToNodes.emptyCache(dbcx, new SignatureParms(new APIProps()));
            clsUtils.waitForKeyboardInput("BBB1 post empty");
            clsUtils.waitForKeyboardInput("BBB2 post empty");
            clsUtils.waitForKeyboardInput("BBB3 post empty");
            clsUtils.waitForKeyboardInput("BBB4 post empty");
            System.gc();
            Runtime.getRuntime().gc();
            clsUtils.waitForKeyboardInput("CCC1 post GC");
            clsUtils.waitForKeyboardInput("CCC2 post GC");
            clsUtils.waitForKeyboardInput("CCC3 post GC");
            clsUtils.waitForKeyboardInput("CCC4 post GC");
            //clsUtils.waitForKeyboardInput("3 after classify and empty and GC iLoop [" + iLoop + "]");
            Data_WordsToNodes.printDataStructWordsToNodes("3 post empty cache pre GC ", false);

            api.Log.Log ("at program end in non thre3aded model");
            System.exit(0);
        }

        if ( iNumThreads > 0 )
        {
            Connection dcb1 = api.statics.DBConnectionJX.getConnection ("API", "t1") ;
            SingleThreadTest st1 = new SingleThreadTest ("1", dcb1, "13,18","701663,701664,701665",bEmptyCacheWhenDonePerThread);
            //test 2 SingleThreadTest st1 = new SingleThreadTest ("1", dcb1, "100002","13","701663,701664,701665", bEmptyCacheWhenDonePerThread);
            // test 3 SingleThreadTest st1 = new SingleThreadTest ("1", dcb1, "13","701663,701664,701665", bEmptyCacheWhenDonePerThread);
            st1.start();
        }
        if ( iNumThreads > 1 )
        {
         Connection dcb2 = api.statics.DBConnectionJX.getConnection ("API", "t2") ;
         SingleThreadTest st2 = new SingleThreadTest ("1", dcb2, "18,100002","701663,701664,701665",bEmptyCacheWhenDonePerThread);
         // test 2 SingleThreadTest st2 = new SingleThreadTest ("1", dcb2, "100011","701663,701664,701665", bEmptyCacheWhenDonePerThread);
         // test 3 SingleThreadTest st2 = new SingleThreadTest ("2", dcb2, "18","701663,701664,701665", bEmptyCacheWhenDonePerThread);
         st2.start();
        }
        if ( iNumThreads > 2 )
        {
         Connection dcb3 = api.statics.DBConnectionJX.getConnection ("API", "t3") ;
         //SingleThreadTest st3 = new SingleThreadTest ("3", dcb3, "100002,100011");
         SingleThreadTest st3 = new SingleThreadTest ("1", dcb3, "100002,100011","701663,701664,701665",bEmptyCacheWhenDonePerThread);
         st3.start();
        }
        if ( iNumThreads > 3 )
        {
         Connection dcb4 = api.statics.DBConnectionJX.getConnection ("API", "t4") ;
         SingleThreadTest st4 = new SingleThreadTest ("1", dcb4, "100011,13","701663,701664,701665",bEmptyCacheWhenDonePerThread);
         st4.start();
        }
        if ( iNumThreads > 4 )
        {
        Connection dcb11 = api.statics.DBConnectionJX.getConnection ("API", "t11") ;
         //SingleThreadTest st11 = new SingleThreadTest ("11", dcb11, "13,100002");
         SingleThreadTest st11 = new SingleThreadTest ("11", dcb11, "18","701663,701664,701665",
                 bEmptyCacheWhenDonePerThread);
         st11.start();
        }
        if ( iNumThreads > 5 )
        {
         Connection dcb12 = api.statics.DBConnectionJX.getConnection ("API", "t12") ;
         SingleThreadTest st12 = new SingleThreadTest ("12", dcb12, "18,100011","701663,701664,701665", bEmptyCacheWhenDonePerThread);
         st12.start();
        }
        if ( iNumThreads > 6 )
        {
         Connection dcb13 = api.statics.DBConnectionJX.getConnection ("API", "t13") ;
         SingleThreadTest st13 = new SingleThreadTest ("13", dcb13, "100002,13","701663,701664,701665", bEmptyCacheWhenDonePerThread);
         st13.start();
        }
        if ( iNumThreads > 7 )
        {
         Connection dcb14 = api.statics.DBConnectionJX.getConnection ("API", "t14") ;
         SingleThreadTest st14 = new SingleThreadTest ("14", dcb14, "100011,18","701663,701664,701665", bEmptyCacheWhenDonePerThread);
         st14.start();
        }

/*

        while ( true )
        {
            clsUtils.pause_this_many_milliseconds();
        }
*/
        clsUtils.waitForKeyboardInput("AAA1 pre emptycache");
        clsUtils.waitForKeyboardInput("AAA2 pre emptycache");
        clsUtils.waitForKeyboardInput("AAA3 pre emptycache");
        clsUtils.waitForKeyboardInput("AAA4 pre emptycache");
        Data_WordsToNodes.emptyCache(dbc, new SignatureParms(new APIProps()));
        clsUtils.waitForKeyboardInput("BBB1 post empty");
        clsUtils.waitForKeyboardInput("BBB2 post empty");
        clsUtils.waitForKeyboardInput("BBB3 post empty");
        clsUtils.waitForKeyboardInput("BBB4 post empty");
        System.gc();
        Runtime.getRuntime().gc();
        clsUtils.waitForKeyboardInput("CCC1 post GC");
        clsUtils.waitForKeyboardInput("CCC2 post GC");
        clsUtils.waitForKeyboardInput("CCC3 post GC");
        clsUtils.waitForKeyboardInput("CCC4 post GC");
        //clsUtils.waitForKeyboardInput("3 after classify and empty and GC iLoop [" + iLoop + "]");
        Data_WordsToNodes.printDataStructWordsToNodes("3 post empty cache pre GC ", false);

        api.Log.Log ("at program end in non thre3aded model");
        System.exit(0);

    }


    private static class SingleThreadTest extends Thread
    {

        String sThreadID = null;
        String sCorpusListComma = null;
        String sCorpusListComma2 = null;
        String sCommaDocIDs = null;
        Connection dbc = null;

        String SKEY = "824056799";

        public SingleThreadTest (String sThreadID_, Connection dbc_,
                                 String sCorpusListComma_, String sCommaDocIDs_,
                                 boolean bEmptyCacheWhenDone)
        {
            this (sThreadID_, dbc_, sCorpusListComma_, sCorpusListComma_, sCommaDocIDs_, bEmptyCacheWhenDone);
        }

        public SingleThreadTest (String sThreadID_, Connection dbc_,
                                 String sCorpusListComma_, String sCorpusListComma_2,
                                 String sCommaDocIDs_,
                                 boolean bEmptyCacheWhenDone)
        {
            dbc = dbc_;
            sThreadID = sThreadID_;
            sCorpusListComma = sCorpusListComma_;
            sCorpusListComma2 = sCorpusListComma_2;
            sCommaDocIDs = sCommaDocIDs_;
        }




        public void run ()
        {
            boolean bVerbose = false;
            //test3DNodeDocNodeVisualization (dbc);
            //test3DNodeDocNodeVisualization_usingOracleForCount (dbc);
            //test3DNodeDocNodeVisualization_usingOracleForCount_allOracleMethod (dbc);
            //test3DNodeDocNodeVisualization_v4_builtFrom1_doAllInMem(dbc);
            //test3DNodeDocNodeVisualization_v5_sortByDocID (dbc);
            try
            {
                int iLoop = 0;
                String[] sArrDocIDs = sCommaDocIDs.split(",");
                while (true && (iNumLoops <  0 || iLoop < iNumLoops ) && !UtilFile.bFileExists("E:/temp/stoplooping.txt") )
                {

                    String sDocID = sArrDocIDs[iLoop % sArrDocIDs.length ];
                    String sURL = "http://66.134.131.35/itsapi/ts?fn=tsclassify.TSClassifyDoc" +
                            //"&NodeSet=1377184"+
                            //"&NodeToScoreExplain=210006"+
                            //"&ExplainScores=true" +    // 701663
                            "&DocIDSource="+sDocID + // perseus select documentid, docurl from document where  ( contains (FULLTEXT, 'HAZARDOUS WASTE AND MATERIAL and remediation and nongovernmental') > 0)   \\66.134.131.60\fdrive\trec\tars\Fr94\12\FR941230_1\FR941230-1-00042.HTM
                            "&post=false" +
                            "&skey=" + SKEY;

/*                    if ( sCorpusListComma.equals("13"))
                        sCorpusListComma= "18";
                    else if ( sCorpusListComma.equals("18"))
                        sCorpusListComma= "13";*/
                    long lStartLastLoop = System.currentTimeMillis();
                    //MainTest.executeAPI (sURL + "&Corpora=18&NodeSet=210010&sweepcache=true" , dbc , false);
                    if ( bVerbose ) api.Log.Log ("PRE API CALL sCorpusListComma [" + sCorpusListComma + "] t[" + Thread.currentThread().getName() + "] sThreadID [" + sThreadID + "] at top of loop ms in loop [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStartLastLoop ) + "]");
                    String ssweepCache = "false";
                    if ( bConfig_sweepcache )
                        ssweepCache = "true";


                    if ( iLoop  % 2 == 0  ) // which corpus
                        MainTest.executeAPI (sURL + "&Corpora="+sCorpusListComma+"&sweepcache="+ssweepCache , dbc , bUseSystemOutIfTrue );
                    //System.out.println("skip first call");
                    else
                        MainTest.executeAPI (sURL + "&Corpora="+sCorpusListComma2+"&sweepcache="+ssweepCache , dbc , bUseSystemOutIfTrue );
                    String[] sArrCorpora = sCorpusListComma.split(",");
                    if ( bConfig_invalidate )
                    {
                        for ( int i = 0; i < sArrCorpora.length; i++)
                          Data_WordsToNodes.invalidateNodeSigCache_byCorpus("tester", Integer.parseInt(sArrCorpora [i]));
                    }
                    if ( bVerbose ) api.Log.Log ("POST API CALL [" + Thread.currentThread().getName() + "] sThreadID [" + sThreadID + "] at top of loop ms in loop [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStartLastLoop ) + "]");
                    //com.indraweb.util.clsUtils.pause_this_many_milliseconds(500);
                    iLoop++;
                } // while loop
                if ( bEmptyCacheWhenDonePerThread )
                    Data_WordsToNodes.emptyCache(dbc, new SignatureParms(new APIProps()));

                api.Log.Log ("t [" + sThreadID + "]done loop iNumLoops [" + iNumLoops + "]");
            } catch ( Throwable e )
            {
                e.printStackTrace();
            }
/*
                Data_WordsToNodes.printCacheContents ("%%%%%%%%%%%% 1.A pre Corpora=13,18&sweepcache=false 1.  pre first classify " , dbc);

                Data_WordsToNodes.printCacheContents ("pre classify" , dbc);


                MainTest.executeAPI (sURL + "&Corpora=18&NodeSet=210010&sweepcache=false" , dbc , false);
                Data_WordsToNodes.printCacheContents ("post first classify into config corpora set" , dbc);

                Data_WordsToNodes.emptyCache (dbc);
                Data_WordsToNodes.printCacheContents ("post empty" , dbc);

                MainTest.executeAPI (sURL + "&sweepcache=true" , dbc , false);
                Data_WordsToNodes.printCacheContents ("post classify into config corpora set with sweep on " , dbc);

                MainTest.executeAPI (sURL + "&Corpora=18&sweepcache=false" , dbc , false);
                Data_WordsToNodes.printCacheContents ("post classify " , dbc);

                Data_WordsToNodes.invalidateNodeSigCache_byCorpus (18);

                MainTest.executeAPI (sURL + "&Corpora=18&sweepcache=false" , dbc , false);
                Data_WordsToNodes.printCacheContents ("post invalidate 18 and classify to 18" , dbc);

                MainTest.executeAPI (sURL + "&Corpora=18&sweepcache=true" , dbc , false);
                Data_WordsToNodes.printCacheContents ("post classify to 18 with sweep to remove 13" , dbc);

                MainTest.executeAPI (sURL + "&Corpora=13,18&sweepcache=true" , dbc , false);
                Data_WordsToNodes.printCacheContents ("post classify to 18 with sweep to remove 13" , dbc);

                Data_WordsToNodes.emptyCache (dbc);
                Data_WordsToNodes.printCacheContents ("post empty cache" , dbc);


                Data_WordsToNodes.refreshCache (dbc , "13,18" , true);
                Data_WordsToNodes.printCacheContents ("\r\n\r\n%%%%%%%%%%%% 1.D post Corpora=13&sweepcache=false 1.  pre first classify " , dbc);

                Data_WordsToNodes.refreshCache (dbc , "" , true);
                Data_WordsToNodes.printCacheContents ("\r\n\r\n%%%%%%%%%%%% 1.D post Corpora=13&sweepcache=false 1.  pre first classify " , dbc);

                Data_WordsToNodes.printCacheContents ("\r\n\r\n%%%%%%%%%%%% 1.D post Corpora=13&sweepcache=false 1.  pre first classify " , dbc);

                Data_WordsToNodes.printCacheContents ("\r\n\r\n%%%%%%%%%%%% 1.D post Corpora=13&sweepcache=false 1.  pre first classify " , dbc);
                Data_WordsToNodes.refreshCache (dbc , "" , true);
                Data_WordsToNodes.refreshCache (dbc , null , true);
                Data_WordsToNodes.printCacheContents ("\r\n\r\n%%%%%%%%%%%% 1.D post Corpora=13&sweepcache=false 1.  pre first classify " , dbc);
                MainTest.executeAPI (sURL + "&Corpora=13&sweepcache=true" , dbc , false);
                Data_WordsToNodes.printCacheContents ("A.2 " , dbc);


                Data_WordsToNodes.refreshCache (dbc , "" , true);
                Data_WordsToNodes.printCacheContents ("A.3.  should be empty" , dbc);
                // Data_WordsToNodes.invalidateVSSweepNodeSigCa] vIntegerNodesThisTitleWord.size()  [che_byCorpus(13);
                //Data_WordsToNodes.invalidateNodeSigCache_byCorpus(18);
                Data_WordsToNodes.refreshCache (dbc , "18" , false);
                Data_WordsToNodes.printCacheContents ("A.5.  should be 18 only" , dbc);
                Data_WordsToNodes.refreshCache (dbc , "" , false);
                // PROBLEM : should be empty here
                // set tsclassify to take the corpus list param and add as needed
                // need to run invalidate on a node that's in there that is not in the desired list
                // save off NFS's not nodeids in case a writer comes in in the mean time
                Data_WordsToNodes.printCacheContents ("B.  shnould be empty" , dbc);
                Data_WordsToNodes.refreshCache (dbc , "18,13" , false);
                Data_WordsToNodes.printCacheContents ("C.  should be 13 only" , dbc);
                Data_WordsToNodes.refreshCache (dbc , "" , false);
                Data_WordsToNodes.printCacheContents ("D. should be empty " , dbc);
                Data_WordsToNodes.refreshCache (dbc , "13,18" , false);
                Data_WordsToNodes.printCacheContents ("4.  post refresh post invalidate 13" , dbc);
                Data_WordsToNodes.printCacheContents ("4.  post refresh post invalidate 13" , dbc);
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus (13);
                Data_WordsToNodes.refreshCache (dbc , "" , false);
                Data_WordsToNodes.printCacheContents ("4.  post refresh post invalidate 13" , dbc);
                Data_WordsToNodes.refreshCache (dbc , "" , false);
                Data_WordsToNodes.printCacheContents ("6.  post refres post invalidate 18" , dbc);
                api.Log.Log ("------------ PRE SECOND CLASSIFY ------------ ");
                MainTest.executeAPI (sURL , dbc , true);
                Data_WordsToNodes.printCacheContents ("7.  post second classify" , dbc);
*/
        }

    }
}

