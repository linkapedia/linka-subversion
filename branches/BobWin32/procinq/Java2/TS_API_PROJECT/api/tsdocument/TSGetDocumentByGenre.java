/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: May 4, 2004
 * Time: 4:26:58 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsdocument;

import com.iw.system.User;
import com.iw.system.Document;

import java.sql.*;
import java.io.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

public class TSGetDocumentByGenre {
    public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sGID = (String) props.get ("GID", true);
        Statement stmt = null; ResultSet rs = null;
        String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // Ensure user is a member of the admin group
        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

		try {

            String sSQL = getGenreSQL()+" where d.GenreID = "+sGID+" order by d.troublestatus desc";
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
                if (loop == 1) { out.println("<DOCUMENTS>"); }

                Document d = new Document(rs);
                d.emitXML(out, u, true);
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No Documents were found.</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}

            out.println("</DOCUMENTS>");
		}

		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { if (stmt != null) { stmt.close(); stmt = null; } if (rs != null) { rs.close(); rs = null; } }
	}

    public static String getGenreSQL() {
        return " select /*+ INDEX(document, documentgenreid) */ d.DocumentID,d.GenreID,d.DocTitle,d.DocURL,d.DocumentSummary,d.DateLastFound,"+
               " d.visible, d.fulltext,d.repositoryid,d.troublestatus,d.documentsecurity, 100 from Document d";
    }
}

