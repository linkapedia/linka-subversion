package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;

import com.indraweb.execution.Session;
import com.iw.system.Document;
import com.iw.system.User;
import api.Log;

/**
 * Adds a document relationship to the ITS server.  Typically done through a classification request, but can
 * also be done through this API.  A document with no NodeDocument Relationships will not show up in any
 * taxonomies, but it will be available for full text searching.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocURL  The URL or UNC that links uniquely to this document.
 *  @param  DocTitle    The document title or subject heading.
 *  @param  RepositoryID    The unique repository identifier of this document.
 *  @param  DocSummary  (optional) A brief abstract of 4000 characters or less.
 *  @param  GenreID (optional) Genre/Folder identifier, if applicable
 *  @param  FilePath    (optional) Absolute path to the file location on disk
 *  @param  Visible (optional) Optional flag for whether the document comes back from standard CQL queries
 *  @param  Group (optional) LDAP Security group with permissions to this document (default is all groups)
 *  @param  Lang  (optional) Language of the document, used for selecting the full text lexer.  Defaults to "english".
 *  @param  Fmt  (optional) Format of the document, either "binary" or "text".  Defaults to "text".
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */


public class TSAddDocument
{
	// add a node document relationship
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Statement stmt = null;

        try {
            // Ensure user is a member of the admin group
            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // required fields:
            String sgid = (String) props.get ("genreid");
            String stitle = (String) props.get ("doctitle");
            String surl = (String) props.get ("docurl");

            if (surl == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS); }
            if (stitle == null) { props.put("doctitle", surl); }
            if (sgid == null) { props.put("genreid", "1"); }

            Document d = api.tsdocument.TSEditDocProps.GetDocument(props, dbc);
            int DocumentID = 0;

            if (d == null) {
                d = api.tsdocument.TSEditDocProps.InsertDocument(props, dbc, out);
                if (d != null) { d.emitXML(out, u, true); }
                else { out.println("<FAILURE>Document insert attempted FAILED.</FAILURE>"); }
            } else {
                DocumentID = new Integer(d.get("DOCUMENTID")).intValue();
                api.tsdocument.TSEditDocProps.UpdateDocument(DocumentID+"", props, dbc, out);
                d = api.tsdocument.TSEditDocProps.GetDocument(props, dbc);
            }

            try { DocumentID = Integer.parseInt(d.get("DOCUMENTID")); }
            catch (Exception e) { }

            // if genreid = 241, this is a narrative, insert it into the narrative table
            if (sgid.equals("241")) {
                String sSQL = "insert into narrative values("+d.get("DOCUMENTID")+")";

                try { // update the document table
                    stmt = dbc.createStatement();
                    //out.println("<SQL>"+sSQL+sSQL2+"</SQL>");
                    if (stmt.executeUpdate(sSQL) == 0) {
                        api.Log.Log("Insert document failed: "+sSQL);
                        throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION); }
                    stmt.close();
                } catch ( Exception e) {
                    api.Log.Log("Insert document failed (2): "+sSQL);
                    throw e;
                } finally { stmt = null; }
            }

            if (DocumentID != 0) { out.println("<DOCUMENTID>"+DocumentID+"</DOCUMENTID>"); }

		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { if (stmt != null) { stmt.close(); }}
	}
}
