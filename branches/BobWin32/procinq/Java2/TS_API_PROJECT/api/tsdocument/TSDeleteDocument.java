package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.results.Locked;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * This removes the document from the directory.  Note that this operation is permanent.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID  Unique document identifier.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tsdocument.TSDeleteDocument&SKEY=499861005&DocumentId=13284

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Document was removed successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSDeleteDocument
{
	// TSRemoveDocument (sessionid, documentid)
	//
	// Note: This API function will only mark a CLIENT SIDE document for deletion
	// Clients do NOT have permission to mark files on Indraweb server for deletion

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDocumentID = (String) props.get ("DocumentID", true);

		try {

			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            // Check to ensure that the NodeDocument table is writable
            if (Locked.NodeDocumentLocked(dbc)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODEDOCUMENT_LOCKED); }

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			String sSQL = "delete from document where DocumentId = "+sDocumentID;
			Statement stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Cannot delete Document with ID ("+sDocumentID+") !</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_DELETE_DOC_FAILURE);
			}
            out.println("<SUCCESS>Document was removed successfully</SUCCESS>");

		    stmt.close();
		} catch ( TSException tse ){
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
