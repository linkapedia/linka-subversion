package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.emitxml.EmitValue;

import com.indraweb.execution.Session;
import com.iw.system.User;
import api.Log;

/**
 * Add a description here.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID  Unique document identifier.
 *  @param  DocTitle    (optional) The document title or subject heading.
 *  @param  DocSummary  (optional) A brief abstract of 4000 characters or less.
 *  @param  GenreID (optional) Genre/Folder identifier, if applicable
 *  @param  FilePath    (optional) Absolute path to the file location on disk
 *  @param  Visible (optional) Optional flag for whether the document comes back from standard CQL queries
 *  @param  Lang  (optional) Language of the document, used for selecting the full text lexer.  Defaults to "english".
 *  @param  Fmt  (optional) Format of the document, either "binary" or "text".  Defaults to "text".
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditDocument
{
	// add a node document relationship
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Statement stmt = null;

        try {
            // Ensure user is a member of the admin group
            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // required fields:
            String sdocumentid = (String) props.get ("documentid", true);

            boolean bSuccess = api.tsdocument.TSEditDocProps.UpdateDocument(sdocumentid, props, dbc, out);
            if (bSuccess) { out.println("<SUCCESS>Document update succeeded.</SUCCESS>"); }
            else { out.println("<FAILURE>Document update attempted FAILED.</FAILURE>"); }

		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { if (stmt != null) { stmt.close(); }}
	}
}
