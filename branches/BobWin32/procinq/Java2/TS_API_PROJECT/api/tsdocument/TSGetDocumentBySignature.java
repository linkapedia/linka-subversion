package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Document;
import com.iw.system.User;

/**
 * Add a description here.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Signature  Signature word used to look up existing documents.
 *  @param  Frequency    (optional) Minimum frequency threshold for document lookup.
 *  @param  GenreID (optional)  Limit document results to a specific genre or folder.
 *  @param  MaxResults (optional)   Maximum number of results to return.  Default is 500.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetDocumentBySignature {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {
		String sWord = (String) props.get ("Signature", true);
        String sFreq = (String) props.get ("Frequency");
        String sGenreID = (String) props.get ("GenreID");
        String sMax = (String) props.get ("MaxResults", "500");
        int iMax = new Integer(sMax).intValue();

        String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // Ensure user is a member of the admin group
        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

        Statement stmt = null;
        ResultSet rs = null;
		try {
            dbc.setAutoCommit(false);

            // first run the stored procedure to fill the temporary table
            buildSignatureCache (sWord, dbc);

            // second, select information from the temporary table
            String sSQL = Document.getSQLwithoutTable(dbc)+", thecount from (select documentid, count(*) thecount "+
                    "from conceptcache group by documentid) a, document d where a.documentid = d.documentid ";
            if (sGenreID != null) { sSQL = sSQL + " and d.GENREID = "+sGenreID; }
            sSQL = sSQL + " order by thecount desc";

            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

			int iCacheResults = 0;
            while (rs.next() && (iCacheResults <= iMax)) {
                Document d = new Document(rs);

                iCacheResults++;

                if (iCacheResults == 1) { out.println("<DOCUMENTS>"); }

                out.println ("   <DOCUMENT> ");
                d.emitXML(out, u, false);
                out.println ("      <COUNT>"+d.get("THECOUNT")+"</COUNT>");
                out.println ("   </DOCUMENT>");
            }

            dbc.commit();
            dbc.setAutoCommit(true);

			// If no results found, throw an exception
			if ( iCacheResults == 0) {
				out.println ( "<DEBUG>No documents found associated with: "+sWord+" </DEBUG>");
                out.println("<NUMRESULTS>0</NUMRESULTS>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}
            out.println("</DOCUMENTS>");
            out.println("<NUMRESULTS>"+iCacheResults+"</NUMRESULTS>");
		}

		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}

    // build concept cache
    public static boolean buildSignatureCache (String sWord, Connection dbc) {
        CallableStatement cs = null;

        if (true) { api.Log.Log("begin GetDocsBySignature ("+sWord+"); end;"); }
        try {
            cs = dbc.prepareCall ("begin GetDocsBySignature (:1); end;"); cs.setString(1, sWord);
            cs.execute(); return true;
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (cs != null) { try { cs.close(); } catch (Exception e) {} cs = null; }}
    }
}
