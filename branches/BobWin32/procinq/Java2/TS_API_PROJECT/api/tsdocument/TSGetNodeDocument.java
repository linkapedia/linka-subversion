package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.net.*;
import java.util.Enumeration;
import java.util.zip.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.results.*;
import api.security.*;

import com.iw.system.User;

import com.indraweb.database.*;
import com.indraweb.network.*;
import com.indraweb.html.*;

// TSGetNodeDocument (sessionid, nodeid, genreid)
//
// This API function returns an XML object containing all documents (from both the
//   server and client databases) that exist in <nodeid> and are of type <genreid>.
//   The XML document list will be sorted by: Score1,Score2,..,Score6,GenreID
//   
//   <genreid> may be 0, indicating documents of any genre should be returned.
//
//   This function spins off two threads, one to query the client database and the 
//   other to query the server database.   The query to the server no longer touches the 
//   database (1/2/02) it now retrieves a pre-generated, compressed file from the web server.
//
//   As the server side XML is compressed, it must be decompressed first before it is read.
//
//   After both threads return, all data is parsed and thrown into a hash table.   This
//   prevents duplication.   Client documents are processed last to ensure they are given
//   precedence over server documents as they may have been changed or updated.
//
//   The hash table is then sorted by the criteria listed above, serialized into an XML
//   document object, and returned as a streamed output.
//
//   - Written by MAP 10/30/01
//
//   Update 7/29/2002: if there are active ROC cut offs for this corpus, use the cut offs
//   and resort the results on the fly.
//
//   Update 9/25/2003: use CQL to do this query, and trust SCORE1 do not calc ROC cut offs on the fly,
//   and eliminate the server side results query

/**
 * Returns an XML structure containing all documents that are classified into the given <i>node identifier</i>.
 * Documents may optionally be restricted by genre or by a score threshold.  This list will have any appropriate
 * blacklists applied to it before being returned by the API.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique topic identifier from which to retrieve documents.
 *  @param  FolderID (optional)    Genre/Folder identifier, if applicable
 *  @param  Start (optional)    Used for next/previous, document result to begin, defaults to 1.
 *  @param  End (optional)    Used for next/previous, document result to end, defaults to 10.
 *  @param  ScoreThreshold (optional)   Scoring threshold to limit documents, defaults to 75 (2 stars and above).
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tsdocument.TSGetNodeDocument&CorpusID=300012&Start=1&SKEY=988417272&ScoreThreshold=50.0&NodeID=1088308&End=2

 *	@return	A series of ITS NODEDOCUMENT objects ordered by BUCKET SCORE descending.
 *  \verbatim
 <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <NODEDOCUMENTS>
            <NODEDOCUMENT>
                <NODEID>1087920</NODEID>
                <DOCUMENTID>200001</DOCUMENTID>
                <CORPUSID>300012</CORPUSID>
                <PARENTID>1087909</PARENTID>
                <SCORE1>75.0</SCORE1>
                <DOCSUMMARY><![CDATA[ operating and technology expenditure, with theoperating and technology expenditure, with the... Armscor to determine client needs, to establish... obtain military systems and equipment, both... upgrading of computer network... It is an acquisition organisation and the... needs, to establish technology, to... needs, to establish technology, to... for example, the acquisition cash flow for... - Making its acquisition services available to... - Providing acquisition support to defence...]]></DOCSUMMARY>
                <DATESCORED>2002-10-13 22:20:58.0</DATESCORED>
                <INDEXWITHINNODE>1</INDEXWITHINNODE>
                <GENREID>121</GENREID>
                <NODETITLE><![CDATA[ Impacts of New Technology]]></NODETITLE>
                <DOCTITLE><![CDATA[ Armscor s Net Income Reportedly Tumbles]]></DOCTITLE>
                <DOCURL><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></DOCURL>
                <DOCUMENTSUMMARY><![CDATA[ null]]></DOCUMENTSUMMARY>
                <DATELASTFOUND>2002-10-16 09:50:48.0</DATELASTFOUND>
                <VISIBLE>1</VISIBLE>
                <FULLTEXT><![CDATA[ \\66.134.131.60\fdrive\trec\tars\Fbis\FB496090\FBIS4-23527.HTM]]></FULLTEXT>
            </NODEDOCUMENT>
 ...
  \endverbatim
 */
public class TSGetNodeDocument {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// Get input parameters.. GenreID is optional
		String sNodeID = (String) props.get ("NodeID", true);
		String sGenreID = (String) props.get ("FolderID");
		String sStart = (String) props.get ("Start");
		String sEnd = (String) props.get ("End");
		String sDate = (String) props.get ("Date");
        String sOrder = (String) props.get ("Order", "SCORE1 DESC");
		String sScoreThreshold = (String) props.get ("ScoreThreshold", "75");
		float fScoreThreshold = new Float(sScoreThreshold).floatValue();
		int iScoreThreshold = (int) fScoreThreshold;

		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // build the CQL query here
        String query = "SELECT <NODEDOCUMENT> WHERE NODEID = "+sNodeID+" AND "+
                       "SCORE1 >= "+iScoreThreshold;

        if (sGenreID != null) { query = query + " AND GENREID = "+sGenreID; }

        query = query + " ORDER BY "+sOrder;

        props.put("query", query);
        props.put("start", sStart);
        props.put("rowmax", ""+(1+ ((new Integer(sEnd).intValue()) - (new Integer(sStart).intValue()))));

        api.tscql.TSCql.handleTSapiRequest(props, out, dbc);

        return;
	}
}
