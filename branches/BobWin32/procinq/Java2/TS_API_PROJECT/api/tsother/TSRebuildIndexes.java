package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.execution.Session;
import com.indraweb.database.*;
import com.indraweb.utils.oracle.CustomFields;
import com.iw.system.Index;
import com.iw.system.User;

/**
 * Rebuild all full text indexes within the database.  Index synchronization procedures are used, and thus
 *  only entries which have been altered or added since the last rebuild are applied.  To force a full rebuild,
 *  use the optional "Build" parameter.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  Index (optional)    Specify only a single index to rebuild
 *  @param  Table (optional)    Restrict rebuild to indexes within a given table.  This option is required if Index parameter is present.
 *  @param  Build (optional)    If this parameter exists, a full rebuild will be used rather than a synchronization
 *  @param  Field (optional)    This parameter is only used if both Build and Index are present.  If building a specific index from scratch, the field name is required.
 *  @param  Thesaurus (optonal) Synchronize all thesaurus within the database
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TSRebuildIndexes&SKEY=-132981656

 *	@return	no return object is applicable.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
...
  </TSRESULT>
  \endverbatim
 */
public class TSRebuildIndexes
{
	public static void handleTSapiRequest (api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

        String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        String sBuild = (String) props.get("Build");
        String sIndex = (String) props.get("Index");
        String sField = (String) props.get("Field");
        String sTable = (String) props.get("Table");

        // load thesaurus?
        String sThesaurus = (String) props.get("Thesaurus");
		String sUsername = Session.cfg.getProp ("DBString_API_User");
		String sPassword = Session.cfg.getProp ("DBString_API_Pass");

        // determine whether to build/rebuild and if extra indexes (idrac) are required
        boolean bSuccess = false; boolean bBuild = true;
        if (sBuild == null) { bBuild = false; }

        // if the "INDEX" parameter is present and BUILD is also true, field must be present
        if ((sIndex != null) && (sBuild != null) && ((sField == null) || (sTable == null))) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }

        // this hashtable contains the list of indexes to rebuild along with their table
        Hashtable htIndexesToRebuild = new Hashtable();
        if (sIndex != null) {
            Index index;
            if (bBuild) { index = new Index(sIndex.toUpperCase(), sTable.toUpperCase(), sField.toUpperCase()); }
            else { index = new Index(sIndex.toUpperCase()); }
            htIndexesToRebuild.put(sIndex.toUpperCase(), index);
        }
        else { htIndexesToRebuild = CustomFields.getIndexes(dbc); }

        // SECURITY
		if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

        // Re/build loop
        Enumeration eH = htIndexesToRebuild.elements();
        while (eH.hasMoreElements()) {
            Index index = (Index) eH.nextElement();

            long lStartadd = System.currentTimeMillis();
            if (bBuild) {
                if (index.getName().toUpperCase().equals("DOCFULLTEXT")) {
                    bSuccess = BuildIndex(index.getTable(), index.getName(), index.getField(), dbc, true);
                } else {
                    bSuccess = BuildIndex(index.getTable(), index.getName(), index.getField(), dbc);
                }
            }
            else { bSuccess = RebuildIndex(index.getName(), dbc); }
            long lEndadd = System.currentTimeMillis() - lStartadd;
            if (bSuccess) { api.Log.Log(index.getName()+" index created in "+lEndadd+" ms."); }
            else { api.Log.Log(index.getName()+" index create FAILED in "+lEndadd+" ms."); }
        }

        // if thesaurus is not null, rebuild this thesaurus
        if ((sThesaurus != null) && (sUsername != null) && (sPassword != null)) {
            long lStartadd = System.currentTimeMillis();
            RebuildThesaurus(sThesaurus, sUsername, sPassword, dbc);
            long lEndadd = System.currentTimeMillis() - lStartadd;
            if (bSuccess) { api.Log.Log("Thesaurus "+sThesaurus+" rebuilt in "+lEndadd+" ms."); }
            else { api.Log.Log("Thesaurus "+sThesaurus+" rebuilt in "+lEndadd+" ms."); }
        }
    }

    // rebuild the index -- assumption: index already exists
    // execute ctx_ddl.sync_index('DocFullText');
    public static boolean RebuildIndex(String sIndex, Connection dbc) {
        CallableStatement cs = null;

        if (true) { api.Log.Log("begin ctx_ddl.sync_index ("+sIndex+"); end;"); }
        try {
            cs = dbc.prepareCall ("begin ctx_ddl.sync_index (:1); end;"); cs.setString(1, sIndex);
            cs.execute(); return true;
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (cs != null) { try { cs.close(); } catch (Exception e) {} cs = null; }}
    }

    // rebuild the index -- assumption: index already exists
    // execute ctx_ddl.sync_index('DocFullText');
    public static boolean RebuildThesaurus(String sThesaurus, String sUsername, String sPassword, Connection dbc) {
        CallableStatement cs = null;

        try {
            cs = dbc.prepareCall ("begin synchthesaurus (:1, :2, :3); end;");
            cs.setString(1, sThesaurus); cs.setString(2, sUsername); cs.setString(3, sPassword);
            cs.execute(); return true;
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (cs != null) { try { cs.close(); } catch (Exception e) {} cs = null; }}
    }

    // build the index -- assumption: index does not already exist
    public static boolean BuildIndex(String sTable, String sIndex, String sColumn, Connection dbc) {
        return BuildIndex(sTable, sIndex, sColumn, dbc, false);
    }
    public static boolean BuildIndex(String sTable, String sIndex, String sColumn, Connection dbc, boolean bFullText) {
        Statement stmt = null;
        String sSQL = "drop index "+sIndex;

        if (true) { api.Log.Log(sSQL); }
        try { stmt = dbc.createStatement();
              stmt.execute(sSQL);
        } catch (Exception e) { api.Log.LogError(e);
        } finally { if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }}

        stmt = null;
        sSQL = "create index "+sIndex+" on "+sTable+" ("+sColumn+") indextype is ctxsys.context";
        if (bFullText) { sSQL = sSQL + " parameters ('datastore COMMON_DIR storage index_location')"; }

        if (true) { api.Log.Log(sSQL); }
        try { stmt = dbc.createStatement();
            stmt.execute(sSQL); return true;
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }}
    }
}
