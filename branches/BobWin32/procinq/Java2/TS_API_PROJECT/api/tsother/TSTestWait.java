/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Jan 22, 2003
 * Time: 11:47:08 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsother;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Connection;

public class TSTestWait
{

    private static int iCallCounter = 0;
    private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

    // ***********************************************************
    public static void handleTSapiRequest(HttpServletRequest req,
                                          HttpServletResponse res,
                                          api.APIProps props,
                                          PrintWriter out,
                                          Connection dbc
                                          )
            // ***********************************************************
            throws Throwable
    {
        int iWaittimeMS = Integer.parseInt((String) props.get("WAITTIME", true));

        int iWaittimeMSRemaining = iWaittimeMS ;

        int iLoop=0;
        long lStart = System.currentTimeMillis();
        while ( true )
        {
            int iWaitTimeMSThisLoop = -1;
            boolean bBreak = false;
            if ( iWaittimeMSRemaining < 0 ) {
                System.out.print(" error - wait < 0 ");
                throw new Exception (" error - wait < 0 ");
            }
            else if ( iWaittimeMSRemaining < 60000 ) {
                iWaitTimeMSThisLoop = iWaittimeMSRemaining;
                bBreak = true;
            }
            else
            {
                iWaitTimeMSThisLoop = 60000;
                iWaittimeMSRemaining = iWaittimeMSRemaining - 60000;
                bBreak = false;
            }

            // System.out.println (  iLoop + ". " + new java.util.Date() +  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TSMETA IN FIXED WAIT MODE start wait ms [" + iWaitTimeMSThisLoop+ "]" );
            com.indraweb.util.clsUtils.pause_this_many_milliseconds ( iWaitTimeMSThisLoop );
            // System.out.println (  iLoop + ". " + new java.util.Date() +  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DONE WAIT TSMETA IN FIXED WAIT MODE start wait ms [" + iWaitTimeMSThisLoop+ "]" );
            // System.out.flush();
            //out.println("<WAITING>" + iLoop + ":" + iWaitTimeMSThisLoop + "</WAITING>");
            //out.flush();

            if ( bBreak )
                break;
            iLoop++;
        }
        long lDur = System.currentTimeMillis() - lStart ;

        out.println("<WAITCOMPLETED>" + iWaittimeMS + ":" + lDur + "</WAITCOMPLETED>");
    }

} // public static void handleTSapiRequest ( HttpServletRequest req, api.APIProps props, PrintWriter out, Connection dbc )





