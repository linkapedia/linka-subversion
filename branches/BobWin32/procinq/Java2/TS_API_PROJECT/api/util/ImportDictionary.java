package api.util;

import java.io.*;
import java.sql.*;


import java.text.DecimalFormat;
import org.apache.log4j.Logger;

/**
 * Import a dictionary into database to check signatures
 *
 *	@authors Andres Restrepo, All Rights Reserved.
 *
 *	@return	SUCCESS tag, if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
<TSRESULT>
<SUCCESS>Add dictionary ok</SUCCESS>
</TSRESULT>
\endverbatim
 */
public class ImportDictionary {

    // TSAddImageNode (imageid,description,url,image(blob),nodeid)
    private static final Logger log = Logger.getLogger(ImportDictionary.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        log.debug("ImportDictionary");
        byte[] file = (byte[]) props.get("inputstream", null);
        String name = (String) props.get("name", null);
        log.debug("Dictionary name- >"+name);
        PreparedStatement pstmt = null;
        String sSql = "";
        int codSec = -1;

        //insert dictionary
        try {
            codSec = insertDictionary(dbc, name, null);
            if (codSec == -1) {
                log.error("ImportDictionary not sequence available");
                return;
            }
        } catch (SQLException se) {
            log.error("ImportDictionary -> insertDictionary SQLException: " + se.getMessage());
            return;
        } catch (Exception e) {
            log.error("ImportDictionary -> insertDictionary Exception: " + e.getMessage());
            return;
        }

        log.debug("<-------insert words------>");
        //insert words
        try {
            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(file)));
            String wordLine = null;
            sSql = "insert into words (dictionaryid,word,status,ocurrence) values(?,?,?,?)";
            pstmt = dbc.prepareStatement(sSql);
            DecimalFormat twoDigits = new DecimalFormat("0");
            int size = file.length;
            int sizePartial = 0;
            while ((wordLine = bufferReader.readLine()) != null) {
                sizePartial = sizePartial + wordLine.getBytes().length + 2;
                if (sizePartial > size) {
                    sizePartial -= 2;
                }
                log.debug("WORD: " + wordLine);
                try {
                    pstmt.setInt(1, codSec);
                    pstmt.setString(2, wordLine);
                    pstmt.setString(3, null);
                    pstmt.setString(4, null);
                    pstmt.executeUpdate();
                    log.debug("insert word -> " + wordLine + " is OK");
                } catch (SQLException se) {
                    log.error("ImportDictionary SQLException: " + se.getMessage());
                } catch (Exception e) {
                    log.error("ImportDictionary Exception: " + e.getMessage());
                }
                long percent = ((sizePartial) * 100) / size;
                log.debug("PERCENT % " + percent);
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                out.flush();
            }
            out.println("<SUCCESS>Add dictionary ok</SUCCESS>");
        } catch (IOException ioe) {
            log.error("ImportDictionary (reading file) IOException: " + ioe.getMessage());
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }

    }

    private static int insertDictionary(Connection dbc, String name, String status) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String sSql = "";
        int codSec = -1;
        try {
            sSql = "select DICTIONARY_DICTIONARYID_SEQ.nextval from dual";
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSql);
            if (rs.next()) {
                codSec = rs.getInt(1);
            }
            if (codSec == -1) {
                //error get sequence
                //...
                log.error("insertDictionary not sequence available");
                return -1;
            } else {
                log.debug("Sequence available: " + codSec);
                //insert row DICTIONARY
                log.debug("insertDictionary");
                sSql = "insert into dictionary (id,name,status) values (?,?,?)";
                pstmt = dbc.prepareStatement(sSql);
                pstmt.setInt(1, codSec);
                pstmt.setString(2, name);
                pstmt.setString(3, status);
                pstmt.executeUpdate();
                log.debug("insertDictionary OK");
                return codSec;
            }
        } catch (Exception e) {
            log.error("ImportDictionary Exception: " + e.getMessage());
        } finally {
            //close conecction
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return -1;
    }
}
