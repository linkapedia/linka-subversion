package api.util;

/**
 *
 * @author andres
 */
public class S3StorageException extends Exception {

    public S3StorageException() {
        super();
    }

    public S3StorageException(String message) {
        super(message);
    }

    public S3StorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public S3StorageException(Throwable cause) {
        super(cause);
    }
}
