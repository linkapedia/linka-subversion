package api.util.google.crawl;

import api.util.interfaces.INotifyAll;
import java.util.List;
import java.util.TimerTask;

public class Handler extends TimerTask {
    
    private INotifyAll notify;
    private List<WorkerSearch> list;
    
    public Handler(List<WorkerSearch> list, INotifyAll notify) {
        this.list = list;
        this.notify = notify;
    }
    
    @Override
    public void run() {
        int cont = 0;
        for (WorkerSearch ws : list) {
            if (!ws.getThread().isAlive()) {
                cont++;
            }
        }
        if (cont == list.size()) {
            notify.onFinish("Google crawl Finished");
        }
    }
    
    public void startThread() {
        for (int i = 0; i < list.size(); i++) {
            this.list.get(i).getThread().start();
        }
    }
}
