package api.util.search.images;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class SearchImages {

    private static final Logger log = Logger.getLogger(SearchImages.class);

    /**
     * method to return info of the image -title,url,image(byte[])
     *
     * @param urlParam
     * @return hashmap with the information
     * @throws IOException
     */
    public HashMap<String, Object> getImageInfoURL(String urlParam) throws IOException {

        HttpURLConnection con = null;
        InputStream is = null;
        ByteArrayOutputStream bao = null;
        URL url = new URL(urlParam);
        log.debug("url: " + url);
        try {
            con = (HttpURLConnection) url.openConnection();
            HashMap<String, Object> mapResult = new HashMap<String, Object>();

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = con.getInputStream();
                byte barray[] = new byte[1024];
                bao = new ByteArrayOutputStream();
                int b = 0;
                while ((b = is.read(barray, 0, 1024)) != -1) {
                    bao.write(barray, 0, b);
                }
                mapResult.put("image", bao.toByteArray());
                String title = urlParam.substring(urlParam.lastIndexOf("/") + 1, urlParam.length());
                log.debug("TITLE IMAGE: -> " + title);
                mapResult.put("title", title);
                log.debug("URL IMAGE: -> " + urlParam);
                mapResult.put("url", urlParam);

                return mapResult;
            } else {
                log.debug("Image not found");
                return null;
            }
        } finally {
            if (is != null) {
                is.close();
            }
            if (con != null) {
                con.disconnect();
            }
        }
    }
}
