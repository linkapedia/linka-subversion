package api.util.search.images;

import api.util.S3Storage;
import api.util.S3StorageException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
//import com.intellisophic.linkapedia.amazon.services.S3.bo.ManageBucket;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.linkapedia.images.bo.BuilderImage;

/**
 *
 * @author andres
 */
public class WorkerImagesS3 {

    private static final Logger LOG = Logger.getLogger(WorkerImagesS3.class);
    private final String FOLDER_IMAGE = "images";
    private final String BUCKET_NAME = "nodeimages";

    /**
     *
     * @param id
     * @param images
     * @return index with image error
     */
    public int process(Long id, List<byte[]> images) {
        StringBuilder idSb = new StringBuilder();
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentType("image/jpeg");

        //transform image the is.
        idSb.append(id);
        idSb = idSb.reverse();

        byte[] bytes;
        int index = 0;
        int numberImagesSaved = 0;
        for (byte[] is : images) {
            try {
                BufferedImage image = ImageIO.read(new ByteArrayInputStream(is));
                BuilderImage builder = new BuilderImage(image, BuilderImage.SITE_IMAGE);
                image = builder.build();
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                ImageIO.write(image, "JPG", bao);
                String key = idSb.toString() + "/" + FOLDER_IMAGE + "/" + idSb.toString() + "_" + index + ".jpg";
                S3Storage.storageData(BUCKET_NAME, key, new ByteArrayInputStream(bao.toByteArray()), meta);
                numberImagesSaved++;
                if (index == 4) {
                    break;
                }
                index++;
            } catch (S3StorageException e) {
                LOG.error("Error S3StorageException: storageing image ", e);
                continue;
            } catch (IOException e) {
                LOG.error("Error IO: tranform image ", e);
                continue;
            } catch (Exception e) {
                LOG.error("Exception general in the process", e);
                continue;
            } finally {
            }
        }
        return numberImagesSaved;
    }
}
