/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.util.search.images;

import api.util.interfaces.ISearchNotify;
import com.iw.db.ConnectionFactory;
import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class NotifySearchBing implements ISearchNotify<List<HashMap<String, Object>>> {

    private static final Logger LOG = Logger.getLogger(NotifySearchBing.class);
    private PrintWriter out;
    private int num;
    private long percent = 0;
    private Connection con;
    private DecimalFormat twoDigits = new DecimalFormat("0");
    private int i = 0;
    private String sSQL = null;
    private PreparedStatement pstmt = null;

    public void close() {
        try {
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            LOG.debug("NotifySearchBing: SQLException " + ex.getMessage());
        }
    }

    public NotifySearchBing(PrintWriter out) {

        this.out = out;
        con = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        sSQL = "insert into NODEIMAGES(imageid, description, url, nodeid, image) values(SEC_IMAGESNODES.nextval, ?, ?, ?, ?)";
        try {
            pstmt = con.prepareStatement(sSQL);
        } catch (Exception ex) {
            LOG.error("Exception: " + ex.getMessage());
        }
    }

    public void setPlaceMessage(PrintWriter out) {
        this.out = out;
    }

    public void setNum(int num) {
        this.num = num;
    }

    /**
     *
     * @param listMap
     * @throws Exception
     */
    @Override
    synchronized public void onSearchResult(List<HashMap<String, Object>> result, Map<String, String> params) throws SQLException {
        //save into database
        for (HashMap<String, Object> map : result) {
            pstmt.setString(1, (String) map.get("title"));
            pstmt.setString(2, (String) map.get("url"));
            pstmt.setInt(3, Integer.parseInt((String) map.get("nodeid")));
            pstmt.setBinaryStream(4, new ByteArrayInputStream((byte[]) map.get("image")));
            pstmt.executeUpdate();
            LOG.debug(map.get("nodeid"));
            LOG.debug(map.get("title"));
            LOG.debug(map.get("url"));
            LOG.debug("INSERT IMAGE OK!");
        }
    }

    @Override
    synchronized public void pr() {
        percent = ((i + 1) * 100) / num;
        out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
        LOG.debug("finished one proccess: " + percent + "%");
        out.flush();
        i++;
    }
}