package api.util;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import java.io.InputStream;

/**
 *
 * @author andres
 */
public class S3Storage {

    private static final String AWS_ACCESS_KEY;
    private static final String AWS_SECRET_KEY;

    static {
        AWS_ACCESS_KEY = ConfServer.getValue("linkapedia.aws.accessKey");
        AWS_SECRET_KEY = ConfServer.getValue("linkapedia.aws.secretKey");
    }

    public static void storageData(String bucketName, String key, InputStream is, ObjectMetadata meta) throws S3StorageException {
        BasicAWSCredentials yourAWSCredentials = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);
        AmazonS3Client amazonS3Client = new AmazonS3Client(yourAWSCredentials);
        amazonS3Client.createBucket(bucketName);
        amazonS3Client.putObject(bucketName, key, is, meta);
    }
}
