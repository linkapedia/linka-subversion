package api.util.split;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import org.apache.log4j.Logger;

public class UtilThread extends Thread {

    private static final Logger log = Logger.getLogger(UtilThread.class);
    private File pathArchiveToSplit;
    private String NAME_DIRECTORY = "Directory";
    private String NAME_NEWARCHIVES = "file";

    public UtilThread(File file) {
        this.pathArchiveToSplit = file;
    }

    @Override
    public void run() {
        File archive = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {

            archive = new File(pathArchiveToSplit.getAbsolutePath());
            fr = new FileReader(archive);
            br = new BufferedReader(fr);
            int k = 1;
            String line;
            StringBuilder stringB = new StringBuilder();
            File directory = new File(pathArchiveToSplit.getParent() + File.separator + NAME_DIRECTORY);
            if (!directory.exists()) {
                directory.mkdir();
            }
            File directoryToArchive = new File(directory.getAbsolutePath() + File.separator + pathArchiveToSplit.getName().substring(0, pathArchiveToSplit.getName().indexOf(".")));
            if (!directoryToArchive.exists()) {
                directoryToArchive.mkdir();
            }

            while ((line = br.readLine()) != null) {
                stringB.append(line);
                line = line.toLowerCase();
                if (line.contains("</html")) {

                    File fileToCreate = new File(directoryToArchive.getAbsolutePath() + File.separator + NAME_NEWARCHIVES + "-" + k + ".txt");
                    fileToCreate.createNewFile();
                    FileWriter fw = null;
                    PrintWriter pw = null;
                    try {
                        fw = new FileWriter(fileToCreate);
                        pw = new PrintWriter(fw);
                        pw.println(stringB.toString());
                    } catch (Exception exception) {
                        log.error("Error in UtilThread::run()", exception);
                    } finally {
                        try {
                            if (null != fw) {
                                fw.close();
                            }
                        } catch (Exception exception) {
                            log.error("Error in UtilThread::run()", exception);
                        }
                    }
                    stringB.delete(0, stringB.length());
                    k++;
                }

            }
            archive.delete();
            log.info("Archives generated: " + k);
        } catch (Exception exception) {
            log.error("Error in UtilThread::run()", exception);
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception exception) {
                log.error("Error in UtilThread::run()", exception);
            }
        }
    }
}
