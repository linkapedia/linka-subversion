package api.util;

import api.util.bean.PackNode;
import api.util.bean.SignatureBean;
import com.iw.db.ConnectionFactory;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class NodeUtils {

    private static final Logger LOG = Logger.getLogger(NodeUtils.class);
    private final Connection con;

    public NodeUtils(Connection con) {
        this.con = con;
    }

    public String getRootNode(int corpusId) throws Exception {
        LOG.debug("NodeUtils: getRootNode");
        String sql = "SELECT NODE.NODEID FROM NODE WHERE NODE.PARENTID=-1 and NODE.CORPUSID = ?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, corpusId);
            rs = pstmt.executeQuery();
            String node = "";
            if (rs.next()) {
                node = String.valueOf(rs.getInt("nodeid"));
            }
            return node;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }

    }

    /**
     * get images from a nodeid
     *
     * @param nodeid
     * @return
     * @throws SQLException
     */
    public List<byte[]> getImagesNodeId(long nodeid) throws SQLException {
        LOG.info("Get images for the nodeId: " + nodeid);
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("Error getting connection from oracle");
        }
        List<byte[]> listReturn = new ArrayList<byte[]>();
        String sql = "SELECT IMAGE FROM NODEIMAGES WHERE NODEID = ?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, nodeid);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listReturn.add(rs.getBytes("IMAGE"));
            }
            return listReturn;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public List<byte[]> getImagesByCorpusId(int corpusId) throws SQLException {
        LOG.info("Get images for the corpusId: " + corpusId);
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("Error getting connection from oracle");
        }
        List<byte[]> listReturn = new ArrayList<byte[]>();
        String sql = "SELECT IMAGE FROM CORPUSIMAGES WHERE CORPUSID = ?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, corpusId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listReturn.add(rs.getBytes("IMAGE"));
            }
            return listReturn;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * get PackNode with information (nodeid,nodetitle,source)
     *
     * @param nodeid
     * @return
     * @throws Exception
     */
    public PackNode getPackNode(int nodeid) throws Exception {
        LOG.debug("NodeUtils: getPackNode");
        PackNode pack = null;
        String sql = "SELECT NODE.NODEID, NODE.NODETITLE,NODE.NODEDESC, NODEDATA.NODESOURCE FROM NODE, NODEDATA WHERE NODE.NODEID=? and NODE.NODEID = NODEDATA.NODEID";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                String node = String.valueOf(rs.getInt("nodeid"));
                String nodetitle = rs.getString("nodetitle");
                String term = rs.getString("nodesource");
                String desc = rs.getString("nodedesc");
                if (term != null && !term.isEmpty()) {
                    PackNode pn = new PackNode();
                    pn.setNodeid(node);
                    pn.setNodetitle(nodetitle);
                    pn.setNodesource(term);
                    pn.setNodeDescription(desc);
                    pack = pn;
                }
            }
            return pack;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

   

    /**
     * get tree nodes with information (nodeid,nodetitle,source)
     *
     * @param nodeid
     * @return
     * @throws Exception
     */
    public List<PackNode> getTreeNodes(int nodeid) throws Exception {
        LOG.debug("NodeUtils: getTreeNodes");
        List<PackNode> listReturn = new ArrayList<PackNode>();
        String sql = "SELECT p.NODEID,p.NODETITLE, p.NODEDESC, NODEDATA.NODESOURCE FROM NODEDATA,(SELECT NODEID, NODETITLE, NODEDESC FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID) p WHERE NODEDATA.NODEID(+)=p.NODEID ORDER BY NODEDATA.NODEID";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String node = String.valueOf(rs.getInt("nodeid"));
                String nodetitle = rs.getString("nodetitle");
                String term = rs.getString("nodesource");
                String desc = rs.getString("nodedesc");
                if (term != null) {
                    PackNode pn = new PackNode();
                    pn.setNodeid(node);
                    pn.setNodetitle(nodetitle);
                    pn.setNodesource(term);
                    pn.setNodeDescription(desc);
                    listReturn.add(pn);
                }
            }
            return listReturn;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    /**
     * get tree nodes list nodeids
     *
     * @param nodeid
     * @return
     * @throws Exception
     */
    public List<Long> getTreeNodesNodeid(long nodeid) throws Exception {
        LOG.debug("NodeUtils: getTreeNodesNodeid");
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("Error getting connection from oracle");
        }
        List<Long> listReturn = new ArrayList<Long>();
        String sql = "SELECT n.NODEID FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, nodeid);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listReturn.add(rs.getLong("NODEID"));
            }
            return listReturn;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    //logic to selected a bridge node tree down
    public List<Integer> getBridgeNodes(int nodeId) throws Exception {
        LOG.debug("NodeUtils: getBridgeNodes");
        List<Integer> listReturn = new ArrayList<Integer>();
        String sql = "select nod.nodeid, (select count(*) from node where parentid = nod.nodeid)"
                + " as NUM_CHILDREN, (select count(*) from signature where nodeid = nod.nodeid ) as NUM_SIGNATURES"
                + " from node nod where nod.nodeid in"
                + " (select nodeid from node START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID)";

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeId);
            rs = pstmt.executeQuery();
            int nodeidAux;
            int numChildren;
            int numSig;
            while (rs.next()) {
                nodeidAux = rs.getInt("nodeid");
                numChildren = rs.getInt("NUM_CHILDREN");
                numSig = rs.getInt("NUM_SIGNATURES");
                if (numChildren > 0 && numSig < 2) {
                    //is bridge node
                    listReturn.add(nodeidAux);
                }
            }
            return listReturn;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    /**
     * get signatures from one node
     *
     * @param nodeid
     * @return
     * @throws Exception
     */
    public List<SignatureBean> getSignatures(int nodeid) throws Exception {
        LOG.debug("NodeUtils: getSignatures");
        List<SignatureBean> listReturn = new ArrayList<SignatureBean>();
        String sql = "SELECT sig.signatureword, sig.signatureoccurences, sig.lang FROM SIGNATURE sig WHERE sig.nodeid = ?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                SignatureBean sb = new SignatureBean();
                sb.setWord(rs.getString("signatureword").trim());
                sb.setOcurrence(rs.getInt("signatureoccurences"));
                sb.setLang(rs.getString("lang"));
                listReturn.add(sb);
            }
            return listReturn;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    /**
     *
     * @param nodeid
     * @param terms
     * @param deletefirst
     * @return
     * @throws Exception
     */
    public int insertSignatures(int nodeid, List<SignatureBean> terms, boolean deletefirst) throws Exception {
        LOG.debug("insertSignatures");
        if (terms.isEmpty()) {
            return 0;
        }
        con.setAutoCommit(false);
        PreparedStatement pstmt = null;
        Statement stmt = null;
        try {
            if (deletefirst) {
                String sSQL = " DELETE FROM SIGNATURE WHERE NodeID = " + nodeid;
                stmt = con.createStatement();
                stmt.executeUpdate(sSQL);
            }
            String sql = "INSERT INTO SIGNATURE(NODEID, SIGNATUREWORD, SIGNATUREOCCURENCES, LANG) values(?, ?, ?, ?)";
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            for (SignatureBean sb : terms) {
                pstmt.setString(2, sb.getWord());
                pstmt.setInt(3, sb.getOcurrence());
                pstmt.setString(4, sb.getLang());
                pstmt.executeUpdate();
            }
            con.commit();
            LOG.debug("Commit ok");
            return 1;
        } catch (Exception e) {
            LOG.error("Error delete and add signatures to nodeid: " + nodeid + " " + e.getMessage());
            con.rollback();
            return 0;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * insert musthaves to one node
     *
     * @param nodeid
     * @param terms
     * @return
     */
    public int insertMusthaves(int nodeid, List<String> terms) throws Exception {
        LOG.debug("insertMusthaves");
        //consult musthave to not insert duplicates
        List<String> list = new ArrayList<String>();
        String sql = "SELECT MUSTWORD from MUSTHAVE WHERE NODEID=?";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("MUSTWORD"));
            }
            //insert musthave
            sql = "INSERT INTO MUSTHAVE(NODEID, MUSTWORD, LANG) values(?, ?, 'EN')";
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            for (String term : terms) {
                if (!list.contains(term)) {
                    pstmt.setString(2, term);
                    pstmt.executeUpdate();
                } else {
                    LOG.info("MustHave already exits: " + term + " NodeID: " + nodeid);
                }
            }
            return 1;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }

    }

    public int validateAuthorizationByCorpus(int corpusid, String sKey, PrintWriter out) throws Exception {
        LOG.debug("validateAuthorization");
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        if (!u.IsAdmin(corpusid, out)) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * use this method to security API
     *
     * @param nodeid
     * @param sKey
     * @param out
     * @return
     * @throws Exception
     */
    public int validateAuthorization(int nodeid, String sKey, PrintWriter out) throws Exception {
        LOG.debug("validateAuthorization");
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        String sSQL = " SELECT CORPUSID FROM NODE WHERE NODEID = " + nodeid;
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sSQL);
        rs.next();
        try {
            int iCorpusId = rs.getInt(1);
            if (!u.IsAdmin(iCorpusId, out)) {
                return 0;
            } else {
                return 1;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * delete musthaves recursively from any node
     *
     * @param nodeid
     * @return
     */
    public int deleteMH(int nodeid) throws Exception {
        LOG.debug("NodeUtils: deleteMH");
        String sql = "DELETE FROM MUSTHAVE mh WHERE mh.NODEID IN(SELECT NODEID FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID)";
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            pstmt.executeUpdate();
            return 1;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    /**
     * delete signatures recursively from any node
     *
     * @param nodeid
     * @return
     * @throws Exception
     */
    public int deleteSig(int nodeid) throws Exception {
        LOG.debug("NodeUtils: deleteSig");
        String sql = "DELETE FROM SIGNATURE sig WHERE sig.NODEID IN(SELECT NODEID FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID)";
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            pstmt.executeUpdate();
            return 1;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    /**
     * delete images recursively from any node
     *
     * @param nodeid
     * @return
     * @throws Exception
     */
    public int deleteImages(int nodeid) throws Exception {
        LOG.debug("NodeUtils: deleteImages");
        String sql = "DELETE FROM NODEIMAGES ni WHERE ni.NODEID IN(SELECT NODEID FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID)";
        PreparedStatement pstmt = null;
        try {
            pstmt = this.con.prepareStatement(sql);
            pstmt.setInt(1, nodeid);
            pstmt.executeUpdate();
            return 1;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    /**
     * get all nodes to search (Bing(Images) | Google(web Pages))
     *
     * @param nodeId
     * @param tax
     * @return
     * @throws Exception
     */
    public Map<String, String> getNodeSearch(int nodeId, String tax) throws Exception {
        LOG.debug("getNodeSearch(int, String, Connection)");
        String sSQL = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        Map<String, String> map = new HashMap<String, String>();
        Map<String, String> nodeMap = new HashMap<String, String>();

        sSQL = "SELECT NODEID,NODETITLE FROM NODE n WHERE n.PARENTID<>(-1) START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID";
        try {
            pstmt = this.con.prepareStatement(sSQL);
            pstmt.setInt(1, nodeId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                map.put(String.valueOf(rs.getInt("nodeid")), rs.getString("nodetitle"));
            }
            //select musthaves from each nodeid
            if ((map != null) && (!map.isEmpty())) {
                sSQL = "SELECT MUSTWORD FROM MUSTHAVE WHERE NODEID=?";
                pstmt = this.con.prepareStatement(sSQL);
                String wordToMap = "";
                String firstMustWord = "";
                String mustword = "";
                String key = "";
                String value = "";
                boolean i = true;
                boolean f = false;
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    key = entry.getKey();
                    value = entry.getValue();
                    pstmt.setInt(1, Integer.valueOf(key));
                    rs = pstmt.executeQuery();
                    firstMustWord = "";
                    mustword = "";
                    i = true;
                    f = false;
                    while (rs.next()) {
                        mustword = rs.getString("mustword");
                        if (i) {
                            firstMustWord = mustword;
                            i = false;
                        }
                        if (mustword.equalsIgnoreCase(value)) {
                            f = true;
                            break;
                        }
                    }
                    wordToMap = "";
                    if (f) {
                        if (!mustword.equals("")) {
                            wordToMap = getTermToSearch(tax, mustword);
                        }
                    } else if (!i) {
                        if (!firstMustWord.equals("")) {
                            wordToMap = getTermToSearch(tax, firstMustWord);
                        }
                    }
                    if (!wordToMap.equals("")) {
                        nodeMap.put(key, normalizeTerm(wordToMap));
                    }
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return nodeMap;
    }

    private static String getTermToSearch(String tax, String mustword) {
        String word = (tax.equals("") ? "" : "\"" + tax + "\" ");
        mustword = mustword.trim();
        if (StringUtils.countMatches(mustword, " ") <= 1) {
            word += "\"" + mustword + "\"";
        } else {
            word += mustword;
        }
        return word;
    }

    private static String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    private static String normalizeTerm(String s) {
        String t = unAccent(s);
        t = t.replaceAll("'", "");
        return t;
    }

    /**
     * get affinity term to one corpus this return "" if the affinity property
     * is not activated or affinity term is null
     *
     * @param corpusid
     * @return
     * @throws Exception
     */
    public String getFirstAffinityTerm(int corpusid) throws Exception {
        LOG.debug("getFirstAffinityTerm(int)");
        String sSQL = "SELECT CORPUS_NAME, CORPUSTERMS FROM CORPUS WHERE CORPUSID=? AND CORPUSAFFINITY=1";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String affinityTerm = null;
        String corpusName = null;
        String[] terms = null;
        try {
            pstmt = this.con.prepareStatement(sSQL);
            pstmt.setInt(1, corpusid);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                affinityTerm = rs.getString("CORPUSTERMS");
                corpusName = rs.getString("CORPUS_NAME");
                if ((affinityTerm != null) && (!affinityTerm.isEmpty())) {
                    terms = affinityTerm.split(",");
                    //new logic
                    boolean i = true;
                    boolean f = false;
                    String firstAffinityTerm = "";
                    for (String s : terms) {
                        if (i) {
                            firstAffinityTerm = s;
                            i = false;
                        }
                        if (s.equalsIgnoreCase(corpusName)) {
                            f = true;
                            break;
                        }
                    }
                    if (f) {
                        return corpusName;
                    } else if (!i) {
                        return firstAffinityTerm;
                    }
                }


            } else {
                LOG.debug("Affinity is not activate or not have affinity terms");
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return "";
    }
}