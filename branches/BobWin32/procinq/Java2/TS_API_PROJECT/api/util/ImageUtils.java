package api.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.linkapedia.images.bo.BuilderImage;
import org.linkapedia.images.util.ImageCreation;

/**
 *
 * @author andres
 */
public class ImageUtils {

    private static final Logger LOG = Logger.getLogger(ImageUtils.class);

    /**
     * get byte from inputstream to resuse the inputstream
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static byte[] getBytes(InputStream is) throws Exception {
        byte[] buffer = new byte[8192];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int n;
        baos.reset();
        while ((n = is.read(buffer, 0, buffer.length)) != -1) {
            baos.write(buffer, 0, n);
        }
        return baos.toByteArray();
    }

    /**
     * create logic for transform images(view in web page)
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static byte[] transformImageThumnail(byte[] data) throws Exception {
        final int WIDTH = 80;
        final int HEIGHT = 60;
        BufferedImage buffered = ImageIO.read(new ByteArrayInputStream(data));

        buffered = ImageCreation.scaleImage(buffered, HEIGHT, WIDTH, true);
        buffered = ImageCreation.convertToJPG(buffered, 0.25f);

        ByteArrayOutputStream bao = new ByteArrayOutputStream();

        ImageIO.write(buffered, "JPG", bao);
        return bao.toByteArray();
    }

    /**
     * create logic for transform images(view in web page)
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static byte[] transformImage(byte[] data) throws Exception {

        BufferedImage buffered = ImageIO.read(new ByteArrayInputStream(data));

        BuilderImage builder = new BuilderImage(buffered, BuilderImage.SITE_IMAGE);
        buffered = builder.build();

        ByteArrayOutputStream bao = new ByteArrayOutputStream();

        ImageIO.write(buffered, "JPG", bao);
        return bao.toByteArray();
    }
}
