/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 22, 2003
 * Time: 11:18:11 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.util;

import java.lang.reflect.Method;

public class UtilReflection
{
    public static void printObjectArrayClassNames ( Object[] oArr )
    {
        for ( int i = 0; i < oArr.length; i++ )
        {
            Object o = oArr[i];
            String sClassName = o.getClass().getName();
            System.out.println(" object arr elem [" + i + "] classname [" + sClassName + "]" );
        }
    }

    public static void printClassNameMethodsAndSigs ( Class c, String sMethName)
    {
        String sClassName = c.getName();
        System.out.println("Class name [" + sClassName + "]" );
        Method[] mArr = c.getDeclaredMethods();
        for ( int i = 0; i < mArr.length; i++ )
        {
            Method meth = mArr[i];

            if ( sMethName == null || meth.getName().equals ( sMethName ) )
            {
                System.out.println(" Method name [" + i + "] methname [" + meth.getName() + "]" );
                Class[] cArrArgsThisMethod = meth.getParameterTypes();
                for ( int j = 0; j < cArrArgsThisMethod.length; j++ )
                {
                    String sClassNameArg = cArrArgsThisMethod[j].getName();
                    System.out.println("  Arg [" + j + "] classname [" + sClassNameArg + "]" );
                }

            }
            else
                System.out.println(" Method name not fitting spec [" + i + "] methname [" + meth.getName() + "]" );
        }
    }

}
