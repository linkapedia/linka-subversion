
package api.util.interfaces;

/**
 *
 * @author andres
 */
public interface IWorker {

    public long workTime();
    public Thread getThread();
}
