package api.util.bean;

/**
 *
 * @author andres
 */
public class PackNode {
    private String nodeid;
    private String nodetitle;
    private String nodeDescription;
    private String nodesource;

    public String getNodeid() {
        return nodeid;
    }

    public void setNodeid(String nodeid) {
        this.nodeid = nodeid;
    }

    public String getNodesource() {
        return nodesource;
    }

    public void setNodesource(String nodesource) {
        this.nodesource = nodesource;
    }

    public String getNodeDescription() {
        return nodeDescription;
    }

    public void setNodeDescription(String nodeDescription) {
        this.nodeDescription = nodeDescription;
    }

    public String getNodetitle() {
        return nodetitle;
    }

    public void setNodetitle(String nodetitle) {
        this.nodetitle = nodetitle;
    }
    
    
    
    
}
