package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Given an ROC Setting ID, find and return all available tiers (stars) from the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  RID     Unique ROC setting identifier for this bucket.
 *  @param  Max (optional) Maximum number of tiers to return (default is 3)
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSGetAvailableStars&RID=1&SKEY=993135977

 *	@return	a series of STAR tags if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <STARS>
        <STAR1>1</STAR1>
        <STAR2>2</STAR2>
        <STAR3>3</STAR3>
    </STARS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetAvailableStars
{
	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception {

        Statement stmt = null;
        ResultSet rs = null;
        Hashtable ht = new Hashtable();

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            String sRID = (String) props.get ("rid", true);
            String sMax = (String) props.get ("max", "3");

            String sSQL = " select starCount from ROCBUCKETLOCALCLASSIFIER where rocSettingID = "+sRID;

            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            int loop = 0;
            while ( rs.next() ) {
                ht.put(rs.getString(1), "1");
            }

            out.println("<STARS>");
            for (int i=1; i <= (new Integer(sMax).intValue()); i++) {
                if (!ht.containsKey(i+"")) { out.println("   <STAR"+i+">"+i+"</STAR"+i+">"); } }
            out.println("</STARS>");
		}
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
