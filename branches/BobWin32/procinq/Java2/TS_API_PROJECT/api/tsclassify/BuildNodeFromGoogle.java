package api.tsclassify;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.EmitFilterGoogle;
import api.util.NodeUtils;
import api.util.bean.SearchBean;
import api.util.google.crawl.Handler;
import api.util.google.crawl.NotifySearch;
import api.util.google.crawl.WorkerSearch;
import api.util.interfaces.INotifyAll;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Andres
 */
public class BuildNodeFromGoogle implements INotifyAll {

    private static final Logger LOG = Logger.getLogger(BuildNodeFromGoogle.class);
    private static final int THREAD_SIZE = 4;
    private static final int WATCH_THREAD = 15000;
    private final List<WorkerSearch> workers = new ArrayList<WorkerSearch>();
    private NotifySearch notifySearch = null;
    private Handler handle = null;
    private Timer timer = null;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        LOG.debug("BuildNodeFromGoogle(APIProps, PrintWriter, Connection");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        String corpusid = (String) props.get("corpusid");

        if ((nodeid == null) || (corpusid == null)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        //get necessary parameters
        //filter custom search id - com - edu/org etc
        // 1 = org / edu
        // 2 = com
        String customsearch = (String) props.get("customsearch");
        String numLinks = (String) props.get("numlinks");

        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("customsearch", customsearch);
        mapParams.put("numlinks", numLinks);
        mapParams.put("corpusid", corpusid);

        Map<String, String> terms = new HashMap<String, String>();

        try {
            LOG.debug("Going to search terms for {corpusID=" + corpusid + "}");
            LOG.debug("Going to search terms for {nodeID=" + nodeid + "}");
            LOG.debug("Going to search terms for {customSearch=" + EmitFilterGoogle.getMessage(Integer.parseInt(customsearch)) + "}");
            LOG.debug("Going to search terms for {numLinks=" + numLinks + "}");

            //Get the first affinity term of the corpusid.
            String taxSearch = nu.getFirstAffinityTerm((Integer.parseInt(corpusid)));
            LOG.debug("First Affinity term " + taxSearch);
            terms = nu.getNodeSearch(Integer.parseInt(nodeid), taxSearch);
        } catch (Exception e) {
            LOG.error("An exception ocurred: ", e);
            throw e;
        }

        if (terms.isEmpty()) {
            LOG.debug("Not found terms to search");
        }
        //logic to the threads
        BuildNodeFromGoogle bnfg = new BuildNodeFromGoogle();
        if ((terms != null) && (!terms.isEmpty())) {
            bnfg.notifySearch = new NotifySearch(out, terms.size());
            //logic to oragnize thread
            Thread th = null;
            WorkerSearch ws;
            for (int i = 0; i < THREAD_SIZE; i++) {
                ws = new WorkerSearch(bnfg.notifySearch, mapParams);
                th = new Thread(ws);
                ws.setThread(th);
                bnfg.workers.add(ws);
            }
            //set terms to all threads
            int index = 0;
            SearchBean sb = null;
            for (Map.Entry<String, String> entry : terms.entrySet()) {
                sb = new SearchBean(entry.getKey(), entry.getValue());
                bnfg.workers.get(index).setTerm(sb);
                index = ((index + 1) % THREAD_SIZE);
            }

            bnfg.handle = new Handler(bnfg.workers, bnfg);

            bnfg.handle.startThread();
            bnfg.timer = new Timer();
            long currentTime = System.currentTimeMillis() + WATCH_THREAD;
            bnfg.timer.schedule(bnfg.handle, new java.util.Date(currentTime), WATCH_THREAD);
            synchronized (bnfg) {
                bnfg.wait();
            }

            int tErrors = 0;
            int tSuccess = 0;
            for (WorkerSearch wos : bnfg.workers) {
                for (List<String> l : wos.getArrayCSV()) {
                    out.println("<CSV>");
                    out.println(URLEncoder.encode(StringUtils.join(l, ","), "UTF-8"));
                    out.println("</CSV>");
                }
                tErrors += wos.getNumTermsError();
                tSuccess += wos.getNumTermsSuccess();
            }
            out.println("<NUMERRORS>");
            out.println(tErrors);
            out.println("</NUMERRORS>");
            out.println("<NUMSUCCESS>");
            out.println(tSuccess);
            out.println("</NUMSUCCESS>");

            out.println("<SUCCESS>Google crawl finished successfully.</SUCCESS>");
            LOG.debug("FINISHED");

        } else {
            LOG.error("Terms to consult = 0");
        }
    }

    public void onFinish(String message) {
        LOG.debug(message);
        synchronized (this) {
            this.notify();
        }
        timer.cancel();
    }
}
