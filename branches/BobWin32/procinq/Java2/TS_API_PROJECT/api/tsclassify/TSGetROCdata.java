package api.tsclassify;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.tsclassify.ROCHelper.DataCache_Classifier;
import api.tsclassify.ROCHelper.DataCache_RocBucketLocalClassifier;
import api.tsclassify.ROCHelper.DataROCCHOutputLines;
import com.indraweb.execution.Session;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilSets;
import com.iw.system.User;
import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * Use document statistics to generate an ROC curve for a given taxonomy.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param CorpusID Unique corpus identifier of the taxonomy to be evaluated.
 *
 * @note http://ITSERVER/servlet/ts?fn=tsclassify.TSGetROCdata&SKEY=801312271&CorpusID=22
 *
 * @return none \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <CLASSLOAD>Fri Jun 07 13:16:40 EDT 2002</CLASSLOAD> <CALLCOUNT>2373</CALLCOUNT> <TIMEOFCALL_MS>40</TIMEOFCALL_MS>
 * </TSRESULT> \endverbatim
 */
public class TSGetROCdata {

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        // Get input parameters.. GenreID is optional
        String sCorpusID = (String) props.get("CorpusID", true);
        String sDirectoryPath = (String) props.get("DirectoryPath", com.indraweb.execution.Session.cfg.getProp("IndraHome") + "/temp");
        api.Log.Log("Start ROC file gen to [" + sDirectoryPath + "]");

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) {
            Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            Log.LogError("in TSGetROCData", e);
            throw e;
        }

        // set cache for reload
        DataCache_Classifier.invalidateCache();
        DataCache_RocBucketLocalClassifier.invalidateCache();


        // Step 1 : get total true and false
        int[] iArrFandTCount = getTotalNumDocsFalseAndTrueInROCValidate(sCorpusID, dbc);
        int totalfalse = iArrFandTCount[0];
        int totaltrue = iArrFandTCount[1];

        // Step 2: get the set of active classifiers from CLASSIFIER table
        HashSet hsIntegerClassifierIDs = DataCache_Classifier.gethsAllIntegerClassifierIDsExcludeStarCounts(dbc);

        // Step 3: for each classifier create the .pts file
        Vector vsFileNames = new Vector();
        api.Log.Log("Processing ROC for [" + hsIntegerClassifierIDs.size() + "] classifiers");
        Iterator iter = hsIntegerClassifierIDs.iterator();
        while (iter.hasNext()) {
            Integer IClassifierID = (Integer) iter.next();
            String sClassifierName = DataCache_Classifier.getStrClassifierNameFromIntegerIClassifierID(IClassifierID, dbc);
            // don't want to analyzed the already analyzed result : classifier for score1 which is ROC itself
            if (!sClassifierName.equals(TSApplyROC.sCLASSIFIERNAME_StarCountInt)) {
                String sFileName = sDirectoryPath + "/data" + IClassifierID + ".pts";
                vsFileNames.addElement(sFileName);
                emitOneColPtsFileData(sCorpusID, IClassifierID.intValue(),
                        totalfalse, totaltrue, dbc, out, sFileName);
            }
        }

        String sSpcSepFQFileList = com.indraweb.util.UtilStrings.convertVecToString(vsFileNames, " ");
        // Step 4: CALLOUT TO ROCCH
        Vector vsExecOutput = doROCCHCallout(sSpcSepFQFileList);

        // filter to lines starting with '[' (one per slope range)
        for (int i2 = vsExecOutput.size() - 1; i2 >= 0; i2--) {
            String sLine = (String) vsExecOutput.elementAt(i2);
            if (!sLine.startsWith("[")) {
                vsExecOutput.removeElementAt(i2);
            }
        }

        int iTotalNumROCCHLines = vsExecOutput.size();
        Vector vOfDataROCCHOutputLines = new Vector();
        Enumeration enumExecOutput = vsExecOutput.elements();
        int i = 0;
        while (enumExecOutput.hasMoreElements()) {
            String sLine = (String) enumExecOutput.nextElement();
            // api.Log.Log(i + ". exec output [" + sLine + "]" );
            i++;

            DataROCCHOutputLines dataROCCHOutputLines = new DataROCCHOutputLines(sLine);
            vOfDataROCCHOutputLines.addElement(dataROCCHOutputLines);
            api.Log.Log("rocch data object #" + i + " of [" + iTotalNumROCCHLines + "] :" + dataROCCHOutputLines.getDesc());
        }

        // Step 5: MERGE ROCCH RESULTS WITH ROCBucketLocalClassifier data to get cutoffs and classifiers
        Vector vDataROCBucketLocalClassifier = DataCache_RocBucketLocalClassifier.getVecDataROCBucketLocalClassifier(sCorpusID, dbc);
        Enumeration eROCBucketLocalClassifier = vDataROCBucketLocalClassifier.elements();
        // iterate thru starcounts and fill in the classifier and cutoff score in ROCBUCKETLOCALCLASSIFIER
        while (eROCBucketLocalClassifier.hasMoreElements()) {

            DataCache_RocBucketLocalClassifier dataRocBucket = (DataCache_RocBucketLocalClassifier) eROCBucketLocalClassifier.nextElement();

            float fIsoSlope = dataRocBucket.fRATIOTNTOTP * dataRocBucket.fCOSTRATIOFPTOFN;

            api.Log.Log(dataRocBucket.iSTARCOUNT + " Star Isoslope : fRATIOTNTOTP  " + dataRocBucket.fRATIOTNTOTP
                    + " * " + "fCOSTRATIOFPTOFN " + dataRocBucket.fCOSTRATIOFPTOFN + "= IsoSlope " + fIsoSlope);
            // now find the matching ROC Entry


            boolean bFoundARange = false;
            for (int iSlopeRangeIndex = 0; iSlopeRangeIndex < vOfDataROCCHOutputLines.size(); iSlopeRangeIndex++) {
                DataROCCHOutputLines dataROCCHLine = (DataROCCHOutputLines) vOfDataROCCHOutputLines.elementAt(iSlopeRangeIndex);
                api.Log.Log("## Testing if in range ? fIsoSlope [" + fIsoSlope + "] vs dataROCCHLine range [" + dataROCCHLine.sSlopeRangeMin + "-" + dataROCCHLine.sSlopeRangeMax + "]");

                // check if this ROCCH slope range is the right one for the current isocost
                if (isInRange(fIsoSlope, iSlopeRangeIndex, dataROCCHLine)) {
                    dataRocBucket.iCLASSIFIERID = dataROCCHLine.iClassifierID;
                    dataRocBucket.dCUTOFFSCOREGTE = Float.parseFloat(dataROCCHLine.sSlopeRangeMin);
                    bFoundARange = true;
                    dataRocBucket.setClassifierCutoffAndTPFP(dataROCCHLine.iClassifierID,
                            dataROCCHLine.fScoreCutoff,
                            Float.parseFloat(dataROCCHLine.sTPRate),
                            Float.parseFloat(dataROCCHLine.sFPRate));

                    dataRocBucket.updateMeToDB(dbc);   // put the output info into the db
                }
            }

            if (!bFoundARange) {
                throw new Exception("slope range not found for dataRocBucket [" + dataRocBucket.iSTARCOUNT + "] fIsoSlope [" + fIsoSlope + "]");
            }

            DataCache_Classifier.invalidateCache();
            DataCache_Classifier.fillCache(dbc);

        }
    }

    private static boolean isInRange(float fIsoSlope, int iSlopeRangeIndex, DataROCCHOutputLines dataROCCHLine)
            throws Exception {
        String sSlopeRangeMin = dataROCCHLine.sSlopeRangeMin;
        String sSlopeRangeMax = dataROCCHLine.sSlopeRangeMax;

        //api.Log.Log ("testing sSlopeRangeMin [" + sSlopeRangeMin+ "] sSlopeRangeMax [" + sSlopeRangeMax + "]" );
        char minfirstchar = sSlopeRangeMin.charAt(0);
        if (!Character.isDigit(minfirstchar)) {
            throw new Exception("Error in ROCCH output or its parse and interpretation - slope min must be numbers in ROCCH output");
        }

        char maxfirstchar = sSlopeRangeMax.charAt(0);
        boolean bIsUpperRangeInf = false;
        if (!Character.isDigit(maxfirstchar)) {
            if (maxfirstchar != 'I') // as in first character of "Inf"
            {
                throw new Exception("Error in ROCCH output or its parse and interpretation - slope max must be numbers or Inf in ROCCH output");
            } else {
                bIsUpperRangeInf = true;
            }
        }
        if (bIsUpperRangeInf) {
            if (fIsoSlope > Float.parseFloat(sSlopeRangeMin)) {
                api.Log.Log(" yes isoslope 1 [" + fIsoSlope + "] is in range index [" + iSlopeRangeIndex + "] range [" + sSlopeRangeMin + "," + sSlopeRangeMax + "] range classifier [" + dataROCCHLine.iClassifierID + "]");
                return true;
            } else {
                return false;
            }

        } else {
            if (fIsoSlope > Float.parseFloat(sSlopeRangeMin) && fIsoSlope <= Float.parseFloat(sSlopeRangeMax)) {
                api.Log.Log(" yes isoslope 2 [" + fIsoSlope + "] is in range index [" + iSlopeRangeIndex + "] range [" + sSlopeRangeMin + "," + sSlopeRangeMax + "] range classifier [" + dataROCCHLine.iClassifierID + "]");
                return true;
            } else {
                return false;
            }

        }

    }

    private static int[] getTotalNumDocsFalseAndTrueInROCValidate(String sCorpusID, Connection dbc)
            throws Exception {
        String sSQL = " select R.ROCValidation from ROCValidation R, NodeDocument D, Node N "
                + " where N.NodeID = D.NodeID and "
                + " D.NodeID = R.NodeID and R.DocumentID = D.DocumentID and "
                + " N.CorpusID = " + sCorpusID;

        ResultSet rs = null;
        Statement stmt = null;

        try {
            api.Log.Log("sSQL [" + sSQL + "]");
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

        } catch (SQLException qe) {
            EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
        }

        // Need to get total number of results for calculating percentages
        int totaltrue = 0;
        int totalfalse = 0;
        while (rs.next()) {
            int iROCValidation = rs.getInt(1);
            if (iROCValidation == 0) {
                totalfalse++;
            } else if (iROCValidation == 1) {
                totaltrue++;
            } else {
                rs.close();
                stmt.close();
                throw new Exception("iROCValidation != 0 or 1");
            }
        }
        rs.close();
        stmt.close();
        api.Log.Log("Processing ROC got T/F counts from ROC validation");

        // Bail if no work has been done
        if ((totalfalse) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
        }
        if ((totaltrue) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
        }
        int[] iArrFandTCount = new int[2];
        iArrFandTCount[0] = totalfalse;
        iArrFandTCount[1] = totaltrue;
        return iArrFandTCount;
    }

    private static void emitOneColPtsFileData(String sCorpusID,
            int iClassifierID,
            int totalfalse,
            int totaltrue,
            Connection dbc,
            PrintWriter out,
            String sFileName) throws Exception {
        String sAscDesc = "DESC";
        if (DataCache_Classifier.getSortOrderForClassifierID(iClassifierID, dbc).intValue() != 0) {
            sAscDesc = "ASC";
        }
        String sColName = DataCache_Classifier.getColNameForClassifierID(iClassifierID, dbc);
        String sSQL = " select R.ROCValidation, D." + sColName
                + " from ROCValidation R, NodeDocument D, Node N "
                + " where R.ROCValidation is not null and N.NodeID = D.NodeID and "
                + " D.NodeID = R.NodeID and R.DocumentID = D.DocumentID and "
                + " N.CorpusID = " + sCorpusID + " order by D." + sColName + " " + sAscDesc;

        //System.out.println("sSQL [" + sSQL + "]"    );
        ResultSet rs = null;
        Statement stmt = null;
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
        } catch (SQLException qe) {
            EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
        }

        // compute precision and false positive percentages through accumulation (f * c)
        int iYes = 0;
        int iNo = 0;
        int loop = 0;
        while (rs.next()) {
            loop++;
            int iYesNo = rs.getInt(1);
            float fScore = rs.getFloat(2);

            if (iYesNo == 1) {
                iYes++;
            } else {
                iNo++;
            }
            float fPF = new Integer(iNo).floatValue() / new Integer(totalfalse).floatValue();
            float fPT = new Integer(iYes).floatValue() / new Integer(totaltrue).floatValue();

            if (loop == 1) {
                File f1 = new File(sFileName);

                if (f1.exists()) {
                    f1.delete();
                }
            }

            com.indraweb.util.UtilFile.addLineToFile(sFileName,
                    fPF + "\t" + fPT + "\tCid(" + iClassifierID + ")\tSc{"
                    + fScore + "}\tCol<" + sColName + ">\r\n");
            //out.println("   <VALUE" + loop + ">" + fPF + "\t" + fPT + "\tCid[" + ccd.iClassifierID + "]\tSc{" + fScore + "}\tCol<" + ccd.sColName+ "></VALUE" + loop + ">");
        }
        rs.close();
        stmt.close();
        api.Log.Log("wrote pts file [" + sFileName + "] lines [" + loop + "]");

    }

    private static Vector doROCCHCallout(String sSpcSepFQFileList)
            throws Exception {
        String sOSHost = (String) java.lang.System.getProperties().get("os.name");
        String sExec = null;
        if (sOSHost.toLowerCase().indexOf("windows") >= 0) {
            sExec = "cmd /c perl \"" + Session.sIndraHome + "\\ROCCH\\ROCCH\" -compress " + sSpcSepFQFileList;
            api.Log.Log("roc sExec [" + sExec + "]");
        } else { // linux
            sExec = "perl " + Session.sIndraHome + "/rocch/rocch -compress " + sSpcSepFQFileList;
            api.Log.Log("roc sExec [" + sExec + "]");
        }
        com.iw.calloutmgr.CalloutMgr callmgr = new com.iw.calloutmgr.CalloutMgr(
                sExec, true, com.iw.calloutmgr.CalloutMgr.IOUTPUTCAPTUREMODE_All, "Locally best");
        callmgr.execItSynchronous();
        if (UtilFile.bFileExists("/temp/IndraDebug_showROCCallout.txt")) {
            if (callmgr.getVecOutput() != null) {
                api.Log.Log("ROC callout returns [" + UtilSets.vToStrDelim(callmgr.getVecOutput(), "\r\n") + "]");
            } else {
                api.Log.Log("ROC callout returns null vec");
            }
        }
        return callmgr.getVecOutput();
    }
}
