package api.tsclassify;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Add a new classifier to the ITS system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  Name    Classifier readable name.
 *  @param  Path    Path to classifier java class definition.
 *  @param  Colname (optional)  Force a classifier to use a specific database column, otherwise the system will select one for you.
 *  @param  Sort (optional) Classifier score's sort order, either ascending (1) or descending (0).  Default is descending.
 *
 *  @note   http://ITSERVER/servlet/ts?fn=tsclassify.TSCreateClassifierSKEY=801312271&name=test&path=system.score&sort=1

 *	@return SUCCESS object indicating the database column selected for use.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>SCORE8</SUCCESS>
    </TSRESULT>
 \endverbatim
 */
public class TSCreateClassifier
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sName = (String) props.get ("name", true);
        String sPath = (String) props.get ("path", true);
        String sSort = (String) props.get ("sort", "0");

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String colName = GetNextAvailableColumn(dbc);
            if (colName.equals("NONE")) {
                api.Log.LogError("Error: Attempt to add a new classifier, no columns are available.");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
            }

			String sSQL = " insert into Classifier (ClassifierName, ColName, ClassPath, SortOrder, Active) "+
                          " values ('"+sName+"', '"+colName+"', '"+sPath+"', "+sSort+", 1)";
			Statement stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Create classifier failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>"+colName+"</SUCCESS>");
		    stmt.close();
		}

		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}

    // return column name, or "NONE" if none are available
	public static String GetNextAvailableColumn(Connection dbc)
		throws Exception
	{
        Hashtable htColumns = new Hashtable();
		String sSQL = "select upper(colname) from classifier";
        Statement stmt = null; ResultSet rs = null;
		try {
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			while ( rs.next() ) {
				String sName = rs.getString(1);
                htColumns.put(sName, "1");
            }
        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}

        sSQL = "select column_name from all_tab_columns where table_name = 'NODEDOCUMENT' and column_name like 'SCORE%' ";

        try {
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			while ( rs.next() ) {
				String sName = rs.getString(1);
                if (!htColumns.containsKey(sName)) { return sName; }
            }

            return "NONE";
        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
