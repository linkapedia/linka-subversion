/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 31, 2003
 * Time: 10:40:58 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsclassify.ROCHelper;

import com.indraweb.database.JDBCIndra_Connection;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

public class DataCache_RocBucketLocalClassifier {

    public int iROCSETTINGID = -1;
    public int iSTARCOUNT = -1;
    public float fCOSTRATIOFPTOFN = -1;
    public double dCUTOFFSCOREGTE = -1;
    public int iCLASSIFIERID = -1;
    public double dSCORE1VAL = -1;
    public float fTRUEPOS = -1;
    public float fFALSEPOS = -1;
    public String sCOLNAME = null;
    public String sClassifierName = null;
    public float fRATIOTNTOTP = -1;

    public DataCache_RocBucketLocalClassifier(int iROCSETTINGID_,
            int iSTARCOUNT_,
            float fCOSTRATIOFPTOFN_,
            double dCUTOFFSCOREGTE_,
            int iCLASSIFIERID_,
            String sClassifierName_,
            float fRATIOTNTOTP_,
            double dSCORE1VAL_,
            float fTRUEPOS_,
            float fFALSEPOS_,
            String sCOLNAME_) {
        iROCSETTINGID = iROCSETTINGID_;
        iSTARCOUNT = iSTARCOUNT_;
        fCOSTRATIOFPTOFN = fCOSTRATIOFPTOFN_;
        dCUTOFFSCOREGTE = dCUTOFFSCOREGTE_;
        iCLASSIFIERID = iCLASSIFIERID_;
        sClassifierName = sClassifierName_;
        dSCORE1VAL = dSCORE1VAL_;
        fTRUEPOS = fTRUEPOS_;
        fFALSEPOS = fFALSEPOS_;
        sCOLNAME = sCOLNAME_;


        fRATIOTNTOTP = fRATIOTNTOTP_;

    }

    public void setClassifierCutoffAndTPFP(int iClassifierID_, double dCutoffScoreGTE_, float fTP_, float fFP_) {
        iCLASSIFIERID = iClassifierID_;
        fTRUEPOS = fTP_;
        fFALSEPOS = fFP_;
        dCUTOFFSCOREGTE = dCutoffScoreGTE_;
    }

    public void updateMeToDB(Connection dbc)
            throws Exception {
        String sSQL = "update ROCBUCKETLOCALCLASSIFIER set "
                + " CLASSIFIERID = " + iCLASSIFIERID
                + ", TRUEPOS = " + fTRUEPOS
                + ", FALSEPOS = " + fFALSEPOS
                + ", CUTOFFSCOREGTE = " + dCUTOFFSCOREGTE
                + " where ROCSETTINGID = " + iROCSETTINGID + " and STARCOUNT = " + iSTARCOUNT;

        api.Log.Log("sSQL [" + sSQL + "]");

        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQL, dbc, true, 1);

    }

    public static void invalidateCache() {
        bCacheStale = true;
    }
    private static boolean bCacheStale = true;
    private static Hashtable htStrCorpusIDToIntegerROCSettingID = null;
    private static Hashtable htIntegerROCSettingID_toVDataROCBucketLocalClassifier = null;
    private static Hashtable htStrClassifierNamesToColNames;
    private static Hashtable htStrColNamesToStrClassifierNames;
    private static Hashtable htStrCorpusID_toVDataROCBucketLocalClassifier = null;
    private static final Object oSemaphore = new Object();

    public static Vector getVecDataROCBucketLocalClassifier(String sCorpusID, Connection dbc) throws Exception {
        if (bCacheStale) {
            fillCache(dbc);
        }
        return (Vector) htStrCorpusID_toVDataROCBucketLocalClassifier.get(sCorpusID);
    }

    private static void fillCache(Connection dbc) throws Exception {
        // if stale in the first place (may be late getting here) or otherwise stupidly calling
        if (bCacheStale) // prevent two threads second repeating work in race
        {
            synchronized (oSemaphore) {

                if (bCacheStale) // prevent two threads second repeating work in race
                {
                    // filled by this function:
                    htStrCorpusIDToIntegerROCSettingID = new Hashtable();
                    htStrClassifierNamesToColNames = new Hashtable();
                    htIntegerROCSettingID_toVDataROCBucketLocalClassifier = new Hashtable();
                    htStrCorpusID_toVDataROCBucketLocalClassifier = new Hashtable();
                    htStrColNamesToStrClassifierNames = new Hashtable();

                    {  // scope for  htIntegerCorpusIDsToROCSettingIDs refill
                        // save off map of corpusids to rocsettingids
                        String sSQL1 = "sELECT corpusid, ROCSETTINGID from corpus ";

                        Statement stmt = dbc.createStatement();
                        ResultSet rs = stmt.executeQuery(sSQL1);

                        // Need to get total number of results for calculating percentages
                        while (rs.next()) {
                            Integer ICORPUSID = new Integer(rs.getInt(1));
                            Integer IROCSETTINGID = new Integer(rs.getInt(2));
                            htStrCorpusIDToIntegerROCSettingID.put("" + ICORPUSID, IROCSETTINGID);
                        }
                        if (stmt != null) {
                            stmt.close();
                            stmt = null;
                        }
                        if (rs != null) {
                            rs.close();
                            rs = null;
                        }
                    }// scope close



                    // step 2 : map rocsettings to the set of vectors representing them
                    //; for each rocsettingid
                    { // scope
                        // now get the vector of data per rocsettingID
                        String sSQL = "sELECT distinct(ROCSETTINGID) from rocbucketlocalclassifier";
                        Vector vIntegerROCSettingIDs = JDBCIndra_Connection.executeQueryAgainstDBVector(sSQL, dbc);
                        for (int i = 0; i < vIntegerROCSettingIDs.size(); i++) {
                            BigDecimal bd = (BigDecimal) vIntegerROCSettingIDs.elementAt(i);
                            int iROCSettingID = bd.intValue();
                            Vector vDataROCBucketLocalClassifierThisROCSetting = new Vector();
                            // first get ROCSetting for this corpus
                            sSQL = "sELECT RATIOTNTOTP from ROCSETTING where ROCSettingID = " + iROCSettingID;
                            float fRATIOTNTOTP = JDBCIndra_Connection.executeQueryAgainstDBFloat(sSQL, dbc);
                            // then loop thru and fill in the cutoffs and classifier ID's

                            sSQL = "SELECT STARCOUNT, COSTRATIOFPTOFN, CUTOFFSCOREGTE, "
                                    + " c.CLASSIFIERID, SCORE1VAL, TRUEPOS, FALSEPOS, COLNAME, CLASSIFIERNAME "
                                    + " FROM ROCBucketLocalClassifier r, Classifier c"
                                    + " WHERE ROCSETTINGID = " + iROCSettingID
                                    + " and r.classifierID =  c.classifierID  "
                                    + " order by STARCOUNT ASC";
                            //api.Log.Log("sSQL  [" + sSQL + "]" );

                            Statement stmt = dbc.createStatement();
                            ResultSet rs = stmt.executeQuery(sSQL);

                            // Need to get total number of results for calculating percentages
                            int iRowNum = 0;
                            while (rs.next()) {
                                iRowNum++;
                                int iSTARCOUNT = rs.getInt(1);
                                float fCOSTRATIOFPTOFN = rs.getFloat(2);
                                float fCUTOFFSCOREGTE = rs.getFloat(3);
                                int iCLASSIFIERID = rs.getInt(4);
                                float fSCORE1VAL = rs.getInt(5);
                                float fTRUEPOS = rs.getInt(6);
                                float fFALSEPOS = rs.getInt(7);
                                String sCOLNAME = rs.getString(8);
                                String sCLASSIFIERNAME = rs.getString(9);

                                vDataROCBucketLocalClassifierThisROCSetting.addElement(new DataCache_RocBucketLocalClassifier(
                                        iROCSettingID, iSTARCOUNT, fCOSTRATIOFPTOFN,
                                        fCUTOFFSCOREGTE, iCLASSIFIERID, sCLASSIFIERNAME, fRATIOTNTOTP, fSCORE1VAL, fTRUEPOS, fFALSEPOS, sCOLNAME));
                                htStrClassifierNamesToColNames.put(sCLASSIFIERNAME, sCOLNAME);
                                htStrColNamesToStrClassifierNames.put(sCOLNAME, sCLASSIFIERNAME);
                            }

                            if (stmt != null) {
                                stmt.close();
                                stmt = null;
                            }
                            if (rs != null) {
                                rs.close();
                                rs = null;
                            }
                            htIntegerROCSettingID_toVDataROCBucketLocalClassifier.put(new Integer(iROCSettingID), vDataROCBucketLocalClassifierThisROCSetting);
                        }// for all ROCSettingIDs
                    } // scope close



                    // step 3 : now expand out to vector set per corpus so no indirect lookup at run time as to which corpus
                    // uses which rocsettingID
                    {  // scope
                        Enumeration eStrCorpusIDs = htStrCorpusIDToIntegerROCSettingID.keys();

                        // for all corpusids, find out the associated rocsettingid
                        // and add to the corpus to vector lookup hashtable
                        while (eStrCorpusIDs.hasMoreElements()) {
                            String sCorpusID = (String) eStrCorpusIDs.nextElement();
                            Integer IROCSsettingID = (Integer) htStrCorpusIDToIntegerROCSettingID.get(sCorpusID);
                            Vector vDataROCBucketLocalClassifierThisROCSetting =
                                    (Vector) htIntegerROCSettingID_toVDataROCBucketLocalClassifier.get(IROCSsettingID);
                            htStrCorpusID_toVDataROCBucketLocalClassifier.put(sCorpusID, vDataROCBucketLocalClassifierThisROCSetting);
                        }
                    }

                    api.Log.Log("completed ROC cache refresh");
                    bCacheStale = false;
                }
            } // synchronize cache
        } // if stale in the first place (may be late getting here) or otherwise stupidly calling
    }

    public static Vector getSetOfColNamesThisROCSet(Vector vDataROCBucketLocalClassifier, Connection dbc) throws Exception {
        if (bCacheStale) {
            fillCache(dbc);
        }
        HashSet hsColsSeen = new HashSet();
        Vector vColNames = new Vector();
        for (int i = 0; i < vDataROCBucketLocalClassifier.size(); i++) {
            DataCache_RocBucketLocalClassifier dataRoc = (DataCache_RocBucketLocalClassifier) vDataROCBucketLocalClassifier.elementAt(i);

            if (!hsColsSeen.contains(dataRoc.sCOLNAME)) {
                hsColsSeen.add(dataRoc.sCOLNAME);
                vColNames.addElement(dataRoc.sCOLNAME);
            }
        }
        return vColNames;
    }

    public static Hashtable gethtStrClassifierNamesToColNames(Connection dbc) throws Exception {
        if (bCacheStale) {
            fillCache(dbc);
        }
        return htStrClassifierNamesToColNames;
    }

    public static Hashtable gethtStrColNamesToClassifierNames(Connection dbc) throws Exception {
        if (bCacheStale) {
            fillCache(dbc);
        }
        return htStrColNamesToStrClassifierNames;
    }

    @Override
    public String toString() {
        return "iROCSETTINGID 	" + iROCSETTINGID + "	"
                + "iSTARCOUNT 	" + iSTARCOUNT + "	"
                + "fCOSTRATIOFPTOFN 	" + fCOSTRATIOFPTOFN + "	"
                + "dCUTOFFSCOREGTE 	" + dCUTOFFSCOREGTE + "	"
                + "iCLASSIFIERID 	" + iCLASSIFIERID + "	"
                + "dSCORE1VAL 	" + dSCORE1VAL + "	"
                + "fTRUEPOS 	" + fTRUEPOS + "	"
                + "fFALSEPOS 	" + fFALSEPOS + "	"
                + "sCOLNAME 	" + sCOLNAME + "	"
                + "sClassifierName 	" + sClassifierName + "	";

    }
}
