package api.tsclassify;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.execution.Session;
import com.indraweb.ir.clsStemAndStopList;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;
import com.iw.system.HashTree;
import com.iw.system.InvokeAPI;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dom4j.Element;

/**
 * TSNodeRefreshApply processes nodeID(s), searching existing documents that contain signature and title terms to score against a node. May typically be used after a node signature has changed or when
 * documents are newly available for scoring.
 *
 * By default, all nodeIDs in the NODEREFRESH table will be processed node by node, searching for documents and scoring them against one node at a time. TSClassify is invoked with a single document
 * and single node.
 *
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param NodeID (optional) Node identifier of the nodeID to be refreshed. Ignores and does not touch content in the NODEREFRESH table in this case.
 * @param MaxNumNodes (optional) Sets the maximum number of nodes that will be processed. (Ignored if NodeID passed in.)
 * @param MaxDocsPerNode (optional) Sets the maximum number of documents that will be classified per node. Ignored if NodeID passed in.
 * @param RepositoryID (optional) Restricts documents searched to those in a given repository.
 * @param PropAffinityNodeTitlesOnly (optional) Specifies if (1) title terms alone or (2) all signature terms should be used in fulltext searching for docs to classify. Defaults to false
 * @param DeleteProcessedNodes (optional) Provides an option to post classify results but not remove records from NodeRefresh table. May typically be set to false in a test context. Default is true.
 * @param Post (optional) Defaults to true. If false then classifies will be unposted and records will not be removed from the NODEREFRESH table.
 * @param DocumentID (optional) For test purposes, a single document can be specified to override the text search result set.
 *
 * @note http://ITSSERVER/itsapi/ts?fn=tsnode.TSNodeRefreshApply&SKEY=9919294812
 * @return Statistics and failure indicators of the run are returned in XML. \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <NUMNODESDONE>10</NUMNODESDONE>
 * <NUMDOCSTOTAL>200</NUMDOCSTOTAL> <NUMDOCSUNIQUE>191</NUMDOCSUNIQUE> <NUMCLASSIFIES>2000</NUMCLASSIFIES> <NUMNDINSERTORUPDATES>23</NUMNDINSERTORUPDATES>
 * <NUMBASICNDSCOREFAILS>0</NUMBASICNDSCOREFAILS> <CLASSLOAD>Tue Feb 03 04:32:46 EST 2004</CLASSLOAD> <CALLCOUNT>0</CALLCOUNT> <TIMEOFCALL_MS>71513</TIMEOFCALL_MS> </TSRESULT>\endverbatim
 */
public class TSNodeRefreshApply {

    // ***********************************************************
    public static void handleTSapiRequest(HttpServletRequest req,
            HttpServletResponse res,
            api.APIProps props,
            PrintWriter out,
            Connection dbc)
            // ***********************************************************
            throws Throwable {

        // get nfs cache up to speed
        int iCountFailsMeansMissingTagBasicFail = 0;
        int iCountSuccessMeaningNDInsertTrue = 0;
        int iCountNonFailNonInsert = 0;
        int iCountTota_1N1D_Classifies = 0;

        boolean bAffinityNodeTitlesOnly = false;
        //String sPropAffinityNodeTitlesOnly = (String) props.get ("PropAffinityNodeTitlesOnly");
        // if (sPropAffinityNodeTitlesOnly != null)
        bAffinityNodeTitlesOnly = props.getbool("PropAffinityNodeTitlesOnly", "false");

        boolean bDeleteProcessedNodes = props.getbool("DeleteProcessedNodes", "true");
        //Data_WordsToNodes.verifyCacheUpToDate (dbc , bAffinityNodeTitlesOnly);
        HashSet hsUniqueDocIDsAcrossNodes = new HashSet();

        int iProp_NodeID = props.getint("NODEID", "-1");
        int iProp_DocumentID = props.getint("DOCUMENTID", "-1");
        int iProp_MaxNumNodes = props.getint("MAXNUMNODES", "-1");
        int iProp_MaxDocsPerNode = props.getint("MAXDOCSPERNODE", "-1");
        //int iProp_MinNodeDocsPerNode = Integer.parseInt ((String) props.get ("MinNodeDocsPerNode" , "-1"));
        int iProp_RepositoryID = props.getint("REPOSITORYID", "-1");

        boolean bProp_post = props.getbool("post", "true");
        String sKey = (String) props.get("SKEY", true);
        // HBK CONTROL - SET ON IN PRODUCTION
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        //Ensure user is a member of the admin group
        if (!u.IsMember(out)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        /*
         * // debug if (false && !u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); } System.out.println("security override in TSNodeRefreshApply");
         * System.out.println("security override in TSNodeRefreshApply"); System.out.println("security override in TSNodeRefreshApply"); System.out.println("security override in TSNodeRefreshApply");
         * System.out.println("security override in TSNodeRefreshApply");
         */
        // GET NODES
        Vector vNodeIDsToBeProcessed = null;
        if (iProp_NodeID > 0) {
            Integer INodeID = new Integer(iProp_NodeID);
            vNodeIDsToBeProcessed = new Vector();
            vNodeIDsToBeProcessed.addElement(INodeID);
        } else {
            String sSQLGetNodes = "select nodeid from noderefresh";
            vNodeIDsToBeProcessed = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLGetNodes, dbc, iProp_MaxNumNodes);

        }

        int iTotalDocsCounter = 0;
        //HashSet hsUniquDocIDStrings = new HashSet();
        long lStartThisNode = System.currentTimeMillis();
        for (int iNodeIndex = 0; iNodeIndex < vNodeIDsToBeProcessed.size()
                && (iProp_MaxNumNodes < 0 || iNodeIndex < iProp_MaxNumNodes); iNodeIndex++) {
            String sNodeID = vNodeIDsToBeProcessed.elementAt(iNodeIndex).toString();
            lStartThisNode = System.currentTimeMillis();
            String sConstrainRepos = "";

            com.iw.scoring.NodeForScore nfs =
                    Data_NodeIDsTo_NodeForScore.getNodeForScore(Integer.parseInt(sNodeID), true);


            String sSQLConstrainFTContains = null;
            String[] sArrFinal = null;
            try {
                if (bAffinityNodeTitlesOnly) // title only
                {
                    String[] sArrTitleTerms = nfs.getsArrNodeTitleWords_lowerStemOption(dbc, Session.cfg.getPropBool("StemOnClassifyNodeDocScoring"));
                    sArrFinal = sArrTitleTerms;
                } else {
                    sArrFinal = nfs.getSarr(Session.cfg.getPropBool("StemOnClassifyNodeDocScoring"), true, true);
                }
                sSQLConstrainFTContains = buildSQL_FTContainsClause(dbc, sArrFinal);
            } catch (Throwable t) {
                api.Log.LogError("failure in getsArrNodeTitleWords_lowerStemOption", t);
                throw new Exception("failure in getsArrNodeTitleWords_lowerStemOption see log output for stacktrace ");
            }

            if (iProp_RepositoryID > -1) {
                sConstrainRepos = " repositoryid = " + iProp_RepositoryID + " and  ";
            }

            String sConstrainRowCount = "";
            if (iProp_MaxDocsPerNode > -1) {
                sConstrainRowCount = " rownum < " + (iProp_MaxDocsPerNode + 1) + " and ";
            }

            String sSQL = "select documentid from document where "
                    + sConstrainRepos + sConstrainRowCount;
            sSQL = sSQL + sSQLConstrainFTContains + " order by score(1)  DESC";

            api.Log.Log(" submitted sSQL call for node refresh nodeid [" + vNodeIDsToBeProcessed.elementAt(iNodeIndex) + "] [\t" + sSQL + "\t] ");

            long lStartDocFetchThisNode = System.currentTimeMillis();
            Vector vDocIDsWithWords = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBVector(sSQL, dbc, iProp_MaxDocsPerNode);

            if (iProp_DocumentID > -1) // debug specuify the docid
            {
                api.Log.Log("node refresh overriding doc set with id [" + iProp_DocumentID + "]");
                api.Log.Log("node refresh overriding doc set with id [" + iProp_DocumentID + "]");
                api.Log.Log("node refresh overriding doc set with id [" + iProp_DocumentID + "]");
                api.Log.Log("node refresh overriding doc set with id [" + iProp_DocumentID + "]");
                vDocIDsWithWords = new Vector();
                vDocIDsWithWords.addElement(new Integer(iProp_DocumentID));


            }

            // api.Log.Log (" vDocIDsWithWords.size() [\t" + vDocIDsWithWords.size() + "\t] ");
            api.Log.Log(" # docids to score for node [" + vNodeIDsToBeProcessed.elementAt(iNodeIndex)
                    + "] is [" + vDocIDsWithWords.size() + "] ");
            long lTimeInDocFetch = System.currentTimeMillis() - lStartDocFetchThisNode;
            iTotalDocsCounter += vDocIDsWithWords.size();
            //sbNodeSet.append(sConnect + sNodeID);
            //sConnect = ",";
            //api.Log.Log ("sbNodeSet [" + sbNodeSet.toString() + "]");





            for (int iDocIndex = 0; iDocIndex < vDocIDsWithWords.size()
                    && (iProp_MaxDocsPerNode < 0 || iDocIndex < iProp_MaxDocsPerNode); iDocIndex++) {

                //api.Log.Log ("Start refresh iDocIndex [" + iDocIndex );
                iCountTota_1N1D_Classifies++;
                // HAVE DOCS NODES
                long lStartLoop = System.currentTimeMillis();
                String sDocID = vDocIDsWithWords.elementAt(iDocIndex).toString();
                //hsUniqueDocIDsAcrossNodes.add (sDocID);

                //HashTree htArguments = new HashTree (props.getPropsCommonType () , true);
                HashTree htArguments = new HashTree();
                //htArguments.put ("NodeSet" , "" + sbNodeSet.toString());
                if (props.get("post") != null) {
                    htArguments.put("post", props.get("post"));
                }
                htArguments.put("skey", props.get("skey"));

                htArguments.put("NodeSet", "" + sNodeID);

                htArguments.put("DocIDSource", "" + sDocID);

                InvokeAPI invokeapi = new InvokeAPI("tsclassify.TSClassifyDoc", htArguments);
                if (invokeapi.GetTaxonomyServer() == null) {
                    invokeapi.SetTaxonomyServer((String) Session.cfg.getProp("API"));

                }
                org.dom4j.Document dom4jdocTsClassisfy = invokeapi.dExecute();

                Element elemRoot = dom4jdocTsClassisfy.getRootElement();
                //logTagsThisElementContains(elemRoot, "elemRoot");
                Element elem_CLASSIFYSTATS = elemRoot.element("CLASSIFYSTATS");
                // sysoutTagsThisElementContains(elem_CLASSIFICATIONRESULTSET, "elem_CLASSIFICATIONRESULTSET");
                if (elem_CLASSIFYSTATS == null) {
                    iCountFailsMeansMissingTagBasicFail++;
                    api.Log.Log(
                            "FAIL elem_CLASSIFYSTATS == null "
                            + " Done Doc # [" + (iDocIndex + 1) + "] of [" + vDocIDsWithWords.size()
                            + "] iSuccess [" + iCountSuccessMeaningNDInsertTrue
                            + "] For sDocID [" + sDocID
                            + "] Doc ID Failed iFails [" + iCountFailsMeansMissingTagBasicFail
                            + "]");
                    continue;
                }
                //logTagsThisElementContains(elem_CLASSIFYSTATS, "elem_CLASSIFYSTATS");
                String sClassifyResultText = "";
                String sSuccess = "";

                Element elem_CLASSIFYRESULT = elem_CLASSIFYSTATS.element("CLASSIFYRESULT");
                if (elem_CLASSIFYRESULT == null) {
                    iCountFailsMeansMissingTagBasicFail++;
                    api.Log.Log(
                            "FAIL elem_CLASSIFYRESULT == null "
                            + " Done Doc # [" + (iDocIndex + 1) + "] of [" + vDocIDsWithWords.size()
                            + "] iSuccess [" + iCountSuccessMeaningNDInsertTrue
                            + "] For sDocID [" + sDocID
                            + "] Doc ID Failed iFails [" + iCountFailsMeansMissingTagBasicFail
                            + "]");
                    continue;
                } else {

                    sClassifyResultText = elem_CLASSIFYRESULT.getText();
                    String sInsertNum = com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sClassifyResultText, "NDI ", "NDU");
                    int iNumInserts = Integer.parseInt(sInsertNum.trim());
                    if (iNumInserts > 0) {
                        sSuccess = "SUCCESS INSERT ";
                        iCountSuccessMeaningNDInsertTrue++;

                    } else {
                        iCountNonFailNonInsert++;
                    }
                    //api.Log.Log ("Doc ID Succeed iFails [" + iFailsMeansMissingTagBasicFail + "] iSuccess [" + iSuccessMeaningNDInsertTrue + "] For NodeRefressh [" + sDocID+ "] # [" + (iDocIndex+1)+ "] of [" + vDocIDsWithWords.size() + "]");


                    /*
                     * api.Log.Log ( sSuccess + "Done Doc # [" + (iDocIndex+1)+ "] of [" + vDocIDsWithWords.size() + "] in node index [" + (iNodeIndex) + "] of [" +vNodeIDsToBeProcessed.size () + "]
                     * sClassifyResultText [" + sClassifyResultText + "] For docid [" + sDocID + "] For nodeid [" + sNodeID + "] " + "] iCountTota_1N1D_Classifies [" + iCountTota_1N1D_Classifies+ "]"
                     * );
                     */


                    //api.Log.Log ("time this doc [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis (lStartLoop) + "]");
                }
            } // for per doc
            // print set of docs api.Log.Log ("vDocIDsWithWords [" + com.indraweb.util.UtilSets.vToStr(vDocIDsWithWords) + "]");
            //api.Log.Log ("hsUniqueDocIDsAcrossNodes.size() [" + hsUniqueDocIDsAcrossNodes.size() + "] start node refresh nodeid [" + sNodeID + " # [" + (iNodeIndex+1)  + "] of [" +vNodeIDsToBeProcessed.size ()  + "] #docs [" + vDocIDsWithWords.size () + "] hsUniqueDocIDsAcrossNodes.size() [" + hsUniqueDocIDsAcrossNodes.size() + "]");
            hsUniqueDocIDsAcrossNodes.addAll(vDocIDsWithWords);

            api.Log.Log("done refresh nodeid [" + sNodeID
                    + "] node index [" + (iNodeIndex) + "] of [" + vNodeIDsToBeProcessed.size()
                    + "] Upserts [" + iCountSuccessMeaningNDInsertTrue
                    + "] #docs this node [" + vDocIDsWithWords.size()
                    + "] doc query time [" + lTimeInDocFetch
                    + "] node time total [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStartThisNode)
                    + "] sArrFinal.length [" + sArrFinal.length
                    + "] hsUniqueDocIDsAcrossNodes.size() [" + hsUniqueDocIDsAcrossNodes.size() + "]");

        } // for per node
        if (bProp_post && bDeleteProcessedNodes) {
            // delete records from noderefresh pending
            Enumeration enumNodeIDsToBeProcessed = vNodeIDsToBeProcessed.elements();
            while (enumNodeIDsToBeProcessed.hasMoreElements()) {
                Object ONodeID = enumNodeIDsToBeProcessed.nextElement();
                String sSQLDelete = "delete from noderefresh where nodeid = " + ONodeID.toString();
                com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLDelete, dbc, true, -1);
            }
        }
        out.println("<NUMNODESDONE>" + vNodeIDsToBeProcessed.size() + "</NUMNODESDONE>");
        out.println("<NUMDOCSTOTAL>" + iTotalDocsCounter + "</NUMDOCSTOTAL>");
        out.println("<NUMDOCSUNIQUE>" + hsUniqueDocIDsAcrossNodes.size() + "</NUMDOCSUNIQUE>");
        out.println("<NUMCLASSIFIES>" + iCountTota_1N1D_Classifies + "</NUMCLASSIFIES>");
        out.println("<NUMNDINSERTORUPDATES>" + iCountSuccessMeaningNDInsertTrue + "</NUMNDINSERTORUPDATES>");
        out.println("<NUMBASICNDSCOREFAILS>" + iCountFailsMeansMissingTagBasicFail + "</NUMBASICNDSCOREFAILS>");
        api.Log.Log("TSsNodeRefreshApply completed #docs total [" + iTotalDocsCounter
                + "] vs unique docs [" + hsUniqueDocIDsAcrossNodes.size()
                + "] # nodes [" + vNodeIDsToBeProcessed.size()
                + "] iSuccessInsert [" + iCountSuccessMeaningNDInsertTrue
                + "] bAffinityNodeTitlesOnly [" + bAffinityNodeTitlesOnly
                + "] count NonFail NonInsert [" + iCountNonFailNonInsert
                + "] iCountTota_1N1D_Classifies [" + iCountTota_1N1D_Classifies + "]"
                + "] failed classifies [" + iCountFailsMeansMissingTagBasicFail + "]");

    } // public static void handleTSapiRequest ( HttpServletRequest req, api.APIProps props, PrintWriter out, Connection dbc )

    private static String buildSQL_FTContainsClause(Connection dbc, String[] sArrTerms) throws Exception {
        StringBuilder sbConstraint = new StringBuilder("(");
        String sConnector = "";
        sbConstraint.append(" contains (DOCUMENT.FULLTEXT, '");
        for (int i = 0; i < sArrTerms.length; i++) {
            String sTermLcaseStem = sArrTerms[i].toLowerCase();
            if (clsStemAndStopList.getHTStopWordList().get(sTermLcaseStem) == null
                    && sTermLcaseStem.indexOf(")") < 0 && sTermLcaseStem.indexOf("(") < 0
                    && !sTermLcaseStem.endsWith("-")) {
                sbConstraint.append(sConnector).append(sTermLcaseStem);
                sConnector = " or ";
                //sConnector = " and ";
                //sConnector = " , ";  // 'soccer, Brazil*3, cup'
            } // if not a stop word
        }
        sbConstraint.append("', 1) > 0 )");
        return sbConstraint.toString();
    }

    public static void logTagsThisElementContains(Element e, String sPArent) {
        String seName = e.getName();
        Iterator iterElementsRoot = e.elementIterator();
        while (iterElementsRoot.hasNext()) {
            Element elemNext = (Element) iterElementsRoot.next();
            String elemNextName = elemNext.getName();
            String elemNextText = elemNext.getTextTrim();
            api.Log.Log("[" + sPArent + "] seName [" + seName + "]. elemNextName[" + elemNextName + "] elemNextText [" + elemNextText + "]");
        }

    }
}
