package api.tsclassify;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.security.*;
import api.TSException;
import api.emitxml.*;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Review all documents which are marked as edited but have since been re-classified.  This function
 * will allow editors to adjust changes that may have been made to the document since the last editorial cycle.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note   http://ITSSERVER/itsapi/ts?fn=tsclassify.TSReviewBatchResults&SKEY=-132981656

 *	@return A series of NODEDOCUMENT objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODEDOCUMENTS>"); }
        <NODEDOCUMENT>");
            <NODEID>482198</NODEID>");
            <DOCUMENTID>94929</DOCUMENTID>");
            <NODETITLE><The Middle Ages</NODETITLE>");
            <DOCTITLE>Crusades and their effect on Europe</DOCTITLE>");
            <DOCURL>http://29.111.48.10/~middleages/kdoffi/axi.html</DOCURL>");
            <GENREID>16</GENREID>");
            <SCORE>100</SCORE>");
            <DOCSUMMARY><![CDATA[The crusades had a profound effect on ...]]></DOCSUMMARY>");
        </NODEDOCUMENT>");
...
 \endverbatim
 */
public class TSReviewBatchResults {
	// Description:

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

        Statement stmt = null; ResultSet rs = null;
        String sSQL = "select ND.NodeID, ND.DocumentID, N.NodeTitle, D.DocTitle, D.DocURL, "+
                      " D.GenreID, ND.Score1, ND.DocSummary "+
                      " from NodeDocument ND, Node N, Document D where "+
                      " ND.NodeID = N.NodeID and D.DocumentID = ND.DocumentID and "+
                      " ND.Edited = 1 and D.UpdatedDate < D.DateLastFound ORDER BY ND.DocumentID asc ";
        try {
            String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user exists
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            int loop = 0;
            while ( rs.next() ) {
                loop++;

                int iNodeID = rs.getInt(1);
                int iDocumentID = rs.getInt(2);
                String sNodeTitle = rs.getString(3);
                String sDocTitle = rs.getString(4);
                String sDocURL = rs.getString(5);
                String sGenreID = rs.getString(6);
                String sScore = rs.getString(7);
                String sDocSum = rs.getString(8);

                if (loop == 1) { out.println("<NODEDOCUMENTS>"); }
                out.println("   <NODEDOCUMENT>");
                out.println("      <NODEID>"+iNodeID+"</NODEID>");
                out.println("      <DOCUMENTID>"+iDocumentID+"</DOCUMENTID>");
                out.println("      <NODETITLE><![CDATA["+sNodeTitle+"]]></NODETITLE>");
                out.println("      <DOCTITLE><![CDATA["+sDocTitle+"]]></DOCTITLE>");
                out.println("      <DOCURL><![CDATA["+sDocURL+"]]></DOCURL>");
                out.println("      <GENREID>"+sGenreID+"</GENREID>");
                out.println("      <SCORE>"+sScore+"</SCORE>");
                out.println("      <DOCSUMMARY><![CDATA["+Clean(sDocSum)+"]]></DOCSUMMARY>");
                out.println("   </NODEDOCUMENT>");
            }
            if (loop != 0) { out.println("</NODEDOCUMENTS>"); }

        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { try { rs.close(); } catch (Exception e) {} rs = null; }
            if (stmt != null) { try { stmt.close(); } catch (Exception e) {} stmt = null; }
        }
    }

    public static String Clean (String dirtyString) {
        if (dirtyString == null) { return "None available"; }
        String sReturn = "";
        for (int k = 0; k < dirtyString.length(); k++) {
            int ichar = (int) dirtyString.charAt(k);
            if ((ichar != 13) && (ichar != 10)) { sReturn = sReturn + dirtyString.charAt(k); }
        }

        return sReturn;
    }
}