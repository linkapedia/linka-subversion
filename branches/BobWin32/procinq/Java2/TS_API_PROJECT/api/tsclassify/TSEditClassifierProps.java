package api.tsclassify;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.iw.system.User;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * Change the properties of a classifier within the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CID Unique classifier identifier of the classifier to be changed.
 *  @param  Name (optional) Classifier readable name.
 *  @param  Colname (optional)  Force a classifier to use a specific database column.
 *  @param  Path (optional) Path to classifier java class definition.
 *  @param  Sort (optional) Classifier score's sort order, either ascending (1) or descending (0)
 *  @param  Active (optional)   Set classifier to active (1) or inactive (0).
 *
 *  @note   http://ITSERVER/servlet/ts?fn=tsclassify.TSEditClassifierProps&SKEY=801312271&CID=4&Active=1

 *	@return SUCCESS object if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>4</SUCCESS>
    </TSRESULT>
 \endverbatim
 */
public class TSEditClassifierProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        String sCID = (String) props.get ("CID", true);
		String sName = (String) props.get ("name");
        String sColName = (String) props.get ("colname");
        String sPath = (String) props.get ("path");
        String sSort = (String) props.get ("sort");
        String sActive = (String) props.get ("active");

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " update Classifier set classifierID = "+sCID;

            if (sName != null) { sSQL = sSQL + ", classifierName = '"+sName+"'"; }
            if (sColName != null) { sSQL = sSQL + ", colName = '"+sColName+"'"; }
            if (sPath != null) { sSQL = sSQL + ", classPath = '"+sPath+"'"; }
            if (sSort != null) { sSQL = sSQL + ", sortOrder = "+sSort; }
            if (sActive != null) { sSQL = sSQL + ", active = "+sActive; }

            sSQL = sSQL + " where classifierID = "+sCID;

			Statement stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Modificiation of classifierID "+sCID+" failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>"+sCID+"</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
