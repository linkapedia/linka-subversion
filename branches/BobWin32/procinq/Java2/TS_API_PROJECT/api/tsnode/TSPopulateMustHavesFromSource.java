/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import api.util.bean.PackNode;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSPopulateMustHavesFromSource {

    private static final Logger log = Logger.getLogger(TSPopulateMustHavesFromSource.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }

        List<PackNode> listNodes = new ArrayList<PackNode>();
        try {
            listNodes = nu.getTreeNodes(Integer.parseInt(nodeid));
        } catch (Exception e) {
            log.debug("PopulateMustHavesFromSource: error to get listNodes");
            throw e;
        }


        Properties properties = new Properties();
        properties.load(TSPopulateMustHavesFromSource.class.getClassLoader().getResourceAsStream("config-server.properties"));
        List<String> vRegex = new ArrayList<String>();
        int i = 0;
        while (true) {
            String re = properties.getProperty("tsapi.addmusthaves.regex" + i);
            if (re == null) {
                break;
            } else {
                vRegex.add(re);
            }
            i++;
        }
        if (vRegex.isEmpty()) {
            log.debug("There are not data in regex.properties");
            throw new Exception("File regex.properties malformed");
        }

        String node = "";
        String nodetitle = "";
        String source = "";
        //logic of the method: insert musthaves
        int percent = 0;
        int pi = 0;
        int num = listNodes.size();
        for (PackNode pn : listNodes) {
            node = pn.getNodeid();
            nodetitle = pn.getNodetitle();
            source = pn.getNodesource();
            log.debug("Search Synonymous to nodeid: " + node + " NodeTitle: " + nodetitle);
            List<String> listMustHaves = new ArrayList<String>();
            //REGEX
            List<String> params = new ArrayList<String>();
            for (String regex : vRegex) {
                params.add(nodetitle);
                regex = getRegexWithParams(params, regex);
                Pattern p = Pattern.compile(regex,
                        Pattern.MULTILINE
                        | Pattern.CASE_INSENSITIVE
                        | Pattern.UNICODE_CASE);
                Matcher m = p.matcher(source);
                while (m.find()) {
                    String term = m.group(1);
                    String termMustHave = postProccessRegex(term);
                    if ((termMustHave != null) && (!termMustHave.isEmpty())) {
                        listMustHaves.add(termMustHave);
                    }
                }
            }
            if (!listMustHaves.isEmpty()) {
                try {
                    nu.insertMusthaves(Integer.parseInt(node), listMustHaves);
                } catch (Exception e) {
                    log.error("Error to insert musthaves nodeid: " + node + " NodeTitle: " + nodetitle);
                }
            }
            percent = ((pi + 1) * 100) / num;
            out.println("<STATUS>" + percent + "</STATUS>");
            log.debug("finished one proccess: " + percent + "%");
            out.flush();
            pi++;
        }
        out.println("<SUCCESS>successfully.</SUCCESS>");
        log.debug("FINISHED");
    }

    /**
     * replace param? in the regex
     *
     * @param params
     * @param exp
     * @return
     */
    private static String getRegexWithParams(List<String> params,
            String exp) {
        for (int i = 0; i < params.size(); i++) {
            if (exp.contains("param" + i)) {
                exp = exp.replace("param" + i, params.get(i));
            }
        }
        return exp.trim();
    }

    /**
     * eliminate more the two spaces the synonymous not should pass of 5 words I
     * should to validate if is a valid musthave or not
     *
     * @param group
     * @return
     */
    private static String postProccessRegex(String group) {
        String term = "";
        term = group;
        term = term.replaceAll("(\\s{2,})", " ");
        int countWord = StringUtils.countMatches(term, " ");
        if (countWord < 5) {
            return term.trim();
        }
        return null;
    }
}
