package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * This API call has been deprecated, and should no longer be used.
 * @note    This API call has been deprecated.
 */
public class TSRemoveAllNodesSubscriber
{
	// TSRemoveNodeSubscriber (sessionid, userid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // ** DEPRECATED, RETURN
        if (true) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {

			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			if (u.GetNodeHash().size() < 1) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }

			Enumeration eT = u.GetNodeHash().keys();
			while (eT.hasMoreElements()) {
				String sNodeID = (String) eT.nextElement();
				u.RevokeNodeTracked(sNodeID);
			}
			if (!u.SaveUser(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_EDIT_SUBSCRIBER_FAILURE); }
			
		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
