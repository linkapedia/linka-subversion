package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSFindBridgeNodes {

    private static final Logger LOG = Logger.getLogger(TSFindBridgeNodes.class);
    private static final int NUM_NODES_UPDATE = 50;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        LOG.debug("TSFindBridgeNodes");

        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");

        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        boolean b = false;
        try {
            //get all nodes
            int nodeId = Integer.parseInt(nodeid);
            List<Integer> nodes = nu.getBridgeNodes(nodeId);

            //update each node in the tree if is a bridge node
            dbc.setAutoCommit(false);
            int i = 1;
            StringBuilder sb = new StringBuilder();
            boolean firstTime = true;
            for (Integer nId : nodes) {
                //continue building the string
                if (!firstTime) {
                    sb.append(" or ");
                }
                sb.append("n.NODEID=");
                sb.append(nId);
                firstTime = false;

                if (i == NUM_NODES_UPDATE || i == nodes.size()) {
                    //restart logic and update nodes
                    i = 1;
                    saveBridgeProperty(sb.toString(), dbc);
                    firstTime = true;
                    sb.setLength(0);
                } else {
                    i++;
                }
            }
            dbc.commit();
            b = true;
        } catch (Exception e) {
            LOG.error("Error TSFindBridgeNodes ", e);
            dbc.rollback();
        }
        out.println("<RESULT>" + b + "</RESULT>");
        LOG.debug("FINISHED");
    }

    private static boolean saveBridgeProperty(String nodeIds, Connection con) throws Exception {
        LOG.debug("updateChildren(String, Connection, int)");

        String sql = "UPDATE \"SBOOKS\".\"NODE\" n SET n.BRIDGENODE = '1' WHERE "
                + nodeIds;

        Statement smt = null;
        try {
            smt = con.prepareStatement(sql);
            smt.executeUpdate(sql);
        } finally {
            if (smt != null) {
                smt.close();
            }
        }
        return true;
    }
}
