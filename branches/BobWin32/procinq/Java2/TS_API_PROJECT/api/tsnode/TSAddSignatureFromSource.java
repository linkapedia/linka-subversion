package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import api.util.bean.PackNode;
import api.util.bean.SignatureBean;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSAddSignatureFromSource {

    private static final Logger LOG = Logger.getLogger(TSAddSignatureFromSource.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        LOG.debug("TSAddSignatureFromSource");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }

        List<PackNode> listNodes = new ArrayList<PackNode>();
        try {
            listNodes = nu.getTreeNodes(Integer.parseInt(nodeid));
        } catch (Exception e) {
            LOG.debug("PopulateMustHavesFromSource: error to get listNodes");
            throw e;
        }
        String node = "";
        String source = "";

        int percent = 0;
        int pi = 0;
        int num = listNodes.size();
        //by each node extract <LinkedSection></LinkedSection> and put signatures
        for (PackNode pn : listNodes) {
            node = pn.getNodeid();
            source = pn.getNodesource();
            List<SignatureBean> sb = nu.getSignatures(Integer.parseInt(node));
            List<SignatureBean> sbs = extractSignaturesFromSource(source);
            List<SignatureBean> sbFinal = pruneDuplicates(sb, sbs);
            try {
                nu.insertSignatures(Integer.parseInt(node), sbFinal, true);
            } catch (Exception e) {
                LOG.error("Not insert signatures to nodeid: " + node + " " + e.getMessage());
            }
            percent = ((pi + 1) * 100) / num;
            out.println("<STATUS>" + percent + "</STATUS>");
            LOG.debug("finished one proccess: " + percent + "%");
            out.flush();
            pi++;
        }
        out.println("<SUCCESS>finished</SUCCESS>");
        LOG.debug("FINISHED");
    }

    /**
     * extract part from <LinkedSection>
     *
     * @param source
     * @return
     */
    private static List<SignatureBean> extractSignaturesFromSource(String source) {
        LOG.debug("extractSignaturesFromSource");
        String linkedSection = "";
        String wordsLinkedSection[] = null;
        try {
            if (source.contains("<LinkedSection>")) {
                linkedSection = source.substring(
                        source.indexOf("<LinkedSection>") + 15,
                        source.indexOf("</LinkedSection>"));
                wordsLinkedSection = linkedSection.trim().split("\\|");
                //separate the words in a map and create SignatureBean Object
                Map<String, Integer> map = new TreeMap<String, Integer>(String.CASE_INSENSITIVE_ORDER);
                int cont = 0;
                for (int i = 0; i < wordsLinkedSection.length; i++) {
                    cont = 0;
                    for (int k = 0; k < wordsLinkedSection.length; k++) {
                        if (wordsLinkedSection[i].trim().equalsIgnoreCase(wordsLinkedSection[k].trim())) {
                            cont++;
                        }
                    }
                    map.put(wordsLinkedSection[i], cont);
                }
                List<SignatureBean> listSb = new ArrayList<SignatureBean>();
                for (Map.Entry<String, Integer> entry : map.entrySet()) {
                    SignatureBean sb = new SignatureBean();
                    sb.setWord(entry.getKey().trim());
                    sb.setOcurrence(entry.getValue());
                    sb.setLang("EN");
                    listSb.add(sb);
                }
                return listSb;
            } else {
                LOG.debug("The source not contain <LinkedSection>");
                return new ArrayList<SignatureBean>();
            }
        } catch (Exception e) {
            LOG.error("extractSignaturesFromSource " + e.getMessage());
            return new ArrayList<SignatureBean>();
        }
    }

    /**
     * combine two list in one. eliminate duplicates.
     *
     * @param l1 list 1
     * @param l2 list 2
     * @return
     */
    private static List<SignatureBean> pruneDuplicates(List<SignatureBean> l1, List<SignatureBean> l2) {
        LOG.debug("pruneDuplicates");
        List<SignatureBean> listReturn = new ArrayList<SignatureBean>();
        Map<String, SignatureBean> map = new TreeMap<String, SignatureBean>(String.CASE_INSENSITIVE_ORDER);

        //combine lists
        l1.addAll(l2);
        for (SignatureBean sb : l1) {
            map.put(sb.getWord(), sb);
        }
        int cont = 0;
        for (Map.Entry<String, SignatureBean> entry : map.entrySet()) {
            cont = 0;
            for (SignatureBean sb : l1) {
                if (sb.getWord().equalsIgnoreCase(entry.getKey())) {
                    cont += sb.getOcurrence();
                }
            }
            entry.getValue().setOcurrence(cont);
        }
        for (Map.Entry<String, SignatureBean> entry : map.entrySet()) {
            listReturn.add(entry.getValue());
        }
        return listReturn;
    }
}
