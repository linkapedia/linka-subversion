package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 * This API call has been deprecated, and should no longer be used.  Please use TSCQL to achieve this functionality.
 * @note    This API call has been deprecated.
 */
public class TSNodeTitleSearch
{
	
	private static int iCallCounter = 0;
	private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

	// TSNodeTitleSearch (sessionid, keywords, corpusid)
	// Note: Corpusid is OPTIONAL
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		synchronized (ICallCounterSemaphore)
			{ iCallCounter++; }
		out.println ("<CALLCOUNT_FN>" + iCallCounter + "</CALLCOUNT_FN>");

		String sKeywords = (String) props.get ("Keywords");
		String sCorpusID = (String) props.get ("CorpusID");

		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
		
		// Break sKeywords into array of words separated by spaces
		out.println ("<KEYWORDS> "+sKeywords+"</KEYWORDS>");
		
        Vector vKeywords = new Vector();

        while ( true ) {
			int endLoc = sKeywords.indexOf (' ') ;
            if (endLoc == -1) {
			    vKeywords.addElement( sKeywords );
                break; // last element
            } else {
                vKeywords.addElement( sKeywords.substring ( 0, endLoc ) );
                sKeywords = sKeywords.substring ( endLoc + 1  ) ;
			}
		}

		try {
			
			String sSQL = Node.getSQL(dbc)+" where NodeStatus != -1 ";
			for (int loop = 0; loop < vKeywords.size(); loop++) {
				sSQL = sSQL + "and upper(NodeTitle) like upper('%"+vKeywords.elementAt(loop)+"%') ";
			}
				
			if (sCorpusID != null) { sSQL = sSQL + "and CorpusID = "+sCorpusID+" "; }

			sSQL = sSQL + " order by CorpusId, NodeTitle asc";
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.
                Node n = new Node(rs);

                // security now handled by the node emitter
				//if (u.IsAuthorized(n.getCorpusID(), out)) {
					loop = loop + 1;
					if (loop == 1) { out.println ("<NODES> "); }
                    n.emitXML(out, u, true);
                //}
			}
		
			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Sorry no matches for keywords: "+sKeywords+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}
			out.println ("</NODES> ");
		}
		
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		} 
	}
}
