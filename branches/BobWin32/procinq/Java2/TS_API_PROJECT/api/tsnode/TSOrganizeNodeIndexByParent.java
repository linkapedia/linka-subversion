package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Organize node children by the "node index within parent" attribute.  This function may be run periodically
 *  to synchronize node children whose NIWP property may have become exaggerated or dilluted.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID    Unique node identifier of this node being organized.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSOrganizeNodeIndexByParent&NodeID=2000&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Nodes organized successfully..</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSOrganizeNodeIndexByParent {
    // changed: 12/10/03 -- actually edit the name
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sParentID = (String) props.get("NodeID", true);
        String sKey = (String) props.get("SKEY", true);

        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
        ResultSet rs = null;
        Vector v = new Vector();
        try {
            // Ensure user is a member of the admin group
            if (!u.IsMember(out)) {
                out.println("<NOTAUTHORIZED>You are not authorized to use this function</NOTAUTHORIZED>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            String sSQL = "select nodeid from node where parentid = " + sParentID + " order by nodeindexwithinparent";
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            int loop = 0;
            while (rs.next()) {
                loop++;
                String sNodeID = rs.getString(1);
                sSQL = "update node set nodeindexwithinparent = " + loop + ", dateupdated = sysdate where nodeid = " + sNodeID;
                v.addElement(sSQL);
            }
            rs.close();
            rs = null;
            stmt.close();
            stmt = null;

            for (int i = 0; i < v.size(); i++) {
                try {
                    stmt = dbc.createStatement(); stmt.executeUpdate((String) v.elementAt(i));
                } catch (Exception e) {
                    throw e;
                } finally {
                    if (stmt != null) { stmt.close(); stmt = null; }
                }
            }

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }

        out.println("<SUCCESS>Nodes organized successfully.</SUCCESS>");
    }
}
