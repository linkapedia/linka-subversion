package api.tsnode;

import com.iw.system.Node;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

/**
 * return structure of a node.
 *
 *	@authors Andres Restrepo.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetAllNodesFromNodeID&corpus=???&node=999012&SKEY=8919294812

 *	@return	a series of NODE objects
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
<TSRESULT>
<NODES>
<NODE>
<NODEID>1087175</NODEID>
<CORPUSID>300012</CORPUSID>
<NODETITLE><![CDATA[ Fertilizers]]> </NODETITLE>
<PARENTID>1087101</PARENTID>
<NODEINDEXWITHINPARENT>3</NODEINDEXWITHINPARENT>
<DEPTHFROMROOT>1</DEPTHFROMROOT>
<DATESCANNED>null</DATESCANNED>
<DATEUPDATED>null</DATEUPDATED>
<NODESTATUS>1</NODESTATUS>
<NODEDESC><![CDATA[ ]]> </NODEDESC>
<NODESIZE>1</NODESIZE>
<LINKNODEID>1087175</LINKNODEID>
</NODE>
<NODE>
<NODEID>1087101</NODEID>
<CORPUSID>300012</CORPUSID>
<NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
<PARENTID>1087099</PARENTID>
<NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
<DEPTHFROMROOT>0</DEPTHFROMROOT>
<DATESCANNED>null</DATESCANNED>
<DATEUPDATED>null</DATEUPDATED>
<NODESTATUS>1</NODESTATUS>
<NODEDESC><![CDATA[ ]]> </NODEDESC>
<NODESIZE>1</NODESIZE>
<LINKNODEID>1087101</LINKNODEID>
</NODE>
...
</NODES>
</TSRESULT>
\endverbatim
 */
public class TSGetAllNodesFromNodeID {

    private static final Logger log = Logger.getLogger(TSGetAllNodesFromNodeID.class);
    public static User u;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {

        String sKey = (String) props.get("SKEY", true);
        u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        Object node = props.get("node");
        Statement stmt = null;
        ResultSet rs = null;

        String sSQL = Node.getSQL(dbc)
                + "start with nodeid =" + node.toString()
                + "connect by prior nodeid = parentid order by nodeindexwithinparent ";
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            out.println("<NODES>");
            while (rs.next()) {
                Node n = new Node(rs);
                n.emitXML(out, u, true);
            }
            out.println("</NODES>");
        } catch (Exception e) {
            log.error("Class: TSGetAllNodesFromNodeID ", e);

        } finally {
            if (stmt != null) {
                stmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}