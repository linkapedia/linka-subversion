package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Remove all node-document relationships from the server for a given document.  Though this removal is
 * immediate, the relationship persists in an invisible state to prevent future relationships from being
 * instantiated (see also: blacklisting)
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID    Unique document identifier of the node-document relationship being purged.
 *  @param  Permanent   If present, blacklisting will be disregarded and the relationship will be purged permanently.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSRemoveNodeDocuments&DocumentID=15002&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationships deleted successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveNodeDocuments
{
	// remove all node relationships for this document
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDocumentID = (String) props.get ("DocumentID", true);
        String permanent = (String) props.get ("permanent");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
		try {
            // check authorization level before completing this irreversable action
			if ((u == null) || (!u.IsMember(out))) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            String sSQL;

            if (permanent != null) { sSQL = "delete from NodeDocument where DocumentId = "+sDocumentID; }
            else { sSQL = "update NodeDocument set score1 = -10.0 where DocumentId = "+sDocumentID; }

			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Error erasing node document relationships.</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
            out.println("<SUCCESS>Node document relationships deleted successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { if (stmt != null) { stmt.close(); }}
	}
}
