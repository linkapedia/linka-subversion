package api.tsnode;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Vector;

/**
 * Find all leaf nodes in the hierarchy below the given node which have less than X characters in the node source. For each, merge the node, along with the node source and title, into the parent
 * source.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param NodeID Unique node identifier of this node.
 * @param MinChar The minimum number of characters (threshold) before the node is joined
 *
 * @note http://ITSSERVER/itsapi/ts?fn=tsnode.TSRollupLeafNodes&NodeID=2000&MinChar=50&SKEY=9919294812
 *
 * @return	NODEID identifier of the newly created node \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <SUCCESS>Node rollup successful.</SUCCESS> </TSRESULT> \endverbatim
 */
public class TSRollupLeafNodes {

    private static int Count = 0;
    private static int Total = 0;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sMinChar = (String) props.get("MinChar", "50");
        String sShowStatus = (String) props.get("ShowStatus");

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        DecimalFormat twoDigits = new DecimalFormat("0");

        if (u == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
        }

        int iCorpusID = -1;
        int iDepth = -1;

        // 1) Get the CORPUSID of the new PARENT node
        String sSQL = " select CorpusId, DepthFromRoot from Node where NodeID = " + sNodeID;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            iCorpusID = rs.getInt(1);
            iDepth = rs.getInt(2);
            if (!u.IsAdmin(iCorpusID, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } catch (Exception e) {
            api.Log.LogError("error in node rollup, sql: " + sSQL, e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        Vector vLeafs = new Vector();

        // first,  get all nodes from the given node down, along with node source length and parent
        sSQL = "select n.nodeid, n.parentid, n.nodetitle from node n, nodedata d where n.nodeid = d.nodeid (+) "
                + "and n.nodeid in (select nodeid from node start with nodeid = " + sNodeID + " "
                + "connect by prior nodeid = parentid) and (length(d.nodesource) < " + sMinChar + " or d.nodesource is null) and "
                + "n.nodeid not in (select parentid from node where corpusid = " + iCorpusID + ")";

        stmt = null;
        rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) {
                vLeafs.add(rs.getString(1) + "##" + rs.getString(2) + "##" + rs.getString(3));
            }

        } catch (Exception e) {
            api.Log.LogError("error in tsrollup, sql: " + sSQL, e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        Log.Log("Total leaf size: " + vLeafs.size());
        Total = vLeafs.size();

        for (int i = 0; i < vLeafs.size(); i++) {
            String[] s = ((String) vLeafs.elementAt(i)).split("##");
            Log.Log("invoking rollupNode with " + s[0] + " " + s[1] + " " + s[2] + " ");
            rollupNode(s[0], s[1], s[2], out, dbc);

            long percent = ((i + 1) * 100) / Total;
            if (out != null) {
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                out.flush();
            }
        }

        out.println("<SUCCESS>Nodes deleted successfully.</SUCCESS>");

        Count = 0;
        Total = 0;
    }

    public static void rollupNode(String sNodeID, String sParentID, String sNodeTitle, PrintWriter out, Connection dbc) throws Exception {
        // retrieve node source from the leaf
        String sOldNodeSource = "";
        try {
            sOldNodeSource = UtilClob.getClobStringFromNodeData(new Long(sNodeID).longValue(), dbc);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } // ignore an error here, return empty string

        // add the title to it
        sOldNodeSource = sNodeTitle + " " + sOldNodeSource;

        // retrieve node source from the parent
        String sNewNodeSource = "";
        try {
            sNewNodeSource = UtilClob.getClobStringFromNodeData(new Long(sParentID).longValue(), dbc);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } // ignore an error here, return empty string

        // concat them together
        sNewNodeSource = sNewNodeSource + " " + sOldNodeSource;

        // and insert back
        try {
            UtilClob.insertClobToNodeData(new Long(sParentID).longValue(), sNewNodeSource, dbc);
        } catch (Exception e) {
            api.Log.LogError(e);
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
        }

        // and delete the leaf node
        String sSQL = "delete from node where nodeid = " + sNodeID;
        Statement stmt = null;
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }
}
