package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.iw.classification.Data_WordsToNodes;

/**
 * Remove a specific signature word - frequency pair for a given node.  This removal is immediate
 *  and permanent.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID    Unique node identifier.
 *  @param  Word    Signature word being purged.
 *  @param  Frequency   Signature frequency of the word being purged.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSRemoveNodeSigWord&NodeID=15002&Word=test&Frequency=9&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Signature word removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveNodeSigWord
{
	// TSAddNodeSigWord (sessionid, nodeid, word, frequency)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sWord = (String) props.get ("Word", true);
		String sFreq = (String) props.get ("Frequency", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
			
			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			sSQL = " delete from Signature where nodeid = "+sNodeID+" and "+
						  " SignatureWord = '"+sWord+"' and SignatureOccurences = "+sFreq;
			stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from Signature table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_DELETE_SIGNATURE_FAILURE);
			}

            // purge signature cache by corpus
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus
                    ("TSRemoveNodeSigWord", iCorpusId);

			out.println("<SUCCESS>Signature word removed successfully.</SUCCESS>");
		    stmt.close();
			
			sSQL = " update Node set NodeLastSync = SYSDATE, dateupdated = sysdate where NodeID = " +sNodeID;
			stmt = dbc.createStatement();	
			rs = stmt.executeQuery (sSQL);
			rs.next();
			rs.close(); stmt.close();
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
