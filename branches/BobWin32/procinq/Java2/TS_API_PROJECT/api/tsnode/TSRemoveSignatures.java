package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.iw.classification.Data_WordsToNodes;

/**
 * Remove all signature words associated with the given node.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSRemoveSignatures&NodeID=15002&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Signatures removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveSignatures
{
	// TSRemoveSignatures (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
        String sLang = (String) props.get ("lang");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null; ResultSet rs = null;
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
            rs = null; stmt = null;

			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			sSQL = "delete from signature where NodeId = "+sNodeID;
            if (sLang != null) sSQL = sSQL + " and lang = '"+sLang+"'";
			stmt = dbc.createStatement();
			
			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Cannot delete signatures.  No signatures exist?</DEBUG>");
                return;
				//throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
			}

            // purge signature cache by corpus
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus
                    ("TSRemoveSignatures", iCorpusId);


			out.println("<SUCCESS>Signatures removed successfully.</SUCCESS>");
		    stmt.close(); stmt = null;

			sSQL = " update Node set NodeLastSync = SYSDATE, dateupdated = sysdate where NodeID = " +sNodeID;
			stmt = dbc.createStatement();	
			rs = stmt.executeQuery (sSQL);
			rs.next();
			rs.close(); stmt.close();
            rs = null; stmt = null;
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		} finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
