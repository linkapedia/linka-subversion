package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.iw.classification.Data_WordsToNodes;

/**
 * Delete a node from the server.  Typically this is done on a temporary basis, and the node is purged during
 *  a system purge cycle.  However the "permanently" flag may be used to force immediate deletion.  Nodes
 *  deleted in this way are not recoverable.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of this node.
 *  @param  Permanently If this argument is present, the node is deleted immediately and permanently.
 *  @param  Recursive   If this argument is present, all descendents of this node will be marked for deletion as well.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSDeleteNode&NodeID=2000&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node deleted successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSDeleteNode
{
	// TSDeleteNode (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID");
		String sPermanently = (String) props.get ("permanently");
        String sRecursive = (String) props.get ("recursive");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
			
			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
            boolean bRecurse = false; boolean bPermanent = false;
            if (sRecursive != null) { bRecurse = true; }
            if (sPermanently != null) { bPermanent = true; }

            if (bRecurse) { deleteNodeRecursively(sNodeID, bPermanent, dbc, out); }
            else {
                if (bPermanent) { deleteNodePermanently(sNodeID, dbc, out); }
                else { deleteNode(sNodeID, dbc, out); }
            }

            // purge signature cache by corpus
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus
                    ("TSDeleteNode", iCorpusId);

            out.println("<SUCCESS>Node deleted successfully.</SUCCESS>");

		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}

    public static void deleteNodeRecursively (String sNodeID, boolean bPermanent,
                                              Connection dbc, PrintWriter out) throws Exception {
        String sSQL = "select nodeid from node START WITH nodeid = "+sNodeID+" connect by prior nodeid = parentid";
        Statement stmt = null; ResultSet rs = null;

        try {
			stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

			while (rs.next()) {
                sNodeID = rs.getString(1);
                if (bPermanent) { deleteNodePermanently(sNodeID, dbc, out); }
                else { deleteNode(sNodeID, dbc, out); }
            }

        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }

    public static void deleteNode (String sNodeID, Connection dbc, PrintWriter out) throws Exception {
        String sSQL = "update Node set NodeStatus = -1, dateupdated = sysdate where NodeId = "+sNodeID;
        Statement stmt = null;

        try {
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>The specified node does not exist</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
			}
        } catch (Exception e) { throw e; }
        finally { if (stmt != null) { stmt.close(); stmt = null; }}
    }

    public static void deleteNodePermanently (String sNodeID, Connection dbc, PrintWriter out) throws Exception {
        String sSQL = "delete from Node where NodeId = "+sNodeID;
        Statement stmt = null;

        try {
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>The specified node does not exist</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
			}
        } catch (Exception e) { throw e; }
        finally { if (stmt != null) { stmt.close(); stmt = null; }}
    }
}
