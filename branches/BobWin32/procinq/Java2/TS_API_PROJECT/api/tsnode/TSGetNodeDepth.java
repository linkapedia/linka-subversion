package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * This API call has been deprecated.  Please use the TSGetNodeProps API call.
 * @note    This API call has been deprecated.
 */
public class TSGetNodeDepth
{
	// TSGetNodeDepth (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        TSGetNodeProps.handleTSapiRequest(props, out, dbc);
        return;
    }
}
