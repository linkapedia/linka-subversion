package api.tsnode;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.database.SQLGenerators;
import com.indraweb.encyclopedia.DocForScore;
import com.indraweb.execution.Session;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.PhraseParms;
import com.indraweb.ir.clsStemAndStopList;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.util.UtilRandom;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Takes a document from either a http post multipart stream call and generates signatures for an already existing node. They are returned in XML and optionally posted to the database.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param NODEID Node to add signature terms to, and determines the corpus to run against
 * @param POST (optional) Default is false. True indicates add the sigs from this document to the node.
 * @param VERBOSE (optional) Default is false. True indicates to output debugging data to system.out
 * @param PARSEPARMS* (optional) ParseParms for document parse.
 * @param PHRASEPARMS* (optional) ParseParms for phrase extract as signature terms.
 * @param SIGNATUREPARMS* (optional) ParseParms for phrase extract as signature terms.
 *
 *
 * @return XML stream of terms and counts if successful. \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <TERMCOUNT> <TERM>uranium</TERM> <COUNT>83</COUNT> </TERMCOUNT> <TERMCOUNT>
 * <TERM>du</TERM> <COUNT>57</COUNT> </TERMCOUNT> <TERMCOUNT> <TERM>depleted</TERM> <COUNT>55</COUNT> </TERMCOUNT> ... \endverbatim
 */
public class TSCreateSigsFromDoc {

    public static void handleTSapiRequest(HttpServletRequest req,
            HttpServletResponse res,
            api.APIProps props,
            PrintWriter out,
            Connection dbc)
            // ***********************************************************
            throws Exception {

        String sFileNameFullTemp = null;
        try {
            boolean bDebug = false;
            boolean bDebugging = false;
            //boolean bVerbose = com.indraweb.util.UtilFile.bFileExists ("/temp/IndraDebugVerboseTSGetDocSigsInCorpus.txt");

            ParseParms parseparms = new ParseParms(props);
            PhraseParms phraseparms = new PhraseParms(props);
            SignatureParms sigparms = new SignatureParms(props);

            String sURLfull = null;
            String sProp_URL = (String) props.get("URL");
            String sData = (String) props.get("inputstream");

            String sKey = (String) props.get("SKEY", true);

            int iProp_NodeID = props.getint("NODEID", true);
            int iProp_DocIDSource = props.getint("DOCIDSOURCE", "-1");
            int iProp_DocIDAbstract = props.getint("DOCIDABSTRACT", "-1");

            boolean bProp_post = props.getbool("post", "false");
            boolean bProp_verbose = props.getbool("verbose", "true");

            // if classify text was streamed in, add HTML headers (until we can eliminate the parser)
            if ((props.get("fileposted") == null) && (sProp_URL != null)) {
                sData = "<HTML>\n<HEAD><TITLE>" + sData.substring(0, 40) + "</TITLE>\n</HEAD>"
                        + "\n<BODY>\n" + sData + "\n</BODY>\n</HTML>";
            }

            /*
             * to be used if and when add node function is returned here String sProp_PARENTID = null; String sProp_NodeDesc = null; String sProp_NODETITLE = null; //String sProp_WordsAndFreqs = null;
             * String sProp_NODEINDEXWITHINPARENT = null; if ( bProp_post ) // old model - includes add node { sProp_PARENTID = (String) props.get ("PARENTID", "-1"); sProp_NODETITLE = (String)
             * props.get ("NODETITLE", null); sProp_NodeDesc = (String) props.get ("NODEDESC", null); //sProp_WordsAndFreqs = (String) props.get ("WordsAndFreqs", null ); // word1:8;word2:7;word3:4
             * sProp_NODEINDEXWITHINPARENT = (String) props.get ( "INDEXWITHINPARENT", "end" );
             *
             * }
             */

            // return proposed set

            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            if (bDebugging == false && u == null) {
                Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                Log.LogError("in TSCreateSigsFromDoc", e);
                throw e;
            }


            //Ensure user is a member of the admin group // debug
            if (bDebugging == false && bProp_post && !u.IsMember(out)) {
                Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                Log.LogError("in TSCreateSigsFromDoc", e);
                throw e;
            }

            Vector vThree = resolveURLs(iProp_DocIDSource, iProp_DocIDAbstract, false, sProp_URL, dbc);

            sURLfull = (String) vThree.elementAt(0);
            sProp_URL = (String) vThree.elementAt(1);
            sFileNameFullTemp = (String) vThree.elementAt(2);
            System.out.println("sURLfull [" + sURLfull + "]");
            System.out.println("sProp_URL [" + sProp_URL + "]");
            System.out.println("sFileNameFullTemp [" + sFileNameFullTemp + "]");

            String sSQLGetNodeTitle = "select nodetitle from node where nodeid = " + props.getint("NODEID", true);
            String sNodeTitle = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLGetNodeTitle, dbc);

            com.indraweb.profiler.Timer timerDocForScore = com.indraweb.profiler.Profiler.getTimer("DocForScore", true);
            DocForScore docForScore = new DocForScore(
                    sURLfull,
                    sProp_URL,
                    true,
                    false, // no stem needed for sig inserts to db
                    sNodeTitle,
                    null,
                    true,
                    null,
                    true,
                    false,
                    1,
                    parseparms,
                    sData);

            int iSigGenRangeSize = props.getint("SigGenRangeSize", "" + SignatureParms.getSigGenRangeSize_Default());


            //restricted to sig table as well String sSQL = "select n.nodeid from node n, signature s where n.nodeid = s.nodeid and corpusid = " + iProp_CORPUSID +
            //        " and n.nodeid in ( select nodeid from signature ) order by nodeid ";
            // delete old sigs
            if (bProp_post) {
                String sSQLdelsigs = "delete from signature where nodeid = " + iProp_NodeID;
                JDBCIndra_Connection.executeUpdateToDB(sSQLdelsigs, dbc, false, -1);
            }

            String sSQLGetCorpusThisNode = "select corpusid from node where nodeid = " + iProp_NodeID;
            int iCorpusID = (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLGetCorpusThisNode, dbc);
            String sSQL = "select n.nodeid from node n where corpusid = " + iCorpusID;
            //System.out.println("sSQL [" + sSQL+"]");
            Vector vINodeIDsThisCorpus = JDBCIndra_Connection.executeQueryAgainstDBVector(sSQL, dbc);
            Vector vNodeIDsRandom = UtilRandom.randomPermuteVector(vINodeIDsThisCorpus, 0);

//            for ( int i = 0; i < 20 ; i++)
//                System.out.println("vINodeIDsThisCorpus.elementAt(i) [" + vINodeIDsThisCorpus.elementAt(i)+"]");
//            for ( int i = 0; i < 20 ; i++)
//                System.out.println("vNodeIDsRandom.elementAt(i) [" + vNodeIDsRandom.elementAt(i)+"]");

            //System.out.println("vNodeIDsRandom.size() [" + vNodeIDsRandom.size()+"]");
            Object[] oArrNodeIDsRandom = convertVectorTo_ObjArrayCapped(vNodeIDsRandom, iSigGenRangeSize);
            String sNodeIDList = com.indraweb.util.UtilSets.convertObjectArrayToStringWithDelims(oArrNodeIDsRandom, ",");

            String sSQLGetSigs = "select signatureword, signatureoccurences, nodeid from signature "
                    + " where nodeid in (" + sNodeIDList + ") ";


            Hashtable htDocForScoreWordsNCountINOUT = docForScore.getHtWordsNCount(Session.cfg.getPropBool("StemOnClassifyNodeDocScoring"));
            //System.out.println("1 htDocForScoreWordsNCountINOUT.size() [" + htDocForScoreWordsNCountINOUT.size()+"]");

            docForScore.getTopic().addPhrasesToCounts(phraseparms);

            int iDocSize = docForScore.getHtWordsNCount(Session.cfg.getPropBool("StemOnClassifyNodeDocScoring")).size();
            // BIG CALL OUT - REMOVE SIGS THAT THIS DOC DOES NOT HAVE HIGHER THAN NEIGHBORS

            filterOutTermsOccuringMoreFreqInExistingSigs(sSQLGetSigs,
                    htDocForScoreWordsNCountINOUT,
                    sigparms.dSigTieDifferenceEpsilonPercent,
                    dbc);
            //System.out.println("2 htDocForScoreWordsNCountINOUT.size() [" + htDocForScoreWordsNCountINOUT.size()+"]");

            Hashtable htStop = clsStemAndStopList.getHTStopWordListStemmed();
            Enumeration e = htDocForScoreWordsNCountINOUT.keys();
            Vector vSigResults = new Vector();
            int i = 0;
            while (e.hasMoreElements()) {
                String sTerm = (String) e.nextElement();
                sTerm = sigparms.signatureTermQualifierModifier(sTerm);
                if (sTerm != null && htStop.get(sTerm) == null) {
                    Integer ICount = (Integer) htDocForScoreWordsNCountINOUT.get(sTerm);
                    SigResult sigresult = new SigResult(sTerm, ICount.intValue(), htDocForScoreWordsNCountINOUT.size());
                    //api.Log.Log (i + ". adding final term [" + sTerm + "] count [" + ICount.intValue() + "]" );
                    vSigResults.addElement(sigresult);
                }
                i++;
            }

            // sort and truncate
            java.util.Collections.sort(vSigResults, new SigResultComparator());

            // by now it's sorted and truncated - emit XML
            Enumeration e2 = vSigResults.elements();
            int iIndex = 0;
            StringBuffer sbAddNodeTermCount = null;
            if (bProp_post) {
                sbAddNodeTermCount = new StringBuffer();
            }
            String sSemiColon = "";
            Vector vFieldNames = new Vector();
            vFieldNames.addElement("NODEID");
            vFieldNames.addElement("SIGNATUREWORD");
            vFieldNames.addElement("SIGNATUREOCCURENCES");

            Hashtable htStopTerms = null;
            htStopTerms = clsStemAndStopList.getHTStopWordList();

            while (e2.hasMoreElements()) {
                // exit when have enough sig terms for this new one
                if (sigparms.iMaxSigTermsPerNode > 0 && iIndex == sigparms.iMaxSigTermsPerNode) {
                    break;
                }

                SigResult sigresult = (SigResult) e2.nextElement();
                String sTerm = sigresult.sTerm.trim();
                // if not a stop term - with stem on or off ...
                if (htStopTerms.get(sTerm) == null) {

                    out.println("<TERMCOUNT>");
                    //out.println(" <INDEX>" + iIndex + "</INDEX>");
                    out.println(" <TERM>" + sTerm + "</TERM>");
                    out.println(" <COUNT>" + sigresult.iCount + "</COUNT>");
                    if (bProp_post) {
                        Vector vValues = new Vector();
                        vValues.addElement("" + iProp_NodeID);
                        vValues.addElement("'" + sTerm + "'");
                        vValues.addElement("" + sigresult.iCount);
                        String sSQLInsertSig = SQLGenerators.genSQLInsert("signature", vFieldNames, vValues);
                        //System.out.println("sSQLInsertSig [" + sSQLInsertSig + "]" );
                        JDBCIndra_Connection.executeUpdateToDB(sSQLInsertSig, dbc, false, -1);


                    } // if not a stop term
                }
                out.println("</TERMCOUNT>");
                iIndex++;
            } // for each sig term

            String sSQLUpdateNodeSize = "update node set NodeSize = " + docForScore.getSCarr(false).length
                    + ", dateupdated = sysdate where nodeid = " + iProp_NodeID;
            JDBCIndra_Connection.executeUpdateToDB(sSQLUpdateNodeSize, dbc, false, -1);

            out.flush();
            if (bProp_post) {
                dbc.commit();
                /*
                 * code to add a node through TSAddNode instead - if and when desired
                 *
                 * Hashtable htArguments_AddNode = new Hashtable(); htArguments_AddNode.put ("WordsAndFreqs",sbAddNodeTermCount.toString()); htArguments_AddNode.put ("ParentID",sProp_PARENTID);
                 * htArguments_AddNode.put ("CorpusID",""+iProp_CORPUSID); String sNodeTitleFinal = sProp_NODETITLE; if ( sNodeTitleFinal == null || sNodeTitleFinal.trim().equals("")) sNodeTitleFinal
                 * = docForScore.getTitle(false); htArguments_AddNode.put ("NodeTitle",sNodeTitleFinal); htArguments_AddNode.put ("NodeSize",""+docForScore.getSCarr(bStemOn).length);
                 * htArguments_AddNode.put ("NodeDesc",""+sProp_NodeDesc); htArguments_AddNode.put ("NodeIndexInParent",""+sProp_NODEINDEXWITHINPARENT); htArguments_AddNode.put ("ParentRel","1");
                 * htArguments_AddNode.put ("SKEY",sKey); //NodeIndexInParent=20
                 *
                 * if ( true ) { InvokeAPI API = new InvokeAPI ("tsnode.TSAddNode", htArguments_AddNode); Hashtable htResults = API.Execute(out); String sSuccess = (String) htResults.get("SUCCESS");
                 * if ( bVerbose ) System.out.println("tsaddnode sSuccess [" + sSuccess + "]" ); String sNodeID = (String) htResults.get("NODEID"); if ( bVerbose ) System.out.println("tsaddnode
                 * sNodeID [" + sNodeID + "]" ); } else { System.out.println("debug tsaddnode" ); System.out.println("debug tsaddnode" ); System.out.println("debug tsaddnode" );
                 * System.out.println("debug tsaddnode" ); System.out.println("debug tsaddnode" ); try { MainTest.testTSAddNode(htArguments_AddNode, dbc); } catch (Throwable t) { throw new Exception
                 * ("error in MainTest.testTSAddNode"); } }
                 */
                //System.out.println("hbk post tsnode.TSAddNode " );
            }
            out.println("<SUCCESS>" + "</SUCCESS>");


        } catch (Exception eTopLevel) {
            //String stack = Log.stackTraceToString (e21);
            Log.LogError("in TSCreateSigsFromDoc", eTopLevel);
            EmitGenXML_ErrorInfo.emitException("TSException", eTopLevel, out);
        } finally {  // ACCOUNTING
            boolean bLeaveTempFileIndicator = com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebug_LeaveCreateNodeSigsTempFiles.txt");
            if (!bLeaveTempFileIndicator && sFileNameFullTemp != null && sFileNameFullTemp.toLowerCase().indexOf("classificationtest") < 0) {
                // delete remove temp file
                System.out.println("deleteing file sFileNameFullTemp [" + sFileNameFullTemp + "]");
                com.indraweb.util.UtilFile.deleteFile(sFileNameFullTemp);
            } else {
                System.out.println("not deleteing create sigs temp file as present : IndraDebug_LeaveCreateNodeSigsTempFiles.txt sFileNameFullExplainTemp [" + sFileNameFullTemp + "]");
            }
        }
    } // public static void handleTSapiRequest ( HttpServletRequest req, api.APIProps props, PrintWriter out, Connection dbc )

    private static class SigResult {

        int iIndex = -1;
        String sTerm = null;
        int iNodeDocSize = -1;
        int iCount = -1;
        double dFreq = -1.0;

        public SigResult(String sTerm_, int iCount_, int iNodeDocSize) {

            sTerm = sTerm_;
            iCount = iCount_;
            dFreq = (double) ((double) iCount / (double) iNodeDocSize);
        }
    }

    private static class SigResultComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {

            SigResult sr1 = (SigResult) o1;
            SigResult sr2 = (SigResult) o2;
            if (sr1.dFreq < sr2.dFreq) {
                return 1;
            } else if (sr1.dFreq > sr2.dFreq) {
                return -1;
            } else {
                return sr1.sTerm.compareTo(sr2.sTerm);
            }
        }

        @Override
        public boolean equals(Object obj) {
            return false;
        }
    }

    private static Vector resolveURLs(
            int iProp_DocIDSource,
            int iProp_DocIDAbstract,
            boolean bVerbose,
            String sProp_URL,
            Connection dbc) throws Exception {
        String sURLfull = null;
        String sFileNameFullTemp = null;
        try {
            //BUILD DOC FOR SCORE
            //boolean bUseNuggets = com.indraweb.execution.Session.cfg.getPropBool ("UseNuggets");

            //api.Log.Log ("iProp_DocIDSource [" + iProp_DocIDSource + "]");
/*
             * code path not tested - from classify - can take an ID and translate into temp file thru oracle if (( iProp_DocIDSource > 0 ) || ( iProp_DocIDAbstract > 0 )) // grab a url and read it
             * from the DB { boolean bAbstractOnly = false; if (iProp_DocIDAbstract > 0) { bAbstractOnly = true; iProp_DocIDSource = iProp_DocIDAbstract; }
             *
             * //iNodeToScoreExplain = -2; // code for : EXPLAIN ALL - IN GUI EXPLAIN MODE String sSQLGetURL = "select docurl from document where documentid = " + iProp_DocIDSource;
             *
             * //System.out.println("tsclassify docidsource sSQLGetURL [" + sSQLGetURL + "]" ); String sDB_URL = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBStr (sSQLGetURL , dbc);
             * //api.Log.Log ("explain iProp_DocIDSource [" + iProp_DocIDSource + "] sDB_URL [" + sDB_URL + "]");
             *
             * if (!sDB_URL.toLowerCase ().startsWith ("http")) { if (!sDB_URL.toLowerCase ().startsWith ("file")) sURLfull = "file:///" + sDB_URL; else sURLfull = sDB_URL; } else sURLfull = sDB_URL;
             *
             * if (true) // debug { api.Log.Log ("override file !!"); api.Log.Log ("override file !!"); api.Log.Log ("override file !!"); api.Log.Log ("override file !!"); //sFileNameFullExplainTemp =
             * "C:/Program Files/ITS/CorpusSource/classificationtest/Corpus11_Node95634_gulfTopic.html"; //sFileNameFullTemp = "C:/Program
             * Files/ITS/CorpusSource/classificationtest/Corpus16Node207974_GulfWarIllnesses.html"; sFileNameFullTemp = "C:/Program Files/ITS/CorpusSource/classificationtest/DepletedUranium.html";
             * sURLfull = "file:///" + sFileNameFullTemp; } else {
             *
             * String sPath = com.indraweb.execution.Session.cfg.getProp ("IndraHome") + "/tempExplain/"; sFileNameFullTemp = OracleDocumentConnector.getTextFromOracleInATempFileName (
             * iProp_DocIDSource , -1 , dbc , bVerbose , bAbstractOnly , sPath); sURLfull = "file://" + sFileNameFullTemp; }
             *
             *
             * }
             * else // usual mode - posted file
             */
            {
                String sURLCorpusRelative = sProp_URL;
                if (!sURLCorpusRelative.toLowerCase().startsWith("http")) {
                    if (!sURLCorpusRelative.toLowerCase().startsWith("file")) {
                        sURLfull = "file:///" + com.indraweb.execution.Session.cfg.getProp("IndraHome") + sURLCorpusRelative;
                    } else {
                        sURLfull = sURLCorpusRelative;
                    }

                    sFileNameFullTemp = com.indraweb.execution.Session.cfg.getProp("IndraHome") + sURLCorpusRelative;
                } else {
                    sURLfull = sURLCorpusRelative;
                }
            }

        } catch (Exception e) {
            api.Log.LogError("error resolving url", e);
            throw e;
        }

        Vector vThree = new Vector();
        vThree.addElement(sURLfull);
        vThree.addElement(sProp_URL);
        vThree.addElement(sFileNameFullTemp);

        /*
         * api.Log.Log ("doc/file info RESULT VTHREE [" + iProp_DocIDSource + "] sURLfull [" + sURLfull + "] sProp_URL [" + sProp_URL + "] sFileNameFullTemp [" + sFileNameFullTemp );
         */

        return vThree;
    }

    private static void filterOutTermsOccuringMoreFreqInExistingSigs(String sSQLGetSigs,
            Hashtable htDocForScoreWordsNCountINOUT,
            double dSigTieDifferenceEpsilon,
            Connection dbc) throws Exception {


        //api.Log.Log ("in filterOutTermsOccuringMoreFreqInExistingSigs words pre [" + UtilSets.htToString(htDocForScoreWordsNCountINOUT, "\r\n")+ "]" );

        Hashtable htNodeIDToIntNodeSize = new Hashtable();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQLGetSigs);

            int iLoop = 0;
            while (rs.next()) {
                String sWord = rs.getString(1).toLowerCase();
                int iExistingSigsCount = rs.getInt(2);
                int iNodeID = rs.getInt(3);
                Integer INodeSize = (Integer) htNodeIDToIntNodeSize.get("" + iNodeID);
                if (INodeSize == null) {
                    String sSQLGetNodeSize = "select nodesize from node where nodeid = " + iNodeID;
                    int iNodeSize = (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLGetNodeSize, dbc);
                    INodeSize = new Integer(iNodeSize);
                    htNodeIDToIntNodeSize.put("" + iNodeID, INodeSize);
                }
                double dExistingSigsWordFreq = (double) iExistingSigsCount / INodeSize.doubleValue();
                Integer IDocCount = (Integer) htDocForScoreWordsNCountINOUT.get(sWord);
                if (IDocCount == null) {
                    IDocCount = new Integer(0);
                }
                //api.Log.Log (iLoop + ". doc size [" + (double) htDocForScoreWordsNCountINOUT.size() + "] pre node size [" + INodeSize+ "]" );
                //api.Log.Log (iLoop + ".doc count [" + IDocCount  + "] node count [" + iExistingSigsCount + "]" );
                //int iDocCount = com.indraweb.utils.sets.UtilSets.hash_get_count_for_string (htdocWordFreq_StrToDouble_INOUT , sWord.toLowerCase ());
                double dDocFreq = (double) (IDocCount.doubleValue() / (double) htDocForScoreWordsNCountINOUT.size());
                if (dDocFreq < (dExistingSigsWordFreq - (dSigTieDifferenceEpsilon / (double) 100))) {
                    htDocForScoreWordsNCountINOUT.remove(sWord.toLowerCase());
//                    api.Log.Log (iLoop + ". removing doc term [" + sWord.toLowerCase() +
//                            "] freq [" + dDocFreq + "] dExistingSigsWordFreq [" + dExistingSigsWordFreq + "]" );
                }
//                else
//                    api.Log.Log (iLoop + ". keeping doc term [" + sWord.toLowerCase() +
//                            "] freq [" + dDocFreq + "] dExistingSigsWordFreq [" + dExistingSigsWordFreq + "]" );

                iLoop++;
            }

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        //api.Log.Log ("after filterOutTermsOccuringMoreFreqInExistingSigs words pre [" + UtilSets.htToString(htDocForScoreWordsNCountINOUT, "\r\n")+ "]" );

    }

    // ***************************************************
    public static Object[] convertVectorTo_ObjArrayCapped(Vector v, int iCap) // ***************************************************
    {
        int iSize = iCap;
        if (v.size() < iCap) {
            iSize = v.size();
        }
        Object[] oArr = new Object[iSize];
        Enumeration e = v.elements();
        int i = 0;
        while (e.hasMoreElements() && i < iSize) {
            oArr[i] = e.nextElement();
            i++;
        }
        return oArr;
    }
}
