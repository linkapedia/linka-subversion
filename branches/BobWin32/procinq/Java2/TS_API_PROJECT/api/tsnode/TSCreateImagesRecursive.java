package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import api.util.bean.SearchBean;
import api.util.interfaces.INotifyAll;
import api.util.search.images.HandlerBing;
import api.util.search.images.NotifySearchBing;
import api.util.search.images.SearchImageWorker;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Andres
 */
public class TSCreateImagesRecursive implements INotifyAll {

    private static final Logger LOG = Logger.getLogger(TSCreateImagesRecursive.class);
    private static final int THREAD_SIZE = 3;
    private static final long WATCH_THREAD = 15000;
    private static final boolean FILTER_NODES_WITH_IMAGES = false;
    private final List<SearchImageWorker> workers = new ArrayList<SearchImageWorker>();
    private HandlerBing handle = null;
    private NotifySearchBing notify = null;
    private Timer timer = null;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        LOG.debug("TSCreateImagesRecursive");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        String corpusid = (String) props.get("corpusid");

        if ((nodeid == null) || (corpusid == null)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if ((nodeid.isEmpty()) || (corpusid.isEmpty())) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        //get terms to search
        Map<String, String> terms = new HashMap<String, String>();

        try {
            //Get the affinity term from corpusid
            String taxSearch = nu.getFirstAffinityTerm((Integer.parseInt(corpusid)));
            LOG.debug("Affinity term for taxonomy: " + taxSearch);
            terms = nu.getNodeSearch(Integer.parseInt(nodeid), taxSearch);
            LOG.debug("Terms for the search(taxonomy musthave+node musthave): " + terms);
        } catch (Exception e) {
            LOG.error("An exception ocurred: ", e);
            throw e;
        }
        if (terms.isEmpty()) {
            LOG.debug("Not found terms to search");
        }

        if (FILTER_NODES_WITH_IMAGES) {
            //filter node with images
            try {
                terms = filterNodesWithImages(terms, nodeid, dbc);
            } catch (Exception ex) {
                throw ex;
            }
            if (terms != null && terms.isEmpty()) {
                LOG.debug("All nodes have images");
            }
        }

        //start logic to manager thread
        //create list thread

        TSCreateImagesRecursive tsImage = new TSCreateImagesRecursive();
        tsImage.notify = new NotifySearchBing(out);
        tsImage.notify.setNum(terms.size());
        tsImage.handle = new HandlerBing(tsImage.workers, tsImage);
        Thread th;
        SearchImageWorker imageWorker;
        for (int i = 0; i < THREAD_SIZE; i++) {
            imageWorker = new SearchImageWorker(tsImage.notify);
            th = new Thread(imageWorker);
            imageWorker.setThread(th);
            tsImage.workers.add(imageWorker);
        }
        //set terms to each thread
        int index = 0;
        for (Map.Entry<String, String> entry : terms.entrySet()) {
            SearchBean infoSearch = new SearchBean(entry.getKey(), entry.getValue());
            tsImage.workers.get(index).addSearchTerm(infoSearch);
            index = ((index + 1) % THREAD_SIZE);
        }

        tsImage.handle.startThread();
        tsImage.timer = new Timer();
        long currentTime = System.currentTimeMillis() + WATCH_THREAD;
        tsImage.timer.schedule(tsImage.handle, new java.util.Date(currentTime), WATCH_THREAD);
        synchronized (tsImage) {
            tsImage.wait();
        }

        //get result
        LOG.debug("This terms not found in BING");

        List<String> termsFails = new ArrayList<String>();
        int tErrors = 0;
        int tSuccess = 0;
        for (SearchImageWorker siw : tsImage.workers) {
            termsFails.addAll(siw.getListTermsError());
            tErrors += siw.getNumTermsErrors();
            tSuccess += siw.getNumTermsSuccess();
        }
        if (!termsFails.isEmpty()) {
            for (String termE : termsFails) {
                out.println("<TERM>");
                out.println(URLEncoder.encode(termE, "UTF-8"));
                out.println("</TERM>");
            }
        }
        out.println("<NUMERRORS>");
        out.println(tErrors);
        out.println("</NUMERRORS>");
        out.println("<NUMSUCCESS>");
        out.println(tSuccess);
        out.println("</NUMSUCCESS>");

        out.println("<SUCCESS>images created successfully.</SUCCESS>");
        LOG.debug("FINISHED");
    }

    private static Map<String, String> filterNodesWithImages(Map<String, String> map, String nodeid, Connection dbc) throws Exception {
        //select nodes without images
        Map<String, String> mapReturn = new HashMap<String, String>();
        String sql = "SELECT NODEID FROM NODE n WHERE NOT EXISTS(SELECT 1 FROM NODEIMAGES WHERE NODEID = n.NODEID) START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = dbc.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(nodeid));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String node = String.valueOf(rs.getInt("NODEID"));
                String term = map.get(node);
                if (term != null) {
                    mapReturn.put(node, term);
                }
            }
            rs.close();
            rs = null;
            pstmt.close();
            pstmt = null;
            return mapReturn;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        }

    }

    @Override
    public void onFinish(String message) {
        LOG.debug(message);
        synchronized (this) {
            this.notify();
        }
        notify.close();
        timer.cancel();
    }
}
