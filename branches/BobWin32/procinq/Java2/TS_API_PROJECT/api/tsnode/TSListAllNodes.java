package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.results.NodeDocumentResult;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
//import api.results.*;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

// TSListAllNodes
// Return all nodes in a given corpus, returned in depth first order
// Note: this API function should be used sparingly, as it is not time efficient.
/**
 * Retrieve all nodes within a given corpus, returned in depth first order.  This function may take several
 *  minutes to complete.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID  Unique corpus identifier.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSListAllNodes&CorpusID=9&SKEY=8919294812

 *	@return	a series of NODE objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>1087175</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Root Node]]> </NODETITLE>
            <PARENTID>-1</PARENTID>
            <NODEINDEXWITHINPARENT>3</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>1</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087175</LINKNODEID>
        </NODE>
        <NODE>
            <NODEID>1087101</NODEID>
            <CORPUSID>300012</CORPUSID>
            <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
            <PARENTID>1087099</PARENTID>
            <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
            <DEPTHFROMROOT>0</DEPTHFROMROOT>
            <DATESCANNED>null</DATESCANNED>
            <DATEUPDATED>null</DATEUPDATED>
            <NODESTATUS>1</NODESTATUS>
            <NODEDESC><![CDATA[ ]]> </NODEDESC>
            <NODESIZE>1</NODESIZE>
            <LINKNODEID>1087101</LINKNODEID>
        </NODE>
 ...
    </NODES>
 </TSRESULT>
  \endverbatim
 */
public class TSListAllNodes
{
    public static User u;
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sKey = (String) props.get("SKEY", true);
		String sCountries = (String) props.get("Country"); // optional
		u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			//Hashtable htAuth = u.GetAuthorizedHash(out);
			//if (htAuth.containsKey(sCorpusID)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			// 
			out.println("<NODES>");
			if (sCountries != null) { 
				// Format sCountries to conduct SQL query correctly
				sCountries = "'"+sCountries+"'";
				sCountries = com.indraweb.util.UtilStrings.replaceStrInStr(sCountries, ",", "','");
				RecurseWriteNode(sCorpusID, "-1", sCountries, dbc, out); 
			} else { RecurseWriteNode(sCorpusID, "-1", dbc, out); }
			out.println("</NODES>");
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}

	public static void RecurseWriteNode (String sCorpusID, String sParent, Connection dbc, PrintWriter out) 
	throws Exception {
		String sSQL = Node.getSQL(dbc)+" where CorpusID = "+sCorpusID+" and ParentID = "+sParent+
					  " and CorpusId = " + sCorpusID+" order by NodeIndexWithinParent asc";
        Statement stmt = dbc.createStatement();
		ResultSet rs = stmt.executeQuery (sSQL);
 
		int loop = 0;
		while ( rs.next() ) {
            Node n = new Node(rs);
            out.println ("   <NODE>");
            n.emitXML(out, u, false);

			WriteDocument(n.get("NODEID"), dbc, out);
			out.println ("   </NODE>");
			RecurseWriteNode (sCorpusID, n.get("NODEID"), dbc, out);
		}
		
		rs.close();
	    stmt.close();
	}

    // write nodes only from specific countries:
	public static void RecurseWriteNode (String sCorpusID, String sParent, String sCountries, Connection dbc, PrintWriter out) 
	throws Exception {
		String sSQL = Node.getSQL(dbc)+" where NodeStatus != -1 and ParentId = " + sParent +
				      " and CorpusID = "+sCorpusID+" and NodeID in (select NodeID from IdracNode where "+
				      " NodeCountry in ("+sCountries+")) order by NodeIndexWithinParent asc";
		Statement stmt = dbc.createStatement();
		ResultSet rs = stmt.executeQuery (sSQL);

		int loop = 0;
		while ( rs.next() ) {
            Node n = new Node(rs);
            out.println ("   <NODE> ");
            n.emitXML(out, u, false);
			WriteDocument(n.get("NODEID"), sCountries, dbc, out);
			out.println ("   </NODE>");
			RecurseWriteNode (sCorpusID, n.get("NODEID"), sCountries, dbc, out);
		}
		
		rs.close();
	    stmt.close();
	}
	
	public static void WriteDocument (String sNodeID, Connection dbc, PrintWriter out) 
	throws Exception {
		String sSQL = " select D.DocumentID, D.DocTitle, D.DocURL, N.DocSummary, "+
					  " N.Score1, N.Score2, N.Score3, N.Score4, N.Score5, N.Score6, "+
					  " to_char(N.DateScored, 'MM/DD/YYYY HH24:MI:SS'), D.GenreID from Document D, "+ 
					  " NodeDocument N where D.DocumentId = N.DocumentID AND N.NodeID = "+sNodeID+
					  " order by N.indexwithinnode asc";
        out.println("<DEBUG>"+sSQL+"</DEBUG>");
        Statement stmt = dbc.createStatement();
		ResultSet rs = stmt.executeQuery (sSQL);

		int loop = 0;
		while (rs.next()) {
			if (loop == 0) { out.println ("<NODEDOCUMENTS> "); } loop++;

			String iDocID = rs.getString(1);
			String sDocTitle = rs.getString(2);
			String sDocURL = rs.getString(3);
			String sDocSummary = rs.getString(4);
			String fScore1 = rs.getString(5);
			String fScore2 = rs.getString(6);
			String fScore3 = rs.getString(7);
			String fScore4 = rs.getString(8);
			String fScore5 = rs.getString(9);
			String fScore6 = rs.getString(10);
			String sDateScored = rs.getString(11);
			String sFolderID = rs.getString(12);
			String sType = "0";

			out.println ("   <NODEDOCUMENT> ");
			out.println ("	    <NODEID>"+sNodeID+"</NODEID>");
			out.println ("	    <DOCUMENTID>"+iDocID+"</DOCUMENTID>");
			out.println ("      <FOLDERID>"+sFolderID+"</FOLDERID>");
			out.println ("      <DOCTITLE><![CDATA["+sDocTitle+"]]></DOCTITLE>");
			out.println ("      <DOCURL><![CDATA["+sDocURL+"]]></DOCURL>");
			out.print ("      <DOCSUMMARY><![CDATA[");
			for (int k = 0; k < sDocSummary.length(); k++) {
				int ichar = (int) sDocSummary.charAt(k);
				if ((ichar != 13) && (ichar != 10)) { out.print(sDocSummary.charAt(k)); }
			}
			out.println ("]]></DOCSUMMARY>");
			out.println ("      <SCORE1>"+fScore1+"</SCORE1>");
			out.println ("      <SCORE2>"+fScore2+"</SCORE2>");
			out.println ("      <SCORE3>"+fScore3+"</SCORE3>");
			out.println ("      <SCORE4>"+fScore4+"</SCORE4>");
			out.println ("      <SCORE5>"+fScore5+"</SCORE5>");
			out.println ("      <SCORE6>"+fScore6+"</SCORE6>");
			out.println ("      <DATESCORED>"+sDateScored+"</DATESCORED>");
			out.println ("		<TYPE>"+sType+"</TYPE>");
			out.println ("   </NODEDOCUMENT>");
		}
		if (loop > 0) { out.println("</NODEDOCUMENTS>"); }
	} // only retrieve documents within a specific set of countries
	public static void WriteDocument (String sNodeID, String sCountries, Connection dbc, PrintWriter out) 
	throws Exception {
		Vector vResults = new Vector(); String sLastDocTitle = "";
		String sSQL = " select D.DocumentID, D.DocTitle, D.DocURL, N.DocSummary, "+
			 	      " N.Score1, N.Score2, N.Score3, N.Score4, N.Score5, N.Score6, "+
	   	 			  " to_char(N.DateScored, 'MM/DD/YYYY HH24:MI:SS'), D.GenreID from Document D, "+
					  " NodeDocument N where D.DocumentId = N.DocumentID AND N.NodeID = "+sNodeID+ 
					  " and D.Country in ("+sCountries+") "+
					  " order by N.IndexWithinNode, D.DocTitle asc";
        out.println("<DEBUG>"+sSQL+"</DEBUG>");
        Statement stmt = dbc.createStatement();
		ResultSet rs = stmt.executeQuery (sSQL);

		int loop = 0;
		while (rs.next()) {
			loop++;
			if (loop == 1) { out.println("<NODEDOCUMENTS>"); }
			String iDocID = rs.getString(1);
			String sDocTitle = rs.getString(2);
			String sDocURL = rs.getString(3);
			String sDocSummary = rs.getString(4);
			String fScore1 = rs.getString(5);
			String fScore2 = rs.getString(6);
			String fScore3 = rs.getString(7);
			String fScore4 = rs.getString(8);
			String fScore5 = rs.getString(9);
			String fScore6 = rs.getString(10);
			String sDateScored = rs.getString(11);
			String sFolderID = rs.getString(12);
			String sType = "0";
			
			if (sDocSummary == null) { sDocSummary = "(no abstract found)"; }

			if ((!sDocTitle.equals(sLastDocTitle)) && (!sLastDocTitle.equals(""))) {
				Enumeration eV = vResults.elements();

				PrintNodeDocumentResults(vResults, sCountries, out, dbc);
				vResults = new Vector();
			}
			NodeDocumentResult ndr = new NodeDocumentResult(sNodeID, iDocID+"", sFolderID,
															sDocTitle, sDocURL, sDocSummary,
															fScore1,"0.0","0.0","0.0","0.0","0.0",
															sDateScored);
			vResults.addElement(ndr); sLastDocTitle = sDocTitle;
		}
		if (!vResults.isEmpty()) { 
			PrintNodeDocumentResults(vResults, sCountries, out, dbc); 
		}
		if (loop != 0) { out.println("</NODEDOCUMENTS>"); }

	}
	
	public static void PrintNodeDocumentResults (Vector vResults, String sCountries, 
												 PrintWriter out, Connection dbc)
		throws Exception {
		NodeDocumentResult ndr = (NodeDocumentResult) vResults.elementAt(0);

		out.println ("   <NODEDOCUMENT> ");
		out.println ("	    <NODEID>"+ndr.GetNodeID()+"</NODEID>");
		out.println ("	    <DOCUMENTID>"+ndr.GetDocID()+"</DOCUMENTID>");
		out.println ("      <FOLDERID>"+ndr.GetFolderID()+"</FOLDERID>");
		out.println ("      <DOCTITLE><![CDATA["+ndr.GetDocTitle()+"]]></DOCTITLE>");
		out.println ("      <DOCURLS>");
		
		Enumeration eV = vResults.elements();
		while (eV.hasMoreElements()) {
			NodeDocumentResult n = (NodeDocumentResult) eV.nextElement();
			out.println ("      <DOCURL><![CDATA["+n.GetDocURL()+"]]></DOCURL>");
		}
		
		out.println ("      </DOCURLS>");
		out.print ("      <DOCSUMMARY><![CDATA[");
		for (int k = 0; k < ndr.GetDocSummary().length(); k++) {
			int ichar = (int) ndr.GetDocSummary().charAt(k);
			if ((ichar != 13) && (ichar != 10)) { out.print(ndr.GetDocSummary().charAt(k)); }
		}
		out.println ("]]></DOCSUMMARY>");
		out.println ("      <SCORE1>"+ndr.GetScore1()+"</SCORE1>");
		out.println ("      <DATESCORED>"+ndr.GetDateScored()+"</DATESCORED>");
		out.println ("		<TYPE>0</TYPE>");
		out.println ("   <COUNTRIES>");

		eV = vResults.elements();
		while (eV.hasMoreElements()) {
			NodeDocumentResult n = (NodeDocumentResult) eV.nextElement();

			// print out associated countries
			String sSQL = " select country from Document where "+
					      " DocumentID = "+n.GetDocID()+" and "+
						  " Country in ("+sCountries+")";
			Statement stmt2 = dbc.createStatement();	
			ResultSet rs2 = stmt2.executeQuery (sSQL);
			while ( rs2.next() ) { 
				String s = rs2.getString(1);
				out.println("       <COUNTRY>"+s+"</COUNTRY>");
			} rs2.close(); stmt2.close();
		}
					
		out.println ("   </COUNTRIES>");
		out.println ("   </NODEDOCUMENT>");

		return;
	}

}

