package api.tsnode.musthavegate.components;

import api.report.OutputFormatException;
import com.iw.system.MustHaveGateReport;

/**
 *
 * @author andres
 */
public interface OutputFormat {

    public String getData(MustHaveGateReport data) throws OutputFormatException;
}
