package api.tsnode.musthavegate.components;

/**
 * info in each node tree
 * @author andres
 */
public class ModelTreeNode {

    private String id;
    private String title;
    private String parentid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }
}
