/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Nov 3, 2003
 * Time: 5:17:14 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.multilingual;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.database.DBAccess_Central_connection;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.ir.ParseParms;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Vector;

/**
 * TSMigrateCorpusLanguageData is the API function for corpus data migration from old to new schema. Once all corpora have been migrated into the multilingual database schema this function may be
 * deprecated or eliminated.
 *
 * Data migration means that new signature data are created from existing data.
 *
 * @author Henry Kon
 * @since 3.x
 */
public class TSMigrateCorpusLanguageData {

    // get node titles into sig table
    // get sig words into sig table
    /**
     * Propagates the node title and signature term set information from a corpus in the old schema to the new. <p>
     *
     *
     * @param CorpusID the corpus to be translated
     * <p/>
     * @return XML out has <RESULT>SUCCSES</RESULT>;
     * <p/>
     * @since 3.x
     */
    // add an idrac document object
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        //String sKey = (String) props.get ("SKEY" , true);
        //User u = (User) com.indraweb.execution.Session.htUsers.get (sKey);

        try {
            // Ensure user is a member of the admin group
            // if (u == null)
            // {
            //     throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            // }

            System.out.println("security disabled in TSMigrateCorpusLanguageData");
            // required fields:
            if ((String) props.get("corpusid") == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }

            int iCorpusID = props.getint("corpusid", true);

            try {
                doMigrate(iCorpusID, dbc);
            } catch (Throwable t) {
                Log.LogError("error in translate", t);
                throw new Exception("error in translate : " + t.getMessage());
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }

    /**
     * migrates data from old signature table to new (NOT a translation)
     */
    public static void doMigrate(int iCorpusID, Connection dbc) throws Throwable {


        String sSQLDeleteOldTarget = "delete from signaturenew where  " + // english
                " nodeid in ( select nodeid from node where corpusid = "
                + iCorpusID + ")";
        System.out.println("sSQLDeleteOldTarget [" + sSQLDeleteOldTarget + "]");
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLDeleteOldTarget, dbc, true, 0);


        // now loop over all sigs by nodeid, by signatureoccurences desc
        String sSQL = "select n.nodeid, signatureword, signatureoccurences "
                + " from node n, signature s where s.nodeid = n.nodeid and corpusid = "
                //+ iCorpusID ;
                + iCorpusID + " order by nodeid, signatureoccurences desc";
        System.out.println("sSQL [" + sSQL + "]");
        // get old data - node, word, count
        Vector vQueryData = localExecuteQueryAgainstDBVector(sSQL, dbc);
        Enumeration eDataRows = vQueryData.elements();

        int iLoop = -1;
        int iNodeIDLast = -1;
        int iTermPosCounter = 1;
        int iMaxCounter = -1;
        int iNodeCount = 0;
        HashSet hsTitleTerms = null;
        while (eDataRows.hasMoreElements()) {

            iLoop++;
            if (iLoop % 1000 == 0) {
                System.out.println("Entering sig new insert # [" + iLoop + "]");
                //debug System.out.println ("COMMITING EVERY TIME NOW!" );
                if (iLoop > 0) {
                    dbc.commit();
                }
            }
            //debug dbc.commit();
            Vector vRow = (Vector) eDataRows.nextElement();
            int iNodeID = ((BigDecimal) vRow.elementAt(0)).intValue();
            String sSigWord = ((String) vRow.elementAt(1));
            int iSigCount = ((BigDecimal) vRow.elementAt(2)).intValue();

            if (iSigCount > iMaxCounter) {
                iMaxCounter = iSigCount;
            }

            // position counter reset
            if (iNodeID != iNodeIDLast) {
                iNodeCount++;
                String sSQLDeleteOldSig = "delete from Signaturenew where NODEID = " + iNodeID;
                com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLDeleteOldSig, dbc, false, -1);

                if (iNodeIDLast > 0) {
                    updateNodeTitleCountWords(iNodeIDLast, iMaxCounter, dbc);
                    /*
                     * String sSQLDeletedups = "delete from Signaturenew where NODEID = " + iNodeID + " and termposition > 0 and termid in " + "( select from Signaturenew where NODEID = " + iNodeID +
                     * " and termposition > 0 ; com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB( sSQLDeleteOldSig , dbc, false, -1);
                     */
                }
                iTermPosCounter = 1;
                iMaxCounter = -1;
                iNodeIDLast = iNodeID;
                hsTitleTerms = insertNodeTitleToNewSigStructure(iNodeID, -1, dbc);

            }

            if (!hsTitleTerms.contains(sSigWord)) {
                int iTermID = com.indraweb.ir.TermHelper.addTerm(sSigWord, api.multilingual.TSTranslateCorpus.iLANGID_English, -1, dbc);
                String sSQLInsertSig = "insert into Signaturenew (NODEID, TERMID, TERMPOSITION, SIGNATUREOCCURENCES) "
                        + " values (" + iNodeID + "," + iTermID + "," + iTermPosCounter + "," + iSigCount + ")";
                com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLInsertSig, dbc, false, -1);
                iTermPosCounter++;
            } else {
                //System.out.println("eliminating dup iNodeID [" + iNodeID+ "] sSigWord[" + sSigWord + "]" );
            }
        }
        dbc.commit();
        System.out.println("completed [" + iLoop + "] source sig terms across [" + iNodeCount + "] nodes inserts for corpus [" + iCorpusID + "]");



    }

    private static void updateNodeTitleCountWords(int iNodeID, int iCount, Connection dbc)
            throws Exception {
        String sSQLUpdateTitleCounts = "update Signaturenew set SIGNATUREOCCURENCES = " + iCount
                + " where nodeid = " + iNodeID + " and TERMPOSITION < 0";
        //" values ("+iNodeID+","+iTermID+","+(iTitlePos+i)+","+iCount+ ")";
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLUpdateTitleCounts, dbc, false, -1);
    }

    private static HashSet insertNodeTitleToNewSigStructure(int iNodeID, int iCount, Connection dbc)
            throws Exception {
        HashSet hsreturn = new HashSet();
        String sNodeTitle = DBAccess_Central_connection.getNodeTitleSimple_NoAncestry((long) iNodeID, dbc);
        String[] sArrNodeTitleCleanedStopped = com.indraweb.util.UtilTitleAndSigStringHandlers.convertTitleToSigStyleSArr(sNodeTitle, false, ParseParms.getDelimitersAll_Default());
        // first emit title info all single words

        String sSigTermWordWithinTitle = null;
        // start title positions down low negatives
        int iTitlePos = (0 - sArrNodeTitleCleanedStopped.length);
        for (int i = 0; i < sArrNodeTitleCleanedStopped.length; i++) {
            hsreturn.add(sArrNodeTitleCleanedStopped[i]);
            int iTermID = com.indraweb.ir.TermHelper.addTerm(sArrNodeTitleCleanedStopped[i], api.multilingual.TSTranslateCorpus.iLANGID_English, -1, dbc);
            String sSQLInsertSig = "insert into Signaturenew (NODEID, TERMID, TERMPOSITION, SIGNATUREOCCURENCES) "
                    + " values (" + iNodeID + "," + iTermID + "," + (iTitlePos + i) + "," + iCount + ")";
            com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLInsertSig, dbc, false, -1);
        }
        return hsreturn;
    }

    public static void testOracleMultilingual(Connection dbc) throws Throwable {
        int iStrLenPre = 256;
        char[] cArr = new char[iStrLenPre];

        for (int i = 0; i < iStrLenPre; i++) {
            cArr[i] = (char) (i + 255);
        }
        String sChars = new String(cArr);
        api.Log.Log("sChars pre convert [" + sChars + "]");
        sChars = com.indraweb.util.UtilStrings.replaceStrInStr(sChars, "'", "''");
        api.Log.Log("sChars post convert [" + sChars + "]");
        String sSQL = " insert into Classifier (ClassifierName, ColName, ClassPath, SortOrder, Active) "
                + " values ('hktest', 'hktest', '" + sChars + "', " + " 0, 0)";


        api.Log.Log("insert sSQL [" + sSQL + "]");
        Statement stmtCAcheClear = dbc.createStatement();
        stmtCAcheClear.executeUpdate(sSQL);
        dbc.commit();
        stmtCAcheClear.close();

        String sSQLTest = "select ClassPath from Classifier where ClassifierName = 'hktest'";
        api.Log.Log("test select sSQL [" + sSQL + "]");
        String sCharsTest = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLTest, dbc);
        api.Log.Log("sCharsTest from the db [" + sCharsTest + "]");
        api.Log.Log("sCharsTest from the db [" + sCharsTest + "]");

        char[] cArrReturned = new char[256];


        for (int i = 0; i < 256; i++) {
            cArr[i] = (char) (i + 255);
        }


    }

    public static Vector localExecuteQueryAgainstDBVector(String aSQL, Connection dbc) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            if (dbc == null) {
                Log.LogError("ExecuteQueryAgainstDBVector do not want null");
            }
            //dbc = new IndraDBContext( JDBCIndra.dbNameMain ); }

            stmt = dbc.createStatement();
            rs = stmt.executeQuery(aSQL);
            Vector outVecReturnData = new Vector();
            long l = -1;

            ResultSetMetaData rsMD = rs.getMetaData();

            while (rs.next()) {
                Vector vToAdd = new Vector();
                for (int i = 0; i < rsMD.getColumnCount(); i++) {
                    vToAdd.addElement(rs.getObject(i + 1));
                }
                outVecReturnData.addElement(vToAdd);

            }
            return outVecReturnData;
        } catch (Exception e) {
            Log.LogError("error in ExecuteQueryAgainstDBVector [" + aSQL + "]", e);
        } catch (Throwable e) {
            Log.LogError("error throwable [" + aSQL + "]");
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        return null;

    }	 // executeQueryAgainstDBVector ...
}
