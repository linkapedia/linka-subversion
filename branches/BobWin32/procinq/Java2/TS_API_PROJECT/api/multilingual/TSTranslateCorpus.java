package api.multilingual;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.ir.TermHelper;
import com.indraweb.ir.texttranslate.Data_TextTranslate;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Vector;

/**
 * TSTranslateCorpus is the API function for corpus language translation. Language translation means that a new signature set (including node titles) is created from an existing one.
 *
 * Be sure this is not confused with a TSMigrateCorpus call which moves data from old to new signature table structures in English only
 *
 * @author Henry Kon
 * @since 3.x
 */
public class TSTranslateCorpus {

    /**
     * Translates the term set of a corpus' signature set from any language to any target "to" language. <p> Looks to the LANGUAGETRANSLATOR table of the DB for the highest ranked classpath to callout
     * to for translation services. Signature rows are inserted in the target language.
     *
     * @param CorpusID the corpus to be translated
     * @param langfrom the source corpus language
     * @param langto the target corpus language
     * @return XML out has <RESULT>SUCCSES</RESULT>;
     * @since 3.x
     */
    // add an idrac document object
    public static final int iLANGID_Arabic = 1;
    public static final int iLANGID_English = 2;
    public static final int iLANGID_Bulgarian = 4;
    public static final int iLANGID_Catalan = 5;
    public static final int iLANGID_Chinese_Simplified = 6;
    public static final int iLANGID_Chinese_Traditional = 7;
    public static final int iLANGID_Croatian = 8;
    public static final int iLANGID_Czech = 9;
    public static final int iLANGID_Danish = 10;
    public static final int iLANGID_Dutch = 11;
    public static final int iLANGID_Estonian = 12;
    public static final int iLANGID_Finnish = 13;
    public static final int iLANGID_French = 14;
    public static final int iLANGID_German = 15;
    public static final int iLANGID_Greek = 16;
    public static final int iLANGID_Hebrew = 17;
    public static final int iLANGID_Hungarian = 18;
    public static final int iLANGID_Icelandic = 19;
    public static final int iLANGID_Indonesian = 20;
    public static final int iLANGID_Italian = 21;
    public static final int iLANGID_Japanese = 22;
    public static final int iLANGID_Korean = 23;
    public static final int iLANGID_Latvian = 24;
    public static final int iLANGID_Lithuanian = 25;
    public static final int iLANGID_Norwegian = 26;
    public static final int iLANGID_Polish = 27;
    public static final int iLANGID_Portuguese = 28;
    public static final int iLANGID_Romanian = 29;
    public static final int iLANGID_Russian = 30;
    public static final int iLANGID_Serbian = 31;
    public static final int iLANGID_Slovak = 32;
    public static final int iLANGID_Slovenian = 33;
    public static final int iLANGID_Spanish = 34;
    public static final int iLANGID_Swedish = 35;
    public static final int iLANGID_Turkish = 36;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        //String sKey = (String) props.get ("SKEY" , true);
        //User u = (User) com.indraweb.execution.Session.htUsers.get (sKey);

        try {
            // Ensure user is a member of the admin group
            // if (u == null)
            // {
            //     throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            // }

            System.out.println("security disabled in TSTranslateCorpus");
            // required fields:
            if ((String) props.get("langfrom") == null
                    || (String) props.get("corpusid") == null
                    || (String) props.get("langto") == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }

            int iCorpusID = props.getint("corpusid", true);
            int iLangFrom = props.getint("langfrom", true);
            int iLangTo = props.getint("langto", true);

            try {
                doTranslate(iCorpusID, iLangFrom, iLangTo, dbc, false);
            } catch (Throwable t) {
                Log.LogError("error in translate", t);
                throw new Exception("error in translate : " + t.getMessage());
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }

    public static void doTranslate(int iCorpusID, int iLangFrom, int iLangTo, Connection dbc, boolean bDebug) throws Throwable {
        com.indraweb.util.UtilFile.addLineToFile("c:/t4.txt", "\r\nstart xlate corpus [" + iCorpusID + "] lang [" + iLangFrom + "] to [" + iLangTo + "]\r\n");

        // step 1 - delete old sigs this corpus this target lang.
        String sSQLDelOldSigs = "delete from signaturenew where "
                + " nodeid in ( select n.nodeid from node n, term t, signaturenew s "
                + " where n.nodeid = s.nodeid and t.termid = s.termid and "
                + " corpusid = " + iCorpusID + " and languageid = " + iLangTo + ")";
//                " nodeid in ( select n.nodeid from node n where " +
//                " corpusid = " + iCorpusID + ")" ;
        System.out.println("sSQLDelOldSigs[" + sSQLDelOldSigs + "]");
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLDelOldSigs, dbc, true, -1);
        // translate all terms in corpus to target language
        //get all terms from the sig table for this node
        String sSQL = "select n.nodeid, term, termposition, signatureoccurences from node n, signaturenew s, term t where "
                + " s.nodeid = n.nodeid and  t.termid = s.termid and n.corpusid = " + iCorpusID
                + " and rownum < 1000 and LANGUAGEID = " + iLangFrom;

        System.out.println("sSQL [" + sSQL + "]");
        System.out.println("ROWNUM LIMITED!!!!");

        Vector vRowsTermSigData =
                TSMigrateCorpusLanguageData.localExecuteQueryAgainstDBVector(sSQL, dbc);
        Enumeration eRowsTermSigData = vRowsTermSigData.elements();

        int iLoop = -1;
        String sClasspathTranslator = Data_TextTranslate.getClasspathOfTranslator(
                iLangFrom, iLangTo, 1, dbc);
        if (sClasspathTranslator == null) {
            throw new Exception("no language translator defined for language ids to and from : "
                    + iLangFrom + "->" + iLangTo);
        }

        System.out.println("sClasspathTranslator [" + sClasspathTranslator + "]");

        Class classTarget = Class.forName(sClasspathTranslator);
        Method method = getInvokeMethod(classTarget, sClasspathTranslator);
        while (eRowsTermSigData.hasMoreElements()) {
            iLoop++;
//            Object O = eRowsTermSigData.nextElement ();
            Vector vRow = (Vector) eRowsTermSigData.nextElement();
            Integer INodeID = new Integer(((BigDecimal) vRow.elementAt(0)).intValue());
            String sTerm = (String) vRow.elementAt(1);
            Integer ITermPosition = new Integer(((BigDecimal) vRow.elementAt(2)).intValue());

            Object[] oArr = new Object[3];
            int iO = 0;
            oArr[iO++] = sTerm;
            oArr[iO++] = new Integer(iLangFrom);
            oArr[iO++] = new Integer(iLangTo);

            String sTermTranslated = (String) method.invoke(classTarget, oArr);
            int iTermID = TermHelper.addTerm(sTermTranslated, iLangTo, iLangFrom, dbc);

            String sSQLInsertSig = "insert into Signaturenew (NODEID, TERMID, TERMPOSITION, SIGNATUREOCCURENCES) "
                    + " values (" + INodeID + "," + iTermID + "," + ITermPosition + "," + iCorpusID + ")";
            com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLInsertSig, dbc, false, -1);


            /*
             * String sLangName = Data_TextTranslate.getLangName (1 , dbc); System.out.println ("sLangName [" + sLangName + "]"); DataTables.emptyCacheOfQueryAndData
             * (Data_TextTranslate.sQueryLANGUAGE);
             */

            /*
             * TextTranslator_BabelFish termTranslate_Babelfish = new TextTranslator_BabelFish (); String sTerm = "nuclear"; String sTranslated = termTranslate_Babelfish.translateText (sTerm , 1 ,
             * 15);
             */
            //System.out.println ("sTerm [" + sTerm + "] sTranslated  [" + sTranslated + "]");


        }


    }

    public static void testOracleMultilingual(Connection dbc) throws Throwable {
        int iStrLenPre = 256;
        char[] cArr = new char[iStrLenPre];

        for (int i = 0; i < iStrLenPre; i++) {
            cArr[i] = (char) (i + 255);
        }
        String sChars = new String(cArr);
        api.Log.Log("sChars pre convert [" + sChars + "]");
        sChars = com.indraweb.util.UtilStrings.replaceStrInStr(sChars, "'", "''");
        api.Log.Log("sChars post convert [" + sChars + "]");
        String sSQL = " insert into Classifier (ClassifierName, ColName, ClassPath, SortOrder, Active) "
                + " values ('hktest', 'hktest', '" + sChars + "', " + " 0, 0)";


        api.Log.Log("insert sSQL [" + sSQL + "]");
        Statement stmtCAcheClear = dbc.createStatement();
        stmtCAcheClear.executeUpdate(sSQL);
        dbc.commit();
        stmtCAcheClear.close();

        String sSQLTest = "select ClassPath from Classifier where ClassifierName = 'hktest'";
        api.Log.Log("test select sSQL [" + sSQL + "]");
        String sCharsTest = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLTest, dbc);
        api.Log.Log("sCharsTest from the db [" + sCharsTest + "]");
        api.Log.Log("sCharsTest from the db [" + sCharsTest + "]");

        char[] cArrReturned = new char[256];


        for (int i = 0; i < 256; i++) {
            cArr[i] = (char) (i + 255);
        }


    }

    private static Method getInvokeMethod(Class c, String sClasspath) throws Throwable {
        Class[] classArrParmsThisClasspath = null;
        Method method = null;

        Class c1 = Class.forName("java.lang.String");
        Class c2 = Class.forName("java.lang.Integer");
        Class c3 = Class.forName("java.lang.Integer");

        classArrParmsThisClasspath = new Class[3];
        classArrParmsThisClasspath[0] = c1;
        classArrParmsThisClasspath[1] = c2;
        classArrParmsThisClasspath[2] = c3;

        try {
            return c.getMethod("translateText", classArrParmsThisClasspath);
        } catch (Throwable t) {
            throw t;
        }

    }
}
