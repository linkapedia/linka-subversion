package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Retrieve the properties of a repository given its unique identifier.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique repository identifier of the target repository.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSGetRepositoryProps&SKEY=-132981656&RID=7

 *	@return	a series of REPOSITORY objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <REPOSITORY>
          <REPOSITORYID>1</REPOSITORYID>
          <REPOSITORYTYPE>ROC Analysis</REPOSITORYTYPE>
          <REPOSITORYNAME>ROC Analysis</REPOSITORYNAME>
          <REPOSITORYLOCATION>/nike</REPOSITORYLOCATION>
          <USERNAME>null</USERNAME>
          <PASSWORD>null</PASSWORD>
          <FULLTEXT>1</FULLTEXT>
      </REPOSITORY>
  </TSRESULT>
  \endverbatim
 */
public class TSGetRepositoryProps
{
	public static void handleTSapiRequest ( 
										   api.APIProps props, 
										   PrintWriter out, 
										   Connection dbc )
		throws Exception
	{

		String sID = (String) props.get ("RID", true);

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = " select R.RepositoryTypeId, R.RepositoryName, R.RepositoryLoc, "+ 
						  " R.Username, R.Password, R.FullText, T.RepositoryClassName "+			
						  " from Repository R, RepositoryType T where R.RepositoryID = "+sID+
						  " and T.RepositoryTypeId = R.RepositoryTypeId";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				int iRepositoryType = rs.getInt(1);
				String sRepositoryName = rs.getString(2);
				String sRepositoryLoc = rs.getString(3);
				String sUsername = rs.getString(4);
				String sPassword = rs.getString(5);
				int iFullText = rs.getInt(6);
				String sClass = rs.getString(7);

				loop = loop + 1;
				out.println (" <REPOSITORY>");
				out.println ("   <REPOSITORYID>"+sID+"</REPOSITORYID>");
				out.println ("   <REPOSITORYNAME>"+sRepositoryName+"</REPOSITORYNAME>");
				out.println ("   <REPOSITORYTYPE>"+iRepositoryType+"</REPOSITORYTYPE>");
				out.println ("   <REPOSITORYCLASS>"+sClass+"</REPOSITORYCLASS>");
				out.println ("   <REPOSITORYLOCATION>"+sRepositoryLoc+"</REPOSITORYLOCATION>");
				out.println ("   <USERNAME>"+sUsername+"</USERNAME>");
				out.println ("   <PASSWORD>"+sPassword+"</PASSWORD>");
				out.println ("   <FULLTEXT>"+iFullText+"</FULLTEXT>");
				out.println ("</REPOSITORY>");
			}

			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Repository not found</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_CORPORA_THIS_DB);
			}
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
