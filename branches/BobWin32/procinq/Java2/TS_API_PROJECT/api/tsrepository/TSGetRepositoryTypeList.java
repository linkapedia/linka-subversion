package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Retrieve a list of available repository types in this server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSGetRepositoryTypeList&SKEY=-132981656

 *	@return	a series of REPOSITORYTYPE objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <TYPES>
        <TYPE>
            <REPOSITORYTYPEID>1</REPOSITORYTYPEID>
            <REPOSITORYTYPENAME>ROC Analysis</REPOSITORYTYPENAME>
            <REPOSITORYCLASSNAME>ROC</REPOSITORYCLASSNAME>
        </TYPE>
        <TYPE>
            <REPOSITORYTYPEID>2</REPOSITORYTYPEID>
            <REPOSITORYTYPENAME>Filesystem</REPOSITORYTYPENAME>
            <REPOSITORYCLASSNAME>Filesystem</REPOSITORYCLASSNAME>
        </TYPE>
...
  </TSRESULT>
  \endverbatim
 */
public class TSGetRepositoryTypeList
{
	public static void handleTSapiRequest ( 
										   api.APIProps props, 
										   PrintWriter out, 
										   Connection dbc )
		throws Exception
	{
		/*
		SQL> describe repository
		 Name                                      Null?    Type
		 ----------------------------------------- -------- ----------------------------
		 REPOSITORYID                              NOT NULL NUMBER(5)
		 REPOSITORYTYPEID                          NOT NULL NUMBER(5)
		 REPOSITORYNAME                            NOT NULL VARCHAR2(256)
		 REPOSITORYLOC                                      VARCHAR2(500)
		 USERNAME                                           VARCHAR2(50)
		 PASSWORD                                           VARCHAR2(50)
		 FULLTEXT                                           NUMBER(1)
		*/
		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = " select RepositoryTypeID, RepositoryTypeName, RepositoryClassName "+
						  " from RepositoryType ";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				String sID = rs.getString(1);
				String sName = rs.getString(2);
				String sClass = rs.getString(3);

				loop = loop + 1;
				if (loop == 1) { out.println("<TYPES>"); }
				out.println (" <TYPE>");
				out.println ("   <REPOSITORYTYPEID>"+sID+"</REPOSITORYTYPEID>");
				out.println ("   <REPOSITORYTYPENAME>"+sName+"</REPOSITORYTYPENAME>");
				out.println ("   <REPOSITORYCLASSNAME>"+sClass+"</REPOSITORYCLASSNAME>");
				out.println ("</TYPE>");
			}

			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Error: There are no repositories types defined in the database.</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}
			out.println("</TYPES>");
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
