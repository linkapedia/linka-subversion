package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Retrieve a list of available repositories in this server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSCreateRepository&SKEY=-132981656

 *	@return	a series of REPOSITORY objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <REPOSITORIES>
        <REPOSITORY>
            <REPOSITORYID>1</REPOSITORYID>
            <REPOSITORYTYPE>ROC Analysis</REPOSITORYTYPE>
            <REPOSITORYNAME>ROC Analysis</REPOSITORYNAME>
            <REPOSITORYLOCATION>/nike</REPOSITORYLOCATION>
            <USERNAME>null</USERNAME>
            <PASSWORD>null</PASSWORD>
            <FULLTEXT>1</FULLTEXT>
        </REPOSITORY>
        <REPOSITORY>
            <REPOSITORYID>2</REPOSITORYID>
            <REPOSITORYTYPE>ROC Analysis</REPOSITORYTYPE>
            <REPOSITORYNAME>ROC Analysis</REPOSITORYNAME>
            <REPOSITORYLOCATION>/nike</REPOSITORYLOCATION>
            <USERNAME>michale</USERNAME>
            <PASSWORD>null</PASSWORD>
            <FULLTEXT>0</FULLTEXT>
        </REPOSITORY>
...
  </TSRESULT>
  \endverbatim
 */
public class TSGetRepositoryList
{
	public static void handleTSapiRequest ( 
										   api.APIProps props, 
										   PrintWriter out, 
										   Connection dbc )
		throws Exception
	{
        try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = " select R.RepositoryID, T.RepositoryTypeName, R.RepositoryName, "+
						  " R.RepositoryLoc, R.Username, R.Password, R.Fulltext "+
						  " from Repository R, RepositoryType T where "+
						  " R.RepositoryTypeID = T.RepositoryTypeID";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				String sID = rs.getString(1);
				String sType = rs.getString(2);
				String sName = rs.getString(3);
				String sLoc = rs.getString(4);
				String sUsername = rs.getString(5);
				String sPassword = rs.getString(6);
				int iFull = rs.getInt(7);

				loop = loop + 1;
				if (loop == 1) { out.println("<REPOSITORIES>"); }
				out.println (" <REPOSITORY>");
				out.println ("   <REPOSITORYID>"+sID+"</REPOSITORYID>");
				out.println ("   <REPOSITORYTYPE>"+sType+"</REPOSITORYTYPE>");
				out.println ("   <REPOSITORYNAME>"+sName+"</REPOSITORYNAME>");
				out.println ("   <REPOSITORYLOCATION>"+sLoc+"</REPOSITORYLOCATION>");
				out.println ("   <USERNAME>"+sUsername+"</USERNAME>");
				out.println ("   <PASSWORD>"+sPassword+"</PASSWORD>");
				out.println ("   <FULLTEXT>"+iFull+"</FULLTEXT>");
				out.println ("</REPOSITORY>");
			}

			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>Error: There are no repositories.</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}
			out.println("</REPOSITORIES>");
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
