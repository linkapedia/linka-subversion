package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Set the date of a given watched folder.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique repository identifier of the target repository.
 *  @param  Path    Watched folder path target to be altered.
 *  @param  Date    Date of the folder being altered in the form: MM/DD/YYYY HH24:MI:SS
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSSetFolderDate&SKEY=-132981656&RID=1&Path=/src/docs/xxx&Date=01/15/2004+11:46:43

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <<SUCCESS>Watched folder updated successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSSetFolderDate
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sID = (String) props.get ("RID", true);
		String sPath = (String) props.get ("Path", true);
		String sDate = (String) props.get ("Date", true);

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = " update WatchFolder set DateLastCapture = "+
						  " to_date('"+sDate+"', 'MM/DD/YYYY HH24:MI:SS') "+
						  " where Path = '"+sPath+"' and RepositoryID = "+sID;
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Update to watched folder table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Watched folder updated successfully.</SUCCESS>");
		    stmt.close();
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
