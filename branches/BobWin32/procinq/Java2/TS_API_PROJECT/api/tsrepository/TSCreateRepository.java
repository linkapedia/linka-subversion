package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Create a new repository for this server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  Type    Unique repository type identifier indicating the type of repository being added.
 *  @param  Name    Name of this repository.
 *  @param  Location (optional) Location of this repository.
 *  @param  Username (optional) Username that is required to access this location, if applicable.
 *  @param  Password (optional) Password that is required to access this location, if applicable.
 *  @param  FullText (optional) Set to 1 if documents included in the full text index.  Default is 1.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSCreateRepository&SKEY=-132981656&Type=1&Name=Test

 *	@return	SUCCESS tag containing the new repository identifier, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>6</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSCreateRepository
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sType = (String) props.get ("Type", true);
		String sName = (String) props.get ("Name", true);
		String sLoc = (String) props.get ("Location", "");
		String sUsername = (String) props.get ("Username", "");
		String sPassword = (String) props.get ("Password", "");
		String sFullText = (String) props.get ("Full", "1");

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			int ID = GetNextAvailableRepositoryID(dbc);
			
			/*
			SQL> describe repository
			 Name                                      Null?    Type
			----------------------------------------- -------- ----------------------------
			 REPOSITORYID                              NOT NULL NUMBER(5)
			 REPOSITORYTYPEID                          NOT NULL NUMBER(5)
			 REPOSITORYNAME                            NOT NULL VARCHAR2(256)
			 REPOSITORYLOC                                      VARCHAR2(500)
			 USERNAME                                           VARCHAR2(50)
			 PASSWORD                                           VARCHAR2(50)
			 FULLTEXT                                           NUMBER(1)
			*/
			
			String sSQL = " insert into Repository values ("+ID+", "+sType+", "+
						  " '"+sName+"', '"+sLoc+"', '"+sUsername+"', '"+sPassword+"', "+
						  " "+sFullText+")";
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Create repository failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>"+ID+"</SUCCESS>");
		    stmt.close();
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
	public static int GetNextAvailableRepositoryID(Connection dbc)
		throws TSException
	{
			String sSQL = "select max(RepositoryID) from Repository"; 
				
			int iReturn = 0;
			Statement stmt = null;
			try {
				stmt = dbc.createStatement();	

				// If query statement failed, throw an exception
				ResultSet rs = stmt.executeQuery (sSQL);
				rs.next();
				iReturn = rs.getInt(1);
				stmt.close();
			}
			catch ( Exception e ) { return 1; }
			return iReturn+1;
	}	
}
