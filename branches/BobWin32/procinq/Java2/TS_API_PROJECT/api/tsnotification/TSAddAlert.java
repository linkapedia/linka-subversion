package api.tsnotification;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLDecoder;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Adds a CQL based alert into the system with the credentials of the current users login identifier.
 * These queries are periodically run on behalf of the user and email reports are generated from them.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  UserID  Unique user identifier of the user creating the alert.
 *  @param  Email   The email address where the user will receive the alert.  Note, this cannot be outside of the allowed domain specified in the user's profile.
 *  @param  Query   The CQL query that the user wants run on their behalf.
 *  @param  RunFreq (optional) Duration in days between alerts.  Default is -1, which indicates that the alert should never be run.
 *  @param  FromEmail (optional)    Allows the user to create alerts on behalf of another user.   If not specified, will default to the user's email address.
 *  @param  Notes (optional)    Additional information about the query in the user interface.
 *
 *  @note   http://ITSSERVER/itsapi/ts?fn=tsnotification.TSAddAlert&SKEY=-132981656&UserID=mhoey&email=mhoey@indraweb.com&query=SELECT%20<DOCUMENT>%20WHERE%20DOCTITLE=''anthrax''

 *	@return A success object is returned if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>Notification alert added successfully.</SUCCESS>
    </TSRESULT>
  \endverbatim
 */
public class TSAddAlert
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        String sName = (String) props.get ("Name");
		String sUserID = (String) props.get ("UserID", true);
        String sEmail = (String) props.get ("Email", true);
        String sQuery = (String) props.get ("Query", true);
        String sRunFreq = (String) props.get ("RunFreq", "-1");
        String sNotes = (String) props.get ("Notes", "");
        String sFromEmail = (String) props.get ("FromEmail", sEmail);
        String sID = (String) props.get("ID"); // optional ID to modify instead of add

        sQuery = URLDecoder.decode(sQuery, "UTF-8");

        // the following fields are optional and used by IDRAC
        String sLanguage = (String) props.get ("Language");
        String sRegion = (String) props.get ("Region");
        String sKeywords = (String) props.get ("Keywords");
        String sAbstract = (String) props.get ("Abstract");
        String sBibliography = (String) props.get ("Bibliography");
        String sTitle = (String) props.get ("Title");
        String sFullText = (String) props.get ("FullText");

        Statement stmt = null;

        if (sName == null) { sName = sQuery; }
        sNotes = sNotes.replaceAll("'", "''"); // fix apostraphies, if they exist

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            if (!u.DomainAuthorized(sEmail) && !u.IsMember(out)) {
                out.println("<ERROR>"+sEmail+" is not an authorized domain.</ERROR>");
                return;
            }

            // the e-mail address must be in the correct domain.  if it isn't

			String sSQL = "insert into Notification (AlertName, UserDN, CQL, Email, RunFrequency, LastRunDate, "+
                          "FromEmail, Notes, CreateDate";
            // if optional fields are here add them
            if (sLanguage != null) { sSQL = sSQL + ", IDRAClanguage"; }
            if (sRegion != null) { sSQL = sSQL + ", IDRACregion"; }
            if (sKeywords != null) { sSQL = sSQL + ", IDRACkeywords"; }
            if (sAbstract != null) { sSQL = sSQL + ", IDRACabstract"; }
            if (sBibliography != null) { sSQL = sSQL + ", IDRACbibliography"; }
            if (sTitle != null) { sSQL = sSQL + ", IDRACtitle"; }
            if (sFullText != null) { sSQL = sSQL + ", IDRACfulltext"; }

            sSQL = sSQL + ") values ('"+Clean(sName)+"', '"+sUserID+"', '"+Clean(sQuery)+"', '"+sEmail+
                          "', "+sRunFreq+", SYSDATE-30, '"+sFromEmail+"', '"+Clean(sNotes)+"', SYSDATE";

            // if optional fields are here add them
            if (sLanguage != null) { sSQL = sSQL + ", '"+sLanguage+"'"; }
            if (sRegion != null) { sSQL = sSQL + ", '"+sRegion+"'"; }
            if (sKeywords != null) { sSQL = sSQL + ", '"+Clean(sKeywords)+"'"; }
            if (sAbstract != null) { sSQL = sSQL + ", '"+Clean(sAbstract)+"'"; }
            if (sBibliography != null) { sSQL = sSQL + ", '"+Clean(sBibliography)+"'"; }
            if (sTitle != null) { sSQL = sSQL + ", '"+Clean(sTitle)+"'"; }
            if (sFullText != null) { sSQL = sSQL + ", '"+Clean(sFullText)+"'"; }

            sSQL = sSQL + ")";

            // if ID is there, change it to an update
            if (sID != null) {
                sSQL = "update Notification set RunFrequency = "+sRunFreq;
                if (!sNotes.equals("")) { sSQL = sSQL+", Notes = '"+sNotes+"'"; }
                sSQL = sSQL + " where AlertID = "+sID;
            }

            api.Log.Log("SQL: "+sSQL);

            stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Insert into notification table failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Notification alert added successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse )	 {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}

    public static String Clean (String s) {
        return s.replaceAll("&", "'||chr(38)||'");
        //return s.replaceAll("'", "''");
    }
}
