package api.tsnotification;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Change the run frequency of a previously saved alert.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ID  Unique identifier associated with this alert.
 *  @param  RunFreq Duration in days between alerts.  A value of -1 indicates that the alert should never be run.
 *
 *  @note   http://ITSSERVER/itsapi/ts?fn=tsnotification.TSEditAlert&SKEY=-132981656&ID=1&RunFreq=7

 *	@return A success object is returned if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>Notification alert changed successfully.</SUCCESS>
    </TSRESULT>
  \endverbatim
 */
public class TSEditAlert
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        // the only run frequency may be altered
		String sID = (String) props.get ("ID", true);
        String sRunFreq = (String) props.get ("RunFreq", true);

        Statement stmt = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = "update Notification set RunFrequency = "+sRunFreq+" where AlertID = "+sID;
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Notification table alteration failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Notification alert changed successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse )	 {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
