package api.tsnotification;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Remove an alert from the ITS system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ID  Unique identifier associated with this alert.
 *  @param  UserID  Unique user identifier of the user owning the alert.
 *  @param  Email   The email address where the user is receiving the alert.
 *
 *  @note   http://ITSSERVER/itsapi/ts?fn=tsnotification.TSRemoveAlert&SKEY=-132981656&ID=1&UserID=mhoey&Email=mhoey@indraweb.com

 *	@return A success object is returned if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>Notification alert deleted successfully.</SUCCESS>
    </TSRESULT>
  \endverbatim
 */
public class TSRemoveAlert
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        // all three must match up to successfully remove an alert
		String sID = (String) props.get ("ID", true);
        String sUserID = (String) props.get ("UserID", true);
        String sEmail = (String) props.get ("Email", true);

        Statement stmt = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            if (!u.DomainAuthorized(sEmail) && !u.IsMember(out)) {
                out.println("<ERROR>"+sEmail+" is not an authorized domain.</ERROR>");
                return;
            }

			String sSQL = "delete from Notification where AlertID = "+sID+" and "+
						  "(lower(UserDN) = '"+sUserID.toLowerCase()+"' or  "+
                          "lower(Email) = '"+sEmail.toLowerCase()+"')";
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from notification table failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Notification alert deleted successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse )	 {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
