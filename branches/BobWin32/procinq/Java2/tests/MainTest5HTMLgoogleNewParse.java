package com.iw.metasearch;

import com.indraweb.html.HTMLMemDoc;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.execution.Session;
import com.indraweb.util.UtilNet;
import com.indraweb.network.InputStreamsGen;

import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.ChangedCharSetException;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import java.io.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Vector;
import java.util.Properties;

public class MainTest5HTMLgoogleNewParse
{


    private static final int ISE_GOOGLE = 0;

    private static int iCallCounter = 0;

    public static void main (String[] args)
    {
        String[] sArrColNames = new String[50];

        try
        {
            Vector vFileLines = com.indraweb.util.UtilFile.getVecOfFileStrings ("c:/temp/temptab.txt");
            int iNumNonBlankResults = 0;
            for ( int i = 0 ; i < vFileLines.size () && i < 100; i++ )
            {
                String sLine = (String) vFileLines.elementAt (i);
                String[] sArrLineSplit = sLine.split ("\t");
                for ( int j = 0 ; j < sArrLineSplit.length ; j++ )
                {
                    if (i == 0)
                        sArrColNames[j] = sArrLineSplit[j];
                    else
                    {
                        String sValue = sArrLineSplit[j];
                        //System.out.println("record [" + i + "] col [" + j + "][" + sArrColNames[j] + "] value [" + sValue + "]" );
                    }

                }

                if (i == 0)
                    continue;
                // per person
                String sNameF = sArrLineSplit[5];
                String sNameL = sArrLineSplit[6];
                String sNameM = sArrLineSplit[7];
                String sNameStreet = sArrLineSplit[12];

                System.out.print ("\r\n\r\n " + i + " ========== google [" + sNameF + ":" + sNameL + ":" + sNameStreet + "]");
                String sAnswer = SEGoogle.getAnswer (sNameF , sNameM , sNameL , sNameStreet);
                if ( !sAnswer.trim().equals(""))
                {
                    iNumNonBlankResults++;
                    double dSuccess = (double) iNumNonBlankResults/ (double) i;
                    System.out.print ("SUCCESS, dSuccess rate [" + iNumNonBlankResults +
                        "] of [" + i + "] dSuccess [" + dSuccess + "]");
                }
                else
                {
                    double dSuccess = (double) iNumNonBlankResults/ (double) i;
                    System.out.print ("FAIL, dSuccess rate [" + iNumNonBlankResults +
                        "] of [" + i + "] dSuccess [" + dSuccess + "]");
                }
            }

            //System.out.println ("start MainTest3Html");
            //System.out.println (" getText () [" + getText () + "]");
            //System.out.println ("done MainTest3Html");
        } catch ( Exception e )
        {
            e.printStackTrace ();
            e.printStackTrace ();
        }
    }

    public static String getText (String sURL) throws Exception
    {
        //yes URL url = new URL ("http://www.google.com");
        //no URL url = new URL ("http://www.google.com/search?num=100&hl=en&lr=lang_en&ie=UTF-8&oe=UTF-8&safe=off&q=patrick+mehr+lexington%2C+ma&btnG=Google+Search");
        //no URL url = new URL ("http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=twist&btnG=Google+Search");
        // yahoo yes URL url = new URL ("http://phone.people.yahoo.com/py/psPhoneSearch.py?srch=bas&D=1&FirstName=patrick&LastName=mehr&City=lexington&State=ma&searchFor=Telephone&Search=Search");
        // switch boartd
        //URL url = new URL ("http://www.switchboard.com/bin/cginbr.dll?ID=11640112&ABS=0&FUNC=MORE&TYPE=1008&MEM=1&L=kon&T=lexington&S=MA&QV=02F9A78755914A3FF5CF3203O01F77BE5B23CD43F78303203O03F92C3DB93CD43F1C303203");
/*
        URL url = new URL ("http://www.google.com/search?num=100&hl=en&lr=lang_en"+
         //"&ie=UTF-8&oe=UTF-8&safe=off&q=patrick+mehr+lexington%2C+ma&btnG=Google+Search");
         "&q=patrick+mehr+lexington%2C+ma");
*/
        //String sURL = "http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=patrick+mehr+lexington%2C+ma&btnG=Google%20Search";
        //String sURL = "http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=patrick+mehr+lexington%2C+ma";
        //String sURL = "http://phone.people.yahoo.com/py/psPhoneSearch.py?srch=bas&D=1&FirstName=patrick&LastName=mehr&City=lexington&State=ma&searchFor=Telephone&Search=Search";
        URL url = new URL (sURL);

        UrlHtmlAttributes urlhrmlAttr = new UrlHtmlAttributes (sURL);
        InputStream is = InputStreamsGen.timedSocketConnect (url , urlhrmlAttr.getsURL () , 500000);

        BufferedReader reader = new BufferedReader (new InputStreamReader (is , "8859_1"));

        StringBuffer buffer = new StringBuffer ();

        int read;

        while (( read = reader.read () ) != -1)
        {
            buffer.append ((char) read);
        }

        //String html = "<html><body>Hello & Bye<BR>test</BR><br> ...</br><BR>Google.co.in offered in: software</BR><BR>line   jjjj</BR><BR> manoj & &lt; test</BR></body></html>";
        String sHTML = buffer.toString ();


        final StringBuffer sbBuf = new StringBuffer (100000);
        InputStreamReader isRead = null;
        //File ficle = null;
        HTMLDocument htmlDoc = null;
        HTMLEditorKit htmlEditorKit = null;
        StringReader sr = null;
        try
        {
            iCallCounter = 0;
            htmlDoc = new HTMLDocument ()
            {
                public HTMLEditorKit.ParserCallback getReader (int pos)
                {
                    return new HTMLEditorKit.ParserCallback ()
                    {
                        // This method is whenever text is encountered in the HTML file
                        public void handleText (char[] cArrData , int iPos)
                        {
                            iCallCounter++;
                            String sLine = new String (cArrData);
                            if ( sLine.indexOf("(781)") >= 0)

                            {
                                System.out.println (iCallCounter +
                                    ":" + iPos +
                                    " (781)result [" + sLine + "]");
                                sbBuf.append (cArrData);
                                sbBuf.append ('\n');
                            }
                        }


                        public void flush () throws BadLocationException
                        {
                        }

                        public void handleComment (char[] data , int pos)
                        {
                        }

                        public void handleStartTag (HTML.Tag tag , MutableAttributeSet a , int pos)
                        {
/*
                            System.out.println ("iCallCounter [" + iCallCounter +
                                    "] tag.toString()  [" + tag.toString () +
                                    "] a.toString()  [" + a.toString () + "]"
                            );
*/
                        }

                        public void handleEndTag (HTML.Tag t , int pos)
                        {
                        }

                        public void handleSimpleTag (HTML.Tag t , MutableAttributeSet a , int pos)
                        {
                        }

                        public void handleError (String errorMsg , int pos)
                        {
                        }

                        public void handleEndOfLineString (String eol)
                        {
                        }

                    };
                }
            };

// Create a reader on the HTML content

// Parse the HTML

            sr = new StringReader (sHTML);
            htmlEditorKit = new HTMLEditorKit ();
            htmlDoc.putProperty ("IgnoreCharsetDirective" , new Boolean (true));

            htmlEditorKit.read (sr , htmlDoc , 0);
        } catch ( ChangedCharSetException e1 )
        {

            System.out.println ("Exception " + e1);
            sr.close ();
// reload html
            sr = new StringReader (sHTML);
// remove any loaded stuff from document
            try
            {
                htmlDoc.remove (0 , htmlDoc.getLength ());
            } catch ( BadLocationException e )
            {
            }
// ignore charset
            htmlDoc.putProperty ("IgnoreCharsetDirective" , new Boolean (true));
// retry load
            try
            {
                htmlEditorKit.read (sr , htmlDoc , 0);
            } catch ( BadLocationException e2 )
            {


            } catch ( Exception e )
            {
            }


        } catch ( Exception e )
        {
            System.out.println (e);
        }




// Return the text
        return sbBuf.toString ();
    }


    private static class SEGoogle
    {
        // http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=patrick+mehr+lexington%2C+ma&btnG=Google%20Search
        protected static String sURLPre = "http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=";
        protected static String sURLPost1 = "+lexington%2C+ma";
        protected static String sURLPost2 = "&btnG=Google%20Search";

        public static String getAnswer (String sNameF , String sNameM , String sNameL , String sNameStreet)
                throws Exception
        {
            String sURL = sURLPre + getMiddle (sNameF , sNameL) + sURLPost1 + sURLPost2;
            System.out.println (" sURL [" + sURL + "]");
            return getText (sURL);
        }

        static String getMiddle (String sNameF , String sNameL)
        {
            return sNameF + "+" + sNameL;


        }


    }
}
