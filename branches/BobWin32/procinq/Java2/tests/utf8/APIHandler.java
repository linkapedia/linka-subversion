package api;

import java.io.*;
import java.util.Properties;
import java.util.Enumeration;
import java.util.Hashtable;
import java.sql.Connection;
import java.lang.reflect.Method;

import api.security.*;
import api.Log;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;

import javax.servlet.*;
import javax.servlet.http.*;

public class APIHandler
{
    private static String sServerUpSince = null;
    private static int iCallCounter = 0;
    private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on
    private static Hashtable htPrintWriters = new Hashtable();
    private static Hashtable htPropsByThread = new Hashtable();
    private static Hashtable htBooleanEmittingXML = new Hashtable();
    private static boolean bDebugging = false;

    public static PrintWriter getout()
    {
        Thread t = Thread.currentThread();
        return (PrintWriter) htPrintWriters.get(t);
    }

    public static APIProps getPropThisThread()
    {
        Thread t = Thread.currentThread();
        return (APIProps) htPropsByThread.get(t);
    }

    public static boolean getEmittingXML()
    {
        Thread t = Thread.currentThread();
        return ((Boolean) htBooleanEmittingXML.get(t)).booleanValue();
    }

    public static void setEmittingXML(boolean bXMLtrue)
    {
        Thread t = Thread.currentThread();
        htBooleanEmittingXML.put(t, new Boolean(bXMLtrue));
    }

    static boolean bGotServerUpSince = false;
    static String sXMLOutFileName_Trigger = null;
    static String sXMLOutFileName_ArgsYN = null;
    static String sXMLOutFileName_XML = null;

    public static void doGetMine(
            HttpServletRequest req,
            HttpServletResponse res,
            api.APIProps props,
            OutputStream out1,
            Connection dbc,
            boolean bXML) throws Exception
    {

/*
  public XmlWriter(OutputStream out, String encoding)
    throws java.io.UnsupportedEncodingException, java.io.IOException {
    this(new OutputStreamWriter(out, encoding));
  }
*/

        //out.
        PrintWriter out = new PrintWriter ( new OutputStreamWriter (out1, "UTF8"));

        boolean bVerbose = true; // careful - basic logging here
        // hbk timer com.indraweb.profiler.Timer timer = com.indraweb.profiler.Profiler.getTimer("method doGetMine", true);
        // hbk timer try
        // hbk timer {
        //System.out.println("in APIHandler.doGetMine() dbc = [" + dbc + "]");
        java.io.FileWriter fw = null;

        if (sXMLOutFileName_Trigger == null)
        {
            String sOSHost = (String) java.lang.System.getProperties().get("os.name");
            if (sOSHost.toLowerCase().indexOf("windows") >= 0)
            {
                sXMLOutFileName_XML = "c:/temp/xml.txt";
                sXMLOutFileName_Trigger = "c:/temp/IndraXMLToFile.txt";
                sXMLOutFileName_ArgsYN = "c:/temp/IndraXMLToFileArgs.txt";
            } else
            { // linux
                sXMLOutFileName_Trigger = "/temp/IndraXMLToFile.txt";
                sXMLOutFileName_XML = "/temp/xml.txt";
                sXMLOutFileName_ArgsYN = "/temp/IndraXMLToFileArgs.txt";
            }
        }

        if (com.indraweb.util.UtilFile.bFileExists(sXMLOutFileName_Trigger))
        {
            try
            {
                api.Log.Log("file [" + sXMLOutFileName_Trigger + "] present, streaming XML to [" + sXMLOutFileName_XML + "]\r\n");
                boolean bIncludeArgs = false;
                if (com.indraweb.util.UtilFile.bFileExists(sXMLOutFileName_ArgsYN))
                {
                    api.Log.Log("file [" + sXMLOutFileName_ArgsYN + "] present, including args in output\r\n");
                    bIncludeArgs = true;
                } else
                    api.Log.Log("file [" + sXMLOutFileName_ArgsYN + "] not present, not including args in output\r\n");

                out = new XMLOutRedirector(out, sXMLOutFileName_XML, true);
                if (bIncludeArgs)
                {
                    StringBuffer sbArgs = new StringBuffer();
                    Enumeration e = props.keys();
                    int i = 0;
                    while (e.hasMoreElements())
                    {
                        String sPropKey = (String) e.nextElement();
                        sbArgs.append("IndraXMLargs " + i + ". " + sPropKey + ":" + (String) props.get(sPropKey) + "\r\n");
                        i++;
                    }
                    api.Log.Log ("\r\n\r\nAPI CALL counter [" + iCallCounter + "] [" +
                            new java.util.Date() + "] \r\n--- ARGS : \r\n[\r\n" +
                            sbArgs.toString() + "]");
                }

            } catch (Exception e)
            {
                Log.LogError("error writing xml to XML out file", e);
            }
        }

        String sWhichDB = null;
        int icallCounterAtStart = iCallCounter;
        Thread t = Thread.currentThread();
        htPrintWriters.put(t, out);  // make accessible thruout the system for this thread
        htPropsByThread.put(t, props);

        if (bXML)
            htBooleanEmittingXML.put(t, new Boolean(true));
        else
            htBooleanEmittingXML.put(t, new Boolean(false));

        long lTimeStart = System.currentTimeMillis();

        String sFunction = null;

        try
        {
            sWhichDB = (String) props.get("whichdb", "API");
            //if (  != null )
            //throw new Exception ("reserved word used in URL submission : emitXML");
            if (bXML)
            {
                out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
                props.put("emitXML", "true");
            } else
            {
                props.put("emitXML", "false");
                out.println((new java.util.Date()).toString());
            }
            // guarantee no two api calls get the same counter even across threads
            synchronized (ICallCounterSemaphore)
            {
                iCallCounter++;
            }

            // STATIC INITS  hbk 2002 04 27
            if (!bGotServerUpSince)
            {
                bGotServerUpSince = true;
                sServerUpSince = (new java.util.Date()).toString();
            }

            // OUTPUT
            if (bXML)
            {
                out.println("<TSRESULT>");
                if (props.getbool("showdebug", "false"))
                {
                    Enumeration e = req.getHeaderNames();
                    while (e.hasMoreElements())
                    {
                        String sHeaderName = (String) e.nextElement();
                        out.println("<req_" + sHeaderName + ">" + req.getHeader(sHeaderName) + "</req_" + sHeaderName + ">");
                    }
                }
            } else
                out.println("<HTML>");
            sFunction = "api." + props.get("fn", true);

            String sDate = "<" + new java.util.Date().toString().substring(0, 19) + ">";
            if (((!sFunction.equals("api.security.TSLogin")) &&
                    (!sFunction.equals("api.security.TSGetConfigParams")) &&
                    (!sWhichDB.equals("SVR"))))
            {
                String sKey = (String) props.get("SKEY");
                if (sKey == null)
                {
                    Log.Log("****** API call REJECTED NO SESSION KEY [" + icallCounterAtStart + "] ***  [" + sFunction + "]");
                    out.println("<MISSINGSKEY>1</MISSINGSKEY>");
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
                } else
                {
                    User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
                    String sFn = (String) props.get("fn", true);

                    // allow functions through eg for debugging - without user/authentication and security
                    boolean bAllowFnThruNoSecurity = sFn.equals("tsother.TSTestWait");
                    // DEBUG
                    //if (u == null && !bAllowFnThruNoSecurity)
                    if (!bDebugging && u == null && !bAllowFnThruNoSecurity)
                    {
                        out.println("<SESSIONEXPIRED>1</SESSIONEXPIRED>");
                        Log.Log("****** API call REJECTED SESSION EXPIRED (" + sKey + ") [" + icallCounterAtStart + "] ***  [" + sFunction + "]");
                        Log.Log("****** Current size of SESSION queue: " + com.indraweb.execution.Session.htUsers.size() + " *****");
                        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
                    }

                    if (sFunction.equals("api.tsclassify.TSClassifyDoc"))
                    {
                        System.gc();
                        long lTotalmem = Runtime.getRuntime().totalMemory();
                        long lFreemem = Runtime.getRuntime().freeMemory();

                        if (u != null)
                            Log.Log(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] dburl ["+props.get("dburl")+"] user [" + u.GetUsername() + "." + sKey + "] [" + sFunction + ":" + props.get("url") + "] mem [" + lTotalmem + ":" + lFreemem + "]");
                        //Log.Log("****** API call desc [" + sCALLERDESCIfSpecified + "] N [" + sNODEIDIfSpecified + "] callcount [" + icallCounterAtStart + "] user [" + u.GetUsername() + "." + sKey + "] [" + sFunction + ":" + props.get("url") + "] mem [" + lTotalmem + ":" + lFreemem + "]");
                        else
                            Log.Log(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] dburl ["+props.get("dburl")+"]user [" + "no user" + "." + sKey + "] [" + sFunction + ":" + props.get("url") + "] mem [" + lTotalmem + ":" + lFreemem + "]");

                    } else
                    {
                        if (u != null)
                            Log.Log(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] user [" + u.GetUsername() + "." + sKey + "] [" + sFunction + "]");
                        else
                            Log.Log(Thread.currentThread().getName() + ": OPEN CALL [" + icallCounterAtStart + "] user [" + "no user" + "." + sKey + "] [" + sFunction + "]");
                    }
                }
            } else
                Log.Log("OPEN CALL [" + icallCounterAtStart + "] fn [" + sFunction + "]");

            // @todo eventually test caching and/or reusing the class and method objects below
            // create the set of class objects for passing into the API call as per java reflection
            Class cAPIProperties = Class.forName("api.APIProps");
            Class cPrintWriter = Class.forName("java.io.PrintWriter");
            Class cConnection = Class.forName("java.sql.Connection");
            //Class[] cArr = { cHttpServletRequest, cHttpServletResponse, cConnection };
            Class[] cArr = {cAPIProperties,
                            cPrintWriter,
                            cConnection};
            // create the (super)class against which to invoke the API method call
            Class cTSapi = null;
            try
            {
                //out.println("<CLASSNAME>"+sFunction+"</CLASSNAME>");
                cTSapi = Class.forName(sFunction);
            } catch (ClassNotFoundException cnfe)
            {
                //UtilFile.addLineToFile ("c:/t.t", sFunction);
                throw new TSNoSuchObjectException("name [" + sFunction + "]");
            }
            // api.tscorpus.TSGetCorpus.handleTSapiRequest (
            // *********************************************
            // first look for the class without the multipart
            // *********************************************
            Method method = null;
            //classify needed an additional patm - this technique allows multple invoked method sigs
            int iTRADITIONAL_NON_CLASSIFY = 1;
            int iCLASSIFYWITHREQUESTREQUIRED = 2;
            int iTSMETASEARCH = 3;
            int iWhichParameterList = iTRADITIONAL_NON_CLASSIFY;
            try
            {
                method = cTSapi.getMethod("handleTSapiRequest", cArr);
            } catch (NoSuchMethodException nsme)
            {
                String sFn = (String) props.get("fn");
                //if ( true || !sFn.equals("tsmetasearchnode.TSMetasearchNode"))
                Class cHttpServletRequest = Class.forName("javax.servlet.http.HttpServletRequest");
                Class cHttpServletResponse = Class.forName("javax.servlet.http.HttpServletResponse");
                //Class[] cArr = { cHttpServletRequest, cHttpServletResponse, cConnection };
                Class[] cArr2 = null;
                if (sFn.equals("tsmetasearchnode.TSMetasearchNode"))
                {
                    iWhichParameterList = iTSMETASEARCH;
                    cArr2 = new Class[4];
                    cArr2[0] = cHttpServletRequest;
                    cArr2[1] = cHttpServletResponse;
                    cArr2[2] = cAPIProperties;
                    cArr2[3] = cPrintWriter;
                } else // assume
                {
                    // *********************************************
                    // else  look for the class with the multipart
                    // *********************************************
                    //Class[] cArr = { cHttpServletRequest, cHttpServletResponse, cConnection };
                    iWhichParameterList = iCLASSIFYWITHREQUESTREQUIRED;
                    cArr2 = new Class[5];
                    cArr2[0] = cHttpServletRequest;
                    cArr2[1] = cHttpServletResponse;
                    cArr2[2] = cAPIProperties;
                    cArr2[3] = cPrintWriter;
                    cArr2[4] = cConnection;
                    // create the (super)class against which to invoke the API method call
                }


                try
                {
                    method = cTSapi.getMethod("handleTSapiRequest", cArr2);
                } catch (NoSuchMethodException nsme2)
                {
                    out.println("<DEBUG>no such method - even tried multipart </DEBUG>");
                    Log.LogError("no such method [" + sFunction + "]", nsme2);
                    com.indraweb.util.Log.NonFatalError("no such method [" + sFunction + "]", nsme2);
                    throw new TSNoSuchMethodException("no such method on invoke object [" + sFunction + "]");
                }

            }

            try
            {
                if (iWhichParameterList == iTRADITIONAL_NON_CLASSIFY)
                {
                    Object[] oArr = {props, out, dbc};
                    method.invoke(cTSapi, oArr);  // invoke any of the API functions
                } else if (iWhichParameterList == iCLASSIFYWITHREQUESTREQUIRED)
                {
                    //System.out.println("running  case 2 iWhichParameterList to [" + iTSMETASEARCH + "]");
                    Object[] oArr = {req, res, props, out, dbc};
                    method.invoke(cTSapi, oArr);  // invoke any of the API functions
                } else if (iWhichParameterList == iTSMETASEARCH)
                {
                    //System.out.println("running  case 3 iWhichParameterList to [" + iTSMETASEARCH + "]");
                    Object[] oArr = {req, res, props, out};
                    method.invoke(cTSapi, oArr);  // invoke any of the API functions
                } else
                {
                    throw new Exception("unknown iWhichParameterList value [" + iWhichParameterList + "]");
                }
                //System.out.println("back from call out ");
            } catch (java.lang.reflect.InvocationTargetException ite)
            {
                Log.LogError("EXCEPTION on back from call out", ite);
                System.out.println("EXCEPTION on back from call out ");
                Throwable t2 = ite.getTargetException();
                out.println("<INTERNALERRMSG>" + t2.getMessage() + "</INTERNALERRMSG>");
                String strace = Log.stackTraceToString(t2);
                out.println("<INTERNALSTACKTRACE>" + strace + "</INTERNALSTACKTRACE>");
            } catch (Exception e)
            {
                if (dbc != null)
                    dbc = api.statics.DBConnectionJX.freeConnection(dbc, sWhichDB, "APIHandler.doGetMine() exception");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUTSIDE_INVOKED_METHOD,
                        "outside error message [" + e.getMessage() + "]");
            }

//            try {
//                if ( dbc != null )
//                    dbc = api.statics.DBConnectionJX.freeConnection(dbc, sWhichDB, "APIHandler.doGetMine()" );
//            } catch (Exception e) {
//                throw e;
//            }


        } catch (TSDBException dbe)
        {
            EmitGenXML_ErrorInfo.emitException("", dbe, out);
        } catch (TSNoSuchObjectException nsoe)
        {
            EmitGenXML_ErrorInfo.emitException("", nsoe, out);
        } catch (TSNoSuchMethodException nsme)
        {
            EmitGenXML_ErrorInfo.emitException("", nsme, out);
        } catch (TSException tse)
        {
            EmitGenXML_ErrorInfo.emitException("", tse, out);
        } catch (Exception e)
        {
            out.println(EmitGenXML_ErrorInfo.getsXML_JavaException("Exception", e));
        } finally
        {
/*
            if ( dbc != null ) {
                try {
                    dbc = api.statics.DBConnectionJX.freeConnection(dbc, sWhichDB, "apihandler finally");
                } catch (Exception e) {
                    Log.LogFatal("Error freeing db connection in DB : " + sWhichDB, e);
                }
            }
*/
            if (bXML)
            {
                out.println("<CLASSLOAD>" + sServerUpSince + "</CLASSLOAD>");
                out.println("<CALLCOUNT>" + icallCounterAtStart + "</CALLCOUNT>");
            }
            Enumeration eNumParms = props.keys();
            int iParmCount = 1;
            long lTime = (System.currentTimeMillis() - lTimeStart);
            Log.Log("DONE CALL  [" + icallCounterAtStart + "] [" + sFunction + "] *** " +
                    lTime + " ms");
            if (bXML)
            {
                //while ( eNumParms.hasMoreElements() )
                //{
                //	String sParm = (String) eNumParms.nextElement();
                //	String sValue = (String) props.get( sParm );
                //out.println ("<QUERYPARM>" + iParmCount++ + ". " + sParm+"="+sValue + "</QUERYPARM>");
                //}

                out.println("<TIMEOFCALL_MS>" + (lTime) + "</TIMEOFCALL_MS>");
                out.println("</TSRESULT>");
            } else
            {
                out.println("<BR>Call Duration (ms) : " + (lTime));
                out.println("</HTML>");
            }

            try
            {
                out.flush();
                out.close();
            } catch (Exception e)
            {
                Log.LogError("error closing xml txt file stream", e);
            }

        }
        // hbk timer }
        //finally // // hbk timer
        //{
        //timer.stop();
        //if ( UtilFile.bFileExists ("c:/temp/temphk2.txt")) // hbk control
        //{
        //com.indraweb.profiler.Profiler.printReport(); // hbk control - remove timers
        //com.indraweb.profiler.Profiler.removeAllTimers();
        //}
        //}
    } // public void doGetMine ( Properties props, PrintWriter out)
}
