
import java.awt.Color;
import java.awt.Dimension;
import java.text.NumberFormat;
import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.LineAndShapeRenderer;
//import org.jfree.chart.title.TextTitle;
import org.jfree.data.*;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.util.SortOrder;

public class ParetoChartDemo extends ApplicationFrame
{

    public ParetoChartDemo(String s)
    {
        super(s);
        DefaultKeyedValues defaultkeyedvalues = new DefaultKeyedValues();
        defaultkeyedvalues.addValue("1-2", new Integer(-63));
        defaultkeyedvalues.addValue("2-3", new Integer(-25));
        defaultkeyedvalues.addValue("3-4", new Integer(-13));
        defaultkeyedvalues.addValue("4-5", new Integer(-7));
        defaultkeyedvalues.addValue("5-6", new Integer(-1));
        defaultkeyedvalues.addValue("6-7", new Integer(3));
        defaultkeyedvalues.addValue("7-8", new Integer(5));
        defaultkeyedvalues.addValue("8-9", new Integer(8));
        defaultkeyedvalues.addValue("9-10", new Float (9.5 ));
        defaultkeyedvalues.addValue("10+", new Float(12));
/*
        defaultkeyedvalues.addValue("11-12", new Float(10.1));
        defaultkeyedvalues.addValue("12-13", new Float(11.5));
        defaultkeyedvalues.addValue("13-14", new Float(12.5));
        defaultkeyedvalues.addValue("14-15", new Float(13.5));
        defaultkeyedvalues.addValue("15+", new Float(17));
*/
        // defaultkeyedvalues.sortByValues(SortOrder.DESCENDING);

        //org.jfree.data.KeyedValues keyedvalues = DataUtilities.getCumulativePercentages(defaultkeyedvalues);
        DefaultKeyedValues keyedvalues = new DefaultKeyedValues();
            keyedvalues.addValue("1-2", new Float(  0.001143511 ));
        keyedvalues.addValue("2-3", new Float(  0.021154946 ));
        keyedvalues.addValue("3-4", new Float(  0.222412807 ));
        keyedvalues.addValue("4-5", new Float(  0.496855346 ));
        keyedvalues.addValue("5-6", new Float(  0.668381933 ));
        keyedvalues.addValue("6-7", new Float(  0.785591767 ));
        keyedvalues.addValue("7-8", new Float(  0.877072613 ));
        keyedvalues.addValue("8-9", new Float(  0.923956547 ));
        keyedvalues.addValue("9-10", new Float( 0.949685535 ));
        keyedvalues.addValue("10+", new Float( 1.0));
/*
        keyedvalues.addValue("10-11", new Float(0.967981704 ));
        keyedvalues.addValue("11-12", new Float(0.98056032  ));
        keyedvalues.addValue("12-13", new Float(0.987421384 ));
        keyedvalues.addValue("13-14", new Float(0.991995426 ));
        keyedvalues.addValue("14-15", new Float(0.993138937 ));
        keyedvalues.addValue("15+", new Float(  1           ));
*/






        org.jfree.data.CategoryDataset categorydataset = DatasetUtilities.createCategoryDataset("% change", defaultkeyedvalues);
        JFreeChart jfreechart = ChartFactory.createBarChart("Propert tax change after assessment exemption of $100K", "2003 Property Tax ($1000's)",
                "% Tax change", categorydataset, PlotOrientation.VERTICAL, true, true, false);
        jfreechart.addSubtitle(new TextTitle("By assessed value as of Jan 2004"));
        //jfreechart.addSubtitle(new TextTitle("As of January 2004"));
        jfreechart.setBackgroundPaint(new Color(0xbbbbdd));
        CategoryPlot categoryplot = jfreechart.getCategoryPlot();
        CategoryAxis categoryaxis = categoryplot.getDomainAxis();
        categoryaxis.setLowerMargin(0.02D);
        categoryaxis.setUpperMargin(0.02D);
        NumberAxis numberaxis = (NumberAxis)categoryplot.getRangeAxis();
        numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        LineAndShapeRenderer lineandshaperenderer = new LineAndShapeRenderer();
        org.jfree.data.CategoryDataset categorydataset1 = DatasetUtilities.createCategoryDataset(" % homes", keyedvalues);
        NumberAxis numberaxis1 = new NumberAxis("Cum. % homes");
        numberaxis1.setNumberFormatOverride(NumberFormat.getPercentInstance());
        categoryplot.setSecondaryRangeAxis(0, numberaxis1);
        categoryplot.setSecondaryDataset(0, categorydataset1);
        categoryplot.setSecondaryRenderer(0, lineandshaperenderer);
        categoryplot.mapSecondaryDatasetToRangeAxis(0, new Integer(0));
        categoryplot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
        ChartPanel chartpanel = new ChartPanel(jfreechart);
        chartpanel.setPreferredSize(new Dimension(550, 270));
        setContentPane(chartpanel);
    }

    public static void main(String args[])
    {
        ParetoChartDemo paretochartdemo = new ParetoChartDemo("");
        paretochartdemo.pack();
        RefineryUtilities.centerFrameOnScreen(paretochartdemo);
        paretochartdemo.setVisible(true);
    }
}