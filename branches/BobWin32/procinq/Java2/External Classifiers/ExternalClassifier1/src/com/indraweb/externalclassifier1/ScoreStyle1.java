package com.indraweb.externalclassifier1;

import com.indraweb.encyclopedia.DocForScore;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import com.iw.bo.MustHaveBO;
import com.iw.bo.interfaces.IMustHaveBO;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.iw.scoring.NodeForScore;
import com.iw.scoring.scoring2.ScoreExplainData;
import com.iw.scoring.scoring2.ScoresBetweenTwoTextObjects;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.log4j.Logger;

/**
 * ScoreStyle1 is an external classifier supplied as default with the CDMS. An external classifier must implement the public static void genScores method. This method is invoked through reflection
 * during classification.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 */
public class ScoreStyle1 {

    private static final Logger log = Logger.getLogger(ScoreStyle1.class);
    public static final Integer ISCORE1_VECTORDOTPRODUCT = new Integer(1);
    public static final Integer ISCORE3_COVERAGE = new Integer(3);
    public static final Integer ISCORE4_Score1TimesCorpusDocWords_Div132 = new Integer(4);
    public static final Integer ISCORE2001_s1xs3x10 = new Integer(2001);
    public static final String SSCORE1_VECTORDOTPRODUCT = "Freq";
    public static final String SSCORE3_COVERAGE = "Cov";
    public static final String SSCORE4_Score1TimesCorpusDocWords_Div132 = "FreqScaled";
    public static final String SSCORE7_FtimesC = "f*c";
    public static final String SSCORE7_FTimesCHat = "f*c^";  // .25
    public static final String SSCORE7_FTimesCHat_20 = "f*c^20";
    public static final String SSCORE7_FTimesCHat_15 = "f*c^15";
    private static double dMinForSSCORE7and8_FTimesCHat = .25;
    private static double dMinForSSCORE7and8_FTimesCHat_20 = .20;
    private static double dMinForSSCORE7and8_FTimesCHat_15 = .15;
    public static final String SSCORE8_FbTimesC = "fb*c";
    public static final String SSCORE8_FbTimesCHat = "fb*c^";
    public static final String SSCORE8_FbTimesCHat_20 = "fb*c^20";
    public static final String SSCORE8_FbTimesCHat_15 = "fb*c^15";
    public static long ltimeTotalInScore1 = 0;
    public static long ltimeTotalInScore2 = 0;
    public static long ltimeTotalInScore3 = 0;
    public static long ltimeTotalInScoreagg = 0;
    private static final String SSCORE0 = "nodetitlematch";

    /*
     * 2004 03 26 - to print score contributions (SC) for a given node only public static boolean bVerbose_printScoreContribution = true; // more detail // hbk 2004 03 23 public static int
     * iNodeIdVerbose_printScoreContribution = 1401868; // more detail // hbk 2004 03 23
     */
    /**
     * Given the two text objects contained in ScoresBetweenTwoTextObjects, calculate the relevant scores and insert them into a hashtable (by name) <p> The hashtable may not be empty upon invocation
     * in that it may contain results of prior score generators. <p>
     *
     * @param sbtto Container of two text objects, the hashtable of scores, and score explain data if collected
     * @param BScoreExplain true means generate scoring explain information?
     * @param out http stream for in line (XML) comments etc.
     * @param BStemOn true means stem terms before scoring
     * @param BThesauruson true means use the thesaurus feature in scoring
     */
    public static void genScores(ScoresBetweenTwoTextObjects sbtto, Boolean BScoreExplain, PrintWriter out, Boolean BStemOn, Boolean BThesauruson, Connection dbc) {
        log.debug("genScores()");
        boolean bVerbose = false;
        try {
            boolean bScoreExplain = BScoreExplain.booleanValue();
            boolean bStemOn = BStemOn.booleanValue();
            boolean bThesauruson = BThesauruson.booleanValue();
            // was the data struct built with or without non title words as hook words
            com.iw.scoring.NodeForScore nfs = (com.iw.scoring.NodeForScore) sbtto.to1;
            DocForScore dfs = (DocForScore) sbtto.to2;
            long lNodeID = nfs.getNodeID();

            int iNumWords_NodeSize = ((com.iw.scoring.NodeForScore) sbtto.to1).getNodeSize();

            // may be stemmed down
            String[] sArrNodeSigWordsSortedDescMayBeStemmed =
                    ((com.iw.scoring.NodeForScore) sbtto.to1).getSarr(bStemOn, true, true);
            int[] iArrNodeSigWordsSortedDescMayBeStemmed =
                    ((NodeForScore) sbtto.to1).getiarr();

            if (sArrNodeSigWordsSortedDescMayBeStemmed == null) {
                // debug issue - should not be in production
                api.Log.LogError("2 shouldn't this be set from a file - sure you want to go to db for sigs ? Maybe In Debug mode? \r\n");
            } else {

                // may be stemmed down
                int iNumSigWordsNode = sArrNodeSigWordsSortedDescMayBeStemmed.length;

                // DOC ---------------
                // may be stemmed
                // unique word count doc - stemmed coun t
                Hashtable htWordCountsDoc2StemMaybe = ((DocForScore) sbtto.to2).getHtWordsNCount(bStemOn);

                if (htWordCountsDoc2StemMaybe == null) {
                    api.Log.LogFatal("htWordCountsDoc2StemMaybe == null");
                }

                // may be stemmed
                int iNumWordsBaseDocStemMaybePostBlend = dfs.getDocWordsEffectivePostBlend();

                if (sbtto.to1 instanceof NodeForScore && sbtto.to2 instanceof DocForScore) {
                    ExplainDisplayDataInstanceTABLE eddiTABLE_SummarlStatsNonTerm = null;
                    if (bScoreExplain) {
                        Vector vOArrRowExplain_SummaryTab_1 = new Vector();
                        String[] sArrColNames_Tab1_TopAllStatsNonTerm = {"Name", "Value"};
                        eddiTABLE_SummarlStatsNonTerm = new ExplainDisplayDataInstanceTABLE(
                                "ScoreStyle1", // source
                                "Summary", // tab name
                                ExplainDisplayDataContainer.sExplainDataRenderType_TABLE,
                                "Node and document properties, Aggregate score values", // Legend
                                "Summary statistics across scores", // tooltip
                                sArrColNames_Tab1_TopAllStatsNonTerm,
                                vOArrRowExplain_SummaryTab_1,
                                null); // coloring

                        sbtto.scoreExplainData.addExplainDisplayDataInstance(eddiTABLE_SummarlStatsNonTerm, 0);

                        Object[] oArr = new Object[2];
                        oArr[0] = "Node title";
                        oArr[1] = nfs.getTitle(false);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Node ID";
                        oArr[1] = "" + nfs.getNodeID();
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Node word count";
                        oArr[1] = "" + iNumWords_NodeSize;
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Node sig term count";
                        oArr[1] = "" + iNumSigWordsNode;
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document ID";
                        oArr[1] = "" + dfs.getDocID();
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document title";
                        oArr[1] = dfs.getTitle(false);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document local URL";
                        oArr[1] = dfs.getURL();
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document text read (bytes)";
                        oArr[1] = "" + dfs.getTopic().lTotalTextBytesReadByParser;
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document truncation (bytes)";
                        oArr[1] = "" + dfs.getTopic().iNumBytesOfDocsToUse;
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document truncated";
                        oArr[1] = "" + dfs.getTopic().bTruncatedByiNumBytesOfDocsToUse;
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document base word count";
                        oArr[1] = "" + dfs.getHtWordsNCount(bStemOn).size();
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Document base weight";
                        oArr[1] = "" + dfs.getWeight();
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Doc base word count weighted";
                        oArr[1] = (dfs.getWeight() * dfs.getHtWordsNCount(bStemOn).size());
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Doc blend num docs";
                        oArr[1] = (dfs.getArrDFSBlendedDocs().length - 1);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        if (dfs.getArrDFSBlendedDocs().length > 1) {
                            oArr = new Object[2];
                            oArr[0] = "Doc blend 1 base word count";
                            if (dfs.getArrDFSBlendedDocs()[1] != null) {
                                oArr[1] = "" + dfs.getArrDFSBlendedDocs()[1].getHtWordsNCount(bStemOn).size();
                            } else {
                                oArr[1] = "null or empty abstract";
                            }
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);

                            oArr = new Object[2];
                            oArr[0] = "Doc blend 1 weight";
                            if (dfs.getArrDFSBlendedDocs()[1] != null) {
                                oArr[1] = "" + dfs.getArrDFSBlendedDocs()[1].getWeight();
                            } else {
                                oArr[1] = "null or empty abstract";
                            }
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);

                            oArr = new Object[2];
                            oArr[0] = "Doc blend 1 word count weighted";
                            if (dfs.getArrDFSBlendedDocs()[1] != null) {
                                oArr[1] = "" + dfs.getArrDFSBlendedDocs()[1].getDocWordsEffectivePostBlend();
                            } else {
                                oArr[1] = "null or empty abstract";
                            }
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        }
                        oArr = new Object[2];
                        oArr[0] = "Composite doc effective word count";
                        oArr[1] = "" + iNumWordsBaseDocStemMaybePostBlend;
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);

                    }

                    double[] dArr_Dot1_Cov3 = getScore1_3_DOT_COV(
                            iNumWords_NodeSize, // won't be stemmed down
                            sArrNodeSigWordsSortedDescMayBeStemmed, // stemmed
                            iArrNodeSigWordsSortedDescMayBeStemmed,
                            bScoreExplain,
                            sbtto.scoreExplainData,
                            dfs, // added for passing in phrase yes/no variable
                            bStemOn,
                            bThesauruson,
                            nfs,
                            bVerbose);

                    double dScore1_FreqVecDotProdScore1 = dArr_Dot1_Cov3[0];
                    double dScore3_cov = dArr_Dot1_Cov3[1];
                    if (bScoreExplain) {
                        Object[] oArr = new Object[2];
                        oArr[0] = "Frequency";
                        oArr[1] = newNumFmt(dScore1_FreqVecDotProdScore1);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = "Coverage";
                        oArr[1] = newNumFmt(dScore3_cov);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                    }
                    sbtto.putScore(SSCORE1_VECTORDOTPRODUCT, dScore1_FreqVecDotProdScore1);
                    sbtto.putScore(SSCORE3_COVERAGE, dScore3_cov);



                    // ******************
                    // agg scores
                    // ******************
                    double dAggDotScore4 = getAggScore4(dScore1_FreqVecDotProdScore1, iNumWords_NodeSize,
                            bScoreExplain,
                            sbtto.scoreExplainData,
                            (int) lNodeID);
                    sbtto.putScore(SSCORE4_Score1TimesCorpusDocWords_Div132, dAggDotScore4);

                    // F0 = fbase
                    double dFtimesCScore7 = dScore3_cov * dAggDotScore4;
                    double dFbTimesCScore8 = dScore3_cov * dScore1_FreqVecDotProdScore1;

                    sbtto.putScore(SSCORE8_FbTimesC, dFbTimesCScore8);
                    sbtto.putScore(SSCORE7_FtimesC, dFtimesCScore7);
                    if (bScoreExplain) {
                        //base freq
                        Object[] oArr0 = new Object[2];
                        oArr0[0] = "Base Frequency";
                        oArr0[1] = newNumFmt(dScore1_FreqVecDotProdScore1);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr0);
                        oArr0 = new Object[2];
                        oArr0[0] = SSCORE8_FbTimesC + " score";
                        oArr0[1] = newNumFmt(dFbTimesCScore8);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr0);
                        // norm freq
                        Object[] oArr = new Object[2];
                        oArr[0] = "Normalized Frequency";
                        oArr[1] = newNumFmt(dAggDotScore4);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        oArr = new Object[2];
                        oArr[0] = SSCORE7_FtimesC + " score";
                        oArr[1] = newNumFmt(dFtimesCScore7);
                        eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                    }
                    // ****************
                    // new score f*c^  - if c < x then force cov = 0
                    // ****************
                    // .25
                    if (dScore3_cov < dMinForSSCORE7and8_FTimesCHat) {
                        sbtto.putScore(SSCORE7_FTimesCHat, 0);
                        sbtto.putScore(SSCORE8_FbTimesCHat, 0);
                        if (bScoreExplain) {
//                        // norm
                            Object[] oArr = new Object[2];
                            oArr[0] = SSCORE7_FTimesCHat + " score";
                            oArr[1] = "0";
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        }
                    } else {
                        sbtto.putScore(SSCORE8_FbTimesCHat, dFbTimesCScore8);
                        sbtto.putScore(SSCORE7_FTimesCHat, dFtimesCScore7);
                        if (bScoreExplain) {
                            // norm
                            Object[] oArr = new Object[2];
                            oArr[0] = SSCORE7_FTimesCHat + " score";
                            oArr[1] = newNumFmt(dFtimesCScore7);
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        }
                    }
                    // .20
                    if (dScore3_cov < dMinForSSCORE7and8_FTimesCHat_20) {
                        sbtto.putScore(SSCORE7_FTimesCHat_20, 0);
                        sbtto.putScore(SSCORE8_FbTimesCHat_20, 0);
                        if (bScoreExplain) {
                            // base
                            Object[] oArr0 = new Object[2];
                            oArr0[0] = SSCORE8_FbTimesCHat_20 + " score";
                            oArr0[1] = "0";
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr0);
                            // norm
                            Object[] oArr = new Object[2];
                            oArr[0] = SSCORE7_FTimesCHat_20 + " score";
                            oArr[1] = "0";
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        }
                    } else {
                        sbtto.putScore(SSCORE8_FbTimesCHat_20, dFbTimesCScore8);
                        sbtto.putScore(SSCORE7_FTimesCHat_20, dFtimesCScore7);
                        if (bScoreExplain) {
                            // base freq
                            Object[] oArr0 = new Object[2];
                            oArr0[0] = SSCORE8_FbTimesCHat_20 + " score";
                            oArr0[1] = newNumFmt(dFbTimesCScore8);
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr0);
                            // norm freq
                            Object[] oArr = new Object[2];
                            oArr[0] = SSCORE7_FTimesCHat_20 + " score";
                            oArr[1] = newNumFmt(dFtimesCScore7);
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        }
                    }
                    // .15
                    if (dScore3_cov < dMinForSSCORE7and8_FTimesCHat_15) {
                        sbtto.putScore(SSCORE7_FTimesCHat_15, 0);
                        sbtto.putScore(SSCORE8_FbTimesCHat_15, 0);
                        if (bScoreExplain) {
                            // base freq
                            Object[] oArr0 = new Object[2];
                            oArr0[0] = SSCORE8_FbTimesCHat_15 + " score";
                            oArr0[1] = "0";
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr0);
                            // norm freq
                            Object[] oArr = new Object[2];
                            oArr[0] = SSCORE7_FTimesCHat_15 + " score";
                            oArr[1] = "0";
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        }
                    } else {
                        sbtto.putScore(SSCORE8_FbTimesCHat_15, dFbTimesCScore8);
                        sbtto.putScore(SSCORE7_FTimesCHat_15, dFtimesCScore7);
                        if (bScoreExplain) {
                            // base freq
                            Object[] oArr0 = new Object[2];
                            oArr0[0] = SSCORE8_FbTimesCHat_15 + " score";
                            oArr0[1] = newNumFmt(dFbTimesCScore8);
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr0);
                            // norm freq
                            Object[] oArr = new Object[2];
                            oArr[0] = SSCORE7_FTimesCHat_15 + " score";
                            oArr[1] = newNumFmt(dFtimesCScore7);
                            eddiTABLE_SummarlStatsNonTerm.addVectorRow(oArr);
                        }
                    }

                    sbtto.putScore(SSCORE0, getScoreNodeTitleMatch(nfs, dfs, dbc));
                } else {
                    throw new Exception("invalid doc object types in scoring");
                }
            } // if scarr != null - debug issue
        } catch (Exception e) {
            api.Log.LogError("top level in ScoreStyle1", e);
        }


    } // fillScores
    // *************************************************
    private static int iDebugCallCounter = 0;

    private static double[] getScore1_3_DOT_COV(long iNodeSize,
            String[] sArrNodeSigWordsSortedDescMayBeStemmed,
            int[] iArrNodeSigWordsSortedDescMayBeStemmed,
            boolean bScoreExplain,
            ScoreExplainData scoreExplainData,
            DocForScore dfsM_main,
            boolean bStemOn,
            boolean bThesauruson,
            NodeForScore nfs,
            boolean bVerbose)
            throws Exception {
        bVerbose = true;
        iDebugCallCounter++;

        //Object[][] oArrArrScoreExplain
        //com.indraweb.profiler.Timer timer = com.indraweb.profiler.Profiler.getTimer("method FillScores->getScore1_3_DOT_COV", true);


        //api.Log.Log ("doc terms [" + UtilSets.htToStr(htWordAndCountSlaveDoc2, "\r\n" )+ "]" );

        double dScore_1_Frequency = 0;
        double dScore_1_increment = 0;
        //int iNumNegatives = 0;

        //String s_aSearchTerms[] = new String[20];
        float average_hit_location = 0;

        long lDocNumUniqueWords = dfsM_main.getDocWordsEffectivePostBlend();
        double dNodeFreq, dDocFreq;

        // pre_calc_score_for_percentages_in_printout i.e., % of signature covered
        double interdoc_score_precalc = 0;

        //long lNodeID = nfs.getNodeID ();
        //if (bVerbose) api.Log.Log ("scorestyle1 : lNodeID [" + lNodeID + "]");
        // boolean bDidLastLineEndInCRLF = true;
        //StringBuffer sbNotInWebDoc = new StringBuffer( " not in webDoc [" );
//        int iNumTermsToUse = UtilSets.min (
//                sArrNodeSigWordsSortedDescMayBeStemmed.length ,
//                com.indraweb.execution.Session.cfg.getPropInt ("NumSigTermsUsedInVectorScore")
//        );
        int iNumSigTermsInclTitlesAndThesAndALL = sArrNodeSigWordsSortedDescMayBeStemmed.length;
        boolean bNodeSCArrHasPhrases = nfs.getbNodeSCArrHasPhrases();
        //String sDocTextForPhrase = null;
        boolean[] bArrCoverageDataCollectorPerSigWord = new boolean[iNumSigTermsInclTitlesAndThesAndALL];
        // for each node term

        String[] sArrColNames_Explain1And3;
        Object[][] oArrArr_Explain = null;

        int iCol_Term = -1;
        int iCol_FreqSc1 = -1;
        int iCol_FreqSc1pct = -1;
        int iCol_CovSc3 = -1;
        int iCol_Nodecount = -1;
        int iCol_Nodefreq = -1;
        int iCol_Doccount = -1;
        int iCol_Docfreq = -1;

        if (bScoreExplain) {


            if (dfsM_main.getArrDFSBlendedDocs().length > 1) {
                scoreExplainData.addExplain(1,
                        "iNodeSize [" + iNodeSize + "] "
                        + "docWordsEffectivePostBlend [" + dfsM_main.getDocWordsEffectivePostBlend() + "] ",
                        bScoreExplain, (int) nfs.getNodeID());
            } else {
                scoreExplainData.addExplain(1,
                        "doc is blended, # in blend [" + dfsM_main.getArrDFSBlendedDocs().length + "]",
                        bScoreExplain, (int) nfs.getNodeID());
            }


            int i = 0;
            iCol_Term = i++;
            iCol_FreqSc1 = i++;
            iCol_FreqSc1pct = i++;
            iCol_CovSc3 = i++;
            iCol_Nodecount = i++;
            iCol_Nodefreq = i++;
            iCol_Doccount = i++;
            iCol_Docfreq = i++;

            String[] sArrColNames_Explain1And3_ = {"Term",
                "Frequency",
                "Frequency %",
                "Coverage",
                "Node count",
                "Node frequency",
                "Doc count",
                "Doc frequency"
            };
            sArrColNames_Explain1And3 = sArrColNames_Explain1And3_; // needn't init if not explaiing / autoinit requirement
            oArrArr_Explain = new Object[iNumSigTermsInclTitlesAndThesAndALL][sArrColNames_Explain1And3.length];
            Vector vRowColorRule = new Vector();
            vRowColorRule.addElement(new Integer(iCol_FreqSc1 + 1)); // hbk 2004 03 23
            vRowColorRule.addElement(ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS);
            vRowColorRule.addElement("");

            ExplainDisplayDataInstanceTABLE eddiTABLE_TermScoresBigTable = new ExplainDisplayDataInstanceTABLE(
                    "ScoreStyle1",
                    "Frequency & Coverage per term",
                    ExplainDisplayDataContainer.sExplainDataRenderType_TABLE,
                    "Frequency and Coverage per term", //desc
                    "Frequency and Coverage absolute contributions per term", // tooltip
                    sArrColNames_Explain1And3,
                    oArrArr_Explain,
                    vRowColorRule); // no row coloring Vector
            scoreExplainData.addExplainDisplayDataInstance(eddiTABLE_TermScoresBigTable);


        }


        // fill in the dfs array (blending) full text blocks for phrases
        // get  blended doc arr for phrases
        // pre compute these since used per term
        DocForScore[] dfsArrBlend = dfsM_main.getArrDFSBlendedDocs();
        String[] sArrDFSTextForPhrase = new String[dfsArrBlend.length];
        boolean[] bArrGotDFSTextForPhrase = new boolean[dfsArrBlend.length];
        String[] sArrDFSThesaurusExpandedString = new String[dfsArrBlend.length];
        Hashtable[] htArrThesaurusExpandedHTCount = new Hashtable[dfsArrBlend.length];
        for (int iDfsIdx = 0; iDfsIdx < dfsArrBlend.length; iDfsIdx++) {
            if (bThesauruson) {
                if (dfsArrBlend[iDfsIdx] != null) {
                    sArrDFSThesaurusExpandedString[iDfsIdx] = dfsArrBlend[iDfsIdx].getsThesaurusExpandedStrings(nfs.getCorpusID(), bStemOn);
                    htArrThesaurusExpandedHTCount[iDfsIdx] = dfsArrBlend[iDfsIdx].gethtWordCountThesExpansion(nfs.getCorpusID(), bStemOn);
                    /*
                     * if (bVerbose2) api.Log.Log ("iDfsIdx [" + iDfsIdx + "] sArrDFSThesaurusExpandedString [iDfsIdx] [" + sArrDFSThesaurusExpandedString[iDfsIdx] + "]");
                     */
                }
            }
        }

        // =============== PER SIG TERM OUTER LOOP LEVEL 1
        for (int iTermIdx = 0; iTermIdx < iNumSigTermsInclTitlesAndThesAndALL; iTermIdx++) {
            int iSigTermCount = iArrNodeSigWordsSortedDescMayBeStemmed[iTermIdx];
            String sSigTermToTestFor = sArrNodeSigWordsSortedDescMayBeStemmed[iTermIdx];
            //api.Log.Log ("in scorestyle1 " + iTermIdx + ". sSigTermToTestFor [" + sSigTermToTestFor + "]" );
            if (sSigTermToTestFor == null || sSigTermToTestFor.trim().equals("")) // || UtilStrings.isNumericFasterFirstCharBased (sSigTermToTestFor))
            {
                continue;
            }
            // see if this sig element is a phrase or not for scoting
            long lDocCountWeighted = 0;

            // individual words are all set - been weighted already
            // if term is a phrase

            if (false) {
                api.Log.Log("DEBUG ScoreStyle1 (OUTSIDE per DFS) : "
                        + " sig term sSigTermToTestFor : [" + sSigTermToTestFor
                        + "] iSigIdx  [" + iTermIdx + "] "
                        + "] iSigTermCount [" + iSigTermCount + "]");
            }


            boolean bPhraseTerm = bNodeSCArrHasPhrases && sSigTermToTestFor.indexOf(" ") >= 0;
            if (bPhraseTerm) // is it a phrase
            {
                // =============== PER BLENDED IN DOC INNER LOOP LEVEL 2 - for all doc blend section
                for (int iDFSIdx = 0; iDFSIdx < dfsArrBlend.length; iDFSIdx++) {
                    if (dfsArrBlend[iDFSIdx] != null) {
                        /*
                         *
                         * if (sSigTermToTestFor.equals (sTermOfDebugInterest)) api.Log.Log ("1/4 phrase non thes sSigTermToTestFor [" + sSigTermToTestFor + "] in text [" +
                         * sArrDFSTextForPhrase[iDFSIdx] + "]");
                         */
                        // store away phrase info per document blend component
                        if (bArrGotDFSTextForPhrase[ iDFSIdx] == false) // if no phrases then there was no need to precomputer this string
                        {
                            sArrDFSTextForPhrase[ iDFSIdx] = dfsArrBlend[ iDFSIdx].getTopic().getAllTextInOne_delimRemoved(bStemOn, true);
                            bArrGotDFSTextForPhrase[ iDFSIdx] = true;
                        }

                        // scoring point 1/4 phrase non thes
                        //String sDFSText = sArrDFSTextForPhrase[iDFSIdx];
                        int iDfsWeight = dfsArrBlend[ iDFSIdx].getWeight();

                        int iCountSigTermInDoc1 = UtilStrings.countNumOccurrencesOfStringInString(sArrDFSTextForPhrase[iDFSIdx], sSigTermToTestFor);
                        lDocCountWeighted += iDfsWeight * iCountSigTermInDoc1; // 1/4 phrase non thes

                        // count increment phrase from thesaurus expanded doc
                        // scoring point 2/4 phrase thes
                        if (bThesauruson && sArrDFSThesaurusExpandedString[iDFSIdx] != null) {
                            /*
                             * if (sSigTermToTestFor.equals (sTermOfDebugInterest)) api.Log.Log ("2/4 phrase thes sSigTermToTestFor [" + sSigTermToTestFor + "] in text [" +
                             * sArrDFSThesaurusExpandedString[iDFSIdx] + "]");
                             */
                            int iCountSigTermInDoc2 = UtilStrings.countNumOccurrencesOfStringInString(
                                    sArrDFSThesaurusExpandedString[iDFSIdx], sSigTermToTestFor);
                            lDocCountWeighted += iDfsWeight * iCountSigTermInDoc2; // 2/4 phrase thes

                        }
                    } // if not null
                }  // PER BLEND DOC

            } else // not a phrase
            {
                //if ( bVerbose )
                //    api.Log.Log ("scorestyle1 testing [" +  sSigTermToTestFor + "] not as a phrase");

                // only want a single hit for this.
                // hbk 2004 03 24 for ( int iDFSIdx = 0; iDFSIdx < dfsArrBlend.length; iDFSIdx++ )
                for (int iDFSIdx = 0; iDFSIdx < dfsArrBlend.length; iDFSIdx++) {
                    if (dfsArrBlend[iDFSIdx] != null) {
                        // scoring point 3/4 non phrase non thes
                        int iDfsWeight = dfsArrBlend[iDFSIdx].getWeight();
                        /*
                         * if (sSigTermToTestFor.equals (sTermOfDebugInterest)) api.Log.Log ("3/4 non phrase non thes sSigTermToTestFor [" + sSigTermToTestFor + "]");
                         */
                        int iCountSigTermInDoc3 = UtilSets.hash_get_count_for_string(
                                //dfsArrBlend[iDFSIdx].getHtWordsNCount(bStemOn),
                                dfsArrBlend[iDFSIdx].getHtWordsNCount(bStemOn),
                                sSigTermToTestFor);
                        lDocCountWeighted += iDfsWeight * iCountSigTermInDoc3;    // 3/4 non phrase non thes

                        // scoring point 4/4 non phrase thes
                        if (bThesauruson && sArrDFSThesaurusExpandedString[ iDFSIdx] != null) {
                            /*
                             * if (sSigTermToTestFor.equals (sTermOfDebugInterest)) api.Log.Log ("4/4 non phrase thes sSigTermToTestFor [" + sSigTermToTestFor + "]");
                             */

                            int iCountSigTermInDoc4 = UtilSets.hash_get_count_for_string(
                                    htArrThesaurusExpandedHTCount[ iDFSIdx], sSigTermToTestFor);

                            lDocCountWeighted += iDfsWeight * iCountSigTermInDoc4;  // 4/4 non phrase thes

                        }
                    }
                }
            } // else not a phrase

            dNodeFreq = (double) 10 * iSigTermCount / (double) iNodeSize;
            dDocFreq = (double) 10 * lDocCountWeighted / (double) lDocNumUniqueWords;
            double dIncrement = (dNodeFreq * dDocFreq * (double) 10);
            dScore_1_Frequency += dIncrement;
            if (dIncrement != 0) {
                bArrCoverageDataCollectorPerSigWord[ iTermIdx] = true;
            }
            if (bScoreExplain) {
                String sDashOrPlus = "-";
                if (dIncrement != 0) {
                    sDashOrPlus = "+";
                }

                String sExplain = iTermIdx + ". wd" + sDashOrPlus + " [" + sSigTermToTestFor
                        + "] Ncnt [" + iSigTermCount
                        + "] Dcnt [" + lDocCountWeighted
                        + "] incr [" + newNumFmt(dIncrement) + "]";
                if (bScoreExplain) {
                    scoreExplainData.addExplain(1, sExplain, bScoreExplain, (int) nfs.getNodeID());
                }
                oArrArr_Explain[iTermIdx][iCol_Term] = sSigTermToTestFor;
                //api.Log.Log("oArrArr_Explain["+iTermIdx+"]["+iCol_Term+"] = "+sSigTermToTestFor);
                oArrArr_Explain[iTermIdx][iCol_Nodecount] = new Integer(iSigTermCount);
                //api.Log.Log("oArrArr_Explain["+iTermIdx+"]["+iCol_Nodecount+"] = "+iSigTermCount);
                oArrArr_Explain[iTermIdx][iCol_Doccount] = new Integer((int) lDocCountWeighted);
                //api.Log.Log("oArrArr_Explain["+iTermIdx+"]["+iCol_Doccount+"] = "+lDocCountWeighted);

                if (dNodeFreq != 0) {
                    oArrArr_Explain[iTermIdx][iCol_Nodefreq] = new Double(dNodeFreq);
                } else {
                    oArrArr_Explain[iTermIdx][iCol_Nodefreq] = "";
                }
                if (dDocFreq != 0) {
                    oArrArr_Explain[iTermIdx][iCol_Docfreq] = new Double(dDocFreq);
                } else {
                    oArrArr_Explain[iTermIdx][iCol_Docfreq] = "";
                }
                if (dIncrement != 0) {
                    oArrArr_Explain[iTermIdx][iCol_FreqSc1] = new Double(dIncrement);
                } else {
                    oArrArr_Explain[iTermIdx][iCol_FreqSc1] = "";
                }
                if (dIncrement != 0) {
                    oArrArr_Explain[iTermIdx][iCol_CovSc3] =
                            new Double((double) ((double) 1 / (double) iNumSigTermsInclTitlesAndThesAndALL));
                } else {
                    oArrArr_Explain[iTermIdx][iCol_CovSc3] = "";
                }
            }
            //System.out.println("dIncrement[" + dIncrement + "] dScore_1  [" + dScore_1 + "]" );
        } // for all sig terms

        // SCORE 3 COV
        // now get coverage - num words which hit
        int iTotalHits = 0;
        for (int i = 0; i < bArrCoverageDataCollectorPerSigWord.length; i++) {
            if (bArrCoverageDataCollectorPerSigWord[i]) {
                iTotalHits++;
            }
        }

        //int iTotalHits_postNegatives = iTotalHits - iNumNegatives;
        double score_3 = ((double) iTotalHits) / (double) iNumSigTermsInclTitlesAndThesAndALL;
        /*
         * if (bVerbose_printScoreContribution && nfs.getNodeID () == iNodeIdVerbose_printScoreContribution) { api.Log.Log ("SC score_3 [" + score_3 + "] = iTotalHits [" + iTotalHits + "] ) /
         * iNumTermsToUse [" + iNumTermsToUse + "] "); }
         */
        //api.Log.Log ("SC score_3 [" + score_3 + "] = iTotalHits [" + iTotalHits + "] ) / denom [" + iNumSigTermsInclTitlesAndThesAndALL + "] ");
        if (bScoreExplain) {
            scoreExplainData.addExplain(3, "SCORE3 cov: hits [" + iTotalHits
                    + "] denom [" + iNumSigTermsInclTitlesAndThesAndALL
                    + "] score3 = a/b = [" + score_3 + "]",
                    bScoreExplain,
                    (int) nfs.getNodeID());

            // PCT CONTRIB CALCULATE % contrib , percent
            scoreExplainData.addExplain(1, " SCORE1/FINAL [" + newNumFmt(dScore_1_Frequency, 4) + "]", bScoreExplain,
                    (int) nfs.getNodeID());

            Object[][] oArrArr_PieChartScore1Contrib = new Object[iTotalHits][2];

            // get pie chart and score 1 freq % contrib
            int iIndexWithinTotalHits = 0;
            double dTotalPct = 0;
            try {

                for (int iTermIndex = 0; iTermIndex < oArrArr_Explain.length; iTermIndex++) {


                    Object oDFreqSc1Increment = oArrArr_Explain[iTermIndex][iCol_FreqSc1];

                    //if (oDFreqSc1Increment == null) {
                    //    api.Log.Log ("pct contrib iTermIndex  ["  + iTermIndex  + " ] [ "+iCol_FreqSc1+" ]" );
                    //    api.Log.Log ("pct contrib dScore_1_Frequency ["  + dScore_1_Frequency + "]" );
                    //    api.Log.Log ("calculating percentage contrib to that score]" );
                    //}
                    if (oDFreqSc1Increment instanceof String) {
                        if (oDFreqSc1Increment != null
                                && !oDFreqSc1Increment.toString().trim().equals("")) {
                            //api.Log.Log ("pct contrib sDFreqSc1Increment  [" + sDFreqSc1Increment + "]" );
                            {
                                oArrArr_Explain[iTermIndex][iCol_FreqSc1pct] = "";

                            }
                        } else {
                            oArrArr_Explain[iTermIndex][iCol_FreqSc1pct] = "";
                        }
                    } else if (oDFreqSc1Increment instanceof Double) {
                        Double DFreqSc1Increment = (Double) oDFreqSc1Increment;
                        if (DFreqSc1Increment.doubleValue() != 0) {
                            double dPCtContrib = 100 * DFreqSc1Increment.doubleValue()
                                    / dScore_1_Frequency;
                            //api.Log.Log ("dPCtContrib [" + dPCtContrib + "]" );
                            dTotalPct += dPCtContrib;
                            oArrArr_PieChartScore1Contrib[iIndexWithinTotalHits][0] = sArrNodeSigWordsSortedDescMayBeStemmed[iTermIndex];
                            oArrArr_PieChartScore1Contrib[iIndexWithinTotalHits][1] = new Double(dPCtContrib);
                            // also fill this in for the other explain report tab
                            //oArrArr_Explain1And3 [ iIndexWithinTotalHits ] [ iCol_FreqSc1pct ] =
                            //        UtilStrings.4ouble(dPCtContrib, 1);
                            oArrArr_Explain[iTermIndex][iCol_FreqSc1pct] =
                                    newNumFmt(dPCtContrib, 4);
                            iIndexWithinTotalHits++;
                        }
                        oArrArr_Explain[iTermIndex][iCol_FreqSc1] =
                                newNumFmt(DFreqSc1Increment.doubleValue());
                    } else {
                        throw new Exception("object class not handled in score explain");
                    }
                }

// now reduce precision on all doubles for display
                for (int iTermIndex = 0; iTermIndex < oArrArr_Explain.length; iTermIndex++) {
                    for (int iColIndex = 0; iColIndex < oArrArr_Explain[0].length; iColIndex++) {
                        Object o = oArrArr_Explain[iTermIndex][iCol_FreqSc1];
                        if (o.getClass().getName().equals("java.util.Double")) {
                            oArrArr_Explain[iTermIndex][iCol_FreqSc1] =
                                    newNumFmt(((Double) o).doubleValue());
                        }

                    }
                }

            } catch (Exception e) {
                api.Log.LogError("error in scoresstyle1", e);
            }


            //score_1


            String[] sArrDummyColNames = {"Term", "% Contrib to Freq Sc1"};
            ExplainDisplayDataInstanceTABLE eddiTABLE_PieChartScore1Contrib = new ExplainDisplayDataInstanceTABLE(
                    "ScoreStyle1",
                    "Frequency Contribution",
                    ExplainDisplayDataContainer.sExplainDataRenderType_PIECHART,
                    "% contribution to Frequency score of " + newNumFmt(dScore_1_Frequency, 4), //desc
                    "Percent contribution to Frequency score per term", // tooltip
                    sArrDummyColNames, //col names array (not needed for pie chart)
                    oArrArr_PieChartScore1Contrib,
                    null);
            scoreExplainData.addExplainDisplayDataInstance(eddiTABLE_PieChartScore1Contrib);


        }
        //timer.stop();

        double[] dArr_DOT_COV = new double[2];
        dArr_DOT_COV[0] = dScore_1_Frequency;
        dArr_DOT_COV[1] = score_3;
        return dArr_DOT_COV;
    } // getScore1_3_DOT_COV

    // FREQ SCALED ************************************************************************************
    static double getAggScore4(double dScore1,
            // ************************************************************************************
            int inumWordsInCorpusDoc,
            boolean bScoreExplain,
            ScoreExplainData scoreExplainData,
            int iNodeID) {

        //com.indraweb.profiler.Timer timer = com.indraweb.profiler.Profiler.getTimer("method ScoreStyle1.FillScores->getAggScore4", true);
        double dScore4 = 0;

        if (dScore1 != 0) {
            dScore4 = (dScore1 * (double) inumWordsInCorpusDoc) / (double) 132;
        }

        if (bScoreExplain) {
            scoreExplainData.addExplain(4, " FINAL : "
                    + " dScore4 = ( dScore1 * inumWordsInCorpusDoc ) / 132 "
                    + " dScore1 [" + newNumFmt(dScore1, 4) + " ]"
                    + " inumWordsInCorpusDoc [" + inumWordsInCorpusDoc + " ]"
                    + " dScore4 [" + newNumFmt(dScore4, 4) + "]",
                    bScoreExplain,
                    iNodeID);
        }
        //timer.stop();
        return dScore4;
    }

    public static String d(double d) {
        return (UtilStrings.numFormatDouble(d, 4));
    }
    private static DecimalFormat df = new DecimalFormat("0.000");

    public static String newNumFmt(double d) {
        return df.format(d);
        // UtilStrings.numFormatDouble (dScore1_FreqVecDotProdScore1 , 4)
    }

    public static String newNumFmt(double d, int i) {
        return newNumFmt(d);
        // UtilStrings.numFormatDouble (dScore1_FreqVecDotProdScore1 , 4)
    }

    /**
     * Method to calculate a pseudo page rank score. <br />
     * <code>
     * If a document title contains the node title -- score 60 pts
     * If a document title contains a must have - different from the node title - score 30 pts
     * If a must haves are in the first 100 words /( not including title ) -- add 20 pts
     * If a document is a HTML link -- and it is the main link on the site
     * (www.domain.com, www.domain.com/default.html or any of the standard front pages )
     * score + 10 pts as this site is about the concept.
     * </code>
     *
     * @param nfs NodeForScore Element.
     * @param dfs DocForScore Element.
     * @return score
     */
    private static double getScoreNodeTitleMatch(NodeForScore nfs, DocForScore dfs, Connection dbc) {
        log.debug("getScoreNodeTitleMatch(NodeForScore, DocForScore");
        double pageRankScore = 0;
        try {
            //Retrieve all the must haves for the node.
            IMustHaveBO mustHaveBO = new MustHaveBO(dbc);
            String mustHavesByNodeId = mustHaveBO.getMustHavesByNodeID(Integer.valueOf(String.valueOf(nfs.getNodeID())));

            //If a document title contains the node title.
            String docTitle = dfs.getTitle(false);
            log.debug("Document Title: " + docTitle);
            String nodeTitle;
            if (docTitle != null && !docTitle.isEmpty()) {
                //We get the node title from the cache.
                nodeTitle = nfs.getTitle(true);
                if (nodeTitle != null && !nodeTitle.isEmpty()) {
                    if (docTitle.contains(nodeTitle)) {
                        log.debug("DocTitle found in node title. Added 60pts to the score.");
                        pageRankScore = 60;
                    }
                }

                //If a document title contains a must have +add 30pts.
                String[] mustHaves;
                if (mustHavesByNodeId != null && !mustHavesByNodeId.isEmpty()) {
                    mustHaves = mustHavesByNodeId.split(",");
                    for (int i = 0; i < mustHaves.length; i++) {
                        if (docTitle.contains(mustHaves[i])) {
                            log.debug("Must have {" + mustHaves[i] + "} found in docTitle. Added 30pts to the score.");
                            pageRankScore += 30;
                            break;
                        }
                    }
                }
            }

            //If a must haves are in the first 100 words
            String first100Words;
            //Get all document content
            String documentContent;
            String[] wordsFromContent;
            StringBuilder documentWordSplit;
            if (dfs.getTopic().getsbAllTextInOne() != null) {
                documentContent = dfs.getTopic().getsbAllTextInOne().toString();
                if (documentContent.contains(" ")) {
                    wordsFromContent = documentContent.split(" ");
                    int limit = (wordsFromContent.length < 100) ? wordsFromContent.length : 100;
                    documentWordSplit = new StringBuilder();
                    for (int i = 0; i < limit; i++) {
                        documentWordSplit.append(wordsFromContent[i]).append(" ");
                    }
                    first100Words = documentWordSplit.toString();
                    String[] mustHaves;
                    if (mustHavesByNodeId != null && !mustHavesByNodeId.isEmpty()) {
                        mustHaves = mustHavesByNodeId.split(",");
                        for (int i = 0; i < mustHaves.length; i++) {
                            if (first100Words.contains(mustHaves[i])) {
                                log.debug("Must have found in first 100 words. Added 20pts to the score.");
                                pageRankScore += 20;
                                break;
                            }
                        }
                    }
                }
            }


            /**
             * If a document is a HTML link -- and it is the main link on the site (www.domain.com, www.domain.com/default.html or any of the standard front pages ) score + 10 pts as this site is
             * about the concept.
             */
            String documentWebURL = dfs.getsDocWebURL();
            if (isMainLink(documentWebURL)) {
                log.debug("This is the main link {" + documentWebURL + "}. Added 10pts to the score.");
                pageRankScore += 10;
            }

        } catch (Exception e) {
            log.error("An exception has ocurred. " + e.getLocalizedMessage());
        }
        return pageRankScore;
    }

    /**
     * Method to validate if the link is the main link of the page.
     *
     * @param link Valid URL of the link.
     *
     */
    private static boolean isMainLink(String link) {
        log.debug("isMainLink(String)");
        String domainPart = "";
        String internalSitePart = "";
        String webProtocol = "http";
        try {
            if (link == null || link.isEmpty()) {
                return false;
            }

            if (link.contains("/")) {
                //Remove the last slash.
                if (link.lastIndexOf("/") == link.length() - 1) {
                    link = link.substring(0, link.length() - 1);
                }
                //remove http/https/ftp part.
                if (link.contains("https://")) {
                    webProtocol = "https://";
                    link = link.replace("https://", "");
                } else if (link.contains("http://")) {
                    webProtocol = "http://";
                    link = link.replace("http://", "");
                } else if (link.contains("ftp://")) {
                    webProtocol = "ftp://";
                    link = link.replace("ftp://", "");
                }

                if (link.contains("/")) {
                    domainPart = webProtocol + link.substring(0, link.indexOf("/"));
                    if (link.indexOf("/") + 1 <= link.length()) {
                        internalSitePart = link.substring(link.indexOf("/") + 1, link.length());
                    }
                }

                //If this is a URL like: www.domain.com || www.domain.com/
                if (domainPart != null && !domainPart.isEmpty() && (internalSitePart == null || internalSitePart.isEmpty())) {
                    return true;
                }
                //Look if we have more // in the url
                if (domainPart != null && !domainPart.isEmpty() && (internalSitePart != null && !internalSitePart.isEmpty())) {
                    //find the patters for the main page url. index.* default.* 
                    if (!internalSitePart.contains("/")) {
                        if (internalSitePart.contains("index") || internalSitePart.contains("default")) {
                            return true;
                        }
                    } else {
                        return false;
                    }

                }
            }
        } catch (Exception e) {
            log.error("An exception ocurred. " + e.getMessage());
        }
        return false;
    }
}