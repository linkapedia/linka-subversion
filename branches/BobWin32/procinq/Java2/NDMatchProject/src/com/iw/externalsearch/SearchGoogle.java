// complicated case ... title plus first few words

package com.iw.externalsearch;

import com.indraweb.html.*;
import com.indraweb.util.Log;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import com.indraweb.webelements.IndraURL;
import com.iw.metasearch.Data_SearchengineCallout;
import com.iw.scoring.NodeForScore;

import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import java.io.File;


public class SearchGoogle
{

    // ***********************************************************************
    public static void doSearch(
            // ***********************************************************************
            Integer ISEID, // SEID for this (google) SE
            NodeForScore nfs,
            //  int iCorpusID_,
            //  int iSigGenRangeID_,
            //   long lDocIDofTopic_,
            //   long lNodeID_,
            api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQManager_ForRefreshNode,
            com.indraweb.util.IntContainer intContainerNumIURLsThisSE
            //java.sql.Connection dbc_
            )

            throws Exception
    {
        int iNumHits = doSearchInternal
                (
                        ISEID, // SEID for this (google) SE
                        nfs,
                        workQManager_ForRefreshNode,
                        1
                );
        intContainerNumIURLsThisSE.setiVal(iNumHits);
    }


    // ***********************************************************************
    public static int doSearchInternal(
            // ***********************************************************************
            Integer ISEID, // SEID for this (google) SE
            NodeForScore nfs,
            //  int iCorpusID_,
            //  int iSigGenRangeID_,
            //   long lDocIDofTopic_,
            //   long lNodeID_,
            api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQManager_ForRefreshNode,
            int iWhichGoogleMode
            //java.sql.Connection dbc_
            )

            throws Exception
    {


/*  for testing empty Q with waiters for X secs
        Log.logcr("inside SearchGoogle pre wait [" + ISEID + "]");
        com.indraweb.util.clsUtils.pause_this_many_milliseconds(20000);
        Log.logcr("inside SearchGoogle post wait [" + ISEID + "]");
*/

        boolean bVerbose = false;
        File f = new File ("/temp/IndraDebugMetaSearchVerbose.txt");
        if ( f.exists() )
            bVerbose = true;

        int iSEID = ISEID.intValue();
        int iNum_ResultsOnPage = Data_SearchengineCallout.getNumResultsOnPage(iSEID);
        int iNum_MaxPerSearchThisSE = Data_SearchengineCallout.getMaxPerSearchThisSE(iSEID);
        String sSEShortName = Data_SearchengineCallout.getSEShortName(iSEID);
        Vector vResultSet_ofIndraMetaURLs = new Vector();

        boolean boolMadeItToEndThisResultPage = false;
        boolean boolMadeItToEndAllX00ThisTopic = false;
        HTMLMemDoc hm = null;

        String[] searchTerms = null;
        //searchTerms = DBAccess_Central_connection.getSigWords_sArr_withTitle_SortedDesc(false, nfs.getNodeID(), dbc_);
        searchTerms = nfs.getSarr(false, true, false);

        int individualSearchHitSeekingCurently = 0;
        // truncate pool if < max








        for (int searchEngineResultsPageIndex = 0;
             individualSearchHitSeekingCurently < iNum_MaxPerSearchThisSE;
                //(searchEngineResultsPageIndex + 1) * iNum_ResultsOnPage <= iNum_MaxPerSearchThisSE;
             searchEngineResultsPageIndex++)
        {

            // **********************************************
            // ******** CREATE SEARCH URL *******************
            // **********************************************
            UrlHtmlAttributes urlHtmlAttributesContext_SearchURL = null;
            int iNumSearchHitsSoFar_priorToCurrentPage = 0;









/*
            for (int iNoMetaURLsFound = 0; iNoMetaURLsFound < 2; iNoMetaURLsFound++)
            {
                if ( iNoMetaURLsFound  > 0 )
                    iWhichGoogleMode = 1;

*/

            String sURLSearch = null;
            if (iWhichGoogleMode == 1)
            {
                sURLSearch = createURLSearchString_mode_1(
                        iSEID,
                        nfs,
                        searchTerms,
                        searchEngineResultsPageIndex,
                        nfs.getNodeID(),
                        iNum_ResultsOnPage,
                        sSEShortName
                );
            } else
            {
                sURLSearch = createURLSearchString_mode_2(
                        searchEngineResultsPageIndex,
                        nfs,
                        iNum_ResultsOnPage
                );
            }

            urlHtmlAttributesContext_SearchURL = new UrlHtmlAttributes(sURLSearch);
            System.out.println("[" + Data_SearchengineCallout.getSEShortName(iSEID) + "] N [" + nfs.getNodeID() + "] search URL [" + sURLSearch + "]\r\n");

            try
            {
                hm = new HTMLMemDoc(urlHtmlAttributesContext_SearchURL,
                        sSEShortName + " Search Sequence " + searchEngineResultsPageIndex,
                        2
                        , 240000);  // 2 retries for document // was from configparmas Session.cfg.getPropInt("socketTimeoutMSSearchEngines")
                //hm.writeToFile ( "c:/t.t");
            } catch (Exception e)
            {
                Log.NonFatalError(sSEShortName + " putDBMetaSearchResultSet(): can't get htmlmemdoc for [" +
                        sSEShortName + "] search");
                Log.printStackTrace_mine(62, e);
                return -1;			// EARLY RETURN !!!!!!!!!!!!!!!!!!!!!!!!!!1
            }

            try
            {
                individualSearchHitSeekingCurently = parseSEOutput_addQElements(
                        iSEID,
                        searchEngineResultsPageIndex,
                        hm,
                        vResultSet_ofIndraMetaURLs,
                        workQManager_ForRefreshNode,
                        iNum_ResultsOnPage,
                        iNum_MaxPerSearchThisSE,
                        sSEShortName,
                        iNumSearchHitsSoFar_priorToCurrentPage,
                        bVerbose
                );
/*
                    break; // done inside loop - no retry required
*/
            } catch (Exception e4)
            {
                Log.NonFatalError("[" + sSEShortName + "] got Exception ", e4);
            }
/*
            } // for ( int iNoMetaURLsFound = 0; iNoMetaURLsFound < 2; iNoMetaURLsFound )
*/

            // if got to last result (200 for AV 1000 for HB) or if no more results
            int numCandidateURLsCollectedThisNodeID = -1;  // TEST IF WORK COMPLETED - CANDIDATE URL TO DB
            int iCounter = 0;
            for (int i = 0; i < vResultSet_ofIndraMetaURLs.size(); i++)
            {
                IndraURL iu = (IndraURL) vResultSet_ofIndraMetaURLs.elementAt(i);
                if (iu.getWhichSE() == iSEID)
                    iCounter++;
            }
            numCandidateURLsCollectedThisNodeID = iCounter;


            if (individualSearchHitSeekingCurently % iNum_ResultsOnPage != 0)
            {
                break;  // got to
            }

/*
            if (individualSearchHitSeekingCurently == iNum_MaxPerSearchThisSE ||
                    numCandidateURLsCollectedThisNodeID >= iNum_MaxPerSearchThisSE
*/
            if (numCandidateURLsCollectedThisNodeID >= iNum_MaxPerSearchThisSE
            )
            {
                boolMadeItToEndAllX00ThisTopic = true;
                //Log.logClearcr(sSEShortName+ " COMPLETED MS FOR TOPIC " +
                //       " Num Cand Urls Collected this Node&SE [ " + numCandidateURLsCollectedThisNodeID + " ] " +
                //      " SearchPageIndex[ " + searchEngineResultsPageIndex + " ] " +
                //     " SearchURL_" + sSEShortName + " [ " + urlHtmlAttributesContext_SearchURL.getsURL() + " ] \r\n");
                break;  // got to
            }

            if (individualSearchHitSeekingCurently ==
                    (searchEngineResultsPageIndex * iNum_ResultsOnPage) + 1)
            {
                Log.FatalError("REALLY WANT TO BE HERE ? 11-27 : QUERY FAIL? - abort corpus topic no hits this page ? " +
                        sSEShortName + " SearchPageIndex [ " + searchEngineResultsPageIndex + " ] " +
                        "SearchPageIndex[ " + searchEngineResultsPageIndex + " ] " +
                        "SearchURL_" + sSEShortName + " [ " + urlHtmlAttributesContext_SearchURL.getsURL() + " ] " +
                        "] \r\n\r\n");
                //com.indraweb.database.WebCacheWrites.setCacheIDValidCode (
                searchEngineResultsPageIndex--; // try again
                // should automatically try again
            }
        }  // for ( int searchEngineResultsPageIndex = 0; (searchEngineResultsPageIndex+1) * num_ResultsOnPage <= num_MaxPerTopic; searchEngineResultsPageIndex++)

        int iNumURLs = vResultSet_ofIndraMetaURLs.size();
        // record indicating that all X records this document completed for AV

        //if (boolMadeItToEndAllX00ThisTopic == false) {
        //Log.NonFatalError(sSEShortName+ " nonfatal error boolMadeItToEndAllX00ThisTopic == false ");
        //}

        return vResultSet_ofIndraMetaURLs.size();
    }  // public static void doSearch (

    // ***********************************************************************
    public static String createURLSearchString_mode_1(
            int iSEID,
            NodeForScore nfs,
            String[] search_terms,
            // ***********************************************************************
            int searchResultsPageIndex,
            long lNodeID_,
            int num_ResultsOnPage,
            String sWhichSEAmI
            )
    {

        StringBuffer sb_TermString = new StringBuffer();
        int iNumSigTermsToUse = UtilSets.min(search_terms.length,
                30 // was configparams Session.cfg.getPropInt("NumSigTermsUsedInSearch")
        );
        int iNumWordsSoFar = 0;
        Hashtable htWordsAdded = new Hashtable();

        for (int i = 0; i < iNumSigTermsToUse && iNumWordsSoFar < 3; i++)
        {
            if (search_terms[i] != null && !UtilStrings.isNumericInteger(search_terms[i]))
            {
                if (i == 0)  // title
                {
                    String sTitleClean = null;
                    try
                    {
                        sTitleClean = nfs.getTitle(false);
                    } catch (Exception e)
                    {
                        Log.FatalError("error getting dirty title in google MS\r\n", e);
                    }

                    // hbk 2001 04 26 StringTokenizer st = new StringTokenizer ( search_terms[i] );
                    StringTokenizer st = new StringTokenizer(sTitleClean);
                    boolean bNeedPlusSignBetween = false;
                    while (st.hasMoreElements())
                    {
                        if (bNeedPlusSignBetween)
                            sb_TermString.append("+");
                        String wordToAdd = (String) st.nextElement();
                        htWordsAdded.put(wordToAdd, wordToAdd);
                        sb_TermString.append(wordToAdd);
                        bNeedPlusSignBetween = true;
                        // iNumWordsSoFar++;  // was in priori to 4/26/2001
                        // which makes these all non title words counted
                    }
                } else
                {
                    String wordToAdd = search_terms[i];
                    if (htWordsAdded.get(wordToAdd) == null)
                    {
                        sb_TermString.append("+");    // WORD DELIMITER K SET THIS
                        sb_TermString.append(wordToAdd);
                        htWordsAdded.put(wordToAdd, wordToAdd);
                        iNumWordsSoFar++;
                    } else
                        continue;
                }
            }
        }
        //if (!Session.cfg.getPropBool("GoogleSearchesPDFs"))
        if (false)
            sb_TermString.append("+-filetype%3Apdf");
        //if (!Session.cfg.getPropBool("GoogleSearchesDOCs"))
        if (false)
            sb_TermString.append("+-filetype%3Adoc");

        sb_TermString.append("+-filetype%3Axls");
        sb_TermString.append("+-filetype%3Aps");
        sb_TermString.append("+-filetype%3Appt");
        sb_TermString.append("+-filetype%3Artf");

        final String search_n_0 = "http://www.google.com/search?q=";   // put query terms here
        //final String search_n_0 = "http://216.239.51.100/search?q=";   // put query terms here
        final String search_n_1 = "&num=" + num_ResultsOnPage + "&hl=en&lr=lang_en&safe=active&start=";

        // concat number in here, 0, 100, ...
        final String search_n_2 = "&sa=N";

        String searchString_AsURL =
                search_n_0 + sb_TermString.toString() +
                search_n_1 +
                (100 * searchResultsPageIndex) + search_n_2;

        searchString_AsURL = UtilStrings.replaceStrInStr(searchString_AsURL, " ", "%20");

        //Log.logClearcr("[" + sWhichSEAmI + "] search URL [" + searchString_AsURL + "]\r\n");
        return searchString_AsURL;

    }  // createURLSearchStringFromSearchWordsArray



// ***********************************************************************
    public static String createURLSearchString_mode_2(
// ***********************************************************************
            int searchResultsPageIndex,
            NodeForScore nfs,
            int inum_ResultsOnPage
            )
    {


        String sTitleClean = null;
        try
        {
/*
            sTitleClean = DBAccess_Central_connection.getNodeTitle(false, true, true, false, lNodeID_,
                    dbc_);
*/
            sTitleClean = nfs.getTitle(false);
        } catch (Exception e)
        {
            Log.FatalError("error getting title in google2 MS\r\n", e);
        }

        // SIMPLE CASE - TITLE ONLY
        String[] sTitleArrNoStops = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption(
                sTitleClean, " ", true, true);
        StringBuilder sb_TermString = new StringBuilder();
        boolean bNeedPlusSignBetween = false;
        for (int i = 0; i < sTitleArrNoStops.length; i++)
        {
            if (bNeedPlusSignBetween)
                sb_TermString.append("+");
            bNeedPlusSignBetween = true;
            String wordToAdd = sTitleArrNoStops[i];
            sb_TermString.append(wordToAdd);
        }
        if (false) // !Session.cfg.getPropBool("GoogleSearchesPDFs"))
            sb_TermString.append("+-filetype%3Apdf");
        if (false) // !Session.cfg.getPropBool("GoogleSearchesDOCs"))
            sb_TermString.append("+-filetype%3Adoc");


        // as of date
        // new as of jan 14 2001
        // http://www.google.com/search?q=love&num=100&hl=en&lr=lang_en&safe=active&start=100&sa=N
        //  http://www.google.com/search?q=love+dog+war&num=100&hl=en&lr=lang_en&safe=active&start=100&sa=N
        // http://www.google.com/search?q=
        //love+time+dog
        //&num=100&hl=en&lr=lang_en&safe=active&start=
        //100
        //&sa=N

        final String search_n_0 = "http://www.google.com/search?q=";   // put query terms here
        //final String search_n_0 = "http://216.239.51.100/search?q=";   // put query terms here
        final String search_n_1 = "&num=" + inum_ResultsOnPage + "&hl=en&lr=lang_en&safe=active&start=";
        // concat number in here, 0, 100, ...
        final String search_n_2 = "&sa=N";
        // search_n_0 + search_n_1 + search_n_15 + 0 * spi + search_n_2



        String searchString_AsURL =
                search_n_0 + sb_TermString.toString() +
                search_n_1 +
                (100 * searchResultsPageIndex) + search_n_2;

        searchString_AsURL = UtilStrings.replaceStrInStr(searchString_AsURL, " ", "%20");

        return searchString_AsURL;

    }  // createURLSearchStringFromSearchWordsArray


    // ***********************************************************************
    private static int parseSEOutput_addQElements(
            // ***********************************************************************
            int iSEID,
            int searchEngineResultsPageIndex,
            HTMLMemDoc hm,
            Vector vResultSet_ofIndraMetaURLs_,
            api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQForMS_ToPutIndraURLsInto,
            int num_ResultsOnPage,
            int num_MaxPerSearchThisSE,
            String sWhichSEAmI,
            int iNumSearchHitsSoFar_priorToCurrentPage,
            boolean bVerbose

            ) throws Exception
    {
        int individualSearchHitSeekingCurently = 0;

        HTMLElement[] htmlElemArr = hm.getElements();
        individualSearchHitSeekingCurently = (searchEngineResultsPageIndex * num_ResultsOnPage) + 1;
        int iNumNewSearchHitsAddedThisResultPage = 0;
        //Log.logClearcr("*** next SearchGoogle page *** ");

        try
        {
            //hm.writeToFile ( "c:/temp/temp19.txt");
            //Log.LogPopup("remove this 123\r\n", false);
            boolean foundWEBRESULTS = false;

            int[] HTMLTagArr_tablesOpenAndClose =
                    {UtilHTML.HTMLTagType_OPENTABLE,
                     UtilHTML.HTMLTagType_CLOSETABLE};

            int[] HTMLTagArr_PsOpenAndClose =
                    {UtilHTML.HTMLTagType_OpenP,
                     UtilHTML.HTMLTagType_CloseP};
            // find the table with 100 Ps in it
            int[] iArrTableDelimiters = hm.findTypes(HTMLTagArr_tablesOpenAndClose,
                    0,
                    -1,
                    false);


            int[] iArrP_InRsultsTable = hm.findTypes(HTMLTagArr_PsOpenAndClose,
                    0,
                    -1,
                    false);


            // loop thru 100 P's
            //if ( iArrP_InRsultsTable.length > 102 )
            //Log.FatalError ( "Google error or format change - incorrect num <p>'s in url " + hm.getURLName() );

            // wihthin 1 out of 100 blocks find the individual info
            String siaTitle = null;
            String siaSummary = null;
            String siaDate = "";
            String siaURL = null;
            for (int j = 0; j < iArrP_InRsultsTable.length - 1
                    ; j++)
            {

                int iStartElem = iArrP_InRsultsTable[j];
                if (hm.getIntHTMLTypeMine(iStartElem + 1) != UtilHTML.HTMLTagType_OpenAHref)
                {
                    boolean bFinallyFoundItMaybeWasPDF = false;
                    while (iStartElem < iArrP_InRsultsTable[j + 1])
                    {
                        iStartElem++;
                        if (hm.getIntHTMLTypeMine(iStartElem + 1) == UtilHTML.HTMLTagType_OpenAHref)
                        {
                            bFinallyFoundItMaybeWasPDF = true;
                            break;
                        }
                    }
                    if (!bFinallyFoundItMaybeWasPDF)
                        continue; // e.g., [pdf] didn't find a link here but keep trying
                }

                siaURL = hm.getsHTMLTextOrURL(iStartElem + 1);
                siaTitle = hm.getsHrefText(iStartElem + 1).trim();

                //				System.out.println (vResultSet_ofIndraMetaURLs_.size() + ". GG found [" + siaTitle + "] [" + siaURL + "]\r\n");


                int iEndElem = iArrP_InRsultsTable[j + 1];
                int[] HTMLTagArr_FontsOpen = {UtilHTML.HTMLTagType_TagOpen_Font};
                // find the table with 100 Ps in it
                int[] iArrFonts = hm.findTypes(HTMLTagArr_FontsOpen,
                        iStartElem,
                        iEndElem,
                        false);
                // find green font
                int i = -1;
                boolean bFoundGreen = false;
                for (i = 0; i < iArrFonts.length; i++)
                {
                    String attrs = hm.getsHTMLAttrs(iArrFonts[i]);
                    //if (attrs != null && attrs.indexOf ( "{color=green}" ) >= 0 )
                    if (attrs != null && attrs.indexOf("{color=#008000}") >= 0)
                    {
                        iEndElem = iArrFonts[i] - 1;
                        bFoundGreen = true;
                        break;
                    }
                }

                if (!bFoundGreen || siaURL.toLowerCase().endsWith("html#pdf")) // must be pdf or something
                {
                    individualSearchHitSeekingCurently++;
                    continue;  // to next url
                }

                if (i == iArrFonts.length)
                    Log.FatalError("error in google result page - no green for a link  [" + hm.getURLName() + "]\r\n");
                if (i >= iArrP_InRsultsTable[j + 1])
                    Log.FatalError("error in google result page - green font after next link - no green this link  [" + hm.getURLName() + "]\r\n");

                // find Description if present
                //UtilFile.println (  htmlElemArr[ docIndexesHrefAndText [ j ] ].getFullTokenizedHTMLLineLine() ) ;
                int iLocDescriptioncolon = UtilHTMLMem.findHTMLTextOrAnchor(hm,
                        UtilHTML.HTMLTagType_TEXT,
                        "Description:",
                        iStartElem,
                        iEndElem,
                        1,
                        UtilStrings.CONST_CompareContains,
                        true,
                        false,
                        false
                );
                if (iLocDescriptioncolon > 0)
                    iEndElem = iLocDescriptioncolon - 1;

                //Log.log ( individualSearchHitSeekingCurently +
                //". got google data siaDate ["		+ siaDate + "] " +
                //" siaURL ["		+ siaURL + "] " +
                //" siaTitle ["	+ siaTitle + "] " +
                //" siaSummary ["	+ siaSummary + "] \r\n" ) ;
                siaSummary = UtilHTMLMem.getTextBlock(hm, iStartElem, iEndElem, null);

                if (!siaURL.toLowerCase().startsWith("http"))
                {
                    if (siaURL.toLowerCase().indexOf("http") == 0)
                    {
                        Log.FatalError(
                                "skipping MetaUrl no http in string from SE [" + sWhichSEAmI +
                                " siaDate [" + siaDate + "] " +
                                " siaURL [" + siaURL + "] " +
                                " siaTitle [" + siaTitle + "] " +
                                " siaSummary [" + siaSummary + "] " +
                                " hm orig doc cacheID [" + hm.getCacheID_originating() + "] " +
                                "\r\n");

                        continue;
                    }

                    // has an http : now see if there are two
                    if (UtilStrings.countNumOccurrencesOfStringInString(siaURL.toLowerCase(), "http") == 0)
                    {
                        Log.FatalError(
                                "skipping MetaUrl x no http in string from SE [" + sWhichSEAmI +
                                " siaDate [" + siaDate + "] " +
                                " siaURL [" + siaURL + "] " +
                                " siaTitle [" + siaTitle + "] " +
                                " siaSummary [" + siaSummary + "] " +
                                " hm orig doc cacheID [" + hm.getCacheID_originating() + "] " +
                                "\r\n");

                        continue;
                    } else // == 1
                    {
                        siaURL = UtilStrings.getStrStartingWithThisToEnd1Based(
                                siaURL, "http", 1);
                    }
                }

                // handle special case where summary is just a <BR> : as in 198 example

                IndraURL indraurl = null;
                if ( true )  // hbk control 2003 03 04 true for production
                    indraurl = new com.indraweb.webelements.IndraURL(siaURL, siaTitle, siaDate, siaSummary, iSEID, //+ 2 SUMMARY HTMLTokenizer[offset=4540,type=text,tag=null,attrs=null,text=INTERDISCIPLINARY BIBLICAL RESEARCH INSTITUTE IBRI Research Report #44 (1996) GENESIS 11 AND ARCHAEOLOGICAL EVIDENCE FOR PALEOLITHIC MAN Daniel E. Wonderly 4064 Pleasant Valley Rd. Oakland, MD 21550 INTRODUCTION Many Bible students in the modern...
                        individualSearchHitSeekingCurently);
                else {   // test mode - run against one URL hbk control 2003 03 04
                    // REMEMBER TO SET ONLY SE TO GOOGLE
                    System.out.println("hbk control - FIXED GOOGLE FIND");
                    indraurl = new com.indraweb.webelements.IndraURL(
                        // siaURL,
/*
                        "http://localhost:8080/itsapi/ts?" +
                            //"fn=tsmetasearchnode.TSMetasearchNode&post=true&"+
                            "fn=tsother.TSTestWait&"+
                            "post=true&"+
                            "MACHTHREADINFO=M+[http://localhost:8080/itsap]++TI+[0]+MI+[0]&nodeid=1087105&CALLERDESC=M+[http://localhost:8080/itsap]++TI+[0]+MI+[0]&"+
                            "NUMTHREAD=8&"+
                            "SKEY=11111111&"+
                            "whichdb=SVR",
*/
                        "http://www.ams.usda.gov/nop/nosbnationallistdatabase.xls",
                        siaTitle, siaDate, siaSummary, iSEID,
                        individualSearchHitSeekingCurently);

                    vResultSet_ofIndraMetaURLs_.addElement(indraurl);
                    workQForMS_ToPutIndraURLsInto.addWorkItem(indraurl, "" + iSEID);
                    iNumNewSearchHitsAddedThisResultPage++;

                    break;
                }


                //if ( individualSearchHitSeekingCurently+j %10 == 0 )
                // ADD RESULT
                if ( bVerbose )
                    Log.logcr("SearchGoogle verbose output : inside SearchGoogle [" + sWhichSEAmI + "] added indraurl [" + j +
                        "] [" + indraurl.getTitle() + "] indraurl [" + indraurl.getURL() + "]");
                vResultSet_ofIndraMetaURLs_.addElement(indraurl);
                workQForMS_ToPutIndraURLsInto.addWorkItem(indraurl, "" + iSEID);
                iNumNewSearchHitsAddedThisResultPage++;


                if (individualSearchHitSeekingCurently % num_ResultsOnPage == 0)
                {
                    //Log.log(" metaUrl " + sWhichSEAmI + " results page [ " + searchEngineResultsPageIndex +
                    //      " ] of [ " + (num_MaxPerSearchThisSE / num_ResultsOnPage) + " ] \r\n", false);
                    break;
                }

                individualSearchHitSeekingCurently++;

                if (iNumNewSearchHitsAddedThisResultPage + iNumSearchHitsSoFar_priorToCurrentPage >=
                        num_MaxPerSearchThisSE)
                    break;
            } // for ( int j = 0; j < iArrP_InRsultsTable.length - 2; j++ ) // for each hit of 100

            if (individualSearchHitSeekingCurently == 0)
            {
                String sErr = sWhichSEAmI + "  never found any web results " +
                        " SE [" + sWhichSEAmI +
                        "] searchURL [" + hm.getURLName() +
                        "] \r\n";
                Log.NonFatalError(sErr);
                // WebCacheWrites.setCacheIDValidCode  ( hm.getCacheID_originating (), WebCacheWrites.CACHE_INVALID_REASON_CODE__SearchFail, dbcMain ) ;
                throw new Exception();
            }
        } catch (Exception e)
        {
            Log.printStackTrace_mine(65, e);
        }

        return individualSearchHitSeekingCurently;

    }	// private static int parseSEResultSet ()


}

