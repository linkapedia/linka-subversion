package com.iw.metasearch;

import com.indraweb.webelements.IndraURL;

import java.util.Vector;


/**
 * will be one of these per topic, stored in the topic object, covers multiple SE's
 */


public class MetaSearchResultSet {
    // vector of arrays of indraURLs - one array per search engine
    private Vector VecSEResultSetsOnePerEngine = new Vector();

    public void addResult(com.indraweb.webelements.IndraURL iURL) {
        VecSEResultSetsOnePerEngine.addElement(iURL);
    }

    public com.indraweb.webelements.IndraURL getResult(int i) {
        if (i < VecSEResultSetsOnePerEngine.size())
            return (IndraURL) VecSEResultSetsOnePerEngine.elementAt(i);
        else
            return null;
    }


}
