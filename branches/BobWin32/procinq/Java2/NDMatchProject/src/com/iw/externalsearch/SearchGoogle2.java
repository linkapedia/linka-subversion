// does titles only

package com.iw.externalsearch;

import com.iw.scoring.NodeForScore;


public class SearchGoogle2 {
    // ***********************************************************************
    public static void doSearch(
            // **********************************************************************
            Integer ISEID, // SEID for this (google) SE
            NodeForScore nfs,
            //  int iCorpusID_,
            //  int iSigGenRangeID_,
            //   long lDocIDofTopic_,
            //   long lNodeID_,
            api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQManager_ForRefreshNode,
            com.indraweb.util.IntContainer intContainerNumIURLsThisSE
            //java.sql.Connection dbc_
            )

            throws Exception {
        int iNumHits = com.iw.externalsearch.SearchGoogle.doSearchInternal
                (
                        ISEID, // SEID for this (google) SE
                        nfs,
                        workQManager_ForRefreshNode,
                        2
                );

        intContainerNumIURLsThisSE.setiVal(iNumHits);
    }
}