package com.iw.metasearch;

import com.indraweb.encyclopedia.Topic;

import java.util.Vector;

public interface MetaSearchInterface {

    public Vector putDBMetaSearch_CandidateURLResultSetThisCorpTopic(
            Topic curTopicCorpus_,
            int iCorpusID_,
            int iSigGenRangeID_,
            long lDocIDofTopic_,
            long lNodeID_,
            java.sql.Connection dbc_
            )
            throws Exception;


}
