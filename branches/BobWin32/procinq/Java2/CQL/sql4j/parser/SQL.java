package sql4j.parser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import sql4j.util.Misc;

public class SQL {

    private SQLStatement sqlStatement;
    private String name;
    //private static String sSQLtemp = " select doctitle, nodetitle where documentid = 123";

    /**
     * SQL constructor comment.
     */
    public SQL() {
        super();
    }

    public SQL(File f) {
        init(Misc.file2string(f));
    }

    /**
     * Construct a SQL using a sqlString.
     *
     */
    public SQL(String sqlString) {
        init(sqlString);
    }

    /**
     * Insert the method's description here. Creation date: (5/3/01 1:45:35 AM)
     *
     * @param sql java.lang.String
     * @param errRecover boolean
     * @param gui boolean
     */
    public SQL(String sqlString, boolean errRecover, boolean gui) {
        try {
            byte[] bytes = sqlString.getBytes("UTF-8");
            InputStream is = new ByteArrayInputStream(bytes);
            SQLParser p = null;
            if (errRecover) {
                p = new SQLParserWithErrProcess(is);
            } else {
                p = new SQLParser(is);
            }
            sqlStatement = (SQLStatement) p.parse().value;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public SelectStatement getSelectStatement() {
        if (sqlStatement instanceof SelectStatement) {
            return (SelectStatement) sqlStatement;
        } else {
            return null;
        }
    }

    public SQLStatement getSQLStatement() {
        return sqlStatement;
    }

    /**
     * Insert the method's description here. Creation date: (4/30/01 12:04:09
     * AM)
     */
    private void init(String sqlString) {
        try {
            byte[] bytes = sqlString.getBytes("UTF-8");
            InputStream is = new ByteArrayInputStream(bytes);;
            SQLParserWithErrProcess p = new SQLParserWithErrProcess(is);
            sqlStatement = (SQLStatement) p.parse().value;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test program for the SQL parser and Mapping classes.
     *
     */
    public static void main(String[] args) {
        //Mappings m=new Mappings(sql);
        //m.setVisible(true);
    }

    public void setName(String n) {
        name = n;
    }

    /**
     * // above is not used anywhere - but initialization may be important -
     * HBK Insert the method's description here. Creation date: (12/17/2001
     * 2:27:37 PM)
     */
    ViewDef getViewDef() {
        return (sqlStatement instanceof ViewDef) ? (ViewDef) sqlStatement : null;

    }

    /**
     * Insert the method's description here. Creation date: (12/17/2001 2:27:37
     * PM)
     */
    void newMethod() {
    }
}
