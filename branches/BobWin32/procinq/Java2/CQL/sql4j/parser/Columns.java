package sql4j.parser;

/**
 * Insert the type's description here.
 * Creation date: (10/20/00 4:06:35 AM)
 * @author: Jianguo Lu
 * A list of columns.

 **/
import java.util.*;



import sql4j.schema.*;import sql4j.util.Misc;
import sql4j.util.StringSet;

public class Columns {
	private Vector columns=new Vector();



/**
 * Insert the method's description here.
 * Creation date: (1/12/01 8:15:14 PM)
 */
public Columns() {}/**
 * Insert the method's description here.
 * Creation date: (1/12/01 8:07:57 PM)
 */        
public Columns(Column c) {
	columns = new Vector();
	columns.add(c);
}
public Columns(Vector c) {
	columns = c;
}
    /**
     * Insert the method's description here.
     * Creation date: (1/12/01 8:07:57 PM)
     */
        public Columns add(Column c){
            columns.add(c);
            return this;
        }
    /**
     * Insert the method's description here.
     * Creation date: (1/12/01 8:07:57 PM)
     */
        public Columns addUnique(Column c) throws Exception {
            if ( !containsColNamed ( c.getColumnName().toString() ) )
                columns.add(c);
            return this;
        }
/**
 * Insert the method's description here.
 * Creation date: (1/30/01 11:28:35 AM)
 * @param cs com.ibm.commerce.migration.parser.sql.Columns
 */
public void addAll(Columns cs) {
	if (cs!=null) columns.addAll(cs.toVector());
}

    public boolean contains(Object o) {
        return columns.contains(o);
    }
    public boolean contains(String sTableName, String sColumnName, boolean bTableNameMatters ) {
        Enumeration e = columns.elements();
        while ( e.hasMoreElements() )
        {
            Column c = (Column) e.nextElement();
            if ( sColumnName.equals ( c.getColumnName().toString() ) )
            {
                if (bTableNameMatters)
                {
                    if ( sTableName.equals ( c.getTableName() ) )
                        return true;

                }
                else
                    return true;
            }
        }

        return false;
    }
	public Enumeration elements(){
		return columns.elements();
	}
/**
 * Insert the method's description here.
 * Creation date: (1/30/01 12:22:46 PM)
 * @return com.ibm.commerce.migration.parser.sql.Column
 */
public Column first() {
	if (columns==null) {
		return null;
	}else {
		return (Column)columns.elementAt(0);
	}
}
/**
 * Insert the method's description here.
 * Creation date: (5/9/01 1:58:14 AM)
 */
public String getColumnNames() {
	Vector stringVector = new Vector();
	for (Enumeration e = columns.elements(); e.hasMoreElements();) {
		Column c= (Column) e.nextElement();
		if(c!=null){
			stringVector.add(c.getName());
		}
	}
	String result = Misc.toString(stringVector);
	return result;
}
/**
 * The complete qualified name of the columns.
 * For column names only, use getColumnNames().
 * Creation date: (10/23/00 12:32:10 AM)
 */
public String toString() {
	/**Selection selection = new Selection(columns);
	return selection.toString();
	**/
	Vector stringVector = new Vector();
	for (Enumeration e = columns.elements(); e.hasMoreElements();) {
		Column c= (Column) e.nextElement();
		if(c!=null){
			stringVector.add(c.toString());
		}
	}
	String result = Misc.toString(stringVector);
	return result;
}
public Vector toVector() {
	return columns;
}

 public Tables getTables() throws Exception
 {
    Tables tReturn = new Tables ();
    for (Enumeration e = columns.elements(); e.hasMoreElements();)
    {
        Column c = (Column) e.nextElement();
        String sTabName = c.getTableName();
        tReturn.add(new Table (sTabName ));
    }
     return tReturn;
 }
    public void resetTables ( Schema s ) throws Exception
    {
        for (Enumeration e = this.elements(); e.hasMoreElements();) {
            Column c= (Column) e.nextElement();
            String sColName = c.getName();
            Hashtable htKnownColToTable = s.getColumnTableMapping().getMapping();
            HashSet hs = (HashSet ) htKnownColToTable.get( sColName );
            if ( hs.size() == 1 )          {
                String s2 = (String) hs.toArray()[0];
                c.setTableName(s2);
            }
            else
                throw new Exception ( "bad schema def for col [" + sColName + "]");
        }
    }
    public Columns getDeepCopy ()
    {
        Columns cols2 = new Columns();
        for (Enumeration e = this.elements(); e.hasMoreElements();) {
            Column c = (Column) e.nextElement();
            cols2.add (new Column( c.getTableName(), c.getColumnName() ));
        }
        return cols2;
    }
    public int size(){
        return columns.size();
    }

    public boolean containsColNamed ( String s )  throws Exception
    {
        return getColNames(false).contains(s);
    }

    public StringSet getColNames( boolean bIncludeTableNameDot)
    throws Exception
    {
        StringSet ss = new StringSet();
        for (Enumeration e = this.elements(); e.hasMoreElements();) {
             Column c = (Column) e.nextElement();
            if ( bIncludeTableNameDot )
                ss.add(c.getTableName()+"."+c.getColumnName());
            else
                ss.add(c.getColumnName().toString());
        }
        return ss;
     }

    public Column getColumn(String sTableName, String sColName)
    {
       Column colReturn = null;
       for (Enumeration e = columns.elements(); e.hasMoreElements();)
       {
           Column c = (Column) e.nextElement();
           if ( c.getTableName().equals( sTableName) && c.getColumnName().toString().equals( sColName)) {
            colReturn = c;
            break;
           }
       }
       return colReturn;
    }
    public Columns getColsThisTable (String sTableName)
    {
       Columns colsReturn = new Columns();
       for (Enumeration e = columns.elements(); e.hasMoreElements();)
       {
           Column c = (Column) e.nextElement();
           //api.Log.Log ("hbk processing [" + c.getColumnName() + "]" );
           if ( c.getTableName().equals( sTableName) ) {
               colsReturn.add ( c );
           }
       }
       return colsReturn;
    }

    public Columns getUnknownCols ()
    {
        return getColsThisTable("UNKNOWNTABLE");
    }
 }