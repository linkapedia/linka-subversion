/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Sep 30, 2003
 * Time: 4:22:12 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package sql4j.CQLInterpreter;

import java.util.*;
import java.io.*;
import java.sql.*;

import com.indraweb.util.UtilStrings;
import com.indraweb.util.UtilSets;
import com.indraweb.execution.Session;
import com.indraweb.utils.oracle.CustomFields;

import sql4j.parser.*;

import sql4j.schema.*;
import sql4j.util.*;
import com.iw.system.User;
import com.iw.system.Index;

public class CQLSchema
{
    static Mapping mapKnownTablesToCols_StrToStrSet = null;
    static Schema schema;
    static HashMap hmUnknownColsCandidates_StrColToStrSetTabs;
    //static HashSet hsTextIndexedCols = null;
    static Hashtable  htObjectColsExpans_StrToStrSetCols;

    private static String[] sArrTables = {
        "NODE","DOCUMENT","IDRACNODE","NODEDOCUMENT","CORPUS",
        "SIGNATURE","DOCUMENTSECURITY"};

    static Mapping defineWhereKnownColsComeFrom (Connection dbc) throws Exception
    {
        // new col step 2
        Mapping mapTablesToCols_StrToStrSet = new Mapping ();

        Set setCols_UNKNOWNTable = new HashSet ();
        setCols_UNKNOWNTable.add ("NODEID");
        setCols_UNKNOWNTable.add ("CORPUSID");
        setCols_UNKNOWNTable.add ("DOCUMENTID");
        setCols_UNKNOWNTable.add ("CS_FTSCORE");
        mapTablesToCols_StrToStrSet.put ("UNKNOWNTABLE" , setCols_UNKNOWNTable);

        for (int i = 0; i < sArrTables.length; i++ )
        {

            String sTableName = sArrTables[i];
            Enumeration enumStrColNamesThisTable = CustomFields.getFields(sTableName, dbc).keys();
            Set setCols_ThisTable = new HashSet ();
            while ( enumStrColNamesThisTable.hasMoreElements())
            {
                String sColName = (String) enumStrColNamesThisTable.nextElement();
                if ( !setCols_UNKNOWNTable.contains(sColName))
                    setCols_ThisTable.add ( sColName );
            }
            mapTablesToCols_StrToStrSet.put (sArrTables[i] , setCols_ThisTable);

        }

        return mapTablesToCols_StrToStrSet;

    }

    static HashMap defineWhereUnkColsCanComeFrom () throws Exception
    {
        HashMap hmReturn = new HashMap ();
// where CAN we get unknown cols from
// NODEID
        StringSet ssetTablesForColNodeid = new StringSet ();
        ssetTablesForColNodeid.add ("NODE");
        ssetTablesForColNodeid.add ("IDRACNODE");
        ssetTablesForColNodeid.add ("NODEDOCUMENT");
        ssetTablesForColNodeid.add ("SIGNATURE");
        hmReturn.put ("NODEID" , ssetTablesForColNodeid);
// CORPUSID
        StringSet ssetTablesForColCorpusid = new StringSet ();
        ssetTablesForColCorpusid.add ("CORPUS");
        ssetTablesForColCorpusid.add ("NODE");
        hmReturn.put ("CORPUSID" , ssetTablesForColCorpusid);
// DOCUMENTID
        StringSet ssetTablesForColDocumentid = new StringSet ();
        ssetTablesForColDocumentid.add ("DOCUMENT");
        ssetTablesForColDocumentid.add ("DOCUMENTSECURITY");
        ssetTablesForColDocumentid.add ("NODEDOCUMENT"); // hbk 203 09 24
        hmReturn.put ("DOCUMENTID" , ssetTablesForColDocumentid);

// CS_FTSCORE
        StringSet ssetTablesForColCS_FTScore = new StringSet ();
        ssetTablesForColCS_FTScore.add ("DOCUMENT");
        ssetTablesForColCS_FTScore.add ("NODEDOCUMENT"); // hbk 203 09 24
        ssetTablesForColCS_FTScore.add ("IDRACNODE"); // hbk 203 09 24
        ssetTablesForColCS_FTScore.add ("NODE"); // hbk 203 09 24
        hmReturn.put ("CS_FTSCORE" , ssetTablesForColCS_FTScore);

        return hmReturn;
    }

    public static boolean getIsColTextIndexed (String s, Connection dbc) throws Exception
    {
        Hashtable htIndexes = CustomFields.getIndexes(dbc);
        Vector vTabAndCol = UtilStrings.splitByStrLen1(s, ".");
        return ( CustomFields.isTableFieldIndexed ( htIndexes,
                (String) vTabAndCol.elementAt(0),
                (String) vTabAndCol.elementAt(1) ));
    }

    /**
     * WHICH COLUMNS ARE INTERMEDIA FULLTEXT COLS
     */
    static HashSet defineWhichColsAreTextIndexed (boolean bFullyQualified, Connection dbc) throws Exception
    {
        Hashtable htIndexes = CustomFields.getIndexes(dbc);
        HashSet hsStrColNamesReturn = new HashSet ();
        Enumeration enumIndexes = htIndexes.elements();
        while ( enumIndexes.hasMoreElements())
        {
            Index index = (Index) enumIndexes.nextElement();
            if ( bFullyQualified )
                hsStrColNamesReturn.add ( index.getTable().toUpperCase() + "." + index.getField().toUpperCase() );
            else
                hsStrColNamesReturn.add ( index.getField().toUpperCase() );
        }

        return hsStrColNamesReturn;
    }

    static Hashtable defineObjectColsExpans_htStrToStrSetCols (Connection dbc) throws Exception
    {

        Hashtable htReturn = new Hashtable ();
// where CAN we get unknown cols from
// <DOCUMENT>
// add new col step 1
        // <DOCUMENT>
        StringSet ssetColsThisObject_document = new StringSet ();

        Hashtable htDocFields = CustomFields.getFields("DOCUMENT", dbc);
        StringSet ss1 = new StringSet();
        ss1.addAll(UtilSets.htKeysToHashSet(htDocFields));
        ss1.add("CS_FTSCORE");
        htReturn.put ("<DOCUMENT>" , ss1);

        // <NODE>
        Hashtable htNodeFields = CustomFields.getFields("NODE", dbc);
        StringSet ss2 = new StringSet();
        ss2.addAll(UtilSets.htKeysToHashSet(htNodeFields));
        ss2.add("CS_FTSCORE");
        htReturn.put ("<NODE>" , ss2);


/*
        StringSet ssetColsThisObject_signature = new StringSet ();
        ssetColsThisObject_signature.add ("SIGNATUREWORD");
        ssetColsThisObject_signature.add ("SIGNATUREOCCURENCES");
        htReturn.put ("<SIGNATURE>" , ssetColsThisObject_node);
*/

        // <IDRACNODE>
        HashSet hsIdracNodeFields = UtilSets.htKeysToHashSet(CustomFields.getFields("NODE", dbc));
        hsIdracNodeFields.addAll(UtilSets.htKeysToHashSet(CustomFields.getFields("IDRACNODE", dbc)));
        if ( hsIdracNodeFields.size() + 1 <
                    CustomFields.getFields("NODE", dbc).size() +
                    CustomFields.getFields("IDRACNODE", dbc).size() )
        {
            String sFindOverlap = CustomFields.getTableIntersect ("NODE", "IDRACNODE", dbc);
            api.Log.Log ("sFindOverlap [" + sFindOverlap  + "]");
            throw new Exception ("overlapping column between Node and IdracNode tables [" + sFindOverlap+ "]");
        }
        StringSet ss3 = new StringSet();
        ss3.addAll(hsIdracNodeFields);
        ss3.add("CS_FTSCORE");
        htReturn.put ("<IDRACNODE>" , ss3);

        // <NODEDOCUMENT>
        HashSet hsNodeDocFields = UtilSets.htKeysToHashSet(CustomFields.getFields("NODE", dbc));
        hsNodeDocFields.addAll(UtilSets.htKeysToHashSet(CustomFields.getFields("DOCUMENT", dbc)));
        if ( hsNodeDocFields.size() <
                    CustomFields.getFields("NODE", dbc).size() +
                    CustomFields.getFields("DOCUMENT", dbc).size() )
            throw new Exception ("overlapping column between Node and Document tables");
        hsNodeDocFields.addAll(UtilSets.htKeysToHashSet(CustomFields.getFields("NODEDOCUMENT", dbc)));

        // LEGACY COLS FROM THE NODEDOC OBJECT WE DO NOT WANT EMITTED
        hsNodeDocFields.remove("DOCUMENTTYPE");
//        hsNodeDocFields.remove("SCORE2");
//        hsNodeDocFields.remove("SCORE3");
//        hsNodeDocFields.remove("SCORE4");
//        hsNodeDocFields.remove("SCORE5");
//        hsNodeDocFields.remove("SCORE6");
//        hsNodeDocFields.remove("SCORE7");
//        hsNodeDocFields.remove("SCORE8");
//        hsNodeDocFields.remove("SCORE9");
//        hsNodeDocFields.remove("SCORE10");
//        hsNodeDocFields.remove("SCORE11");
//        hsNodeDocFields.remove("SCORE12");

        StringSet ss4 = new StringSet();
        ss4.addAll(hsNodeDocFields);
        ss4.add("CS_FTSCORE");
        htReturn.put ("<NODEDOCUMENT>" , ss4);

        // <CORPUS>
        Hashtable htCorpusFields = CustomFields.getFields("CORPUS", dbc);
        StringSet ss5 = new StringSet();
        ss5.addAll(UtilSets.htKeysToHashSet(htCorpusFields));
        ss5.add("CS_FTSCORE");
        htReturn.put ("<CORPUS>" , ss5);

        return htReturn;
    }


    /**
     * expand objects eg <DOCUMENT> to documentid, docurl, doctitle
     */
    static String expandObjCols (Hashtable hT , String sSQL)
            throws Exception
    {
        Enumeration I = hT.keys ();
        while (I.hasMoreElements ())
        {
            String s = (String) I.nextElement ();
            sSQL = sSQL.replaceAll (s , ( (StringSet) hT.get (s) ).toStr ());
        }

        return sSQL;
    }

    static Hashtable getHTobjectColsExpans_StrToStrSetCols ()
    {
        return htObjectColsExpans_StrToStrSetCols;
    }

}
