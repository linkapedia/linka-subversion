/*
* Created by IntelliJ IDEA.
* User: henry kon
* Date: Sep 24, 2002
* Time: 7:45:00 AM
* To change template for new class use
* Code Style | Class Templates options (Tools | IDE Options).
*/
package sql4j.CQLInterpreter;

import com.indraweb.execution.Session;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.Log;
import com.indraweb.utils.oracle.CustomFields;

import java.io.PrintWriter;
import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.Hashtable;
import java.util.HashSet;
import java.util.Vector;

import api.APIProps;
import com.iw.system.*;

public class CQLxmlGen {

    private static CQLInterpret cqli = null;
    private static Integer ISynchParse = new Integer(1);

    // ********************************************************************************
    public static int topLevelCallFromTSCql
        (
            String sProp_CQLquery,
            int iStartRow_1_based,
            int iRowMax,
            PrintWriter out,
            Connection dbc,
            boolean bSkipDbConnect,
            boolean bExecuteFinalSQL,
            User user
            )
            throws Exception {
            //com.indraweb.util.Log.logClear("completed handleTSapiRequest\r\n");
        // ********************************************************************************
        //int iQid = -1;
        //double dIWScoreFloor = -1.0;
        //double dFTScoreFloor = -1.0;

        String sSelClause = com.indraweb.util.UtilStrings.getStringBetweenThisAndThat
                (sProp_CQLquery.toUpperCase(), "SELECT ", " WHERE ");
        Vector vObjectsCols = com.indraweb.util.UtilStrings.splitByStrLenLong(sSelClause, ",");

        int iObjectTypeSelected = -1;
        String sObjectSelected = ((String) vObjectsCols.elementAt(0)).trim().toUpperCase();
        sObjectSelected  = stripObject(sObjectSelected);
        if (sObjectSelected.equals("DOCUMENT"))
            iObjectTypeSelected = CQLInterpret.iObj_DOCUMENT;
        else if (sObjectSelected.equals("NODE"))
            iObjectTypeSelected = CQLInterpret.iObj_NODE;
        else if (sObjectSelected.equals("IDRACNODE"))
            iObjectTypeSelected = CQLInterpret.iObj_IDRACNODE;
        else if (sObjectSelected.equals("NODEDOCUMENT"))
            iObjectTypeSelected = CQLInterpret.iObj_NODEDOCUMENT;
        else
            throw new Exception("invalid CQL selected object type [" + vObjectsCols.elementAt(0) + "]");

        if ( false )// debug for if called from maintest without proper uudecode
        {
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            api.Log.Log("debug running sProp_CQLquery.replaceAll(%20");
            sProp_CQLquery = sProp_CQLquery.replaceAll("%20"," ");
            sProp_CQLquery = sProp_CQLquery.replaceAll("%3D"," = ");
        }

        boolean bVerbose = com.indraweb.util.UtilFile.bFileExists("/temp/IndraCQLVerbose.txt");
        if (bVerbose) com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputCQL.txt", "\r\n\r\n" +
            new java.util.Date() + "\r\n1. " + CQLInterpreterUtils.formatSQL(sProp_CQLquery));

        long lTimeStart = -1;
        CQLCombinedDataFT cds = null;
        synchronized (ISynchParse) {
            lTimeStart = System.currentTimeMillis();
            int i = 0;
            //String sCQLTranslated = null;
            int iLocalCallCounter = -1;

            cds = null;
            if (cqli == null) {
                cqli = new sql4j.CQLInterpreter.CQLInterpret(dbc);
            }

            // TRANSLATE QUERY - 2 forms potentially if analysis - INTO CDS
            try {
                // PARSE AND GEN COMPONENT / MODE QUERIES IN HERE !
                //api.Log.Log ("sProp_CQLquery [" + sProp_CQLquery + "]" );
                // ***********************
                // ***********************
                // ***********************
                // ***********************
                cds = cqli.cqlTranslateTopLevel(sProp_CQLquery, cqli, user, dbc, iObjectTypeSelected);
                // this is after all QA requirements getting back in to production model
            } catch (Exception e) {
                String sStack = com.indraweb.util.Log.stackTraceToString(e);
                String sErr = iLocalCallCounter + ". failed translate of sql [" + sProp_CQLquery + "] ";
                api.emitxml.EmitGenXML_ErrorInfo.emitException(sErr, e, out);
                api.Log.LogError(sErr + ":" + sStack, e);
                throw e;
            }
        }     // synchronized ISynchParse

        // **************************************************************
        // **************************************************************
        // TRRANSLATE ONLY? OR EXECUTE ALSO ?
        // **************************************************************
        // **************************************************************

        int iRecordCount = -1;

        long lTimeTranslate = System.currentTimeMillis() - lTimeStart;
        if (!bSkipDbConnect) {
            try {
                // ***************************************
                // ***************************************
                // MODE 1 QUERY EXECUTION
                // ***************************************
                // ***************************************
                if ( bVerbose )
                    api.Log.Log("cds.iMode [" + cds.iMode  + "]");
                if (cds.iMode == 1) // 1     // if no runqa piece
                {
                    //com.indraweb.util.Log.logClear(" using mode 1 in CQL \r\n");

                    long lTimeStartExec = System.currentTimeMillis();

                    //if (bVerbose) Log.logClear("VERY FINAL SQL / TOPIC \r\n[" + ( cds.sVeryFinalIWQuery) + "]\r\n");
                    if (bVerbose) Log.logClear("VERY FINAL SQL / TOPIC \r\n[" + CQLInterpreterUtils.formatSQL(cds.sVeryFinalIWQuery) + "]\r\n");
                    if (bVerbose) com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputCQL.txt",
                            "VERY FINAL SQL / TOPIC \r\n[" + CQLInterpreterUtils.formatSQL(cds.sVeryFinalIWQuery) + "]\r\n");

                    int iMaxRecordCount = Session.cfg.getPropInt("MaxRecordCount");
                    if (iRowMax > iMaxRecordCount) {
                        iMaxRecordCount = iRowMax;
                    } // added by MAP 2/21/03 to supercede hard limits

                    if (bVerbose) com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputCQL.txt", "3. " + CQLInterpreterUtils.formatSQL(cds.sVeryFinalIWQuery));

                    if ( bExecuteFinalSQL )
                    {
                        if ( iObjectTypeSelected == CQLInterpret.iObj_DOCUMENT )
                            iRecordCount = genOutput_DOCUMENT(cds.sVeryFinalIWQuery, iStartRow_1_based, iRowMax, out, dbc, iMaxRecordCount, cds.user, bVerbose, cds.user);
                        else if ( iObjectTypeSelected == CQLInterpret.iObj_NODE)
                            iRecordCount = genOutput_NODE(cds.sVeryFinalIWQuery, iStartRow_1_based, iRowMax, out, dbc, iMaxRecordCount, cds.user, bVerbose, cds.user);
                        else if ( iObjectTypeSelected == CQLInterpret.iObj_IDRACNODE )
                            iRecordCount = genOutput_NODE(cds.sVeryFinalIWQuery, iStartRow_1_based, iRowMax, out, dbc, iMaxRecordCount, cds.user, bVerbose, cds.user);
                        else if ( iObjectTypeSelected == CQLInterpret.iObj_NODEDOCUMENT )
                        {
                            iRecordCount = genOutput_NODEDOCUMENT(cds.sVeryFinalIWQuery, iStartRow_1_based, iRowMax, out, dbc, iMaxRecordCount, user, bVerbose, user);
                        }else
                            throw new Exception("invalid CQL selected object type iObjectTypeSelected  [" + iObjectTypeSelected  + "]");
                        if (bVerbose)
                            com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputCQL.txt", "4. " + "iRecordCount  [" + iRecordCount + "] ms [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lTimeStartExec)+ "]" );

                    } else    {
                        api.Log.Log("skipping final SQL run");
                    }
                }  // if mode == 1
                // ***************************************
/*
                // ***************************************
                // MODE 2 QUERY EXECUTION
                // ***************************************
                // ***************************************
                else if (cds.iMode == 2)  // 2 with runQA piece
                {
                    if (bVerbose) com.indraweb.util.Log.logClear(" using mode 2 in CQL \r\n");

                    String sFTConstraint = UtilStrings.getStrAfterThisToEnd1Based(
                            cds.sSQL_OriginalIWCQLQuery, "IW_FULLTEXT", 1);
                    sFTConstraint = UtilStrings.getStrAfterThisToEnd1Based(sFTConstraint, "'", 1);
                    sFTConstraint = UtilStrings.getsUpToNthOfThis_1based(sFTConstraint, "'", 1);
                    //sFTConstraint = sFTConstraint.substring(0, sFTConstraint.length()-1  );
                    sFTConstraint = "contains (FULLTEXT, '" + sFTConstraint + "' , 1) > 0 ";
                    //com.indraweb.util.Log.logClearcr("sFTConstraint :" + sFTConstraint);

                    //cds.data_DocScoreCollector_FT= new data_DocScoreCollector();
                    //Log.logClear("sSQL_OriginalIWCQLQuery : " + sSQL_OriginalIWCQLQuery+ "\r\n");
                    //Log.logClear("cds.sSQLOriSave :" + cds.sSQLVeryOriCQLSQLSave+ "\r\n");

                    StringBuffer sbSqlNew = new StringBuffer();
                    sbSqlNew.append(
                            " select DOCUMENT.DOCUMENTID, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, DOCUMENT.DOCURL, " +
                            " DOCUMENT.DOCUMENTSUMMARY, score(1)  " +
                            " from" +
                            "  DOCUMENT " +
                            "  where " +
                            " DOCUMENT.DOCURL not like '%idrac%' and " +
                            "  kkFULLTEXTkk"
                    );
                    //sFTConstraint = sFTConstraint.replaceAll("$", "\\$");
                    String sSQLOracleVeryFinal = CQLCombinedDataFT.replaceStrInStr(
                            sbSqlNew.toString(), "kkFULLTEXTkk", sFTConstraint);
                    //String sSQLFinal = sbSqlNew.toString().replaceAll("kkFULLTEXTkk", sFTConstraint);

                    cds.sVeryFinalOracleQuery = sSQLOracleVeryFinal;
                    if (bVerbose) Log.logClear("mode 2 execution cds.sVeryFinalOracleQuery  :" + cds.sVeryFinalOracleQuery + "\r\n");
                    CQLXMLGenMode2.genCombinedOutput(cds, iStartRow_1_based, iRowMax,
                            out, dbc, -1, bVerbose);
                } // mode = 2
*/
                else if (cds.iMode == 3)  // 3 back towards production - no runqa
                // detect the FT or as a separate subquery
                {
                    if (bVerbose) com.indraweb.util.Log.logClear(" using mode 3 in CQL \r\n");

                    //Log.logClear("sSQL_OriginalIWCQLQuery : " + sSQL_OriginalIWCQLQuery+ "\r\n");
                    //Log.logClear("cds.sSQLOriSave :" + cds.bInRunQAMode+ "\r\n");

                    StringBuffer sbSqlNew = new StringBuffer();
                    sbSqlNew.append(
                            " select DOCUMENT.DOCUMENTID, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, DOCUMENT.DOCURL, " +
                            " DOCUMENT.DOCUMENTSUMMARY, score(1)  " +
                            " from" +
                            "  DOCUMENT " +
                            "  where " +
                            cds.sMode3CombinedAndConstraint
                    );

                    cds.sVeryFinalOracleQuery = sbSqlNew.toString();

                    //api.Log.Log ("cds.sVeryFinalOracleQuery [" + cds.sVeryFinalOracleQuery + "]" );
                    if ( bExecuteFinalSQL )
                        CQLXMLGenMode2.genCombinedOutput(cds, iStartRow_1_based, iRowMax,
                            out, dbc, -1, bVerbose);
                } // mode = 3
                else
                    throw new Exception("invalid CQL mode " + cds.iMode);

            } catch (Exception e) {
                String sStack = com.indraweb.util.Log.stackTraceToString(e);

                api.Log.LogError("err in topLevelCallFromTSCql ", e);
                com.indraweb.util.Log.NonFatalError("err in topLevelCallFromTSCql:" + sStack, e);
                throw e;
            }
        }  // if ! translate only
        return iRecordCount;
    }

    private static String stripObject (String s )
    {
        return s.replaceAll("<","").replaceAll(">","");

    }
    // ********************************************************************************
    private static int genOutput_DOCUMENT(String sCQLTranslated,
                                          int iStartRow_1_based,
                                          int iRowMax,
                                          PrintWriter out,
                                          Connection dbc,
                                          int iMaxRecordCount,
                                          User u,
                                          boolean bVerbose,
                                          User user

                                          ) throws Exception {

        PreparedStatement ps = dbc.prepareStatement(sCQLTranslated);
        java.sql.ResultSet rs = ps.executeQuery();
        //api.Log.Log ("sCQLTranslated [" + sCQLTranslated + "] iMaxRecordCount [" + iMaxRecordCount + "]");

        int loop = 0;
        int iCountTotal = 1;
        HashSet hsIDocIDs = new HashSet();

        while (rs.next() && loop < iMaxRecordCount ) {
            com.iw.system.Document d = new com.iw.system.Document(rs);
            hsIDocIDs.add(new Integer(d.get("DOCUMENTID")));

            if (d.isAuthorized(u)) loop++;

            if (loop >= iStartRow_1_based && (iRowMax < 0 || iCountTotal <= iRowMax)) {
                if ((iCountTotal == 1) && d.isAuthorized(u)) {
                    out.println("<DOCUMENTS>");
                }

                String sCountry = null;

                if (d.get("DOCUMENTSUMMARY") != null) {
                    d.set("DOCUMENTSUMMARY", d.get("DOCUMENTSUMMARY").replace('\r', ' '));
                    d.set("DOCUMENTSUMMARY", d.get("DOCUMENTSUMMARY").replaceAll("" + '\n', " "));
                }
                if (d.get("DOCTITLE") != null) {
                    d.set("DOCTITLE", d.get("DOCTITLE").replace('\r', ' '));
                    d.set("DOCTITLE", d.get("DOCTITLE").replaceAll("" + '\n', " "));
                }

                if ( d.emitXML(out, user, true) )
                    iCountTotal++;

            }
        }

        if (iCountTotal > 1)
            out.println("</DOCUMENTS>");
        rs.close();
        ps.close();
        out.println("<NUMRESULTS>" + loop + "</NUMRESULTS>");
        //if (bVerbose) com.indraweb.util.Log.logClearcr("hsIDocIDs count [" + hsIDocIDs.size() + "] from mode 1 genOutput_DOCUMENT [" + hsIDocIDs + "]");

        return loop;

    }

    // ********************************************************************************
    private static String cleanBadCharacters(String s) {
        if (s == null) {
            return s;
        }
        String sNew = new String();
        for (int k = 0; k < s.length(); k++) {
            int ichar = (int) s.charAt(k);
            if (ichar > 300) {
                api.Log.Log("Warning! Found illegal character with ascii value: " + ichar);
            }
            else {
                sNew = sNew + s.charAt(k);
            }
        }

        return sNew;
    }

    // ********************************************************************************
    private static int genOutput_NODE(String sCQLTranslated,
                                      int iStartRow_1_based,
                                      int iRowMax,
                                      PrintWriter out,
                                      Connection dbc,
                                      int iMaxRecordCount,
                                      User u,
                                      boolean bVerbose,
                                      User user
                                      ) throws Exception {

        PreparedStatement ps = dbc.prepareStatement(sCQLTranslated);
        java.sql.ResultSet rs = ps.executeQuery();

        int iCountTotal = 1;
        int loop = 0;

        while (rs.next() && loop < iMaxRecordCount ) {
            Node n = new Node(rs);
            if (n.isAuthorized(u)) loop++;

            if (loop >= iStartRow_1_based && (iRowMax < 0 || iCountTotal <= iRowMax)) {
                if ((iCountTotal == 1) && n.isAuthorized(u)) {
                    out.println("<NODES>");
                }

                if ( n.emitXML(out, u, true))
                    iCountTotal++;
            }
        }

        if (iCountTotal > 1)
            out.println("</NODES>");
        out.println("<NUMRESULTS>" + loop + "</NUMRESULTS>");
        rs.close();
        ps.close();
        return loop;
    }

    // ********************************************************************************
    // ********************************************************************************
    private static int genOutput_NODEDOCUMENT(String sCQLTranslated,
                                      int iStartRow_1_based,
                                      int iRowMax,
                                      PrintWriter out,
                                      Connection dbc,
                                      int iMaxRecordCount,
                                      User u,
                                      boolean bVerbose,
                                      User user
                                      ) throws Exception {

        PreparedStatement ps = dbc.prepareStatement(sCQLTranslated);
        java.sql.ResultSet rs = ps.executeQuery();

        int index = sCQLTranslated.toLowerCase().indexOf(" from ", 0);
        //sCQLTranslated = sCQLTranslated.substring(0, index).replaceAll("DOCUMENTSUMMARY", "DOCUMENTSUMMARY,DOCUMENT.DOCUMENTSUMMARY2,DOCUMENT.DOCUMENTSUMMARY3") +
        //        sCQLTranslated.substring(index, sCQLTranslated.length());
        //if (bVerbose) api.Log.Log("<NODEDOC> sCQLTranslated [" + sCQLTranslated+ "]");

        int iCountTotal = 1;
        int loop = 0;

            while (rs.next() && loop < iMaxRecordCount ) {
                NodeDocument nd = new NodeDocument(rs);
                if (nd.isAuthorized(u)) loop++;

                if (loop >= iStartRow_1_based && (iRowMax < 0 || iCountTotal <= iRowMax)) {

                    if ((iCountTotal == 1) && nd.isAuthorized(u)) {
                        out.println("<NODEDOCUMENTS>");
                    }

                    if ( nd.emitXML(out, u, true))
                        iCountTotal++;
                }
            }

        if (iCountTotal > 1)
            out.println("</NODEDOCUMENTS>");
        out.println("<NUMRESULTS>" + loop + "</NUMRESULTS>");
        rs.close();
        ps.close();
        return loop;
    }



}
