/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 27, 2002
 * Time: 1:45:08 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import com.indraweb.util.*;
import com.indraweb.execution.Session;
import com.iw.system.User;

import java.sql.*;
import java.io.*;
import java.util.Vector;
import java.util.HashSet;


public class CQLCombinedDataFT {  // for return form interpreter and storing data

    // two queries - IW and FT
    public String sSQL_OriginalIWCQLQuery = null;
    public String sCQLVerbatim = null;

    public String sMode3CombinedAndConstraint = null;

    public Vector vSSQLInsertsForCombinedTable = new Vector();

    // SCORE COLLECTOR CONTENT
    public data_DocScoreCollector    data_DocScoreCollectorIWPiece = null; // IW Agg score
    public data_DocScoreCollector    data_DocScoreCollector_FT = null; // ORACLE Full Text score

    public double dScoreFloorFtimesC = -1;
    public double dScoreFloorFTScore = -1;
    public String sNodeConstraint = null;
    public String sFTAnd = null;
    public int iTransactionID  = -1;

    public String sVeryFinalOracleQuery = null;
    public String sVeryFinalIWQuery = null;

    public User user = null; // added 7/31/04 - administrator may run as different users

    public int iQid = -1;

    public int iMode = -1;
    public boolean bMode_InCombinedFulltextNull = false;

    public void populateData_IW (
                                  int iStartRow_1_based ,
                                  int iRowMax,
                                  PrintWriter out,
                                  Connection dbc,
                                  boolean bVerbose)
    throws Exception
    {
        data_DocScoreCollectorIWPiece = new data_DocScoreCollector();
        String sSQLFinal = null;
        if ( this.iMode != 3 )
        {
            String sConstraintFTAnd = "";
            if ( this.sFTAnd != null ) {
                sConstraintFTAnd = " contains (FULLTEXT, " + this.sFTAnd + ", 1 ) > 0 and ";
            }

            StringBuffer sbSqlNew = new StringBuffer();

            sbSqlNew.append(
                " select DOCUMENT.DOCUMENTID, nda.nodeid, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, " +
                    " DOCUMENT.DOCURL, " +
                    " DOCUMENT.DOCUMENTSUMMARY, score1, score5, score6 " +
                " from" +
                "  DOCUMENT, nodedocument ndA" +
                "  where " +
                    // " DOCUMENT.DOCURL not like '%idrac%' and " +
                        sConstraintFTAnd +
                    "  ndA.documentid = DOCUMENT.DOCUMENTID " +
                //    "  ndA.score1 > 0 " +
                //"  and ##NODECONSTRAINT## order by (score5*score6) DESC "
                    "  and ##NODECONSTRAINT## "
                );

            sSQLFinal = sbSqlNew.toString().replaceAll("##NODECONSTRAINT##", sNodeConstraint);


        }  // mode == 2 only I believe
        else // in mode 3 which is combined FT (mode 2 was quality FT)
        {
/*
            SELECT WHERE NODEID ='421448' AND FULLTEXT LIKE 'Osteroporosis'
            COMBINE FULLTEXT LIKE '(Osteoporosis ACCUM bone loss)' and
            (docurl not like '%(Link: www.idrac%')www.idrac%')
            SCOREMIN(TopicMin=.2,FTMin=20)
*/
/*            String sCQLCln = cleanStringWhiteSpace( sSQLVeryOriCQLSQLSave );
            String sCQLClnLow  = sCQLCln.toLowerCase();
            int i = 0;
            //Log.logClear("sSQL_OriginalIWCQLQuery : " + sSQL_OriginalIWCQLQuery+ "\r\n");
            //Log.logClear("sSQL_FullTextModeOracle_2:" + sSQL_FullTextModeOracle_2+ "\r\n");
            //Log.logClear("sSQLOriSave :" + sSQLOriSave+ "\r\n");
            if ( sNodeConstraint == null )  // take user original string for node constraint
            {

            }
                sNodeConstraint = UtilStrings.getStrAfterThisToEnd1Based(sORIToUse, "where", 1);
                sNodeConstraint = UtilStrings.getsUpToNthOfThis_1based( sNodeConstraint, "iw_fulltext", 1);
                //Log.logClear("sNodeConstraint :" + sNodeConstraint + "\r\n");
            }*/
            //String sCQLCleaned = cleanStringWhiteSpace (
            StringBuffer sbSqlNew = new StringBuffer();


            String sNodeCorpusConstraint_HackToMarryTopicAndFT =
                    getMode3NodeCorpusConstraint_HackToMarryTopicAndFT(this.sCQLVerbatim);

            //Log.logClear("extra piece going into sVeryFinalIWQuery \r\n[" + this.sVeryFinalIWQuery + "]\r\n");
//            Log.logClear("extra piece going into IW GET VERY FINAL \r\n[" + UtilStrings.getStrAfterThisToEnd1Based(this.sVeryFinalIWQuery, "where", 1 ) + "]\r\n");
/*
            "SELECT <DOCUMENT> WHERE NODEID ='421448' AND FULLTEXT LIKE 'Osteroporosis'  "+
                " COMBINE FULLTEXT LIKE '(Osteoporosis ACCUM bone loss)' and " +
                " (docurl not like '%www.idrac%') " +
                " SCOREMIN(TopicMin=.1,FTMin=7)   "
*/

            String sCQLCln = cleanStringWhiteSpace(this.sCQLVerbatim);
            sCQLCln = sCQLCln.replaceAll(" WHERE ", " where ")  ;
            sCQLCln = sCQLCln.replaceAll(" COMBINE FULLTEXT LIKE", " combine fulltext like");
/*  FT INDEXED COLS
            hs.add("DOCTITLE");
            hs.add("DOCUMENTSUMMARY");
            hs.add("NODETITLE");
            hs.add("FULLTEXT");
*/
            String sCQLCln_userWhereClauseOnly = UtilStrings.getStringBetweenThisAndThat(
                    sCQLCln, "where", "combine fulltext");
            String sLower = sCQLCln_userWhereClauseOnly.toLowerCase();
            boolean bIncludingNodeTable = sLower.indexOf("nodetitle") > 0 ||
                 sLower.indexOf("nodestatus") > 0   ||
                 sLower.indexOf("datescanned") > 0 ||
                sLower.indexOf("dateupdated") > 0 ||
                sLower.indexOf("corpusid") > 0;


            sCQLCln_userWhereClauseOnly= sCQLCln_userWhereClauseOnly.replaceAll("NODEID", "nodeid");

            if ( bIncludingNodeTable )
                sCQLCln_userWhereClauseOnly= sCQLCln_userWhereClauseOnly.replaceAll("nodeid", "n.nodeid");

            sCQLCln_userWhereClauseOnly = likeToContainsExpandCleanedWhereClause (
                    sCQLCln_userWhereClauseOnly);

            boolean bIncludeNodeTable = false;
            sbSqlNew.append (
                " select DOCUMENT.DOCUMENTID, nd.nodeid, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, " +
                    " DOCUMENT.DOCURL, " +
                    " DOCUMENT.DOCUMENTSUMMARY, score1, score5, score6 " +
                " from" );
            if (bIncludingNodeTable)
            {    // if any node table specific cols must add that table
                sbSqlNew.append (
                    "  DOCUMENT, nodedocument nd, node n" +
                    "  where " +
                        "  nd.nodeid = n.nodeid and " +
                        "  nd.documentid = DOCUMENT.DOCUMENTID " +
                        " and  ( " +
                        sCQLCln_userWhereClauseOnly
                        + " )"
                );
            }
            else
            {
                sbSqlNew.append (
                    "  DOCUMENT, nodedocument nd" +
                    "  where " +
                        "  nd.documentid = DOCUMENT.DOCUMENTID " +
                        " and  ( " +
                        sCQLCln_userWhereClauseOnly
                        + " )"
                );
            }


            sSQLFinal = sbSqlNew.toString();

            //api.Log.Log("sSQLFinal [" + sSQLFinal + "]" );
            sSQLFinal = sSQLFinal.replaceAll(" CORPUSID", " N.CORPUSID");
            sSQLFinal = sSQLFinal.replaceAll(" corpusid", " n.corpusid");
            //api.Log.Log("sSQLFinal [" + sSQLFinal + "]" );

        } // mode 3

        //if ( Session.cfg.getPropBool("CQLTopicSide_OrderBy"))
        sSQLFinal = sSQLFinal + " order by score5 * score6 desc ";

        if ( bVerbose ) Log.log("SQL FINAL, TOPIC SIDE, CQL MODE [" +  this.iMode + "] GET VERY FINAL \r\n[" + CQLInterpreterUtils.formatSQL(sSQLFinal) + "]\r\n");

        // NOW GET THE IW DATA
        if ( dbc != null )
        {
            Statement stmt = dbc.createStatement();
            java.sql.ResultSet rs = stmt.executeQuery (sSQLFinal);

            int loop = 0;
            int iNumOutput = 0;
            int iCountTotal = 0;
            boolean bAnyRecs = true;
            HashSet hsIDocIDs = new HashSet();
            while ( rs.next() )
            {
                loop++;
                iCountTotal++;
                if ( loop >= iStartRow_1_based && (iRowMax < 0 || iNumOutput < iRowMax) )
                {
                    iNumOutput++;

                    int iDocucmentID = rs.getInt(1);
                    hsIDocIDs.add ( new Integer (iDocucmentID));
                    int iNodeID = rs.getInt(2);
                    int iGenreID = rs.getInt(3);
                    String sDocTitle = rs.getString(4);
                    String sDocURL = rs.getString(5);
//                    com.indraweb.util.Log.logClearcr(iNumOutput + ". sDocURL [" + sDocURL  + "]");
                    String sDocumentSummary = rs.getString(6);
                    double dScore1 = rs.getDouble(7);
                    double dScore5 = rs.getDouble(8);
                    double dScore6 = rs.getDouble(9);

                    if ( iNodeID < 1 )        {
                        int idebugme = 0;
                    }
                    data_DocScoreCollectorIWPiece.add(
                            iDocucmentID,
                            iNodeID,
                            iGenreID,
                            sDocTitle,
                            sDocURL,
                            sDocumentSummary,
                            dScore1,
                            dScore5,
                            dScore6 ,
                            -1.0,
                            -100
                    );

                }
            }
            if ( bVerbose ) com.indraweb.util.Log.logcr("Doc count topic side CQL MODE [" +  this.iMode + "]  count [" + hsIDocIDs.size() + "] from mode [" + this.iMode + "] set of topic docid's [" + hsIDocIDs + "]");
            if ( bVerbose ) Log.logClear("*** num TOPIC source recs [" + iCountTotal + "] *** \r\n");
            rs.close();
            stmt.close();
        }
        else {
            com.indraweb.util.Log.NonFatalError("DBC IS NULL 1\r\n");
            if ( bVerbose ) Log.logClear("*** num IW source recs [0] (dbc null) \r\n");
        }

    }                   // end sub

    public void populateData_Oracle (
                                  int iStartRow_1_based ,
                                  int iRowMax,
                                  PrintWriter out,
                                  Connection dbc,
                                  boolean bVerbose
                                  )
    throws Exception
    {
        data_DocScoreCollector_FT = new data_DocScoreCollector();

        //        if ( Session.cfg.getPropBool("CQLFTSide_OrderBy"))
        //            sVeryFinalOracleQuery= sVeryFinalOracleQuery+ " order by score(1) desc ";

        if ( bVerbose ) Log.log("FT GET VERY FINAL in populateData_Oracle \r\n[" + CQLInterpreterUtils.formatSQL(sVeryFinalOracleQuery) + "]\r\n");

        if ( dbc != null )
        {
            Statement stmt = dbc.createStatement();
            java.sql.ResultSet rs = stmt.executeQuery (sVeryFinalOracleQuery);

            int loop = 0;
            int iNumOutput = 0;
            int iCountTotal = 0;
            boolean bAnyRecs = true;
            HashSet hsIDocIDs = new HashSet();
            while ( rs.next() )
            {
                loop++;
                iCountTotal++;
                if ( loop >= iStartRow_1_based && (iRowMax < 0 || iNumOutput < iRowMax) )
                {
                    iNumOutput++;

                    int iDocucmentID = rs.getInt(1);
                    hsIDocIDs.add ( new Integer (iDocucmentID));
                    int iGenreID = rs.getInt(2);
                    String sDocTitle = rs.getString(3);
                    String sDocURL = rs.getString(4);
//                    com.indraweb.util.Log.logClearcr (iNumOutput + ". sDocURL [" + sDocURL  + "]");
                    String sDocumentSummary = rs.getString(5);
                    double dScore1_ora = rs.getDouble(6);

                    data_DocScoreCollector_FT.add(
                            iDocucmentID,
                            -1,  // no node in oracle only
                            iGenreID,
                            sDocTitle,
                            sDocURL,
                            sDocumentSummary,
                            -1.0,
                            -1.0,
                            -1.0,
                            dScore1_ora,
                            -100
                    );

                }
            }
            if ( bVerbose ) com.indraweb.util.Log.logcr("FT SIDE MODE [" +  this.iMode + "] unique docIDs [" + hsIDocIDs.size() +
                "] over [" + iCountTotal + "] nodedoc records, in CQL mode [" + this.iMode + "] set of FT docid's [" + hsIDocIDs + "]");
            //Log.logClear("*** num FT source recs [" + iCountTotal + "] *** \r\n");

            rs.close();
            stmt.close();
        }
        else {
            com.indraweb.util.Log.NonFatalError("DBC IS NULL 2\r\n");
            if ( bVerbose ) Log.logClear("*** num FT source recs [0] (dbc null) \r\n");
        }
    }                   // end sub

    // **********************************************
	public static String replaceStrInStr (String s, String replace, String with)
	// **********************************************
	{
		int dontReplaceInsideReplacedStringCounter = 0;
		try
		{
			while (true)
			{
				// don't recurse infinitely
				String testStr = s.substring( dontReplaceInsideReplacedStringCounter ) ;
				int replaceableLoc = testStr.indexOf ( replace );
				if ( replaceableLoc >= 0 )
				{
					//if ( replaceableLoc < testStr.length() )
					//{
						String x1 = s.substring ( 0, replaceableLoc + dontReplaceInsideReplacedStringCounter );
						String x2 = s.substring ( dontReplaceInsideReplacedStringCounter + replaceableLoc + replace.length());
						s =  x1 + with + x2;
					//}
					//else
					//{
						//s =  s.substring ( 0, replaceableLoc + dontReplaceInsideReplacedStringCounter ) +
							//with
						//;
					//}

					dontReplaceInsideReplacedStringCounter += (replaceableLoc + with.length()  );
					//dontReplaceInsideReplacedStringCounter += (replaceableLoc + replace.length() + 1 );
				}
				else
				{
					return s;
				}
			}
		}
		catch ( Exception e )
		{
			Log.FatalError ("replaceStrInStr" + s + "," + replace + "," + with, e );
		}
		return null;

	}
    public static String cleanStringWhiteSpace ( String s )
    {
        s = s.replaceAll("\n", " ");
        s = s.replaceAll("\t", " ");
        s = s.replaceAll("\r", " ");
        s = convertAllDoubleSpaceToSingleSpaces(s);
        return s;
    }

    public static String convertAllDoubleSpaceToSingleSpaces ( String s )
	{
        while (true)
        {
            int iLenPre = s.length();
            s = s.replaceAll("  ", " ");
            if ( s.length() == iLenPre )
                break;
        }
		return s;
	}

    private static String getMode3NodeCorpusConstraint_HackToMarryTopicAndFT ( String sCQL )
    throws Exception
    {
        String sCQLLow = sCQL.toLowerCase();
        int iLocWHERE = sCQLLow.indexOf("where");

        if ( iLocWHERE < 0 )
            throw new Exception ("no 'where' in CQL [" + sCQL  + "]");

//        int iLocAfter   Where = iLocWHERE + 5;
        //int iLocWHERE = sCQLLow.indexOf("combine");

         //if ( iLocWHERE < 0 )
           //  throw new Exception ("no 'where' in CQL [" + sCQL  + "]");
//        iLocCOMBINE =
        return null;
    }

    private static String likeToContainsExpandCleanedWhereClause (String sCQLWhereClause)
    throws Exception
    {
        String [] sArrColsNeedingLikeToContains = {
            // which columns have intermedia fulltext index
                                                   "fulltext",
                                                   "doctitle",
                                                   "documentsummary",
                                                   "nodetitle"
//                                                   "docurl"
                                                   };

        sCQLWhereClause= sCQLWhereClause.replaceAll("LIKE ", "like ");
        sCQLWhereClause= sCQLWhereClause.replaceAll("LIKE ", "like ");
        for ( int i = 0;  i < sArrColsNeedingLikeToContains.length; i++ )
        {
            try {
                sCQLWhereClause = likeToContainsExpandCleanedWhereClause_subProcessor(sCQLWhereClause,
                    sArrColsNeedingLikeToContains[i]);
            }
            catch ( Exception e ) {
                com.indraweb.util.Log.FatalError("failed to translate like to contains for col [" +
                    sArrColsNeedingLikeToContains [i] + "]", e);
                throw e;
            }
        }
        //com.indraweb.util.Log.logClearcr("sCQLWhereClause[" + sCQLWhereClause+ "]");
        return sCQLWhereClause;
    }

    private static String likeToContainsExpandCleanedWhereClause_subProcessor(String sCQLWhereClause,
                                                                              String sColToDo)
        throws Exception
    {
        // FULLTEXT LIKE '(Osteoporosis ACCUM bone loss)'
        //com.indraweb.util.Log.logClearcr("sCQLWhereClause [" + sCQLWhereClause + "]");
        sCQLWhereClause= sCQLWhereClause.replaceAll(sColToDo.toUpperCase() + " " , sColToDo.toLowerCase() + " ");

        String sColLikePreKeyString = sColToDo.toLowerCase() + " like";
        while ( true )
        {
            String sPre = sCQLWhereClause;
            int iLoc_EndLikePreArg = sCQLWhereClause.indexOf ( sColLikePreKeyString );
            if ( iLoc_EndLikePreArg == -1 )  // if all FT constraints on this col are converted
                break;
            int i = sCQLWhereClause.indexOf(")'", iLoc_EndLikePreArg);
            if ( i == -1 )
                throw new Exception ("FT cols must surround arguments by '( and )' not the case in [" + sCQLWhereClause + "]" );
            int iLoc_EndThisColLike_AtomicWhereClause= 2 + sCQLWhereClause.indexOf(")'", iLoc_EndLikePreArg);

            String sFTAnd = sCQLWhereClause.substring(
                    iLoc_EndLikePreArg + sColLikePreKeyString.length(),
                     iLoc_EndThisColLike_AtomicWhereClause);
            String sFTConsrtaint = " contains (" + sColToDo + ", " + sFTAnd + " ) > 0 ";
            String sCQLPrefix = sCQLWhereClause.substring(0, iLoc_EndLikePreArg);
            String sCQLSuffix = sCQLWhereClause.substring(iLoc_EndThisColLike_AtomicWhereClause);

            sCQLWhereClause = sCQLPrefix + sFTConsrtaint + sCQLSuffix;
        }
        return sCQLWhereClause;
    }

}          // end class
