/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Sep 2, 2002
 * Time: 5:40:39 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import com.indraweb.execution.Session;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import com.indraweb.utils.oracle.CustomFields;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;
import sql4j.parser.*;
import sql4j.schema.Schema;
import sql4j.util.StringSet;

public class CQLInterpret {

    public final static int iObj_DOCUMENT = 1;
    public final static int iObj_NODE = 2;
    public final static int iObj_IDRACNODE = 3;
    public final static int iObj_NODEDOCUMENT = 4;
    public static boolean bRunWithSecurity_NodeCorpus = false;
    public static boolean bRunWithSecurity_Document = false;
    boolean bDebugging = false;
    boolean bDebugNoUserObject_noLdap = bDebugging;

    /**
     * construct CQL interpreter class
     */
    public CQLInterpret(Connection dbc) throws Exception {
        CQLSchema.mapKnownTablesToCols_StrToStrSet = CQLSchema.defineWhereKnownColsComeFrom(dbc);
        CQLSchema.hmUnknownColsCandidates_StrColToStrSetTabs = CQLSchema.defineWhereUnkColsCanComeFrom();
        CQLSchema.htObjectColsExpans_StrToStrSetCols = CQLSchema.defineObjectColsExpans_htStrToStrSetCols(dbc);
        //CQLSchema.hsTextIndexedCols = CQLSchema.defineWhichColsAreTextIndexed (true, dbc);

        CQLSchema.schema = new Schema(CQLSchema.mapKnownTablesToCols_StrToStrSet);

    }

    /**
     * connect strings with connector - util - build AND and comma lists
     */
    /**
     * TOP LEVEL translate SQL into CQL SQL
     */
    public CQLCombinedDataFT cqlTranslateTopLevel(final String sCQLVerbatim,
            CQLInterpret cqli,
            User user,
            Connection dbc,
            int iObjectTypeSelected)
            throws Exception {
        synchronized (this) {
            if (com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebugSaveCQLQueries.txt")) {
                UtilFile.addLineToFile("/temp/IndraDebugSaveCQLQueries.txt", sCQLVerbatim + "\r\n");
            }
        }

        boolean bVerbose = com.indraweb.util.UtilFile.bFileExists("/temp/IndraCQLVerbose.txt");

        if (bVerbose) {
            com.indraweb.util.Log.logClear(
                    "STEP 1 ORIGINAL CQL\r\n[" + CQLInterpreterUtils.formatSQL(sCQLVerbatim) + "]\r\n");
        }

        CQLCombinedDataFT cds = new CQLCombinedDataFT(); // no qid

        String sCQLMorphing = com.indraweb.util.UtilStrings.cleanStringWhiteSpaceToSpaces(sCQLVerbatim, true).trim();
        //api.Log.Log ("sCQLMorphing 100 [" + sCQLMorphing + "]" );
        // SECURITY:"CN=Prototype User,CN=Users,DC=indraweb"

        String sSecuritySpecAlertsInBand = null;
        String sDocSecIDsAlertsList = null;
        // e.g. still "SELECT <DOCUMENT> WHERE NODEID = 16717 ORDER BY DOCUMENTID"
        if (sCQLMorphing.endsWith("\"")) {
            int iIndexSecuritySpec = sCQLMorphing.toUpperCase().indexOf("SECURITY:\"CN=");
            if (iIndexSecuritySpec > 0) {
                int iHeadLen = "SECURITY:\"".length();
                sSecuritySpecAlertsInBand = sCQLMorphing.substring(iIndexSecuritySpec);
                sCQLMorphing = sCQLMorphing.substring(0, iIndexSecuritySpec).trim();
                sSecuritySpecAlertsInBand = sSecuritySpecAlertsInBand.substring(iHeadLen, sSecuritySpecAlertsInBand.length() - 1);
            }
            long lStart = System.currentTimeMillis();

            cds.user = User.getUser(sSecuritySpecAlertsInBand, dbc);

            //HashSet hsSecIDs = user.GetSecurityIdentifiers (sSecuritySpecAlertsInBand , dbc);
            //api.Log.Log   ("time in user.GetSecurityIdentifiers ms [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart) + "]");
            //sDocSecIDsAlertsList = com.indraweb.util.UtilSets.hsToStr (hsSecIDs , "," , true);
            //api.Log.Log("SECURITYSPEC [" + sSecuritySpecAlertsInBand+ "] sSecIDsAlertsList  [" + sSecIDsAlertsList + "]" );
        } else {
            cds.user = user;
        }

        // e.g. still "SELECT <DOCUMENT> WHERE NODEID = 16717 ORDER BY DOCUMENTID"
        cds.sCQLVerbatim = sCQLVerbatim;

        String sSQLForParser_UsedBelow = null;

        // 2003 03 30 String sCQLSQLWorking = sCQLSQLVeryOri;
        // THIS LINE is causing IDRAC problems
        sCQLMorphing = errorCheckAndNormalizeCQL(sCQLMorphing);  // initial error checks
        // code FULLTEXT NEAR opertor as a like
        sCQLMorphing = CQLInterpretPreprocessors.convertNearToCodedLike(sCQLMorphing, dbc);
        // code NODEID UNDER opertor as a like
        sCQLMorphing = CQLInterpretPreprocessors.convertNodeIDUnderToCodedLike(sCQLMorphing);

        // *****************************************************
        // MODE = 2 IW_FULLTEXT QA ETC FILL / CDS
        // *****************************************************
        int iLocationIWFulltext = sCQLMorphing.toLowerCase().indexOf("iw_fulltext");
        int iLocation_CombineFulltext = sCQLMorphing.toLowerCase().indexOf("combine fulltext like");
        int iLocation_CombineFulltextNull = sCQLMorphing.toLowerCase().indexOf("combine fulltext null");

        if (iLocation_CombineFulltext > 0) {
            cds.iMode = 3;
            int iLocation_CombineFulltextLOWER = sCQLMorphing.indexOf("combine fulltext like");
            int iLocation_CombineFulltextUPPER = sCQLMorphing.indexOf("COMBINE FULLTEXT LIKE");
            if (iLocation_CombineFulltextUPPER < 0 && iLocation_CombineFulltextLOWER < 0) {
                throw new Exception("COMBINE FULLTEXT LIKE directive can not be in mixed case");
            }

            int iLocationScoreMins = sCQLMorphing.toLowerCase().indexOf("scoremin");
            if (iLocationScoreMins < 0) {
                sCQLMorphing = sCQLMorphing + " SCOREMIN (topicmin=.2,ftmin=1)";
            }
            //String sMode3sAllPreCombined = UtilStrings.getsUpToNthOfThis_1based(
            //      sCQLSQLWorking, "COMBINE FULLTEXT LIKE",1).trim();


            int iLocScoreMin = sCQLMorphing.toLowerCase().indexOf("scoremin");
            String sMode3sScoreMins_parmlist = null;

            if (iLocScoreMin > 0) {
                sMode3sScoreMins_parmlist = UtilStrings.getStrAfterThisToEnd1Based(
                        sCQLMorphing.toLowerCase(), "scoremin", 1).trim();
            } else {
                sMode3sScoreMins_parmlist = "(topicmin=.2,ftmin=1)";
            }

            // BREAK UP
            // COMBINE FULLTEXT LIKE '(Osteoporosis accru bone loss)' and
            // docurl not like 'http:www.idrac%' SCOREMIN(TopicMin=.1,FTMin=7)"
            {
                // ** START
                String s1AllBetweenCFTLandSCOREMIN = UtilStrings.getStringBetweenThisAndThat(
                        sCQLMorphing, "COMBINE FULLTEXT LIKE", "SCOREMIN").trim();
                // **
                // now have s1AllBetweenCFTLandSCOREMIN = '(Osteoporosis accru bone loss)' and
                // docurl not like 'http:www.idrac%'

                s1AllBetweenCFTLandSCOREMIN = s1AllBetweenCFTLandSCOREMIN.replaceAll("\\) \'", ")'");
                int iLocEndLikeParam = s1AllBetweenCFTLandSCOREMIN.indexOf(")'", 2) + 2;

                int iLoc_AND =
                        s1AllBetweenCFTLandSCOREMIN.indexOf("AND", iLocEndLikeParam);
                int iLOC_and =
                        s1AllBetweenCFTLandSCOREMIN.indexOf("and", iLocEndLikeParam);
                iLoc_AND = com.indraweb.util.UtilSets.max(iLOC_and, iLoc_AND);
                String sLikeParm = s1AllBetweenCFTLandSCOREMIN.substring(0, iLocEndLikeParam).trim();
                // sLikeParm = '(Osteoporosis accru bone loss)'

                //for the and after combine fulltext in
                //"SELECT <DOCUMENT> WHERE NODETITLE LIKE 'Osteoporosis' AND " +
                // "    FULLTEXT LIKE 'bones and procedure and health and twist' " +
                //" COMBINE FULLTEXT LIKE '(Osteoporosis bone loss)' and (docurl not like 'http:www.idrac' or DATELASTFOUND = SYSDATE) SCOREMIN(TopicMin=.1,FTMin=7)"
                String sAdditionalWhereConstraint = "";
                if (iLoc_AND > 0) {
                    sAdditionalWhereConstraint = s1AllBetweenCFTLandSCOREMIN.substring(iLocEndLikeParam + 1);
                }
                //            String s2LikeParamInS2 = UtilStrings.getStringBetweenThisAndThat (
                //                    sCQLSQLWorking, "COMBINE FULLTEXT LIKE", "SCOREMIN").trim();

                // get the additional where constraint in FT
                //            String s2AllAfterLikeParamInS2= UtilStrings.getStringBetweenThisAndThat (
                //                    sCQLSQLWorking, "COMBINE FULLTEXT LIKE", "SCOREMIN").trim();


                if (sMode3sScoreMins_parmlist.toLowerCase().indexOf("topicmin") < 0) {
                    throw new Exception("scoremin directive must contain topicmin e.g., as in 'SCOREMIN(topicmin=.2,ftmin=1)'");
                }
                if (sMode3sScoreMins_parmlist.toLowerCase().indexOf("ftmin") < 0) {
                    throw new Exception("scoremin directive must contain ftmin e.g., as in 'SCOREMIN(topicmin=.2,ftmin=1)'");
                }
                cds.dScoreFloorFtimesC = Double.parseDouble(getParm(sMode3sScoreMins_parmlist.toLowerCase(), "topicmin"));
                cds.dScoreFloorFTScore = Integer.parseInt(getParm(sMode3sScoreMins_parmlist.toLowerCase(), "ftmin"));

                sSQLForParser_UsedBelow = sCQLMorphing.substring(0, iLocation_CombineFulltext);
                //String sSqlHeadIWCQLQuery = com.indraweb.util.UtilStrings.getStringBetweenThisAndThat (
                //        sCQLMorphing , "SELECT" , "WHERE");
                //System.out.println("sSqlHeadIWCQLQuery:"+sSqlHeadIWCQLQuery);
                cds.sMode3CombinedAndConstraint =
                        " contains (FULLTEXT, " + sLikeParm + ", 1) > 0 " + sAdditionalWhereConstraint;
                //          com.indraweb.util.Log.logClear("cds.sMode3CombinedAndConstraint [" + cds.sMode3CombinedAndConstraint + "]\r\n");
                //com.indraweb.util.Log.logClear("mode 3 cds.sVeryFinalOracleQuery  [" + cds.sVeryFinalOracleQuery + "]\r\n");
                //com.indraweb.util.Log.logClear("mode 3 sSQLForParser_UsedBelow [" + sSQLForParser_UsedBelow + "]\r\n");
            }

        } // mode 3
        else if (iLocation_CombineFulltextNull > 0) // mode 3 sub 2
        {
            cds.iMode = 3;
            cds.bMode_InCombinedFulltextNull = true;
            cds.dScoreFloorFTScore = 1;

            int iLocation_CombineFulltextLOWER = sCQLMorphing.toLowerCase().indexOf("combine fulltext null");

            sSQLForParser_UsedBelow = sCQLMorphing.substring(0, iLocation_CombineFulltextLOWER);

        } // mode 3 sub 2
        else {
            cds.iMode = 1;
            sSQLForParser_UsedBelow = sCQLMorphing;
        }

        if (cds.iMode != 3) // hand manipulation of user query this mode  // true for production
        {
            // ********************************
            // PICK OFF ORDER BY
            // ********************************
            String sOrderBY = "";
            int iLocOrderBy = sCQLMorphing.toLowerCase().indexOf("order by");
            if (iLocOrderBy > 0) {
                //sOrderBY = sCQLMorphing.toLowerCase ().substring (iLocOrderBy);
                sOrderBY = sCQLMorphing.substring(iLocOrderBy);
            }


            int iLocOrderByInSQL = sSQLForParser_UsedBelow.toLowerCase().lastIndexOf("order by");
            if (iLocOrderByInSQL > 0) {
                sSQLForParser_UsedBelow = sSQLForParser_UsedBelow.substring(0, iLocOrderByInSQL);
            }
            sSQLForParser_UsedBelow = sSQLForParser_UsedBelow.trim();
            String sSQLForParser_lower = sSQLForParser_UsedBelow.toLowerCase().trim();
            //api.Log.Log ("sSQLForParser_lower  [" + sSQLForParser_lower + "]\r\n");

            if (!sSQLForParser_lower.startsWith("select ")) {
                throw new Exception("CQL supports select queries only, and not [" + sCQLVerbatim + "]");
            }
            if (!(sSQLForParser_lower.indexOf(" where ") > 0)) {
                throw new Exception("no where clause found in CQL [" + sCQLVerbatim + "]");
            }

            int iIndexWhere = sSQLForParser_UsedBelow.toLowerCase().indexOf(" where ");
            String sSQLOri_UpperPostSelectPreWhere = sSQLForParser_lower.substring(6, sSQLForParser_lower.indexOf(" where ")).toUpperCase();
            sSQLOri_UpperPostSelectPreWhere = sSQLOri_UpperPostSelectPreWhere.trim();
            if (sSQLOri_UpperPostSelectPreWhere.indexOf("<") < 0) {
                sSQLOri_UpperPostSelectPreWhere = " " + "<" + sSQLOri_UpperPostSelectPreWhere + ">";
            }


            String sSQLOri_UpperPostWhereMixed = sSQLForParser_UsedBelow.substring(iIndexWhere + 7);
            //api.Log.Log ("sSQLOri_UpperPostWhereMixed [" + sSQLOri_UpperPostWhereMixed      + "]");
            // I believe that the distinct here is irrelevant - will not change now - in test cycle
            StringBuffer sbSCHEMA_ALL_TABS = CQLSchema.schema.getMasterTableList(); // sql in wants it for start - chicken and egg it seems
            sSQLForParser_UsedBelow = "SELECT DISTINCT " + sSQLOri_UpperPostSelectPreWhere
                    + " FROM " + sbSCHEMA_ALL_TABS.toString() + " WHERE " + sSQLOri_UpperPostWhereMixed;

            // **********************************************
            // Expand objects to cols
            // **********************************************
            //api.Log.Log ("sSQLForParser_UsedBelow  2 [" + sSQLForParser_UsedBelow  + "]");

            sSQLForParser_UsedBelow = CQLSchema.expandObjCols(CQLSchema.htObjectColsExpans_StrToStrSetCols,
                    sSQLForParser_UsedBelow);
            sSQLForParser_UsedBelow = sSQLForParser_UsedBelow.replaceAll("''", "#&#&27");
            //api.Log.Log ("sSQLForParser_UsedBelow  3 [" + sSQLForParser_UsedBelow  + "]");



            String s = Session.sIndraHome;
            // has expanded SELECT set and expanded FROM set relative to original
            if (bVerbose) {
                com.indraweb.util.Log.logClear("STEP 2 SQL TO PARSER \r\n[" + CQLInterpreterUtils.formatSQL(sSQLForParser_UsedBelow) + "]\r\n");
            }
            if (bVerbose) {
                com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputCQL.txt", "\r\n\r\n"
                        + "2. SQL TO PARSER \r\n[" + CQLInterpreterUtils.formatSQL(sSQLForParser_UsedBelow) + "]\r\n");
            }
            // ***********************************************************
            // ***********************************************************
            // BUILD SQL OBJECT
            // ***********************************************************
            // ***********************************************************

            SQL sql = new SQL(sSQLForParser_UsedBelow);

            // 1a GRAB SELECT CLAUSE
            SelectStatement selStat = sql.getSelectStatement();

            if (!(selStat instanceof SelectStatement)) {
                throw new Exception("CQL parser accepts select statements only.  Submitted CQL ["
                        + sCQLVerbatim + "] "
                        + " modified to SQL for parser [" + sSQLForParser_UsedBelow + "]");
            }

            //api.Log.Log ("selStat.toPrint() [" + selStat.toPrint () + "]");
            //api.Log.Log ("selStat.getTables().toString() [" + selStat.getTables ().toString () + "]");
            //api.Log.Log ("selStat.getColumns().toPrint() [" + selStat.getColumns ().toPrint () + "]");

            // BUILD OUTER QUERY

            selStat.addTableNamesToColumns(CQLSchema.schema);
            Columns colsUSER_SEL_COLS = selStat.getSelection().getColumns();
            //api.Log.Log ("colsUSER_SEL_COLS from parser SelectStatement returned [" + colsUSER_SEL_COLS + "]");
            // SECURITY :::::::::: ADD TABLE DOC SECURITY IF THE DOCUMENT OR IDRACDOCUMENT TABLES ARE HERE

            // RESOLVE USER SELETED COLS THAT COME FROM MULTIPLE TABLES
            //api.Log.Log ("1 colsUSER_SEL_COLS (unresolved)  [" + colsUSER_SEL_COLS + "]"); // post resolve
            Tables tablesUserSel = colsUSER_SEL_COLS.getTables();
            //api.Log.Log ("1.1 tables in colsUSER_SEL_COLS(.getTables()) (unresolved - tablesUserSel)  [" + tablesUserSel + "]"); //

            sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown( // side effect - set tables in Cols arg
                    colsUSER_SEL_COLS,
                    CQLSchema.hmUnknownColsCandidates_StrColToStrSetTabs,
                    new Tables());

            //api.Log.Log ("2 colsUSER_SEL_COLS (resolved)  [" + colsUSER_SEL_COLS + "]"); // post resolve
            //api.Log.Log ("2.1 tabs colsUSER_SEL_COLS.getTables() (resolved)  [" + colsUSER_SEL_COLS.getTables() + "]"); // post resolve

            // ******************************************
            // VERIFY WHICH SUBQUERY AND OUTER QUERY PIECES ARE NEEDED
            // BUILD THE INTERSECT PIECES
            // ******************************************

            // 1b GRAB SELECT CLAUSE, build colsUSER_SEL_COLS_IDS
            String sNodeDoc_Node_hints = "";
            if (colsUSER_SEL_COLS.getTables().contains("NODE")
                    && colsUSER_SEL_COLS.getTables().contains("NODEDOCUMENT")) {
                sNodeDoc_Node_hints = " /*+ index(NODEDOCUMENT NODEDOCUMENTNODEID) */ ";
            }


            //api.Log.Log ("colsUSER_SEL_COLS [" + colsUSER_SEL_COLS + "]" );
            Columns colsUSER_SEL_COLS_IDS =
                    sql4j.CQLInterpreter.CQLInterpreterUtils.getIDCols(colsUSER_SEL_COLS);
            //api.Log.Log ("colsUSER_SEL_COLS_IDS [" + colsUSER_SEL_COLS_IDS + "]" );

            WhereCondition wc = ((SelectStatement) sql.getSQLStatement()).getWhereClause().getWhereCondition();

            Vector vFTLikePredicates = new Vector();
            Hashtable htFtIndexes = CustomFields.getIndexes(dbc);


            CQLTreeWalkers.recurseCollectFullTextWCs(wc, vFTLikePredicates, htFtIndexes);
            CQLTreeWalkers.recurseUpperCaseColumnNames(wc);


            if (iObjectTypeSelected == iObj_DOCUMENT
                    || iObjectTypeSelected == iObj_NODEDOCUMENT) {
                CQLOptimizer.sweepMarkIfExitsNodeCorpusAtoms_recursive(wc);
            }
            CQLOptimizer.sweepMarkIfAllUnderAreNodeNegsConnectedByAnd(wc);

            boolean bIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree =
                    wc.getIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree();

            boolean bIsDocElementSelected = (iObjectTypeSelected == iObj_DOCUMENT
                    || iObjectTypeSelected == iObj_NODEDOCUMENT);

            boolean bAreWeInSetMode = bIsDocElementSelected
                    && (wc.getNodeCorpusANDSemanticsHereOrBelow()
                    || bIfAllUnderAreNodeNegsConnectedByAnd_IfExistsInSubTree);

            if (bVerbose) {
                api.Log.Log("CQL : bAreWeInNodeAndMode  [" + bAreWeInSetMode + "]");
            }
            //api.Log.Log ("wc.toString() [" + wc.toString() + "]");
            //api.Log.Log ("wc.getTables().toString ()  [" + wc.getTables().toString () + "]");
            //api.Log.Log ("wc.getColumns().toString() [" + wc.getColumns ().toString () + "]");


            String sSQLWhere_BaseQ = null;   // used for not IN mode - just JOINS
            String sSQLWhere_InSubQ = null;  // used for IN mode - not just JOINS

            if (!bAreWeInSetMode) // NEW WAY - no IN but JOIN
            {
                sSQLWhere_BaseQ = sql4j.CQLInterpreter.BuildWhere_BaseQ.buildWhere_BaseQRecurse(
                        colsUSER_SEL_COLS_IDS,
                        CQLSchema.schema,
                        CQLSchema.hmUnknownColsCandidates_StrColToStrSetTabs,
                        wc,
                        0,
                        "Start" //mpqc
                        );
                //api.Log.Log ("colsUSER_SEL_COLS_IDS[" + colsUSER_SEL_COLS_IDS + "]" );
                //api.Log.Log ("colsUSER_SEL_COLS_IDS.getTables()  [" + colsUSER_SEL_COLS_IDS.getTables() + "]" );
                //api.Log.Log ("sSQLWhere_BaseQ [" + sSQLWhere_BaseQ  + "]" );


            } else // old way - using "IN" subQ mode
            {
                //**************************************************
                // 1c  build all for after the "IN"
                // *************************************************
                //api.Log.Log ("wc.toPrint() [" + wc.toPrint() + "]" );
                sSQLWhere_InSubQ = sql4j.CQLInterpreter.BuildWhere_SubQ.buildWhere_SubQRecurse(
                        colsUSER_SEL_COLS_IDS,
                        CQLSchema.schema,
                        CQLSchema.hmUnknownColsCandidates_StrColToStrSetTabs,
                        wc,
                        0,
                        "Start",
                        new Tables(),
                        iObjectTypeSelected);
            }

            //api.Log.Log ("sSQLWhere_BaseQ [" + sSQLWhere_BaseQ + "]");
            //api.Log.Log ("sSQLWhere_InSubQ [" + sSQLWhere_InSubQ + "]");

            // ADD SECURITY CONSTRAINT
            String sDocSecurityConstraint = "";
            // sSecIDsAlertsList = "0"; // test - allows in debuger hbk control 2003 03 31

            // if debugging and don't want security - use this if instead:







            /*
             * if (user == null ) // for debugging - to get a user list for doc sec. { // throw new Exception ("comment this exception and put in password for login if used needed"); user =
             * api.security.TSLogin.localLogin ("hkon" , "racer9" , // debug new PrintWriter (System.out) , dbc); if (user == null) throw new Exception ("user null in CQLInterpret debug");
             *
             * api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!"); api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!"); api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!");
             * api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!"); api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!"); api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!");
             * api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!"); api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!"); api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!");
             * api.Log.Log ("!!!!!!! DEBUG : USING DEBUG USER !!!!!!!!!!"); }
             */







            if (user == null && !bDebugging) {
                throw new Exception("user null in CQLInterpret");
            }

            if (user == null && bDebugging) {
                api.Log.Log("!!!!!!! CQLInterpret DEBUG : USING DEBUG USER !!!!!!!!!!");
                api.Log.Log("!!!!!!! CQLInterpret DEBUG : USING DEBUG USER !!!!!!!!!!");
                if (!bDebugNoUserObject_noLdap) {
                    user = api.security.TSLogin.localLogin("hkon", "racer9", new PrintWriter(System.out), dbc);
                    if (user == null) {
                        throw new Exception("user null in TSClassify debug");
                    }
                }
            }

            // document security mechanism changed, this code is no longer used
            /*
             * if (colsUSER_SEL_COLS.getTables ().containsTableNamed ("DOCUMENT")) { // ADD DOC SECURITY if (user != null) { if (sDocSecIDsAlertsList == null) { if (bRunWithSecurity_Document) {
             * HashSet hsToUse = user.GetSecurityPermissions (); String sSecIdInListFromUserObject = com.indraweb.util.UtilSets.hsToStr (hsToUse , "," , true); String sComma = ""; if
             * (!sSecIdInListFromUserObject.trim ().equals ("")) sComma = ","; sDocSecurityConstraint = " DOCUMENTSECURITY.SECURITYID in ( 0 " + sComma + sSecIdInListFromUserObject + ")"; } } else //
             * authorization for alerts passed in band in the CQL expression { String sComma = ","; if (sDocSecIDsAlertsList.trim ().equals ("")) sComma = ""; // CQL sec spec returned nothing from
             * getSec... else sComma = ","; // CQL sec spec returned nothing from getSec...
             *
             * sDocSecurityConstraint = " " + " DOCUMENTSECURITY.SECURITYID in (0 " + sComma + sDocSecIDsAlertsList + ")"; } }
             *
             * }
             */

            String sNodeSecConstraint = "";

            // NODE SECURITY
            //hbk 2003 09 24 api.Log.Log ("DEBUG CHANGE : false && colsUSER_SEL_COLS.getTables().containsTabl )" );
            /*
             * if (colsUSER_SEL_COLS.getTables ().containsTableNamed ("NODE")) { if (bRunWithSecurity_NodeCorpus && user != null) { String sCorpusList = user.GetAuthorizedCorporaAsString (new
             * PrintWriter (System.out)); sNodeSecConstraint = " NODE.CORPUSID in (" + sCorpusList + ") "; } /* else // debug mode - { api.Log.Log ("!!!!!!! NODE SEC TURNED OFF !!!!!!!!!!");
             * api.Log.Log ("!!!!!!! NODE SEC TURNED OFF!!!!!!!!!!"); api.Log.Log ("!!!!!!! NODE SEC TURNED OFF !!!!!!!!!!"); api.Log.Log ("!!!!!!! NODE SEC TURNED OFF !!!!!!!!!!"); sNodeSecConstraint
             * = " NODE.CORPUSID in " + "(0,100047,100046,100045,100013,100012,100011,100010,2200001,100040,100058,100027" + "
             * ,100057,300012,18,17,16,11,100009,100008,100007,100006,100005,100004,100003,1000" + "02,100001,5,200004,22,21) " ; ;
             *
             *
             * }
             * } // if user SEL columns contains NODE
             */

            String[] sArrTwoReturnSelectionAndOrderBy =
                    convertSelectAndOrderByForCS_FTSCORE_andAddColsToOrderBy(
                    colsUSER_SEL_COLS,
                    sOrderBY, colsUSER_SEL_COLS.toString(), vFTLikePredicates,
                    colsUSER_SEL_COLS.getTables(), iObjectTypeSelected);

            String sSelectClausePostFTScoreAdjust = "select distinct "
                    + sNodeDoc_Node_hints
                    + sArrTwoReturnSelectionAndOrderBy[0];

            sOrderBY = sArrTwoReturnSelectionAndOrderBy[1];


            /*
             * Vector vOutOneStrSelectAppendForOrderByContains = new Vector (); if (iLocOrderBy > 0) { int iContainsCountPreFulltextContainsConvert =
             * com.indraweb.util.UtilStrings.countNumOccurrencesOfStringInString (sOrderBY.toLowerCase () , "contains"); sOrderBY = processOrderByClauseForFulltextSort (sOrderBY , wc ,
             * vOutOneStrSelectAppendForOrderByContains , sSelectClausePREOrderByForcedAppendage); }
             *
             * api.Log.Log ("sSelectClausePREOrderByForcedAppendage [" + sSelectClausePREOrderByForcedAppendage+ "]" ); String sSelectClausePostOrderByAppendage =
             * sSelectClausePREOrderByForcedAppendage; if (vOutOneStrSelectAppendForOrderByContains.size () > 0) sSelectClausePostOrderByAppendage = sSelectClausePostOrderByAppendage + (String)
             * vOutOneStrSelectAppendForOrderByContains.elementAt (0); api.Log.Log ("sSelectClausePostOrderByAppendage [" + sSelectClausePostOrderByAppendage + "]" );
             */

            String sFromClause = null;
            String sWhereClause = null;


            // select docid where (doctitle = 1 and nodeid = 2 ) or (doctitle = 3 and nodeid = 4 )
            // SET MODE MEANS USING THE SUB QUERY MODEL WITH DOCID IN OR NODEID IN

            if (!bAreWeInSetMode) // mode 1 - new way - no IN
            {
                Tables tablesFromBaseWhere = wc.getTables();

                StringSet ssTablesFromBaseSelect = colsUSER_SEL_COLS.getTables().toSet();
                StringSet ssTablesFromBaseWhere = tablesFromBaseWhere.toSet();

                StringSet ssTablesNewFromWhere = ssTablesFromBaseWhere.minus(ssTablesFromBaseSelect);

                //api.Log.Log ("ssTablesFromBaseSelect  [" + ssTablesFromBaseSelect + "]");
                //api.Log.Log ("ssTablesFromBaseWhere  [" + ssTablesFromBaseWhere + "]");
                //api.Log.Log ("ssTablesNewFromWhere  [" + ssTablesNewFromWhere  + "]");

                Tables tablesBaseSelAndBaseWhere = new Tables(
                        ssTablesNewFromWhere.union(ssTablesFromBaseSelect));

                if (bRunWithSecurity_Document && colsUSER_SEL_COLS.getTables().containsTableNamed("DOCUMENT")) {
                    tablesBaseSelAndBaseWhere.add(new Table("DOCUMENTSECURITY"));
                }
                if (bRunWithSecurity_Document && colsUSER_SEL_COLS.getTables().containsTableNamed("NODEDOCUMENT")) {
                    tablesBaseSelAndBaseWhere.add(new Table("DOCUMENTSECURITY"));
                }

                tablesBaseSelAndBaseWhere =
                        CQLInterpreterUtils.getExtendToIncludeJoinTabs(tablesBaseSelAndBaseWhere, true);
                String sIMPLIED_JOINS_UserSel_and_NewTablesFromWhere =
                        CQLInterpreterUtils.buildJoinStrings(tablesBaseSelAndBaseWhere, true, wc);

                HashSet hsStrTablesFrom = new HashSet();
                Enumeration enumTables = tablesBaseSelAndBaseWhere.toVector().elements();
                Vector vStrTabNamesFrom = new Vector();

                // optimize - put some tables first
                while (enumTables.hasMoreElements()) {
                    Table t = (Table) enumTables.nextElement();
                    String sTableName = t.getName();
                    if (sTableName.equals("NODEDOCUMENT")) {
                        vStrTabNamesFrom.addElement(sTableName);
                    } else {
                        hsStrTablesFrom.add(sTableName);
                    }
                }
                vStrTabNamesFrom.addAll(hsStrTablesFrom);

                sFromClause = " from " + UtilSets.vToStrPlain(vStrTabNamesFrom, ",");


                //api.Log.Log ("hbk sImpliedJoinsCustom [" + sImpliedJoinsCustom + "]" );
                //api.Log.Log ("hbk sDocSecurityConstraint [" + sDocSecurityConstraint + "]" );
                //api.Log.Log ("hbk sSQLWhere_BaseQ [" + sSQLWhere_BaseQ + "]" );

                sWhereClause = " where "
                        + c(
                        c(
                        c(
                        sIMPLIED_JOINS_UserSel_and_NewTablesFromWhere,
                        "and",
                        sDocSecurityConstraint),
                        "and",
                        sNodeSecConstraint),
                        "and",
                        "(" + sSQLWhere_BaseQ + ")");
            } else // else true Mode 2 older way : bAreWeInNodeAndMode // with IN
            {
                //api.Log.Log("colsUSER_SEL_COLS (unresolved)[" + colsUSER_SEL_COLS + "]" );
                sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown( // side effect - set tables in Cols arg
                        colsUSER_SEL_COLS,
                        CQLSchema.hmUnknownColsCandidates_StrColToStrSetTabs,
                        new Tables());
                Tables tablesUserSelResolved = colsUSER_SEL_COLS.getTables();
                //api.Log.Log("colsUSER_SEL_COLS (resolved)[" + colsUSER_SEL_COLS + "]" );
                //api.Log.Log("tablesUserSelResolved [" + tablesUserSelResolved + "]" );
                Tables tablesUserSelAndJoins = sql4j.CQLInterpreter.CQLInterpreterUtils.getExtendToIncludeJoinTabs(tablesUserSelResolved, false);
                //api.Log.Log("tablesUserSelAndJoins [" + tablesUserSelAndJoins + "]" );

                if (bRunWithSecurity_Document && tablesUserSelAndJoins.containsTableNamed("DOCUMENT")) {
                    tablesUserSelAndJoins.add(new Table("DOCUMENTSECURITY"));
                }
                String sJoin_tablesUserSelAndJoins =
                        sql4j.CQLInterpreter.CQLInterpreterUtils.buildJoinStrings(tablesUserSelAndJoins, true, wc);

                //api.Log.Log("sJoin_tablesUserSelAndJoins[" + sJoin_tablesUserSelAndJoins+ "]" );
                StringBuilder sbTableListForFrom = new StringBuilder();
                sbTableListForFrom.append(colsUSER_SEL_COLS.getTables().toString());
                if (bRunWithSecurity_Document && colsUSER_SEL_COLS.getTables().containsTableNamed("DOCUMENT")) {
                    sbTableListForFrom.append(", DOCUMENTSECURITY");
                }
                sFromClause = " from " + sbTableListForFrom.toString();
                //api.Log.Log("sFromClause  [" + sFromClause + "]" );
                sWhereClause = null;
                //api.Log.Log ("sbFinalCQL 2 [" + sbFinalCQL + "]");
                String sAnd_sNodeSecConstraintPre_sUSER_SEL_IMPLIED_JOINS = "";
                if ((!sDocSecurityConstraint.trim().equals("")
                        || !sJoin_tablesUserSelAndJoins.trim().equals(""))) {
                    sAnd_sNodeSecConstraintPre_sUSER_SEL_IMPLIED_JOINS = " and ";
                }
                //api.Log.Log ("sJoin_tablesUserSelAndJoins [" + sJoin_tablesUserSelAndJoins + "]");
                //api.Log.Log ("colsUSER_SEL_COLS_IDS [" + colsUSER_SEL_COLS_IDS + "]");
                sWhereClause = " where "
                        + c(
                        c(
                        c(
                        sDocSecurityConstraint,
                        "and",
                        sNodeSecConstraint),
                        "and",
                        sJoin_tablesUserSelAndJoins),
                        "and",
                        " (" + colsUSER_SEL_COLS_IDS.toString()
                        + ")" + " in " + "( ");


                //api.Log.Log ("sWhereClause [" + sWhereClause + "]");
                //api.Log.Log ("colsUSER_SEL_COLS_IDS [" + colsUSER_SEL_COLS_IDS + "]");

            }  // else mode 2 newer way
            //api.Log.Log ("sbFinalCQL 3 [" + sbFinalCQL + "]");

//            api.Log.Log ("sCQLSubQuery 4 [" + sCQLSubQuery + "]");
            //api.Log.Log ("sbFinalSQL 4 [" + CQLInterpreterUtils.formatSQL (sbFinalSQL.toString()) + "]");
            //api.Log.Log ("sCQLSubQuery 5 [" + CQLInterpreterUtils.formatSQL(sSQLWhere_SubQ) + "]");
            //api.Log.Log ("sFromClause [" + sFromClause + "]");
            StringBuilder sbFinalSQL = new StringBuilder();
            if (!bAreWeInSetMode) // newer way
            {
                sbFinalSQL.append(sSelectClausePostFTScoreAdjust).append(" ").append(sFromClause).append(" ").append(sWhereClause);
            } else // older IN way
            {
                sbFinalSQL.append(sSelectClausePostFTScoreAdjust).append(" ").append(sFromClause).append(" ").append(sWhereClause).append(sSQLWhere_InSubQ).append(")");
            }
            // this order by seems not to be working


            String sSQLMorphing = sbFinalSQL.toString();

            sSQLMorphing = sSQLMorphing.toString().replaceAll("#&#&27", "''");
            //System.out.println ("SQL RETURNED [" + CQLInterpreterUtils.formatSQL(sFinalFinal) + "]\r\n");

            // IW oriignal query is done - but new mode needed still
            cds.sSQL_OriginalIWCQLQuery = sSQLMorphing;
            // sSQLMorphing = sSQLMorphing + " " + sOrderBY + sDistinctOuterClose;
            if (sOrderBY != null) {
                sSQLMorphing = sSQLMorphing + " " + sOrderBY;
            }

            cds.sVeryFinalIWQuery = sSQLMorphing;
            //api.Log.Log ("sSQLMorphing sbFinalSQL final [" + CQLInterpreterUtils.formatSQL (sSQLMorphing) + "]");

        }  // if not mode 3

        return cds;
    } // cqlTranslateTopLevel

    static String errorCheckAndNormalizeCQL(String sSQL)
            throws Exception {
        String sReturn = null;
        if (sSQL.trim().endsWith(";")) {
            throw new Exception("do not end CQL with ; - this is not SQL");
        }
// else if..
        // Changed by MAP 5/17/2006 - do not replace parenthesis that are inside of quoted areas
        //sSQL = com.indraweb.util.UtilStrings.replaceStrInStr ( sSQL, "(", " ( " );
        //sSQL = com.indraweb.util.UtilStrings.replaceStrInStr ( sSQL, ")", " ) " );
        sSQL = com.indraweb.util.UtilStrings.cleanStringWhiteSpaceToSpaces(sSQL, true);

        return sSQL;
    }

    private static String cvtLikeClauseStringToContains(String sFTIndexName, String sSQLLikeClause) {
        /*
         * int iIndexLeftmostParen = sSQLLikeClause.indexOf('('); int iIndexRightmostParen = sSQLLikeClause.lastIndexOf(')'); String sSQLPieceBetweenOuterParens =
         * sSQLLikeClause.substring(iIndexLeftmostParen , iIndexRightmostParen + 1);
         */
        int iIndexEndLike = sSQLLikeClause.toLowerCase().indexOf("like");
        String sSQLPieceLikeClauseWOLike =
                sSQLLikeClause.substring(iIndexEndLike + 5);
        return "contains (" + sFTIndexName + ", " + sSQLPieceLikeClauseWOLike + ", 1) > 1";
    }

    public static String getParm(String sParmStr, String sParm) throws Exception {
        sParmStr = com.indraweb.util.UtilStrings.convertAllDoubleSpaceToSingleSpaces(sParmStr);
        sParmStr = sParmStr.replaceAll(" =", "=");
        sParmStr = sParmStr.replaceAll("= ", "=");
        sParmStr = sParmStr.trim();

        int iindexparmname = sParmStr.indexOf(sParm + "=");
        if (iindexparmname < 0) {
            return "PARM NOT FOUND : " + sParmStr;
        }

        int iIndexEquals = sParmStr.indexOf("=", iindexparmname);
        if (sParm.equals("ftand")) {
            int iIndexftand = sParmStr.indexOf("ftand=");
            String sftand = sParmStr.substring(iIndexftand + 6);
            sftand = UtilStrings.getsUpToNthOfThis_1based(sftand, "'", 2) + "'";
            //api.Log.Log("sftand 4 : " + sftand);
            return sftand;


        }

        int iCommaAfterEquals = sParmStr.indexOf(",", iIndexEquals);
        int iCloseParenEqualsAfter = sParmStr.indexOf(")", iIndexEquals);
        int iEndDelim = com.indraweb.util.UtilSets.min(iCommaAfterEquals, iCloseParenEqualsAfter);
        if (iEndDelim == -1) {
            iEndDelim = com.indraweb.util.UtilSets.max(iCommaAfterEquals, iCloseParenEqualsAfter);
        }
        if (iEndDelim == -1) {
            throw new Exception("error retrieving parm [" + sParm + "] in string [" + sParmStr + "]");
        }

        if (iindexparmname < 0 && !sParm.equals("ftand")) {
            throw new Exception("parm [" + sParm + "] not found in  [" + sParmStr + "]\r\n");
        }
        if (iindexparmname < 0 && sParm.equals("ftand")) {
            return null;
        }

        String sreturn = sParmStr.substring(iIndexEquals + 1, iEndDelim);
        //com.indraweb.util.Log.logClear("parm [" + sParm + "] value ["  +  sreturn +  "]\r\n");
        return sreturn.trim();

    }

    public static String getNodeConstraint(int iQid, Connection dbc)
            throws Exception {
        try {
            String sSQL = "select nodeid from trecqnode where version = 2 and questionid = "
                    + iQid + " order by nodeid ";
            //com.indraweb.util.Log.logClear("getNodeConstraint sql [" + sSQL + "]\r\n");
            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);
            int iNumNodes = 0;
            StringBuilder sbret = new StringBuilder("(");
            while (rs.next()) {
                int iNodeID = rs.getInt(1);
                if (iNumNodes > 0) {
                    sbret.append(" or ");
                }


                sbret.append("NODEID = ").append(iNodeID);
                iNumNodes++;
            }
            rs.close();
            stmt.close();
            sbret.append(")");
            if (iNumNodes == 0) {
                throw new Exception("iNumNodes == 0 iQid :" + iQid);
            }

            return sbret.toString();
        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("getNodeConstraint err", e);
        }
        return "bad return from getNodeConstraint ";
    }

    private static String c(String s1, String sConn, String s2) {
        String sReturn = null;
        boolean b1 = !s1.trim().equals("");
        boolean b2 = !s2.trim().equals("");

        if (b1) {
            if (b2) {
                sReturn = " " + s1 + " " + sConn + " " + s2 + " ";
            } else {
                sReturn = " " + s1 + " ";
            }
        } else {
            if (b2) {
                sReturn = " " + s2 + " ";
            } else {
                sReturn = " ";
            }
        }

        //api.Log.Log("s1 [" + s1 + "] connect [" + sConn+ "] s2 [" + s2 + "] yields [" + sReturn + "]" );
        return sReturn;
    }

    /**
     * converts select and order by like clauses to handle FT sort
     */
    private static String[] convertSelectAndOrderByForCS_FTSCORE_andAddColsToOrderBy(
            Columns colsAlreadyResolved,
            String sOrderBy,
            String sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct,
            Vector vFTLikePredicates,
            Tables tablesInUseSoFar,
            int iObjectTypeSelected)
            throws Exception {

        // PROCESS SELECT CLAUSE TO MOD THE FTSCORE COL TO THE RESOLVED VERSION
        //api.Log.Log ("sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct [" + sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct + "]" );
        if (iObjectTypeSelected == CQLInterpret.iObj_DOCUMENT) // side effect : changes vector:
        {
            convertFTLIkePredListToDocObjectOnlyForNodeAndMode(vFTLikePredicates, iObjectTypeSelected);
        }

        String sFTSCORE_REPLACE = null;
        if (vFTLikePredicates != null && vFTLikePredicates.size() > 0) {

            sFTSCORE_REPLACE = convertCS_FTSCoreToResolvedExpression(vFTLikePredicates);
        }
        Vector vSelectClauseElements = null;
        boolean bOrderByDescFlag_FTSCORE = false;
        int iIndexOfCS_FTSCORE_InSelect = -1;
        {
            sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct = sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct.trim().toUpperCase();


            //api.Log.Log ("sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct [" + sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct + "]" );
            vSelectClauseElements = UtilStrings.splitByStrLen1(sSelectClauseCommaDelimUpperTableQualifiedNoSelectDistinct,
                    ",", true);

            //api.Log.Log ("vSelectColsTrimmed [" + UtilSets.vToStr (vSelectClauseElements) + "]");
            // find the ftindex col index in the select clause
            //was : iIndexOfCS_FTSCORE_InSelect = vSelectClauseElements.indexOf("DOCUMENT.CS_FTSCORE");
            for (int i = 0; i < vSelectClauseElements.size(); i++) {
                String sCol = (String) vSelectClauseElements.elementAt(i);
                //api.Log.Log ("sCol [" + sCol + "]" );
                if (sCol.indexOf(".CS_FTSCORE") >= 0) {
                    iIndexOfCS_FTSCORE_InSelect = i;
                    break;
                }
            }
            if (iIndexOfCS_FTSCORE_InSelect >= 0) {
                if (vFTLikePredicates.size() > 0) {
                    vSelectClauseElements.setElementAt(sFTSCORE_REPLACE, iIndexOfCS_FTSCORE_InSelect);
                } else // select 100 instead as a dummy FT score/placeholder
                {
                    vSelectClauseElements.setElementAt("100 FTSCORE", iIndexOfCS_FTSCORE_InSelect);
                }
            }
        }

        // PROCESS ORDER BY CLAUSE
        String sOrderbyReturn = "";
        Vector vOrderByElements = null;
        {
            boolean[] bArrOrderByDesc = null;
            if (sOrderBy.length() > 0) {
                final String sORDERBY = "order by";
                sOrderBy = sOrderBy.substring(sORDERBY.length()).trim().toUpperCase();

                if (sOrderBy.length() > 0) {
                    vOrderByElements = UtilStrings.splitByStrLenLong(sOrderBy,
                            ",");

                    //api.Log.Log ("vOrderByElements 1 [" + UtilSets.vToStr (vOrderByElements) + "]");
                    Columns colsORderBy = new Columns();
                    int iFTSortIndex = -1;

                    bArrOrderByDesc = new boolean[vOrderByElements.size()];

                    Vector vUpperOrLowerOrNoneIndicators = new Vector();
                    for (int iObyElemIdx = 0; iObyElemIdx < vOrderByElements.size(); iObyElemIdx++) {
                        String sOrderByElement = (String) vOrderByElements.elementAt(iObyElemIdx);
                        if (sOrderByElement.trim().toUpperCase().endsWith(" DESC")) {
                            sOrderByElement = sOrderByElement.substring(0, sOrderByElement.length() - 5);
                            bArrOrderByDesc[iObyElemIdx] = true;
                        } else if (sOrderByElement.trim().toUpperCase().endsWith(" DESCENDING")) {
                            sOrderByElement = sOrderByElement.substring(0, sOrderByElement.length() - 11);
                            bArrOrderByDesc[iObyElemIdx] = true;
                        } else if (sOrderByElement.trim().toUpperCase().endsWith(" ASC")) {
                            sOrderByElement = sOrderByElement.substring(0, sOrderByElement.length() - 4);
                            bArrOrderByDesc[iObyElemIdx] = false;
                        } else if (sOrderByElement.trim().toUpperCase().endsWith(" ASCENDING")) {
                            sOrderByElement = sOrderByElement.substring(0, sOrderByElement.length() - 10);
                            bArrOrderByDesc[iObyElemIdx] = false;
                        } else {
                            bArrOrderByDesc[iObyElemIdx] = false;
                        }

                        // PROCESS NOW FOR UPPER LOWER
                        sOrderByElement = sOrderByElement.replaceAll(" ", "");
                        String sOrderByElement_upper = sOrderByElement.toUpperCase();
                        if (sOrderByElement_upper.startsWith("UPPER(")) {
                            vUpperOrLowerOrNoneIndicators.addElement("UPPER");
                            if (!sOrderByElement_upper.endsWith(")")) {
                                throw new Exception("Error in order by clause missing closing paren [" + sOrderByElement + "]");
                            }
                            sOrderByElement = sOrderByElement.substring(6);
                            sOrderByElement = sOrderByElement.substring(0, sOrderByElement.length() - 1);
                        } else if (sOrderByElement_upper.startsWith("LOWER(")) {
                            vUpperOrLowerOrNoneIndicators.addElement("LOWER");
                            if (!sOrderByElement_upper.endsWith(")")) {
                                throw new Exception("Error in order by clause missing closing paren [" + sOrderByElement + "]");
                            }
                            sOrderByElement = sOrderByElement.substring(6);
                            sOrderByElement = sOrderByElement.substring(0, sOrderByElement.length() - 1);
                        } else {
                            vUpperOrLowerOrNoneIndicators.addElement("");
                        }

                        Column colOby = new Column(sOrderByElement.trim());
                        String sColName = colOby.getColumnName().toString();

                        if (sOrderByElement.trim().toUpperCase().startsWith("FTSCORE")) {
                            if (iFTSortIndex >= 0) {
                                throw new Exception("only one FTSCORE SORT allowed in the ORDER BY");
                            } else {
                                iFTSortIndex = iObyElemIdx;
                            }

                            colOby.setTableName("NA");
                            colsORderBy.add(colOby);
                        } else if (sColName.trim().equals("")) {
                            continue;
                        } else {
                            colOby.setTableName("UNKNOWNTABLE");
                            colsORderBy.add(colOby);
                        }
                    }

                    //api.Log.Log ("order cols pre resolve [" + colsORderBy.toString () + "]");
                    if (colsORderBy.toVector().size() > 0) {
                        // order by clause col resolution
                        sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown( // side effect - set tables in Cols arg
                                colsAlreadyResolved,
                                colsORderBy,
                                CQLSchema.hmUnknownColsCandidates_StrColToStrSetTabs,
                                new Tables());
                        //api.Log.Log ("order cols post resolve [" + colsORderBy.toString () + "]");

                        vOrderByElements.clear();

                        // RECONSTRUCT the vector now that cols are resolved in light of select cols and from clause
                        // keep in mind : iIdxForOrderBy needed since it's Post Resolution Insert In Light Of Having Skipped FTSCORE "col" Possibly
                        //int xiIdxForOrderBy = 0;
                        if (vUpperOrLowerOrNoneIndicators.size() != colsORderBy.toVector().size()) {
                            throw new Exception("vUpperOrLowerOrNoneIndicators.size () != colsORderBy.toVector ().size()");
                        }
                        for (int iObyElemIdx = 0; iObyElemIdx < colsORderBy.toVector().size(); iObyElemIdx++) {
                            Column colOrderby = (Column) colsORderBy.toVector().elementAt(iObyElemIdx);
                            //iIdxForOrderBy++;
                            StringBuilder sbFinalObyClauseToAdd = new StringBuilder();

                            if (vUpperOrLowerOrNoneIndicators.elementAt(iObyElemIdx).equals("UPPER")) {
                                sbFinalObyClauseToAdd.append("UPPER (");
                            }
                            if (vUpperOrLowerOrNoneIndicators.elementAt(iObyElemIdx).equals("LOWER")) {
                                sbFinalObyClauseToAdd.append("LOWER (");
                            }

                            if (iObyElemIdx == iFTSortIndex) {
                                sbFinalObyClauseToAdd.append("FTSCORE");
                            } else {
                                sbFinalObyClauseToAdd.append(colOrderby.getTableName()).append(".").append(colOrderby.getName());
                            }

                            // tail of order by
                            if (vUpperOrLowerOrNoneIndicators.elementAt(iObyElemIdx).equals("UPPER")) {
                                sbFinalObyClauseToAdd.append(")");
                            }
                            if (vUpperOrLowerOrNoneIndicators.elementAt(iObyElemIdx).equals("LOWER")) {
                                sbFinalObyClauseToAdd.append(")");
                            }

                            vOrderByElements.addElement(sbFinalObyClauseToAdd.toString());
                        }
                    }

                    if (vOrderByElements.size() != bArrOrderByDesc.length) {
                        throw new Exception("vOrderByElements.size() != bArrOrderByDesc.length");
                    }
                    //api.Log.Log ("vOrderByElements  [" + UtilSets.vToStr (vOrderByElements) + "]");

                    int iIndexOfCS_FTSCORE_InOrderBy = vOrderByElements.indexOf("FTSCORE");

                    if (iIndexOfCS_FTSCORE_InOrderBy >= 0) {
                        if (iIndexOfCS_FTSCORE_InSelect < 0) {
                            throw new Exception("can not order by FTSCORE if not part of the <Object> or  selectedcolumn set");
                        } else {

                            //api.Log.Log ("vFTLikePredicates.size()  [" + vFTLikePredicates.size() + "]" );
                            //api.Log.Log ("vFTLikePredicates [" + UtilSets.vToStr(vFTLikePredicates) + "]" );

                            //String sCS_FTSCoreToResolvedExpression = "(" +
                            //        convertCS_FTSCoreToResolvedExpression ( vFTLikePredicates) + ")";
                            //vSelectClauseElements.setElementAt (sCS_FTSCoreToResolvedExpression, iIndexOfCS_FTSCORE_InSelect );
                            if (!bOrderByDescFlag_FTSCORE) {
                                vOrderByElements.setElementAt("" + (iIndexOfCS_FTSCORE_InSelect + 1), iIndexOfCS_FTSCORE_InOrderBy);
                            } else {
                                vOrderByElements.setElementAt("" + (iIndexOfCS_FTSCORE_InSelect + 1) + " DESC", iIndexOfCS_FTSCORE_InOrderBy);
                            }
                        }
                    }
                }
                String[] sArrOrderByCols = (String[]) UtilSets.convertVectorTo_ObjArray(vOrderByElements);
                StringBuilder sb = new StringBuilder();
                if (sArrOrderByCols != null) {
                    for (int i = 0; i < sArrOrderByCols.length; i++) {
                        if (i > 0) {
                            sb.append(",");
                        }
                        if (!bArrOrderByDesc[i]) {
                            sb.append(sArrOrderByCols[i].toString());
                        } else {
                            sb.append(sArrOrderByCols[i].toString()).append(" DESC");
                        }

                    }
                }
                sOrderbyReturn = sb.toString();
            }
        }

        String[] sArrSelectCols = (String[]) UtilSets.convertVectorTo_ObjArray(vSelectClauseElements);
        String sSelectReturn = UtilSets.convertObjectArrayToStringWithDelims(sArrSelectCols, ",");


        //api.Log.Log ("sSelectReturn [" + sSelectReturn + "]");
        //api.Log.Log ("sOrderbyReturn  [" + sOrderbyReturn + "]");
        String[] sArrTwoReturnSelectionAndOrderBy = new String[2];
        sArrTwoReturnSelectionAndOrderBy[0] = sSelectReturn;
        if (sOrderbyReturn.length() > 0) {
            sArrTwoReturnSelectionAndOrderBy[1] = "ORDER BY " + sOrderbyReturn;
        }
        //api.Log.Log ("sOrderbyReturn [" + sOrderbyReturn + "]" );

        return sArrTwoReturnSelectionAndOrderBy;
    }

    private static String convertCS_FTSCoreToResolvedExpression(Vector vFTLikePredicates) {
        StringBuilder sbFTResolvedExpression = new StringBuilder();
        sbFTResolvedExpression.append(" (( ");
        for (int i = 0; i < vFTLikePredicates.size(); i++) {

            Object oPredicate = vFTLikePredicates.elementAt(i);
            //api.Log.Log ("oPredicate.getClass().getName()  [" + oPredicate.getClass ().getName () + "]");
            LikePredicate lp = (LikePredicate) oPredicate;
            /*
             * api.Log.Log ( "lp.getColumn1() [" + lp.getColumn1() + "] \r\n" + "lp.getColumn2() [" + lp.getColumn2() + "] \r\n" + "lp.getLabel() [" + lp.getLabel() + "] \r\n" + "lp.getPattern() [" +
             * lp.getPattern() + "] \r\n" + "lp.toString() [" + lp.toString() + "] \r\n" + "" );
             */
            if (i > 0) {
                sbFTResolvedExpression.append(" + ");
            }
            sbFTResolvedExpression.append(lp.toString(false));
        }
        sbFTResolvedExpression.append(" ) / ").append(vFTLikePredicates.size()).append(" )");
        //api.Log.Log ("sbFTMultiply [" + sbFTResolvedExpression.toString () + "]");
        return sbFTResolvedExpression.toString() + " FTSCORE ";
    }

    // get DOCUMENT object FT items to determine DOC object FTSCORE clause
    private static void convertFTLIkePredListToDocObjectOnlyForNodeAndMode(
            Vector vFTLikePredicates_inOut, int iObjectTypeSelected)
            throws Exception {
        // to solve a FT select and order by on NODE cols when only selecting a DOC
        // object - in the special NODE AND mode ...
        // see if we are in special case of <DOCUMENT> object selection and node only containst query

        // loop to see if any of vFTLikePredicates are on a <DOCUMENT> object col
        for (int i = vFTLikePredicates_inOut.size() - 1; i >= 0; i--) {
            Object oPredicate = vFTLikePredicates_inOut.elementAt(i);
            //api.Log.Log ("oPredicate.getClass().getName()  [" + oPredicate.getClass ().getName () + "]");
            LikePredicate lp = (LikePredicate) oPredicate;

//            api.Log.Log (
//                    "lp.getColumn1()  [" + lp.getColumn1 () + "] \r\n" +
//                    "lp.getColumn2()  [" + lp.getColumn2 () + "] \r\n" +
//                    "lp.getLabel()  [" + lp.getLabel () + "] \r\n" +
//                    //"lp.getColumn1().getName()  [" + lp.getColumn1().getName() + "] \r\n" +
//                    //"lp.getColumn2().getName()  [" + lp.getColumn2().getName() + "] \r\n" +
//                    "lp.getPattern()  [" + lp.getPattern () + "] \r\n" +
//                    "lp.toString()  [" + lp.toString () + "] \r\n" +
//                    ""
//            );
            String sCol1 = lp.getColumn1().getName().trim().toUpperCase();
            // if not from the

            StringSet ssDocObjCols = null;
            if (iObjectTypeSelected == iObj_DOCUMENT) {
                ssDocObjCols = (StringSet) CQLSchema.getHTobjectColsExpans_StrToStrSetCols().get("<DOCUMENT>");
            } else {
                throw new Exception("invalid state not doc obj.");
            }

            if (!lp.isColFTIndexed() || !ssDocObjCols.contains(sCol1)) {
                vFTLikePredicates_inOut.removeElementAt(i);
            }
        }
    }

    /*
     * private static String xprocessOrderByClauseForFulltextSort ( String sOrderBy, WhereCondition wc, Vector vOutOneStrSelectAppendForOrderByContains , String sSelectClausePreOrderByForcedAppendage)
     * throws Exception
     *
     * {
     *
     * String sOrderByLiteral = "order by"; int iIndexEndOrderBy = sOrderBy.toLowerCase ().indexOf (sOrderByLiteral) + sOrderByLiteral.length (); String sOrderByClausesOnly = sOrderBy.substring
     * (iIndexEndOrderBy).trim ();
     *
     * Vector vStrSorts = com.indraweb.util.UtilStrings.splitByStrLen1 (sOrderByClausesOnly , ",");
     *
     * Enumeration enumSorts = vStrSorts.elements (); int iVIndex = 0; // oracle requires it to be in the select list StringBuffer sbContainsSelectsAddition = new StringBuffer (); int
     * iNumAdditionalSelectColsAddedSoFar = 0; while (enumSorts.hasMoreElements ()) { String sSort = ( (String) enumSorts.nextElement () ).trim (); String sSortLower = sSort.toLowerCase ().trim ();
     * String sLikeString = " like ";
     *
     * int iIndexOfLike = sSortLower.indexOf (sLikeString);
     *
     * if (iIndexOfLike > 0) { int iNumSelectedColsComingIn = UtilStrings.countNumOccurrencesOfStringInString ( sSelectClausePreOrderByForcedAppendage , ",") + 1;
     *
     * if (com.indraweb.util.UtilStrings.countNumOccurrencesOfStringInString (sSortLower , sLikeString) > 1) throw new Exception ("invalid order by clause - need { } around search term 'like' [" +
     * sSort + "]");
     *
     *
     * String sLikeCol = sSort.substring (0 , iIndexOfLike); String sLikeValue = null; String sAscDesc = ""; if (sSortLower.endsWith ("asc")) { sLikeValue = sSortLower.substring (iIndexOfLike +
     * sLikeString.length () , sSortLower.length () - 3); sAscDesc = " ASC"; } else if (sSortLower.endsWith ("desc")) { sLikeValue = sSortLower.substring (iIndexOfLike + sLikeString.length () ,
     * sSortLower.length () - 4).trim (); sAscDesc = " DESC"; } else { sLikeValue = sSortLower.substring (iIndexOfLike + sLikeString.length ()).trim (); }
     *
     * String sContainsStringNoASCDESC = "contains (" + sLikeCol + ", " + sLikeValue + ")"; sbContainsSelectsAddition.append (" , " + sContainsStringNoASCDESC);
     *
     * //String sContainsStringWithASCDESC = "contains ("+ sLikeCol + ", " + sLikeValue+ ")" + sAscDesc; // now add the order by number String sSelColIndexReplacesContainsStringWithASCDESC = "" + (
     * iNumSelectedColsComingIn + iNumAdditionalSelectColsAddedSoFar + 1 ) + sAscDesc; iNumAdditionalSelectColsAddedSoFar++;
     *
     * sSelColIndexReplacesContainsStringWithASCDESC = sSelColIndexReplacesContainsStringWithASCDESC.trim (); vStrSorts.setElementAt (sSelColIndexReplacesContainsStringWithASCDESC , iVIndex); }
     * iVIndex++; }
     *
     * // now build the output string
     *
     * StringBuffer sbOrderByWithContains = new StringBuffer (" order by "); enumSorts = vStrSorts.elements (); String sComma = ""; while (enumSorts.hasMoreElements ()) { String sSortWithContains =
     * (String) enumSorts.nextElement (); sbOrderByWithContains.append (sComma.trim () + sSortWithContains.trim ()); sComma = ", "; }
     *
     * vOutOneStrSelectAppendForOrderByContains.addElement (sbContainsSelectsAddition.toString ());
     *
     * String sRet = sbOrderByWithContains.toString ();
     *
     * return sRet; }
     */
}
