/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Sep 9, 2002
 * Time: 4:10:06 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package sql4j.CQLInterpreter;

import sql4j.parser.*;
import sql4j.schema.*;
import sql4j.util.*;

import java.util.*;

public class BuildWhere_SubQ
{

    boolean bContainsNodeTableOrCorpusTable;

    static int iCallCounter = 0;

    public static String buildWhere_SubQRecurse (
            Columns cols_IDS ,
            Schema schema ,
            HashMap hmUnknownCols_StringColToStringSetTables ,
            WhereCondition wc ,
            int iDepth ,
            String sLeftRightOrStart ,
            Tables tablesKnownPrior,
            int iObjectTypeSelected
            ) throws Exception
    {
       // if ( iDepth == 0 )
         //   CQLInterpreterUtils.printTree(wc, 0);

        if ( iObjectTypeSelected == CQLInterpret.iObj_DOCUMENT ||
             iObjectTypeSelected == CQLInterpret.iObj_NODEDOCUMENT )
        {
            if ( iDepth == 0 && ( NodeNegative.isAtomicNodeNeg(wc, false) ||
                wc.getIfAllUnderAreNodeNegsConnectedByAnd() ))
                throw new Exception ("invalid CQL query - positive constraint required");
        }


        iCallCounter++;
        StringBuffer sbCQLSubquery = new StringBuffer ();

        // WALK TREE FOR SUBSUQUERIES
        // start by assuming that doc is in the select list
        // (7)
        if (wc instanceof AtomicWhereCondition)
        {
            AtomicWhereCondition awc = (AtomicWhereCondition) wc;

            if ( awc.getIfAllUnderAreNodeNegsConnectedByAnd())
            {
                if ( NodeNegative.isAtomicNodeNeg(awc, false))
                    NodeNegative.isAtomicNodeNeg(awc, true);
                else
                    throw new Exception ("error - getIfAllUnderAreNodeNegsConnectedByAnd and !NodeNegative.isAtomicNodeNeg()");
            } else {

            }
            // System.out.println("colsUSER_SEL_COLS_IDS [" + colsUSER_SEL_COLS_IDS + "]");
            String sAtomicSubPiece = buildAtomicSubquerySelectPiece (
                    cols_IDS ,
                    schema ,
                    hmUnknownCols_StringColToStringSetTables ,
                    awc ,
                    tablesKnownPrior
            );
            // System.out.println(iCallCounter + ":" + iDepth + ":" + sLeftRightOrStart + " AtomicWhereCondition  " +                     " \r\ngetOperator() [" + awc.getOperator() +                     "] \r\nawc.toString() [" + awc.toString() +                     "] \r\nawc.getColumn1() [" + awc.getColumn1() +                     "] \r\nawc.getColumn2() [" + awc.getColumn2() + "]\r\n"             );
            sbCQLSubquery.append (sAtomicSubPiece);
        }   // atomic where
        else if (wc instanceof CompoundWhereCondition)
        {
            CompoundWhereCondition cwc = (CompoundWhereCondition) wc;
            //System.out.println(iCallCounter + ":" + iDepth + ":" + sLeftRightOrStart + " CompoundWhereCondition" + "\r\ncwc.getOperator() [" + cwc.getOperator() + "]\r\n" + " toString() [" + cwc.toString() + "]\r\n" );

            String sOp = cwc.getOperator ();
            // if ( sOp.equals("AND") && exists_anyNodeTableSpecific_Or_CorpusConstraintsBelow ( wc ) ) {

            if (sOp.equals ("AND") || sOp.equals ("OR"))
            {
                String sConnector = null;
                //System.out.println(iCallCounter + ":" + iDepth + " wcLeft.getClass().getName() " + wcLeft.getClass().getName() + " wcLeft.toString() [" + wcLeft.toString() + " lp.getOperator [" + lp.getOperator() + "] " + " lp.getPattern [" + lp.getPattern() + "] "
                //sConnector = NodeNegative.analyzeNodeNegs_swapLR_MinusRHS (cwc);
                NodeNegative.analyzeNodeNegs_swapLR_OnRHSNodeNegAnd_LHSNot ( cwc );
                if (sOp.equals ("AND"))
                {
                    // default will be intersect ....
                    sConnector = "intersect";
                    // unless 1) my right child is a "node neg and" (sic)  Root --> minus
                    if ( !wc.getIfAllUnderAreNodeNegsConnectedByAnd() )
                    {

                        if ( cwc.getRight().getIfAllUnderAreNodeNegsConnectedByAnd() )
                        {
                            sConnector = "minus";
                        }
                    }

                    // or 2) I am myself a "node neg and"  --> union
                    else if ( wc.getIfAllUnderAreNodeNegsConnectedByAnd() )
                    {

                        if ( cwc.getRight().getIfAllUnderAreNodeNegsConnectedByAnd() )
                        {
                            sConnector = "union";
                        }
                    }

                }
                else if (sOp.equals ("OR"))
                    sConnector = "union";

                if ( sConnector == null )
                    throw new Exception ("CQL junction not resolved");

                sbCQLSubquery.append ("(");
                sbCQLSubquery.append (buildWhere_SubQRecurse (
                        cols_IDS ,
                        schema ,
                        hmUnknownCols_StringColToStringSetTables ,
                        cwc.getLeft () ,
                        iDepth + 1 ,
                        "L" ,
                        tablesKnownPrior,
                        iObjectTypeSelected
                ) );
                sbCQLSubquery.append (")");
                sbCQLSubquery.append (" " + sConnector + " " );

                //System.out.println(iCallCounter + ":" + iDepth + " wcRight.getClass().getName() " + wcRight.getClass().getName() + " wcRight.toString() [" + wcRight.toString() + "]");
                sbCQLSubquery.append (" (" +
                        buildWhere_SubQRecurse (
                                cols_IDS ,
                                schema ,
                                hmUnknownCols_StringColToStringSetTables ,
                                cwc.getRight () ,
                                iDepth + 1 ,
                                "R" ,
                                tablesKnownPrior,
                                iObjectTypeSelected )
                        + ")");


            }
            else
                throw new Exception ("compound where condition not sOp.equals('AND') || sOp.equals('OR') [" + sOp + "]");

            // System.out.println("sbCQLSubquery[" + sbCQLSubquery+ "]");

        } // compound where
        else
            throw new Exception ("wc instance of unknown type");

        return sbCQLSubquery.toString ();

    }


    /**
     * ATOMIC
     * build select IDs from tables where atomic - for intersect and union construction
     */
    private static String buildAtomicSubquerySelectPiece (
            Columns cols_IDS , // may be ID's only
            Schema schema , // contains known col to table mappings
            HashMap hmUnknownCols_StringColToStringSetTables ,
            AtomicWhereCondition awc ,
            Tables tablesKnownPrior
            ) throws Exception
    {
        //api.Log.Log ("SQ awc.toPrint() [" + awc.toPrint() + "]" );
        //api.Log.Log ("SQ cols_IDS 1 [" + cols_IDS + "]");
        cols_IDS = cols_IDS.getDeepCopy ();
        //api.Log.Log ("SQ cols_IDS.toString() 2 [" + cols_IDS.toString () + "]");
        cols_IDS.resetTables (schema); // (5)

        // RESOLVE UNKNOWN USER SEL COLS IDS TABS
        sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown (// side effect - set tables in Cols arg
                cols_IDS ,
                hmUnknownCols_StringColToStringSetTables ,
                new Tables ());
        //api.Log.Log ("SQ post resolve cols_IDS 3 [" + cols_IDS + "]");
        Tables tabs_FromIDcols = cols_IDS.getTables ();
        //api.Log.Log ("SQ tabs_FromIDcols 4 [" + tabs_FromIDcols + "]");
// (6)
        Columns colsAtomicWhere = awc.getColumns ();
        //api.Log.Log ("SQ colsUCSUBTREE 5 [" + colsUCSUBTREE + "]");
        sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown (// side effect - set tables in Cols arg
                colsAtomicWhere ,
                hmUnknownCols_StringColToStringSetTables ,
                cols_IDS.getTables ());
        //api.Log.Log ("SQ colsUCSUBTREE 6  [" + colsUCSUBTREE + "]");
        Tables tabsUCSUBTREE_TABS = colsAtomicWhere.getTables ();
        //api.Log.Log ("SQ tabsUCSUBTREE_TABS 7 [" + tabsUCSUBTREE_TABS + "]");

// (7)                                                2
        Tables tabsUSER_SEL_COLS_IDS_TABS = cols_IDS.getTables (); // still known and unknown
        //api.Log.Log ("SQ tabsUSER_SEL_COLS_IDS_TABS 8 [" + tabsUSER_SEL_COLS_IDS_TABS + "]");

// (8) union 6 and 7
        Tables tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS = new Tables ();
        //api.Log.Log ("SQ tabsUCSUBTREE_TABS 9 [" + tabsUCSUBTREE_TABS + "]");
        //api.Log.Log ("SQ tabsUSER_SEL_COLS_IDS_TABS 10 [" + tabsUSER_SEL_COLS_IDS_TABS + "]");
        tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS.addAll (tabsUCSUBTREE_TABS);
        //api.Log.Log ("SQ tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS 11 [" + tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS + "]");
        tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS.addAll (tabsUSER_SEL_COLS_IDS_TABS);
        //api.Log.Log ("SQ tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS 12 [" + tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS + "]");
// (8.5)
        tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS =
                sql4j.CQLInterpreter.CQLInterpreterUtils.getExtendToIncludeJoinTabs (
                tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS, false );
        //api.Log.Log ("SQ tabsUSER_SEL_AND_SUBTREE_IMPLIED_JOINS_TABS 13 [" + tabsUSER_SEL_AND_SUBTREE_IMPLIED_JOINS_TABS + "]");

        //api.Log.Log ("SQ tabsAllThisSubSelect [" + tabsAllThisSubSelect + "]");

// RESOLVE UNKNOWN TABS in (8)
        sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown (// side effect - set tables in Cols arg
                cols_IDS ,
                hmUnknownCols_StringColToStringSetTables , tablesKnownPrior
        );

// System.out.println("colsUSER_SEL_COLS_IDS_NEW_TAB_ASSIGNMENT resolved [" + colsUSER_SEL_COLS_IDS_NEW_TAB_ASSIGNMENT + "]");
// (9)
        Tables tabsAllThisAtomicSubSelectInclJoin = // this may be just an insurance call
                CQLInterpreterUtils.getExtendToIncludeJoinTabs ( tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS, false);

        String sJoinStrAllThisSubSelectInclJoin = CQLInterpreterUtils.buildJoinStrings ( tabsAllThisAtomicSubSelectInclJoin,
                true, awc);    // this is an atomic WC - null since no joins will be built
// System.out.println("sAllTabsJoins [" + sAllTabsJoins + "]");
        String sAWC = awc.toString ();
//System.out.println("sAWC [" + sAWC + "]");
        Tables tabsFINAL_USER_SEL_AND_JOINS_RESOLVED = cols_IDS.getTables ();
        String sWC = awc.toString ();

        String sNodeDoc_Node_hints = "";
//System.out.println("tabsAllThisSubSelect [" + tabsAllThisSubSelect + "]");
        boolean bHasNode = tabsAllThisAtomicSubSelectInclJoin.toSet ().contains ("NODE");
        boolean bHasNodeDoc = tabsAllThisAtomicSubSelectInclJoin.toSet ().contains ("NODEDOCUMENT");

        if (bHasNode && bHasNodeDoc)
            sNodeDoc_Node_hints = " /*+ index(NODEDOCUMENT NODEDOCUMENTNODEID) */ ";

        StringBuffer sbFinal = new StringBuffer ();
//System.out.println("sbFinal1[" + sbFinal+ "]");
        sbFinal.append ("select " + sNodeDoc_Node_hints + cols_IDS.toString ());
//System.out.println("sbFinal2[" + sbFinal+ "]");
        sbFinal.append (" from " + tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS.toString ());
//System.out.println("sbFinal3[" + sbFinal+ "]");
        String sANDifNeeded = "";
        if (!sJoinStrAllThisSubSelectInclJoin.trim ().equals (""))
            sANDifNeeded = " and ";
        sbFinal.append (" where " + sJoinStrAllThisSubSelectInclJoin + sANDifNeeded + sAWC);

        //System.out.println("sbFinal4[" + sbFinal+ "]");
        return sbFinal.toString ();
    }
}

