/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 29, 2002
 * Time: 8:31:50 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import java.util.*;
import java.io.PrintWriter;
import java.sql.*;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.util.*;

public class SQLRunQAMode1 {
    // **********************************************************************
    public static void runQA(
            CQLCombinedDataFT cds,
            PrintWriter out ,
            Connection dbc
            )  throws Exception
    {
        inserttrecCombineRecs(cds, out, dbc);
        outPRNums (cds, out, dbc);

    }
    // **********************************************************************
    private static void inserttrecCombineRecs (
                CQLCombinedDataFT cds,
                PrintWriter out ,
                Connection dbc
                )  throws Exception
    {
        String sSQLdel = "delete from treccombine where xactid = " + cds.iTransactionID;
        int inumdel = executeUpdateToDB(sSQLdel, dbc, true, -1 );
        com.indraweb.util.Log.logClear("SQL QA inumdel [" + inumdel + "] \r\n");
        dbc.setAutoCommit(false);
        Statement stmt = dbc.createStatement();
        for ( int i = 0; i < cds.vSSQLInsertsForCombinedTable.size(); i++ )
        {
            String sSQL = (String) cds.vSSQLInsertsForCombinedTable.elementAt(i);
            stmt.executeUpdate (sSQL );
            //com.indraweb.util.Log.logClear("SQL QA insert [" + sSQL + "] \r\n");
        }
        stmt.close();
        dbc.commit();
        Log.logClear("SQL QA completed treccombine recs [" + cds.vSSQLInsertsForCombinedTable.size()+ "] \r\n");
    }

    // **********************************************************************
    private static void outPRNums (
            CQLCombinedDataFT cds,
            PrintWriter out ,
            Connection dbc
            )  throws Exception
    {
        String sSQLDocsTrechits = "select distinct (documentid) from trecqrel where qestionid = " +
            cds.iQid + " and status = 1 ";
        HashSet hsTrecDocs = gethsFromSQLIDocIDs ( sSQLDocsTrechits ,  dbc) ;

        int[] iArrTopNumRecs = {15,30,50,100};

    Log.logClear("************* \r\n");
    Log.logClear("************* QUALITY REPORT Q " + cds.iQid + "\r\n");
    Log.logClear("************* \r\n");
        Log.logClear("iTransactionID:"+cds.iTransactionID+ "\r\n");
        Log.logClear("iQid:"+cds.iQid+ "\r\n");
        Log.logClear("sNodeConstraint:"+cds.sNodeConstraint+"\r\n");
        for ( int iTNRidx = 0;iTNRidx < iArrTopNumRecs.length; iTNRidx++)
        {
            String sSQLDocsIWhits = "select distinct (documentid) from treccombine where xactid = " +
                    cds.iTransactionID + " and counter < " + iArrTopNumRecs[iTNRidx];
            HashSet hsCombinedDocs =    gethsFromSQLIDocIDs ( sSQLDocsIWhits,  dbc) ;

            int iCntCombined = hsCombinedDocs.size();
            int iCntTrec = hsTrecDocs.size();
            HashSet hsIntersect = hsIntersect (hsTrecDocs, hsCombinedDocs);
            HashSet hsFalseP = hsMinus (hsCombinedDocs, hsTrecDocs);
            HashSet hsFalseN = hsMinus (hsTrecDocs, hsCombinedDocs);
            int iCntIntersect= hsIntersect (hsTrecDocs, hsCombinedDocs).size();
            int iTopNumRecs =  iArrTopNumRecs[iTNRidx] ;
            Log.logClear("iTopNumRecs:"+iTopNumRecs+"\r\n");
            Log.logClear("iCntCombined:"+iCntCombined+"\r\n");
            Log.logClear("iCntTrec:"+iCntTrec+"\r\n");
            Log.logClear("iCntIntersect:"+iCntIntersect+"\r\n");
            //int iCntFalseNeg = hsMinus (hsTrecDocs, hsCombinedDocs).size();
            //int iCntFalsePos = hsMinus (hsCombinedDocs, hsTrecDocs).size();
            String sP = "P_" +iTopNumRecs + ">";
            String sR = "R_" +iTopNumRecs + ">";


            double dP = ((double) iCntIntersect/ (double) iCntCombined);
            double dR = ((double) iCntIntersect/ (double) iCntTrec );

            Log.logClear("<"+sP + UtilStrings.numFormatDouble( dP,2) +"</" + sP+ "\r\n");
            Log.logClear("<"+sR + UtilStrings.numFormatDouble(dR,2) +"</" + sR+ "\r\n");
            out.println("<"+sP + UtilStrings.numFormatDouble( dP,2) +"</" + sP );
            out.println("<"+sR + UtilStrings.numFormatDouble( dR,2)  +"</" + sP );

            Log.logClear("FALSE POS #[" + hsFalseP.size() + "]  [" + getStrFromHs(hsFalseP) + "]\r\n");
            Log.logClear("FALSE NEG #[" + hsFalseN.size() + "]  [" + getStrFromHs(hsFalseN) + "]\r\n");


            Log.logClear("\r\n");

        }
    }

    private static String getStrFromHs(HashSet hs ) throws Exception
    {
        Vector v = new Vector();
        Iterator i = hs.iterator();
        while ( i.hasNext() ) {
            v.add(i.next() );
        }
        com.indraweb.util.sort.SortIntegers.sortIntVec( v );
        return getStrFromVec(v);
    }

    public static String getStrFromVec(Vector v)
    {
        StringBuffer sb = new StringBuffer();
        for ( int i = 0; i < v.size(); i++)
        {
            if ( i > 0)
                sb.append(",");
            sb.append(v.elementAt(i));
        }
        return sb.toString();

    }

    // **********************************************************************
    public static HashSet gethsFromSQLIDocIDs ( String sSQL, Connection dbc  )
    throws Exception
    {
        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery (sSQL);
        HashSet hsRet = new HashSet();
        while ( rs.next() ) {
            int iDocID = rs.getInt(1) ;
            hsRet.add(new Integer ( iDocID));
        }
        rs.close();
        stmt.close();
        return hsRet;
    }

    // **********************************************************************
    public static HashSet hsIntersect (HashSet hs1, HashSet hs2)
        throws Exception
    {
        HashSet hsnew = new HashSet();
        Iterator e = hs1.iterator();
        while ( e.hasNext() )
        {
            Object O = e.next();
            if (hs2.contains(O))
                hsnew.add(O);
        }
        return hsnew;
    } // hsIntersect

    public static HashSet hsMinus (HashSet hs1, HashSet hs2)
    {
        HashSet hsnew = new HashSet();
        Iterator e = hs1.iterator();
        while ( e.hasNext() )
        {
            Object O = e.next();
            if (!hs2.contains(O))
                hsnew.add(O);
        }
        return hsnew;
    } // hsMinus


    // ----------------------------------------------------------
	public static int executeUpdateToDB (String aSQL,
										  Connection dbc,
										  boolean bCommit ,
											int iMinUpdates)
throws Exception
	{
	// ----------------------------------------------------------
		//Log.logClear("JDBCIndra_Connection.java executeUpdateToDB aSQL [" + aSQL + "]\r\n");
		Statement stmt = null;
		int iNumUpdates = -1;

		try {
			stmt = dbc.createStatement();
			iNumUpdates = stmt.executeUpdate (aSQL);
			if ( iNumUpdates < iMinUpdates )
			{
				Log.log ("update did not fix min number records [" + aSQL + "]\r\n");
				throw new Exception ("update did not fix min number records [" + aSQL + "]");
			}
			if (bCommit)
				dbc.commit();
		}
		catch(Throwable  e) {
				throw new Exception("error in executeUpdateToDB " + e.getMessage());
		}
		finally  {
			stmt.close();
			// hbk timer timer.stop();
		}
		return iNumUpdates;
	}

}