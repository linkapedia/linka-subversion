
package sql4j.CQLInterpreter;

import java.util.*;
import java.sql.*;
import java.io.PrintWriter;

import com.indraweb.util.Log;
import com.indraweb.util.*;
import com.indraweb.execution.Session;
import com.iw.system.User;
import sql4j.parser.SQL;

public class CQLTestClient
{

    static boolean bSkipDBConnect = false;
    // run db query
    static boolean bExecuteFinalSQL = true;
    //static boolean bSkipDBConnect = false;  // false to gen the query AND execute it
    // loop ?
    static boolean bRunTestArrayLoopVsSingleTest_falseForSingleTest = false;  // false to gen the query AND execute it
    static int iTestCellToRun = -1;  // false to gen the query AND execute it
    //static int iTestCellToRun = 141;  // false to gen the query AND execute it
    static boolean bPauseWithKeyboardWaitBetweenTests = false;  // false to gen the query AND execute it
    static int iStartIndexInclusive = 0;
    static int iNumToRun = 200;
    static final int iDB_client = 1;  // false to gen the query AND execute it
    static final int iDB_perseus = 2;  // false to gen the query AND execute it
    static final int iDB_gaea = 4;  // false to gen the query AND execute it
    static final int iDB_medusa = 8;  // false to gen the query AND execute it
    static final int iDB_pandora = 16;  // false to gen the query AND execute it
    static int iDB = iDB_medusa ;  // false to gen the query AND execute it

    static int iCallCounter = 0;
    static HashSet hsSkipTests = new HashSet();

    public static void main (String[] args)
    {
        hsSkipTests.add (new Integer (14));  // too long USES NODE UNNECESSARILY I THINK :
        hsSkipTests.add (new Integer (17));  // too long sElEcT <dOcUmEnT> wHeRe cOrPuSiD = 1 AnD CoRpUsId = 100040
        hsSkipTests.add (new Integer (18));  // too long
        hsSkipTests.add (new Integer (61));  // too long
        hsSkipTests.add (new Integer (27));  // security
        hsSkipTests.add (new Integer (28));  // security
        hsSkipTests.add (new Integer (29));  // security
        hsSkipTests.add (new Integer (30));  // security
        hsSkipTests.add (new Integer (41));  // pos constraint
        hsSkipTests.add (new Integer (43));  // too slow
        hsSkipTests.add (new Integer (47));  // too slow - hours
        hsSkipTests.add (new Integer (48));  // too slow - 40 mins so far
        hsSkipTests.add (new Integer (53));  // pos constraint
        hsSkipTests.add (new Integer (73));  // pos constraint
        hsSkipTests.add (new Integer (84));  // debug : score1 not in select doc where node ... and score > 50
        hsSkipTests.add (new Integer (86));  // too slow
        hsSkipTests.add (new Integer (87));  // debug ? select doc where node order ftscore ... not selected set ... debug ?
        hsSkipTests.add (new Integer (88));  // slow
        int iTestCellToStartAt = 0;  // false to gen the query AND execute it
        try
        {  /* Vector v = new Vector();
            v.addElement("test");
            int iPOs = v.indexOf("test") ;
            api.Log.Log ("zxczxcz" + iPOs);*/

            initSessionForTest ();
            Connection dbc = null;
            if (bSkipDBConnect == false)
                dbc = initializeDB ("API");

            int iRowMax = -1;

            java.io.PrintWriter pw = new java.io.PrintWriter (System.out);
            api.Log.Log ("!!!!!!! CQLInterpret DEBUG : USING DEBUG USER !!!!!!!!!!");
            api.Log.Log ("!!!!!!! CQLInterpret DEBUG : USING DEBUG USER !!!!!!!!!!");
            //User user = api.security.TSLogin.localLogin ("hkon" , "racer9" ,
              //      new PrintWriter (System.out) , dbc);
            //if (user == null)
              //  throw new Exception ("user null in TSClassify debug");
            if (bRunTestArrayLoopVsSingleTest_falseForSingleTest)
            {
                Vector vTestCells = new Vector ();
                vTestCells = createVTestCells ();
                for ( int iLoop = 0 ; iLoop < vTestCells.size () ; iLoop++ )
                {

                    if ( iTestCellToRun >= 0 && iTestCellToRun != iLoop )
                        continue;
                    if ( hsSkipTests.contains(new Integer (iLoop) ) )
                        continue;
                    if ( iLoop < iTestCellToStartAt )
                    {
                        api.Log.Log ("skip test [" + iLoop + "]");
                        continue;

                    }
                    api.Log.Log ("\r\n\r\n\r\n#########################################################################");
                    api.Log.Log ("#########################################################################");
                    api.Log.Log ("#########################################################################");
                    api.Log.Log ("############ CQL Test # STARTING RUN [" + iLoop + "] of [" +
                            vTestCells.size () + "] " + new java.util.Date() );
                    long lStartThisLoopIteration = System.currentTimeMillis() ;
                    TestCell tc = (TestCell) vTestCells.elementAt (iLoop);
                    api.Log.Log ("$$$$$$$$$$$$$$ " + iLoop + ". CQL [" + tc.sCQL + "] ");

                    if (iLoop < iStartIndexInclusive || iLoop >= iStartIndexInclusive + iNumToRun)
                    {
                        api.Log.Log ("SKIPPING CQL TEST INDEX [" + iLoop + "]");
                        continue;
                    }

                    try
                    {
                        int iRecordCount = CQLxmlGen.topLevelCallFromTSCql (    // looped call
                                tc.sCQL ,
                                1 ,
                                iRowMax ,
                                pw ,
                                dbc ,
                                bSkipDBConnect ,
                                bExecuteFinalSQL,
                                null
                        );
                        pw.flush ();
                        String sRecs= "-";
                        if (iRecordCount > 0)
                            sRecs = "+";
                        com.indraweb.util.Log.logcr(sRecs + "testcell [" + iLoop + "] #rec [" + iRecordCount + "] done in ms [" + (System.currentTimeMillis()- lStartThisLoopIteration) + "] " +
                          " tc.sCQL [" + tc.sCQL + "]" );

                    } catch ( Throwable t )
                    {
                        api.Log.LogError("top level test loop" , t);
                    }
                    if ( bPauseWithKeyboardWaitBetweenTests )   {
                        System.out.println(" i [" + iLoop + "] hit any key to continue");
                        com.indraweb.util.HKonLib.PauseForKeyboardInput();
                    }
                }
            }
            else // run one test
            {
               // while ( true )
                {

                    api.Log.Log ("############  CQL Test Client SINGLE RUN ");
                    int iRecordCount = -1;
                    long lStartThisLoopIteration = System.currentTimeMillis() ;
                    String sCQL = null;
                    try
                    {

                        sCQL =
                                //sArrTests[i],
                                //"SELECT <NODEDOCUMENT> WHERE NODEID = 1087178";
                                //"SELECT <NODEDOCUMENT> WHERE NODEID = 1087178";
                                //"SELECT <NODE> WHERE NODEID = 16717";
                                //"SELECT <NODE> WHERE NODEID = 16717 AND NODEID = 16718 " ;
                                //"SELECT <DOCUMENT> WHERE NODEID = 16717 AND NODEID = 16718 " ;
                                //"SELECT <NODE> WHERE PARENTID = 24241";
                                //"SELECT <IDRACNODE> WHERE NODETITLE = 'node1.1 10318485'"; // works

                                // ***********************************

                                //"SELECT <IDRACNODE> WHERE NODETITLE = 'anthrax'", // works
                                //"SELECT <LINKNODE> WHERE NODEID = 10318485",
                                //"SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'Medicine' ORDER BY DATELASTFOUND" ,
                                //"SELECT <DOCUMENT> WHERE (NODEID = '421517')  combine fulltext null" ,
                                //"SELECT <DOCUMENT> WHERE NODEID = 421448 combine fulltext null",
    //                            "SELECT <DOCUMENT> WHERE NODEID = 421448 AND (FULLTEXT LIKE '(Osteroporosis)' or FULLTEXT LIKE '(Osteroporosis2)')  " +
    //            " COMBINE FULLTEXT LIKE '(Osteoporosis ACCUM bone loss)' and " +
    //            " (docurl not like '%www.idrac%') ",
            //"SELECT <NODE>  WHERE  NODETITLE = 'test'"
            //"SELECT <NODEDOCUMENT>  WHERE  CORPUSID = '30'"

            // 2004 02 13 test MINUS IN NOT NODETITLE QUERY
            // "SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE 'CHINA') AND ( NODETITLE NOT LIKE 'Taiwan')";
            //"SELECT <DOCUMENT> WHERE ( NODETITLE LIKE 'CHINA') AND ( NODETITLE NOT LIKE 'Taiwan')";
            //"SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE 'CHINA') AND ( NODETITLE NOT LIKE 'Taiwan6') ";
            //"SELECT <DOCUMENT> WHERE ( DOCTITLE  LIKE 'CHINA') AND ( NODETITLE NOT LIKE 'Taiwan6') ";
            //"SELECT <DOCUMENT> WHERE ( DOCTITLE  LIKE 'water and ') AND ( NODETITLE NOT LIKE 'Taiwan6') ";
                                //  original hoey query
           //"SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE ' BOTTLED AND  WATER') AND ( NODETITLE <> 'Solubilities of Acrylonitrile in Water')";
           //  node not doc on left
           //"SELECT <DOCUMENT> WHERE ( DOCTITLE = 'T1') AND ( NODETITLE LIKE 'T2' AND NODETITLE <> 'T3' )";
    //       "SELECT <DOCUMENT> WHERE NODETITLE <> 'T1' AND DOCTITLE NOT LIKE 'T2' AND DOCTITLE NOT LIKE 'T3'";
           //  node neg at level 0 on doc selection
           //"SELECT <DOCUMENT> WHERE NODETITLE <> 'Solubilities of Acrylonitrile in Water'";
           //"SELECT <NODE> WHERE NODETITLE LIKE 't1' AND  (NODETITLE NOT LIKE 't2' AND  NODETITLE NOT LIKE 't3')";


    /*
                                "SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE ' BOTTLED AND WATER') AND " +
                                             " ( NODETITLE <> 'Solubilities of Acrylonitrile in Water' )" ;
    */
                        // hbk22  "SELECT <DOCUMENT> WHERE DOCUMENTID = 1111 AND  NODEID = 2222" +
                        //hbk22 "SELECT <DOCUMENT> WHERE NODEID = 1 AND NODEID = 2 and DOCUMENTID = 2" +
                        //hbk22  "SELECT <NODEDOCUMENT>  WHERE NODEID = 1340577 AND SCORE1 > 25 ORDER BY SCORE1 DESC" +
    //                            "SELECT <NODEDOCUMENT>   WHERE  NODEID = 1369686  AND SCORE1 > 25 ORDER BY SCORE1 DESC" +
    //                            "SELECT <NODEDOCUMENT> WHERE NODEID = 1340595 AND CS_PUBLISHERTEXT LIKE 'test'" +
    //                            "SELECT <NODEDOCUMENT> WHERE NODEID = 1340595 AND CS_PUBLISHERTEXT LIKE 'CS_PUBLISHERTEXTtest' AND FULLTEXT LIKE 'doctest' order by nodeid,    CS_PUBLISHERTEXT LIKE 'CS_PUBLISHERTEXTtest'" +
    //                            " SELECT \t <DOCUMENT> WHERE NODEID = 1340595 ORDER BY NODEID, \t    FTSCORE  , \t DOCUMENTID" +
    //                            " SELECT \t <NODEDOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID  " +
    //                            " SELECT \t <DOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID  " +
    //                            " SELECT \t <NODE> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, NODEID " +
    //                            " SELECT \t <DOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID" +
    //                            " SELECT \t <NODEDOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID" +
    //                              " SELECT \t <DOCUMENT> WHERE SCORE1 > 50 AND NODETITLE LIKE 'MOSQUITO' ORDER BY FTSCORE, DOCUMENTID" +
    // bad query                              "SELECT <DOCUMENT> WHERE NODEID = 421448 AND (DOCTITLE LIKE 'aids'  or DOCTITLE LIKE 'title')    ORDER BY DOCTITLE LIKE 'aids'  DESC" +
    //                    "SELECT <NODEDOCUMENT> WHERE (NODEID= 421450 and NODEID= 421451 and FULLTEXT like 'johnny')  and FULLTEXT LIKE 'amyotrophic' " +
    //                    "SELECT <DOCUMENT> WHERE NODEID UNDER 16717" +
    //                    "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'CANINEcrisp' AND DOCTITLE LIKE 'time' ORDER BY FTSCORE DESC, DOCUMENTID ASC, DOCTITLE DESC" +
                        // "SELECT <DOCUMENT> WHERE SIGNATUREWORD LIKE 'IODINE'" +
                        //"SELECT <DOCUMENT> WHERE SIGNATUREWORD LIKE 'hormone' AND REPOSITORYID = 12" +
                        //"SELECT <NODEDOCUMENT> WHERE SIGNATUREWORD LIKE 'hormone' AND CORPUSID = 12" +
                       //"seleCt <docuMent> wHere signatureWord = 'hormOne' and cOrpusid = 12" +
                       //"SeLeCt <dOcUmEnT> wHeRe NoDeId unDer 1" +
                       //"SeLeCt <dOcUmEnT> wHeRe \tnodEiD    uNdeR 1" +
                       //"SeLeCt <dOcUmEnT> wHeRe \tnodEiD    uNdeR 1 aNd  nodEiD    uNdeR 2   " +
                       //"SeLeCt <dOcUmEnT> wHeRe (NoDeId unDer 1 oR (nOdEiD = 2 AnD NoDeId under 3)) aNd FuLlTeXt lIkE 'aStRoLoGeR' OrDeR By dOcUmEnTiD DeSc" +
                       //"seleCt <docuMent> wHere signatureWord = 'hormOne' and cOrpusid = 12" +
                       //"SELECT <DOCUMENT> WHERE NODEID = 1340595 AND CS_PUBLISHERTEXT LIKE 'CS_PUBLISHERTEXTtest' AND FULLTEXT LIKE 'doctest' order by documentid,    CS_PUBLISHERTEXT, FTSCORE DESC" +
                       //"SELEcT Node WHeRE NODEID = 1125825 ORDER BY nodeTITLE ASC" +
                       //"SELECT <NODE> WHERE NODETITLE LIKE '%%gsm%%' ORDER BY NODETITLE ASC" +
                       //"SELECT <IDRACNODE> WHERE NODEID = 421390" +
                       // "SELECT <NODEDOCUMENT> WHERE (NODETITLE LIKE 'forging') and ((NODETITLE LIKE 'nitration') or (NODETITLE LIKE 'filtration')) order by nodetitle ASC, docsize DESC, FTSCORE DESC" +
                       //"SELECT <IDRACNODE> WHERE NODETITLE = 'node1.1 10318485'" +
                       //"SELECT <IDRACNODE> WHERE NODEID = 1406603" +
                      // "SELECT <IDRACNODE> WHERE NODETITLE = 'Overall Conclusions And Benefit/Risk Assessment Quality'" +
                      //"SELECT NODE  WHERE  (FULLTEXT LIKE '(fulltextand)') AND (DOCTITLE LIKE '(doctitle)') AND ((NODETITLE = 'topictitle'))AND CORPUSID = '30' COMBINE FULLTEXT LIKE '(fulltextor)'"+
                      //"SELECT NODE  WHERE  (FULLTEXT LIKE '(fulltextand)') COMBINE FULLTEXT LIKE '(fulltextor)'"+
                      //"SELECT <DOCUMENT> WHERE  (FULLTEXT LIKE '(fulltextand)') COMBINE FULLTEXT LIKE '(fulltextor)'"+
                      //"SELECT <DOCUMENT> WHERE  FULLTEXT LIKE '(fulltextand)' COMBINE FULLTEXT LIKE '(fulltextor)'"+
                      //"SELECT <DOCUMENT> WHERE  DOCTITLE LIKE '(fulltextand)' and DOCTITLE LIKE 'dog' COMBINE FULLTEXT LIKE 'fulltextor'"+
                      //"SELECT <DOCUMENT> WHERE FULLTEXT LIKE '(autism)' COMBINE FULLTEXT LIKE '(autistic)'" +
                      // hbk 2005 06 32 obsolete "SELECT <NODE> WHERE NODETITLE = 'Sub Three'" +
                                 //"SELECT <NODEDOCUMENT> WHERE NODETITLE LIKE 'magnetic' AND NODETITLE LIKE 'gadolinim'" +
                               // "SELECT <NODE> WHERE NODETITLE LIKE 'magnetic' AND NODETITLE LIKE 'gadolinim' ORDER BY FTSCORE DESC" +
                                //"SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'magnetic' AND DOCTITLE LIKE 'gadolinim' " +
                               // "SELECT <NODE> WHERE NODETITLE LIKE 'magnetic' ORDER BY FTSCORE asc" +
                               // "SELECT <DOCUMENT> WHERE DOCTITLE LIKE 'magnetic' " +
                                // "SELECT <DOCUMENT> WHERE NODETITLE LIKE 'magnetic' AND NODETITLE LIKE   'gadolinim' " +
                                // "SELECT <NODE> WHERE NODETITLE LIKE 'magnetic'  ORDER BY FTSCORE asc" +
                    // "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'time'" +





                                //ORI: "SELECT <DOCUMENT> WHERE ( DOCTITLE NOT LIKE ' SYN({REMOVED},idrac) AND  " +
                                // "SYN({IDRAC},idrac)' AND  DOCTITLE NOT LIKE ' SYN({REORGANIZED},idrac) AND  " +
                                // "SYN({IDRAC},idrac)') AND ( NODETITLE LIKE 'SYN({AAA}, idrac)' AND NOT( NODETITLE LIKE 'SYN({BBB}, idrac)' " +
                                // "OR NODETITLE LIKE 'SYN({CCC}, idrac)'))   AND VISIBLE = 1 ORDER BY DOCTITLE ASC" +

                                                                                                //FULLTEXT LIKE 'SYN(dog INDRAHK)'
                                //1 "SELECT <DOCUMENT> WHERE ( FULLTEXT LIKE ' SYN(REMOVED,idrac) ' " +
                                //2 "SELECT <DOCUMENT> WHERE DOCTITLE LIKE ' SYN({REMOVED},INDRAHK) AND SYN({IDRAC},INDRAHK)' " +
                                // " AND DOCTITLE NOT LIKE ' SYN({REMOVED2},INDRAHK) AND SYN({IDRAC2},INDRAHK)' " +
                                //3
                                //4 "SELECT <DOCUMENT> WHERE  ( DOCTITLE NOT LIKE ' SYN({REMOVED},idrac) AND      SYN({IDRAC},idrac)'    AND     DOCTITLE NOT LIKE ' SYN({REORGANIZED},idrac) AND   	SYN({IDRAC},idrac)' )  " +
                                //5 "SELECT <DOCUMENT> WHERE  NODETITLE LIKE 'SYN({EXP1}, idrac)' " +
                                //"SELECT <DOCUMENT> WHERE  NODETITLE LIKE 'SYN({EXP1}, idrac)'  AND   (  	NODETITLE NOT LIKE 'SYN({EXP2}, idrac)' AND NODETITLE NOT LIKE 'SYN({EXP3}, idrac)' )    AND VISIBLE = 1   ORDER BY DOCTITLE ASC  " +
                                // double minus test
                                //" SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE 'CHINA') AND ( NOT  NODETITLE = 'Taiwan')  AND ( NODETITLE NOT LIKE 'Japan') " +
                                "SELECT <DOCUMENT> WHERE NODEID under 32046" +

                        //"AND  " +
                                // "SYN({IDRAC},idrac)') AND ( NODETITLE LIKE 'SYN({AAA}, idrac)' AND NOT( NODETITLE LIKE 'SYN({BBB}, idrac)' " +
                                // "OR NODETITLE LIKE 'SYN({CCC}, idrac)'))   AND VISIBLE = 1 ORDER BY DOCTITLE ASC" +

                      //  "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'CANINE' ORDER BY FTSCORE DESC" +

                             //   " SELECT \t <DOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID  " +
                                //"SELECT <NODEDOCUMENT> WHERE FULLTEXT LIKE 'Malaria and Mosquito' ORDER BY DATELASTFOUND" +
                                //"SELECT <NODEDOCUMENT>   WHERE  NODEID = 1369686  AND SCORE1 > 25 ORDER BY SCORE1 DESC" +
                        //"SELECT <NODEDOCUMENT> WHERE NODEID = 1401416 AND DOCUMENTID = 1006898" +
                        //3 "SELECT <NODEDOCUMENT> WHERE DOCTITLE LIKE 'testing1' OR DOCSUMMARY LIKE 'testing2'" +
                        //" (NODETITLE NOT LIKE 'T1' AND NODETITLE NOT LIKE 'T2') AND  DOCTITLE LIKE 'T3' " +
                                     " " ;
                        //" ( NODETITLE <> 'Solubilities of Acrylonitrile in Water' AND " +
                        //" NODETITLE <> 'test nodetitle' )";


    /*       "SELECT <DOCUMENT> WHERE ( NODETITLE <> 'tw1' AND " +
                        " NODETITLE <> 'tw2' )";*/
                     //   "SELECT <NODE>  WHERE  DOCTITLE = 'test' AND DOCTITLE = 'test'";
                                //  swap the pos and neg
           //"SELECT <DOCUMENT> WHERE ( NODETITLE <> 'Solubilities of Acrylonitrile in Water') AND ( DOCTITLE LIKE 'BOTTLED AND  WATER') ";
             //           "SELECT <NODE> WHERE NODEID <> 16717 AND NODEID = 16717";
            // no AND "SELECT <DOCUMENT> WHERE ( NODETITLE <> 'Solubilities of Acrylonitrile in Water')";
    /*
                        [12:28] IndraWebMP:  SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE ' CHINA') AND ( NODETITLE NOT LIKE 'Taiwan')
                        [12:28] IndraWebMP:  SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE ' CHINA') AND ( NODETITLE <> 'Taiwan')
                        [12:28] IndraWebMP: SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE ' CHINA') AND ( NODETITLE !='Taiwan')
    */

                    iRecordCount = CQLxmlGen.topLevelCallFromTSCql (    // single call
                            sCQL,
                                1 ,
                                iRowMax ,
                                pw ,
                                dbc ,
                                bSkipDBConnect ,
                                bExecuteFinalSQL ,
                                null
                        );
                        pw.flush ();
                        // com.indraweb.util.Log.logcr("Test [" + i + "] done in ms [" + (System.currentTimeMillis()- lStartThisLoopIteration) + "]");

                    } catch ( Throwable t )
                    {
                        com.indraweb.util.Log.NonFatalError ("top level test loop 2" , t);

                    }
                    com.indraweb.util.Log.logcr("testsingle #rec [" + iRecordCount + "] done in ms [" + (System.currentTimeMillis()- lStartThisLoopIteration) + "] " +
                  " tc.sCQL [" + sCQL + "]" );
                } // while
            }  // else run one test
            pw.close ();
            com.indraweb.util.Log.logcr ("#### CQL Test Client # FINISHING RUN(S)");

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in main" , e);
        }
        int i = 0;
        System.out.println ("DONE CQLTestClient");

    }

// as cut and pasted from ts.java for local	use
    public static Connection initializeDB (String sWhichDB)
    {
        try
        {
            Connection dbc = api.statics.DBConnectionJX.getConnection (sWhichDB);
            Hashtable htargs = new Hashtable ();

            String sSQL = "	select ParamName, ParamValue from ConfigParams ";
            Statement stmt = dbc.createStatement ();
            ResultSet rs = stmt.executeQuery (sSQL);

            while (rs.next ())
            {
                String sK = rs.getString (1);
                String sV = rs.getString (2);

                if (sV == null)
                {
                    sV = "";
                }
                htargs.put (sK , sV);
            }

            Session.cfg.addToProperties (htargs);
            rs.close ();
            stmt.close ();
            return dbc;
        } catch ( Exception e )
        {
            api.Log.LogError ("error	getting	DB connection internal stack trace" , e);
            return null;
        }
    }

    private static void initSessionForTest () throws Exception
    {

        // SET UP ENV SINCE NOT COMING THRU JRUN
        if (!com.indraweb.execution.Session.GetbInitedSession ())
        {
            //com.indraweb.util.UtilFile.addLineToFile("/temp/temp.txt", new java.util.Date() +	": doing init session  \r\n");
            Hashtable htprops = new Hashtable ();
            // Load	environment	from JRUN config application variables first
            Session.sIndraHome = "C:/Program Files/ITS";
            Session.sLogNameDetail = Session.sIndraHome + "/logs/LogDetail.txt";
            htprops.put ("ImportDir" , "C:/TEMP");
            htprops.put ("AuthenticationSystem" , "ActiveDirectory");
            htprops.put ("MachineName" , "indraweb-e5j05c");
            htprops.put ("DBString_API_User" , "sbooks");
            htprops.put ("DBString_SVR_User" , "sbooks");
            htprops.put ("DBString_API_Pass" , "racer9");
            htprops.put ("DBString_SVR_Pass" , "racer9");
            String sDBString = null;
            if (iDB == iDB_client)
                sDBString = "jdbc:oracle:thin:@66.134.131.60:1521:client";
            else if (iDB == iDB_perseus)
                sDBString = "jdbc:oracle:thin:@66.134.131.37:1521:perseus";
            else if (iDB == iDB_medusa)
                sDBString = "jdbc:oracle:thin:@66.134.131.62:1521:medusa";
            else if (iDB == iDB_pandora)
                sDBString = "jdbc:oracle:thin:@66.134.131.60:1521:pandora";
            else
                throw new Exception ("invalid DB selected in CQLTestCLient");

            api.Log.Log ("db : " + sDBString);
            api.Log.Log ("db : " + sDBString);
            api.Log.Log ("db : " + sDBString);
            api.Log.Log ("db : " + sDBString);
            api.Log.Log ("db : " + sDBString);
            htprops.put ("DBString_API_OracleJDBC" , sDBString);

            htprops.put ("DBString_SVR_OracleJDBC" , "jdbc:oracle:thin:@66.134.131.37:1521:gaea");
            htprops.put ("IndraHome" , Session.sIndraHome);
            htprops.put ("IndraFolderHome" , Session.sIndraHome);		//}
            htprops.put ("outputToFile" , "true");
            htprops.put ("outputToScreen" , "true");
            htprops.put ("MaxRecordCount" , "500");

            Session.cfg = new com.indraweb.execution.ConfigProperties (htprops);
            Session.SetbInitedSession (true);
        }

    }

    private static class TestCell
    {
        public float fTestNum = -1;
        public String sTestName = null;
        public String sCQL = null;

        public TestCell (float fTestNum_ , String sTestName_ , String sCQL_)
        {
            fTestNum = fTestNum_;
            sTestName = sTestName_;
            if ( sCQL_.indexOf("SECURITY:") < 0 && sCQL_.toUpperCase().indexOf("COMBINE FULLTEXT LIKE") < 0 )
                sCQL = mixcase (sCQL_);
            else
                sCQL = sCQL_;
        }
    }
    private static String mixcase (String s)
    {
        char[] cArr = s.toCharArray();
        for ( int i = 0; i < cArr.length; i++)
        {
            char[] cArr2 = new char[1];
            cArr2[0] = cArr[i];
            String s2 = new String(cArr2);
            if ( i % 2 == 0 )
                s2 = s2.toLowerCase();
            else
                s2 = s2.toUpperCase();

            cArr[i] = s2.toCharArray()[0];
        }

        String s3 = new String (cArr);
        api.Log.Log ("mix case [" + s + "] to [" + s3 + "]" );
        return s3;

    }

    private static Vector createVTestCells ()
    {
        Vector vTC = new Vector ();
        int i = 0;
        // simple start
        if (true && ( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "",
            "SELECT <NODE> WHERE NODEID = 16717"));

        if (true && ( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "",
            "SELECT <NODEDOCUMENT> WHERE NODEID = 16717"));

        if (true && ( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "",
            "SELECT <IDRACNODE> WHERE NODEID = 16717"));

        if (true && ( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "",
            "SELECT <DOCUMENT> WHERE NODEID = 16717"));

        if (true && ( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'Osteoporosis' "));

        if (true  && ( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 421445 AND DOCURL NOT LIKE 'http://www.idrac.com'"));

        if (true  && ( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID = 16717"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE DOCTITLE Like 'gulf war' and FULLTEXT Like 'king'"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (FULLTEXT LIKE 'king')"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                " SELECT <DOCUMENT> WHERE (NODEID = 1) AND " +
                " FULLTEXT LIKE 'Astrologer' " +
        " order by DOCUMENTID desc "));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'blood' and FULLTEXT LIKE 'hospital'"));

/*
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 421448 AND (DOCTITLE LIKE 'aids'  or DOCTITLE LIKE 'title')    ORDER BY DOCTITLE LIKE 'aids'  DESC"));
*/

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE ( FULLTEXT LIKE 'kidney')" ));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                " SELECT <DOCUMENT> WHERE (NODEID = 1 OR (NODEID = 2 AND NODEID = 3)) AND " +
                " FULLTEXT LIKE 'Astrologer' " +
                " order by DOCUMENTID desc "
        ));

        /* removed - never returned from medusa ?
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE (NODEID= 421450 or NODEID= 421451 and FULLTEXT like 'johnny') " +
                " or FULLTEXT LIKE 'amyotrophic' "
        ));
*/
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE ( NODEID = '421450' ) "
        ));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE ( NODEID = '421450' or CORPUSID = 421451 ) "
        ));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <NODE> WHERE NODETITLE LIKE '(Anthrax)'"
        )) ;

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE LIKE  '(ANTHRAX)'"));


/*  too slow  to run
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = '1231233' and NODEID = '1231234' and score1 > 0 "));
*/
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "corpus and" ,
        "SELECT <DOCUMENT> WHERE CORPUSID = 1 AND CORPUSID = 100040"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'merck' AND OUTDATED = 0 ORDER BY IDRACNUMBER ASC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT>  WHERE  NODEID = 215489 AND SCORE1 > 0"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODEID='215489' AND NODEID='210009')"));
/*  FIX FULLTEXT NEAR
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "select <DOCUMENT> WHERE FULLTEXT NEAR '((monday, tuesday, wednesday ) ,4 ,true ) AND (thursday or friday)'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT NEAR    '(monday, tuesday, wednesday), 20, TRUE'"));
*/
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE DATELASTFOUND > SYSDATE - 1"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "select <DOCUMENT> WHERE NODEID under 100000"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'testing'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT NEAR '((monday, tuesday, wednesday ) ,4 ,true ) AND (thursday or friday)' or nodeid under 15052"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID under 32046"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'SYN(dog INDRAHK)' SECURITY:\"CN=Prototype UserCN=UsersDC=indraweb\""));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'SYN(dog INDRAHK)' SECURITY:\"CN=Prototype UserCN=UsersDC=indraweb\""));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 SECURITY:\"CN=Prototype UserCN=UsersDC=indraweb\""));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 SECURITY:\"CN=Prototype UserCN=UsersDC=indraweb\""));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE NODETITLE = 'mErck'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE like 'mErck%%'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE = 'merck'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODETITLE = 'Aids' and NODETITLE NOT LIKE 'WARNING LETTER') AND OUTDATED=0 AND VISIBLE = 1 ORDER BY DOCTITLE DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE = 'text1' and NODETITLE NOT LIKE 'text2' and NODETITLE NOT LIKE 'text3'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODETITLE = 'text1' and NODETITLE NOT LIKE 'text2') or (NODETITLE LIKE 'text3' and NODETITLE NOT LIKE 'text4')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE like 'text1' and NODETITLE <> 'text2' and NODETITLE not LIKE 'text3' and NODETITLE != 'text4'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODETITLE like 'text1' and NODETITLE NOT = 'text2' ) and (NODETITLE not LIKE 'text3' and NODETITLE NOT LIKE 'text4')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE like 'texta1' and NODETITLE not like 'texta2' and NODETITLE not LIKE 'texta3' and NODETITLE like 'texta4'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODETITLE like 'texta1' and NODETITLE not like 'texta2') "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE ( NODETITLE not like 'texta1' and NODETITLE not like 'texta2' )"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE ( NODETITLE like 'texta1' and NODETITLE not like 'texta2' )"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE like 'aids' OR (NODETITLE like 'warning letter' OR NODETITLE like 'Acquired immunedeficiency syndrome')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE like 'aids' and (NODETITLE not like 'warning letter' AND NODETITLE not like 'Acquired immunedeficiency syndrome')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE like '@@text1' and (NODETITLE not like '@@text2' AND NODETITLE not like '@@text3')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE = '@@text1' and NODETITLE <> '@@text2' "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE = '@@text1' or NODETITLE <> '@@text2' "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE <> '@@text1' or NODETITLE <> '@@text2' "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE <> '@@text1' and NODETITLE = '@@text2' "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE <> '@@text1' and NODETITLE = '@@text2' and NODETITLE = '@@text3' "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE <> '@@text1' and NODETITLE = '@@text2' and NODETITLE <> '@@text3' "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE <> 'text1' and NODETITLE <> 'text2' and NODETITLE = 'text3'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE <> 'text1' and NODETITLE <> 'text2' and NODETITLE <> 'text3'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE = 'text1' and (NODETITLE <> 'text2' and NODETITLE <> 'text3')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE ( NODETITLE = '@@text1' and (NODETITLE <> '@@text2') ) or ( NODETITLE = '@@text3' and (NODETITLE <> '@@text4') )"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE ( NODETITLE = '@@text1' and (NODETITLE <> '@@text2') ) or ( NODETITLE = '@@text3' and (NODETITLE <> '@@text4') )"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 AND SCORE1 > 75 ORDER BY SCORE1 DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID UNDER 16717 AND SCORE1 > 75 ORDER BY SCORE1 DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID UNDER 9948 AND SCORE1 > 50 ORDER BY SCORE1 DESC" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 1091364 AND SCORE1 >= 25 ORDER BY SCORE1 DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 ORDER BY DOCUMENTID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 ORDER BY DOCUMENTID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODEID = 16717 and NODEID = 16718) ORDER BY DOCUMENTID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 OR NODEID = 16717 AND NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 OR (NODEID = 16717 AND NODEID = 16717 )"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 OR NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 OR NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID = 16717 OR NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 AND NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 AND DOCTITLE LIKE 'testing'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE DOCTITLE LIKE 'testing1' AND DOCTITLE LIKE 'testing2'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE DOCTITLE LIKE 'testing1' OR DOCSUMMARY LIKE 'testing2'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE DOCTITLE LIKE 'testing1' OR DOCTITLE LIKE 'testing2'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE DOCTITLE LIKE 'testing1' OR DOCTITLE LIKE 'testing2'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 11319680 ORDER BY SCORE1 DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 and GENREID = 11 ORDER BY SCORE1 desc"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE (NODETITLE LIKE '(anthrax)') ORDER BY CORPUSID desc"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (FULLTEXT LIKE '(anthrax)') "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (DOCTITLE LIKE '(anthrax)') "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "node security ?" ,
                "SELECT <NODE> WHERE PARENTID = 1340257" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 and GENREID = 11 ORDER BY SCORE1" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717 and GENREID = 11 AND VISIBLE = 1 ORDER BY SCORE1"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 and NODEID = 16717 and SCORE1 > 50 ORDER BY SCORE1 DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE (NODETITLE LIKE '(anthrax)' OR CORPUSID = 5) OR CORPUSID = 6"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODETITLE LIKE '(anthrax)' OR CORPUSID = 5) OR CORPUSID = 6"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODETITLE LIKE '(anthrax)' OR CORPUSID = 5) OR CORPUSID = 6 order by FTSCORE DESC "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE (NODETITLE LIKE '(anthrax)' OR CORPUSID = 5) OR CORPUSID = 5"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID UNDER 16717 AND NODETITLE LIKE '(texting)'" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'LOS' AND NODEID UNDER 16717 AND NODEID UNDER 16717" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID UNDER 16717 AND NODEID UNDER 16718"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 AND NODEID = 16718"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (DOCTITLE LIKE '(anthrax)')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID UNDER 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 AND NODEID = 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID = 16717 AND NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID = 16717 OR NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID UNDER 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 16717 OR NODEID = 16717 "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID UNDER 16717 ORDER BY NODEID DESC" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE FULLTEXT LIKE 'testing' and NODEID UNDER 16717 ORDER BY NODEID ASC, NODEID DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODEID UNDER 16717 AND NODEID UNDER 16717 ) AND FULLTEXT LIKE 'uranium' OR FULLTEXT LIKE 'enriched'" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID UNDER 16717 AND NODEID UNDER 16717 AND FULLTEXT LIKE 'uranium' OR FULLTEXT LIKE 'enriched'" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID UNDER 16717" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODEID UNDER 16717 AND NODEID UNDER 16717 ) OR FULLTEXT LIKE 'uranium' OR FULLTEXT LIKE 'enriched'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID UNDER 16717 AND NODEID UNDER 16717" ));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID UNDER 16717 AND NODEID UNDER 16717 OR FULLTEXT LIKE 'uranium' OR FULLTEXT LIKE 'enriched'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID UNDER 16717 AND NODEID UNDER 16718"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (NODEID UNDER 16717 AND NODEID UNDER 16717 ) OR FULLTEXT LIKE 'uranium' OR FULLTEXT LIKE 'enriched'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID UNDER 16717 AND NODEID UNDER 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID UNDER 16717 AND NODEID UNDER 16717"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID = 16717 ORDER BY NODEID, NODETITLE"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODE> WHERE NODEID = 16717 ORDER BY NODEID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE LIKE 'Supercritical' AND FULLTEXT LIKE 'Supercritical'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE ( DOCTITLE LIKE 'pharmacovigilance')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 207970 ORDER BY SCORE1"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'Medicine' ORDER BY DATELASTFOUND"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE DOCTITLE LIKE 'Medical'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE FULLTEXT LIKE 'heart attack'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (FULLTEXT LIKE '(lung)') AND (DOCTITLE LIKE '(cancer)')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (FULLTEXT LIKE '(testing)')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE DOCTITLE LIKE 'Merck' AND FULLTEXT LIKE 'medicine'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODEID = 1087353"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (FULLTEXT LIKE '(anthrax)')"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 1104112 AND SCORE1 >= 50 ORDER BY SCORE1 DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "",
            "SELECT <DOCUMENT> WHERE NODEID = 421448 AND (FULLTEXT LIKE '(Osteroporosis)' or FULLTEXT LIKE '(Osteroporosis2)')  " +
            " COMBINE FULLTEXT LIKE '(Osteoporosis ACCUM bone loss)' and " +
            " (docurl not like '%www.idrac%') "));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
            "SELECT <DOCUMENT> WHERE NODETITLE LIKE 'Osteoporosis' "));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE (FULLTEXT LIKE 'heart') AND (DOCTITLE LIKE 'meeting')"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                " COMBINE FULLTEXT LIKE '(Osteoporosis bone loss)' and (docurl not like 'http:www.idrac%' " +
        " or DATELASTFOUND = SYSDATE or repositoryID = 8 )"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE LIKE 'Osteoporosis' AND FULLTEXT LIKE 'bones and procedure and health and twist' COMBINE FULLTEXT LIKE '(Osteoporosis bone loss)' and (docurl not like 'http:www.idrac%' or DATELASTFOUND = SYSDATE) SCOREMIN(TopicMin=.1,FTMin=7)"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE LIKE 'Osteoporosis' AND FULLTEXT LIKE 'bones and procedure and health and twist' COMBINE FULLTEXT LIKE '(Osteoporosis bone loss)' and (docurl not like 'http:www.idrac%' or DATELASTFOUND = SYSDATE) SCOREMIN(TopicMin=.1,FTMin=7)"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "good one from web advanced gui query painter" ,
            "SELECT <DOCUMENT>  WHERE (FULLTEXT LIKE '(fulltextand)') AND (DOCTITLE LIKE '(doctitle)') AND ((NODETITLE = 'topictitle'))AND CORPUSID = '5' COMBINE FULLTEXT LIKE '(fulltextor)'"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE NODETITLE LIKE 'Osteoporosis' " +
                " AND FULLTEXT LIKE 'bones and procedure and health' COMBINE FULLTEXT LIKE " +
                " '(Osteoporosis bone loss)' and (docurl not like 'http:www.idrac%' or DATELASTFOUND = SYSDATE) " +
        " SCOREMIN(TopicMin=.1,FTMin=7)"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "",
            "SELECT <DOCUMENT> WHERE NODEID = 421448 AND (FULLTEXT LIKE '(Osteroporosis)' or FULLTEXT LIKE '(Osteroporosis2)')  " +
            " COMBINE FULLTEXT LIKE '(Osteoporosis ACCUM bone loss)' and " +
            " (docurl not like '%www.idrac%') " +
            " SCOREMIN(TopicMin=.1,FTMin=7)"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE DOCTITLE Like 'TOPICAND1 gulf war' AND FULLTEXT Like 'TOPICAND2 LIKE saddam' " +
                " COMBINE FULLTEXT LIKE '(FTCOMBINED supercritical AND mike AND joe and cap)' "     +
        " SCoREMIN (topicMin=.2,fTmin=1)"));

        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
                "SELECT <DOCUMENT> WHERE (NODETITLE LIKE 'Osteoporosis' OR " +
                " (NODEID = 2 AND NODEID = 3)) AND FULLTEXT LIKE 'bones' " +
                " COMBINE FULLTEXT LIKE '(Osteoporosis accru bone loss)' " +
        " SCOREMIN(TopicMin=.1,FTMin=7)"));


        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT>   WHERE  NODEID = 1369686  AND SCORE1 > 25 ORDER BY SCORE1 DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE NODEID = 1340595 AND CS_PUBLISHERTEXT LIKE 'test'"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT \t <DOCUMENT> WHERE NODEID = 1340595 ORDER BY NODEID, \t    FTSCORE  , \t DOCUMENTID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT \t <NODEDOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT \t <DOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT \t <NODE> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, NODEID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT \t <DOCUMENT> WHERE NODETITLE LIKE 'MOSQUITO' AND DOCTITLE LIKE 'E6' ORDER BY FTSCORE, DOCUMENTID"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE (NODEID= 421450 and NODEID= 421451 and FULLTEXT like 'johnny')  and FULLTEXT LIKE 'amyotrophic' "));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE SIGNATUREWORD LIKE 'hormone' AND CORPUSID = 12"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE SIGNATUREWORD LIKE 'hormone' AND REPOSITORYID = 12"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEdocument> WHERE NODETITLE LIKE '%%shitake%%' ORDER BY NODETITLE ASC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE NODETITLE LIKE '(Osteoporosis)' and score5 * score6 > .01 COMBINE FULLTEXT LIKE '(Osteoporosis bone loss)' and (docurl not like '%http:www.idrac%' or DATELASTFOUND = SYSDATE) SCOREMIN(TopicMin=.1,FTMin=7)"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE (NODETITLE LIKE 'nitration') order by nodetitle ASC, docsize, FTSCORE DESC"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <DOCUMENT> WHERE DOCTITLE LIKE 'nitration' order by FTSCORE"));
        if (( iDB & ( iDB_client | iDB_perseus | iDB_medusa) ) > 0) vTC.addElement (new TestCell (i++ , "" ,
        "SELECT <NODEDOCUMENT> WHERE (NODETITLE LIKE 'forging') and ((NODETITLE LIKE 'nitration') or (NODETITLE LIKE 'filtration')) order by nodetitle ASC, docsize DESC, FTSCORE DESC"));

//                             +
//                            "" +
//                            "" +
//                            " " +
//                            "   " +
//                            "   " +
//                            "  " +
//                            " " +

        return vTC;
    }

}

