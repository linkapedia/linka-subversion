/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Mar 12, 2003
 * Time: 8:06:02 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

public class SyntaxCheck
{
    public static int parenMatch ( String s ) throws Exception
    {
        char[] cArr = s.toCharArray();
        int iParenDepth = 0;

        for ( int i = 0; i < cArr.length; i++ )
        {
            if ( cArr[i] == '(' )
                iParenDepth++;
            else if ( cArr[i] == ')' )
                iParenDepth--;

            if ( iParenDepth < 0 )
                throw new Exception ("mismatched parentheses, missing open paren");
        }

        if ( iParenDepth > 0 )
            throw new Exception ("mismatched parentheses, missing close paren");
        return 0; // success
    }
}

