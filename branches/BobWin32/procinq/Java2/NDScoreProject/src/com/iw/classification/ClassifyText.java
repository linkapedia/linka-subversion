package com.iw.classification;

import api.util.affinity.AffinityPass;
import com.indraweb.encyclopedia.DocForScore;
import com.indraweb.execution.Session;
import com.indraweb.ir.clsStemAndStopList;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.util.IntContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.iw.scoring.NodeForScore;
import com.iw.scoring.scoring2.ScoresBetweenTwoTextObjects;
import java.sql.Connection;
import java.util.*;
import org.apache.log4j.Logger;

public class ClassifyText {

    private static final int iSPECIALGUIEXPLAINMODE = -2;
    private static final Logger log = Logger.getLogger(ClassifyText.class);

    /**
     *
     * @param dfs
     * @param dbc
     * @param icfg_classification_DocCountMin
     * @param out
     * @param bStemOn
     * @param iNodeIDToExplain
     * @param htCorporaToConsider
     * @param bWantPhrasing
     * @param intContainer_NumNDSDone_OutParmAccounting
     * @param intContainer_NumNodes_outParmAccounting
     * @param htNodeSet
     * @param bQuietXMLModeRefreshMultiCall
     * @param bProp_purgeCache
     * @param nfsDynamicClassify
     * @param sigparms
     * <p/>
     * @return
     * <p/>
     * @throws Exception
     */
    public static Vector classifyDoc(DocForScore dfs,
            Connection dbc,
            int icfg_classification_DocCountMin,
            java.io.PrintWriter out,
            boolean bStemOn,
            int iNodeIDToExplain,
            Hashtable htCorporaToConsider,
            boolean bWantPhrasing,
            IntContainer intContainer_NumNDSDone_OutParmAccounting,
            IntContainer intContainer_NumNodes_outParmAccounting,
            Hashtable htNodeSet,
            boolean bQuietXMLModeRefreshMultiCall,
            boolean bProp_purgeCache,
            NodeForScore nfsDynamicClassify,
            SignatureParms sigparms)
            throws Exception {
        ScoresBetweenTwoTextObjects[] arrScoresBetweenTwoTextObjects = null;
        ExplainDisplayDataInstanceTABLE eddiTABLE_ROC = null;
        long lClassifyDocStart = System.currentTimeMillis();
        try {

            // *********************************************************************
            // find the set of nodes that have one word of more from the doc as candidates for scoring
            // *********************************************************************
            if (Session.stopList == null) {
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
            }

            HashSet hsNodeIDsOrNodeForScoresThatContainWord_outparm = new HashSet();
            // ******************************************************
            // GET NODES WITH WORDS FROM THIS DOC
            // ******************************************************
            // initializes thesaurus expanded text blocks and this vector is used in affinity if no node set passed in
            Vector<String> vAllDocWordsTitleFirst_minCount = dfs.getEnumDocWordsWithTitleWordsFirst_populateThesExpansionTermsPerCorpusOrCorpusSet(icfg_classification_DocCountMin, bStemOn, true, htCorporaToConsider, dbc);

            // either user can pass in the set of nodes to score against
            if (htNodeSet != null) {
                Enumeration enumIntNodeSet = htNodeSet.elements();
                Data_WordsToNodes.DoRefreshIfNeeded(htCorporaToConsider, false, dbc, sigparms);

                try {
                    while (enumIntNodeSet.hasMoreElements()) {
                        hsNodeIDsOrNodeForScoresThatContainWord_outparm.add(
                                Data_NodeIDsTo_NodeForScore.getNodeForScore((Integer) enumIntNodeSet.nextElement(), true));
                    }
                } finally {
                    Data_WordsToNodes.rwlockcache.done("bottom accumulateNFS_ContainingWords if (htNodeSet != null)  mode");
                }

                intContainer_NumNodes_outParmAccounting.setiVal(htNodeSet.size());
            } else if (iNodeIDToExplain > -1) {
                Vector vIntCorporaToConsider = new Vector();
                Enumeration eCorporaToConsider = htCorporaToConsider.keys();
                while (eCorporaToConsider.hasMoreElements()) {
                    vIntCorporaToConsider.addElement(eCorporaToConsider.nextElement());
                }

                Data_WordsToNodes.DoRefreshIfNeeded(htCorporaToConsider, false, dbc, sigparms);

                NodeForScore nfs = null;
                try {
                    nfs = Data_NodeIDsTo_NodeForScore.getNodeForScore(new Integer(iNodeIDToExplain), true);
                } finally {
                    Data_WordsToNodes.rwlockcache.done("bottom accumulateNFS_ContainingWords else if (iNodeIDToExplain > -1 )mode");
                }

                hsNodeIDsOrNodeForScoresThatContainWord_outparm.add(nfs);
                intContainer_NumNodes_outParmAccounting.setiVal(hsNodeIDsOrNodeForScoresThatContainWord_outparm.size());
            } else if (nfsDynamicClassify == null) {
                // hbk control classify - true for production - which nodes to score against
                com.indraweb.profiler.Timer timer = com.indraweb.profiler.Profiler.getTimer("get nodes with words", true);
                // hbk 2004 03 25 vAllDocWordsTitleFirst_minCount = thesaurusExpand (vAllDocWordsTitleFirst_minCount, ;
                Enumeration enumAllDocWordsTitleFirst_minCount = vAllDocWordsTitleFirst_minCount.elements();

                try {
                    Data_WordsToNodes.accumulateNFS_ContainingWords(
                            enumAllDocWordsTitleFirst_minCount,
                            hsNodeIDsOrNodeForScoresThatContainWord_outparm,
                            htCorporaToConsider,
                            intContainer_NumNodes_outParmAccounting,
                            dbc,
                            true,
                            bStemOn,
                            bProp_purgeCache,
                            sigparms);
                } finally {
                    Data_WordsToNodes.rwlockcache.done("bottom accumulateNFS_ContainingWords else if (true) mode");
                }
                timer.stop();

            } else if (nfsDynamicClassify != null) {
                hsNodeIDsOrNodeForScoresThatContainWord_outparm.add(nfsDynamicClassify);

            } else // debug mode - pick a node to score
            {
                throw new Exception("invalid mode in ClassifyText");
            }

            //Filter nodes New Affinity pass NODE**********************************
            boolean bNewAfiinityPass = false;
            long lAffinityPassByNodeStart = System.currentTimeMillis();
            AffinityPass affinityPass = new AffinityPass(dbc);
            Iterator iterFilter = hsNodeIDsOrNodeForScoresThatContainWord_outparm.iterator();
            //com.indraweb.profiler.Profiler.addStatisticCount("Num ScoreCollects", hsNodeIDsOrNodeForScoresThatContainWord_outparm.size());
            log.info("Filtering by node");
            NodeForScore nfs = null;
            while (iterFilter.hasNext()) {
                nfs = (NodeForScore) iterFilter.next();
                log.debug("Using document: " + nfs.getTitle(bStemOn));
                bNewAfiinityPass = affinityPass.checkAffinityTermsNode(vAllDocWordsTitleFirst_minCount, nfs);
                if (!bNewAfiinityPass) {
                    log.debug("No terms found that match the affinity terms from the node.");
                    iterFilter.remove();
                }
            }
            if (!bNewAfiinityPass) {
                log.warn("This document won't be classified as it failed the affinity pass.");
            }
            long lAffinityPassByNodeEnd = System.currentTimeMillis();

            log.info("Affinity pass by node finished in: " + (lAffinityPassByNodeEnd - lAffinityPassByNodeStart) + "ms");

            arrScoresBetweenTwoTextObjects = new ScoresBetweenTwoTextObjects[hsNodeIDsOrNodeForScoresThatContainWord_outparm.size()];
            intContainer_NumNDSDone_OutParmAccounting.setiVal(hsNodeIDsOrNodeForScoresThatContainWord_outparm.size());
            int loop = 0;
            int iNumDocNodesScoredGened = 0;
            // ***************************************************************
            // FOR ALL NODES FOUND FOR ALL WORDS FOR THIS DOC - FILL SCORES AND GIST
            // ***************************************************************
            com.indraweb.profiler.Timer timer25 = com.indraweb.profiler.Profiler.getTimer("while outside scoreCollect", true);
            //Data_NodeIDsTo_NodeForScore dataNodeToNFS = Data_NodeIDsTo_NodeForScore.getInstance();
            Iterator iterNFS_toBeScored = hsNodeIDsOrNodeForScoresThatContainWord_outparm.iterator();
            com.indraweb.profiler.Profiler.addStatisticCount("Num ScoreCollects", hsNodeIDsOrNodeForScoresThatContainWord_outparm.size());
            while (iterNFS_toBeScored.hasNext()) {
                nfs = (NodeForScore) iterNFS_toBeScored.next();

                arrScoresBetweenTwoTextObjects[loop] = new ScoresBetweenTwoTextObjects(nfs, dfs, nfs.getNodeID() == iNodeIDToExplain || iNodeIDToExplain == iSPECIALGUIEXPLAINMODE);
                if (nfs.getNodeSize() <= 0) {
                    log.error("nfs.getNodeSize() <= 0 on node [" + nfs.getNodeID() + "]\r\n");
                }

                //this is the call to another project which will call out to exetrnal classifiers
                // scores are collected inside each arrScoresBetweenTwoTextObjects [ loop ] object
                boolean bExplain = iNodeIDToExplain == nfs.getNodeID() || iNodeIDToExplain == iSPECIALGUIEXPLAINMODE;
                eddiTABLE_ROC = com.iw.scoring.scoring3.InvokeExternalClassify.scoreCollect(
                        arrScoresBetweenTwoTextObjects[loop],
                        dbc,
                        bExplain,
                        out,
                        bStemOn,
                        true);
                out.flush();
                iNumDocNodesScoredGened++;
                loop++;
            } // while ( enumNodeIDs.hasMoreElements() )
            timer25.stop();

            if (out != null && !bQuietXMLModeRefreshMultiCall) {
                out.println(
                        "<PROFILE> classifyDoc "
                        + " numNodes [" + Data_NodeIDsTo_NodeForScore.getNumNodes()
                        + "]</PROFILE>");
            }

            // sort classified results by agg score
        } catch (Throwable t) {
            throw new Exception("throwable in classifyDoc() [" + t.getMessage() + "]");
        }

        Vector v = new Vector();
        v.addElement(arrScoresBetweenTwoTextObjects);
        v.addElement(eddiTABLE_ROC);

        long lClassifyDocEnd = System.currentTimeMillis();
        log.info("ClassifyDoc function finished in: " + (lClassifyDocEnd - lClassifyDocStart) + "ms");
        return v;
    } // public static ScoresBetweenTwoTextObjects[] classifyDoc ( NodeForScore dfsm1_,
}
