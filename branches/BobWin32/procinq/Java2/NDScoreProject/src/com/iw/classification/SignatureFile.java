/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Feb 25, 2004
 * Time: 5:17:50 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.iw.classification;


import com.indraweb.database.DBAccess_Central_connection;
import com.indraweb.execution.Session;
import com.indraweb.ir.Thesaurus;
import com.indraweb.util.*;
import com.indraweb.signatures.WordFrequencies;
import com.iw.scoring.NodeForScore;

import java.io.*;
import java.sql.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;


public class SignatureFile
{
    public static final String CLOSESTRING = "\tEOF";


    public static boolean bSigFileExists(int iCorpusID, boolean bStem)
    {
        String sfnameSigWordCountsFile = getFileName(iCorpusID, bStem);
        return UtilFile.bFileExists(sfnameSigWordCountsFile);

    }

    // artiificially create new corpus files for volume tseting
    // ---------------------------------------------------------------------------
    public static void corpusFileMangler (String sFNamein ,
                                          String sFileNameOut ,
                                          int iShiftAmt) throws Exception
    {
        // ---------------------------------------------------------------------------


        // open source file for read
        // open temp file for write
        // loop thru source file
        // if line != line then
        //   write to temp file

        String[] sArrShifts = {"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w"};
        if (iShiftAmt > sArrShifts.length)
            throw new Exception ("iShiftAmt > sArrShifts.length");
        // create replacement for vowels chars
        String[] sArrReplacements = new String[5];
        for ( int i1 = 0 ; i1 < sArrReplacements.length ; i1++ )
            sArrReplacements[i1] = sArrShifts[( iShiftAmt + i1 ) % sArrShifts.length];

        System.out.println ("replacing a with [" + sArrReplacements[0] + "]");
        System.out.println ("replacing e with [" + sArrReplacements[1] + "]");
        System.out.println ("replacing i with [" + sArrReplacements[2] + "]");
        System.out.println ("replacing o with [" + sArrReplacements[3] + "]");
        System.out.println ("replacing u with [" + sArrReplacements[4] + "]");

        try
        {
            String textline;
            BufferedReader in = new BufferedReader (new FileReader (sFNamein));
            PrintWriter outFile = new PrintWriter (new BufferedWriter (new FileWriter (sFileNameOut)));

            while (in.ready ())
            {
                textline = in.readLine ();
                if (!textline.startsWith ("TITLE:<-1>"))
                {

                    if (textline.startsWith ("TITLE:"))
                    {
                        String sPreString = UtilStrings.getsUpToNthOfThis_1based (textline , "[" , 1);
                        String sNode = UtilStrings.getStringBetweenThisAndThat (textline , "[" , "]");
                        sNode = ( iShiftAmt + 1 ) + sNode;
                        String sPostForReplacement = UtilStrings.getStrAfterThisToEnd1Based (textline , "]" , 1);
                        sPostForReplacement = sPostForReplacement.replaceAll ("a" , sArrReplacements[0]);
                        sPostForReplacement = sPostForReplacement.replaceAll ("e" , sArrReplacements[1]);
                        sPostForReplacement = sPostForReplacement.replaceAll ("i" , sArrReplacements[2]);
                        sPostForReplacement = sPostForReplacement.replaceAll ("o" , sArrReplacements[3]);
                        sPostForReplacement = sPostForReplacement.replaceAll ("u" , sArrReplacements[4]);

                        outFile.write (sPreString + "[" + sNode + "]" + sPostForReplacement + "\r\n");
                    }
                    else
                    {
                        textline = textline.replaceAll ("a" , sArrReplacements[0]);
                        textline = textline.replaceAll ("e" , sArrReplacements[1]);
                        textline = textline.replaceAll ("i" , sArrReplacements[2]);
                        textline = textline.replaceAll ("o" , sArrReplacements[3]);
                        textline = textline.replaceAll ("u" , sArrReplacements[4]);
                        outFile.write (( iShiftAmt + 1 ) + textline + "\r\n");
                    }
                }
                else
                {
                    outFile.write (textline + "\r\n");
                }
            }
            outFile.close ();
            in.close ();

        } catch ( Exception e )
        {
            api.Log.LogError ("error 123123 " + sFNamein , e);
        }
    }


    public static String getFileName ( int iCorpusID, boolean bStem )
    {
        String sStem = "_nonStem";
        if (bStem)
            sStem = "_stem";

        return Session.cfg.getProp ("IndraHome") + "/CorpusSource/" +
                "SigWordCountsFile_" + iCorpusID + sStem + ".txt";

    }
}
