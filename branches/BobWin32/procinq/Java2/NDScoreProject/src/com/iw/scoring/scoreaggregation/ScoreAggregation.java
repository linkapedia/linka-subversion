package com.iw.scoring.scoreaggregation;

import com.indraweb.util.Log;

public class ScoreAggregation {
    static final int SM_0_nodeid = 0;
    static final int SM_INTRINSIC_1000_numTitleWordsInCorpusDoc = 1000;
    static final int SM_INTRINSIC_1004_numWordsInCorpusDoc = 1004;
    static final int SM_INTRINSIC_1008_numWordsInCorpusSig = 1008;
    static final int SM_INTRINSIC_1012_numTitleWordsInWebDoc = 1012;
    static final int SM_INTRINSIC_1016_numWordsInWebDoc = 1016;
    static final int SM_INTRINSIC_1020_numInLinksToWebDoc_notImplemented = 1020;

    public static final int SM_level1_1_SIGWORD_FREQ_DOT_PRODUCT = 1;
    public static final int SM_level1_3_SIGWORD_COVERAGE = 3;
    // 10, 22, 26, 30 - multiply by log title length to increase when long, and

    static final int SM_level1_6_TITLE_TO_TITLE_COVER_MIN = 6;
    static final int SM_level1_10_TITLE_TO_TITLE_PCT_COVER = 10;
    // adjust this - 8 minus value
    public static final int SM_level1_14_TITLE_TO_TITLE_MATCH = 14;
    static final int SM_level1_18_TITLE_MIN_COVER_BY_TEXT = 18;
    public static final int SM_level1_22_TITLE_TO_TEXT_PCT_COVER = 22;
    static final int SM_level1_26_TITLE_IN_TEXT_COUNT = 26;
    static final int SM_level1_30_TITLE_IN_TEXT_COUNT_COMMAREV = 30;
    static final int SM_level1_34_NUM_SE_RETURNING_THIS = 34;
    static final int SM_level1_50_SE_FINDING_DOC = 50;
    static final int SM_level1_52_INDEX_IN_SE_FINDING_DOC = 52;

    static final int SM_AGG_2000_Score1TimesScore3times10 = 2000;
    static final int SM_AGG_2001_NumSigWordscovered = 2001;
    public static final int SM_AGG_3000_CutOffs_1_2_3 = 3000;

    public static final int SM_4_Score1TimesCorpusDocWords_Div132 = 4; // dynamic
    public static final int SM_7_FtimesC = 7; // dynamic
    public static final int SM_AGG_2002_Score1TimesScore4times10 = 2002; // to DB
    public static final int SM_AGG_3001_Like3000ButUsingScore4ForFreqDot = 3001; // to DB

    public static String scoreMethodShortName(int iSM) {
        String sName = null;
        switch (iSM) {
            case SM_0_nodeid:
                sName = "0_NodeID";
                break;
            case SM_INTRINSIC_1000_numTitleWordsInCorpusDoc:
                sName = "1000_numTitleWordsInCorpusDoc";
                break;
            case SM_INTRINSIC_1004_numWordsInCorpusDoc:
                sName = "1004_numWordsInCorpusDoc";
                break;
            case SM_INTRINSIC_1008_numWordsInCorpusSig:
                sName = "1008_numWordsInCorpusSig";
                break;
            case SM_INTRINSIC_1012_numTitleWordsInWebDoc:
                sName = "1012_numTitleWordsInWebDoc";
                break;
            case SM_INTRINSIC_1016_numWordsInWebDoc:
                sName = "1016_numWordsInWebDoc";
                break;
            case SM_INTRINSIC_1020_numInLinksToWebDoc_notImplemented:
                sName = "1020_numInLinksToWebDoc_notImplemented";
                break;
            case SM_level1_1_SIGWORD_FREQ_DOT_PRODUCT:
                sName = "1_SIGWORD_FREQ_DOT_PRODUCT";
                break;
            case SM_level1_3_SIGWORD_COVERAGE:
                sName = "3_SIGWORD_COVERAGE";
                break;
            case SM_4_Score1TimesCorpusDocWords_Div132:
                sName = "4_Score1TimesCorpusDocWords_Div132";
                break;
            case SM_level1_6_TITLE_TO_TITLE_COVER_MIN:
                sName = "6 Topic title-to-title coverage MIN";
                break;
                // 10, 22, 26, 30 - multiply by log title length to increase when long, and
            case SM_level1_10_TITLE_TO_TITLE_PCT_COVER:
                sName = "10_TITLE_TO_TITLE_PCT_COVER";
                break; // ignore ?
                // adjust this - 8 minus value
            case SM_level1_14_TITLE_TO_TITLE_MATCH:
                sName = "14_TITLE_TO_TITLE_MATCH";
                break;
            case SM_level1_18_TITLE_MIN_COVER_BY_TEXT:
                sName = "18_TITLE_MIN_COVER_BY_TEXT";
                break;
            case SM_level1_22_TITLE_TO_TEXT_PCT_COVER:
                sName = "22_TITLE_TO_TEXT_PCT_COVER";
                break; // ignore ?
            case SM_level1_26_TITLE_IN_TEXT_COUNT:
                sName = "26_TITLE_IN_TEXT_COUNT";
                break;
            case SM_level1_30_TITLE_IN_TEXT_COUNT_COMMAREV:
                sName = "30_TITLE_IN_TEXT_COUNT_COMMAREV";
                break;
            case SM_level1_34_NUM_SE_RETURNING_THIS:
                sName = "34_NUM_SE_RETURNING_THIS";
                break;
            case SM_level1_50_SE_FINDING_DOC:
                sName = "50_SE_FINDING_DOC";
                break;
            case SM_level1_52_INDEX_IN_SE_FINDING_DOC:
                sName = "52_INDEX_IN_SE_FINDING_DOC";
                break;
            case SM_AGG_2000_Score1TimesScore3times10:
                sName = "2000_Score1TimesScore3times10";
                break;
            case SM_AGG_2001_NumSigWordscovered:
                sName = "2001_NumSigWordscovered";
                break;
            case SM_AGG_2002_Score1TimesScore4times10:
                sName = "AGG_2002_Score1TimesScore4times10";
                break;
            case SM_AGG_3000_CutOffs_1_2_3:
                sName = "3000_CutOffs_1_2_3_4_5_6";
                break;
            case SM_AGG_3001_Like3000ButUsingScore4ForFreqDot:
                sName = "SM_AGG_3001_Like3000ButUsingScore4ForFreqDot";
                break;
            case (0 - SM_AGG_3001_Like3000ButUsingScore4ForFreqDot):
                sName = "negative SM_AGG_3001_Like3000ButUsingScore4ForFreqDot";
                break;
            default:
                Log.FatalError(" SM not found : " + iSM);
                break;
        }  // switch
        return sName;
    }
}
