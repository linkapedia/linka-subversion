package com.iw.classification;

public class NodeCount {
    int iNodeID = -1;
    int iCount = -1;

    public static int iNumNCConstructors = 0;  // hbk control 2002 12 27

    public NodeCount(int iNodeID_, int iCount_) {
        iNodeID = iNodeID_;
        iCount = iCount_;
        iNumNCConstructors++;  // hbk control 2002 12 27
        if (iNumNCConstructors % 5000 == 0)
            System.out.println("NodeCount constructor #:" + iNumNCConstructors);
    }

}
