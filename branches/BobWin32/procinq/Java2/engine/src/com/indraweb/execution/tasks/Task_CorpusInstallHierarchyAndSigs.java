package com.indraweb.execution.tasks;

import com.indraweb.corpusinstall.CorpusFileDescriptor;
import com.indraweb.corpusinstall.CreateTopicVector;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.execution.Session;
import com.indraweb.html.UtilHTML;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.PhraseParms;
import com.indraweb.signatures.Signature;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.signatures.WordFrequencies;
import com.indraweb.util.Log;
import com.indraweb.util.UtilFileAndDirNames;
import com.indraweb.util.UtilRandom;
import com.indraweb.util.UtilStrings;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Task_CorpusInstallHierarchyAndSigs {

    static boolean removeThisDummyflag = false;

    // *******************************************************************
    public static void doIt(
            // *******************************************************************
            int iCorpusID,
            String sCorpusFolder,
            String sProp_CorpusName,
            java.io.PrintWriter out,
            java.sql.Connection dbc_,
            boolean bVerboseSigLogging,
            boolean bBuildHierarchyToNodeNodePathNodeData,
            boolean bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest,
            boolean bProp_doSigGen,
            PhraseParms phraseparms,
            ParseParms parseparms,
            SignatureParms sigparms,
            WordFrequencies wordfreq)
            throws Exception, Throwable {


        if (!bProp_doSigGen) {
            api.Log.Log("doSigGen false, skipping signature build");
        }

        // ****************************************************
        // get vector of all files and folders into vec with parents correct
        // ****************************************************
        // get corpus location
        // STEP 1 - build the node hierarchy into the DB
        try {
            // STEP 1 BUILD HIERARCHY - OBSOLETE
            if (bBuildHierarchyToNodeNodePathNodeData) {
                api.Log.Log("start hierarchy build step 1");
                int iNumNodesInsertedIntoDB = step1_buildHierarchy_NodeNodePathNodeData(
                        iCorpusID,
                        sCorpusFolder,
                        sProp_CorpusName,
                        out,
                        dbc_,
                        bVerboseSigLogging,
                        parseparms,
                        phraseparms,
                        wordfreq,
                        sigparms);
                api.Log.Log("done hierarchy build step 1 iNumNodesInsertedIntoDB [" + iNumNodesInsertedIntoDB + "]");
            }

            Vector vCFDNodesLeavesOrAll = null;
            // user cnf files thru nodepath table or use clob data thru nodedata tables as text sourcesa
/*
             * if ( bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest ) { api.Log.Log ("starting sig gen using old method 1");
             *
             * vCFDNodesLeavesOrAll = step2a_genSigsFromCNFFilesAsHistorically ( iCorpusID , dbc_ , sSigWords_NoInsertToDB_IfStartsWith , bVerboseSigLogging , iNumBytesCorpusDocsToTake ); api.Log.Log
             * ("done sig gen using old method 1"); } else
             */
            if (bProp_doSigGen) {
                // STEP 2 GET VEC RANDOM CFDs for slicing (just as a data fetch pre step 3
                //api.Log.Log ("starting collect vCFD random step 2");

                vCFDNodesLeavesOrAll = step2_getUsingDBSource_CDFVecAllAcrossRangesRandom(
                        iCorpusID,
                        dbc_,
                        parseparms,
                        phraseparms);
                //api.Log.Log ("done collect vCFD random step 2");

                // common clob vs file now

                // PRINT DEBUG           for ( int i = 0; i < vCFDNodesLeavesOrAll.size(); i++)
                //            {
                //                CorpusFileDescriptor cfd = (CorpusFileDescriptor) vCFDNodesLeavesOrAll.elementAt(i);
                //                api.Log.Log ("in Step 2 post build cfd [" + cfd.toString()+"]");
                //
                //            }


                // STEP 3 GET BREAK INTO RANGES AND ORTHOG
                //api.Log.Log ("start collect vCFD random step 3");
                Signature.step3_breakIntoSigRangesAndOrthog(
                        iCorpusID,
                        vCFDNodesLeavesOrAll,
                        dbc_,
                        bVerboseSigLogging,
                        false, // bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest
                        parseparms,
                        sigparms,
                        phraseparms,
                        wordfreq);
                //api.Log.Log ("done collect vCFD random step 3");
            } // if not skipping sig gen

            // print debug            for ( int i = 0; i < vCFDNodesLeavesOrAll.size(); i++)
            //            {
            //                CorpusFileDescriptor cfd = (CorpusFileDescriptor) vCFDNodesLeavesOrAll.elementAt(i);
            //                api.Log.Log (i + ". post orthog [" + cfd.toString() + "]" );
            //                api.Log.Log ("\r\n");
            //            }
            //            api.Log.Log ("\r\n");


        } catch (Exception e) {
            Log.NonFatalError("error in herirachy build or sig gen \r\n", e);
        }

    } // doIt

    // *******************************************************************
    private static int step1_buildHierarchy_NodeNodePathNodeData(int iCorpusID,
            // *******************************************************************
            String sCorpusFolder,
            String sProp_CorpusName,
            java.io.PrintWriter out,
            java.sql.Connection dbc_,
            boolean bVerboseSigLogging,
            ParseParms parseparms,
            PhraseParms phraseparms,
            WordFrequencies wordfreq,
            SignatureParms sigparms)
            throws Throwable {
        api.Log.Log("building hierarchy for corpus [" + iCorpusID + "] title [" + sProp_CorpusName + "]");
        // verify no existing nodes already corpus this ID - must start clean
        String sSQL = "select count (*) from node where corpusid = " + iCorpusID;
        int i2 = JDBCIndra_Connection.executeSQLInt(sSQL, dbc_);

        // Corpus installer must start on a fresh corpus
        if (i2 > 0) {
            throw new Exception("nodes preexisting for corpus [" + iCorpusID + "] in corpus installer.  Corpus installer must start on a fresh corpus.");
        }

        Vector vCorpusFileAndDirDescriptors = new Vector();

        boolean bIndraweb = Session.cfg.getPropBool("Indraweb", false, "false");

        long iNodeStartPoint = com.indraweb.utils.oracle.UniqueSpecification.getSubsequentNodeID(dbc_, bIndraweb);

        /*
         * if (lNumNodeSlotsLeft < 100000) { Log.FatalError("only 100,000 nodes left - enlarge node ranges "); }
         */

        // keep a buffer of 100 nodes between client corpora
        CreateTopicVector.step1_1_getVecOfCorpusFileDescriptors_recursive(
                vCorpusFileAndDirDescriptors,
                sCorpusFolder,
                (iNodeStartPoint), // node ID start point
                null,
                true, // root
                true,
                -1,
                dbc_,
                0,
                bVerboseSigLogging,
                phraseparms,
                parseparms,
                wordfreq,
                sigparms);
        api.Log.Log("Corpus file disk scan - num folders and files (should be #files*2= " + vCorpusFileAndDirDescriptors.size());
        //api.Log.Log ("set of folders and files [" + UtilSets.vToStr(vCorpusFileAndDirDescriptors)+ "]" );
        api.Log.Log("nodeID start : " + iNodeStartPoint);

        out.println("<NUMNODES>" + (vCorpusFileAndDirDescriptors.size() / 2) + "</NUMNODES>");
        // VECTOR HAS BOTH FOLDERS AND html file names now
        // ****************************************************
        // populate DB with files and nodes and filenodes and nodes and hierarchy
        // ****************************************************
        // Log.LogPopup ("adding DB hierarchy", false);

        // check or put all nodes in to the DB
        int iNumNodesInsertedIntoDB = 0;

        //if corpus name not specified take it from the title of the root CFD
        if (sProp_CorpusName == null) {
            CorpusFileDescriptor cfd = (CorpusFileDescriptor) vCorpusFileAndDirDescriptors.elementAt(0);
            String sURL = cfd.getURL() + "000.html";
            sURL = UtilStrings.replaceStrInStr(sURL, "file:///", "");
            sProp_CorpusName = UtilHTML.getTitleFastFromHTMLFile(sURL);
            String SQL = "update corpus set Corpus_Name = '" + sProp_CorpusName + "' where corpusid = " + iCorpusID;
            JDBCIndra_Connection.executeUpdateToDB(SQL, dbc_, true, 1);
        }
        // both folders and files here
        Log.logClear("vCorpusFileAndDirDescriptors.size() [" + vCorpusFileAndDirDescriptors.size() + "]\r\n");
        for (int i = 0; i < vCorpusFileAndDirDescriptors.size(); i++) {
            CorpusFileDescriptor cfd = (CorpusFileDescriptor) vCorpusFileAndDirDescriptors.elementAt(i);

            String sURLLookup = UtilFileAndDirNames.stripUpToAndIncl_IndraHome(cfd.getURL(), false);

            if (!sURLLookup.toLowerCase().endsWith(".html")) {
                Vector vTrackerOfDBUpdates = new Vector();
                // UPDATE DB WITH HIER, NODE, AND FILE INFO
                // VERIFY 000.html file present
                //Log.log ("\r\n\r\n" + i + ". hbk " + cfd.getURL() + "\r\n");
                String sFolderNAmeForRootFileVerify = null;
                if (!cfd.getURL().endsWith("/") && !cfd.getURL().endsWith("\\")) {
                    sFolderNAmeForRootFileVerify = cfd.getURL() + "/000.html";
                } else {
                    sFolderNAmeForRootFileVerify = cfd.getURL() + "000.html";
                }

                if (!com.indraweb.util.UtilFile.bFileExists(sFolderNAmeForRootFileVerify)) {
                    Log.logClear("Corpus Normal Form violated, continuing : file not found : " + sFolderNAmeForRootFileVerify);
                }


                // !!!!!! WRITE CFD updateDB_WithThis_CFD_NodeAndNodePathAndNodeData !!!!!!!!!!!!
                cfd.updateDB_WithThis_CFD_NodeAndNodePathAndNodeData(iCorpusID, vTrackerOfDBUpdates, dbc_, false, true, wordfreq, sigparms);
                iNumNodesInsertedIntoDB += vTrackerOfDBUpdates.size() / 2;

                // commit every 100 nodes
//                if (( iNumNodesInsertedIntoDB % 1 ) == 0)
//                {
                //api.Log.Log (iNumNodesInsertedIntoDB + ". hierarchy install node [" + cfd.lNodeID + "] url [" + cfd.getURL () + "]");
                dbc_.commit();
//                }
            }  // leave 000.htmls out
        }

        if (iNumNodesInsertedIntoDB > 0) {
            dbc_.commit();
        }

        //Log.logClear("completed hierarchy install - starting sigs now db  nodes size [" + vCorpusFileAndDirDescriptors.size() + "]\r\n" );

        // ****************************************************
        // populate DB with signatures
        // ****************************************************

        //Signature.printCDFTree (( CorpusFileDescriptor )vCorpusFileAndDirDescriptors.elementAt(0), 0);

        return iNumNodesInsertedIntoDB;
    } // private static void step1_buildHierarchy_NodeNodePathNodeData( int iCorpusID,

    private static Vector step2_getUsingDBSource_CDFVecAllAcrossRangesRandom(int iCorpusID,
            java.sql.Connection dbc_,
            ParseParms parseparms,
            PhraseParms phraseparms)
            // *******************************************************************
            throws Throwable {


        // ****************************************************
        // RANDOMIZE THE VECTOR FOR SIG GEN PURPOSES
        // ****************************************************
        String sSQL = "	select node.nodeid, nodetitle, nodesource from nodedata, node "
                + " where node.nodeid = nodedata.nodeid (+) and "
                + // if testing particular nodes
                //   " ( node.nodeid = 132562 or node.nodeid = 132574 )and " +
                " node.corpusid = " + iCorpusID;
        //api.Log.Log ("sSQL [" + sSQL + "]" ) ;
        Statement stmt = null;
        ResultSet rs = null;
        Vector vCFDNodesLeavesOrAll = new Vector();
        long lSeed = 1;
        try {
            stmt = dbc_.createStatement();
            rs = stmt.executeQuery(sSQL);

            int iLoop = 0;
            while (rs.next()) {
                iLoop++;
                //Vector vDBResult = JDBCIndra_Connection.ExecuteDBQueryVecOfVecsReturned (sSQL , dbc_ , -1);
                //Vector v3ParentID =(Vector) vDBResult.elementAt(3);

                int iNodeID = rs.getInt(1);
                //String sPath = (String) v1Paths.elementAt (iLoop);
                String sNodeTitle = rs.getString(2);

                vCFDNodesLeavesOrAll.addElement(
                        new CorpusFileDescriptor("nodedata " + iNodeID,
                        (long) iNodeID,
                        null,
                        -1,
                        sNodeTitle,
                        CorpusFileDescriptor.iFILE_OR_DIR_TYPE_htmlfile,
                        -1,
                        true, // clob as source
                        false, // NA - not running indexdocument
                        false, // NA - not running indexdocument
                        parseparms,
                        phraseparms));
            }
//            for ( int i = 0 ; i < vCFDNodesLeavesOrAll.size () ; i++ )
//            {
//                CorpusFileDescriptor cfd = (CorpusFileDescriptor) vCFDNodesLeavesOrAll.elementAt (i);
//                //api.Log.Log (i + ". cfd pre sig gen cfd.lNodeID [" + cfd.lNodeID + "]");
//                //api.Log.Log (i + ". cfd pre sig gen cfd.getTitle() [" + cfd.getTitle () + "]");
//
//            }
            if (iLoop == 0) {
                api.Log.LogFatal("no NODE and or NODEDATA records for corpus [" + iCorpusID + "]", new Exception("no vDBResult"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        //Log.log ("past loop");
        vCFDNodesLeavesOrAll = UtilRandom.randomPermuteVector(vCFDNodesLeavesOrAll, lSeed);
        //api.Log.Log ("sig gen query got vCFDNodesLeavesOrAll.size()  [" + vCFDNodesLeavesOrAll.size () + "] to randomize \r\n");			 // hbk control

        return vCFDNodesLeavesOrAll;
    } //  private static void step2b_getCDFVecRandom (int iCorpusID ,
}
