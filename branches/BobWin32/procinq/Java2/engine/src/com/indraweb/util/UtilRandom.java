package com.indraweb.util;

import java.util.*;

public class UtilRandom
{
	/**
	 * Given a Vector, return an array of ints which serve as a random sequence on the Vector
	 * 
	 * not guaranteed to move or touch the very first element I guess
	 * @param v any Vector
	 * @return int[] containing random Vector cover 
	 */
	// **************************************
	public static int[]  randomPermuteIntArr ( int[] iArr, long lSeed )
	// **************************************
	{
		Random r = new Random( lSeed );
		Object oTempForSwap = null;
			
		int iTempForSwap = -1;
		for ( int i = iArr.length - 1; i > 0; i-- )
		{
			int iSlot = getRandomInt_Min0_MaxSizeMinus1 ( r, iArr.length ) ;  	
			iTempForSwap = iArr[i];
			iArr[i] = iArr[iSlot];
			iArr[iSlot] = iTempForSwap;
		} 
		return iArr; 

	}		

	// **************************************
	public static Vector randomPermuteVector ( Vector v, long lSeed )
	// **************************************
	{
		int[] iArrPermutation = new int [ v.size() ];
		for ( int i = 0; i < v.size(); i++)
		{
			iArrPermutation[i] = i;	
		}
		
		iArrPermutation = randomPermuteIntArr ( iArrPermutation, lSeed ); 
		Vector vReturnRandom = new Vector();
		
		for ( int i = 0; i < v.size(); i++)
		{
			vReturnRandom.addElement ( v.elementAt ( iArrPermutation [ i ] ) );
		}
		
		return vReturnRandom;

	}		
	

	
	
	
	// *****************************
	public static int getRandomInt_Min0_MaxSizeMinus1 ( Random r, int max )
	// *****************************
	{
		double dRand = -1;
			
		int iRandAcrossVSizeRange = -1;
		while ( true )
		{ // in case we hit a random integer = vec size, then don't want index out of bounds - resample
			dRand = r.nextDouble();
			iRandAcrossVSizeRange = (int) ( dRand * max );
			if ( iRandAcrossVSizeRange != max )  // which will usually be the case
				break;
			
		}
		// Log.log ("rand # : " + iRandAcrossVSizeRange + "\r\n");
		return iRandAcrossVSizeRange;

	} 
		
}
