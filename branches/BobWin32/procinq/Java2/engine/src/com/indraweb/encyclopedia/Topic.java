// ------ (c) IndraWeb.com, Inc. ------
// 1999 01 20 used  document class as starting point
package com.indraweb.encyclopedia;

import com.indraweb.execution.Session;
import com.indraweb.html.HTMLTokenizerRedirector;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.html.UtilHTML;
import com.indraweb.html.htmltags.headingtags.HtmlTagMeta;
import com.indraweb.ir.*;
import com.indraweb.network.ExceptionStreamGenFail;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.signatures.WordFrequencies;
import com.indraweb.util.Log;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import com.indraweb.webelements.IndraURL;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.*;
import org.apache.log4j.Logger;

public class Topic {

    /*
     * places where word -> integer changes will affect things. 1) will need a master table/data struct of 1a) words -> int's for use during orthog and during doc scoring (if mem use in orthog makes a
     * diff - could just orthog as current and deal later) (perhaps have a separate hashtable of integers to int counts as needed in the topic class) (then only in doc scoring would it be needed) (
     * this could be a staged approach - leave orthog in word space, insert into db in word space, (load from DB into the int space for classification). (test speed of conversion of 20K of text to int
     * from word space) ( 1b) int -> words for search strings identification or insert to DB 2) general strategy Q: do we leave existing text DB structures as they are ... SIGNATURE, 3) sigs will be
     * orthog'd on ints not words 4) word freq files will be converted to int 5) sig count files will be in int space not word space
     */
    private static int iNumTotalTopcis = 0;
    public boolean bHaveNonTitleText = false;
    public boolean m_bFoundTitle = false;    // will be called
    public String m_DocumentTitle = "";    // will be called
    public String m_sTopicURL; // eg "file:///G:/SYSTEM/val4/IndraHome/CorpusSource/Britannica/hkBritTopLevels/article/BritTopicCanonical_Archaeology.html"
    public int m_URL_openedOKIndicator; // 0 - never returned from create/open URL // 1 - good // 2 failure on open
    public boolean bTruncatedByiNumBytesOfDocsToUse; // 0 - never returned from create/open URL // 1 - good // 2 failure on open
    public int iNumBytesOfDocsToUse; // 0 - never returned from create/open URL // 1 - good // 2 failure on open
    public int m_URL_completedReadIndicator; // 0 - never started read  // 1 started not completed //2 completed
    public int lTotalTextBytesReadByParser; // 0 - never started read  // 1 started not completed //2 completed
    public long m_lThisTopicNumUniquewords;
    // ALL TEXT IN ONE ...
    boolean bWantStringBuffForAllTextInOne = false;
    public StringBuilder sbAllTextInOne = null;
    public StringBuilder sbAllTextTitleInCaseNone = null;
    private String sAllTextInOneStopElim_NotStemmed = null;
    private String sAllTextInOneStopElim_Stemmed = null;
    private String sTitleStopElim_NotStemmed = null;
    private String sTitleStopElim_Stemmed = null;
    private Hashtable htThesaurusExpansionText = new Hashtable();
    public Hashtable htWordsDocOnly_noDescendants = new Hashtable(); //size will be var m_lThisTopicNumUniquewords
    public static final int DOC_GENRE_ProhibittedAsPerN2H2 = 69;
    private boolean CurrentlyParsingSectionOfDoc_Script = false;
    private boolean CurrentlyParsingSectionOfDoc_Title = false;
    private boolean CurrentlyParsingSectionOfDoc_Body = false;
    private boolean bGotFirstBodyText = false;
    private boolean CurrentlyParsingSectionOfDoc_Style = false;
    public boolean TopicValidOpenedOK = true;
    private IndraURL iurl = null;
    private boolean bCompletedStreamOpen = false;
    public Vector vMetaTags = new Vector();
    // some char's were not representable in oracle
    private Hashtable htCorpusID_To_StringThesaurusExpansion = new Hashtable();
    private ParseParms parseparms = null;
    private int iNodeID = -1;
    private static int iIndexDocument = 0;
    public static int iTopicObjectCounter = 0;
    private static final Logger log = Logger.getLogger(Topic.class);

    public boolean getbTopicStream_CompletedStreamOpen() {
        return bCompletedStreamOpen;
    }

    public int getTitleWordCount() {
        String[] sArrTitleWords = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption(m_DocumentTitle, " ", true);
        return sArrTitleWords.length;
    }

    public static Topic getATopicFromMinInputs(String sURL,
            boolean bThisTopicIsForSigGen_DiffScoring_and_FillInTitlesToDB_,
            boolean bWantStringBuffForAllTextInOne_,
            int iNumTextBytesToTakeFromDoc_,
            java.sql.Connection dbc_,
            boolean bWantPhrases,
            boolean bTakeFirstLineAsTitleIfNoTitleTag,
            boolean bAddIndexingTextFromTitle,
            boolean bTakeTitleFromStream,
            String sDelimiters,
            WordFrequencies wordfreq)
            throws Exception, Throwable {
        if (!sURL.toLowerCase().startsWith("file:") && !sURL.toLowerCase().startsWith("http:")) {
            sURL = "file:///" + Session.sIndraHome + sURL;
        }

        UrlHtmlAttributes uh = new UrlHtmlAttributes(sURL);
        long ltgetATopicFromMinInputs_start = System.currentTimeMillis();

        int iSocketTimeoutMSWebDocs = Session.cfg.getPropInt("socketTimeoutMSWebDocs");
        Topic t = new Topic(
                uh,
                true,
                dbc_, // fix this
                null,
                bThisTopicIsForSigGen_DiffScoring_and_FillInTitlesToDB_,
                // phrase scoring false ,
                iSocketTimeoutMSWebDocs,
                bWantStringBuffForAllTextInOne_,
                bWantPhrases,
                bTakeFirstLineAsTitleIfNoTitleTag,
                false,
                1,
                null,
                null, // text content not known in advance,
                bAddIndexingTextFromTitle,
                bTakeTitleFromStream,
                new PhraseParms(),
                new ParseParms(sDelimiters, false, iNumTextBytesToTakeFromDoc_),
                -1,
                wordfreq,
                null);

        long ltgetATopicFromMinInputs_end = System.currentTimeMillis();
        log.debug("GetATopicFromMinInputs completed in [" + (ltgetATopicFromMinInputs_end - ltgetATopicFromMinInputs_start) + "] ms for [" + sURL + "]\r\n");
        return t;
    } // getATopicFromMinInputs

    public Topic(
            UrlHtmlAttributes urlHtmlAttributesContext_,
            boolean fatalIfNotFound,
            java.sql.Connection dbc_,
            IndraURL iurl_,
            boolean bThisTopicIsForSigGen_DiffScoring_and_FillInTitlesToDB,
            int iTimedSocketTimeout,
            boolean bWantStringBuffForAllTextInOne_,
            boolean bWantPhrases,
            boolean bTakeFirstLineAsTitleIfNoTitleTag,
            boolean bVerbose,
            int iWeight,
            String sDocTitleToUse,
            String sTextContentIfKnownElseNull,
            boolean bAddIndexingTextFromTitle,
            boolean bTakeTitleFromStream,
            PhraseParms phraseparms,
            ParseParms parseParms_,
            int iNodeID,
            WordFrequencies wordfreq,
            SignatureParms sigparms) throws Exception, Throwable {

        this.bWantStringBuffForAllTextInOne = bWantStringBuffForAllTextInOne_;
        this.iNodeID = iNodeID;
        this.parseparms = parseParms_;

        iTopicObjectCounter++;

        log.debug("Topic constructing from url [" + urlHtmlAttributesContext_.getsURL() + "]");
        if (bWantPhrases && !bWantStringBuffForAllTextInOne_) {
            throw new Exception("invalid state in topic - want phrases without full string");
        }
        iNumTotalTopcis++;

        // get text from incoming string - not from a url/file/stream
        if (bWantStringBuffForAllTextInOne_) {
            sbAllTextInOne = new StringBuilder();
        }
        if (sTextContentIfKnownElseNull != null) {
            addTextFromString_egTopic(
                    urlHtmlAttributesContext_,
                    iurl_,
                    sDocTitleToUse,
                    sTextContentIfKnownElseNull,
                    parseParms_,
                    wordfreq,
                    sigparms);
            addPhrasesToCounts(phraseparms);
        } else {
            addTextFromStream_egDocument(
                    urlHtmlAttributesContext_,
                    iTimedSocketTimeout,
                    bWantPhrases,
                    bVerbose,
                    sDocTitleToUse,
                    bAddIndexingTextFromTitle,
                    bTakeTitleFromStream,
                    parseparms,
                    wordfreq,
                    sigparms);

        }
    }   // public Topic (String sTopicURL_, String sTopicIndex_, String sDirHTMLSumm_, boolean cachingOnly  )

    private void addTextFromString_egTopic(
            UrlHtmlAttributes urlHtmlAttributesContext_,
            IndraURL iurl_,
            String sDocTitleToPrependToAllText_andForTopicCompletion,
            String sTextContentIfKnownElseNull,
            ParseParms parseParms,
            WordFrequencies wordfreq,
            SignatureParms sigparms) throws Exception {
        try {
            m_sTopicURL = urlHtmlAttributesContext_.getsURL();
            log.debug("topic gen from [" + m_sTopicURL + "]");
            m_URL_openedOKIndicator = 0;		// 0 - never returned from create/open URL  // 1 - opened OK  // 2 failure on open
            m_URL_completedReadIndicator = 0;		// 0 - never started read  // 1 started not completed //2 completed
            iurl = iurl_;
            bCompletedStreamOpen = true;
            m_URL_openedOKIndicator = 1;	  // 0 - never returned from create/open URL // 1 - opened OK  // 2 failure on open
            m_URL_completedReadIndicator = 1;	  // 0 - never started read  // 1 started not completed //2 completed

            try {

                Hashtable htStopWords = clsStemAndStopList.getHTStopWordList();

                //if ( sDocTitleToUse != null && !sDocTitleToUse.trim ().equals ("") )

                // see how titles did and include
                // if not passed in and then not in db then see if the parsed doc had one ?
                if (sDocTitleToPrependToAllText_andForTopicCompletion == null) {
                    //api.Log.Log( "doc has no title" );
                    sDocTitleToPrependToAllText_andForTopicCompletion = "";
                }

                if (sTextContentIfKnownElseNull == null) {
                    sTextContentIfKnownElseNull = "";
                }

                // ADD BODY TEXT FROM A STRING - CLIP IF DESIRED
                if (parseParms.getNumBytesToTake() > 0) {
                    sTextContentIfKnownElseNull = sTextContentIfKnownElseNull.substring(0, parseParms.getNumBytesToTake());
                }
                addIndexingText(sTextContentIfKnownElseNull, htStopWords, false, 1, wordfreq, sigparms);

                // ADD TITLE TEXT FROM A STRING
                if (sDocTitleToPrependToAllText_andForTopicCompletion != null && !sDocTitleToPrependToAllText_andForTopicCompletion.trim().equals("")) {
                    addIndexingText(sDocTitleToPrependToAllText_andForTopicCompletion, htStopWords, true, 1, wordfreq, sigparms);
                }
                return;
            } catch (Exception e) {
                m_URL_openedOKIndicator = 2;   // not applicable actually
                Log.FatalError(" error occured in string building " + e.getMessage(), e);
            }
            m_lThisTopicNumUniquewords = htWordsDocOnly_noDescendants.size();
            m_URL_completedReadIndicator = 2;	  // 0 - never started read  // 1 started not completed //2 completed
        } catch (Exception e) {
            api.Log.LogError("caught exception in Topic constructor/addTextFromString_egTopic ", e);
            throw e;
        }
    }

    private void addTextFromStream_egDocument(
            UrlHtmlAttributes urlHtmlAttributesContext_,
            int iTimedSocketTimeout,
            boolean bWantPhrases,
            boolean bVerbose,
            String sDocTitleToUse,
            boolean bAddIndexingTextFromTitle,
            boolean bTakeTitleFromStream,
            ParseParms parseParms,
            WordFrequencies wordfreq,
            SignatureParms sigparms) throws Exception {
        try {
            m_URL_openedOKIndicator = 1;	  // 0 - never returned from create/open URL // 1 - opened OK  // 2 failure on open
            m_URL_completedReadIndicator = 1;	  // 0 - never started read  // 1 started not completed //2 completed

            try {
                //if (iIndexDocument % 500 == 0)
                //api.Log.Log("in IndexDocument call [" + iIndexDocument + "] m_sTopicURL [" + m_sTopicURL + "] urlHtmlAttributesContext_.getsURL() [" + urlHtmlAttributesContext_.getsURL() + "]");
                iIndexDocument++;

                Hashtable htStopWords = clsStemAndStopList.getHTStopWordList();
                //api.Log.Log("*** calling index document ***");

                // create stream from input string
                ByteArrayInputStream is = new ByteArrayInputStream(urlHtmlAttributesContext_.getText().getBytes("UTF-8")); // urlHtmlAttributesContext_.getText()

                IndexDocument(
                        is,
                        m_sTopicURL,
                        iurl,
                        //bDoPhraseScoring ,
                        urlHtmlAttributesContext_,
                        iTimedSocketTimeout,
                        bWantPhrases,
                        //bTakeFirstLineAsTitleIfNoTitleTag,
                        bVerbose,
                        htStopWords,
                        bAddIndexingTextFromTitle,
                        bTakeTitleFromStream,
                        parseParms,
                        wordfreq,
                        sigparms);

                //api.Log.Log ("after index doc result:" + this.toString());

                if (sDocTitleToUse != null && !sDocTitleToUse.trim().equals("") & !m_bFoundTitle) // ADD TITLE TEXT FROM A STREAM
                {
                    addIndexingText(sDocTitleToUse.toLowerCase(), htStopWords, false, 1, wordfreq, sigparms);
                }

                // see how titles did and include
                // if not passed in and then not in db then see if the parsed doc had one ?
                if (sDocTitleToUse == null) {
                    //api.Log.Log( "doc has no title" );
                    sDocTitleToUse = "";
                }

                //api.Log.Log("addTestFromStream: sbAllTextInOne pre [" + sbAllTextInOne.toString().length() + "] [" + sbAllTextInOne.toString() + "]");
                // print all text               if (UtilFile.bFileExists("/temp/IndraDebugPrintAllTextOfDocs.txt") )
                //                {
                //api.Log.Log("*** sbAllTextInOne.toString () [" + sbAllTextInOne.toString() + "]");
                //                }
            } catch (ExceptionStreamGenFail es) {
                //Log.NonFatalError("ExceptionStreamGenFail cant open this (possibly redirected) URL in topic constructor [ " + es.getMessage() + " ]\r\n");
                m_URL_openedOKIndicator = 2;   // not opened URL correctly
                return;
            } catch (Exception e) {
                Log.FatalError(" error occured in indexing " + e.getMessage(), e);
            }
            m_lThisTopicNumUniquewords = htWordsDocOnly_noDescendants.size();
            m_URL_completedReadIndicator = 2;	  // 0 - never started read  // 1 started not completed //2 completed
            //Log.log (":: completed topic size [" + htWordsDocOnly_noDescendants.size()  +
            //"] title [" + m_DocumentTitle +
            //"] url [" + UtilFileAndDirNames.StripIndraHome ( m_sTopicURL ) +
            //"]\r\n");
        } catch (Throwable t) {
            api.Log.LogError("caught throwable in Topic constructor", t);
            if (t instanceof java.lang.OutOfMemoryError) {
                api.Log.LogError("caught java.lang.OutOfMemoryError in Topic constructor/addTextFromStream_egDocument ", t);
                throw new Exception("caught java.lang.OutOfMemoryError in Topic constructor/addTextFromStream_egDocument ");
            }
        }

    }
    private static int indexedcounter = 0;

    private int IndexDocument(
            InputStream isTopStrm,
            String sURLName,
            IndraURL iurl_,
            UrlHtmlAttributes urlHtmlAttributesContext_,
            int iTimedSocketTimeout_,
            boolean bWantPhrases,
            boolean bVerbose,
            Hashtable htStopWords,
            boolean bAddIndexingTextFromTitle,
            boolean bTakeTitleFromStream,
            ParseParms parseParms,
            WordFrequencies wordfreq,
            SignatureParms sigparms)
            throws Exception {

        indexedcounter++;

        try {
            int iWordCounter = 0;
            if (indexedcounter % 500 == 0) {
                api.Log.Log("Indexing doc # "
                        + "[" + indexedcounter + "] "
                        + " sURLName  [" + sURLName + "]"
                        + " bIncludeNumbersFromDoc [" + parseParms.getIncludeNumbers() + "]"
                        + " iNumBytesOfDocsToUse_ [" + parseParms.getNumBytesToTake() + "]"
                        + " bWantPhrases [" + bWantPhrases + "]");
            }
            long ltStartIndexDocument = System.currentTimeMillis();

            //hack hack hack not needed next call
            //long lcacheID = WebCacheReads.getCacheIDFromURL ( sURLName, dbcMain_ );
            if (Session.stopList == null) // really should be done in session as a privatized variabel
            {
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
            }

            String strCurrentLine;  // HTMLTokenizer[offset=2048,type=text      , tag=null,attrs=null,text=Reproductive System

            long lTotalTextBytesSoFar = 0;
            boolean bTitleMayHaveBeenOfAFilteredDoc = false; // n2h2
            try {
                com.ms.util.HTMLTokenizer lHTMLTok = null;
                try {
                    lHTMLTok = HTMLTokenizerRedirector.getTokenizer(isTopStrm, true, urlHtmlAttributesContext_, iTimedSocketTimeout_, 0, bVerbose);
                } catch (Exception es) {
                    throw es;
                }
                StringTokenizer stHtmlTok;
                // to tokenize individual html lines like : HTMLTokenizer[offset=5823,type=text,tag=null,attrs=null, text=The female ...

                String strHTMLTextItem, strNextLine;

                // for each line in the html tokenizer output file
                int j = 0;

                int failedcount = 0;
                boolean bGotAnHTMLTokenSoFarAllLines = false;
                // get doc title from user calling classify say or from db ...

                while (lHTMLTok.hasMoreTokens()) {
                    //boolean bThisIsAPropTitle_RepeatedInText = false;
                    boolean bDefaultThisLine = false;
                    /*
                     * if ( j > 0 && !bGotAnHTMLTokenSoFarAllLines ) { System.out.println ( "not html aborting parse [" + sURLName + "], " ); throw new Exception ( "not html [" + sURLName + "]" ); }
                     */

                    if (parseParms.getNumBytesToTake() > 0 && lTotalTextBytesSoFar >= parseParms.getNumBytesToTake()) {
                        //Log.log ("reached max doc bytes of [" +
                        //		 iNumBytesOfDocsToUse_ + "] for url [" +
                        //		 m_sTopicURL + "]\r\n");
                        if (iurl_ != null) {
                            iurl_.setFlagTopicWentOverByteMAxOnRead();
                        }
                        bTruncatedByiNumBytesOfDocsToUse = true;
                        break;
                    }
                    // get next line out of HTML tokenizer (eg of form: HTMLTokenizer[offset=2048,type=tag,tag=META,attrs={name=TYPE, content=ARTICLE, /=},text=null")
                    strNextLine = null;

                    try {
                        lHTMLTok.nextToken();
                        // strNextLine = lHTMLTok.text; // get another line from the HTML tokenizer
                        strNextLine = lHTMLTok.toString(); // get another line from the HTML tokenizer

                        //api.Log.Log (j + ". xx strNextLine [" + strNextLine + "]\r\n");  // DEBUG
                        //if ( UtilStrings.getStrContains ( strNextLine.toLowerCase(), "type=text" ) )
                        // System.out.print ( j + ". xx CurrentlyParsingSectionOfDoc_Script [" + CurrentlyParsingSectionOfDoc_Script+ "] [" + strNextLine + "]\r\n" );
                        //if ( UtilFile.bFileExists( "c:/temp.txt") )
                        //Log.log ( j + ". " + strNextLine + "\r\n" );
                    } catch (java.text.ParseException e) {
                        failedcount++;
                        if (failedcount > 50) {
                            //Log.NonFatalError("failedcount > 50 Parse exceptions in line of HTML file line, skiping file " + j + ". " + sURLName);
                            //if ( !m_bFoundTitle && bTakeFirstLineAsTitleIfNoTitleTag && sbAllTextTitleInCaseNone != null )
                            //{
                            //   bTitleIsFirstLineSub = true;
                            //  m_DocumentTitle = sbAllTextTitleInCaseNone.toString();
                            //}
                            return -1;
                        }
                        continue; // try again see if fails
                    }
                    failedcount = 0;
                    //api.Log.Log  ( "strNextLine [" + strNextLine + "]" );

                    // DEBUG FIND A PARTICULAR WORD
                    // if (strNextLine.indexOf ("analysis") > 0)
                    //	int debugme  = 0;

                    int itokType = -1;
                    stHtmlTok = null;
                    strCurrentLine = null;

                    if (j == 0) {
                        if (!(strNextLine.toLowerCase().indexOf("tag=") > 0)) {
                            //System.out.println ( "not html aborting parse [" + sURLName + "] first parse line [" + strNextLine + "]" );
                            //throw new Exception ( "not html [" + sURLName + "] first parse line [" + strNextLine + "]" );
                            api.Log.LogError("not html strNextLine [" + strNextLine + "]");
                            throw new Exception("not html strNextLine [" + strNextLine + "]");
                        }
                    }
                    stHtmlTok = new StringTokenizer(strNextLine);	 // parse the one line
                    strCurrentLine = strNextLine;

                    //api.Log.Log("*** line: " + strCurrentLine);

                    // set machine state
                    try {
                        itokType = UtilHTML.HTMLTagType(stHtmlTok);
                    } catch (Exception e) {
                        Log.NonFatalError("error dsds ", e);
                    }

                    switch (itokType) {
                        // To Do :  what to do with text prior to
                        // note : under current setup, any text prior to title will probalby end up in the title hash
                        //        tables if not as the title of the doc, causing error flag if/when real title text comes.

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OPENTITLE:
                            if (iurl != null) {
                                iurl.setdidURLGetReadOK(true, 3);  // assume that if we got a title it opened ok
                            }
                            CurrentlyParsingSectionOfDoc_Title = true;
                            // TEST IF SECOND TITLE IN DOC (ERROR ? i guess so)
                            break;


                        case UtilHTML.HTMLTagType_CLOSETITLE:
                            CurrentlyParsingSectionOfDoc_Title = false;
                            break;



                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OPENBODY:
                            CurrentlyParsingSectionOfDoc_Body = true;
                            break;

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OPENHEAD1:
                            break;
                        case UtilHTML.HTMLTagType_CLOSEHEAD1:
                            break;

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OPENHEAD2:
                            break;
                        case UtilHTML.HTMLTagType_CLOSEHEAD2:
                            break;

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OPENHEAD3:
                            break;
                        case UtilHTML.HTMLTagType_CLOSEHEAD3:
                            break;

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OPENSCRIPT:
                            CurrentlyParsingSectionOfDoc_Script = true;
                            break;
                        case UtilHTML.HTMLTagType_CLOSESCRIPT:
                            CurrentlyParsingSectionOfDoc_Script = false;
                            break;

                        // note : add more levels - 9 final in HTML I read

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OPENSTYLE:
                            CurrentlyParsingSectionOfDoc_Style = true;
                            break;
                        case UtilHTML.HTMLTagType_CLOSESTYLE:
                            CurrentlyParsingSectionOfDoc_Style = false;
                            break;

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_META:
                            //vMetaTags.addElement ( new HtmlTagMeta ( strCurrentLine )  );
                            vMetaTags.addElement(new HtmlTagMeta(strCurrentLine));
                            break;

                        // ###########################################################################
                        case UtilHTML.HTMLTagType_OpenAHref:

                            // embed a see link if desired
                            break;


                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        // START HTMLTagType_TEXT ###########################################################################
                        case UtilHTML.HTMLTagType_TEXT:
                            if (CurrentlyParsingSectionOfDoc_Script) {
                                break;
                            }

                            if (!CurrentlyParsingSectionOfDoc_Title) {
                                bHaveNonTitleText = true;
                            } else {
                                //api.Log.Log ("debug test ");
                            }


                            // for meta content, headers, regular text - all - index the words themselves (header declaration and text are separate)
                            //this will be the entire text string

                            strHTMLTextItem = stHtmlTok.nextToken(",");
                            strHTMLTextItem = stHtmlTok.nextToken("=");
                            try {   // in case the text is nothing but an '=' sign as in an equation ...
                                StringBuilder sb = new StringBuilder(); // in case the text is nothing but an '=' sign as in an equation ...
                                //strHTMLTextItem = stHtmlTok.nextToken( "=" );
                                int iEqCnt = 0;
                                while (stHtmlTok.hasMoreTokens()) {
                                    strHTMLTextItem = stHtmlTok.nextToken("=");
                                    if (iEqCnt > 0) {
                                        sb.append(" = ").append(strHTMLTextItem).append(" ");
                                    } else {
                                        sb.append(strHTMLTextItem).append(" ");
                                    }

                                    iEqCnt++;
                                }
                                strHTMLTextItem = sb.toString();
                            } catch (NoSuchElementException e2) {
                                strHTMLTextItem = "";
                                // Log.NonFatalError ( "NoSuchElementException in topic parser " + e2.getMessage() ) ;

                                //HKonLib.print ( "continuing over NoSuchElementException ('=' in text?): " + e.toString() + ", " +
                                //	stHtmlTok);
                            }


                            if (CurrentlyParsingSectionOfDoc_Body && bGotFirstBodyText == false) {
                                bGotFirstBodyText = true;
                                strHTMLTextItem = " " + strHTMLTextItem;
                            }

                            strHTMLTextItem = strHTMLTextItem.concat(" ");    // add trail blank so stem routine can see last token

                            if (!strHTMLTextItem.equals("")) { // ie, if not text = '='?
                                if (bTitleMayHaveBeenOfAFilteredDoc) // n2h2
                                {
                                    if (strHTMLTextItem.indexOf("Use the N2H2 URL Checker") >= 0) {

                                        if (iurl_ != null) {
                                            iurl_.setFlagThisWasObjectionableContent();
                                            iurl_.setdidURLGetReadOK(true, 4);
                                            if (bVerbose) //Log.logcr("n2h2 filter url : " + sURLName);
                                            //if ( !m_bFoundTitle && bTakeFirstLineAsTitleIfNoTitleTag && sbAllTextTitleInCaseNone != null )
                                            // {
                                            //    bTitleIsFirstLineSub = true;
                                            //    m_DocumentTitle = sbAllTextTitleInCaseNone.toString();
                                            //}
                                            {
                                                return 1;
                                            }
// early return !!  porn site as per N2H2
                                        }

                                    }


                                }

                                strHTMLTextItem = strHTMLTextItem.concat(" ");    // add trail blank so stem routine can see last token
                                lTotalTextBytesSoFar += strHTMLTextItem.length();


                                // see if it's time for a new node.
                                if (CurrentlyParsingSectionOfDoc_Title) {
                                    // count only h
                                    if (!m_bFoundTitle && bTakeTitleFromStream) {
                                        m_DocumentTitle = strHTMLTextItem.trim();	// give document a title

                                        if (m_DocumentTitle.equals("Requested Page is Unavailable")) // 3. getLine [HTMLTokenizer[offset=41,type=text,tag=null,attrs=null,text=Bess Can't Go There]
                                        {
                                            bTitleMayHaveBeenOfAFilteredDoc = true;
                                        }
                                        // DBAccessMetaSearch.updateTitle ( m_lDocIDThisTopic, m_DocumentTitle, dbcMain_ );
                                    }

                                    //TreeNodeSetParserLevel (0 , m_DocumentTitle , sURLName);
                                    m_bFoundTitle = true;

                                }


                                strHTMLTextItem = strHTMLTextItem.toLowerCase();
                                //api.Log.Log("*** strHTMLTextItem: " + strHTMLTextItem);

                                if (!CurrentlyParsingSectionOfDoc_Style
                                        && !CurrentlyParsingSectionOfDoc_Script
                                        && (bAddIndexingTextFromTitle || !CurrentlyParsingSectionOfDoc_Title)) {
                                    // ADD BODY TEXT FROM A STREAM
                                    iWordCounter += addIndexingText(strHTMLTextItem,
                                            //false , // title special hadndling already done above (check that)
                                            htStopWords, false, 1, wordfreq, sigparms);
                                }
                            }  // if (!strHTMLTextItem.equals(""))  - if not a null piece of text
                            break;
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################
                        // END HTMLTagType_TEXT ###########################################################################

                        default:
                            bDefaultThisLine = true;
                            break;
                    } // end switch

                    if (!bDefaultThisLine) {
                        bGotAnHTMLTokenSoFarAllLines = true;  // want to find at least one real html token in the first 10 lines else quit out
                    }
                    j++;
                } // while (lHTMLTok.hasMoreTokens())

                //com.indraweb.ir.PhraseExtractor.buildPhrases ( sbAllTextInOne.toString(), this.htWordsDocOnly_noDescendants, 2, 3, " ", Topic.getsDelimitersText(), 5 );
                //System.exit(1);

            } catch (ExceptionStreamGenFail e3) {
                //Log.NonFatalError("ExceptionStreamGenFail in IndexDocument() e.msg [" + e3.getMessage() + "]\r\n");
                iurl_.setdidURLGetReadOK(false, 4);
                throw e3;
            } catch (Exception e2) {
                String sErr_getMessage = e2.getMessage();
                String sErr_stack = Log.stackTraceToString(e2);
                log.error("sErr_getMessage [" + sErr_getMessage + "]");
                log.error("sErr_stack [" + sErr_stack + "]");

                if (bVerbose) {
                    Log.logcr("IndexDocument err [" + e2.getMessage() + "] ");
                }
                if (iurl_ != null) {
                    iurl_.setdidURLGetReadOK(false, 5);
                }
            } finally {
                //api.Log.Log("IndexDocument finally \r\n");
                if (iurl_ != null) {
                    iurl_.setNumTextBytesTotal((int) lTotalTextBytesSoFar);
                }
                long ltIndexDocument_End = System.currentTimeMillis();
                long durationIndexing = ltIndexDocument_End - ltStartIndexDocument;
                //if (( durationIndexing ) > 1000 )
                String time = UtilStrings.numFormatLong((durationIndexing), 4);
                log.info("IndexDocument completed [" + time + "] ms " + "URL");
                //);
                lTotalTextBytesReadByParser = (int) lTotalTextBytesSoFar;
                if (durationIndexing > Session.cfg.getPropInt("NumMSForDocIndexConsideredLong")) {
                    Log.logClear("IndexDocument time (ms) [" + time + "] ms URL \r\n");
                }
                try {
                    if (isTopStrm != null) {
                        isTopStrm.close();
                    }
                } catch (IOException e2) {
                    Log.NonFatalError("IOException unable to close stream in IndexDocument", e2);
                }
            }
        } catch (Exception e) {
            log.error("An exception ocurred. ", e);
        }

        return 1;
    }  // private int   IndexDocument (InputStream isTopStrm, int iTopicID, String sURLName,

    /**
     *
     * @param bStemmed
     * @param bKeepLocalReference
     * @return
     */
    public String getAllTextInOne_delimRemoved(boolean bStemmed, boolean bKeepLocalReference) {
        String sReturn = null;
        if (bStemmed) {
            String s = null;
            if (sAllTextInOneStopElim_Stemmed == null) {
                s = com.indraweb.ir.IRStringHandlers.convertNormalizeString_ElimStopStemOption(
                        sbAllTextInOne.toString(),
                        parseparms.getDelimiters(),
                        false,
                        bStemmed);
                if (bKeepLocalReference) {
                    sAllTextInOneStopElim_Stemmed = s;
                }
                sReturn = s;
            } else {
                sReturn = sAllTextInOneStopElim_Stemmed;
            }
        } else {
            // not stemmed
            String s = null;
            if (sAllTextInOneStopElim_NotStemmed == null) {
                s = com.indraweb.ir.IRStringHandlers.convertNormalizeString_ElimStopStemOption(
                        sbAllTextInOne.toString(),
                        parseparms.getDelimiters(),
                        false,
                        bStemmed);
                if (bKeepLocalReference) {
                    sAllTextInOneStopElim_NotStemmed = s;
                }
                sReturn = s;
            } else {
                sReturn = sAllTextInOneStopElim_NotStemmed;
            }
        }
        return sReturn;
    } // public String getAllTextInOneStopElim ( boolean bStemmed,

    public String getTitleStopElim(boolean bStemmed, boolean bKeepLocalReference) {
        String sReturn = null;
        if (bStemmed) {
            String s = null;
            if (sTitleStopElim_Stemmed == null) {
                s = com.indraweb.ir.IRStringHandlers.convertNormalizeString_ElimStopStemOption(
                        m_DocumentTitle,
                        parseparms.getDelimiters(),
                        true,
                        true);
                if (bKeepLocalReference) {
                    sTitleStopElim_Stemmed = s;
                }
            }
            sReturn = s;
        } else {
            String s = null;
            if (sTitleStopElim_NotStemmed == null) {
                s = com.indraweb.ir.IRStringHandlers.convertNormalizeString_ElimStopStemOption(
                        m_DocumentTitle,
                        parseparms.getDelimiters(),
                        true,
                        false);
                if (bKeepLocalReference) {
                    sTitleStopElim_Stemmed = s;
                }
            }
            sReturn = s;
        }
        return sReturn;
    } // public String getTitleStopElim ( boolean bStemmed,

    public String getTitleNoCache() {
        return m_DocumentTitle;
    }

    /**
     * get a single text block containing the title and text multiword thesaurus expansions with repeats for accurate counts
     * <p/>
     * @param bStemmed
     * @param iID_CorpusOrThesaurus
     * @param bCorpusIDTrueThesaurusIDFalse
     * @param dbc
     * @return
     * @throws Exception
     */
    public String getThesaurusExpansionText(
            boolean bStemmed,
            int iID_CorpusOrThesaurus,
            boolean bCorpusIDTrueThesaurusIDFalse,
            Connection dbc)
            throws Exception {
        String sCacheLookup = iID_CorpusOrThesaurus + ":" + bStemmed + ":" + bCorpusIDTrueThesaurusIDFalse;
        String sReturn = (String) htCorpusID_To_StringThesaurusExpansion.get(sCacheLookup);
        if (sReturn == null) {
            Thesaurus thes = Thesaurus.cacheGetThesaurus(bStemmed,
                    iID_CorpusOrThesaurus,
                    bCorpusIDTrueThesaurusIDFalse,
                    dbc);

            String sAllTextInOne_delimRemoved = getAllTextInOne_delimRemoved(false, false);
            if (bStemmed) {
                sAllTextInOne_delimRemoved = com.indraweb.ir.IRStringHandlers.convertNormalizeString_ElimStopStemOption(
                        sAllTextInOne_delimRemoved,
                        parseparms.getDelimiters(),
                        false,
                        bStemmed);
            }

            Vector vStrings = thes.getMultiWordExpansions(sAllTextInOne_delimRemoved);
// now stem if wanted

            sReturn = UtilStrings.convertVecToString(vStrings, " ");
            htThesaurusExpansionText.put(sCacheLookup, sReturn);
        }
        return sReturn;
    }

    public String getsAllTextInOneStopElim_NotStemmed() {
        return sAllTextInOneStopElim_NotStemmed;
    }

    private int addIndexingText(String sText,
            Hashtable htStopWords,
            boolean bPrependAndTitleIfTrue,
            int iTitleMultiplier,
            WordFrequencies wordfreq,
            SignatureParms sigparms) throws Exception {
        bPrependAndTitleIfTrue = false;

        int iWordCounter = 0;

        // STEP 1/2 APPEND TO TEXT BLOCKS

        //if ( bTitle == false && sText!= null && !sText.trim().equals("") )
        if (!bPrependAndTitleIfTrue && (sText != null && !sText.trim().equals(""))) {
            bHaveNonTitleText = true;
        }

        if (parseparms.getDelimiters() == null) {
            throw new Exception("delimiter set needed in topic or document text accretion");
        }

        // if ( sText.toLowerCase().indexOf("very good idea") > 0 )
        //     api.Log.Log ("in addIndexingText very good idea [" + sText  +"]");// sdelim [" + sDelim + "]" );

        if (bWantStringBuffForAllTextInOne) {
            //api.Log.Log ("sbAllTextInOne pre add text [" + sbAllTextInOne + "]" )  ;
            //if (bTitle)
            //{
            // char[] cArrText = sText.toCharArray();
            // processAddTitle ( sDocTitleToUse, vecCPhraseScorers_DoingTitleToTitle );
            //sbAllTextInOne.insert ( 0, sText.trim().concat(" "));
            if (bPrependAndTitleIfTrue) {
                sbAllTextInOne.insert(0, sText.trim().concat(" "));
                m_DocumentTitle = sText;
                m_bFoundTitle = true;

            } else {
                String s = sText.trim();
                if (!s.equals("")) {
                    sbAllTextInOne.append(" ").append(s);
                }
            }
        }


        // STEP 2/2 FILL IN THE COUNTERS
        try {
            // not to be confused with sig gen - this is just doc term count accretion
            // GET TITLE COUNT (MAX) - don't want to be in the mud with
            int iMaxPriorWordsCount = 1;
            if (bPrependAndTitleIfTrue) {
                //                if ( htWordsDocOnly_noDescendants.size() == 0 )
                //                    api.Log.Log ("should be calling this with title AFTER with text... for max prior count correctness");
                // get word count max - requires that other words be accreted pre title
                iMaxPriorWordsCount = getMaxPriorWordCount(iMaxPriorWordsCount, wordfreq, sigparms);
            }

            StringTokenizer strTokTextString = new StringTokenizer(sText, parseparms.getDelimiters());
            int iTokenCounter = 0;
            while (strTokTextString.hasMoreTokens()) {
                iTokenCounter++;
                String strWord = strTokTextString.nextToken(parseparms.getDelimiters()).toLowerCase();

                //if ( iCallCtr_addIndexingText == 6 )
                //    api.Log.Log (iCallCtr_addIndexingText+
                //            ". title individual tokenized word strWord [" + strWord + "]" );

                if (strWord != null && htStopWords.get(strWord) == null) {
                    iWordCounter++;

                    //newCount = ((Integer) hasht.get (s)).intValue() ;
                    //api.Log.Log ("topic.addIndexingText Incremented doc count by 1 for word [" + strWord + "]" );
                    if (!strWord.trim().equals("")) {
                        if (bPrependAndTitleIfTrue) {
                            //api.Log.Log ("set title word in add text [" + strWord + "] node [" + iNodeID + "] to title cnt [" + iTitleMultiplier+ "] size [" + this.htWordsDocOnly_noDescendants.size() + "]" );
                            //UtilSets.hash_increment_count_for_string (this.htWordsDocOnly_noDescendants , strWord , iMaxPriorWordsCount);
                            //api.Log.Log ("set title term to iMaxPriorWordsCount [" + iMaxPriorWordsCount + "] for word [" + strWord + "]" );
                            htWordsDocOnly_noDescendants.put(strWord, new Integer(iTitleMultiplier * iMaxPriorWordsCount));
                        } else {
                            //api.Log.Log ("incr non title word in add text [" + strWord  + "] by 1 from [" + this.htWordsDocOnly_noDescendants.get (strWord ) + "] size [" + this.htWordsDocOnly_noDescendants.size() + "]" );
                            UtilSets.hash_increment_count_for_string(this.htWordsDocOnly_noDescendants, strWord, 1);
                        }

                    }
                } // if not a stop word
            } // while (strTokTextString.hasMoreTokens ())
        } // try
        catch (NoSuchElementException e2) {
            // done loop
            //api.Log.LogError ("in topic parser sDelim [" + sDelim + "] stext [" + sText + "]" , e2);
        } catch (Exception e3) {
            api.Log.LogError("in topic parser sDelim [" + parseparms.getDelimiters() + "] stext [" + sText + "]", e3);
        }



        //api.Log.Log (iCallCtr_addIndexingText +". post addIndexingText len [" +sbAllTextInOne.toString().length()+ "] [" + sbAllTextInOne.toString() + "] \r\n" +
        //            " counters [" + UtilSets.htToStr(this.htWordsDocOnly_noDescendants, "\r\n")+ "]");// sdelim [" + sDelim + "]" );
        return iWordCounter;
    }

    public int addPhrasesToCounts(PhraseParms phraseparms) throws Exception {
        //api.Log.Log ("htWordsDocOnly_noDescendants in 1 phrase add node [" +iNodeID+  "] [" + UtilSets.htToStr(htWordsDocOnly_noDescendants)+ "]" );
        Hashtable htPhraseCollector = new Hashtable();
        try {
            //Log.logClear ("hbk control iPhraseCountMultiplier [" + iPhraseCountMultiplier + "]\r\n");

            try {
                //api.Log.Log ("processing for phrases [" + sbAllTextInOne.toString ().toLowerCase ()+ "]" );
                int iNumPhrasesBuilt = com.indraweb.ir.PhraseExtractor.buildPhrases(sbAllTextInOne.toString(),
                        htPhraseCollector,
                        phraseparms.iPhraseLenMin, //  Session.cfg.getPropInt ("PhraseLenMin") ,
                        phraseparms.iPhraseRepeatMin, // Session.cfg.getPropInt ("PhraseRepeatMin") ,
                        parseparms.getDelimiters(),
                        phraseparms.iPhraseLenMax, // Session.cfg.getPropInt ("PhraseLenMax") ,
                        phraseparms.iPhraseCountMultiplier,
                        phraseparms.bPhraseWordsCanStartWithNonLetters);
                //api.Log.Log ("htWordsDocOnly_noDescendants in 2 phrase add node [" +iNodeID+  "] [" + UtilSets.htToStr(htWordsDocOnly_noDescendants)+ "]" );
                //api.Log.Log ("found [" + iNumPhrasesBuilt + "] phrases" );

            } catch (Exception e) {
                Log.NonFatalError("eror in build phrases \r\n", e);
            }

            Enumeration ePhraseKeys = htPhraseCollector.keys();
            HashSet hsDelimsAsChars = UtilSets.strToCharHashSet(parseparms.getDelimiters());
            while (ePhraseKeys.hasMoreElements()) {
                String sPhrase = (String) ePhraseKeys.nextElement();
                Integer ICount = (Integer) htPhraseCollector.get(sPhrase);
                //api.Log.Log ("node [" + iNodeID + "] found sPhrase  [" + sPhrase  + "] at count [" + ICount + "]"  );
                String sPhraseCleaned = PhraseExtractor.cleanPhrase(sPhrase, hsDelimsAsChars);
                //api.Log.Log ("node [" + iNodeID + "] found sPhrase [" + sPhrase + "] but adding [" + sPhraseCleaned + "] at count [" + ICount + "]"  );

                // PHASE INCREMENT
//                if ( iNodeID == 100000016)
//                    api.Log.Log ("htWordsDocOnly_noDescendants in 3 phrase add node [" +iNodeID+  "] [" + UtilSets.htToStr(htWordsDocOnly_noDescendants, "\r\n")+ "]" );
                UtilSets.hash_increment_count_for_string(this.htWordsDocOnly_noDescendants, sPhraseCleaned, ICount.intValue());
//                if ( iNodeID == 100000016)
//                    api.Log.Log ("htWordsDocOnly_noDescendants in 4 phrase add node [" +iNodeID+  "] [" + UtilSets.htToStr(htWordsDocOnly_noDescendants, "\r\n")+ "]" );
                //UtilSets.hash_increment_count_for_string(this.htWordsDocOnly_noDescendants, sPhraseCleaned, ICount.intValue() * iWeight);
                //Log.logClear ("sPhraseCleaned [" + sPhraseCleaned + "] count [" + ICount + "]\r\n");
            }
        } catch (Exception e) {
            Log.logClear("error in buildphrases [" + e.getMessage() + "]\r\n");
            Log.NonFatalError("error in buildphrases\r\n", e);
            throw e;
        }

        return htPhraseCollector.size(); // num phrases found

    }

    @Override
    public String toString() {
        String sballstring = "null";
        if (sbAllTextInOne != null) {
            sballstring = sbAllTextInOne.toString();
        }
        return "\r\n\r\n================= START TOPIC title [" + getTitleNoCache() + "] ================ \r\n ht [" + UtilSets.htToStr(htWordsDocOnly_noDescendants, "\r\n") + "] \r\nsbAllTextInOne [" + sballstring + "]\r\n==== END TOPIC\r\n\r\n";
    }

    public StringBuilder getsbAllTextInOne() {
        return sbAllTextInOne;
    }

    public int getNodeID() {
        return (int) iNodeID;
    }

    private int getMaxPriorWordCount(int iMaxPriorWordsCount,
            WordFrequencies wordfreq,
            SignatureParms sigparms) throws Exception {
        Enumeration enumAllWords = htWordsDocOnly_noDescendants.keys();
        while (enumAllWords.hasMoreElements()) {
            String sTerm = (String) enumAllWords.nextElement();
            int iCount = ((Integer) htWordsDocOnly_noDescendants.get(sTerm)).intValue();
            if (iCount > iMaxPriorWordsCount) {
                // get max counts only from terms not in the dynamic stop list
                if (wordfreq != null) {
                    if (wordfreq.getWord(sTerm.toLowerCase()) == null) {
                        if (sigparms != null) {
                            // get max counts only from terms that qualify as sig terms
                            if (sigparms.signatureTermQualifierModifier(sTerm) != null) {
                                iMaxPriorWordsCount = iCount;
                            }
                        } else {
                            throw new Exception("invalid mode in topic text accretion - wordfreq not null but sigparms is");
                        }

                    }
                } else {
                    //api.Log.Log("sTerm2 [" + sTerm + "] gets max -> iCount [" + iCount + "]");
                    iMaxPriorWordsCount = iCount;
                }

            }
        }
        return iMaxPriorWordsCount;
    }
}
