package com.indraweb.html;


import com.indraweb.util.Log;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.clsUtils;

import java.util.Vector;


/*
	public static final int HTMLTagType_OPENTITLE		= 1;
	public static final int HTMLTagType_CLOSETITLE	= 2;
	public static final int HTMLTagType_OPENHEAD		= 3;
	public static final int HTMLTagType_CLOSEHEAD		= 4;
	public static final int HTMLTagType_OPENHEAD1		= 5;
	public static final int HTMLTagType_CLOSEHEAD1	= 6;
	public static final int HTMLTagType_OPENHEAD2		= 7;
	public static final int HTMLTagType_CLOSEHEAD2	= 8;
	public static final int HTMLTagType_OPENHEAD3		= 9;
	public static final int HTMLTagType_CLOSEHEAD3	= 10;
	public static final int HTMLTagType_TEXT			= 11;
	public static final int HTMLTagType_META			= 12;
	public static final int HTMLTagType_OpenA			= 13;
	public static final int HTMLTagType_CloseA		= 14;
	public static final int HTMLTagType_OpenAHref     = 15;
	public static final int HTMLTagType_COMMENT       = 16;
	public static final int HTMLTagType_OPENHTML      = 17;
	public static final int HTMLTagType_CLOSEHTML     = 18;
	public static final int HTMLTagType_OPENBODY      = 19;
	public static final int HTMLTagType_CLOSEBODY     = 20;
	public static final int HTMLTagType_BR	        = 21;
	public static final int HTMLTagType_OpenP 	    = 22;
	public static final int HTMLTagType_CloseP 	    = 23;
	public static final int HTMLTagType_OPENTABLE 	= 24;
	public static final int HTMLTagType_CLOSETABLE 	= 25;
*/


public class UtilHTMLMem {

    // ******************************************************************************************
    public static int findHTMLTextOrAnchor(
            // BLOCK******************************************************************************************
            HTMLMemDoc memDoc,
            int iType_TextOrAnchor, // text or anchor specifier
            String sToLookFor, //
            int iStart,
            int iEnd,
            int whichOneOfInterest, // index num starting at 1
            int compareMode,
            boolean trimMode, // yes trim if true
            boolean fatalIfNotFound,
            boolean confirmUnique) {

        if (!((iType_TextOrAnchor == UtilHTML.HTMLTagType_TEXT) ||
                (iType_TextOrAnchor == UtilHTML.HTMLTagType_OpenA))) {
            throw new java.lang.RuntimeException("UtilHTMLMem.findHTMLTextOrAnchor - invalid mode");
        }


        int whichOneSoFar = 0;
        int iFoundLoc = -1;
        if (iEnd == 0)
            iEnd = memDoc.getlastIndex();

        int iElem = -1;
        for (iElem = iStart; iElem <= iEnd; iElem++) {
            // if text and text matches eg, "Article"
            if (memDoc.isOfTypeMine(iElem, iType_TextOrAnchor)) {
                String foundText = null;
                if (iType_TextOrAnchor == UtilHTML.HTMLTagType_TEXT)
                    foundText = memDoc.getsHTMLText(iElem);
                else
                    foundText = memDoc.getsHTMLAttrsAsAnchorOnly(iElem);
                if (trimMode)
                    foundText = UtilStrings.myTrimFunnySpaces(foundText);
                if (UtilStrings.myCompare(foundText, sToLookFor, compareMode)) {
                    whichOneSoFar++;
                    // if confirming unique or have reached index of interest
                    if (whichOneSoFar == whichOneOfInterest) {

                        if (whichOneOfInterest == 1 && confirmUnique) {

                            int iDocTextOrAnchorLocDupCheck = findHTMLTextOrAnchor(
                                    memDoc, // RECURSE
                                    iType_TextOrAnchor,
                                    sToLookFor,
                                    iElem + 1,
                                    iEnd,
                                    1, // which one of interest
                                    compareMode,
                                    trimMode, // yes trim if true
                                    false, /// fatal if not found
                                    false // confirm unique
                            );
                            if (iDocTextOrAnchorLocDupCheck > -1) {
                                Log.FatalError("findHTMLText : non unique text " + sToLookFor);
                            }
                        }

                        iFoundLoc = iElem;
                        break; // iElement is set
                    } else
                        whichOneSoFar++;
                }
            }  // if ( memDoc.getHTMLType ( i ) == UtilHTML.HTMLTagType_TEXT)
        }  // for (int i = iStart; i <= iEnd ; i++ )

        if (iFoundLoc == -1) {
            if (fatalIfNotFound) {
                Log.FatalError("findText : text not found : by elem " + iElem + ":" + sToLookFor + " in doc : " + memDoc.getURLName());
            }

        }

        return iFoundLoc; // might be -1 if not found

    }  // public static int findHTMLTextOrAnchor

    // ******************************************************************************************
    public static int findHTMLOpenAHREF(
            // BLOCK******************************************************************************************
            HTMLMemDoc memDoc,
            String sToLookFor, // eg, "Article"
            int iStart,
            int iEnd,
            int whichOneOfInterest, // -1 if confirm unique, else index num starting at 1
            int compareMode, // equal, startswith, contains
            boolean trimMode,
            boolean fatalIfNotFound,
            boolean confirmUnique) {

        if (iEnd == 0)
            iEnd = memDoc.getlastIndex();

        //int curStartLoc = iStart;
        int whichOneSoFar = 0;

        for (int iElem = iStart; iElem <= iEnd; iElem++) {
            if (memDoc.isOfTypeMine(iElem, UtilHTML.HTMLTagType_OpenAHref)) {
                // find text block after the last HREF
                int[] endTypes = {UtilHTML.HTMLTagType_CloseA};
                String nextTextBlock = getTextBlock(memDoc, iElem + 1, iEnd, endTypes);

                if (UtilStrings.myCompare(nextTextBlock, sToLookFor, compareMode)) {
                    whichOneSoFar++;
                    if (whichOneSoFar == whichOneOfInterest) {
                        //remove: String hrefOnly= memDoc.getsHTMLAttrsAsHrefOnly ( iElem );

                        if (whichOneOfInterest == 1 && confirmUnique) {
                            int iDocHREFDupCheck = findHTMLOpenAHREF(// RECURSE look for uniqueness
                                    memDoc,
                                    sToLookFor,
                                    iElem + 1, // past the text and </A>
                                    iEnd,
                                    1, // -1 if confirm unique, else index num starting at 1
                                    compareMode,
                                    trimMode,
                                    false, // fatal if not found
                                    false);  // confirm unique

                            if (iDocHREFDupCheck > -1) {
                                Log.FatalError("findHTMLTextHREFForText : non unique HREF text " + sToLookFor);
                            }
                        }
                        return iElem;
                    }

                }  // if ( memDoc.isOfTypeMine ( iElem+1, UtilHTML.HTMLTagType_TEXT ) )
            } // if ( memDoc.isOfTypeMine (iElem, UtilHTML.HTMLTagType_AHref ) )
        } // for (int iElem = iStart ; iElem <= iEnd ; iElem++ )
        return -1; // not found
    } // public static int findHTMLOpenAHREF

    // ******************************************************************************************
    public static int findNextOfType(HTMLMemDoc memDoc,
                                     // BLOCK******************************************************************************************
                                     int type,
                                     int iStart,
                                     int iEnd,
                                     boolean fatalIfNotFound) {
        int[] ints = new int[1];
        ints[0] = type;
        return findNextOfType(memDoc, ints, iStart, iEnd, fatalIfNotFound);
    }


    // ******************************************************************************************
    public static int findNextOfType(
            // BLOCK******************************************************************************************
            HTMLMemDoc memDoc,
            int[] types,
            int iStart,
            int iEnd,
            boolean fatalIfNotFound) {

        if (iEnd == 0 || iEnd == -1)
            iEnd = memDoc.getlastIndex();

        for (int i = iStart; i <= iEnd; i++) {
            if (intInArray(memDoc.getIntHTMLTypeMine(i), types))
                return i;
        }
        if (fatalIfNotFound)
            Log.FatalError("findNextOfType : not found " + memDoc.getURLName());
        return -1;
    }


    // ******************************************************************************************
    public static boolean intInArray(int i, int[] arr)
            // BLOCK******************************************************************************************
    {
        for (int j = 0; j < arr.length; j++) {
            if (i == arr[j])
                return true;
        }
        return false;
    }


    // TODO: define types which will not cause  space to be put between blocks, e,g, see end of argon
    // article - chemical formula
    // ******************************************************************************************
    public static String getTextBlock(HTMLMemDoc memDoc, int iStart, int iEnd, int[] endTypes)
            // BLOCK******************************************************************************************
    {
        StringBuffer sb = new StringBuffer();

        if (iEnd == 0)
            iEnd = memDoc.getlastIndex();

        for (int iElem = iStart; iElem <= iEnd; iElem++) {
            if (memDoc.isOfTypeMine(iElem, UtilHTML.HTMLTagType_TEXT)) {
                sb.append(memDoc.getsHTMLText(iElem) + " ");
            } else if (endTypes != null) {
                if (intInArray(memDoc.getIntHTMLTypeMine(iElem), endTypes)) {
                    break;
                }
            }
        }

        return sb.toString();


    }




    // ******************************************************************************************
    public static int writeToFile(String filename, HTMLMemDoc memDoc)
            // BLOCK******************************************************************************************
    {
        return writeToFile(filename, memDoc, 0, 0);
    }


    // ******************************************************************************************
    public static int writeToFile(String filename, HTMLMemDoc memDoc, int[] elemsOfInterest)
            // BLOCK******************************************************************************************
    {

        try {
            UtilFile.addLineToFileKill(filename,
                    new java.util.Date() + "," +
                    memDoc.getTopicNameSpaces() + "," +
                    memDoc.getURLName() + "\r\n");
            for (int iElem = 0; iElem < elemsOfInterest.length; iElem++) {
                String s = memDoc.getElement(elemsOfInterest[iElem]).summarizeHTMLElemRecord(
                        Integer.toString(iElem) + "/" + Integer.toString(elemsOfInterest[iElem]));
                UtilFile.addLineToFile(filename, s);
            }
            return 0;	 // success

        } catch (Exception e) {
            Log.FatalError("writeToFile ", e);
            return 1;	// error
        }
    } // public static int writeToFile (String filename, HTMLMemDoc memDoc, int iStart, int iEnd )


    // ******************************************************************************************
    public static int writeToFile(String filename, HTMLMemDoc memDoc, int iStart, int iEnd)
            // BLOCK******************************************************************************************
    {

        try {
            UtilFile.addLineToFileKill(filename,
                    new java.util.Date() + "," +
                    memDoc.getTopicNameSpaces() + "," +
                    memDoc.getURLName() + "\r\n");
            if (iEnd == 0)
                iEnd = memDoc.getlastIndex();

            for (int iElem = iStart; iElem <= iEnd; iElem++) {
                String s = memDoc.getElement(iElem).summarizeHTMLElemRecord(Integer.toString(iElem));
                memDoc.getElements()[iElem].getsHTMLTextOrURL();
                UtilFile.addLineToFile(filename, s);
            }
            return 0;	 // success

        } catch (Exception e) {
            Log.FatalError("writeToFile ", e);
            return 1;	// error
        }
    } // public static int writeToFile (String filename, HTMLMemDoc memDoc, int iStart, int iEnd )







}




