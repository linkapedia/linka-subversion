package com.indraweb.workfactory;

public class WorkPolicy 
{
	int numThreadsToRunInParallel = -1;
	int timeAllowedPerThreadBeforeAbort = -1;
			
	public WorkPolicy( int numThreadsToRunInParallel_,
					   int timeAllowedPerThreadBeforeAbort_ )
	{
		numThreadsToRunInParallel		= numThreadsToRunInParallel_;
		timeAllowedPerThreadBeforeAbort = timeAllowedPerThreadBeforeAbort_;
		
	} 
	
	public int getNumThreadsToRunInParallel ()
	{
		return numThreadsToRunInParallel;
	} 
	public int getTimeAllowedPerThreadBeforeAbort ()
	{
		return timeAllowedPerThreadBeforeAbort;
	} 
}
