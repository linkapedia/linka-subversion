package com.indraweb.ir.texttranslate;

import java.io.*;
import java.net.*;
import org.apache.log4j.Logger;

public class MultiLingualTest
{
    private static final Logger log = Logger.getLogger(MultiLingualTest.class);
    /* working babelfish html - to translate any term to German:

        <form action="http://babelfish.altavista.com/babelfish/tr"  method="post" onSubmit="return verifyTEXT()"  name="transform">
        <input type="hidden" name="doit" value="done">
        <input type="hidden" name=tt  value="urltext" >
        <input type="hidden" name="intl" value="1">
        <textarea name="urltext"></textarea>
        <br>
        <select name="lp">
        <option value="en_zh" >English to Chinese</option>
        <option value="en_de" SELECTED>English to German</option>
        </select>&nbsp; <input type="Submit" value="Translate">&nbsp;
        </form>
    */

/*
 issues ? : hidden, textfields, select boxes - setProp vs part of URL - when does it matter ?
*/

    public static void main (String args[]) throws Exception
    {
        /*String s = "nuclear";  // term to translate
        //URL url = new URL ("http://babelfish.altavista.com/babelfish/tr");
        URL url = new URL ("http://babelfish.altavista.com/babelfish/tr?" +
                //"lp=en_de" +
                //"&name=transform"+
                //<input type="hidden" name="doit" value="done">
                //<input type="hidden" name=tt  value="urltext" >
                //<input type="hidden" name="intl" value="1">
                "doit=done" +
                "&tt=urltext" +
                "&intl=1"
        );
        HttpURLConnection httpURLConn = (HttpURLConnection) url.openConnection ();
        httpURLConn.setRequestProperty("Connection", "Keep-Alive");
        httpURLConn.setDoOutput(true);
        httpURLConn.setUseCaches(false);
        httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
        httpURLConn.setRequestProperty("Accept-Language", "en");
        //httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);
        httpURLConn.setDoOutput (true);
        httpURLConn.setDoInput (true);
        //onSubmit="return verifyTEXT()" name="transform"
        httpURLConn.setRequestProperty ("name" , "transform");
        httpURLConn.setRequestProperty ("lp" , "en_de");       // should this be using & ?
        httpURLConn.setRequestMethod("POST");
        //conn.setRequestProperty ("method" , "post");*/

        long lStart = System.currentTimeMillis();

        URL url;
        URLConnection urlConn;
        DataOutputStream printout;
        BufferedReader input;
        url = new URL("http://babelfish.altavista.com/babelfish/tr");
        urlConn = url.openConnection();
        urlConn.setDoInput(true);
        urlConn.setDoOutput(true);
        urlConn.setUseCaches(false);
        urlConn.setRequestProperty
                ("Content-Type", "application/x-www-form-urlencoded");

        // Send POST output.
        printout = new DataOutputStream(urlConn.getOutputStream());

        String content = "doit=" + URLEncoder.encode("done", "UTF-8") +
                "&tt=" + URLEncoder.encode("urltext", "UTF-8") +
                "&intl=1" +
                "&urltext=" + URLEncoder.encode("nuclear", "UTF-8") +
                "&lp=en_de";
        printout.writeBytes(content);
        printout.flush();
        printout.close();
        // Get response data.
        input  = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
        StringBuilder sb = new StringBuilder ();
        String str;
        while (null != ((str = input.readLine()))) {
            sb.append(str);
        }
        input.close();

        String sHTMLResult = sb.toString();
        if (sHTMLResult.toLowerCase ().indexOf ("kern") >= 0)
            log.info("FOUND german term for nuclear");
        else
            log.info("DID NOT FIND german term for nuclear");


        long lEnd = System.currentTimeMillis() - lStart;

        log.debug("Completed in " + lEnd + " ms.");

/*
        httpURLConn.connect ();
        BufferedWriter writer = new BufferedWriter (new OutputStreamWriter (httpURLConn.getOutputStream ()));
        writer.write ("nuclear");
        writer.close ();

        DataInputStream input = new DataInputStream (httpURLConn.getInputStream ());
        String str = null;
        String firstLine = null;

        String sResult = readInStream (input);
        System.out.print (sResult);
        if (sResult.toLowerCase ().indexOf ("kern") >= 0)
            System.out.print ("FOUND german term for nuclear");
        else
            System.out.print ("DID NOT FIND german term for nuclear");
        input.close ();
*/


    }

    private static String readInStream (DataInputStream input) throws Exception
    {
        // Sys out the input stream
        byte[] bArr = new byte[30000];

        StringBuilder sb = new StringBuilder ();
        int iBytesRead = 0;
        while (true)
        {
            bArr = new byte[30000];
            if (( iBytesRead = input.read (bArr) ) <= 0)
                break;

            byte[] bArrTemp = new byte[iBytesRead];
            System.arraycopy(bArr, 0, bArrTemp, 0, iBytesRead);
            String sTemp = new String (bArrTemp);
            sb.append (sTemp);
        }
        return sb.toString ();
    }
}
