package com.indraweb.util;

import com.indraweb.execution.*;

import java.util.*;
import java.io.*;

public class UtilDataTable
{
	private static Hashtable htMissingKnown = new Hashtable();
	
	
	static boolean bTesting = false;
	

	private static boolean bdefault_do_bm_ctd_Etc = false;
	
	// *******************************************************
	static private void tableToFile ( String sFilename, String[][] sArrArrTable )
	// *******************************************************
	{	
		int iNumRows = sArrArrTable.length;
		int iNumCols = sArrArrTable[0].length;
		
		for ( int i = 0; i < iNumRows; i++ )
		{
			int iLocalNumCols = sArrArrTable[i].length;
			if ( iLocalNumCols != iNumCols)
				Log.FatalError ("bad col count");
			StringBuffer sb = new StringBuffer();
				sb.append ( sArrArrTable [ i ] [ 0 ]  );	
			for ( int j = 1; j < iNumCols; j++ )
			{
				sb.append ( "," + sArrArrTable [ i ] [ j ]);	
			}
			if ( i == 0 )
				UtilFile.addLineToFileKill (sFilename, sb.toString() + "\r\n");
			else
				UtilFile.addLineToFile (sFilename, sb.toString()  + "\r\n");
		} 
	} 
	

	// *******************************************************
	private static String[][] concatTablesColWise ( String[][] sArrArr1, String[][] sArrArr2  )  
	// *******************************************************
	{
		return concatTablesColWise ( sArrArr1, sArrArr2, "-9999"  );
		
	}
	// *******************************************************
	private static String[][] concatTablesColWise ( String[][] sArrArr1, String[][] sArrArr2, String spacer )  
	// *******************************************************
	{
		int iNumRows1 = sArrArr1.length;	
		int iNumRows2 = sArrArr2.length;	
		
		int iNumCols1 = sArrArr1[0].length;	
		int iNumCols2 = sArrArr2[0].length;	
		
		if ( iNumRows1 != iNumRows2 ) 
			Log.FatalError ("row nums not match ");
		
		String[][] sArrArrReturn = new String[iNumRows1][iNumCols1+ iNumCols2 + 1];
		
		for ( int iRow = 0; iRow < iNumRows1; iRow++ )
		{
			if ( sArrArr1[iRow].length != iNumCols1 )
				Log.FatalError ("not same # cols 1");
			if ( sArrArr2[iRow].length != iNumCols2 )
				Log.FatalError ("not same # cols 2");
			
			for ( int jcol = 0; jcol < iNumCols1; jcol++ )
			{
				sArrArrReturn[iRow][jcol] = sArrArr1[iRow][jcol];
			}
			if ( spacer == null )
				sArrArrReturn [ iRow ] [ iNumCols1 ] = "-9999";
			else
				sArrArrReturn [ iRow ] [ iNumCols1 ] = spacer;
				
			
			for ( int jcol = 0 ; jcol < iNumCols2; jcol++ )
			{
				sArrArrReturn [ iRow ] [ jcol + iNumCols1 + 1 ] = sArrArr2 [ iRow ] [ jcol ];
			}
			
		} 
		return sArrArrReturn;
	}
	
	// *******************************************************
	private static String[][] concatTablesRowWise ( String[][] sArrArr1, String[][] sArrArr2 )
	// *******************************************************
	{
		int iNumRows1 = sArrArr1.length;	
		int iNumRows2 = sArrArr2.length;	
		
		int iNumCols1 = sArrArr1[0].length;	
		int iNumCols2 = sArrArr2[0].length;	
		
		if ( iNumCols1 != iNumCols2 ) 
			Log.FatalError ("col nums not match ");
		
		String[][] sArrArrReturn = new String [ iNumRows1 + iNumRows2 ] [ iNumCols1 ];

		// get first tables rows
		for ( int iRow = 0; iRow < iNumRows1; iRow++ )
		{
			if ( sArrArr1[iRow].length != iNumCols1 )
				Log.FatalError ("bad col count in row concat 2");
			for ( int jCol = 0; jCol < iNumCols1; jCol++ )
			{
				sArrArrReturn [ iRow ] [ jCol ] = sArrArr1 [ iRow ] [ jCol ];	
			}
		}
			
		// get second tables rows
		for ( int iRow = 0; iRow < iNumRows2; iRow++ )
		{
			if ( sArrArr2[iRow].length != iNumCols2 )
				Log.FatalError ("bad col count in row concat 2" );
			
			for ( int jCol = 0 ; jCol < iNumCols2; jCol++ )
			{
				sArrArrReturn[ iRow + iNumRows1 ] [ jCol ] = sArrArr2 [ iRow ] [ jCol  ];	
			}
		}
			
		return sArrArrReturn;
	}
	
	// *******************************************************
	static private String [][] buildRelative ( String sTabId, 
	// *******************************************************
											   String sProd, 
											   String[][] sArrArrProd, 
											   String[][] sArrArrprod2,
											   String sDesc)
	{
		int iNumRowsProd = sArrArrProd.length;
		int iNumColsProd = sArrArrProd[0].length;
		
		int iNumRowsprod2 = sArrArrprod2.length;
		int iNumColsprod2 = sArrArrprod2[0].length;
		
		if ( iNumRowsProd != iNumRowsprod2 )
			Log.FatalError ("ng 1 ");
		
		if ( iNumColsProd != iNumColsprod2 )
			Log.FatalError ("ng 2 ");
		
		
		
		String[][] sArrArrRel = new String [ iNumRowsprod2 ] [ iNumColsprod2 ];
		
		// get First row group bys
		for ( int i = 0; i < iNumColsprod2; i++ )
		{
			sArrArrRel[0][i] = sArrArrProd [0][i];
		} 
		// get First col dates
		for ( int i = 0; i < iNumRowsprod2; i++ )
		{
			sArrArrRel[i][0] = sArrArrProd [i][0];
		} 
		// get data
		for ( int i = 1; i < iNumRowsprod2; i++ )
		{
			for ( int j = 1; j < iNumColsprod2; j++ )
			{
				sArrArrRel[i][j] = Double.toString (Double.valueOf ( sArrArrProd [i][j]  ).doubleValue() - 
								   Double.valueOf ( sArrArrprod2 [i][j]  ).doubleValue());
			} 
		}
	
		return sArrArrRel;	
	} 
	
	// *******************************************************
	private static String [][] concatColsMultipleTables ( 
	// *******************************************************
		Vector vStringArrArrs, String[] sArrSpacers														 
	)
	{
		String[][] sArrArrAggregator = (String[][]) vStringArrArrs.elementAt(0);
		String [][] temp = null;
		
		int iNumRows = sArrArrAggregator.length;

		for ( int i = 1; i < vStringArrArrs.size(); i++ )
		{
			String[][] next = (String[][]) vStringArrArrs.elementAt(i) ;
			if ( iNumRows != next.length )
				Log.FatalError ("ng 4 ");
			temp = concatTablesColWise	( sArrArrAggregator, next, sArrSpacers[i] );
			sArrArrAggregator = temp;
		} 
		
		return sArrArrAggregator;
	}
}
