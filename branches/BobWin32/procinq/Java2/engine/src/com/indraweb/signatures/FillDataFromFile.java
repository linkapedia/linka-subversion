/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 3, 2004
 * Time: 6:11:36 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.signatures;

import com.iw.classification.SignatureFile;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;
import com.iw.scoring.NodeForScore;
import com.indraweb.util.UtilProfiling;
import com.indraweb.ir.ParseParms;

import java.util.Vector;

public class FillDataFromFile
{
    public static int fillData_FromFile ( int iCorpusID,
                                           boolean bStemVersion_determineWhichFile
//Connection dbc
                                           )
    {
        int iNumNodesAddedReturn = 0;
        boolean bVerbose = false;
// called by a write locked routine
//UtilFile.addLineToFileKill("cCorpusID == null in NodeForScore nodei:/t.t", "START LOAD DS : " + new java.util.Date() + "\r\n");
// NOTE : could multithread the db read of new file and the mem empty of the old file

//Log.logClearcr("cfg_classification_SigCountMin [" + icfg_classification_SigCountMin + "]\r\n" );
//Log.logClear("bAffinityNodeTitlesOnly [" + bAffinityNodeTitlesOnly + "]\r\n" );  // hbk control 2002 11 07

        {
            String sfnameSigWordCountsFile_emptyOrFill = null;
            try
            {

                sfnameSigWordCountsFile_emptyOrFill = SignatureFile.getFileName ( iCorpusID, bStemVersion_determineWhichFile );
// get node range
                long lStart = System.currentTimeMillis ();

// loop thru nodeid, signatureword, SIGNATUREOCCURENCES
// read from the file into mem
                if ( bVerbose )
                {
                    api.Log.Log ( "START fill node sig data struct corpus [" + iCorpusID + "]" );
                }
//Vector vStrNodeSigPhrases_DescAsPerSigFile = new Vector ();

                java.io.BufferedReader in = null;
//if (bVerbose) api.Log.Log ("@#@ bEmptyingIfTrue [" + bEmptyingIfTrue + "] sfnameSigWordCountsFile_emptyOrFill [" + sfnameSigWordCountsFile_emptyOrFill + "]");
                in = new java.io.BufferedReader ( new java.io.FileReader ( sfnameSigWordCountsFile_emptyOrFill ) );

                int iLineNum1Based = 0;
// *********************************************************
// DEBUG : CAP #NODES PER SIG WORD FILE
// *********************************************************

// ****** START FILE READ LOOP
// ****** START FILE READ LOOP
// ****** START FILE READ LOOP
// assume no more than 20 words in a phrase - avoids mem issue of newing a str[] every time,
// instead keep reusing sArrTermWordsToForAffinityAssociation
//String[] sArrTermWordsToForAffinityAssociation = new String[iParm_NumSigTermsUsedInVectorScore];

                Vector vStrPerNodeDBSigWords = new Vector ();
                Vector vIntPerNodeDBSigCounts = new Vector ();
                Vector[] vArrVUnder_ArrThesaurusList = null;
                boolean bFoundCloseString = false;

                int iTitleTermCounterPerNode = 0;
                while ( in.ready () )
                {
                    iLineNum1Based++;
                    String sFileLine = in.readLine ();  // mem optimization opportunity : tokenize - fewer stringnew's ?
//                    if ( bVerbose2  )
//System.out.println(" in data struct loader : processing line [" + sFileLine + "] line [" + iLineNum1Based +"]");	// hbk control 2002 11 07
                    if ( sFileLine == null || sFileLine.trim ().equals ( "" ) )
                    {
                        com.indraweb.util.Log.log ( "null line in sig file [" + sfnameSigWordCountsFile_emptyOrFill + "] line [" + iLineNum1Based + "]" );
                        continue;
                    }

                    int iLineNumOneBasedThisNode = 0;
// *********************************************************
// *********************************************************
// - a regular nodeid/word/count/ lines - non title word
// *********************************************************

                    if ( !sFileLine.startsWith ( "\t" ) ) // REGULAR OR THES TERM FILE LINE
                    {   // size cap

                        String[] sArrLineTabSplit = sFileLine.split ( "\t" );
// adaptation	5	tn	hy
                        String sTerm = sArrLineTabSplit[0];
                        int iTermCount = Integer.parseInt ( sArrLineTabSplit[1] );
                        boolean bIndicatorTitleTerm = sArrLineTabSplit[2].equals ( "hy" );
// regular term
// don't count thes terms in the counter
                        if ( bIndicatorTitleTerm )
                        {
                            iTitleTermCounterPerNode++;
                        }

// regular or thesaurus term now
                        vStrPerNodeDBSigWords.addElement ( sTerm );
                        vIntPerNodeDBSigCounts.addElement ( new Integer ( iTermCount ) );

                    } // else CLOSE A NODE - CREATE NFS
                    else if ( !sFileLine.equals ( SignatureFile.CLOSESTRING ) )
                    {
                        String[] sArrLineTabSplit = sFileLine.split ( "\t" );

                        long lNodeID = Integer.parseInt ( sArrLineTabSplit[1] );
                        int iNodeSize = Integer.parseInt ( sArrLineTabSplit[2] );
                        int lNodeIDParent = Integer.parseInt ( sArrLineTabSplit[3] );
                        String sNodeTitle = sArrLineTabSplit[4];


                        iLineNumOneBasedThisNode++;

// at this point - if supporting stemmed as well - not new NFS - but find existing in case
// other stemming side is already loaded

                        iNumNodesAddedReturn++;
// copy title plus regular sigs into final arrays for NFS
                        int iFinalSize = vStrPerNodeDBSigWords.size ();
                        String[] sArrFinalSigsFromFile = new String[iFinalSize];
                        vStrPerNodeDBSigWords.copyInto ( sArrFinalSigsFromFile );
                        int[] iArrFinalSigsFromFile = new int[vStrPerNodeDBSigWords.size ()];
                        for ( int i = 0; i < iFinalSize; i++ )
                            iArrFinalSigsFromFile[i] = ( ( Integer ) vIntPerNodeDBSigCounts.elementAt ( i ) ).intValue ();

                        NodeForScore nfsNew = new NodeForScore
                                (
                                        iCorpusID,
                                        lNodeID,
                                        lNodeIDParent,
                                        iNodeSize, //5
                                        sArrFinalSigsFromFile,
                                        iArrFinalSigsFromFile,
                                        bStemVersion_determineWhichFile,
//true, // bNodeTitleAlreadyInList terms already in the set,
                                        iTitleTermCounterPerNode,
                                        sNodeTitle,
                                        ParseParms.getDelimitersAll_Default()
                                );
                        Data_NodeIDsTo_NodeForScore.loadDataStructIndexingData_oneNFS ( nfsNew );

                        iTitleTermCounterPerNode = 0;

//htNodeIDToNodeForScore.put ( INodeID, nfsNew );

                        vStrPerNodeDBSigWords = new Vector ();
                        vIntPerNodeDBSigCounts = new Vector ();

                    }
                    else if ( sFileLine.equals ( SignatureFile.CLOSESTRING ) )
                    {
                        bFoundCloseString = true;
                        break;
                    }


//vStrNodeSigPhrases_DescAsPerSigFile.clear ();

                } // while true ( file line reading )

// ****** END FILE READ LOOP

                in.close ();

                if ( !bFoundCloseString )
                {
                    api.Log.LogError ( "sig file for corpus [" + iCorpusID + "] is corrupted, delete and rerun." );
                    throw new Exception ( "sig file for corpus [" + iCorpusID + "] is corrupted, delete and rerun." );
                }


                api.Log.Log ( "LOADED corpus from file [" +
                        iCorpusID +
                        "] # nodes [" + iNumNodesAddedReturn + "] " +
                        "new mem node count [" + Data_NodeIDsTo_NodeForScore.getNumNodes () + "] ms [" +
                        UtilProfiling.elapsedTimeMillis ( lStart ) + "] " );


            }
            catch ( Exception e )
            {
                api.Log.LogError ( "error loading sig file [" + sfnameSigWordCountsFile_emptyOrFill + "] continuing ", e );

            }
           // Data_NodeIDsTo_NodeForScore.printDataStructNodeToNFS ( false, null, true );

            return iNumNodesAddedReturn;
        }


    }

}
