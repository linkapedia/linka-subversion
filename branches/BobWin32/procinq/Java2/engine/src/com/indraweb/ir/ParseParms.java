/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jun 24, 2004
 * Time: 6:26:03 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.ir;

import api.APIProps;

public class ParseParms {

    private static String sDelimiters_default = "<>?!/, %&+.:;#*?\u00A0?\"\t\n\r\u001A{}[]()|";
    private static boolean bIncludeNumbers_default = true;
    private static int iNumBytesToTake_default = -1;
    private String sDelimiters = null;
    private boolean bIncludeNumbers = bIncludeNumbers_default;
    private int iNumBytesToTake = iNumBytesToTake_default;

    public ParseParms(String sDelimiters_,
            boolean bIncludeNumbers_,
            int iNumBytesToTake_) {
        if (sDelimiters_ == null) {
            sDelimiters = sDelimiters_default;
        } else {
            sDelimiters = sDelimiters_;
        }

        bIncludeNumbers = bIncludeNumbers_;
        iNumBytesToTake = iNumBytesToTake_;
    }

    public ParseParms(APIProps props) {
        try {
            sDelimiters = (String) props.get("Delimiters", "" + sDelimiters_default);
            bIncludeNumbers = props.getbool("IncludeNumbers", "" + bIncludeNumbers_default);
            iNumBytesToTake = props.getint("NumBytesToTake", "" + iNumBytesToTake_default);
        } catch (Exception e) {
            api.Log.LogError("unable to construct parseparms", e);
        }
    }

    public static String getDelimitersAll_Default() {
        return sDelimiters_default;
    }

    public static boolean getIncludeNumbers_default() {
        return bIncludeNumbers_default;
    }

    public static int getNumBytesToTake_default() {
        return iNumBytesToTake_default;
    }

    public String getDelimiters() {
        return sDelimiters;
    }

    public int getNumBytesToTake() {
        return iNumBytesToTake;
    }

    public boolean getIncludeNumbers() {
        return bIncludeNumbers;
    }

    @Override
    public String toString() {
        return "ParseParms{" + "sDelimiters=" + sDelimiters + ", bIncludeNumbers=" + bIncludeNumbers + ", iNumBytesToTake=" + iNumBytesToTake + '}';
    }
    
}
