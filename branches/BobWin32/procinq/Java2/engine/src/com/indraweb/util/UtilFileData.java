package com.indraweb.util;

import java.util.*;
import java.io.*;
import com.indraweb.database.*;

public class UtilFileData
{
	
	/**
	 * data structure for columnated data in format : 
	 * documentID\tWordID\tWord
	 * 123\t321\tTIME
	 * 124\t322\tLOVE
	 * 125\t323\tPEACE
	 * 126\t324\tWAR
	 * 
	 * types are the format types "int", "java.lang.String", etc.
	 * 
	 */
	
	String sFName = null;
	String sDelim = null;
	int iNumRowsLines = -1;
	
	String[][] sArrArr_rowColData = null;
	String [] sArrColNames = null;
	String [] sArrTypes = null;
	String [] sArrQuotesForDBInserts = null;

	
	
	
	
	
	
	//	Expr1000	as_of_Date	security_Code	account_code
	
	
	


	// ***************************************************
	public String getDataValue ( int iRow, int iCol ) 
	// ***************************************************
	{
		return sArrArr_rowColData [ iRow ] [ iCol ];
	}
	
	// ***************************************************
	public String[] getDataRow ( int iRow ) 
	// ***************************************************
	{
		return sArrArr_rowColData [ iRow ];
	}
	
	// ***************************************************
	public String getColName ( int iCol ) 
	// ***************************************************
	{
		return sArrColNames [ iCol ];
	}

	// ***************************************************
	public String getColQuoteString ( int iCol ) 
	// ***************************************************
	{
		if ( sArrQuotesForDBInserts == null )
		{
			sArrQuotesForDBInserts = new String[sArrTypes.length];

			for ( int i = 0; i < sArrTypes.length; i++ )
			{
				if ( sArrTypes[i].equals ("java.lang.String") )
				{
					sArrQuotesForDBInserts[i] = "'";
				}
				else if ( sArrTypes[i].equals ("date") )
				{
					sArrQuotesForDBInserts[i] = "#";
				}
				else if  ( 
						  sArrTypes[i].equals ("int") ||
						  sArrTypes[i].equals ("java.lang.Double") ||
						  sArrTypes[i].equals ("java.lang.Integer") ||
						  sArrTypes[i].equals ("java.lang.Float") ||
						  sArrTypes[i].equals ("int") ||
						  sArrTypes[i].equals ("double") ||
						  sArrTypes[i].equals ("float"))
				{
					sArrQuotesForDBInserts[i] = "";
				}
				else
					Log.FatalError ("unknown type [" + sArrTypes [i] + "]\r\n");
			}
		}
		return sArrQuotesForDBInserts [ iCol ];
	}
	
	// ***************************************************
	public UtilFileData ( String sFName_, String sDelim_, String[] sArrTypes_, String x ) 
	// ***************************************************
	{
		sFName = sFName_;
		sDelim = sDelim_;
		sArrTypes = sArrTypes_;
		
	}
	
	// ***************************************************
	public int confirmFormat ( )
	// ***************************************************
	{	// just verify corect # delims each line
		return loadData ( false, -1 );
	}
	
	// ***************************************************
	public int getNumRows ( )
	// ***************************************************
	{	
		return sArrArr_rowColData.length;
	}
	
	// ***************************************************
	public int loadData (boolean bActuallyFillDataArray, int iMax )
	// ***************************************************
	{	
		Enumeration e = UtilFile.enumFileLines ( sFName );
		
		int iCorrectNumDelims = -1;
		int iLine = 0;
		try 
		{
			while ( e.hasMoreElements() )
			{
				String sLine = (String) e.nextElement();
				if ( iMax > 0 && iLine >= iMax )
				{
					return 0;
				}
					
				int iNumDelims = UtilStrings.countNumOccurrencesOfStringInString ( sLine, sDelim );
				Log.log ("line [" + iLine + " has :"  + iNumDelims + "] [" + sLine + "\r\n");
				// line 1 - get col names, set expected delim #
				if ( iCorrectNumDelims == -1 )
				{
					iCorrectNumDelims = iNumDelims;
					sArrColNames = new String[ iCorrectNumDelims + 1 ];
					sArrColNames = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption
						( sLine, sDelim , false, false, true, false, -1  );
					if ( sArrArr_rowColData  == null )
					{
						iNumRowsLines = getLineCount ();
						if ( bActuallyFillDataArray )
						{
							// iNumRowsLines-1 due to header
							sArrArr_rowColData = new String [iNumRowsLines-1] [iCorrectNumDelims+1];
						}
					} 
				}
				else
				{
					if ( iCorrectNumDelims != iNumDelims )
					{
						clsUtils.waitForKeyboardInput ("file [" + sFName + "] line [" + iLine + 
							"] 0-based has incorrect # delims [" + iCorrectNumDelims + "] vs [" + iNumDelims + "]\r\n" );
						return 1;

					}
					else
					{
						// line -1 due to header
						if ( bActuallyFillDataArray )
						{
							sArrArr_rowColData[iLine-1] = 
								confirmSchemaComplianceGetRowData ( sLine, sArrTypes , sDelim, iCorrectNumDelims);		
						}
					}
				}
				iLine++;
				
			} // while ( e.hasMoreElements() )
		}
		catch ( Exception e2 )
		{
			Log.FatalError ("at line [" + iLine + "]" , e2 );
		}

		return 0;
	}
	
	// ***************************************************
	private String[] confirmSchemaComplianceGetRowData ( String sLine, String[] sArrTypes, String sDelim, int iNumDelims )	
	// ***************************************************
	{
		String[] sArrDataElems = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption
			( sLine, sDelim, false,false, true , false, -1 );
		
		if ( sArrDataElems.length != iNumDelims+1 )
			Log.FatalError ("line data #elems [" + sArrDataElems.length + "] and #delims [" + iNumDelims + "] not +1 away [" + sLine + "]\r\n");
		
		for ( int i = 0; i < sArrDataElems.length; i++ )
		{
			String sType = sArrTypes[i].trim();
			String sData = sArrDataElems[i];
			
			if ( sType.equals ("int") || sType.equals ("java.lang.Integer") )
			{
				int iTest = Integer.parseInt ( sData );
			}
			else if ( sType.equals ("double") || sType.equals ("java.lang.Double") )
			{	
				Double DTest = new Double ( sData );
			}
			else if ( sType.equals ("java.lang.String") ||
					  sType.equals ("date") )
			{	
				// Double DTest = new Double ( sData );
			}
			else
				Log.FatalError("unknown type in UtilFileData.java [" + sType+ "]\r\n");
			
		} 
		return sArrDataElems;
	}
	
	// ***************************************************
	public int getLineCount()
	// ***************************************************
	{
		Enumeration e = UtilFile.enumFileLines ( sFName );
		
		int iLine = 0;
		while ( e.hasMoreElements() )
		{
			Object o = e.nextElement();
			iLine++;
		}
		return iLine;
	}
	
	// ***************************************************
	public static int getLineCount( String sFileName )
	// ***************************************************
	{
		Enumeration e = UtilFile.enumFileLines ( sFileName );
		
		int iLine = 0;
		while ( e.hasMoreElements() )
		{
			Object o = e.nextElement();
			iLine++;
		}
		return iLine;
	}
	
	
	
	// ***************************************************
	public static int confirmColumnCounts ( String sFileName, String sDelim, int iMax ) 
	// ***************************************************
	{
		Enumeration e = UtilFile.enumFileLines ( sFileName );
		
		int iCorrectNumDelims = -1;
		int iLine = 0;
		try 
		{
			while ( e.hasMoreElements() )
			{
				String sLine = (String) e.nextElement();
				if ( iMax > 0 && iLine >= iMax )
				{
return iCorrectNumDelims;
				}
					
				int iNumDelims = UtilStrings.countNumOccurrencesOfStringInString ( sLine, sDelim );
				//Log.log ("line [" + iLine + " has :"  + iNumDelims + "] [" + sLine + "\r\n");
				// line 1 - get col names, set expected delim #
				if ( iCorrectNumDelims == -1 )
				{
					iCorrectNumDelims = iNumDelims;
				}
				else
				{
					if ( iCorrectNumDelims != iNumDelims )
					{
						Log.log("file [" + sFileName + "] line [" + iLine +
							"] 0-based has incorrect # delims correct [" + iCorrectNumDelims + "] vs [" + iNumDelims + "]\r\n", false );
						return -1;

					}
				}
				iLine++;
				
			} // while ( e.hasMoreElements() )
		}
		catch ( Exception e2 )
		{
			Log.FatalError ("at line [" + iLine + "]" , e2 );
		}

		return iCorrectNumDelims;
		
	} 
	
	// ***************************************************
	public static void AddColumnFixedValue (	String sFileSourceNoPath, 
												String sHeader, 
												String sFixedValue, 
												String sDelim , 
												String sFolder, 
												String sTempFileName )	
		throws Exception
	// ***************************************************
			   
			//UtilFileData.AddColumnFixedValue ( sFilesToMod[i], 
											   //"AS_OF_DATE", 
											   //sFileDates[i], 
											   //"," , 
											   //sFolder,
											   //"temp22.txt");
	
			   
	{
		Enumeration e = UtilFile.enumFileLines ( sFolder + "/" +  sFileSourceNoPath );
		
		int iCorrectNumDelims = -1;
		PrintWriter tempfile = new PrintWriter(new BufferedWriter(new FileWriter(sFolder + "/" + sTempFileName, true)))	;	
		int i = 0;
		while ( e.hasMoreElements() )
		{
			String sLine = (String) e.nextElement();
			if ( i == 0 )
				tempfile.write (sLine + sDelim + sHeader  + "\r\n");
			else
				tempfile.write (sLine + sDelim + sFixedValue  + "\r\n");
				
				
			i++;
				
		} // while ( e.hasMoreElements() )
		tempfile.close();
		
		UtilFile.deleteFile (sFolder +"/" + sFileSourceNoPath ) ;
		UtilFile.renameFile ( sFolder + "/" + sTempFileName , sFolder +"/" + sFileSourceNoPath ) ;
		
	} 
	
	
	
	
	
	// ***************************************************
	public static void project (	
									String sFileSourceNoPath, 
									String sTargetFileNameFullPath, 
									String sDelim , 
									String sFolder, 
									int[] iArrProjectIndexes,
									int iNumColsMandated)	
		throws Exception
	// ***************************************************
			   
	{
		Enumeration e = UtilFile.enumFileLines ( sFolder + "/" +  sFileSourceNoPath );
		
		int iCorrectNumDelims = -1;
		PrintWriter tempfile = new PrintWriter(new BufferedWriter(new FileWriter( sTargetFileNameFullPath, true)))	;	
		int i = 0;
		while ( e.hasMoreElements() )
		{
			String sLine = (String) e.nextElement();
			String[] sArrSource = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption
							   ( sLine,sDelim, false );
			if ( iNumColsMandated != sArrSource.length) 
				Log.FatalError (sFileSourceNoPath + " has wrong # elems line [" + i + "] 0 based\r\n");
			
			String[] sArrProjected = (String[]) UtilSets.projectArray ( sArrSource, iArrProjectIndexes );
			
			i++;
				
		} // while ( e.hasMoreElements() )
		tempfile.close();
		
		
	} 
	
	
	
	
	
	
	
	
	
	
}
