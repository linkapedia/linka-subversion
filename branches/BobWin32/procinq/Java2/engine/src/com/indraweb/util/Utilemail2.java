package com.indraweb.util;

import java.io.*;
import java.net.*;
import com.indraweb.execution.*;

public class Utilemail2
{
	
	private static boolean bEmailWorking = true;
	// ****************************************
	static public synchronized void sendMail (
	// ****************************************
												String sEmailAddressTo,
												String sEmailAddressFrom,
												String sSubject,
												String sBodyText,
												String sSmtpServer
											)
		throws Exception
	{
		
		//if ( Session.cfg.getPropBool("EmailsTotallyWillNotBeSent") )
//			return;

		/*
			send(SOCK, "To: $To\r\n", 0);
		    send(SOCK, "From: $From\r\n", 0);
		    send(SOCK, "Reply-To: $From\r\n", 0);
		    send(SOCK, "X-Mailer: TribeSoftware.com\r\n", 0);
		    send(SOCK, "Subject: $Subject\r\n\r\n", 0);
		*/
		
		String sAlertTime = UtilStrings.getStrMinusTail ( new java.util.Date().toString(), 8);
		String sHeader = "";
		sHeader += "Date/time mail originated, source machine time	: "  + sAlertTime + "\r\n";
		
		sBodyText = sHeader + "\r\n" + 
					"\r\nThis is a client side alert from the Indraweb monitoring and alert system \r\n\r\n"  +
					"Machine [" + Session.cfg.getProp ("MachineName") + "]\r\n"  +
					"Time of alert [" + sAlertTime + "]\r\n\r\n"  + 
					"Message Below =====================================\r\n\r\n" + 
					"SUBJECT: [" + sSubject + "]\r\n\r\n" + sBodyText + 
					"\r\n\r\n===========================================\r\n\r\n" + 
					"NetReach : 215-283-2300 extension 400 \r\n"  +
					"MP home 610-265-8038, cell 610-322-3132, mpuscar@hotmail.com\r\n"  +
					"HK home 781-861-8202, cell 781-405-4265\r\n"  ;
		
		/*
		FROM: Judy <judy@dcnw.com>
		DATE: Sat 22 Jun 1996 12:11:11 -0400
		SUBJECT: Basic SMTP message
		FROM: Sat 22 Jun 1996 12:11:11 -0400
		DATE: Sat 22 Jun 1996 12:11:11 -0400
		*/
		
		if ( !bEmailWorking )
			Log.log("can't sent java email subject [" + sSubject + "] body [" + sBodyText + "]" );
		else
		{
		
			try 
			{ 
				Socket s1 = new Socket ( sSmtpServer,25 ); 
				InputStream is1=s1.getInputStream(); 
                                                                BufferedReader br1 = new BufferedReader(new InputStreamReader(is1));
				OutputStream os1=s1.getOutputStream(); 
				DataOutputStream dos1=new DataOutputStream(os1); 

				if (bCheckResponse(br1.readLine(),"220")) 
					ending(s1); 

				WriteLine(dos1,"helo host.com", true); 
				if (bCheckResponse(br1.readLine(),"250")) 
					ending(s1); 

				// mail from: <info@indraweb.com>
				WriteLine(dos1,"mail from: <"+sEmailAddressFrom+">", true); 
				if (bCheckResponse(br1.readLine(),"250")) 
					ending(s1); 

				WriteLine(dos1,"rcpt to:<"+sEmailAddressTo+">", true); 
				if (bCheckResponse(br1.readLine(),"250")) 
					ending(s1); 

				WriteLine(dos1,"data", true); 
				if (bCheckResponse(br1.readLine(),"354")) 
					ending(s1); 

				WriteLine(dos1,"To: "+sEmailAddressTo, true); 
				WriteLine(dos1,"Subject : " + sAlertTime + ":" + Session.cfg.getProp ("MachineName") + ":" + sSubject + "\n", true); 

				WriteLine(dos1,sBodyText, true); 
				WriteLine(dos1,"\r\n", true); 
				WriteLine(dos1,".", true); 
				WriteLine(dos1,"\r\n", true); 
				if (bCheckResponse(br1.readLine(),"250")) 
					ending(s1); 

				WriteLine(dos1,"quit", true); 
				s1.close(); 
			} 
			catch (Exception e) 
			{ 
				bEmailWorking = false;
				Log.NonFatalError("can't sent java email subject [" + sSubject + "] body [" + sBodyText + "]", e );
			} 
		} // else if email working 
		
	}
	
    
	private static void ending(Socket s1) 
		throws Exception
    { 
        try 
        { 
			Log.NonFatalError ("Email not sent\r\n");
            s1.close(); 
            //stop(); 
        } 
        catch (IOException e) 
		{
			Log.NonFatalError("error 1 in java email ", e );
		} 
    } 
	

    private static void WriteLine(DataOutputStream dos1,String s1, boolean bWriteLastBackslashN) 
		throws Exception
    { 
        try 
        { 
            for (int i=0;i<s1.length();i++) 
                dos1.write(s1.charAt(i)); 
			if ( bWriteLastBackslashN )
				dos1.write('\n'); 
            dos1.flush(); 
        } 
        catch (Exception e) 
        { 
			Log.log ("only logging Exception in WriteLine rethrowing \r\n");
			throw e;
        } 
    } 

    private static boolean bCheckResponse(String sResp, String sVal) 
    { 
        if (sResp.indexOf(sVal)!=0) 
        { 
			Log.log ("not got needed response wanted in java mail \r\n");
            return true; 
        } 
        return false; 
    } 

}


/*
LISTING 2.3 INTERACTIVE SMTP SESSION 
 
220 chora.sakes.com Microsoft Exchange Internet Mail Connector ready
HELO pop3.dcnw.com
250 OK
MAIL FROM: Judy <judy@dcnw.com>
250 OK - mail from Judy <judy@dcnw.com>
RCPT TO: Spyros <spyros@patmos.sakes.com>
250 OK - Recipient Spyros <spyros@patmos.sakes.com>
DATA
354 Send data. End with CRLF.CRLF
TO: Spyros Sakellariadis <spyros@patmos.sakes.com>
FROM: Judy <judy@dcnw.com>
DATE: Sat 22 Jun 1996 12:11:11 -0400
SUBJECT: Basic SMTP message
X-Info: Evaluation version at pop3.dcnw.com
Message-Id: <18143885500034@dcnw.com>

*/