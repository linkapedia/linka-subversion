package com.indraweb.util;

public class UtilProfiling
{
	public static long elapsedTimeMillis ( long sinceTime )
	{
		long t2 = System.currentTimeMillis() - sinceTime;
		return t2;
	}
	
	public static double elapsedTimeHours ( long sinceTime )
	{
		return ( (double) ( System.currentTimeMillis() - sinceTime ) / (double) ( 1000 * 3600 ) ) ;
	}
	
	
	public static double elapsedTimeSeconds ( long lTStart )
	{
		long lTNow = System.currentTimeMillis();	
		return ( ((double) lTNow - (double) lTStart ) / 1000 ) ;
	} 
	
	public static double elapsedTimeMinutes ( long lTStart )
	{
		long lTNow = System.currentTimeMillis();	
		return ( ((double) lTNow - (double) lTStart ) /  ( (double) 1000 * (double) 60 ) ) ;
	} 
	
	public static String sElapsedTimeSeconds ( long lTStart )
	{
		return UtilStrings.numFormatDouble ( elapsedTimeSeconds ( lTStart ), 2 );
	} 
	
	public static long resetTimeStart ( long lTStart, long lResetPeriod )
	{
		if ( System.currentTimeMillis() - lTStart > lResetPeriod )
		{
			return System.currentTimeMillis();
		} 
		else
			return lTStart;
		
	} 
	
	public static double rateSincePerSecond ( long lTStart, int count )
	{
		long lTNow = System.currentTimeMillis();	
		double dSecondsSince = ((double) lTNow - (double) lTStart ) / (double) 1000  ;
		
		double dRate = count / dSecondsSince;
		return dRate;
	} 
	
	public static double rateSincePerMinute ( long lTStart, int count )
	{
		long lTNow = System.currentTimeMillis();	
		double dSecondsSince = ((double) lTNow - (double) lTStart ) / ((double) 1000  * (double) 60)  ;
		
		double dRate = count / dSecondsSince;
		return dRate;
	} 
	
	public static String sRateSince ( long lTStart, int count )
	{
		return UtilStrings.numFormatDouble ( rateSincePerSecond ( lTStart, count ), 2 );
	} 
	
	public static double convertMSToMinutes ( long lTMSDuration )
	{
		double dMinutes = ((double) lTMSDuration / (double) 1000 ) / ( double) 60;
		return dMinutes;
	} 
	
	public static double convertMSToHours ( long lTMSDuration )
	{
		double dMinutes = ((double) lTMSDuration / (double) 1000 ) / (( double) 60 *( double) 60 ) ;
		return dMinutes;
	} 
	
}
