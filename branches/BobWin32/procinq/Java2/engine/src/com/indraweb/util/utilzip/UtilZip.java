package com.indraweb.util.utilzip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.InputStream;

import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;
import java.util.Enumeration;
import java.util.Vector;

import com.indraweb.util.*;

import java.util.Date;

public class UtilZip 
{
	// from http://www.javaworld.com/javaworld/jw-11-1998/jw-11-howto.html
	// input parm OutputSteam osStream e.g., from OutputSteam osStream
		// FileOutputStream fos = new FileOutputStream ( "c:/temp/temp.zip");

	public static void test ()
		throws Exception
	{
		zipFiles ( "C:/ausr/hkon/IndraWeb/lycos", "C:/temp/temp/temp/temp/temphk.zip", -1, null, null);
		//zipFiles ( "C:/Documents and Settings/IndraWeb/IndraHome/CorpusSource/worldbook", "C:/temp/temp/temp/temp/temphk.zip", -1);
		unzipFile ( "C:/temp/temp/temp/temp/temphk.zip", "C:/temp/temp/temp/temp");
		
	} 

	
	// ***********************************************
	// UNZIP 
	// ***********************************************
	
	
	// ***********************************************
	public static int zipFiles ( 
	// ***********************************************
								  String sBaseInputPath, 
								  String sFileNameZipOutput,
								  int iMaxNumFilesToZip, 
								  String sConstrainContains,
								  String sConstrainEndswith)
		throws Exception
	{
		FileOutputStream fos = new FileOutputStream ( sFileNameZipOutput );
		int iNumZipped = zipFiles ( sBaseInputPath, fos , iMaxNumFilesToZip, sConstrainContains, sConstrainEndswith );
		fos.close();
		return iNumZipped;
	}
	
	// ***********************************************
	public static int zipFiles	( 
	// ***********************************************
									String sBaseInputPath, 
									OutputStream osStream ,
									int iMaxNumFilesToZip,
								  String sConstrainContains,
								  String sConstrainEndswith
								 )
		throws Exception
	{
		ZipOutputStream zipoutputstream = new ZipOutputStream
				(osStream);
		//(System.out);

		// Select your choice of stored (not compressed) or
		//   deflated (compressed).

		zipoutputstream.setMethod(ZipOutputStream.DEFLATED);
		
		Vector vsFileNames = new Vector();
							
		com.indraweb.util.UtilFileEnumerator.enumerateFilesInFolder_Recursive 
			(vsFileNames, sBaseInputPath,  iMaxNumFilesToZip, sConstrainContains, sConstrainEndswith);
		
		Enumeration esFileNames	= vsFileNames.elements();

		int iNumZipped = 0;
		while ( esFileNames.hasMoreElements() )
		{
			String sFileNameNextToZip = (String) esFileNames.nextElement();
			File file = new File(sFileNameNextToZip);

			//System.out.println ("start zipping : " + sFileNameNextToZip );
			byte [] rgb = new byte [1000];

			int n;

			FileInputStream fileinputstream;

			// Calculate the CRC-32 value.  This isn't strictly necessary
			//   for deflated entries, but it doesn't hurt.

			CRC32 crc32 = new CRC32();

			fileinputstream = new FileInputStream(file);

			while ((n = fileinputstream.read(rgb)) > -1)
			{
				crc32.update(rgb, 0, n);
			}

			fileinputstream.close();

			// Create a zip entry.

			String sFileNameOnlyNoPath = sFileNameNextToZip.substring ( sBaseInputPath.length() + 1 );
			ZipEntry zipentry = new ZipEntry ( sFileNameOnlyNoPath);
			//ZipEntry zipentry = new ZipEntry(rgstring[i]);

			zipentry.setSize(file.length());
			zipentry.setTime(file.lastModified());
			zipentry.setCrc(crc32.getValue());

			// Add the zip entry and associated data.

			zipoutputstream.putNextEntry(zipentry);

			fileinputstream = new FileInputStream(file);

			while ((n = fileinputstream.read(rgb)) > -1)
			{
				zipoutputstream.write(rgb, 0, n);
			}

			fileinputstream.close();

			zipoutputstream.closeEntry();
			//com.indraweb.util.Log.log (iFileIndex + ". completed zipping : " + sFileNameNextToZip +"\r\n");
			iNumZipped++;
		} // files loop 

		zipoutputstream.close();
		return iNumZipped;
	}	// public static void zipFiles ( String[] rgstring )
	

	// ***********************************************
	public static void zipFile ( 
	// ***********************************************
								  String sPathToZip,
								  String sFileToZip,
								  String sFileNameZipOutput)
		throws Exception
	{
		FileOutputStream fos = new FileOutputStream ( sFileNameZipOutput );
		zipFile ( sPathToZip, sFileToZip, fos );
		fos.close();
	}

	// ***********************************************
	public static void zipFile	( 
	// ***********************************************
								String sPathToZip, 
								String sFileToZip, 
								OutputStream osStream)
		throws Exception
	{
		ZipOutputStream zipoutputstream = new ZipOutputStream
				(osStream);
		zipoutputstream.setMethod(ZipOutputStream.DEFLATED);
		
		File file = new File(sPathToZip);

		byte [] rgb = new byte [1000];
		int n;

		FileInputStream fileinputstream;
		CRC32 crc32 = new CRC32();
		fileinputstream = new FileInputStream(file);

		while ((n = fileinputstream.read(rgb)) > -1) {
			crc32.update(rgb, 0, n);
		}

		fileinputstream.close();

		// Create a zip entry.
		ZipEntry zipentry = new ZipEntry (sFileToZip);

		zipentry.setSize(file.length());
		zipentry.setTime(file.lastModified());
		zipentry.setCrc(crc32.getValue());

		// Add the zip entry and associated data.

		zipoutputstream.putNextEntry(zipentry);
		fileinputstream = new FileInputStream(file);

		while ((n = fileinputstream.read(rgb)) > -1) {
			zipoutputstream.write(rgb, 0, n);
		}

		fileinputstream.close();
		zipoutputstream.closeEntry();
		zipoutputstream.close();
	}	// public static void zipFile ( String[] rgstring )


	// ***********************************************
	// UNZIP 
	// ***********************************************
	
	// unzip the stream with specific readings of data as encoded above in zipFiles
	// ***********************************************
	public static void unzipFile (  
	// ***********************************************
									String sFileNameZipFile,
									String sTargetPath
									)
		throws Exception
	{
		FileInputStream fis = new FileInputStream (sFileNameZipFile);
		unzipStream ( sTargetPath, fis );	
	}

	// ***********************************************
	public static void unzipStream (  
	// ***********************************************
									String sTargetPath, 
									InputStream isStream
									)
		throws Exception
	{
		
		ZipInputStream zipinputstream = new ZipInputStream(isStream);

		ZipEntry zipentry;
		while (true)
		{
			zipentry = zipinputstream.getNextEntry();
			if ( zipentry == null ) 
				break;
			
			String sFQRelName = zipentry.getName();
			String sNameOnly;
			if ( UtilStrings.getStrContains (sFQRelName, "/"))
				sNameOnly = UtilStrings.getAllAfterLastOfThis (sFQRelName, "/");
			else
				sNameOnly = sFQRelName;
			
			String sPathRelWithNoFileName = sFQRelName.substring (0, sFQRelName.length() - sNameOnly.length() );

			if ( !sPathRelWithNoFileName.equals ("") && !UtilFile.bDirExists (sTargetPath+"/"+sPathRelWithNoFileName ))
			{
				UtilFile.createFolder (sTargetPath+"/"+sPathRelWithNoFileName);
				//	throw new Exception("can't create path [" + fileTargetFolderPath + "]");
			} 
			/*
			Vector vPathElements = UtilStrings.split (sName, "/");
			if ( vPathElements.size() > 0 ) 
			{
				String sPathTestIfExists
				Enumeration e = vPathElements.elements();
				while ( e.hasMoreElements() )
				{
					String spath = (String) e.nextElement();	
				} 
				
			} 
			*/
			
			byte [] rgb = new byte [1000];

			int n;

			FileOutputStream fos = new FileOutputStream (sTargetPath+"/"+sFQRelName );
			
			while ((n = zipinputstream.read( rgb)) > -1)
			{
				fos.write (rgb, 0, n );
			}

			fos.close();
			zipinputstream.closeEntry();
		}

		zipinputstream.close();
	}
}
	
	
	
	
	

/*
Original code from web page example 

import java.io.File;
import java.io.FileInputStream;

import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import java.util.Date;

public
class Zip
{
  public
  static
  void
  main(String [] rgstring)
    throws Exception
  {
    ZipOutputStream zipoutputstream = new ZipOutputStream(System.out);

    // Select your choice of stored (not compressed) or
    //   deflated (compressed).

    zipoutputstream.setMethod(ZipOutputStream.DEFLATED);

    for (int i = 0; i < rgstring.length; i++)
    {
      File file = new File(rgstring[i]);

      byte [] rgb = new byte [1000];

      int n;

      FileInputStream fileinputstream;

      // Calculate the CRC-32 value.  This isn't strictly necessary
      //   for deflated entries, but it doesn't hurt.

      CRC32 crc32 = new CRC32();

      fileinputstream = new FileInputStream(file);

      while ((n = fileinputstream.read(rgb)) > -1)
      {
        crc32.update(rgb, 0, n);
      }

      fileinputstream.close();

      // Create a zip entry.

      ZipEntry zipentry = new ZipEntry(rgstring[i]);

      zipentry.setSize(file.length());
      zipentry.setTime(file.lastModified());
      zipentry.setCrc(crc32.getValue());

      // Add the zip entry and associated data.

      zipoutputstream.putNextEntry(zipentry);

      fileinputstream = new FileInputStream(file);

      while ((n = fileinputstream.read(rgb)) > -1)
      {
        zipoutputstream.write(rgb, 0, n);
      }

      fileinputstream.close();

      zipoutputstream.closeEntry();
    }

    zipoutputstream.close();
  }
}


The ZipInputStream class 
The ZipInputStream class is a subclass of class FilterInputStream. It reads data in zip file format from an input stream. It redefines the read() method so that any data read from the stream is first decompressed. It works with any input stream. 

The following methods manipulate these input streams: 

getNextEntry() 

Begin reading a new entry. This method reads information from the input stream and creates and returns a ZipEntry instance. 

read() 

Reads data from the input stream. 

closeEntry() 

Finish reading an entry. This method skips past any additional information about the entry in the input stream. 

close() 

Closes the input stream. 

The following code demonstrates how to create a ZipInputStream and how to read entries from it: 



import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;

public
class Unzip
{
  public
  static
  void
  main(String [] rgstring)
    throws Exception
  {
    ZipInputStream zipinputstream = new ZipInputStream(System.in);

    while (true)
    {
      // Get the next zip entry.  Break out of the loop if there are
      //   no more.

      ZipEntry zipentry = zipinputstream.getNextEntry();

      if (zipentry == null) break;

      // Read data from the zip entry.  The read() method will return
      //   -1 when there is no more data to read.

      byte [] rgb = new byte [1000];

      int n;

      while ((n = zipinputstream.read(rgb)) > -1)
      {
        // In real life, you'd probably write the data to a file.
      }

      zipinputstream.closeEntry();
    }

    zipinputstream.close();
  }
}

*/