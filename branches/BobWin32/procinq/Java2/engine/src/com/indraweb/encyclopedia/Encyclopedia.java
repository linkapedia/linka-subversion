// class Encyclopedia
// to do : put proper search terms in the search window

package com.indraweb.encyclopedia;


import java.util.*;
import java.io.*;

import com.indraweb.database.*;
import com.indraweb.html.*;
import com.indraweb.ir.*;
//import com.iw.scoring.*;
//import com.iw.metasearch.*;
// import com.indraweb.sources.*;
import com.indraweb.signatures.*;
import com.indraweb.util.*;
import com.indraweb.execution.ThreadInfo;
import com.indraweb.execution.Session;
import com.indraweb.corpusinstall.CorpusFileDescriptor;


public class Encyclopedia
{
	//private	CycTermByTopicMatrixAndSearchResults	cmFrequencyMAtrix;
	private Vector							vCorpusTopics		= new Vector();
	private Vector vCorpusFileDescriptors = null;
	private int										iCorpusID			= -1;

	// total word counts for all docs in this encyclopedia
	public	Hashtable htWordsAllCorpusTopics  = new Hashtable();

	public String	m_saAllTopicsAllWords[];


	// ----------------------------------------------------------------
	public Encyclopedia (    // CONSTRUCTOR
							int iCorpusID_ ,
							Vector vCFDNodesLeavesOrAll_VectorSlice,
							java.sql.Connection dbc_,
                            ParseParms parseParms,
                            WordFrequencies wordfreq,
                            SignatureParms sigparms
						)
	// ----------------------------------------------------------------
	{
		try
		{

			iCorpusID = iCorpusID_;
			vCorpusFileDescriptors = vCFDNodesLeavesOrAll_VectorSlice;

			// ------------------------------------------------------------
			// String sRunOutputDir = Session.cfg.getProp("IndraHome") + "/" + "RunOutputs" + "/CorpusID_" + iCorpusID_;
			//UtilFile.ensureFolderExists ( sRunOutputDir );

			try
			{
				// TURN THE LIST OF CFDs into a list of Topics

				for ( int iTopicIndex = 0 ;
					  iTopicIndex < vCFDNodesLeavesOrAll_VectorSlice.size();
					  iTopicIndex++
					 )
				{

					CorpusFileDescriptor cfd = ( CorpusFileDescriptor ) vCFDNodesLeavesOrAll_VectorSlice.elementAt ( iTopicIndex );
					Topic tNew = cfd.getThisCFDTopic( dbc_ , wordfreq, sigparms);
					//api.Log.Log (older)  ("grabbing node [" + cfd.iNodeID + "] in topic ["+ (iTopicGlobalIndex + 1) +
						//"] of [" + iGlobalCycTopicsEndPoint +
						//"] corpus URL [" +  UtilFileAndDirNames.stripUpToAndIncl_IndraHome ( tNew.m_sTopicURL, true ) +
						//"] title [" + tNew.m_DocumentTitle +
						//"] \r\n");
					vCorpusTopics.addElement ( tNew ) ;
		/*			api.Log.Log  (older) ( "vCorpusTopics size [" +vCorpusTopics.size()  + "] iTopicIndex [" +
                            iTopicIndex+
                            "] tNew.m_sTopicURL [" + tNew.m_sTopicURL+
                            "] tNew.m_lThisTopicNumUniquewords [" + tNew.m_lThisTopicNumUniquewords + "]\r\n" );
*/				} // for (int iTopicIndex = 0; iTopicIndex < m_iNumCorpusTopics ;iTopicIndex++)


				// ****************************************************
                // ************* collectAllWordsFromContainedTopics
				// ****************************************************
				collectAllWordsFromContainedTopics  ( vCorpusTopics ) ;

				// ****************************************************
				// ************* FillCycArrayAllDocsAllWords
				// ****************************************************
				fillCycArrayAllDocsAllWords  (  ) ;



			}
			catch ( Exception e2 )
			{
				Log.FatalError ("in encyc", e2 );
			}
		}
		catch ( Throwable t )
		{
			Log.FatalError ( "Encyclopedia.Encyclopedia() : error creating cyc corpusID [" + iCorpusID_ + "]", t );
		}

	} // public Encyclopedia (int iCorpusID_  )

	// ------------------------------------------------------------
	public Topic getCorpusTopic ( int t )
	// ------------------------------------------------------------
	{
		return (Topic) vCorpusTopics.elementAt (t) ;
	}

	// ------------------------------------------------------------
	public CorpusFileDescriptor getCorpusCFD ( int t )
	// ------------------------------------------------------------
	{
		return ( CorpusFileDescriptor ) vCorpusFileDescriptors.elementAt (t) ;
	}


    private static HashSet hsDebugStrUniqueWordsConsideredInOrthog = new HashSet();
	// --------------------------------------------------------------------
	private void collectAllWordsFromContainedTopics ( Vector vCorpusTopics )
	// --------------------------------------------------------------------
	{
		// int iMax = Session.cfg.getPropInt ("SigGenRangeSize");

		int iMax = vCorpusTopics.size();
		for ( int i = 0; i < iMax; i++)
		{
			Topic t = ( Topic ) vCorpusTopics.elementAt ( i );
			Hashtable ht = t.htWordsDocOnly_noDescendants;
            //api.Log.Log ("in collecting all words node [" + t.getNodeID() + "] count [" + ht.size() + "]" );
            int iWdIdx = 0;
			for ( Enumeration e = ht.keys () ; e.hasMoreElements () ;)
			{
				// increment the global cyc counter for this word
				String sWord = ( String ) e.nextElement();
                //api.Log.Log (iWdIdx++ + ". node [" +t.getNodeID()+ "] in Encyclopedia.collectAllWordsFromContainedTopics add wd [" + sWord + "] node [" +
                //        ( ( Topic ) vCorpusTopics.elementAt(i)).getNodeID() + "]" );
                hsDebugStrUniqueWordsConsideredInOrthog.add( sWord );

				UtilSets.hash_increment_count_for_string
					(
						this.htWordsAllCorpusTopics,
						sWord,
						( ( Integer ) ( ( Topic ) vCorpusTopics.elementAt(i)).htWordsDocOnly_noDescendants.get ( sWord ) ).intValue ()
					);
			}
            if ( i % 1000 == 0 )
                api.Log.Log ("# unq wds after topic [" + i + "] hs.size [" + hsDebugStrUniqueWordsConsideredInOrthog.size() + "]" );

		}
        // api.Log.Log ("done collecting all cyc words count [" + hsDebugStrUniqueWordsConsideredInOrthog.size()+ "]" );

	}

	// --------------------------------------------------------------------
	private void fillCycArrayAllDocsAllWords (  )
	// --------------------------------------------------------------------
	{

		m_saAllTopicsAllWords = new String [ htWordsAllCorpusTopics.size () ];   // words

		int rowWord = 0 ;
		// fill row headers with the set of all words across all documents in cyc
		for (Enumeration e = htWordsAllCorpusTopics.keys() ; e.hasMoreElements() ;)
		{
			// increment the global cyc counter for this word
			String word = (String) e.nextElement();
			m_saAllTopicsAllWords [ rowWord ] = word;
			rowWord += 1;
		}
        //if (true)
        //    api.Log.Log ("cyc.m_saAllTopicsAllWords [" + UtilSets.sArrToStr( m_saAllTopicsAllWords )+ "]" );

	}


	public int getNumTopics()
	{
		return ( vCorpusTopics.size() );
	}

    public String toString()
    {

        StringBuffer sb = new StringBuffer();
        sb.append ("\r\n=============== ENCYCLOPEDIA tostring [");
        sb.append ("vtop.siz["+vCorpusTopics.size()+"]");
        sb.append ("vcfd.siz["+vCorpusFileDescriptors.size()+"]");
        for ( int i = 0; i < vCorpusTopics.size(); i++ )
        {
            Topic t = (Topic) vCorpusTopics.elementAt(i);
            CorpusFileDescriptor cfd = (CorpusFileDescriptor) vCorpusFileDescriptors.elementAt(i);
            sb.append(i + ". " + cfd.toString() + "\r\n\r\n");
        }
        sb.append ("\r\n] end cyc\r\n");
        return sb.toString();
    }

}
