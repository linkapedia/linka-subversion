/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Oct 27, 2003
 * Time: 2:00:44 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.indraweb.ir.texttranslate;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;
import java.text.SimpleDateFormat;

// This is the repository class for type Filesystem.   This class is assumed to be
//  an NT filesystem.   Please use the UNIXFilesystem class for UNIX based filesystems.
public class RegExpTest
{
	// Private variable htVectors contains an entry for each corpus containing
	//   documents tagged to be reindexed through Verity
	public int iClassifyCounter = 0;
	public int iCrawlCounter = 0;
    public static Vector vHyperlinks = new Vector();


    private static Hashtable htVisited = new Hashtable();
    private static Hashtable htFileLocations = new Hashtable();
    private static String Key = "";

    public Hashtable getFileLocations() { return htFileLocations; }

    // extract all HTML hyperlinks from a given document and return in a vector

    // extract all HTML hyperlinks from a given document and return in a vector
    public static void extractHyperTextLinks(String page, String rootURL) {


        Pattern p = Pattern.compile("\\s*href\\s*=\\s*\"*?([^>\" ]+)\"*?.*?>{1}?(.*?)(</a>{1}?)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(page); boolean bResult = m.find();

        while (bResult) {
            String sURL = m.group(1);

            if ((!sURL.toLowerCase().startsWith("http")) && (!sURL.startsWith("/"))) {
                if ((getDirectory(rootURL)).endsWith("/")) {
                    sURL = getDirectory(rootURL)+sURL;
                } else { sURL = getDirectory(rootURL)+"/"+sURL; }
            }
            if (sURL.startsWith("/")) {
                if ((getBase(rootURL)).endsWith("/")) {
                    sURL = getBaseWithoutSlash(rootURL)+sURL;
                } else { sURL = getBase(rootURL)+sURL; }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf("#");
            if (iPound != -1) { sURL = sURL.substring(0, iPound); }

            try {
                URI u = new URI(sURL);
                u = u.normalize();
                sURL = u.toString();
            } catch (Exception e) {
                System.out.println("Could not normalize URL: "+sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll(new Character((char)0).toString(), "");

            if (hasKey() && (sURL.indexOf(Key) != -1)) {
                if (!alreadyVisited(sURL) && (!isImage(sURL))) {
                    vHyperlinks.add(sURL); markVisited(sURL); }
            }
            bResult = m.find();
        }
    }

    // return the URL directory that this file was found in on the web server
    public static String getDirectory (String URL) {
        while (URL.endsWith("/")) { return URL; }
        int index = URL.lastIndexOf('/');

        return URL.substring(0, index);
    }
    // return the base URL (host)
    public static String getBase (String URL) {
        int index = URL.indexOf('/', 7);

        return URL.substring(0, index);
    }

    public static String getBaseWithoutSlash (String URL) {
        int index = URL.indexOf('/', 7);

        return URL.substring(0, index-1);
    }

    // mark or check if page has already been spidered
    public static void markVisited (String URL) { htVisited.put(URL.toLowerCase(), "1"); }
    public static boolean alreadyVisited (String URL) { if (htVisited.containsKey(URL.toLowerCase())) { return true; } return false; }

    // setKey sets the keyword field.  This allows us to ensure matches are only found from within the directory of the starting point.
    public static String setKey (String sKey) {
        if (!sKey.endsWith("/")) { sKey = getDirectory(sKey)+"/"; }
        if (sKey.startsWith("http://")) {
            int index = sKey.indexOf('/', 7);
            if (index == -1) { return ""; }
            sKey = sKey.substring(index);
        }
        Key = sKey;
        return Key;
    }
    // these functions determine whether or not to check the URL for a specific key directory
    public static String getKey() { return Key; }
    public static boolean hasKey() { if (!Key.equals("")) { return true; } return false; }

    // look at the document extension, make sure it's not an image file
    public static boolean isImage (String URL) {
        if (URL.toLowerCase().endsWith(".jpg")) { return true; }
        if (URL.toLowerCase().endsWith(".gif")) { return true; }
        if (URL.toLowerCase().endsWith(".jpeg")) { return true; }
        if (URL.toLowerCase().endsWith(".bmp")) { return true; }
        if (URL.toLowerCase().endsWith(".wav")) { return true; }
        if (URL.toLowerCase().endsWith(".css")) { return true; }
        if (URL.toLowerCase().endsWith(".xml")) { return true; }
        if (URL.indexOf("javascript") != -1) { return true; }
        if (URL.indexOf("mailto") != -1) { return true; }
        if (URL.indexOf("..") != -1) { System.out.println("Found URL: "+URL+" has relative paths, discarding.."); return true; }
        return false;
    }


    // HEAD request the URL and return the last modified date, if applicable, style: 1976-01-05 13:50:08
    public String doHeadRequest (String URL) {
        HttpURLConnection httpCon = null;

        try {
            URL myURL = new URL(URL);
            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestMethod("HEAD");

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            long changed = httpCon.getLastModified();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmss");
            return sdf.format(new Date(changed));

        } catch (Exception e) {
            System.out.println("HTTP HEAD request failure: "+e.toString());
            return "";
        }
        finally { httpCon.disconnect(); }
    }

}
