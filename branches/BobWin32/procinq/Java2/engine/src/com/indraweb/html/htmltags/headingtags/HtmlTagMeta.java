package com.indraweb.html.htmltags.headingtags;

import com.indraweb.html.htmltags.HtmlTag;
import com.indraweb.html.*;
import com.indraweb.util.*;

public class HtmlTagMeta extends HtmlTag
{
	public String name = null;
	public String content = null;
	
	public HtmlTagMeta ( String MSTokenizerLineString ) 
	{
		super.setTagType ( UtilHTML.HTMLTagType_META );
		//[offset=98,type=tag,tag=META,attrs={name=bookTitle, content=Infancy and Toddlerhood: The first two years},text=null
		//[offset=197,type=tag,tag=META,attrs={name=chapterTitle, content=EMOTIONAL AND SOCIAL DEVELOPMENT IN INFANCY AND TODDLERHOOD},text=null
		//[offset=321,type=tag,tag=META,attrs={name=Ancestor Titles, content=Theories of Infant and Toddler Personality || Erik Erikson: TRUST AND AUTONOMY},text=null
		name	= UtilStrings.getStringBetweenThisAndThat ( MSTokenizerLineString.toLowerCase(), "{name=", ", content=" );
		content = UtilStrings.getStringBetweenThisAndThat ( MSTokenizerLineString.toLowerCase(), ", content=", "},text" );
	} 
}
