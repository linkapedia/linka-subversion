package com.indraweb.util;

public class UtilSQLBuilder
{
	// *****************************************
	public static String buildConstraint ( String[] sArrWhere_ )
	// *****************************************
	{
		
		StringBuffer sb = new StringBuffer();
		boolean bFirstDoneAlready = false;
		for ( int i = 0; i < sArrWhere_.length; i++)
		{
			if ( sArrWhere_[i] != null &&  !sArrWhere_[i].trim().equals("") ) 
			{
				if ( bFirstDoneAlready == true )
					sb.append ( " and " );
				else
					sb.append ( " where "  );
					
				sb.append ( sArrWhere_[i] );
				bFirstDoneAlready = true;
			}
		} 
		
		return sb.toString();
	} // private static String buildConstraint ( String[] sArrWhere_ )
	
	// *****************************************
	public static String buildIntOrConstraint ( String sColName, 
												String sComparator, 
												int[] iArrIntValues )
	// *****************************************
	{
		
		StringBuffer sb = new StringBuffer();
		sb.append ("(");
		boolean bFirstDoneAlready = false;
		for ( int i = 0; i < iArrIntValues.length; i++)
		{
			if ( bFirstDoneAlready == true )
				sb.append ( " or " );
					
			sb.append ( sColName + " "  + sComparator + " " +  iArrIntValues[i] );
			bFirstDoneAlready = true;
		} 
		sb.append (")");
		return sb.toString();
	} // private static String buildConstraint ( String[] sArrWhere_ )
	
	
	
}
