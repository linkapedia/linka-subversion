/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Nov 8, 2003
 * Time: 4:01:41 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.ir;

import java.sql.Connection;

public class TermHelper {

    public static int addTerm(String sTerm, int iLangID, int iLangIDFrom, Connection dbc)
            throws Exception {
        String sSQLPre = "select termid from term where term = '" + sTerm + "'";
        int iTermID = com.indraweb.database.JDBCIndra_Connection.executeSQLInt(sSQLPre, dbc);

        if (iTermID < 0) {
            String sSQLGetSeq = "select SEQ_TERMTABLE.nextval from dual";
            iTermID = com.indraweb.database.JDBCIndra_Connection.executeSQLInt(sSQLGetSeq, dbc);
            String sSQLInsert = "insert into term (TERMID, TERM, LANGUAGEID, LANGUAGEIDFROM) "
                    + " values (" + iTermID + ", '" + sTerm + "', " + iLangID + "," + iLangIDFrom + ")";
            com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLInsert, dbc, false, -1);
        }
        return iTermID;
    }
}
