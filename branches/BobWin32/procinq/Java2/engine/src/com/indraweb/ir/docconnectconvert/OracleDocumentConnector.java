package com.indraweb.ir.docconnectconvert;

import java.sql.*;
import java.io.PrintWriter;
import java.util.Vector;

/**
 * This is called by classify when in a GUI invoked classify
 */

public class OracleDocumentConnector
{

    private static String getURLContentAsStringThruOracle (int iDOCIDSource ,
                                                          int iNumBytes ,
                                                          Connection dbc)
    {
        String sReturn = null;
        String sDocIDSource = "" + iDOCIDSource;  // http://ist-socrates.berkeley.edu/~bsp/caucasus/newsletter/2003-03ccan.pdf
        CallableStatement cs = null;
        try
        {
            cs = dbc.prepareCall ("begin GETTEXTSTREAM (:1, :2); end;");
            cs.setString (1 , sDocIDSource);
            cs.registerOutParameter (2 , Types.CLOB);
            cs.execute ();

            Clob c = cs.getClob (2);
            //System.out.println("<LENGTH>"+c.length()+"</LENGTH>");
            int iKeep = (int) c.length ();
            if (iNumBytes < c.length () && iNumBytes > 0)
                iKeep = iNumBytes;
            //sReturn = c.getSubString(0,iKeep-1);
            sReturn = c.getSubString (1 , iKeep);
        } catch ( Exception e )
        {
            api.Log.LogError (e);

        } finally
        {
            if (cs != null)
            {
                try
                {
                    cs.close ();
                } catch ( Exception e )
                {
                    e.printStackTrace ();
                }
                cs = null;
            }
        }
        //System.out.println("iDOCIDSource [" + iDOCIDSource + "] <KEPT>"+sReturn.length() +"</KEPT>");
        //System.out.println("iDOCIDSource [" + iDOCIDSource + "] sReturn ["+sReturn +"]");
        return sReturn;
    }

/*
    This function has been altered to return the content and not the filename
*/
    public static String[] getTextOrTwoFromOracleInATempFileName (int iDocIDSource ,
                                                           int iNumBytes ,
                                                           Connection dbc ,
                                                           boolean bVerbose ,
                                                           int iProp_Dweight,
                                                           int iProp_Sweight,
                                                           String sDocTitleToUse) throws Exception
    {
        String[] sArrReturn = new String[2];

        if ( iProp_Dweight != 0 ) sArrReturn[0] = getOracleData(iDocIDSource, iNumBytes, dbc, false);
        if ( iProp_Sweight != 0 ) sArrReturn[1] = getOracleData(iDocIDSource, iNumBytes, dbc, true);

        return sArrReturn;
    }

    private static String getOracleData (int iDocIDSource, int iNumBytes, Connection dbc, boolean bAbstractOnly )
            throws Exception
    {
        String s = "";

        boolean bIsAbstractNull = false;
        if (bAbstractOnly)
        {
            Statement stmt = null;
            ResultSet rs = null;
            try
            {
                String sSQL = " select DocumentSummary from Document where DocumentId = " + iDocIDSource;
                stmt = dbc.createStatement ();
                rs = stmt.executeQuery (sSQL);
                rs.next ();
                s = rs.getString (1);
                if ( s == null || s.trim().equals(""))
                    bIsAbstractNull = true;
                else
                    s = "<HTML>\r\n<BODY>\r\n" + s + "\r\n</BODY>\r\n</HTML>";
                    //s = "<HTML><HEAD><TITLE>" + sDocTitleToUse + "</TITLE></HEAD>\r\n<BODY>\r\n" + s + "\r\n</BODY>\r\n</HTML>";
            } catch ( Exception e )
            {
                throw e;
            } finally
            {
                if (rs != null)
                {
                    rs.close ();
                    rs = null;
                }
                if (stmt != null)
                {
                    stmt.close ();
                    stmt = null;
                }
            }
        }
        else // not abstract only
        {
            Statement stmt = null;
            ResultSet rs = null;
            try
            {
                //api.Log.Log ("oracle doc connector retrieving URL [" + sDocURL + "]" );
                String sOracleString = OracleDocumentConnector.getURLContentAsStringThruOracle (iDocIDSource , iNumBytes , dbc);
                if ( sOracleString.toLowerCase().indexOf("<html>") < 0)
                    //s = "<HTML><HEAD><TITLE>" + sDocTitleToUse + "</TITLE></HEAD>\r\n<BODY>\r\n" +
                    s = "<HTML>\r\n<BODY>\r\n" +
                            sOracleString
                            + "\r\n</BODY>\r\n</HTML>";
                else
                    s = sOracleString;
            } catch ( Exception e )
            {
                throw e;
            } finally
            {
                if (rs != null)
                {
                    rs.close ();
                    rs = null;
                }
                if (stmt != null)
                {
                    stmt.close ();
                    stmt = null;
                }
            }
        }
        //api.Log.Log(" in writeOracleFileToDisk  ["  + s + "]" );
        if (bIsAbstractNull && bAbstractOnly) return null;

        return s;
    }


}
