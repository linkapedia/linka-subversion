package com.indraweb.workfactory;

import com.indraweb.util.*;

public class WorkTask // extends Thread
{
	static final int wuState_IDLE				= 1;
	static final int wuState_IN_PROCESS			= 2;
	static final int wuState_COMPLETE_SUCCESS	= 3;
	static final int wuState_COMPLETE_FAILED	= 4;
	
	
	int iWuState = wuState_IDLE;
	String sMsgCompletedSuccessful = "I am done";
	String sMsgCompletedUnsuccessful = "something went wrong";
	private String sTaskType = null;
	private long lTaskNode = -1;
	
	public void setTaskType ( String sTaskType_ ) 
	{
		sTaskType = sTaskType_;
	} 
	public String getTaskType (  ) 
	{
		return sTaskType;
	} 
	
	public void setTaskNode ( long lTaskNode_ ) 
	{
		lTaskNode = lTaskNode_;
	} 
	public long getTaskNode (  ) 
	{
		return lTaskNode;
	} 
	
	public void setState_inProcess ()
	{
		iWuState = wuState_IN_PROCESS;
	} 
	
	public void setState_completed ( int iCopletionCode_ )
	{
		iWuState = iCopletionCode_;
	} 
	
	public boolean isInProcess ()
	{
		return ( iWuState == wuState_IN_PROCESS );
	} 
	
	public void doMyWorkSynchronous ( WorkResource wResource_ )
	{
		Log.FatalError ( " pure virtual ");
	} 

	
	
} 
