/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 6, 2004
 * Time: 6:10:36 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.signatures;

import api.APIProps;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.Thesaurus;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.UtilTitleAndSigStringHandlers;
import java.sql.Connection;
import java.util.Vector;

public class SigExpandersTitleThesaurus {

    /**
     * given raw sig content from the db (no thes, no titles)
     */
    public static SignaturesContainer sigExpandAndDedup(
            Vector vStr,
            Vector vInt,
            String sTitleSelf,
            String sTitleParent,
            boolean bThesExpandAndSigFilter,
            SignatureParms sigparms,
            Thesaurus thesaurus,
            ParseParms parseparms,
            boolean bDebug) throws Exception {
        if (vStr.size() != vInt.size()) {
            throw new Exception("invlid sig vector sizes");
        }
        // ways to improve this :
        // 1) leave titles and non separate til the end
        // 2) work from a policy config as to howb to expand
        // 3) use the vector under concept to represent thesaurus ids

        //        if ( UtilFile.bFileExists("/temp/IndraDebugSigExpander.txt"))
        //        {
        //            api.Log.Log (
        //                    "1 Vstr, vInt [" + StringAndCount.toString(vStr, vInt)  + "] \r\n" +
        //                    "2 sTitleSelf[" + sTitleSelf  + "] \r\n" +
        //                    "3 sTitleParent[" + sTitleParent + "] \r\n" +
        //                    "4 sigparms[");
        //            sigparms.printme();
        //
        //            api.Log.Log (
        //
        //                     "] \r\n" +
        //                    "5 bThesExpand [" + bExpand + "] \r\n" +
        //                    "6 thesaurus ["+ thesaurus.toString() + "] \r\n" +
        //                    "7 parseparms ["+"xxxx"+"] \r\n" );
        //        }

        int iNumTitleTerms = 0;

        //api.Log.Log ( "A0. v into sigExpandAndDedup [" + sigContNonTitles_InOut_ToWithTitles.toString() + "]" );

        int iMaxCount_preTitles = UtilSets.maxOfIntVector(vInt);
        //iMaxCount = 755; // debug
        // STEP 1 ============= PARENT TITLE
        String[] sArrTitleCleaned_preThes_Parent = null;
        SignaturesContainer sigContTitle_Parent = null;
        if (sigparms.getNumParentTitleTermsAsSigs() > 0 && sTitleParent != null) {
            sigContTitle_Parent = new SignaturesContainer();
            {
                sArrTitleCleaned_preThes_Parent =
                        UtilTitleAndSigStringHandlers.convertTitleToSigStyleSArr_parseparms(
                        sTitleParent, false, parseparms, sigparms.iNumParentTitleTermsAsSigs);
                //api.Log.Log ("parent title arr pre thes [" + UtilSets.sArrToStr(sArrNodeTitleParentCleanedStopped_preThes)+"]" );
                sigContTitle_Parent.addAll(sArrTitleCleaned_preThes_Parent,
                        bThesExpandAndSigFilter, thesaurus, new Integer(iMaxCount_preTitles), sigparms);
            }
        }

        // STEP 2 ============= SELF TITLE
        String[] sArrTitleCleaned_preThes_Self = null;
        SignaturesContainer sigContTitle_Self = new SignaturesContainer();
        sArrTitleCleaned_preThes_Self =
                UtilTitleAndSigStringHandlers.convertTitleToSigStyleSArr_parseparms(
                sTitleSelf, false, parseparms, -1);
        // add self title as a whole separate phrase and thes expand that
        sigContTitle_Self.addAsSingleTermWithThesExpandButNotBroken(
                sTitleSelf.toLowerCase(),
                new Integer(iMaxCount_preTitles),
                thesaurus,
                sigparms,
                false); // don't add self
        sigContTitle_Self.addAll(sArrTitleCleaned_preThes_Self,
                bThesExpandAndSigFilter, thesaurus, new Integer(iMaxCount_preTitles), sigparms);
        //api.Log.Log ("sigContTitle_Self post thes [" + sigContTitle_Self.toString() +"]" );


        // STEP 3 =============  NOW NON-TITLE REAL SIGS
        // now add parents if wanted also

        Integer IMaxCount = new Integer(iMaxCount_preTitles);
        // insert parent and self counts into the vectors that were given by the db

        //Vector v = null;
        //api.Log.Log (" sigContNonTitles_InOut_ToWithTitles.toString() pre titles [" + sigContNonTitles_InOut_ToWithTitles.toString() + "]" );
        SignaturesContainer sigContAll = new SignaturesContainer();

        if (sigContTitle_Parent != null) {
            sigContAll.addAll(sigContTitle_Parent, 0, sigparms, true, thesaurus);
        }

        sigContAll.addAll(sigContTitle_Self, 0, sigparms, true, thesaurus);
        //api.Log.Log (" sigCont both titles so far [" + sigContAll.toString() + "]" );
        // both titles so far - get a deduped count

        //api.Log.Log (" sigCont vec both titles so far [" + UtilSets.vToStr(sigContAll.getVStr()) + "]" );
        //api.Log.Log (" hs vec both titles so far [" + UtilSets.hsToStr(UtilSets.vToHS(sigContTitle_Self.getVStr())) + "]" );
        //sigContAll.setTitleTermCount(UtilSets.vToHS(sigContTitle_Self.getVStr()).size());
        sigContAll.setTitleTermCount(UtilSets.vToHS(sigContAll.getVStr()).size());
        // now add the base terms from the db
        sigContAll.addAll(vStr, vInt, sigparms, true, thesaurus);

        //api.Log.Log (" sig cont title parent pre dedup [" + sigContTitle_Parent.toString() + "]" );
        //api.Log.Log (" sig cont title self pre dedup [" + sigContTitle_Self.toString() + "]" );
        //api.Log.Log (" sig cont pre dedup [" + sigContAll.toString() + "]" );
        sigContAll.deDup();
        //api.Log.Log (" sig cont post dedup [" + sigContAll.toString() + "]" );
        // hbk 2004 07 21 api.Log.Log (" sigContAll [" + sigContAll.toString() + "]" );
        return sigContAll;
    }

    static String[] thesExpandStringArray(String[] sArr,
            boolean bStemOn,
            Thesaurus thesaurus,
            SignatureParms sigparms) throws Exception {
        //api.Log.Log ("C1. sarr thes expand pre [" + UtilSets.sArrToStr(sArr)+ "]" );
        Vector vRet = new Vector();

        for (int i = 0; i < sArr.length; i++) {
            vRet.addElement(sArr[i]);
            Vector vStrTitleThesExpansions_FullTitleAndWordAtTimeTitle =
                    thesaurus.getMultiWordExpansions(sArr[i]);
            if (vStrTitleThesExpansions_FullTitleAndWordAtTimeTitle != null) {
                vRet.addAll(vStrTitleThesExpansions_FullTitleAndWordAtTimeTitle);
            }
        }
        String[] sArrReturn = new String[vRet.size()];
        vRet.copyInto(sArrReturn);
        //api.Log.Log ("C1. sarr thes expand post [" + UtilSets.sArrToStr(sArrReturn)+ "]" );
        return sArrReturn;
    }

    public static SignaturesContainer getADefaultExpansion(Vector vStr_InOut,
            Vector vInt_InOut,
            String sNodeTitle,
            int iCorpusId,
            int iParentNodeID,
            Connection dbc,
            int iNumParentTitleTermMaxAsSigs // on way into sigs display for user to manipulate - may not want that
            ) throws Exception {
        int iSarrTitleLen = 0;

        ParseParms parseparms = new ParseParms(
                ParseParms.getDelimitersAll_Default(),
                ParseParms.getIncludeNumbers_default(),
                ParseParms.getNumBytesToTake_default());
        Thesaurus thesaurus = Thesaurus.cacheGetThesaurus(false, iCorpusId, true, dbc);

        APIProps apiprops = new api.APIProps();
        apiprops.put("NumParentTitleTermsAsSigs", "" + iNumParentTitleTermMaxAsSigs);
        SignatureParms sigparms = new SignatureParms(apiprops);

        String sTitleParent = null;
        if (iNumParentTitleTermMaxAsSigs > 0) {
            String sSQL2 = "select nodetitle from node where nodeid = " + iParentNodeID;
            sTitleParent = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQL2, dbc);
        }

        //SignaturesContainer sigContNonTitles = new SignaturesContainer();
        //sigContNonTitles.addAll(vStr_InOut, vInt_InOut, sigparms, false, thesaurus  );
        // needs to be identical to the way file builder gens them ...
        //api.Log.Log ("DD. in TSGetNodesigs - pre extend [" + UtilSets.vToStrPairDelimStr (vStrSigWords, vIntSigCounts, "\r\n")+ "]" );
        SignaturesContainer sigCont = sigExpandAndDedup(
                vStr_InOut,
                vInt_InOut,
                sNodeTitle,
                sTitleParent,
                true, // expand
                sigparms,
                thesaurus,
                parseparms,
                false // false,
                );
        //api.Log.Log ("EE. in TSGetNodesigs - post extend [" + UtilSets.vToStrPairDelimStr (vStr_InOut, vInt_InOut, "\r\n")+ "]" );
        return sigCont;
    }

    // use this for local, eg user display of thesaurus purposes
    // use sig content handlers do thesurus expansions
    public static Vector[] vecTermsFromString(
            //                            String sProp_NodeTitle,
            String sProp_DCSigs,
            Vector vStrPerNodeDBSigWords_out,
            Vector vIntPerNodeDBSigCounts_out //                            IntContainer ic_out
            ) {
        String sFinalString = sProp_DCSigs;
        // let's leave title term expansion on the back end side for now ...
//        if ( sProp_NodeTitle != null )
//        {
//            String[] sArrNodeTitleCleanedStopped =
//               UtilTitleAndSigStringHandlers.convertTitleToSigStyleSArr ( sProp_NodeTitle, false);
//
//            StringBuffer  sb = new StringBuffer();
//            for ( int i = 0; i < sArrNodeTitleCleanedStopped.length; i++)
//            {
//                sb.append ( sArrNodeTitleCleanedStopped[i] + "|||");
//            }
//            if (ic_out != null ) ic_out.setiVal ( sArrNodeTitleCleanedStopped.length );
//            sFinalString = sb.toString() + sFinalString;
//        }


        // probably no thesaurus terms in a call here ?
        Vector[] vArrUnder_ArrThesaurusList_ = null;
        // 3,TERM1||thesterm1.1||thesterm1.2||...|#|7,TERM2||thesterm2.1||thesterm2.2||...| |

        // 1/3 get all terms and their thesaurus equivalents   -------------- |#|"

        //api.Log.Log ("vecTermsFromString: sProp_DCSigs [" + sProp_DCSigs + "]" );
        //Vector vStrSplitBases =

        //String[] sArrSplitBases = sProp_DCSigs.split("|#|");
        String[] sArrSplitBases = UtilStrings.splitByStrLenLong_notDelimiters_arrayConvert(sProp_DCSigs, "|||");
        //vStrSplitBases.copyInto(sArrSplitBases);
        //UtilSets.sArrPrint(sArrSplitBases);
        for (int iBaseIdx = 0; iBaseIdx < sArrSplitBases.length; iBaseIdx++) {
            String sBaseOrBaseAndThes = sArrSplitBases[ iBaseIdx];
            //api.Log.Log ("processing base term [" + iBaseIdx + "] [" +sBaseOrBaseAndThes + "]" );
            // 2/3 get term and its thesaurus equivalents ------------- "||"
            String[] sArrSplitBasePlus1 = UtilStrings.splitByStrLenLong_notDelimiters_arrayConvert(sBaseOrBaseAndThes, "||");
            for (int iBasePlus1 = 0; iBasePlus1 < sArrSplitBasePlus1.length; iBasePlus1++) {
                String sBasePlus1Str = sArrSplitBasePlus1[iBasePlus1];
                //api.Log.Log ("processing base+1 term [" + iBasePlus1 + "] [" +sBasePlus1Str + "]" );
                if (iBasePlus1 == 0) {
                    // 3/3 get base term  -------------- ","
                    int iIndexComma = -1;
                    iIndexComma = sBaseOrBaseAndThes.indexOf(",");
                    String sCount = sBasePlus1Str.substring(0, iIndexComma);
                    String sTerm = sBasePlus1Str.substring(iIndexComma + 1);
                    //int iIndexBasePlus1Splitter = sTerm.indexOf("||");
                    //if ( iIndexBasePlus1Splitter > 0 )
                    //    sTerm = sTerm.substring(iIndexBasePlus1Splitter+1);
                    vStrPerNodeDBSigWords_out.addElement(sTerm);
                    vIntPerNodeDBSigCounts_out.addElement(new Integer(sCount));
                } else if (iBasePlus1 >= 1) // first thes term this base term
                {
                    if (vArrUnder_ArrThesaurusList_ == null) {
                        vArrUnder_ArrThesaurusList_ = new Vector[sArrSplitBases.length];
                    }
                    if (iBasePlus1 == 1) {
                        vArrUnder_ArrThesaurusList_[ iBaseIdx] = new Vector();
                    }
                    vArrUnder_ArrThesaurusList_[ iBaseIdx].addElement(sBasePlus1Str);
                }

            } // within a base idx - across base and count then thes terms
        }  // across base indexes

        //ic_out.setiVal(-1); // not incorporating titles yet
        return vArrUnder_ArrThesaurusList_;
    }
}
