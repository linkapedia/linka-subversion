 package com.indraweb.execution;

import com.indraweb.ir.clsStemAndStopList;

import com.indraweb.util.UtilFile;
import com.indraweb.util.clsUtils;
import com.iw.license.IndraLicense;
import java.util.Date;
import java.util.Hashtable;
import java.util.Random;
import org.apache.log4j.Logger;

public class Session {

    private static final Logger log = Logger.getLogger(Session.class);

    public static String cfgFileLocal = null;

    public static String sLogNameDetail;

    // private static String[] sArrCfgStartsWithDoPrint = { "StepToRun" , "NodeStart", "NodeEnd",  };
    public static String programVersion = "1.1.0";
    public static String corpusInstallOptions = "100";
    public static ConfigProperties cfg = null;
    public static Hashtable htAllWebWordCountsInDB = null;
    public static clsStemAndStopList stopList = null;
    public static boolean[] cfgbArr_MetaSearch_Run_SE = null;
    // iArrScoreMethodsThatAreInLineDuringWebTopicCreation mean sLwhich score methods require a web doc word-by-word callout to score corpus doc title containment in web doc
    // esp. with phrase matching ...
    //
    public static int[] iArr_SMs_InLineWordByWord_DuringWebTopicParse = {18, 22, 26, 30};
    public static int[] iArrScoringMethodsToCareAbout = {1, 3, 14};  // config cfg list of scoring methods
    public static long lTStartProgram = -1;
    public static Date dTStartProgram = null;
    public static String sIndraHome = null;
    public static Hashtable htUsers = new Hashtable();
    public static Hashtable htGroups = new Hashtable();

    public static IndraLicense license = new IndraLicense("0");

    private static boolean bInitedSession = false;

    public static boolean GetbInitedSession() {
        return bInitedSession;
    }

    public static boolean SetbInitedSession(boolean b) {
        bInitedSession = b;
        return b;
    }

    public static synchronized void initializeSession ( String[] args, boolean bInitStopList) throws Exception {
        log.debug("initializeSession(String[], boolean)");
        if (bInitedSession){
            return;
        }

        log.info("### Svr Inited [" + new java.util.Date() + "]");
        if (args != null){
            cfg = new ConfigProperties(args);
        }
        sIndraHome = cfg.getProp("IndraHome");
        if ( sIndraHome == null )
        {
            String sOSHost = (String) java.lang.System.getProperties().get("os.name");
            if (sOSHost.toLowerCase().indexOf("windows") >= 0)
                sIndraHome = "c:/Program Files/ITSHarvesterClient";
            else
                sIndraHome = "/home/admin/src/ITSHarvesterClient";
        }
        String sConfigFileLocal = cfg.getProp("ConfigFileLocal");
        if ( sConfigFileLocal != null )
            cfgFileLocal = sIndraHome + "/" + sConfigFileLocal;


        PauseHaltManager.testAndPauseIndefinitely_IfFileExists(cfg.getProp("IndraHome") + "/HaltFile_StopPre_MS.txt", 60000);

        // ADD subsequent CONFIG FILE
        if ( cfgFileLocal != null )
        {
            if (UtilFile.bFileExists(cfgFileLocal)) {
                com.indraweb.util.Log.log("found local config cfgFileLocal (sIndraHome + '/' + cfg.getProp ( 'ConfigFileLocal' )):" + cfgFileLocal);
                cfg.addToProperties(cfgFileLocal, true);
            } else {
                com.indraweb.util.Log.log("not found local config cfgFileLocal (sIndraHome + '/' + cfg.getProp ( 'ConfigFileLocal' )):" + cfgFileLocal);
                System.out.println("not found local config cfgFileLocal (sIndraHome + '/' + cfg.getProp ( 'ConfigFileLocal' )):" + cfgFileLocal);
            }
        }
/*
        String cfgFileSelected = sIndraHome + "/" + cfg.getProp("IndraConfigSelected");

        if (UtilFile.bFileExists(cfgFileSelected))
            cfg.addToProperties(cfgFileSelected, true);
        if (cfg.getProp("runid") == null)
            throw new Exception("'runid' java argument (machine name append) not found ");
        Session.cfg.setProp("MachineName", UtilNet.getLocalHostName() + "_" + cfg.getProp("runid"));
*/

        //check that home dir exists
        if (!UtilFile.bDirExists(sIndraHome)) {
            System.out.println("Home dir not exists [" + sIndraHome + "]");
            throw new Exception("Home dir not exists [" + sIndraHome + "]");
        }
        //clsStemAndStopList stopList = null;
        if ( bInitStopList )
        {
            String sStopListFile = cfg.getProp("StopListFile");
/*
            if (sStopListFile == null )
            {
                sStopListFile = "/commonwords.txt";
            }
*/
            stopList = new clsStemAndStopList(sIndraHome + sStopListFile);
        }
        bInitedSession = true;

    }

    private static void memtest() throws Exception {
        if (true)  // test memory storage strings hashtables keywait sysexit
        {
            int iNumWords = 100000;
            int iMinLen = 6;
            int iEveryXReset = 100;

            if (iNumWords % iEveryXReset != 0)
                throw new Exception("iNumWords % iNumUnique != 0");

            Hashtable htUnique = new Hashtable();
            Hashtable htAll = new Hashtable();

            long lmempre_pregc = Runtime.getRuntime().freeMemory();
            System.gc();
            long lmempre_postgc = Runtime.getRuntime().freeMemory();
            Random rand = null;
            for (int kWIdx = 0; kWIdx < iNumWords; kWIdx++) {
                if (kWIdx % 100000 == 0)
                    System.out.println("kWIdx [" + kWIdx + "] ");

                if (kWIdx % iEveryXReset == 0) {
                    rand = new Random((long) 100);
                    //System.out.println("new rand at kWIdx [" + kWIdx  + "] ");
                }
                int iLen = com.indraweb.util.UtilRandom.getRandomInt_Min0_MaxSizeMinus1(rand, 15);

                if (iLen < iMinLen)
                    iLen = iMinLen;

                char[] carr = new char[iLen];

                for (int j = 0; j < iLen; j++) {
                    int ichar = 48 + com.indraweb.util.UtilRandom.getRandomInt_Min0_MaxSizeMinus1
                            (rand, 42);
                    char c = (char) ichar;
                    carr[j] = c;
                }

                String s = new String(carr);
                htAll.put(new Integer(kWIdx), s);
                htUnique.put(s, s);
                //System.out.println("new rand at kWIdx [" + kWIdx  + "] s [" + s + "]");
            }

            long lmempost_pregc = Runtime.getRuntime().freeMemory();
            System.gc();
            long lmempost_postgc = Runtime.getRuntime().freeMemory();

            clsUtils.waitForKeyboardInput("at testwait 0 " +
                    "\r\nhtunique.size() [" + htUnique.size() + "] " +
                    "\r\nhtAll.size() [" + htAll.size() + "] " +
                    "\r\nlmempre_pregc [" + lmempre_pregc + "] " +
                    "\r\nlmempre_postgc[" + lmempre_postgc + "] " +
                    "\r\nlmempost_pregc [" + lmempost_pregc + "] " +
                    "\r\nlmempost_postgc [" + lmempost_postgc + "] " +
                    "\r\ngap [" + (lmempost_postgc - lmempre_postgc) + "] "
            );
            htUnique = null;
            htAll = null;
            System.gc();
            long lmempostafterall = Runtime.getRuntime().freeMemory();

            clsUtils.waitForKeyboardInput("at testwait 1 " +
                    "\r\nlmempostafterall [" + lmempostafterall + "] " +
                    "\r\ngap [" + (lmempost_postgc - lmempre_postgc) + "] "
            );
            clsUtils.waitForKeyboardInput("at testwait 1 " +
                    "\r\nlmempostafterall [" + lmempostafterall + "] " +
                    "\r\ngap [" + (lmempost_postgc - lmempre_postgc) + "] "
            );
            clsUtils.waitForKeyboardInput("at testwait 1 " +
                    "\r\nlmempostafterall [" + lmempostafterall + "] " +
                    "\r\ngap [" + (lmempost_postgc - lmempre_postgc) + "] "
            );
            System.exit(1);
        }

    }

}
