package com.indraweb.util.sort;

import java.util.*;

public class SortStrings
{
	public static void sortStringVec ( Vector v ) throws Exception
	{
		com.indraweb.util.sort.Sort.sort ( v, new comp() );
	} 
	
	public static void sortStringArr ( String[] Sarr ) throws Exception
	{
		com.indraweb.util.sort.Sort.sort ( Sarr, new comp() );
	} 
	
	
	static class comp implements IComparator
	{
		public int compare ( Object o1, Object o2 )
		{
			return ( ((String ) o1).compareTo ( (String ) o2 ));
		}
		
	}
	
	
	
	
}
