package com.indraweb.html;

//import com.ms.xml.parser.*;		// for html tokenizer

import com.indraweb.network.ExceptionStreamGenFail;
import com.indraweb.util.Log;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.clsUtils;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Vector;


public class UtilHTML {

    // NOTE !!! REMEMBER TO FIX 	String[] HTMLTypeDecodeArr below if you change this below  list
    static int ii = 1;
    public static final int HTMLTagType_UNKNOWNNotProgrammed = 0;
    public static final int HTMLTagType_OPENTITLE = 1;
    public static final int HTMLTagType_CLOSETITLE = 2;
    public static final int HTMLTagType_OPENHEAD = 3;
    public static final int HTMLTagType_CLOSEHEAD = 4;
    public static final int HTMLTagType_OPENHEAD1 = 5;
    public static final int HTMLTagType_OPENHEAD2 = 6;
    public static final int HTMLTagType_OPENHEAD3 = 7;
    public static final int HTMLTagType_OPENHEAD4 = 8;
    public static final int HTMLTagType_OPENHEAD5 = 9;
    public static final int HTMLTagType_OPENHEAD6 = 10;
    public static final int HTMLTagType_CLOSEHEAD1 = 11;
    public static final int HTMLTagType_CLOSEHEAD2 = 12;
    public static final int HTMLTagType_CLOSEHEAD3 = 13;
    public static final int HTMLTagType_CLOSEHEAD4 = 14;
    public static final int HTMLTagType_CLOSEHEAD5 = 15;
    public static final int HTMLTagType_CLOSEHEAD6 = 16;
    public static final int HTMLTagType_TEXT = 17;
    public static final int HTMLTagType_META = 18;
    public static final int HTMLTagType_OpenA = 19;
    public static final int HTMLTagType_CloseA = 20;
    public static final int HTMLTagType_OpenAHref = 21;
    public static final int HTMLTagType_OPENCOMMENT = 22;
    public static final int HTMLTagType_CLOSECOMMENT = 23;
    public static final int HTMLTagType_OPENHTML = 24;
    public static final int HTMLTagType_CLOSEHTML = 25;
    public static final int HTMLTagType_OPENBODY = 26;
    public static final int HTMLTagType_CLOSEBODY = 27;
    public static final int HTMLTagType_BR = 28;
    public static final int HTMLTagType_OpenP = 29;
    public static final int HTMLTagType_CloseP = 30;
    public static final int HTMLTagType_OPENTABLE = 31;
    public static final int HTMLTagType_CLOSETABLE = 32;
    public static final int HTMLTagType_OPENLINK = 33;
    public static final int HTMLTagType_CLOSELINK = 34;
    public static final int HTMLTagType_OPENSTYLE = 35;
    public static final int HTMLTagType_CLOSESTYLE = 36;

    public static final int HTMLTagType_SGMLArticleTitle = 37;
    public static final int HTMLTagType_SGMLCloseArticleTitle = 38;
    public static final int HTMLTagType_SGMLUniqueKey = 39;
    public static final int HTMLTagType_SGMLCloseUniqueKey = 40;
    public static final int HTMLTagType_SGMLOpenPara = 41;
    public static final int HTMLTagType_SGMLClosePara = 42;
    public static final int HTMLTagType_SGMLOpenPronunciation = 43;
    public static final int HTMLTagType_SGMLClosePronunciation = 44;
    public static final int HTMLTagType_SGMLOpenContributorSig = 45;
    public static final int HTMLTagType_SGMLCloseContributorSig = 46;
    public static final int HTMLTagType_TableTagOpen_td = 47;
    public static final int HTMLTagType_TableTagOpen_tr = 48;
    public static final int HTMLTagType_TableTagClose_td = 49;
    public static final int HTMLTagType_TableTagClose_tr = 50;
    public static final int HTMLTagType_TagOpen_Font = 51;
    public static final int HTMLTagType_TagClose_Font = 52;
    public static final int HTMLTagType_OPENSCRIPT = 53;
    public static final int HTMLTagType_CLOSESCRIPT = 54;

    public static final int XMLTagType_RESULT = 55;
    public static final int XMLTagType_RESULT_TITLE = 56;
    public static final int XMLTagType_RESULT_SUMMARY = 57;
    public static final int XMLTagType_RESULT_URL = 58;
    public static final int XMLTagType_ENGINE = 59;

    public static final int XMLTagType_CLOSERESULT = 60;
    public static final int XMLTagType_CLOSERESULT_TITLE = 61;
    public static final int XMLTagType_CLOSERESULT_SUMMARY = 62;
    public static final int XMLTagType_CLOSERESULT_URL = 63;
    public static final int XMLTagType_CLOSEENGINE = 64;

    public static final int HTMLTagType_TagOpen_Style = 65;
    public static final int HTMLTagType_TagClose_Style = 66;

    public static final int HTMLTagType_INDRATRUNCATE = 67;

    // Open close tags for Taxonomy server XML
    public static final int XMLTagType_OPENNODEDOCUMENTS = 68;
    public static final int XMLTagType_CLOSENODEDOCUMENTS = 69;
    public static final int XMLTagType_OPENNODEDOCUMENT = 70;
    public static final int XMLTagType_CLOSENODEDOCUMENT = 71;
    public static final int XMLTagType_OPENNODEID = 72;
    public static final int XMLTagType_CLOSENODEID = 73;
    public static final int XMLTagType_OPENDOCID = 74;
    public static final int XMLTagType_CLOSEDOCID = 75;
    public static final int XMLTagType_OPENGENREID = 76;
    public static final int XMLTagType_CLOSEGENREID = 77;
    public static final int XMLTagType_OPENDOCURL = 78;
    public static final int XMLTagType_OPENSCORE5 = 79;
    public static final int XMLTagType_OPENSCORE6 = 80;
    public static final int XMLTagType_CLOSESCORE1 = 81;
    public static final int XMLTagType_CLOSEDOCURL = 82;
    public static final int XMLTagType_OPENDOCSUM = 83;
    public static final int XMLTagType_CLOSEDOCSUM = 84;
    public static final int XMLTagType_OPENSCORE1 = 85;
    public static final int XMLTagType_OPENSCORE2 = 86;
    public static final int XMLTagType_OPENSCORE3 = 87;
    public static final int XMLTagType_OPENSCORE4 = 88;
    public static final int XMLTagType_CLOSESCORE2 = 89;
    public static final int XMLTagType_CLOSESCORE3 = 90;
    public static final int XMLTagType_CLOSESCORE4 = 91;
    public static final int XMLTagType_CLOSESCORE5 = 92;
    public static final int XMLTagType_CLOSESCORE6 = 93;
    public static final int XMLTagType_OPENDATESCORED = 94;
    public static final int XMLTagType_CLOSEDATESCORED = 95;
    public static final int XMLTagType_OPENDOCTITLE = 96;
    public static final int XMLTagType_CLOSEDOCTITLE = 97;

    public static final int[] HTMLTagTypeArr_OpenAHREF_TEXT = {HTMLTagType_TEXT, HTMLTagType_OpenAHref};
    public static final int[] HTMLTagTypeArr_TEXT = {HTMLTagType_TEXT};
    // NOTE !!! REMEMBER TO FIX 	String[] HTMLTypeDecodeArr below if you change this above list

    // ---------------------------------------------------------
    private static String[] HTMLTypeDecodeArr =
            // ---------------------------------------------------------
            {
                "HTMLTagType_UNKNOWNNotProgrammed",
                "HTMLTagType_OPENTITLE",
                "HTMLTagType_CLOSETITLE",
                "HTMLTagType_OPENHEAD",
                "HTMLTagType_CLOSEHEAD",
                "HTMLTagType_OPENHEAD1",
                "HTMLTagType_OPENHEAD2",
                "HTMLTagType_OPENHEAD3",
                "HTMLTagType_OPENHEAD4",
                "HTMLTagType_OPENHEAD5",
                "HTMLTagType_OPENHEAD6",
                "HTMLTagType_CLOSEHEAD1",
                "HTMLTagType_CLOSEHEAD2",
                "HTMLTagType_CLOSEHEAD3",
                "HTMLTagType_CLOSEHEAD4",
                "HTMLTagType_CLOSEHEAD5",
                "HTMLTagType_CLOSEHEAD6",
                "HTMLTagType_TEXT",
                "HTMLTagType_META",
                "HTMLTagType_OpenA",
                "HTMLTagType_CloseA",
                "HTMLTagType_OpenAHref",
                "HTMLTagType_OPENCOMMENT",
                "HTMLTagType_CLOSECOMMENT",
                "HTMLTagType_OPENHTML",
                "HTMLTagType_CLOSEHTML",
                "HTMLTagType_OPENBODY",
                "HTMLTagType_CLOSEBODY",
                "HTMLTagType_BR",
                "HTMLTagType_OpenP",
                "HTMLTagType_CloseP",
                "HTMLTagType_OPENTABLE",
                "HTMLTagType_CLOSETABLE",
                "HTMLTagType_OPENLINK",
                "HTMLTagType_CLOSELINK",
                "HTMLTagType_OPENSTYLE",
                "HTMLTagType_CLOSESTYLE",
                "HTMLTagType_SGMLArticleTitle",
                "HTMLTagType_SGMLCloseArticleTitle",
                "HTMLTagType_SGMLUniqueKey",
                "HTMLTagType_SGMLCloseUniqueKey",
                "HTMLTagType_SGMLOpenPara",
                "HTMLTagType_SGMLClosePara",
                "HTMLTagType_SGMLOpenPronunciation",
                "HTMLTagType_SGMLClosePronunciation",
                "HTMLTagType_SGMLOpenContributorSig",
                "HTMLTagType_SGMLCloseContributorSig",
                "HTMLTagType_TableTagOpen_td",
                "HTMLTagType_TableTagOpen_tr",
                "HTMLTagType_TableTagClose_td",
                "HTMLTagType_TableTagClose_tr",
                "HTMLTagType_TagOpen_Font",
                "HTMLTagType_TagClose_Font",
                "HTMLTagType_OPENSCRIPT",
                "HTMLTagType_CLOSESCRIPT",

                "XMLTagType_RESULT",
                "XMLTagType_RESULT_TITLE",
                "XMLTagType_RESULT_SUMMARY",
                "XMLTagType_RESULT_URL",
                "XMLTagType_ENGINE",

                "XMLTagType_CLOSERESULT",
                "XMLTagType_CLOSERESULT_TITLE",
                "XMLTagType_CLOSERESULT_SUMMARY",
                "XMLTagType_CLOSERESULT_URL",
                "XMLTagType_CLOSEENGINE",

                "HTMLTagType_TagOpen_Style",
                "HTMLTagType_TagClose_Style",
                "HTMLTagType_INDRATRUNCATE",

                "XMLTagType_OPENNODEDOCUMENTS",
                "XMLTagType_CLOSENODEDOCUMENTS",
                "XMLTagType_OPENNODEDOCUMENT",
                "XMLTagType_CLOSENODEDOCUMENT",
                "XMLTagType_OPENNODEID",
                "XMLTagType_CLOSENODEID",
                "XMLTagType_OPENDOCID",
                "XMLTagType_CLOSEDOCID",
                "XMLTagType_OPENGENREID",
                "XMLTagType_CLOSEGENREID",
                "XMLTagType_OPENDOCURL",
                "XMLTagType_OPENSCORE5",
                "XMLTagType_OPENSCORE6",
                "XMLTagType_CLOSESCORE1",
                "XMLTagType_CLOSEDOCURL",
                "XMLTagType_OPENDOCSUM",
                "XMLTagType_CLOSEDOCSUM",
                "XMLTagType_OPENSCORE1",
                "XMLTagType_OPENSCORE2",
                "XMLTagType_OPENSCORE3",
                "XMLTagType_OPENSCORE4",
                "XMLTagType_CLOSESCORE2",
                "XMLTagType_CLOSESCORE3",
                "XMLTagType_CLOSESCORE4",
                "XMLTagType_CLOSESCORE5",
                "XMLTagType_CLOSESCORE6",
                "XMLTagType_OPENDATESCORED",
                "XMLTagType_CLOSEDATESCORED",
                "XMLTagType_OPENDOCTITLE",
                "XMLTagType_CLOSEDOCTITLE"
            };


    // ---------------------------------------------------------
    public static String decodeCodeToElemTypeMine(int i)
            // ---------------------------------------------------------
    {
        return HTMLTypeDecodeArr[i];
    }


    /**
     * METHOD HTMLTagType
     */ //input 1: input stream for the html content
    // output:  what kind of token is this of the 5:

    //	final int HTMLTagType_OPENHEAD1 = 1;
    //	final int HTMLTagType_CLOSEHEAD1 = 2;

    //	final int HTMLTagType_OPENTITLE = 3;
    //	final int HTMLTagType_CLOSETITLE = 4;
    //	final int HTMLTagType_TEXT       = 5;

    //


    // ---------------------------------------------------------
    public static int HTMLTagType(StringTokenizer sHTMLTokenizerLine_) throws Exception {
        // ---------------------------------------------------------
        // dump the first token ("HTMLTokenizer[offset=2048")
        sHTMLTokenizerLine_.nextToken(", ");

        String strTypeString, strTagString;
        strTypeString = sHTMLTokenizerLine_.nextToken(",");
        strTagString = sHTMLTokenizerLine_.nextToken(",");
        // HKonLib.print ("Entering HTMLTagType	(StringTokenizer st) strTypeString = " + strTypeString, HKonLib.hkdebug_level_routine_entry_exit);

        // OPEN ***************** "type=tag"
        if (strTypeString.equalsIgnoreCase("type=tag")) {
            // 'a' (href) tag 238.HTMLTokenizer[offset=10185,type=tag,tag=a,attrs={href=/www.britannica.com/bcom/eb/article/3/0,5716,117263+1,00.html},text=null
            if (strTagString.equalsIgnoreCase("tag=a")) {
                String attrs = sHTMLTokenizerLine_.nextToken(":"); // to get rest of tokens
                if (UtilStrings.getStrContains(attrs.toLowerCase(), "attrs={href=")) {
                    return HTMLTagType_OpenAHref;
                }
                return HTMLTagType_OpenA;
            } else if (strTagString.equalsIgnoreCase("tag=head")) {
                return HTMLTagType_OPENHEAD;
            } else if (strTagString.equalsIgnoreCase("tag=h1")) {
                return HTMLTagType_OPENHEAD1;
            } else if (strTagString.equalsIgnoreCase("tag=h2")) {
                return HTMLTagType_OPENHEAD2;
            } else if (strTagString.equalsIgnoreCase("tag=h3")) {
                return HTMLTagType_OPENHEAD3;
            } else if (strTagString.equalsIgnoreCase("tag=h4")) {
                return HTMLTagType_OPENHEAD4;
            } else if (strTagString.equalsIgnoreCase("tag=h5")) {
                return HTMLTagType_OPENHEAD5;
            } else if (strTagString.equalsIgnoreCase("tag=h6")) {
                return HTMLTagType_OPENHEAD6;
            } else if (strTagString.equalsIgnoreCase("tag=meta")) {
                return HTMLTagType_META;
            } else if (strTagString.equalsIgnoreCase("tag=title")) {
                return HTMLTagType_OPENTITLE;
            } else if (strTagString.equalsIgnoreCase("tag=html")) {
                return HTMLTagType_OPENHTML;
            } else if (strTagString.equalsIgnoreCase("tag=body")) {
                return HTMLTagType_OPENBODY;
            } else if (strTagString.equalsIgnoreCase("tag=br")) {
                return HTMLTagType_BR;
            } else if (strTagString.equalsIgnoreCase("tag=p")) {
                return HTMLTagType_OpenP;
            } else if (strTagString.equalsIgnoreCase("tag=table")) {
                return HTMLTagType_OPENTABLE;
            } else if (strTagString.equalsIgnoreCase("tag=link")) {
                return HTMLTagType_OPENLINK;
            } else if (strTagString.equalsIgnoreCase("tag=style")) {
                return HTMLTagType_OPENSTYLE;
            } else if (strTagString.equalsIgnoreCase("tag=article.title")) {
                return HTMLTagType_SGMLArticleTitle;
            } else if (strTagString.equalsIgnoreCase("tag=unique.key")) {
                return HTMLTagType_SGMLUniqueKey;
            } else if (strTagString.equalsIgnoreCase("tag=para")) {
                return HTMLTagType_SGMLOpenPara;
            } else if (strTagString.equalsIgnoreCase("tag=pronunciation")) {
                return HTMLTagType_SGMLOpenPronunciation;
            } else if (strTagString.equalsIgnoreCase("tag=contributor.sig")) {
                return HTMLTagType_SGMLOpenContributorSig;
            } else if (strTagString.equalsIgnoreCase("tag=tr")) {
                return HTMLTagType_TableTagOpen_tr;
            } else if (strTagString.equalsIgnoreCase("tag=td")) {
                return HTMLTagType_TableTagOpen_td;
            } else if (strTagString.equalsIgnoreCase("tag=font")) {
                return HTMLTagType_TagOpen_Font;
            } else if (strTagString.equalsIgnoreCase("tag=script")) {
                return HTMLTagType_OPENSCRIPT;
            } else if (strTagString.equalsIgnoreCase("tag=result")) {
                return XMLTagType_RESULT;
            } else if (strTagString.equalsIgnoreCase("tag=title")) {
                return XMLTagType_RESULT_TITLE;
            } else if (strTagString.equalsIgnoreCase("tag=summary")) {
                return XMLTagType_RESULT_SUMMARY;
            } else if (strTagString.equalsIgnoreCase("tag=url")) {
                return XMLTagType_RESULT_URL;
            } else if (strTagString.equalsIgnoreCase("tag=engine-name")) {
                return XMLTagType_ENGINE;
            } else if (strTagString.equalsIgnoreCase("tag=style")) {
                return HTMLTagType_OPENSTYLE;
            } else if (strTagString.equalsIgnoreCase("tag=nodedocuments")) {
                return XMLTagType_OPENNODEDOCUMENTS;
            } else if (strTagString.equalsIgnoreCase("tag=nodedocument")) {
                return XMLTagType_OPENNODEDOCUMENT;
            } else if (strTagString.equalsIgnoreCase("tag=nodeid")) {
                return XMLTagType_OPENNODEID;
            } else if (strTagString.equalsIgnoreCase("tag=documentid")) {
                return XMLTagType_OPENDOCID;
            } else if (strTagString.equalsIgnoreCase("tag=folderid")) {
                return XMLTagType_OPENGENREID;
            } else if (strTagString.equalsIgnoreCase("tag=doctitle")) {
                return XMLTagType_OPENDOCTITLE;
            } else if (strTagString.equalsIgnoreCase("tag=docurl")) {
                return XMLTagType_OPENDOCURL;
            } else if (strTagString.equalsIgnoreCase("tag=docsumary")) {
                return XMLTagType_OPENDOCSUM;
            } else if (strTagString.equalsIgnoreCase("tag=score1")) {
                return XMLTagType_OPENSCORE1;
            } else if (strTagString.equalsIgnoreCase("tag=score2")) {
                return XMLTagType_OPENSCORE2;
            } else if (strTagString.equalsIgnoreCase("tag=score3")) {
                return XMLTagType_OPENSCORE3;
            } else if (strTagString.equalsIgnoreCase("tag=score4")) {
                return XMLTagType_OPENSCORE4;
            } else if (strTagString.equalsIgnoreCase("tag=score5")) {
                return XMLTagType_OPENSCORE5;
            } else if (strTagString.equalsIgnoreCase("tag=score6")) {
                return XMLTagType_OPENSCORE6;
            } else if (strTagString.equalsIgnoreCase("tag=datescored")) {
                return XMLTagType_OPENDATESCORED;
            } else {
                // Log.log  ("Exiting HTMLTagType unknown tag within type=open tag [" + strTagString + "]" , HKonLib.hkdebug_level_routine_entry_exit);
                return HTMLTagType_UNKNOWNNotProgrammed; //
            }

        }

        // CLOSE ***************** "type=close tag"
        // or handle the HTML tokens of type close tag {HEAD, TITLE)
        // ("type=close tag,tag={HEAD, TITLE")
        else if (strTypeString.equalsIgnoreCase("type=close tag")) {
            if (strTagString.equalsIgnoreCase("tag=a")) {
                return HTMLTagType_CloseA;
            }
            if (strTagString.equalsIgnoreCase("tag=head")) {
                return HTMLTagType_CLOSEHEAD;
            } else if (strTagString.equalsIgnoreCase("tag=h1")) {
                return HTMLTagType_CLOSEHEAD1;
            } else if (strTagString.equalsIgnoreCase("tag=h2")) {
                return HTMLTagType_CLOSEHEAD2;
            } else if (strTagString.equalsIgnoreCase("tag=h3")) {
                return HTMLTagType_CLOSEHEAD3;
            } else if (strTagString.equalsIgnoreCase("tag=h4")) {
                return HTMLTagType_CLOSEHEAD4;
            } else if (strTagString.equalsIgnoreCase("tag=h5")) {
                return HTMLTagType_CLOSEHEAD5;
            } else if (strTagString.equalsIgnoreCase("tag=h6")) {
                return HTMLTagType_CLOSEHEAD6;
            } else if (strTagString.equalsIgnoreCase("tag=title")) {
                return HTMLTagType_CLOSETITLE;
            } else if (strTagString.equalsIgnoreCase("tag=html")) {
                return HTMLTagType_CLOSEHTML;
            } else if (strTagString.equalsIgnoreCase("tag=body")) {
                return HTMLTagType_CLOSEBODY;
            } else if (strTagString.equalsIgnoreCase("tag=p")) {
                return HTMLTagType_CloseP;
            } else if (strTagString.equalsIgnoreCase("tag=table")) {
                return HTMLTagType_CLOSETABLE;
            } else if (strTagString.equalsIgnoreCase("tag=link")) {
                return HTMLTagType_CLOSELINK;
            } else if (strTagString.equalsIgnoreCase("tag=style")) {
                return HTMLTagType_CLOSESTYLE;
            } else if (strTagString.equalsIgnoreCase("tag=article.title")) {
                return HTMLTagType_SGMLCloseArticleTitle;
            } else if (strTagString.equalsIgnoreCase("tag=unique.key")) {
                return HTMLTagType_SGMLCloseUniqueKey;
            } else if (strTagString.equalsIgnoreCase("tag=pronunciation")) {
                return HTMLTagType_SGMLClosePronunciation;
            } else if (strTagString.equalsIgnoreCase("tag=contributor.sig")) {
                return HTMLTagType_SGMLCloseContributorSig;
            } else if (strTagString.equalsIgnoreCase("tag=tr")) {
                return HTMLTagType_TableTagClose_tr;
            } else if (strTagString.equalsIgnoreCase("tag=td")) {
                return HTMLTagType_TableTagClose_td;
            } else if (strTagString.equalsIgnoreCase("tag=font")) {
                return HTMLTagType_TagClose_Font;
            } else if (strTagString.equalsIgnoreCase("tag=script")) {
                return HTMLTagType_CLOSESCRIPT;
            } else if (strTagString.equalsIgnoreCase("tag=result")) {
                return XMLTagType_CLOSERESULT;
            } else if (strTagString.equalsIgnoreCase("tag=title")) {
                return XMLTagType_CLOSERESULT_TITLE;
            } else if (strTagString.equalsIgnoreCase("tag=summary")) {
                return XMLTagType_CLOSERESULT_SUMMARY;
            } else if (strTagString.equalsIgnoreCase("tag=url")) {
                return XMLTagType_CLOSERESULT_URL;
            } else if (strTagString.equalsIgnoreCase("tag=engine-name")) {
                return XMLTagType_CLOSEENGINE;
            } else if (strTagString.equalsIgnoreCase("tag=style")) {
                return HTMLTagType_CLOSESTYLE;
            } else if (strTagString.equalsIgnoreCase("tag=INDRATRUNCATE")) {
                return HTMLTagType_INDRATRUNCATE;
            } else if (strTagString.equalsIgnoreCase("tag=nodedocuments")) {
                return XMLTagType_CLOSENODEDOCUMENTS;
            } else if (strTagString.equalsIgnoreCase("tag=nodedocument")) {
                return XMLTagType_CLOSENODEDOCUMENT;
            } else if (strTagString.equalsIgnoreCase("tag=nodeid")) {
                return XMLTagType_CLOSENODEID;
            } else if (strTagString.equalsIgnoreCase("tag=documentid")) {
                return XMLTagType_CLOSEDOCID;
            } else if (strTagString.equalsIgnoreCase("tag=folderid")) {
                return XMLTagType_CLOSEGENREID;
            } else if (strTagString.equalsIgnoreCase("tag=doctitle")) {
                return XMLTagType_CLOSEDOCTITLE;
            } else if (strTagString.equalsIgnoreCase("tag=docurl")) {
                return XMLTagType_CLOSEDOCURL;
            } else if (strTagString.equalsIgnoreCase("tag=docsumary")) {
                return XMLTagType_CLOSEDOCSUM;
            } else if (strTagString.equalsIgnoreCase("tag=score1")) {
                return XMLTagType_CLOSESCORE1;
            } else if (strTagString.equalsIgnoreCase("tag=score2")) {
                return XMLTagType_CLOSESCORE2;
            } else if (strTagString.equalsIgnoreCase("tag=score3")) {
                return XMLTagType_CLOSESCORE3;
            } else if (strTagString.equalsIgnoreCase("tag=score4")) {
                return XMLTagType_CLOSESCORE4;
            } else if (strTagString.equalsIgnoreCase("tag=score5")) {
                return XMLTagType_CLOSESCORE5;
            } else if (strTagString.equalsIgnoreCase("tag=score6")) {
                return XMLTagType_CLOSESCORE6;
            } else if (strTagString.equalsIgnoreCase("tag=datescored")) {
                return XMLTagType_CLOSEDATESCORED;
            } else {
                return HTMLTagType_UNKNOWNNotProgrammed;
            }
        } else if (strTypeString.equalsIgnoreCase("type=comment")) {
            return HTMLTagType_OPENCOMMENT;
        } else if (strTypeString.equalsIgnoreCase("type=text")) {
            return HTMLTagType_TEXT;
        } else if (strTypeString.equalsIgnoreCase("type=font")) {
            return HTMLTagType_TagClose_Font;
        } else {
            return HTMLTagType_UNKNOWNNotProgrammed; //
        }

    } // 	static int HTMLTagType	(StringTokenizer st) {


    /**
     * Consume from an HTMLTokenizer object all tokens until we reach a text item
     * of type text contianing the given string
     * Param: String to be found
     * Return: boolean
     * true if found
     * false if not
     */



    /**
     * Consume from an HTMLTokenizer object all tokens until we reach a text item
     * of type tag contianing the given string
     * Param: String to be found
     * Return: void
     */

    // -------------------------------------------------------------------
    public static Vector HTMLTokenizerFromURL
            (
            UrlHtmlAttributes urlHtmlAttributesContext_,
            int iNumRetriesIfNullStreamResults_,
            int iTimeoutTimedSocket
            ) throws Exception {
        // -------------------------------------------------------------------
        // don't really use this cuz need to close the stream somewhere ... just for cut and paste!!!
        try {
            String sURLori = urlHtmlAttributesContext_.getsURL();
            java.io.InputStream isURLInputStream = null;

            int retryCounter = 0;
            while (true) {
                try {

                    try {
                        isURLInputStream = com.indraweb.network.InputStreamsGen.getDirectInputStreamFromURL(urlHtmlAttributesContext_, iTimeoutTimedSocket, true);
                    } catch (Exception e) {
                        Log.log(" WebCacheReads.getInputStreamFromURL() : can't open file/URL [" + urlHtmlAttributesContext_.getsURL() + "] from original URL[" +
                                urlHtmlAttributesContext_.getsURL() + "]\r\n");
                        isURLInputStream = null;
                    }

                    /*
					isURLInputStream = WebCacheReads.getInputStreamFromURL(
							new CacheProfile ( useCache, null ),
							iTopicID,
							dbc,
							false,
							null,
							urlHtmlAttributesContext_
						 );
					}
					*/

                } catch (Exception e) {
                    Log.NonFatalError("error creating steam for url [" + sURLori + "]", e);
                }

                if (isURLInputStream == null) {
                    //long ldocID = WebCacheReads.getDocIDFromURL ( sURLori , dbc );
                    //long lcacheID = WebCacheReads.getCacheIDFromDocID ( ldocID ,dbc );
                    if (retryCounter + 1 > iNumRetriesIfNullStreamResults_) {
                        Log.NonFatalError("null URLInputStream on final retry ori url [" + sURLori + "] " +
                                " after [" + retryCounter + " ] retries	" +
                                " new URL [" + urlHtmlAttributesContext_.getsURL() + "]\r\n");
                    } else {
                        Log.NonFatalError("null URLInputStream will retry ori url [" + sURLori + "] " +
                                " removing cache record - this is after [" + retryCounter + " ] retries	" +
                                " new URL [" + urlHtmlAttributesContext_.getsURL() + "]\r\n");
                    }

                    retryCounter++;

                } else // succeeded - exit
                {
                    if (retryCounter > 0) {
                        Log.NonFatalError("success after Failover on retry # [" + retryCounter +
                                "] urlori [" + sURLori + "] " +
                                " new URL [" + urlHtmlAttributesContext_.getsURL() + "]\r\n");
                    }
                    break;
                }
                clsUtils.pause_this_many_milliseconds(1000 * 2 * retryCounter);

            } // while ( true ) // try to open things a few times.

            Vector v = null;
            try {
                if (isURLInputStream != null) {
                    com.ms.util.HTMLTokenizer lHTMLTok = HTMLTokenizerRedirector.getTokenizer(
                            isURLInputStream,
                            true,
                            urlHtmlAttributesContext_,
                            iTimeoutTimedSocket,
                            0,
                            true
                    );

                    v = new Vector();
                    v.addElement(isURLInputStream);
                    v.addElement(lHTMLTok);
                }
            } catch (ExceptionStreamGenFail es) {
                // same condition as if stream were null - probably want to rethrow at some point
            }

            return v;

        } catch (Exception e) {
            Log.printStackTrace_mine(95, e);
            throw e;
        }
    }





    // *****************************************************************
    public static void captureHTMLToFile(String sURL, String fileNameAndPath, boolean fatalIfFail)
            // *****************************************************************
    {

        UrlHtmlAttributes urlh = new UrlHtmlAttributes(sURL);
        captureHTMLToFile (urlh , fileNameAndPath, fatalIfFail);

    }
    // *****************************************************************
    public static void captureHTMLToFile(UrlHtmlAttributes urlHtmlAttributesContext_, String fileNameAndPath, boolean fatalIfFail)
            // *****************************************************************
    {

        try {
            //Log.FatalError ("this routine truncated - get proper URL stream and will work fine");
            java.io.InputStream isURLInputStream = com.indraweb.network.InputStreamsGen.getDirectInputStreamFromURL(urlHtmlAttributesContext_, 100000, true);

            //WebCacheReads.getInputStreamFromURL(
            //new CacheProfile ( CacheProfile.CACHE_READ_Allowed, null ) ,
            //-1,
            //null,
            //false,
            //null,
            //urlHtmlAttributesContext_
            //);

            if (isURLInputStream == null) {
                Log.FatalError("isURLInputStream null in captureHTMLToFile for URL [" +
                        urlHtmlAttributesContext_.getsURL() + "]");
            }


            byte[] barr = new byte[1000];
            int x = -1;
            PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(fileNameAndPath, false)));

            while (true) {
                x = isURLInputStream.read(barr);
                if (x == -1)
                    break;
                String s = new String(barr);
                file.write(s);
            }
            file.write("<!--[" + urlHtmlAttributesContext_.getsURL() + "]" + ">\r\n");
            file.close();


        } catch (Exception e) {
            if (fatalIfFail)
                Log.FatalError("ffsfdf", e);
            else
                Log.NonFatalError("faffa", e);

        }

    }

    // ---------------------------------------------------------

    // get type
    // get text
    // 220.HTMLTokenizer[offset=4058,type=text,tag=null,attrs=null,text=1.
    // 221.HTMLTokenizer[offset=4062,type=close tag,tag=b,attrs=null,text=null



    // **************************************************
    public static String getTitleFastFromHTMLFile(String sFileName)
            // **************************************************
    {

        String textline;
        String textlineSpaceCompressed = null;
        ;
        BufferedReader in = null;
        String sTitleReturn = null;
        StringBuffer sbTitle = null;

        try {
            //sFileName = "\\66.134.131.38\public\corpussource\000.html";

            in = new BufferedReader(new FileReader(sFileName));
            boolean bFoundTitleOpen = false;
            sbTitle = new StringBuffer();

            while (in.ready()) {
                textline = in.readLine();
                textlineSpaceCompressed = UtilStrings.replaceStrInStr(textline, " ", "");

                if (!bFoundTitleOpen) {
                    int iLocTitleStart_compressedLine = textlineSpaceCompressed.toLowerCase().indexOf("<title>");
                    int iLocTitleStart = textline.toLowerCase().indexOf("<title>");
                    if (iLocTitleStart_compressedLine >= 0 && iLocTitleStart < 0)
                        Log.FatalError("case not handled - <title> with spaces in it");

                    // is <title> on this line
                    if (iLocTitleStart >= 0) {
                        //Log.log ( "from [" + textline + "] to [" + textlineSpaceCompressed + "]\r\n" );
                        // is </title> on same line
                        int iLocTitleEnd = textline.toLowerCase().indexOf("</title>");
                        if (iLocTitleEnd >= 0) // if end title on same line
                        {
                            // 7 is len of "<title>"
                            sbTitle.append(textline.substring(iLocTitleStart + 7, iLocTitleEnd));
                            break;
                        } else {
                            sbTitle.append(textline.substring(iLocTitleStart + 7));
                        }
                        bFoundTitleOpen = true;
                    }
                } else // already had <title> before this line
                {
                    int iLocTitleEnd_compressedLine = textlineSpaceCompressed.toLowerCase().indexOf("</title>");
                    int iLocTitleEnd = textline.toLowerCase().indexOf("</title>");
                    if (iLocTitleEnd_compressedLine >= 0 && iLocTitleEnd < 0)
                        Log.FatalError("case not handled - </title> with spaces in it");

                    if (iLocTitleEnd >= 0) // if end title on same line
                    {
                        //Log.log ( "from [" + textline + "] to [" + textlineSpaceCompressed + "]\r\n" );
                        sbTitle.append(textline.substring(iLocTitleEnd + 8));
                        break;  // done this file
                    }
                }

            }
            in.close();

        } catch (Exception e) {
            Log.NonFatalError("Exception in getTitleFastFromHTMLFile", e);
            return null;
        } finally {
            try {
                in.close();
            } catch (Exception e) {
            }
            ;
        }
        sTitleReturn = sbTitle.toString();
        //Log.log ( "title in doc [" + sFileName + "] [" + sTitleReturn + "]\r\n" );
        // Log.log ( "title fast [" + sTitleReturn + "]\r\n" );
        return sTitleReturn;
    } // getTitleFastFromHTMLFile

}

