package com.indraweb.webelements;

import com.indraweb.encyclopedia.Topic;
import com.indraweb.execution.Session;
import com.indraweb.util.Log;
import com.indraweb.util.Utilemail2;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class IndraURL implements Cloneable {
    public String sURL;//		= null;
    private String title;//		= null;
    private String date;//	= null;
    private String summary;//	= null;
    private String summaryNewStyle;//	= null;
    private int iSEid;	//	= null;
    private int sequenceNumWithinSEresultSet = -1;
    private boolean bFlagTopicWentOverByteMaxOnRead = false;
    private boolean bFlagThisWasObjectionableContent = false;
    private boolean bdidURLGetReadOK = false;
    private int iNumTextBytesTotal = -1;

    private Hashtable htIntegerSEsFindingThisURL = new Hashtable();  // used only when in dedup-compressed form

    /**
     * return a vec of Integers represnting the SE's that found this URL
     */
    public Vector getVecIntegerSEsFindingThisURL() {
        Enumeration e = htIntegerSEsFindingThisURL.elements();
        Vector vIntegerSEsReturn = new Vector();
        while (e.hasMoreElements()) {
            vIntegerSEsReturn.addElement(e.nextElement());
        }
        return vIntegerSEsReturn;
    }


    // **********************************************
    public IndraURL ( String sURL_, String title_, String date_, String summary_,
                    int iSEid_, int sequenceNum_)
    //int whichSE_, int sequenceNum_, int iNodeIDTargetedCrawl)

            // **********************************************
    {
        sURL = sURL_;
        title = title_;
        date = date_;
        summary = summary_;
        iSEid = iSEid_;
        sequenceNumWithinSEresultSet = sequenceNum_;
        this.addFoundByThisSE( iSEid_ );
    }


    // **********************************************
    public String getURL()
            // **********************************************
    {
        if (sURL == null)
            return "null";
        return sURL;
    }

    // **********************************************
    public String getTitle()
            // **********************************************
    {
        return title;
    }

    // **********************************************
    public String getDate()
            // **********************************************
    {
        return date;
    }

    // **********************************************
    public String getDocSummary(boolean bNewStyle)
            // **********************************************
    {
        if (!bNewStyle)
            return summary;
        else
            return summaryNewStyle;
    }

    // **********************************************
    public int getWhichSE()
            // **********************************************
    {
        return iSEid;
    }



    // **********************************************
    public String printMeString()
            // **********************************************
    {
        return (
                "sURL [" + sURL + "]" +
                "  date [" + date + "]" +
                "  title [" + title + "]" +
                //"  doc summary [" + summary + "]" +
                "\r\n"
                );
    }



    // **********************************************
    public synchronized void addFoundByThisSE ( int iSE_ )
    // **********************************************
    {
        // use a hashtable rather than vector so that if we get multiple signals that the same SE found the same URL, then that also
        // will be condensed properly and deduped
        htIntegerSEsFindingThisURL.put(new Integer(iSE_), new Integer(iSE_));
    }


    // **********************************************
    public void setFlagTopicWentOverByteMAxOnRead()
            // **********************************************
    {
        bFlagTopicWentOverByteMaxOnRead = true;
    }

    // **********************************************
    public boolean getFlagTopicWentOverByteMAxOnRead()
            // **********************************************
    {
        return bFlagTopicWentOverByteMaxOnRead;
    }


    // **********************************************
    public void setNumTextBytesTotal(int i)
            // **********************************************
    {
        iNumTextBytesTotal = i;
    }

    // **********************************************
    public int getNumTextBytesTotal()
            // **********************************************
    {
        return iNumTextBytesTotal;
    }




    // **********************************************
    public void setFlagThisWasObjectionableContent()
            // **********************************************
    {
        bFlagThisWasObjectionableContent = true;
    }

    // **********************************************
    public boolean getFlagThisWasObjectionableContent()
            // **********************************************
    {
        return bFlagThisWasObjectionableContent;
    }

    // **********************************************
    public void setdidURLGetReadOK(boolean b, int iCode)
            // **********************************************
    {
        bdidURLGetReadOK = b;
    }

    // **********************************************
    public boolean getdidURLGetReadOK()
            // **********************************************
    {
        return bdidURLGetReadOK;
    }




}


