/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Oct 31, 2003
 * Time: 9:05:36 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.indraweb.ir.texttranslate.translators;

import com.indraweb.ir.texttranslate.TextTranslatorInterface;

/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Oct 27, 2003
 * Time: 4:45:26 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

import java.util.Hashtable;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.io.DataOutputStream;

import api.multilingual.TSTranslateCorpus;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class TextTranslator_BabelFish implements TextTranslatorInterface
{



    Hashtable htLanguageNameToLanguageInt = new Hashtable();

    public boolean runTest ()
    {

        return false;

    }

    public static String translateText (String sToTranslate, Integer ILanguageIDFrom, Integer ILanguageIDTo, boolean bDebug) throws Exception
    {

        com.indraweb.util.UtilFile.addLineToFile("c://temp/temp.txt", new java.util.Date() + " babel lang [" + ILanguageIDFrom + "] to [" + ILanguageIDTo + "] word [" + sToTranslate+ "]\r\n" );
        String sLangFrom = null;
        String sLangTo = null;

        if ( ILanguageIDFrom.intValue() == TSTranslateCorpus.iLANGID_English)
            sLangFrom = "en";
        else
            throw new Exception ("Babelfish translator can not translate from [" + ILanguageIDFrom + "] [" + ILanguageIDTo + "]" );

        if ( ILanguageIDTo.intValue() == TSTranslateCorpus.iLANGID_French)
            sLangTo = "fr";
        else if ( ILanguageIDTo.intValue() == TSTranslateCorpus.iLANGID_German)
            sLangTo = "de";
        else if ( ILanguageIDTo.intValue() == TSTranslateCorpus.iLANGID_Spanish)
            sLangTo = "sp";
        else
            throw new Exception ("Babelfish translator can not translate from [" + ILanguageIDFrom + "] [" + ILanguageIDTo + "]" );

        if ( ILanguageIDFrom.intValue()  == TSTranslateCorpus.iLANGID_English )
        if ( ILanguageIDTo.intValue() == TSTranslateCorpus.iLANGID_German)
            sLangFrom = "en";
        if ( ILanguageIDFrom.intValue()  == TSTranslateCorpus.iLANGID_English )
            sLangFrom = "en";

        URL url;
        URLConnection urlConn;
        DataOutputStream printout;
        BufferedReader input;
        url = new URL ("http://babelfish.altavista.com/babelfish/tr");
        urlConn = url.openConnection ();
        urlConn.setDoInput (true);
        urlConn.setDoOutput (true);
        urlConn.setUseCaches (false);
        urlConn.setRequestProperty
                ("Content-Type" , "application/x-www-form-urlencoded");

        // Send POST output.
        printout = new DataOutputStream (urlConn.getOutputStream ());

        String content = "doit=" + URLEncoder.encode ("done", "UTF-8") +
                "&tt=" + URLEncoder.encode ("urltext", "UTF-8") +
                "&intl=1" +
                "&urltext=" + URLEncoder.encode (sToTranslate, "UTF-8") +
                "&lp="+sLangFrom+"_"+sLangTo+"";
        printout.writeBytes (content);
        printout.flush ();
        printout.close ();
        // Get response data.
        input  = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
        StringBuilder sb = new StringBuilder ();
        String str;
        int i = 0;
        while (null != ( ( str = input.readLine () ) ))
        {
            //System.out.println (str);
            com.indraweb.util.UtilFile.addLineToFile("c://temp/temp.txt", i + ". babel lang [" + ILanguageIDFrom + "] to [" + ILanguageIDTo + "] str [" + str + "]\r\n" );
            sb.append (str);
            if ( i == 68)
            {
                String sSub = str.substring(str.length()-20);
                for ( int i2 = 0; i2 < sSub.length(); i2++ )
                {
                    char c = sSub.charAt(i2);
                    com.indraweb.util.UtilFile.addLineToFile("c://temp/temp.txt",
                            "  " + i2 + ". char [" + c + "] Character.getNumericValue(c) [" + Character.getNumericValue(c) + "] Character.getType(c) [" + Character.getType(c) + "]\r\n" );                }
            }
            i++ ;
        }
        input.close ();

        String sHTMLResult = sb.toString ();
        String sTranslatedText = extractTranslatedText(sHTMLResult);
        System.out.println ("babel lang [" + ILanguageIDFrom + "] to [" + ILanguageIDTo + "] word [" + sToTranslate+ "] to [" + sTranslatedText + "]" );
        com.indraweb.util.UtilFile.addLineToFile("c://temp/temp.txt", "babel lang [" + ILanguageIDFrom + "] to [" + ILanguageIDTo + "] word [" + sToTranslate+ "] to [" + sTranslatedText + "]\r\n" );
        return sTranslatedText;

/*
        if (sHTMLResult.toLowerCase ().indexOf ("kern") >= 0)
            System.out.print ("FOUND german term for nuclear");
        else
            System.out.print ("DID NOT FIND german term for nuclear");


        long lEnd = System.currentTimeMillis () - lStart;

        System.out.println ("Completed in " + lEnd + " ms.");
*/
    }

    public static String extractTranslatedText(String sPage) throws Exception
    {



        //int iIndex = sPage.indexOf("<tr> <tr> <td bgcolor=");
        //System.out.println ("iIndex [" + iIndex + "]");

       // Pattern p = Pattern.compile("\\s*<frame (.*?)src\\s*=\\s*\"(.*?)\"", Pattern.CASE_INSENSITIVE);
        //Pattern p = Pattern.compile("^<tr> <tr> <td bgcolor=(.*) class=s><Div style=padding:10px; lang=de>(.*)", Pattern.CASE_INSENSITIVE);

        Pattern p = Pattern.compile(
  //"<tr> <tr> <td bgcolor=white  class=s><Div style=padding:10px; lang=de>(.*)", Pattern.CASE_INSENSITIVE);
  "<tr> <tr> <td bgcolor=white  class=s><Div style=padding:10px; lang=..>(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(sPage);
        boolean bResult = m.find();
        String sTranslatedText = null;

        int iLoop = 0;
        while (bResult) {
            iLoop++;
            if ( iLoop > 1 )
                throw new Exception ("too many translate lines in Babelfish output");
            //String sColor = m.group(1);
            sTranslatedText = m.group(1);
            /*
            if ((!sURL.toLowerCase().startsWith("http")) && (!sURL.startsWith("/"))) {
                if ((getDirectory(sRootURL)).endsWith("/")) {
                    sURL = getDirectory(sRootURL)+sURL;
                } else { sURL = getDirectory(sRootURL)+"/"+sURL; }
            }
            if (sURL.startsWith("/")) {
                if ((getBase(sRootURL)).endsWith("/")) {
                    sURL = getBaseWithoutSlash(sRootURL)+sURL;
                } else { sURL = getBase(sRootURL)+sURL; }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf("#");
            if (iPound != -1) { sURL = sURL.substring(0, iPound); }

            try {
                URI u = new URI(sURL);
                u = u.normalize();
                sURL = u.toString();
            } catch (Exception e) {
                System.out.println("Could not normalize URL: "+sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll(new Character((char)0).toString(), "");

            if (hasKey() && (sURL.indexOf(getKey()) != -1)) {
                if (!alreadyVisited(sURL) && (!isImage(sURL))) {
                    System.out.println("Found in frame: "+sURL);
                    vHyperlinks.add(sURL); markVisited(sURL); }
            }
*/
            bResult = m.find();
        }
        if ( sTranslatedText == null )
            throw new Exception ("translation text not found for [" + sPage + "]");
        sTranslatedText = com.indraweb.util.UtilStrings.getsUpToNthOfThis_1based(sTranslatedText, "</div>", 1);

        return sTranslatedText;
    }



}
