package com.indraweb.execution;

import com.indraweb.util.*;

public class PauseHaltManager
{
	public static void testAndPauseIndefinitely_IfFileExists ( String filename, int recheckInXmillisToContinue )
	{
		int i = 0;
		while ( true)  
		{
			i++;
			if ( UtilFile.bFileExists (  filename ) )
			{
				System.out.println ("\r\nMGR pause # [" + i + "] retest for file [" + filename + "] every [" + recheckInXmillisToContinue + " ms\r\n");
				
				clsUtils.pause_this_many_milliseconds ( recheckInXmillisToContinue );
			} 
			else
				break;
		}
	} 
	
	
}
