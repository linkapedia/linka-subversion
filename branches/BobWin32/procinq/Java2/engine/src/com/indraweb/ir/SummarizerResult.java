package com.indraweb.ir;

import java.util.*;

public class SummarizerResult
{
	public Vector vStrDocSnippets = new Vector();
	public Vector vStrDocSnippets_debug = new Vector();
	public Vector vStrDocNuggets = new Vector();
	
	public String getDocSummary ( boolean bDebug )
	{
		StringBuffer sb = new StringBuffer();
		
		Enumeration e = null;
		if ( bDebug )
			e = vStrDocSnippets_debug.elements();
		else
			e = vStrDocSnippets.elements();
			
		int i = 0;
		while ( e.hasMoreElements() )
		{
			if ( i > 0 )
				sb.append ( "... ");
			sb.append ( (String) e.nextElement() );
			i++;
		} 
		return sb.toString();
	} 
}
