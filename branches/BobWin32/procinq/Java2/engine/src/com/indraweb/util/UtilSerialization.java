package com.indraweb.util;

import java.io.*;

public class UtilSerialization
{
    /**
     * Write a serialized version of this GFSDataTable using java serialization.
     * This allows for simple short term, persistent storage of data sets appropriate
     * for testing purposes.
     *@param file a File object representing the file
     *@see readFrom
     */
    public void writeTo( File file, Object o ) 
	throws IOException
    {
		FileOutputStream fostream = new FileOutputStream( file );
		ObjectOutputStream oos = new ObjectOutputStream( fostream );
		oos.writeObject( o );
		fostream.close();
    }

    /**
     * Read a serialized GFSDataTable from disk file represented by the File
     * parameter.
     * @param file
     * @return GFSDataTable
     */
    public static Object readFrom( File file ) 
	throws IOException, ClassNotFoundException
    {
		FileInputStream fistream = new FileInputStream( file );
		ObjectInputStream ois = new ObjectInputStream( fistream );
		Object o = ois.readObject();
		return o;
    }

}

/*
    /**
     * Write a serialized version of this GFSDataTable using java serialization.
     * This allows for simple short term, persistent storage of data sets appropriate
     * for testing purposes.
     *@param file a File object representing the file
     *@see readFrom
     * /
    public void writeTo( File file ) 
	throws IOException
    {
	FileOutputStream fostream = new FileOutputStream( file );
	ObjectOutputStream oos = new ObjectOutputStream( fostream );
	oos.writeObject( this );
	fostream.close();
    }

    /**
     * Read a serialized GFSDataTable from disk file represented by the File
     * parameter.
     * @param file
     * @return GFSDataTable
     * /
    public static GFSDataTable readFrom( File file ) 
	throws IOException, ClassNotFoundException
    {
	FileInputStream fistream = new FileInputStream( file );
	ObjectInputStream ois = new ObjectInputStream( fistream );
	GFSDataTable data = (GFSDataTable) ois.readObject();
	return data;
    }

*/