package com.indraweb.signatures;

import com.indraweb.util.UtilSets;
import com.indraweb.encyclopedia.StringAndCount;
import com.indraweb.ir.Thesaurus;

import java.util.*;

public class SignaturesContainer
{

//    public static int iTERMTYPE_BASETERM = 1;
//    public static int iTERMTYPE_THESEXPANDED_WHOLE = 2;
//    public static int iTERMTYPE_THESEXPANDED_PHRASEELEMENT = 3; // NA for titles
//
    private Vector vStr = null;
    private Vector vInt = null;
    private int iNumTitleTermsIfNotItselfATitle = 0;



    public SignaturesContainer ( ) throws Exception
    {

        this.vStr = new Vector();
        this.vInt = new Vector();
    }

    public SignaturesContainer ( Vector vStr, Vector vInt ) throws Exception
    {

        if ( vStr.size() != vInt.size() )
            throw new Exception ("must be same size vectors");
        this.vStr = vStr;
        this.vInt= vInt;
    }

    public void removeElementAt (int i)
    {
        String sRemoved = (String) vStr.elementAt(i);
        vStr.removeElementAt(i);
        vInt.removeElementAt(i);
    }

    private void add (String s, Integer I,
                      boolean bThesExpandAndSigFilter,
                      SignatureParms sigParms,
                      Thesaurus thesaurus ) throws Exception
    {

        if ( bThesExpandAndSigFilter )
        {
            String[] sArr = new String[1];
            sArr[0] = s;
            sArr = SigExpandersTitleThesaurus.thesExpandStringArray(
                        sArr,
                        false,
                        thesaurus,
                        sigParms
                );

            for ( int i = 0; i < sArr.length; i++)
            {
                String sSigTermToInsert = null;
                //if ( bThesExpandAndSigFilter )  // if expanding then on way out of db
                    sSigTermToInsert = sigParms.signatureTermQualifierModifier ( sArr[i] );
                //else
                //    sSigTermToInsert = sArr[i];

                if ( sSigTermToInsert != null )
                {
                    vStr.addElement(sArr[i]);
                    vInt.addElement(I);
                }
            }
        }
        else
        {
            vStr.addElement(s);
            vInt.addElement(I);
        }
    }

//    public void addAll (Vector vStr, Vector vInt ) throws Exception
//    {
//        Enumeration enum = vStr.elements();
//        int i = 0;
//        while ( enum.hasMoreElements() )
//        {
//            add ( (String) enum.nextElement(), (Integer) vInt.elementAt(i), false, null, null );
//            i++;
//        }
//    }


    public void addAll (Vector vStr, Vector vInt, SignatureParms sigParms,
                            boolean bThesExpand, Thesaurus thesaurus ) throws Exception
    {
        Enumeration enumeration = vStr.elements();
        int i = 0;
        while ( enumeration.hasMoreElements() )
        {
            add ( (String) enumeration.nextElement(), (Integer) vInt.elementAt(i), bThesExpand, sigParms, thesaurus );
            i++;
        }
    }

    public void addAll ( SignaturesContainer sigCont, int iLocation,
                 SignatureParms sigParms,
                 boolean bThesExpand, Thesaurus thesaurus ) throws Exception
    {
        Vector vStrIncoming = sigCont.getVStr();
        Vector vIntIncoming = sigCont.getVInt();
        int iSize = vStrIncoming.size();
        for ( int i = 0; i < iSize; i++ )
        {
            add ( (String) vStrIncoming.elementAt(i), (Integer) vIntIncoming.elementAt(i),
                    bThesExpand, sigParms, thesaurus);
        }
   }

    public void addAll (String[] sArr,
                        boolean bThesExpand,
                        Thesaurus thesaurus,
                        Integer ICount,
                        SignatureParms sigParms ) throws Exception
    {
        for(int i = 0; i < sArr.length; i++)
        {
            add ( sArr[i], ICount, bThesExpand, sigParms, thesaurus );
        }

    }

    // as a single full title for example
    public void addAsSingleTermWithThesExpandButNotBroken (
                        String s,
                        Integer ICount,
                        Thesaurus thesaurus,
                        SignatureParms sigParms,
                        boolean bIncludeThisAntecedentEGTitleAsIs ) throws Exception
    {
        if ( bIncludeThisAntecedentEGTitleAsIs )
            add ( s, ICount, false, sigParms, null );    // keep whole title as a sig for ex.
        HashSet hsSyns = thesaurus.getSynWholeWord(s);
        if ( hsSyns != null )
        {
            Iterator iter = hsSyns.iterator();
            while ( iter.hasNext () )
            {
                String sFullWordSynToInsert = ( String) iter.next ();
                String sSigTermToInsert = sigParms.signatureTermQualifierModifier ( sFullWordSynToInsert );
                if ( sFullWordSynToInsert != null )
                    add ( sSigTermToInsert, ICount, false, null, null );

            }
        }
    }

    //api.Log.Log ("parent title post thes [" + UtilSets.sArrToStr(sArrNodeTitleParentCleanedStopped_thesExpanded)+"]" );



    public Vector getVStr()
    {
        return vStr;
    }
    public Vector getVInt()
    {
        return vInt;
    }

    public int getMax()
    {
        return UtilSets.maxOfIntVector (vInt);
    }

    public String toString()
    {
        return "iNumTitleTermsIfNotItselfATitle [" + iNumTitleTermsIfNotItselfATitle + "]" + StringAndCount.toString(vStr, vInt);
    }

    public int size()
    {
        return vStr.size();
    }

    public void insertAt ( SignaturesContainer sigCont )
    {


    }

    public void deDup ()
    {
        Hashtable htWordsAndTerms = new Hashtable();
        for ( int i = 0; i < vStr.size(); i++)
        {
            String s = (String) vStr.elementAt(i);
            Integer IPrevValue = (Integer) htWordsAndTerms.get(s);
            if ( IPrevValue != null )
            {
                int iCnt = ((Integer) vInt.elementAt(i)).intValue();
                if ( iCnt > IPrevValue.intValue())
                    htWordsAndTerms.put(s, new Integer (iCnt));
            }
            else // if never been seen
            {
                htWordsAndTerms.put(s, vInt.elementAt(i));
            }
        }

        // now we have max counts per term ... keep same order
        Vector vStrSaved = vStr;
        vStr = new Vector();
        vInt= new Vector();
        Enumeration enumStrInOrder = vStrSaved.elements() ;
        int i = 0;
        int iNumTitleTermsIfNotItselfATitle_final = iNumTitleTermsIfNotItselfATitle;
        while ( enumStrInOrder.hasMoreElements())
        {
            String sTerm = (String) enumStrInOrder.nextElement();
            Integer ICountToUseRemaining = (Integer) htWordsAndTerms.get (sTerm);
            if ( ICountToUseRemaining != null )
            {
                vStr.addElement(sTerm);
                vInt.addElement(ICountToUseRemaining);
                htWordsAndTerms.remove(sTerm);
            }
            else // if eliminating dups from parent title to self or within either ... decrement
            {
                if ( i < iNumTitleTermsIfNotItselfATitle )
                    iNumTitleTermsIfNotItselfATitle_final--;

            }
            i++;
        }
        iNumTitleTermsIfNotItselfATitle = iNumTitleTermsIfNotItselfATitle_final;
    }

    public void setTitleTermCount(int iTitleCountInTerms)
    {
        this.iNumTitleTermsIfNotItselfATitle = iTitleCountInTerms;
    }
    public int getTitleTermCount()
    {
        return iNumTitleTermsIfNotItselfATitle;
    }
}
