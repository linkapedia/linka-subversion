package com.indraweb.signatures;

import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.encyclopedia.StringAndCount;
import com.indraweb.util.Log;
import com.indraweb.util.UtilStrings;

public class DBAccecssSigWords {

    public static final int CONST_WHICHTITLE_UNCLEAN = -2;
    public static final int CONST_WHICHTITLE_CLEAN = -1;

    // ***********************************************************
    public static String getCorpusDocTitleFromDocID(long lDocID, int iWhichTitle, java.sql.Connection dbcWords)
            throws Exception // ***********************************************************
    {
        String sql = "select TitleOrLargeSigWord from iwSigWords where "
                + " DocID = " + lDocID + " and SigWordIndex = " + iWhichTitle;

        String title = JDBCIndra_Connection.executeQueryAgainstDBStr(sql, dbcWords);
        return title;
    }

    // --------------------------------------------------------------------------------------
    public static long getCorpusDocWordCountFromDocID(long lDocID, java.sql.Connection dbcWords)
            throws Exception // --------------------------------------------------------------------------------------
    {
        String sql = "select WordCountInDoc from iwSigWords where "
                + " DocID = " + lDocID + " and SigWordIndex = -2";

        long l = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbcWords);
        return l;
    }

    // --------------------------------------------------------------------------------------
    public static long getNumSigTermsFromDocID(long lDocID, java.sql.Connection dbcWords)
            throws Exception // --------------------------------------------------------------------------------------
    {
        String sql = "select max (sigWordIndex) from iwSigWords where "
                + " DocID = " + lDocID;

        long l = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbcWords);
        if (l == 0) {
            return -1;
        }

        return l + 1;
    }
    static Integer semaphoreReserveUrlAndDocID = new Integer(1);

    // ***********************************************************
    public static int insertToDBSigWords(
            StringAndCount[] scArrSortedSignatWordAndCountThisTopic,
            java.sql.Connection dbcCentral,
            long lNodeID_,
            int iNumParentTitleTermAsSigs,
            int iMaxSigTermsPerNode) throws Exception // ***********************************************************
    {
        //api.Log.Log ("scarr pre db insert sigs node [" + lNodeID_ + "] [" + StringAndCount.toString(scArrSortedSignatWordAndCountThisTopic) + "]");
        // delete previous signature words
        // insert individual signature words
        String sql = null;
        int iNumInserts = 0;
        // no parent titles to the DB ...
        for (int i = 0; i < scArrSortedSignatWordAndCountThisTopic.length
                && (i < iMaxSigTermsPerNode
                || iMaxSigTermsPerNode < 0); i++) {


            // don't re-add a term already put in as part of parent node title
            //boolean bIsWordNumeric = com.indraweb.util.UtilStrings.isNumericFasterFirstCharBased(sWordTrunced);
            //if ( !UtilStrings.isStringNothingButTheseChars ( sWordTrunced, cArrWordsMadeOfOnlyTheseWontBeInserted) )
            {
                String sSigTerm = UtilStrings.replaceStrInStr(scArrSortedSignatWordAndCountThisTopic[i].word.toLowerCase(), "'", "''");
                sql = "insert into signature "
                        + " ( nodeID, SignatureWord, SignatureOccurences ) "
                        + " Values ( "
                        + lNodeID_ + ","
                        + "'" + sSigTerm + "'," + // SigWord
                        scArrSortedSignatWordAndCountThisTopic[i].count + // WordCountInDoc
                        ")";
                // System.out.print (i+ ". sig insert sql [" + sWord + "]\r\n");
                try {
                    iNumInserts++;
                    JDBCIndra_Connection.executeInsertToDB(sql, dbcCentral, true);
                } catch (Exception e) {
                    Log.log(e.getMessage() + " error on insertToDBSigWords [" + sql + "]");
                }
            }
        }

        return iNumInserts;
    }
    static private boolean hasWarningBeenGiven_getSignatureFromDocID = false;

    // ***********************************************************
    public static long getNumSigTermsFromNodeID(long lNodeID_, java.sql.Connection dbc)
            throws Exception // ***********************************************************
    {

        String sql = "select count (*) from signature where nodeid =  " + lNodeID_;

        long l = JDBCIndra_Connection.executeQueryAgainstDBLong(sql, dbc);
        return l;
    }
}
