package com.indraweb.html;


import com.indraweb.execution.Session;
import com.indraweb.util.Log;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.clsUtils;

import java.io.InputStream;
import java.util.Vector;


/* maintains an object array of HTMLElement objects representing an HTML document */

public class HTMLMemDoc {

    private HTMLElement[] HTMLElements = null;

    private String URLName = null;
    private String topicNameSpaces = null;
    private int numElements = -1;
    private InputStream is = null;
    private long lCacheID_originating = -1;


    // ACCESSORS

    public String getURLName() {
        return URLName;
    }

    public int getNumElements() {
        return HTMLElements.length;
    }

    public int getlastIndex() {
        return HTMLElements.length - 1;
    }

    public String getFullTokenizedHTMLLineLine(int i) {
        return HTMLElements[i].getFullTokenizedHTMLLineLine();
    }

    public int getIntHTMLTypeMine(int i) {
        return HTMLElements[i].getIntHTMLTypeMine();
    }

    public String getsHTMLType(int i) throws Exception {
        if (i == -1) {
            return "TYPE NOT INITIALIZED";
        } else if ((i >= 0) && (i < HTMLElements.length)) {
            return HTMLElements[i].getsHTMLType();
        } else
            throw new Exception("TYPE HTMLELEM OUT OF BOUNDS");

    }


    public String getsHTMLTypeDecode(int i) {
        return HTMLElements[i].getTypeDecodeToString();
    }

    public String getsHTMLTag(int i) {
        return HTMLElements[i].getsHTMLTag();
    }

    public String getsHTMLAttrs(int i) {
        return HTMLElements[i].getsHTMLAttrs();
    }

    public String getTopicNameSpaces() {
        return topicNameSpaces;
    }


    public String getsHTMLAttrsAsHrefOnly(int i) {


        HTMLElement h = HTMLElements[i];
        String s = h.getsHTMLTextOrURL();
        return s;
    }

    public String getsHTMLAttrsAsAnchorOnly(int i) {

        if (!isOfTypeMine(i, UtilHTML.HTMLTagType_OpenA)) {
            Log.FatalError("HTMLMemDoc.getHTMLAttrsAsHrefOnly incorrect type - no anchor here");
        }
        String anchorLarge = HTMLElements[i].getsHTMLAttrs();

        return UtilStrings.getStringBetweenThisAndThat(anchorLarge, "{name=", "}");

    }

    public String getsHTMLText(int i) {
        return HTMLElements[i].getsHTMLText();
    }

    public String getsHTMLAll(int i) {
        return HTMLElements[i].getsHTMLAll();
    }

    public String getsHrefURL(int i) {
        return HTMLElements[i].getsHrefURL();
    }

    public String getsHrefText(int i) {
        int iEndPoint = UtilHTMLMem.findNextOfType(this, UtilHTML.HTMLTagType_CloseA, i, -1, true);
        String s = UtilHTMLMem.getTextBlock(this, i, iEndPoint, null);
        return s;
    }

    public String getsHTMLTextOrURL(int i) {
        return HTMLElements[i].getsHTMLTextOrURL();
    }

    public HTMLElement[] getElements() {
        return HTMLElements;
    }

    public HTMLElement getElement(int i) {
        return HTMLElements[i];
    }

    public long getCacheID_originating() {
        return lCacheID_originating;
    }


    // CONSTRUCTOR

    public HTMLMemDoc(UrlHtmlAttributes urlHtmlAttributesContext_,
                      String topicNameSpaces_,
                      int iNumRetriesIfSteamNotCreated_,
                      int iTimeoutTimedSocket)
            throws Exception {
        //Log.log ("hbk 2001 06 11 start constructor htmlmemdoc url [" + urlHtmlAttributesContext_.getsURL() + "]\r\n");
        long tStart = System.currentTimeMillis();
        Vector HTMLElementsVec = null;
        try {

            URLName = urlHtmlAttributesContext_.getsURL();
            topicNameSpaces = topicNameSpaces_;
            int retryCount = 0;
            boolean bSucceeded = false;
            while (retryCount <= iNumRetriesIfSteamNotCreated_) {
                urlHtmlAttributesContext_.setURL(URLName);

                Vector vInputStreamAndHTMLTokenizer =
                        UtilHTML.HTMLTokenizerFromURL(
                                urlHtmlAttributesContext_,
                                iNumRetriesIfSteamNotCreated_,
                                iTimeoutTimedSocket);


                is = (InputStream) vInputStreamAndHTMLTokenizer.elementAt(0);
                // cache may have been updated as I have seen here
                // lCacheID_originating = WebCacheReads.getCacheIDFromURL ( urlHtmlAttributesContext_.getsURL() , dbcMain );
                com.ms.util.HTMLTokenizer lHTMLTok = (com.ms.util.HTMLTokenizer) vInputStreamAndHTMLTokenizer.elementAt(1);

                HTMLElementsVec = new Vector();

                int index = 0;
                int iNumInaRowInEror = 0;
                try {
                    while (lHTMLTok.hasMoreTokens()) {
                        int nextTokType = -2;
                        try {
                            // long tTokenStart = System.currentTimeMillis();
                            nextTokType = lHTMLTok.nextToken();
                            // Log.log ("lHTMLTok.nextToken time [" + (System.currentTimeMillis() - tTokenStart ) + "]\r\n");
                            iNumInaRowInEror = 0;
                        } catch (Exception e) {
                            iNumInaRowInEror++;
                            if (iNumInaRowInEror > 20) {
                                //WebCacheWrites.deleteCacheID ( lCacheID_originating, dbcMain );
                                Log.log("error hit this 20 html in a row error \r\n");
                                throw new Exception_retry();
                            }
                            Log.NonFatalError("HTMLFileToMemory.eatHTMLText error in lHTMLTok.nextToken() e.getmessage : [" + e.getMessage() + "]\r\n");
                            clsUtils.pause_this_many_milliseconds(300); // to avoid fast spin loop where I can't get in
                        }

                        HTMLElementsVec.addElement(new HTMLElement(nextTokType, lHTMLTok.toString()));
                        // System.out.println ( index + ". " +  lHTMLTok.toString());
                        index++;


                    } // while ( lHTMLTok.hasMoreTokens () )
                } catch (Exception e) {
                    if (is != null)
                        is.close();
                    retryCount++;
                    continue;
                }
                is.close();
                bSucceeded = true;
                break; // success
            } // retry loop

            if (!bSucceeded)
                Log.FatalError("ERROR - unable to create htmlmemdoc for " +
                        " url [" + URLName + "]" +
                        " lastest cached ID [" + lCacheID_originating + "]\r\n"
                );

            HTMLElements = new HTMLElement[HTMLElementsVec.size()];
            HTMLElementsVec.copyInto(HTMLElements);
            numElements = HTMLElements.length;
        } catch (Exception_retry e) {
            Log.FatalError("Exception_retry inside HTMLMemDoc constructor e.getmessage [" + e.getMessage() + "]\r\n");
        } finally {
            if ((System.currentTimeMillis() - tStart) > 30000)
                Log.log("long constructor htmlmemdoc time ms [" + (System.currentTimeMillis() - tStart) + "] [" +
                        urlHtmlAttributesContext_.getsURL() +
                        "]\r\n");
        }
    }  // public HTMLMemDoc (	UrlHtmlAttributes urlHtmlAttributesContext_,

    class Exception_retry extends Exception {
    }

    public void writeToFile(String filename) {
        UtilHTMLMem.writeToFile(filename, this);
    }

    public boolean isOfTypeMine(int i, int typeMineTest) {
        if (HTMLElements[i].getIntHTMLTypeMine() == typeMineTest)
            return true;
        else
            return false;
    }

    public Vector getVecOfElems() {
        Vector v = new Vector();

        for (int i = 0; i < HTMLElements.length; i++) {
            v.addElement(HTMLElements[i]);
        }

        return v;

    }

    public void writeToFile(String filename, int[] elemsOfInterest) {
        UtilHTMLMem.writeToFile(filename, this, elemsOfInterest);
    }

    // ******************************************************************************************
    public int[] findTypes(
            // BLOCK******************************************************************************************
            int[] types,
            int iStart,
            int iEnd,
            boolean fatalIfNotFound) {

        Vector v = new Vector();

        if (iEnd == 0 || iEnd == -1)
            iEnd = this.getlastIndex();

        for (int i = iStart; i <= iEnd; i++) {
            if (UtilSets.isIntInIntArray(this.getIntHTMLTypeMine(i), types))
                v.addElement(new Integer(i));
        }

        if (fatalIfNotFound && v.size() == 0)
            Log.FatalError("findTypes : no element found of type " + this.getURLName());

        return UtilSets.convertVectorTo_intArray(v);
    }


    public static HTMLMemDoc getHtmlMemDocFromMinInputs(String sURL)
            throws Exception {

        UrlHtmlAttributes urlHtmlAttributesContext = new UrlHtmlAttributes(sURL);
        HTMLMemDoc memDoc = new HTMLMemDoc(urlHtmlAttributesContext,
                "test",
                0, Session.cfg.getPropInt("socketTimeoutMSWebDocs")); // 0 retries assume success first time
        return memDoc;

    }


}
