package com.indraweb.database;

import com.indraweb.util.Log;
import java.sql.*;
import java.util.Vector;

public class JDBCIndra_Connection {

    public static int numDBReads = 0;
    public static int numDBupdates = 0;
    public static long numDBrecordsReturned = 0;

    public static int executeSQLInt(String SQL, Connection dbc)
            throws Exception {
        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery(SQL);
        long lresult = -1;
        while (rs.next()) {
            lresult = rs.getLong(1);
        }
        rs.close();
        stmt.close();
        return (int) lresult;
    }

    // ----------------------------------------------------------
    public static String executeQueryAgainstDBStr(String aSQL, Connection dbc)
            throws Exception {

        // ----------------------------------------------------------

        if (dbc.isClosed()) {
            Log.FatalError("jdbcindra_connection hbk hbk 1 dbc closed [" + dbc + "]");
        }
        numDBReads++;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(aSQL);

            String sReturnVal = null;
            int i = 0;
            while (rs.next()) {
                //MessageBox(0, "in FillListviewWithWebInfoForDocAndNode looping",         "This message box from Java", 0);   
                sReturnVal = rs.getString(1);
                i++;
                numDBrecordsReturned++;
            }
            return sReturnVal;
        } catch (SQLException ex) {
            Log.printStackTrace_mine(192, ex);
            Log.FatalError("SQLException 1 in runQueryAgainstDB: " + ex.getMessage(), ex);
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (rs != null) {
                rs.close();
                rs = null;
            }
        }
        return null;

    }	 // runQueryAgainstDV ... ()

    public static long executeQueryAgainstDBLong(String aSQL, Connection dbc) throws Exception {
        if (dbc.isClosed()) {
            Log.FatalError("jdbcindra_connection hbk hbk 2 dbc closed [" + dbc + "]");
        }

        //Log.log ( aSQL + "\r\n");
        numDBReads++;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(aSQL);

            long lReturnVal = -1;
            if (rs.next()) {
                lReturnVal = rs.getLong(1);
            }
            return lReturnVal;
        } catch (Exception e) {
            Log.FatalError("error in ExecuteQueryAgainstDBlong sql [" + aSQL + "]", e);
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (rs != null) {
                rs.close();
                rs = null;
            }
        }
    }

    public static float executeQueryAgainstDBFloat(String aSQL, Connection dbc) throws Exception {
        if (dbc.isClosed()) {
            Log.FatalError("jdbcindra_connection hbk hbk 5 dbc closed [" + dbc + "]");
        }

        //Log.log ( aSQL + "\r\n");
        numDBReads++;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(aSQL);

            float fReturnVal = -1;
            if (rs.next()) {
                fReturnVal = rs.getFloat(1);
            }

            return fReturnVal;
        } catch (Exception e) {
            Log.FatalError("error in ExecuteQueryAgainstDBfloat sql [" + aSQL + "]", e);
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (rs != null) {
                rs.close();
                rs = null;
            }
        }

    }	 // public static long ExecuteQueryAgainstDBlong (String aSQL, IndraDBContext dbc,

    public static Vector executeQueryAgainstDBVector(String aSQL, Connection dbc) throws Exception {
        return executeQueryAgainstDBVector(aSQL, dbc, -1);

    }

    public static Vector executeQueryAgainstDBVector(String aSQL, Connection dbc, int iMaxNumRows) throws Exception {
        if (dbc.isClosed()) {
            Log.FatalError("jdbcindra_connection hbk hbk 3 dbc closed [" + dbc + "]");
        }
        Statement stmt = null;
        ResultSet rs = null;
        numDBReads++;
        try {
            if (dbc == null) {
                Log.FatalError("ExecuteQueryAgainstDBVector do not want null");
            }

            stmt = dbc.createStatement();
            rs = stmt.executeQuery(aSQL);
            Vector outVecReturnData = new Vector();

            ResultSetMetaData rsMD = rs.getMetaData();

            int iColumnCount = rsMD.getColumnCount();
            if (iColumnCount > 1 && iMaxNumRows > 0) {
                throw new Exception("invalid mode in vector get data iColumnCount > 1 && iMaxNumRows > 0 [" + aSQL + "]");
            }

            int iRowNum = 0;
            while (rs.next() && (iRowNum < iMaxNumRows || iMaxNumRows < 0)) {
                for (int i = 0; i < iColumnCount; i++) {
                    outVecReturnData.addElement(rs.getObject(i + 1));
                }
                iRowNum++;

            }
            return outVecReturnData;
        } catch (Exception e) {
            Log.FatalError("error in ExecuteQueryAgainstDBVector [" + aSQL + "]", e);
        } catch (Throwable e) {
            Log.FatalError("error throwable [" + aSQL + "]");
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (rs != null) {
                rs.close();
                rs = null;
            }
        }
        return null;

    }	 // ExecuteQueryAgainstDBVector ...

    public static Vector executeDBQueryVecOfVecsReturned(String aSQL, Connection dbc, int iNumRecordsWanted) throws SQLException {
        if (dbc.isClosed()) {
            Log.FatalError("jdbcindra_connection hbk hbk 4 dbc closed [" + dbc + "]");
        }

        numDBReads++;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(aSQL);

            Vector outVecofVecsOfReturnData = new Vector();
            int iNumRowsReturnedThusFar = 0;
            ResultSetMetaData rsMD = rs.getMetaData();

            while (rs.next()) {
                if (iNumRowsReturnedThusFar == 0) {
                    for (int i = 0; i < rsMD.getColumnCount(); i++) {
                        outVecofVecsOfReturnData.addElement(new Vector());
                    }
                }
                for (int i = 0; i < rsMD.getColumnCount(); i++) {
                    ((Vector) outVecofVecsOfReturnData.elementAt(i)).addElement(rs.getObject(i + 1));
                }


                iNumRowsReturnedThusFar++;
                if (iNumRowsReturnedThusFar % 1000 == 0) {
                    Log.log("getting rec #[" + iNumRowsReturnedThusFar + "] on sql [" + aSQL + "]\r\n");
                }

                if (iNumRecordsWanted > 0
                        && iNumRowsReturnedThusFar == iNumRecordsWanted) {
                    break;
                }
            }

            numDBrecordsReturned += outVecofVecsOfReturnData.size();
            return outVecofVecsOfReturnData;
        } catch (Exception e) {
            Log.FatalError("error in ExecuteDBQueryVecOfVecsReturned [" + aSQL + "]", e);
        } catch (Throwable e) {
            Log.FatalError("error throwable [" + aSQL + "]");
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (rs != null) {
                rs.close();
                rs = null;
            }
        }
        return null;

    }	 // ExecuteDBQueryVecOfVecsReturned ... ()

    public static int executeUpdateToDB(String aSQL,
            Connection dbc,
            boolean bCommit,
            int iMinUpdates)
            throws Exception {
        return executeUpdateToDB(aSQL, dbc, bCommit, false, iMinUpdates);
    }

    public static int executeUpdateToDB(String aSQL,
            Connection dbc,
            boolean bCommit,
            boolean bFatalIfThrowable,
            int iMinUpdates)
            throws Exception {
        Statement stmt = null;
        int iNumUpdates = -1;
        if (dbc.isClosed()) {
            Log.FatalError("jdbcindra_connection hbk hbk 5 dbc closed [" + dbc + "]");
            throw new Exception("jdbcindra_connection hbk hbk 5 dbc closed [" + dbc + "]");

        }
        // hbk timer Timer timer = Profiler.getTimer("method insertOrUpdateNodeDocInfotoDB->executeUpdateToDB", true);
        try {
            stmt = dbc.createStatement();
            iNumUpdates = stmt.executeUpdate(aSQL);
            if (iNumUpdates < iMinUpdates) {
                Log.log("update did not fix min number records [" + aSQL + "]\r\n");
                throw new Exception("update did not fix min number records [" + aSQL + "]");
            }
            if (bCommit) {
                dbc.commit();
            }
        } catch (Throwable e) {
            if (bFatalIfThrowable) {
                Log.FatalError("throwable in executeUpdateToDB [" + aSQL + "]", e);
            } else {
                throw new Exception("error 2 in executeUpdateToDB [" + aSQL + "] " + e.getMessage());
            }
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        return iNumUpdates;
    }

    // ----------------------------------------------------------
    public static void executeInsertToDB(String aSQL,
            Connection dbc,
            boolean bCommit)
            throws Exception {
        executeInsertToDB(aSQL, dbc, bCommit, true);
    }

    // ----------------------------------------------------------
    public static void executeInsertToDB(String aSQL,
            Connection dbc,
            boolean bCommit,
            boolean bFatalIfThrowable)
            throws Exception {
        // ----------------------------------------------------------
        Statement stmt = null;
        //Log.logClear("JDBCIndra_Connection.java executeInsertToDB aSQL [" + aSQL + "]\r\n");
        if (dbc.isClosed()) {
            Log.FatalError("jdbcindra_connection hbk hbk 7 dbc closed [" + dbc + "]");
        }
        // hbk timer Timer timer = Profiler.getTimer("method insertOrUpdateNodeDocInfotoDB->executeInsertToDB", true);
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(aSQL);
            if (bCommit) {
                dbc.commit();
            }
        } catch (Throwable e) {
            if (bFatalIfThrowable) {
                Log.FatalError("throwable in executeUpdateToDB [" + aSQL + "]", e);
            } else {
                throw new Exception("error 3 in executeUpdateToDB [" + aSQL + "] " + e.getMessage());
            }
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }
}
