package com.iw.apishell;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

public class User {
    // User attributes
    private String ID;
    private String Email;
    private String Password;
    private String Name;
    private String EmailStatus;
    private String ScoreThreshold;
    private String ResultsPerPage;
    private String UserStatus;
    private String SessionKey;

    // constructor(s)
    public User (HashTree ht) {
        ID = (String) ht.get("ID");
        Password = (String) ht.get("PASSWORD");
        Email = (String) ht.get("EMAIL");
        Name = (String) ht.get("NAME");
        EmailStatus = (String) ht.get("EMAILSTATUS");
        ScoreThreshold = (String) ht.get("SCORETHRESHOLD");
        ResultsPerPage = (String) ht.get("RESULTSPERPAGE");
        UserStatus = (String) ht.get("USERSTATUS");
        SessionKey = (String) ht.get("KEY");
    }

    // accessor functions
    public String getID() { return ID; }
    public String getEmail() { return Email; }
    public String getPassword() { return Password; }
    public String getName() { return Name; }
    public String getEmailStatus() { return EmailStatus; }
    public String getScoreThreshold() { return ScoreThreshold; }
    public String getResultsPerPage() { return ResultsPerPage; }
    public String getUserStatus() { return UserStatus; }
    public String getSessionKey() { return SessionKey; }
}
