package com.iw.repository;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.net.*;
import org.textmining.text.extraction.WordExtractor;

import com.iw.system.*;
import com.iw.fcrawl.*;
import com.iw.utils.PDFUtils;

// Google is an extension of the WebCache repository.   Links have already been
// extracted from Google, all this program needs to do is walk down a list of URLs
// and save each file to disk while updating a file based index.

public class Google extends WebCache {
    public String GoogleBase = "/base.txt";

    // constructor: WebCache specific constructor
	public Google (HashTree ht, HashTree ht2) {
        // set attributes
        SetName((String) ht.get("REPOSITORYNAME"));
        SetLocation((String) ht.get("REPOSITORYLOCATION"));

        File fProxyConfigurationFile = null;
        if (ht2.containsKey("proxyconfig")) { fProxyConfigurationFile = new File((String) ht2.get("proxyconfig")); }
		if ((fProxyConfigurationFile != null) && (fProxyConfigurationFile.exists())) {
            System.out.println("Loading proxy configuration file..");
            try { ReadProxyFile(fProxyConfigurationFile); } catch (Exception e) {}
		}

        if (vProxies.size() > 0) { bUseProxies = true; }
        if (ht2.containsKey("interval")) { lInterval = 1000 * new Integer((String) ht2.get("interval")).longValue(); }

		SetArguments(ht2);

        CacheFile = GetLocation()+"/crawlcachedata.txt"; System.out.println("Cache file: "+CacheFile);
        QueueFile = GetLocation()+"/queuedata.txt"; System.out.println("Queue file: "+CacheFile);
        GoogleBase = GetLocation()+"/base.txt"; System.out.println("Base file: "+GoogleBase);

        if (ht2.containsKey("restore")) { RestoreQueueFromFile();  }
        else { getGoogleURLs(); }
	}

    // Specific web crawl routine to Pub Med abstracts!  Fill the crawl counter serially in descending order.
    @Override
	public void GetNewObjects(String sURL, String sDateLastCapture, boolean bRecurse) {
        while (vHyperlinks.size() > 0) {
            iCrawlCounter++;

            // on every 20th result, write the current crawl queue out to disk
            if ((iCrawlCounter % 20) == 0) {
                try {
                    File f = new File(QueueFile);
                    if (f.exists()) { f.delete(); }
                    synchronized (f) {
                        PrintWriter out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
                        Enumeration eV = vHyperlinks.elements();
                        while (eV.hasMoreElements()) { out.println((String) eV.nextElement()); }
                        out.close();
                    }
                } catch (Exception e) { System.out.println("Warning: could not write queue file.\n"); e.printStackTrace(System.out); }
            }

            String[] sURLelements = new String[3];
            synchronized (vHyperlinks) {
                sURLelements = ((String) vHyperlinks.firstElement()).split("\t");
                vHyperlinks.remove(vHyperlinks.firstElement());
            }
            boolean bWriteToFile = true;

            if (sURLelements[0].toUpperCase().indexOf("GOOGLE") != -1) bWriteToFile = false;
            String page = getData(sURLelements, bWriteToFile);

            if (!bWriteToFile) extractHyperTextLinks(page, sURLelements[0]);
        }

        System.out.println(iCrawlCounter+" new web pages found on web site "+sURL);
        System.out.println("");
        System.out.flush();
    }

    // extract all HTML hyperlinks from a given document and return in a vector
    public static void extractHyperTextLinks(String page, String rootURL) {
        int lastPosition = 0;           // position of "http:" substring in page

        //<a href=http://www.dsd.es.northropgrumman.com/rf/ASPIS.html onmousedown
        //Pattern p = Pattern.compile("\\s*href\\s*=\\s*\"*?([^>\" ]+)\"*?.*?>{1}?(.*?)(</a>{1}?)", Pattern.CASE_INSENSITIVE);
        Pattern p = Pattern.compile("\\s*href=http(.*?) onmousedown", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(page); boolean bResult = m.find();

        while (bResult) {
            String sURL = "http" + m.group(1);

            if ((!sURL.toLowerCase().startsWith("http")) && (!sURL.startsWith("/"))) {
                if ((getDirectory(rootURL)).endsWith("/")) {
                    sURL = getDirectory(rootURL)+sURL;
                } else { sURL = getDirectory(rootURL)+"/"+sURL; }
            }
            if (sURL.startsWith("/")) {
                if ((getBase(rootURL)).endsWith("/")) {
                    sURL = getBaseWithoutSlash(rootURL)+sURL;
                } else { sURL = getBase(rootURL)+sURL; }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf("#");
            if (iPound != -1) { sURL = sURL.substring(0, iPound); }

            try {
                URI u = new URI(sURL);
                u = u.normalize();
                sURL = u.toString();
            } catch (Exception e) {
                System.out.println("Could not normalize URL: "+sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll(new Character((char)0).toString(), "");

            if (!alreadyVisited(sURL) && (!isImage(sURL))) {
                vHyperlinks.add(sURL+"\t08-01-2005\tUnknown"); markVisited(sURL); }
            bResult = m.find();
        }
    }

    public String getData (String[] URLdata, boolean bWriteToFile) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        boolean bPDFDocument = false;
        boolean bWordDocument = false;

        String URL = URLdata[0];
        String s = "";

        try {
            URL myURL = null;

            // since we are running single threaded, pull the next available proxy server
            if (bUseProxies) {
                iProxyIndex++; if (iProxyIndex >= vProxies.size()) { iProxyIndex = 0; }

                myURL = new URL("http",                                       // protocol,
                                  (String) vProxies.elementAt(iProxyIndex),     // host name or IP of proxy server to use
                                  new Integer((String) vPorts.elementAt(iProxyIndex)).intValue(),
                                  URL);

                System.out.println("Using proxy: "+(String) vProxies.elementAt(iProxyIndex)+" Port: "+(String) vPorts.elementAt(iProxyIndex));

                if ((lInterval != -1) && (lLastRun[iProxyIndex] != 0) && (URL.indexOf("google") != -1)) {
                    long lCurrent = System.currentTimeMillis() - (long) lLastRun[iProxyIndex];

                    //System.out.println("Current time: "+lCurrent+" Verse time interval: "+lInterval);
                    while (lCurrent < lInterval) {
                        System.out.println("NOTE: Moving to fast, will pause for "+(lInterval - lCurrent)+" ms before retrieving: "+URL);
                        Thread.sleep((int) (lInterval - lCurrent));
                        lCurrent = System.currentTimeMillis() - (long) lLastRun[iProxyIndex];
                    }
                }

                lLastRun[iProxyIndex] = System.currentTimeMillis();

                // if an "interval" argument was given, pause until interval has passed.
                //System.out.println("fyi: interval ["+lInterval+"] proxy index: ["+lLastRun[iProxyIndex]+"]");
            } else {
                System.out.println("Not using proxy: URL is "+URL);
                myURL = new URL(URL);
            }

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "htm";
            if (httpCon.getContentType().equals("text/html")) { sExt = "html"; }
            if (httpCon.getContentType().equals("plain/text")) { sExt = "txt"; }
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; bPDFDocument = true; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; bWordDocument = true; }

            int len = 0; File tempFile = null; 
            FileOutputStream fos = null;

            byte[] buffer = new byte[512];

            while( (len=bis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
            } bis.close(); httpCon.disconnect();

            if (bWriteToFile) {
                tempFile = getTempFile(GetLocation(), sExt);
                System.out.println("Writing to temp file: "+tempFile.getAbsolutePath());

                fos = new FileOutputStream(tempFile);
                fos.write(bos.toByteArray());

                bos.flush(); fos.flush();
                bos.close(); fos.close();
            } else {
                s = bos.toString();
            }

            // If this is a PDF document, and WriteToFile is true, we will use the PDFBox libraries
            // to effect a conversion.
            //System.out.println("Write to File: "+bWriteToFile+" PDF Doc: "+bPDFDocument+" Filename: "+tempFile.getAbsolutePath());
            if (bWriteToFile && bPDFDocument) {
                System.out.println("Filtering from PDF: "+tempFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath()+".html");
                try { PDFUtils.convertPDFToHTML(tempFile, tempFile.getAbsolutePath()+".html"); }
                catch (java.lang.OutOfMemoryError e) {
                    System.err.println("Out of memory trying to filter file "+ tempFile.getAbsolutePath() +" .. skipping ...");
                    return "";
                }
                tempFile.delete();
                tempFile = new File(tempFile.getAbsolutePath()+".html");
            }
            if (bWriteToFile && bWordDocument) {
                System.out.println("Filtering from MSWORD: "+tempFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath()+".html");
                tempFile.delete();
                tempFile = FcrawlUtils.MSWORDtoHTML(new File(tempFile.getAbsolutePath()));
                tempFile = new File(tempFile.getAbsolutePath()+".html");
            }

            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("Item content retrieved "+URL+" in "+lEnd+" ms.");

            // Write the "just run" URL out to disk
            if (bWriteToFile) {
                File f = new File(CacheFile);
                synchronized (f) {
                    PrintWriter out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), true));
                    out.println(URL+"\t"+tempFile.getAbsolutePath()+"\t"+URLdata[1]+"\t"+URLdata[2]);
                    out.close();
                }
            }

            //System.out.println("RETURNING: ### "+s+" ###");
            return s;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms attempting to retrieve : "+URL+" : "+e.toString());
            //e.printStackTrace(System.out);
            return "";
        } finally {
            // Check to make sure that we have not exceeded our maximum run time.  If we have,
            //  please dump our memory image out to disk and empty out our processing.

            ///// TO DO /////
        }
    }

    public static File MSWORDtoHTML(File WordFile) throws Exception {
        File tempFile = new File(WordFile.getAbsolutePath()+".html");
        if (tempFile.exists()) {
            tempFile.delete();
        }

        System.out.println("Filtering: "+ WordFile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

        FileInputStream in = new FileInputStream (WordFile.getAbsolutePath());
        WordExtractor extractor = new WordExtractor();

        String s = extractor.extractText(in);
        FcrawlUtils.wrapHTML(s, WordFile.getName(), tempFile);

        return tempFile;
    }

    // Get the list of google URLs from a base file
    public boolean getGoogleURLs () {
        // restore the current queue
        System.out.println("Retrieving list of GOOGLE queries to run..");
        try {
            BufferedReader qin = new BufferedReader(new FileReader(GoogleBase));
            while (qin.ready() ) {
                String sTextLine = (String) qin.readLine();
                System.out.print(".");
                //System.out.println("Restoring: "+sTextLine+" to the queue.");
                vHyperlinks.add(sTextLine);
            }
            System.out.println(" ");
            qin.close();
        } catch (Exception e) { System.out.println("Error reading base file: "+GoogleBase); return false; }

        System.out.println("");

        return true;
    }
}