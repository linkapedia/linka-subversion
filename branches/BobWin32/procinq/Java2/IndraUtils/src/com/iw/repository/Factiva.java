package com.iw.repository;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

import com.iw.system.*;
import com.iw.tools.*;
import com.iw.fcrawl.*;
import com.iw.fcrawl.Machine;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

// This is the repository class for type Factiva.  The Factiva class will spider through a filesystem,
// identifying XML articles and classifying them into the database.  As an additional step, the text
// filtered from this XML can optionally be published as HTML on a local server.

public class Factiva extends Filesystem
{
	// Private variable htVectors contains an entry for each corpus containing
	//   documents tagged to be reindexed through Verity
	private Hashtable htVectors = new Hashtable();
    private Hashtable htTitles = new Hashtable();

    private FactivaFileFilter hff = new FactivaFileFilter();
    private String Location = "/temp/";

    // constructor: just use the standard constructor
	public Factiva (HashTree ht, HashTree ht2) {
        Location = (String) ht.get("REPOSITORYLOCATION");

		SetArguments(ht2);
	}
	// Specific filesystem routine to retrieve new objects on the filesystem that have
	//  changed since DateLastCapture.
	public void GetNewObjects(String sLocation, String sDateLastCapture, boolean bRecurse) {
		// initialize directory information
		File fDirectory = new File(sLocation);
		String FilePaths[] = fDirectory.list();

        //System.out.println("Total files found (including directories) directly under "+sLocation+" is "+FilePaths.length);

		// FCrawl currently only processes HTML files
		FactivaFileFilter hff = new FactivaFileFilter();

		// Format DateLastCapture into a Calendar object
		String sLastCapture = sDateLastCapture.substring(0, 19);
		Calendar c = Calendar.getInstance();
		c.set(new Integer(sDateLastCapture.substring(0,4)).intValue(), // Year
			  new Integer(sDateLastCapture.substring(5,7)).intValue()-1, // Month
			  new Integer(sDateLastCapture.substring(8,10)).intValue(), // Day
			  new Integer(sDateLastCapture.substring(11,13)).intValue(), // Hour
			  new Integer(sDateLastCapture.substring(14,16)).intValue(), // Minutes
			  new Integer(sDateLastCapture.substring(17,19)).intValue()); // Seconds

		//System.out.println(FilePaths.length+" files found in directory "+
		//				   fDirectory.getAbsolutePath());

		// loop through each file in this directory
		if (FilePaths != null) {
		for (int i=0; i<FilePaths.length; i++) {
			File f = new File(sLocation+"/"+FilePaths[i]);

			if (!f.isDirectory() && (!f.getName().equals(".file"))) {
				long l = f.lastModified();
				java.util.Date d = new java.util.Date(l);
				java.util.Date dc = c.getTime();

				//System.out.println("Filesystem:Checking file ("+FilePaths[i]+") last modified: "+d.toString()+
				//				   " vs "+dc.toString());
				if (d.after(dc)) {
					System.out.print("."); System.out.flush();
					//System.out.println("File: "+FilePaths[i]+" changed. Last modified: "+d.toString());
					synchronized (this) {
                        iCrawlCounter++;

                        try { String sPath = PublishDocument(f); AddNewDocument(sPath); }
                        catch (DupTitleException dte) {
                            System.err.println("Warning: Duplicate title found and ignored."); }
                        catch (FileNotFoundException fe) {
                            System.err.println("Error: File not found: "+f.getAbsolutePath());  }
                        catch (IOException ioe) {
                            ioe.printStackTrace(System.err);
                            return;
                        }
                    }
				} else {
                    System.out.print("."); System.out.flush();
                    //System.out.println("File: "+FilePaths[i]+" has not changed. It will not be classified. Last modified: "+d.toString());
                }
			}
            // if this is a directory and recurse is true, continue collecting
			if (f.isDirectory() && bRecurse) {
				GetNewObjects(sLocation+"/"+FilePaths[i],
							  sDateLastCapture, true);
			}
		}}

		System.out.println("");
		System.out.flush();
	}

    public String buildPath (File f) {
        String sPath = Location;
        if (!(new File(sPath).exists())) { new File(sPath).mkdirs(); }

        sPath = sPath + "/"+f.getName().substring(0,1);
        if (!(new File(sPath).exists())) { new File(sPath).mkdirs(); }

        sPath = sPath + "/"+f.getName().substring(2,3);
        if (!(new File(sPath).exists())) { new File(sPath).mkdirs(); }

        sPath = sPath+"/"+f.getName()+".html";

        return sPath;
    }

    // strip the RDF out of this document and publish it as HTML on the local web server (if applicable)
    public String PublishDocument (File f) throws FileNotFoundException, IOException, DupTitleException {
        boolean bInsideDescriptor = false;

        // put file into a subdirectory - creating those subdirectories if necessary
        String sPath = buildPath(f);

        // write out the file
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(sPath)));

            // read in the file and write it as HTML to the new location
            out.println(getText(f));

            out.flush();
            out.close();
        } catch (IOException ioe) {
            System.err.println("Error: I/O exception writing to: "+sPath);

            throw ioe;
        } catch (DupTitleException dte) {
            // this title was already included in this batch and loaded into the system
            throw dte;
        } catch (Exception e) {
            System.err.println("Could not get XML body from source.");
            throw new IOException("Could not get XML body from source");
        }

        return sPath;
    }

	public boolean UpdateDocumentSecurity (String sPath, Machine M) { return true; }
	public boolean ReindexDocuments () { return true; }

	// Lookup this document to find all associated corpora.   Then put the document
	//  path into the vector for each corpus, plus the overall vector.   These vectors
	//  will later be used to reindex documents in the full text verity engine.
	public void AddDocumentIndex(String sPath, Machine M) {

		/* This will be re-done differently, soon.
		HashTree htArguments = GetArguments();

		htArguments.put("Path", sPath);
		InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentCorpora", htArguments);
		HashTree htResults = API.Execute();

		if (!htResults.containsKey("DOCUMENT")) {
			//System.out.println("The document "+sPath+" did not classify into any topics.");
			return;
		} */
		return;
	}

    private String getText (File f) throws Exception {
        StringBuffer sb = new StringBuffer();
        sb.append("<HTML>\n<HEAD>\n");

        FileInputStream fis = new FileInputStream(f);

        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURI().toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);

        Element eDistDoc = doc.getRootElement();
        if (eDistDoc == null) {
            throw new Exception("No root element for file: " + f.getAbsolutePath());
        }

        Element eHead = eDistDoc.element("ReplyItem");
        Element eBody = eDistDoc.element("ArchiveDoc");

        if (eHead != null) {
            Element eTitle = eHead.element("Title");
            String title = f.getName();
            double wc = 0.0;

            if (eTitle != null) title = getSource(eTitle.element("Headline"));

            List l = eHead.elements("Num");

            for (int i = 0; i < l.size(); i++) {
                Element e = (Element) l.get(i);
                if (e.attribute("fid") != null) {
                    Attribute a = e.attribute("fid");
                    if (a.getText().trim().equals("wc")) {
                        String swc = e.attribute("value").getText();
                        wc = Double.parseDouble(swc);
                        i = l.size();
                    }}
            }

            if (htTitles.containsKey(title)) {
                // another document in this title already exists in this batch
                //   if this document, with the same title, has a word count within 10%,
                //   flag it as a duplicate.
                double owc = ((Double) htTitles.get(title)).doubleValue();
                double result = wc/owc;

                if ((result >= 0.9) && (result <= 1.1))
                    throw new DupTitleException();

            } else { htTitles.put(title, new Double(wc)); }

            sb.append("<TITLE>"+title+"</TITLE>\n</HEAD>\n");
        }

        if ((eBody != null) && (eBody.element("Article") != null)) {
            sb.append("<BODY>\n");

            Element eArticle = eBody.element("Article");
            Element e1 = eArticle.element("HandL");
            Element e2 = eArticle.element("TailParas");

            if (e1 != null) sb.append("<P>"+getSource(e1.element("LeadPara"))+"</P>\n");
            if (e2 != null) sb.append("<P>"+getSource(e2)+"</P>\n");

            sb.append("</BODY>\n</HTML>\n");
        }

        fis.close();

        return sb.toString();
    }

    private String getSource (Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) { sb.append(((Text) obj).getText()); }
            else if (obj instanceof Element) {
                if (((Element) obj).attribute("char") != null)
                    sb.append(((Element) obj).attribute("char").getText());
                else if (((Element) obj).getQualifiedName().equals("br")) { sb.append(" "); }
                else sb.append(getSource((Element) obj));
            }
        }

        return sb.toString();
    }

    // allow any HTML, text, word document or PDF files
	class FactivaFileFilter implements FilenameFilter {
	 public boolean accept (File dir, String name) {
	     if (name.toLowerCase().endsWith ("xml")) return true;
	     else return false;
        }
    }

    class DupTitleException extends Exception {}
}
