package com.iw.repository;

// This is the repository class for type Filesystem.   This class is assumed to be
//  an NT filesystem.   Please use the UNIXFilesystem class for UNIX based filesystems.

import java.io.*;
import java.util.Vector;
import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashSet;
import java.net.URLEncoder;

import com.iw.system.*;
import com.iw.fcrawl.*;

public class CacheCrawl extends Repository
{
    public String CacheFile = "/tmp/crawlcachedata.txt";

    public CacheCrawl (HashTree ht, HashTree ht2) {
        SetName((String) ht.get("REPOSITORYNAME"));
        SetLocation((String) ht.get("REPOSITORYLOCATION"));
        SetUsername((String) ht.get("USERNAME"));
        SetPassword((String) ht.get("PASSWORD"));

		SetArguments(ht2);

        if (ht2.containsKey("cachefile")) { CacheFile = (String) ht2.get("cachefile"); }
	}

    public CacheCrawl() { System.out.println("CacheCrawl() invoked (programming error)."); }

    Hashtable htDocuments = new Hashtable();

    private int iCrawlCounter = 0;

    // Specific web crawl routine to retrieve new objects on the web site that have
    //  changed since DateLastCapture.
    public void GetNewObjects(String sURL, String sDateLastCapture, boolean bRecurse) {
        System.out.println(Thread.currentThread().getName() + " CacheCrawl looking for new classification candidates in " + sURL);

        String textline;
        BufferedReader in = null;
        String fname = CacheFile;
        //preQualFile (fname, 3);

        int iLineNum = 0;
        int iCountDups = 0;
        try {
            System.out.println("Opening file: "+fname);
            in = new BufferedReader(new FileReader(fname));
            String sURLlower = sURL.toLowerCase();
            while (in.ready() ) {
                iLineNum++;
                String sTextLine = in.readLine();
                //if (!sTextLine.toLowerCase().startsWith(sURLlower) || sTextLine.startsWith("#") )
                //    continue;

                Vector vDataLine = split(sTextLine, "\t");
                if (vDataLine.size() < 2)
                    throw new Exception("line " + iLineNum + " in file " + fname +
                        " is not of normal format [" + sTextLine + "]");
                String sURLelement = (String) vDataLine.elementAt(0);
                String sRealPath = (String) vDataLine.elementAt(1);

                if ( !new File(sRealPath).exists()) {
                    if ( iLineNum % 100 == 0 )
                        System.out.println("ignore missing file " + sRealPath);
                    continue;
                }

                //System.out.print(".");
                synchronized ( htDocuments ) {
                    iCrawlCounter++;
                    htDocuments.put(sURLelement, new com.iw.fcrawl.DocumentContainer(vDataLine));
                    AddNewDocument(sURLelement);
                }
            }

            in.close();
            System.out.println(" ");
        }
        catch ( Exception e )
        {
            e.printStackTrace(System.out);
        }

        System.out.println(new java.util.Date() + " done cache crawling - thread: " + Thread.currentThread().getName() +
                " #lines:" + iLineNum +
                " #unique urls" + htDocuments.size());
        System.out.println("");
        System.out.flush();

    }

    public void preQualFile (String fname, int iCountDesired )
    {

        String textline;
        BufferedReader in = null;

        int iLineNum = 0;
        int iCountDups = 0;
        try
        {
            in = new BufferedReader(new FileReader(fname));
            while (in.ready())
            {
                iLineNum++;
                String sTextLine = in.readLine();
                if (sTextLine.startsWith("#") )
                    continue;

                Vector vDataLine = split(sTextLine, "\t");
                if (vDataLine.size() != 2)
                    throw new Exception("line " + iLineNum + " in file " + fname +
                        " is not of normal format [" + sTextLine + "]");
            }

            in.close();
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }

    }

    // hbk control - do I get here?
    private int iCallcount_ClassifyDocument = 0;
    private int iClassifyCounter = 0;

    public boolean ClassifyDocument(String sURL, com.iw.fcrawl.Machine M) {
        long    lStartClassify = System.currentTimeMillis();
        HashTree htArguments = GetArguments();
        if (sURL == null) {
            System.out.println("Path was null.");
            return false;
        }

        com.iw.fcrawl.DocumentContainer dc = (com.iw.fcrawl.DocumentContainer) htDocuments.get(sURL);

        try {
            String api = M.GetAPI();
            String rid = (String) htArguments.get("rid");
            String gid = (String) htArguments.get("gid");
            String docsumm = (String) htArguments.get("docsumm");
            String batch = (String) htArguments.get("batch");
            String corpora = (String) htArguments.get("corpora");

            HashTree htLocalArguments = new HashTree();

            htLocalArguments.put("SKEY", M.GetSKEY());
            htLocalArguments.put("api", api);
            htLocalArguments.put("RepositoryID", rid);
            htLocalArguments.put("ShowScores", "true");
            htLocalArguments.put("DBURL", URLEncoder.encode(sURL,"UTF-8"));
            htLocalArguments.put("post", "true");   // hbk control
            htLocalArguments.put("FulltextURL", dc.sPath);
            htLocalArguments.put("docsumm", docsumm);

            if (batch != null) htLocalArguments.put("batch", batch);
            if (gid != null) htLocalArguments.put("GenreID", gid);
            if (corpora != null) htLocalArguments.put("Corpora", corpora);
            if (dc.sTitle != null) htLocalArguments.put("DocTitle", dc.sTitle);

            iCallcount_ClassifyDocument++;
            //System.out.println("tname [" + Thread.currentThread().getName() + "] Classify #" + iCallcount_ClassifyDocument + ": DBURL: " + sURL + " Posted path: " + sPath + " Real path: " + sPath);
            //addLineToFile("/temp/temp17.txt", "CacheClassify #" + iCallcount_ClassifyDocument + ": DBURL: " + sPath + " Posted path: " + sURL + " Real path: " + sPath + "\r\n");

            //System.out.println(Thread.currentThread().getName() +  " @@@ post classify #"+iClassifyCounter+" ms [" + (System.currentTimeMillis() -lStartClassify )+ "] " + iClassifyCounter + " of " + iCrawlCounter + " complete in ms [" + (System.currentTimeMillis() - lStartClassify) + "] tname [" + Thread.currentThread().getName() + "] " );
            //System.out.println("\r\n" + Thread.currentThread().getName() +  "pre classify " + iClassifyCounter + " of " + iCrawlCounter + " complete tname [" + Thread.currentThread().getName() + "].");
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htLocalArguments);
            HashTree htResults = null;
            boolean bRunClassify = false; //debug hbk control
            if (bRunClassify)
                htResults = new HashTree();
            else {
                System.out.print("classify: "+dc.sPath);
                htResults = API.PostExecute(new File(dc.sPath));  // hbk control
            };

            if (!htResults.containsKey("CLASSIFICATIONRESULTSET"))
            {
                System.out.println("\nThe document "+dc.sPath+" did not classify "+ " into any topics. ("+iClassifyCounter+")");
                return false;
            } else { System.out.println(" success: "+dc.sPath); }
            iClassifyCounter++;

        } catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        }

        return true;
    }


    public static Vector split(String s, String splitterDelimiters)
    {
        Vector v = new Vector();
        StringTokenizer st = new StringTokenizer(s, splitterDelimiters);

        String ss = null;
        while (st.hasMoreElements())
        {
            ss = (String) st.nextElement();
            v.addElement(ss);
        }
        return v;
    }


    static boolean bSemaphore = false;

    public static void addLineToFile(String filename, String lineToadd)
    {
//System.out.println ("in addline to file [" + filename + "] [" + lineToadd + "]" );

//System.out.println ("PAUSE  400\r\n");
//clsUtils.pause_this_many_milliseconds ( 400 );
        if (filename == null)
            System.out.println("addLineToFile : null filename for line to add of [" + lineToadd + "]");
        int iTry = 0;
        while (true)
        {

            try
            {
// works both for existing and new files
                PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
                file.write(lineToadd);
                file.close();
                break;
            } catch (FileNotFoundException e)
            {
                long lPauseTime = (long) ((double) iTry * (double) 1000);
                if (iTry < 10)
                {
                    System.out.println(" addLineToFile location 1 will retry can't write - this is retry #[" + iTry + "] and pause (ms) of  [" + lPauseTime +
                            "] file locked [" + filename + "] text [" + lineToadd + "] retry up to 10 times ");
                    iTry++;
                } else
                {
                    System.out.println(" addLineToFile location 1 done retrying - GIVE UP on try [" + iTry + "] and pause (ms) of  [" + lPauseTime +
                            "] file locked [" + filename + "] text [" + lineToadd + "] gave up after 10 times ");

                }
            } catch (Exception e)
            {
                System.out.println("can't add line to file [" + filename + "] text [" + lineToadd + "]");
            }
        }
    } // addlinetofile

    public boolean UpdateDocumentSecurity (String sPath, com.iw.fcrawl.Machine M) { return true; }
	public boolean ReindexDocuments () { return true; }

	// Lookup this document to find all associated corpora.   Then put the document
	//  path into the vector for each corpus, plus the overall vector.   These vectors
	//  will later be used to reindex documents in the full text verity engine.
	public void AddDocumentIndex(String sPath, com.iw.fcrawl.Machine M) {

		/* This will be re-done differently, soon.
		HashTree htArguments = GetArguments();

		htArguments.put("Path", sPath);
		InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentCorpora", htArguments);
		HashTree htResults = API.Execute();

		if (!htResults.containsKey("DOCUMENT")) {
			//System.out.println("The document "+sPath+" did not classify into any topics.");
			return;
		} */
		return;
	}
}

