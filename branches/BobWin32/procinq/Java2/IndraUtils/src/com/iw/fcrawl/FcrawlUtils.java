package com.iw.fcrawl;

import org.textmining.text.extraction.WordExtractor;

import java.io.*;

import com.iw.system.HashTree;
import com.iw.utils.PDFUtils;

public class FcrawlUtils {
    // GetArgHash takes command line arguments and parses them into a Hash structure
	public static HashTree getArgHash ( String[] args) {
		HashTree htHash = new HashTree();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}

    // Filter a PDF document to an HTML document
    public static File PDFtoHTML(File PDFfile) throws Exception {
        File tempFile = new File("temp.html");
        //org.apache.log4j.helpers.LogLog.setQuietMode(true);

        try {
            System.out.println("Filtering: " + PDFfile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
            PDFUtils.convertPDFToHTML(PDFfile, tempFile);
            wrapHTML(PDFfile.getName(), tempFile);
        } catch (Exception ex) {
            ex.printStackTrace(System.out); throw ex;
        }

        return tempFile;
    }
    // Filter a WORD document to an HTML document
    public static File MSWORDtoHTML(File WordFile) throws Exception {
        File tempFile = new File("temp.html");
        if (tempFile.exists()) {
            tempFile.delete();
        }

        System.out.println("Filtering: "+ WordFile.getAbsolutePath() + " to " + tempFile.getAbsolutePath());

        FileInputStream in = new FileInputStream (WordFile.getAbsolutePath());
        WordExtractor extractor = new WordExtractor();

        String s = extractor.extractText(in);
        wrapHTML(s, WordFile.getName(), tempFile);

        return tempFile;
    }

    // take a text file and wrap it with HTML
    public static void wrapHTML(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>"+f.getName()+"</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    // take a text file and wrap it with HTML
    public static void wrapHTML(String title, File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>"+title+"</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public static void wrapHTML(String s, String title, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>"+title+"</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }
}
