package com.iw.repository;

import java.io.*;
import java.util.*;
import java.net.*;

import com.iw.system.*;
import com.iw.fcrawl.*;
import com.iw.utils.PDFUtils;

// The WEBCACHE repository is a specific version of WebCrawl that does not classify real-time.
//  Found files are instead stored on the filesystem for conversion (to HTML) and batch classify.

// WebCaches maintain, in memory, a list of pages pending to be crawled (QueueFile) and a list of pages
//  that have already been crawled (CacheFile).  This in-memory image is periodically dumped to file
//  for crash recovery purposes.  They are written to the top level directory as determined by
//  the repository definition (see: GetLocation() ).

public class WebCache extends WebCrawl {

    // constructor: WebCache specific constructor
    public WebCache () { }
	public WebCache (HashTree ht, HashTree ht2) {
        // set attributes
        SetName((String) ht.get("REPOSITORYNAME"));
        SetLocation((String) ht.get("REPOSITORYLOCATION"));
        SetUsername((String) ht.get("USERNAME"));
        SetPassword((String) ht.get("PASSWORD"));

        File fProxyConfigurationFile = null;
        if (ht2.containsKey("proxyconfig")) { fProxyConfigurationFile = new File((String) ht2.get("proxyconfig")); }
		if ((fProxyConfigurationFile != null) && (fProxyConfigurationFile.exists())) {
            System.out.println("Loading proxy configuration file..");
            try { ReadProxyFile(fProxyConfigurationFile); } catch (Exception e) {}
		}

        if (vProxies.size() > 0) { bUseProxies = true; }

        if (ht2.containsKey("interval")) { lInterval = 1000 * new Integer((String) ht2.get("interval")).longValue(); }

        int iHour = 24*365;
        if (!ht2.containsKey("endtimes")) { System.out.println("No ending time specified.  Assuming infinite run."); }
        else { iHour = new Integer((String) ht2.get("endtimes")).intValue(); }

        // set stop time, if applicable
        java.util.Date d = c.getTime();
        c.add(Calendar.HOUR, iHour);

		SetArguments(ht2);

        CacheFile = GetLocation()+"/crawlcachedata.txt"; System.out.println("Cache file: "+CacheFile);
        QueueFile = GetLocation()+"/queuedata.txt"; System.out.println("Queue file: "+QueueFile);

        if (ht2.containsKey("restore")) { RestoreQueueFromFile(); bRestore = true; }
	}

    // tab delimited file with file locations, metadata
    public String CacheFile = ""; // list of files that have already been crawled, along with corresponding URL location
    public String QueueFile = ""; // list of files currently in our memory queue as candidates to be crawled
    public Calendar c = Calendar.getInstance();
    public long lInterval = -1;

    // proxy server - port - last run ms
    public Vector vProxies = new Vector(); public Vector vPorts = new Vector(); public long[] lLastRun;
    public int iProxyIndex = -1;
    public boolean bUseProxies = false;
    public boolean bRestore = false;

    // Restore the QUEUE from the QUEUEFILE output
    public boolean RestoreQueueFromFile () {
        // restore the current queue
        try {
            BufferedReader qin = new BufferedReader(new FileReader(QueueFile));
            while (qin.ready() ) {
                String sTextLine = (String) qin.readLine();
                System.out.print(".");
                //System.out.println("Restoring: "+sTextLine+" to the queue.");
                vHyperlinks.add(sTextLine);
            }
            System.out.println(" ");
            qin.close();
        } catch (Exception e) { System.out.println("Error reading queue file: "+QueueFile); return false; }

        System.out.println("");

        // restore the list of pages that were already crawled
        try {
            BufferedReader cin = new BufferedReader(new FileReader(CacheFile));
            while (cin.ready() ) {
                String sTextLine = (String) cin.readLine();
                String[] sTextArr = sTextLine.split("\t");
                if (sTextArr.length == 2) {
                    //System.out.println("Mark visited: "+(String)sTextArr[0]+" to the cache.");
                    System.out.print(".");
                    markVisited(sTextArr[0]);
                }
            }
            System.out.println(" ");
            cin.close();
        } catch (Exception e) { System.out.println("Error reading crawl file: "+CacheFile); return false; }

        return true;
    }

	// These three functions are repository specific and must be overridden
	public boolean ClassifyDocument (String sPath, com.iw.fcrawl.Machine M) {
        // in WebCache mode, documents are not classified at run time.
        return true;
    }

    // given a URL, retrieve the HTML and return it
    // NOTE: for WebCache, write this document out even if it's a PDF
    public String getData (String URL) {
        // if this URL already exists in our cachecrawler, do not write it to disk, only spider it
        boolean bWriteToFile = true;
        //if (alreadyVisited(URL)) { bWriteToFile = false; }

        System.out.println("Get data: "+URL+" "+bWriteToFile);
        return getData (URL, bWriteToFile);
    }
    public String getData (String URL, boolean bWriteToFile) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");
        boolean bPDFDocument = false;

        try {
            URL myURL = null;

            // since we are running single threaded, pull the next available proxy server
            if (bUseProxies) {
                iProxyIndex++; if (iProxyIndex >= vProxies.size()) { iProxyIndex = 0; }

                myURL = new URL("http",                                       // protocol,
                                  (String) vProxies.elementAt(iProxyIndex),     // host name or IP of proxy server to use
                                  new Integer((String) vPorts.elementAt(iProxyIndex)).intValue(),
                                  URL);

                System.out.println("Using proxy: "+(String) vProxies.elementAt(iProxyIndex)+" Port: "+(String) vPorts.elementAt(iProxyIndex));

                if ((lInterval != -1) && (lLastRun[iProxyIndex] != 0)) {
                    long lCurrent = System.currentTimeMillis() - (long) lLastRun[iProxyIndex];

                    //System.out.println("Current time: "+lCurrent+" Verse time interval: "+lInterval);
                    while (lCurrent < lInterval) {
                        System.out.println("NOTE: Moving to fast, will pause for "+(lInterval - lCurrent)+" ms.");
                        Thread.sleep((int) (lInterval - lCurrent));
                        lCurrent = System.currentTimeMillis() - (long) lLastRun[iProxyIndex];
                    }
                }

                lLastRun[iProxyIndex] = System.currentTimeMillis();

                // if an "interval" argument was given, pause until interval has passed.
                //System.out.println("fyi: interval ["+lInterval+"] proxy index: ["+lLastRun[iProxyIndex]+"]");
            } else {
                System.out.println("Not using proxy: URL is "+URL);
                myURL = new URL(URL);
            }

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "other";
            if (httpCon.getContentType().equals("text/html")) { sExt = "html"; }
            if (httpCon.getContentType().equals("plain/text")) { sExt = "txt"; }
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; bPDFDocument = true; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; }

            int len = 0; File tempFile = null; FileOutputStream fos = null;

            byte[] buffer = new byte[512];
            int bytesRead = 0;

            while( (len=bis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
                returnString = returnString.append(new String(buffer));
            } bis.close(); httpCon.disconnect();

            if (bWriteToFile) {
                tempFile = getTempFile(GetLocation(), sExt);
                System.out.println("Writing to temp file: "+tempFile.getAbsolutePath());

                fos = new FileOutputStream(tempFile);
                fos.write(bos.toByteArray());

                bos.flush(); fos.flush();
                bos.close(); fos.close();
            }

            // If this is a PDF document, and WriteToFile is true, we will use the PDFBox libraries
            // to effect a conversion.
            //System.out.println("Write to File: "+bWriteToFile+" PDF Doc: "+bPDFDocument+" Filename: "+tempFile.getAbsolutePath());
            if (bWriteToFile && bPDFDocument) {
                System.out.println("Filtering: "+tempFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath()+".html");
                PDFUtils.convertPDFToHTML(tempFile, tempFile.getAbsolutePath()+".html");
            } else if (bWriteToFile && sExt.equals("doc")) {
                System.out.println("Filtering: "+tempFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath()+".html");
                tempFile = FcrawlUtils.MSWORDtoHTML(tempFile);
            }

            String s = returnString.toString();

            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("Item content length is "+s.length()+", retrieved "+URL+" in "+lEnd+" ms.");

            // Write the "just run" URL out to disk
            if (bWriteToFile) {
                File f = new File(CacheFile);
                synchronized (f) {
                    PrintWriter out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), true));
                    out.println(URL+"\t"+tempFile.getAbsolutePath());
                    out.close();
                }
            }

            //System.out.println("RETURNING: ### "+s+" ###");
            return s;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms attempting to retrieve : "+URL+" : "+e.toString());
            e.printStackTrace(System.out);
            return "";
        } finally {
            // Check to make sure that we have not exceeded our maximum run time.  If we have,
            //  please dump our memory image out to disk and empty out our processing.

            ///// TO DO /////
        }
    }

    // Specific web crawl routine to retrieve new objects but not classify them!
	public void GetNewObjects(String sURL, String sDateLastCapture, boolean bRecurse) {
		System.out.println("Looking for classification candidates in "+sURL);

		// initialize directory information
		if (!bRestore) { vHyperlinks.add(sURL); }
        setKey(sURL);

        while (vHyperlinks.size() > 0) {
            iCrawlCounter++;

            // on every 20th result, write the current crawl queue out to disk
            if ((iCrawlCounter % 20) == 0) {
                try {
                    File f = new File(QueueFile);
                    if (f.exists()) { f.delete(); }
                    synchronized (f) {
                        PrintWriter out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
                        Enumeration eV = vHyperlinks.elements();
                        while (eV.hasMoreElements()) { out.println((String) eV.nextElement()); }
                        out.close();
                    }
                } catch (Exception e) { System.out.println("Warning: could not write queue file.\n"); e.printStackTrace(System.out); }
            }

            String sURLelement = (String) vHyperlinks.firstElement();
            String sData = getData(sURLelement);

            if ((bRecurse) && (sURLelement.toLowerCase().indexOf("htm") != -1)) {
                extractHyperTextLinks(sData.replaceAll("\n", " ").replaceAll("\r", " "), sURLelement);
                extractFrameTextLinks(sData.replaceAll("\n", " ").replaceAll("\r", " "), sURLelement);
            }
            vHyperlinks.remove(sURLelement);
            //try { Thread.sleep(250); } catch (Exception e) { System.out.println("Could not pause."); } // let's try not to annoy the system administrators :)
        }

		System.out.println(iCrawlCounter+" new web pages found on web site "+sURL);
		System.out.println("");
		System.out.flush();
	}

    public void ReadProxyFile (File f)
	throws Exception {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(f);
			BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
			String sData = new String();

            int iLoop = 0;
			while ((sData = buf.readLine()) != null) { iLoop++;
                int iSeparate = sData.indexOf("\t");
				String sProxy = sData.substring(0, iSeparate);
				String sPort = sData.substring(iSeparate+1, sData.length());

                vProxies.add(sProxy); vPorts.add(sPort);
				System.out.println("Loading proxy: "+sData);
			}
            lLastRun = new long[iLoop];
            for (int i=0; i < iLoop; i++) { lLastRun[i] = 0; }

		} catch (Exception e) { e.printStackTrace(System.out); return;
		} finally { fis.close(); }
	}
}