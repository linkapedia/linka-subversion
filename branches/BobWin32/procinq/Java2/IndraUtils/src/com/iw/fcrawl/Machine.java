package com.iw.fcrawl;

import java.io.*;
import java.util.*;

// Generic Document object (without node relationship)
public class Machine
{
	// private variables
	private int NumThreads;
    private int NumProblems = 0;
	private String API;
	private String SKEY;

	// classification threads are publically accessible
	public Vector vClassifyThreads = new Vector();

	// Accessor methods to private variables
	public int GetNumberOfThreads() { return NumThreads; }
    public int NumberOfProblems() { return NumProblems; }
	public String GetAPI() { return API+"/itsapi/ts?fn="; }
	public String GetSKEY() { return SKEY; }

	// Set methods for private variables
	public int SetNumberOfThreads(int iThreads) { NumThreads = iThreads; return NumThreads; }
    public int FoundProblem() { NumProblems++; return NumberOfProblems(); }
	public String SetAPI (String sAPI) { API = sAPI; return API; }
	public String SetSKEY (String sKey) { SKEY = sKey; return SKEY; }
	
	// Object constructor(s)
	public Machine (int iNumThreads, String sAPI) {
		this.NumThreads = iNumThreads; this.API = sAPI;
	}
	public Machine (int iNumThreads, String sAPI, String sKey) {
		this.NumThreads = iNumThreads; this.API = sAPI; this.SKEY = sKey;
	}
	public Machine () {}
}
