package com.iw.repository;

import java.io.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;
import java.text.SimpleDateFormat;

import com.iw.system.*;

// This is the repository class for type Filesystem.   This class is assumed to be
//  an NT filesystem.   Please use the UNIXFilesystem class for UNIX based filesystems.
public class WebCrawl extends Repository
{
	// Private variable htVectors contains an entry for each corpus containing
	//   documents tagged to be reindexed through Verity
	public int iClassifyCounter = 0;
	public int iCrawlCounter = 0;
    public static final Vector vHyperlinks = new Vector();

    private Hashtable htVectors = new Hashtable();
    private static Hashtable htVisited = new Hashtable();
    private static Hashtable htFileLocations = new Hashtable();
    private static String Key = "";

    public Hashtable getFileLocations() { return htFileLocations; }

    // given a URL, retrieve the HTML and return it
    private String getData (String URL) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");

        try {
            URL myURL = new URL(URL);
            httpCon = (HttpURLConnection) myURL.openConnection();
            //httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            DataInputStream dis = new DataInputStream(new BufferedInputStream(is));

            //System.out.println("Get: "+URL+" ("+httpCon.getContentType()+")");

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "html";
            /*
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; }
            */

            // NOTE
            //  CODE IS IN A HACKED STATE SOASTO WORK FOR SCHERING ONLY
            if ((httpCon.getContentType().equals("text/html"))) {
                File tempFile = getTempFile(GetLocation(), sExt);
                FileOutputStream p = new FileOutputStream(tempFile);

                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while((bytesRead = dis.read(buffer, 0, buffer.length)) >0) {
                    p.write(buffer); returnString = returnString.append(new String(buffer));
                }
                p.write(buffer); returnString = returnString.append(new String(buffer));
                p.close();

                String s = returnString.toString();

                long lEnd = System.currentTimeMillis() - lStart;
                //System.out.println("Retrieved "+URL+" in "+lEnd+" ms.");

                if (s.indexOf("66.134.131.56") != -1) { System.out.println("Do not classify this one.  It is an index document."); return s; }
                else { System.out.println("This document will be classified."); htFileLocations.put(URL, tempFile.getAbsolutePath()); return ""; }

            } else { return "-1"; }

            /*
            // if the item is not of type "text/html" or "text/plain", it should be filtered
            if (!httpCon.getContentType().equals("text/html")) {
                if (true) return "";

                // in order to filter the document it must first exist in the database
                String sDocumentID = InsertDocument(URL, GetLocation()+tempFile.getName());
                if (sDocumentID.equals("-1")) {
                    System.out.println("Document will not be classified.");
                } else {
                    //System.out.println("Attempt to filter: "+tempFile.getAbsolutePath());
                    long lFs = System.currentTimeMillis();

                    String sConvertedDocumentText = FilterPDFDocument(tempFile.getAbsolutePath());

                    if (sConvertedDocumentText.equals("")) { return ""; }

                    String sOldPath = tempFile.getAbsolutePath();

                    tempFile = getTempFile(GetLocation(), "html");
                    FileWriter fw = new java.io.FileWriter (tempFile, true);
                    fw.write(sConvertedDocumentText); fw.close();

                    System.out.println("Document filtered from "+sOldPath+" to "+tempFile.getAbsolutePath()+" "+
                    "in "+(System.currentTimeMillis() - lFs)+" ms.");

                    System.out.println("Put file: "+tempFile.getAbsolutePath());
                    htFileLocations.put(URL, tempFile.getAbsolutePath());
                }
                return "";

            } else {
                if ((returnString.toString()).indexOf("66.134.131.56") == -1) {
                    System.out.println("Put file: "+tempFile.getAbsolutePath()); htFileLocations.put(URL, tempFile.getName());
                }
            }
            */

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms. : "+e.toString());
            e.printStackTrace(System.out);
            return "";
        }
        finally { httpCon.disconnect(); }

    }

    // extract all HTML hyperlinks from a given document and return in a vector
    public static void extractFrameTextLinks(String page, String rootURL) {
        int lastPosition = 0;           // position of "http:" substring in page

        Pattern p = Pattern.compile("\\s*<frame (.*?)src\\s*=\\s*\"(.*?)\"", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(page); boolean bResult = m.find();

        while (bResult) {
            String sURL = m.group(2);
            if ((!sURL.toLowerCase().startsWith("http")) && (!sURL.startsWith("/"))) {
                if ((getDirectory(rootURL)).endsWith("/")) {
                    sURL = getDirectory(rootURL)+sURL;
                } else { sURL = getDirectory(rootURL)+"/"+sURL; }
            }
            if (sURL.startsWith("/")) {
                if ((getBase(rootURL)).endsWith("/")) {
                    sURL = getBaseWithoutSlash(rootURL)+sURL;
                } else { sURL = getBase(rootURL)+sURL; }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf("#");
            if (iPound != -1) { sURL = sURL.substring(0, iPound); }

            try {
                URI u = new URI(sURL);
                u = u.normalize();
                sURL = u.toString();
            } catch (Exception e) {
                System.out.println("Could not normalize URL: "+sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll(new Character((char)0).toString(), "");

            if (hasKey() && (sURL.indexOf(getKey()) != -1)) {
                if (!alreadyVisited(sURL) && (!isImage(sURL))) {
                    System.out.println("Found in frame: "+sURL);
                    vHyperlinks.add(sURL); markVisited(sURL); }
            }
            bResult = m.find();
        }
    }


    // extract all HTML hyperlinks from a given document and return in a vector
    public static void extractHyperTextLinks(String page, String rootURL) {
        int lastPosition = 0;           // position of "http:" substring in page

        Pattern p = Pattern.compile("\\s*href\\s*=\\s*\"*?([^>\" ]+)\"*?.*?>{1}?(.*?)(</a>{1}?)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(page); boolean bResult = m.find();

        while (bResult) {
            String sURL = m.group(1);

            if ((!sURL.toLowerCase().startsWith("http")) && (!sURL.startsWith("/"))) {
                if ((getDirectory(rootURL)).endsWith("/")) {
                    sURL = getDirectory(rootURL)+sURL;
                } else { sURL = getDirectory(rootURL)+"/"+sURL; }
            }
            if (sURL.startsWith("/")) {
                if ((getBase(rootURL)).endsWith("/")) {
                    sURL = getBaseWithoutSlash(rootURL)+sURL;
                } else { sURL = getBase(rootURL)+sURL; }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf("#");
            if (iPound != -1) { sURL = sURL.substring(0, iPound); }

            try {
                URI u = new URI(sURL);
                u = u.normalize();
                sURL = u.toString();
            } catch (Exception e) {
                System.out.println("Could not normalize URL: "+sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll(new Character((char)0).toString(), "");

            if (hasKey() && (sURL.indexOf(Key) != -1)) {
                if (!alreadyVisited(sURL) && (!isImage(sURL))) {
                    vHyperlinks.add(sURL); markVisited(sURL); }
            }
            bResult = m.find();
        }
    }

    // return the URL directory that this file was found in on the web server
    public static String getDirectory (String URL) {
        while (URL.endsWith("/")) { return URL; }
        int index = URL.lastIndexOf('/');

        return URL.substring(0, index);
    }
    // return the base URL (host)
    public static String getBase (String URL) {
        int index = URL.indexOf('/', 7);

        return URL.substring(0, index);
    }

    public static String getBaseWithoutSlash (String URL) {
        int index = URL.indexOf('/', 7);

        return URL.substring(0, index-1);
    }

    // mark or check if page has already been spidered
    public static void markVisited (String URL) { htVisited.put(URL.toLowerCase(), "1"); }
    public static boolean alreadyVisited (String URL) { if (htVisited.containsKey(URL.toLowerCase())) { return true; } return false; }

    // setKey sets the keyword field.  This allows us to ensure matches are only found from within the directory of the starting point.
    public static String setKey (String sKey) {
        if (!sKey.endsWith("/")) { sKey = getDirectory(sKey)+"/"; }
        if (sKey.startsWith("http://")) {
            int index = sKey.indexOf('/', 7);
            if (index == -1) { return ""; }
            sKey = sKey.substring(index);
        }
        Key = sKey;
        return Key;
    }
    // these functions determine whether or not to check the URL for a specific key directory
    public static String getKey() { return Key; }
    public static boolean hasKey() { if (!Key.equals("")) { return true; } return false; }

    // look at the document extension, make sure it's not an image file
    public static boolean isImage (String URL) {
        if (URL.toLowerCase().endsWith(".jpg")) { return true; }
        if (URL.toLowerCase().endsWith(".gif")) { return true; }
        if (URL.toLowerCase().endsWith(".jpeg")) { return true; }
        if (URL.toLowerCase().endsWith(".bmp")) { return true; }
        if (URL.toLowerCase().endsWith(".wav")) { return true; }
        if (URL.toLowerCase().endsWith(".css")) { return true; }
        if (URL.toLowerCase().endsWith(".xml")) { return true; }
        if (URL.indexOf("javascript") != -1) { return true; }
        if (URL.indexOf("mailto") != -1) { return true; }
        if (URL.indexOf("..") != -1) { System.out.println("Found URL: "+URL+" has relative paths, discarding.."); return true; }
        return false;
    }

	// Specific web crawl routine to retrieve new objects on the web site that have
	//  changed since DateLastCapture.
	public void GetNewObjects(String sURL, String sDateLastCapture, boolean bRecurse) {
		System.out.println("Looking for classification candidates in "+sURL);

        // get all the documents already run from the database, they should be
        // run first to ensure that they have not changed.
        Hashtable htOldDocuments = new Hashtable();
        try { htOldDocuments = getDocumentsByRepository(); }
        catch (Exception e) {
            System.err.println("Fatal error: cannot retrieve documents by repository.");
            e.printStackTrace(System.err);
            return;
        }

		// initialize directory information
		vHyperlinks.add(sURL);
        setKey(sURL);

        while (vHyperlinks.size() > 0) {
            String sURLelement = (String) vHyperlinks.firstElement();
            boolean bClassifyIt = true;

            // if the URL is an existing document (already in our database), do not retrieve it
            // unless it has changed since our last run
            if (htOldDocuments.containsKey(sURLelement)) {
                String sDate = doHeadRequest(sURLelement);  // YYYYMMDDHHMISS
                String sLast = (String) htOldDocuments.get(sURLelement);  // YYYYMMDDHHMISS

                if (sDate.compareTo(sLast) < 0) { bClassifyIt = false; }
            }

            String sData = getData(sURLelement);
            if (bRecurse) extractHyperTextLinks(sData.replaceAll("\n", " ").replaceAll("\r", " "), sURLelement);
            if (bClassifyIt) {
                iCrawlCounter++;
                synchronized (this) { System.out.println("Added new document: "+sURLelement); AddNewDocument(sURLelement); }
            }

            vHyperlinks.remove(sURLelement);
            //try { Thread.sleep(250); } catch (Exception e) { System.out.println("Could not pause."); } // let's try not to annoy the system administrators :)
        }

		System.out.println(htFileLocations.size()+" new web pages found on web site "+sURL);
		System.out.println("");
		System.out.flush();

        //try { CloseConnection(); } catch (Exception e) {}
	}

	// These three functions are repository specific and must be overridden
	public boolean ClassifyDocument (String sPath, com.iw.fcrawl.Machine M) {
		HashTree htArguments = GetArguments();
        if (sPath == null) { System.out.println("Path was null."); return false; }

        String sTmpFile = (String) htFileLocations.get(sPath);
        String sPostedPath = (String) htFileLocations.get(sPath);
        System.out.println("Classify document: "+sPostedPath);

		if (iCrawlCounter == 0) { iCrawlCounter = GetNewDocuments().size(); }

		try {
			String api = M.GetAPI();
			String rid = (String) htArguments.get("rid");
			String gid = (String) htArguments.get("gid");
			String docsumm = (String) htArguments.get("docsumm");

			HashTree htLocalArguments = new HashTree();

			htLocalArguments.put("SKEY", M.GetSKEY());
            htLocalArguments.put("api", api);
			htLocalArguments.put("RepositoryID", rid);
			htLocalArguments.put("ShowScores", "true");
			htLocalArguments.put("DBURL", sPath);
			htLocalArguments.put("post", "true");
            htLocalArguments.put("FulltextURL", sPostedPath);
			htLocalArguments.put("docsumm", docsumm);
			if (gid != null) { htLocalArguments.put("GenreID", gid); }

            System.out.println("Classify DBURL: "+sPath+" Posted path: "+sPostedPath);

			InvokeAPI API = new InvokeAPI ("tsclassify.TSClassifyDoc", htLocalArguments);
			HashTree htResults = API.PostExecute(new File(sPostedPath));
			iClassifyCounter++;

			System.out.println(iClassifyCounter+" of "+iCrawlCounter+" complete.");

			if (!htResults.containsKey("CLASSIFICATIONRESULTSET")) {
				//System.out.println("\nThe document "+sPath+" did not classify "+
				//				   " into any topics. ("+iClassifyCounter+")");
				return false;
			} else {
				//System.out.println("\nDocument: "+sPath+" classified successfully! ("+iClassifyCounter+")");
			}

		} catch (Exception e) {
            e.printStackTrace(System.out);
            return false; }

		return true;
	}
	public boolean UpdateDocumentSecurity (String sPath, com.iw.fcrawl.Machine M) { return true; }
	public boolean ReindexDocuments () { return true; }

	// Lookup this document to find all associated corpora.   Then put the document
	//  path into the vector for each corpus, plus the overall vector.   These vectors
	//  will later be used to reindex documents in the full text verity engine.

    // update 7/7/03: this no longer makes sense with interMedia
	public void AddDocumentIndex(String sPath, com.iw.fcrawl.Machine M) {

		/* This will be re-done differently, soon.
		HashTree htArguments = GetArguments();

		htArguments.put("Path", sPath);
		InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentCorpora", htArguments);
		HashTree htResults = API.Execute();

		if (!htResults.containsKey("DOCUMENT")) {
			//System.out.println("The document "+sPath+" did not classify into any topics.");
			return;
		} */

		return;
	}

    // HEAD request the URL and return the last modified date, if applicable, style: 1976-01-05 13:50:08
    public String doHeadRequest (String URL) {
        HttpURLConnection httpCon = null;

        try {
            URL myURL = new URL(URL);
            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestMethod("HEAD");

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            long changed = httpCon.getLastModified();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmss");
            return sdf.format(new Date(changed));

        } catch (Exception e) {
            System.out.println("HTTP HEAD request failure: "+e.toString());
            return "";
        }
        finally { httpCon.disconnect(); }
    }

 	// Remove signature cache files from each machine, ensuring that they are homogenious
	public Hashtable getDocumentsByRepository () throws Exception {
        HashTree htArguments = GetArguments();
        Hashtable ht = new Hashtable();

		InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentsByRepository", htArguments);
		HashTree htResults = API.Execute(false, false);

        if (htResults.containsKey("DOCUMENTS")) {
            HashTree htDocuments = (HashTree) htResults.get("DOCUMENTS");
            Enumeration eH = htDocuments.elements();

            while (eH.hasMoreElements()) {
                HashTree htDocument = (HashTree) eH.nextElement();
                String sURL = (String)htDocument.get("DOCURL");
                String sDate = (String)htDocument.get("DATELASTFOUND");
                ht.put(sURL, sDate);
                vHyperlinks.add(sURL); markVisited(sURL);
            }
        }

		return ht;
	}

	// constructor: just use the standard constructor
	public WebCrawl (HashTree ht, HashTree ht2) {
        SetName((String) ht.get("REPOSITORYNAME"));
        SetLocation((String) ht.get("REPOSITORYLOCATION"));
        SetUsername((String) ht.get("USERNAME"));
        SetPassword((String) ht.get("PASSWORD"));

		SetArguments(ht2);
        //try { GetConnection(); } catch (Exception e) { System.out.println("Could not initialize database connection."); }
	}

    public WebCrawl () {}
}
