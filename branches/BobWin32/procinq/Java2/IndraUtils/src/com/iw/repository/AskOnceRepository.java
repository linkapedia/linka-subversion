package com.iw.repository;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

import com.iw.system.*;
import com.iw.tools.*;
import com.iw.fcrawl.*;
import com.iw.fcrawl.Machine;

// This is the repository class for type Filesystem.   This class is assumed to be
//  an NT filesystem.   Please use the UNIXFilesystem class for UNIX based filesystems.

public class AskOnceRepository extends Repository {
    // Private variable htVectors contains an entry for each corpus containing
    //   documents tagged to be reindexed through Verity
    private Hashtable htVectors = new Hashtable();
    public int iClassifyCounter = 0;
    public int iCrawlCounter = 0;

    public AskOnceRepository() {
    }

    public void GetNewObjects(HashTree htFolder) {
        Enumeration eNodes = htFolder.elements();

        while (eNodes.hasMoreElements()) {
            HashTree htNode = (HashTree) eNodes.nextElement();
            GetNewObjects((String) htNode.get("NODEID"), (String) htNode.get("NODETITLE"), false);
        }
    }

    // Specific filesystem routine to retrieve new objects on the filesystem that have
    //  changed since DateLastCapture.
    public void GetNewObjects(String sLocation, String sNodeTitle, boolean bRecurse) {
        // initialize directory information
        File fDirectory = new File(sLocation);
        try {
            AskOnce ao = new AskOnce("guest", "askonce", "localhost", "query");
            Vector v = ao.processResults(sLocation);

            System.out.println(" ");

            // loop through each file in this directory
            for (int i = 0; i < v.size(); i++) {
                File f = (File) v.elementAt(i);

                iCrawlCounter++;
                synchronized (this) {
                    System.out.println("TITLE: "+sNodeTitle+" File: "+f.getAbsolutePath());
                    AddNewDocument(f.getAbsolutePath());
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

        System.out.println("");
        System.out.flush();
    }

    // These three functions are repository specific and must be overridden
    public boolean ClassifyDocument(String sPath, com.iw.fcrawl.Machine M) throws com.iw.fcrawl.ClassifyException {
        HashTree htArguments = GetArguments();
        File tempFile = null;

        if (iCrawlCounter == 0) {
            iCrawlCounter = GetNewDocuments().size();
        }

        System.out.println("Classifying: " + sPath);

        try {
            String sPostedPath = sPath;

            if (sPath.toLowerCase().endsWith(".pdf")) {
                tempFile = FcrawlUtils.PDFtoHTML(new File(sPath));
                sPostedPath = tempFile.getAbsolutePath();
            } else if (sPath.toLowerCase().endsWith(".doc")) {
                tempFile = FcrawlUtils.MSWORDtoHTML(new File(sPath));
                sPostedPath = tempFile.getAbsolutePath();
            }

            String api = M.GetAPI();
            String rid = (String) htArguments.get("rid");
            String gid = (String) htArguments.get("gid");
            String batch = (String) htArguments.get("batch");
            String docsumm = (String) htArguments.get("docsumm");
            String corpora = (String) htArguments.get("corpora");

            HashTree htLocalArguments = new HashTree();

            htLocalArguments.put("SKEY", M.GetSKEY());
            htLocalArguments.put("RepositoryID", rid);
            htLocalArguments.put("api", api);
            htLocalArguments.put("ShowScores", "false");
            htLocalArguments.put("DBURL", sPath);
            htLocalArguments.put("post", "true");
            htLocalArguments.put("batch", batch);
            htLocalArguments.put("WANTDOCSUMMARY", "true");
            if (gid != null) {
                htLocalArguments.put("GenreID", gid);
            }
            if (corpora != null) {
                htLocalArguments.put("Corpora", corpora);
            }

            try {
                InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htLocalArguments);
                System.out.println("Posting file: " + sPostedPath);
                HashTree htResults = API.PostExecute(new File(sPostedPath));
                iClassifyCounter++;

                System.out.println(iClassifyCounter + " of " + iCrawlCounter + " complete.");
                System.out.println("\nFilesystem:Document: " + sPath + " classified successfully! (" + iClassifyCounter + ")");
            } catch (com.iw.fcrawl.ClassifyException ce) {
                System.out.println("There was a system error while classifying document: " + sPath);
                System.out.println("Error code: " + ce.TS_ERROR_CODE);
                System.out.println("Error message: " + ce.TS_EXCEPTION_GETMESSAGE);
                System.out.println("Error description: " + ce.TS_ERROR_DESC);
                System.out.println("");

                throw ce;
            } finally {
                if (tempFile != null) tempFile.delete();
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        }

        return true;
    }

    public boolean UpdateDocumentSecurity(String sPath, Machine M) {
        return true;
    }

    public boolean ReindexDocuments() {
        return true;
    }

    // override standard repository method and derive date from the filesystem
    public String GetCurrentDate(HashTree htFolder) throws Exception {
        String sPath = (String) htFolder.get("PATH");

        try {
            File f = new File("/temp/.file");
            FileOutputStream out = new FileOutputStream(f);
            out.write(
                    0);
            out.close();

            // Convert to: 'MM/DD/YYYY HH24:MI:SS'
            java.util.Date d = new java.util.Date(f.lastModified());
            SimpleDateFormat gmtf = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");
            //gmtf.setTimeZone( TimeZone.getTimeZone( "UTC" ) );

            System.out.println("Current date: " + gmtf.format(d));
            f.delete();

            return gmtf.format(d);

        } catch (Exception e) {
            System.out.println("Warning! Could not write a file to the target filesystem.");
            e.printStackTrace(System.out);

            return GetCurrentDate();
        }
    }

    // Lookup this document to find all associated corpora.   Then put the document
    //  path into the vector for each corpus, plus the overall vector.   These vectors
    //  will later be used to reindex documents in the full text verity engine.
    public void AddDocumentIndex(String sPath, Machine M) {

        /* This will be re-done differently, soon.
		HashTree htArguments = GetArguments();

		htArguments.put("Path", sPath);
		InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentCorpora", htArguments);
		HashTree htResults = API.Execute();

		if (!htResults.containsKey("DOCUMENT")) {
			//System.out.println("The document "+sPath+" did not classify into any topics.");
			return;
		} */
        return;
    }

    // constructor: just use the standard constructor
    public AskOnceRepository(HashTree ht, HashTree ht2) {
        SetArguments(ht2);
    }
}
