/*
 * Created by IntelliJ IDEA.
 * User: administrator
 * Date: Jul 25, 2003
 * Time: 1:57:00 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.calloutmgr;

import java.sql.Connection;
import java.io.*;
import java.util.Vector;

import api.Log;

public class CalloutMgr {

    private boolean bVerbose = false;
    String sExec = null;
    int iOUTPUTCAPTUREMODE = 0;
    String sRegExpKeepOutputIndicator = null;

    private Vector vExecPgmOutput = new Vector();

    public Vector getVecOutput()
    {
        return vExecPgmOutput;
    }

    // different modes for which lines of system output to put in the output
    public static final int IOUTPUTCAPTUREMODE_ThisRegExpOnly = 1;
    public static final int IOUTPUTCAPTUREMODE_ThisRegExpAndAllAfter = 2;
    public static final int IOUTPUTCAPTUREMODE_All = 3;

    public CalloutMgr ( String sExec_, boolean bVerbose_ )
            throws Exception
    {
        bVerbose = bVerbose_;
        sExec = sExec_;
        iOUTPUTCAPTUREMODE = 0;
    }
    public CalloutMgr ( String sExec_, boolean bVerbose_, int iOUTPUTCAPTUREMODE_, String sRegExpKeepOutputIndicator_ )
            throws Exception
    {
        bVerbose = bVerbose_;
        sExec = sExec_;
        iOUTPUTCAPTUREMODE = iOUTPUTCAPTUREMODE_;
        sRegExpKeepOutputIndicator = sRegExpKeepOutputIndicator_;  // anything including null if mode is all
    }

    public int execItSynchronous()
        throws Exception
    {
        if ( bVerbose )
            api.Log.Log("Executing command [" + sExec + "]");
        Process pro = Runtime.getRuntime().exec(sExec);
        InputStream error = pro.getErrorStream();
        InputStream output = pro.getInputStream();
        Thread err = new Thread(new OutErrReader("syserr", error));
        Thread out = new Thread(new OutErrReader("sysout", output));
        out.start();
        err.start();
        pro.waitFor();
        out.join();
        err.join();

        int iExitVal = pro.exitValue();
        if ( bVerbose )
            api.Log.Log("completed exec exit val [" + iExitVal + "]");

        if (iExitVal != 0)
        {
            api.Log.LogError("WARNING: Exec completed with rtn [" + iExitVal + "] command [" + sExec + "]");
            throw new Exception("WARNING: Exec completed with rtn [" + iExitVal + "] command [" + sExec + "] ");
        }

        return iExitVal;

    }

    public class OutErrReader implements Runnable
    {
        InputStream is;
        String sStreamName = null;

        public OutErrReader(String sStreamName, InputStream is)
        {
            this.is = is;
            this.sStreamName = sStreamName;
        }

        public void run()
        {
            try
            {
                BufferedReader in = new BufferedReader(new InputStreamReader(is));
                String sLine = null;
                int i = 0;
                boolean bHitRegExpTriggerToKeepOutput = false;
                while ((sLine = in.readLine()) != null)
                {
                    i++;

                    if ( bVerbose )
                        api.Log.Log("Exec output [" + sStreamName + "] #" + i + " line [" + sLine + "]" );

                    synchronized ( vExecPgmOutput )
                    {
                        if ( iOUTPUTCAPTUREMODE == IOUTPUTCAPTUREMODE_All)
                        {
                                vExecPgmOutput.addElement ( sLine );
                        }
                        else if ( iOUTPUTCAPTUREMODE == IOUTPUTCAPTUREMODE_ThisRegExpOnly )
                        {
                            if ( sLine.indexOf( sRegExpKeepOutputIndicator) >= 0 )
                                vExecPgmOutput.addElement ( sLine );

                        }
                        else if ( iOUTPUTCAPTUREMODE == IOUTPUTCAPTUREMODE_ThisRegExpAndAllAfter )
                        {
                            if ( bHitRegExpTriggerToKeepOutput || sLine.indexOf( sRegExpKeepOutputIndicator) >= 0 )
                            {
                                vExecPgmOutput.addElement ( sLine );
                                bHitRegExpTriggerToKeepOutput = true;
                            }
                        }
                        else if ( iOUTPUTCAPTUREMODE == 0 )
                        { // do nothing - did not want output captured
                        }
                        else
                            throw new Exception ("invalid output capture mode in CalloutMgr [" + iOUTPUTCAPTUREMODE + "]");
                    }
                }
                is.close();
            } catch (Exception e)
            {
                try
                {
                    api.Log.LogError("Stream [" + sStreamName + "] error in threaded stream reader [" + sStreamName + "]", e);
                } catch (Exception e2)
                {
                    // this is the second backstop ... nothing to do here - can't throw exception from a run
                }
            }
        }
    }

}

/* alternative method ?

from http://groups.google.com/groups?q=java+linux+Runtime.getRuntime().exec&hl=en&lr=&ie=UTF-8&selm=39DCCD29.9C898075%40libero.it&rnum=1

   class exec {
 public static void main(String[] args) throws Exception {
  Process proc = Runtime.getRuntime().exec(args[0]);
  String moreString;
//***** Reading output of the command *****
  InputStream OUT = proc.getInputStream();
  InputStreamReader Ireader = new InputStreamReader(OUT);
  BufferedReader reader = new BufferedReader(Ireader);
  java.io.BufferedReader err =
     new java.io.BufferedReader(new java.io.InputStreamReader
           (proc.getErrorStream()) );
  proc.waitFor();
  while ((moreString = reader.readLine()) != null) {
    System.out.println(moreString);
  }
  while ((moreString = err.readLine()) != null) {
    System.out.println(moreString);
  }
 }
}

 */