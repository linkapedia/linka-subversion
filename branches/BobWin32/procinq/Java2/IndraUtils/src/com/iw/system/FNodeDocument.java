package com.iw.system;

import java.io.*;
import java.util.*;

// Generic NodeDocument object
public class FNodeDocument
{
	// variables
	public String NodeID;
	public String DocumentPath;
	public String dbURL;

	// Object constructor(s)
	public FNodeDocument (String NodeID, String DocPath) {
		this.NodeID = NodeID; this.DocumentPath = DocPath;
		this.dbURL = DocPath;
	}
}
