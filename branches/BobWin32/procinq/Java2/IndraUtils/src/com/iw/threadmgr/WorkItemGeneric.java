/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Jan 15, 2003
 * Time: 12:18:29 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.threadmgr;

import java.util.Hashtable;

public abstract class WorkItemGeneric
{
    Hashtable htResultData = null;
    public void addResultData ( String sKey, String sValue )
    {
        if (htResultData == null )
            htResultData = new Hashtable();

        htResultData.put ( sKey, sValue );
    }
}
