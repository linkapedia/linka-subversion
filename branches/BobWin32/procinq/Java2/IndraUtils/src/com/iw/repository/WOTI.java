package com.iw.repository;

// This is the repository class for type Filesystem.   This class is assumed to be
//  an NT filesystem.   Please use the UNIXFilesystem class for UNIX based filesystems.
import com.iw.system.HashTree;
import com.iw.system.InvokeAPI;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

public class WOTI extends CacheCrawl {

    public String CacheFile = "/tmp/crawlcachedata.txt";

    public WOTI(HashTree ht, HashTree ht2) {
        SetName((String) ht.get("REPOSITORYNAME"));
        SetLocation((String) ht.get("REPOSITORYLOCATION"));
        SetUsername((String) ht.get("USERNAME"));
        SetPassword((String) ht.get("PASSWORD"));

        SetArguments(ht2);

        if (ht2.containsKey("cachefile")) {
            CacheFile = (String) ht2.get("cachefile");
        }
    }

    public WOTI() {
        System.out.println("WOTI() invoked (programming error).");
    }
    Hashtable htDocuments = new Hashtable();
    private int iCrawlCounter = 0;

    // Specific web crawl routine to retrieve new objects on the web site that have
    //  changed since DateLastCapture.
    public void GetNewObjects(String sURL, String sDateLastCapture, boolean bRecurse) {
        System.out.println(Thread.currentThread().getName() + " CacheCrawl looking for new classification candidates in " + sURL);

        String textline;
        BufferedReader in = null;
        String fname = CacheFile;
        //preQualFile (fname, 3);

        int iLineNum = 0;
        int iCountDups = 0;
        try {
            System.out.println("Opening file: " + fname);
            in = new BufferedReader(new FileReader(fname));
            String sURLlower = sURL.toLowerCase();

            // GENREID	DOCTITLE	AUTHOR	SOURCE	CITATION	DOCUMENTSUMMARY	SORTDATE	FULLTEXT	DOCURL	REVIEWSTATUS	SUBMITTEDBY
            while (in.ready()) {
                iLineNum++;
                String sTextLine = in.readLine();
                //if (!sTextLine.toLowerCase().startsWith(sURLlower) || sTextLine.startsWith("#") )
                //    continue;

                sTextLine = sTextLine.replaceAll("\t", "\t ");

                Vector vDataLine = split(sTextLine, "\t");
                if (vDataLine.size() == 10) {
                    vDataLine.add(0, "1");
                }
                if (vDataLine.size() != 11) {
                    throw new Exception("line " + iLineNum + " in file " + fname
                            + " is not of normal format (" + vDataLine.size() + " columns) [" + sTextLine + "]");
                }

                Hashtable ht = new Hashtable();

                //System.out.println("### "+sTextLine);
                String sGenreID = (String) vDataLine.elementAt(0);
                ht.put("GENREID", sGenreID); //.out.println("Found genreid: "+sGenreID);

                String sDocTitle = (String) vDataLine.elementAt(1);
                if (sDocTitle.length() > 1) {
                    sDocTitle = sDocTitle.substring(1);
                }
                ht.put("DOCTITLE", sDocTitle); //System.out.println("Found doctitle: "+sDocTitle);

                String sAuthor = (String) vDataLine.elementAt(2);
                if (sAuthor.length() > 1) {
                    sAuthor = sAuthor.substring(1);
                }
                ht.put("AUTHOR", sAuthor); //System.out.println("Found author: "+sAuthor);

                String sSource = (String) vDataLine.elementAt(3);
                if (sSource.length() > 1) {
                    sSource = sSource.substring(1);
                }
                ht.put("SOURCE", sSource); //System.out.println("Found source: "+sSource);

                String sCitation = (String) vDataLine.elementAt(4);
                if (sCitation.length() > 1) {
                    sCitation = sCitation.substring(1);
                }
                ht.put("CITATION", sCitation); //System.out.println("Found citation: "+sCitation);

                String sDocSum = (String) vDataLine.elementAt(5);
                if (sDocSum.length() > 1) {
                    sDocSum = sDocSum.substring(1);
                }
                ht.put("DOCSUM", sDocSum); //System.out.println("Found docsum: "+sDocSum);

                String sSortDate = (String) vDataLine.elementAt(6);
                if (sSortDate.length() > 1) {
                    sSortDate = sSortDate.substring(1);
                    sSortDate = formatWotiDate(sSortDate);
                }
                ht.put("SORTDATE", sSortDate); //System.out.println("Found date: "+sSortDate);

                String sRealPath = (String) vDataLine.elementAt(7);
                ht.put("FULLTEXT", sRealPath.substring(1)); //System.out.println("Found path: "+sRealPath);

                String sURLelement = (String) vDataLine.elementAt(8);
                ht.put("DOCURL", sURLelement.substring(1)); //System.out.println("Found url: "+sURLelement);

                String sReviewStatus = (String) vDataLine.elementAt(9);
                if (sReviewStatus.length() > 1) {
                    sReviewStatus = sReviewStatus.substring(1);
                }
                ht.put("REVIEWSTATUS", sReviewStatus); //System.out.println("Found status: "+sReviewStatus);

                String sSubmitted = (String) vDataLine.elementAt(10);
                if (sSubmitted.length() > 1) {
                    sSubmitted = sSubmitted.substring(1);
                }
                ht.put("SUBMITTED", sSubmitted); //System.out.println("Found submitted: "+sSubmitted);

                if (sGenreID.startsWith("GENREID")) {
                    continue;
                }
                if (!new File(sRealPath.substring(1)).exists()) {
                    System.out.println("ignore missing file " + sRealPath.substring(1));
                    continue;
                }

                System.out.println(" ");
                synchronized (htDocuments) {
                    iCrawlCounter++;
                    htDocuments.put(sURLelement.substring(1), ht);
                    AddNewDocument(sURLelement.substring(1));
                }
            }

            in.close();
            System.out.println(" ");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            System.out.println(new java.util.Date() + " done cache crawling - thread: " + Thread.currentThread().getName()
                    + " #lines:" + iLineNum
                    + " #unique urls" + htDocuments.size());
            System.out.println("");
            System.out.flush();
        }
    }

    public void preQualFile(String fname, int iCountDesired) {

        String textline;
        BufferedReader in = null;

        int iLineNum = 0;
        int iCountDups = 0;
        try {
            in = new BufferedReader(new FileReader(fname));
            while (in.ready()) {
                iLineNum++;
                String sTextLine = in.readLine();
                if (sTextLine.startsWith("#")) {
                    continue;
                }
                if (sTextLine.startsWith("GENREID")) {
                    continue;
                }

                Vector vDataLine = split(sTextLine, "\t");
                if (vDataLine.size() != 2) {
                    throw new Exception("line " + iLineNum + " in file " + fname
                            + " is not of normal format [" + sTextLine + "]");
                }
            }

            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    // hbk control - do I get here?
    private int iCallcount_ClassifyDocument = 0;
    private int iClassifyCounter = 0;

    public boolean ClassifyDocument(String sURL, com.iw.fcrawl.Machine M) {
        long lStartClassify = System.currentTimeMillis();
        HashTree htArguments = GetArguments();
        if (sURL == null) {
            System.out.println("Path was null.");
            return false;
        }

        Hashtable ht = (Hashtable) htDocuments.get(sURL);
        String sPath = (String) ht.get("FULLTEXT");

        try {
            String api = M.GetAPI();
            String rid = (String) htArguments.get("rid");
            String gid = (String) htArguments.get("gid");
            String batch = (String) htArguments.get("batch");
            String docsumm = (String) htArguments.get("docsumm");

            HashTree htLocalArguments = new HashTree();

            htLocalArguments.put("SKEY", M.GetSKEY());
            htLocalArguments.put("api", api);
            htLocalArguments.put("RepositoryID", rid);
            htLocalArguments.put("ShowScores", "true");
            htLocalArguments.put("DBURL", URLEncoder.encode(sURL, "UTF-8"));
            if (batch != null) {
                htLocalArguments.put("batch", batch);
            }
            if (docsumm != null) {
                htLocalArguments.put("docsumm", docsumm);
            }
            htLocalArguments.put("post", "true");   // hbk control
            htLocalArguments.put("FulltextURL", sPath);
            if (gid != null) {
                htLocalArguments.put("genreid", gid);
            }

            if (!((String) ht.get("GENREID")).equals(" ")) {
                htLocalArguments.put("genreid", (String) ht.get("GENREID"));
            }
            if (!((String) ht.get("DOCTITLE")).equals(" ")) {
                htLocalArguments.put("doctitle", (String) ht.get("DOCTITLE"));
            }
            if (!((String) ht.get("AUTHOR")).equals(" ")) {
                htLocalArguments.put("author", (String) ht.get("AUTHOR"));
            }
            if (!((String) ht.get("SOURCE")).equals(" ")) {
                htLocalArguments.put("source", (String) ht.get("SOURCE"));
            }
            if (!((String) ht.get("CITATION")).equals(" ")) {
                htLocalArguments.put("citation", (String) ht.get("CITATION"));
            }
            if (!((String) ht.get("DOCSUM")).equals(" ")) {
                htLocalArguments.put("docsummary", (String) ht.get("DOCSUM"));
            }
            if (!((String) ht.get("SORTDATE")).equals(" ")) {
                htLocalArguments.put("sortdate", (String) ht.get("SORTDATE"));
            }
            if (!((String) ht.get("REVIEWSTATUS")).equals(" ")) {
                htLocalArguments.put("reviewstatus", (String) ht.get("REVIEWSTATUS"));
            }
            if (!((String) ht.get("SUBMITTED")).equals(" ")) {
                htLocalArguments.put("submitted", (String) ht.get("SUBMITTED"));
            }

            iCallcount_ClassifyDocument++;
            System.out.println("tname [" + Thread.currentThread().getName() + "] Classify #" + iCallcount_ClassifyDocument
                    + ": DBURL: " + sURL + " Posted path: " + sPath + " Real path: " + sPath);

            System.out.println(Thread.currentThread().getName() + " @@@ post classify #" + iClassifyCounter + " ms ["
                    + (System.currentTimeMillis() - lStartClassify) + "] " + iClassifyCounter + " of " + iCrawlCounter
                    + " complete in ms [" + (System.currentTimeMillis() - lStartClassify) + "] tname ["
                    + Thread.currentThread().getName() + "] ");
            System.out.println("\r\n" + Thread.currentThread().getName() + "pre classify " + iClassifyCounter
                    + " of " + iCrawlCounter + " complete tname [" + Thread.currentThread().getName() + "].");

            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htLocalArguments);
            HashTree htResults = null;
            boolean bRunClassify = false; //debug hbk control
            if (bRunClassify) {
                htResults = new HashTree();
            } else {
                //System.out.print("not doing actual classifications yet");
                htResults = API.PostExecute(new File(sPath));  // hbk control
            };

            if (!htResults.containsKey("CLASSIFICATIONRESULTSET")) {
                System.out.println("\nThe document " + sPath + " did not classify " + " into any topics. (" + iClassifyCounter + ")");
                return false;
            }
            iClassifyCounter++;

        } catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        } finally {
            System.out.println("\r\n" + Thread.currentThread().getName() + "post classify " + iClassifyCounter
                    + " of " + iCrawlCounter + " complete tname [" + Thread.currentThread().getName() + "].");
        }

        return true;
    }

    public static Vector split(String s, String splitterDelimiters) {
        Vector v = new Vector();
        StringTokenizer st = new StringTokenizer(s, splitterDelimiters);

        String ss = null;
        while (st.hasMoreElements()) {
            ss = (String) st.nextElement();
            v.addElement(ss);
        }
        return v;
    }
    static boolean bSemaphore = false;

    public static void addLineToFile(String filename, String lineToadd) {
//System.out.println ("in addline to file [" + filename + "] [" + lineToadd + "]" );

//System.out.println ("PAUSE  400\r\n");
//clsUtils.pause_this_many_milliseconds ( 400 );
        if (filename == null) {
            System.out.println("addLineToFile : null filename for line to add of [" + lineToadd + "]");
        }
        int iTry = 0;
        while (true) {

            try {
// works both for existing and new files
                PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
                file.write(lineToadd);
                file.close();
                break;
            } catch (FileNotFoundException e) {
                long lPauseTime = (long) ((double) iTry * (double) 1000);
                if (iTry < 10) {
                    System.out.println(" addLineToFile location 1 will retry can't write - this is retry #[" + iTry + "] and pause (ms) of  [" + lPauseTime
                            + "] file locked [" + filename + "] text [" + lineToadd + "] retry up to 10 times ");
                    iTry++;
                } else {
                    System.out.println(" addLineToFile location 1 done retrying - GIVE UP on try [" + iTry + "] and pause (ms) of  [" + lPauseTime
                            + "] file locked [" + filename + "] text [" + lineToadd + "] gave up after 10 times ");

                }
            } catch (Exception e) {
                System.out.println("can't add line to file [" + filename + "] text [" + lineToadd + "]");
            }
        }
    } // addlinetofile

    public boolean UpdateDocumentSecurity(String sPath, com.iw.fcrawl.Machine M) {
        return true;
    }

    public boolean ReindexDocuments() {
        return true;
    }

    public String formatWotiDate(String str) {
        SimpleDateFormat sdfIn = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss zzz");
        //SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat sdfOut = new SimpleDateFormat("MM/dd/yyyy");
        java.util.Date date = new java.util.Date();
        String dateOut = new String();

        try {
            date = sdfIn.parse(str);
            dateOut = sdfOut.format(date);

        } catch (Exception e) {
            System.out.println("ERROR!  Poorly formed Date found in Metadata file.  " + e.toString());
        }

        return dateOut;
    }

    // Lookup this document to find all associated corpora.   Then put the document
    //  path into the vector for each corpus, plus the overall vector.   These vectors
    //  will later be used to reindex documents in the full text verity engine.
    public void AddDocumentIndex(String sPath, com.iw.fcrawl.Machine M) {

        /*
         * This will be re-done differently, soon.
         * HashTree htArguments = GetArguments();
         *
         * htArguments.put("Path", sPath);
         * InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentCorpora", htArguments);
         * HashTree htResults = API.Execute();
         *
         * if (!htResults.containsKey("DOCUMENT")) {
         * //System.out.println("The document "+sPath+" did not classify into any topics.");
         * return;
         * }
         */
        return;
    }
}
// constructor: just use the standard constructor


/*
        try
        {
            Statement stmt = null;
            ResultSet rslt = null;

            try
            {
                stmt = dbc.createStatement();
                rslt = stmt.executeQuery(sSQL);

                while (rslt.next())
                {
                    String sDOCCACHEFILE = rslt.getString(1);
                    System.out.println("sDOCCACHEFILE [" + sDOCCACHEFILE + "]");
                }
            } finally
            {
                rslt.close();
                stmt.close();
            }

        }
        catch (Exception e)
        {
            System.out.println("error in crawlcache : " + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            if (dbc != null)
                try { dbc.close();} catch ( Exception e2 ) { System.out.println("error closing dbc in crawlcache"); e2.printStackTrace(); }
        }

*/