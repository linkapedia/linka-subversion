/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Jan 14, 2003
 * Time: 3:48:01 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.threadmgr;

import java.util.Vector;


/**
 * This class is the superclass to cover both producer and consumer threads in a
 * blocked Q currently implemented as WorkQManager which is:
 * Work Queue and Producer-Consumer Thread Manager Generic
 * Generic means that this will work for any producer consumer problem.
 */


public abstract class ThreadGenericProdCons extends Thread {

    protected String sDesc;
    protected WorkQManager workQ;
    protected String sDB_OracleJDBC = null;
    protected String sDB_user = null;
    protected String sDB_pass = null;
    protected boolean bStopLooping = false;
    protected int iCountProducedOrConsumed = 0;
    protected int iCountConsumedAndCompleted = 0;  // relevant for a consumer only

    /**
     * set the description of the thread and the Q to put things into
     */
    public ThreadGenericProdCons (  String sDesc_,
                                    WorkQManager prodConsWorkQGeneric_,
                                    String sDB_OracleJDBC_,
                                    String sDB_user_,
                                    String sDB_pass_
                                  )
    {
        sDesc = sDesc_;
        workQ = prodConsWorkQGeneric_; // where to put torobjects
        sDB_OracleJDBC = sDB_OracleJDBC_; // optional
        sDB_user = sDB_user_; // optional
        sDB_pass = sDB_pass_; // optional
    }

    public ThreadGenericProdCons (  String sDesc_,
                                    WorkQManager prodConsWorkQGeneric_
                                  )
    {
        sDesc_ = sDesc;
        workQ = prodConsWorkQGeneric_;
    }

    public void stopLooping ()
    {
        bStopLooping = true;
    }

    public void run ()
    {
        System.out.println("coding/design ERROR - subclass of abstract class ThreadGenericProdCons failed to implement run() - this ThreadGenericProdCons run is executing");
    }



}
