package com.iw.fcrawl;

import java.io.*;
import java.util.*;

// Generic Node object
public class DocumentContainer
{
	// variables
	public String sURL;
	public String sPath;
    public String sDate;
    public String sTitle;

    public DocumentContainer () {}
    public DocumentContainer (Vector v) {
        sURL = (String) v.elementAt(0);
        sPath = (String) v.elementAt(1);

        if (v.size() == 3) sTitle = (String) v.elementAt(2);
        if (v.size() == 4) sDate = (String) v.elementAt(3);
    }
}
