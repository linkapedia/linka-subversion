package com.iw.system;

import api.security.Group;
import api.security.LDAP_Connection;
import api.security.TSLogin;
import com.indraweb.execution.Session;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.Connection;
import java.util.*;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;

// The user class represents an instantiation of an individual user of the Taxonomy Server
public class User implements Serializable {
    // Private attributes
    //
    // The string, float, and int values represent individual attributes of the user object
    // htNodesTracked contains a hashtable of node identifiers with the nodes this user is currently tracking
    // htExpertNodes contains a hashtable of node identifiers with the nodes in which this user has an expert rating
    // vGroups is a vector of group access permissions that this user has.

    private String dn;
    private String sUsername;
    private String sFullname;
    private String sPassword;
    private String sPhone;
    private String sEmail;
    private String sUserID;
    private float fScoreThreshold;
    private int iResultsPerPage;
    private int iUserStatus;
    private int iEmailStatus;
    private Hashtable htNodesTracked;
    private Hashtable htExpertNodes;
    private Hashtable htGroupPermissions = new Hashtable();
    private HashSet hsSecurityPermissions = new HashSet();
    private String sDocumentSecurityHexmask;
    private String SessionKey;
    // idrac only
    private Vector vDomainsAllowed = new Vector();

    // Accessor methods to private variables
    public String GetDN() {
        return dn;
    }

    public String GetUsername() {
        return sUsername;
    }

    public String GetFullname() {
        return sFullname;
    }

    public String GetPassword() {
        return sPassword;
    }

    public String GetPhoneNum() {
        return sPhone;
    }

    public String GetEmail() {
        return sEmail;
    }

    public String GetUserID() {
        return sUserID;
    }

    public float GetScoreThreshold() {
        return fScoreThreshold;
    }

    public int GetResultsPerPage() {
        return iResultsPerPage;
    }

    public int GetUserStatus() {
        return iUserStatus;
    }

    public int GetEmailStatus() {
        return iEmailStatus;
    }

    public Hashtable GetNodeHash() {
        return htNodesTracked;
    }

    public Hashtable GetExpertNodes() {
        return htExpertNodes;
    }

    public Hashtable GetGroupPermissions() {
        if (htGroupPermissions == null) {
            htGroupPermissions = new Hashtable();
        }
        return htGroupPermissions;
    }

    public HashSet GetSecurityPermissions() {
        if (hsSecurityPermissions == null) {
            hsSecurityPermissions = new HashSet();
        }
        return hsSecurityPermissions;
    }

    public String GetDocumentSecurityHexmask() {
        return sDocumentSecurityHexmask;
    }

    public String GetSessionKey() {
        return SessionKey;
    }

    // Set value methods for private variables
    public void SetDN(String s) {
        dn = s;
    }

    public void SetUsername(String s) {
        sUsername = s;
    }

    public void SetFullname(String s) {
        sFullname = s;
    }

    public void SetPassword(String s) {
        sPassword = s;
    }

    public void SetPhoneNum(String s) {
        sPhone = s;
    }

    public void SetEmail(String s) {
        sEmail = s;
    }

    public void SetUserID(String s) {
        sUserID = s;
    }

    public void SetScoreThreshold(float f) {
        fScoreThreshold = new Float(f).floatValue();
    }

    public void SetResultsPerPage(int i) {
        iResultsPerPage = i;
    }

    public void SetUserStatus(int i) {
        iUserStatus = i;
    }

    public void SetEmailStatus(int i) {
        iEmailStatus = i;
    }

    public void SetSessionKey(String s) {
        SessionKey = s;
    }

    public void SetGroupPermissions(Hashtable ht) {
        htGroupPermissions = ht;
    }

    public void SetSecurityPermissions(HashSet hs) {
        hsSecurityPermissions = hs;
    }

    public void SetDocumentSecurityHexmask(String hexmask) {
        sDocumentSecurityHexmask = hexmask;
    }

    public Vector SetDomainsAllowed(String s) { // idrac only
        String[] domains = s.split(";");
        for (int i = 0; i < domains.length; i++) {
            vDomainsAllowed.addElement(domains[i]);
        }
        return vDomainsAllowed;
    }

    public void GrantNodeTracked(String NodeID) {
        htNodesTracked.put(NodeID, "1");
    }

    public void GrantNodeTracked(int NodeID) {
        htNodesTracked.put("" + NodeID, "1");
    }

    public void RevokeNodeTracked(String NodeID) {
        htNodesTracked.put(NodeID, "0");
    }

    public void RevokeNodeTracked(int NodeID) {
        htNodesTracked.put("" + NodeID, "0");
    }

    public void GrantNodeExpert(String NodeID, String AccessLevel) {
        htExpertNodes.put(NodeID, AccessLevel);
    }

    public void GrantNodeExpert(int NodeID, String AccessLevel) {
        htExpertNodes.put("" + NodeID, AccessLevel);
    }

    public void GrantNodeExpert(String NodeID, int AccessLevel) {
        htExpertNodes.put(NodeID, "" + AccessLevel);
    }

    public void GrantNodeExpert(int NodeID, int AccessLevel) {
        htExpertNodes.put("" + NodeID, "" + AccessLevel);
    }

    public void RevokeNodeExpert(String NodeID) {
        htExpertNodes.put(NodeID, "0");
    }

    public void RevokeNodeExpert(int NodeID) {
        htExpertNodes.put("" + NodeID, "0");
    }

    // Object constructors
    public User(String dn, String sUsername, String sFullname, String sPassword, String sPhone,
            String sEmail, String sUserID, float fScoreThreshold,
            int iResultsPerPage, int iUserStatus, int iEmailStatus) {

        this.dn = dn;
        this.sUsername = sUsername;
        this.sFullname = sFullname;
        this.sPassword = sPassword;
        this.sPhone = sPhone;
        this.sEmail = sEmail;
        this.sUserID = sUserID;
        this.fScoreThreshold = fScoreThreshold;
        this.iResultsPerPage = iResultsPerPage;
        this.iUserStatus = iUserStatus;
        this.iEmailStatus = iEmailStatus;

        this.htNodesTracked = new Hashtable();
        this.htExpertNodes = new Hashtable();
    }

    public User() {
        this.fScoreThreshold = new Float(50.0).floatValue();
        this.iResultsPerPage = 10;

        // User status: -1 (not activated), 0 (disabled), 1 (active)
        this.iUserStatus = -1;
        this.iEmailStatus = 0;

        this.htNodesTracked = new Hashtable();
        this.htExpertNodes = new Hashtable();
    }

    @Override
    public String toString() {
        return this.GetDN();
    }

    // factory pattern method
    public static HashSet GetSecurityIdentifiers(String UserDN, Connection dbc) {
        // instantiate an LDAP connection by forcing ADMIN user
        //  this actually breaks our current paradigm but is required
        //  in order to get security permissions for other users
        String LdapUsername = Session.cfg.getProp("LdapUsername");
        String LdapPassword = Session.cfg.getProp("LdapPassword");

        LDAP_Connection lc = new LDAP_Connection(LdapUsername, LdapPassword, null);
        try { // try to establish an LDAP connection and return user information
            User u = lc.GetUser(UserDN);
            if (u == null) {
                throw new Exception("UserDN does not exist: " + UserDN);
            }

            lc.Refresh_Connection();
            Hashtable ht = lc.GetGroups(u);
            HashSet hs = TSLogin.GetSecurityPermissions(ht, dbc, lc);

            return hs;
        } catch (Exception e) {
            api.Log.LogError(e);
            return new HashSet();
        } finally {
            lc.close();
        }
    }

    public static User getUser(String UserDN, Connection dbc) throws Exception {
        // instantiate an LDAP connection by forcing ADMIN user
        //  this actually breaks our current paradigm but is required
        //  in order to get security permissions for other users
        String LdapUsername = Session.cfg.getProp("LdapUsername");
        String LdapPassword = Session.cfg.getProp("LdapPassword");

        LDAP_Connection lc = new LDAP_Connection(LdapUsername, LdapPassword, null);
        try { // try to establish an LDAP connection and return user information
            User u = lc.GetUser(UserDN);
            if (u == null) {
                throw new Exception("UserDN does not exist: " + UserDN);
            }

            lc.Refresh_Connection();
            Hashtable ht = lc.GetGroups(u);
            HashSet hs = TSLogin.GetSecurityPermissions(ht, dbc, lc);
            u.SetGroupPermissions(ht);
            u.SetSecurityPermissions(hs);
            u.SetDocumentSecurityHexmask(api.security.DocumentSecurity.getSecurity(hs));

            api.Log.Log("login requested for notification.  user mask: " + u.GetDocumentSecurityHexmask());

            return u;
        } catch (Exception e) {
            api.Log.LogError(e);
            throw e;
        } finally {
            lc.close();
        }
    }

    // Check authorization to add alert by e-mail address
    public boolean DomainAuthorized(String EmailAddress) {
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return true;
        }

        Enumeration ev = vDomainsAllowed.elements();

        while (ev.hasMoreElements()) {
            String s = (String) ev.nextElement();
            if (EmailAddress.toLowerCase().indexOf(s) != -1) {
                return true;
            }
        }

        return false;
    }

    // Check authorization of this user for a given document
    public boolean IsAuthorized(String documentHexmask) {
        return api.security.DocumentSecurity.isAuthorized(documentHexmask, GetDocumentSecurityHexmask());
    }

    // Check authorization of this user for a given corpus
    public boolean IsAuthorized(int CorpusID, PrintWriter out) {
        return IsAuthorized("" + CorpusID, out);
    }

    public boolean IsAuthorized(String CorpusID, PrintWriter out) {
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return true;
        }

        LDAP_Connection lc = null;

        try {
            Hashtable htGroups = null;
            if (GetGroupPermissions().isEmpty()) {
                lc = new LDAP_Connection(this.GetDN(), this.GetPassword(), out);
                htGroups = lc.GetGroups(this);
            } else {
                htGroups = GetGroupPermissions();
            }

            Enumeration he = htGroups.elements();

            while (he.hasMoreElements()) {
                Group g = (Group) he.nextElement();
                if (g.IsAuthorized(CorpusID)) {
                    return true;
                }
            }
            if (IsMember(out)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            api.Log.LogError(e);
            return false;
        } finally {
            if (lc != null) {
                lc.close();
            }
        }
    }

    public Hashtable GetAdminAuthorizedHash(PrintWriter out) {
        Hashtable htReturn = new Hashtable();
        LDAP_Connection lc = null;

        try {
            Hashtable htGroups = null;
            if (GetGroupPermissions().isEmpty()) {
                lc = new LDAP_Connection(this.GetDN(), this.GetPassword(), out);
                htGroups = lc.GetGroups(this);
            } else {
                htGroups = GetGroupPermissions();
            }

            Enumeration he = htGroups.elements();

            while (he.hasMoreElements()) {
                Group g = (Group) he.nextElement();
                Hashtable ht = g.GetAdminAccess();

                Enumeration et = ht.keys();
                while (et.hasMoreElements()) {
                    String s = (String) et.nextElement();
                    String v = (String) ht.get(s);

                    if (v.equals("1")) {
                        htReturn.put(s, v);
                    }
                }
            }
            return htReturn;

        } catch (Exception e) {
            api.Log.LogError(e);
        } finally {
            if (lc != null) {
                lc.close();
            }
        }
        return htReturn;
    }

    public Hashtable GetAuthorizedHash(PrintWriter out) {
        Hashtable htReturn = new Hashtable();
        LDAP_Connection lc = null;

        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return htReturn;
        }

        try {
            Hashtable htGroups = null;
            if (GetGroupPermissions().isEmpty()) {
                lc = new LDAP_Connection(this.GetDN(), this.GetPassword(), out);
                htGroups = lc.GetGroups(this);
            } else {
                htGroups = GetGroupPermissions();
            }

            Enumeration he = htGroups.elements();

            while (he.hasMoreElements()) {
                Group g = (Group) he.nextElement();
                Hashtable ht = g.GetCorpusAccess();

                Enumeration et = ht.keys();
                while (et.hasMoreElements()) {
                    String s = (String) et.nextElement();
                    String v = (String) ht.get(s);

                    htReturn.put(s, v);
                }
            }
            return htReturn;

        } catch (Exception e) {
            api.Log.LogError(e);
        } finally {
            if (lc != null) {
                lc.close();
            }
        }
        return htReturn;
    }

    public boolean IsAdmin(int CorpusID, PrintWriter out) {
        return IsAdmin("" + CorpusID, out);
    }

    public boolean IsAdmin(String CorpusID, PrintWriter out) {
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return true;
        }

        LDAP_Connection lc = null;

        try {
            Hashtable htGroups = null;

            if (GetGroupPermissions().isEmpty()) {
                lc = new LDAP_Connection(this.GetDN(), this.GetPassword(), out);
                htGroups = lc.GetGroups(this);
            } else {
                htGroups = GetGroupPermissions();
            }

            Enumeration he = htGroups.elements();

            while (he.hasMoreElements()) {
                Group g = (Group) he.nextElement();
                if (g.IsAdmin(CorpusID)) {
                    return true;
                }
            }
            if (IsMember(out)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            api.Log.LogError(e);
            return false;
        } finally {
            if (lc != null) {
                lc.close();
            }
        }
    }

    // Check to see if user is a member of a specific group.   If no arguments given,
    // assume we are checking the admin group
    public boolean IsMember(PrintWriter out) {
        try {
            return IsMember(com.indraweb.execution.Session.cfg.getProp("AdminGroup"), out);
        } catch (Exception e) {
            api.Log.LogError(e);
            return false;
        }
    }

    public boolean IsMember(String sGroupDN, PrintWriter out) {
        // changed 03/21/03 -- why call LDAP? isn't this in the user object?
        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
            return true;
        }
        if (this.htGroupPermissions.containsKey(sGroupDN)) {
            return true;
        }
        return false;
    }

    // Saving users is different depending upon which authentication system you use
    public boolean SaveUser(PrintWriter out) {
        String sLdapUsername = Session.cfg.getProp("LdapUsername");
        String sLdapPassword = Session.cfg.getProp("LdapPassword");
        LDAP_Connection lc = new LDAP_Connection(sLdapUsername, sLdapPassword, out);

        try {
            if (lc.GetAuthentication() == 1) {
                return SaveUserAD(out, lc);
            }
            return SaveUserOpenLDAP(out, lc);
        } catch (Exception e) {
            api.Log.LogError(e);
            return false;
        } finally {
            lc.close();
        }
    }

    public boolean SaveUserAD(PrintWriter out, LDAP_Connection lc) {
        try {
            DirContext ctx = lc.getDirContext();
            String sTempDN = this.dn;

            if (sTempDN.toUpperCase().endsWith(lc.GetDC().toUpperCase())) {
                sTempDN = sTempDN.substring(0, sTempDN.length() - (lc.GetDC().length() + 1));
            }

            // Specify the changes to make
            int iModificationItems = 8;

            // Get the attribute list -- need a baseline to compare
            Attributes attrs = ctx.getAttributes(sTempDN);
            Attribute aE = attrs.get("expertNodes");
            Attribute aN = attrs.get("nodesTracked");

            // Create new attributes
            Attribute htN = new BasicAttribute("nodesTracked");
            Attribute htE = new BasicAttribute("expertNodes");
            Attribute aS = new BasicAttribute("scoreThreshold", new Float(this.GetScoreThreshold()).toString());
            Attribute aR = new BasicAttribute("resultsPerPage", new Integer(this.GetResultsPerPage()).toString());
            Attribute aU = new BasicAttribute("userStatus", new Integer(this.GetUserStatus()).toString());
            Attribute aES = new BasicAttribute("emailStatus", new Integer(this.GetEmailStatus()).toString());

            // Get keys
            Enumeration htNe = this.GetNodeHash().keys();
            Enumeration htEe = this.GetExpertNodes().keys();

            int iNodeCounter = 0;
            int iExpertCounter = 0;

            // Loop through our hashtables and build attribute hashs
            while (htNe.hasMoreElements()) {
                String sNodeID = (String) htNe.nextElement();
                String sIs = (String) this.GetNodeHash().get(sNodeID);
                if (sIs.equals("1")) {
                    htN.add(sNodeID);
                    iNodeCounter++;
                }
            }
            while (htEe.hasMoreElements()) {
                String sNodeID = (String) htEe.nextElement();
                String sIs = (String) this.GetExpertNodes().get(sNodeID);
                htE.add(sNodeID + "A" + sIs);
                iExpertCounter++;
            }

            if (aN == null) {
                iModificationItems = iModificationItems - 1;
            }
            if (aE == null) {
                iModificationItems = iModificationItems - 1;
            }
            if (iNodeCounter == 0) {
                iModificationItems = iModificationItems - 1;
            }
            if (iExpertCounter == 0) {
                iModificationItems = iModificationItems - 1;
            }

            ModificationItem[] mods = new ModificationItem[iModificationItems];
            // Set the simple attributes of User
            if (attrs.get("scoreThreshold") == null) {
                mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, aS);
            } else {
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, aS);
            }
            if (attrs.get("resultsPerPage") == null) {
                mods[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE, aR);
            } else {
                mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, aR);
            }
            if (attrs.get("userStatus") == null) {
                mods[2] = new ModificationItem(DirContext.ADD_ATTRIBUTE, aU);
            } else {
                mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, aU);
            }
            if (attrs.get("emailStatus") == null) {
                mods[3] = new ModificationItem(DirContext.ADD_ATTRIBUTE, aES);
            } else {
                mods[3] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, aES);
            }

            int iModItem = 4;

            // Always begin by removing everything
            if (aN != null) {
                mods[iModItem] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("nodesTracked"));
                iModItem++;
            }
            if (aE != null) {
                mods[iModItem] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("expertNodes"));
                iModItem++;
            }

            // Create modification items for nodesTracked and expertNodes
            if (iNodeCounter > 0) {
                mods[iModItem] = new ModificationItem(DirContext.ADD_ATTRIBUTE, htN);
                iModItem++;
            }
            if (iExpertCounter > 0) {
                mods[iModItem] = new ModificationItem(DirContext.ADD_ATTRIBUTE, htE);
                iModItem++;
            }

            // Now: modify attributes and classes in Active Directory, and
            //  store results in a session variable for caching
            ctx.modifyAttributes(sTempDN, mods);
            com.indraweb.execution.Session.htUsers.put(this.SessionKey, this);

            return true;

        } catch (Exception e) {
            api.Log.LogError(e);
            return false;
        }
    }

    public boolean SaveUserOpenLDAP(PrintWriter out, LDAP_Connection lc) {
        try {
            DirContext ctx = lc.getDirContext();
            String sTempDN = this.dn;

            if (sTempDN.endsWith(lc.GetDC())) {
                sTempDN = sTempDN.substring(0, sTempDN.length() - (lc.GetDC().length() + 1));
            }

            Attributes attrs = ctx.getAttributes(sTempDN);

            // We always save users as Indraperson
            Attribute oc = attrs.get("objectClass");
            NamingEnumeration ne = oc.getAll();
            while (ne.hasMore()) {
                oc.add((String) ne.next());
            }
            oc.add("Indraperson");

            // Save hashtable containing Nodes Tracked
            Attribute htN = new BasicAttribute("nodesTracked");
            Enumeration htNe = this.GetNodeHash().keys();
            while (htNe.hasMoreElements()) {
                String sNodeID = (String) htNe.nextElement();
                String sIs = (String) this.GetNodeHash().get(sNodeID);
                if (sIs.equals("1")) {
                    htN.add(sNodeID);
                }
            }

            // Save hashtable containing Expert Nodes
            Attribute htE = new BasicAttribute("expertNodes");
            Enumeration htEe = this.GetExpertNodes().keys();
            while (htEe.hasMoreElements()) {
                String sNodeID = (String) htEe.nextElement();
                String sIs = (String) this.GetExpertNodes().get(sNodeID);
                htE.add(sNodeID + "A" + sIs);
            }

            // Now save individual fields inside Indraperson
            Attribute aS = new BasicAttribute("scoreThreshold", new Float(this.GetScoreThreshold()).toString());
            Attribute aR = new BasicAttribute("resultsPerPage", new Integer(this.GetResultsPerPage()).toString());
            Attribute aU = new BasicAttribute("userStatus", new Integer(this.GetUserStatus()).toString());
            Attribute aE = new BasicAttribute("emailStatus", new Integer(this.GetEmailStatus()).toString());

            if (this.GetNodeHash().size() > 0) {
                attrs.put(htN);
            } else {
                attrs.remove("nodesTracked");
            }
            if (this.GetExpertNodes().size() > 0) {
                attrs.put(htE);
            } else {
                attrs.remove("expertNodes");
            }

            attrs.put(oc);
            attrs.put(aS);
            attrs.put(aR);
            attrs.put(aU);
            attrs.put(aE);

            ctx.modifyAttributes(sTempDN, DirContext.REPLACE_ATTRIBUTE, attrs);
            //ctx.rebind(sTempDN, this, attrs);
            com.indraweb.execution.Session.htUsers.put(this.SessionKey, this);

            return true;

        } catch (Exception e) {
            e.printStackTrace(out);
            return false;
        }
    }

    public HashMap GetAuthorizedCorpora(PrintWriter out) {
        HashMap hm = new HashMap();
        Hashtable htAuth = this.GetAuthorizedHash(out);
        Enumeration e = htAuth.keys();
        while (e.hasMoreElements()) {
            hm.put(e.nextElement(), "1");
        }
        return hm;
    }

    public String GetAuthorizedCorporaAsString(PrintWriter out) {
        String s = "0";
        Hashtable htAuth = this.GetAuthorizedHash(out);
        Enumeration e = htAuth.keys();
        while (e.hasMoreElements()) {
            s = s + "," + e.nextElement();
        }
        return s;
    }
}
