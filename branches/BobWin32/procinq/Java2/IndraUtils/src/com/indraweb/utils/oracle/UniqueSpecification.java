/*
This class is used to generate unique identifiers/specifications for various objects within the
system.  Static classes are synchronized to prevent the same identifier from being handed to the
client in more than one instance.
 */
package com.indraweb.utils.oracle;

import java.sql.*;

public class UniqueSpecification {

    private static Integer iCorpusSemaphore = new Integer(1);
    private static Integer iNodeSemaphore = new Integer(1);

    // Get the next available CORPUSID from within the database.  If the boolean argument is
    // set to true, this is an Indraweb server and identification should be generated in the lower range.
    public static int getSubsequentCorpusID(Connection dbc) throws Exception {
        return getSubsequentCorpusID(dbc, false);
    }

    public static int getSubsequentCorpusID(Connection dbc, boolean bIndraweb) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        String sSQL = "SELECT count(*) FROM corpus WHERE ";

        int iCorpusID = 1000000;
        int iCount = 0;

        synchronized (iCorpusSemaphore) {
            if (bIndraweb) {
                sSQL = sSQL + "corpusid < 1000000";
            } else {
                sSQL = sSQL + "corpusid >= 1000000";
            }

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                rs.next();
                iCount = rs.getInt(1);
            } catch (Exception e) {
                api.Log.LogError(e);
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            if (iCount > 0) {
                sSQL = "SELECT MAX(corpusid) FROM corpus WHERE ";

                if (bIndraweb) {
                    sSQL = sSQL + "corpusid < 1000000";
                } else {
                    sSQL = sSQL + "corpusid >= 1000000";
                }

                try {
                    stmt = dbc.createStatement();
                    rs = stmt.executeQuery(sSQL);

                    rs.next();
                    iCorpusID = rs.getInt(1);
                    iCorpusID++;
                } catch (Exception e) {
                    api.Log.LogError(e);
                } finally {
                    if (rs != null) {
                        rs.close();
                        rs = null;
                    }
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                }
            }
        }

        return iCorpusID;
    }

    // Get the next available NODEID from within the database.  If the boolean argument is
    // set to true, this is an Indraweb server and identification should be generated in the lower range.
    public static long getSubsequentNodeID(Connection dbc) throws Exception {
        return getSubsequentNodeID(dbc, false);
    }

    public static long getSubsequentNodeID(Connection dbc, boolean bIndraweb) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;

        long iNodeID = 100000000;

        synchronized (iNodeSemaphore) {
            String sSQL = "SELECT MAX(nodeid) FROM node";

            /* 			//FIXME
            if (bIndraweb)
            sSQL = sSQL + "nodeid < 100000000";
            else
            sSQL = sSQL + "nodeid >= 100000000"; */

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                rs.next();
                iNodeID = rs.getLong(1);
                iNodeID++;
            } catch (Exception e) {
                api.Log.LogError(e);
                return iNodeID;
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        }

        api.Log.Log("Returning: " + iNodeID);

        return iNodeID;
    }
}
