/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Apr 1, 2004
 * Time: 2:07:13 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.indraweb.utils.oracle;

import com.iw.system.Index;
import com.iw.system.IndraField;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.UtilSets;

import java.util.*;
import java.sql.*;

public class CustomFields
{

    // get all custom fields for a given table
    private static Hashtable htTablesCached = null;

    public static synchronized void getFieldNames_cacheInvalidate ()
    {
        htTablesCached = null;
    }

    public static synchronized Hashtable getFields (String sTableName , Connection dbc) throws Exception
    {
        if (htTablesCached == null) htTablesCached = new Hashtable ();
        if (htTablesCached.containsKey (sTableName)) return (Hashtable) htTablesCached.get(sTableName);

        Hashtable htGetFields_StrTabName_cache = new Hashtable();

        String sSQL = "select column_name, data_type, data_length from user_tab_columns where " +
                "table_name = '" + sTableName.toUpperCase () + "'";
        Statement stmt = null;
        ResultSet rs = null;

        try
        {
            stmt = dbc.createStatement ();
            rs = stmt.executeQuery (sSQL);

            while (rs.next ())
            {
                IndraField inf = new IndraField (rs.getString (1) , rs.getString (2) , rs.getInt (3));
                //api.Log.Log ("table [" + sTableName + "] has col [" + inf.getName () + "]");
                htGetFields_StrTabName_cache.put (inf.getName () , inf);
            }

        } catch ( Exception e )
        {
            throw e;
        } finally
        {
            if (rs != null)
            {
                rs.close ();
                rs = null;
            }
            if (stmt != null)
            {
                stmt.close ();
                stmt = null;
            }
        }

        htTablesCached.put (sTableName, htGetFields_StrTabName_cache);

//        String sColsThisTab = UtilSets.htKeysToStr (htGetFields_StrTabName_cache);
//        api.Log.Log (" table [" + sTableName + "] has cols [" + sColsThisTab + "]");

        return htGetFields_StrTabName_cache;
    }

    public static String getTableIntersect (String sTable1 , String sTable2 , Connection dbc) throws Exception
    {
        HashSet hs1 = UtilSets.convertHTKeysToHashSet (getFields (sTable1 , dbc));
        HashSet hs2 = UtilSets.convertHTKeysToHashSet (getFields (sTable2 , dbc));
        hs2.retainAll (hs1);
        return UtilSets.hsToStr (hs1 , "," , false);
    }

    // get all "oracle text" indexes
    private static Hashtable htIndexes_StrToIndex_cache = null;

    public static synchronized void getIndexes_cacheInvalidate ()
    {
        htIndexes_StrToIndex_cache = null;
    }

    public static synchronized Hashtable getIndexes (Connection dbc) throws Exception
    {
        if (htIndexes_StrToIndex_cache == null)
        {
            Hashtable ht = new Hashtable ();

            String sSQL = "SELECT I.INDEX_NAME, I.TABLE_NAME, C.COLUMN_NAME " +
                    " FROM   ALL_IND_COLUMNS C , ALL_INDEXES I" +
                    " WHERE  C.INDEX_NAME = I.INDEX_NAME" +
                    " AND    C.TABLE_NAME = I.TABLE_NAME" +
                    " AND    I.INDEX_TYPE = 'DOMAIN'" +
                    " AND NOT EXISTS ( SELECT 'X' FROM ALL_CONSTRAINTS WHERE CONSTRAINT_NAME = I.INDEX_NAME) " +
                    " ORDER BY INDEX_NAME , COLUMN_POSITION";
            Statement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = dbc.createStatement ();
                rs = stmt.executeQuery (sSQL);

                while (rs.next ())
                {
                    Index i = new Index (rs.getString (1) , rs.getString (2) , rs.getString (3));
                    ht.put (i.getName () , i);
                }
            } catch ( Exception e )
            {
                throw e;
            } finally
            {
                if (rs != null)
                {
                    rs.close ();
                    rs = null;
                }
                if (stmt != null)
                {
                    stmt.close ();
                    stmt = null;
                }
            }
            htIndexes_StrToIndex_cache = ht;
        }
        return htIndexes_StrToIndex_cache;
    }

    // true if this hashtable of Index objects contains this table field
    public static boolean isTableFieldIndexed (Hashtable htIndex , String sTableDotField)
    {
        Vector vTabAndCol = UtilStrings.splitByStrLen1 (sTableDotField , ".");
        return ( CustomFields.isTableFieldIndexed (htIndex ,
                (String) vTabAndCol.elementAt (0) ,
                (String) vTabAndCol.elementAt (1)) );
    }

    public static boolean isTableFieldIndexed (Hashtable htIndex , String Table , String Field)
    {
        Enumeration enumIndexElements = htIndex.elements ();
        while (enumIndexElements.hasMoreElements ())
            if (( (Index) enumIndexElements.nextElement () ).isThisTableField (Table , Field))
                return true;
        return false;
    }

}
