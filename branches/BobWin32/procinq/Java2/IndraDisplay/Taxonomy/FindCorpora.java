package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;
import org.dom4j.Element;

public class FindCorpora
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception {

        ITS its = new ITS((String) props.get("SKEY", true));
        String query = (String) props.get("Keywords", true);

        Vector vNodes = its.searchCorpora(query);

		HTMLDocument Document = new HTML.HTMLDocument();
		Document.AddVariable("Title", "Taxonomy Matches");
        Document.AddVariable("Query", query);
		Document.WriteTemplate(out, "header-admin.tpl");
        Document.WriteTemplate(out, "search/head-full.tpl");

        // determine number of results and write into variables
        int iMaxRes = vNodes.size();
        int iNumRes = iMaxRes;
        Document.AddVariable("NUMRES", iNumRes+"");
        Document.AddVariable("MAXRES", ""+iMaxRes);
        Document.WriteTemplate(out, "search/category-head.tpl");
        Document.WriteTemplate(out, "search/category-start.tpl");

        int loop = 0;

        for (loop = 0; loop < vNodes.size(); loop++) {
            Object o = (Object) vNodes.elementAt(loop);

            if (!(o instanceof Node)) { loop = vNodes.size(); }
            else {
                String sNodeTree = Search.BuildNodeTree
								(out, (String) ((Node) o).get("LINKNODEID"), props);

                Document.AddVariable("NODETREE", sNodeTree);
                Document.SetHash((Node) o);
                Document.WriteTemplate(out, "search/advanced-corpus-result.tpl");
            }
        }

        if (loop == 0) Document.WriteTemplate(out, "search/no-category-result.tpl");

        Document.WriteTemplate(out, "search/category-foot.tpl");

        Document.WriteTemplate(out, "search/corpus-head.tpl");
        Document.WriteTemplate(out, "search/category-start.tpl");
        for (loop = 0; loop < vNodes.size(); loop++) {
            Object o = (Object) vNodes.elementAt(loop);

            if (!(o instanceof Node)) {
                Iterator i = (Iterator) o;

                while (i.hasNext()) {
                    Element e = (Element) i.next();
                    Document.AddVariable(e.getName(), e.getText());
                }

                Document.WriteTemplate(out, "search/advanced-corpus-result2.tpl");
            }
        }

        if (loop == 0) Document.WriteTemplate(out, "search/no-category-result.tpl");
        Document.WriteTemplate(out, "search/category-foot.tpl");

    }
}