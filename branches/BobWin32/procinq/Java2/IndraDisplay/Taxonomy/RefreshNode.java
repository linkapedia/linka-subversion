package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class RefreshNode
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		try {
			// Call: GetCorpusProps
			HashTree htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			InvokeAPI API = new InvokeAPI ("tsclassify.TSAddRefreshNode", htArguments);
			HashTree htResults = API.Execute(false, false);

            String sSuccess = "Your node refresh directive has been scheduled and confirmed.  This node will "+
                              "be refreshed during the next node refresh cycle.<p>";

			// A success is indicated with a simple SUCCESS tag
			if (!htResults.containsKey("SUCCESS")) {
                sSuccess = "Your node refresh directive could not be scheduled.  Please check the ITSAPI "+
                           "log files for more details.<p>";
			}

            sSuccess = sSuccess + "Click <a href='/servlet/Main?template=Taxonomy.AdminBrowse&NodeID="+sNodeID+
                                  "'>here to return</a> to the node administration screen.";

			HTMLDocument Document = new HTMLDocument();
			Document.WriteSuccess(out, sSuccess);

		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
            String sSuccess = "Your node refresh directive could not be scheduled.  Please check the ITSAPI "+
                              "log files for more details.<p>";
            sSuccess = sSuccess + "Click <a href='/servlet/Main?template=Taxonomy.AdminBrowse&NodeID="+sNodeID+
                              "'>here to return</a> to the node administration screen.";
            out.print("<!-- "); e.printStackTrace(out); out.println(" -->");
 			Document.WriteSuccess(out, sSuccess);
			Log.LogError(e, out);
		}
	}
}
