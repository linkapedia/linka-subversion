package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class CreateRepository
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sType = (String) props.get ("Type");
		String sName = (String) props.get ("Name");
		String sLoc = (String) props.get ("Location");
		String sUsername = (String) props.get ("Username");
		String sPassword = (String) props.get ("Password");
		String sFullText = (String) props.get ("Full");
		
		if ((sType == null) && (sName == null) && (sLoc == null) &&
			(sUsername == null) && (sPassword == null) && (sFullText == null)) {
		
			try {
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Create New Repository");
				Document.WriteTemplate(out, "header-nosearch.tpl");
			
				// build option type list by getting list of types
				HashTree htArguments = new HashTree(props);
				InvokeAPI API = new InvokeAPI ("tsrepository.TSGetRepositoryTypeList", htArguments);
				HashTree htResults = API.Execute(false, false);
					
				if (htResults.containsKey("SESSIONEXPIRED")) {		
					Server.Login.handleTSapiRequest(props, out, req, res);
					return;
				}
				if (!htResults.containsKey("TYPES")) { 
					throw new Exception ("No repository types defined in the database.");
				}
				HashTree htTypes = (HashTree) htResults.get("TYPES");
				Enumeration eTR = htTypes.elements();
				String sOptionTypes = "";
					
				while (eTR.hasMoreElements()) {
					HashTree htT = (HashTree) eTR.nextElement();
					String sKey = (String) htT.get("REPOSITORYTYPENAME");
					String sValue = (String) htT.get("REPOSITORYTYPEID");
						
					sOptionTypes = sOptionTypes+" <option selected value="+sValue+"> "+sKey; 
				}
					
				Document.AddVariable("FULL", "<option select value=1> On<option value=0> Off\n");
				Document.AddVariable("TYPE", sOptionTypes);
				Document.AddVariable("TEMPLATE", "Taxonomy.CreateRepository");
				Document.WriteTemplate(out, "repository/edit.tpl"); 
				Document.WriteTemplate(out, "repository/edit-foot.tpl");
				Document.WriteFooter(out);
			} catch (Exception e) {
				String sErrorMsg = e.getMessage();
				Log.LogError(e, out);
				return;
			}
		} else { // Edit this repository now
			// Call: GetRepositoryProps
			HashTree htArguments = new HashTree(props);
			if (sType != null) { htArguments.put("Type", sType); }
			if (sName != null) { htArguments.put("Name", sName); }
			if (sLoc != null) { htArguments.put("Location", sLoc); }
			if (sUsername != null) { htArguments.put("Username", sUsername); }
			if (sPassword != null) { htArguments.put("Password", sPassword); }
			if (sFullText != null) { htArguments.put("Full", sFullText); }
			
			InvokeAPI API = new InvokeAPI ("tsrepository.TSCreateRepository", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			boolean bSuccess = true;
			if (!htResults.containsKey("SUCCESS")) { 
				HTMLDocument Document = new HTMLDocument();
				String sFail = "Your repository ("+sName+") could not be created: Permission denied.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.Repository'>"+
							  "Click here to return to the repository menu</a>.";
				Document.WriteSuccess(out, sFail);	
			}
			String sID = (String) htResults.get("SUCCESS");
			
			// Foreach: Associated folder, insert relationship into the database
			Hashtable htProps = new Hashtable();
			Enumeration eProps = props.keys();
			while (eProps.hasMoreElements()) {
				String sKey = (String) eProps.nextElement();
				if (sKey.length() > 3) {
					if (sKey.substring(0,4).equals("path")) { 
						// extract the paths
						String sIdentifier = sKey.substring(4,sKey.length());
						String sPath = (String) props.get(sKey);
						String sRecurse = (String) props.get("Recurse"+sIdentifier);
							if (!Taxonomy.EditRepository.AddFolder(out, sPath, sRecurse, sID, props)) { bSuccess = false; }
					}
				}
			}
			
			// Now that modified folders are added, add any new ones that were specified
			String sNewPath1 = (String) props.get("NewPath1");
			String sNewPath2 = (String) props.get("NewPath2");
			String sNewPath3 = (String) props.get("NewPath3");
			String sRecurse1 = (String) props.get("NewRecurse1");
			String sRecurse2 = (String) props.get("NewRecurse2");
			String sRecurse3 = (String) props.get("NewRecurse3");

			Taxonomy.EditRepository.AddFolder(out, sNewPath1, sRecurse1, sID, props);
			Taxonomy.EditRepository.AddFolder(out, sNewPath2, sRecurse2, sID, props);
			Taxonomy.EditRepository.AddFolder(out, sNewPath3, sRecurse3, sID, props);			

			if (!htResults.containsKey("SUCCESS")) {
				HTMLDocument Document = new HTMLDocument();
				String sFail = "Your repository ("+sName+") could not be created: Permission denied.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Repository'>"+
								  "Click here to return to the repository menu</a>.";
				Document.WriteSuccess(out, sFail);
			} else {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "Your repository ("+sName+") has been created successfully.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Repository'>"+
								  "Click here to return to the repository menu</a>.";
				Document.WriteSuccess(out, sSuccess);
			}
		}
	}
}
