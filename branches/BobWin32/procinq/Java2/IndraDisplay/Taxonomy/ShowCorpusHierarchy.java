package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import Server.*;
import HTML.*;
import Logging.*;
import com.iw.system.*;

public class ShowCorpusHierarchy
{
	// Given a Corpus (or possibly a node), display a table of contents, displaying
	//  the hierarchy along the way..
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			
			String submit = (String) props.get("submit");
			
			if (submit == null) {
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Taxonomy Table of Contents");
				Document.AddVariable("OPTION_CORPUS_LIST", req);
				Document.WriteHeader(out);
				
				Document.WriteTemplate(out, "showcorpushi-front.tpl");
				
				Document.WriteFooter(out);
				
			} else {
				String sCorpusID = (String) props.get ("CorpusID", true);	
				String sNodeID = (String) props.get ("NodeID");	
				String sMaxDepth = (String) props.get ("Depth");

                ITS its = new ITS((String) props.get("SKEY"));
                Vector vNodes = its.getCorpusNodeContents(sCorpusID, sNodeID, sMaxDepth);

				if (vNodes.size() < 1) {
					throw new Exception("Sorry, nothing was found for the corpus or topic specified.");
				}

				// Determine the user's access level to this corpus (node)
				HashTree htArgPerm = new HashTree(props);
				htArgPerm.put("CorpusID", sCorpusID);
				InvokeAPI API = new InvokeAPI ("tscorpus.TSListCorpusAccess", htArgPerm);
				HashTree htResults = API.Execute(false, false);

				boolean bAdminAccess = true;
				if (htResults.containsKey("NOTAUTHORIZED")) { bAdminAccess = false; }

				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Taxonomy Table of Contents");
				Document.AddVariable("OPTION_CORPUS_LIST", req);
				Document.WriteTemplate(out, "header-nosearch.tpl");
				
				// Build the NodeTree
				out.println("<blockquote>");

				int loop = 0;
                for (int i = 0; i < vNodes.size(); i++) {
					loop++;
					if (loop == 1) {
						out.println(Taxonomy.DisplayResults.BuildNodeTree(out,sNodeID, props)+"<p>");
						out.println("<b>"); 
					}
					Node n = (Node) vNodes.elementAt(i);
                    Document.SetHash(n);
					String sSpaces = "";

					for (int j = 0; j < new Integer(n.get("DEPTHFROMROOT")).intValue(); j++) {
						sSpaces = sSpaces + "&nbsp; &nbsp; ";
					}
					Document.AddVariable("SPACING", sSpaces);
					if (bAdminAccess) { Document.WriteTemplate(out, "showcorpushi-admin.tpl"); }
					else { Document.WriteTemplate(out, "showcorpushi.tpl"); }
					if (loop == 1) { out.println("</b>"); }
				}

				out.println("</blockquote>");
				Document.WriteFooter(out);
			}
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
