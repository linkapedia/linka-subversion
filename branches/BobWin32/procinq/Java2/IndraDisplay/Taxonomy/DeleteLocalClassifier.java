package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class DeleteLocalClassifier
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sID = (String) props.get ("RID");
        String sStar = (String) props.get ("star");

		// Call: GetRepositoryProps
		HashTree htArguments = new HashTree(props);
		htArguments.put("rocsettingid", sID);
        htArguments.put("starcount", sStar);

		InvokeAPI API = new InvokeAPI ("tsrocsettings.TSRemoveROCBucket", htArguments);
		HashTree htResults = API.Execute(false, false);

		if (htResults.containsKey("SESSIONEXPIRED")) {
			Server.Login.handleTSapiRequest(props, out, req, res);
			return;
		}

		if (!htResults.containsKey("SUCCESS")) {
			HTMLDocument Document = new HTMLDocument();
			String sFail = "That local classifier could not be deleted: Permission denied<P>"+
                              "<script>window.opener.location = window.opener.location.href;</script>"+
							  "<a href='javascript:window.close()'>"+
							  "Click here to return to the ROC settings menu</a>.";
            Document.WriteSuccess(out, sFail);
		} else {
			HTMLDocument Document = new HTMLDocument();
            String sSuccess = "That local classifier was successfully removed.<P>"+
                              "<script>window.opener.location = window.opener.location.href;</script>"+
							  "<a href='javascript:window.close()'>"+
							  "Click here to return to the ROC settings menu</a>.";
			Document.WriteSuccess(out, sSuccess);
		}
	}
}
