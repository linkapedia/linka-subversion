package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

import Server.*;
import HTML.*;
import Logging.*;
import com.iw.system.*;

public class GetWordProps
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sThesaurusID = (String) props.get ("ThesaurusID", true);	
		String sWordID = (String) props.get ("WordID", true);	
		String submit = (String) props.get ("Submit");
		String action = (String) props.get ("Action");

		// If action is defined, a delete was requested
		if (action != null) {
			String sObjectID = (String) props.get ("ObjectID", true);
			String sRelationship = (String) props.get ("Rel", true);

			if (sRelationship.equals("0")) {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "Sorry, you did not specify a relationship type.<P>"+
								  "<a href='javascript:window.close()'>"+
								  "Click here to close this window.</a>.";
				sSuccess = sSuccess + "<SCRIPT> opener.location.reload(true); </SCRIPT>";

				Document.WriteSuccess(out, sSuccess);
				return;
			}
			
			HashTree htArguments = new HashTree(props);
			htArguments.put("ThesaurusID", sThesaurusID);
			htArguments.put("WordID1", sWordID);
			htArguments.put("WordID2", sObjectID);
			htArguments.put("Relationship", sRelationship);
			InvokeAPI API = new InvokeAPI ("tsthesaurus.TSRemoveWordRelationship", htArguments);
			HashTree htResults = API.Execute(false, false);
				
			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}
		}
		
		// If results were submitted, process them here
		if (submit != null) {
			String sAnchorWord = (String) props.get("AnchorWord");
			String sWordName = (String) props.get("WordName");
			String sObject = (String) props.get("object");
			if (sAnchorWord == null) { sAnchorWord = ""; }
			
			// Step 1: Check to see if the user changed the word name.  If so, it needs to be
			//   updated in the database
			if (!sAnchorWord.equals(sWordName)) {
				HashTree htArguments = new HashTree(props);
				htArguments.put("ThesaurusID", sThesaurusID);

				// URL encode word names to catch ampersands
				htArguments.put("OldName", URLEncoder.encode(sWordName, "UTF-8"));
				htArguments.put("NewName", URLEncoder.encode(sAnchorWord, "UTF-8"));
				
				InvokeAPI API = new InvokeAPI ("tsthesaurus.TSEditWordName", htArguments);
				HashTree htResults = API.Execute(false, false);
				
				if (htResults.containsKey("SESSIONEXPIRED")) {		
					Server.Login.handleTSapiRequest(props, out, req, res);
					return;
				}
			}
			
			// Step 2: Update all relationships upon submittal (no way of knowing what changed)
			Enumeration e = props.keys();
			while (e.hasMoreElements()) { 
				String s = (String) e.nextElement();
				if ((!s.equals("id")) && (!s.equals("anchorword")) && (!s.equals("object")) &&
					(!s.equals("wordname")) && (!s.equals("submit")) && (!s.equals("skey")) &&
					(!s.equals("relationship")) && (!s.equals("thesaurusid")) &&
					(!s.equals("template")) && (!s.equals("whichdb")) && (!s.equals("wordid"))) {
					
					String sRelationship = (String) props.get(s);	
					
					if (!sRelationship.equals("0")) {
						HashTree htArguments = new HashTree(props);
						htArguments.put("ThesaurusID", sThesaurusID);
						htArguments.put("Word1", URLEncoder.encode(sAnchorWord, "UTF-8"));
						htArguments.put("Word2", URLEncoder.encode(s, "UTF-8"));
						htArguments.put("Relationship", sRelationship);
						InvokeAPI API = new InvokeAPI ("tsthesaurus.TSAddWordRelationship", htArguments);
						HashTree htResults = API.Execute(false, false);
			
						if (htResults.containsKey("SESSIONEXPIRED")) {		
							Server.Login.handleTSapiRequest(props, out, req, res);
							return;
						}
					}
				}			
			}
			
			// Step 3: Add new relationship if one was specified
			if (!sObject.equals("")) {
				String sRelationship = (String) props.get("relationship");
		
				if (sRelationship.equals("0")) {
					HTMLDocument Document = new HTMLDocument();
					String sSuccess = "Sorry, you did not specify a relationship type.<P>"+
									  "<a href='javascript:window.close()'>"+
									  "Click here to close this window.</a>.";

					sSuccess = sSuccess + "<SCRIPT> opener.location.reload(true); </SCRIPT>";
					Document.WriteSuccess(out, sSuccess);
					return;
				}

				HashTree htArguments = new HashTree(props);
				htArguments.put("ThesaurusID", sThesaurusID);
				htArguments.put("Word1", URLEncoder.encode(sAnchorWord, "UTF-8"));
				htArguments.put("Word2", URLEncoder.encode(sObject, "UTF-8"));
				htArguments.put("Relationship", sRelationship);
				InvokeAPI API = new InvokeAPI ("tsthesaurus.TSAddWordRelationship", htArguments);
				HashTree htResults = API.Execute(false, false);
	
				if (htResults.containsKey("SESSIONEXPIRED")) {		
					Server.Login.handleTSapiRequest(props, out, req, res);
					return;
				}
				
			}
			
			// Step 4: Print success page
			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "This thesaurus term has been updated successfully.<P>"+
							  "<a href='javascript:window.close()'>"+
							  "Click here to close this window.</a>.";

			sSuccess = sSuccess + "<SCRIPT> opener.location.reload(true); </SCRIPT>";
			Document.WriteSuccess(out, sSuccess);
			return;
			
		}
		
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Thesaurus Word Editor");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.AddVariable("WordID", sWordID);
			Document.AddVariable("ThesaurusID", sThesaurusID);

			if (sWordID.equals("0")) {
	  	        Document.AddVariable("ANCHORWORD", "");
				Document.WriteTemplate(out, "thesaurus/browse-word-head.tpl");
				Document.WriteTemplate(out, "thesaurus/browse-word-foot.tpl");
				return; 			
			}
			
			Document.AddVariable("Header", "<tr><td><b>Indexing Term</b></td> <td><b>Relationship</b></td></tr>");

			// Get the Word properties
			HashTree htArguments = new HashTree(props);
			htArguments.put("ThesaurusID", sThesaurusID);
			htArguments.put("WordID", sWordID);
			InvokeAPI API = new InvokeAPI ("tsthesaurus.TSGetWordProps", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}
			
			if (action != null) {
				if (action.equals("Delete")) {
					String sSuccess = "The thesaurus term has been deleted successfully.<P>"+
									  "<a href='javascript:window.close()'>"+
									  "Click here to close this window.</a>.";	
	
					sSuccess = sSuccess + "<SCRIPT> opener.location.reload(true); </SCRIPT>";
					Document.WriteSuccess(out, sSuccess);
					return;
				}
			}
			if (!htResults.containsKey("WORDS")) {
				String sSuccess = "The term that you have selected is the object of a "+
								  "relationship but<BR> has no relationships, itself.<P>"+
								  "<a href='javascript:window.close()'>"+
								  "Click here to close this window.</a>.";

				sSuccess = sSuccess + "<SCRIPT> opener.location.reload(true); </SCRIPT>";
				Document.WriteSuccess(out, sSuccess);
				return;
			} else {
				HashTree htWords = (HashTree) htResults.get("WORDS");	
				Enumeration e = htWords.elements(); 
				int loop = 0;
				
				while (e.hasMoreElements()) {
					loop = loop + 1;
					HashTree htWord = (HashTree) e.nextElement();
					Document.SetHash(htWord);
					String sRelationshipUnique = (String) htWord.get("OBJECTNAME");
					if (loop == 1) { Document.WriteTemplate(out, "thesaurus/browse-word-head.tpl"); }
					if (htWord.get("RELATIONSHIPCODE").equals("1")) { 
						Document.AddVariable("Relationship", "<select name='"+sRelationshipUnique+"'>\n"+
															 "<option selected value=1> Synonym\n"+
															 "<option value=2> Broader Term\n"+
															 "<option value=3> Narrower Term\n"+
															 "</select>");
					} else if (htWord.get("RELATIONSHIPCODE").equals("2")) {
						Document.AddVariable("Relationship", "<select name='"+sRelationshipUnique+"'>\n"+
															 "<option value=1> Synonym\n"+
															 "<option selected value=2> Broader Term\n"+
															 "<option value=3> Narrower Term\n"+
															 "</select>");
					} else if (htWord.get("RELATIONSHIPCODE").equals("3")) {
						Document.AddVariable("Relationship", "<select name='"+sRelationshipUnique+"'>\n"+
															 "<option value=1> Synonym\n"+
															 "<option value=2> Broader Term\n"+
															 "<option selected value=3> Narrower Term\n"+
															 "</select>");
					}
					Document.WriteTemplate(out, "thesaurus/browse-word.tpl");
				}
			}
			Document.WriteTemplate(out, "thesaurus/browse-word-foot.tpl");
		}
		catch (Exception e) {
			String sErrorMsg = e.getMessage();
			out.println("<BR>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
