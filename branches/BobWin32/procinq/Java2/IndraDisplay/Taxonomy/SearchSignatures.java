package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Logging.*;
import com.iw.system.*;
import Server.ITS;


public class SearchSignatures
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
        String sWord = (String) props.get ("Signature");
        String sFreq = (String) props.get ("Frequency");
        String sGenre = (String) props.get ("GenreID");
        HTMLDocument Document = new HTMLDocument();

        if (sWord == null) {
			Document.AddVariable("Title", "Search by Signature");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-admin.tpl");

            // Call: ListFolders
            HashTree htArguments = new HashTree(props);
            InvokeAPI API = new InvokeAPI ("tsgenre.TSListFolders", htArguments);
            HashTree htResults = API.Execute(false, false);

            String sGenreSelectList = "<option value=0> All Folders ";

            if (htResults.containsKey("GENRES")) {
                HashTree htGenres = (HashTree) htResults.get("GENRES");
                Enumeration eG = htGenres.elements();

                while (eG.hasMoreElements()) {
                    HashTree htGenre = (HashTree) eG.nextElement();
                    String sGenreID = (String) htGenre.get("GENREID");
                    String sGenreName = (String) htGenre.get("GENRENAME");
                    String sGenreStatus = (String) htGenre.get("GENRESTATUS");

                    if (sGenreStatus.equals("1")) {
                        sGenreSelectList = sGenreSelectList +
                            "<option value="+sGenreID+"> "+sGenreName;
                    }
                }
            }

            Document.AddVariable("GENRESELECTLIST", sGenreSelectList);
			Document.WriteTemplate(out, "search/bysignature.tpl");
			Document.WriteFooter(out);
        } else {
            try {
                Document.AddVariable("SIGNATURE", sWord);

                ITS its = new ITS((String) props.get("SKEY"));
                Vector vDocuments = its.getDocumentsBySignature(sWord, sFreq, sGenre);

                String numResults = ""+vDocuments.size();
                if (numResults.equals("0")) {
                    Document.WriteSuccess(out, "No document results were found matching the signature: "+sWord);
                } else {
                    Document.AddVariable("NUMRESULTS", numResults);

                    Document.WriteTemplate(out, "header-admin.tpl");
                    Document.WriteTemplate(out, "search/signature-head.tpl");

                    int loop = 0;
                    for (int i = 0; i < vDocuments.size(); i++) {
                        loop++;
                        Document d = (Document) vDocuments.elementAt(i);
                        Document.SetHash(d);
                        Document.AddVariable("EDOCTITLE", d.getEncodedField("DOCTITLE"));
                        Document.AddVariable("EDOCURL", d.getEncodedField("DOCURL"));

                        Document.AddVariable("LOOP", loop+"");

                        Document.WriteTemplate(out, "search/signature-result.tpl");
                    }
                    Document.WriteTemplate(out, "search/fulltext-foot.tpl");
                    Document.WriteFooter(out);
                }
            } catch (Exception e) {
                String sErrorMsg = e.getMessage();
                Document.WriteError(out, "<BLOCKQUOTE>Sorry, your request could not be processed at this time.</BLOCKQUOTE>");
                Log.LogError(e, out);
            }
        }
	}
}
