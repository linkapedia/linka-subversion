package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;
import com.indraweb.execution.Session;

public class AddAlert
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sCQL = (String) props.get ("CQL", true);
        String sName = (String) props.get ("name");
        String sRunFreq = (String) props.get ("runfreq");
        String sNotes = (String) props.get ("notes");

        // Read EMAIL, DISTINGUISHED_NAME from cookie
        Cookie Cookies[] = req.getCookies();
		String sEmail = "null"; String sUserDN = "null";

        if (req.getCookies() != null) {
            int cLen = Cookies.length;

            // look through and read from cookies
            for (int i=0; i < cLen; i++) {
                if (Cookies[i].getName().equals("ID")) { sUserDN = (String) Cookies[i].getValue(); }
                if (Cookies[i].getName().equals("EMAIL")) { sEmail = (String) Cookies[i].getValue(); }
             }
        }

        if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none"))
            sEmail = "info@intellisophic.com";

        if (sEmail.equals("null")) {
            HTMLDocument Document = new HTMLDocument();
			Document.WriteSuccess(out, "A valid e-mail address is required to save alerts.  See your system administrator for more details.");
            return;
        }

        HTMLDocument Document = new HTMLDocument();
		try {
            if (sRunFreq != null) {
                // Call: AddAlert
                HashTree htArguments = new HashTree(props);
                htArguments.put("Name", sName.replaceAll("\n", "<br>"));
                htArguments.put("UserID", sUserDN);
                htArguments.put("Email", sEmail);
                htArguments.put("Query", sCQL);
                htArguments.put("RunFreq", sRunFreq);
                htArguments.put("FromEmail", sEmail);
                htArguments.put("Notes", sNotes.replaceAll("\n", "<br>"));

                InvokeAPI API = new InvokeAPI ("tsnotification.TSAddAlert", htArguments);
                HashTree htResults = API.Execute(false, false);

                String sSuccess = "";

                if (!htResults.containsKey("SUCCESS")) {
                    sSuccess = "Your query could not be processed at this time.";
                    Log.LogError("Attempt to save query failed, information: "+sName+", "+sUserDN+", "+sEmail+", "+sCQL+", "+sRunFreq+", "+sNotes);
                } else { sSuccess = "Your query has been successfully added to your profile.."; }

                Document.WriteSuccess(out, sSuccess+" <P><a href='javascript:window.close()'>Click here to return</a>.");
            } else {
                Document.AddVariable("QUERY", URLEncoder.encode(sCQL, "UTF-8"));
                Document.WriteTemplate(out, "users/save-query.tpl");
            }
		}
		catch (Exception e) {
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
