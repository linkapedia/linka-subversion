package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.Math.*;
import java.lang.reflect.Method;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class TaxonomyOfTaxonomies
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		HTMLDocument Document = new HTML.HTMLDocument();
		Document.AddVariable("Title", "Available Taxonomies");
		Document.WriteTemplate(out, "taxonomy-of-taxonomies.tpl");
	}
}
