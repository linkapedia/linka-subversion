package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class GetNuggets
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID");	
		String sDocID = (String) props.get ("DocID");	
		String sDocURL = (String) props.get ("DocURL");
		String sDocTitle = (String) props.get ("DocTitle");
		
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("DOCTITLE", sDocTitle);
			Document.AddVariable("DOCURL", sDocURL);
			Document.AddVariable("Title", "Document Nuggets");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-admin.tpl");
			Document.WriteTemplate(out, "nuggets-head.tpl");
			
			HashTree htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			htArguments.put("DocumentID", sDocID);
			InvokeAPI API = new InvokeAPI ("tsnugget.TSGetSnippets", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			if (!htResults.containsKey("SNIPPETS")) {
				out.println("<blockquote><br>Sorry, this document has no nuggets available.<p>");
			} else {
				HashTree htSnippets = (HashTree) htResults.get("SNIPPETS");
				Enumeration eS = htSnippets.elements();
				int loopi = 0;
				
				while (eS.hasMoreElements()) {
					loopi++;
					HashTree htSnippet = (HashTree) eS.nextElement();
					Document.SetHash(htSnippet);
					Document.AddVariable("SNIPPET", (String) htSnippet.get("PHRASE"));
					Document.AddVariable("LOOPI", ""+loopi);
					
					HashTree htArgs2 = new HashTree(props);
					htArgs2.put("NuggetID", (String) htSnippet.get("NUGGETID"));
					InvokeAPI API2 = new InvokeAPI ("tsnugget.TSGetNugget", htArgs2);
					HashTree htResults2 = API2.Execute(false, false);
					
					if (!htResults2.containsKey("NUGGET")) {
						Document.AddVariable("NUGGET", "This snippet does not contain any nuggets.");
					} else {
						HashTree htNugget = (HashTree) htResults2.get("NUGGET");
						Document.SetHash(htNugget);
					}
					
					Document.WriteTemplate(out, "nugget.tpl");
				}
			}
			
			Document.WriteTemplate(out, "nuggets-foot.tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
