import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import HTML.HTMLDocument;
import Logging.APIProps;
import Logging.Log;

import com.indraweb.execution.ConfigProperties;
import com.indraweb.execution.Session;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;

/**
*	Main servlet class for IndraWeb Taxonomy Display
*	All rights reserved.
*	(c)IndraWeb.com,Inc. 2002
*
*	@author mpuscar
*
*	@param	req	Contains the name-value pairs:
*						template=functionname
*						parm1=parameterValue1
*						parm2=parameterValue2 ...
*
*	@param	res	response is written as HTML to the output stream of res

*	@return	html via output stream
*/

public class Main extends HttpServlet
{
	// This is a "convenience method" so, after overriding it there is no need to call the super()
	// This function takes the place of the initializeSession method in the engine project.
	public void init()
          throws ServletException
	{
		// Only run this initialization at startup time
		if ( Session.GetbInitedSession() ) return;

		// Load environment from JRUN config application variables first
		Enumeration eParams = getServletContext().getInitParameterNames();
		Hashtable htprops = new Hashtable();

		while (eParams.hasMoreElements()) {
			String sParamName = (String) eParams.nextElement();
			String sParamValu = (String) getServletContext().getInitParameter(sParamName);

			htprops.put(sParamName, sParamValu);
		}

		Session.sIndraHome = (String) getServletContext().getInitParameter("IndraHome");
		Session.cfg = new ConfigProperties(htprops);

		Session.SetbInitedSession(true);

		return;
	}

	public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!request.getContentType().substring(0,19).equals("multipart/form-data")) {
			doGet(request, response);
		} else {
			PrintWriter out	= response.getWriter();
			response.setContentType("text/html");
			MultipartParser mpr = new MultipartParser(request, 1000000000);

			Part p = null;
			APIProps apiprops = new APIProps();

			Cookie Cookies[] = request.getCookies();
			if (request.getCookies() != null) {
				int cLen = Cookies.length;

				for (int i=0; i < cLen; i++) {
					if ((Cookies[i].getName().equals("SKEY")) && (!apiprops.containsKey("SKEY"))) {
						apiprops.put("SKEY", (String) Cookies[i].getValue()); }
					if (Cookies[i].getName().equals("ID")) {
						String ID = (String) Cookies[i].getValue();
						ID = ID.replace('|',',');
						apiprops.put("ID", ID); }
				}
			}

            String sFilename = null;
			try {
				// get indrahome from jrun config
				String sIndraHome = getServletContext().getInitParameter("IndraHomeFolder");
				if ( sIndraHome == null )
					throw new Exception("IndraHomeFolder not specified in jrun config");
				// loop through each part that was sent
				while ((p = mpr.readNextPart()) != null) {
					String sParmName = p.getName();
					String sParmValue = new String("");
					if (p.isParam()) {
						ParamPart pp = (ParamPart) p;
						sParmValue = pp.getStringValue();
					}
					if (p.isFile()) {
						FilePart fp = (FilePart) p;

						// Write content type and file name into props
						apiprops.put ("Content-type", fp.getContentType());
						apiprops.put ("FilePath", fp.getFilePath());
						sParmValue = fp.getFileName();

						if (fp.getFileName() != null) {
							// Write file out to temp directory here
							sFilename = Session.cfg.getProp("ImportDir")+fp.getFileName();
							File fFile = new File(sFilename);

                            int fileloop = 0;
							while (fFile.exists()) {
								sFilename = sFilename+fileloop; fileloop++;
								fFile = new File(sFilename);
							}

                            Log.Log("Wrote file: "+sFilename+" from "+fp.getFilePath());

							InputStream is = fp.getInputStream();
							FileOutputStream fo = new FileOutputStream(fFile);
							int c;

							while ((c = is.read()) != -1) {
								fo.write(c);
							}

							// close streams
							is.close(); fo.close();

							sParmValue = fFile.getName();
						}
					}
					if (sParmValue != null) { apiprops.put ( sParmName, sParmValue ); }
				}

				Class cProperties = Class.forName ("Logging.APIProps");
				Class cHttpServletRequest = Class.forName ("javax.servlet.http.HttpServletRequest");
				Class cHttpServletResponse = Class.forName ("javax.servlet.http.HttpServletResponse");
				Class cPrintWriter = Class.forName ("java.io.PrintWriter");
				Class[] cArr = { cProperties, cPrintWriter, cHttpServletRequest, cHttpServletResponse };
				// create the (super)class against which to invoke the API method call
				Class cTSapi = null;

				// sFunction variable will equal the "template" input parameter
				String sFunction = (String) apiprops.get ("template");
				cTSapi = Class.forName ( sFunction);

				Method method = null;
				method = cTSapi.getMethod( "handleTSapiRequest", cArr );

				String sWhichDB = "API";
				apiprops.put ( "whichdb", "API" );

				Object[] oArr = { apiprops, out, request, response};
				method.invoke ( cTSapi, oArr );  // invoke any of the API functions
			}
			// If an error occurs attempting to invoke the class, or it does not exist, print the main page
			catch ( Exception e ) {
				out.println("<!--"); e.printStackTrace(out); out.println("-->");

				try {
					HTMLDocument Document = new HTMLDocument();
					Document.WriteError(out, e.toString());
				}
				catch (Exception e2) { Log.LogError(e2, out); }
			} finally { if (sFilename != null) {
                //File f = new File(sFilename);
                //if (f.exists()) { f.delete(); }
            }}
		}
	}

	public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
	throws ServletException, IOException
    {
		PrintWriter out	= res.getWriter();
		res.setContentType("text/html; charset=UTF-8");

		Logging.APIProps apiprops = new Logging.APIProps( );

		try {
			// PARMS
			Enumeration e4 = req.getParameterNames();
			while ( e4.hasMoreElements() )	{
				String sParmName = (String) e4.nextElement();
				String sParmValue = req.getParameter ( sParmName );
				apiprops.put ( sParmName, sParmValue );
			}

			String sIndraHome = getServletContext().getInitParameter("IndraHomeFolder");
			if ( sIndraHome == null )
				throw new Exception("IndraHomeFolder not specified in jrun config");
			//api.statics.ConfigProperties.initConfigs(sIndraHome);

			// Before invoking the requested class, read cookie for class list.
			//  If class list exists, load it into variable, otherwise load it from
			//  the database.
			Cookie Cookies[] = req.getCookies();

			if (req.getCookies() != null) {
				int cLen = Cookies.length;

				for (int i=0; i < cLen; i++) {
					if ((Cookies[i].getName().equals("SKEY")) && (!apiprops.containsKey("SKEY"))) {
						String SKEY = (String) Cookies[i].getValue();
						if (SKEY != null) {	apiprops.put("SKEY", SKEY); }
						else {
							Log.LogError("Error: Read a NULL session key from the cookie file.");
							apiprops.put("template", "Server.Login");
						}
					}
					if (Cookies[i].getName().equals("ID")) {
						String ID = (String) Cookies[i].getValue();
						if (ID != null) {
							ID = ID.replace('|',',');
							apiprops.put("ID", ID);
						}
					}
				}
			}

			Class cProperties = Class.forName ("Logging.APIProps");
			Class cHttpServletRequest = Class.forName ("javax.servlet.http.HttpServletRequest");
			Class cHttpServletResponse = Class.forName ("javax.servlet.http.HttpServletResponse");
			Class cPrintWriter = Class.forName ("java.io.PrintWriter");

			Class[] cArr = { cProperties, cPrintWriter, cHttpServletRequest, cHttpServletResponse };
			// create the (super)class against which to invoke the API method call
			Class cTSapi = null;

			// sFunction variable will equal the "template" input parameter
			String sFunction = (String) apiprops.get ("template");
			if (sFunction != null) {
				try {
					cTSapi = Class.forName ( sFunction);

                    String sWhichDB = "API";
                    apiprops.put ( "whichdb", "API" );

                    // ************************************
                    /* DEBUG: when can't find the class uncomment this
                    out.println("Debug: method/parm list for class: "+cTSapi.toString());
                    Method[] methodz = cTSapi.getMethods();
                    out.println("<br>Debug: Methodz size: "+methodz.length);
                    for (int i = 0; i < methodz.length; i++) {
                        Method m = (Method) methodz[i];
                        out.println("<br><br>Method "+i+": "+m.getName());
                        Class[] classz = m.getParameterTypes();
                        for (int j = 0; j < classz.length; j++) {
                            Class c = (Class) classz[j];
                            out.println("<br>Param Type: "+c.getName());
                        }
                    }
                    */


                // ************************************
                    // */
                    Method method = cTSapi.getMethod( "handleTSapiRequest", cArr );

					Object[] oArr = { apiprops, out, req, res};
                    // ************************************
                    /* DEBUG: when can't find the class uncomment this
                    out.println("<br><br>Debug: Method/parm list being sought by Main.java: ");
                    out.println("<br>Param Type: "+apiprops.getClass().getName());
                    out.println("<br>Param Type: "+out.getClass().getName());
                    out.println("<br>Param Type: "+req.getClass().getName());
                    out.println("<br>Param Type: "+res.getClass().getName());
                    */

					method.invoke ( cTSapi, oArr );  // invoke any of the API functions
				} catch ( Exception e ) {
					out.println("<br>Could not find function: "+sFunction);
					Log.LogError ("sFunction:"+ sFunction, e);
					throw e;
				}
			} else { Server.Login.handleTSapiRequest(apiprops, out, req, res); }
		}
		// If an error occurs attempting to invoke the class, or it does not exist, print the main page
		catch ( Exception e ) {
			Log.LogError(e, out);
			try { Taxonomy.FrontPage.handleTSapiRequest(apiprops, out, req, res); }
			catch (Exception e2) { Log.LogError(e2, out); }
		}
	} // public void doGet(HttpServletRequest req,

} // public class ts extends HttpServlet
