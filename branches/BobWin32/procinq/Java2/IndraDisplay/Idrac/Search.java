package Idrac;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.HashTree;
import com.iw.system.InvokeAPI;

public class Search
{
	public static String BuildQuery (String query, String sQueryType) {
		return BuildQuery (query, sQueryType, "LIKE");
	}

	public static String BuildNodeTree(PrintWriter out, String sNodeID, APIProps props) {
		Hashtable htArguments = new HashTree(props);
		htArguments.put("NodeID", sNodeID);
			
		// Call: GetNodeProps to retrieve Node data
		InvokeAPI API = new InvokeAPI ("tsnode.TSGetNodeTree", htArguments);
		Vector vResults = new Vector();
        try { vResults = API.vExecute(false, false); } catch (Exception e) { return ""; }

		// If this node doesn't exist, return an empty string
		if (vResults.size() < 1) { return ""; }
		Vector vNodes = (Vector) vResults.elementAt(0);
		
		String sTaxonomy = new String("");
		Enumeration eV = vNodes.elements();
		while (eV.hasMoreElements()) {
			Vector vNode = (Vector) eV.nextElement();
		
			if (vNodes.lastElement() != vNode) {
				String sNodeTitle = (String) vNode.elementAt(2);
		
//				if (sNodeTitle.length() > 25) { 
//					sNodeTitle = sNodeTitle.substring(0, 25)+"...";	
//				}
		
				// If this is the parent node, return now
				if (sTaxonomy.equals("")) {
					sTaxonomy = "<a href='/servlet/Main?template=Idrac.DisplayResults&NodeID="+
						    (String) vNode.elementAt(0)+"'> "+
						    sNodeTitle+"</a> ";
				} else {
					sTaxonomy = "<a href='/servlet/Main?template=Idrac.DisplayResults&NodeID="+
						    (String) vNode.elementAt(0)+"'> "+
						    sNodeTitle+"</a> > "+sTaxonomy;
				}
			}
		}
		return sTaxonomy;	
	}

	public static String BuildQuery (String query, String sQueryType, String sOperator) {
		String sQuery = "";
		
		char[] cArr = new char[query.length()+1];
		query.getChars(0, query.length(), cArr, 0);

		for (int i = 0; i < query.length(); i++) {
			if ((cArr[i] == '(') || (cArr[i] == ')')) { 
				sQuery = sQuery + " "+cArr[i]; }
			else {
				int j = i; // get temporary pointer
				Hashtable ht = GetNextWord(cArr, i, j, query);
				Enumeration eH = ht.keys();
				String sWord = (String) eH.nextElement();

				j = new Integer((String) ht.get(sWord)).intValue();
						
				if (!sWord.equals("")) {
					if ((sWord.toUpperCase().equals("AND")) || 
						(sWord.toUpperCase().equals("OR")) || 
						(sWord.toUpperCase().equals("NOT"))) { sQuery = sQuery + " "+sWord; }
					else {
						sQuery = sQuery + " "+sQueryType+" "+sOperator+" '"+sWord+"'";
						i=j+1;
						ht = GetNextWord(cArr, i, i, query);
						eH = ht.keys();
						String sNextWord = (String) eH.nextElement();
						
						if (!((sNextWord.toUpperCase().equals("AND")) || 
							  (sNextWord.toUpperCase().equals("OR")) || 
							  (sNextWord.toUpperCase().equals("NOT")))) { 
							if (cArr[j] == ' ') { sQuery = sQuery + " AND "; }
						}
					}
					i = j;
				}
			}
		} 
		return sQuery;
	}
	
	public static Hashtable GetNextWord (char[] cArr, int i, int j, String sQuery) {
		Hashtable ht = new Hashtable();
		String sWord = "";
		
		if (j >= cArr.length) { ht.put("", j+""); return ht; }
		
		if (cArr[j] == '"') {
			j++;
			while ((cArr[j] != '"') && (j < sQuery.length())) { j++; }
			sWord = new String().copyValueOf(cArr, i+1, ((j-i)-1));
		} else {
			while ((cArr[j] != ' ') && (cArr[j] != ')') && (j < sQuery.length())) { j++; }
			sWord = new String().copyValueOf(cArr, i, (j-i));
			if (cArr[j] == ')') { j=j-1; }
		}	

		ht.put(sWord, j+"");
		return ht;
	}
	
	// if document is greater than 300 characters, truncate at 
	// next available whitespace
	public static String TruncateDocumentSummary (String sDocSummary) {
		return TruncateDocumentSummary (sDocSummary, 300); }
	public static String TruncateDocumentSummary (String sDocSummary, int length) {
		if (sDocSummary == null) { sDocSummary = "(no abstract found)"; }
		while ((sDocSummary.length() > length) && (sDocSummary.charAt(length) != ' ')) { length++; }
		if (sDocSummary.length() > length) { 
			sDocSummary = sDocSummary.substring(0, length)+"...";
		}
	
		if (sDocSummary.equals("null")) { sDocSummary = "No abstract found."; }
		return sDocSummary;
	}
}