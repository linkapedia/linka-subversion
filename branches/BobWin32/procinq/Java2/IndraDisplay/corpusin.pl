#!/usr/bin/perl

## corpusin.pl
#
# Given a filename, input an XML version of the corpus 
#  using the Topic Maps XTM DTD
#
# Step 1: Get the filename to be processed from input
# Step 2: Check for a lock file.   If a lock file exists, exit immediately (nothing to do)
# Step 3: Create the lock file, just in case the user hits that submit button twice.
# Step 4: Process the corpus into the database, logging any warnings to file
# Step 5: Done! Remove the lock file, send out a success e-mail to the recipient.

use DBI;
use Getopt::Long;
use Net::SMTP;

# Signal switches catch interrupts
#
# If one of these occur, make sure to erase all inserts made into the database
$SIG{INT} = \&cleanup;
$SIG{QUIT} = \&cleanup;
$SIG{STOP} = \&cleanup;
$SIG{HUP} = \&cleanup;
$SIG{TERM} = \&cleanup;

local ($filename) = "corpusout.xml";
local ($email) = "mpuscar\@indraweb.com";
local ($clientid) = 0;

&GetOptions ("filename=s" => \$filename,
			 "email=s" => \$email);

open (FILE, "$filename") || &die_error("Filename: $filename not found.");
local (@lines) = <FILE>; close(FILE);

local ($dbh) = DBI->connect("dbi:Oracle:client", "sbooks", "racer9");
#$dbh->{AutoCommit} = 0;

local ($cid, $cname, $cdesc, $r1, $r2, $r3, $r4, $r5, $r6);
local ($nid, $ntitle, $nsize, $clid, $pid, $niwp, $depth);
local ($signatures, $sigword, $sigfreq, $corpus);
local ($savedcorpus) = -1;
local ($nextline) = 0;
local ($global_warning) = "";

$nid = -1; $corpus = -1;
$cid = -1; $cdesc = "";

# Loop through each line to extract the XML data
foreach $line (@lines) {
    #######################
    #
    # Corpus related regex
    #
    #######################

    if ($nextline == 1) {
        if ($line !~ /resourceData/) {
            $cdesc = $cdesc . $line;
        } else { $nextline = 0; }
    }
    # id="300012" xmln
    if ($line =~ /<topicMap id=\"(.*?)\" xmln/) { $cid = $1; $savedcorpus = $cid; }
    if (($cid != -1) && ($line =~ /String>(.*)<\/baseNameString/))
    {
        $cname = $1;
    }

    if ($line =~ /<resourceData id=\"ROCF1\">(.*)<\/resourceData>/) { $r1 = $1; }
    if ($line =~ /<resourceData id=\"ROCF2\">(.*)<\/resourceData>/) { $r2 = $1; }
    if ($line =~ /<resourceData id=\"ROCF3\">(.*)<\/resourceData>/) { $r3 = $1; }
    if ($line =~ /<resourceData id=\"ROCC1\">(.*)<\/resourceData>/) { $r4 = $1; }
    if ($line =~ /<resourceData id=\"ROCC2\">(.*)<\/resourceData>/) { $r5 = $1; }
    if ($line =~ /<resourceData id=\"ROCC3\">(.*)<\/resourceData>/) { $r6 = $1; }
    if ($line =~ /<resourceData id=\"CorpusDesc\">/) { $nextline = 1; }
    
    if (($cid != -1) && ($line =~ /<\/occurence>/)) { 
		# Insert corpus into the database
		$cdesc =~ s/\n//gi; $cdesc =~ s/\'//gi;
		# warn "Inserting Corpus ID: $cid Name: $cname R1: $r1 R2: $r2 R3: $r3 R4: $r4 R5: $r5 R6: $r6 DESC: $cdesc \n";
		my ($q)="insert into corpus (corpusid, clientid, corpus_name, rocf1, rocf2, rocf3, rocc1, rocc2, rocc3, corpusdesc, corpusactive, corpusrunfrequency, corpuslastrun) values ($cid, 0, '$cname', $r1, $r2, $r3, $r4, $r5, $r6, '$cdesc', 1, 90, SYSDATE)";
		my ($sth) = $dbh->prepare($q) || &die_error("Error: Cannot insert corpus, $q, $!");
		$sth->execute() || &die_error("Error: Cannot insert corpus, $q, $!");
		$sth->finish();
        $cid = -1;
    } 

    #######################
    #
    # Node and Signature related regex
    #
    #######################

    if ($line =~ /<topic id=(.*?)>/) { $nid = $1; $nid =~ s/\"//gi; }
    if (($nid != -1) && ($line =~ /String>(.*)<\/baseNameString/))
    {
        $ntitle = $1;
    }
    if ($line =~ /<topic id=(.*)>/) { $nid = $1; }
    if (($nid != -1) && ($line =~ /String>(.*)<\/baseNameString/)) { $ntitle = $1; }

    if ($line =~ /<instanceOf>(.*)<\/instanceOf>/) { $corpus = $1; }
    if ($line =~ /<resourceData id=\"ClientId\">(.*)<\/resourceData>/) { $clid = $1; }
    if ($line =~ /<resourceData id=\"NodeSize\">(.*)<\/resourceData>/) { $nsize = $1; }
    if ($line =~ /<resourceData id=\"ParentId\">(.*)<\/resourceData>/) { $pid = $1; }
    if ($line =~ /<resourceData id=\"NodeIndexWithinRoot\">(.*)<\/resourceData>/) { $niwp = $1; }
    if ($line =~ /<resourceData id=\"DepthFromRoot\">(.*)<\/resourceData>/) { $depth = $1; }
    if ($line =~ /<resourceData id=\"Signatures\">(.*)<\/resourceData>/) {
		$signatures = $1; 
        if ($signatures =~ /CDATA\[(.*)\]/) { $signatures = $1; }
        $signatures =~ s/\]//gi;
        		
		$ntitle =~ s/\'//gi;
		# Insert node into the database
	    # warn "Inserting Node ID: $nid Title: $ntitle Corpus: $corpus Client: $clid Size: $nsize Parent: $pid Index: $niwp Depth: $depth\n";
		my ($q)="insert into node (nodeid, corpusid, clientid, nodetitle, nodesize, parentid, nodeindexwithinparent, depthfromroot) values ($nid, $corpus, $clid, '$ntitle', $nsize, $pid, $niwp, $depth)";
		my ($sth) = $dbh->prepare($q) || &add_warning("Error: Cannot insert node, $q, $!");
	    $sth->execute() || &add_warning("Error: Cannot insert node, $q, $!");
	    $sth->finish();

		# Process the Signatures
		my (@sigs) = split(/\,/,$signatures);
		foreach $sig (@sigs) {
		   ($sigword, $sigfreq) = split(/\|\|/, $sig);
            $sigword =~ s/\'//gi;
            $sigfreq =~ s/\'//gi;		
			$sigword =~ s/\W//g;
		    $sigfreq =~ s/\W//g;

		   if (($sigword ne "") && ($sigfreq ne "")) {
			   #warn "Inserting Signature: Node: $nid Word: $sigword Freq: $sigfreq\n";
	 		    my ($q)="insert into signature (nodeid, signatureword, SIGNATUREOCCURENCES) values ($nid, '$sigword', $sigfreq)";
		        my ($sth) = $dbh->prepare($q) || &add_warning("Error: Cannot insert signature, $q, $!");
				$sth->execute() || &add_warning("Error: Cannot insert signature, $q, $!");
				$sth->finish(); 
		   }
		}
		$nid = -1;
	}
}

$dbh->disconnect();

open (FILE, ">$filename.log");
print FILE "Your corpus import succeeded. \n\n";
print FILE "Possible warnings along the way are listed below.\n$global_warning\n";
close(FILE);

unlink($filename);

$smtp = Net::SMTP->new('smtp.covad.net');
$smtp->mail('corpusinfo\@indraweb.com');
$smtp->to($email);

$smtp->data();
$smtp->datasend("To: $email\n");
$smtp->datasend("Subject: Corpus import ($filename)\n");
$smtp->datasend("\n");
$smtp->datasend("Your corpus import succeeded!   \n\n");
$smtp->datasend("Possible warnings along the way are listed below.\n$global_warning\n");
$smtp->datasend("\n");
$smtp->dataend();

exit(1);

sub cleanup {
    if ($savedcorpus != -1) {
		local ($sth) = $dbh->prepare("delete from corpus where corpusid = $savedcorpus");
		$sth->execute || die "Cannot cleanup signature information.\n";
		$sth->finish;
    }
    $dbh->disconnect();
    exit(1);
}

# DIE_ERROR
#
# If this function gets called, something catastrophic has happened.   Send an e-mail to the user
# who kicked this program off with more information about why, exactly, it failed.  Also, close out
# any open connections, such as the database connection.
#
# NOTE- authentication not implemented yet, I will log this to a file (for now)
sub die_error {
    my ($err_string) = $_[0];

	open (FILE, ">$filename.log");
    print FILE "Your corpus import has failed.   The error message is listed below.\n\n$err_string\n\n";
	print FILE "Possible warnings before the error occured are listed below.\n$global_warning\n";
	close(FILE);

	unlink($filename);

	#$smtp = Net::SMTP->new('smtp.netreach.net');
    $smtp = Net::SMTP->new('smtp.covad.net');
    $smtp->mail('corpusinfo\@indraweb.com');
    $smtp->to($email);

    $smtp->data();
    $smtp->datasend("To: $email\n");
    $smtp->datasend("Subject: Corpus import ($filename)\n");
    $smtp->datasend("\n");
    $smtp->datasend("Your corpus import has failed.   The error message is listed below.\n\n$err_string\n\n");
	$smtp->datasend("Possible warnings before the error occured are listed below.\n$global_warning\n");
    $smtp->datasend("\n");
    $smtp->dataend();

	$dbh->disconnect();
    die $err_string;
}

# ADD_WARNING
#
# Something bad happened, but not catastrophic.   Add the warning to our global warning file.   These
# warnings will be accumulated and sent to the user on success.   Also log this warning to our log file.
sub add_warning {
    my ($warning) = $_[0];

	$global_warning = $global_warning . $warning . "\n";

	open (FILE, ">$filename.log");
	print FILE "Possible warnings before the error occured are listed below.\n\n$global_warning\n";
	close(FILE);

	return 1;
}

1;
