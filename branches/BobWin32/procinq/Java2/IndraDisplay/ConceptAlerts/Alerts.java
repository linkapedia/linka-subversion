package ConceptAlerts;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

// Alerts:
//   1) Create a SELECT LIST widget with all documents with repositorytype 8.  Show doctitle and color code.
//   2) {Add} - pop up a dialog allowing the user to paste in text
//   3) {Edit} - pop up a dialog allowing the user to resubmit an existing document
//   4) {Remove} - pop up a warning dialog them remove the document from the system
//   5) {View} - go to a page, show document at the top followed by similar documents and ranking, with date filtering
//   6) {Click} - if the user clicks on the document, pop up a short page with the document

public class Alerts {
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception {

        ITS its = new ITS((String) props.get("SKEY"));

        String query = "SELECT <DOCUMENT> WHERE GENREID = 241 ORDER BY TROUBLESTATUS DESC";
        Vector vDocuments = its.CQL(query);

		HTMLDocument Document = new HTML.HTMLDocument();
		Document.AddVariable("Title", "Product Pipeline Intelligence");
		Document.WriteTemplate(out, "header-nosearch.tpl");

        // allow restriction by genre
        Vector vFolders = its.getFolders();
        String sGenres = "";

        if (vFolders.size() > 0) {
            for (int i = 0; i < vFolders.size(); i++) {
                Genre g = (Genre) vFolders.elementAt(i);
                sGenres = sGenres + "<option value=\""+g.getID()+"\"> "+g.getName()+"</option>\n";
            }
        }
        Document.AddVariable("GENRES", sGenres);

        // concept alert specific templates here
        Document.WriteTemplate(out, "concept/head.tpl");
        int loop = 0;

        for (int i = 0; i < vDocuments.size(); i++) {
            Document d = (Document) vDocuments.elementAt(i);
            Document.SetHash(d); loop++;

            // set an alternating background color
            if ((loop % 2) == 0) { Document.AddVariable("BACKGROUND", "#CCCCCC"); }
            else { Document.AddVariable("BACKGROUND", "#FFFFFF"); }

            // set the color code.  this is hardcoded until we build an infrastructure for this.
            if (d.get("TROUBLESTATUS").equals("4")) { Document.AddVariable("COLOR", "red"); }
            else if (d.get("TROUBLESTATUS").equals("3")) { Document.AddVariable("COLOR", "yellow"); }
            else if (d.get("TROUBLESTATUS").equals("2")) { Document.AddVariable("COLOR", "green"); }
            else { Document.AddVariable("COLOR", "spacer"); }

            Document.WriteTemplate(out, "concept/document.tpl");
        }

        Document.WriteTemplate(out, "concept/foot.tpl");

		Document.WriteFooter(out);
	}
}
