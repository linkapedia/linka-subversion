package ConceptAlerts;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

// Alerts:
//   1) Create a SELECT LIST widget with all documents with repositorytype 8.  Show doctitle and color code.
//   2) {Add} - pop up a dialog allowing the user to paste in text
//   3) {Edit} - pop up a dialog allowing the user to resubmit an existing document
//   4) {Remove} - pop up a warning dialog them remove the document from the system
//   5) {View} - go to a page, show document at the top followed by similar documents and ranking, with date filtering
//   6) {Click} - if the user clicks on the document, pop up a short page with the document

public class Report {
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception {

        String sDocumentID = (String) props.get ("did", true);
        String sDate = (String) props.get ("date", "-1");
        String sGenreID = (String) props.get ("genreid");

        HTMLDocument Document = new HTML.HTMLDocument();
        Document.AddVariable("Title", "Narrative Report");
        Document.WriteTemplate(out, "header-nosearch.tpl");

        if (!sDate.equals("-1")) {
           Document.AddVariable("RESULTS"+sDate, "SElECTED");
        } else { Document.AddVariable("ALL", "SELECTED"); }

        ITS its = new ITS((String) props.get("SKEY"));
        Vector vDocuments = its.getSimilarDocuments(sDocumentID, sGenreID, sDate);

        String numResults = vDocuments.size()+"";
        if (numResults.equals("0")) {
            Document.WriteSuccess(out, "No document results were found matching that node set.");
        } else {
            Document.AddVariable("NUMRESULTS", numResults);
            Document.AddVariable("DOCUMENTID", sDocumentID);

            Document.WriteTemplate(out, "concept/head-report.tpl");
            float iTotal = new Float(0.0).floatValue();

            for (int loop = 0; loop < vDocuments.size(); loop++) {
                Document d = (Document) vDocuments.elementAt(loop);
                Document.SetHash(d);
                float iCount = new Float(d.get("COUNT")).floatValue();

                Document.AddVariable("PERC", ((int) ((iCount/iTotal) * 100.0)) + " %");

                Document.AddVariable("EDOCTITLE", d.getEncodedField("DOCTITLE"));
                Document.AddVariable("EDOCURL", d.getEncodedField("DOCURL"));

                Document.AddVariable("LOOP", loop+"");

                Document.WriteTemplate(out, "concept/signature-result.tpl");

                if (loop == 1) { // update the trouble status each time a report is generated
                   if (!updateTroubleStatus(props, sDocumentID, ((int) ((iCount/iTotal) * 100.0)), out)) {
                       out.println("<!-- error: could not update the document status -->");
                   }
                }
            }
            Document.WriteTemplate(out, "search/fulltext-foot.tpl");
            Document.WriteFooter(out);
        }
	}

    public static boolean updateTroubleStatus (APIProps props, String sDocumentID, int iPercentage, PrintWriter out) {
        HashTree htArgs = new HashTree(props);

        String sTroubleStatus = "0";
        if (iPercentage >= 70) { sTroubleStatus = "4"; } else
        if (iPercentage >= 50) { sTroubleStatus = "3"; } else
        if (iPercentage >= 30) { sTroubleStatus = "2"; } else
        sTroubleStatus = "0";

        htArgs.put("DocumentID", sDocumentID);
        htArgs.put("TroubleStatus", sTroubleStatus);

        InvokeAPI API = new InvokeAPI("tsdocument.TSEditDocument", htArgs);
        HashTree htResults;

        try { htResults= API.Execute(false, false); }
        catch (Exception e) { api.Log.LogError(e); return false; }

        if (htResults.containsKey("SUCCESS")) { return true; }
        return false;
    }
}