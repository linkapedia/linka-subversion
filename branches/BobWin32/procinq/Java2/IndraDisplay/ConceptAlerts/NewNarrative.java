package ConceptAlerts;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

import com.indraweb.execution.Session;

// Either:
//   1) {View Add} - submit = null, did = null
//   2) {Post Add} - submit = true, did = null
//   3) {View Edit} - submit = false, did = X
//   4) {Post Edit} - submit = true, did = true

public class NewNarrative {
    public static void handleTSapiRequest(APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
            throws Exception {

        String submit = (String) props.get("Submit");
        String nodelist = (String) props.get("nodelist");
        String sDocumentID = (String) props.get("did", "");
        String sDocTitle = (String) props.get("dtitle", "");
        String sDocText = (String) props.get("abstract", "");

        if (nodelist != null) { removeUncheckedResults(req, props, out); return; }

        HTMLDocument Document = new HTML.HTMLDocument();
        Document.AddVariable("DOCUMENTID", sDocumentID);

        // post edit.  remove the existing document then post.
        if ((submit != null) && (!sDocumentID.equals(""))) {
            if (!removeDocument(props, sDocumentID, out)) {
                out.println("<script>alert('Sorry, this narrative could not be changed.  Please see your ITS " +
                        "system administrator for more details.'); window.close(); </script>");
                return;
            }
            classifyDoc(props, sDocTitle, sDocText, out);

        } else // post add.  just add the existing document
            if ((submit != null) && (sDocumentID.equals(""))) {
                if (sDocText.equals("")) {
                    out.println("<script>alert('Error: Narrative abstract may not be empty.');</script>");
                    return;
                }
                classifyDoc(props, sDocTitle, sDocText, out);

            } else // view edit. select document information and display
                if ((submit == null) && (!sDocumentID.equals(""))) {
                    Document d = getDocProps(props, sDocumentID, out);
                    if (d == null) {
                        out.println("<script>alert('Sorry, this narrative could not be accessed.  Please see your ITS " +
                                "system administrator for more details.'); window.close(); </script>");
                        return;
                    }
                    Document.SetHash(d);

                    writeForm(Document, out);
                } else { // view add.  display empty form.
                    writeForm(Document, out);
                }
    }

    // print out the document form
    public static void writeForm(HTMLDocument html, PrintWriter out) {
        html.AddVariable("Title", "Add Narrative");
        html.WriteTemplate(out, "concept/header-nosearch.tpl");
        html.WriteTemplate(out, "concept/edit.tpl");
    }

    // remove a document (permanently) from the database
    public static boolean removeDocument(APIProps props, String sDocumentID, PrintWriter out) {
        ITS its = new ITS((String) props.get("SKEY"));
        try { return its.deleteDocument(sDocumentID); }
        catch (Exception e) { api.Log.LogError(e); return false; }
    }

    public static Document getDocProps(APIProps props, String sDocumentID, PrintWriter out)
            throws Exception {

        HashTree htArgs = new HashTree(props);
        ITS its = new ITS((String) props.get("SKEY"));

        String query = "SELECT <DOCUMENT> WHERE DOCUMENTID = " + sDocumentID;
        Vector vDocuments = its.CQL(query);

        if (vDocuments.size() < 1) {
            return null;
        }
        return (Document) vDocuments.elementAt(0);
    }

    // classify the document
    public static void classifyDoc(APIProps props, String sDocTitle, String sDocText, PrintWriter out)
            throws Exception {

        // replace all quotes with double quotes in the abstract.  change all & to and
        sDocText = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocText, "'", "''"));
        sDocText = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocText, "&", " and "));

        HashTree htArgs = new HashTree(props);

        htArgs.put("DBURL", URLEncoder.encode("#" + sDocTitle, "UTF-8"));
        htArgs.put("GenreID", "241");
        htArgs.put("RepositoryID", "99999");
        htArgs.put("DocTitle", URLEncoder.encode(sDocTitle, "UTF-8"));
        htArgs.put("DocSummary", URLEncoder.encode(sDocText, "UTF-8"));

        htArgs.put("post", "true");
        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        // write whatever is in that text box into the temporary file
        File f = Server.Classify.getTempFile(Session.sIndraHome + "/classifyfiles/");
        if (sDocText.toLowerCase().indexOf("<HTML>") == -1) {
            sDocText = Server.Classify.addHTMLHeaders(sDocText);
        }
        PrintWriter fout = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), true));
        fout.println(sDocText);
        fout.close();

        HashTree htResults = API.PostExecute(f, false);

        if (!htResults.containsKey("CLASSIFICATIONRESULTSET")) {
            api.Log.Log("ClassificationResultSet tag does not exist on attempt to add narrative.");
            out.println("<script>alert('Sorry, this narrative could not be added.  Please see your ITS " +
                    "system administrator for more details.'); window.location = '/servlet/Main?template=ConceptAlerts.Alerts'; </script>");
        } else {
            Document doc = null;
            try {
                doc = getDocument(props, "#" + sDocTitle, out);
            } catch (Exception e) {
                api.Log.LogError(e);
                out.println("<script>alert('Sorry, this narrative could not be added.  Please see your ITS " +
                    "system administrator for more details.'); window.location = '/servlet/Main?template=ConceptAlerts.Alerts'; </script>");
            }
            writeClassifyResults(props, doc, htResults, out);
        }

        f.delete();
    }

    public static Document getDocument (APIProps props, String sDocURL, PrintWriter out) throws Exception {
        ITS its = new ITS((String) props.get("SKEY"));

        String query = "SELECT <DOCUMENT> WHERE GENREID = 241 AND DOCURL = '"+sDocURL+"'";
        Vector v = its.CQL(query);

        if (v.size() == 0) { throw new Exception("No documents found."); }
        return (Document) v.elementAt(0);
    }

    // print checkbox and delete unchecked results
    public static void writeClassifyResults(APIProps props, Document d, HashTree htResults, PrintWriter out) {
        HashTree htClass = (HashTree) htResults.get("CLASSIFICATIONRESULTSET");
        Vector vNodes = Server.Classify.Sort(htClass);
        Enumeration eT = vNodes.elements();

        HTMLDocument Document = new HTMLDocument();
        Document.AddVariable("DOCUMENTID", d.get("DOCUMENTID"));
        Document.AddVariable("Title", "Add Narrative");
        Document.WriteTemplate(out, "concept/header-nosearch.tpl");

        // Loop through each result and print it
        int loop = 0;
        while (eT.hasMoreElements()) {
            HashTree htNodeBucket = (HashTree) eT.nextElement();
            float fScore = new Float((String) htNodeBucket.get("SCORE1")).floatValue();

            // Ensure threshold match
            if (fScore > new Float(50.0).floatValue()) {
                loop = loop + 1;
                if (loop == 1) {
                    Document.WriteTemplate(out, "concept/classify-start.tpl");
                }
                String sNodeTree
                        = Taxonomy.Search.BuildNodeTree(out, (String) htNodeBucket.get("NODEID"), props);
                sNodeTree = loop + ". <a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID=" +
                        (String) htNodeBucket.get("NODEID") + "'><i><b>" +
                        (String) htNodeBucket.get("NODETITLE") + "</b></i></a> &nbsp; " +
                        "<BR>" + sNodeTree + "<P>";
                Document.SetHash(htNodeBucket);
                Document.AddVariable("NODETREE", sNodeTree);
                Document.WriteTemplate(out, "similar/search.tpl");
            }
        }
        if (loop != 0) {
            Document.WriteTemplate(out, "concept/search-end.tpl");
        } else {
            out.println("<blockquote>Sorry, this document did not classify into any topics.</blockquote>");
        }
    }

    public static void removeUncheckedResults(HttpServletRequest req, APIProps props, PrintWriter out) {
        String sDocumentID = (String) props.get("did", "");

        String sNodeIDs = "0";
        Enumeration eR = req.getParameterNames();
        while (eR.hasMoreElements()) {
            String s = (String) eR.nextElement();
            if (s.startsWith("CLASSIFY")) {
                sNodeIDs = sNodeIDs+","+s.substring(8, s.length());
            }
        }

        HashTree htArguments = new HashTree(props);
        htArguments.put("NodeList", sNodeIDs);
        htArguments.put("DocumentID", sDocumentID);

        if (!sNodeIDs.equals("0")) {
            // remove all relationships that were not checked
            InvokeAPI API = new InvokeAPI("tsnode.TSRemoveAllNodeDocumentNot", htArguments);
            HashTree htResults;

            try { htResults = API.Execute(false, false); }
            catch (Exception e) {
                api.Log.LogError(e);
                out.println("<script>alert('Sorry, this narrative could not be added.  Please see your ITS " +
                    "system administrator for more details.'); window.location = '/servlet/Main?template=ConceptAlerts.Alerts'; </script>"); return;
            }

            if (!htResults.containsKey("SUCCESS")) {
                api.Log.Log("ClassificationResultSet tag does not exist on attempt to add narrative.");
                out.println("<script>alert('Sorry, this narrative could not be added.  Please see your ITS " +
                    "system administrator for more details.'); window.location = '/servlet/Main?template=ConceptAlerts.Alerts'; </script>"); return;
            }
        }
        out.println("<script>alert('Your narrative was successfully added to the system.'); "+
                    "window.location = '/servlet/Main?template=ConceptAlerts.Alerts'; </script>");

    }
}