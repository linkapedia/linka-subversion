package ConceptAlerts;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import HTML.*;
import Server.*;
import Logging.*;

import com.indraweb.execution.Session;

public class RemoveNarrative {
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception {

        String sDocumentID = (String) props.get ("did", true);

        if (!ConceptAlerts.NewNarrative.removeDocument(props, sDocumentID, out)) {
            out.println("<script>alert('Sorry, this narrative could not be changed.  Please see your ITS "+
                        "system administrator for more details.'); window.close(); </script>");
            return;
        } else {
            out.println("<script>alert('Your narrative was removed successfully..'); "+
                        "opener.location=opener.location.href; window.close(); </script>");
        }
	}
}
