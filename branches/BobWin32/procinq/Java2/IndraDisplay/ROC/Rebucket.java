package ROC;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class Rebucket
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		
		String sSuccess = "This corpus has been rebucketed successfully.<P>"+
						  "<a href='/servlet/Main?template=Taxonomy.EditCorpus&CorpusID="+sCorpusID+"'>"+
						  "Click here to return to the corpus properties</a>.";
			
		HashTree htArguments = new HashTree(props);
		htArguments.put("CorpusID", sCorpusID);
        String sROCRebucketFunctionToUse = "tsclassify.TSApplyROC";
        if ( com.indraweb.util.UtilFile.bFileExists("c:/temp/IndraDebugOldROCRebucket.txt")) {
            api.Log.Log( "debug info: using old ROC rebucket model as file exists [c:/temp/IndraDebugOldROCRebucket.txt]");
            sROCRebucketFunctionToUse = "tscorpus.TSRebucket";
        }
		InvokeAPI API = new InvokeAPI(sROCRebucketFunctionToUse, htArguments);
		HashTree htResults = API.Execute(false, false);
			
		if (!htResults.containsKey("SUCCESS")) {
			sSuccess = "Sorry, an error occured while processing your request.";	
		}
	
		HTMLDocument Document = new HTMLDocument();
		Document.WriteSuccess(out, sSuccess);			
	}
}
