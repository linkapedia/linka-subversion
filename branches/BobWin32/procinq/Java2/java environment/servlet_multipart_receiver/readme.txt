This is the README file for the com.oreilly.servlet package.
------------------------------------------------------------ 

The com.oreilly.servlet package contains a set of useful utility classes for
servlet developers. Included are classes to help servlets parse parameters,
handle multipart requests (file uploads), generate multipart responses (server
push), negotiate locales for internationalization, return files, manage socket
connections, and act as RMI servers, among other things. There's even a class
to help applets communicate with servlets. 

Why did I write these classes? They started out as chapter examples in my book
Java Servlet Programming being published by O'Reilly & Associates. Then one
happy day I recognized they could, with a little modification, live on their
own as reusable utility classes. That day, or maybe a few procrastinating days
later, com.oreilly.servlet was born. 

The classes in the com.oreilly.servlet package have been offered from this
site since April 1998. The classes have been widely tested on a multitude of
platforms and server configurations, and you should find this a very stable
release. 

I hope you enjoy using these classes and that they help you write more
powerful and elegant servlets. 

Lastly, please respect the license for this package. 

Thanks,
Jason Hunter 

 
