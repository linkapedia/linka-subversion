package com.iw.system;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.table.TableModel;
import javax.swing.*;
import java.util.*;
import java.util.List;
import java.io.*;
import java.net.URLEncoder;
import java.awt.*;
import java.awt.datatransfer.*;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;
import com.iw.guiservershared.TableModelXMLBeanSerializable;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * The ITS object defines both a user session and contains methods used to interface with the ITS API.  Each of
 * these methods are simply abstract another layer onto the raw InvokeAPI class.
 *
 * A pointer to the ITS object should be kept persistently as it is the container for both the user's session
 *  key and the location of the API server.
 *
 *	@authors Michael A. Puscar, Indraweb Inc, All Rights Reserved.
 *
 *  @note   Session and ITS server information are stored as a public property of the frame.
 *	@return An ITS object.
 */

public class ITS implements Serializable {
    private String SKEY = "NONE";
    private String API = "http://localhost/";
    private String domainController = null;

    // Clipboard object moved here to avoid marriage to individual panes and components.
    public Clipboard clipboard = new Clipboard("local");
    public TreePath originalSelectionPath = null;

    public DefaultMutableTreeNode cutNode = null;
    private static PrintStream Systemout = null;

    public ITS() {
    }

    public ITS(String SessionKey, String API) {
        if (SessionKey != null) {
            this.SKEY = SessionKey;
        }
        if (API != null) {
            this.API = API + "itsapi/ts?fn=";
        }
    }

    // Accessor functions
    public void SetSessionKey(String SessionKey) {
        if (SessionKey != null) {
            this.SKEY = SessionKey;
        }
    }

    public void SetAPI(String API) {
        if (API != null) {
            this.API = API + "itsapi/ts?fn=";
        }
    }

    public String getSessionKey() {
        return SKEY;
    }

    public String getAPI() {
        return API;
    }

    public HashTree getArguments() {
        HashTree ht = new HashTree();
        ht.put("api", API);
        ht.put("SKEY", SKEY);

        return ht;
    }

    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeAPI(sAPIcall, htArgs, true); }
    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs, boolean bReplaceQuotes) throws Exception {
        return InvokeAPI(sAPIcall, htArgs, bReplaceQuotes, false);
    }
    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs, boolean bReplaceQuotes, boolean bDebug)
            throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        HashTree ht = null;
        try { ht = API.Execute(bDebug, false, bReplaceQuotes); }
        catch (SessionExpired e) { sessionTimedOut(); }
        catch (Exception e) { throw e; }

        return ht;
    }

    public Vector InvokeVectorAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeVectorAPI(sAPIcall, htArgs, false); }
    public Vector InvokeVectorAPI(String sAPIcall, Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        Vector v = new Vector();
        try { v = API.vExecute(bDebug, false); }
        catch (SessionExpired e) { sessionTimedOut(); }
        catch (Exception e) { throw e; }

        return v;
    }

    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeDomAPI(sAPIcall, htArgs, false); }
    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try { d = API.dExecute(bDebug, false); }
        catch (Exception e) { throw e; }

        return d;
    }

    public org.dom4j.Document InvokeDomPostAPI(String sAPIcall, Hashtable htArgs) throws Exception {
        return InvokeDomPostAPI(sAPIcall, htArgs, false); }
    public org.dom4j.Document InvokeDomPostAPI(String sAPIcall, Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try { d = API.dExecutePost(bDebug, false); }
        catch (Exception e) { throw e; }

        return d;
    }

    public static void activateLogging() {
        // logging streams
        PrintStream printstream_out = null;

        try {
            Systemout = System.out;

            File logfile_out = new File("IWclient_out_" + System.currentTimeMillis() + ".txt");
            FileOutputStream fos_out = new FileOutputStream(logfile_out);
            printstream_out = new PrintStream(fos_out);
            System.setOut(printstream_out);
            System.setErr(printstream_out);

            System.out.println("Client logging enabled.");

            System.out.flush();
            System.err.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deactivateLogging() {
        if (Systemout != null) {
            System.setOut(Systemout);
            System.setErr(Systemout);

            System.out.println("Client logging disabled.");
        }
    }

    public static User getUser() {
        File fp = new File("user.data");
        if (fp.exists()) {
            try { return User.deserializeObject("user.data"); }
            catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Note, due to a system change your previous user "+
                    "preferences have been lost.", "Information", JOptionPane.NO_OPTION);
            }
        }
        return null;
    }

    public String getDomainController() throws Exception {
        if (domainController == null) {
            Hashtable htArgs = getArguments();
            htArgs.put("PARAMNAME", "DC");

            try { // invoke the API
                HashTree htResults = InvokeAPI("security.TSGetConfigParams", htArgs);

                if (!htResults.containsKey("CONFIGPARAMS")) {
                    return "DC=";
                }
                HashTree htConfig = (HashTree) htResults.get("CONFIGPARAMS");
                return (String) htConfig.get("DC");

            } catch (Exception e) {
                throw e;
            }
        }

        return domainController;
    }

    // hardcoded (for now), will read from file
    public String getClientVersion() {
        return "ProcinQ Client 1.2";
    }

    // get the server version
    public String getServerVersion() {
        Hashtable htArgs = getArguments();

        try {
            HashTree htResults = InvokeAPI("tsother.TSGetVersion", htArgs);
            if (!htResults.containsKey("VERSION")) { return "Server version unavailable."; }

            return (String) htResults.get("VERSION");
        } catch (Exception e) {
             return "Server version unavailable.";
        }
    }

    public String getAdminGroup() throws Exception {
        if (domainController == null) {
            Hashtable htArgs = getArguments();
            htArgs.put("PARAMNAME", "AdminGroup");

            try { // invoke the API
                HashTree htResults = InvokeAPI("security.TSGetConfigParams", htArgs);

                if (!htResults.containsKey("CONFIGPARAMS")) {
                    return "";
                }
                HashTree htConfig = (HashTree) htResults.get("CONFIGPARAMS");
                return (String) htConfig.get("AdminGroup");

            } catch (Exception e) {
                throw e;
            }
        }

        return domainController;
    }

    // ITS Login
    // Arguments: Username, Password
    // Returns: User object
    public User Login(String Username, String Password) throws Exception {
        // LOGIN routine takes a USERID and a PASsWORD
        Hashtable htArgs = new Hashtable();
        htArgs.put("UserID", Username); // example: "sn=cifaadmin,ou=users,dc=cifanet";
        htArgs.put("Password", Password); // example: racer9
        htArgs.put("api", API);

        try {
            // Invoke the ITSAPI now.
            HashTree htResults = InvokeAPI("security.TSLogin", htArgs);

            // If there is no "subscriber" tag, an error has occured
            if (!htResults.containsKey("SUBSCRIBER")) {
                throw new Exception("Invalid username, password combination.");
            }

            // Get user hash tree
            HashTree htUser = (HashTree) htResults.get("SUBSCRIBER");

            if (!htUser.containsKey("KEY")) {
                throw new Exception("Invalid username, password combination.");
            }

            return new User(htUser);
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getFolders() throws Exception {
        return getFolders(null); }
    public Vector getFolders(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) htArgs.put("CorpusID", c.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsgenre.TSListFolders", htArgs);

            if (!htResults.containsKey("GENRES")) {
                return new Vector();
            }

            HashTree htGenres = (HashTree) htResults.get("GENRES");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                Genre g = new Genre(htGenre); v.add(g);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getSubjectAreas() throws Exception {
        return getSubjectAreas(null); }
    public Vector getSubjectAreas(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) htArgs.put("CorpusID", c.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tssubjectarea.TSListSubjectAreas", htArgs);

            if (!htResults.containsKey("SUBJECTAREAS")) {
                return new Vector();
            }

            HashTree htGenres = (HashTree) htResults.get("SUBJECTAREAS");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                SubjectArea sa = new SubjectArea(htGenre); v.add(sa);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public void FlagAlert(String AlertID) throws Exception {
        Hashtable htArgs = getArguments();
		try {
			htArgs.put("ID", AlertID); // indicates that we want all results
            HashTree ht = InvokeAPI("tsnotification.TSLastRunNow", htArgs);

 			// If there is no elements, an error has occured
			if (!ht.containsKey("SUCCESS")) { throw new Exception("Attempt to flag alert failed."); }
		} catch (Exception e) {
			System.out.println("Flag alert failed: "+e.getMessage());
            e.printStackTrace(System.out);
		}
    }

    public Vector getNotificationAlerts(String UserID) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

		try {
			htArgs.put("Email", "admin"); // indicates that we want all results
            htArgs.put("LastRun", "true"); // only return alerts which have not been run since last interval
            htArgs.put("UserID", UserID);

            org.dom4j.Document doc = InvokeDomAPI("tsnotification.TSViewAlerts", htArgs);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("ALERTS");
            if (eNodes == null) return new Vector();

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();
                Alert a = new Alert(eNode);

                v.add(a);
            }

 			// If there is no elements, an error has occured
			if (v.size() < 1) { throw new Exception("There are no alerts available at this time."); }

            return v;

		} catch (Exception e) {
			System.out.println("Get alerts failed: "+e.getMessage());
            e.printStackTrace(System.out);
			return null;
		}
    }

    public Vector getConceptAlerts() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        htArgs.put("gid", "241");

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSGetDocumentByGenre", htArgs);

            if (!htResults.containsKey("DOCUMENTS")) {
                return new Vector();
            }

            HashTree htDocs = (HashTree) htResults.get("DOCUMENTS");
            Enumeration eD = htDocs.elements();
            while (eD.hasMoreElements()) {
                HashTree htDoc = (HashTree) eD.nextElement();
                Document d = new Document(htDoc); v.add(d);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getSecurityGroups() throws Exception {
        return getSecurityGroups(null, null); }
    public Vector getSecurityGroups(String s) throws Exception {
        return getSecurityGroups(null, s); }
    public Vector getSecurityGroups(Corpus c) throws Exception {
        return getSecurityGroups(c, null); }
    public Vector getSecurityGroups(Corpus c, String Access) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) htArgs.put("CorpusID", c.getID());

        // invoke the API
        try {
            HashTree htResults;

            if (c != null) { htResults = InvokeAPI("tscorpus.TSListCorpusAccess", htArgs); }
            else { htResults = InvokeAPI("tsuser.TSListGroups", htArgs); }

            if (!htResults.containsKey("GROUPS")) {
                return new Vector();
            }

            HashTree htGroups = (HashTree) htResults.get("GROUPS");
            Enumeration eG = htGroups.elements();
            while (eG.hasMoreElements()) {
                HashTree htGroup = (HashTree) eG.nextElement();
                SecurityGroup sg = new SecurityGroup(htGroup);

                if (Access == null) { v.add(sg); }
                else if (Access.equals(sg.getAccess())) { v.add(sg); }
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getFields(String TableName) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("Table", TableName);

        Vector v = new Vector();

        try {
            org.dom4j.Document doc = InvokeDomAPI("tsother.TSGetTableInfo", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            Element eTable = elemRoot.element("TABLE");
            if (eTable == null) return new Vector();

            Iterator i = eTable.elements().iterator();
            while (i.hasNext()) {
                Element eField = (Element) i.next();

                Iterator i2 = eField.elements().iterator();
                IndraField inf = new IndraField(i2);

                v.add(inf);
            }
        } catch (Exception e) {
            throw e;
        }

        return v;
    }

    public Vector getRepositories() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        try {
            HashTree htResults = InvokeAPI("tsrepository.TSGetRepositoryList", htArgs);

            if (!htResults.containsKey("REPOSITORIES")) {
                throw new Exception("There are no repositories in this database.");
            }

            HashTree htReps = (HashTree) htResults.get("REPOSITORIES");
            Enumeration eR = htReps.elements();
            while (eR.hasMoreElements()) {
                HashTree htRepository = (HashTree) eR.nextElement();

                // repository type 1 is reserved, do not include in the list
                Repository R = new Repository(htRepository);
                if (!R.getID().equals("1")) { v.add(R); }
            }

        } catch (Exception e) { throw e; }

        return v;
    }

    public Vector CQL(String Query) throws Exception {
        return CQL(Query, 1, 500);
    }

    public Vector CQL(String Query, int Start, int RowMax) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("query", Query);
        htArgs.put("start", "" + Start);
        htArgs.put("rowmax", "" + RowMax);

        Vector v = new Vector();
        //System.out.println("CQL: "+Query);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscql.TSCql", htArgs);
            Element elemRoot = doc.getRootElement ();

            // return different structures depending upon NODE or DOCUMENT
            if (Query.toUpperCase().indexOf("<NODE>") != -1) {
                Element eNodes = elemRoot.element("NODES");
                if (eNodes == null) return new Vector();

                Iterator i = eNodes.elements().iterator();
                while (i.hasNext()) {
                    Element eNode = (Element) i.next();

                    Iterator i2 = eNode.elements().iterator();
                    Node n = new Node(i2);

                    v.add(n);
                }
            } else if ((Query.toUpperCase().indexOf("<DOCUMENT>") != -1) ||
                       (Query.toUpperCase().indexOf("<NARRATIVE>") != -1)) {
                Element eDocs = elemRoot.element("DOCUMENTS");
                if (eDocs == null) return new Vector();

                Iterator i = eDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eDoc = (Element) i.next();

                    Iterator i2 = eDoc.elements().iterator();
                    com.iw.system.Document d = new com.iw.system.Document(i2);

                    v.add(d);
                }
            } else if (Query.toUpperCase().indexOf("<NODEDOCUMENT>") != -1) {
                Element eNodeDocs = elemRoot.element("NODEDOCUMENTS");
                if (eNodeDocs == null) return new Vector();

                Iterator i = eNodeDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eNodeDoc = (Element) i.next();

                    Iterator i2 = eNodeDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            } else {
                throw new Exception("The CQL selection object was invalid.");
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Hashtable CQLHash(String Query) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("query", Query);
        htArgs.put("start", "1");
        htArgs.put("rowmax", "10000");

        Hashtable ht = new Hashtable();
        //System.out.println("CQL: "+Query);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscql.TSCql", htArgs, false);
            Element elemRoot = doc.getRootElement ();

            // return different structures depending upon NODE or DOCUMENT
            if (Query.toUpperCase().indexOf("<NODE>") != -1) {
                Element eNodes = elemRoot.element("NODES");
                if (eNodes == null) return ht;

                Iterator i = eNodes.elements().iterator();
                while (i.hasNext()) {
                    Element eNode = (Element) i.next();

                    Iterator i2 = eNode.elements().iterator();
                    Node n = new Node(i2);

                    ht.put(n.get("NODEID"), n);
                }
            } else if (Query.toUpperCase().indexOf("<DOCUMENT>") != -1) {
                Element eDocs = elemRoot.element("DOCUMENTS");
                if (eDocs == null) return new Hashtable();

                Iterator i = eDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eDoc = (Element) i.next();

                    Iterator i2 = eDoc.elements().iterator();
                    com.iw.system.Document d = new com.iw.system.Document(i2);

                    ht.put(d.get("DOCUMENTID"), d);;
                }
            } else if (Query.toUpperCase().indexOf("<NODEDOCUMENT>") != -1) {
                Element eNodeDocs = elemRoot.element("NODEDOCUMENTS");
                if (eNodeDocs == null) return new Hashtable();

                Iterator i = eNodeDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eNodeDoc = (Element) i.next();

                    Iterator i2 = eNodeDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    ht.put(nd.get("NODEID")+nd.get("DOCUMENTID"), nd);
                }
            } else {
                throw new Exception("The CQL selection object was invalid.");
            }

            return ht;
        } catch (Exception e) {
            throw e;
        }
    }

    public TableModel tsThesaurusNodeReport ( Hashtable htArgs  ) throws Exception {

        Hashtable htArgsMerge = mergeHashTables( getArguments(), htArgs, true);

        Vector v = new Vector();

        try {
            InvokeAPI API = new InvokeAPI("tsthesaurus.TSThesaurusNodeReport", htArgsMerge);
            org.dom4j.Document docExecuteSqlResult = API.dExecute(false, false);
            Element elemRoot = docExecuteSqlResult.getRootElement();
            Element elem_DATATABLE = elemRoot.element("OBJECT_TABLEMODEL_BEANXML");
            if ( elem_DATATABLE == null )
                throw new Exception("System error: tsthesaurus.TSThesaurusNodeReport returns no tablemodel." );

            String sXMLized_DATATABLE = elem_DATATABLE.getText().trim();
            Vector vTableModelXMLizedFormReturn = (Vector) XMLBeanSerializeHelper.deserializeFromString(sXMLized_DATATABLE.trim());
            TableModelXMLBeanSerializable tableModelIWSerializable = new TableModelXMLBeanSerializable();
            tableModelIWSerializable.setFromIWXMLData(new ByteArrayInputStream(sXMLized_DATATABLE.getBytes()));

            return tableModelIWSerializable;
        } catch (Exception e) {
            throw e;
        }
    }

    // take a text file and wrap it with HTML
    public static void wrapHTMLstatic(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>None</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public static void wrapHTMLstatic (File f, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData +"\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(sOut);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>None</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public File wrapHTML(String title, String buffer) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";

        File tempFile = new File("file.html");
        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>"+title+"</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(buffer);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }

        return tempFile;
    }

    public static void wrapHTMLstatic(String s, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(String s, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(File f, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData +"\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(sOut);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    // 0: contains, 1: starts with, 2: ends with
    public Vector topicSearch(String SearchTerm, int Type) throws Exception {
        String query = SearchTerm;

        switch (Type) {
            case 0:
                query = "%%" + query + "%%";
                break;
            case 1:
                query = query + "%%";
                break;
            case 2:
                query = "%%" + query;
                break;
        }

        query = query.replaceAll("'", "''");
        query = "SELECT <NODE> WHERE NODETITLE LIKE '" + query + "' ORDER BY NODETITLE ASC";
        return CQL(query, 1, 500);
    }

    public Vector topicSearch(String SearchTerm, int Type, Corpus corpus) throws Exception {
        String query = SearchTerm;

        switch (Type) {
            case 0:
                query = "%%" + query + "%%";
                break;
            case 1:
                query = query + "%%";
                break;
            case 2:
                query = "%%" + query;
                break;
        }

        query = query.replaceAll("'", "''");
        query = "SELECT <NODE> WHERE NODETITLE LIKE '" + query + "' AND CORPUSID = " + corpus.getID() + " ORDER BY NODETITLE ASC";
        return CQL(query, 1, 500);
    }

    // titles and thesaurus - all for a tsclassify to expand a pure sig set
    // NOT A MOD OF THE INPUT SIGNATURES OBJECT ... NEW OBJECT RETURNED
    public Signatures signatureExpand ( Signatures signatures, int iNodeID,
                                        int iCorpusID ) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", ""+ iNodeID);
        htArgs.put("CorpusID", ""+ iCorpusID);
        String sDynamicNodeSigs = signatures.toString(true);
        htArgs.put("Sigs", sDynamicNodeSigs);
        htArgs.put("NumParentTitleTermsAsSigs", ""+getUser().getNumParentTitleTermAsSigs());
        htArgs.put("WordFreqMaxForSig", ""+getUser().getWordFreqMaxForSig());
        htArgs.put("IncludeNumbers", ""+getUser().getIncludeNumbers());


        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tssignature.TSSignatureExpand", htArgs);
            Element elemRoot = doc.getRootElement ();

            Element eSignatures = elemRoot.element("SIGNATURES");
            Iterator iter = eSignatures.elements().iterator();

            return new Signatures (iter);

        } catch (Exception e) {
            throw e;
        }
    } // signatureexpand


    // handles both document and abstract classify
    // returns a vector of NodeDocument objects
    public Vector classify (int DocumentID, String DocTitle, boolean bDocClassify ) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocIDSource", "" + DocumentID);
        htArgs.put("DocTitle", DocTitle);

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        User u = getUser();
        htArgs.put("Corpora", u.getCorporaToUse());
        htArgs.put("NumParentTitleTermsAsSigs", ""+u.getNumParentTitleTermAsSigs());
        if (u.getCorporaToUse().equals("-1")) {
            throw new EmptyCorpusToUseList();
        }
        htArgs.put("dweight", u.getDocumentWeight()+"");
        htArgs.put("sweight", u.getAbstractWeight()+"");

        Vector v = new Vector(); // return set

        Vector vResults = InvokeVectorAPI("tsclassify.TSClassifyDoc", htArgs);

        if (vResults.size() < 3) {
            throw new Exception("ClassificationResultSet tag does not exist in XML output docid [" + DocumentID + "]");
        }

        Vector vDocs = (Vector) vResults.elementAt(2);

        Enumeration eD = vDocs.elements(); int loop = 0;
        while (eD.hasMoreElements()) {
            loop++; Object o = eD.nextElement();
            if (loop > 1) { // item number one is NUMNODEDOCSCORES
                Vector vNodeDocument = (Vector) o;
                NodeDocument nd = new NodeDocument(vNodeDocument);
                nd.set("DOCUMENTID",DocumentID+""); nd.set("DOCTITLE",DocTitle);

                v.add(nd); // add this document to the vector
            }
        }
        return v;
    }

    // classify a document into a specific node, given dynamic node parameters
    // returns a new NodeDocument object
    /* "&DynamicNodeSigs=3,term1|||5,term2||thesterm21||thesterm22|||7,term3||thesterm31" +
                         "&DCNodeTitle=hello world" +
                         "&DCNumTitleWords=1" +
                         "&DCNodeSize=51" +
                         "&DCBuildThesaurus=false"
     */

    // used by SignatureTestPanel
    int iCallCntClass = 0;
    public NodeDocument classify (NodeDocument nd, Vector vSignatureObjects ) throws Exception {
        NodeDocument ndResult = null;

        // thesaurus expand these signatures

        Hashtable htArgs = getArguments();
        htArgs.put("DocIDSource", nd.get("DOCUMENTID"));
        htArgs.put("DocTitle", nd.get("DOCTITLE"));

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        htArgs.put("CorpusID", nd.get("CORPUSID"));
        htArgs.put("Corpora", nd.get("CORPUSID"));
        htArgs.put("NodeID", ""+ nd.get("NODEID"));
        htArgs.put("NodeSet", ""+ nd.get("NODEID"));
        htArgs.put("NodeTitle", ""+ nd.get("NODETITLE"));
        htArgs.put("NodeSize", ""+ nd.get("NODESIZE"));
        htArgs.put("NumParentTitleTermsAsSigs", ""+getUser().getNumParentTitleTermAsSigs());


        User u = getUser();
        htArgs.put("dweight", u.getDocumentWeight()+"");
        htArgs.put("sweight", u.getAbstractWeight()+"");

        // classify with sigs - thesaurus and title expand these signatures
        Signatures signaturesObject = ITSHelper.vSignatureObjectsToSignaturesObject(vSignatureObjects);
        Signatures signaturesObjectExpanded = signatureExpand (signaturesObject,
                Integer.parseInt(nd.get("NODEID")),
                Integer.parseInt(nd.get("CORPUSID")));
        htArgs.put("Sigs", signaturesObjectExpanded.toString());

        // invoke the API
        try {
            Enumeration enum = htArgs.keys();

            org.dom4j.Document doc = InvokeDomAPI("tsclassify.TSClassifyDoc", htArgs);
            Element elemRoot = doc.getRootElement ();

            Element eNodes = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eNodes == null) return null;

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                if (eNode.getQName().getName().equals("NODE")) {
                    Iterator i2 = eNode.elements().iterator();
                    ndResult = new NodeDocument(i2);
                }
            }

            return ndResult;
        } catch (Exception e) {
            throw e;
        }
    }


    public Vector classifyNarrative (String DocTitle, String DocText) throws Exception {
        Hashtable htArgs = getArguments();

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        htArgs.put("NumParentTitleTermsAsSigs", ""+getUser().getNumParentTitleTermAsSigs());

        User u = getUser(); htArgs.put("Corpora", u.getCorporaToUse());
        if (u.getCorporaToUse().equals("-1")) {
            throw new EmptyCorpusToUseList();
        }

        Vector v = new Vector();
        File tempFile = wrapHTML(DocTitle, DocText);

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);
            org.dom4j.Document doc = API.dExecute(tempFile);
            Element elemRoot = doc.getRootElement ();

            Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eDocs == null) throw new Exception("ClassificationResultSet tag does not exist in XML output");

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                if (eDoc.getQName().getName().equals("NODE")) {
                    Iterator i2 = eDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            tempFile.delete();
        }

        return v;
    }

    private void sessionTimedOut() {
        JOptionPane.showMessageDialog(null, "Your session has timed out.  Please restart the application.", "Information", JOptionPane.NO_OPTION);
        System.exit(-1);
    }

    // collect up the contents of two ht;s into one - leaves originals unchanged
    private static Hashtable mergeHashTables (Hashtable ht1, Hashtable ht2,
                                              boolean bOverlapAllowed) throws Exception {


		Hashtable htMerged = new Hashtable();

        Enumeration e = ht1.keys();
		Object k1;
		while (e.hasMoreElements())  {
			k1 = (Object) e.nextElement();
			Object v1 = ht1.get( k1);
            htMerged.put ( k1, v1);
		}

        Enumeration e2 = ht2.keys();
		Object k2;
		while (e2.hasMoreElements())  {
			k2 = (Object) e2.nextElement();
            if ( !bOverlapAllowed && ht1.get(k2) != null )
                throw new Exception ("merge hashtables - overlap occurred [" + k2 + "]" );
			Object v2 = ht2.get( k2);
            htMerged.put ( k2, v2);
		}

		return htMerged;
	}

}
