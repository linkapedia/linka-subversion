package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 24, 2003
 * Time: 1:35:47 PM
 */

/*
  <TSRESULT>
      <REPOSITORY>
          <REPOSITORYID>1</REPOSITORYID>
          <REPOSITORYTYPE>ROC Analysis</REPOSITORYTYPE>
          <REPOSITORYNAME>ROC Analysis</REPOSITORYNAME>
          <REPOSITORYLOCATION>/nike</REPOSITORYLOCATION>
          <USERNAME>null</USERNAME>
          <PASSWORD>null</PASSWORD>
          <FULLTEXT>1</FULLTEXT>
      </REPOSITORY>
  </TSRESULT>
 */
import java.util.Vector;

public class Repository implements Comparable {
    // User attributes
    private String RepositoryID;
    private String RepositoryName;
    private String RepositoryType;
    private String RepositoryLocation;
    private String Username;
    private String Password;
    private boolean Fulltext;

    // constructor(s)
    public Repository () { }
    public Repository (HashTree ht) {
        RepositoryID = (String) ht.get("REPOSITORYID");
        RepositoryName = (String) ht.get("REPOSITORYNAME");
        RepositoryType = (String) ht.get("REPOSITORYTYPE");
        RepositoryLocation = (String) ht.get("REPOSITORYLOCATION");
        Username = (String) ht.get("USERNAME");
        Password = (String) ht.get("PASSWORD");
        String sFT = (String) ht.get("FULLTEXT");
        if ((sFT != null) && (sFT.equals("1"))) { Fulltext = true; } else { Fulltext = false; }
    }

    public Repository (Vector v) {
        RepositoryID = (String) v.elementAt(0);
        RepositoryType = (String) v.elementAt(1);
        RepositoryName = (String) v.elementAt(2);
        RepositoryLocation = (String) v.elementAt(3);
        Username = (String) v.elementAt(4);
        Password = (String) v.elementAt(5);
        String sFT = (String) v.elementAt(6);
        if ((sFT != null) && (sFT.equals("1"))) { Fulltext = true; } else { Fulltext = false; }
    }

    // accessor functions
    public String getID() { return RepositoryID; }
    public String getType() { return RepositoryType; }
    public String getName() { return RepositoryName; }
    public String getLocation() { return RepositoryLocation; }
    public String getUsername() { return Username; }
    public String getPassword() { return Password; }
    public boolean isFullText() { return Fulltext; }

    public void setID(String ID) { RepositoryID = ID; }
    public void setType(String Type) { RepositoryType = Type; }
    public void setName(String Name) { RepositoryName = Name; }
    public void setLocation(String Location) { RepositoryLocation = Location; }
    public void setFullText(boolean FullText) { Fulltext = FullText; }

    public String toString() { return RepositoryName; }
    public int compareTo(Object o) { return RepositoryName.compareTo(o.toString()); }
}
