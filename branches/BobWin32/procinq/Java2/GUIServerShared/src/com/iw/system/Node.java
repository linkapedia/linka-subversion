package com.iw.system;

import com.iw.tools.PubliclyCloneable;

import java.util.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.net.URLEncoder;

import org.dom4j.Element;

public class Node implements PubliclyCloneable, Serializable, Comparable {

    // attributes inherited from combining node classes
	private Vector vChildNodes = new Vector();
    private Vector vSignatures = new Vector();

    public Hashtable htn;

    // constructor(s)
    public Node () {}
    public Node (HashTree ht) {
        htn = new Hashtable();

        Enumeration eN = ht.keys();
        while (eN.hasMoreElements()) {
            String key = (String) eN.nextElement();
            String val = (String) ht.get(key);

            htn.put(key, val);
        }

        if (!htn.containsKey("NODESIZE")) htn.put("NODESIZE", "0");
    }
    public Node (Hashtable ht) {
        if (htn == null) htn = new Hashtable();
        htn = ht;

        if (!htn.containsKey("NODESIZE")) htn.put("NODESIZE", "0");
    }
    public Node (Vector v) {
        if (htn == null) htn = new Hashtable();
        for (int i = 0; i < v.size(); i++) {
            VectorValue vv = (VectorValue) v.elementAt(i);
            htn.put(vv.getName(), vv.getValue());
        }
    }
    public Node (Iterator i) {
        if (htn == null) htn = new Hashtable();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            htn.put(e.getName(), e.getText());
        }
    }

    public Node (ResultSet rs) throws Exception {
        ResultSetMetaData rsmd = rs.getMetaData();

        if (htn == null) htn = new Hashtable();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            if (rs.getString(i) != null)
                htn.put(rsmd.getColumnName(i), rs.getString(i));
            else htn.put(rsmd.getColumnName(i), "");
        }
    }
    public Node (int ID, int Corpus, int Parent, String Title, int Index, int Depth, String Desc,
                 int Size, int Status, int LinkNode) {
        if (htn == null) htn = new Hashtable();

        htn.put("NODEID", ID+"");
        htn.put("CORPUSID", Corpus+"");
        htn.put("PARENTID", Parent+"");
        htn.put("NODETITLE", Title);
        htn.put("NODEINDEXWITHINPARENT", Index+"");
        htn.put("DEPTHFROMROOT", Depth+"");
        htn.put("NODEDESC", Desc);
        htn.put("NODESIZE", Size+"");
        htn.put("NODESTATUS", Status+"");
        htn.put("LINKNODEID", LinkNode+"");
    }

    // return true or false if this node is a link
    public boolean isLink() {
        //System.out.println("is link ("+get("NODETITLE")+") NODEID ("+get("NODEID")+") LINKNODEID ("+get("LINKNODEID")+")");
        if (get("LINKNODEID") == null) return false;
        if (get("NODEID").equals(get("LINKNODEID"))) return false;

        return true;
    }

    public boolean isAuthorized(User u) {
        return u.IsAuthorized(get("CORPUSID"), new PrintWriter(System.out));
    }

    // 04/14/04 -- use this function to enable NODE security
    public boolean emitXML(PrintWriter out, User u, boolean WithNodeWrap) {
        if (u.IsAuthorized(get("CORPUSID"), new PrintWriter(System.out))) {
            if (WithNodeWrap) { out.println("<NODE>"); }

            if ( htn != null ) {
                Enumeration enumCustomColnames = htn.keys();
                while ( enumCustomColnames.hasMoreElements() )
                {
                    String sColName = (String) enumCustomColnames.nextElement();

                    // special case: switch node and linknode
                    if (sColName.equals("NODEID")) { out.println("   <LINKNODEID>"+htn.get(sColName)+"</LINKNODEID>"); }
                    else if (sColName.equals("LINKNODEID")) { out.println("   <NODEID>"+htn.get(sColName)+"</NODEID>"); }
                    else { out.println("   <"+sColName+"><![CDATA[" + htn.get(sColName) + "]]></"+sColName+">"); }
                }
            }

            if (WithNodeWrap) { out.println("</NODE>"); }

            return true;
        }
        return false;
    }

    public static String getSQL(Connection dbc) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("NODE", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            if (loop == 1) sSQL = sSQL + "n." + inf.getName();
            else sSQL = sSQL + ", n." + inf.getName();
        }

        sSQL = sSQL + " from Node n";

        return sSQL;
    }
    public static String getSQLwithoutTable(Connection dbc) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("NODE", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            if (loop == 1) sSQL = sSQL + "n." + inf.getName();
            else sSQL = sSQL + ", n." + inf.getName();
        }

        return sSQL;
    }
    public static String getSQL(Connection dbc, String dateformat) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("NODE", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            String sField = "n."+inf.getName();
            if (inf.getType().equals("DATE")) {
                sField = "to_char("+sField+", '"+dateformat+"')";
            }

            if (loop == 1) sSQL = sSQL + "n." + sField;
            else sSQL = sSQL + ", n." + sField;
        }

        sSQL = sSQL + " from Node n";

        return sSQL;
    }
    public static String getSQLwithoutTable(Connection dbc, String dateformat) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("NODE", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            String sField = "n."+inf.getName();
            if (inf.getType().equals("DATE")) {
                sField = "to_char("+sField+", '"+dateformat+"')";
            }

            if (loop == 1) sSQL = sSQL + "n." + sField;
            else sSQL = sSQL + ", n." + sField;
        }

        return sSQL;
    }
    public static String getGroupBy(Connection dbc) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("NODE", dbc);

        String sSQL = "group by "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            if (loop == 1) sSQL = sSQL + "n." + inf.getName();
            else sSQL = sSQL + ", n." + inf.getName();
        }

        return sSQL;
    }

    // accessor functions
    public Vector GetChildren() { return vChildNodes; }
    public Vector getSignatures() { return vSignatures; }

    // Add a child node relationship to this node
	public Vector AddChild(int NodeID) { return AddChild(NodeID+""); }
	public Vector AddChild(String NodeID) {
		vChildNodes.addElement(NodeID);

		return vChildNodes;
	}

	// Remove a child node relationship from this node
	public Vector RemoveChild(int NodeID) { return RemoveChild(NodeID+""); }
	public Vector RemoveChild(String NodeID) {
		vChildNodes.removeElement(NodeID);

		return vChildNodes;
	}

    public void setSignatures(String Value) {
        // sample: abdominal||29,abdominal ultrasound||12,aneurysm||6,aortic aneurysm||5,b-mode||4,doppler||6,echoes||5,imaging||12,ionizing||4
        String[] SigFreqPairs = Value.split(",");
        for (int i = 0; i < SigFreqPairs.length; i++) {
            String[] WordFreq = SigFreqPairs[i].split("\\|\\|");
            try {
                Signature s = new Signature(WordFreq[0], new Double(WordFreq[1]).doubleValue());
                vSignatures.add(s);
            } catch (Exception e) {
                System.out.println("* WARNING* Could not extract signature from: "+SigFreqPairs[i]);
                e.printStackTrace(System.err);
            }
        }
    }

    // accessor function -- 5/13/2004 only one accessor function required
    public String get(String fieldName) {
        String s = (String) htn.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }
        return s.replaceAll(" <br> ", "\n");
    }
    public void set(String Key, String Value) {
        if (htn == null) htn = new Hashtable();

        if (Key.equals("ID")) { htn.put("NODEID", Value); } // some of these values are required by the SAX parser
        else if (Key.equals("BASENAMESTRING")) { htn.put("NODETITLE", Value); }
        else if (Key.equals("INSTANCEOF")) { htn.put("CORPUSID", Value); }
        else if (Key.equals("SIGNATURES")) { setSignatures(Value); }
        else { htn.put(Key, Value); }
    }

    public Hashtable getHash() { return htn; }

    // accessor functions with encoding
    public String getEncodedField(String fieldName) throws Exception {
        return URLEncoder.encode(get(fieldName), "UTF-8"); }

    public String toString() {
        String s = get("NODETITLE"); if (s == null) return super.toString();
        return s;
    }
    public int compareTo(Object o) {
        String s = get("NODETITLE"); if (s == null) return super.toString().compareTo(o.toString());
        return s.compareTo(o.toString());
    }

    public Object clone() {
        Node n = new Node(htn);

        return n;
    }
}
