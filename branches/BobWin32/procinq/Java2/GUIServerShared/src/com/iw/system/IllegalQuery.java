/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Dec 15, 2003
 * Time: 10:22:21 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.system;

public class IllegalQuery extends Exception {
    public IllegalQuery (String message) {
        super(message);
    }
    public IllegalQuery () {
        super();
    }
}
