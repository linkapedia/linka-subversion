package com.iw.system;

/**
 *
 * @author andres
 */
public class ParamsQueryReport {

    private String lessSignature;
    private String lessMusthave;
    private String moreSignature;
    private String moreMusthave;

    public String getLessSignature() {
        return lessSignature;
    }

    public void setLessSignature(String lessSignature) {
        this.lessSignature = lessSignature;
    }

    public String getLessMusthave() {
        return lessMusthave;
    }

    public void setLessMusthave(String lessMusthave) {
        this.lessMusthave = lessMusthave;
    }

    public String getMoreSignature() {
        return moreSignature;
    }

    public void setMoreSignature(String moreSignature) {
        this.moreSignature = moreSignature;
    }

    public String getMoreMusthave() {
        return moreMusthave;
    }

    public void setMoreMusthave(String moreMusthave) {
        this.moreMusthave = moreMusthave;
    }
}