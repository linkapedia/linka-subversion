
package com.iw.guiservershared.externalexplain;

import com.iw.guiservershared.xmlserializejava.IXMLBeanSerializable;

import java.util.Vector;
import java.util.Enumeration;

public abstract class ExplainDisplayDataInstance implements IXMLBeanSerializable
{
    protected String sClassPath= null;
    protected String sCalloutName = null;
    protected String sTabName = null;
    protected String sDescription = null;
    protected String sTooltip = null;
    protected String sRenderAs = null;
    protected Integer IReportPage = null;

    public ExplainDisplayDataInstance (){};
    public void setFromSerializableForm (Object oThisInSerializedForm ) throws Exception
    {
        throw new Exception ("this method must be overridden by subclasses");
    }
    public Object getSerializableForm ()
    {
        return null;
    }
    public String getTabName ()
    {
        return sTabName;
    }

    public String getTooltip()
    {
        return sTooltip;
    }
    public String getDescription()
    {
        return sDescription;
    }
    public void setDescription(String sDescription)
    {
        this.sDescription = sDescription;
    }
    public String getRenderAs()
    {
        return sRenderAs;
    }
    public String getXMLVisualDebug ( int iDepthRecursionForIndent ) throws Exception
    {
        throw new Exception ("must be overridden");
    }
    public boolean bValidateDeserializableForm (Object o) throws Exception
    {return true;}

    protected Object[][] buildoArrArrFromVector ( Vector vOArrDataRows, int iColCount )
    {
        Object[][] oArrArrRet = new Object[vOArrDataRows.size()][iColCount];
        Enumeration enumRows = vOArrDataRows.elements();
        int i = 0;
        while ( enumRows.hasMoreElements())
        {
            oArrArrRet[i] = (Object[]) enumRows.nextElement();
            i++;
        }
        return oArrArrRet;
    }

}
