package com.iw.system;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author andres
 */
public class ModelMusthaveGateReport {

    private String nodeId;
    private String nodeTitle;

    @XmlElement(type = String.class)
    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @XmlElement(type = String.class)
    public String getNodeTitle() {
        return nodeTitle;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }
}
