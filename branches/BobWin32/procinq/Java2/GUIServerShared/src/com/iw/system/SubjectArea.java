package com.iw.system;

import java.util.Vector;

public class SubjectArea implements Comparable {
    // attributes
    private String SubjectAreaID;
    private String SubjectAreaName;
    private boolean SubjectAreaActive = false;
    private boolean bModified = false;

    // constructor(s)
    public SubjectArea () { }
    public SubjectArea (HashTree ht) {
        SubjectAreaID = (String) ht.get("SUBJECTAREAID");
        SubjectAreaName = (String) ht.get("SUBJECTNAME");
        String Status = (String) ht.get("SUBJECTAREASTATUS");

        if (Status.equals("1")) { SubjectAreaActive = true; }
    }

    public SubjectArea (Vector v) {
        SubjectAreaID = (String) v.elementAt(0);
        SubjectAreaName = (String) v.elementAt(1);
        String Status = (String) v.elementAt(1);

        if (Status.equals("1")) { SubjectAreaActive = true; }
    }

    // accessor functions
    public String getID() { return SubjectAreaID; }
    public String getName() { return SubjectAreaName; }
    public boolean getActive() { return SubjectAreaActive; }
    public boolean isModified() { return bModified; }

    public void setID(String ID) { SubjectAreaID = ID; }
    public void setName(String Name) { SubjectAreaName = Name; }
    public void setActive(boolean Active) { SubjectAreaActive = Active; }
    public void setModified(boolean Modified) { bModified = Modified; }

    public String toString() { return SubjectAreaName; }
    public int compareTo(Object o) { return SubjectAreaName.toLowerCase().compareTo(o.toString().toLowerCase()); }
}
