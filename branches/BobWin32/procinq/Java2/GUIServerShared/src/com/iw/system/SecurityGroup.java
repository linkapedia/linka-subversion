package com.iw.system;

import java.util.Vector;
import java.sql.ResultSet;

public class SecurityGroup implements Comparable {
    // attributes
    private String ID;
    private String Name;
    private String Access;

    // constructor(s)
    public SecurityGroup () { }
    public SecurityGroup (HashTree ht) {
        ID = (String) ht.get("GROUPID");
        Name = (String) ht.get("GROUPNAME");
        Access = (String) ht.get("ACCESS");
    }

    public SecurityGroup (Vector v) {
        ID = (String) v.elementAt(0);
        Name = (String) v.elementAt(1);
        Access = (String) v.elementAt(1);
    }

    public SecurityGroup (ResultSet rs) throws Exception {
        ID = rs.getString(1);
        Name = rs.getString(2);
        Access = "1";
    }

    // accessor functions
    public String getID() { return ID; }
    public String getName() { return Name; }
    public String getAccess() { return Access; }

    public void setID(String ID) { this.ID = ID; }
    public void setName(String Name) { this.Name = Name; }
    public void setAccess(String Access) { this.Access = Access; }

    public String toString() { return Name; }
    public int compareTo(Object o) { return Name.toLowerCase().compareTo(o.toString().toLowerCase()); }
}
