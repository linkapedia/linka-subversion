package com.iw.system;

import java.io.IOException;

import org.xml.sax.*;
import org.dom4j.*;

public class InvokeHandler
        implements ElementHandler {

    public int Size = 0;

    public InvokeHandler () {
        super();
    }

    public void startElement(String uri, String localName,
                             String qName, Attributes attributes)
            throws SAXException {

        //System.out.println("startElement: "+qName);
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        System.out.println("endElement: "+qName);
    }

    public void onStart(ElementPath ep) {
        //System.out.println("startElement: "+ep.getPath());
    }

    public void onEnd(ElementPath ep) {
        //System.out.println("endElement: "+ep.getPath());
    }

    public void characters(char ch[], int start, int length)
            throws SAXException {
        // no op
        //System.out.println("characters: "+ch);
    }

    public InputSource resolveEntity(String publicId, String systemId)
// 	throws IOException, SAXException
            throws SAXException {
        System.out.println("Resolve entity: "+publicId+" "+systemId);
        return null;
    }

    public void notationDecl(String name, String publicId, String systemId)
            throws SAXException {
        System.out.println("notation Dec1: "+name+" "+publicId+" "+systemId);
        // no op
    }

    public void unparsedEntityDecl(String name, String publicId,
                                   String systemId, String notationName)
            throws SAXException {
        System.out.println("unparsed Entity Dec1: "+name+" "+publicId+" "+systemId+" "+notationName);
        // no op
    }

    public void setDocumentLocator(Locator locator) {
        System.out.println("set document locator: "+locator.toString());
        // no op
    }

    public void startDocument()
            throws SAXException {
        System.out.println("start document");
        // no op
    }

    public void endDocument()
            throws SAXException {
        System.out.println("end document");
        // no op
    }

    public void startPrefixMapping(String prefix, String uri)
            throws SAXException {
        System.out.println("start prefix mapping: "+prefix+" "+uri);
        // no op
    }

    public void endPrefixMapping(String prefix)
            throws SAXException {
        System.out.println("end prefix mapping: "+prefix);

        // no op
    }

    public void ignorableWhitespace(char ch[], int start, int length)
            throws SAXException {
        // no op
    }

    public void processingInstruction(String target, String data)
            throws SAXException {
        System.out.println("procesing instruction: "+target+" "+data);
        // no op
    }

    public void skippedEntity(String name)
            throws SAXException {
        System.out.println("skipped entity: "+name);
        // no op
    }

    public void warning(SAXParseException e)
            throws SAXException {
        System.out.println("Warning! ");
        e.printStackTrace(System.out);
        // no op
    }

    public void error(SAXParseException e)
            throws SAXException {
        System.out.println("Error! ");
        e.printStackTrace(System.out);
        // no op
    }

    public void fatalError(SAXParseException e)
            throws SAXException {
        System.out.println("Fatal Error! Line: "+e.getLineNumber());
        e.printStackTrace(System.out);
        throw e;
    }
}

