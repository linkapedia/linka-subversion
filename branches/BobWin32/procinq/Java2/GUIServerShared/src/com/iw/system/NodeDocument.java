package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import org.dom4j.Element;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Iterator;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class NodeDocument extends Node implements Comparable {
    // Combined attributes
    public Hashtable htnd = new Hashtable();

    // constructor(s)
    public NodeDocument(HashTree ht) {
        htnd = new Hashtable();

        Enumeration eN = ht.keys();
        while (eN.hasMoreElements()) {
            String key = (String) eN.nextElement();
            String val = (String) ht.get(key);

            htnd.put(key, val);
        }
    }

    public NodeDocument (Vector v) {
        for (int i = 0; i < v.size(); i++) {
            VectorValue vv;
            try {
                vv = (VectorValue) v.elementAt(i);
                htnd.put(vv.getName(), vv.getValue());
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }
    }
    public NodeDocument (Iterator i) {
        if (htnd == null) htnd = new Hashtable();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            try { htnd.put(e.getName(), e.getText()); }
            catch (Exception ex) { ex.printStackTrace(System.out); }
        }
    }

    public NodeDocument(ResultSet rs) throws Exception {
        ResultSetMetaData rsmd = rs.getMetaData();

        for (int i = 1; i <= rsmd.getColumnCount(); i++)
            if (rs.getString(i) != null)
                htnd.put(rsmd.getColumnName(i), rs.getString(i));
            else htnd.put(rsmd.getColumnName(i), "");    }

    public NodeDocument(Document d) {
        if (d.htd != null) {
            // inherit fields from document
            Enumeration eD = d.htd.keys();
            while (eD.hasMoreElements()) {
                String key = (String) eD.nextElement();
                String val = (String) d.htd.get(key);

                htnd.put(key, val);
            }
        }
    }

    public NodeDocument(Document d, Node n) {

        if (d.htd != null) {
            // inherit fields from document
            Enumeration eD = d.htd.keys();
            while (eD.hasMoreElements()) {
                String key = (String) eD.nextElement();
                String val = (String) d.htd.get(key);

                htnd.put(key, val);
            }
        }

        if (n.htn != null) {
            // inherit fields from node
            Enumeration eN = n.htn.keys();
            while (eN.hasMoreElements()) {
                String key = (String) eN.nextElement();
                String val = (String) n.htn.get(key);

                htnd.put(key, val);
            }
        }
    }

    public Document getDocProps() {
        Document d = new Document();
        d.htd = htnd;

        return d;
    }

    public boolean emitXML(PrintWriter out, User u, boolean WithDocWrap) {
        boolean bAuthorized = isAuthorized(u);

        if (bAuthorized) {
            if (WithDocWrap) { out.println("<NODEDOCUMENT>"); }

            if ( htnd != null ) {
                Enumeration enumCustomColnames = htnd.keys();
                while ( enumCustomColnames.hasMoreElements() )
                {
                    String sColName = (String) enumCustomColnames.nextElement();
                    try { out.println("   <"+sColName+"><![CDATA[" + new String(get(sColName).getBytes("UTF-8"))+ "]]></"+sColName+">"); }
                    catch (Exception e) { e.printStackTrace(System.out); }
                }
            }

            if (WithDocWrap) { out.println("</NODEDOCUMENT>"); }

            return true;
        }
        return false;
    }

    public boolean isAuthorized(User u) {
        boolean bNodeAuthorized = u.IsAuthorized(get("CORPUSID"), new PrintWriter(System.out));
        boolean bDocAuthorized = u.IsAuthorized(get("DOCUMENTSECURITY"));

        if (bNodeAuthorized && bDocAuthorized) return true;

        return false;
    }

    // accessor functions

    // the following three static functions are used to sort NodeDocument vectors by score
    public static void qs(Vector v) {

        Vector vl = new Vector();                      // Left and right sides
        Vector vr = new Vector();
        NodeDocument el;
        int key;                                    // key for splitting

        if (v.size() < 2) return;                      // 0 or 1= sorted

        NodeDocument nd = (NodeDocument) v.elementAt(0);
        key = new Integer(nd.get("SCORE1")).intValue();               // 1st element key

        // Start at element 1
        for (int i = 1; i < v.size(); i++) {
            el = (NodeDocument) v.elementAt(i);
            if (new Integer(el.get("SCORE1")).intValue() < key)
                vr.addElement(el);// Add to right
            else
                vl.addElement(el);                     // Else add to left
        }

        qs(vl);                                        // Recursive call left
        qs(vr);                                        //    "        "  right
        vl.addElement(v.elementAt(0));

        addVect(v, vl, vr);
    }

    // ********************************************************************************
    private static String cleanBadCharacters(String s) {
        if (s == null) {
            return s;
        }
        String sNew = new String();
        for (int k = 0; k < s.length(); k++) {
            int ichar = (int) s.charAt(k);
            if (ichar <= 300) {
                sNew = sNew + s.charAt(k);
            }
        }

        return sNew;
    }

    // Add two vectors together, into a destination Vector
    private static void addVect(Vector dest, Vector left, Vector right) {

        int i;
        dest.removeAllElements();                     // reset destination

        for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
        for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
    }

    // accessor function -- 5/13/2004 only one accessor function required
    public String get(String fieldName) {
        String s = (String) htnd.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }
        return s.replaceAll(" <br> ", "\n");
    }
    public void set(String fieldName, String fieldValue) { htnd.put(fieldName, fieldValue); }

    // accessor functions with encoding
    public String getEncodedField(String fieldName) throws Exception {
        return URLEncoder.encode(get(fieldName), "UTF-8"); }

    public String getDecodedField(String fieldName) throws Exception {
        return URLDecoder.decode(get(fieldName), "UTF-8"); }

    public String toString() {
        String s = get("NODETITLE"); if (s == null) return super.toString();
        return s;
    }
    public int compareTo(Object o) {
        String s = get("NODETITLE"); if (s == null) return super.toString().compareTo(o.toString());
        return s.compareTo(o.toString());
    }
}
