package com.iw.system;

import org.dom4j.Element;

import java.util.Vector;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Henry Kon, Indraweb
 * All rights reserved.
 * Date: Jul 6, 2004
 */

import java.util.Vector;
import java.util.Iterator;
import java.util.Enumeration;

public class Signatures {
    // User attributes
    private Vector vSignatureObjects= new Vector();

    // constructor(s)
    public Signatures (Iterator i) {
        while (i.hasNext()) {
            Element eSignature = (Element) i.next();
            try {
                Signature signatureobject = new Signature ( eSignature );
                vSignatureObjects.addElement (signatureobject);
            }
            catch (Exception ex) { ex.printStackTrace(System.out); }
        }
    }

    public Signatures (Vector vSignature_) {
        Enumeration e = vSignature_.elements();
        while (e.hasMoreElements() ) {
            Signature signature = (Signature) e.nextElement();
            try {
                vSignatureObjects.addElement (signature);
            }
            catch (Exception ex) { ex.printStackTrace(System.out); }
        }
    }

    public Signatures (String[] sigArr) {
        for (int i = 0; i < sigArr.length; i++) {
            Signature signature = new Signature(sigArr[i], 1.0);

            try {
                vSignatureObjects.addElement (signature);
            }
            catch (Exception ex) { ex.printStackTrace(System.out); }
        }
    }

    public Signatures () {
    }

    public void add (Signature signature)
    {
        vSignatureObjects.addElement(signature);
    }

    // accessor functions
    public Vector getVecOfSignatureObjects() { return vSignatureObjects; }

    public int size() { return vSignatureObjects.size(); }
    public Signature elementAt(int num) { return (Signature) vSignatureObjects.elementAt(num); }

    //          <resourceData id="Signatures">1997||1,antibiotic||4,antibiotics||3,antimicrobial||1,confirm||1,consideration||2,covert||1,culpable||1,etiologic||1,favoring||2,fortiori||1,ill gulf war||3,important||2,mycoplasmas||2,organ||1,overgrowth||1,randomized||1,rheumatoid||1,trial||3,tully||2</resourceData>
    public String getTopicMapString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < vSignatureObjects.size(); i++) {
            Signature s = (Signature) vSignatureObjects.elementAt(i);
            if (i != 0) sb.append(",");
            sb.append(s.getWord()+"||"+s.getWeight());
        }
        return sb.toString();
    }

    public String toString()
    {
        return toString(true);

    }
    public String InxightToString()
    {
        return InxightToString(true);

    }

    public String InxightToString(boolean bStripTrailingPointZero)
    {
        return InxightToString(vSignatureObjects, bStripTrailingPointZero);

    }

    public String toString(boolean bStripTrailingPointZero)
    {
        return toString(vSignatureObjects, bStripTrailingPointZero);

    }

    public static String InxightToString (Vector vSignatureObjects, boolean bStripTrailingPointZero )
    {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < vSignatureObjects.size(); i++) {
            if (i > 0) sb.append(" | ");
            Signature s = (Signature) vSignatureObjects.elementAt(i);

            sb.append("NOCASE(\""+s.getWord()+"\")");
            //if (s.getWord().indexOf(" ") != -1) sb.append("NOCASE(\""+s.getWord()+"\")");
            //else sb.append("FT(\""+s.getWord()+"\",OKAPI_BM25,0)");
        }
        return sb.toString();
    }

    public static String toString (Vector vSignatureObjects, boolean bStripTrailingPointZero )
    {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < vSignatureObjects.size(); i++) {
            if (i != 0)
                sb.append("|||");
            Signature s = (Signature) vSignatureObjects.elementAt(i);
            if ( bStripTrailingPointZero ) {
                String sWt = Double.toString(s.getWeight());
                if ( sWt.endsWith(".0"))
                    sWt = sWt.substring( 0, sWt.length()-2 );

                sb.append( sWt + "," + s.getWord());
            } else {
                sb.append( s.getWeight() + "," + s.getWord());
            }
        }
        return sb.toString();
    }
}
