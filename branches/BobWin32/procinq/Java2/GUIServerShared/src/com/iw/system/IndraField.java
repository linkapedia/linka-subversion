package com.iw.system;

import org.dom4j.Element;

import java.util.*;
import java.io.PrintWriter;
import java.sql.ResultSet;

public class IndraField {
    // Combined attributes
    private String fieldName;
    private String dataType;
    private int dataLength;

    // constructor(s)
    public IndraField() { }
    public IndraField(String fieldName, String dataType, int dataLength) {
        this.fieldName = fieldName;
        this.dataType = dataType;
        this.dataLength = dataLength;
    }
    public IndraField(Hashtable ht) {
        this.fieldName = (String) ht.get("NAME");
        this.dataType = (String) ht.get("DATATYPE");
        this.dataLength = Integer.parseInt((String) ht.get("LENGTH"));
    }
    public IndraField(Iterator i) {
        while (i.hasNext()) {
            Element e = (Element) i.next();
            String s = e.getName();

            if (s.equals("NAME")) this.fieldName = e.getText();
            if (s.equals("DATATYPE")) this.dataType = e.getText();
            if (s.equals("LENGTH")) this.dataLength = Integer.parseInt(e.getText());
        }
    }

    public String getName() { return fieldName; }
    public String getType() { return dataType; }
    public int getLength() { return dataLength; }
}
