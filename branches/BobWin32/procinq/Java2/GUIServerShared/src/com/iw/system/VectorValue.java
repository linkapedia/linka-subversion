package com.iw.system;

import java.util.*;
import java.io.*;

public class VectorValue {
	private VectorTree vParent;
    private String name;
    private String value;

	// Accessor and set methods for new Hashtable attribute, htParent..
	public VectorTree GetParent() { return vParent; }
    public String getName() { return name; }
    public String getValue() { return value; }

	public void SetParent(VectorTree vParent) {
		this.vParent = vParent;
	}

	// Another constructor
	public VectorValue () { value = ""; }
    public VectorValue (String name, String value) { this.name = name; this.value = value; }
}
