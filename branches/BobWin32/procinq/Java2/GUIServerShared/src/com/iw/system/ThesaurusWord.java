package com.iw.system;

import java.util.Vector;

public class ThesaurusWord implements Comparable {
    // User attributes
    private String WordID;
    private String WordName;
    private String DisplayName;
    private String ThesaurusName;
    private int Relationship = -1;

    // constructor(s)
    public ThesaurusWord (HashTree ht) {
        if (ht.containsKey("WORDID")) {
            WordID = (String) ht.get("WORDID");
            WordName = (String) ht.get("WORDNAME");
        } else if (ht.containsKey("OBJECTID")) {
            WordID = (String) ht.get("OBJECTID");
            WordName = (String) ht.get("OBJECTNAME");
            Relationship = new Integer((String) ht.get("RELATIONSHIPCODE")).intValue();
        }
        DisplayName = WordName+"    ";
    }

    public ThesaurusWord (Vector v) {
        WordID = (String) v.elementAt(0);
        WordName = (String) v.elementAt(1);
        DisplayName = WordName+"    ";
    }

    // accessor functions
    public String getID() { return WordID; }
    public String getName() { return WordName; }
    public int getRelationship() { return Relationship; }
    public String getThesName() { return ThesaurusName; }

    public void setID(String ID) { WordID = ID; }
    public void setThesaurusName(String Name) { ThesaurusName = Name; }
    public void setName(String Name) { WordName = Name; }
    public void setRelationship(int Relat) { Relationship = Relat; }
    public int compareTo(Object o) { return WordName.compareTo(o.toString()); }
    public String toString() { return DisplayName; }
}
