package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class MarkLogicOut extends ExportElement {
    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select all nodes in the taxonomy that contain signatures, and provide the full
          node context in the label.
        */

        // select root node
        File f = new File(c.getName().replaceAll(" ", "_")+"-marklogic.xml");
        if (f.exists()) f.delete();
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        File f2 = new File(c.getName().replaceAll(" ", "_")+"-marklogic-training.xml");
        if (f2.exists()) f2.delete();
        FileOutputStream fos2 = new FileOutputStream(f2);
        Writer out2 = new OutputStreamWriter(fos2, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2006 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<marklogic-taxonomy>\n");

        // *** WRITE SAMPLE HEADER HERE
        out2.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out2.write("<!-- "+c.getName()+" Taxonomy Training Set, Copyright 2006 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out2.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out2.write("<training-set>\n");

        // *** LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, c, out, out2);

        out.write("</marklogic-taxonomy>\n");
        out2.write("</training-set>\n");

        out.close();
        out2.close();

        return f;
    }

    private void buildChildren(ITS server, Corpus c, Writer out, Writer out2) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+c.getID(), 1, 50000);
        if (vNodes.size() < 1) return;

        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            if ((vSignatures.size() > 0) && (n.get("NODEID").equals(n.get("LINKNODEID")))) {
                Signatures signatures = new Signatures(vSignatures);
                writeSample(n, vSignatures, out2);

                out.write("   <cts:label name=\""+n.get("NODEID")+"\">\n");
                out.write("      <cts:class name=\""+n.get("NODETITLE")+"\" />\n");
                out.write("   </cts:label>\n");
            }

            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }
        }
    }

    private void writeSample(ITSTreeNode n, Vector signatures, Writer out2) throws Exception {
        if (signatures.size() == 0) throw new Exception("This topic has no signatures.");

        out2.write("   <terms name=\""+n.get("NODEID")+"\">\n");
        try {
            // calculate the filler frequency
            double totalwordfreq = 0;
            for (int i = 0; i < signatures.size(); i++) {
                Signature s = (Signature) signatures.elementAt(i);
                totalwordfreq = totalwordfreq + s.getWeight();
            }

            double nodesize = Double.parseDouble(n.get("NODESIZE"));
            double fillweight = nodesize - totalwordfreq;
            if (fillweight < 0) fillweight = 0;

            signatures.add(0, new Signature("filler", fillweight));

            double outer = 0; double total = 0;

            // loop through the signatures and write them
            while (total < nodesize) {
                outer++;
                for (int i = 0; i < signatures.size(); i++) {
                    Signature s = (Signature) signatures.elementAt(i);
                    //System.out.println("**DEBUG** signature: "+s.getWord()+" weight: "+s.getWeight()+" outer: "+outer);
                    if (s.getWeight() >= outer) { out2.write(s.getWord()+" "); total++; }
                }

                if (total < nodesize) {
                    outer++;
                    for (int i = signatures.size()-1; i > -1; i--) {
                        Signature s = (Signature) signatures.elementAt(i);
                        if (s.getWeight() >= outer) { out2.write(s.getWord()+" "); total++; }
                    }
                }
            }

            out2.write("   </terms>\n");
        } catch (Exception e) { e.printStackTrace(System.err); throw e; }
    }

    private File createOrGetFile(String path) throws Exception {
        File f = new File(path);
        if (!f.exists()) f.mkdir();

        return f;
    }


    private String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method

    public static String getEncodedName(String s) {
        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        // s = s.replaceAll("\"", "&quot;");
        // inxight doesnt want to have these encoded
        s = s.replaceAll(">", "&gt;");
        s = s.replaceAll("'", "&apos;");

        return s;
    }
}