package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class UltraseekOut extends ExportElement {
    public String corpusID = "";
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable nodes = new Hashtable();
    public Hashtable usedNames = new Hashtable();

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {

        corpusID = c.getID();
        if (ppbin != null) this.ppb = ppbin;

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { throw new Exception("No root node found for corpus "+corpusID); }

        // open two files: the .otl file for the topic definitions and a .tax file to create the hierarchy
        File fotl = new File(c.getName().replaceAll(" ", "_")+".xml");
        if (fotl.exists()) { fotl.delete(); }

        FileOutputStream foso = new FileOutputStream(fotl);
        Writer outo = new OutputStreamWriter(foso, "UTF8");

        outo.write("<?xml version='1.0' encoding='UTF-8'?>\r\n");
        outo.write("<!DOCTYPE UltraseekTopics [\r\n");
        outo.write("<!ELEMENT UltraseekTopics (creator, date, location, subject*)>\r\n");
        outo.write("<!ELEMENT creator    (#PCDATA)>\r\n");
        outo.write("<!ELEMENT date       (#PCDATA)>\r\n");
        outo.write("<!ELEMENT location   (#PCDATA)>\r\n");
        outo.write("<!ELEMENT subject    (id, name, superTopic?,\r\n");
        outo.write("                     ((blurb?, keywords?, url*, rule*)|xref))>\r\n");
        outo.write("<!ELEMENT name       (#PCDATA)>\r\n");
        outo.write("<!ELEMENT id         (#PCDATA)>\r\n");
        outo.write("<!ELEMENT superTopic (#PCDATA)>\r\n");
        outo.write("<!ELEMENT blurb	     (#PCDATA)>\r\n");
        outo.write("<!ELEMENT keywords   (#PCDATA)>\r\n");
        outo.write("<!ELEMENT url        (#PCDATA)>\r\n");
        outo.write("<!ELEMENT rule       (required+, forbidden*)>\r\n");
        outo.write("<!ELEMENT required   (#PCDATA)>\r\n");
        outo.write("<!ELEMENT forbidden  (#PCDATA)>\r\n");
        outo.write("<!ELEMENT xref       (#PCDATA)>\r\n");
        outo.write("<!ATTLIST UltraseekTopics version   NMTOKEN  #IMPLIED>\r\n");
        outo.write("<!ATTLIST subject         xml:lang  NMTOKEN  #IMPLIED>\r\n");
        outo.write("<!ATTLIST url             stars     NMTOKEN  #IMPLIED>\r\n");
        outo.write("]>\r\n");
        outo.write("<!-- "+c.getName()+" Taxonomy, Copyright 2006 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. --> \r\n");
        outo.write("<!--" + c.getDescription() + "-->\r\n");
        outo.write("<UltraseekTopics version=\"3.1\">\r\n");
        outo.write("<creator>intellisophic</creator>\r\n");
        outo.write("<date>2005-08-23T20:09:03Z</date>\r\n");
        outo.write("<location>intranetts.global.som.com:8765</location>\r\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        writeNode(rootNode, server, outo);

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, outo);

        outo.write("</UltraseekTopics>\r\n");

        outo.close();

        return fotl;
    }

    private void writeNode(ITSTreeNode n, ITS server, Writer outo) throws Exception {
        Vector vSignatures = server.getNodeSignatures(n);

        n.set("NODETITLE", ((String) n.get("NODETITLE")).replaceAll(" & ", " &amp; "));

        if (((String) n.get("PARENTID")).equals("-1")) n.set("PARENTID", "0");
        outo.write("<subject>\r\n");
        outo.write("   <id>"+ReplaceXMLChars(n.get("NODEID"))+"</id>\r\n");
        outo.write("   <name>"+ReplaceXMLChars(n.get("NODETITLE"))+"</name>\r\n");
        outo.write("   <superTopic>"+ReplaceXMLChars(n.get("PARENTID"))+"</superTopic>\r\n");
        if (vSignatures.size() > 0) {
            outo.write("   <keywords>"+buildSigs(vSignatures)+"</keywords>\r\n");
        }
        outo.write("   <rule><required>"+
                   returnWords(ReplaceXMLChars(n.get("NODETITLE")))+
                   mustHave(n, server)+
                   "</required></rule>\r\n");
        outo.write("</subject>\r\n");

        nodesProcessed++;
        if (ppb != null) {
            long perc = (nodesProcessed*100) / totalNodes;
            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
        }
    }

    public String mustHave(Node n, ITS server) {
        StringBuffer sb = new StringBuffer();
        Vector vHave = new Vector();
        try { vHave = server.getMusthaves(n.get("NODEID")); }
        catch (Exception e) { e.printStackTrace(System.err); }

        for (int i = 0; i < vHave.size(); i++) {
            if (n.get("NODETITLE").toLowerCase().indexOf(((String) vHave.elementAt(i)).toLowerCase()) < 0) {
                String s = ReplaceXMLChars((String) vHave.elementAt(i)).toLowerCase();
                sb.append(",");
                if (s.indexOf(" ") > -1) sb.append("\"");
                sb.append(s);
                if (s.indexOf(" ") > -1) sb.append("\"");
            }
        }

        return sb.toString();
    }

    public String buildSigs(Vector v) {
        int words = 0;
        String keywords = "";

        for (int i = 0; i < v.size(); i++) {
            Signature s = (Signature) v.elementAt(i);
            if (!commonWords.containsKey(s.getWord().toLowerCase())) {
                words++; if (words > 1) keywords = keywords + ",";
                String word = ReplaceXMLChars(s.getWord());
                if (word.indexOf(" ") > -1) keywords = keywords + "\"";
                keywords = keywords + word;
                if (word.indexOf(" ") > -1) keywords = keywords + "\"";
            }
        }

        return keywords;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer outo) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() > 0) {
            for (int i = 0; i < vNodes.size(); i++) {
                ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
                writeNode(n, server, outo);

                buildChildren(server, n, outo);
            }
        }
        //int depth = new Integer(p.get("DEPTHFROMROOT")).intValue();
        //for (int i = 0; i < depth; i++) { outo.write(" "); }
    }

    private String returnWords(String Phrase) {
        StringBuffer sb = new StringBuffer("");

        String[] sArr = Phrase.split(" ");

        int words = 0;

        if (Phrase.indexOf(" ") > -1) sb.append("\"");
        sb.append(Phrase.toLowerCase());
        if (Phrase.indexOf(" ") > -1) sb.append("\"");

        return sb.toString();
    }

    private static String ReplaceXMLChars (String s){
    	 s = s.replaceAll("&amp;", "&");
         s = s.replaceAll("&", "&amp;");
         s = s.replaceAll("<", "&lt;");
         s = s.replaceAll(">", "&gt;");

         if (s.indexOf(" ") == -1) {
             s = s.replaceAll("\"", "&quot;");
             s = s.replaceAll("'", "&apos;");
         }

         return s;
    	
    }
    
    private static String replaceNonAlpha (String s) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (Character.isLetterOrDigit(chars[i])) buffer.append(chars[i]);
            else buffer.append("-");
        }
        return buffer.toString();
    }
}