package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;
import java.util.regex.*;

public class InxightNameCatalogOut extends ExportElement {
    public ITSTreeNode rootNode = null;
    public String corpusID = null;
    private DecimalFormat twoDigits = new DecimalFormat("0");

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception 
    {
        corpusID = c.getID();

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*  Here's the drill: Select the parent node in this taxonomy.   
         	From there, proceed to do a depth-first traverse throughout the taxonomy until all nodes are handled.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { System.err.println("No root node found for corpus "+corpusID); return null; }

        File f = new File(c.getName().replaceAll(" ", "").replaceAll("-","")+"_IXTNameCatalog.xml");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<!-- "+c.getName()+" Taxonomic Content, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<catalog> name=\""+c.getName()+"\"\n");
        
        //*** WRITE ROOT NODE INFORMATION HERE
        out.write("<entity_category name=\""+InxightEncode(rootNode.get("NODETITLE").trim().toLowerCase())+"\">\n");
        
        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, out);

        out.write("</entity_category>\n");
        out.write("</catalog>");

        out.close();

        return f;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer out) throws Exception 
    {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() < 1) return;

        for (int i = 0; i < vNodes.size(); i++) 
        {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);

            System.out.println("**Exporting topic: " + n.get("NODETITLE"));

            // Get Must Have Terms for this Node
            Vector vHave = new Vector();
            vHave = server.getMusthaves(n.get("NODEID"));

            if (vHave.size()>0) 
            {
            	//Must have terms are variants - Only write entity if has must haves!
            	out.write("<entity_name canonical=\""+InxightEncode(n.get("NODETITLE").trim().toLowerCase())+"|"+n.get("NODEID")+"\">\n");
            	
              	for (int j = 0; j < vHave.size(); j++) 
                	out.write("<variant name\""+InxightEncode(vHave.elementAt(j).toString().trim().toLowerCase())+"\" type=\"\"/>\n");
             	
             	out.write("</entity_name>\n");
            }
            
            if (n.get("NODEID").equals(n.get("LINKNODEID")))	//get this nodes children
            {
            	buildChildren(server, n, out);
            }

            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }

        }
    }

     private String InxightEncode (String s){
       	s=s.toLowerCase();
       	s=s.replaceAll("[|]", "");
       	s=s.replaceAll("[�]", "");
       	s=s.replaceAll("[�]", "");
       	s=s.replaceAll("[*]", "");
       	s=s.replaceAll("[=]", "");
       	s=s.replaceAll("[?]", "");
       	s=s.replaceAll("[:]", "");
       	s=s.replaceAll("[(]", "");
       	s=s.replaceAll("[)]","");
       	s=s.replaceAll("\"","");
       	s=s.replaceAll("&lt","<");
       	s=s.replaceAll("&gt",">");
        s=s.replaceAll("&amp;", "&");
        s=s.replaceAll("&apos;", "'");
        s=s.replaceAll("&", "&amp;");
        s=s.replaceAll("<", "&lt;");
        s=s.replaceAll(">", "&gt;");

        if (s.indexOf(" ") == -1) {
            s=s.replaceAll("\"", "&quot;");
            s=s.replaceAll("'", "&apos;");
        }

 /*       	if (s.equals("window")){
        		s="'window'";
        	}
        	if (s.equals("count")){
        		s="'count'";
        	}
        	if (s.equals("sentence")){
        		s="'sentence'";
        	}
        	if (s.equals("paragraph")){
        		s="'paragraph'";
        	}
        	if (s.equals("stem")){
        		s="'stem'";
        	}
        	if (s.equals("pos")){
        		s="'pos'";
        	}
        	if (s.equals("min")){
        		s="'min'";
        	}
        	if (s.equals("max")){
        		s="'max'";
        	}
        	if (s.equals("prob")){
        		s="'prob'";
        	}
        	if (s.equals("case")){
        		s="'case'";
        	}
        	if (s.equals("word")){
        		s="'word'";
        	}
        	if (s.equals("sent")){
        		s="'sent'";
        	}
        	if (s.equals("prior")){
        		s="'prior'";
        	}
        	if (s.equals("para")){
        		s="'para'";
        	}
*/

      	return s;
    }
}
