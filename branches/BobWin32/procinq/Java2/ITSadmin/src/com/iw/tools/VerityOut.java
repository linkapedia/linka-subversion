package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class VerityOut extends ExportElement {
    public String corpusID = "";
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable nodes = new Hashtable();
    public Hashtable usedNames = new Hashtable();

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {

        corpusID = c.getID();
        if (ppbin != null) this.ppb = ppbin;

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { throw new Exception("No root node found for corpus "+corpusID); }

        // open two files: the .otl file for the topic definitions and a .tax file to create the hierarchy
        File fotl = new File(c.getName().replaceAll(" ", "_")+".otl");
        if (fotl.exists()) { fotl.delete(); }

        FileOutputStream foso = new FileOutputStream(fotl);
        Writer outo = new OutputStreamWriter(foso, "UTF8");

        File ftax = new File(c.getName().replaceAll(" ", "_")+".tax");
        if (ftax.exists()) { ftax.delete(); }

        FileOutputStream fost = new FileOutputStream(ftax);
        Writer outt = new OutputStreamWriter(fost, "UTF8");

        // *** WRITE THE HEADER HERE
        outo.write("$control: 1\r\n");
        outo.write("# "+c.getName()+" Taxonomy, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. \r\n");
        outo.write("# Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. \r\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        writeNode(rootNode, server, outo, outt);

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, outo, outt);

        outo.write("# End Of File\r\n");

        outo.close();
        outt.close();

        return fotl;
    }

    private void writeNode(ITSTreeNode n, ITS server, Writer outo, Writer outt) throws Exception {
        int depth = new Integer(n.get("DEPTHFROMROOT")).intValue();
        outo.write("# Beginning of "+n.get("NODETITLE")+" topic\r\n");

        String cattitle = replaceNonAlpha(n.get("NODETITLE").toLowerCase());
        int loop = 0;
        while (usedNames.containsKey(cattitle)) {
            loop++; cattitle = replaceNonAlpha(n.get("NODETITLE").toLowerCase())+loop;
        }
        n.set("CATTITLE", cattitle); usedNames.put(cattitle, "used");

        for (int i = 0; i < depth; i++) { outo.write("*"); } if (depth > 0) outo.write(" ");
        outo.write(n.get("CATTITLE")+" ACCRUE\r\n");
        outo.write("  /annotation = \""+n.get("NODETITLE")+".\"\r\n");

        ITSTreeNode parent = null;
        if (!n.get("PARENTID").equals("-1")) parent = (ITSTreeNode) nodes.get(n.get("PARENTID"));

        nodes.put(n.get("NODEID"), n);

        outt.write("category: "+cattitle+"\r\n");
        outt.write("Name: "+n.get("NODETITLE")+"\r\n");
        outt.write("Created: "+n.get("DATESCANNED")+"\r\n");
        outt.write("Modified: "+n.get("DATEUPDATED")+"\r\n");
        if (parent != null) outt.write("primary-parent: "+parent.get("CATTITLE")+"\r\n");
        outt.write("\r\n");

        Vector vSignatures = server.getNodeSignatures(n);

        if (vSignatures.size() > 0) {
            /*
            for (int i = 0; i < depth; i++) { out.write("*"); }
            out.write("* ");
            out.write("1.0 WORD\r\n");
            out.write("  /wordtext = \""+n.get("NODETITLE")+"\"\r\n");
            */
            double totalwords = 0.0;
            for (int j = 0; j < vSignatures.size(); j++) {
                Signature s = (Signature) vSignatures.elementAt(j);
                totalwords = totalwords + s.getWeight();
            }

            for (int j = 0; j < vSignatures.size(); j++) {
                Signature s = (Signature) vSignatures.elementAt(j);
                String[] words = s.getWord().split(" ");

                for (int i = 0; i < depth; i++) { outo.write("*"); }
                outo.write("* ");
                // scale the weight between 0.0 and 1.0
                // FREQ/NODESIZE = X .. 100F = N*X;
                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(2);

                double scaledWeight = s.getWeight()/totalwords;

                outo.write(df.format(scaledWeight)+" WORD\r\n");
                if (words.length == 1) { outo.write("  /wordtext = "+s.getWord()+"\r\n"); }
                else {
                    for (int i = 0; i < depth; i++) { outo.write("*"); }
                    outo.write("* <many><phrase>\r\n");
                    for (int i = 0; i < words.length; i++) {
                        for (int k = 0; k < depth; k++) { outo.write("*"); }
                        outo.write("** <many><stem>\r\n");
                        outo.write("  /wordtext = "+words[i]+"\r\n");
                    }
                }
            }
        }
        nodesProcessed++;
        if (ppb != null) {
            long perc = (nodesProcessed*100) / totalNodes;
            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
        }
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer outo, Writer outt) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() > 0) {
            for (int i = 0; i < vNodes.size(); i++) {
                ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
                writeNode(n, server, outo, outt);

                buildChildren(server, n, outo, outt);
            }
        }
        //int depth = new Integer(p.get("DEPTHFROMROOT")).intValue();
        //for (int i = 0; i < depth; i++) { outo.write(" "); }
    }

    private static String replaceNonAlpha (String s) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (Character.isLetterOrDigit(chars[i])) buffer.append(chars[i]);
            else buffer.append("-");
        }
        return buffer.toString();
    }
}