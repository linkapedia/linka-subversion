package com.iw.tools;

import java.net.URL;

public class ManageResource {

	public static URL getResource(String resource)throws Exception{
		URL url = ManageResource.class.getClass().getResource(resource);
		return url;
	}
}
