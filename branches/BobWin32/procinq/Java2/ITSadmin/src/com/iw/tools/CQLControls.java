package com.iw.tools;

import com.iw.system.IllegalQuery;

import java.util.*;
import java.util.regex.*;

public class CQLControls {
    // attributes: these are the CQL attributes assuming an advanced search
    // query.  this query builder will not work for more complex queries.
    public String query = null;
    public String Language = null;
    public String Title = null;
    public String Abstract = null;
    public String Bibliography = null;
    public String FullText = null;
    public String Keywords = null;
    public String Outdated = null;
    public String OrderBy = null;
    public String Region = null;

    boolean bUseThesauri = false;

    Hashtable StopHash = new Hashtable();

    // constructor method
    public CQLControls() {
        InitializeStopWords();
    }

    public CQLControls(String CQL) {
        query = CQL;
        Matcher m;

        // example complex query:
        //
        // SELECT <IDRACDOCUMENT> WHERE
        //( FULLTEXT LIKE 'test' and FULLTEXT LIKE 'test' or
        //  FULLTEXT LIKE 'test' and FULLTEXT LIKE 'test') AND
        //( DOCTITLE LIKE 'test' and DOCTITLE LIKE 'test' or
        //  DOCTITLE LIKE 'test' and DOCTITLE LIKE 'test') AND
        //( BIBLIOGRAPHY LIKE 'test' and BIBLIOGRAPHY LIKE 'test' or
        //  BIBLIOGRAPHY LIKE 'test' and BIBLIOGRAPHY LIKE 'test') AND
        //( ABSTRACT LIKE 'test' and ABSTRACT LIKE 'test' or
        //  ABSTRACT LIKE 'test' and ABSTRACT LIKE 'test')
        // ORDER BY IDRACNUMBER ASC

        // 1) Extract all information between WHERE and ORDER BY
        Pattern r = null; boolean bResult = false;
        if (query.toLowerCase().indexOf("order by") != -1) {
            r = Pattern.compile("ORDER BY (.*)", Pattern.CASE_INSENSITIVE);
            m = r.matcher(query);
            bResult = m.find();
            OrderBy = (String) m.group(1).toString();

            r = Pattern.compile(" WHERE (.*)ORDER BY", Pattern.CASE_INSENSITIVE);
        } else {
            r = Pattern.compile(" WHERE (.*)", Pattern.CASE_INSENSITIVE);
        }
        m = r.matcher(query);
        bResult = m.find();
        query = (String) m.group(1).toString();

        // 2) Break first level of parenthesis into FOREACH:
        char[] cArr = new char[query.length() + 1];
        ArrayList aParen = new ArrayList();

        cArr = query.toCharArray();
        int pos = 0;

        // first we need to extract the locations of all parenthesis in the query String
        while ((query.indexOf("(", pos) != -1) || (query.indexOf(")", pos) != -1)) {
            int i = query.indexOf("(", pos);
            int j = query.indexOf(")", pos);
            if ((i < j) && (i != -1)) {
                aParen.add(i+"");
                pos = i + 1;
            } else {
                aParen.add(j+"");
                pos = j + 1;
            }
        }

        // now, pair off the top level of parens into objects to be processed; these
        // are actually individual elements of the query object
        Object[] iParens = aParen.toArray();
        pos = 0;
        int lower = 0;

        for (int i = 0; i < iParens.length; i++) {
            Integer tmplocation = (Integer) iParens[i];
            int location = tmplocation.intValue();
            if (cArr[location] == '(') {
                // if POS was ZERO (0) then this is the lower bounds
                if (pos == 0) {
                    lower = location;
                }
                pos++;
            } else {
                pos = pos - 1;
            }

            // if POS is back to ZERO (0) then we've reached the upper bounds
            if (pos == 0) {
                String sub = query.substring(lower, (location - lower + 1));

                // 3) Remove first level of parenthesis plus added CQL wording
                // i.e. change: ((ABSTRACT LIKE 'a' and ABSTRACT LIKE 'b') or ABSTRACT LIKE 'c')
                //        into: ((a and b) or c) // and save in ABSTRACT field
                if (sub.indexOf("DOCUMENTSUMMARY LIKE") != -1) { // abstract
                    sub = sub.replaceAll("DOCUMENTSUMMARY LIKE ", "");
                    sub = sub.replaceAll("'", "");
                    Abstract = sub;
                } else if (sub.indexOf("FULLTEXT LIKE") != -1) { // full text
                    sub = sub.replaceAll("FULLTEXT LIKE ", "");
                    sub = sub.replaceAll("'", "");
                    FullText = sub;
                } else if (sub.indexOf("BIBLIOGRAPHY LIKE") != -1) { // bibliography
                    sub = sub.replaceAll("BIBLIOGRAPHY LIKE ", "");
                    sub = sub.replaceAll("'", "");
                    Bibliography = sub;
                } else if (sub.indexOf("DOCTITLE LIKE ") != -1) { // doc title
                    sub = sub.replaceAll("DOCTITLE LIKE ", "");
                    sub = sub.replaceAll("'", "");
                    Title = sub;
                } else if (sub.indexOf("LANGUAGE = ") != -1) { // language
                    sub = sub.replaceAll("LANGUAGE = ", "");
                    sub = sub.replaceAll("'", "");
                    Language = sub;
                } else if (sub.indexOf("COUNTRY = ") != -1) { // region
                    sub = sub.replaceAll("COUNTRY = ", "");
                    sub = sub.replaceAll("'", "");
                    Region = sub;
                } else if (sub.indexOf("NODETITLE") != -1) { // nodetitle
                    sub = sub.replaceAll("NODETITLE = ", "");
                    sub = sub.replaceAll("'", "\"");
                    Keywords = sub;
                }
            }
        }
    }

    public void InitializeStopWords() {
        // load the list of interMedia stop words at application start time
        // load STOPHASH here
    }

    // check against our reserved word list to ensure no reserved words
    // are put into our CQL queries
    public boolean ReservedWord(String Word) {
        if (StopHash.containsKey(Word.toLowerCase())) {
            return true;
        }
        return false;
    }

    // given a standard query, format into CQL for use against the ITS API
    // if an operator is not specified, assume that it is a LIKE
    public String BuildQuery(String query, String sQueryType, int iType) throws IllegalQuery {
        // rules:
        // 1) if query requests type contains, starts with, or ends with,
        //    then do not attempt to use thesauri
        // 2) if query contains quotes (indicates phrasing) do not use
        //    thesauri OR allow "contains, starts with, ends with" queries
        if (query.indexOf("\"") == -1) {
            if (iType == 1) {
                query = "*" + query + "*";
                bUseThesauri = false;
            } // contains
            if (iType == 2) {
                query = query + "**";
                bUseThesauri = false;
            } // starts with
            if (iType == 3) {
                query = "**" + query;
                bUseThesauri = false;
            } // ends with
        }
        query = query.replaceAll("'", "''");

        return BuildQuery(query, sQueryType, "LIKE");
    }

    public String BuildQuery(String query, String sQueryType) throws IllegalQuery {
        return BuildQuery(query, sQueryType, "LIKE");
    }

    public String BuildQuery(String query, String sQueryType, String sOperator) throws IllegalQuery {
        if (sOperator.equals("LIKE")) {
            return BuildLIKEQuery(query, sQueryType);
        } else {
            return BuildEQUALSQuery(query, sQueryType);
        }
    }

    public String BuildEQUALSQuery(String query, String sQueryType) {
        String sOperator = "=";
        String sQuery = "";

        // ? means any character and translates into an underscore in interMedia
        // (using this character means thesauri will be turned off)
        if ((query.indexOf("\"") == -1) && (query.indexOf("?") != -1)) {
            bUseThesauri = false;
            query = query.replaceAll("?", "_");
        }

        char[] cArr = new char[query.length() + 1];  //cArr.Initialize();
        cArr = query.toCharArray();

        // cycle through each character in the query
        for (int i = 0; i < query.length(); i++) {
            if ((cArr[i] == '(') || (cArr[i] == ')')) {
                sQuery = sQuery + " " + cArr[i];
            } else {
                int j = i; // get temporary pointer
                Hashtable ht = GetNextWord(cArr, i, j, query);
                Enumeration eH = ht.keys();

                String sWord = ""; // get the next available word starting at pointer
                if (eH.hasMoreElements())
                    sWord = (String) eH.nextElement();

                // get a pointer to the current word
                j = new Integer((String) ht.get(sWord)).intValue();

                // ensure the word is valid
                if (!sWord.equals("") && !ReservedWord(sWord)) {
                    if ((sWord.toUpperCase().equals("AND")) ||
                            (sWord.toUpperCase().equals("OR"))) {
                        sQuery = sQuery + " " + sWord;
                    } else {
                        //if (i > 0)
                        //    sQuery = sQuery + " (" + sQueryType;
                        //else
                            sQuery = sQuery + " " + sQueryType;

                        if (sWord.toUpperCase().equals("NOT")) {
                            sQuery = sQuery + " <>";
                        } else {
                            // adding thesaurus enhancement
                            if (sOperator.toLowerCase().equals("like")) {
                                // if this word contains a * do not query it as
                                // a thesaurus, and convert those * to use percent signs
                                if (sWord.indexOf("*") != -1) {
                                    sQuery = sQuery + " " + sOperator + " '" +
                                            sWord.replaceAll("*", "%") + "'";
                                } else if (bUseThesauri) {
                                    sQuery = sQuery + " " + sOperator +
                                            " 'SYN(" + sWord + ",idrac_v2)'";
                                } else {
                                    sQuery = sQuery + " " + sOperator + " '" + sWord + "'";
                                }
                            } else {
                                sQuery = sQuery + " " + sOperator + " '" + sWord + "'";
                            }
                        }

                        // look ahead to the next word, if applicable
                        if ((cArr.length > j) && (cArr[j] == ')')) { sQuery = sQuery + ")"; }
                        i = j + 1;
                        if ((cArr.length > j) && (cArr[j] == '(')) { sQuery = sQuery + "("; }
                        ht = GetNextWord(cArr, i, i, query);
                        eH = ht.keys();

                        String sNextWord = "";
                        if (eH.hasMoreElements()) {
                            sNextWord = (String) eH.nextElement();
                        }

                        if (!((sNextWord.toUpperCase().equals("AND")) ||
                                (sNextWord.toUpperCase().equals("OR")) ||
                                (sNextWord.toUpperCase().equals("NOT")))) {
                            if (sWord.toUpperCase().equals("NOT")) {
                                sQuery = sQuery + " '" + sNextWord + "'";
                                j = new Integer((String) ht.get(sNextWord)).intValue();
                            } else {
                                if ((j < cArr.length) &&
                                        (cArr[j] == ' ')) {
                                    sQuery = sQuery + " AND ";
                                }
                            }
                        }
                    }
                    i = j;
                }
            }
        }
        // before it's all over, check parenth levels and ensure matching
        //  note this is a fix for bug # 434
        int iopenParen = 0;
        int icloseParen = 0;
        cArr = new char[sQuery.length() + 1]; //cArr.Initialize();
        cArr = sQuery.toCharArray();

        for (int k = 0; k < sQuery.length(); k++) {
            if (cArr[k] == '(') {
                iopenParen++;
            }
            if (cArr[k] == ')') {
                icloseParen++;
            }
        }

        // add ending parens if missing
        for (int k = 0; k < (iopenParen - icloseParen); k++) {
            sQuery = sQuery + ")";
        }

        query = sQuery;
        return sQuery;
    }

    public String BuildLIKEQuery(String query, String sQueryType) throws IllegalQuery {
        String sOperator = "LIKE";
        String sQuery = "";

        // ? means any character and translates into an underscore in interMedia
        // (using this character means thesauri will be turned off)
        if ((query.indexOf("\"") == -1) && (query.indexOf("?") != -1)) {
            bUseThesauri = false;
            query = query.replaceAll("?", "_");
        }

        if (query.toUpperCase().trim().startsWith("NOT")) {
            sOperator = "NOT LIKE";
            query = query.trim().substring(4);

            if (query.toUpperCase().indexOf(" NOT ") != -1) { throw new IllegalQuery("Double negative failure."); }
        }
        sQuery = " " + sQueryType + " " + sOperator + " '";

        // look through each word and change into a thesaurus term if thesaurus is true
        char[] cArr = new char[query.length() + 1]; //cArr.Initialize();
        cArr = query.toCharArray();

        // cycle through each character in the query
        for (int i = 0; i < query.length(); i++) {
            if ((cArr[i] == '(') || (cArr[i] == ')')) {
                sQuery = sQuery + " " + cArr[i];
            } else {
                int j = i; // get temporary pointer
                Hashtable ht = GetNextWord(cArr, i, j, query);
                Enumeration eH = ht.keys();

                String sWord = ""; // get the next available word starting at pointer
                if (eH.hasMoreElements()) {
                    sWord = (String) eH.nextElement();
                }

                // get a pointer to the current word
                j = new Integer((String) ht.get(sWord)).intValue();

                // ensure the word is valid
                if (!sWord.equals("") && !ReservedWord(sWord)) {
                    if ((sWord.toUpperCase().equals("AND")) ||
                            (sWord.toUpperCase().equals("NOT")) ||
                            (sWord.toUpperCase().startsWith("NEAR")) ||
                            (sWord.toUpperCase().equals("OR"))) {
                        sQuery = sQuery + " " + sWord + " ";
                    } else {
                        // adding thesaurus enhancement
                        if (sOperator.toLowerCase().equals("like")) {
                            // if this word contains a * do not query it as
                            // a thesaurus, and convert those * to use percent signs
                            if (sWord.indexOf("*") != -1) {
                                sQuery = sQuery + " " + sWord.replaceAll("*", "%");
                            } else if (bUseThesauri) {
                                sQuery = sQuery + " SYN(" + sWord + ",idrac_v2)";
                            } else {
                                sQuery = sQuery + " " + sWord;
                            }
                        } else {
                            sQuery = sQuery + sWord;
                        }

                        // look ahead to the next word, if applicable
                        i = j + 1;
                        ht = GetNextWord(cArr, i, i, query);
                        eH = ht.keys();

                        String sNextWord = "";
                        if (eH.hasMoreElements()) {
                            sNextWord = (String) eH.nextElement();
                        }

                        if (!((sNextWord.toUpperCase().equals("AND")) ||
                                (sNextWord.toUpperCase().equals("OR")) ||
                                (sNextWord.toUpperCase().equals("NOT")))) {
                            if (sWord.toUpperCase().equals("NOT")) {
                                sQuery = sQuery + " '" + sNextWord + "'";
                                j = new Integer((String) ht.get(sNextWord)).intValue();
                            } else {
                                if ((j < cArr.length) &&
                                        (cArr[j] == ' ')) {
                                    sQuery = sQuery + " AND ";
                                }
                            }
                        }
                    }
                    i = j;
                }
            }
        }
        // before it's all over, check parenth levels and ensure matching
        //  note this is a fix for bug # 434

        int iopenParen = 0;
        int icloseParen = 0;
        cArr = new char[sQuery.length() + 1]; //cArr.Initialize();
        cArr = sQuery.toCharArray();

        for (int k = 0; k < sQuery.length(); k++) {
            if (cArr[k] == '(') {
                iopenParen++;
            }
            if (cArr[k] == ')') {
                icloseParen++;
            }
        }

        // add ending parens if missing
        for (int k = 0; k < (iopenParen - icloseParen); k++) {
            sQuery = sQuery + ")";
        }

        sQuery = sQuery + "'";
        sQuery = sQuery.toUpperCase().replaceAll("AND NOT", "NOT");
        query = sQuery;
        return sQuery;
    }

    // get the next work in an array of characters
    public Hashtable GetNextWord(char[] cArr, int i, int j, String sQuery) {
        Hashtable ht = new Hashtable();

        String sWord = "";

        if (j >= cArr.length) {
            ht.put("", j + "");
            return ht;
        }

        if (cArr[j] == '(')  //skip past parens
        {
            j++;
            i++;
        }

        if (isQuote(cArr[j])) {
            j++;
            while ((!isQuote(cArr[j])) && (j < sQuery.length())) {
                j++;
            }
            sWord = copyValueOf(cArr, i, ((j - i) - 1));
            if (isQuote(cArr[j])) {
                j++;
            }
        } else {
            while ((j < sQuery.length()) && (cArr[j] != ' ') && (cArr[j] != ')'))
                j++;

            sWord = copyValueOf(cArr, i - 1, (j - i));
            if (j >= cArr.length)
                j = j - 1;
            if (cArr[j] == ')')
                j = j - 1;
        }

        ht.put(sWord, j + "");
        return ht;
    }

    // is this a quote? we must look for french quotes too
    public boolean isQuote(char c) {
        if ((int) c == 34) {
            return true;
        }
        if ((int) c == 8220) {
            return true;
        }
        if ((int) c == 8221) {
            return true;
        }

        return false;
    }

    // copy a segment of a character array into a String
    public String copyValueOf(char[] cArr, int offset, int count) {
        String sWord = "";
        for (int i = 1; i <= count; i++) {
            if ((i + offset) < cArr.length) {
                sWord = sWord + cArr[i + offset];
            }
        }

        return sWord;
    }
}
