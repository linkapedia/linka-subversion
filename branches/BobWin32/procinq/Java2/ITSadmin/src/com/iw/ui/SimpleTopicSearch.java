package com.iw.ui;

import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.tools.*;
import com.iw.tools.SwingWorker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class SimpleTopicSearch extends TopicSearch implements ActionListener {
    public SimpleTopicSearch (ITSAdministrator frame) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Advanced Topic Search", true, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(675, 480);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        searchButton = new JButton("Search");
        searchButton.addActionListener(this);
        clearButton = new JButton("Close");
        clearButton.addActionListener(this);

        queryLabel = new JLabel("CQL:");
        queryLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        queryLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        queryText = new JTextField();

        resultsTable = new JSortTable();
        resultsTable.addMouseListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestGrid(2, 1);
                            {
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(queryLabel);
                                    layout.parent();
                                }
                                layout.add(queryText);
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                            {
                                layout.add(new JScrollPane(resultsTable));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(searchButton);
                                        layout.addSpace(10);
                                        layout.add(clearButton);
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();

            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(searchButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        final TopicSearch ds = this;

        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    if (e.getSource().equals(clearButton)) {
                        MDIframe.dispose();
                    } else if (e.getSource().equals(searchButton)) {
                        try {
                            loadResults();
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    protected void loadResults() throws Exception {
        Vector vNodes = new Vector();

        try {
            vNodes = runSearch();
        } catch (IllegalQuery e) {
            JOptionPane.showMessageDialog(ITSframe, "Sorry, your query syntax is invalid.  Please recheck your query " +
                    "syntax and try again.",
                    "Error", JOptionPane.NO_OPTION);
            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);
            return;
        }

        if (vNodes.size() == 0) {
            JOptionPane.showMessageDialog(ITSframe, "No topics were found to match your search criteria.",
                    "Information", JOptionPane.NO_OPTION);

            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);
            return;
        }
        Hashtable htCorpus = new Hashtable();

        DefaultSortTableModel dtm = new DefaultSortTableModel(vNodes.size(), 3);
        dtm.setEditable(false);
        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            dtm.setValueAt(n, i, 0);
            dtm.setValueAt(n.get("NODEDESC"), i, 1);

            if (!htCorpus.containsKey(n.get("CORPUSID"))) {
                htCorpus.put(n.get("CORPUSID"), its.getCorpora(n.get("CORPUSID")));
            }

            dtm.setValueAt((Corpus) htCorpus.get(n.get("CORPUSID")), i, 2);
        }
        Vector v = new Vector();
        v.add("Title");
        v.add("Definition");
        v.add("Taxonomy");
        dtm.setColumnIdentifiers(v);

        resultsTable.setModel(dtm);
    }

    // return a vector of topics
    public Vector runSearch()
            throws Exception {
        try { return its.CQL(queryText.getText().toUpperCase()); }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw e;
        }
    }
}
