package com.iw.tools.mq.util;

import java.awt.Component;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

public class RenderCell extends DefaultTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1345604306402498218L;

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		Component cell = super.getTableCellRendererComponent(table, value,
				isSelected, hasFocus, row, column);

		this.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
		if (column==table.getColumnCount()-1) {
			String amount = (String) value;
			if (amount.equals("Running")) {
				cell.setBackground(Color.GREEN);
				cell.setForeground(Color.BLACK);
			} else {
				cell.setBackground(Color.RED);
				cell.setForeground(Color.BLACK);
			}
		} else {
			if (isSelected) {
				cell.setBackground(Color.DARK_GRAY);
				cell.setForeground(Color.WHITE);
			}else{
				cell.setBackground(Color.WHITE);
				cell.setForeground(Color.BLACK);
			}
		}
		return cell;
	}
}
