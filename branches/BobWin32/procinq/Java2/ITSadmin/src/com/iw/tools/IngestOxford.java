package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.iw.ui.PopupProgressBar;

import javax.swing.*;

//import java.awt.*;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.net.*;

import java.awt.event.*;

import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.XMLReaderFactory;
import org.dom4j.io.SAXReader;

import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.dom4j.Entity;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class IngestOxford {
	
    private static String[] sTitles = new String[10];
    public static int iLevelsProcessed = 40;

    public static Corpus createCorpusFromOxfordDictionary(File f, ITS its) throws Exception {
        System.out.println("Reading dictionary XML file from Oxford...");
        System.out.println("File: " + f.getAbsolutePath());

        FileInputStream fis = new FileInputStream(f);

        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);
        Element opr = doc.getRootElement();

        //Create the corpus and root node
    	String sFilename = f.getName();
    	Corpus c = its.addCorpus(sFilename);
        Node rootNode = its.getCorpusRoot(c.getID());

        // loop through each letter in the volume
        Iterator i = opr.elements("letter").iterator();
        	
        System.out.println("NodeCount (Elements): " + opr.nodeCount());
        
        while (i.hasNext()) {
            Element eLetter = (Element) i.next();
            String sLetter = eLetter.attribute("value").getText();
            ITSTreeNode letterNode = addNode(sLetter, rootNode.get("NODEID"), 1, c.getID(), its);
    		if (letterNode == null) 
    			return null;
            
            System.out.println("Added node: "+sLetter);
    		
            Iterator i2 = eLetter.elements("e").iterator();

            while (i2.hasNext()) {
                Element eArticle = (Element) i2.next();
                
                if ((eArticle != null))
                {
                    String sCurrentTitle;
                    Element eCurrentElement;

                	//iterate through title (hg, hw) and source (sg, se1) elements
                	if ((eArticle.element("sg") != null) && 						//has content
                		(eArticle.element("sg").element("se1") != null) &&			//has content
                		//has more than must a reference to another topics ("See xxx")
                		(!getSource(eArticle.element("sg").element("se1")).toLowerCase().startsWith("see "))) 
                	{
                		sCurrentTitle = eArticle.element("hg").elementText("hw");
                		List list;
                		
                		// add node to database
	                    ITSTreeNode currentNode = addNode(sCurrentTitle, letterNode.get("NODEID"), 2, c.getID(), its);
	                    if (currentNode != null) 
	                    {
	                		System.out.println("Added node: "+sLetter+" -> "+sCurrentTitle);

	                		//Add MustHaves found in node title (abbreviations, variations, expanded forms)
	                		if (eArticle.element("hg").element("vg") != null)
	                		{
		                		list = eArticle.element("hg").element("vg").elements("var");
		                        addMustHaves(list, currentNode, its);
	                		}
	                		list = eArticle.element("hg").elements("abbr");
	                		addMustHaves(list, currentNode, its);
	                    	
	                		list = eArticle.element("hg").elements("exp");
	                		addMustHaves(list, currentNode, its);
	                		
	                		//Add Signatures found in node title (ctx (context))
	                    	Signature nodeSig = null;
	                        Signatures vsig = new Signatures();
	               	
	                        Iterator iSignatures = eArticle.element("hg").elements("ctx").iterator();
	                		while (iSignatures.hasNext()){
	                			eCurrentElement = (Element) iSignatures.next();
	                			sCurrentTitle = eCurrentElement.getTextTrim();
	                    		nodeSig = new Signature(sCurrentTitle.toLowerCase(), 1);
	                   	     	vsig.add(nodeSig);
	               	     		its.saveSignatures(currentNode.get("NODEID"),vsig.getVecOfSignatureObjects(),false);
	                    	}
	                    		                    	
		                    // loop recursively through the XML tags to get source data for main node
		                    recurseElement(eArticle, c.getID(), its, currentNode, 2);
	                    
		                    //*****************************************************************************
		                    //iterate through ssect tags (if they exist, they follow se1 tags)
		                    // and create a subtopic of each
		                    Iterator iSubNodes = eArticle.element("sg").elements("ssect").iterator();

		                    while (iSubNodes.hasNext()) {
		                        Element eSubNode = (Element) iSubNodes.next();
		                        
		                        if ((eSubNode != null))
		                        {
				                	if ((eSubNode.element("sg") != null) && 					//has content
				                    	(eSubNode.element("sg").element("se1") != null))		//has content 
				                    {
				                		String sSubTitle = eSubNode.element("shg").elementText("shw");
			                    		
			                    		// add node to database
			    	                    ITSTreeNode subNode = addNode(sSubTitle, currentNode.get("NODEID"), 3, c.getID(), its);
			    	                    if (subNode != null) 
			    	                    {
			    	                		System.out.println(sLetter+" -> "+sCurrentTitle+" -> "+sSubTitle);
		
			    	                		//Add MustHaves found in node title (abbreviations, variations, expanded forms)
			    	                		if (eSubNode.element("shg").element("vg") != null)
			    	                		{
			    	                			list = eSubNode.element("shg").element("vg").elements("var");
			    		                	    addMustHaves(list, currentNode, its);
			    	                		}
			    	                		
			    	                		list = eSubNode.element("shg").elements("abbr");
			    	                		addMustHaves(list, currentNode, its);
			    	                    	
			    	                		list = eSubNode.element("shg").elements("exp");
			    	                		addMustHaves(list, currentNode, its);
		    	                    	
			    	                		//Add Signatures found in node title (ctx (context))
			    	                    	nodeSig = null;
			    	                        vsig = new Signatures();
			    	               	
			    	                        iSignatures = eSubNode.element("shg").elements("ctx").iterator();
			    	                		while (iSignatures.hasNext()){
			    	                			eCurrentElement = (Element) iSignatures.next();
			    	                			sCurrentTitle = eCurrentElement.getTextTrim();
			    	                    		nodeSig = new Signature(sCurrentTitle.toLowerCase(), 1);
			    	                   	     	vsig.add(nodeSig);
			    	               	     		its.saveSignatures(currentNode.get("NODEID"),vsig.getVecOfSignatureObjects(),false);
			    	                    	}
			    	                    		                    	
			    		                    // loop recursively through the XML tags to get source data  
			    		                    recurseElement(eSubNode, c.getID(), its, subNode, 3);
			    	                    }        
			                    	}
		                        }
		                    }
	                	}
	                }
                }
            }
        }
        fis.close();
                
        return c;
    }

    public static void recurseElement(Element e, String sCorpusID, ITS its, ITSTreeNode mainNode, int mainNodeLevel) {
        //  each article/section has one "sg" tag which contains 1-n "se1" tags.
    	//  each se1 may have subnodes within them (<hn> tags)
    	Iterator iMainSource = e.element("sg").elements("se1").iterator();
    	StringBuffer sbMainNodeSource = new StringBuffer();

    	if (!iMainSource.hasNext()) return;
    	
        while (iMainSource.hasNext()) {
            Element eSE1 = (Element) iMainSource.next();

            //determine if this source section has any subheadings which should be subnodes
            if (eSE1.element("h1") != null ||
            	eSE1.element("h2") != null ||
            	eSE1.element("h3") != null)
            {
	            //iterate through all of the content, saving off source to main node until hit first heading
	            //  once find heading elements (h1, h2 or h3), create subnodes and their source
                List content = eSE1.content();
                Iterator iAllContent = content.iterator();

                int[] headings = new int[iLevelsProcessed];
                String[] nodeIDs = new String[iLevelsProcessed];
                String[] titles = new String[iLevelsProcessed];
                int[] levels = new int[iLevelsProcessed];
                StringBuffer[] source = new StringBuffer[iLevelsProcessed];
                //initialize stringbuffers
                for (int loop=0;loop<iLevelsProcessed;loop++){source[loop] = new StringBuffer();};

                //set first topic to the main one passed in
                int iIndex = 0;	//set index for arrays
                headings[iIndex] = 0;
                levels[iIndex] = mainNodeLevel;
                nodeIDs[iIndex] = mainNode.get("NODEID");	

	            while (iAllContent.hasNext())
	            {
	            	//process source with headings
	            	Object obj = iAllContent.next();
	                if (obj instanceof Text) 
	                {
     	   	    		source[iIndex].append(((Text) obj).getText());
     	   	    		source[iIndex].append(" ");
	                }
	                else if (obj instanceof Element) {
	                	 Element eCurrentElement = (Element) obj;
	                	 if (eCurrentElement.getQualifiedName().toString().toLowerCase().startsWith("h"))
	                	 {
	         	   	    	iIndex++;
	         	   	    	headings[iIndex] = new Integer(eCurrentElement.getQualifiedName().substring(1,2)).intValue();
	         	   	    	titles[iIndex] = eCurrentElement.getTextTrim();
	         	   	    	//heading elements do not have own source, just title text
	                	 }
	                	 else
	                	 {
	     	   	    		source[iIndex].append(getSource((Element) obj));
	     	   	    		source[iIndex].append(" ");
	                	 }
	                }
	        	    else if (obj instanceof Entity) 
	        	    {
	                 	if (((Entity) obj).getText() != null)
	                 	{
	     	   	    		source[iIndex].append(((Entity) obj).getText());
	     	   	    		source[iIndex].append(" ");
	                 	}
	            	}

	            }
         
	            //create subnodes and set their source 
                try 
                {
            		//really wacky logic here because although subheadings must be higher numbers, 
            		//the source doesn't always start at h1.  sometimes starts at h2, then has sibling of h1
             		// example hierarchy:
             		//mainnode
             		//  h2
             		//		h3
             		//	h1
             		//		h2
             		//			h3
                	for (int i=1;i<iLevelsProcessed;i++)
                	{
                		if (titles[i] != null)
                		{
                			//look for most recent heading that is less than current heading number
                			int iParentLevel = i-1;
                			for (; iParentLevel>=0 && (headings[i] <= (headings[iParentLevel])) ;iParentLevel--){};
                			
	                		ITSTreeNode subNode = addNode(titles[i], nodeIDs[iParentLevel], levels[iParentLevel]+1, sCorpusID, its);
	                		
	                		if (subNode!= null)
	                		{
	                			//set nodeID
	                			nodeIDs[i] = subNode.get("NODEID");
	                			levels[i]= levels[iParentLevel]+1;
	                			if (source[i] != null)
		                		{	//set node source
		                			its.setNodeSource(subNode, source[i].toString());
		                		}	
	                		}
	                		
                		}
                	}
                }
                catch (Exception exception) {
                    System.err.println("Could not set node source for: " + mainNode.get("NODETITLE"));
                    exception.printStackTrace(System.err);
                }        
            }
            else	//get and add text to main node source
            {
            	sbMainNodeSource.append(getSource(eSE1));
            	sbMainNodeSource.append(" ");
            }
        }
        
        //set node source for main node passed in
        try {	
        	its.setNodeSource(mainNode, sbMainNodeSource.toString());
        }
        catch (Exception exception) {
            System.err.println("Could not set node source for: " + mainNode.get("NODETITLE"));
            exception.printStackTrace(System.err);
        }        
   }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh) && !(prevCh != (char) 39))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    //iterate through all sub tags and accumulate source text
    public static String getSource(Element eSource) 
    {
    	 StringBuffer sb = new StringBuffer();

         List content = eSource.content();
         Iterator it = content.iterator();

         while (it.hasNext()) {
             Object obj = it.next();
             if (obj instanceof Text) {
                 sb.append(((Text) obj).getText());
             } else if (obj instanceof Element) {
                 if (((Element) obj).attribute("char") != null)
                     sb.append(((Element) obj).attribute("char").getText());
                 else
                     sb.append(getSource((Element) obj));
             } else if (obj instanceof Entity) {
             	if (((Entity) obj).getText() != null)
             		sb.append(((Entity) obj).getText());
                 else
                     sb.append(getSource((Element) obj));
             }
         }

         return sb.toString();
    }

    public static ITSTreeNode addNode(String sNodeTitle, String sParent, int Depth, String sCorpusID, ITS its) {
        ITSTreeNode n = new ITSTreeNode();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", sCorpusID);
        n.set("DEPTHFROMROOT", Depth + "");
 
        try {
            n = its.addNode(n);
            //System.out.println("Added node: " + sNodeTitle + ", Parent: " + sParent);

        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;
    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }

	
    public static void addMustHaves(List list, ITSTreeNode currentNode, ITS its) 
    {
        if (list.size() != 0)
        {
        	Iterator iMustHaves = list.iterator();
        	while (iMustHaves.hasNext()){
        			Element eCurrentElement = (Element) iMustHaves.next();
        			String sCurrentTitle = eCurrentElement.getTextTrim();
            		try {its.addMusthave(currentNode.get("NODEID"),sCurrentTitle);}
        	        catch (Exception e) {
        	        	System.err.println("Could not add must have for: " + currentNode.get("NODETITLE"));
        	        	e.printStackTrace(System.err);
           	        }        
           }
        }
        return;
    }
}