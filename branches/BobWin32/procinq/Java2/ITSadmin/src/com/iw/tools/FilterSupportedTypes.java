package com.iw.tools;

import javax.swing.filechooser.*;
import java.io.File;

public class FilterSupportedTypes extends FileFilter {
    public boolean accept(File f) {
        String Name = f.getName().toLowerCase();
        if (Name.endsWith("pdf") || Name.endsWith("txt") || Name.endsWith("htm") || Name.endsWith("html")) {
            return true;
        }
        return false;
    }

    public String getDescription() {
        return "Text documents (*.txt); HTML documents (*.htm/*.html); PDF documents (*.pdf)";
    }

}
