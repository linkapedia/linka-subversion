package com.iw.system;

public class Alert {
    // User attributes
    private String ID = null;
    private String Name;
    private String UserDN;
    private String Email;
    private String CQL;
    private String Notes;
    private FrequencyDrop RunFrequency;

    // constructor(s)
    public Alert (String ID, String Name, String UserDN, String Email, String CQL, String Notes, FrequencyDrop RunFrequency) {
        this.ID = ID; this.Name = Name; this.UserDN = UserDN; this.Email = Email; this.CQL = CQL; this.Notes = Notes;
        this.RunFrequency = RunFrequency;
    }

    // accessor functions
    public String getID() { return ID; }
    public String getName() { return Name; }
    public String getDN() { return UserDN; }
    public String getEmail() { return Email; }
    public String getCQL() { return CQL; }
    public String getNotes() { return Notes; }
    public FrequencyDrop getRunFrequency() { return RunFrequency; }

    public void setID(String ID) { this.ID = ID; }
    public void setName(String Name) { this.Name = Name; }
    public void setDN(String DN) { this.UserDN = DN; }
    public void setEmail(String Email) { this.Email = Email; }
    public void setCQL(String CQL) { this.CQL = CQL; }
    public void setNotes(String Notes) { this.Notes = Notes; }
    public void setFreq(FrequencyDrop fd) { this.RunFrequency = fd; }

    public String toString() { return Name; }
}
