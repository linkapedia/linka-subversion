/*
 * GUI : Display list the taxonomies and list the resources
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
 */

package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.iw.system.ITS;
import com.iw.tools.wiki.init.IWikiNotify;
import com.iw.tools.wiki.init.StartProcess;

public class CreateCsvWikiUI extends JPanel implements ActionListener,IWikiNotify {

	public ITS its = null;
	public ITSAdministrator ITSframe = null;

	protected JInternalFrame MDIframe;
	private static final long serialVersionUID = 116309844149711179L;

	private ArrayList<String> categories = new ArrayList<String>();
	private JList list;

	private JScrollPane panelScroll;

	private JPanel panelButtons;
	private JPanel panelText = new JPanel();
	private JPanel panelList = new JPanel();
	private JLabel lblTitle = new JLabel("LOG");
	private JButton btnOk;
	private JButton btnErase;
	private JTextField text;
	DefaultListModel modelList;

	public CreateCsvWikiUI() throws Exception {
	}

	public void showWin(ITSAdministrator frame) {
		panelButtons = new JPanel();
		JDesktopPane jdesktoppane = frame.jdp;

		MDIframe = new JInternalFrame("Create CSV", false, true, false, true);
		MDIframe.setBackground(Color.lightGray);
		its = frame.its;
		ITSframe = frame;

		MDIframe.setFrameIcon(ITSframe.iIndraweb);

		modelList = new DefaultListModel();
		text = new JTextField(30);

		list = new JList(modelList);
		list.setEnabled(false);
		btnOk = new JButton("Generate");
		btnOk.addActionListener(this);
		btnErase = new JButton("Erase log");
		btnErase.addActionListener(this);

		panelScroll = new JScrollPane(list);
		panelList.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		panelText.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		panelButtons.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		panelButtons.setLayout(new GridLayout(1, 3));
		panelButtons.add(btnOk);
		panelButtons.add(btnErase);

		panelList.setLayout(new BorderLayout());

		panelList.add(lblTitle, BorderLayout.NORTH);
		panelList.add(panelScroll, BorderLayout.CENTER);
		panelText.add(new JLabel("Category:"), BorderLayout.NORTH);
		panelText.add(text, BorderLayout.CENTER);

		MDIframe.getRootPane().setDefaultButton(btnOk);
		MDIframe.getContentPane().add(panelList, BorderLayout.NORTH);
		MDIframe.getContentPane().add(panelText, BorderLayout.CENTER);
		MDIframe.getContentPane().add(panelButtons, BorderLayout.SOUTH);
		MDIframe.pack();
		setVisible(true);

		jdesktoppane.add(MDIframe);
		MDIframe.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btnOk)) {
			if (categories.contains(text.getText())) {
				JOptionPane.showMessageDialog(null,
						"Already exits");
			} else {
				categories.add(text.getText());
				if (text.getText().isEmpty()
						|| (text.getText().length() >= 100)) {
					JOptionPane.showMessageDialog(null,
							"text is empty or lenght >100");
				} else {
					StartProcess sp = new StartProcess("Category:"
							+ text.getText(), this);
					new Thread(sp).start();
				}
			}
		} else if (e.getSource().equals(btnErase)) {
			text.setText("");
			modelList.clear();
		}
	}

	@Override
	public void wikiNotify(String notify) {
		
		modelList.addElement(notify);
		Runnable ru=new Runnable() {
			@Override
			public void run() {
				list.setSelectedIndex(modelList.getSize()-1);
				list.ensureIndexIsVisible(modelList.getSize()-1);
			}
		};
		SwingUtilities.invokeLater(ru);
	}
}
