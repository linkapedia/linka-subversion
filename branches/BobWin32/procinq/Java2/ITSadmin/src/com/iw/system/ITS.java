package com.iw.system;

import info.bliki.wiki.template.URLEncode;

import java.awt.Component;
import java.awt.List;
import java.awt.datatransfer.Clipboard;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.zip.GZIPOutputStream;

import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.apache.http.client.utils.URLEncodedUtils;
import org.dom4j.Element;
import org.jfree.util.Log;
import org.pdfbox.Main;
import org.textmining.text.extraction.WordExtractor;
import org.xeustechnologies.jtar.TarEntry;
import org.xeustechnologies.jtar.TarInputStream;

import com.iw.guiservershared.TableModelXMLBeanSerializable;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;
import com.iw.license.IndraLicense;
import com.iw.tools.BingInfoBean;
import com.iw.tools.GoogleInfoBean;
import com.iw.tools.HTMLtoTXT;
import com.iw.ui.ITSAdministrator;
import com.iw.ui.PopupProgressBar;
import net.sf.json.JSONObject;

/**
 * The ITS object defines both a user session and contains methods used to
 * interface with the ITS API. Each of these methods are simply abstract another
 * layer onto the raw InvokeAPI class.
 *
 * A pointer to the ITS object should be kept persistently as it is the
 * container for both the user's session key and the location of the API server.
 *
 * @authors Michael A. Puscar, Intellisophic Inc, All Rights Reserved.
 *
 * @note Session and ITS server information are stored as a public property of
 * the frame.
 * @return An ITS object.
 */
public class ITS extends Component implements Serializable {

    private static final long serialVersionUID = -7427524221486609329L;
    private String SKEY = "NONE";
    private String API = "http://localhost/";
    private String domainController = null;
    // Clipboard object moved here to avoid marriage to individual panes and
    // components.
    public Clipboard clipboard = new Clipboard("local");
    public TreePath originalSelectionPath = null;
    public DefaultMutableTreeNode[] cutNode = null;
    private static PrintStream Systemout = null;

    public ITS() {
    }

    public ITS(String SessionKey, String API) {
        if (SessionKey != null) {
            this.SKEY = SessionKey;
        }
        if (API != null) {
            this.API = API + "itsapi/ts?fn=";
        }
    }

    // Accessor functions
    public void SetSessionKey(String SessionKey) {
        if (SessionKey != null) {
            this.SKEY = SessionKey;
        }
    }

    public void SetAPI(String API) {
        if (API != null) {
            this.API = API + "itsapi/ts?fn=";
        }
    }

    public String getSessionKey() {
        return SKEY;
    }

    public String getAPI() {
        return API;
    }

    public HashTree getArguments() {
        HashTree ht = new HashTree();
        ht.put("api", API);
        ht.put("SKEY", SKEY);
        return ht;
    }

    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs)
            throws Exception {
        return InvokeAPI(sAPIcall, htArgs, true);
    }

    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs,
            boolean bReplaceQuotes) throws Exception {
        return InvokeAPI(sAPIcall, htArgs, bReplaceQuotes, false);
    }

    public HashTree InvokeAPI(String sAPIcall, Hashtable htArgs,
            boolean bReplaceQuotes, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        HashTree ht = null;
        try {
            ht = API.Execute(bDebug, false, bReplaceQuotes);
        } catch (SessionExpired e) {
            sessionTimedOut();
        } catch (Exception e) {
            throw e;
        }

        return ht;
    }

    public Vector InvokeVectorAPI(String sAPIcall, Hashtable htArgs)
            throws Exception {
        return InvokeVectorAPI(sAPIcall, htArgs, false);
    }

    public Vector InvokeVectorAPI(String sAPIcall, Hashtable htArgs,
            boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        Vector v = new Vector();
        try {
            v = API.vExecute(bDebug, false);
        } catch (SessionExpired e) {
            sessionTimedOut();
        } catch (Exception e) {
            throw e;
        }

        return v;
    }

    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs)
            throws Exception {
        return InvokeDomAPI(sAPIcall, htArgs, false);
    }

    public org.dom4j.Document InvokeDomAPI(String sAPIcall, Hashtable htArgs,
            boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try {
            d = API.dExecute(bDebug, false);
        } catch (Exception e) {
            throw e;
        }

        return d;
    }

    public org.dom4j.Document InvokeDomPostAPI(String sAPIcall, Hashtable htArgs)
            throws Exception {
        return InvokeDomPostAPI(sAPIcall, htArgs, false);
    }

    public org.dom4j.Document InvokeDomPostAPI(String sAPIcall,
            Hashtable htArgs, boolean bDebug) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try {
            d = API.dExecutePost(bDebug, false);
        } catch (Exception e) {
            throw e;
        }

        return d;
    }

    public org.dom4j.Document InvokeDomPostStatusAPI(String sAPIcall, File f,
            Hashtable htArgs, PopupProgressBar bar, boolean sendGZ)
            throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try {
            d = API.dPostExecuteStatus(f, bar, sendGZ);
        } catch (Exception e) {
            throw e;
        }

        return d;
    }

    public org.dom4j.Document InvokeDomStatusAPI(String sAPIcall,
            Hashtable htArgs, PopupProgressBar bar) throws Exception {
        return InvokeDomStatusAPI(sAPIcall, htArgs, bar, false);
    }

    public org.dom4j.Document InvokeDomStatusAPI(String sAPIcall,
            Hashtable htArgs, PopupProgressBar bar, boolean bDebug)
            throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        org.dom4j.Document d = null;
        try {
            d = API.dExecute(bDebug, false, bar);
        } catch (Exception e) {
            throw e;
        }

        return d;
    }

    private TarInputStream InvokeTarApi(String sAPIcall, Hashtable htArgs,
            boolean bDebug, boolean bUseProxy) throws Exception {
        InvokeAPI API = new InvokeAPI(sAPIcall, htArgs);
        TarInputStream tarFile = null;
        try {
            tarFile = API.tarExecute(bDebug, bUseProxy);
        } catch (Exception e) {
            throw e;
        }

        return tarFile;
    }

    public static void activateLogging() {
        // logging streams
        PrintStream printstream_out = null;

        try {
            Systemout = System.out;

            File logfile_out = new File("IWclient_out_"
                    + System.currentTimeMillis() + ".txt");
            FileOutputStream fos_out = new FileOutputStream(logfile_out);
            printstream_out = new PrintStream(fos_out);
            System.setOut(printstream_out);
            System.setErr(printstream_out);

            System.out.println("Client logging enabled.");

            System.out.flush();
            System.err.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deactivateLogging() {
        if (Systemout != null) {
            System.setOut(Systemout);
            System.setErr(Systemout);

            System.out.println("Client logging disabled.");
        }
    }

    public void addImageLocalNode(ArrayList<FileMetadata> listFile,
            byte[] infoData) {
        try {
            Hashtable<String, String> htArgs = getArguments();
            InvokeAPI invokeApi = new InvokeAPI("tsnode.TSAddImageNode", htArgs);
            HashTree hashTree = invokeApi.PostExecute(listFile, infoData, true);
            if (hashTree.get("SUCCESS") == null) {
                JOptionPane.showMessageDialog(null, "Error saving images");
            } else {
                JOptionPane.showMessageDialog(null, "Add image successful");
            }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    public void addImageLocalCorpus(ArrayList<FileMetadata> listFile,
            byte[] infoData) {
        try {
            Hashtable<String, String> htArgs = getArguments();
            InvokeAPI invokeApi = new InvokeAPI("tscorpus.TSAddImageCorpus",
                    htArgs);
            HashTree hashTree = invokeApi.PostExecute(listFile, infoData, true);
            if (hashTree.get("SUCCESS") == null) {
                JOptionPane.showMessageDialog(null, "Error saving images");
            } else {
                JOptionPane.showMessageDialog(null, "Add image successful");
            }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    public void addDictionary(String nameDictionary, File dictionary,
            PopupProgressBar bar) {
        Hashtable htArgs = getArguments();
        htArgs.put("name", nameDictionary);
        try {
            org.dom4j.Document doc = InvokeDomPostStatusAPI(
                    "util.ImportDictionary", dictionary, htArgs, bar, true);
            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(null, "Error add dictionary");
            } else {
                JOptionPane
                        .showMessageDialog(null, "Add dictionary successful");
            }

        } catch (Exception e) {
            System.out.println("Exception from addImagesNodeRecursive: "
                    + e.getMessage());
        }

    }

    public void addImageURLCorpus(Corpus c, String url) {
        Hashtable htArgs = getArguments();
        htArgs.put("url", url);
        htArgs.put("corpusid", c.getID());

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI(
                    "tscorpus.TSAddImageCorpusURL", htArgs);

            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(null, "Error saving image");
            } else {
                JOptionPane.showMessageDialog(null, "Add image successful");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
    }

    public void addImageURLNode(ITSTreeNode n, String url) {
        Hashtable htArgs = getArguments();
        htArgs.put("url", url);
        htArgs.put("nodeid", n.get("NODEID"));

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSAddImageNodeURL",
                    htArgs);

            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(null, "Error saving image");
            } else {
                JOptionPane.showMessageDialog(null, "Add image successful");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
    }

    public static User getUser() {
        File fp = new File("user.data");
        if (fp.exists()) {
            try {
                return User.deserializeObject("user.data");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        "Note, due to a system change your previous user "
                        + "preferences have been lost.", "Information",
                        JOptionPane.NO_OPTION);
            }
        }
        return null;
    }

    public String getDomainController() throws Exception {
        if (domainController == null) {
            Hashtable<String, String> htArgs = getArguments();
            htArgs.put("PARAMNAME", "DC");

            try { // invoke the API
                HashTree htResults = InvokeAPI("security.TSGetConfigParams",
                        htArgs);

                if (!htResults.containsKey("CONFIGPARAMS")) {
                    return "DC=";
                }
                HashTree htConfig = (HashTree) htResults.get("CONFIGPARAMS");
                return (String) htConfig.get("DC");

            } catch (Exception e) {
                throw e;
            }
        }

        return domainController;
    }

    // hardcoded (for now), will read from file
    public String getClientVersion() {
        return "ProcinQ Client 1.5";
    }

    // get the server version
    public String getServerVersion() {
        Hashtable htArgs = getArguments();

        try {
            HashTree htResults = InvokeAPI("tsother.TSGetVersion", htArgs);
            if (!htResults.containsKey("VERSION")) {
                return "Server version unavailable.";
            }

            return (String) htResults.get("VERSION");
        } catch (Exception e) {
            return "Server version unavailable.";
        }
    }

    public String getAdminGroup() throws Exception {
        if (domainController == null) {
            Hashtable htArgs = getArguments();
            htArgs.put("PARAMNAME", "AdminGroup");

            try { // invoke the API
                HashTree htResults = InvokeAPI("security.TSGetConfigParams",
                        htArgs);

                if (!htResults.containsKey("CONFIGPARAMS")) {
                    return "";
                }
                HashTree htConfig = (HashTree) htResults.get("CONFIGPARAMS");
                return (String) htConfig.get("AdminGroup");

            } catch (Exception e) {
                throw e;
            }
        }

        return domainController;
    }

    public Vector getMyGroups() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        try {
            HashTree htResults = InvokeAPI("security.TSGetMyGroups", htArgs);
            if (!htResults.containsKey("GROUPS")) {
                return v;
            }

            HashTree htGroups = (HashTree) htResults.get("GROUPS");
            Enumeration eG = htGroups.elements();

            while (eG.hasMoreElements()) {
                HashTree htGroup = (HashTree) eG.nextElement();
                AlertGroup ag = new AlertGroup((String) htGroup.get("DC"),
                        (String) htGroup.get("GROUPNAME"));
                v.add(ag);
            }
        } catch (Exception e) {
            throw e;
        }

        return v;
    }

    public Vector getMyAlerts(User u) throws Exception {
        return getMyAlerts(u.getID());
    }

    public Vector getMyAlerts(String DN) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        // System.out.println("getMyAlerts: "+DN);

        htArgs.put("UserID", DN);

        try {
            HashTree htResults = InvokeAPI("tsnotification.TSViewAlertsByDN",
                    htArgs);
            if (!htResults.containsKey("ALERTS")) {
                return v;
            }

            HashTree htGroups = (HashTree) htResults.get("ALERTS");
            Enumeration eG = htGroups.elements();

            while (eG.hasMoreElements()) {
                HashTree htGroup = (HashTree) eG.nextElement();

                String runFreq = (String) htGroup.get("RUNFREQ");
                FrequencyDrop fd = new FrequencyDrop("Never", runFreq);

                if (runFreq.equals("1")) {
                    fd = new FrequencyDrop("Daily", runFreq);
                } else if (runFreq.equals("7")) {
                    fd = new FrequencyDrop("Weekly", runFreq);
                } else if (runFreq.equals("30")) {
                    fd = new FrequencyDrop("Monthly", runFreq);
                }

                Alert a = new Alert((String) htGroup.get("ALERTID"),
                        (String) htGroup.get("ALERTNAME"),
                        (String) htGroup.get("USERDN"),
                        (String) htGroup.get("EMAIL"),
                        (String) htGroup.get("QUERY"),
                        (String) htGroup.get("NOTES"), fd);

                v.add(a);
            }
        } catch (Exception e) {
            throw e;
        }

        // System.out.println("Results: "+v.size());
        return v;
    }

    // ITS Login
    // Arguments: Username, Password
    // Returns: User object
    public User Login(String Username, String Password) throws Exception {
        // LOGIN routine takes a USERID and a PASsWORD
        Hashtable htArgs = new Hashtable();
        htArgs.put("UserID", Username); // example:
        // "sn=cifaadmin,ou=users,dc=cifanet";
        htArgs.put("Password", Password); // example: racer9
        htArgs.put("api", API);

        try {
            // Invoke the ITSAPI now.
            HashTree htResults = InvokeAPI("security.TSLogin", htArgs);

            // If there is no "subscriber" tag, an error has occured
            if (!htResults.containsKey("SUBSCRIBER")) {
                throw new Exception("Invalid username, password combination.");
            }
            // Get user hash tree
            HashTree htUser = (HashTree) htResults.get("SUBSCRIBER");

            if (!htUser.containsKey("KEY")) {
                throw new Exception("Invalid username, password combination.");
            }

            return new User(htUser);
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getFolders() throws Exception {
        return getFolders(null);
    }

    public Vector getFolders(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) {
            htArgs.put("CorpusID", c.getID());
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsgenre.TSListFolders", htArgs);

            if (!htResults.containsKey("GENRES")) {
                return new Vector();
            }

            HashTree htGenres = (HashTree) htResults.get("GENRES");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                Genre g = new Genre(htGenre);
                v.add(g);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getSubjectAreas() throws Exception {
        return getSubjectAreas(null);
    }

    public Vector getSubjectAreas(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) {
            htArgs.put("CorpusID", c.getID());
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tssubjectarea.TSListSubjectAreas",
                    htArgs);

            if (!htResults.containsKey("SUBJECTAREAS")) {
                return new Vector();
            }

            HashTree htGenres = (HashTree) htResults.get("SUBJECTAREAS");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                SubjectArea sa = new SubjectArea(htGenre);
                v.add(sa);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getNarrativeTypes() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSGetNarrativeTypes",
                    htArgs);

            if (!htResults.containsKey("NARRATIVETYPES")) {
                return new Vector();
            }

            HashTree htGenres = (HashTree) htResults.get("NARRATIVETYPES");
            Enumeration eG = htGenres.elements();
            while (eG.hasMoreElements()) {
                HashTree htGenre = (HashTree) eG.nextElement();
                v.add(htGenre);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getConceptAlerts() throws Exception {
        return getNarratives();
    }

    public Vector getSecurityGroups() throws Exception {
        return getSecurityGroups(null, null);
    }

    public Vector getSecurityGroups(String s) throws Exception {
        return getSecurityGroups(null, s);
    }

    public Vector getSecurityGroups(Corpus c) throws Exception {
        return getSecurityGroups(c, null);
    }

    public Vector getSecurityGroups(Corpus c, String Access) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) {
            htArgs.put("CorpusID", c.getID());
        }

        // invoke the API
        try {
            HashTree htResults;

            if (c != null) {
                htResults = InvokeAPI("tscorpus.TSListCorpusAccess", htArgs);
            } else {
                htResults = InvokeAPI("tsuser.TSListGroups", htArgs);
            }

            if (!htResults.containsKey("GROUPS")) {
                return new Vector();
            }

            HashTree htGroups = (HashTree) htResults.get("GROUPS");
            Enumeration eG = htGroups.elements();
            while (eG.hasMoreElements()) {
                HashTree htGroup = (HashTree) eG.nextElement();
                SecurityGroup sg = new SecurityGroup(htGroup);

                if (Access == null) {
                    v.add(sg);
                } else if (Access.equals(sg.getAccess())) {
                    v.add(sg);
                }
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getFields(String TableName) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("Table", TableName);

        Vector v = new Vector();

        try {
            org.dom4j.Document doc = InvokeDomAPI("tsother.TSGetTableInfo",
                    htArgs, false);
            Element elemRoot = doc.getRootElement();

            Element eTable = elemRoot.element("TABLE");
            if (eTable == null) {
                return new Vector();
            }

            Iterator i = eTable.elements().iterator();
            while (i.hasNext()) {
                Element eField = (Element) i.next();

                Iterator i2 = eField.elements().iterator();
                IndraField inf = new IndraField(i2);

                v.add(inf);
            }
        } catch (Exception e) {
            throw e;
        }

        return v;
    }

    public Vector getRepositories() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        try {
            HashTree htResults = InvokeAPI("tsrepository.TSGetRepositoryList",
                    htArgs);

            if (!htResults.containsKey("REPOSITORIES")) {
                throw new Exception(
                        "There are no repositories in this database.");
            }

            HashTree htReps = (HashTree) htResults.get("REPOSITORIES");
            Enumeration eR = htReps.elements();
            while (eR.hasMoreElements()) {
                HashTree htRepository = (HashTree) eR.nextElement();

                // repository type 1 is reserved, do not include in the list
                Repository R = new Repository(htRepository);
                if (!R.getID().equals("1")) {
                    v.add(R);
                }
            }

        } catch (Exception e) {
            throw e;
        }

        return v;
    }

    public void applyNodeRefresh(ITSTreeNode Node) throws Exception {
        applyNodeRefresh(Node, false, -1, null);
    }

    public void applyNodeRefresh(ITSTreeNode Node,
            boolean bAffinityNodeTitlesOnly) throws Exception {
        applyNodeRefresh(Node, bAffinityNodeTitlesOnly, -1, null);
    }

    public void applyNodeRefresh(ITSTreeNode Node,
            boolean bAffinityNodeTitlesOnly, int iMaxDocsPerNode)
            throws Exception {
        applyNodeRefresh(Node, bAffinityNodeTitlesOnly, iMaxDocsPerNode, null);
    }

    public void applyNodeRefresh(ITSTreeNode Node,
            boolean bAffinityNodeTitlesOnly, int iMaxDocsPerNode,
            String sRepositoryID) throws Exception {

        Hashtable htArgs = getArguments();
        htArgs.put("NODEID", Node.get("NODEID"));
        if (bAffinityNodeTitlesOnly) {
            htArgs.put("PropAffinityNodeTitlesOnly", "true");
        }
        if (iMaxDocsPerNode < 1000) {
            htArgs.put("MAXDOCSPERNODE", iMaxDocsPerNode + "");
        } else {
            htArgs.put("MAXDOCSPERNODE", "-1");
        }
        if (sRepositoryID != null) {
            htArgs.put("REPOSITORYID", sRepositoryID);
        }

        try {
            HashTree htResults = InvokeAPI("tsclassify.TSNodeRefreshApply",
                    htArgs);
            if (!htResults.containsKey("NUMNODESDONE")) {
                throw new Exception("Success tag missing from return.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    // get node properties with source
    public ITSTreeNode getNodeSource(ITSTreeNode n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSGetNodeSource",
                    htArgs);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("NODE");
            Iterator i = eNodes.elements().iterator();
            ITSTreeNode nSrc = new ITSTreeNode(i);

            return nSrc;
        } catch (Exception e) {
            throw e;
        }
    }

    // get node properties with source
    public String getNodeSourceAsString(ITSTreeNode n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSGetNodeSource",
                    htArgs);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("NODE");
            Iterator i = eNodes.elements().iterator();
            ITSTreeNode nSrc = new ITSTreeNode(i);

            return nSrc.get("NODESOURCE");
        } catch (Exception e) {
            throw e;
        }
    }

    public int getNodeCount(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomPostAPI(
                    "tscorpus.TSGetNodeCount", htArgs);
            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("COUNT");
            if (eSuccess == null) {
                throw new Exception("Could not get node count for corpus "
                        + c.getName());
            }

            return Integer.parseInt(eSuccess.getText());

        } catch (Exception e) {
            throw e;
        }
    }

    public void setNodeSource(ITSTreeNode n, String Source) throws Exception {
        // new 3/31/05 - Remove bibliography data from the source, if applicable
        if (Source.toLowerCase().indexOf("\nbibliography") != -1) {
            // System.out.println("*** changing bibliography from: \n"+Source);
            Source = Source.substring(0,
                    Source.toLowerCase().indexOf("\nbibliography"));
            // System.out.println("*** to: \n"+Source);
            // System.out.println(" ");
        }
        // *********************************me(andres) input source into file
        // charset UTF-8

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GZIPOutputStream gou = new GZIPOutputStream(bao);
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(gou,
                    "UTF-8"));
            out.write(Source);
            out.close();

        } catch (UnsupportedEncodingException ue) {
            System.out.println("Not supported encode  : ");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        // *********************************/me(andres)

        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));

        // invoke the API
        try {
            InvokeAPI iv = new InvokeAPI("tsnode.TSUpdateNodeSource", htArgs);
            org.dom4j.Document doc = iv.dExecuteBaoGZ(bao, false, false);

            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                throw new Exception(
                        "Node source could not be updated for node ID "
                        + n.get("NODEID"));
            }

        } catch (Exception e) {
            throw e;
        }
    }

    // Get the root node of a corpus
    // Arguments: CorpusID
    // Returns: NODE object
    public ITSTreeNode getCorpusRoot(String CorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscorpus.TSGetCorpusRoot",
                    htArgs, false);

            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("NODES");
            if (eSuccess == null) {
                throw new Exception("Corpus ID " + CorpusID
                        + " does not have a root node.");
            }

            Element eNode = eSuccess.element("NODE");

            return new ITSTreeNode(eNode.elementIterator());

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getCorpora() throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        // invoke the API
        try {
            Vector vResults = InvokeVectorAPI("tscorpus.TSListCorpora", htArgs);

            if (vResults.size() < 1) {
                System.out.println("Sorry: " + vResults.size()
                        + " elements found.");
                throw new Exception(
                        "There are no taxonomies loaded in this system.");
            }

            Vector vCorpora = (Vector) vResults.elementAt(0);

            Enumeration eC = vCorpora.elements();
            while (eC.hasMoreElements()) {
                Vector vCorpus = (Vector) eC.nextElement();
                Corpus c = new Corpus(vCorpus);

                v.add(c); // add this node to the vector
            }

            return v;

        } catch (Exception e) {
            e.printStackTrace(System.err);
            return new Vector();
        }
    }

    public java.util.List<Corpus> getCorporaList() {
        Hashtable htArgs = getArguments();
        java.util.List<Corpus> newList = new ArrayList<Corpus>();

        // invoke the API
        try {
            Vector vResults = InvokeVectorAPI("tscorpus.TSListCorpora", htArgs);

            if (vResults.size() < 1) {
                System.out.println("Sorry: " + vResults.size()
                        + " elements found.");
                throw new Exception(
                        "There are no taxonomies loaded in this system.");
            }

            Vector vCorpora = (Vector) vResults.elementAt(0);

            Enumeration eC = vCorpora.elements();
            while (eC.hasMoreElements()) {
                Vector vCorpus = (Vector) eC.nextElement();
                Corpus c = new Corpus(vCorpus);

                newList.add(c); // add this node to the vector
            }

            return newList;

        } catch (Exception e) {
            e.printStackTrace(System.err);
            return newList;
        }
    }

    public Corpus getCorpora(String sCorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", sCorpusID);
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSGetCorpusProps", htArgs);

            if (!htResults.containsKey("CORPUS")) {
                throw new Exception(
                        "There are no taxonomies loaded with corpus id "
                        + sCorpusID + ".");
            }

            HashTree htCorpus = (HashTree) htResults.get("CORPUS");
            Corpus c = new Corpus(htCorpus);

            return c;
        } catch (Exception e) {
            throw e;
        }
    }

    public Corpus editCorpusProps(Corpus c) throws Exception {
        c.setDescription(c.getDescription().replaceAll("\n", " "));

        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("CorpusName", c.getName());
        htArgs.put("CorpusDesc", c.getDescription());
        htArgs.put("CorpusActive", c.getActive());
        htArgs.put("CorpusAffinity", c.getAffinity());
        htArgs.put("CorpusTerms", c.getTerms());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSEditCorpusProps",
                    htArgs, true);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Attempt to modify corpus " + c.getName()
                        + " failed.");
            }

            return c;
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeCorpusFolder(Corpus c) throws Exception {
        removeCorpusFolder(c, null);
    }

    public void removeCorpusFolder(Corpus c, Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (g != null) {
            htArgs.put("FolderID", g.getID());
        }

        Vector v = new Vector();

        // invoke the API
        try {
            InvokeAPI("tsgenre.TSRemoveCorpusFolder", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addCorpusFolder(Corpus c, Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("FolderID", g.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            InvokeAPI("tsgenre.TSAddCorpusFolder", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeCorpusSubject(Corpus c) throws Exception {
        removeCorpusSubject(c, null);
    }

    public void removeCorpusSubject(Corpus c, SubjectArea sa) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (sa != null) {
            htArgs.put("SubjectAreaID", sa.getID());
        }

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tssubjectarea.TSRemoveCorpusSubject",
                    htArgs);
            // if (!ht.containsKey("SUCCESS")) { throw new
            // Exception("Could not remove corpus subject areas."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addCorpusSubject(Corpus c, SubjectArea sa) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("SubjectAreaID", sa.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tssubjectarea.TSAddCorpusSubject", htArgs);
            if (!ht.containsKey("SUCCESS")) {
                throw new Exception("Could not add corpus subject area.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeCorpusThesaurus(Corpus c) throws Exception {
        removeCorpusThesaurus(c, null);
    }

    public void removeCorpusThesaurus(Corpus c, Thesaurus t) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (t != null) {
            htArgs.put("ThesaurusID", t.getID());
        }

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tsthesaurus.TSRemoveThesaurusCorpus",
                    htArgs);
            // if (!ht.containsKey("SUCCESS")) { throw new
            // Exception("Could not remove corpus subject areas."); }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addCorpusThesaurus(Corpus c, Thesaurus t) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("ThesaurusID", t.getID());

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tsthesaurus.TSAddThesaurusCorpus", htArgs);
            if (!ht.containsKey("SUCCESS")) {
                throw new Exception(
                        "Could not add thesaurus relationship area.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getMusthaves(String NodeID) throws Exception {
        return getMusthaves(NodeID, "EN", "1");
    }

    public Vector getMusthaves(String NodeID, String lang) throws Exception {
        return getMusthaves(NodeID, lang, "1");
    }

    public Vector getMusthaves(String NodeID, String language, String musthaveType) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("lang", language);
        htArgs.put("musthavetype", musthaveType);
        Vector v = new Vector();

        try {
            HashTree htResults = InvokeAPI("tsmusthave.TSGetMustHaves", htArgs);
            if (!htResults.containsKey("WORDS")) {
                return v;
            }

            HashTree htWords = (HashTree) htResults.get("WORDS");
            Enumeration eG = htWords.elements();

            while (eG.hasMoreElements()) {
                HashTree htWord = (HashTree) eG.nextElement();
                v.add((String) htWord.get("WORDNAME").toString());
                // v.add((String) htWord.get("WORDNAME"));
            }
        } catch (Exception e) {
            throw e;
        }

        return v;
    }

    public String addMusthave(String NodeID, Vector terms) throws Exception {
        return addMusthave(NodeID, terms, true);
    }

    public String addMusthave(String NodeID, Vector terms,
            boolean useCommonWords) throws Exception {
        return addMusthave(NodeID, terms, useCommonWords, "EN");
    }

    public String addMusthave(String NodeID, Vector terms,
            boolean useCommonWords, String lang) throws Exception {
        String sTerms = (String) terms.elementAt(0);
        for (int i = 1; i < terms.size(); i++) {
            sTerms = sTerms + "," + terms.elementAt(i);
        }

        return addMusthave(NodeID, sTerms, useCommonWords, lang, false);
    }

    public String addMusthave(String NodeID, String terms) throws Exception {
        return addMusthave(NodeID, terms, true, "EN", false);
    }

    public String addMusthave(String NodeID, String terms,
            boolean useCommonWords) throws Exception {
        return addMusthave(NodeID, terms, useCommonWords, "EN", false);
    }

    //TODO: TEMPORAL WE NEED PUT THIS IN THE WIKIPEDIA PROCESS
    public java.util.List<String> fixSource(String nodeId, String recursively, PopupProgressBar ppb) {
        String result = "";
        Hashtable htArgs = getArguments();
        htArgs.put("nodeId", nodeId);
        htArgs.put("recursively", recursively);
        java.util.List<String> terms = new ArrayList<String>();
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSPopulateSourceWiki", htArgs, ppb);
            Element elemRoot = doc.getRootElement();

            Element eResult = elemRoot.element("RESULT");
            if (eResult == null) {
                JOptionPane.showMessageDialog(null, "Error in the process");
                result = "The process not finished ok";
            } else {
                // parse data
                java.util.List<Element> eMessages = eResult.element("TERMS").elements();

                for (Element el : eMessages) {
                    terms.add(el.getText());
                }
                result = "Process finished ok";
            }
        } catch (Exception e) {
            Log.debug("Error saving images in S3");
        }
        return terms;

        //-----------------------------------------
    }

    //TODO: remove some bad characters in the britannica source
    public java.util.List<String> fixDescBritannica(String nodeId, String recursively, PopupProgressBar ppb) {
        String result = "";
        Hashtable htArgs = getArguments();
        htArgs.put("nodeId", nodeId);
        htArgs.put("recursively", recursively);
        java.util.List<String> terms = new ArrayList<String>();
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSFixDescBritannica", htArgs, ppb);
            Element elemRoot = doc.getRootElement();

            Element eResult = elemRoot.element("RESULT");
            if (eResult == null) {
                JOptionPane.showMessageDialog(null, "Error in the process");
                result = "The process not finished ok";
            } else {
                // parse data
                java.util.List<Element> eMessages = eResult.element("TERMS").elements();

                for (Element el : eMessages) {
                    terms.add(el.getText());
                }
                result = "Process finished ok";
            }
        } catch (Exception e) {
            Log.debug("Error saving images in S3");
        }
        return terms;

        //-----------------------------------------
    }

    public String addMusthave(String NodeID, String terms,
            boolean useCommonWords, String lang, boolean bRemoveFirst)
            throws Exception {
        return addMusthave(NodeID, terms, useCommonWords, lang, bRemoveFirst, "1");
    }

    public String addMusthave(String NodeID, String terms,
            boolean useCommonWords, String lang, boolean bRemoveFirst, String musthaveType)
            throws Exception {
        if (lang == null) {
            lang = "EN";
        }

        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("lang", lang);
        htArgs.put("musthavetype", musthaveType);

        if (bRemoveFirst) {
            try {
                HashTree htResults = InvokeAPI("tsmusthave.TSRemoveMustHaves",
                        htArgs);
                if (!htResults.containsKey("SUCCESS")) {
                    throw new Exception("Must have removal failed");
                }
            } catch (Exception e) {
                // System.out.println("NodeID: " + NodeID +
                // " has no must have entries to be removed.  That's okay.");
            }
        }

        htArgs.put("Terms", terms);
        if (useCommonWords) {
            htArgs.put("common", "true");
        }

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tsmusthave.TSAddMustHaves", htArgs);
            if (!ht.containsKey("SUCCESS")) {
                throw new Exception("Could not add terms.");
            } else {
                return (String) ht.get("SUCCESS");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String removeMusthave(String NodeID, String term) throws Exception {
        return removeMusthave(NodeID, term, "EN");
    }

    public String removeMusthave(String NodeID, String term, String lang, String musthaveType)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("musthavetype", musthaveType);
        if (lang != null) {
            htArgs.put("lang", lang);
        }
        if (term != null) {
            htArgs.put("Term", term); // if terms is null, it will delete
            // everything
        }
        // invoke the API
        try {
            HashTree ht = InvokeAPI("tsmusthave.TSRemoveMustHaves", htArgs);
            if (!ht.containsKey("SUCCESS")) {
                throw new Exception("Could not remove term(s).");
            } else {
                return (String) ht.get("SUCCESS");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String removeMusthave(String NodeID, String term, String lang)
            throws Exception {
        return removeMusthave(NodeID, term, lang, "1");
    }

    public String addMusthaveRecursive(String NodeID, String depth,
            boolean useNode, boolean useParent, boolean usePhrases,
            boolean useRegex, String terms, String choiceGroupOption)
            throws Exception {
        return addMusthaveRecursive(NodeID, depth, useNode, useParent,
                usePhrases, useRegex, terms, true, choiceGroupOption);
    }

    public String addMusthaveRecursive(String NodeID, String depth,
            boolean useNode, boolean useParent, boolean usePhrases,
            boolean useRegex, String terms, boolean useCommon,
            String choiceGroupOption) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        if (depth != null) {
            htArgs.put("depth", depth);
        }
        if (useNode) {
            htArgs.put("useNode", "true");
        }
        if (useParent) {
            htArgs.put("useParent", "true");
        }
        if (usePhrases) {
            htArgs.put("usePhrases", "true");
        }
        if (terms != null) {
            htArgs.put("Terms", terms);
        }
        if (useCommon) {
            htArgs.put("common", "true");
        }

        // choice group option
        htArgs.put("choiceGroupOption", choiceGroupOption);

        StringBuffer sb = new StringBuffer("");

        if (useRegex) {
            File f = new File("C:/temp/regex.txt");
            FileInputStream fstream = null;
            try {
                fstream = new FileInputStream(f);
                DataInputStream in = new DataInputStream(fstream);
                while (in.available() != 0) {
                    sb.append(in.readLine());
                }
            } catch (Exception e) {
                sb.append("(?<=([Aa]lso known as ))[^.,;\r\n]*|(?<=([Aa]bbr. ))[^.,;\r\n]*|(?<=([Aa]bbr (for)??))[^.,;\r\n]");
                sb.append("*|(?<=([Aa]bb\\.))[^.,;\r\n]*|(?<=([Aa][Kk][Aa].))[^.,;\r\n]*|(?<=([Aa]bbreviation for.))[^.,;\r\n]");
                sb.append("*|(?<=([Ss]ee also.))[^.,;\r\n]*|(?<=([Aa]bb.))[^.,;\r\n]*|(?<=([Ss]ee.))[^.,;\r\n]*|");
                sb.append("(?<=([Aa]lso called ].))[^.,;\r\n]*|(?<=([Ss]ame as.))[^.,;\r\n]*|(?<=([Cc]ompare.))[^.,;\r\n]*");
                sb.append("|(?<=(NODETITLE.\\())[^)]*");
                System.out.println("Could not read the regex file, using: "
                        + sb.toString());
            } finally {
                if (fstream != null) {
                    try {
                        fstream.close();
                    } catch (Exception ex) {
                    }
                }
            }
            htArgs.put("Regex", sb.toString());
        }

        // invoke the API
        try {
            HashTree ht = InvokeAPI("tsmusthave.TSAddMustHavesRecursive",
                    htArgs);
            if (!ht.containsKey("SUCCESS")) {
                throw new Exception("Could not add terms.");
            } else {
                return (String) ht.get("SUCCESS");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addNodeDocument(ITSTreeNode n, Document d, String Score,
            String Summary) throws Exception {
        addNodeDocument(n.get("NODEID"), d.get("DOCUMENTID"), Score, Summary);
    }

    public void addNodeDocument(NodeDocument nd) throws Exception {
        addNodeDocument(nd.get("NODEID"), nd.get("DOCUMENTID"),
                nd.get("SCORE1"), nd.get("DOCSUMMARY"),
                nd.get("INDEXWITHINNODE"), true);
    }

    public void addNodeDocument(NodeDocument nd, boolean bRemoveFirst)
            throws Exception {
        addNodeDocument(nd.get("NODEID"), nd.get("DOCUMENTID"),
                nd.get("SCORE1"), nd.get("DOCSUMMARY"),
                nd.get("INDEXWITHINNODE"), bRemoveFirst);
    }

    public void addNodeDocument(String NodeID, String DocID, String Score,
            String Summary) throws Exception {
        addNodeDocument(NodeID, DocID, Score, Summary, "1", true);
    }

    public void addNodeDocument(String NodeID, String DocID, String Score,
            String Summary, String Index, boolean bRemoveFirst)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocID);
        htArgs.put("NodeID", NodeID);
        htArgs.put("permanent", "true");

        // System.out.println("insert into nodedocument: did: "+DocID+" nid: "+NodeID+" score1: "+Score);
        try { // invoke the API

            HashTree htResults;

            if (bRemoveFirst) {
                htResults = InvokeAPI("tsnode.TSRemoveNodeDocument", htArgs);
            }

            htArgs = getArguments();
            htArgs.put("DocumentID", DocID);
            htArgs.put("NodeID", NodeID);
            htArgs.put("docsummary", Summary);
            htArgs.put("score1", Score);
            htArgs.put("index", Index);

            htResults = InvokeAPI("tsdocument.TSAddNodeDocument", htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) {
                throw new NotAuthorized();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getThesauri() throws Exception {
        return getThesauri(null);
    }

    public Vector getThesauri(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();

        if (c != null) {
            htArgs.put("CorpusID", c.getID());
        }

        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults;
            if (c == null) {
                htResults = InvokeAPI("tsthesaurus.TSGetThesaurusList", htArgs);
            } else {
                htResults = InvokeAPI("tscorpus.TSGetCorpusThesaurus", htArgs);
            }

            if (!htResults.containsKey("THESAURI")) {
                return new Vector(); // throw new
                // Exception("There are no thesauri loaded in this system.");
            }

            HashTree htThesauri = (HashTree) htResults.get("THESAURI");
            Enumeration eT = htThesauri.elements();
            while (eT.hasMoreElements()) {
                HashTree htThesaurus = (HashTree) eT.nextElement();
                Thesaurus t = new Thesaurus(htThesaurus);

                v.add(t);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getThesaurusWords(String ThesaurusID) throws Exception {
        return getThesaurusWords(ThesaurusID, null);
    }

    public Vector getThesaurusWords(String ThesaurusID, String Like)
            throws Exception {
        return getThesaurusWords(ThesaurusID, Like, false);
    }

    public Vector getThesaurusWords(String ThesaurusID, String Like,
            boolean exactMatch) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        if (Like != null) {
            htArgs.put("Like", Like);
        }
        if (exactMatch) {
            htArgs.put("ExactMatch", "true");
        }
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsthesaurus.TSGetWords", htArgs);

            if (!htResults.containsKey("WORDS")) {
                throw new Exception(
                        "There are no thesaurus words loaded in this thesaurus.");
            }

            HashTree htThesWords = (HashTree) htResults.get("WORDS");
            Enumeration eT = htThesWords.elements();
            while (eT.hasMoreElements()) {
                HashTree htWord = (HashTree) eT.nextElement();
                ThesaurusWord tw = new ThesaurusWord(htWord);

                v.add(tw);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getNodeSignatures(ITSTreeNode n) throws Exception {
        return getNodeSignatures(n, "EN");
    }

    public Vector getNodeSignatures(ITSTreeNode n, String language)
            throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("lang", language);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSGetNodeSigs", htArgs);

            if (!htResults.containsKey("SIGNATURES")) {
                return new Vector();
            }

            HashTree htSignatures = (HashTree) htResults.get("SIGNATURES");
            Enumeration eS = htSignatures.elements();
            while (eS.hasMoreElements()) {
                HashTree htSignature = (HashTree) eS.nextElement();
                Signature s = new Signature(htSignature);

                v.add(s);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getSynonyms(Vector Thesauri, String Word) {
        Vector v = new Vector();

        try {
            for (int i = 0; i < Thesauri.size(); i++) {
                Thesaurus t = (Thesaurus) Thesauri.elementAt(i);
                Vector synonyms = getSynonyms(t.getID(), Word);

                for (int j = 0; j < synonyms.size(); j++) {
                    ThesaurusWord tw = (ThesaurusWord) synonyms.elementAt(j);
                    v.add(tw);
                }
            }

        } catch (Exception e) {
            return new Vector();
        }

        return v;
    }

    public Vector getSynonyms(String ThesaurusID, String Word) {
        try {
            Vector vWords = getThesaurusWords(ThesaurusID, Word);
            if (vWords.size() == 0) {
                return new Vector();
            } // word may not exist yet

            ThesaurusWord tw = (ThesaurusWord) vWords.elementAt(0);

            return getSynonyms(ThesaurusID, new Integer(tw.getID()).intValue());
        } catch (Exception e) {
            return new Vector();
        } // none were found, return empty vector
    }

    public Vector getSynonyms(String ThesaurusID, int WordID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("WordID", "" + WordID);
        Vector v = new Vector();

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsthesaurus.TSGetWordProps", htArgs);

            if (!htResults.containsKey("WORDS")) {
                throw new Exception(
                        "There are no thesaurus words loaded in this thesaurus.");
            }

            HashTree htThesWords = (HashTree) htResults.get("WORDS");
            Enumeration eT = htThesWords.elements();
            while (eT.hasMoreElements()) {
                HashTree htWord = (HashTree) eT.nextElement();
                ThesaurusWord tw = new ThesaurusWord(htWord);

                v.add(tw);
            }
            return v;

        } catch (Exception e) {
            throw e;
        }
    }

    public void removeAnchorTerm(String ThesaurusID, String WordID)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("WordID", WordID);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSRemoveWordAnchor",
                    htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) {
                throw new NotAuthorized();
            }
            if (htResults.containsKey("DEBUG")) {
                throw new Exception(
                        "Thesaurus term could not be removed, see log file for more details.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeRelationship(String ThesaurusID, String AnchorID,
            String ObjectID, String Relationship) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("WordID1", AnchorID);
        htArgs.put("WordID2", ObjectID);
        htArgs.put("Relationship", Relationship);

        try { // invoke the API
            HashTree htResults = InvokeAPI(
                    "tsthesaurus.TSRemoveWordRelationship", htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) {
                throw new NotAuthorized();
            }
            if (htResults.containsKey("DEBUG")) {
                throw new Exception(
                        "Thesaurus relationship could not be removed, see log file for more details.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void addRelationship(String ThesaurusID, String AnchorName,
            String ObjectName, String Relationship) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("Word1", AnchorName);
        htArgs.put("Word2", ObjectName);
        htArgs.put("Relationship", Relationship);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSAddWordRelationship",
                    htArgs, false);

            if (htResults.containsKey("NOTAUTHORIZED")) {
                throw new NotAuthorized();
            }
            if (htResults.containsKey("DEBUG")) {
                throw new Exception((String) htResults.get("DEBUG"));
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public SubjectArea addSubjectArea(SubjectArea sa) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("SubjectName", sa.getName());

        if (sa.getActive()) {
            htArgs.put("SubjectAreaStatus", "1");
        } else {
            htArgs.put("SubjectAreaStatus", "0");
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tssubjectarea.TSCreateSubjectArea",
                    htArgs);

            if (!htResults.containsKey("SUBJECTAREA")) {
                throw new Exception("Could not create a new subject area.");
            }

            sa = new SubjectArea((HashTree) htResults.get("SUBJECTAREA"));

            return sa;
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeSubjectArea(SubjectArea sa) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("SubjectAreaID", sa.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tssubjectarea.TSRemoveSubjectArea",
                    htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Could not remove a subject area.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Genre addFolder(Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("FolderName", g.getName());

        if (g.getActive()) {
            htArgs.put("FolderStatus", "1");
        } else {
            htArgs.put("FolderStatus", "0");
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsgenre.TSCreateFolder", htArgs);

            if (!htResults.containsKey("GENRE")) {
                throw new Exception("Could not create a new folder.");
            }

            g = new Genre((HashTree) htResults.get("GENRE"));

            return g;
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeFolder(Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("FolderID", g.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsgenre.TSRemoveFolder", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Could not remove a folder.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void editRelationship(String OldName, String NewName)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("OldName", OldName);
        htArgs.put("NewName", NewName);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSEditWordName", htArgs);

            if (htResults.containsKey("NOTAUTHORIZED")) {
                throw new NotAuthorized();
            }
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception(
                        "Thesaurus relationship could not be remamed, see log file for more details.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveThesaurusProps(String ThesaurusID, String ThesaurusName)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", ThesaurusID);
        htArgs.put("ThesaurusName", ThesaurusName);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSSaveThesaurusProps",
                    htArgs);

            if (htResults.containsKey("DEBUG")) {
                throw new Exception((String) htResults.get("DEBUG"));
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public Thesaurus createThesaurus(String ThesaurusName) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusName", ThesaurusName);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSCreateThesaurus",
                    htArgs);

            if (htResults.containsKey("DEBUG")) {
                throw new Exception((String) htResults.get("DEBUG"));
            }

            return new Thesaurus((HashTree) htResults.get("THESAURUS"));
        } catch (Exception e) {
            throw e;
        }
    }

    public void synchronizeThesaurus() throws Exception {
        Hashtable htArgs = getArguments();

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsthesaurus.TSThesaurusSynch",
                    htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addRefreshNode(String NodeID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsclassify.TSAddRefreshNode",
                    htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("API level failure.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeRefreshNode(String NodeID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        try { // invoke the API
            HashTree htResults = InvokeAPI(
                    "tsclassify.TSDeleteRefreshNodeList", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("API level failure.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getRefreshNodeList() throws Exception {
        return getRefreshNodeList(null);
    }

    public Vector getRefreshNodeList(Corpus c) throws Exception {
        Hashtable htArgs = getArguments();
        Vector v = new Vector();

        if (c != null) {
            htArgs.put("CorpusID", c.getID());
        }

        try { // invoke the API
            HashTree htResults = InvokeAPI("tsclassify.TSGetRefreshNodes",
                    htArgs);
            if (!htResults.containsKey("NODES")) {
                return new Vector();
            }

            // Process NODE RESULTS
            HashTree htNodes = (HashTree) htResults.get("NODES");

            Enumeration eN = htNodes.elements();
            while (eN.hasMoreElements()) {
                HashTree htNode = (HashTree) eN.nextElement();
                ITSTreeNode n = new ITSTreeNode(htNode);

                v.add(n); // add this node to the vector
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public String getNodeCorpus(String NodeID) throws Exception {
        ITSTreeNode n = getNodeProps(NodeID);
        return n.get("CORPUSID");
    }

    // TSListAllNodesWithChildCount
    public Vector getNodesWithChildren(Corpus c) throws Exception {
        return getNodesWithChildren(c, -1);
    }

    public Vector getNodesWithChildren(Corpus c, int Depth) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (Depth >= 0) {
            htArgs.put("Depth", Depth + "");
        }

        Vector v = new Vector();

        try {
            Hashtable htResults = InvokeAPI(
                    "tsnode.TSListAllNodesWithChildCount", htArgs);
            if (!htResults.containsKey("NODES")) {
                throw new Exception("Invalid corpus identifier specified.");
            }

            HashTree htNodes = (HashTree) htResults.get("NODES");
            Enumeration eH = htNodes.elements();
            while (eH.hasMoreElements()) {
                HashTree htNode = (HashTree) eH.nextElement();
                ITSTreeNode n = new ITSTreeNode(htNode);
                n.set("CLIENTID", (String) htNode.get("COUNT"));

                v.add(n);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
            throw e;
        }

        return v;
    }

    // TSListAllNodesWithSigCount
    public Vector getNodesWithSignatures(Corpus c) throws Exception {
        return getNodesWithChildren(c, -1);
    }

    public Vector getNodesWithSignatures(Corpus c, int Depth) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (Depth >= 0) {
            htArgs.put("Depth", Depth + "");
        }

        Vector v = new Vector();

        try {
            Hashtable htResults = InvokeAPI(
                    "tsnode.TSListAllNodesWithSigCount", htArgs);
            if (!htResults.containsKey("NODES")) {
                return new Vector();
            }

            HashTree htNodes = (HashTree) htResults.get("NODES");
            Enumeration eH = htNodes.elements();
            while (eH.hasMoreElements()) {
                HashTree htNode = (HashTree) eH.nextElement();
                ITSTreeNode n = new ITSTreeNode(htNode);
                n.set("CLIENTID", (String) htNode.get("COUNT"));

                v.add(n);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
            throw e;
        }

        return v;
    }

    public Vector getNodesWithDocuments(Corpus c) throws Exception {
        return getNodesWithDocuments(c, -1);
    }

    public Vector getNodesWithDocuments(Corpus c, int Depth) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        if (Depth >= 0) {
            htArgs.put("Depth", Depth + "");
        }

        Vector v = new Vector();

        try {
            Hashtable htResults = InvokeAPI("tsnode.TSListAllNodesWithCount",
                    htArgs);
            if (!htResults.containsKey("NODES")) {
                return new Vector(); // no results found
            }
            HashTree htNodes = (HashTree) htResults.get("NODES");
            Enumeration eH = htNodes.elements();
            while (eH.hasMoreElements()) {
                HashTree htNode = (HashTree) eH.nextElement();
                ITSTreeNode n = new ITSTreeNode(htNode);
                n.set("CLIENTID", (String) htNode.get("COUNT"));

                v.add(n);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
            throw e;
        }

        return v;
    }

    /**
     * delete musthaves recursively from any node
     *
     * @param nodeid
     * @throws Exception
     */
    public void deleteMH(String nodeid) throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("nodeid", nodeid);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsmusthave.TSDeleteMustHavesR",
                    htArguments);
            if (!htResults.containsKey("SUCCESS")) {
                JOptionPane.showMessageDialog(null, "Proccess error");
            } else {
                JOptionPane.showMessageDialog(null, "Proccess ok");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * delete images recursively from any node
     *
     * @param nodeid
     * @throws Exception
     */
    public void deleteImages(String nodeid) throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("nodeid", nodeid);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSDeleteImagesR",
                    htArguments);
            if (!htResults.containsKey("SUCCESS")) {
                JOptionPane.showMessageDialog(null, "Proccess error");
            } else {
                JOptionPane.showMessageDialog(null, "Proccess ok");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * delete signatures recursively from any node
     *
     * @param nodeid
     * @throws Exception
     */
    public void deleteSignatures(String nodeid) throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("nodeid", nodeid);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSDeleteSignaturesR",
                    htArguments);
            if (!htResults.containsKey("SUCCESS")) {
                JOptionPane.showMessageDialog(null, "Proccess error");
            } else {
                JOptionPane.showMessageDialog(null, "Proccess ok");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    // Given a NODEID, get the node properties
    // Arguments: NodeID
    // Returns: NODE object
    public ITSTreeNode getNodeProps(String NodeID) throws Exception {
        String sCQL = "SELECT <NODE> WHERE NODEID = " + NodeID;

        Vector vNode = CQL(sCQL);
        if (vNode.size() == 0) {
            throw new Exception("NODE ID " + NodeID + " does not exist.");
        } else {
            return (ITSTreeNode) vNode.elementAt(0);
        }
    }

    public void populateMustHavesFromSource(String nodeId, PopupProgressBar ppb) {
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeId);

        org.dom4j.Document doc;
        try {
            doc = InvokeDomStatusAPI("tsnode.TSPopulateMustHavesFromSource",
                    htArgs, ppb);
            Element elemRoot = doc.getRootElement();
            Element eSuccess = elemRoot.element("SUCCESS");

            if (eSuccess != null) {
                JOptionPane.showMessageDialog(null, "Process finished ok");
            } else {
                JOptionPane.showMessageDialog(null, "Process finished error");
            }

        } catch (Exception e) {
            Log.debug("Error populateMustHavesFromSource");
            e.printStackTrace();
        }

    }

    public void populateSignatures(String nodeId, PopupProgressBar ppb) {
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeId);

        org.dom4j.Document doc;
        try {
            doc = InvokeDomStatusAPI("tsnode.TSAddSignatureFromSource", htArgs,
                    ppb);
            Element elemRoot = doc.getRootElement();
            Element eSuccess = elemRoot.element("SUCCESS");

            if (eSuccess != null) {
                JOptionPane.showMessageDialog(null, "Process finished ok");
            } else {
                JOptionPane.showMessageDialog(null, "Process finished error");
            }

        } catch (Exception e) {
            Log.debug("Error populateMustHavesFromSource");
            e.printStackTrace();
        }
    }

    public Vector getAllNodesFromNodeID(String corpusId, String nodeId)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", corpusId);
        htArgs.put("NodeID", nodeId);

        Vector v = new Vector(); // return struct

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI(
                    "tscorpus.TSListCorpusNodeContents", htArgs, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) {
                return new Vector();
            }

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                ITSTreeNode n = new ITSTreeNode(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getNodeTree(String NodeID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        Vector v = new Vector(); // return struct

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSGetNodeTree",
                    htArgs, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) {
                return new Vector();
            }

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                ITSTreeNode n = new ITSTreeNode(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getRelatedNodes(String DocumentID, String DocURL)
            throws Exception {
        return getRelatedNodes(DocumentID, DocURL, "50.0");
    }

    public Vector getRelatedNodes(String DocumentID, String DocURL,
            String ScoreThreshold) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        htArgs.put("DocURL", DocURL);
        htArgs.put("ScoreThreshold", ScoreThreshold);

        Vector v = new Vector(); // return struct

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI(
                    "tsdocument.TSGetRelatedNodes", htArgs, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) {
                return new Vector();
            }

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                ITSTreeNode n = new ITSTreeNode(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Document getDocProps(String DocumentID) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCUMENTID = " + DocumentID);
        if (v.size() == 0) {
            throw new Exception("No documents found.");
        }
        return (Document) v.elementAt(0);
    }

    public Document getDocPropsByURL(String DocURL) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCURL = '" + DocURL + "'");
        if (v.size() == 0) {
            throw new Exception("No documents found.");
        }
        return (Document) v.elementAt(0);
    }

    public Vector getSimilarDocuments(String DocumentID) throws Exception {
        return getSimilarDocuments(DocumentID, true);
    }

    public Vector getSimilarDocuments(String DocumentID, boolean useCache)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        if (useCache) {
            htArgs.put("UseCache", "1");
        }

        Vector v = new Vector();

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI(
                    "tsdocument.TSGetSimilarDocumentsByDocumentID", htArgs,
                    false);
            Element elemRoot = doc.getRootElement();

            Element eDocs = elemRoot.element("DOCUMENTS");
            if (eDocs == null) {
                return new Vector();
            }

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                Iterator i2 = eDoc.elements().iterator();
                Document d = new Document(i2);

                v.add(d);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector getNarratives() throws Exception {
        Hashtable htArgs = getArguments();

        Vector v = new Vector();

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsdocument.TSGetNarratives",
                    htArgs, false);
            Element elemRoot = doc.getRootElement();

            Element eDocs = elemRoot.element("DOCUMENTS");
            if (eDocs == null) {
                return new Vector();
            }

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                Iterator i2 = eDoc.elements().iterator();
                Document d = new Document(i2);

                v.add(d);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Document getDocPropsLikeURL(String DocURL) throws Exception {
        Vector v = CQL("SELECT <DOCUMENT> WHERE DOCURL LIKE '%%" + DocURL
                + "%%'");
        if (v.size() == 0) {
            throw new Exception("No documents found.");
        }
        return (Document) v.elementAt(0);
    }

    // Add and Edit document converted to use a POST, because document summaries
    // are sometimes too long
    public Document addDocument(String DocumentURL, Hashtable NameValPairs)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocURL", DocumentURL);

        Enumeration eNV = NameValPairs.keys();
        while (eNV.hasMoreElements()) {
            String sKey = (String) eNV.nextElement();
            String sVal = (String) NameValPairs.get(sKey);

            htArgs.put(sKey, sVal);
        }

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomPostAPI(
                    "tsdocument.TSAddDocument", htArgs);
            Element elemRoot = doc.getRootElement();

            Element eDocument = elemRoot.element("DOCUMENT");
            if (eDocument == null) {
                return (Document) getDocPropsByURL(DocumentURL);
            }

            return new Document(eDocument.elements().iterator());

        } catch (Exception e) {
            throw e;
        }
    }

    public Document editDocument(Document d) throws Exception {
        Hashtable NameValPairs = new Hashtable();
        if ((!d.get("GENREID").equals("null"))
                && (!d.get("GENREID").equals(""))) {
            NameValPairs.put("genreid", d.get("GENREID"));
        }
        if ((!d.get("DOCTITLE").equals("null"))
                && (!d.get("DOCTITLE").equals(""))) {
            NameValPairs.put("doctitle", d.get("DOCTITLE"));
        }
        if ((!d.get("DOCUMENTSUMMARY").equals("null"))
                && (!d.get("DOCUMENTSUMMARY").equals(""))) {
            NameValPairs.put("documentsummary", d.get("DOCUMENTSUMMARY"));
        }
        if ((!d.get("DOCURL").equals("null")) && (!d.get("DOCURL").equals(""))) {
            NameValPairs.put("docurl", d.get("DOCURL"));
        }
        if ((!d.get("DATELASTFOUND").equals("null"))
                && (!d.get("DATELASTFOUND").equals(""))) {
            NameValPairs
                    .put("datelastfound", d.FixDate(d.get("DATELASTFOUND")));
        }
        if ((!d.get("VISIBLE").equals("null"))
                && (!d.get("VISIBLE").equals(""))) {
            NameValPairs.put("visible", d.get("VISIBLE"));
        }

        return editDocument(d.get("DOCUMENTID"), NameValPairs);
    }

    public Document editDocument(String DocumentID, Hashtable NameValPairs)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);

        Enumeration eNV = NameValPairs.keys();
        while (eNV.hasMoreElements()) {
            String sKey = (String) eNV.nextElement();
            String sVal = (String) NameValPairs.get(sKey);

            htArgs.put(sKey, sVal);
        }

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomPostAPI(
                    "tsdocument.TSEditDocument", htArgs);
            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                throw new Exception("Document modification failed.");
            }

            return (Document) getDocProps(DocumentID);
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNodeDocument(String DocumentID, String NodeID,
            String DocURL, String DocTitle, String DocSummary, String Score)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        htArgs.put("NodeID", NodeID);
        htArgs.put("Score1", Score);
        htArgs.put("DocTitle", DocTitle);
        htArgs.put("DocURL", DocURL);
        htArgs.put("DocSummary", DocSummary);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSEditDocProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public ITSTreeNode addNode(ITSTreeNode n) throws Exception {
        User u = ITS.getUser();
        IndraLicense license = u.getLicense();

        // if IDRAC flag is active, we must first determine whether another
        // node already exists with the same name .. link nodes may contain the
        // same name
        if (false
                && ((n.get("NODEID").equals(n.get("LINKNODEID"))) || (n
                .get("NODEID").equals("-1")))
                && (!n.get("NODETITLE").equals("Untitled Topic"))) {
            Vector v = CQL("SELECT <NODE> WHERE NODETITLE = '"
                    + n.get("NODETITLE") + "' AND CORPUSID = "
                    + n.get("CORPUSID"));
            if (v.size() > 0) {
                int i = JOptionPane
                        .showConfirmDialog(
                        null,
                        "A topic with the name "
                        + n.get("NODETITLE")
                        + " already exists in this taxonomy?  Continue?",
                        "Confirm", JOptionPane.YES_NO_OPTION);
                if (i != 0) {
                    throw new TopicExistsError();
                }
            }
        }

        Hashtable htArguments = getArguments();

        htArguments.put("ParentID", n.get("PARENTID"));
        htArguments.put("NodeSize", n.get("NODESIZE"));
        htArguments.put("NodeDesc", n.get("NODEDESC"));
        htArguments.put("NodeTitle", n.get("NODETITLE"));
        htArguments.put("CorpusID", n.get("CORPUSID"));

        if ((n.get("DEPTHFROMROOT") != null)
                && (!n.get("DEPTHFROMROOT").equals(""))) {
            htArguments.put("DepthFromRoot", n.get("DEPTHFROMROOT"));
        }

        if ((n.get("NODEID") != null) && (!n.get("NODEID").equals(""))) {
            if (!n.get("NODEID").equals("-1")) {
                htArguments.put("LinkNodeID", n.get("LINKNODEID"));
            }
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSAddNode", htArguments);
            return getNodeProps((String) htResults.get("NODEID"));
        } catch (NotAuthorized ae) {
            throw ae;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addNodeFromTopicMap(Node n) throws Exception {
        Hashtable htArguments = getArguments();

        htArguments.put("NodeID", n.get("NODEID"));
        htArguments.put("ParentID", n.get("PARENTID"));
        htArguments.put("NodeSize", n.get("NODESIZE"));
        // htArguments.put("NodeDesc", n.get("NODEDESC"));
        htArguments.put("NodeTitle", n.get("NODETITLE"));
        htArguments.put("CorpusID", n.get("CORPUSID"));
        if (n.get("NODEINDEXWITHINPARENT").equals("")) {
            n.set("NODEINDEXWITHINPARENT", "1");
        }
        htArguments
                .put("NodeIndexWithinParent", n.get("NODEINDEXWITHINPARENT"));
        htArguments.put("DepthFromRoot", n.get("DEPTHFROMROOT"));

        if ((n.get("LINKNODEID") != null) && (!n.get("LINKNODEID").equals(""))) {
            if (!n.get("LINKNODEID").equals("-1")) {
                htArguments.put("LinkNodeID", n.get("LINKNODEID"));
            }
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSAddNode", htArguments,
                    false);
            if (!htResults.containsKey("NODEID")) {
                throw new Exception("Error in response from XML");
            }
        } catch (NotAuthorized ae) {
            throw ae;
        } catch (Exception e) {
            throw e;
        }

        saveSignatures(n, n.getSignatures(), false);
    }

    public ITSTreeNode emptyNode(ITSTreeNode Parent) {
        ITSTreeNode n = new ITSTreeNode();
        n.set("NODEID", "-1");
        n.set("PARENTID", Parent.get("NODEID"));
        n.set("NODESIZE", "100");
        n.set("DEPTHFROMROOT", "0");
        n.set("NODEDESC", " ");
        n.set("NODETITLE", "Untitled Topic");
        n.set("CORPUSID", Parent.get("CORPUSID"));

        return n;
    }

    public ITSTreeNode emptyNode(String sCorpusID) {
        return emptyNode(sCorpusID, "Untitled Topic");
    }

    public ITSTreeNode emptyNode(String sCorpusID, String sNodeTitle) {
        ITSTreeNode n = new ITSTreeNode();
        n.set("PARENTID", "-1");
        n.set("NODESIZE", "1");
        n.set("NODEDESC", " ");
        n.set("DEPTHFROMROOT", "0");
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", sCorpusID);

        return n;
    }

    public Corpus addCorpus(String sCorpusName) throws Exception {
        return addCorpus(sCorpusName, sCorpusName);
    }

    public Corpus addCorpus(String sCorpusName, String sNodeName)
            throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("CorpusName", sCorpusName);
        htArguments.put("CorpusActive", "1");

        String sCorpusID = null;

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSCreateCorpus",
                    htArguments);
            sCorpusID = (String) htResults.get("CORPUSID");

            if (sCorpusID == null) {
                return null;
            }

            setCorpusSecurity(sCorpusID);

            ITSTreeNode n = emptyNode(sCorpusID, sNodeName);
            addNode(n);

            return getCorpora(sCorpusID);
        } catch (Exception e) {
            throw e;
        }
    }

    public Corpus addCorpus(String sCorpusName, int iCorpusID) throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("CorpusName", sCorpusName);
        htArguments.put("CorpusID", "" + iCorpusID);
        htArguments.put("CorpusActive", "1");
        htArguments.put("RocSettingID", "1");

        String sCorpusID = null;

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSCreateCorpus",
                    htArguments);
            sCorpusID = (String) htResults.get("CORPUSID");

            if (sCorpusID == null) {
                return null;
            }

            setCorpusSecurity(sCorpusID);

            return getCorpora(sCorpusID);
        } catch (Exception e) {
            throw e;
        }
    }

    public Corpus addCorpusWithoutRoot(String sCorpusName) throws Exception {
        return addCorpus(sCorpusName, sCorpusName);
    }

    public Corpus addCorpusWithoutRoot(String sCorpusName, String sNodeName)
            throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("CorpusName", sCorpusName);
        htArguments.put("CorpusActive", "1");

        String sCorpusID = null;

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSCreateCorpus",
                    htArguments);
            sCorpusID = (String) htResults.get("CORPUSID");

            if (sCorpusID == null) {
                return null;
            }

            setCorpusSecurity(sCorpusID);

            return getCorpora(sCorpusID);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean orthogonalizeCorpus(String sCorpusID) throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("CorpusID", sCorpusID);

        // add preferences to argument list
        User u = ITS.getUser();
        htArguments.put("IncludeNumbers", u.getIncludeNumbers().toString());
        htArguments.put("PhraseLenMin", u.getPhraseLenMin().toString());
        htArguments.put("PhraseLenMax", u.getPhraseLenMax().toString());
        htArguments.put("WordFreqMaxForSig", u.getWordFreqMaxForSig()
                .toString());
        htArguments.put("NumParentTitleTermAsSigs", u
                .getNumParentTitleTermAsSigs().toString());

        String installoptions = "000";
        if (u.isHKSet()) {
            installoptions = "1" + installoptions.substring(1);
        }
        if (u.isLingpipeSet()) {
            installoptions = installoptions.substring(0, 1) + "1"
                    + installoptions.substring(2, 3);
        }
        if (u.isCapitalSet()) {
            installoptions = installoptions.substring(0, 2) + "1";
        }

        if (installoptions.equals("000")) {
            installoptions = "100";
        }

        System.out.println("install options: " + installoptions);

        htArguments.put("installoptions", installoptions);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSInstallCorpus",
                    htArguments);
            String status = (String) htResults
                    .get("TSINSTALLCORPUS_API_CALL_RESULT");

            if (status.toUpperCase().equals("SUCCESS")) {
                return true;
            }
            return false;

        } catch (Exception e) {
            throw e;
        }
    }

    public Corpus addCorpusFromTopicMap(Corpus c) throws Exception {
        Hashtable htArguments = getArguments();
        if (c.getID() != null) {
            htArguments.put("CorpusID", c.getID());
        }
        htArguments.put("CorpusName", c.getName());
        if (c.getDescription() != null) {
            htArguments.put("CorpusDesc", c.getDescription());
        }
        htArguments.put("CorpusActive", "1");

        String sCorpusID = null;

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tscorpus.TSCreateCorpus",
                    htArguments);
            sCorpusID = (String) htResults.get("CORPUSID");

            if (sCorpusID == null) {
                return null;
            }

            setCorpusSecurity(sCorpusID);

            return getCorpora(sCorpusID);
        } catch (Exception e) {
            throw e;
        }
    }

    /*
     * String sCorpus = (String) props.get("Corpus", true); String sCorpusName =
     * (String) props.get("CorpusName", "Copy of ID "+sCorpus); String copySigs
     * = (String) props.get("copySignatures"); String copySource = (String)
     * props.get("copySource"); String copyDocs = (String)
     * props.get("copyDocs"); String sShowStatus = (String)
     * props.get("ShowStatus");
     */
    public Corpus copyCorpus(Corpus c, boolean copySignatures,
            boolean copySource, boolean copyDocuments, PopupProgressBar ppb)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("Corpus", c.getID());
        htArgs.put("CorpusName", c.getName());
        htArgs.put("CorpusDesc", c.getDescription());
        htArgs.put("CorpusActive", "1");
        htArgs.put("ShowStatus", "true");

        if (copySignatures) {
            htArgs.put("copySignatures", "true");
        }
        if (copySource) {
            htArgs.put("copySource", "true");
        }
        if (copyDocuments) {
            htArgs.put("copyDocs", "true");
        }

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tscorpus.TSCopyCorpus", htArgs, ppb, false);
            Element elemRoot = doc.getRootElement();

            Element eCorpus = elemRoot.element("CORPUSID");
            if (eCorpus == null) {
                throw new Exception("Failed to copy corpus " + c.getName());
            }

            String sCorpusID = eCorpus.getText();

            setCorpusSecurity(sCorpusID);

            c.setID(sCorpusID);
            return c;

        } catch (Exception e) {
            throw e;
        }
    }

    /*
     * String sNodeID = (String) props.get("NodeID", true); String sReplace =
     * (String) props.get("replace", true); String replaceTitle = (String)
     * props.get("replacetitle"); String replaceSource = (String)
     * props.get("replacesource"); String sRecurse = (String)
     * props.get("recursive"); String sShowStatus = (String)
     * props.get("ShowStatus");
     */
    public void searchAndReplace(ITSTreeNode n, Hashtable searchreplace,
            boolean replaceTitle, boolean replaceSource, PopupProgressBar ppb)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        if (replaceTitle) {
            htArgs.put("replaceTitle", "true");
        }
        if (replaceSource) {
            htArgs.put("replaceSource", "true");
        }
        htArgs.put("recursive", "true");
        htArgs.put("ShowStatus", "true");

        String replace = "";
        int loop = 0;

        Enumeration eH = searchreplace.keys();
        while (eH.hasMoreElements()) {
            String key = (String) eH.nextElement();
            String val = (String) searchreplace.get(key);

            if (loop > 0) {
                replace = replace + "@@";
            }
            loop++;

            replace = replace + key + "||" + val;
        }

        htArgs.put("replace", replace);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSSearchReplaceNode", htArgs, ppb, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("SUCCESS");
            if (eNodes == null) {
                throw new Exception("Failed to search/replace node: "
                        + n.get("NODETITLE"));
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteCorpus(Corpus c, PopupProgressBar ppb) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", c.getID());
        htArgs.put("permanently", "1");

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tscorpus.TSDeleteCorpus", htArgs, ppb, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("SUCCESS");
            if (eNodes == null) {
                throw new Exception("Failed to delete corpus " + c.getName());
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public void setCorpusAdminSecurity(Corpus c, SecurityGroup g)
            throws Exception {
        Hashtable htArguments = getArguments();

        // after corpus is created, add the admin group
        htArguments.put("CorpusID", c.getID());
        htArguments.put("Access", "1");
        htArguments.put("GroupID", g.getID()); // Admin group
        HashTree htResults = InvokeAPI("tscorpus.TSEditCorpusAccess",
                htArguments);

        if (!htResults.containsKey("SUCCESS")) {
            throw new Exception("cannot change corpus permissions");
        }

        return;
    }

    public void setCorpusReadSecurity(Corpus c, SecurityGroup g)
            throws Exception {
        Hashtable htArguments = getArguments();

        // after corpus is created, add the admin group
        htArguments.put("CorpusID", c.getID());
        htArguments.put("Access", "0");
        htArguments.put("GroupID", g.getID()); // Admin group
        HashTree htResults = InvokeAPI("tscorpus.TSEditCorpusAccess",
                htArguments);

        if (!htResults.containsKey("SUCCESS")) {
            throw new Exception("cannot change corpus permissions");
        }

        return;
    }

    public void revokeCorpusSecurity(Corpus c, SecurityGroup g)
            throws Exception {
        Hashtable htArguments = getArguments();

        // after corpus is created, add the admin group
        htArguments.put("CorpusID", c.getID());
        htArguments.put("Access", "-1");
        htArguments.put("GroupID", g.getID()); // Admin group
        HashTree htResults = InvokeAPI("tscorpus.TSEditCorpusAccess",
                htArguments);

        if (!htResults.containsKey("SUCCESS")) {
            throw new Exception("cannot change corpus permissions");
        }

        return;
    }

    public void setCorpusSecurity(String sCorpusID) throws Exception {
        Hashtable htArguments = getArguments();

        // after corpus is created, add the admin group
        htArguments.put("CorpusID", sCorpusID);
        htArguments.put("Access", "1");
        htArguments.put("GroupID", getAdminGroup() + ","
                + getDomainController()); // Admin group
        HashTree htResults = InvokeAPI("tscorpus.TSEditCorpusAccess",
                htArguments);

        if (!htResults.containsKey("SUCCESS")) {
            throw new Exception("cannot change corpus permissions");
        }

        return;
    }

    public ITSTreeNode copyNode(ITSTreeNode n) throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("NodeID", n.get("NODEID"));
        htArguments.put("ParentID", n.get("PARENTID"));

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSCopyNode", htArguments);
            if (!htResults.containsKey("NODEID")) {
                throw new Exception(
                        "error: attempt to copy a node that was deleted");
            }
            return getNodeProps((String) htResults.get("NODEID"));
        } catch (Exception e) {
            throw e;
        }
    }

    public ITSTreeNode copyNode(ITSTreeNode n, PopupProgressBar ppb)
            throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("NodeID", n.get("NODEID"));
        htArguments.put("ParentID", n.get("PARENTID"));
        htArguments.put("ShowStatus", "true");

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI("tsnode.TSCopyNode",
                    htArguments, ppb, true);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("NODEID");
            if (eNodes == null) {
                throw new Exception(
                        "error: attempt to copy a node that was deleted");
            }

            return getNodeProps(eNodes.getText());

        } catch (Exception e) {
            throw e;
        }
    }

    public void organizeIndexes(String ParentID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", ParentID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI(
                    "tsnode.TSOrganizeNodeIndexByParent", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                System.out.println("Could not reorganize the indexes.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNodeParent(String NodeID, String ParentID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("ParentID", ParentID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public ITSTreeNode moveNode(String NodeID, String ParentID)
            throws Exception {
        Hashtable htArguments = getArguments();
        htArguments.put("NodeID", NodeID);
        htArguments.put("ParentID", ParentID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSMoveNode", htArguments);
            if (!htResults.containsKey("NODE")) {
                throw new Exception(
                        "error: attempt to move a node that was deleted");
            }
            return getNodeProps(NodeID);
        } catch (Exception e) {
            throw e;
        }
    }

    public ITSTreeNode editNodeParent(String NodeID, String ParentID,
            String Index) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("ParentID", ParentID);
        htArgs.put("NodeIndexWithinParent", Index);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("could not edit node properties");
            }
            return getNodeProps(NodeID);
        } catch (Exception e) {
            throw e;
        }
    }

    public ITSTreeNode editNodeParentCorpus(String NodeID, String ParentID,
            String Index, String CorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("ParentID", ParentID);
        htArgs.put("NodeIndexWithinParent", Index);
        htArgs.put("CorpusID", CorpusID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
            return getNodeProps(NodeID);
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNodeIndex(String NodeID, int Index) throws Exception {
        editNodeIndex(NodeID, Index + "");
    }

    public void editNodeIndex(String NodeID, String Index) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("NodeIndexWithinParent", Index);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void updateNodeTypeR(String nodeId) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeId);
        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSUpdateNodeTypeR",
                    htArgs);
            Element elemRoot = doc.getRootElement();
            Element result = elemRoot.element("RESULT");
            JOptionPane.showMessageDialog(null,
                    "Process Update: " + result.getText());
        } catch (Exception e) {
            throw e;
        }
    }

    public void updateListNodeR(String nodeId) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeId);
        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSUpdateListNodeR",
                    htArgs);
            Element elemRoot = doc.getRootElement();
            Element result = elemRoot.element("RESULT");
            JOptionPane.showMessageDialog(null,
                    "Process Update: " + result.getText());
        } catch (Exception e) {
            throw e;
        }
    }

    public void findBridgeNodes(String nodeId) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeId);
        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSFindBridgeNodes",
                    htArgs);
            Element elemRoot = doc.getRootElement();
            Element result = elemRoot.element("RESULT");
            JOptionPane.showMessageDialog(null,
                    "Process Update: " + result.getText());
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNode(ITSTreeNode n) throws Exception {
        User u = ITS.getUser();
        IndraLicense license = u.getLicense();

        // if IDRAC flag is active, we must first determine whether another
        // node already exists with the same name .. link nodes may contain the
        // same name
        // System.out.println("** IDRAC: "+license.isAuthorized(5)+" nodeid: "+n.get("NODEID")+" link: "+n.get("LINKNODEID"));
        if (false
                && ((n.get("NODEID").equals(n.get("LINKNODEID"))) || (n
                .get("NODEID").equals("-1")))
                && (!n.get("NODETITLE").equals("Untitled Topic"))) {
            Vector v = CQL("SELECT <NODE> WHERE NODETITLE = '"
                    + n.get("NODETITLE") + "' AND CORPUSID = "
                    + n.get("CORPUSID"));
            if (v.size() > 0) {
                int i = JOptionPane
                        .showConfirmDialog(
                        null,
                        "A topic with the name "
                        + n.get("NODETITLE")
                        + " already exists in this taxonomy?  Continue?",
                        "Confirm", JOptionPane.YES_NO_OPTION);
                if (i != 0) {
                    throw new TopicExistsError();
                }
            }
        }
        editNode(n.get("LINKNODEID"), n.get("NODETITLE"), n.get("NODEDESC"),
                n.get("NODESIZE"), n.get("NODESTATUS"), n.get("BRIDGENODE"),
                n.get("LISTNODE"), n.get("NODE_TYPE"), n.get("NODEMUSTHAVEGATEINHERITANCE"));
    }

    public void editNode(String NodeID, String NodeTitle, String NodeDesc,
            String NodeSize, String NodeStatus, String bridgeNode,
            String listNode, String nodeType, String nodemusthavegate) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("NodeTitle", NodeTitle);
        htArgs.put("NodeDesc", NodeDesc);
        htArgs.put("NodeSize", NodeSize);
        htArgs.put("NodeStatus", NodeStatus);
        htArgs.put("BRIDGENODE", bridgeNode);
        htArgs.put("LISTNODE", listNode);
        htArgs.put("NODETYPE", nodeType);
        htArgs.put("NODEMUSTHAVEGATEINHERITANCE", nodemusthavegate);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs,
                    false);
        } catch (Exception e) {
            throw e;
        }
    }

    public void editNodeTitle(ITSTreeNode n) throws Exception {
        User u = ITS.getUser();
        IndraLicense license = u.getLicense();

        // if IDRAC flag is active, we must first determine whether another
        // node already exists with the same name .. link nodes may contain the
        // same name
        // System.out.println("** IDRAC: "+license.isAuthorized(5)+" nodeid: "+n.get("NODEID")+" link: "+n.get("LINKNODEID"));
        if (false
                && ((n.get("NODEID").equals(n.get("LINKNODEID"))) || (n
                .get("NODEID").equals("-1")))
                && (!n.get("NODETITLE").equals("Untitled Topic"))) {
            Vector v = CQL("SELECT <NODE> WHERE NODETITLE = '"
                    + n.get("NODETITLE") + "' AND CORPUSID = "
                    + n.get("CORPUSID"));
            if (v.size() > 0) {
                int i = JOptionPane
                        .showConfirmDialog(
                        null,
                        "A topic with the name "
                        + n.get("NODETITLE")
                        + " already exists in this taxonomy?  Continue?",
                        "Confirm", JOptionPane.YES_NO_OPTION);
                if (i != 0) {
                    throw new TopicExistsError();
                }
            }
        }

        String NodeID = n.get("LINKNODEID");
        String NodeTitle = n.get("NODETITLE");

        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);
        htArgs.put("NodeTitle", NodeTitle);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSEditNodeProps", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void parensToMustHaveRecursive(ITSTreeNode n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("LINKNODEID"));
        htArgs.put("recursive", "true");

        // invoke the API
        try {
            HashTree htResults = InvokeAPI(
                    "tsnode.TSParensToMustHaveRecursive", htArgs, true, true);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("recurse failed");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void commasToMustHaveRecursive(ITSTreeNode n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("LINKNODEID"));
        htArgs.put("recursive", "true");

        // invoke the API
        try {
            HashTree htResults = InvokeAPI(
                    "tsnode.TSCommasToMustHaveRecursive", htArgs, true, true);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("recurse failed");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public int numberOfChildren(ITSTreeNode n) throws Exception {
        Vector v = CQL("SELECT <NODE> WHERE PARENTID = " + n.get("NODEID"));
        return v.size();
    }

    public void deleteNode(ITSTreeNode n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("LINKNODEID"));
        htArgs.put("permanently", "true");
        htArgs.put("recursive", "true");

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSDeleteNode", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteThesaurus(Thesaurus t) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ThesaurusID", t.getID());

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsthesaurus.TSDeleteThesaurus",
                    htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("delete failed");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteDocument(Document d) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", d.get("DOCUMENTID"));

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsdocument.TSDeleteDocument",
                    htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void buildSignatures(File f, ITSTreeNode n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("post", "true");

        // invoke the API
        try {
            // sig parms
            htArgs.put("WordFreqMaxForSig", ""
                    + getUser().getWordFreqMaxForSig());
            htArgs.put("IncludeNumbers", "" + getUser().getIncludeNumbers());
            // phase parms
            htArgs.put("PhraseLenMin", "" + getUser().getPhraseLenMin());
            htArgs.put("PhraseLenMax", "" + getUser().getPhraseLenMax());

            InvokeAPI API = new InvokeAPI("tsnode.TSCreateSigsFromDoc", htArgs);
            HashTree htResults = API.PostExecute(f);
        } catch (Exception e) {
            throw e;
        }
    }

    public void buildSignatures(ITSTreeNode n) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));

        // invoke the API
        try {
            // sig parms
            htArgs.put("WordFreqMaxForSig", ""
                    + getUser().getWordFreqMaxForSig());
            htArgs.put("IncludeNumbers", "" + getUser().getIncludeNumbers());
            // phase parms
            htArgs.put("PhraseLenMin", "" + getUser().getPhraseLenMin());
            htArgs.put("PhraseLenMax", "" + getUser().getPhraseLenMax());

            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSCreateSigsForNode",
                    htArgs, true);
            Element elemRoot = doc.getRootElement();

            // Element eSuccess = elemRoot.element("TERMCOUNT");
        } catch (Exception e) {
            throw e;
        }
    }

    public void purgeConceptCache(Document d) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("ConceptID", d.get("DOCUMENTID"));

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tspurge.TSPurgeConceptCache",
                    htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void alphabetizeChildren(ITSTreeNode n) throws Exception {
        alphabetizeChildren(n, false);
    }

    public void alphabetizeChildren(ITSTreeNode n, boolean Recurse)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        if (Recurse) {
            htArgs.put("recursive", "true");
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSAlphabetizeChildren",
                    htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void capitalizeChildren(ITSTreeNode n) throws Exception {
        capitalizeChildren(n, false);
    }

    public void capitalizeChildren(ITSTreeNode n, boolean Recurse)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("NodeTitle", n.get("NODETITLE"));
        if (Recurse) {
            htArgs.put("recursive", "true");
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSCapitalizeChildren",
                    htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public String editNodeStatusRecurse(ITSTreeNode n, int status)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("Status", status + "");

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSChangeNodeStatusRecursive", htArgs, null, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("SUCCESS");
            if (eNodes == null) {
                throw new Exception("Failed to modify nodes from: "
                        + n.get("NODETITLE"));
            }

            return eNodes.getText();

        } catch (Exception e) {
            throw e;
        }
    }

    public void rollUpNodes(ITSTreeNode n, int minlength, PopupProgressBar ppb)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("MinChar", getUser().getCharacterThreshold() + "");
        htArgs.put("ShowStatus", "true");

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSRollupLeafNodes", htArgs, ppb, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("SUCCESS");
            if (eNodes == null) {
                throw new Exception(
                        "Failed to recursively roll up nodes from: "
                        + n.get("NODETITLE"));
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public void stripChildren(ITSTreeNode n) throws Exception {
        stripChildren(n, false);
    }

    public void stripChildren(ITSTreeNode n, boolean Recurse) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("NodeTitle", n.get("NODETITLE"));
        if (Recurse) {
            htArgs.put("recursive", "true");
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSStripChildren", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void stripChildrenWhitespace(ITSTreeNode n, boolean Recurse)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", n.get("NODEID"));
        htArgs.put("NodeTitle", n.get("NODETITLE"));
        if (Recurse) {
            htArgs.put("recursive", "true");
        }

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSStripChildrenWhitespace",
                    htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveAlert(Alert a) throws Exception {
        Hashtable htArgs = getArguments();

        htArgs.put("UserID", a.getDN());
        htArgs.put("Name", a.getName());
        htArgs.put("Email", a.getEmail());
        htArgs.put("Query", a.getCQL());
        htArgs.put("Notes", a.getNotes());
        htArgs.put("RunFreq", a.getRunFrequency().getValue());

        if (!a.getID().startsWith("CN")) {
            htArgs.put("ID", a.getID());
        }

        try {
            HashTree htResults = InvokeAPI("tsnotification.TSAddAlert", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Attempt to save this alert failed!");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeAlert(Alert a) throws Exception {
        Hashtable htArgs = getArguments();

        htArgs.put("ID", a.getID());

        try {
            HashTree htResults = InvokeAPI("tsnotification.TSRemoveAlertByID",
                    htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Attempt to remove this alert failed!");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveFolder(Genre g) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("FolderID", g.getID());
        htArgs.put("FolderName", g.getName());

        String sFolderStatus = "1";
        if (!g.getActive()) {
            sFolderStatus = "0";
        }

        htArgs.put("FolderStatus", sFolderStatus);

        try {
            HashTree htResults = InvokeAPI("tsgenre.TSEditGenreProps", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Signature removal failed");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveSubjectArea(SubjectArea sa) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("SubjectAreaID", sa.getID());
        htArgs.put("SubjectName", sa.getName());

        String sSubjectAreaStatus = "1";
        if (!sa.getActive()) {
            sSubjectAreaStatus = "0";
        }

        htArgs.put("SubjectAreaStatus", sSubjectAreaStatus);

        try {
            HashTree htResults = InvokeAPI(
                    "tssubjectarea.TSEditSubjectAreaProps", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Signature removal failed");
            }
        } catch (Exception e) {
            System.out.println("could not edit genre props for genre "
                    + sa.getID());
            throw e;
        }
    }

    public void purgeSignatures(String CorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);

        // http://ITSSERVER/itsapi/ts?fn=&SKEY=-132981656
        try {
            HashTree htResults = InvokeAPI("tspurge.TSPurgeSigCache", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception(
                        "System could not purge this signature cache file.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveSignaturesLinkedSection(String nodeid, String signatures)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", nodeid);
        try {
            HashTree htResults = InvokeAPI("tsnode.TSRemoveSignatures", htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Signature removal failed");
            }
        } catch (Exception e) {
            // System.out.println("NodeID: " + NodeID +
            // " has no signatures to be removed.  That's okay.");
        }
        htArgs.put("Signatures", signatures);
        HashTree htResults = InvokeAPI("tsnode.TSAddNodeSignatures", htArgs);

        if (!htResults.containsKey("SUCCESS")) {
            throw new Exception("Signature addition failed");
        }
    }

    public void saveSignatures(String NodeID, Vector Signatures)
            throws Exception {
        saveSignatures(NodeID, Signatures, true, null);
    }

    public void saveSignatures(String NodeID, Vector Signatures, String lang)
            throws Exception {
        saveSignatures(NodeID, Signatures, true, lang);
    }

    public void saveSignatures(Node n, Vector Signatures) throws Exception {
        saveSignatures(n.get("NODEID"), Signatures, true, null);
    }

    public void saveSignatures(Node n, Vector Signatures, boolean bRemoveFirst)
            throws Exception {
        saveSignatures(n.get("NODEID"), Signatures, bRemoveFirst, null);
    }

    public void saveSignatures(String NodeID, Vector Signatures,
            boolean bRemoveFirst) throws Exception {
        saveSignatures(NodeID, Signatures, bRemoveFirst, null);
    }

    public void saveSignatures(String NodeID, Vector Signatures,
            boolean bRemoveFirst, String lang) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", NodeID);

        if (lang != null) {
            htArgs.put("lang", lang);
        }

        if (bRemoveFirst) {
            try {
                HashTree htResults = InvokeAPI("tsnode.TSRemoveSignatures",
                        htArgs);
                if (!htResults.containsKey("SUCCESS")) {
                    throw new Exception("Signature removal failed");
                }
            } catch (Exception e) {
                // System.out.println("NodeID: " + NodeID +
                // " has no signatures to be removed.  That's okay.");
            }
        }

        if (Signatures.size() > 0) {
            String str = "";
            for (int i = 0; i < Signatures.size(); i++) {
                Signature s = (Signature) Signatures.elementAt(i);
                if (i != 0) {
                    str = str + ",";
                }
                str = str + s.getWord() + "||" + s.getWeight();
            }
            htArgs.put("Signatures", str);
            HashTree htResults = InvokeAPI("tsnode.TSAddNodeSignatures", htArgs);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Signature addition failed");
            }
        }
    }

    public void deleteNodeDocument(String DocumentID, String NodeID)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        htArgs.put("NodeID", NodeID);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsnode.TSRemoveNodeDocument",
                    htArgs);
            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("remove relationship failed");
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteNodeDocuments(String DocumentID) throws Exception {
        deleteNodeDocuments(DocumentID, false);
    }

    public void deleteNodeDocuments(String DocumentID, boolean bPermanent)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocumentID", DocumentID);
        if (bPermanent) {
            htArgs.put("Permanent", "true");
        }

        // invoke the API
        try {
            InvokeAPI("tsnode.TSRemoveNodeDocuments", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector CQL(String Query) throws Exception {
        return CQL(Query, 1, 500);
    }

    public Vector CQL(String Query, int Start, int RowMax) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("query", Query);
        htArgs.put("start", "" + Start);
        htArgs.put("rowmax", "" + RowMax);

        Vector v = new Vector();
        // System.out.println("CQL: "+Query);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscql.TSCql", htArgs, false);
            Element elemRoot = doc.getRootElement();

            // return different structures depending upon NODE or DOCUMENT
            if (Query.toUpperCase().indexOf("<NODE>") != -1) {
                Element eNodes = elemRoot.element("NODES");
                if (eNodes == null) {
                    return new Vector();
                }

                Iterator i = eNodes.elements().iterator();
                while (i.hasNext()) {
                    Element eNode = (Element) i.next();

                    Iterator i2 = eNode.elements().iterator();
                    ITSTreeNode n = new ITSTreeNode(i2);

                    v.add(n);
                }
            } else if (Query.toUpperCase().indexOf("<DOCUMENT>") != -1) {
                Element eDocs = elemRoot.element("DOCUMENTS");
                if (eDocs == null) {
                    return new Vector();
                }

                Iterator i = eDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eDoc = (Element) i.next();

                    Iterator i2 = eDoc.elements().iterator();
                    com.iw.system.Document d = new com.iw.system.Document(i2);

                    v.add(d);
                }
            } else if (Query.toUpperCase().indexOf("<NODEDOCUMENT>") != -1) {
                Element eNodeDocs = elemRoot.element("NODEDOCUMENTS");
                if (eNodeDocs == null) {
                    return new Vector();
                }

                Iterator i = eNodeDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eNodeDoc = (Element) i.next();

                    Iterator i2 = eNodeDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            } else {
                throw new Exception("The CQL selection object was invalid.");
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    public Hashtable CQLHash(String Query) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("query", Query);
        htArgs.put("start", "1");
        htArgs.put("rowmax", "10000");

        Hashtable ht = new Hashtable();
        // System.out.println("CQL: "+Query);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscql.TSCql", htArgs, false);
            Element elemRoot = doc.getRootElement();

            // return different structures depending upon NODE or DOCUMENT
            if (Query.toUpperCase().indexOf("<NODE>") != -1) {
                Element eNodes = elemRoot.element("NODES");
                if (eNodes == null) {
                    return ht;
                }

                Iterator i = eNodes.elements().iterator();
                while (i.hasNext()) {
                    Element eNode = (Element) i.next();

                    Iterator i2 = eNode.elements().iterator();
                    ITSTreeNode n = new ITSTreeNode(i2);

                    ht.put(n.get("NODEID"), n);
                }
            } else if (Query.toUpperCase().indexOf("<DOCUMENT>") != -1) {
                Element eDocs = elemRoot.element("DOCUMENTS");
                if (eDocs == null) {
                    return new Hashtable();
                }

                Iterator i = eDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eDoc = (Element) i.next();

                    Iterator i2 = eDoc.elements().iterator();
                    com.iw.system.Document d = new com.iw.system.Document(i2);

                    ht.put(d.get("DOCUMENTID"), d);
                    ;
                }
            } else if (Query.toUpperCase().indexOf("<NODEDOCUMENT>") != -1) {
                Element eNodeDocs = elemRoot.element("NODEDOCUMENTS");
                if (eNodeDocs == null) {
                    return new Hashtable();
                }

                Iterator i = eNodeDocs.elements().iterator();
                while (i.hasNext()) {
                    Element eNodeDoc = (Element) i.next();

                    Iterator i2 = eNodeDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    ht.put(nd.get("NODEID") + nd.get("DOCUMENTID"), nd);
                }
            } else {
                throw new Exception("The CQL selection object was invalid.");
            }

            return ht;
        } catch (Exception e) {
            throw e;
        }
    }

    public TableModel tsThesaurusNodeReport(Hashtable htArgs) throws Exception {

        Hashtable htArgsMerge = mergeHashTables(getArguments(), htArgs, true);

        Vector v = new Vector();

        try {
            InvokeAPI API = new InvokeAPI("tsthesaurus.TSThesaurusNodeReport",
                    htArgsMerge);
            org.dom4j.Document docExecuteSqlResult = API.dExecute(false, false);
            Element elemRoot = docExecuteSqlResult.getRootElement();
            Element elem_DATATABLE = elemRoot
                    .element("OBJECT_TABLEMODEL_BEANXML");
            if (elem_DATATABLE == null) {
                throw new Exception(
                        "System error: tsthesaurus.TSThesaurusNodeReport returns no tablemodel.");
            }

            String sXMLized_DATATABLE = elem_DATATABLE.getText().trim();
            Vector vTableModelXMLizedFormReturn = (Vector) XMLBeanSerializeHelper
                    .deserializeFromString(sXMLized_DATATABLE.trim());
            TableModelXMLBeanSerializable tableModelIWSerializable = new TableModelXMLBeanSerializable();
            tableModelIWSerializable.setFromIWXMLData(new ByteArrayInputStream(
                    sXMLized_DATATABLE.getBytes()));

            return tableModelIWSerializable;
        } catch (Exception e) {
            throw e;
        }
    }

    // take a text file and wrap it with HTML
    public static void wrapHTMLstatic(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(),
                    false));
            out.println("<HTML><HEAD><TITLE>None</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public static void wrapHTMLstatic(File f, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData + "\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        if (tempFile.exists()) {
            tempFile.delete();
        }

        try {
            out = new PrintWriter(new FileOutputStream(
                    tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(sOut);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(File f) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        f.delete();

        try {
            out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(),
                    false));
            out.println("<HTML><HEAD><TITLE>None</TITLE></HEAD>\n<BODY>");
            out.print(sOut);
            out.println("</BODY>\n</HTML>\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public File wrapHTML(String title, String buffer) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";

        File tempFile = new File("file.html");
        if (tempFile.exists()) {
            tempFile.delete();
        }

        try {
            out = new PrintWriter(new FileOutputStream(
                    tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>" + title
                    + "</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(buffer);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }

        return tempFile;
    }

    public static void wrapHTMLstatic(String s, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;

        if (tempFile.exists()) {
            tempFile.delete();
        }

        try {
            out = new PrintWriter(new FileOutputStream(
                    tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(String s, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;

        if (tempFile.exists()) {
            tempFile.delete();
        }

        try {
            out = new PrintWriter(new FileOutputStream(
                    tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(s);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    public void wrapHTML(File f, File tempFile) throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData + "\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            fis.close();
        }

        if (tempFile.exists()) {
            tempFile.delete();
        }

        try {
            out = new PrintWriter(new FileOutputStream(
                    tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(sOut);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }
    }

    // 0: contains, 1: starts with, 2: ends with
    public Vector topicSearch(String SearchTerm, int Type) throws Exception {
        String query = SearchTerm;

        switch (Type) {
            case 0:
                query = "%%" + query + "%%";
                break;
            case 1:
                query = query + "%%";
                break;
            case 2:
                query = "%%" + query;
                break;
        }

        query = query.replaceAll("'", "''");
        query = "SELECT <NODE> WHERE NODETITLE LIKE '" + query
                + "' ORDER BY NODETITLE ASC";
        return CQL(query, 1, 500);
    }

    public Vector topicSearchNode(String nodeid) throws Exception {
        String query = nodeid;
        query = query.replaceAll("'", "''");
        query = "SELECT <NODE> WHERE NODEID = '" + nodeid
                + "' ORDER BY NODETITLE ASC";
        return CQL(query, 1, 500);
    }

    public Vector topicSearch(String SearchTerm, int Type, Corpus corpus)
            throws Exception {
        String query = SearchTerm;

        switch (Type) {
            case 0:
                query = "%%" + query + "%%";
                break;
            case 1:
                query = query + "%%";
                break;
            case 2:
                query = "%%" + query;
                break;
        }

        query = query.replaceAll("'", "''");
        query = "SELECT <NODE> WHERE NODETITLE LIKE '" + query
                + "' AND CORPUSID = " + corpus.getID()
                + " ORDER BY NODETITLE ASC";
        return CQL(query, 1, 500);
    }

    public Vector getAncestorTree(ITSTreeNode n) throws Exception {
        return getAncestorTree(n.get("LINKNODEID"));
    }

    public Vector getAncestorTree(String sNodeID) throws Exception {
        Vector v = new Vector(); // return struct

        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", sNodeID);

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSGetNodeTree",
                    htArgs, false);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("NODES");
            if (eNodes == null) {
                return new Vector();
            }

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                Iterator i2 = eNode.elements().iterator();
                ITSTreeNode n = new ITSTreeNode(i2);

                v.add(n);
            }

            return v;
        } catch (Exception e) {
            throw e;
        }
    }

    // titles and thesaurus - all for a tsclassify to expand a pure sig set
    // NOT A MOD OF THE INPUT SIGNATURES OBJECT ... NEW OBJECT RETURNED
    public Signatures signatureExpand(Signatures signatures, int iNodeID,
            int iCorpusID) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("NodeID", "" + iNodeID);
        htArgs.put("CorpusID", "" + iCorpusID);
        String sDynamicNodeSigs = signatures.toString(true);
        htArgs.put("Sigs", sDynamicNodeSigs);
        htArgs.put("NumParentTitleTermsAsSigs", ""
                + getUser().getNumParentTitleTermAsSigs());
        htArgs.put("WordFreqMaxForSig", "" + getUser().getWordFreqMaxForSig());
        htArgs.put("IncludeNumbers", "" + getUser().getIncludeNumbers());

        // invoke the API
        try {
            org.dom4j.Document doc = InvokeDomAPI(
                    "tssignature.TSSignatureExpand", htArgs);
            Element elemRoot = doc.getRootElement();

            Element eSignatures = elemRoot.element("SIGNATURES");
            Iterator iter = eSignatures.elements().iterator();

            return new Signatures(iter);

        } catch (Exception e) {
            throw e;
        }
    } // signatureexpand

    // classify: using the HTTP POST method
    public Vector classify(File f, boolean bPost) throws Exception {
        return classify(f, bPost, null, null);
    }

    public Vector classify(File f, boolean bPost, Node n) throws Exception {
        return classify(f, bPost, n, null);
    }

    public Vector classify(File f, boolean bPost, Node n, String sDocTitle)
            throws Exception {
        File tempFile = null;
        Vector v = new Vector();
        boolean bUsingTempFile = false;

        // if the file is of type text, it needs an HTML wrapper before being
        // posted
        if (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
            tempFile = new File("temp.html");
            if (tempFile.exists()) {
                tempFile.delete();
            }
            System.out.println("Filtering: " + f.getAbsolutePath() + " to "
                    + tempFile.getAbsolutePath());

            try {
                wrapHTML(f, tempFile);
                bUsingTempFile = true;
            } catch (Exception ex) {
                ex.printStackTrace();
                throw ex;
            }
        } // if the file is of type PDF, filter it before posting
        else if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
            bUsingTempFile = true;
            tempFile = ITS.PDFtoHTML(f);
        } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
            bUsingTempFile = true;
            tempFile = ITS.MSWORDtoHTML(f);
        } else {
            tempFile = f;
            bUsingTempFile = false;
        }

        Hashtable htArgs = getArguments();
        htArgs.put("explainscores", "false");
        if (bPost) {
            htArgs.put("post", "true");
        } else {
            htArgs.put("post", "false");
        }
        if (n != null) {
            htArgs.put("nodeset", n.get("NODEID"));
        }
        if (sDocTitle != null) {
            htArgs.put("DocTitle", sDocTitle);
        }

        User u = getUser();
        htArgs.put("Corpora", u.getCorporaToUse());
        if (u.getCorporaToUse().equals("-1")) {
            throw new EmptyCorpusToUseList();
        }

        if (n != null) {
            htArgs.put("corpusid", n.get("CORPUSID"));
            htArgs.put("Corpora", n.get("CORPUSID"));
        }

        htArgs.put("NumParentTitleTermsAsSigs",
                "" + u.getNumParentTitleTermAsSigs());

        System.out.println(u.getNumParentTitleTermAsSigs());
        System.out.println(u.getCorporaToUse());

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);
            org.dom4j.Document doc = API.dExecute(tempFile);
            Element elemRoot = doc.getRootElement();

            Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eDocs == null) {
                throw new Exception(
                        "ClassificationResultSet tag does not exist in XML output");
            }

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                if (eDoc.getQName().getName().equals("NODE")) {
                    Iterator i2 = eDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (bUsingTempFile) {
                tempFile.delete();
            }
        }

        return v;
    }

    // handles both document and abstract classify
    // returns a vector of NodeDocument objects
    public Vector classify(int DocumentID, String DocTitle, boolean bDocClassify)
            throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("DocIDSource", "" + DocumentID);
        htArgs.put("DocTitle", DocTitle);

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        User u = getUser();
        htArgs.put("Corpora", u.getCorporaToUse());
        htArgs.put("NumParentTitleTermsAsSigs",
                "" + u.getNumParentTitleTermAsSigs());
        if (u.getCorporaToUse().equals("-1")) {
            throw new EmptyCorpusToUseList();
        }
        htArgs.put("dweight", u.getDocumentWeight() + "");
        htArgs.put("sweight", u.getAbstractWeight() + "");

        Vector v = new Vector(); // return set

        Vector vResults = InvokeVectorAPI("tsclassify.TSClassifyDoc", htArgs);

        if (vResults.size() < 3) {
            throw new Exception(
                    "ClassificationResultSet tag does not exist in XML output docid ["
                    + DocumentID + "]");
        }

        Vector vDocs = (Vector) vResults.elementAt(2);

        Enumeration eD = vDocs.elements();
        int loop = 0;
        while (eD.hasMoreElements()) {
            loop++;
            Object o = eD.nextElement();
            if (loop > 1) { // item number one is NUMNODEDOCSCORES
                Vector vNodeDocument = (Vector) o;
                NodeDocument nd = new NodeDocument(vNodeDocument);
                nd.set("DOCUMENTID", DocumentID + "");
                nd.set("DOCTITLE", DocTitle);

                v.add(nd); // add this document to the vector
            }
        }
        return v;
    }

    public void sendResources(ArrayList<String> t, ArrayList<String> r) {
        Hashtable htArgs = getArguments();
        for (int i = 0; i < t.size(); i++) {
            htArgs.put("tax" + i, "" + t.get(i));
        }
        for (int k = 0; k < r.size(); k++) {
            htArgs.put("uri" + k, "" + r.get(k));
        }

        try {
            InvokeAPI("tsclassify.TSGetResource", htArgs);
        } catch (Exception e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }

    }

    public String saveCorpusImagesS3(String corpusId) {
        String result = "";
        Hashtable htArgs = getArguments();
        htArgs.put("corpusId", corpusId);
        try {
            org.dom4j.Document doc = InvokeDomAPI("tscorpus.TSSaveImagesS3",
                    htArgs);
            Element elemRoot = doc.getRootElement();

            Element eResult = elemRoot.element("RESULT");
            if (eResult == null) {
                JOptionPane.showMessageDialog(null, "Error in the process");
            } else {
                result = "Images stored on s3 -> " + eResult.getText();
            }
        } catch (Exception e) {
            Log.debug("Error saving images in S3");
        }
        return result;
    }

    public String saveNodeImagesS3(String corpusId, String nodeId, boolean recursively,
            PopupProgressBar ppb) {
        String result = "ok";
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeId);
        htArgs.put("corpusid", corpusId);
        htArgs.put("recursively", new Boolean(recursively).toString());
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSsaveImagesS3", htArgs, ppb);
            Element elemRoot = doc.getRootElement();

            Element eResult = elemRoot.element("RESULT");
            if (eResult == null) {
                JOptionPane.showMessageDialog(null, "Error in the process");
                result = "The process not finished ok";
            } else {
                result = eResult.getText();
                // parse data
                try {
                    String msg = eResult.element("MSG").getText();
                    java.util.List<Element> eMessages = eResult.element("MESSAGES")
                            .elements();
                    System.out
                            .println(msg
                            + "-----------------------------------------------------");
                    for (Element el : eMessages) {
                        System.out.println(el.getText());
                    }
                    System.out
                            .println("-----------------------------------------------");

                } catch (Exception e) {
                    Log.error("Error parsing data");
                }
            }
        } catch (Exception e) {
            Log.debug("Error saving images in S3");
        }
        return result;
    }

    public GoogleInfoBean consultFromGoogle(Map<String, String> params,
            PopupProgressBar ppb) throws Exception {
        Hashtable htArgs = getArguments();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            htArgs.put(entry.getKey(), entry.getValue());
        }
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsclassify.BuildNodeFromGoogle", htArgs, ppb);
            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            GoogleInfoBean gib = new GoogleInfoBean();
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(null, "Proccess Finished");
            } else {
                JOptionPane.showMessageDialog(null,
                        "Proccess Finished Successful");

                java.util.List<Element> eCSV = elemRoot.elements("CSV");
                if (eCSV != null) {
                    for (Element eAux : eCSV) {
                        gib.addCSV(URLDecoder.decode(eAux.getText(), "UTF-8"));
                    }
                }
                Element eNumErrors = elemRoot.element("NUMERRORS");
                if (eNumErrors != null) {
                    gib.setNumErrors(eNumErrors.getText());
                }
                Element eNumSuccess = elemRoot.element("NUMSUCCESS");
                if (eNumSuccess != null) {
                    gib.setNumSuccess(eNumSuccess.getText());
                }
            }

            return gib;
        } catch (Exception e) {
            Log.debug("Error google crawl");
            return null;
        }

    }

    public BingInfoBean addImagesNodeRecursive(String corpusId, String nodeId,
            PopupProgressBar ppb) throws Exception {
        Hashtable htArgs = getArguments();

        htArgs.put("corpusid", corpusId);
        htArgs.put("nodeid", nodeId);

        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSCreateImagesRecursive", htArgs, ppb);
            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(null, "Proccess Finished");
            } else {
                JOptionPane.showMessageDialog(null,
                        "Proccess Finished Successful");
            }

            BingInfoBean bib = new BingInfoBean();
            /*
             * Element eTerms = elemRoot.element("TERMS"); if (eTerms != null) {
             * bib.setTermsErrors(eTerms.getText().split(",")); }
             */
            java.util.List<String> eTermArray = new ArrayList<String>();
            java.util.List<Element> eTerms = elemRoot.elements("TERM");
            if (eTerms != null) {
                for (Element eTerm : eTerms) {
                    eTermArray.add(URLDecoder.decode(eTerm.getText(), "UTF-8"));
                }
            }
            if (!eTermArray.isEmpty()) {
                bib.setTermsErrosList(eTermArray);
            }

            Element eNumErrors = elemRoot.element("NUMERRORS");
            if (eNumErrors != null) {
                bib.setNumErrors(eNumErrors.getText());
            }
            Element eNumSuccess = elemRoot.element("NUMSUCCESS");
            if (eNumSuccess != null) {
                bib.setNumSuccess(eNumSuccess.getText());
            }
            return bib;
        } catch (Exception e) {
            System.out.println("Exception from addImagesNodeRecursive: "
                    + e.getMessage());
            return null;
        }
    }

    public ArrayList<FileMetadata> getImagesFromCorpus(String corpusId) {
        Hashtable htArgs = getArguments();
        htArgs.put("corpusid", corpusId);
        htArgs.put("DTAR", "1");
        ArrayList<FileMetadata> fileMetadaArray = new ArrayList<FileMetadata>();
        FileMetadata fileMetaData = null;
        TarEntry tarEntry = null;
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        int b = 0;
        byte[] buffer = new byte[2048];
        String strFileMeta = null;
        String[] strFileMetaRows;
        String[] strFileMetaCols;
        HashMap<String, FileMetadata> fileMapping = new HashMap<String, FileMetadata>();
        try {
            TarInputStream tarfile = InvokeTarApi("tscorpus.TSGetImagesCorpus",
                    htArgs, true, false);
            while ((tarEntry = tarfile.getNextEntry()) != null) {
                bao.reset();
                b = -1;
                while ((b = tarfile.read(buffer, 0, buffer.length)) != -1) {
                    bao.write(buffer, 0, b);
                }
                if (!tarEntry.getName().equalsIgnoreCase("fileMetaData")) {
                    fileMetaData = new FileMetadata();
                    fileMetaData.setImage(bao.toByteArray());
                    fileMetaData.setId(tarEntry.getName());
                    fileMetadaArray.add(fileMetaData);
                    fileMapping.put(tarEntry.getName(), fileMetaData);
                } else {
                    strFileMeta = new String(bao.toByteArray());
                }

            }
            if (strFileMeta == null) {
                JOptionPane.showMessageDialog(null,
                        "Error: This Request dont response File Meta Data");
                return null;
            }
            strFileMetaRows = strFileMeta.split("\\^");
            for (String fileMeta : strFileMetaRows) {
                strFileMetaCols = fileMeta.split("\\|\\|");
                if (strFileMetaCols.length >= 3) {
                    fileMetaData = fileMapping.get(strFileMetaCols[0]);
                    fileMetaData.setDescription(strFileMetaCols[1]);
                    fileMetaData.setUri(strFileMetaCols[2]);
                    fileMetaData.setNodeId(strFileMetaCols[3]);
                } else {
                    return null;
                }
            }
            tarfile.close();
            return fileMetadaArray;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out.println("Error tsnode.TSGetImageNode -> "
                    + e.getMessage());
            e.printStackTrace();
        }

        return null;

    }

    public ArrayList<FileMetadata> getImagesFromNode(String nodeId) {
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeId);
        htArgs.put("DTAR", "1");
        ArrayList<FileMetadata> fileMetadaArray = new ArrayList<FileMetadata>();
        FileMetadata fileMetaData = null;
        TarEntry tarEntry = null;
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        int b = 0;
        byte[] buffer = new byte[2048];
        String strFileMeta = null;
        String[] strFileMetaRows;
        String[] strFileMetaCols;
        HashMap<String, FileMetadata> fileMapping = new HashMap<String, FileMetadata>();
        try {
            TarInputStream tarfile = InvokeTarApi("tsnode.TSGetImageNode",
                    htArgs, true, false);
            while ((tarEntry = tarfile.getNextEntry()) != null) {
                bao.reset();
                b = -1;
                while ((b = tarfile.read(buffer, 0, buffer.length)) != -1) {
                    bao.write(buffer, 0, b);
                }
                if (!tarEntry.getName().equalsIgnoreCase("fileMetaData")) {
                    fileMetaData = new FileMetadata();
                    fileMetaData.setImage(bao.toByteArray());
                    fileMetaData.setId(tarEntry.getName());
                    fileMetadaArray.add(fileMetaData);
                    fileMapping.put(tarEntry.getName(), fileMetaData);
                } else {
                    strFileMeta = new String(bao.toByteArray());
                }

            }
            if (strFileMeta == null) {
                JOptionPane.showMessageDialog(null,
                        "Error: This Request dont response File Meta Data");
                return null;
            }
            strFileMetaRows = strFileMeta.split("\\^");
            for (String fileMeta : strFileMetaRows) {
                strFileMetaCols = fileMeta.split("\\|\\|");
                if (strFileMetaCols.length >= 3) {
                    fileMetaData = fileMapping.get(strFileMetaCols[0]);
                    fileMetaData.setDescription(strFileMetaCols[1]);
                    fileMetaData.setUri(strFileMetaCols[2]);
                    fileMetaData.setNodeId(strFileMetaCols[3]);
                } else {
                    // JOptionPane.showMessageDialog(null,"This node not have images");
                    return null;
                }
            }
            tarfile.close();
            return fileMetadaArray;
        } catch (Exception e) {
            System.out.println("Error tsnode.TSGetImageNode -> "
                    + e.getMessage());
            e.printStackTrace();
        }

        return null;

    }

    public void addNodeDesc(String nodeid, PopupProgressBar bar) {
        Hashtable htArgs = getArguments();
        htArgs.put("nodeid", nodeid);
        try {
            org.dom4j.Document doc = InvokeDomStatusAPI(
                    "tsnode.TSAddNodeDescription", htArgs, bar);
            Element elemRoot = doc.getRootElement();
            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(null, "Error add desc");
            } else {
                JOptionPane.showMessageDialog(null, "Add desc successful");
            }

        } catch (Exception e) {
            System.out.println("Exception from AddNodeDescription: "
                    + e.getMessage());
        }
    }

    public void deleteImageNode(String imageid) {
        Hashtable htArgs = getArguments();
        htArgs.put("imageid", imageid);
        try {
            org.dom4j.Document doc = InvokeDomAPI("tsnode.TSDeleteImageNode",
                    htArgs);

            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(this, "Error deleting image");
            } else {
                JOptionPane.showMessageDialog(null, "Delete image successful");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
    }

    public void deleteImageCorpus(String imageid) {
        Hashtable htArgs = getArguments();
        htArgs.put("imageid", imageid);
        try {
            org.dom4j.Document doc = InvokeDomAPI(
                    "tscorpus.TSDeleteImageCorpus", htArgs);

            Element elemRoot = doc.getRootElement();

            Element eSuccess = elemRoot.element("SUCCESS");
            if (eSuccess == null) {
                JOptionPane.showMessageDialog(this, "Error deleting image");
            } else {
                JOptionPane.showMessageDialog(null, "Delete image successful");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
    }

    public void splitArchive() {
        Hashtable htArgs = getArguments();
        try {
            InvokeAPI("util.ApiSetSplitArchive", htArgs);
            JOptionPane.showMessageDialog(null,
                    "Split succefully. Directories created correctly");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public Vector thesaurusExpand(ITSTreeNode n, Vector signatures)
            throws Exception {
        return thesaurusExpand(n.get("CORPUSID"), n.get("NODETITLE"),
                signatures);
    }

    public Vector thesaurusExpand(NodeDocument nd, Vector signatures)
            throws Exception {
        return thesaurusExpand(nd.get("CORPUSID"), nd.get("DOCTITLE"),
                signatures);
    }

    public Vector thesaurusExpand(String CorpusID, String NodeTitle,
            Vector vSignatures) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);
        htArgs.put("NodeTitle", NodeTitle);

        Signatures signatures = new Signatures(vSignatures);
        String dynamicnodesigs = signatures.toString();
        htArgs.put("Sigs", dynamicnodesigs);

        Vector v = new Vector(); // return vector
        HashTree htResults = InvokeAPI("tsthesaurus.TSThesaurusExpand", htArgs);
        if (!htResults.containsKey("SIGNATURES")) {
            return v;
        }

        HashTree htSignatures = (HashTree) htResults.get("SIGNATURES");
        Enumeration eS = htSignatures.elements();

        while (eS.hasMoreElements()) {
            HashTree htSignature = (HashTree) eS.nextElement();
            Signature s = new Signature(htSignature);
            v.add(s);

            if (htSignature.containsKey("THESAURUSTERM")) {
                HashTree htThesaurusTerms = (HashTree) htSignature
                        .get("THESAURUSTERM");
                Enumeration eT = htThesaurusTerms.elements();

                while (eT.hasMoreElements()) {
                    String sWord = (String) eT.nextElement();
                    Signature st = new Signature(sWord, s.getWeight());
                    v.add(st);
                }
            }
        }

        return v;
    }

    public Vector thesaurusExpandDisplay(String CorpusID, String NodeTitle,
            Vector vSignatures) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("CorpusID", CorpusID);
        htArgs.put("NodeTitle", NodeTitle);

        Signatures signatures = new Signatures(vSignatures);
        String dynamicnodesigs = signatures.toString();
        htArgs.put("Sigs", dynamicnodesigs);

        Vector v = new Vector(); // return vector
        HashTree htResults = InvokeAPI("tsthesaurus.TSThesaurusExpand", htArgs);
        if (!htResults.containsKey("SIGNATURES")) {
            return v;
        }

        HashTree htSignatures = (HashTree) htResults.get("SIGNATURES");
        Enumeration eS = htSignatures.elements();

        while (eS.hasMoreElements()) {
            HashTree htSignature = (HashTree) eS.nextElement();
            Signature s = new Signature(htSignature);
            v.add(s);

            if (htSignature.containsKey("THESAURUSTERM")) {
                HashTree htThesaurusTerms = (HashTree) htSignature
                        .get("THESAURUSTERM");
                Enumeration eT = htThesaurusTerms.elements();

                while (eT.hasMoreElements()) {
                    String sWord = (String) eT.nextElement();
                    Signature st = new Signature(sWord, s.getWeight());
                    v.add(st);
                }
            }
        }

        return v;
    }
    // classify a document into a specific node, given dynamic node parameters
    // returns a new NodeDocument object
	/*
     * "&DynamicNodeSigs=3,term1|||5,term2||thesterm21||thesterm22|||7,term3||thesterm31"
     * + "&DCNodeTitle=hello world" + "&DCNumTitleWords=1" + "&DCNodeSize=51" +
     * "&DCBuildThesaurus=false"
     */
    // used by SignatureTestPanel
    int iCallCntClass = 0;

    public NodeDocument classify(NodeDocument nd, Vector vSignatureObjects)
            throws Exception {
        NodeDocument ndResult = null;

        // thesaurus expand these signatures

        Hashtable htArgs = getArguments();
        htArgs.put("DocIDSource", nd.get("DOCUMENTID"));
        htArgs.put("DocTitle", nd.get("DOCTITLE"));

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        htArgs.put("CorpusID", nd.get("CORPUSID"));
        htArgs.put("Corpora", nd.get("CORPUSID"));
        htArgs.put("NodeID", "" + nd.get("NODEID"));
        htArgs.put("NodeSet", "" + nd.get("NODEID"));
        htArgs.put("NodeTitle", "" + nd.get("NODETITLE"));
        htArgs.put("NodeSize", "" + nd.get("NODESIZE"));
        htArgs.put("NumParentTitleTermsAsSigs", ""
                + getUser().getNumParentTitleTermAsSigs());

        User u = getUser();
        htArgs.put("dweight", u.getDocumentWeight() + "");
        htArgs.put("sweight", u.getAbstractWeight() + "");

        // classify with sigs - thesaurus and title expand these signatures
        Signatures signaturesObject = ITSHelper
                .vSignatureObjectsToSignaturesObject(vSignatureObjects);
        Signatures signaturesObjectExpanded = signatureExpand(signaturesObject,
                Integer.parseInt(nd.get("NODEID")),
                Integer.parseInt(nd.get("CORPUSID")));
        htArgs.put("Sigs", signaturesObjectExpanded.toString());

        // invoke the API
        try {
            Enumeration enumeration = htArgs.keys();

            org.dom4j.Document doc = InvokeDomAPI("tsclassify.TSClassifyDoc",
                    htArgs);
            Element elemRoot = doc.getRootElement();

            Element eNodes = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eNodes == null) {
                return null;
            }

            Iterator i = eNodes.elements().iterator();
            while (i.hasNext()) {
                Element eNode = (Element) i.next();

                if (eNode.getQName().getName().equals("NODE")) {
                    Iterator i2 = eNode.elements().iterator();
                    ndResult = new NodeDocument(i2);
                }
            }

            return ndResult;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vector classifyNarrative(String DocTitle, String DocText)
            throws Exception {
        Hashtable htArgs = getArguments();

        htArgs.put("post", "false");
        htArgs.put("explainscores", "false");
        htArgs.put("NumParentTitleTermsAsSigs", ""
                + getUser().getNumParentTitleTermAsSigs());

        User u = getUser();
        htArgs.put("Corpora", u.getCorporaToUse());
        if (u.getCorporaToUse().equals("-1")) {
            throw new EmptyCorpusToUseList();
        }

        Vector v = new Vector();
        File tempFile = wrapHTML(DocTitle, DocText);

        // invoke the API
        try {
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);
            org.dom4j.Document doc = API.dExecute(tempFile);
            Element elemRoot = doc.getRootElement();

            Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
            if (eDocs == null) {
                throw new Exception(
                        "ClassificationResultSet tag does not exist in XML output");
            }

            Iterator i = eDocs.elements().iterator();
            while (i.hasNext()) {
                Element eDoc = (Element) i.next();

                if (eDoc.getQName().getName().equals("NODE")) {
                    Iterator i2 = eDoc.elements().iterator();
                    NodeDocument nd = new NodeDocument(i2);

                    v.add(nd);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            tempFile.delete();
        }

        return v;
    }

    // get data container containing classify explain data
    public static ExplainDisplayDataContainer getExplainData(int iNodeID,
            int iDocIDSource, String sNodeTitle, String sDocTitle,
            String sCorpusID, ITSAdministrator itsAdminFrame,
            boolean bClassifyDocument) throws Exception {
        Hashtable htArgs = itsAdminFrame.its.getArguments();

        User u = getUser();
        htArgs.put("Corpora", sCorpusID);

        if (iDocIDSource != -1) {
            htArgs.put("DocIDSource", "" + iDocIDSource);
            htArgs.put("DocTitle", sDocTitle);
            htArgs.put("dweight", u.getDocumentWeight() + "");
            htArgs.put("sweight", u.getAbstractWeight() + "");
        }
        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put("batch", "false");
        htArgs.put("nodetoscoreexplain", "" + iNodeID);
        htArgs.put("NodeSet", "" + iNodeID);
        htArgs.put("NumParentTitleTermsAsSigs",
                "" + u.getNumParentTitleTermAsSigs());

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        org.dom4j.Document doc = API.dExecute(false, false);

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer = new ExplainDisplayDataContainer(iNodeID,
                iDocIDSource, sNodeTitle, sDocTitle);
        explainDisplayDataContainer
                .setFromSerializableForm(vDisplayDataInstances);

        return explainDisplayDataContainer;
    }

    // get data container containing classify explain data with custom signature
    // set
    public ExplainDisplayDataContainer getExplainData(ITSTreeNode n,
            int iDocIDSource, String sDocTitle, ITSAdministrator itsAdminFrame,
            Vector vSignaturesObjects, boolean bClassifyDocument)
            throws Exception {
        // n.getNodeDocument()
        Hashtable htArgs = itsAdminFrame.its.getArguments();

        User u = getUser();
        htArgs.put("Corpora", n.get("CORPUSID"));

        if (iDocIDSource != -1) {
            htArgs.put("DocIDSource", "" + iDocIDSource);
            htArgs.put("DocTitle", sDocTitle);
            htArgs.put("dweight", u.getDocumentWeight() + "");
            htArgs.put("sweight", u.getAbstractWeight() + "");
        }
        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put("batch", "false");
        htArgs.put("nodetoscoreexplain", n.get("NODEID"));
        htArgs.put("NodeSet", n.get("NODEID"));
        htArgs.put("Corpora", n.get("CORPUSID"));
        htArgs.put("NodeSize", "" + n.get("NODESIZE"));
        htArgs.put("NodeTitle", "" + n.get("NODETITLE"));

        // explain with sigs - thesaurus and title expand these signatures
        Signatures signatures = new Signatures(vSignaturesObjects);
        Signatures signaturesObject = ITSHelper
                .vSignatureObjectsToSignaturesObject(vSignaturesObjects);
        Signatures signaturesObjectExpanded = signatureExpand(signaturesObject,
                Integer.parseInt(n.get("NODEID")),
                Integer.parseInt(n.get("CORPUSID")));
        htArgs.put("Sigs", signaturesObjectExpanded.toString());
        htArgs.put("NumParentTitleTermsAsSigs",
                "" + u.getNumParentTitleTermAsSigs());

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        org.dom4j.Document doc = API.dExecute(false, false);

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer = new ExplainDisplayDataContainer(
                Integer.parseInt(n.get("NODEID")), iDocIDSource,
                n.get("NODETITLE"), sDocTitle);
        explainDisplayDataContainer
                .setFromSerializableForm(vDisplayDataInstances);

        return explainDisplayDataContainer;
    }

    // get data container containing classify explain data
    public static ExplainDisplayDataContainer getExplainData(int iNodeID,
            String sNodeTitle, String sDocTitle, String sCorpusID,
            ITSAdministrator itsAdminFrame, File f) throws Exception {
        Hashtable htArgs = itsAdminFrame.its.getArguments();

        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put("batch", "false");
        htArgs.put("nodetoscoreexplain", "" + iNodeID);
        htArgs.put("NodeSet", "" + iNodeID);
        htArgs.put("Corpora", sCorpusID);
        htArgs.put("NumParentTitleTermsAsSigs", ""
                + getUser().getNumParentTitleTermAsSigs());

        File tempFile = null;
        boolean bUsingTempFile = false;

        // if the file is of type text, it needs an HTML wrapper before being
        // posted
        if (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
            tempFile = new File("temp.html");
            if (tempFile.exists()) {
                tempFile.delete();
            }
            // System.out.println("Filtering: " + f.getAbsolutePath() + " to " +
            // tempFile.getAbsolutePath());

            try {
                wrapHTMLstatic(f, tempFile);
                bUsingTempFile = true;
            } catch (Exception ex) {
                ex.printStackTrace();
                throw ex;
            }
        } // if the file is of type PDF, filter it before posting
        else if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
            bUsingTempFile = true;
            tempFile = ITS.PDFtoHTML(f);
        } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
            bUsingTempFile = true;
            tempFile = ITS.MSWORDtoHTML(f);
        } else {
            tempFile = f;
            bUsingTempFile = false;
        }

        User u = getUser();
        htArgs.put("Corpora", u.getCorporaToUse());
        if (u.getCorporaToUse().equals("-1")) {
            throw new EmptyCorpusToUseList();
        }

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        org.dom4j.Document doc = API.dExecute(tempFile);

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer = new ExplainDisplayDataContainer(iNodeID,
                -1, sNodeTitle, sDocTitle);
        explainDisplayDataContainer
                .setFromSerializableForm(vDisplayDataInstances);

        if (bUsingTempFile) {
            tempFile.delete();
        }

        return explainDisplayDataContainer;
    }

    // get data container containing classify explain data
    public ExplainDisplayDataContainer getExplainData(int iNodeID,
            int iDocIDSource, String sNodeTitle, String sDocTitle,
            String sCorpusID, ITSAdministrator itsAdminFrame, File f)
            throws Exception {
        Hashtable htArgs = itsAdminFrame.its.getArguments();

        htArgs.put("post", "false");
        htArgs.put("explainscores", "true");
        htArgs.put("batch", "false");
        htArgs.put("nodetoscoreexplain", "" + iNodeID);
        htArgs.put("NodeSet", "" + iNodeID);
        htArgs.put("Corpora", sCorpusID);
        User u = getUser();
        htArgs.put("Corpora", sCorpusID);
        htArgs.put("NumParentTitleTermsAsSigs",
                "" + u.getNumParentTitleTermAsSigs());

        InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", htArgs);

        File tempFile = null;
        Vector v = new Vector();
        boolean bUsingTempFile = false;

        // if the file is of type text, it needs an HTML wrapper before being
        // posted
        if (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
            tempFile = new File("temp.html");
            if (tempFile.exists()) {
                tempFile.delete();
            }
            System.out.println("Filtering: " + f.getAbsolutePath() + " to "
                    + tempFile.getAbsolutePath());

            try {
                wrapHTML(f, tempFile);
                bUsingTempFile = true;
            } catch (Exception ex) {
                ex.printStackTrace();
                throw ex;
            }
        } // if the file is of type PDF, filter it before posting
        else if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
            bUsingTempFile = true;
            tempFile = ITS.PDFtoHTML(f);
        } else if (f.getAbsolutePath().toLowerCase().endsWith(".doc")) {
            bUsingTempFile = true;
            tempFile = ITS.MSWORDtoHTML(f);
        } else {
            tempFile = f;
            bUsingTempFile = false;
        }

        org.dom4j.Document doc = API.dExecute(f);

        if (bUsingTempFile) {
            tempFile.delete();
        }

        Vector vDisplayDataInstances = getExplainDataInternal(doc);

        ExplainDisplayDataContainer explainDisplayDataContainer;
        explainDisplayDataContainer = new ExplainDisplayDataContainer(iNodeID,
                iDocIDSource, sNodeTitle, sDocTitle);
        explainDisplayDataContainer
                .setFromSerializableForm(vDisplayDataInstances);

        return explainDisplayDataContainer;
    }

    public static Vector getExplainDataInternal(org.dom4j.Document doc)
            throws Exception {
        Element elemRoot = doc.getRootElement();
        Element elemIWERROR = elemRoot.element("IWERROR");
        if (elemIWERROR != null) {
            Element elemERRMSG = elemIWERROR.element("ERRMSG");

            if (elemERRMSG == null) {
                throw new Exception("invalid IWERROR TAG from server");
            }
            String sServerMSGwithUserMsgEmbedded = elemERRMSG.getTextTrim();
            String sStartUSERERRMSG = "##USERERRMSG##";
            String sEndUSERERRMSG = "##/USERERRMSG##";
            int iIndexStartUSERERRMSG = sServerMSGwithUserMsgEmbedded
                    .indexOf(sStartUSERERRMSG);
            int iIndexEndUSERERRMSG = sServerMSGwithUserMsgEmbedded
                    .indexOf(sEndUSERERRMSG);

            String sUserError = sServerMSGwithUserMsgEmbedded.substring(
                    iIndexStartUSERERRMSG + sStartUSERERRMSG.length(),
                    iIndexEndUSERERRMSG);
            JOptionPane.showMessageDialog(null,
                    "Failed to retrieve explain data [" + sUserError + "]",
                    "Information", JOptionPane.NO_OPTION);
            return null;

        }
        Element elemEXPLAINDISPLAYSET = elemRoot.element("EXPLAINDISPLAYSET");
        String sXMLizedExplainDisplayDataContainer = elemEXPLAINDISPLAYSET
                .getText().trim();

        return (Vector) XMLBeanSerializeHelper
                .deserializeFromString(sXMLizedExplainDisplayDataContainer
                .trim());
    }

    // Rebuild full text indexes
    public boolean rebuildIndexes() throws Exception {
        try {
            HashTree htArguments = getArguments();
            HashTree htResults = InvokeAPI("tsother.TSRebuildIndexes",
                    htArguments);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        }

        return true;
    }

    public void synchronizeBatchRun() throws Exception {
        synchronizeBatchRun("RDATA");
    }

    public void synchronizeBatchRun(String Tablespace) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("Tablespace", Tablespace);

        // invoke the API
        try {
            HashTree htResults = InvokeAPI("tsclassify.TSLoadBatchRun", htArgs);
        } catch (Exception e) {
            throw e;
        }
    }

    public static File HTMLtoTXT(File HTMLfile) throws Exception {
        File tempFile = new File("temp.txt");

        /*
         * // remove meta tags from the HTML FileInputStream fis = null;
         * PrintWriter out = null; String sOut = ""; try { fis = new
         * FileInputStream(HTMLfile); BufferedReader buf = new
         * BufferedReader(new InputStreamReader(fis)); String sData = new
         * String();
         * 
         * while ((sData = buf.readLine()) != null) { if
         * (sData.toLowerCase().indexOf("META") == -1) sOut = sOut + sData; } }
         * catch (Exception e) { throw e; } finally { fis.close(); }
         * 
         * try { out = new PrintWriter(new
         * FileOutputStream(tempFile.getAbsolutePath(), false));
         * out.print(sOut); } catch (Exception e) { throw e; } finally {
         * out.close(); }
         * 
         * // end remove meta tags
         */

        FileReader in = new FileReader(HTMLfile);

        HTMLtoTXT parser = new HTMLtoTXT();
        parser.parse(in);
        in.close();

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileOutputStream(
                    tempFile.getAbsolutePath(), false));
            out.print(parser.getText());
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }

        return tempFile;
    }

    // Filter a PDF document to an HTML document
    public static File PDFtoHTML(File PDFfile) throws Exception {
        File tempFile = new File("temp.html");

        try {
            System.out.println("Filtering PDF: " + PDFfile.getAbsolutePath()
                    + " to " + tempFile.getAbsolutePath());
            Main.PDFtoHTML(PDFfile.getAbsolutePath(),
                    tempFile.getAbsolutePath());
            wrapHTMLstatic(tempFile);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            throw ex;
        }

        return tempFile;
    }

    // Filter a PDF document to an TEXT document
    public static File PDFtoTXT(File PDFfile) throws Exception {
        File tempFile = new File("temp.txt");

        try {
            System.out.println("Filtering PDF: " + PDFfile.getAbsolutePath()
                    + " to " + tempFile.getAbsolutePath());
            Main.PDFtoHTML(PDFfile.getAbsolutePath(),
                    tempFile.getAbsolutePath());
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            throw ex;
        }

        return tempFile;
    }

    // Filter a WORD document to an TEXT document
    public static File MSWORDtoTXT(File WordFile) throws Exception {
        File tempFile = new File("temp.txt");
        if (tempFile.exists()) {
            tempFile.delete();
        }

        System.out.println("Filtering MSWORD: " + WordFile.getAbsolutePath()
                + " to " + tempFile.getAbsolutePath());

        FileInputStream in = new FileInputStream(WordFile.getAbsolutePath());
        WordExtractor extractor = new WordExtractor();

        String s = extractor.extractText(in);

        return tempFile;
    }

    // Filter a WORD document to an HTML document
    public static File MSWORDtoHTML(File WordFile) throws Exception {
        File tempFile = new File("temp.html");
        if (tempFile.exists()) {
            tempFile.delete();
        }

        System.out.println("Filtering MSWORD: " + WordFile.getAbsolutePath()
                + " to " + tempFile.getAbsolutePath());

        FileInputStream in = new FileInputStream(WordFile.getAbsolutePath());
        WordExtractor extractor = new WordExtractor();

        String s = extractor.extractText(in);
        wrapHTMLstatic(s, tempFile);

        return tempFile;
    }

    private void sessionTimedOut() {
        JOptionPane.showMessageDialog(null,
                "Your session has timed out.  Please restart the application.",
                "Information", JOptionPane.NO_OPTION);
        System.exit(-1);
    }

    // collect up the contents of two ht;s into one - leaves originals unchanged
    private static Hashtable mergeHashTables(Hashtable ht1, Hashtable ht2,
            boolean bOverlapAllowed) throws Exception {

        Hashtable htMerged = new Hashtable();

        Enumeration e = ht1.keys();
        Object k1;
        while (e.hasMoreElements()) {
            k1 = (Object) e.nextElement();
            Object v1 = ht1.get(k1);
            htMerged.put(k1, v1);
        }

        Enumeration e2 = ht2.keys();
        Object k2;
        while (e2.hasMoreElements()) {
            k2 = (Object) e2.nextElement();
            if (!bOverlapAllowed && ht1.get(k2) != null) {
                throw new Exception("merge hashtables - overlap occurred ["
                        + k2 + "]");
            }
            Object v2 = ht2.get(k2);
            htMerged.put(k2, v2);
        }

        return htMerged;
    }

    public org.dom4j.Document getNodesReport(String corpusid, ParamsQueryReport json) throws Exception {
        Hashtable htArgs = getArguments();
        htArgs.put("corpusid", corpusid);
        htArgs.put("paramsquery", JSONObject.fromObject(json).toString());
        org.dom4j.Document doc = InvokeDomAPI("report.TSGenerateReport", htArgs);
        return doc;
    }
}
