package com.iw.ui;

import com.iw.system.ITS;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class WikiImport extends JInternalFrame implements ActionListener {

    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JSpinner spinner;
    private JTextField textFieldSpinner;
    private JButton btnRun;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");

    public WikiImport(ITSAdministrator frame) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);

        setSize(384, 252);
        setTitle("Wiki Import");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        JLabel lblNewLabel = new JLabel("Wiki start Point");
        lblNewLabel.setBounds(12, 12, 110, 15);
        getContentPane().add(lblNewLabel);

        textField = new JTextField();
        textField.setBounds(201, 10, 161, 19);
        getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblImportedTaxonomyName = new JLabel("Imported Taxonomy Name");
        lblImportedTaxonomyName.setBounds(12, 50, 183, 15);
        getContentPane().add(lblImportedTaxonomyName);

        textField_1 = new JTextField();
        textField_1.setBounds(201, 48, 161, 19);
        getContentPane().add(textField_1);
        textField_1.setColumns(10);

        JLabel lblDepthOfCrawl = new JLabel("Depth of Crawl");
        lblDepthOfCrawl.setBounds(12, 89, 110, 15);
        getContentPane().add(lblDepthOfCrawl);

        SpinnerModel model =
                new SpinnerNumberModel(0, //initial value
                0, //min
                null, //max
                1);

        spinner = new JSpinner(model);
        spinner.setBounds(201, 87, 66, 20);
        textFieldSpinner = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
        textFieldSpinner.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    textFieldSpinner.setText(textFieldSpinner.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(spinner);

        btnRun = new JButton("Generate");
        btnRun.setBounds(245, 188, 117, 25);
        btnRun.addActionListener(this);
        getContentPane().add(btnRun);

        JLabel lblCategory = new JLabel("Category");
        lblCategory.setBounds(12, 134, 70, 15);
        getContentPane().add(lblCategory);

        textField_2 = new JTextField();
        textField_2.setBounds(201, 132, 161, 19);
        getContentPane().add(textField_2);
        textField_2.setColumns(10);

        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnRun) {
            runWikiImport();
        }
    }

    private void runWikiImport() {
        ReportWikiImport reportWiki = new ReportWikiImport(ITSframe);
        reportWiki.setVisible(true);
    }
}
