/*
 * Class for connecting for webdav server
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
*/
package com.iw.tools.webdav.connection;

import java.io.IOException;
import java.util.List;


import com.googlecode.sardine.DavResource;
import com.googlecode.sardine.Sardine;
import com.googlecode.sardine.SardineFactory;
import com.googlecode.sardine.impl.SardineException;

public class Webdav {
	
	//Properties
	private String urlBase;
	private String user;
	private String password;
	private Sardine objectSardine;
	private String Path;
	
	public String getPath() {
		return Path;
	}
	public void setPath(String path) {
		Path = path;
	}
	public String getUrlBase() {
		return urlBase;
	}
	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Sardine getObjectSardine() {
		return objectSardine;
	}
	public void setObjectSardine(Sardine objectSardine) {
		this.objectSardine = objectSardine;
	}
	
	/**
	 * list to the archives and directories to webdav
	 * @param url : url of the resource
	 * @return array with the resources including the parent
	 * @throws SardineException
	 */
	public List<DavResource> getListResource(String url) throws SardineException{
		try {
			return this.getObjectSardine().list(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Error en la conexion");
			return null;
		}
	}
	
	/**
	 * Constructor : set the properties
	 * @param urlBase
	 * @param Path
	 * @param user
	 * @param Password
	 * @throws SardineException
	 */
	
	public Webdav(String urlBase,String Path,String user, String Password)throws SardineException{
		objectSardine = SardineFactory.begin(user, Password);
		this.urlBase=urlBase;
		this.Path=Path;
	}
	
	/**
	 * Constructor :set the properties without have user and password
	 * @param urlBase
	 * @param Path
	 */
	public Webdav(String urlBase,String Path){
		objectSardine=SardineFactory.begin();
		this.urlBase=urlBase;
		this.Path=Path;
	}
}
