/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Nov 6, 2003
 * Time: 1:40:38 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.iw.system.*;
import com.iw.tools.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;

import com.iw.tools.SortedList;
import com.iw.tools.SwingWorker;
import com.iw.tools.ThesaurusExport;

import com.oculustech.layout.*;

public class OpenThesaurus extends JPanel implements ActionListener, ListSelectionListener, KeyListener {
    public int iButtonPressed;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;

    // Instance attributes used in this example
    private JInternalFrame MDIframe;
    private JPanel openPanel;
    private SortedList listbox;
    private Vector listData;
    private JButton newButton, openButton, cancelButton, deleteButton, synchButton, propsButton, exportButton;
    private JTextField dataField;
    private JScrollPane scrollPane;

    private Component glass = null;

    public static final int BTN_CONNECT = 0x01;
    public static final int BTN_CANCEL = 0x02;

    public OpenThesaurus(ITSAdministrator frame) throws Exception {
        // Create the data model for this example
        listData = frame.its.getThesauri();

        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Manage Thesaurus", false, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(380, 250);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        glass = frame.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        listbox = new SortedList(listData);
        listbox.setFixedCellWidth(240);
        listbox.sort();
        listbox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listbox.addKeyListener(this);

        if (listData.size() > 0) listbox.setSelectedIndex(0);

        newButton = new JButton("New");
        newButton.setMnemonic('n');
        newButton.addActionListener(this);

        openButton = new JButton("Open");
        openButton.setMnemonic('o');
        openButton.addActionListener(this);

        synchButton = new JButton("Sync");
        synchButton.setMnemonic('s');
        synchButton.addActionListener(this);

        deleteButton = new JButton("Delete");
        deleteButton.setMnemonic('d');
        deleteButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setMnemonic('c');
        cancelButton.addActionListener(this);

        propsButton = new JButton("Props");
        propsButton.setMnemonic('p');
        propsButton.addActionListener(this);
        
        exportButton = new JButton("Export");
        exportButton.setMnemonic('e');
        exportButton.addActionListener(this);

        scrollPane = new JScrollPane();
        scrollPane.getViewport().add(listbox);

        layout.addSpace(15);
        layout.nestBox(OculusLayout.VERTICAL);
        {
        	layout.addSpace(5);
            layout.add(scrollPane, OculusLayout.NO_STRETCH, OculusLayout.WANT_STRETCHED);
            layout.addSpace(15);
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
        	layout.addSpace(5);
            layout.add(newButton);
            layout.addSpace(2);
            layout.add(openButton);
            layout.addSpace(2);
            layout.add(deleteButton);
            layout.addSpace(2);
            layout.add(propsButton);
            layout.addSpace(2);
            layout.add(synchButton);
            layout.addSpace(2);
            layout.add(exportButton);
            layout.addSpace(2);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(5);

        openAtUpperCenter();
        setVisible(true);
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(openButton);
        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
    }

    public void valueChanged(ListSelectionEvent event) {
        // See if this is a listbox selection and the
        // event stream has settled
        if (event.getSource() == listbox
                && !event.getValueIsAdjusting()) {
            // Get the current selection and place it in the
            // edit field
            Thesaurus t = (Thesaurus) listbox.getSelectedValue();
        }
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent event) {
        MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            if (event.getSource() == cancelButton) {
                MDIframe.dispose();
            } // cancel: close the window
            if (event.getSource() == openButton) { // open: open a new window with the thesaurus
                MDIframe.dispose();
                ITSframe.openThesaurus(ITSframe, (Thesaurus) listbox.getSelectedValue());
            }
            if (event.getSource() == deleteButton) { // delete this thesaurus
                try {
                    ITSframe.its.deleteThesaurus((Thesaurus) listbox.getSelectedValue());
                    JOptionPane.showMessageDialog(ITSframe, "Thesaurus was removed successfully.");
                    MDIframe.dispose();
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                    JOptionPane.showMessageDialog(ITSframe, "Thesaurus could not be removed.  Please see "+
                        "the error log for more information.");
                }
            }
            if (event.getSource() == newButton) { // new: create a new thesaurus and open it
                String inputValue =
                        JOptionPane.showInputDialog("Please enter the name of the new thesaurus.");
                Thesaurus t = null;
                try {
                    t = its.createThesaurus(inputValue);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    System.out.println("Could not create thesaurus.");
                }
                MDIframe.dispose();
                ITSframe.openThesaurus(ITSframe, t);
            }
            if (event.getSource() == propsButton) {
                if (listbox.getSelectedValue() == null) return;

                ThesaurusProperties p = new ThesaurusProperties(ITSframe, (Thesaurus) listbox.getSelectedValue());
                p.show();
                MDIframe.dispose();
            }
            
            if (event.getSource() == exportButton) {
                if (listbox.getSelectedValue() == null) return;
                try {
                ThesaurusExport Thes1 = new ThesaurusExport();
                
                Thes1.ThesaurusExport(ITSframe, (Thesaurus) listbox.getSelectedValue());
                
                //p.show();
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(ITSframe, "Thesauri could not be exported at this time.");
                    return;
                }
                JOptionPane.showMessageDialog(ITSframe, "Thesaurus was exported successfully.");
                MDIframe.dispose();
            }
            if (event.getSource() == synchButton) { // sync: synchronize thesaurus with the database NOW
                int i = JOptionPane.showConfirmDialog(this, "This operation may take several minutes.  Continue?",
                        "Confirm", JOptionPane.YES_NO_OPTION);
                if (i == 0) {

                    SwingWorker aWorker = new SwingWorker(event) {

                        protected void doNonUILogic() {
                            glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                            glass.setVisible(true);

                            try {
                                its.synchronizeThesaurus();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(ITSframe, "Thesauri could not be synchronized at this time.");
                                return;
                            }
                            JOptionPane.showMessageDialog(ITSframe, "All thesauri were synchronized successfully.");
                            MDIframe.dispose();
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            glass.setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void keyPressed(KeyEvent evt) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent evt) {
        char ch = evt.getKeyChar();
        if (!Character.isLetterOrDigit(ch))
            return;

        String m_key = "";

        m_key += Character.toLowerCase(ch);

        int len = listbox.getModel().getSize();

        for (int i = 0; i < len; i++) {
            String item = new String(
                    listbox.getModel().getElementAt(i).toString().toLowerCase());
            if (item.startsWith(m_key)) {
                listbox.setSelectedIndex(i);
                listbox.ensureIndexIsVisible(i);
                break;
            }

        }
        listbox.ensureIndexIsVisible(listbox.getSelectedIndex());
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }
}
