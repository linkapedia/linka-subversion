/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Mar 12, 2004
 * Time: 10:51:21 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.oculustech.layout.*;
import com.iw.system.ITS;
import com.iw.system.Document;
import com.iw.system.IllegalQuery;
import com.iw.tools.JSortTable;
import com.iw.tools.SwingWorker;
import com.iw.tools.CQLControls;
import com.iw.tools.DefaultSortTableModel;
import com.iw.ui.conceptalerts.AddAlert;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Vector;

public class SimpleDocSearch extends DocumentSearch implements ActionListener {
    public SimpleDocSearch (ITSAdministrator frame) throws Exception {
        buildSearchMenu(frame);
    }
    public SimpleDocSearch (ITSAdministrator frame, String CQL) throws Exception {
        buildSearchMenu(frame);
        fulltextText.setText(CQL);

        glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        glass.setVisible(true);

        loadResults();

        glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        glass.setVisible(false);
    }

    public void buildSearchMenu (ITSAdministrator frame) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        user = ITS.getUser();

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Advanced Document Search", true, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(675, 480);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        searchButton = new JButton("Search");
        searchButton.addActionListener(this);
        cancelButton = new JButton("Close");
        cancelButton.addActionListener(this);
        openButton = new JButton("Open");
        openButton.addActionListener(this);
        relatedButton = new JButton("Related");
        relatedButton.addActionListener(this);
        documentButton = new JButton("Classify");
        documentButton.addActionListener(this);
        alertButton = new JButton("Alert");
        alertButton.addActionListener(this);

        fulltextLabel = new JLabel("CQL:");
        fulltextLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        fulltextLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        fulltextText = new JTextField();

        browseButton = new JButton("...");
        browseButton.addActionListener(this);

        chartButton = new JButton("Chart");
        chartButton.addActionListener(this);
        chartButton.setToolTipText("Generate a bar chart with a historical perspective of this document set.");

        resultsTable = new JSortTable();

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestGrid(2, 1);
                            {
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(fulltextLabel);
                                    layout.parent();
                                }
                                layout.add(fulltextText);
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                            {
                                layout.add(new JScrollPane(resultsTable));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(searchButton);
                                        layout.addSpace(10);
                                        layout.add(cancelButton);
                                        layout.addSpace(10);
                                        layout.add(openButton);
                                        layout.addSpace(10);
                                        layout.add(relatedButton);
                                        layout.addSpace(10);
                                        //layout.add(new JSeparator());
                                        layout.add(documentButton);
                                        layout.addSpace(10);
                                        layout.add(chartButton);
                                        layout.addSpace(10);
                                        layout.add(alertButton);
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();

            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(searchButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        final DocumentSearch ds = this;

        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    if (e.getSource().equals(cancelButton)) {
                        MDIframe.dispose();
                    } else if (e.getSource().equals(browseButton)) {
                        try {
                            TopicSearch ts = new TopicSearch(ITSframe, ds);
                            ts.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    } else if (e.getSource().equals(searchButton)) {
                        try {
                            loadResults();
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    } else if (e.getSource().equals(chartButton)) { // open chart
                        if (resultsTable.getRowCount() == 0) { return; }

                        Vector v = new Vector();
                        for (int i = 0; i < resultsTable.getRowCount(); i++) {
                            try { v.add((com.iw.system.Document) resultsTable.getModel().getValueAt(i, 0)); }
                            catch (ClassCastException e) { }
                        }
                        DocumentChart dc = new DocumentChart(ITSframe, v);
                        ITSframe.jdp.add(dc);
                        dc.setBounds(10, 30, 900, 580);
                        dc.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        dc.setVisible(true);
                        dc.moveToFront();
                        dc.show();
                    } else if (e.getSource().equals(alertButton)) { // open create alert dialog
                        String sCQL = buildCQL(fulltextText.getText(), titleText.getText(), "",
                                               abstractText.getText(), topicText.getText());

                        AddAlert aa = new AddAlert(ITSframe, sCQL);
                        aa.MDIframe.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        aa.setVisible(true);
                        aa.MDIframe.moveToFront();
                        aa.show();
                    } else if (e.getSource().equals(openButton)) {
                        viewDocument();
                    } else if (e.getSource().equals(relatedButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row >= 0) {
                            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);
                            try {
                                NodeDocView ndv = new NodeDocView(ITSframe, d, null);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    } else if (e.getSource().equals(documentButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row >= 0) {
                            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);
                            if (user.modeAbstractDocument() && d.get("DOCUMENTSUMMARY").equals("")) {
                                JOptionPane.showMessageDialog(ITSframe, "Sorry, that abstract is empty.", "Information", JOptionPane.NO_OPTION);
                                return;
                            }
                            try {
                                NodeDocView ndv = new NodeDocView(ITSframe, d, user);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    protected void loadResults() throws Exception {
        Vector vDocs = new Vector();

        try {
            vDocs = runSearch();
        } catch (IllegalQuery e) {
            JOptionPane.showMessageDialog(ITSframe, "Sorry, your query syntax is invalid.  Please recheck your query " +
                    "syntax and try again.",
                    "Error", JOptionPane.NO_OPTION);
            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);
            return;
        }

        if (vDocs.size() == 0) {
            JOptionPane.showMessageDialog(ITSframe, "No documents were found to match your search criteria.",
                    "Information", JOptionPane.NO_OPTION);

            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);
            return;
        }

        DefaultSortTableModel dtm = new DefaultSortTableModel(vDocs.size(), 3);
        dtm.setEditable(false);
        for (int i = 0; i < vDocs.size(); i++) {
            Document d = (Document) vDocs.elementAt(i);
            dtm.setValueAt(d, i, 0);
            dtm.setValueAt(d.get("DOCUMENTSUMMARY"), i, 1);
            dtm.setValueAt(d.get("DOCURL"), i, 2);

        }
        Vector v = new Vector();
        v.add("Document Title");
        v.add("Abstract");
        v.add("Location");
        dtm.setColumnIdentifiers(v);

        resultsTable.setModel(dtm);
    }

    // return a vector of documents
    public Vector runSearch()
            throws Exception {
        try { return its.CQL(fulltextText.getText()); }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw e;
        }
    }
}
