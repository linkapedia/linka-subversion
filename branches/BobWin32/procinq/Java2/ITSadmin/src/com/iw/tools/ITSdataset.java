package com.iw.tools;

import org.jfree.data.*;

import java.util.*;

/**
 * A generic XY dataset builder.
 *
 * @author Michael Puscar
 */
public class ITSdataset extends AbstractDataset implements IntervalXYDataset {

    /** The start values. */
    private Double[] xStart;

    /** The end values. */
    private Double[] xEnd;

    /** The y values. */
    private Double[] y;

    /** the supplied XY series */
    private XYSeries xySeries;

    /** name */
    private String seriesName;

    /**
     * Creates a new dataset.
     */
    public ITSdataset(XYSeries xy, String Name) {
        xySeries = xy; seriesName = Name;

        xStart = new Double[xy.getItemCount()];
        xEnd = new Double[xy.getItemCount()];
        y = new Double[xy.getItemCount()];

        for (int i = 0; i < xy.getItemCount(); i++) {
            XYDataItem item = (XYDataItem) xy.getDataItem(i);
            xStart[i] = new Double(item.getX().doubleValue());
            xEnd[i] = new Double(item.getX().doubleValue()+1.0);
            y[i] = new Double(item.getY().doubleValue());
        }
    }

    /**
     * Returns the number of series in the dataset.
     *
     * @return the number of series in the dataset.
     */
    public int getSeriesCount() {
        return 1;
    }

    /**
     * Returns the name of a series.
     *
     * @param series the series (zero-based index).
     *
     * @return the series name.
     */
    public String getSeriesName(int series) {
        return seriesName;
    }

    /**
     * Returns the number of items in a series.
     *
     * @param series the series (zero-based index).
     *
     * @return the number of items within a series.
     */
    public int getItemCount(int series) {
        return xySeries.getItemCount();
    }

    /**
     * Returns the x-value for an item within a series.
     * <P>
     * The implementation is responsible for ensuring that the x-values are presented in ascending
     * order.
     *
     * @param series  the series (zero-based index).
     * @param item  the item (zero-based index).
     *
     * @return  the x-value for an item within a series.
     */
    public Number getXValue(int series, int item) {
        return xStart[item];
    }

    /**
     * Returns the y-value for an item within a series.
     *
     * @param series  the series (zero-based index).
     * @param item  the item (zero-based index).
     *
     * @return the y-value for an item within a series.
     */
    public Number getYValue(int series, int item) {
        return y[item];
    }

    /**
     * Returns the starting X value for the specified series and item.
     *
     * @param series  the series (zero-based index).
     * @param item  the item within a series (zero-based index).
     *
     * @return the start x value.
     */
    public Number getStartXValue(int series, int item) {
        return xStart[item];
    }

    /**
     * Returns the ending X value for the specified series and item.
     *
     * @param series  the series (zero-based index).
     * @param item  the item within a series (zero-based index).
     *
     * @return the end x value.
     */
    public Number getEndXValue(int series, int item) {
        return xEnd[item];
    }

    /**
     * Returns the starting Y value for the specified series and item.
     *
     * @param series  the series (zero-based index).
     * @param item  the item within a series (zero-based index).
     *
     * @return the start y value.
     */
    public Number getStartYValue(int series, int item) {
        return y[item];
    }

    /**
     * Returns the ending Y value for the specified series and item.
     *
     * @param series  the series (zero-based index).
     * @param item  the item within a series (zero-based index).
     *
     * @return the end y value.
     */
    public Number getEndYValue(int series, int item) {
        return y[item];
    }

    /**
     * Registers an object for notification of changes to the dataset.
     *
     * @param listener  the object to register.
     */
    public void addChangeListener(DatasetChangeListener listener) {
    }

    /**
     * Deregisters an object for notification of changes to the dataset.
     *
     * @param listener  the object to deregister.
     */
    public void removeChangeListener(DatasetChangeListener listener) {
    }

}
