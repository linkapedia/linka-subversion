package com.iw.ui.explain;

import com.iw.ui.ITSAdministrator;
import com.iw.system.Document;
import com.iw.system.InvokeAPI;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.oculustech.layout.OculusBox;
import com.oculustech.layout.OculusLayout;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Iterator;
import java.beans.XMLEncoder;
import java.io.*;
import java.net.URLEncoder;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;

public class ExplainDisplayPane  extends ExplainDisplayPaneLayoutManual  implements ActionListener , KeyListener
{

    ITSAdministrator itsAdminFrame = null;
    JInternalFrame jinternalframe = null;

    public ExplainDisplayPane (ITSAdministrator itsadminFrame , ExplainDisplayDataContainer explainDisplayDataContainer,
                               JInternalFrame jinternalframe
                               ) throws Exception
    {
        super (itsadminFrame, explainDisplayDataContainer);
        this.itsAdminFrame = itsadminFrame;
        this.jinternalframe = jinternalframe;
        jbCancel.addActionListener (this);
    }

    public void keyReleased (KeyEvent ke)
    {

        try
        {
            if (ke.getKeyCode () == KeyEvent.VK_ENTER)
            {
                itsAdminFrame.setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
            }
        } catch ( Exception ex )
        {
            ex.printStackTrace ();
        } finally
        {
            itsAdminFrame.setCursor (Cursor.getPredefinedCursor (Cursor.DEFAULT_CURSOR));
        }
    }

    /** Handle the key typed event from the text field. */
    public void keyTyped (KeyEvent e)
    {
    }

    public void keyPressed (KeyEvent e)
    {
    }


    public void actionPerformed (ActionEvent event)
    {
        if (event.getSource ().equals (jbCancel))
        {
            jinternalframe.dispose();
        }
    }






}
