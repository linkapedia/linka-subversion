package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class AutonomyOut extends ExportElement {
    public String corpusID = null;
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {
        corpusID = c.getID();
        if (ppbin != null) this.ppb = ppbin;

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        File f = new File(c.getName().replaceAll(" ", "_")+"-autonomy.xml");
        if (f.exists()) { throw new Exception("Error: File: "+f.getAbsolutePath()+" already exists."); }

        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<autnresponse xmlns:autn=\"http://schemas.autonomy.com/aci/\">\n");
        out.write("<action>CATEGORYEXPORTTOXML</action>\n");
        out.write("<response>SUCCESS</response>\n");
        out.write("<responsedata>\n");
        out.write("<autn:categories>\n");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = '-1'", 1, 100000);

        Hashtable ht = new Hashtable();

        for (int i = 0; i < vNodes.size(); i++) {
            // *** WRITE NODE INFORMATION HERE
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);
            Vector vHave = new Vector();
            vHave = server.getMusthaves(n.get("NODEID"));
            writeNode(server, n, signatures, out, vHave, null);
            ht.put(n.get("NODEID"), n.get("NODETITLE"));
        }

        out.write("</autn:categories>\n");
        out.write("</responsedata>\n");
        out.write("</autnresponse>\n");

        out.close();

        return f;
    }

    public void writeNode(ITS server, ITSTreeNode n, Signatures s, Writer out, Vector vHave, String parent) throws Exception {
        out.write("<autn:category>\n");
        if (parent == null)
            out.write("    <autn:name>"+getEncodeName(n.get("NODETITLE"))+"</autn:name>\n");
        else
            out.write("    <autn:name>"+parent+" > "+getEncodeName(n.get("NODETITLE"))+"</autn:name>\n");

        out.write("    <autn:id>"+n.get("NODEID")+"</autn:id>\n");

        // must have terms (optional)
        if (vHave.size() > 0) {
            out.write("    <autn:trainingelement>\n");
            out.write("        <autn:type>BOOLEAN</autn:type>\n");
            out.write("        <autn:content>");
            for (int i = 0; i < vHave.size(); i++) {
                String mustword = (String) vHave.elementAt(i);
                if (i > 0) out.write(" OR ");
                if (mustword.indexOf(" ") > -1) out.write("&quot;");
                out.write(getEncodeName(mustword));
                if (mustword.indexOf(" ") > -1) out.write("&quot;");
            }
            out.write("</autn:content>\n");
            out.write("        <autn:language>ENGLISH</autn:language>\n");
            out.write("    </autn:trainingelement>\n");
        }

        out.write("    <autn:details>\n");

        // signatures (mandatory)
        if (s.size() > 0) {
            out.write("       <autn:modifiedterms>");
            for (int j = 0; j < s.size(); j++) {
                Signature sig = s.elementAt(j);
                if (j > 0) out.write(",");
                //if (sig.getWord().indexOf(" ") > -1) out.write("&quot;");

                writeModifiedTerms(sig.getWord(), out);
            } out.write("</autn:modifiedterms>\n");
            out.write("       <autn:modifiedweights>");
            for (int j = 0; j < s.size(); j++) {
                Signature sig = s.elementAt(j);
                if (j > 0) out.write(",");
                if (sig.getWord().indexOf(" ") > -1) {
                   for (int wordcount = 0; wordcount <= sig.getWord().split(" ").length; wordcount++) {
                       if (wordcount > 0) out.write(",");
                       out.write(new Double(sig.getWeight()).intValue()+"");
                   }
                } else {
                    out.write(new Double(sig.getWeight()).intValue()+"");
                }
            } out.write("</autn:modifiedweights>\n");
        } else {
            // signatures are mandatory, just put in the title terms then
            out.write("       <autn:modifiedterms>");
            String title = n.get("NODETITLE");
            writeModifiedTerms(title.toLowerCase(), out);
            out.write("       </autn:modifiedterms>");

            out.write("       <autn:modifiedweights>");

            if (title.indexOf(" ") > -1) {
               for (int wordcount = 0; wordcount <= title.split(" ").length; wordcount++) {
                   if (wordcount > 0) out.write(",");
                   out.write("100");
               }
            } else {
                out.write("100");
            }
            out.write("       </autn:modifiedweights>\n");
        }

        out.write("        <autn:threshold>60</autn:threshold>\n");
        out.write("        <autn:Databases></autn:Databases>\n");
        out.write("        <autn:FieldText></autn:FieldText>\n");
        out.write("        <autn:NumResults>6</autn:NumResults>\n");
        out.write("        <autn:userfields>\n");
        out.write("        </autn:userfields>\n");
        out.write("    </autn:details>\n");

        nodesProcessed++;
        if (ppb != null) {
            long perc = (nodesProcessed * 100) / totalNodes;
            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
        }

        out.write("</autn:category>\n");

        // insert sub nodes here, if applicable
        // ****** CHANGED BY MAP on 5/4/7, FLATTEN HIERARCHY ****** //
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = '"+n.get("NODEID")+"'", 1, 100000);
        for (int k = 0; k < vNodes.size(); k++) {
            ITSTreeNode child = (ITSTreeNode) vNodes.elementAt(k);
            Vector vSignatures = server.getNodeSignatures(child);
            Signatures signaturesChild = new Signatures(vSignatures);
            Vector vHaveChild = new Vector();
            vHaveChild = server.getMusthaves(child.get("NODEID"));
            if (parent == null)
                writeNode(server, child, signaturesChild, out, vHaveChild, getEncodeName(n.get("NODETITLE")));
            else
                writeNode(server, child, signaturesChild, out, vHaveChild, parent + " > " + getEncodeName(n.get("NODETITLE")));
        }
    }

    public void writeModifiedTerms(String wordin, Writer out) throws Exception {
        //wordin = wordin.replaceAll("(", "");
        //wordin = wordin.replaceAll(")", "");

        if (wordin.indexOf(" ") > -1) {
            // loop through each word, and stem it next to the other
            // words 1-2, 2-3, 3-4, 4-5, etc
            // example: Apple Computer Inc would be app~, app~comput~,computinc~, inc~
            String[] words = wordin.split(" ");
            for (int wordloop = 0; wordloop < words.length; wordloop++) {
                String word = words[wordloop];
                String stem = word;

                try { stem = PorterStemmer.stem(word); }
                catch (Exception e) { }

                String output = getEncodeName(stem.toLowerCase());
                if (wordloop > 0) output = ","+output;
                out.write(output);

                if (wordloop != words.length-1) {
                    word = words[wordloop+1];
                    stem = word;

                    try { stem = PorterStemmer.stem(word); }
                    catch (Exception e) { }

                    out.write(getEncodeName(stem.toLowerCase()));
                }

            }

        } else {
            String word = wordin;
            String stem = word;

            try { stem = PorterStemmer.stem(word); }
            catch (Exception e) { }

            out.write(getEncodeName(stem.toLowerCase()));
        }
    }

    public String generateID(Node n) {
        return generateID((String) n.get("NODEID"), (String) n.get("NODETITLE"));
    }
    public String generateID(String NodeID, String NodeTitle) {
        if (NodeTitle == null) {
            System.out.println("Node Title is null for: "+NodeID); NodeTitle = "X";
        }
        if (NodeTitle.length() > 15) NodeTitle = NodeTitle.substring(4,15);

        StringBuffer sb = new StringBuffer("top"+getEncodeName(NodeTitle)+"X-"+NodeID);
        StringBuffer sbout = new StringBuffer();

        for (int i = 0; i < sb.length(); i++) {
            //System.out.println("i: "+i+" (of "+sb.length()+")");
            if (Character.isLetterOrDigit(sb.charAt(i)))
                sbout.append(sb.charAt(i));
        }

        return sbout.toString();
    }

    public static String getEncodeName(String s) {
        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        s = s.replaceAll("\"", "&quot;");
        s = s.replaceAll(">", "&gt;");

        return s;
    }
 private String GetMustHave(Vector vHave){

    	StringBuffer buffer = new StringBuffer();
        if (vHave.size() >0){
        	for (int i = 0; i < vHave.size(); i++) {

        		buffer.append(vHave.elementAt(i).toString().trim());
        		if (i == 0 & vHave.size()>1){
        			buffer.append(" || ");
        		}
        		if (i !=0 & i !=vHave.size() -1 ){
        			buffer.append(" || ");
        		}


            }
          }



    	// output must haves from data structure

    return buffer.toString();
    }

}