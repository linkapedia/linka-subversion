/*
 * class static for manager properties of webdav
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Oct 10, 2011, 11:23 AM
*/

package com.iw.tools;

import java.util.ResourceBundle;

public class ConfClient {
	
	 private static ResourceBundle bundle =
		 ResourceBundle.getBundle("/properties/client");

	public static String getValue(String key) {
		return bundle.getString(key);
	}

	public static int getIntValue(String key) {
		return Integer.parseInt(bundle.getString(key));
	}
}
