package com.iw.ui.explain;

import com.iw.ui.ITSAdministrator;
import com.iw.system.*;
import com.iw.tools.DefaultSortTableModel;

import javax.swing.table.AbstractTableModel;
import javax.swing.*;
import java.util.*;
import java.io.*;

public class TableModelExplainNodeDocScore extends DefaultSortTableModel { //extends org.dom4j.swing.XMLTableModel {
        // implements com.iw.utils.XMLSerializeJava {
    private static boolean DEBUG = true;

    String[] sArrColNames = null;
    Object[][] oArrArrData = new Object[0][0];
    Document doc = null;
    ITSTreeNode node = null;

    public TableModelExplainNodeDocScore (String[] sArrColNames, Object[][] oArrArrData)
    {
         super (oArrArrData, sArrColNames);
         this.sArrColNames = sArrColNames;
         this.oArrArrData = oArrArrData;
         this.node = node;
    }

    public int getColumnCount() {
        return super.getColumnCount();
    }

    public int getRowCount() {
        return (super.getRowCount());
    }

    public String[] getColumnNames() {
        return sArrColNames;
    }

    public String getColumnName(int col) {
        return sArrColNames[col];
    }

    public Object getValueAt(int row, int col) {
        try {
            return oArrArrData[row][col];
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Object[][] getData() {
        return oArrArrData;
    }

    /*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     */
    public Class getColumnClass(int c) {
        try {
        return getValueAt(0, c).getClass();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int row, int col) {
        super.setValueAt(value, row, col);
        oArrArrData[row][col] = value;
        fireTableCellUpdated(row, col);
    }

    private void printDebugData() {
        int numRows = getRowCount();
        int numCols = getColumnCount();

        for (int i=0; i < numRows; i++) {
            System.out.print("    row " + i + ":");
            for (int j=0; j < numCols; j++) {
                System.out.print("  " + oArrArrData[i][j]);
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }
}

