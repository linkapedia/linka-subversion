/*
 * Created by IntelliJ IDEA.
 * User: Intellisophic
 * Date: Jun 29, 2004
 * Time: 1:55:31 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.system;

import org.dom4j.*;
import java.util.*;

// this class represents a single piece of PDF data within an XML corpus ingestion
public class ParagraphElement {

    // paragraph elements
    private boolean pEmphasis = false;
    private boolean pBold = false;
    private boolean pItalic = false;
    private String pFont = "None";
    private String pFontColor = "None";
    private String pFontSize = "0";
    private String pHeight = "0.0";
    private String pOrigFont = "None";

    public ParagraphElement() { }
    public ParagraphElement(ElementPath ep) {
        fillParagraph(ep);
    }

    // accessor functions
    public boolean IsParagraphEmphasis() { return pEmphasis; }
    public boolean IsParagraphBold() { return pBold; }
    public boolean IsParagraphItalic() { return pItalic; }

    public String getFont() { return pFont; }
    public String getFontColor() { return pFontColor; }
    public String getFontSize() { return pFontSize; }
    public String getParagraphHeight() { return pHeight; }
    public String getOrigFont() { return pOrigFont; }
    public String getHeight() { return pHeight; }

    public String getID() {
        return pFontSize+"-"+pOrigFont;
    }

    public void fillParagraph(ElementPath ep) {
        if (ep.getPath().endsWith("PARAGRAPH")) {
            Element e = ep.getCurrent();

            Attribute font = e.attribute("font"); if (font != null) pFont = font.getText();
            Attribute fontColor = e.attribute("font-color"); if (fontColor != null) pFontColor = fontColor.getText();
            Attribute fontSize = e.attribute("font-size"); if (fontSize != null) pFontSize = fontSize.getText();
            Attribute height = e.attribute("height"); if (height != null) pHeight = height.getText();
            Attribute origFont = e.attribute("origfont"); if (origFont != null) pOrigFont = origFont.getText();
        }
    }
}
