/*
 * principal thread 
 * @authors Andres F. Restrepo A.
 *
 * @version 1.0
 * Date : Dic 12, 2011, 18:53 AM
 */
package com.iw.tools.wiki.init;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.apache.commons.lang.StringUtils;

import com.iw.tools.ConfClient;
import com.iw.tools.wiki.util.WikiUtil;

public class StartProcess implements Runnable {

	private String categoryName;
	private WikiUtil u;
	private ArrayList<String> pagesToCsv = new ArrayList<String>();
	private ArrayList<String> csvRow = new ArrayList<String>();
	private IWikiNotify wikiNotify;
	private int depth;
	private int cont=1;
	/**
	 * 
	 * @param categoryName
	 */
	public StartProcess(String category,IWikiNotify wikiNotify)
	{
		this.wikiNotify=wikiNotify;
		this.categoryName=category;
		
	}
	/**
	 * 
	 */
	@Override
	public void run() {
		String categoryOnly = this.categoryName.replace("Category:", "");
		depth = ConfClient.getIntValue("wikipedia.crawl.depth");
		try {
			u = new WikiUtil(categoryOnly);
			startProccess(u.getPageIdFromTitle(this.categoryName), categoryOnly);
			this.wikiNotify.wikiNotify("FINISHED >> "+this.categoryName);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Process recursive to build CSV
	 * 
	 * @param categoryName
	 */
	private void startProccess(String categoryid, String categoryTitle) {
		LinkedHashMap<String, String> categories = null;
		LinkedHashMap<String, String> pages = null;
		String source = "";
		HashMap<String, String> mapContent = new HashMap<String, String>();

		categories = u.getCategoryMembers(categoryid, "subcat");
		pages = u.getCategoryMembers(categoryid, "page");

		// get pageid to source category
		Iterator<String> iterP = pages.keySet().iterator();
		String pageToCategoryId = null;
		String keyp = "";
		while (iterP.hasNext()) {
			keyp = iterP.next();
			if (pages.get(keyp).equalsIgnoreCase(categoryTitle)) {
				pageToCategoryId = keyp;
				break;
			}
		}
		if (pageToCategoryId == null) {
			source = null;
		} else {
			// source of the category
			source = u.getSingleContent(pageToCategoryId);
		}
		categoryTitle = u.filterNode(categoryTitle);

		csvRow.add(categoryTitle);

		source = u.filterSource(source);

		if (source != null) {
			csvRow.add("source");
			csvRow.add("\"" + source + "\"");
			pagesToCsv.add(StringUtils.join(csvRow.toArray(), ","));
			csvRow.remove(csvRow.size() - 1);
			csvRow.remove(csvRow.size() - 1);
		} else {
			pagesToCsv.add(StringUtils.join(csvRow.toArray(), ","));
		}

		ArrayList<String> a = new ArrayList<String>();
		a.addAll(pages.keySet());
		mapContent = u.getContent(a);

		// source of pages
		Iterator<String> iterPages = pages.keySet().iterator();
		String pageid = "";
		String pagename = "";
		while (iterPages.hasNext()) {
			pageid = iterPages.next();
			pagename = pages.get(pageid);
			if (!pagename.equalsIgnoreCase(categoryTitle)) {
				if (!pagename.contains("Portal:")) {
					source = "";
					source = mapContent.get(pageid);

					pagename = u.filterNode(pagename);

					csvRow.add(pagename);

					source = u.filterSource(source);

					if (source != null) {
						csvRow.add("source");
						csvRow.add("\"" + source + "\"");
						pagesToCsv.add(StringUtils.join(csvRow.toArray(), ","));
						csvRow.remove(csvRow.size() - 1);
						csvRow.remove(csvRow.size() - 1);
						csvRow.remove(csvRow.size() - 1);
					} else {
						pagesToCsv.add(StringUtils.join(csvRow.toArray(), ","));
						csvRow.remove(csvRow.size() - 1);
					}
				}
			}
		}
		u.writeCsv(pagesToCsv);
		pagesToCsv.clear();
		Iterator<String> iterCategory = categories.keySet().iterator();
		String subcatid = "";
		String subcatname = "";
		while (iterCategory.hasNext()) {
			subcatid = iterCategory.next();
			subcatname = categories.get(subcatid);
			wikiNotify.wikiNotify("Generating >> " + subcatname+" from "+this.categoryName);
			if(cont<=depth){
				cont++;
				startProccess(subcatid, subcatname);
				cont--;
			}else{
				break;
			}
			
		}
		if (!csvRow.isEmpty())
			csvRow.remove(csvRow.size() - 1);
	}
}
