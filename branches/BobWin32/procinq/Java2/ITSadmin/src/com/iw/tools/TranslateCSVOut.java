package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class TranslateCSVOut extends ExportElement {
    public String corpusID = "";
    public ITSTreeNode rootNode = null;
    public Hashtable nodes = new Hashtable();

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {

        corpusID = c.getID();

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { throw new Exception("No root node found for corpus "+corpusID); }

        // open two files: the .otl file for the topic definitions and a .tax file to create the hierarchy
        File fotl = new File(c.getName().replaceAll(" ", "_")+"-for-translate.csv");
        if (fotl.exists()) { fotl.delete(); }

        FileOutputStream foso = new FileOutputStream(fotl);
        Writer outo = new OutputStreamWriter(foso, "UTF8");

        // outo.write("Start of file \r\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        writeNode(rootNode, server, outo);

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, outo);

        // outo.write("</UltraseekTopics>\r\n"); -- end of file

        outo.close();

        return fotl;
    }

    private void writeNode(ITSTreeNode n, ITS server, Writer outo) throws Exception {

        if (!((String) n.get("PARENTID")).equals("-1")) {
        	outo.write(n.get("NODEID")+" || 1 || "+n.get("NODETITLE")+" || "+n.get("NODESIZE")+"\r\n");

            // loop through each must have term
            Vector v = server.getMusthaves(n.get("NODEID"));
            for (int i = 0; i < v.size(); i++) {
                outo.write(n.get("NODEID")+" || 2 || "+(String) v.elementAt(i)+" || 1\r\n");
            }

            // loop through each signature term
            v = server.getNodeSignatures(n);
            for (int i = 0; i < v.size(); i++) {
                Signature s = (Signature) v.elementAt(i);
                outo.write(n.get("NODEID")+" || 3 || "+s.getWord()+" || "+new Double(s.getWeight()).intValue()+"\r\n");
            }
        }

        nodesProcessed++;
        if (ppb != null) {
            long perc = (nodesProcessed*100) / totalNodes;
            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
        }
    }


    private void buildChildren(ITS server, ITSTreeNode p, Writer outo) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() > 0) {
            for (int i = 0; i < vNodes.size(); i++) {
                ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
                writeNode(n, server, outo);

                buildChildren(server, n, outo);
            }
        }
        //int depth = new Integer(p.get("DEPTHFROMROOT")).intValue();
        //for (int i = 0; i < depth; i++) { outo.write(" "); }
    }


    private static String ReplaceXMLChars (String s){
    	 s = s.replaceAll("&amp;", "&");
         s = s.replaceAll("&", "&amp;");
         s = s.replaceAll("<", "&lt;");
         s = s.replaceAll("\"", "&quot;");
         s = s.replaceAll(">", "&gt;");

         return s;

    }

    private static String replaceNonAlpha (String s) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (Character.isLetterOrDigit(chars[i])) buffer.append(chars[i]);
            else buffer.append("-");
        }
        return buffer.toString();
    }
}