package com.iw.ui;

import com.iw.system.Corpus;
import com.iw.system.ITS;
import com.iw.system.ITSTreeNode;
import com.iw.system.ModelNodeReport;
import com.iw.system.ModelReport;
import com.iw.system.ParamsQueryReport;
import com.iw.tools.ComboBoxTextFilter;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.dom4j.QName;

public final class SignatureQA extends JInternalFrame implements ActionListener {

    private JTable table;
    private JButton btnNewButton;
    private JButton btnCancel;
    private JComboBox comboBox;
    private Corpus currentTaxonomy;
    private String currentText;
    private List<Corpus> listData;
    private JTextField textfield;
    private DefaultTableModel modelTable;
    private JLabel label;
    private Thread worker;
    private JSpinner spinner;
    private JSpinner spinner_1;
    private JSpinner spinner_2;
    private HashMap componentMap;
    private JTextField textFieldSpinner;
    private JTextField textFieldSpinner_1;
    private JTextField textFieldSpinner_2;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");

    public SignatureQA(ITSAdministrator frame) {
        
        setResizable(false);
        setClosable(true);
        setIconifiable(true);

        setTitle("QA Report");
        setLocation(60, 60);
        setSize(870, 500);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        comboBox = new JComboBox();
        comboBox.setBounds(28, 32, 235, 24);
        comboBox.setToolTipText("");
        comboBox.addActionListener(this);
        comboBox.setEditable(true);
        comboBox.getEditor().getEditorComponent().addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                currentTaxonomy = null;
                textfield = (JTextField) comboBox.getEditor().getEditorComponent();
                if (!textfield.getText().equals(currentText) && !(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    currentText = textfield.getText();
                    comboFilter(textfield.getText());
                }
            }

            public void keyTyped(KeyEvent e) {
            }
        });

        getContentPane().add(comboBox);

        JLabel lblNewLabel = new JLabel("Check signature less than");
        lblNewLabel.setBounds(281, 36, 193, 15);
        getContentPane().add(lblNewLabel);

        SpinnerModel model =
                new SpinnerNumberModel(0, //initial value
                0, //min
                null, //max
                1);

        SpinnerModel model1 =
                new SpinnerNumberModel(0, //initial value
                0, //min
                null, //max
                1);

        SpinnerModel model2 =
                new SpinnerNumberModel(0, //initial value
                0, //min
                null, //max
                1);

        spinner = new JSpinner(model);
        spinner.setBounds(471, 34, 54, 22);
        textFieldSpinner = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
        textFieldSpinner.setText("5");
        textFieldSpinner.addKeyListener(new KeyListener() {
            String currentText = null;

            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    textFieldSpinner.setText(textFieldSpinner.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(spinner);

        JLabel lblOrMoreThan = new JLabel("or more than");
        lblOrMoreThan.setBounds(543, 36, 103, 15);
        getContentPane().add(lblOrMoreThan);

        spinner_1 = new JSpinner(model1);
        spinner_1.setBounds(651, 34, 54, 20);
        textFieldSpinner_1 = ((JSpinner.DefaultEditor) spinner_1.getEditor()).getTextField();
        textFieldSpinner_1.setText("40");
        textFieldSpinner_1.addKeyListener(new KeyListener() {
            String currentText = null;

            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                currentText = textFieldSpinner_1.getText();
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    textFieldSpinner_1.setText(textFieldSpinner_1.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(spinner_1);

        JLabel lblCheckMustHaves = new JLabel("Check Must Haves = 0 or more than");
        lblCheckMustHaves.setBounds(281, 74, 262, 15);
        getContentPane().add(lblCheckMustHaves);

        spinner_2 = new JSpinner(model2);
        spinner_2.setBounds(539, 72, 54, 20);
        textFieldSpinner_2 = ((JSpinner.DefaultEditor) spinner_2.getEditor()).getTextField();
        textFieldSpinner_2.setText("5");
        textFieldSpinner_2.addKeyListener(new KeyListener() {
            String currentText = null;

            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                currentText = textFieldSpinner_2.getText();
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    textFieldSpinner_2.setText(textFieldSpinner_2.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(spinner_2);

        btnNewButton = new JButton("Run report");
        btnNewButton.setBounds(717, 32, 117, 25);
        btnNewButton.addActionListener(this);
        getContentPane().add(btnNewButton);

        btnCancel = new JButton("Cancel");
        btnCancel.setBounds(717, 69, 117, 25);
        btnCancel.addActionListener(this);
        getContentPane().add(btnCancel);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(28, 112, 662, 339);
        getContentPane().add(scrollPane);

        table = new JTable();
        modelTable = new DefaultTableModel(
                new Object[][]{},
                new String[]{
            "Node Id", "Node Name", "Links", "Signatures", "Must Haves", "Images"
        });
        table.setModel(modelTable);

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
                setCursor(cursor);
                tableMouseClicked(evt);
                cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                setCursor(cursor);
            }
        });

        label = new JLabel("loading...", com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif"), JLabel.CENTER);
        label.setBounds(717, 106, 117, 38);
        label.setVisible(false);
        getContentPane().add(label);

        scrollPane.setViewportView(table);
        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);

        loadListTaxonomy();
    }

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            String nodeid = null;
            int rowindex = table.getSelectedRow();
            nodeid = table.getModel().getValueAt(rowindex, 0).toString();
            editNode(nodeid);
        }

    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == btnNewButton) {
            if (comboBox.getSelectedItem() instanceof Corpus) {
                currentTaxonomy = (Corpus) comboBox.getSelectedItem();
                currentText = currentTaxonomy.getName();
                startGifAnimationLoading();
            } else {
                JOptionPane.showMessageDialog(this, "Select taxonomy");
            }
        } else if (event.getSource() == btnCancel) {
            this.dispose();
        } else if (event.getSource() == comboBox) {
            if (comboBox.getSelectedItem() instanceof Corpus) {
                currentTaxonomy = (Corpus) comboBox.getSelectedItem();
                currentText = currentTaxonomy.getName();
            }
        }
    }

    public void startGifAnimationLoading() {
        worker = new Thread() {
            @Override
            public void run() {
                loadDataFromOracleDb();
            }
        };
        worker.start();
    }

    public void loadDataFromOracleDb() {
        try {
            ParamsQueryReport json = new ParamsQueryReport();
            json.setLessSignature(textFieldSpinner.getText());
            json.setMoreSignature(textFieldSpinner_1.getText());
            json.setLessMusthave("0");
            json.setMoreMusthave(textFieldSpinner_2.getText());

            label.setVisible(true);
            org.dom4j.Document nodes = ITSframe.its.getNodesReport(currentTaxonomy.getID(), json);
            JAXBContext jc = JAXBContext.newInstance(ModelReport.class);
            StringReader reader = new StringReader(nodes.getRootElement().element(new QName("REPORT")).asXML());

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            ModelReport nodesReport = (ModelReport) unmarshaller.unmarshal(reader);

            modelTable = new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                "Node Id", "Node Name", "Links", "Signatures", "Must Haves", "Images"
            }) {
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };

            table.setModel(modelTable);

            if (nodesReport.getNodes() != null) {
                for (ModelNodeReport node : nodesReport.getNodes()) {
                    modelTable.addRow(new Object[]{node.getNodeId(), node.getNodeTitle(), node.getNodeLinks(), node.getNodeSignatures(), node.getNodeMusthaves(), node.getNodeImages()});
                }
            } else {
                JOptionPane.showMessageDialog(this, "Not results found!");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            label.setVisible(false);
        }
    }

    public void loadListTaxonomy() {
        try {
            listData = its.getCorporaList();

            for (Corpus item : listData) {
                item.setName(item.getName().trim());
                comboBox.addItem(item);
            }
            if (comboBox.getSelectedItem() instanceof Corpus) {
                currentTaxonomy = (Corpus) comboBox.getSelectedItem();
                currentText = currentTaxonomy.getName();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public void comboFilter(String enteredText) {
        if (enteredText.equals("")) {
            comboBox.setModel(new DefaultComboBoxModel(listData.toArray()));
            comboBox.setSelectedItem("");
            comboBox.showPopup();
            return;
        }

        List<Corpus> newList = new ArrayList<Corpus>();
        int size = listData.size();
        Corpus item = null;
        String name = null;

        for (int i = 0; i < size; i++) {
            item = listData.get(i);
            name = item.getName().toLowerCase().trim();
            if (name.length() >= enteredText.length()) {
                if (name.substring(0, enteredText.length()).equals(enteredText.toLowerCase().trim())) {
                    newList.add(item);
                }
            }
        }

        if (newList.size() > 0) {
            comboBox.removeAllItems();
            comboBox.setModel(new DefaultComboBoxModel(newList.toArray()));
            comboBox.setSelectedItem(enteredText);
            comboBox.showPopup();
        } else {
            currentTaxonomy = null;
            comboBox.hidePopup();
        }
    }

    public List<Object> getAllItemsComboBox(JComboBox combobox) {
        List<Object> temp = new ArrayList<Object>();
        int size = combobox.getItemCount();

        for (int i = 0; i < size; i++) {
            temp.add(combobox.getItemAt(i));
        }
        return temp;
    }

    public void editNode(String nodeid) {
        Vector vNodes = new Vector();
        Hashtable htCorpus = new Hashtable();

        try {
            vNodes = ITSframe.its.topicSearchNode(nodeid);
            ITSTreeNode n = (ITSTreeNode) vNodes.get(0);

            Corpus c = currentTaxonomy;
            JDesktopPane jdesktoppane = ITSframe.jdp;
            CorpusManagement DragAndDrop = null;

            try {
                DragAndDrop = new CorpusManagement(ITSframe, c, n);
            } catch (Exception ex) {
                System.out.println("Could not create corpus management frame.");
                ex.printStackTrace(System.out);
                return;
            }

            DragAndDrop.setBounds(50, 50, 850, 550);
            DragAndDrop.setVisible(true);
            DragAndDrop.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            jdesktoppane.add(DragAndDrop);
            DragAndDrop.moveToFront();

            jdesktoppane.setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
}
