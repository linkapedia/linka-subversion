/*
 * GUI : select the taxonomies to send to server
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
 */

package com.iw.ui;

import com.iw.system.*;
import com.iw.tools.TextFilterList;

import javax.swing.*;

import java.awt.event.*;
import java.awt.*;
import java.util.Vector;

public class ExploreCorpus extends JDialog implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2939148145075793288L;
	public ITS its = null;
	public ITSAdministrator ITSframe = null;

	// Instance attributes used in this example
	protected JInternalFrame MDIframe;
	protected JPanel openPanel;
	protected JPanel panelButtons;
	protected JPanel panelS;
	protected JPanel panelSearch;
	protected JList listbox;
	protected Vector<?> listData;
	protected JButton btnOk, btnCancel;
	protected JScrollPane scrollPane;
	protected Vector<?> listSelected;
	private DefaultListModel modelList;
	private TextFilterList txtSearch;
	public static boolean cancelAction;

	private Object[] dat;

	/**
	 * Constructor empty
	 * 
	 * @throws Exception
	 */
	public ExploreCorpus() throws Exception {
	}

	/**
	 * Constructor
	 * 
	 * @param frame
	 *            : the parent who called me
	 * @throws Exception
	 */
	public ExploreCorpus(ITSAdministrator frame) throws Exception {
		cancelAction = true;
		setTitle("Select Taxonomy");
		setModal(true);
		setLocation(60, 60);
		setSize(600, 300);
		setBackground(Color.lightGray);

		its = frame.its;
		ITSframe = frame;

		setBackground(Color.gray);

		// Create the data model for this example

		modelList = new DefaultListModel();
		listData = frame.its.getCorpora();
		listbox = new JList(modelList);
		for (Object tax : listData) {
			modelList.addElement(tax);
		}

		listbox.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		btnOk = new JButton("Ok");
		btnOk.addActionListener(this);

		this.txtSearch = new TextFilterList(this.modelList);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(this);

		scrollPane = new JScrollPane(listbox);

		scrollPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		panelButtons = new JPanel();
		panelButtons.setLayout(new GridLayout(1, 3));
		panelButtons.add(btnOk);
		panelButtons.add(btnCancel);

		this.panelSearch = new JPanel();
		this.panelSearch.setLayout(new BorderLayout());
		this.panelSearch.add(new JLabel("Search:"), BorderLayout.WEST);
		this.panelSearch.add(this.txtSearch, BorderLayout.CENTER);

		this.panelS = new JPanel();
		this.panelS.setLayout(new BorderLayout());
		this.panelS.add(this.panelSearch, BorderLayout.NORTH);
		this.panelS.add(this.panelButtons, BorderLayout.SOUTH);

		this.panelS
				.setBorder(BorderFactory.createEmptyBorder(10, 100, 10, 100));

		this.panelButtons.setBorder(BorderFactory.createEmptyBorder(10, 100,
				10, 100));
		openAtUpperCenter();

		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				cancelAction = false;
			}
		});

		getRootPane().setDefaultButton(btnOk);

		setLayout(new BorderLayout());
		getContentPane().add(panelS, BorderLayout.SOUTH);
		getContentPane().add(scrollPane, BorderLayout.NORTH);

		setVisible(true);
	}

	// events
	public void actionPerformed(ActionEvent event) {
		if (event.getSource().equals(this.btnOk)) {
			Object[] tax = this.listbox.getSelectedValues();
			if (tax.length == 0) {
				JOptionPane.showMessageDialog(this, "Select one taxonomy");
				return;
			}
			setArray();
			dispose();
		} else if (event.getSource().equals(this.btnCancel)) {
			cancelAction = false;
			dispose();
		}
	}

	private void setArray() {
		dat = listbox.getSelectedValues();
	}

	public Object[] getArray() {
		return dat;
	}

	public void openAtUpperCenter() {
		Dimension winsize = this.getSize(), screensize = Toolkit
				.getDefaultToolkit().getScreenSize();
		this.setLocation((screensize.width - winsize.width) / 2,
				(screensize.height - winsize.height) / 3);
	}
}
