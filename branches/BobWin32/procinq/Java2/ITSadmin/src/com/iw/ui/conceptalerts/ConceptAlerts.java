package com.iw.ui.conceptalerts;

// package declaration here

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.tools.JSortTable;
import com.iw.tools.DefaultSortTableModel;
import com.iw.tools.SwingWorker;
import com.iw.tools.NarrMenuItem;
import com.iw.ui.ITSAdministrator;
import com.iw.ui.DocumentChart;

public class ConceptAlerts extends JPanel implements ActionListener {
    protected JTable conceptTable;
    protected JButton okButton;
    protected JButton cancelButton;

    // toolbar items
    protected JButton addButton;
    protected JButton editButton;
    protected JButton deleteButton;
    protected JButton reportButton;

    private JInternalFrame frame;
    private ITSAdministrator ITSframe;
    private User u;
    private NarrMenuItem nmi;
    private ConceptAlerts ca;
    private Component glass;

    private final User user = ITS.getUser();

    public ConceptAlerts(ITSAdministrator ITSframe, NarrMenuItem nmi) {
        u = ITS.getUser(); ca = this;
        this.ITSframe = ITSframe;
        JDesktopPane jdesktoppane = ITSframe.jdp; this.nmi = nmi;

        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        // the pop up thesaurus manager is an internal MDI frame
        frame = new JInternalFrame("Narrative Reports: "+nmi.getNarrativeName(), true, true, false, true);
        frame.setLocation(60, 60);
        frame.setSize(675, 225);
        frame.setBackground(Color.lightGray);
        frame.setFrameIcon(ITSframe.iIndraweb);

        glass = frame.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        conceptTable = new JTable();

        try {
            getAlerts();
        } // fill concept table with latest concept alerts
        catch (Exception e) {
            e.printStackTrace(System.out);
        }

        okButton = new JButton("OK");
        okButton.setToolTipText("Click OK to accept preference changes.");
        okButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);

        JToolBar tools = setToolBar();

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(new JScrollPane(conceptTable));
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(2);
            layout.add(okButton);
            layout.addSpace(10);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.parent();
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(frame);

        frame.getContentPane().add(tools, BorderLayout.NORTH);
        frame.getContentPane().add(ob, BorderLayout.CENTER);
        frame.getRootPane().setDefaultButton(okButton);
        //frame.setContentPane(ob);
        frame.show();
    }

    public NarrMenuItem getNarrative() { return nmi; }

    private JToolBar setToolBar() {
        ImageIcon icoAdd = com.iw.tools.ImageUtils.getImage(this, "itsimages/addAction.gif");
        ImageIcon icoEdit = com.iw.tools.ImageUtils.getImage(this, "itsimages/editAction.gif");
        ImageIcon icoDelete = com.iw.tools.ImageUtils.getImage(this, "itsimages/deleteAction.gif");
        ImageIcon icoReport = com.iw.tools.ImageUtils.getImage(this, "itsimages/explain.gif");

        Action addAction = new toolbarAction(icoAdd, "Add", new Integer(KeyEvent.VK_INSERT));
        Action editAction = new toolbarAction(icoEdit, "Edit", new Integer(KeyEvent.VK_E));
        Action deleteAction = new toolbarAction(icoDelete, "Delete", new Integer(KeyEvent.VK_D));
        Action reportAction = new toolbarAction(icoReport, "Report", new Integer(KeyEvent.VK_R));

        addButton = new JButton(addAction);
        editButton = new JButton(editAction);
        deleteButton = new JButton(deleteAction);
        reportButton = new JButton(reportAction);

        JToolBar jtb = new JToolBar();
        jtb.setFloatable(false);

        addButton.setMaximumSize(new Dimension(30, 30));
        addButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(addButton);
        editButton.setMaximumSize(new Dimension(30, 30));
        editButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(editButton);
        deleteButton.setMaximumSize(new Dimension(30, 30));
        deleteButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(deleteButton);
        reportButton.setMaximumSize(new Dimension(30, 30));
        reportButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(reportButton);

        jtb.setOpaque(true);

        return jtb;
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void getAlerts() throws Exception {
        Vector vDocs = new Vector();
        Vector vQDocs = new Vector();

        try {
            vDocs = ITSframe.its.getConceptAlerts();
        } catch (IllegalQuery e) {
            JOptionPane.showMessageDialog(ITSframe, "Sorry, your query syntax is invalid.  Please recheck your query " +
                    "syntax and try again.",
                    "Error", JOptionPane.NO_OPTION);
            DefaultSortTableModel dtm = new DefaultSortTableModel();
            conceptTable.setModel(dtm);
            return;
        }

        Enumeration eV = vDocs.elements();
        while (eV.hasMoreElements()) {
            Document d = (Document) eV.nextElement();
            if (d.get("NARRATIVEID").equals(nmi.getNarrativeID())) { vQDocs.addElement(d); }
        }

        if (vQDocs.size() == 0) {
            JOptionPane.showMessageDialog(ITSframe, "There are no "+nmi.getNarrativeName()+" narratives in this database.",
                    "Information", JOptionPane.NO_OPTION);

            DefaultTableModel dtm = new DefaultTableModel();
            conceptTable.setModel(dtm);
            return;
        }

        DefaultTableModel dtm = new DefaultTableModel(vQDocs.size(), 2);

        for (int i = 0; i < vQDocs.size(); i++) {
            Document d = (Document) vQDocs.elementAt(i);
            String troubleStatus = d.get("TROUBLESTATUS");

            if (troubleStatus.equals("")) troubleStatus = "0";

            dtm.setValueAt(d, i, 0);
            dtm.setValueAt(new Integer(troubleStatus), i, 1);
        }

        String[] tableColumnNames = new String[2];
        tableColumnNames[0] = "Narrative";
        tableColumnNames[1] = " ";

        dtm.setColumnIdentifiers(tableColumnNames);

        conceptTable.setModel(dtm);

        // set column preferred size ratios and renderers
        TableColumn col = conceptTable.getColumnModel().getColumn(0);
        col.setPreferredWidth(500);
        TableColumn col2 = conceptTable.getColumnModel().getColumn(1);
        col2.setPreferredWidth(60);
        col2.setCellRenderer(new ConceptCellRenderer());
    }

    public void actionPerformed(ActionEvent e) {
        frame.dispose();
    }

    public class toolbarAction extends AbstractAction {
        public toolbarAction(ImageIcon icon,
                             String desc, Integer mnemonic) {
            super(null, icon);
            putValue(SHORT_DESCRIPTION, desc);
            putValue(MNEMONIC_KEY, mnemonic);
        }

        public void actionPerformed(ActionEvent e) {
            SwingWorker aWorker = new SwingWorker(e) {
                NarrativePanel np = null;

                protected void doNonUILogic() {
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    glass.setVisible(true);

                    if (e.getSource().equals(addButton)) {
                        np = new NarrativePanel(ca, ITSframe);
                        np.show();
                    }
                    if (e.getSource().equals(editButton)) {
                        int row = conceptTable.getSelectedRow();
                        Document d = (Document) conceptTable.getModel().getValueAt(row, 0);

                        np = new NarrativePanel(ca, ITSframe, d);
                        np.show();
                    }
                    if (e.getSource().equals(deleteButton)) {
                        int row = conceptTable.getSelectedRow();
                        Document d = (Document) conceptTable.getModel().getValueAt(row, 0);

                        try { ITSframe.its.deleteDocument(d); }
                        catch (Exception e) { e.printStackTrace(System.out); return; }

                        DefaultTableModel dtm = (DefaultTableModel) conceptTable.getModel();
                        dtm.removeRow(row);

                        conceptTable.revalidate();
                    }
                    if (e.getSource().equals(reportButton)) {
                        int row = conceptTable.getSelectedRow();
                        Document d = (Document) conceptTable.getModel().getValueAt(row, 0);

                        Vector v = new Vector();
                        try { v = ITSframe.its.getSimilarDocuments(d.get("DOCUMENTID")); }
                        catch (Exception e) { e.printStackTrace(System.out); return; }

                        if (v.size() == 0) {
                            JOptionPane.showMessageDialog(ITSframe, d.get("DOCTITLE")+" does not contain any similar documents.",
                                "Information", JOptionPane.NO_OPTION);
                            return;
                        }

                        try {
                            NarrativeReportPanel ds = new NarrativeReportPanel (ITSframe, v);
                            ds.narrativeID = (String) d.get("DOCUMENTID");
                            ds.setVisible(true);
                        } catch (Exception e) { e.printStackTrace(System.out); }
                    }
                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    glass.setVisible(false);
                }
            };
        aWorker.start();
        }
    }

    // color coded renderer for right most column
    public class ConceptCellRenderer extends DefaultTableCellRenderer {
        public ConceptCellRenderer() {
            super();
        };
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row, int column) {

            int i = ((Integer) value).intValue();

            setBackground(Color.WHITE);
            if (i > user.getMinorThreshold()) setBackground(Color.GREEN);
            if (i > user.getSevereThreshold()) setBackground(Color.YELLOW);
            if (i > user.getCriticalThreshold()) setBackground(Color.RED);

            //super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            return this;
        }
    }
}

