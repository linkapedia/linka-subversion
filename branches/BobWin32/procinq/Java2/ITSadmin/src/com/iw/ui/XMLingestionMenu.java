package com.iw.ui;

import com.oculustech.layout.*;
import com.iw.tools.PDFimportHandlerPass1;
import com.iw.tools.SwingWorker;
import com.iw.system.PDFelement;
import com.iw.system.Corpus;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Hashtable;
import java.util.Iterator;
import java.io.*;

import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

public class XMLingestionMenu extends OculusBox implements ActionListener {
    protected JButton okButton;
    protected JButton saveButton;
    protected JButton cancelButton;
    protected JLabel labelTitle;

    protected JComboBox[] elementList;
    protected JComboBox[] actionList;

    private JInternalFrame frame;
    private ITSAdministrator ITSframe;
    private PDFimportHandlerPass1 handler;
    private PopupProgressBar ppb;
    private File file;
    private Hashtable ht;
    private Corpus c;

    private int size = 0;

    private final boolean debug = false;
    private final String[] elementValues =
            { "Ignore", "Omit", "Level 1", "Level 2", "Level 3", "Level 4", "Level 5", "Level 6", "Level 7" };

    public XMLingestionMenu(PDFimportHandlerPass1 handler, ITSAdministrator ITSframe, File f) {
        this.handler = handler; this.file = f; this.ITSframe = ITSframe;

        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        frame = new JInternalFrame("Tune Ingestion Parameters", true, true, true, true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setFrameIcon(ITSframe.iIndraweb);

        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        labelTitle = new JLabel("There are "+handler.htElements.size()+" unique labels in this XML document.");
        labelTitle.setToolTipText("There are "+handler.htElements.size()+" unique labels in this XML document.");

        okButton = new JButton("OK");
        okButton.setToolTipText("Click 'OK' to proceed to taxonomy ingestion with these parameters.");
        okButton.addActionListener(this);

        saveButton = new JButton("Save");
        saveButton.setToolTipText("Click 'SAVE' to save progress for this file.");
        saveButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setToolTipText("Cancel this ingestion.");
        cancelButton.addActionListener(this);

        if (debug) {
            System.out.println("Unique objects: "+handler.htElements.size());
            System.out.println(" ");
        }

        size = handler.htElements.size();

        elementList = new JComboBox[size];
        actionList = new JComboBox[size];

        size = size * 5;

        for (int i = 0; i < handler.vOrder.size(); i++) {
            String sID = (String) handler.vOrder.elementAt(i);
            PDFelement pdf = (PDFelement) handler.htElements.get(sID);
            if (debug) System.out.println(pdf);

            elementList[i] = new JComboBox(pdf.example);
            actionList[i] = new JComboBox(elementValues);
        }

        try { readProgress(); }
        catch (Exception e) {
            e.printStackTrace(System.err); System.err.println("*** COULD NOT READ PROGRESS FILE ***");
        }

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(labelTitle);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.nestGrid(2,handler.vOrder.size());
                    {
                        for (int i = 0; i < handler.vOrder.size(); i++) {
                            layout.add(elementList[i]);
                            layout.add(actionList[i]);
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.add(okButton);
                layout.addSpace(10);
                layout.add(saveButton);
                layout.addSpace(10);
                layout.add(cancelButton);
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);

        frame.setLocation(60, 60);
        frame.setPreferredSize(new Dimension(750, 650));
        ITSframe.jdp.add(frame);

        frame.getRootPane().setDefaultButton(okButton);
        frame.setContentPane(new JScrollPane(this));
        frame.show();
        frame.pack();
    }

    public void readProgress() throws Exception {
        File f = new File(file.getName()+".save");
        if (!f.exists()) return;

        FileInputStream fis = new FileInputStream(f);

        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);
        Element ems = doc.getRootElement();

        Iterator i = ems.elements("element").iterator();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            actionList[Integer.parseInt(e.attribute("index").getText())].setSelectedIndex(Integer.parseInt(e.getText()));
        }
    }

    public File saveProgress() throws Exception {
        File f = new File(file.getName()+".save");

        if (f.exists()) {
            int i = JOptionPane.showConfirmDialog(ITSframe, "A save file for this taxonomy already exists.  Overwrite? ",
                    "Confirm", JOptionPane.YES_NO_OPTION);
            if (i != 0) return null;

            f.delete();
        }

        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<elements>\n");

        for (int i = 0; i < handler.vOrder.size(); i++) {
            String sID = (String) handler.vOrder.elementAt(i);
            out.write("   <element id=\""+sID+"\" index=\""+i+"\">"+actionList[i].getSelectedIndex()+"</element>\n");
        }
        out.write("</elements>\n");
        out.close(); fos.close();

        return f;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(saveButton)) {
            try {
                File f = saveProgress();
                if (f != null) JOptionPane.showMessageDialog(ITSframe, "Your progress has been saved successfully.", "Information", JOptionPane.NO_OPTION);
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
                JOptionPane.showMessageDialog(ITSframe, "Error: Could not write to file (Permission denied).", "Error", JOptionPane.NO_OPTION);
            }
            return;
        }
        if (e.getSource().equals(okButton)) {
            ht = new Hashtable();

            // loop through each element, set the status in the vector and ingest
            for (int i = 0; i < handler.vOrder.size(); i++) {
                String sID = (String) handler.vOrder.elementAt(i);
                PDFelement pdf = (PDFelement) handler.htElements.get(sID);

                System.out.println("PUT: "+sID+" "+actionList[i].getSelectedIndex());

                ht.put(sID, new Integer(actionList[i].getSelectedIndex()));
            }

            System.out.println(" ");
            System.out.println("Action performed, hash size: "+ht.size());
            System.out.println(" ");

            ppb = new PopupProgressBar(ITSframe, 0, 100);
            ppb.pack(); ppb.show();

            SwingWorker aWorker = new SwingWorker(e) {
                protected void doNonUILogic() {
                    try { c = com.iw.tools.IngestionUtils.createCorpusFromXML(file, ITSframe.its, ht, size, ppb); ppb.setProgress(100); }
                    catch (Exception ex) { ex.printStackTrace(System.out); }
                    finally { ppb.dispose(); }
                }

                protected void doUIUpdateLogic() {}

                 public void finished() {
                     super.finished();
                     ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                     ITSframe.getGlassPane().setVisible(false);

                     frame.dispose();

                     try {
                         CorpusManagement DragAndDrop = new CorpusManagement(ITSframe, c);

                         DragAndDrop.setBounds(50, 50, 850, 550);
                         DragAndDrop.setVisible(true);
                         DragAndDrop.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                         ITSframe.jdp.add(DragAndDrop);
                         DragAndDrop.moveToFront();

                         ITSframe.jdp.setVisible(true);
                     } catch (Exception ex) {
                         System.err.println("Could not open Corpus Management console for corpus "+c.getName());
                         ex.printStackTrace(System.err);
                     }

                 }
             };
             aWorker.start();


        } else { frame.dispose(); }
    }
}


