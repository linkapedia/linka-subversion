/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Aug 27, 2003
 * Time: 6:06:07 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.BarRenderer;
import org.jfree.data.CategoryDataset;
import org.jfree.data.DatasetUtilities;
import org.jfree.data.DefaultKeyedValues;
import org.jfree.util.Log;
import org.jfree.util.SortOrder;
import org.xml.sax.InputSource;

import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.iw.license.IndraLicense;
import com.iw.system.Corpus;
import com.iw.system.EmptyCorpusToUseList;
import com.iw.system.FileMetadata;
import com.iw.system.ITS;
import com.iw.system.ITSTreeNode;
import com.iw.system.Language;
import com.iw.system.Node;
import com.iw.system.NodeDocument;
import com.iw.system.Signature;
import com.iw.system.Thesaurus;
import com.iw.system.ThesaurusWord;
import com.iw.system.TopicExistsError;
import com.iw.system.User;
import com.iw.tools.BingInfoBean;
import com.iw.tools.DefaultSortTableModel;
import com.iw.tools.GoogleInfoBean;
import com.iw.tools.GoogleNews;
import com.iw.tools.ITSTreeCellRenderer;
import com.iw.tools.ITSTreeTextField;
import com.iw.tools.JSortTable;
import com.iw.tools.ManagerSignatureUtil;
import com.iw.tools.NodeTransferringTree;
import com.iw.tools.NodeTypeBean;
import com.iw.tools.RowColorJTable;
import com.iw.tools.SignatureRenderer;
import com.iw.tools.SwingWorker;
import com.iw.tools.tlexport;
import com.iw.tools.image.ui.Manager;
import com.iw.ui.explain.ExplainDisplayPane;
import com.oculustech.layout.OculusBox;
import com.oculustech.layout.OculusLayout;
import com.oculustech.layout.OculusLayoutHelper;
import com.oculustech.layout.Space;
import javax.swing.JMenu;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

public class CorpusManagement extends JInternalFrame implements KeyListener,
        TreeSelectionListener, ActionListener, MouseListener, FocusListener,
        ChangeListener, TableModelListener {

    /**
     *
     */
    private static final long serialVersionUID = -4210878000216919431L;
    private JTextPane textPane = new JTextPane();
    private NodeTransferringTree tree;
    private ITS its;
    private ITSAdministrator admin;
    private boolean bSaveChanges = false;
    private boolean bLoadProps = true;
    private boolean bEarlyReturn = false;
    private String CorpusID;
    private Component glass;
    private static final Logger log = Logger.getLogger(CorpusManagement.class);
    // this vector is used to store old signatures for roll back, if necessary
    private Vector vUndoSignatures = new Vector();
    private JPopupMenu popupMenu = null;
    private JPopupMenu popupDocMenu = null;
    private MouseListener mouseListener = null;
    private MouseListener mouseDocListener = null;
    private JDialog confirmDialog = null;
    public static final int DEBUG = 1;
    protected int corpusMTlevel = ITS.getUser().getCorpusMTLevel();
    // default widget of the jtree
    ITSTreeTextField treeField;
    // Node property controls
    protected JButton buttonImage;
    protected JTextField textTitle;
    protected JLabel labelDesc;
    protected JTextArea textDesc;
    protected JLabel labelSize;
    protected JTextField textSize;
    protected JLabel labelStatus;
    protected JLabel labelLocked;
    protected JCheckBox checkBoxStatus;
    protected JCheckBox checkBoxLocked;
    protected JCheckBox checkBoxAcr;
    protected JCheckBox isBridgeNode;
    protected JComboBox listNodeType;
    protected JButton buttonAdd;
    protected JButton buttonRemove;
    protected JButton buttonCreate;
    protected JButton buttonRemoveSigAll;
    protected JButton btnNodeProp;
    protected JSortTable tableSignatures;
    protected JTable tableThes;
    protected JSortTable tableHaveGate;
    protected JSortTable tableHave;
    protected RowColorJTable tableDocuments;
    protected JButton buttonSave;
    protected JButton buttonCancel;
    protected JTabbedPane tabPane;
    protected JButton buttonAddThes;
    protected JButton buttonRemoveThes;
    protected JButton buttonEditThes;
    protected JComboBox optNodeMustHaveGate;
    //----------------------------------
    protected JButton buttonAddHaveNodeGate;
    protected JButton buttonEditHaveNodeGate;
    protected JButton buttonRemoveHaveNodeGate;
    protected JButton buttonRemoveAllHaveNodeGate;
    //----------------------------------
    protected JButton buttonAddHave;
    protected JButton buttonEditHave;
    protected JButton buttonRemoveHave;
    protected JButton buttonRemoveAllHave;
    protected JComboBox languageBox;
    // blacklist tab controls
    protected JSortTable tableDocuments2;
    protected JButton openButton2;
    protected JButton relatedButton2;
    protected JButton documentButton2;
    protected JButton chartButton2;
    // signature tab controls
    protected JButton buttonApply;
    protected JButton buttonUndo;
    protected JButton buttonTest;
    protected JButton buttonTest2;
    protected JButton buttonCheckSignatures;
    protected JButton buttonAdd2;
    protected JButton buttonRemove2;
    protected JSortTable tableSignatures2;
    protected JFreeChart chart;
    protected ChartPanel chartPanel;
    // toolbar controls
    protected JButton addButton;
    protected JButton cutButton;
    protected JButton openallButton;
    protected JButton copyButton;
    protected JButton pasteButton;
    protected JButton linkButton;
    protected JButton saveButton;
    protected JButton deleteButton;
    protected JButton splitButton;
    protected JButton joinButton;
    protected JButton orthogButton;
    protected JButton moveupButton;
    protected JButton movedownButton;
    protected JButton indentButton;
    protected JButton outdentButton;
    protected JButton undoButton;
    protected JButton alphaButton;
    protected JButton refreshButton;
    protected JButton propsButton;
    // document pane buttons
    protected JButton openButton;
    protected JButton relatedButton;
    protected JButton documentButton;
    protected JButton explainButton;
    protected JButton abstractButton;
    protected JButton addNDButton;
    protected JButton removeNDButton;
    protected JButton propertiesButton;
    protected JButton chartButton;
    protected JLabel classLabel;
    // corpus source pane
    protected JTextArea nodeSourceArea;
    protected JButton nodeSourceSaveButton;
    protected JButton CreateSigFromSelection;
    protected JButton CreateMustHaveFromSelection;
    protected JButton CreateSigFromSelectionMulti;
    protected JButton CreateMustHaveFromSelectionMulti;
    protected JButton fixSourceWiki;
    protected JButton fixdescBritannica;
    // imagesManager pane
    protected Manager panelImages;
    protected JButton uploadImageFileSystem;
    protected JButton uploadImageUrl;
    protected JButton deleteImage;
    protected JButton refreshImage;
    protected JButton btnImagesS3;
    protected Corpus corp = null;
    // right mouse popup controls
    protected JMenuItem menuGoogle;
    protected JMenuItem menuFillMusthaves;
    protected JMenuItem menuFillSignatures;
    protected JMenuItem menuImportImages;
    protected JMenuItem menuDeleteMustHaves;
    protected JMenuItem menuDeleteImages;
    protected JMenuItem menuDeleteSignatures;
    protected JMenuItem menuPopulateDescription;
    protected JMenuItem menuShow;
    protected JMenuItem menuCut;
    protected JMenuItem menuOal;
    protected JMenuItem menuCop;
    protected JMenuItem menuPas;
    protected JMenuItem menuPal;
    protected JMenuItem menuAlp;
    protected JMenuItem menuAlR;
    protected JMenuItem menulockR;
    protected JMenuItem menuunlockR;
    protected JMenuItem menuRef;
    protected JMenuItem menuCap;
    protected JMenuItem menuCaR;
    protected JMenuItem menuFill;
    protected JMenuItem menuStrip;
    protected JMenuItem menuStriR;
    protected JMenuItem menuStripWS;
    protected JMenuItem menuSR;
    protected JMenuItem menuRoll;
    protected JMenuItem menuHave;
    protected JMenuItem menuHaveFromSource;
    protected JMenuItem menuHaveBranch;
    protected JMenuItem addNodeMustHaveGate;
    //------------------------------------
    protected JSeparator m1;
    protected JSeparator m2;
    protected JSeparator m3;
    protected JSeparator m4;
    protected JSeparator m5;
    protected JSeparator m6;
    protected JSeparator m7;
    //------------------------------------
    protected JMenu mustHavesEdit;
    protected JMenu imagesEdit;
    protected JMenu signaturesEdit;
    protected JMenu nodeMustHaveGate;
    protected JMenuItem menuOTL;
    // doc window right mouse popup controls
    protected JMenuItem menuOpen;
    protected JMenuItem menuRelated;
    protected JMenuItem menuClassify;
    protected JMenuItem menuExplain;
    protected JMenuItem menuRemove;
    protected JMenuItem menuProperties;
    // new requests from Maria
    protected JMenuItem menuParaMust;
    protected JMenuItem menuComaMust;
    protected JMenuItem menuWikiSource;
    protected JMenuItem menuMustHaveComma;
    protected JMenuItem menuCopySigToMustHave;
    protected Action addAction, deleteAction, moveupAction, movedownAction,
            indentAction, outdentAction, splitAction, cutAction, pasteAction,
            linkAction, copyAction, saveAction, undoAction, alphaAction,
            refreshAction, openallAction, joinAction, orthogAction,
            propsAction;
    // logic
    private List<NodeTypeBean> objects;
    private boolean addMenuItems = false;

    public CorpusManagement(ITSAdministrator admin, Corpus c) throws Exception {
        super("Taxonomy Editor: " + c.getName(), true, true, true, true);
        its = admin.its;
        this.admin = admin;
        setFrameIcon(admin.iIndraweb);
        CorpusID = c.getID();
        corp = c;
        createPanels(c);
    }

    public CorpusManagement(ITSAdministrator admin, Corpus c, ITSTreeNode n)
            throws Exception {
        super("Taxonomy Editor: " + c.getName(), true, true, true, true);
        its = admin.its;
        this.admin = admin;
        setFrameIcon(admin.iIndraweb);
        CorpusID = c.getID();
        corp = c;
        createPanels(c);

        // given a node in this constructor, load path to this node and expand
        // along the way
        Vector vNodes = its.getAncestorTree(n);
        DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tree.getModel()
                .getRoot();
        ITSTreeNode TreeNode = (ITSTreeNode) dmtn.getUserObject();

        for (int i = (vNodes.size() - 1); i >= 0; i--) {
            n = (ITSTreeNode) vNodes.elementAt(i);
            if (dmtn != null) {
                tree.refreshNode(dmtn);
                tree.setSelectionPath(new TreePath(dmtn.getPath()));

                if (i != 0) {
                    dmtn = (DefaultMutableTreeNode) tree.getChildNode(dmtn,
                            (ITSTreeNode) vNodes.elementAt(i - 1));
                }
            }
        }

        if (dmtn != null) {
            TreePath tp = new TreePath(dmtn.getPath());
            tree.setSelectionPath(tp);
            tree.scrollPathToVisible(tp);
        } else {
            JOptionPane.showMessageDialog(this,
                    "Warning: The topic that you selected is an orphan in this taxonomy.  "
                    + "You will be taken to the root topic instead.",
                    "Information", JOptionPane.NO_OPTION);
        }
    }

    public CorpusManagement(ITSAdministrator admin, Corpus c, String sNodeID)
            throws Exception {
        super("Taxonomy Editor: " + c.getName(), true, true, true, true);
        its = admin.its;
        this.admin = admin;
        setFrameIcon(admin.iIndraweb);

        CorpusID = c.getID();
        corp = c;
        createPanels(c);

        // given a node in this constructor, load path to this node and expand
        // along the way
        Vector vNodes = its.getAncestorTree(sNodeID);
        DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tree.getModel()
                .getRoot();
        ITSTreeNode TreeNode = (ITSTreeNode) dmtn.getUserObject();

        for (int i = (vNodes.size() - 1); i >= 0; i--) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            tree.refreshNode(dmtn);
            tree.setSelectionPath(new TreePath(dmtn.getPath()));

            if (i != 0) {
                dmtn = (DefaultMutableTreeNode) tree.getChildNode(dmtn,
                        (ITSTreeNode) vNodes.elementAt(i - 1));
            }
        }

        TreePath tp = new TreePath(dmtn.getPath());
        tree.setSelectionPath(tp);
        tree.scrollPathToVisible(tp);
    }

    public void doDefaultCloseAction() {
        if (bSaveChanges) { // yes = 0, no = 1, cancel = 2 ..
            // added 6-6-6, only prompt for changes if user has changes prompt
            // set
            if (ITS.getUser().PromptForTopicChange()) {
                JOptionPane pane = new JOptionPane(
                        "The topic properties have changed.  Save changes?",
                        JOptionPane.QUESTION_MESSAGE,
                        JOptionPane.YES_NO_CANCEL_OPTION);
                JPanel jp = (JPanel) pane.getComponent(1);
                Component[] c = jp.getComponents();

                JButton buttonYes = (JButton) c[0];
                JButton buttonNo = (JButton) c[1];
                JButton buttonCancel = (JButton) c[2];

                KeyAdapter key = new KeyAdapter() {
                    public void keyPressed(KeyEvent ex) {
                        if (ex.getKeyCode() == KeyEvent.VK_Y) {
                            DefaultMutableTreeNode olditem = (DefaultMutableTreeNode) tree
                                    .getSelectionPath().getLastPathComponent();
                            saveNodeProps(olditem);

                            confirmDialog.dispose();
                        }
                        if (ex.getKeyCode() == KeyEvent.VK_N) {
                            confirmDialog.dispose();
                        }
                        if (ex.getKeyCode() == KeyEvent.VK_C) {
                            confirmDialog.dispose();
                            bEarlyReturn = true;
                        }
                    }
                };
                buttonYes.addKeyListener(key);
                buttonNo.addKeyListener(key);
                buttonCancel.addKeyListener(key);

                confirmDialog = pane.createDialog(null, "Question");
                confirmDialog.show();
                Object selectedValue = null;
                selectedValue = pane.getValue();

                if (bEarlyReturn) {
                    return;
                }

                try {
                    int i = ((Integer) selectedValue).intValue();
                    if (i == 0) { // save it
                        DefaultMutableTreeNode olditem = (DefaultMutableTreeNode) tree
                                .getSelectionPath().getLastPathComponent();
                        saveNodeProps(olditem);
                    } else if (i == 2) { // changed my mind (cancel)
                        return;
                    }
                } catch (Exception ex) {
                } // keyboard short cut was used, no action required
            }
        }

        super.doDefaultCloseAction();
    }

    private void createPanels(Corpus c) throws Exception {
        LoadFileNodeType();

        glass = getGlassPane();

        JPanel treePanel = createTreePanel(its, c.getID());
        JPanel textPanel = createTextPanel(its, c.getID());

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                treePanel, textPanel);

        splitPane.setDividerLocation(250);
        splitPane.setOneTouchExpandable(true);

        JToolBar tools = setToolBar();
        getContentPane().add(tools, BorderLayout.NORTH);
        getContentPane().add(splitPane, BorderLayout.CENTER);
        this.addInternalFrameListener(new InternalFrameListener() {
            public void internalFrameOpened(InternalFrameEvent e) {
            }

            public void internalFrameClosing(InternalFrameEvent e) {
            }

            public void internalFrameClosed(InternalFrameEvent e) {
                showHiddenItemsMenuEdit(false);
            }

            public void internalFrameIconified(InternalFrameEvent e) {
            }

            public void internalFrameDeiconified(InternalFrameEvent e) {
            }

            public void internalFrameActivated(InternalFrameEvent e) {
            }

            public void internalFrameDeactivated(InternalFrameEvent e) {
            }
        });
        addItemsMenuEdit();

        TreePath path = tree.getSelectionPath();
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
        loadNodeProps((ITSTreeNode) selectedNode.getUserObject());
    }

    private JPanel createTreePanel(ITS itsadmin, String sCorpusID)
            throws Exception {
        its = itsadmin;
        JPanel treePanel = new JPanel();

        ITSTreeNode n = its.getCorpusRoot(sCorpusID);
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(n, true);
        DefaultTreeModel dtm = new DefaultTreeModel(root);

        tree = new NodeTransferringTree(dtm);
        // tree.setLargeModel(true);
        // tree.setRowHeight(17);

        tree.its = itsadmin;
        tree.addTreeSelectionListener(this);
        tree.addFocusListener(this);

        tree.createChildren(root, n.get("NODEID"));
        tree.revalidate();

        treeField = new ITSTreeTextField();
        treeField.addActionListener(this);
        treeField.addFocusListener(this);

        ITSTreeCellRenderer dtcr = new ITSTreeCellRenderer();
        dtcr.setBounds(new Rectangle(0, 0, 4000, 4000));
        dtcr.setLeafIcon(UIManager.getIcon("Tree.closedIcon"));

        DefaultCellEditor dtce = new DefaultCellEditor(treeField);
        treeField.addActionListener(this);

        tree.setCellEditor(dtce);
        tree.setCellRenderer(dtcr);

        treePanel.setLayout(new BorderLayout());
        treePanel.add(new JScrollPane(tree), BorderLayout.CENTER);
        treePanel.setBorder(BorderFactory.createTitledBorder("Topics"));

        tree.setExpandsSelectedPaths(true);
        tree.addKeyListener(this);
        if (tree.getRowCount() > 0) {
            tree.setSelectionRow(0);
        }

        if (popupMenu == null) {
            popupMenu = new JPopupMenu();
            //######################################################
            menuGoogle = new JMenuItem("Search in Google");
            menuShow = new JMenuItem("Show");
            menuCut = new JMenuItem("Cut");
            menuCop = new JMenuItem("Copy");
            menuPas = new JMenuItem("Paste");
            menuPal = new JMenuItem("Paste as Link");
            menuOal = new JMenuItem("Open All");
            menuAlp = new JMenuItem("Alphabetize");
            menuAlR = new JMenuItem("Alphabetize Recursively");
            menulockR = new JMenuItem("Lock Node Recursively");
            menuunlockR = new JMenuItem("Unlock Node Recursively");
            menuCap = new JMenuItem("Change Case");
            menuCaR = new JMenuItem("Change Case Recursively");
            menuStrip = new JMenuItem("Strip Leading Numbers");
            menuStriR = new JMenuItem("Strip Numbers Recursively");
            menuStripWS = new JMenuItem("Strip Leading and Trailing Spaces");
            menuFill = new JMenuItem("Fill with Web Documents");
            menuRef = new JMenuItem("Queue Topic for Refresh");
            menuSR = new JMenuItem("Search and Replace");
            menuRoll = new JMenuItem("Roll Up Nodes");
            menuHave = new JMenuItem("Add Must Have Terms");
            menuHaveFromSource = new JMenuItem("Add Must Have from Source [New]");
            menuFillMusthaves = new JMenuItem("Add Must Have Recursively from Source");
            menuFillSignatures = new JMenuItem("Add Signatures Recursively from Source");
            menuImportImages = new JMenuItem("Add images Recursively from BING");
            menuDeleteMustHaves = new JMenuItem("Delete musthaves Recursively");
            menuDeleteImages = new JMenuItem("Delete images Recursively");
            menuDeleteSignatures = new JMenuItem("Delete signatures Recursively");

            menuPopulateDescription = new JMenuItem("Set Description from Source Recursively");
            menuOTL = new JMenuItem("Export OTL File");
            menuParaMust = new JMenuItem("Move Inside Parens to Must Have");
            menuComaMust = new JMenuItem("Move Commas to Must Have");
            menuWikiSource = new JMenuItem("Fill Source from Wiki");
            menuMustHaveComma = new JMenuItem("Add Must Have From Topics with Commas");
            menuCopySigToMustHave = new JMenuItem("Copy Sigs To Must Haves");
            menuHaveBranch = new JMenuItem("Add Branch Must Have [New]");
            addNodeMustHaveGate = new JMenuItem("add Node Have Gate");

            User u = ITS.getUser();
            IndraLicense license = u.getLicense();
            //##################################################################
            popupMenu.add(menuGoogle);
            popupMenu.add(menuShow);
            popupMenu.add(new JSeparator());
            popupMenu.add(menuCut);
            popupMenu.add(menuCop);
            popupMenu.add(menuPas);
            popupMenu.add(new JSeparator());
            popupMenu.add(menuPal);
            //popupMenu.add(new JSeparator());
            //popupMenu.add(menuOal);
            //if (!u.lockFunctions()) {
            //    popupMenu.add(menuAlp);
            //    popupMenu.add(menuAlR);
            //}
            //popupMenu.add(menuCap);
            //popupMenu.add(menuCaR);
            //popupMenu.add(new JSeparator());
            //popupMenu.add(menuStrip);
            //popupMenu.add(menuStriR);
            //popupMenu.add(menuStripWS);
            //popupMenu.add(new JSeparator());
            popupMenu.add(menulockR);
            popupMenu.add(menuunlockR);
            popupMenu.add(new JSeparator());
            popupMenu.add(menuSR);
            popupMenu.add(menuRoll);
            //popupMenu.add(menuHave);
            //popupMenu.add(menuDeleteMustHaves);
            //popupMenu.add(menuDeleteImages);
            //popupMenu.add(menuDeleteSignatures);
            //popupMenu.add(menuFillMusthaves);
            //popupMenu.add(menuFillSignatures);
            //popupMenu.add(menuImportImages);
            //popupMenu.add(menuPopulateDescription);
            popupMenu.add(menuOTL);
            //popupMenu.add(new JSeparator());
            //popupMenu.add(menuParaMust);
            //popupMenu.add(menuComaMust);
            //popupMenu.add(menuWikiSource);
            //popupMenu.add(menuMustHaveComma);
            //popupMenu.add(menuCopySigToMustHave);
            //popupMenu.add(new JSeparator());
            //popupMenu.add(menuFill);
            popupMenu.add(menuRef);
            //##################################################################
            menuShow.setEnabled(false);

            menuGoogle.addActionListener(this);
            menuFillMusthaves.addActionListener(this);
            menuFillSignatures.addActionListener(this);
            menuImportImages.addActionListener(this);
            menuDeleteMustHaves.addActionListener(this);
            menuDeleteImages.addActionListener(this);
            menuDeleteSignatures.addActionListener(this);
            menuPopulateDescription.addActionListener(this);
            menuShow.addActionListener(this);
            menuCut.addActionListener(this);
            menuCop.addActionListener(this);
            menuPas.addActionListener(this);
            menuPal.addActionListener(this);
            menuOal.addActionListener(this);
            menuAlp.addActionListener(this);
            menuAlR.addActionListener(this);
            menuCap.addActionListener(this);
            menuCaR.addActionListener(this);
            menulockR.addActionListener(this);
            menuunlockR.addActionListener(this);
            menuStrip.addActionListener(this);
            menuStriR.addActionListener(this);
            menuStripWS.addActionListener(this);
            menuFill.addActionListener(this);
            menuRef.addActionListener(this);
            menuSR.addActionListener(this);
            menuRoll.addActionListener(this);
            menuHave.addActionListener(this);
            menuOTL.addActionListener(this);
            menuParaMust.addActionListener(this);
            menuComaMust.addActionListener(this);
            menuWikiSource.addActionListener(this);
            menuMustHaveComma.addActionListener(this);
            menuCopySigToMustHave.addActionListener(this);
            addNodeMustHaveGate.addActionListener(this);
        }
        //
        popupMenu.setInvoker(tree);

        if (mouseListener == null) {
            mouseListener = new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    TreePath path = tree.getSelectionPath();
                    if (path != null) {
                        setShowMenuGreyOrNot(path);
                    }

                    if (e.isPopupTrigger()) {
                        int x = e.getX();
                        int y = e.getY();
                        int row = tree.getRowForLocation(x, y);
                        if (row != -1) {
                            tree.setSelectionRow(row);
                            popupMenu.show(tree, x, y);
                        }
                    }
                }

                public void mouseReleased(MouseEvent e) {
                    if (e.isPopupTrigger()) {
                        int x = e.getX();
                        int y = e.getY();
                        int row = tree.getRowForLocation(x, y);
                        if (row != -1) {
                            tree.setSelectionRow(row);
                            popupMenu.show(tree, x, y);
                        }
                    }
                }
            };
        }
        tree.addMouseListener(mouseListener);

        return treePanel;
    }

    private void setShowMenuGreyOrNot(TreePath path) {
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path
                .getLastPathComponent();
        ITSTreeNode node = (ITSTreeNode) selectedNode.getUserObject();

        if (node.isLink()) {
            menuShow.setEnabled(true);
        } else {
            menuShow.setEnabled(false);
        } // grey out "show"
    }

    private JPanel createTextPanel(ITS itsadmin, String sCorpusID) {
        JPanel textPanel = new JPanel();

        // load the root node of the given corpus, and populate with those
        // values
        ITSTreeNode n = new ITSTreeNode();
        Vector vSignatures = new Vector();
        try {
            n = itsadmin.getCorpusRoot(sCorpusID);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        try {
            Language lang = null;
            if (languageBox == null) {
                lang = new Language("English", "EN");
            } else {
                lang = (Language) languageBox.getSelectedItem();
            }

            vSignatures = itsadmin.getNodeSignatures(n, lang.getCode());
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        textPanel.setLayout(new BorderLayout());
        textPanel.add(new JScrollPane(textPane), BorderLayout.CENTER);
        textPanel.setMinimumSize(new Dimension(375, 0));
        textPanel.setBorder(BorderFactory
                .createTitledBorder("Topic Properties"));

        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        buttonImage = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/folder.gif"));
        buttonImage.setHorizontalAlignment(SwingConstants.RIGHT);
        buttonImage.setHorizontalTextPosition(SwingConstants.RIGHT);
        buttonImage.setBorder(null);
        buttonImage.setDisabledIcon(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/folder.gif"));
        buttonImage.setEnabled(false);

        textTitle = new JTextField(n.get("NODETITLE"));

        labelDesc = new JLabel("Definition:");
        labelDesc.setHorizontalAlignment(SwingConstants.RIGHT);
        labelDesc.setHorizontalTextPosition(SwingConstants.RIGHT);
        // labelDesc.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        if (n.get("NODEDESC").equals("")) {
            textDesc = new JTextArea(20, 5);
        } else {
            textDesc = new JTextArea(n.get("NODEDESC"), 20, 5);
        }
        textDesc.setLineWrap(true);
        textDesc.setWrapStyleWord(true);
        textDesc.setAutoscrolls(true);

        labelSize = new JLabel("Size:");
        labelSize.setHorizontalAlignment(SwingConstants.RIGHT);
        labelSize.setHorizontalTextPosition(SwingConstants.RIGHT);

        textSize = new JTextField(n.get("NODESIZE"));
        textSize.addFocusListener(this);

        checkBoxStatus = new JCheckBox("Active");
        checkBoxStatus.setMaximumSize(new Dimension(50, checkBoxStatus
                .getMaximumSize().height));
        checkBoxStatus.setSelected(true);

        btnNodeProp = new JButton("Node properties");
        btnNodeProp.addActionListener(this);

        checkBoxAcr = new JCheckBox("Acronym");
        checkBoxAcr.setMaximumSize(new Dimension(50, checkBoxStatus
                .getMaximumSize().height));
        checkBoxAcr.setSelected(true);

        checkBoxLocked = new JCheckBox("Locked");
        checkBoxLocked.setMaximumSize(new Dimension(50, checkBoxStatus
                .getMaximumSize().height));
        checkBoxLocked.setSelected(true);
        checkBoxLocked.addActionListener(this);

        // if any fields are changed, flag the "save changes?" dialog
        textTitle.addKeyListener(this);
        textDesc.addKeyListener(this);
        textSize.addKeyListener(this);

        buttonAdd = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/plus.gif"));
        buttonRemove = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/minus.gif"));
        buttonCreate = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/hammer.gif"));
        buttonRemoveSigAll = new JButton("All",
                com.iw.tools.ImageUtils.getImage(this, "itsimages/minus.gif"));
        buttonRemoveSigAll.setToolTipText("Remove all signatures to this node");
        buttonCreate
                .setToolTipText("Use a piece of unstructured data to build signatures for this topic.");
        buttonAdd.addActionListener(this);
        buttonRemove.addActionListener(this);
        buttonCreate.addActionListener(this);
        buttonRemoveSigAll.addActionListener(this);

        buttonAddThes = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/plus.gif"));
        buttonRemoveThes = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/minus.gif"));
        buttonEditThes = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/hammer.gif"));
        buttonEditThes.setToolTipText("Edit a thesaurus entry.");
        buttonAddThes.addActionListener(this);
        buttonRemoveThes.addActionListener(this);
        buttonEditThes.addActionListener(this);

        buttonAddHave = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/plus.gif"));
        buttonRemoveHave = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/minus.gif"));
        buttonRemoveAllHave = new JButton("All",
                com.iw.tools.ImageUtils.getImage(this, "itsimages/minus.gif"));
        buttonEditHave = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/hammer.gif"));
        buttonEditHave.setToolTipText("Edit a must have entry.");
        buttonAddHave.addActionListener(this);
        buttonRemoveHave.addActionListener(this);
        buttonRemoveAllHave.addActionListener(this);
        buttonEditHave.addActionListener(this);

        //--------------NodeMustHaveGate-------
        buttonAddHaveNodeGate = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/plus.gif"));
        buttonRemoveHaveNodeGate = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/minus.gif"));
        buttonRemoveAllHaveNodeGate = new JButton("All",
                com.iw.tools.ImageUtils.getImage(this, "itsimages/minus.gif"));
        buttonEditHaveNodeGate = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/hammer.gif"));
        buttonEditHaveNodeGate.setToolTipText("Edit a must have entry.");
        buttonAddHaveNodeGate.addActionListener(this);
        buttonRemoveHaveNodeGate.addActionListener(this);
        buttonRemoveAllHaveNodeGate.addActionListener(this);
        buttonEditHaveNodeGate.addActionListener(this);
        //-----------------------------------

        Vector vlang = new Vector();
        Language defaultLang = new Language("English", "EN");
        vlang.add(new Language("Arabic", "AR"));
        vlang.add(new Language("Chinese", "CN"));
        vlang.add(new Language("Dutch", "DU"));
        vlang.add(defaultLang);
        vlang.add(new Language("Farsi", "FS"));
        vlang.add(new Language("French", "FR"));
        vlang.add(new Language("German", "DE"));
        vlang.add(new Language("Italian", "IT"));
        vlang.add(new Language("Japanese", "JP"));
        vlang.add(new Language("Korean", "KR"));
        vlang.add(new Language("Portuguese", "PG"));
        vlang.add(new Language("Russian", "RU"));
        vlang.add(new Language("Spanish", "ES"));
        vlang.add(new Language("Swedish", "SW"));
        languageBox = new JComboBox(vlang);
        languageBox.setSelectedItem(defaultLang);
        languageBox.addActionListener(this);

        tableSignatures = new JSortTable();
        tableSignatures.setRowSelectionAllowed(false);
        tableSignatures.setSelectionForeground(Color.BLUE);
        tableSignatures.addKeyListener(this);
        tableSignatures.addFocusListener(this);

        tableThes = new JTable();
        tableThes.setRowSelectionAllowed(false);
        tableThes.setSelectionForeground(Color.BLUE);

        tableHaveGate = new JSortTable();
        tableHaveGate.setRowSelectionAllowed(false);
        tableHaveGate.setSelectionForeground(Color.BLUE);

        tableHave = new JSortTable();
        tableHave.setRowSelectionAllowed(false);
        tableHave.setSelectionForeground(Color.BLUE);

        JScrollPane scrollThes = new JScrollPane(tableHaveGate);
        JScrollPane scrollHave = new JScrollPane(tableHave);

        // scrollHave.setPreferredSize(new
        // Dimension(487,tableHave.getPreferredSize().height));
        // scrollHave.setMaximumSize(new
        // Dimension(487,tableHave.getMaximumSize().height));
        // scrollThes.setPreferredSize(new
        // Dimension(487,tableThes.getPreferredSize().height));
        // scrollThes.setMaximumSize(new
        // Dimension(487,tableThes.getMaximumSize().height));

        tableDocuments = new RowColorJTable(2,
                ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS.intValue(),
                (Object) new String(""));
        loadDocuments(tableDocuments, new Vector());

        tableDocuments2 = new JSortTable();
        loadDocuments(tableDocuments2, new Vector());

        tableSignatures2 = new JSortTable();
        tableSignatures2
                .setToolTipText("Modify the concept signature of this topic.");
        tableSignatures2.setRowSelectionAllowed(false);
        tableSignatures2.setSelectionForeground(Color.BLUE);
        tableSignatures2.addKeyListener(this);
        tableSignatures2.addFocusListener(this);

        JTextField textField = new JTextField();
        textField.setBorder(new LineBorder(Color.BLACK));
        textField.setForeground(Color.WHITE); // adjust colors as these
        textField.setBackground(Color.BLUE); // will not be taken from
        // the JTable
        tableSignatures.setDefaultEditor(Signature.class,
                new DefaultCellEditor(textField));
        tableSignatures.setCellSelectionEnabled(true);
        tableSignatures2.setDefaultEditor(Signature.class,
                new DefaultCellEditor(textField));
        tableSignatures2.setCellSelectionEnabled(true);

        // Create the rendeder for the cells
        CellRenderer _renderer = new CellRenderer();
        try {
            tableSignatures.setDefaultRenderer(
                    Class.forName("java.lang.Object"), _renderer);
            tableSignatures2.setDefaultRenderer(
                    Class.forName("java.lang.Object"), _renderer);
            tableDocuments.setDefaultRenderer(
                    Class.forName("java.lang.Object"), _renderer);
        } catch (ClassNotFoundException ex) {
            System.out.println("SpreadSheet() Can't modify renderer");
        }

        loadSignatures(tableSignatures, vSignatures);
        //loadThesaurus(tableThes, new Vector());
        loadNodeMustHaveGate(tableHaveGate, new Vector());
        loadMustHave(tableHave, new Vector());

        buttonSave = new JButton("Save");
        buttonCancel = new JButton("Cancel");
        buttonSave.addActionListener(this);
        buttonCancel.addActionListener(this);

        // second tab: documents
        openButton = new JButton("Open");
        openButton.setToolTipText("View this document in a browser window.");
        openButton.addActionListener(this);
        relatedButton = new JButton("Related");
        relatedButton.setToolTipText("View topics related to this document.");
        relatedButton.addActionListener(this);
        addNDButton = new JButton("Add");
        addNDButton
                .setToolTipText("Add a new document relationship to this topic.");
        removeNDButton = new JButton("Remove");
        removeNDButton
                .setToolTipText("Remove the selected document relationship from this topic.");
        removeNDButton.addActionListener(this);
        propertiesButton = new JButton("Properties");
        propertiesButton
                .setToolTipText("Edit the properties of a selected document.");
        propertiesButton.addActionListener(this);
        chartButton = new JButton("Chart");
        chartButton.addActionListener(this);
        chartButton
                .setToolTipText("Generate a bar chart with a historical perspective of this document set.");
        documentButton = new JButton("Classify");
        documentButton.addActionListener(this);
        explainButton = new JButton("Explain");
        explainButton.addActionListener(this);

        // third tab: blacklist
        // second tab: documents
        openButton2 = new JButton("Open");
        openButton2.setToolTipText("View this document in a browser window.");
        openButton2.addActionListener(this);
        relatedButton2 = new JButton("Related");
        relatedButton2.setToolTipText("View topics related to this document.");
        relatedButton2.addActionListener(this);
        chartButton2 = new JButton("Chart");
        chartButton2.addActionListener(this);
        chartButton2
                .setToolTipText("Generate a bar chart with a historical perspective of this document set.");
        documentButton2 = new JButton("Classify");
        documentButton2.addActionListener(this);

        // fourth tab: signatures
        buttonApply = new JButton("Apply");
        buttonApply
                .setToolTipText("Apply concept signature changes permanently to the database.");
        buttonApply.setEnabled(false);
        buttonApply.addActionListener(this);

        buttonUndo = new JButton("Undo");
        buttonUndo.setToolTipText("Undo recent concept signature changes.");
        buttonUndo.setEnabled(false);
        buttonUndo.addActionListener(this);

        buttonTest = new JButton("Docs");
        buttonTest
                .setToolTipText("Test concept signature changes against the current document set.");
        buttonTest.addActionListener(this);

        buttonTest2 = new JButton("Query");
        buttonTest2
                .setToolTipText("Test concept signature changes against a custom document set.");
        buttonTest2.addActionListener(this);

        buttonCheckSignatures = new JButton("Check");
        buttonCheckSignatures.setToolTipText("Quality check for signatures");
        buttonCheckSignatures.addActionListener(this);

        buttonAdd2 = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/plus.gif"));
        buttonRemove2 = new JButton(com.iw.tools.ImageUtils.getImage(this,
                "itsimages/minus.gif"));
        buttonAdd2.addActionListener(this);
        buttonRemove2.addActionListener(this);

        // fifth tab: source
        nodeSourceArea = new JTextArea();
        nodeSourceArea.setWrapStyleWord(true);
        nodeSourceArea.setLineWrap(true);
        nodeSourceSaveButton = new JButton("Save");
        nodeSourceSaveButton
                .setToolTipText("Save this text as the topic source.");
        nodeSourceSaveButton.addActionListener(this);

        CreateSigFromSelection = new JButton("Create Sig");
        CreateSigFromSelection
                .setToolTipText("Create Signature from selection.");
        CreateSigFromSelection.addActionListener(this);

        CreateSigFromSelectionMulti = new JButton("Create Multi Sig");
        CreateSigFromSelectionMulti
                .setToolTipText("Create Signature from multi selection.");
        CreateSigFromSelectionMulti.addActionListener(this);

        CreateMustHaveFromSelection = new JButton("Create Must Have");
        CreateMustHaveFromSelection
                .setToolTipText("Create Must Have from selection.");
        CreateMustHaveFromSelection.addActionListener(this);

        CreateMustHaveFromSelectionMulti = new JButton("Create Multi Must Have");
        CreateMustHaveFromSelectionMulti
                .setToolTipText("Create Must Have from Multi selection.");
        CreateMustHaveFromSelectionMulti.addActionListener(this);

        fixSourceWiki = new JButton("Populate source from wikipedia");
        fixSourceWiki.addActionListener(this);

        fixdescBritannica = new JButton("Fix britannica desc");
        fixdescBritannica.addActionListener(this);

        DefaultKeyedValues defaultkeyedvalues = new DefaultKeyedValues();
        CategoryDataset ds = DatasetUtilities.createCategoryDataset(
                "Signatures", defaultkeyedvalues);
        chart = ChartFactory.createBarChart("Signature Frequency", "Term",
                "Frequency", ds, PlotOrientation.VERTICAL, true, true, false);
        chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(450, 250));

        JComboBox nodeType = new JComboBox();
        nodeType.addItem("Independent");
        nodeType.addItem("Rollup Children");
        nodeType.addItem("Enforce Parent");

        if (its.getServerVersion().equals("ProcinQ Server 3.140")) {
            tableHave.setEnabled(false);
            tableHaveGate.setEnabled(false);
            checkBoxAcr.setEnabled(false);
            // checkBoxLocked.setEnabled(false);
            nodeType.setEnabled(false);
            scrollHave.setEnabled(false);
            buttonAddHave.setEnabled(false);
            buttonAddHaveNodeGate.setEnabled(false);
            buttonRemoveHave.setEnabled(false);
            buttonEditHave.setEnabled(false);
            tableHave.setFocusable(false);
            tableHaveGate.setFocusable(false);

            DisabledTableRenderer rend = new DisabledTableRenderer();
            tableHave
                    .setDefaultRenderer(String.class, (TableCellRenderer) rend);

            tableHaveGate
                    .setDefaultRenderer(String.class, (TableCellRenderer) rend);
        }

        if (corpusMTlevel == 2) {
            DisabledTableRenderer rend = new DisabledTableRenderer();
            tableThes
                    .setDefaultRenderer(String.class, (TableCellRenderer) rend);

            tableThes.setEnabled(false);
            tableThes.setFocusable(false);
            tableThes.setForeground(new Color(33, 33, 33));
        }

        buttonAddThes.setEnabled(false);
        buttonRemoveThes.setEnabled(false);
        buttonEditThes.setEnabled(false);

        // tab ImageManager
        panelImages = new Manager();
        panelImages.Build();
        uploadImageFileSystem = new JButton("Upload Images (Local)");
        uploadImageFileSystem.addActionListener(this);

        uploadImageUrl = new JButton("Upload images (URL)");
        uploadImageUrl.addActionListener(this);
        deleteImage = new JButton("Delete Image");
        deleteImage.addActionListener(this);

        refreshImage = new JButton("Refresh");
        refreshImage.addActionListener(this);

        btnImagesS3 = new JButton("Save images in S3");
        btnImagesS3.addActionListener(this);

        tabPane = layout.nestTabbedPane();
        {
            layout.nestBoxAsTab("General", OculusLayout.HORIZONTAL,
                    OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.nestBox(OculusLayout.HORIZONTAL,
                            OculusLayout.TOP_JUSTIFY);
                    {
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.HORIZONTAL,
                                            OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.add(buttonImage);

                                        layout.add(textTitle);
                                        layout.parent();
                                    }
                                    layout.nestBox(OculusLayout.HORIZONTAL,
                                            OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.add(labelDesc);
                                        layout.add(new JScrollPane(textDesc));
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.addSpace(10);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    optNodeMustHaveGate = new JComboBox();
                                    optNodeMustHaveGate.addItem("Taxonomy Must Have");
                                    optNodeMustHaveGate.addItem("Custom");
                                    optNodeMustHaveGate.addItem("Inherint 1");
                                    optNodeMustHaveGate.addItem("Inherint 2");
                                    optNodeMustHaveGate.addItem("Inherint 3");
                                    optNodeMustHaveGate.addItem("Inherint 4");

                                    optNodeMustHaveGate.setEditable(true);
                                    layout.add(optNodeMustHaveGate);
                                    layout.parent();
                                }
                                layout.addSpace(10);
                                layout.nestGrid(3, 2);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(new JLabel("  "));
                                        layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                        {
                                            layout.add(buttonAddHaveNodeGate);
                                            layout.add(buttonRemoveHaveNodeGate);
                                            layout.add(buttonEditHaveNodeGate);
                                            layout.add(buttonRemoveAllHaveNodeGate);
                                            layout.parent();
                                        }
                                        layout.add(scrollThes);
                                        layout.parent();
                                    }
                                    layout.addSpace(10);
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(new JLabel("Filter Must Haves"));
                                        layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                        {
                                            layout.add(buttonAddHave);
                                            layout.add(buttonRemoveHave);
                                            layout.add(buttonEditHave);
                                            layout.add(buttonRemoveAllHave);
                                            layout.parent();
                                        }
                                        layout.add(scrollHave);
                                        layout.parent();
                                    }
                                    layout.addSpace(10);
                                    layout.addSpace(10);
                                    layout.addSpace(10);
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.HORIZONTAL,
                                    OculusLayout.JUSTIFY_CENTER);
                            {
                                layout.add(new JLabel("Topic Properties: "));
                                layout.parent();
                                layout.addSpace(10);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.JUSTIFY_CENTER);
                                {
                                    isBridgeNode = new JCheckBox("Bridge Node");
                                    listNodeType = new JComboBox();

                                    layout.add(isBridgeNode);
                                    layout.add(new JLabel("|"));
                                    layout.add(new JLabel("Node Type"));
                                    layout.add(listNodeType);
                                }
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.HORIZONTAL,
                                    OculusLayout.JUSTIFY_CENTER);
                            {
                                layout.add(labelSize);
                                layout.add(
                                        textSize,
                                        OculusLayout.ALIGNMENT_SPACE_STRETCHING,
                                        OculusLayout.NO_STRETCH);
                                layout.addSpace(20);
                                layout.add(languageBox);
                                layout.addSpace(80);
                                layout.add(buttonAdd);
                                layout.add(buttonRemove);
                                layout.add(buttonCreate);
                                layout.add(buttonRemoveSigAll);
                                layout.parent();
                            }
                            layout.add(new JScrollPane(tableSignatures));
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL,
                            OculusLayout.RIGHT_JUSTIFY);
                    {
                        // layout.addSpace(10);

                        layout.add(buttonSave, OculusLayout.RIGHT_JUSTIFY,
                                OculusLayout.MAX_STRETCHING_PREFERENCE);
                        layout.add(buttonCancel, OculusLayout.RIGHT_JUSTIFY,
                                OculusLayout.MAX_STRETCHING_PREFERENCE);
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.nestBoxAsTab("Documents", OculusLayout.HORIZONTAL,
                    OculusLayout.TOP_JUSTIFY);
            {
                layout.addSpace(10);
                layout.nestBox(OculusLayout.HORIZONTAL,
                        OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(new JScrollPane(tableDocuments));
                    layout.parent();
                }
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.add(openButton);
                    layout.addSpace(10);
                    layout.add(relatedButton);
                    layout.addSpace(10);
                    // layout.add(addNDButton);
                    // layout.addSpace(10);
                    layout.add(removeNDButton);
                    layout.addSpace(10);
                    layout.add(documentButton);
                    layout.addSpace(10);
                    layout.add(propertiesButton);
                    layout.addSpace(10);
                    layout.add(explainButton);
                    layout.addSpace(10);
                    layout.add(chartButton);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.nestBoxAsTab("Blacklist", OculusLayout.HORIZONTAL,
                    OculusLayout.TOP_JUSTIFY);
            {
                layout.addSpace(10);
                layout.nestBox(OculusLayout.HORIZONTAL,
                        OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(new JScrollPane(tableDocuments2));
                    layout.parent();
                }
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.add(openButton2);
                    layout.addSpace(10);
                    layout.add(relatedButton2);
                    layout.addSpace(10);
                    // layout.add(addNDButton);
                    // layout.addSpace(10);
                    // layout.add(removeNDButton2);
                    // layout.addSpace(10);
                    layout.add(documentButton2);
                    layout.addSpace(10);
                    layout.add(chartButton2);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.nestBoxAsTab("Signatures", OculusLayout.HORIZONTAL,
                    OculusLayout.TOP_JUSTIFY);
            {
                layout.addSpace(10);
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.nestBox(OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.add(chartPanel);
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.add(buttonApply);
                                    layout.add(new Space(5, 5));
                                    layout.add(buttonUndo);
                                    layout.parent();
                                }
                                layout.add(new Space(5, 5));
                                layout.add(new JLabel("      Test"));
                                layout.add(new JSeparator());
                                layout.add(new Space(5, 5));
                                layout.add(buttonTest);
                                layout.add(new Space(5, 5));
                                layout.add(buttonTest2);
                                layout.add(new Space(5, 5));
                                layout.add(buttonCheckSignatures);
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBox(OculusLayout.VERTICAL,
                                OculusLayout.RIGHT_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.HORIZONTAL,
                                    OculusLayout.TOP_JUSTIFY);
                            {
                                layout.add(buttonAdd2, OculusLayout.NO_STRETCH,
                                        OculusLayout.NO_STRETCH);
                                layout.add(buttonRemove2,
                                        OculusLayout.NO_STRETCH,
                                        OculusLayout.NO_STRETCH);
                                layout.addSpace(2);
                                layout.parent();
                            }
                            layout.add(new JScrollPane(tableSignatures2));
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.nestBoxAsTab("Source", OculusLayout.HORIZONTAL,
                    OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL,
                            OculusLayout.TOP_JUSTIFY);
                    {
                        layout.add(new JScrollPane(nodeSourceArea));
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.add(nodeSourceSaveButton);
                            layout.add(CreateSigFromSelection);
                            layout.add(CreateMustHaveFromSelection);
                            layout.add(CreateSigFromSelectionMulti);
                            layout.add(CreateMustHaveFromSelectionMulti);
                            layout.add(fixSourceWiki);
                            layout.add(fixdescBritannica);
                        }
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }

            layout.parent();

            layout.nestBoxAsTab("Images", OculusLayout.HORIZONTAL,
                    OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL,
                            OculusLayout.TOP_JUSTIFY);
                    {
                        layout.add(new JScrollPane(panelImages));
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.add(uploadImageFileSystem);
                            layout.addSpace(5);
                            layout.add(uploadImageUrl);
                            layout.addSpace(5);
                            layout.add(deleteImage);
                            layout.addSpace(5);
                            layout.add(refreshImage);
                            layout.addSpace(5);
                            layout.add(btnImagesS3);

                        }
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
        }
        tabPane.addChangeListener(this);

        textPanel.add(ob);
        return textPanel;
    }

    private ImageIcon scaleInstance(ImageIcon ii) {
        return new ImageIcon(ii.getImage().getScaledInstance(24, 24,
                Image.SCALE_DEFAULT));
    }

    private JToolBar setToolBar() {
        User u = ITS.getUser();
        IndraLicense license = u.getLicense();

        ImageIcon icoAdd = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/addAction.gif");
        ImageIcon icoOal = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/openall.png");
        ImageIcon icoCut = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/cutAction.gif");
        ImageIcon icoCopy = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/copyAction.gif");
        ImageIcon icoPaste = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/pasteAction.gif");
        ImageIcon icoLink = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/chain.gif");
        ImageIcon icoSave = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/saveAction.gif");
        ImageIcon icoDelete = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/deleteAction.gif");
        ImageIcon icoJoin = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/join.png");
        ImageIcon icoSplit = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/split.png");
        ImageIcon icoOrthog = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/build.png");
        ImageIcon icoMoveUp = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/moveupAction.gif");
        ImageIcon icoMoveDown = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/movedownAction.gif");
        ImageIcon icoOutdent = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/outdentAction.gif");
        ImageIcon icoIndent = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/indentAction.gif");
        ImageIcon icoUndo = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/undoAction.gif");
        ImageIcon icoAlpha = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/alphaAction.gif");
        ImageIcon icoRefresh = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/refresh.gif");
        ImageIcon icoProps = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/props.gif");

        addAction = new toolbarAction(icoAdd, "Add", new Integer(
                KeyEvent.VK_INSERT));
        openallAction = new toolbarAction(icoOal, "Open All", new Integer(
                KeyEvent.VK_O));
        cutAction = new toolbarAction(icoCut, "Cut", new Integer(KeyEvent.VK_X));
        copyAction = new toolbarAction(icoCopy, "Copy", new Integer(
                KeyEvent.VK_C));
        pasteAction = new toolbarAction(icoPaste, "Paste", new Integer(
                KeyEvent.VK_V));
        linkAction = new toolbarAction(icoLink, "Paste as Link", new Integer(
                KeyEvent.VK_L));
        saveAction = new toolbarAction(icoSave, "Save", new Integer(
                KeyEvent.VK_S));
        deleteAction = new toolbarAction(icoDelete, "Delete", new Integer(
                KeyEvent.VK_D));
        joinAction = new toolbarAction(icoJoin, "Join", new Integer(
                KeyEvent.VK_J));
        splitAction = new toolbarAction(icoSplit, "Split", new Integer(
                KeyEvent.VK_P));
        orthogAction = new toolbarAction(icoOrthog, "Build", new Integer(
                KeyEvent.VK_B));
        moveupAction = new toolbarAction(icoMoveUp, "Move Up", new Integer(
                KeyEvent.VK_UP));
        movedownAction = new toolbarAction(icoMoveDown, "Move Down",
                new Integer(KeyEvent.VK_DOWN));
        indentAction = new toolbarAction(icoIndent, "Indent", new Integer(
                KeyEvent.VK_LEFT));
        outdentAction = new toolbarAction(icoOutdent, "Outdent", new Integer(
                KeyEvent.VK_RIGHT));
        undoAction = new toolbarAction(icoUndo, "Undo", new Integer(
                KeyEvent.VK_U));
        alphaAction = new toolbarAction(icoAlpha, "Alphabetize", new Integer(
                KeyEvent.VK_A));
        refreshAction = new toolbarAction(icoRefresh, "Refresh Topic",
                new Integer(KeyEvent.VK_R));
        propsAction = new toolbarAction(icoProps, "Properties", new Integer(
                KeyEvent.VK_P));

        addButton = new JButton(addAction);
        openallButton = new JButton(openallAction);
        cutButton = new JButton(cutAction);
        copyButton = new JButton(copyAction);
        pasteButton = new JButton(pasteAction);
        linkButton = new JButton(linkAction);
        saveButton = new JButton(saveAction);
        deleteButton = new JButton(deleteAction);
        joinButton = new JButton(joinAction);
        splitButton = new JButton(splitAction);
        orthogButton = new JButton(orthogAction);
        moveupButton = new JButton(moveupAction);
        movedownButton = new JButton(movedownAction);
        indentButton = new JButton(indentAction);
        outdentButton = new JButton(outdentAction);
        undoButton = new JButton(undoAction);
        alphaButton = new JButton(alphaAction);
        refreshButton = new JButton(refreshAction);
        propsButton = new JButton(propsAction);

        JToolBar jtb = new JToolBar();

        addButton.setMaximumSize(new Dimension(30, 30));
        addButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(addButton);
        cutButton.setMaximumSize(new Dimension(30, 30));
        cutButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(cutButton);
        copyButton.setMaximumSize(new Dimension(30, 30));
        copyButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(copyButton);
        pasteButton.setMaximumSize(new Dimension(30, 30));
        pasteButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(pasteButton);
        linkButton.setMaximumSize(new Dimension(30, 30));
        linkButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(linkButton);
        saveButton.setMaximumSize(new Dimension(30, 30));
        saveButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(saveButton);
        deleteButton.setMaximumSize(new Dimension(30, 30));
        deleteButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(deleteButton);
        moveupButton.setMaximumSize(new Dimension(30, 30));
        moveupButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(moveupButton);
        movedownButton.setMaximumSize(new Dimension(30, 30));
        movedownButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(movedownButton);
        indentButton.setMaximumSize(new Dimension(30, 30));
        indentButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(indentButton);
        outdentButton.setMaximumSize(new Dimension(30, 30));
        outdentButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(outdentButton);
        undoButton.setMaximumSize(new Dimension(30, 30));
        undoButton.setMinimumSize(new Dimension(30, 30));
        // jtb.add(undoButton);
        openallButton.setMaximumSize(new Dimension(30, 30));
        openallButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(openallButton);
        alphaButton.setMaximumSize(new Dimension(30, 30));
        alphaButton.setMinimumSize(new Dimension(30, 30));
        if (!u.lockFunctions()) {
            jtb.add(alphaButton);
        }
        refreshButton.setMaximumSize(new Dimension(30, 30));
        refreshButton.setMinimumSize(new Dimension(30, 30));
        joinButton.setMaximumSize(new Dimension(30, 30));
        joinButton.setMinimumSize(new Dimension(30, 30));
        if (!u.lockFunctions()) {
            jtb.add(joinButton);
        }
        splitButton.setMaximumSize(new Dimension(30, 30));
        splitButton.setMinimumSize(new Dimension(30, 30));
        if (!u.lockFunctions()) {
            jtb.add(splitButton);
        }
        orthogButton.setMaximumSize(new Dimension(30, 30));
        orthogButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(orthogButton);
        propsButton.setMaximumSize(new Dimension(30, 30));
        propsButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(propsButton);
        jtb.add(refreshButton);

        // these buttons are greyed out except when conducting taxonomy building
        if (!license.isAuthorized(IndraLicense.INGESTION)) {
            orthogButton.setEnabled(false);
            joinButton.setEnabled(false);
            splitButton.setEnabled(false);
        }

        jtb.setOpaque(true);

        return jtb;
    }

    public void windowOpened(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void windowClosing(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void windowClosed(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void windowIconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void windowDeiconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void windowActivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void windowDeactivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public class toolbarAction extends AbstractAction {

        public toolbarAction(ImageIcon icon, String desc, Integer mnemonic) {
            super(null, icon);
            putValue(SHORT_DESCRIPTION, desc);
            putValue(MNEMONIC_KEY, mnemonic);
        }

        public void actionPerformed(ActionEvent e) {
            SwingWorker aWorker = new SwingWorker(e) {
                protected void doNonUILogic() {
                    glass.setCursor(Cursor
                            .getPredefinedCursor(Cursor.WAIT_CURSOR));
                    glass.setVisible(true);

                    DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                            .getLastSelectedPathComponent();
                    try {
                        if (e.getSource().equals(saveButton)) {
                            saveNodeProps(item);

                        }
                        if (e.getSource().equals(openallButton)) {
                            openAll(item); // for (int i = 0; i <
                            // tree.getRowCount(); i++)
                            // tree.expandRow(i);
                            ((DefaultTreeModel) tree.getModel())
                                    .nodeStructureChanged(item);
                            for (int i = 0; i < tree.getRowCount(); i++) {
                                tree.expandRow(i);
                            }
                        }
                        if (e.getSource().equals(cutButton)) {
                            tree.cutSelectedNode();
                        }
                        if (e.getSource().equals(copyButton)) {
                            tree.copySelectedNode();
                        }
                        if (e.getSource().equals(pasteButton)) { // PASTE TOPIC
                            tree.pasteIntoSelectedNode(admin);
                        }
                        if (e.getSource().equals(linkButton)) {
                            tree.pasteIntoSelectedNode(admin, true);
                        }
                        if (e.getSource().equals(alphaButton)) {
                            tree.alphabetizeNode();
                        }
                        if (e.getSource().equals(propsButton)) {
                            CorpusProperties p = new CorpusProperties(admin,
                                    corp);
                        }
                        if (e.getSource().equals(refreshButton)) {
                            // MH Added dialog box to confirm button push
                            tree.scheduleNodeRefresh();
                            JOptionPane.showMessageDialog(admin,
                                    "Topics have been queued for crawling.");
                        }
                        if (e.getSource().equals(joinButton)) {
                            if (ITS.getUser().PromptForJoinChange()) {
                                if (JOptionPane
                                        .showConfirmDialog(
                                        admin,
                                        "Are you sure that you want to merge these topics?",
                                        "Confirm",
                                        JOptionPane.YES_NO_OPTION) != 0) {
                                    return;
                                }
                            }

                            TreePath path[] = tree.getSelectionPaths();

                            if (path.length < 2) {
                                JOptionPane.showMessageDialog(admin,
                                        "You must have more than one topic selected to."
                                        + "join topics.", "Error",
                                        JOptionPane.NO_OPTION);
                                return;
                            }

                            DefaultMutableTreeNode dmtnm = (DefaultMutableTreeNode) path[0]
                                    .getLastPathComponent();
                            ITSTreeNode nMaster = (ITSTreeNode) dmtnm
                                    .getUserObject();
                            try {
                                nMaster = its.getNodeSource(nMaster);
                            } catch (Exception e) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "Error encountered while attempting to extract ."
                                        + "topic CLOB from the database.",
                                        "Error", JOptionPane.NO_OPTION);
                                e.printStackTrace(System.out);
                                return;
                            }
                            String sSource = nMaster.get("NODESOURCE");

                            // loop through each topic to be merged, and check
                            // for children. If there is children,
                            // get them and move their parent up a level.
                            for (int i = 1; i < path.length; i++) {
                                DefaultMutableTreeNode dmtns = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                ITSTreeNode nSlave = (ITSTreeNode) dmtns
                                        .getUserObject();
                                Vector vNodes = its
                                        .CQL("SELECT <NODE> WHERE PARENTID = "
                                        + nSlave.get("NODEID"));

                                for (int j = 0; j < vNodes.size(); j++) {
                                    Node child = (Node) vNodes.elementAt(j);
                                    its.editNodeParent(child.get("LINKNODEID"),
                                            nMaster.get("NODEID"),
                                            nSlave.get("NODEINDEXWITHINPARENT"));
                                }
                            }

                            for (int i = 1; i < path.length; i++) {
                                DefaultMutableTreeNode dmtns = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                ITSTreeNode nSlave = (ITSTreeNode) dmtns
                                        .getUserObject();
                                try {
                                    nSlave = its.getNodeSource(nSlave);
                                } catch (Exception e) {
                                    nSlave.set("NODESOURCE", "");
                                }

                                // sSource = sSource + nSlave.get("NODESOURCE");
                                sSource = sSource + nSlave.get("NODESOURCE")
                                        + " " + nSlave.get("NODETITLE") + " ";

                                DefaultMutableTreeNode parent = (DefaultMutableTreeNode) dmtns
                                        .getParent();

                                // delete node
                                its.deleteNode(nSlave);
                                parent.remove(dmtns);

                                ((DefaultTreeModel) tree.getModel())
                                        .nodeStructureChanged(parent);
                            }
                            tree.refreshNode(dmtnm);
                            ((DefaultTreeModel) tree.getModel())
                                    .nodeStructureChanged(dmtnm);

                            try {
                                its.setNodeSource(nMaster, sSource);
                            } catch (Exception e) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "Error encountered attempting to save ."
                                        + "combined topic source to the database.",
                                        "Error", JOptionPane.NO_OPTION);
                                System.out.println("Error saving: " + sSource);
                                e.printStackTrace(System.out);
                                return;
                            }

                            // tabPane.setSelectedIndex(0);
                            tree.setSelectionPath(path[0]);

                            if (ITS.getUser().PromptForJoinChange()) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "The "
                                        + path.length
                                        + " topics that you selected have "
                                        + "been joined successfully.  The new merged topic is named "
                                        + nMaster
                                        .get("NODETITLE"),
                                        "Information",
                                        JOptionPane.NO_OPTION);
                            }
                        }
                        if (e.getSource().equals(splitButton)) {
                            if (item.getChildCount() > 0) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "You may not split a topic with children.",
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            }

                            tree.copySelectedNode();
                            tree.splitNode(admin, item);
                        }

                        // create signatures for this corpus
                        if (e.getSource().equals(orthogButton)) {
                            int i = JOptionPane
                                    .showConfirmDialog(
                                    admin,
                                    "By ingesting this corpus you will destroy "
                                    + "any existing signatures associated with this taxonomy.  Continue? ",
                                    "Confirm",
                                    JOptionPane.YES_NO_OPTION);
                            if (i != 0) {
                                return;
                            }

                            try {
                                boolean b = its.orthogonalizeCorpus(CorpusID);

                                if (true) {
                                    JOptionPane.showMessageDialog(admin,
                                            "Taxonomy build successful.",
                                            "Information",
                                            JOptionPane.NO_OPTION);
                                } else {
                                    JOptionPane
                                            .showMessageDialog(
                                            admin,
                                            "Taxonomy build was unsuccessful, please check "
                                            + "the error log for further details.",
                                            "Error",
                                            JOptionPane.NO_OPTION);
                                }

                            } catch (Exception e) {
                                e.printStackTrace(System.out);

                                JOptionPane.showMessageDialog(admin, "Error: "
                                        + e.getLocalizedMessage(), "Error",
                                        JOptionPane.NO_OPTION);
                                return;
                            }
                        }

                        if (e.getSource().equals(addButton)) { // ADD NEW TOPIC
                            ITSTreeNode parent = (ITSTreeNode) item
                                    .getUserObject();
                            ITSTreeNode node = its.emptyNode(parent);
                            int index = tree.getNodeIndex(item);

                            try {
                                its.addNode(node);
                                tree.refreshNode(item);

                                if (index != -1) {
                                    tree.setSelectionRow(index);
                                }
                            } catch (TopicExistsError tee) {
                                return;
                            } catch (Exception e) {
                                throw e;
                            }
                        }
                        if (e.getSource().equals(deleteButton)) { // DELETE
                            // TOPIC
                            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) item
                                    .getParent();

                            if ((item.getChildCount() > 0)
                                    && (ITS.getUser().PromptForTopicRemoval())) {
                                int i = JOptionPane
                                        .showConfirmDialog(
                                        admin,
                                        "Delete this topic and all children?  Are you sure?",
                                        "Confirm",
                                        JOptionPane.YES_NO_OPTION);
                                if (i != 0) {
                                    return;
                                }
                            } else if (ITS.getUser().PromptForTopicRemoval()) {
                                int i = JOptionPane.showConfirmDialog(null,
                                        "Permanently remove this topic? ",
                                        "Confirm", JOptionPane.YES_NO_OPTION);
                                if (i != 0) {
                                    return;
                                }
                            }

                            its.deleteNode((ITSTreeNode) item.getUserObject());
                            int index = tree.deleteSelectedNode();
                            tree.refreshNode(parent);
                            if (index != -1) {
                                tree.setSelectionRow(index);
                            }
                        }
                        if (e.getSource().equals(indentButton)) { // INDENT
                            // TOPIC
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                indentNode(dmtn);
                                tree.setSelectionPath(new TreePath(dmtn
                                        .getPath()));
                            }
                        }
                        if (e.getSource().equals(outdentButton)) { // OUTDENT
                            // TOPIC
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                outdentNode(dmtn);
                                tree.setSelectionPath(new TreePath(dmtn
                                        .getPath()));
                            }
                        }
                        if (e.getSource().equals(moveupButton)) { // decrement
                            // the index
                            // within
                            // the
                            // parent
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                moveupNode(dmtn);
                            }
                        }
                        if (e.getSource().equals(movedownButton)) { // increment
                            // the index
                            // within
                            // the
                            // parent
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                movedownNode(dmtn);
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    glass.setCursor(Cursor
                            .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    glass.setVisible(false);
                }
            };
            aWorker.start();
        }
    }

    public void reloadDocuments() {
        TreePath path = tree.getSelectionPath();
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path
                .getLastPathComponent();
        ITSTreeNode n = (ITSTreeNode) selectedNode.getUserObject();

        String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = " + n.get("NODEID")
                + " AND SCORE1 > 25 ORDER BY SCORE1 DESC";
        try {
            loadDocuments(tableDocuments, its.CQL(sCQL));
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public void viewDocument(JTable tableDocuments) {
        try {
            int iSelectedRow = tableDocuments.getSelectedRow();
            if (iSelectedRow < 0) {
                JOptionPane.showMessageDialog(admin,
                        "You must first select a document to view.",
                        "Information", JOptionPane.NO_OPTION);
                return;
            }
            int row = tableDocuments.getSelectedRow();
            com.iw.system.Document d = (com.iw.system.Document) tableDocuments
                    .getModel().getValueAt(row, 0);

            String sCommand = "";
            if (ITS.getUser().getPathPrefix().equals("")) {
                sCommand = "cmd /c explorer \"" + d.get("DOCURL") + "\"";
            } else {
                sCommand = "cmd /c explorer \"" + ITS.getUser().getPathPrefix()
                        + d.get("DOCURL") + "\"";
            }

            // System.out.println("start view doc with command [" + sCommand +
            // "]");
            java.lang.Runtime.getRuntime().exec(sCommand);

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    // loop recursively through the item children, loading all sub nodes and
    // opening each in the tree
    public void openAll(DefaultMutableTreeNode item) {
        // if item is null, use the root node of the tree instead
        if (item == null) {
            item = (DefaultMutableTreeNode) tree.getModel().getRoot();
        }
        ITSTreeNode n = (ITSTreeNode) item.getUserObject();

        // (a) remove all children
        item.removeAllChildren();

        // (b) select current children
        Vector v = new Vector();
        try {
            v = its.CQL("SELECT <NODE> WHERE PARENTID = " + n.get("NODEID")
                    + " ORDER BY NODEINDEXWITHINPARENT", 1, 10000);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        // (c) add them and recurse
        for (int i = 0; i < v.size(); i++) {
            ITSTreeNode child = (ITSTreeNode) v.elementAt(i);
            DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode(child,
                    true);

            item.add(dmtn);
            openAll(dmtn);
        }

        return;
    }

    public void invokeExplain(ITSTreeNode n) throws Exception {
        int row = tableDocuments.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(admin,
                    "A document (row) must be selected.", "Information",
                    JOptionPane.NO_OPTION);
            return;
        }

        com.iw.system.Document d = (com.iw.system.Document) tableDocuments
                .getModel().getValueAt(row, 0);

        OculusBox ob = new OculusBox();

        // pop up explain frame is an internal MDI frame
        JInternalFrame jifExplainDisplay = new JInternalFrame(
                "Explain Report for Node " + n.get("NODEID") + " Doc "
                + d.get("DOCTITLE") + d.get("DOCUMENTID"), true, true,
                true, true);
        jifExplainDisplay.setLocation(60, 60);
        jifExplainDisplay.setSize(600, 500); // hbk control
        // Dimension winsize = this.getSize(), screensize =
        // Toolkit.getDefaultToolkit().getScreenSize();
        // setSize((int) (screensize.width / 1.5), (int) (screensize.height /
        // 1.25));
        jifExplainDisplay.setResizable(true);
        jifExplainDisplay.setBackground(Color.lightGray);
        jifExplainDisplay.setFrameIcon(admin.iIndraweb);
        ExplainDisplayDataContainer explainDisplayDataContainer = null;

        try {
            explainDisplayDataContainer = its.getExplainData(
                    new Integer(n.get("NODEID")).intValue(),
                    new Integer(d.get("DOCUMENTID")).intValue(),
                    n.get("NODETITLE"), d.get("DOCTITLE"), n.get("CORPUSID"),
                    admin, ITS.getUser().modeClassifyDocument());
        } catch (EmptyCorpusToUseList e) {
            JOptionPane.showMessageDialog(admin,
                    "Taxonomy list not set.  Please configure your classification "
                    + "preferences and try your classification again.",
                    "Error", JOptionPane.NO_OPTION);
            Preferences p = new Preferences(admin);
            p.show();

            return;
        }

        if (explainDisplayDataContainer == null) {
            return;
        }

        ExplainDisplayPane explainDisplayPane = new ExplainDisplayPane(admin,
                explainDisplayDataContainer, jifExplainDisplay);

        jifExplainDisplay.setContentPane(ob);
        // jifExplainDisplay.show();
        jifExplainDisplay.getContentPane().add(explainDisplayPane);

        explainDisplayPane.setVisible(true);
        // explainDisplayPane.show();

        // jIntFMDI.setMaximum(true );
        admin.jdp.add(jifExplainDisplay);
        // jIntFMDI.setMaximum(true);
        jifExplainDisplay.show();
        jifExplainDisplay.setVisible(true);

        jifExplainDisplay.toFront();
    }

    private void loadNodeProps(ITSTreeNode n) {
        Vector vSignatures = new Vector();
        try {
            Language lang = (Language) languageBox.getSelectedItem();
            vSignatures = its.getNodeSignatures(n, lang.getCode());
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        textTitle.setText(n.get("NODETITLE"));
        String nodeDesc = n.get("NODEDESC");
        textDesc.setText(nodeDesc);
        textSize.setText(n.get("NODESIZE"));

        if (n.get("BRIDGENODE").equals("1")) {
            isBridgeNode.setSelected(true);
        } else {
            isBridgeNode.setSelected(false);
        }
        Integer nodeMusthaveType;
        if (n.get("NODEMUSTHAVEGATEINHERITANCE") == null || n.get("NODEMUSTHAVEGATEINHERITANCE").equals("")) {
            nodeMusthaveType = null;
        } else {
            nodeMusthaveType = Integer.parseInt(n.get("NODEMUSTHAVEGATEINHERITANCE"));
        }
        String userTyped;
        if (nodeMusthaveType == null) {
            //taxonomy musthave
            optNodeMustHaveGate.setSelectedIndex(0);
        } else if (nodeMusthaveType == 0) {
            // use user typed information MusthaveGate
            optNodeMustHaveGate.setSelectedIndex(1);
        } else {
            optNodeMustHaveGate.setSelectedIndex(nodeMusthaveType + 1);
        }

        String node_Type = n.get("NODETYPE");

        if (objects != null) {
            // load object in combopbox
            listNodeType.removeAllItems();
            for (NodeTypeBean nAux : objects) {
                listNodeType.addItem(nAux);
            }
            // select the name for the nodetype
            NodeTypeBean nSelected = null;
            if (node_Type != null && !node_Type.isEmpty()) {
                for (NodeTypeBean nAux : objects) {
                    if (nAux.getId().equals(node_Type)) {
                        nSelected = nAux;
                        break;
                    }
                }
            }
            if (nSelected != null) {
                listNodeType.setSelectedItem(nSelected);
            }
        }

        if (n.get("NODESTATUS").equals("1")) {
            checkBoxStatus.setSelected(true);
            checkBoxLocked.setSelected(false);
        } else if (n.get("NODESTATUS").equals("2")) {
            checkBoxStatus.setSelected(true);
            checkBoxLocked.setSelected(true);
        } else {
            checkBoxStatus.setSelected(false);
            checkBoxLocked.setSelected(false);
        }

        // new for IDRAC - if this is a link node, and it's an IDRAC
        // installation, disable changes,
        if (tree.setModifyPermission(n)) {
            textTitle.setEnabled(true);
            textDesc.setEnabled(true);
            textSize.setEnabled(true);
            tableSignatures.setEnabled(true);
        } else {
            textTitle.setEnabled(false);
            textDesc.setEnabled(false);
            textSize.setEnabled(false);
            tableSignatures.setEnabled(false);
        }

        loadSignatures(tableSignatures, vSignatures);

        Vector vThesaurus = new Vector();
        Vector vHave = new Vector();
        Vector vHaveGate = new Vector();

        try {
            if (corpusMTlevel == 2) {
                return;
            }

            // get the thesauri used for this corpus
            Vector vThesauri = its.getThesauri(corp);

            for (int i = 0; i < vThesauri.size(); i++) {
                Thesaurus t = (Thesaurus) vThesauri.elementAt(i);
                Vector vterms = new Vector();

                // for each thesaurus, attempt to find the thesaurus word so
                // that we can do a synonym lookup
                try {
                    vterms = its.getThesaurusWords(t.getID(), n
                            .get("NODETITLE").toLowerCase(), true);
                    for (int k = 0; k < vterms.size(); k++) {
                        ThesaurusWord tw = (ThesaurusWord) vterms.elementAt(k);
                        if (tw.getName().toLowerCase()
                                .equals(n.get("NODETITLE").toLowerCase())) {
                            try {
                                Vector synonyms = its.getSynonyms(t.getID(),
                                        Integer.parseInt(tw.getID()));
                                for (int l = 0; l < synonyms.size(); l++) {
                                    ThesaurusWord tw2 = (ThesaurusWord) synonyms
                                            .elementAt(l);
                                    tw2.setThesaurusName(t.getName());
                                    vThesaurus.add(tw2);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }
                } catch (Exception e) {
                }

                if ((corpusMTlevel == 0)
                        && (n.get("NODETITLE").trim().indexOf(" ") != -1)) {
                    String[] words = n.get("NODETITLE").split(" ");
                    for (int j = 0; j < words.length; j++) {
                        try {
                            vterms = its.getThesaurusWords(t.getID(),
                                    words[j].toLowerCase(), true);
                            for (int k = 0; k < vterms.size(); k++) {
                                ThesaurusWord tw = (ThesaurusWord) vterms
                                        .elementAt(k);
                                if (tw.getName().toLowerCase()
                                        .equals(words[j].toLowerCase())) {
                                    try {
                                        Vector synonyms = its.getSynonyms(
                                                t.getID(),
                                                Integer.parseInt(tw.getID()));
                                        for (int l = 0; l < synonyms.size(); l++) {
                                            ThesaurusWord tw2 = (ThesaurusWord) synonyms
                                                    .elementAt(l);
                                            tw2.setThesaurusName(t.getName());
                                            vThesaurus.add(tw2);
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                            }
                        } catch (Exception e) {
                            // e.printStackTrace(System.out);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        try {
            Language lang = (Language) languageBox.getSelectedItem();
            if (lang == null) {
                vHave = its.getMusthaves(n.get("NODEID"));
            } else {
                vHave = its.getMusthaves(n.get("NODEID"), lang.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        try {
            Language lang = (Language) languageBox.getSelectedItem();
            if (lang == null) {
                vHaveGate = its.getMusthaves(n.get("NODEID"), "EN", "2");
            } else {
                vHaveGate = its.getMusthaves(n.get("NODEID"), lang.getCode(), "2");
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        //loadThesaurus(tableThes, vThesaurus);
        loadNodeMustHaveGate(tableHaveGate, vHaveGate);
        loadMustHave(tableHave, vHave);
    }

    private void loadSignatures(JTable table, Vector vSigs) {
        loadSignatures(table, vSigs, false);
    }

    private void loadSignatures(JTable table, Vector vSigs, boolean bAddListener) {
        DefaultSortTableModel dtm = new DefaultSortTableModel(vSigs.size(), 2);

        for (int i = 0; i < vSigs.size(); i++) {
            Signature s = (Signature) vSigs.elementAt(i);
            dtm.setValueAt(s.getWord(), i, 0);
            dtm.setValueAt(new Integer((int) s.getWeight()), i, 1);
        }
        Vector v = new Vector();
        v.add("Term");
        v.add("Weight");
        dtm.setColumnIdentifiers(v);

        table.setModel(dtm);

        // add listener if specified
        if (bAddListener) {
            SelectionListener listener = new SelectionListener();
            table.getSelectionModel().addListSelectionListener(listener);
            table.getColumnModel().getSelectionModel()
                    .addListSelectionListener(listener);
            table.getModel().addTableModelListener(this);
        }
    }

    private void loadThesaurus(JTable table, Vector vTerms) {
        DefaultTableModel dtm = new DefaultSortTableModel(vTerms.size(), 2);

        for (int i = 0; i < vTerms.size(); i++) {
            ThesaurusWord tw = (ThesaurusWord) vTerms.elementAt(i);
            dtm.setValueAt(tw.getName(), i, 0);
            dtm.setValueAt(tw.getThesName(), i, 1);
        }

        Vector v = new Vector();
        v.add("Term");
        v.add("Thesaurus");
        dtm.setColumnIdentifiers(v);

        table.setModel(dtm);
    }

    private void loadNodeMustHaveGate(JTable table, Vector vTerms) {
        DefaultSortTableModel dtm = new DefaultSortTableModel(vTerms.size(), 1);

        for (int i = 0; i < vTerms.size(); i++) {
            String s = (String) vTerms.elementAt(i);
            dtm.setValueAt(s, i, 0);
        }

        Vector v = new Vector();
        v.add("Term");
        dtm.setColumnIdentifiers(v);

        table.setModel(dtm);
    }

    private void loadMustHave(JTable table, Vector vTerms) {
        DefaultSortTableModel dtm = new DefaultSortTableModel(vTerms.size(), 1);

        for (int i = 0; i < vTerms.size(); i++) {
            String s = (String) vTerms.elementAt(i);
            dtm.setValueAt(s, i, 0);
        }

        Vector v = new Vector();
        v.add("Term");
        dtm.setColumnIdentifiers(v);

        table.setModel(dtm);
    }

    private void loadDocuments(JTable table, Vector vDocs) {
        loadDocuments(table, vDocs, true);
    }

    private void loadDocuments(JTable table, Vector vDocs, boolean bShowScore) {
        DefaultSortTableModel dtm = new DefaultSortTableModel(vDocs.size(), 3);

        for (int i = 0; i < vDocs.size(); i++) {
            NodeDocument nd = (NodeDocument) vDocs.elementAt(i);
            com.iw.system.Document d = nd.getDocProps();
            dtm.setValueAt(d, i, 0);
            dtm.setValueAt(nd.get("DOCSUMMARY"), i, 1);
            if (bShowScore) {
                dtm.setValueAt(nd.get("SCORE1"), i, 2);
            }
        }
        Vector v = new Vector();
        v.add("Document Title");
        v.add("Gist");
        if (bShowScore) {
            v.add("Score");
        }
        dtm.setColumnIdentifiers(v);

        dtm.setEditable(false);
        table.setModel(dtm);

        dtm.sortColumn(2, false);
    }

    public void saveNodeProps(DefaultMutableTreeNode item) {
        ITSTreeNode n = (ITSTreeNode) item.getUserObject();
        n.set("NODETITLE", textTitle.getText());
        n.set("NODEDESC", textDesc.getText());
        n.set("NODESIZE", textSize.getText());

        if (isBridgeNode.isSelected()) {
            n.set("BRIDGENODE", "1");
        } else {
            n.set("BRIDGENODE", "0");
        }
        int index = optNodeMustHaveGate.getSelectedIndex();
        if (index == 0) {
            //taxonomy musthave
            n.set("NODEMUSTHAVEGATEINHERITANCE", "null");
        } else if (index == 1) {
            // use user typed information MusthaveGate
            n.set("NODEMUSTHAVEGATEINHERITANCE", "0");
        } else {
            n.set("NODEMUSTHAVEGATEINHERITANCE", String.valueOf(index - 1));
        }


        if (listNodeType.getItemCount() > 0) {
            n.set("NODETYPE", objects.get(listNodeType.getSelectedIndex()).getId().toString());
            n.set("NODE_TYPE", objects.get(listNodeType.getSelectedIndex()).getId().toString());
        }

        if (textDesc.getText().length() > 3999) {
            JOptionPane
                    .showMessageDialog(
                    admin,
                    "Your request could not be completed.  "
                    + "The topic definition may not exceed 4000 characters.",
                    "Information", JOptionPane.NO_OPTION);
        }

        if (checkBoxLocked.isSelected()) {
            n.set("NODESTATUS", "2");
        } else if (checkBoxStatus.isSelected()) {
            n.set("NODESTATUS", "1");
        } else {
            n.set("NODESTATUS", "0");
        }

        try {
            its.editNode(n);
            item.setUserObject(n);

            if (n.get("PARENTID").equals("-1")) {
                tree.setSelectionRow(0);
            }
            tree.treeDidChange();
        } catch (TopicExistsError tee) {
            try {
                n = its.getNodeProps(n.get("NODEID"));
            } catch (Exception e) {
                JOptionPane
                        .showMessageDialog(
                        admin,
                        "Your request could not be completed.  "
                        + "Please contact your system administrator for more information.",
                        "Error", JOptionPane.NO_OPTION);
                return;
            }
            item.setUserObject(n);

            if (n.get("PARENTID").equals("-1")) {
                tree.setSelectionRow(0);
            }
            tree.treeDidChange();

            return;
        } catch (Exception e) {
            System.out.println("Could not edit this topic.");
            e.printStackTrace(System.out);
        }

        saveSignatures(tableSignatures, n);
        // saveMustHave(tableHave, n);
    }

    public void undoSignatures(ITSTreeNode n) throws Exception {
        Language lang = null;
        if (languageBox == null) {
            lang = new Language("English", "EN");
        } else {
            lang = (Language) languageBox.getSelectedItem();
        }

        its.saveSignatures(n.get("NODEID"), vUndoSignatures, lang.getCode());
    }

    public Vector getSignatures(JSortTable table) {
        Vector v = new Vector();
        for (int i = 0; i < table.getRowCount(); i++) {
            Object o = table.getValueAt(i, 0);
            String sWord = "";
            if (o instanceof Signature) {
                Signature s = (Signature) o;
                sWord = s.getWord();
            } else {
                sWord = o.toString();
            }

            Signature s = new Signature(sWord, 0);
            s.setWeight(((Integer) table.getValueAt(i, 1)).intValue());

            v.add(s);
        }

        return v;
    }

    public void saveSignatures(JSortTable table, ITSTreeNode n) {
        Language lang = null;
        if (languageBox == null) {
            lang = new Language("English", "EN");
        } else {
            lang = (Language) languageBox.getSelectedItem();
        }

        try {
            Vector v = new Vector();
            for (int i = 0; i < table.getRowCount(); i++) {
                Object o = table.getValueAt(i, 0);
                String sWord = "";
                if (o instanceof Signature) {
                    Signature s = (Signature) o;
                    sWord = s.getWord();
                } else {
                    sWord = (String) o;
                }

                Signature s = new Signature(sWord, 0);
                try {
                    int weight = ((Integer) table.getValueAt(i, 1)).intValue();
                    s.setWeight(weight);

                    // if (weight < 1) throw new
                    // Exception("Encountered negative signature count.");
                } catch (ClassCastException e) {
                    JOptionPane.showMessageDialog(admin,
                            "One or more signatures has a non integer value.  Please "
                            + "correct this error before continuing.",
                            "Error", JOptionPane.NO_OPTION);
                    return;
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(admin,
                            "One or more signatures has a value less than 1.  Please "
                            + "correct this error before continuing.",
                            "Error", JOptionPane.NO_OPTION);
                    return;
                }

                v.add(s);
            }

            if ((n.get("NODESIZE").equals("0"))
                    && (table.equals(tableSignatures))) {
                JOptionPane
                        .showMessageDialog(
                        admin,
                        "Warning: The size attribute of the topic "
                        + n.get("NODETITLE")
                        + " is currently set to 0."
                        + "Documents will not classify against topics with a size of 0 or less.",
                        "Warning", JOptionPane.NO_OPTION);
            }
            its.saveSignatures(n.get("NODEID"), v, true, lang.getCode());
        } catch (Exception e) {
            System.out
                    .println("Could not update the signatures for this topic.");
            e.printStackTrace(System.out);
        } finally {
            bSaveChanges = false;
        }
    }

    public void saveMustHave(JSortTable table, ITSTreeNode n) {
        Language lang = null;
        if (languageBox == null) {
            lang = new Language("English", "EN");
        } else {
            lang = (Language) languageBox.getSelectedItem();
        }

        try {
            Vector v = new Vector();
            for (int i = 0; i < table.getRowCount(); i++) {
                Object o = table.getValueAt(i, 0);
                v.add(o.toString());
            }

            its.removeMusthave(n.get("NODEID"), null, lang.getCode());
            its.addMusthave(n.get("NODEID"), v, false, lang.getCode());
        } catch (Exception e) {
            System.out
                    .println("Could not update the must have terms for this topic.");
            e.printStackTrace(System.out);
        } finally {
            bSaveChanges = false;
        }
    }

    public void outdentNode(DefaultMutableTreeNode item) throws Exception {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) item
                .getParent();
        DefaultMutableTreeNode newparent = (DefaultMutableTreeNode) parent
                .getParent();

        if (newparent != null) {
            ITSTreeNode nParent = (ITSTreeNode) parent.getUserObject();
            ITSTreeNode nNewParent = (ITSTreeNode) newparent.getUserObject();
            ITSTreeNode n = (ITSTreeNode) item.getUserObject();

            if (nNewParent.isLink()) {
                System.out
                        .println("Linked topics do not contain direct descendents.");
                JOptionPane.showMessageDialog(admin,
                        "Linked topics do not contain direct descendents.",
                        "Error", JOptionPane.NO_OPTION);
                return;
            }

            // note: the node is moved to the end, assign a NIWP equal to the
            // last
            // of the new siblings + 1
            DefaultMutableTreeNode lastnewsib = parent;
            while (lastnewsib.getNextSibling() != null) {
                lastnewsib = lastnewsib.getNextSibling();
            }
            int NIWP = new Integer(
                    ((ITSTreeNode) lastnewsib.getUserObject())
                    .get("NODEINDEXWITHINPARENT")).intValue();

            its.moveNode(n.get("LINKNODEID"), nNewParent.get("NODEID"));

            newparent.add(item); // parent.remove(item);
            int index = tree.getNodeIndex(parent);

            ((DefaultTreeModel) tree.getModel())
                    .nodeStructureChanged(newparent);

            TreePath tp = (TreePath) tree.getPathForRow(index);
            System.out.println("Expanding: " + tp);
            if (index != -1) {
                tree.expandRow(index);
            }
        }
    }

    public void indentNode(DefaultMutableTreeNode item) throws Exception {
        DefaultMutableTreeNode sibling = (DefaultMutableTreeNode) item
                .getPreviousSibling();
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) item
                .getParent();
        tree.refreshNode(sibling);

        if (sibling != null) {
            ITSTreeNode nSibling = (ITSTreeNode) sibling.getUserObject();
            ITSTreeNode nParent = (ITSTreeNode) parent.getUserObject();
            ITSTreeNode n = (ITSTreeNode) item.getUserObject();

            if (nSibling.isLink()) {
                System.out
                        .println("Linked topics do not contain direct descendents.");
                JOptionPane.showMessageDialog(admin,
                        "Linked topics do not contain direct descendents.",
                        "Error", JOptionPane.NO_OPTION);
                return;
            }

            its.moveNode(n.get("LINKNODEID"), nSibling.get("NODEID"));

            sibling.add(item); // parent.remove(item);
            int index = tree.getNodeIndex(sibling);

            ((DefaultTreeModel) tree.getModel()).nodeStructureChanged(parent);
            ((DefaultTreeModel) tree.getModel()).nodeStructureChanged(sibling);

            TreePath tp = (TreePath) tree.getPathForRow(index);
            if (index != -1) {
                tree.expandRow(index);
            }
        }
    }

    public void moveupNode(DefaultMutableTreeNode item) throws Exception {
        DefaultMutableTreeNode sibling = (DefaultMutableTreeNode) item
                .getPreviousSibling();
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) item
                .getParent();
        if (sibling != null) {
            ITSTreeNode nSibling = (ITSTreeNode) sibling.getUserObject();
            ITSTreeNode nParent = (ITSTreeNode) parent.getUserObject();
            ITSTreeNode n = (ITSTreeNode) item.getUserObject();

            if (nParent.isLink()) {
                System.out
                        .println("Linked topics do not contain direct descendents.");
                JOptionPane.showMessageDialog(admin,
                        "Linked topics do not contain direct descendents.",
                        "Error", JOptionPane.NO_OPTION);
                return;
            }

            if (!nParent.get("NODEID").equals("-1")) {
                its.organizeIndexes(nParent.get("NODEID"));
            }

            int iSiblingIndex = parent.getIndex(sibling) + 1;
            int iMyIndex = parent.getIndex(item) + 1;

            try {
                its.editNodeIndex(nSibling.get("LINKNODEID"), iMyIndex);
                its.editNodeIndex(n.get("LINKNODEID"), iSiblingIndex);
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
                throw ex;
            }

            tree.refreshNode((DefaultMutableTreeNode) item.getParent());
            item = (DefaultMutableTreeNode) parent
                    .getChildAt(iSiblingIndex - 1);

            if (iSiblingIndex > 0) {
                tree.setSelectionPath(new TreePath(item.getPath()));
            }
        }
    }

    public void movedownNode(DefaultMutableTreeNode item) throws Exception {
        DefaultMutableTreeNode sibling = (DefaultMutableTreeNode) item
                .getNextSibling();
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) item
                .getParent();
        if (sibling != null) {
            ITSTreeNode nSibling = (ITSTreeNode) sibling.getUserObject();
            ITSTreeNode nParent = (ITSTreeNode) parent.getUserObject();
            ITSTreeNode n = (ITSTreeNode) item.getUserObject();

            if (nParent.isLink()) {
                System.out
                        .println("Linked topics do not contain direct descendents.");
                JOptionPane.showMessageDialog(admin,
                        "Linked topics do not contain direct descendents.",
                        "Error", JOptionPane.NO_OPTION);
                return;
            }

            if (!nParent.get("NODEID").equals("-1")) {
                its.organizeIndexes(nParent.get("NODEID"));
            }

            int iSiblingIndex = parent.getIndex(sibling) + 1;
            int iMyIndex = parent.getIndex(item) + 1;

            try {
                its.editNodeIndex(nSibling.get("LINKNODEID"), iMyIndex);
                its.editNodeIndex(n.get("LINKNODEID"), iSiblingIndex);
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
                throw ex;
            }

            tree.refreshNode((DefaultMutableTreeNode) item.getParent());
            item = (DefaultMutableTreeNode) parent
                    .getChildAt(iSiblingIndex - 1);

            if (iSiblingIndex > 0) {
                tree.setSelectionPath(new TreePath(item.getPath()));
            }
        }
    }

    public void refreshSourcePane(ITSTreeNode n) {
        try {
            n = its.getNodeSource(n);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        nodeSourceArea.setText(n.get("NODESOURCE"));
    }

    public void refreshImagesPane(ITSTreeNode n) {
        ArrayList<FileMetadata> images = its.getImagesFromNode(n.get("NODEID"));
        panelImages.setFileMetaData(images);
    }

    public void refreshSignaturePane(ITSTreeNode n) {
        Vector vSignatures = new Vector();
        try {
            Language lang = (Language) languageBox.getSelectedItem();

            vSignatures = its.getNodeSignatures(n, lang.getCode());

            vUndoSignatures = vSignatures;
            loadSignatures(tableSignatures2, vSignatures, true);
        } catch (Exception e) {
            System.out
                    .println("Could not load signatures for this node from the database.");
            e.printStackTrace(System.out);
        }

        rebuildChart(vSignatures, n);
    }

    // rebuild the chart
    public void rebuildChart(Vector vSignatures, ITSTreeNode n) {
        // build chart
        DefaultKeyedValues dkv = new DefaultKeyedValues();
        for (int i = 0; i < vSignatures.size(); i++) {
            Signature s = (Signature) vSignatures.elementAt(i);
            dkv.addValue(s.getWord(), s.getFrequency(n));
        }
        dkv.sortByValues(SortOrder.DESCENDING);

        CategoryDataset ds = DatasetUtilities.createCategoryDataset(null, dkv);
        chart = ChartFactory.createBarChart("Signature Frequency", "Term",
                "Frequency", ds, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot categoryPlot = chart.getCategoryPlot();

        CategoryAxis ca = categoryPlot.getDomainAxis();
        ca.setVisible(false);

        BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);

        chartPanel.setChart(chart);
    }

    // rebuild the chart with something highlighted
    public void rebuildChart(Vector vSignatures, int selectedIndex) {
        Signature selectedSignature = null;

        // build chart
        DefaultKeyedValues dkv = new DefaultKeyedValues();
        for (int i = 0; i < vSignatures.size(); i++) {
            Signature s = (Signature) vSignatures.elementAt(i);
            dkv.addValue(s.getWord(), new Double(s.getWeight()));
            if (i == selectedIndex) {
                selectedSignature = s;
            }
        }
        dkv.sortByValues(SortOrder.DESCENDING);

        // if nothing is selected, nothing should be highlighted
        if (selectedSignature == null) {
            selectedSignature = new Signature("", 0);
        }

        for (int i = 0; i < dkv.getItemCount(); i++) {
            String s = (String) dkv.getKey(i);
            if (s.equals(selectedSignature.getWord())) {
                selectedIndex = i;
            }
        }

        CategoryDataset ds = DatasetUtilities.createCategoryDataset(null, dkv);
        chart = ChartFactory.createBarChart("Signature Frequency", "Term",
                "Frequency", ds, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot categoryPlot = chart.getCategoryPlot();

        CategoryAxis ca = categoryPlot.getDomainAxis();
        ca.setVisible(false);

        BarRenderer renderer2 = (BarRenderer) categoryPlot.getRenderer();
        SignatureRenderer renderer = new SignatureRenderer(selectedIndex);
        renderer.setLabelGenerator(renderer2.getSeriesLabelGenerator(0));
        renderer.setSeriesPaint(0, Color.BLUE);
        // renderer.setBaseItemLabelsVisible();
        categoryPlot.setRenderer(renderer);
        renderer.removeChangeListener(categoryPlot);

        chartPanel.setChart(chart);
    }

    public int supplementNode(ITSTreeNode n) throws Exception {
        return supplementNode(n, n.get("NODETITLE"));
    }

    public int supplementNode(ITSTreeNode n, String sNodeTitle)
            throws Exception {
        Hashtable ht = GoogleNews.getLinks(sNodeTitle);
        int hits = 0;

        if (ht.size() == 0) {
            return -1; // if no hits from google news were found, return -1
        }
        PopupProgressBar ppb = new PopupProgressBar(admin, "", 0, ht.size());
        ppb.show();

        // loop through each of these documents, and classify them against this
        // node only
        Enumeration eH = ht.keys();
        int loop = 0;
        while (eH.hasMoreElements()) {
            loop++;
            ppb.setProgress(loop);

            String url = (String) eH.nextElement();
            String title = (String) ht.get(url);
            File f = null;

            try {
                f = GoogleNews.getDataFile(url);
            } catch (Exception e) {
                System.out.println("Could not retrieve file: " + url);
            }

            // System.out.println("File retrieval process complete.");

            try {
                if (f != null) {
                    int results = ((Vector) its.classify(f, true, n, title))
                            .size();
                    if (results == 0) {
                        f.delete();
                    } else {
                        hits = hits + results;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        }

        ppb.dispose();

        // refresh the node
        if (tabPane.getSelectedIndex() == 1) { // get document relationships
            try {
                String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = "
                        + n.get("NODEID")
                        + " AND SCORE1 > 25 ORDER BY SCORE1 DESC";
                loadDocuments(tableDocuments, its.CQL(sCQL));
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
                return -2; // on error, return -2
            }
        }

        return hits;
    }

    // rebuild the chart using current selection model
    public void rebuildChart() {
        DefaultSortTableModel dstm = (DefaultSortTableModel) tableSignatures2
                .getModel();
        int row = tableSignatures2.getSelectedRow();

        Vector vSignatures = new Vector();
        for (int i = 0; i < tableSignatures2.getRowCount(); i++) {
            Object o = tableSignatures2.getValueAt(i, 0);
            String sWord = "";
            if (o instanceof Signature) {
                Signature s = (Signature) o;
                sWord = s.getWord();
            } else {
                sWord = (String) o;
            }

            Signature s = new Signature(sWord, 0);
            s.setWeight(((Integer) tableSignatures2.getValueAt(i, 1))
                    .intValue());

            vSignatures.add(s);
        }

        rebuildChart(vSignatures, row);
    }

    public void buildSignaturesForNode(ActionEvent e) {
        // JFileChooser chooser = new JFileChooser();
        // chooser.setMultiSelectionEnabled(true);
        // chooser.setFileFilter(new FilterSupportedTypes());

        // int option = chooser.showOpenDialog(this);
        // if (option == JFileChooser.APPROVE_OPTION) {
        // File[] sf = chooser.getSelectedFiles();
        // File f = sf[0];

        SwingWorker aWorker = new SwingWorker(e) {
            private boolean bSuccess = true;
            private String failMessage = "";

            protected void doNonUILogic() {
                // File tempFile = null;
                try {
                    buildSignaturesForNode(e);
                } catch (Exception ex) {
                    failMessage = ex.getMessage();
                    bSuccess = false;
                    ex.printStackTrace();
                }

                /*
                 * // if the file is of type text, it needs an HTML wrapper
                 * before being posted if
                 * (f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
                 * tempFile = new File("temp.html"); if (tempFile.exists()) {
                 * tempFile.delete(); } System.out.println("Filtering: " +
                 * f.getAbsolutePath() + " to " + tempFile.getAbsolutePath());
                 * 
                 * try { its.wrapHTML(f, tempFile);
                 * buildSignaturesForNode(tempFile, e); } catch (Exception ex) {
                 * failMessage = ex.getMessage(); bSuccess = false;
                 * ex.printStackTrace(); } finally { tempFile.delete(); } }
                 * 
                 * // if the file is of type PDF, filter it before posting else
                 * if (f.getAbsolutePath().toLowerCase().endsWith(".pdf")) { try
                 * { tempFile = ITS.PDFtoHTML(f);
                 * buildSignaturesForNode(tempFile, e); } catch (Exception ex) {
                 * ex.printStackTrace(System.out); failMessage =
                 * ex.getMessage(); bSuccess = false; } finally {
                 * tempFile.delete(); } } // if the file is of type WORD DOC,
                 * filter it before posting else if
                 * (f.getAbsolutePath().toLowerCase().endsWith(".doc")) { try {
                 * tempFile = ITS.MSWORDtoHTML(f);
                 * buildSignaturesForNode(tempFile, e); } catch (Exception ex) {
                 * ex.printStackTrace(System.out); failMessage =
                 * ex.getMessage(); bSuccess = false; } finally {
                 * tempFile.delete(); }
                 * 
                 * } else { try { buildSignaturesForNode(f, e); } catch
                 * (Exception ex) { failMessage = ex.getMessage(); bSuccess =
                 * false; ex.printStackTrace(System.out); } }
                 */
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                getGlassPane().setVisible(false);

                if (!bSuccess) {
                    JOptionPane.showMessageDialog(admin,
                            "Could not create concept signatures: "
                            + failMessage + ".", "Error",
                            JOptionPane.NO_OPTION);
                }
            }
        };
        aWorker.start();
        // }
    }

    public void buildSignaturesForNode(final File f, EventObject event)
            throws Exception {
        TreePath path = tree.getSelectionPath();
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path
                .getLastPathComponent();
        ITSTreeNode n = (ITSTreeNode) selectedNode.getUserObject();

        try {
            its.buildSignatures(f, n);
            n = its.getNodeProps(n.get("NODEID"));
            loadNodeProps(n);
            bSaveChanges = false;
            Language lang = (Language) languageBox.getSelectedItem();

            Vector vSignatures = its.getNodeSignatures(n, lang.getCode());
            loadSignatures(tableSignatures, vSignatures);
        } catch (Exception e) {
            throw e;
        }
    }

    public void buildSignaturesForNode(EventObject event) throws Exception {
        TreePath path = tree.getSelectionPath();
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path
                .getLastPathComponent();
        ITSTreeNode n = (ITSTreeNode) selectedNode.getUserObject();

        try {
            its.buildSignatures(n);
            n = its.getNodeProps(n.get("NODEID"));
            loadNodeProps(n);
            bSaveChanges = false;
            Language lang = (Language) languageBox.getSelectedItem();

            Vector vSignatures = its.getNodeSignatures(n, lang.getCode());

            loadSignatures(tableSignatures, vSignatures);
        } catch (Exception e) {
            throw e;
        }
    }

    public void mouseClicked(MouseEvent event) {
    }

    public void mouseReleased(MouseEvent event) {
    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }

    public void keyPressed(KeyEvent e) {
        if ((e.getSource().equals(tableSignatures))
                || (e.getSource().equals(tableSignatures2))) {
            return;
        }

        // if the tree is not in focus, do not do anything on a keyboard hit
        if (!tree.isFocusOwner()) {
            return;
        }

        if (tree.getSelectionCount() != 0) {

            // ADD NODE - INSERT
            if (((KeyEvent) e).getKeyCode() == KeyEvent.VK_INSERT) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        try {
                            DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                                    .getLastSelectedPathComponent();

                            ITSTreeNode parent = (ITSTreeNode) item
                                    .getUserObject();
                            ITSTreeNode node = its.emptyNode(parent);
                            int index = tree.getNodeIndex(item);

                            try {
                                its.addNode(node);
                                tree.refreshNode(item);
                                tree.expandNode(item);
                            } catch (TopicExistsError tee) {
                                return;
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                                return;
                            }
                            if (index != -1) {
                                tree.setSelectionRow(index);
                            }
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // ADD NODE (SIBLING)
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_N)
                    && (((KeyEvent) e).isControlDown())) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        try {
                            DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                                    .getLastSelectedPathComponent();

                            ITSTreeNode parent = (ITSTreeNode) ((DefaultMutableTreeNode) item
                                    .getParent()).getUserObject();
                            ITSTreeNode node = its.emptyNode(parent);
                            int index = tree.getNodeIndex(item);

                            try {
                                its.addNode(node);
                                tree.refreshNode((DefaultMutableTreeNode) item
                                        .getParent());
                                tree.expandNode(item.getParent());
                            } catch (TopicExistsError tee) {
                                return;
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                                return;
                            }
                            if (index != -1) {
                                tree.setSelectionRow(index);
                            }
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // DELETE NODE - DELETE
            if (((KeyEvent) e).getKeyCode() == KeyEvent.VK_DELETE) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        try {
                            DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                                    .getLastSelectedPathComponent();
                            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) item
                                    .getParent();

                            if (item == tree.getModel().getRoot()) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "Sorry, you may not delete the root topic of any taxonomy.",
                                        "Error", JOptionPane.NO_OPTION);

                                throw new IllegalStateException(
                                        "Tried to delete root node. ");
                            }
                            if ((item.getChildCount() > 0)
                                    && (ITS.getUser().PromptForTopicRemoval())) {
                                int i = JOptionPane
                                        .showConfirmDialog(
                                        admin,
                                        "Delete this topic and all children?  Are you sure?",
                                        "Confirm",
                                        JOptionPane.YES_NO_OPTION);
                                if (i != 0) {
                                    return;
                                }
                            } else if (ITS.getUser().PromptForTopicRemoval()) {
                                int i = JOptionPane.showConfirmDialog(null,
                                        "Permanently remove this topic? ",
                                        "Confirm", JOptionPane.YES_NO_OPTION);
                                if (i != 0) {
                                    return;
                                }
                            }

                            its.deleteNode((ITSTreeNode) item.getUserObject());
                            int index = tree.deleteSelectedNode();
                            tree.refreshNode(parent);
                            if (index != -1) {
                                tree.setSelectionRow(index);
                            } else {
                                System.out
                                        .println("did not retrieve correct index value");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                            return;
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // MOVE UP -- CTRL-UP ARROW
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_UP)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        try {
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                moveupNode(dmtn);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // MOVE DOWN -- CTRL-DOWN ARROW
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_DOWN)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        try {
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                movedownNode(dmtn);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // MOVE IN -- CTRL-RIGHT ARROW
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_RIGHT)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        try {
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                indentNode(dmtn);
                                tree.setSelectionPath(new TreePath(dmtn
                                        .getPath()));
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                            return;
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // MOVE OUT -- CTRL-LEFT ARROW
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_LEFT)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        try {
                            TreePath path[] = tree.getSelectionPaths();

                            for (int i = 0; i < path.length; i++) {
                                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) path[i]
                                        .getLastPathComponent();
                                outdentNode(dmtn);
                                tree.setSelectionPath(new TreePath(dmtn
                                        .getPath()));
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                            return;
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // CUT - CTRL-DOWN X
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_X)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        tree.cutSelectedNode();
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // COPY - CTRL-DOWN C
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_C)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        tree.copySelectedNode();
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // PASTE - CTRL-DOWN V
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_V)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        tree.pasteIntoSelectedNode(admin);
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }

            // SAVE - CTRL-DOWN S
            if ((((KeyEvent) e).getKeyCode() == KeyEvent.VK_S)
                    && (((KeyEvent) e).getModifiers() == 2)) {
                SwingWorker aWorker = new SwingWorker(e) {
                    protected void doNonUILogic() {
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        glass.setVisible(true);

                        DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                                .getLastSelectedPathComponent();
                        saveNodeProps(item);
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        glass.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        glass.setVisible(false);
                    }
                };
                aWorker.start();
            }
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(tableSignatures2)) {
            buttonApply.setEnabled(true);
            buttonUndo.setEnabled(true);
            return;
        }
        if ((!e.getSource().equals(tree))
                && (!e.getSource().equals(tableSignatures))) {
            bSaveChanges = true;
        }
    }

    public void tableChanged(TableModelEvent e) {
        rebuildChart();
    }

    public void performAction(ActionEvent event) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addNodeMustHaveGate) {
            TreePath path = tree.getSelectionPath();
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
            ITSTreeNode n = (ITSTreeNode) selectedNode.getUserObject();

            NodeMustHaveGate nodeMhaveG = new NodeMustHaveGate(admin, CorpusID, n.get("NODEID"));
            nodeMhaveG.setVisible(true);
        }

        if (e.getSource() == buttonAddHaveNodeGate) {
        }

        if (e.getSource() == buttonRemoveHaveNodeGate) {
        }

        if (e.getSource() == buttonEditHaveNodeGate) {
        }

        if (e.getSource() == buttonRemoveAllHaveNodeGate) {
        }

        if (e.getSource().equals(languageBox)) {
            // language has changed, reset the must haves and the signatures
            DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                    .getLastSelectedPathComponent();

            ITSTreeNode n = (ITSTreeNode) item.getUserObject();
            loadNodeProps(n);
        }

        // first piece of the event logic is to create signatures
        if (e.getSource().equals(buttonCreate)) {
            buildSignaturesForNode(e);
            return;
        }
        if (e.getSource().equals(btnNodeProp)) {
            DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                    .getLastSelectedPathComponent();
            ITSTreeNode n = (ITSTreeNode) item.getUserObject();
            if (n != null) {
                new NodeProp(admin, n, its);
            } else {
                JOptionPane.showMessageDialog(btnNodeProp, "Select one node!");
            }
        }
        if (e.getSource().equals(buttonRemoveSigAll)) {
            DefaultSortTableModel dstm = (DefaultSortTableModel) tableSignatures
                    .getModel();

            int num = dstm.getRowCount();
            for (int i = 0; i < num; i++) {
                dstm.removeRow(0);
            }

            tableSignatures.revalidate();
            bSaveChanges = true;
        }

        if (e.getSource().equals(checkBoxLocked)) {
            tableSignatures.setEnabled(!checkBoxLocked.isSelected());
            bSaveChanges = true;
            // tableSignatures.setVisible(!checkBoxLocked.isSelected());
        }

        // treeField represents the internal JTextField object assigned for each
        // element in the tree
        if (e.getSource().equals(treeField)) {
            TreePath path = tree.getSelectionPath();
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path
                    .getLastPathComponent();

            if (treeField.node == null) {
                ITSTreeNode n = (ITSTreeNode) selectedNode.getUserObject();

                n.set("NODETITLE", treeField.getText());
                treeField.node = n;
            } else {
                // save node
                try {
                    its.editNodeTitle(treeField.node);
                } catch (TopicExistsError tee) {
                    ITSTreeNode n = treeField.node;
                    try {
                        n = its.getNodeProps(n.get("NODEID"));
                    } catch (Exception ex) {
                        JOptionPane
                                .showMessageDialog(
                                admin,
                                "Your request could not be completed.  "
                                + "Please contact your system administrator for more information.",
                                "Error", JOptionPane.NO_OPTION);
                        return;
                    }
                    selectedNode.setUserObject(n);
                    treeField.node = null;

                    try {
                        tree.setSelectionPath(new TreePath(selectedNode
                                .getNextNode().getPath()));
                    } catch (Exception ex) {
                    } // the last child of a parent will throw an exception -
                    // that's okay

                    textTitle.setText(n.get("NODETITLE"));
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                    JOptionPane.showMessageDialog(admin,
                            "Error: That topic title could not be changed.",
                            "Error", JOptionPane.NO_OPTION);

                    return;
                }

                selectedNode.setUserObject(treeField.node);
                treeField.node = null;

                try {
                    tree.setSelectionPath(new TreePath(selectedNode
                            .getNextNode().getPath()));
                } catch (Exception ex) {
                } // the last child of a parent will throw an exception - that's
                // okay
            }
            textTitle.setText(treeField.getText());

        }

        // the rest of the event logic is here
        SwingWorker aWorker = new SwingWorker(e) {
            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    TreePath path = tree.getSelectionPath();
                    DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path
                            .getLastPathComponent();
                    ITSTreeNode n = (ITSTreeNode) selectedNode.getUserObject();
                    if (e.getSource().equals(menuGoogle)) {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("corpusid", CorpusID);
                        map.put("nodeid", n.get("NODEID"));
                        GoogleCrawlUI ga = new GoogleCrawlUI(admin);
                        ga.showWin();
                        if (ga.getResponse()) {
                            return;
                        }
                        map.putAll(ga.getMap());
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Google crawl", 0, 100);
                        ppb.setVisible(true);
                        try {
                            GoogleInfoBean gib = its
                                    .consultFromGoogle(map, ppb);
                            gib.setFilter(map.get("customsearch"));
                            if (gib != null) {
                                new LogGoogleCrawlUI(gib, admin).showWin();
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "Error processing request");
                            }
                        } catch (Exception ex) {
                            log.error("Error in google crawl");
                        } finally {
                            ppb.dispose();
                        }

                        /*
                         * System.out.println(corp.getName());
                         * System.out.println(n.get("NODETITLE"));
                         * System.out.println
                         * (its.getCorpusRoot(corp.getID()).get( "NODEID"));
                         * 
                         * its.consultFromGoogle(its.getCorpusRoot(corp.getID())
                         * .get("NODEID"), corp.getID(), corp.getName(), n
                         * .get("NODEID"), n.get("NODETITLE"));
                         */

                    }

                    // the regex only is valid to content in group 1
                    if (e.getSource().equals(menuFillMusthaves)) {
                        /*
                         * PopupProgressBar ppb = new PopupProgressBar(admin, 0,
                         * 100); try { ppb.setVisible(true); Vector vNodes =
                         * its.getAllNodesFromNodeID( corp.getID(),
                         * n.get("NODEID")); if (vNodes.size() < 1) { return; }
                         * 
                         * String source = ""; String pathRegex = ""; String
                         * sRegex = ""; File fileRegex = null; FileReader fr =
                         * null; BufferedReader br = null; String line = "";
                         * ArrayList<String> params = null; ArrayList<String>
                         * vRegex;
                         * 
                         * pathRegex = ConfClient.getValue("PATH_REGEX");
                         * fileRegex = new File(pathRegex); fr = new
                         * FileReader(fileRegex); br = new BufferedReader(fr);
                         * params = new ArrayList<String>(); vRegex = new
                         * ArrayList<String>(); line = ""; try { while ((line =
                         * br.readLine()) != null) { vRegex.add(line); } } catch
                         * (Exception e) {
                         * log.error("CorpusManagement: menuFillMusthaves"); }
                         * finally { if (fr != null) { fr.close(); } if (br !=
                         * null) { br.close(); } } int num = vNodes.size(); int
                         * percent = 0; int i = 0;
                         * 
                         * for (int loop = 0; loop < vNodes.size(); loop++) {
                         * ITSTreeNode loopNode = (ITSTreeNode) vNodes
                         * .elementAt(loop); source =
                         * its.getNodeSourceAsString(loopNode); for (String s :
                         * vRegex) { params.clear();
                         * params.add(loopNode.get("NODETITLE")); sRegex =
                         * getRegexWithParams(params, s); Pattern p =
                         * Pattern.compile(sRegex, Pattern.MULTILINE |
                         * Pattern.CASE_INSENSITIVE);
                         * 
                         * Matcher m = p.matcher(source); while (m.find()) {
                         * String termMustHave = ""; termMustHave =
                         * postProccessRegex(m .group(1)); if (termMustHave !=
                         * null) { its.addMusthave( loopNode.get("NODEID"),
                         * termMustHave); }
                         * 
                         * } } i++; percent = (i * 100) / num;
                         * ppb.setProgress(percent); } } finally {
                         * ppb.dispose(); JOptionPane.showMessageDialog(null,
                         * "Process finished"); }
                         */

                        // call api function
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Insert MustHaves", 0, 100);
                        ppb.setVisible(true);
                        try {
                            its.populateMustHavesFromSource(n.get("NODEID"),
                                    ppb);
                        } catch (Exception e) {
                        } finally {
                            ppb.dispose();
                        }

                    }
                    if (e.getSource().equals(menuFillSignatures)) {
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Insert Signatures", 0, 100);
                        ppb.setVisible(true);
                        try {
                            its.populateSignatures(n.get("NODEID"), ppb);
                            Vector vSignatures = new Vector();
                            try {
                                Language lang = (Language) languageBox
                                        .getSelectedItem();
                                vSignatures = its.getNodeSignatures(n,
                                        lang.getCode());
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                            loadSignatures(tableSignatures, vSignatures);
                        } catch (Exception e) {
                        } finally {
                            ppb.dispose();
                        }
                        /*
                         * if (true) { JOptionPane.showMessageDialog(null,
                         * "is not ready, pass this function to the server");
                         * return; } String source = ""; String linkedSection =
                         * ""; String wordsLinkedSection[] = null; Vector
                         * wordsSignature = new Vector(); StringBuilder str =
                         * new StringBuilder(); HashMap<String, Double>
                         * mapSignatures = new HashMap<String, Double>();
                         * 
                         * Vector vNodes =
                         * its.getAllNodesFromNodeID(corp.getID(),
                         * n.get("NODEID")); if (vNodes.size() < 1) {
                         * System.out.println("no selected nodes"); return; }
                         * 
                         * for (int loop = 0; loop < vNodes.size(); loop++) {
                         * source = ""; linkedSection = ""; wordsLinkedSection =
                         * null; wordsSignature = null; mapSignatures.clear();
                         * 
                         * ITSTreeNode loopNode = (ITSTreeNode) vNodes
                         * .elementAt(loop); source =
                         * its.getNodeSourceAsString(loopNode); wordsSignature =
                         * its.getNodeSignatures(loopNode); if
                         * (source.contains("<LinkedSection>")) { linkedSection
                         * = source.substring( source.indexOf("<LinkedSection>")
                         * + 15, source.indexOf("</LinkedSection>"));
                         * wordsLinkedSection = linkedSection.trim()
                         * .split("\\|");
                         * 
                         * } if (wordsLinkedSection != null) { Double numWord =
                         * new Double(0); for (int i = 0; i <
                         * wordsLinkedSection.length; i++) { numWord = 0.0; for
                         * (int k = 0; k < wordsLinkedSection.length; k++) { if
                         * (wordsLinkedSection[i]
                         * .equalsIgnoreCase(wordsLinkedSection[k])) {
                         * numWord++; } } mapSignatures.put(
                         * wordsLinkedSection[i].trim(), numWord); } } for (int
                         * i = 0; i < wordsSignature.size(); i++) { Signature s
                         * = (Signature) wordsSignature .elementAt(i); double
                         * numWordSignature = s.getWeight(); if
                         * (mapSignatures.get(s.toString()) != null) {
                         * numWordSignature += mapSignatures.get(s .toString());
                         * mapSignatures.put(s.toString(), numWordSignature); }
                         * else { mapSignatures.put(s.toString(),
                         * numWordSignature); } } Iterator<String> it =
                         * mapSignatures.keySet() .iterator(); while
                         * (it.hasNext()) { String e = it.next(); Double value =
                         * mapSignatures.get(e); str.append(e + "||" + value +
                         * ","); } String sstr = str.toString(); int index =
                         * str.lastIndexOf(","); if (index > -1) { sstr =
                         * sstr.substring(0, index); } System.out.println(sstr);
                         * its.saveSignaturesLinkedSection(
                         * loopNode.get("NODEID"), sstr);
                         * 
                         * }
                         */
                    }
                    if (e.getSource().equals(menuImportImages)) {
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Import images from BING", 0, 100);
                        ppb.setVisible(true);
                        try {
                            BingInfoBean bib = its.addImagesNodeRecursive(
                                    corp.getID(), n.get("NODEID"), ppb);
                            if (bib != null) {
                                new LogTermsFailBing(bib, admin).showWin();
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "Error processing request");
                            }
                            refreshImagesPane(n);
                        } catch (Exception e) {
                        } finally {
                            ppb.dispose();
                        }

                    }
                    if (e.getSource().equals(menuDeleteMustHaves)) {
                        try {
                            int response = JOptionPane.showOptionDialog(null,
                                    "Are you sure (Delete all musthaves)?",
                                    "Question", JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null,
                                    new Object[]{"Yes", "No"}, "Yes");
                            if (response != -1) {
                                if (response == 0) {
                                    its.deleteMH(n.get("NODEID"));
                                } else {
                                    return;
                                }
                            }
                        } catch (Exception e) {
                            Log.debug("Process to delete musthaves error");
                        }
                    }
                    if (e.getSource().equals(menuDeleteImages)) {
                        try {
                            int response = JOptionPane.showOptionDialog(null,
                                    "Are you sure (Delete all images)?",
                                    "Question", JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null,
                                    new Object[]{"Yes", "No"}, "Yes");
                            if (response != -1) {
                                if (response == 0) {
                                    its.deleteImages(n.get("NODEID"));
                                } else {
                                    return;
                                }
                            }
                        } catch (Exception e) {
                            Log.debug("Process to delete images error");
                        }
                    }
                    if (e.getSource().equals(menuDeleteSignatures)) {
                        try {
                            Vector vSignatures = new Vector();
                            int response = JOptionPane.showOptionDialog(null,
                                    "Are you sure (Delete all signatures)?",
                                    "Question", JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null,
                                    new Object[]{"Yes", "No"}, "Yes");
                            if (response != -1) {
                                if (response == 0) {
                                    its.deleteSignatures(n.get("NODEID"));
                                    Language lang = (Language) languageBox
                                            .getSelectedItem();
                                    vSignatures = its.getNodeSignatures(n,
                                            lang.getCode());
                                    loadSignatures(tableSignatures, vSignatures);
                                } else {
                                    return;
                                }
                            }

                        } catch (Exception e) {
                            Log.debug("Process to delete signatute error");
                        }
                    }
                    if (e.getSource().equals(menuPopulateDescription)) {
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Populate Desc", 0, 100);
                        ppb.setVisible(true);
                        try {
                            its.addNodeDesc(n.get("NODEID"), ppb);
                        } catch (Exception e) {
                        } finally {
                            ppb.dispose();
                        }
                    }
                    if (e.getSource().equals(menuShow)) { // show link location
                        // (if link node)
                        JDesktopPane jdesktoppane = admin.jdp;
                        CorpusManagement DragAndDrop = null;
                        Corpus c = its.getCorpora(n.get("CORPUSID"));
                        try {
                            DragAndDrop = new CorpusManagement(admin, c,
                                    n.get("NODEID"));
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(
                                    admin,
                                    "Could not create corpus management window corpus ["
                                    + c.getID() + "] corpus ["
                                    + c.getName() + "] nodetitle ["
                                    + n.get("NODETITLE") + "]",
                                    "Information", JOptionPane.NO_OPTION);
                            System.out
                                    .print("Could not create corpus management window corpus ["
                                    + c.getID()
                                    + "] corpus ["
                                    + c.getName()
                                    + "] nodetitle ["
                                    + n.get("NODETITLE") + "]");
                            ex.printStackTrace(System.out);
                            return;
                        }

                        DragAndDrop.setBounds(50, 50, 850, 550);
                        DragAndDrop.setVisible(true);
                        DragAndDrop
                                .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                        jdesktoppane.add(DragAndDrop);
                        DragAndDrop.moveToFront();

                        jdesktoppane.setVisible(true);
                    }
                    if ((e.getSource().equals(openButton))
                            || (e.getSource().equals(menuOpen))) { // open
                        // document
                        viewDocument(tableDocuments);
                    }
                    if (e.getSource().equals(openButton2)) {
                        viewDocument(tableDocuments2);
                    }
                    if (e.getSource().equals(chartButton)) { // open chart
                        if (tableDocuments.getRowCount() == 0) {
                            return;
                        }

                        Vector v = new Vector();
                        for (int i = 0; i < tableDocuments.getRowCount(); i++) {
                            try {
                                v.add((com.iw.system.Document) tableDocuments
                                        .getModel().getValueAt(i, 0));
                            } catch (ClassCastException e) {
                            }
                        }
                        DocumentChart dc = new DocumentChart(admin, v);
                        admin.jdp.add(dc);
                        dc.setBounds(10, 30, 900, 580);
                        dc.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        dc.setVisible(true);
                        dc.moveToFront();
                        dc.show();
                    }
                    if (e.getSource().equals(chartButton2)) { // open chart
                        if (tableDocuments2.getRowCount() == 0) {
                            return;
                        }

                        Vector v = new Vector();
                        for (int i = 0; i < tableDocuments2.getRowCount(); i++) {
                            try {
                                v.add((com.iw.system.Document) tableDocuments2
                                        .getModel().getValueAt(i, 0));
                            } catch (ClassCastException e) {
                            }
                        }
                        DocumentChart dc = new DocumentChart(admin, v);
                        admin.jdp.add(dc);
                        dc.setBounds(10, 30, 900, 580);
                        dc.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        dc.setVisible(true);
                        dc.moveToFront();
                        dc.show();
                    }
                    if ((e.getSource().equals(removeNDButton))
                            || (e.getSource().equals(menuRemove))) { // remove
                        // node
                        // document
                        // relationship
                        int row = tableDocuments.getSelectedRow();
                        if (row >= 0) {
                            com.iw.system.Document d = (com.iw.system.Document) tableDocuments
                                    .getModel().getValueAt(row, 0);

                            try {
                                its.deleteNodeDocument(d.get("DOCUMENTID"),
                                        n.get("NODEID"));
                                DefaultSortTableModel dstm = (DefaultSortTableModel) tableDocuments
                                        .getModel();
                                dstm.removeRow(row);
                                tableDocuments.revalidate();
                            } catch (Exception e) {
                                System.out
                                        .println("An API level error occurred while attempting to remove this relationship.");
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                    if ((e.getSource().equals(relatedButton))
                            || (e.getSource().equals(menuRelated))) { // show
                        // related
                        // documents
                        int row = tableDocuments.getSelectedRow();
                        if (row >= 0) {
                            com.iw.system.Document d = (com.iw.system.Document) tableDocuments
                                    .getModel().getValueAt(row, 0);
                            try {
                                NodeDocView ndv = new NodeDocView(admin, d,
                                        null);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                    if (e.getSource().equals(relatedButton2)) { // show related
                        // documents
                        int row = tableDocuments2.getSelectedRow();
                        if (row >= 0) {
                            com.iw.system.Document d = (com.iw.system.Document) tableDocuments2
                                    .getModel().getValueAt(row, 0);
                            try {
                                NodeDocView ndv = new NodeDocView(admin, d,
                                        null);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    }

                    if ((e.getSource().equals(menuProperties))
                            || (e.getSource().equals(propertiesButton))) { // get
                        // document
                        // properties
                        int row = tableDocuments.getSelectedRow();
                        if (row >= 0) {
                            com.iw.system.Document d = (com.iw.system.Document) tableDocuments
                                    .getModel().getValueAt(row, 0);
                            DocumentProperties dp = new DocumentProperties(
                                    admin, d);

                            ComponentListener cl = new ComponentListener() {
                                public void componentResized(ComponentEvent e) {
                                    System.out.println("resized");
                                }

                                public void componentMoved(ComponentEvent e) {
                                    System.out.println("moved");
                                }

                                public void componentShown(ComponentEvent e) {
                                    System.out.println("shown");
                                }

                                public void componentHidden(ComponentEvent e) {
                                    System.out.println("hidden");
                                }
                            };

                            dp.addComponentListener(cl);
                            dp.show();
                        }
                    }

                    if ((e.getSource().equals(documentButton))
                            || (e.getSource().equals(menuClassify))) { // classify
                        // document
                        int row = tableDocuments.getSelectedRow();
                        if (row >= 0) {
                            com.iw.system.Document d = (com.iw.system.Document) tableDocuments
                                    .getModel().getValueAt(row, 0);
                            try {
                                NodeDocView ndv = new NodeDocView(admin, d,
                                        ITS.getUser());
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                    if ((e.getSource().equals(explainButton))
                            || (e.getSource().equals(menuExplain))) { // explain
                        // document
                        invokeExplain(n);
                    }
                    if (e.getSource().equals(documentButton2)) { // classify
                        // document
                        int row = tableDocuments2.getSelectedRow();
                        if (row >= 0) {
                            com.iw.system.Document d = (com.iw.system.Document) tableDocuments2
                                    .getModel().getValueAt(row, 0);
                            try {
                                NodeDocView ndv = new NodeDocView(admin, d,
                                        ITS.getUser());
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                    // source pane
                    if (e.getSource().equals(nodeSourceSaveButton)) {
                        its.setNodeSource(n, nodeSourceArea.getText());

                        // image pane
                    } else if (e.getSource().equals(uploadImageFileSystem)) {

                        ArrayList<FileMetadata> listFile = new ArrayList<FileMetadata>();
                        StringBuilder buffer = new StringBuilder();
                        byte data[] = new byte[1024];
                        int b = 0;
                        FileMetadata fileMeta = new FileMetadata();
                        JFileChooser fileChooser = new JFileChooser();
                        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                                "Images(jpg,gif,png)", "jpg", "gif", "png");
                        fileChooser.setFileFilter(filter);
                        int select = fileChooser.showOpenDialog(null);
                        if (select == JFileChooser.APPROVE_OPTION) {
                            File file = fileChooser.getSelectedFile();
                            System.out.println(file.getAbsolutePath());
                            FileInputStream fis = new FileInputStream(file);

                            ByteArrayOutputStream bao = new ByteArrayOutputStream();
                            while ((b = fis.read(data, 0, 1024)) > 0) {
                                bao.write(data, 0, b);
                            }
                            fileMeta.setNodeId(n.get("NODEID"));
                            fileMeta.setDescription(filterString(file.getName()));
                            fileMeta.setUri(filterString("file://"
                                    + file.getPath()));
                            fileMeta.setImage(bao.toByteArray());
                            // change setId per a value int to send more images
                            fileMeta.setId(String.valueOf(1));
                            listFile.add(fileMeta);
                            buffer.append(fileMeta.getId());
                            buffer.append(";");
                            buffer.append(fileMeta.getDescription());

                            buffer.append(";");
                            buffer.append(fileMeta.getNodeId());
                            buffer.append(";");
                            buffer.append(fileMeta.getUri());
                            buffer.append("!");
                            bao.close();

                            String strBuffer = buffer.toString();
                            if (strBuffer.endsWith("!")) {
                                int last = strBuffer.lastIndexOf('!');
                                strBuffer = strBuffer.substring(0, last);
                            }
                            System.out.println(strBuffer.toString());
                            its.addImageLocalNode(listFile,
                                    strBuffer.getBytes());
                            refreshImagesPane(n);
                        }
                        // image pane

                    } else if (e.getSource().equals(deleteImage)) {
                        if (panelImages.getCurrentImageID() != null) {
                            its.deleteImageNode(panelImages.getCurrentImageID());
                            refreshImagesPane(n);
                        } else {
                            JOptionPane.showMessageDialog(null,
                                    "Select a image");
                        }

                    } else if (e.getSource().equals(uploadImageUrl)) {
                        ExplorerUrl eUrl = new ExplorerUrl();
                        if (!eUrl.cancelAction) {
                            try {
                                its.addImageURLNode(n, eUrl.getUrl());
                                refreshImagesPane(n);
                            } catch (Exception e) {
                                System.out.println("ERROR");
                            }
                        }

                    }
                    if (e.getSource().equals(refreshImage)) {
                        refreshImagesPane(n);
                    }
                    if (e.getSource().equals(btnImagesS3)) {
                        boolean ban;
                        int response = JOptionPane
                                .showOptionDialog(
                                admin,
                                "Do you want save images recursively from this node?",
                                "Question",
                                JOptionPane.YES_NO_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null,
                                new Object[]{"Yes", "No", "Cancel"},
                                "No");
                        if (JOptionPane.OK_OPTION == response) {
                            ban = true;
                        } else if (JOptionPane.NO_OPTION == response) {
                            ban = false;
                        } else {
                            return;
                        }
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Storage in S3", 0, 100);
                        String val = "";
                        try {
                            ppb.setVisible(true);
                            val = its.saveNodeImagesS3(n.get("CORPUSID"), n.get("NODEID"), ban,
                                    ppb);

                        } catch (Exception e) {
                            Log.error("Error saving images in S3");
                        } finally {
                            ppb.dispose();
                        }
                        JOptionPane.showMessageDialog(admin, val);
                    }
                    if (e.getSource().equals(CreateSigFromSelection)) {
                        Vector v = new Vector();
                        Signature s = new Signature(
                                nodeSourceArea.getSelectedText()
                                .replaceAll("\n", " ").trim(), 1);
                        v.add(s);

                        its.saveSignatures(n, v, false);
                    }

                    if (e.getSource().equals(CreateSigFromSelectionMulti)) {
                        Vector v = new Vector();

                        String[] sArr = nodeSourceArea.getSelectedText().split(
                                "\n");

                        for (int i = 0; i < sArr.length; i++) {
                            if (sArr[i].trim() != "") {
                                Signature s = new Signature(sArr[i].replaceAll(
                                        "\n", " ").trim(), 1);
                                v.add(s);

                            }
                        }
                        its.saveSignatures(n, v, false);
                    }

                    if (e.getSource().equals(CreateMustHaveFromSelectionMulti)) {
                        Vector v = new Vector();

                        String[] sArr = nodeSourceArea.getSelectedText().split(
                                "\n");

                        for (int i = 0; i < sArr.length; i++) {
                            if (sArr[i].trim() != "") {
                                its.addMusthave(n.get("NODEID"), sArr[i]
                                        .replaceAll("\n", " ").trim(), false);
                            }
                        }
                    }

                    if (e.getSource().equals(fixSourceWiki)) {
                        String ban = "no";
                        int response = JOptionPane
                                .showOptionDialog(
                                admin,
                                "Do you want populate the source recursively from wikipedia for this node?",
                                "Question",
                                JOptionPane.YES_NO_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null,
                                new Object[]{"Yes", "No", "Cancel"},
                                "No");
                        if (JOptionPane.OK_OPTION == response) {
                            ban = "yes";
                        } else if (JOptionPane.NO_OPTION == response) {
                            ban = "no";
                        } else {
                            return;
                        }
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Populating source", 0, 100);
                        List<String> val = new ArrayList<String>();
                        try {
                            ppb.setVisible(true);
                            val = its.fixSource(n.get("NODEID"), ban, ppb);
                        } catch (Exception e) {
                            Log.error("Error in the process", e);
                        } finally {
                            ppb.dispose();
                        }
                        //parse the list and show result
                        System.out.println("TERMS WITH ERROR");
                        for (String term : val) {
                            System.out.println(term);
                        }
                        JOptionPane.showMessageDialog(admin, "See the result in the console log");

                    }
                    if (e.getSource().equals(fixdescBritannica)) {
                        String ban = "no";
                        int response = JOptionPane
                                .showOptionDialog(
                                admin,
                                "Do you want fix the description recursively from this node?",
                                "Question",
                                JOptionPane.YES_NO_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null,
                                new Object[]{"Yes", "No", "Cancel"},
                                "No");
                        if (JOptionPane.OK_OPTION == response) {
                            ban = "yes";
                        } else if (JOptionPane.NO_OPTION == response) {
                            ban = "no";
                        } else {
                            return;
                        }
                        PopupProgressBar ppb = new PopupProgressBar(admin,
                                "Fix the Description", 0, 100);
                        List<String> val = new ArrayList<String>();
                        try {
                            ppb.setVisible(true);
                            val = its.fixDescBritannica(n.get("NODEID"), ban, ppb);
                        } catch (Exception e) {
                            Log.error("Error in the process", e);
                        } finally {
                            ppb.dispose();
                        }
                        //parse the list and show result
                        System.out.println("TERMS WITH ERROR");
                        for (String term : val) {
                            System.out.println(term);
                        }
                        JOptionPane.showMessageDialog(admin, "See the result in the log");

                    }

                    if (e.getSource().equals(CreateMustHaveFromSelection)) {
                        Vector v = new Vector();
                        its.addMusthave(
                                n.get("NODEID"),
                                nodeSourceArea.getSelectedText()
                                .replaceAll("\n", " ").trim(), false);
                    }

                    if (e.getSource().equals(menuCut)) { // cut node
                        tree.cutSelectedNode();
                    }
                    if (e.getSource().equals(menuOal)) { // open all recursive
                        openAll(selectedNode);
                        ((DefaultTreeModel) tree.getModel())
                                .nodeStructureChanged(selectedNode);
                        for (int i = 0; i < tree.getRowCount(); i++) {
                            tree.expandRow(i);
                        }

                    }
                    if (e.getSource().equals(menuCop)) { // copy node
                        tree.copySelectedNode();
                    }
                    if (e.getSource().equals(menuPas)) { // PASTE TOPIC
                        tree.pasteIntoSelectedNode(admin);
                    }
                    if (e.getSource().equals(menuPal)) { // PASTE TOPIC
                        tree.pasteIntoSelectedNode(admin, true);
                    }
                    if (e.getSource().equals(menuAlp)) { // alphabetize topic
                        // children
                        try {
                            tree.alphabetizeNode();
                        } catch (Exception e) {
                            System.out
                                    .println("Could not alphabetize this node.");
                        }
                    }
                    if (e.getSource().equals(menuAlR)) { // alphabetize topic
                        // children
                        // recursively
                        try {
                            tree.alphabetizeNodeRecursively();
                        } catch (Exception e) {
                            System.out
                                    .println("Could not alphabetize this node.");
                        }
                    }
                    if (e.getSource().equals(menuCap)) { // fix capitalization
                        // of this node
                        try {
                            tree.capitalizeNode();
                        } catch (Exception e) {
                            System.out
                                    .println("Could not change case of this node.");
                        }
                    }
                    if (e.getSource().equals(menuCaR)) { // fix capitalization
                        // of this node
                        // recursively
                        try {
                            tree.capitalizeNodeRecursively();
                        } catch (Exception e) {
                            System.out
                                    .println("Could not change case of this node.");
                        }
                    }
                    if (e.getSource().equals(menuStrip)) { // fix capitalization
                        // of this node
                        try {
                            tree.stripNode();
                        } catch (Exception e) {
                            System.out
                                    .println("Could not strip leading numbers from this node.");
                        }
                    }
                    if (e.getSource().equals(menuStripWS)) { // remove
                        // leading/trailing
                        // whitespace
                        try {
                            tree.stripNodeWhitespace();
                        } catch (Exception e) {
                            System.out
                                    .println("Could not strip leading numbers from this node.");
                        }
                    }
                    /**
                     * (1) make terms in parentheses at end of topic name into
                     * must have and remove from topic for example: original
                     * topic = American Institute of Architects (AIA) new topic
                     * = American Institute of Architects new must have = AIA
                     *
                     */
                    if (e.getSource().equals(menuParaMust)) { // lop off parens
                        // and write to
                        // must have
                        try {
                            tree.stripParensAddMustHaveRecurse(selectedNode, n);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(admin,
                                    e.getMessage(), "Error",
                                    JOptionPane.NO_OPTION);
                        }
                    }
                    /**
                     * (2) separate topic name at commas, making all, except
                     * first, as must haves for example: original topic =
                     * asbestos-cement board, asbestos-cement wallboard,
                     * asbestos sheeting new topic = asbestos-cement board new
                     * must haves = asbestos-cement wallboard & asbestos
                     * sheeting
                     *
                     */
                    if (e.getSource().equals(menuComaMust)) {
                        try {
                            tree.stripCommasAddMustHaveRecurse(selectedNode, n);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(admin,
                                    e.getMessage(), "Error",
                                    JOptionPane.NO_OPTION);
                        }

                    }
                    if (e.getSource().equals(menuSR)) {
                        try {
                            SearchReplace dialog = new SearchReplace(n, admin);
                            dialog.setVisible(true);
                            dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                            dialog.addWindowListener(new WindowAdapter() {
                                public void windowClosing(WindowEvent we) {
                                    // do nothing
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (e.getSource().equals(menuStriR)) { // fix capitalization
                        // of this node
                        // recursively
                        try {
                            tree.stripNodeRecursively();
                        } catch (Exception e) {
                            System.out
                                    .println("Could not strip leading numbers from this node.");
                        }
                    }
                    if (e.getSource().equals(menulockR)) { // fix capitalization
                        // of this node
                        // recursively
                        try {
                            JOptionPane.showMessageDialog(admin,
                                    tree.editNodeStatusRecursively(2),
                                    "Information", JOptionPane.NO_OPTION);
                        } catch (Exception e) {
                            System.out
                                    .println("Could not alter the status of this node.");
                        }
                    }
                    if (e.getSource().equals(menuunlockR)) { // fix
                        // capitalization
                        // of this node
                        // recursively
                        try {
                            JOptionPane.showMessageDialog(admin,
                                    tree.editNodeStatusRecursively(1),
                                    "Information", JOptionPane.NO_OPTION);
                        } catch (Exception e) {
                            System.out
                                    .println("Could not alter the status of this node.");
                        }
                    }
                    if (e.getSource().equals(menuRoll)) { // roll up empty leaf
                        // nodes
                        try {
                            tree.rollUpNodes(admin);
                        } catch (Exception e) {
                            System.out.println("Could not roll up this node.");
                        }
                    }
                    if (e.getSource().equals(menuHave)) {
                        JPanel mhp = new MustHavePanel(admin, n);
                        mhp.setVisible(true);
                    }

                    if (e.getSource().equals(menuOTL)) {
                        tlexport tl = new tlexport();
                        tl.Node = n;
                        tl.exportTaxonomy(its, n);
                    }
                    if (e.getSource().equals(menuWikiSource)) {
                        try {
                            // prompt for file with wiki topic pages
                            // ******Hardcoded starting file/folder structure in
                            // function for now!!
                            // JFileChooser chooser = new JFileChooser();
                            // chooser.setMultiSelectionEnabled(false);

                            // int option =
                            // chooser.showOpenDialog(itsAdminFrame);
                            // if (option == JFileChooser.APPROVE_OPTION) {
                            // File sf = chooser.getSelectedFile();
                            File f = new File(
                                    "c:\\data\\wikipedia\\wiki_source.xml");

                            System.out.println("Reading source XML file...");
                            System.out.println("File: " + f.getAbsolutePath());

                            FileInputStream fis = new FileInputStream(f);

                            SAXReader xmlReader = new SAXReader(false);

                            InputSource ins = new InputSource(fis);
                            ins.setEncoding("UTF-8");
                            ins.setSystemId(f.toURL().toString());

                            org.dom4j.Document doc = xmlReader.read(ins);
                            Element opr = doc.getRootElement();

                            // Get root node of this corpus
                            String sCorpusID = n.get("CORPUSID");

                            // Get vector of all nodes
                            Vector vNodes = its.CQL(
                                    "SELECT <NODE> WHERE CORPUSID = "
                                    + sCorpusID, 1, 50000);
                            if (vNodes.size() < 1) {
                                return;
                            }

                            // loop through each node in the taxonomy and search
                            // for source in file
                            for (int loop = 0; loop < vNodes.size(); loop++) {
                                ITSTreeNode loopNode = (ITSTreeNode) vNodes
                                        .elementAt(loop);

                                String nodeTitle = loopNode.get("NODETITLE");
                                boolean bAlternateTitle = false;
                                if (nodeTitle.charAt(nodeTitle.length() - 1) == 's') {
                                    bAlternateTitle = true;
                                }

                                System.out.println("Processing node: "
                                        + nodeTitle);

                                // Iterate through <title> elements, looking for
                                // a match
                                Iterator iPage = opr.elements("page")
                                        .iterator();
                                while (iPage.hasNext()) {
                                    Element ePage = (Element) iPage.next();
                                    if ((ePage != null)) { // compare title text
                                        if (ePage.elementText("title")
                                                .compareTo(nodeTitle) == 0) { // match
                                            // found,
                                            // add
                                            // source
                                            String sOriginalSource = its
                                                    .getNodeSourceAsString(loopNode);
                                            Element eRevision = ePage
                                                    .element("revision");
                                            String sNewSource = new StringBuffer()
                                                    .append(eRevision
                                                    .elementText("text"))
                                                    .toString();

                                            // remove the {{ }} citations
                                            String sCleanSource = sOriginalSource;
                                            int iSourceLength = sNewSource
                                                    .length();
                                            for (int j = 0; j < iSourceLength; j++) {
                                                if (sNewSource.charAt(j) == '{') {
                                                    // get to end of citation
                                                    while (sNewSource.charAt(j) != '}') {
                                                        j++;
                                                    }
                                                    j++; // gets to second }

                                                    if (j < iSourceLength) {
                                                        j++; // gets to next
                                                        // char that we
                                                        // really want
                                                    }
                                                }
                                                if (j < iSourceLength) {
                                                    sCleanSource = new StringBuffer()
                                                            .append(sCleanSource)
                                                            .append(sNewSource
                                                            .charAt(j))
                                                            .toString();
                                                }
                                            }
                                            its.setNodeSource(loopNode,
                                                    sCleanSource);
                                            break; // found source, go to next
                                            // node
                                        } else {
                                            if (bAlternateTitle) {
                                                // does it match without s at
                                                // end
                                                if (ePage
                                                        .elementText("title")
                                                        .compareTo(
                                                        nodeTitle
                                                        .substring(
                                                        0,
                                                        (nodeTitle
                                                        .length() - 1))) == 0) { // match
                                                    // found,
                                                    // add
                                                    // source
                                                    String sOriginalSource = its
                                                            .getNodeSourceAsString(loopNode);
                                                    Element eRevision = ePage
                                                            .element("revision");
                                                    String sNewSource = new StringBuffer()
                                                            .append(eRevision
                                                            .elementText("text"))
                                                            .toString();

                                                    // remove the {{ }}
                                                    // citations
                                                    String sCleanSource = sOriginalSource;
                                                    int iSourceLength = sNewSource
                                                            .length();
                                                    for (int j = 0; j < iSourceLength; j++) {
                                                        if (sNewSource
                                                                .charAt(j) == '{') {
                                                            // get to end of
                                                            // citation
                                                            while (sNewSource
                                                                    .charAt(j) != '}') {
                                                                j++;
                                                            }
                                                            j++; // gets to
                                                            // second }

                                                            if (j < iSourceLength) {
                                                                j++; // gets to
                                                                // next
                                                                // char
                                                                // that
                                                                // we
                                                                // really
                                                                // want
                                                            }
                                                        }
                                                        if (j < iSourceLength) {
                                                            sCleanSource = new StringBuffer()
                                                                    .append(sCleanSource)
                                                                    .append(sNewSource
                                                                    .charAt(j))
                                                                    .toString();
                                                        }
                                                    }
                                                    its.setNodeSource(loopNode,
                                                            sCleanSource);
                                                    break; // found source, go
                                                    // to next node

                                                    // its.setNodeSource(loopNode,
                                                    // sNewSource);
                                                    // break; //found source, go
                                                    // to next node
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(admin,
                                    "Error populating a topic.  Error msg: "
                                    + e.getMessage(), "Error",
                                    JOptionPane.NO_OPTION);
                        }

                        JOptionPane.showMessageDialog(admin,
                                "Source populated for found topics", "Info",
                                JOptionPane.NO_OPTION);
                    }
                    if (e.getSource().equals(menuMustHaveComma)) {
                        try {
                            // This function will add must haves for topic names
                            // that have commas
                            // BUT will only add the text prior to the comma
                            // For example:
                            // Topic name: "Latulippe, Maria"
                            // Must Have added: "Latulippe"
                            // OR
                            // Topic name: "Maria Latulippe"
                            // Must Have added: <none>

                            // Get root node of this corpus
                            String sCorpusID = n.get("CORPUSID");
                            System.out
                                    .println("Ready to add Must Haves from topics with commas");

                            // Get vector of all nodes
                            Vector vNodes = its.CQL(
                                    "SELECT <NODE> WHERE CORPUSID = "
                                    + sCorpusID, 1, 50000);
                            if (vNodes.size() < 1) {
                                return;
                            }

                            // loop through each node in the taxonomy and search
                            // topic for comma
                            for (int loop = 0; loop < vNodes.size(); loop++) {
                                ITSTreeNode loopNode = (ITSTreeNode) vNodes
                                        .elementAt(loop);

                                String nodeTitle = loopNode.get("NODETITLE");
                                if (nodeTitle.indexOf(",") != -1) {
                                    // Has at least one comma
                                    System.out.println("Processing node: "
                                            + nodeTitle);

                                    int firstComma = nodeTitle.indexOf(",");
                                    int lastComma = nodeTitle.lastIndexOf(",");

                                    // Add must have equal to text before first
                                    // comma
                                    String firstText = nodeTitle.substring(0,
                                            (firstComma));
                                    its.addMusthave(loopNode.get("NODEID"),
                                            firstText.toLowerCase());

                                    // if only a single comma, also add must
                                    // have
                                    // equal to flipped text
                                    if (firstComma == lastComma) // has only one
                                    // comma
                                    {
                                        String secondText = nodeTitle
                                                .substring(firstComma + 1,
                                                nodeTitle.length());
                                        String mustHaveText = secondText.trim()
                                                + " " + firstText;
                                        its.addMusthave(loopNode.get("NODEID"),
                                                mustHaveText.toLowerCase());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(
                                    admin,
                                    "Error adding Must Haves.  Error msg: "
                                    + e.getMessage(), "Error",
                                    JOptionPane.NO_OPTION);
                        }

                        JOptionPane
                                .showMessageDialog(
                                admin,
                                "Must Haves populated for topics containing commas.",
                                "Info", JOptionPane.NO_OPTION);
                    }
                    if (e.getSource().equals(menuCopySigToMustHave)) {
                        try {
                            // This function will copy each signature to be a
                            // must have

                            // Get root node of this corpus
                            String sCorpusID = n.get("CORPUSID");
                            System.out
                                    .println("Ready to move Signatures to Must Haves");

                            // Get vector of all nodes
                            Vector vNodes = its.CQL(
                                    "SELECT <NODE> WHERE CORPUSID = "
                                    + sCorpusID, 1, 50000);
                            if (vNodes.size() < 1) {
                                return;
                            }

                            // loop through each node in the taxonomy
                            for (int loop = 0; loop < vNodes.size(); loop++) {
                                ITSTreeNode loopNode = (ITSTreeNode) vNodes
                                        .elementAt(loop);

                                String nodeTitle = loopNode.get("NODETITLE");
                                Vector vSignatures = its
                                        .getNodeSignatures(loopNode);
                                // Signatures signatures = new
                                // Signatures(vSignatures);

                                if (vSignatures.size() > 0) {
                                    // Has at least one signature
                                    System.out.println("Processing node: "
                                            + nodeTitle);

                                    for (int i = 0; i < vSignatures.size(); i++) {
                                        Signature s = (Signature) vSignatures
                                                .elementAt(i);
                                        if (s.getWord().trim().compareTo("") != 0) // bad
                                        // sigs
                                        // (empty/no
                                        // text)in
                                        // db!
                                        {
                                            // Add must have equal to signature
                                            its.addMusthave(
                                                    loopNode.get("NODEID"), s
                                                    .getWord().trim()
                                                    .toLowerCase());
                                        }
                                        // DOES NOT DELETE SIGNATURES!
                                    }
                                }
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(
                                    admin,
                                    "Error adding Must Haves.  Error msg: "
                                    + e.getMessage(), "Error",
                                    JOptionPane.NO_OPTION);
                        }

                        JOptionPane.showMessageDialog(admin,
                                "Must Haves populated from Signatures.",
                                "Info", JOptionPane.NO_OPTION);
                    }
                    if (e.getSource().equals(menuFill)) { // Fill this node with
                        // web documents
                        try {
                            // pop up a dialog asking for the words, with the
                            // current node title as the default
                            Object[] commentArray = {
                                "Please enter your search terms.", "", ""};
                            Object[] options = {"OK"};

                            final JOptionPane optionPane = new JOptionPane(
                                    commentArray, JOptionPane.QUESTION_MESSAGE,
                                    JOptionPane.OK_CANCEL_OPTION, null,
                                    options, options[0]);

                            optionPane.setInitialSelectionValue(n
                                    .get("NODETITLE"));
                            optionPane.setWantsInput(true);
                            JDialog dialog = optionPane.createDialog(null,
                                    "Search Terms");

                            dialog.pack();
                            dialog.show();

                            Object response = optionPane.getInputValue();
                            String s = n.get("NODETITLE");

                            if (response != JOptionPane.UNINITIALIZED_VALUE) {
                                s = response.toString();
                            }

                            int hits = supplementNode(n, s);

                            if (hits == -2) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "Error retrieving results, see log file for more details.",
                                        "Error", JOptionPane.NO_OPTION);
                            } else if (hits == -1) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "Search engine retrieved no results to classify.",
                                        "Information",
                                        JOptionPane.NO_OPTION);
                            } else if (hits == 0) {
                                JOptionPane
                                        .showMessageDialog(
                                        admin,
                                        "None of the documents retrieved classified into this topic.",
                                        "Information",
                                        JOptionPane.NO_OPTION);
                            } else {
                                JOptionPane.showMessageDialog(admin,
                                        "Topic successfully supplemented with "
                                        + hits + " web documents.",
                                        "Information", JOptionPane.NO_OPTION);
                            }

                            return;

                        } catch (Exception e) {
                            System.out
                                    .println("Could not populate this node with documents.");
                        }
                    }
                    if (e.getSource().equals(menuRef)) { // refresh node
                        try {
                            tree.scheduleNodeRefresh();
                            JOptionPane
                                    .showMessageDialog(
                                    admin,
                                    "Topic scheduled successfully.  This topic will be "
                                    + "refreshed during the next batch cycle.",
                                    "Information",
                                    JOptionPane.NO_OPTION);
                        } catch (Exception e) {
                            JOptionPane
                                    .showMessageDialog(
                                    admin,
                                    "This topic could not be scheduled for refresh.  This "
                                    + "topic may already be scheduled for refresh, see the Topic Refresh Report for "
                                    + "more information.",
                                    "Information",
                                    JOptionPane.NO_OPTION);
                            System.out
                                    .println("Could not schedule this node for refresh.");
                        }
                    }
                    if (e.getSource().equals(buttonAdd)) { // add new signature
                        // term
                        String inputValue = JOptionPane
                                .showInputDialog("Please enter the new value of this new signature term.");
                        if (inputValue == null) {
                            return;
                        }
                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableSignatures
                                .getModel();
                        Vector v = new Vector();
                        v.add(inputValue);
                        v.add(new Integer("1"));
                        dstm.addRow(v);
                        bSaveChanges = true;
                    }
                    //musthave gate
                    if (e.getSource().equals(buttonAddHaveNodeGate)) {
                        String inputValue = JOptionPane
                                .showInputDialog("Please enter the new value of this new must have gate term.");
                        if (inputValue == null) {
                            return;
                        }

                        Language lang = null;
                        if (languageBox == null) {
                            lang = new Language("English", "EN");
                        } else {
                            lang = (Language) languageBox.getSelectedItem();
                        }

                        try {
                            its.addMusthave(n.get("NODEID"), inputValue, false,
                                    lang.getCode(), false, "2"); // 2 for musthave gate
                        } catch (Exception e) {
                            e.printStackTrace(System.err);
                            JOptionPane.showMessageDialog(admin,
                                    "That term could not be added, reason: "
                                    + e.getMessage(), "Error",
                                    JOptionPane.NO_OPTION);
                        }

                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableHaveGate
                                .getModel();
                        Vector v = new Vector();
                        v.add(inputValue);
                        dstm.addRow(v);

                    }

                    if (e.getSource().equals(buttonAddHave)) {
                        String inputValue = JOptionPane
                                .showInputDialog("Please enter the new value of this new must have term.");
                        if (inputValue == null) {
                            return;
                        }

                        Language lang = null;
                        if (languageBox == null) {
                            lang = new Language("English", "EN");
                        } else {
                            lang = (Language) languageBox.getSelectedItem();
                        }

                        try {
                            its.addMusthave(n.get("NODEID"), inputValue, false,
                                    lang.getCode(), false);
                        } catch (Exception e) {
                            e.printStackTrace(System.err);
                            JOptionPane.showMessageDialog(admin,
                                    "That term could not be added, reason: "
                                    + e.getMessage(), "Error",
                                    JOptionPane.NO_OPTION);
                        }

                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableHave
                                .getModel();
                        Vector v = new Vector();
                        v.add(inputValue);
                        dstm.addRow(v);

                    }

                    if (e.getSource().equals(buttonAdd2)) { // add new signature
                        // term (signature
                        // tab)
                        String inputValue = JOptionPane
                                .showInputDialog("Please enter the new value of this new signature term.");
                        if (inputValue == null) {
                            return;
                        }
                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableSignatures2
                                .getModel();
                        Vector v = new Vector();
                        v.add(inputValue);
                        v.add(new Integer("1"));
                        dstm.addRow(v);
                        buttonApply.setEnabled(true);
                        buttonUndo.setEnabled(true);
                    }
                    if (e.getSource().equals(buttonRemove)) { // remove
                        // signature
                        // term
                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableSignatures
                                .getModel();
                        int[] i = tableSignatures.getSelectedRows();

                        for (int loop = i.length - 1; loop >= 0; loop--) {
                            dstm.removeRow(i[loop]);
                        }

                        // issue #484 - focus on signature delete disappears
                        i[0] = i[0] - 1;
                        if (i[0] < 0) {
                            i[0] = 0; // set row index for focus
                        }
                        try {
                            tableSignatures.setRowSelectionInterval(i[0], i[0]);
                        } catch (IllegalArgumentException e) {
                        } // do nothing on this exception type
                        // end issue #484 fix

                        tableSignatures.revalidate();
                        bSaveChanges = true;
                    }

                    if (e.getSource().equals(buttonRemoveHaveNodeGate)) { 
                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableHaveGate
                                .getModel();
                        int[] i = tableHaveGate.getSelectedRows();

                        for (int loop = i.length - 1; loop >= 0; loop--) {
                            try {
                                Object value = dstm.getValueAt(i[loop], 0);
                                if (value.getClass().getName()
                                        .indexOf("Integer") != -1) {
                                    Integer iv = (Integer) value;
                                    value = (iv.intValue()) + "";
                                }

                                Language lang = null;
                                if (languageBox == null) {
                                    lang = new Language("English", "EN");
                                } else {
                                    lang = (Language) languageBox
                                            .getSelectedItem();
                                }

                                its.removeMusthave(n.get("NODEID"),
                                        (String) value, lang.getCode(), "2"); // 2 for nodemusthave gate table
                                dstm.removeRow(i[loop]);
                            } catch (Exception e) {
                                e.printStackTrace(System.err);
                                JOptionPane.showMessageDialog(admin,
                                        "That term could not be removed, reason: "
                                        + e.getMessage(), "Error",
                                        JOptionPane.NO_OPTION);
                            }
                        }

                        // issue #484 - focus on signature delete disappears
                        i[0] = i[0] - 1;
                        if (i[0] < 0) {
                            i[0] = 0; // set row index for focus
                        }
                        try {
                            tableHaveGate.setRowSelectionInterval(i[0], i[0]);
                        } catch (IllegalArgumentException e) {
                        } // do nothing on this exception type
                        // end issue #484 fix

                        tableHaveGate.revalidate();
                    }



                    if (e.getSource().equals(buttonRemoveHave)) { // remove
                        // signature
                        // term
                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableHave
                                .getModel();
                        int[] i = tableHave.getSelectedRows();

                        for (int loop = i.length - 1; loop >= 0; loop--) {
                            try {
                                Object value = dstm.getValueAt(i[loop], 0);
                                if (value.getClass().getName()
                                        .indexOf("Integer") != -1) {
                                    Integer iv = (Integer) value;
                                    value = (iv.intValue()) + "";
                                }

                                Language lang = null;
                                if (languageBox == null) {
                                    lang = new Language("English", "EN");
                                } else {
                                    lang = (Language) languageBox
                                            .getSelectedItem();
                                }

                                its.removeMusthave(n.get("NODEID"),
                                        (String) value, lang.getCode());
                                dstm.removeRow(i[loop]);
                            } catch (Exception e) {
                                e.printStackTrace(System.err);
                                JOptionPane.showMessageDialog(admin,
                                        "That term could not be removed, reason: "
                                        + e.getMessage(), "Error",
                                        JOptionPane.NO_OPTION);
                            }
                        }

                        // issue #484 - focus on signature delete disappears
                        i[0] = i[0] - 1;
                        if (i[0] < 0) {
                            i[0] = 0; // set row index for focus
                        }
                        try {
                            tableHave.setRowSelectionInterval(i[0], i[0]);
                        } catch (IllegalArgumentException e) {
                        } // do nothing on this exception type
                        // end issue #484 fix

                        tableHave.revalidate();
                    }

                    if (e.getSource().equals(buttonRemoveAllHaveNodeGate)) {
                        try {
                            Language lang = (Language) languageBox
                                    .getSelectedItem();
                            String r = its.removeMusthave(n.get("NODEID"),
                                    null, lang.getCode(), "2"); // 2 for musthave gate table
                            Vector vHave = its.getMusthaves(n.get("NODEID"),
                                    lang.getCode(), "2"); // 2 for musthavegate table
                            loadMustHave(tableHaveGate, vHave);
                            JOptionPane.showMessageDialog(null, r);
                        } catch (Exception e) {
                            Log.debug("Error delete musthaves");
                        }
                    }

                    if (e.getSource().equals(buttonRemoveAllHave)) {
                        try {
                            Language lang = (Language) languageBox
                                    .getSelectedItem();
                            String r = its.removeMusthave(n.get("NODEID"),
                                    null, lang.getCode());
                            Vector vHave = its.getMusthaves(n.get("NODEID"),
                                    lang.getCode());
                            loadMustHave(tableHave, vHave);
                            JOptionPane.showMessageDialog(null, r);
                        } catch (Exception e) {
                            Log.debug("Error delete musthaves");
                        }
                    }

                    // signature tab actions
                    if (e.getSource().equals(buttonRemove2)) { // remove
                        // signature
                        // term
                        // (signatureTab)
                        DefaultSortTableModel dstm = (DefaultSortTableModel) tableSignatures2
                                .getModel();
                        int[] i = tableSignatures2.getSelectedRows();
                        for (int loop = i.length - 1; loop >= 0; loop--) {
                            dstm.removeRow(i[loop]);
                        }

                        // issue #484 - focus on signature delete disappears
                        i[0] = i[0] - 1;
                        if (i[0] < 0) {
                            i[0] = 0; // set row index for focus
                        }
                        tableSignatures2.setRowSelectionInterval(i[0], i[0]);
                        // end issue #484 fix

                        tableSignatures2.revalidate();
                        buttonApply.setEnabled(true);
                        buttonUndo.setEnabled(true);
                    }
                    if (e.getSource().equals(buttonUndo)) { // rollback previous
                        // changes
                        try {
                            undoSignatures(n);
                        } catch (Exception e) {
                            JOptionPane
                                    .showMessageDialog(
                                    admin,
                                    "Could not roll back concept signature set.",
                                    "Error", JOptionPane.NO_OPTION);
                            e.printStackTrace(System.out);
                        }
                        buttonUndo.setEnabled(false);
                        loadSignatures(tableSignatures2, vUndoSignatures, true);
                    }
                    if (e.getSource().equals(buttonApply)) { // apply signature
                        // changes
                        // if user is applying signatures, rollback is disabled
                        vUndoSignatures = new Vector();
                        buttonUndo.setEnabled(false);
                        buttonApply.setEnabled(false);

                        saveSignatures(tableSignatures2, n);

                        // refresh general tab as well
                        Language lang = (Language) languageBox
                                .getSelectedItem();
                        Vector vSignatures = its.getNodeSignatures(n,
                                lang.getCode());

                        try {
                            loadSignatures(tableSignatures, vSignatures, true);
                        } catch (Exception e) {
                        } // do not throw an error if general tab fails to
                        // refresh

                    }
                    if (e.getSource().equals(buttonTest2)) { // test signatures
                        // against
                        // custom doc
                        // set
                        // store signatures for possible rollback
                        Language lang = (Language) languageBox
                                .getSelectedItem();
                        vUndoSignatures = its.getNodeSignatures(n,
                                lang.getCode());

                        // save the signatures to the database then reclassify
                        // each document against this node
                        // EDIT: no longer need to save signatures, test them
                        // first
                        // saveSignatures(tableSignatures2, n);
                        buttonUndo.setEnabled(true);

                        // purge the signature cache file for this node
                        // if there is an error, continue anyway
                        // try { its.purgeSignatures(n.getCorpusID(), ); }
                        // catch (Exception e) { e.printStackTrace(System.out);
                        // }

                        // begin classifying documents against this node
                        SignatureTestDocSearch stds = new SignatureTestDocSearch(
                                admin, n, getSignatures(tableSignatures2));
                        stds.show();
                    }
                    if (e.getSource().equals(buttonTest)) { // test signatures
                        // against current
                        // doc set
                        // store signatures for possible rollback
                        Language lang = (Language) languageBox
                                .getSelectedItem();
                        vUndoSignatures = its.getNodeSignatures(n,
                                lang.getCode());

                        // save the signatures to the database then reclassify
                        // each document against this node
                        // EDIT: no longer need to save signatures, test them
                        // first
                        // saveSignatures(tableSignatures2, n);
                        buttonUndo.setEnabled(true);

                        // select the current node document set
                        String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = "
                                + n.get("NODEID")
                                + " AND SCORE1 > 25 ORDER BY SCORE1 DESC";
                        Vector vNodeDocuments = new Vector();
                        try {
                            vNodeDocuments = its.CQL(sCQL);
                        } catch (Exception e) {
                            JOptionPane
                                    .showMessageDialog(
                                    admin,
                                    "Encountered an error while attempting to retrieve documents associated with this topic.",
                                    "Error", JOptionPane.NO_OPTION);
                            e.printStackTrace(System.out);
                            return;
                        }

                        if (vNodeDocuments.size() == 0) {
                            JOptionPane
                                    .showMessageDialog(
                                    admin,
                                    "Sorry, this topic does not contain any document relationships.",
                                    "Information",
                                    JOptionPane.NO_OPTION);
                            return;
                        }

                        // purge the signature cache file for this node
                        // if there is an error, continue anyway
                        // try { its.purgeSignatures(n.getCorpusID()); }
                        // catch (Exception e) { e.printStackTrace(System.out);
                        // }

                        // begin classifying documents against this node
                        SignatureTestPanel stp = new SignatureTestPanel(admin,
                                n, vNodeDocuments,
                                getSignatures(tableSignatures2));
                        stp.setVisible(true);
                    }
                    if (e.getSource().equals(buttonCheckSignatures)) {
                        ArrayList<String> signatureList = new ArrayList<String>();
                        ArrayList<String> signatureListNotFound = new ArrayList<String>();
                        ArrayList<String> signatureToRemove = new ArrayList<String>();
                        ArrayList<String> signatureToAdd = new ArrayList<String>();

                        for (int i = 0; i < tableSignatures2.getRowCount(); i++) {
                            signatureList.add(tableSignatures2.getValueAt(i, 0)
                                    .toString());
                        }
                        ManagerSignatureUtil msu = new ManagerSignatureUtil();
                        signatureListNotFound = msu
                                .returnSignaturesNotFound(signatureList);
                        if (!signatureListNotFound.isEmpty()) {
                            ManagerSignatures ms = new ManagerSignatures(
                                    signatureListNotFound);
                            signatureToAdd = ms.getSigToAdd();
                            signatureToRemove = ms.getSigToRemove();
                            if (signatureToAdd != null) {
                                msu.writeInFile(signatureToAdd);
                            }
                            if (signatureToRemove != null) {
                                // Remove the signature
                                DefaultSortTableModel dstm = (DefaultSortTableModel) tableSignatures2
                                        .getModel();
                                for (String sig : signatureToRemove) {
                                    for (int i = 0; i < tableSignatures2
                                            .getRowCount(); i++) {
                                        if (sig.equals(tableSignatures2
                                                .getValueAt(i, 0).toString())) {
                                            dstm.removeRow(i);
                                        }
                                    }
                                    tableSignatures2.revalidate();
                                    buttonApply.setEnabled(true);
                                    buttonUndo.setEnabled(true);
                                }
                            }
                        } else {
                            JOptionPane.showMessageDialog(admin,
                                    "There are no signatures to analyze");
                        }

                    }
                    if (e.getSource().equals(buttonSave)) { // save node
                        DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                                .getLastSelectedPathComponent();
                        saveNodeProps(item);
                    }
                    if (e.getSource().equals(buttonCancel)) { // do nothing
                        try {
                            DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree
                                    .getLastSelectedPathComponent();

                            ITSTreeNode nd = (ITSTreeNode) item.getUserObject();
                            loadNodeProps(nd);
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                        } finally {
                            bSaveChanges = false;
                        }
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(admin,
                            "Warning! The document properties window could not be created."
                            + "Reason: " + ex.getMessage(), "Error",
                            JOptionPane.NO_OPTION);
                    ex.printStackTrace(System.out);
                }
            }

            /**
             * the synonymous not should pass of 5 words eliminate more the two
             * spaces
             *
             * @param group
             * @return
             */
            private String postProccessRegex(String group) {
                ArrayList<String> terms = new ArrayList<String>();
                String term = "";
                term = group;
                term = term.replaceAll("(\\s{2,})", " ");
                int countWord = StringUtils.countMatches(term, " ");
                if (countWord < 5) {
                    return term.trim();
                }
                return null;
            }

            private String getRegexWithParams(ArrayList<String> params,
                    String line) {
                for (int i = 0; i < params.size(); i++) {
                    if (line.contains("param" + i)) {
                        line = line.replace("param" + i, params.get(i));
                    }
                }
                return line.trim();
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor
                        .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    public void stateChanged(ChangeEvent e) {
        if (e.getSource().equals(tabPane)) {
            loadDocuments(tableDocuments, new Vector());
            loadDocuments(tableDocuments2, new Vector(), false);
        }

        if (tabPane.getSelectedIndex() == 1) { // create context sensitive
            // document menu
            if (popupDocMenu == null) {
                popupDocMenu = new JPopupMenu();

                menuOpen = new JMenuItem("Open");
                menuRelated = new JMenuItem("Related");
                menuClassify = new JMenuItem("Classify");
                menuExplain = new JMenuItem("Explain");
                menuRemove = new JMenuItem("Blacklist");
                menuProperties = new JMenuItem("Properties");

                popupDocMenu.add(menuOpen);
                popupDocMenu.add(menuRelated);
                popupDocMenu.add(menuRemove);
                popupDocMenu.add(new JSeparator());
                popupDocMenu.add(menuClassify);
                popupDocMenu.add(menuExplain);
                popupDocMenu.add(new JSeparator());
                popupDocMenu.add(menuProperties);

                menuOpen.addActionListener(this);
                menuRelated.addActionListener(this);
                menuClassify.addActionListener(this);
                menuExplain.addActionListener(this);
                menuRemove.addActionListener(this);
                menuProperties.addActionListener(this);
            }
            //
            popupDocMenu.setInvoker(tableDocuments);

            if (mouseDocListener == null) {
                mouseDocListener = new MouseAdapter() {
                    public void mousePressed(MouseEvent e) {
                        if (e.isPopupTrigger()) {
                            int x = e.getX();
                            int y = e.getY();
                            int row = tableDocuments
                                    .rowAtPoint(new Point(x, y));
                            if (row != -1) {
                                tableDocuments.getSelectionModel()
                                        .setSelectionInterval(row, row);
                                popupDocMenu.show(tableDocuments, x, y);
                            }
                        }
                    }

                    public void mouseReleased(MouseEvent e) {
                        if (e.isPopupTrigger()) {
                            int x = e.getX();
                            int y = e.getY();
                            int row = tableDocuments
                                    .rowAtPoint(new Point(x, y));
                            if (row != -1) {
                                tableDocuments.getSelectionModel()
                                        .setSelectionInterval(row, row);
                                popupDocMenu.show(tableDocuments, x, y);
                            }
                        }
                    }
                };
            }
            tableDocuments.addMouseListener(mouseDocListener);
        }

        SwingWorker aWorker = new SwingWorker(e) {
            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                if (e.getSource().equals(tabPane)) {
                    ITSTreeNode n = null;
                    try {
                        TreePath path = tree.getSelectionPath();
                        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path
                                .getLastPathComponent();
                        n = (ITSTreeNode) selectedNode.getUserObject();
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                        return;
                    }

                    if (tabPane.getSelectedIndex() == 1) { // get document
                        // relationships
                        try {
                            String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = "
                                    + n.get("NODEID")
                                    + " AND SCORE1 > 25 ORDER BY SCORE1 DESC";
                            loadDocuments(tableDocuments, its.CQL(sCQL));
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                            return;
                        }
                    }
                    if (tabPane.getSelectedIndex() == 2) { // get document
                        // relationships
                        try {
                            String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = "
                                    + n.get("NODEID")
                                    + " AND SCORE1 < 0 ORDER BY SCORE1 DESC";
                            loadDocuments(tableDocuments2, its.CQL(sCQL), false);
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                            return;
                        }
                    }
                    if (tabPane.getSelectedIndex() == 4) {
                        refreshSourcePane(n);
                    }

                    if (tabPane.getSelectedIndex() == 3) {
                        refreshSignaturePane(n);
                    }
                    if (tabPane.getSelectedIndex() == 5) {
                        refreshImagesPane(n);
                    }
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor
                        .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();

    }

    public void focusGained(FocusEvent e) {
        if (e.getSource().equals(tree)) {
            ITSTreeCellRenderer tcr = (ITSTreeCellRenderer) tree
                    .getCellRenderer();
            tcr.setBackgroundSelectionColor(UIManager
                    .getColor("Tree.selectionBackground"));
            tcr.setTextSelectionColor(UIManager
                    .getColor("Tree.selectionForeground"));
            tcr.setBounds(new Rectangle(0, 0, 4000, 4000));

            tree.setCellRenderer(tcr);
        }
        if (e.getSource().equals(treeField)) {
            treeField.selectAll();
        }
    }

    public void focusLost(FocusEvent e) {
        if (e.getSource().equals(textSize)) {
            try {
                Integer.parseInt(textSize.getText());
            } catch (NumberFormatException ex) {
                JOptionPane
                        .showMessageDialog(
                        admin,
                        "Sorry, The topic size field must be an integer value.",
                        "Information", JOptionPane.NO_OPTION);
                textSize.setText("100");
            }
            return;
        }
        // if the signature table was being edited, save those changes first
        // before leaving
        if ((e.getSource().equals(tableSignatures))
                && (tableSignatures.getEditingRow() != -1)) {
            tableSignatures.getCellEditor().stopCellEditing();
        }
        if ((e.getSource().equals(tableSignatures2))
                && (tableSignatures2.getEditingRow() != -1)) {
            tableSignatures2.getCellEditor().stopCellEditing();
        }
        // if focus on tree is lost, make highlighting shade light grey
        if (e.getSource().equals(tree)) {
            ITSTreeCellRenderer tcr = (ITSTreeCellRenderer) tree
                    .getCellRenderer();
            tcr.setBackgroundSelectionColor(Color.lightGray);
            tcr.setTextSelectionColor(Color.BLACK);
            tcr.setBounds(new Rectangle(0, 0, 4000, 4000));
            tree.setCellRenderer(tcr);
        }
    }

    public void valueChanged(TreeSelectionEvent e) {
        // return if tree selection event triggered by a drag, return now
        if (tree.isDragging()) {
            return;
        }

        SwingWorker aWorker = new SwingWorker(e) {
            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    DefaultMutableTreeNode item = null;

                    try {
                        item = (DefaultMutableTreeNode) tree
                                .getLastSelectedPathComponent();
                    } catch (Exception ex) {
                        return;
                    }

                    if (item == null) {
                        return;
                    }

                    if (ITS.getUser().PromptForTopicChange() && bSaveChanges) { // yes
                        // =
                        // 0,
                        // no
                        // =
                        // 1,
                        // cancel
                        // =
                        // 2
                        // ..
                        JOptionPane pane = new JOptionPane(
                                "The topic properties have changed.  Save changes?",
                                JOptionPane.QUESTION_MESSAGE,
                                JOptionPane.YES_NO_CANCEL_OPTION);
                        JPanel jp = (JPanel) pane.getComponent(1);
                        Component[] c = jp.getComponents();

                        JButton buttonYes = (JButton) c[0];
                        JButton buttonNo = (JButton) c[1];
                        JButton buttonCancel = (JButton) c[2];

                        KeyAdapter key = new KeyAdapterEnhanced(e) {
                            public void keyPressed(KeyEvent ex) {
                                if (ex.getKeyCode() == KeyEvent.VK_Y) {
                                    DefaultMutableTreeNode olditem = (DefaultMutableTreeNode) e
                                            .getOldLeadSelectionPath()
                                            .getLastPathComponent();
                                    saveNodeProps(olditem);

                                    confirmDialog.dispose();
                                }
                                if (ex.getKeyCode() == KeyEvent.VK_N) {
                                    confirmDialog.dispose();
                                }
                                if (ex.getKeyCode() == KeyEvent.VK_C) {
                                    bLoadProps = false;
                                    bSaveChanges = false;
                                    tree.setSelectionPath(e
                                            .getOldLeadSelectionPath());
                                    confirmDialog.dispose();
                                    bEarlyReturn = true;
                                }
                            }
                        };
                        buttonYes.addKeyListener(key);
                        buttonNo.addKeyListener(key);
                        buttonCancel.addKeyListener(key);

                        confirmDialog = pane.createDialog(null, "Question");
                        confirmDialog.show();
                        Object selectedValue = null;
                        selectedValue = pane.getValue();

                        if (bEarlyReturn) {
                            return;
                        }

                        try {
                            int i = ((Integer) selectedValue).intValue();
                            if (i == 0) { // save it
                                DefaultMutableTreeNode olditem = (DefaultMutableTreeNode) ((TreeSelectionEvent) e)
                                        .getOldLeadSelectionPath()
                                        .getLastPathComponent();
                                saveNodeProps(olditem);
                            } else if (i == 2) { // changed my mind (cancel)
                                bLoadProps = false;
                                bSaveChanges = false;
                                tree.setSelectionPath(((TreeSelectionEvent) e)
                                        .getOldLeadSelectionPath());
                                return;
                            }
                        } catch (Exception ex) {
                        } // keyboard short cut was used, no action required
                    }

                    if (!bLoadProps) {
                        bLoadProps = true;
                        bSaveChanges = true;
                        return;
                    }

                    bSaveChanges = false;
                    try {
                        ITSTreeNode n = (ITSTreeNode) item.getUserObject();

                        if (item.getChildCount() == 0) {
                            tree.createChildren(item, n.get("NODEID"));
                        }
                        loadNodeProps(n);

                        if (tabPane.getSelectedIndex() == 1) {
                            try {
                                String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = "
                                        + n.get("NODEID")
                                        + " AND SCORE1 > 25 ORDER BY SCORE1 DESC";
                                loadDocuments(tableDocuments, its.CQL(sCQL));
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                                return;
                            }
                        }
                        if (tabPane.getSelectedIndex() == 2) { // get document
                            // relationships
                            try {
                                String sCQL = "SELECT <NODEDOCUMENT> WHERE NODEID = "
                                        + n.get("NODEID")
                                        + " AND SCORE1 < 0 ORDER BY SCORE1 DESC";
                                loadDocuments(tableDocuments2, its.CQL(sCQL),
                                        false);
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                                return;
                            }
                        }
                        if (tabPane.getSelectedIndex() == 4) {
                            refreshSourcePane(n);
                        }

                        if (tabPane.getSelectedIndex() == 3) {
                            refreshSignaturePane(n);
                        }
                        if (tabPane.getSelectedIndex() == 5) {
                            refreshImagesPane(n);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }

            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor
                        .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    public class KeyAdapterEnhanced extends KeyAdapter {

        public TreeSelectionEvent e = null;

        public KeyAdapterEnhanced() {
            super();
        }

        public KeyAdapterEnhanced(TreeSelectionEvent tse) {
            super();
            e = tse;
        }

        public KeyAdapterEnhanced(EventObject tse) {
            super();
            e = (TreeSelectionEvent) tse;
        }
    }

    public class CellRenderer extends JLabel implements TableCellRenderer {

        private LineBorder _selectBorder;
        private EmptyBorder _emptyBorder;
        private Dimension _dim;

        public CellRenderer() {
            super();
            _emptyBorder = new EmptyBorder(1, 2, 1, 2);
            _selectBorder = new LineBorder(Color.BLACK);
            // setOpaque(true);
            setHorizontalAlignment(SwingConstants.CENTER);
        }

        ;

		/**
		 * 
		 * Method defining the renderer to be used when drawing the cells.
		 * 
		 */
		public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {

            Object s = value;

            // setFont(_cellFont);
            setText(s.toString());
            setForeground(Color.BLACK);
            setBackground(Color.WHITE);

            if (isSelected) {
                setForeground(Color.BLACK);
                setBackground(Color.BLUE);
                setBorder(_selectBorder);

            } else {
                setBorder(_emptyBorder);
            }

            return this;
        }
    }

    private String filterString(String term) {
        term = term.replace(";", "");
        term = term.replace("!", "");
        return term;
    }

    public class SelectionListener implements ListSelectionListener {

        SelectionListener() {
            super();
        }

        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                rebuildChart();
            }
        }
    }

    public void addItemsMenuEdit() {
        //-----------------------------------JSeparators------------------------
        m1 = new JSeparator();
        m2 = new JSeparator();
        m3 = new JSeparator();
        m4 = new JSeparator();
        m5 = new JSeparator();
        m6 = new JSeparator();
        m7 = new JSeparator();
        //----------------------------------------------------------------------
        JMenu m = ITSMenu.Editmenu;
        mustHavesEdit = new JMenu("Must Have");
        nodeMustHaveGate = new JMenu("Must Have Gate");
        imagesEdit = new JMenu("Images");
        signaturesEdit = new JMenu("Signatures");
        //----------------------------------------------------------------------
        menuHaveFromSource.setEnabled(false);
        menuHaveBranch.setEnabled(false);
        //----------------------------------------------------------------------
        if (!addMenuItems) {
            m.add(m1);
            m.add(menuAlp);
            m.add(menuAlR);
            m.add(menuCap);
            m.add(menuCaR);
            m.add(m2);
            m.add(menuStrip);
            m.add(menuStriR);
            m.add(menuStripWS);
            m.add(m3);

            mustHavesEdit.add(menuHave);
            mustHavesEdit.add(menuHaveFromSource);
            mustHavesEdit.add(menuFillMusthaves);
            mustHavesEdit.add(menuCopySigToMustHave);
            mustHavesEdit.add(menuHaveBranch);
            mustHavesEdit.add(menuParaMust);
            mustHavesEdit.add(menuComaMust);
            mustHavesEdit.add(menuDeleteMustHaves);

            m.add(mustHavesEdit);
            m.add(m4);

            nodeMustHaveGate.add(addNodeMustHaveGate);
            m.add(nodeMustHaveGate);
            m.add(m7);

            imagesEdit.add(menuImportImages);
            imagesEdit.add(menuDeleteImages);

            m.add(imagesEdit);
            m.add(m5);

            signaturesEdit.add(menuFillSignatures);
            signaturesEdit.add(menuDeleteSignatures);

            m.add(signaturesEdit);
            m.add(m6);
            m.add(menuPopulateDescription);

            addMenuItems = true;
        }
    }

    public void showHiddenItemsMenuEdit(boolean flag) {
        menuAlp.setVisible(flag);
        menuAlR.setVisible(flag);
        menuCap.setVisible(flag);
        menuCaR.setVisible(flag);
        menuStrip.setVisible(flag);
        menuStriR.setVisible(flag);
        menuStripWS.setVisible(flag);
        mustHavesEdit.setVisible(flag);
        imagesEdit.setVisible(flag);
        signaturesEdit.setVisible(flag);
        menuPopulateDescription.setVisible(flag);
        nodeMustHaveGate.setVisible(flag);

        m1.setVisible(flag);
        m2.setVisible(flag);
        m3.setVisible(flag);
        m4.setVisible(flag);
        m5.setVisible(flag);
        m6.setVisible(flag);
        m7.setVisible(flag);
    }

    public void LoadFileNodeType() {
        objects = new ArrayList<NodeTypeBean>();
        File file = new File("node_type.txt");
        FileReader fr = null;
        BufferedReader br = null;
        String line;
        NodeTypeBean obj = null;
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                if (line.equalsIgnoreCase("")) {
                    break;
                }
                String[] l = line.split(",");
                obj = new NodeTypeBean();
                obj.setId(l[0]);
                obj.setName(l[1]);
                objects.add(obj);
            }
        } catch (FileNotFoundException e1) {
            System.err.println("Error get file " + e1.getMessage());
            Log.error("Error get file: ", e1);
        } catch (IOException e) {
            System.err.println("Error read file " + e.getMessage());
            Log.error("Error read file ", e);
        } finally {
            try {
                if (null != br) {
                    br.close();
                }
                if (null != fr) {
                    fr.close();
                }
            } catch (IOException e) {
                Log.error("Error clossing file");
            }
        }
    }
}
