package com.iw.system;

import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;

import com.iw.ui.PopupProgressBar;

public class ProgressStatusHandler implements ElementHandler {

    private PopupProgressBar progress = null;

    public ProgressStatusHandler () { super(); }
    public ProgressStatusHandler (PopupProgressBar progress) { super(); this.progress = progress; }

    public void onStart(ElementPath ep) {
    }

    public void onEnd(ElementPath ep) {
    	//System.out.println(ep.getPath());
        if ((progress != null) && (ep.getPath().endsWith("STATUS"))) {
            int percent = new Integer(ep.getCurrent().getText()).intValue();
            progress.setProgress(percent);
        }
    }

}

