package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.ui.ITSAdministrator;

public class RefreshNodePopup extends JPanel implements ActionListener {
    protected JCheckBox candidateCheckbox;
    protected JComboBox repositoryDropdown;
    protected JSlider numberdocumentsSlide;
    protected JButton okButton;
    protected JButton cancelButton;

    private JInternalFrame MDIframe;
    private ITSAdministrator itsadmin;
    private ITS its;
    private ITSTreeNode Node;

    public RefreshNodePopup(ITSAdministrator itsFrame, ITSTreeNode n) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        its = itsFrame.its; Node = n; itsadmin = itsFrame;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Refresh Topic", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(575, 155);
        MDIframe.setBackground(Color.lightGray);

        MDIframe.setFrameIcon(itsFrame.iIndraweb);

        JDesktopPane jdesktoppane = itsFrame.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        candidateCheckbox = new JCheckBox();
        candidateCheckbox.setToolTipText("Check if only title words should be used in selecting candidate documents.");

        Vector vRepositories;
        try { vRepositories = its.getRepositories(); }
        catch (Exception e) { e.printStackTrace(System.out); vRepositories = new Vector(); }
        vRepositories.add(0, "All");

        repositoryDropdown = new JComboBox(vRepositories);
        repositoryDropdown.setSelectedIndex(0);

        Dictionary dLabels = new Hashtable();
        dLabels.put(new Integer("1"), new JLabel("1"));
        dLabels.put(new Integer("250"), new JLabel("250"));
        dLabels.put(new Integer("500"), new JLabel("500"));
        dLabels.put(new Integer("750"), new JLabel("750"));
        dLabels.put(new Integer("1000"), new JLabel("All"));

        numberdocumentsSlide = new JSlider();
        numberdocumentsSlide.setValue(500);
        numberdocumentsSlide.setMaximum(1000);
        numberdocumentsSlide.setFont(Font.decode("MS Sans Serif-11"));
        numberdocumentsSlide.setToolTipText("Maximum number of candidate documents");
        numberdocumentsSlide.setLabelTable(dLabels);
        numberdocumentsSlide.setMajorTickSpacing(250);
        numberdocumentsSlide.setMinorTickSpacing(50);
        numberdocumentsSlide.setPaintTicks(true);
        numberdocumentsSlide.setPaintLabels(true);

        okButton = new JButton("OK");
        okButton.setToolTipText("Proceed with topic refresh");
        okButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setToolTipText("Abort this action.");
        cancelButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestGrid(2,3);
                {
                    layout.add(new JLabel("Select candidate documents from titles only:"));
                    layout.add(candidateCheckbox);
                    layout.add(new JLabel("Restrict candidate documents to repository: "));
                    layout.add(repositoryDropdown);
                    layout.add(new JLabel("Maximum number of candidate documents:"));
                    layout.add(numberdocumentsSlide);
                    layout.parent();
                }
                layout.parent();
            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(okButton);
            layout.add(new Space(3,3));
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void actionPerformed(ActionEvent e) {
        MDIframe.dispose(); // close the window when the action is taken

        if (e.getSource().equals("cancelButton")) { return; }

        // build parameters
        String repositoryID = "-1";
        if (!repositoryDropdown.getSelectedItem().equals("All")) {
            repositoryID = ((Repository) repositoryDropdown.getSelectedItem()).getID();
        }

        RefreshNodeProgressBar rnpb =
                new RefreshNodeProgressBar(itsadmin, Node,
                        candidateCheckbox.isSelected(), repositoryID, numberdocumentsSlide.getValue());
        rnpb.setVisible(true);
    }
}
