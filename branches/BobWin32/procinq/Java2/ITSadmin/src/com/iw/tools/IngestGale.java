package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.iw.ui.PopupProgressBar;

import javax.swing.*;

//import java.awt.*;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.net.*;

import java.awt.event.*;

import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.XMLReaderFactory;
import org.dom4j.io.SAXReader;

import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.dom4j.Entity;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class IngestGale {
	
    private static String[] sTitles = new String[10];
    public static int iLevelsProcessed = 40;
    private static boolean bFirstSig = true;
    
    public static Corpus createCorpusFromGaleDictionary(File f, ITS its) throws Exception 
    {
        System.out.println("Reading dictionary XML file from Oxford...");
        System.out.println("File: " + f.getAbsolutePath());

        FileInputStream fis = new FileInputStream(f);

        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);
        Element opr = doc.getRootElement();

        //Create the corpus and root node
    	String sFilename = "Acronyms, Initialims, and Abbreviations";
    	Corpus c = its.addCorpus(sFilename);
        Node rootNode = its.getCorpusRoot(c.getID());

        //loop through every file and get single topics (head title), 
        //its must-haves (h1-h6) and signatures (p for each heading)) 
        for (int i=1; opr!=null; i++)
        {
        	//get topic name (<head> title)
        	String sTopicName = opr.element("head").elementText("title");
            ITSTreeNode TopicNode = addNode(sTopicName, rootNode.get("NODEID"), 1, c.getID(), its);
    		if (TopicNode == null) 
    			continue;	//get next file and keep trying
    		    			
            System.out.println("Added new node: "+sTopicName);

            //iterate through <div> elements and get must haves <h1>...<h6> and signatures <p>
            Iterator iDiv = opr.element("body").elements("div").iterator();
        	
            while (iDiv.hasNext()) 
            {
                Element eDiv = (Element) iDiv.next();
                if ((eDiv != null))
                {
                	//check for any headings (h1...h6) which will be must haves
                    if (eDiv.element("h1") != null){
                    	addMustHave(TopicNode, eDiv.elementText("h1"), its);
                    	if (eDiv.element("h2") != null){
                    		addMustHave(TopicNode, eDiv.elementText("h2"), its);
                    		if (eDiv.element("h3") != null){
                    			addMustHave(TopicNode, eDiv.elementText("h3"), its);
                            	if (eDiv.element("h4") != null){
                            		addMustHave(TopicNode, eDiv.elementText("h4"), its);
                            		if (eDiv.element("h5") != null){
                            			addMustHave(TopicNode, eDiv.elementText("h5"), its);
                                    	if (eDiv.element("h6") != null){
                                    		addMustHave(TopicNode, eDiv.elementText("h6"), its);
                                    	}
                            		}
                            	}
                    		}
                    	}
                    }
                    
                    //check for paragraphs, which will be signatures
                    //iterate through <p> elements and get signatures
                    Iterator iP = eDiv.elements("p").iterator();
                	
                    while (iP.hasNext()) 
                    {
                        Element eP = (Element) iP.next();
                        Signatures vsig = new Signatures();
                        bFirstSig = true;
                        getSignatures(eP, vsig, TopicNode, its);

                    /*if (eP != null)
                    {
	                    Signature nodeSig = null;
	                   
           	
	                    String pText = eDiv.elementText("p");
	                    //assumes only one set of parentheses and one set of square brackets at most
                    	//or one of either set;
                    	//and square brackets come first
                        if (pText.indexOf("[")>0)
                        {
                        	//split at bracket for mulitple sigs
                        	String [] sBrackets = pText.split("\\[");
                        	nodeSig = new Signature(sBrackets[0].trim().toLowerCase(), 1);
                        	vsig.add(nodeSig);
                        	
                        	//split at parentheses if exist
                        	if (sBrackets[1].indexOf("(")>0)
                        	{
                            	String [] sParentheses = sBrackets[1].split("\\(");
                            	String [] sBracket2 = sParentheses[0].split("\\]");
                            	nodeSig = new Signature(sBracket2[0].trim().toLowerCase(), 1);
                            	vsig.add(nodeSig);
                            	
                               	//for now, do not add "source" code
                            	//iEnd = sParentheses[1].indexOf(")");
                            	//nodeSig = new Signature(sParentheses[1].substring(1,iEnd).trim().toLowerCase(), 1);
                            	//vsig.add(nodeSig);
                        	}
                        	else 
                        	{
                        		String [] sBracket2 = sBrackets[1].split("\\]");
                            	nodeSig = new Signature(sBracket2[0].trim().toLowerCase(), 1);
                            	vsig.add(nodeSig);
                        	}
                        }
                        else if (pText.indexOf("(")>0)	//no square brackets in definition
                        {
                        	String [] sParentheses = pText.split("\\(");
                        	nodeSig = new Signature(sParentheses[0].trim().toLowerCase(), 1);
                        	vsig.add(nodeSig);
                          	
                        	//for now, do not add "source" code
                        	//iEnd = sParentheses[1].indexOf(")");
                        	//nodeSig = new Signature(sParentheses[1].substring(1,iEnd).trim(), 1);
                        	//vsig.add(nodeSig);
                        }
                        else	//no square brackets nor parentheses in definition
                        {
                        	nodeSig = new Signature(pText.toLowerCase(), 1);
	            			vsig.add(nodeSig);
	                    }
                   	*/     	
               	     	its.saveSignatures(TopicNode.get("NODEID"),vsig.getVecOfSignatureObjects(),false);
                    }                	
                }
                //close current filestream
                fis.close();
                                
                //try to open next file
                String sNewFileNumber; 
                int iFileNumber = i+1;
                String sFileNumber = Integer.toString(iFileNumber);
                int iLength = sFileNumber.length();
                
                //Pad string with zeros to be length of 5
                if (iLength == 1)
                	sNewFileNumber = "0000"+sFileNumber;
                else if (iLength == 2)
                	sNewFileNumber = "000"+sFileNumber;
                else if (iLength == 3)
                	sNewFileNumber = "00"+sFileNumber;
                else if (iLength == 4)
                	sNewFileNumber = "0"+sFileNumber;
                else
                	sNewFileNumber = sFileNumber;
                	
                f = new File("c:\\temp\\gale\\aiad_0001\\aiad_0001\\xml\\aiad_0038_0001_1_"+sNewFileNumber+".xml");
                fis = new FileInputStream(f);

                ins = new InputSource(fis);
                ins.setEncoding("UTF-8");
                ins.setSystemId(f.toURL().toString());

                doc = xmlReader.read(ins);
                opr = doc.getRootElement();
           }
        }
        
                
        return c;
    }
    
    public static void getSignatures(Element eSource, Signatures vsig, ITSTreeNode currentNode, ITS its) 
    {
        List content = eSource.content();
        Iterator it = content.iterator();
        
        while (it.hasNext()) 
        {
            Object obj = it.next();
            if (obj instanceof Text) 
            {
            	String sText = ((Text) obj).getText();
           	 	if (sText.indexOf("(")>0)
           	 	{
           	 		String [] sParentheses = sText.split("\\(");
           	 		addSignature(sParentheses[0], vsig, currentNode, its, bFirstSig);
           	 	}
           	 	else
           	 		addSignature(sText, vsig, currentNode, its, bFirstSig);           		 
            } else if (obj instanceof Element) 
            {
                if (((Element) obj).attribute("char") != null)
           	 		addSignature(((Element) obj).attribute("char").getText(), vsig, currentNode, its, bFirstSig);           		 
                else
               	 	getSignatures(((Element) obj), vsig, currentNode, its);
            } else if (obj instanceof Entity) 
            {
            	if (((Entity) obj).getText() != null)
            		addSignature(((Entity) obj).getText(), vsig, currentNode, its, bFirstSig);	
            	else
              	 	getSignatures(((Element) obj), vsig, currentNode, its);
            }
            bFirstSig = false;
        }
         return;
   }

    public static void addSignature(String sigName, Signatures vsig, ITSTreeNode currentNode, ITS its, boolean bFirstSig) 
    {
    	Signature nodeSig = null;
    	
    	//remove any square brackets and make sure has content other than that
    	sigName = sigName.replaceAll("\\[","");
    	sigName = sigName.replaceAll("\\]","");
    	String sNewName = sigName.trim();
    	if (sNewName.length() > 0)
    	{
	      	if (bFirstSig)
	    	{
	    		//this value really should be a must have
	           	addMustHave(currentNode, sNewName, its);
	    	}
	    	else
	    	{
				nodeSig = new Signature(sNewName.trim().toLowerCase(), 1);
		    	vsig.add(nodeSig);
		    	System.out.println("Adding new signature: "+sNewName.trim().toLowerCase());
	    	}
    	}
    	return;
    }
    
    public static ITSTreeNode addNode(String sNodeTitle, String sParent, int Depth, String sCorpusID, ITS its) {
        ITSTreeNode n = new ITSTreeNode();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", sCorpusID);
        n.set("DEPTHFROMROOT", Depth + "");
 
        try {
            n = its.addNode(n);
            System.out.println("*********************************");
           //System.out.println("Added node: " + sNodeTitle + ", Parent: " + sParent);

        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;
    }
    
    public static void addMustHave(ITSTreeNode currentNode, String sMusthave, ITS its) 
    {
		try {its.addMusthave(currentNode.get("NODEID"),sMusthave.trim());}
        catch (Exception e) 
        {
        	System.err.println("Could not add musthave for: " + currentNode.get("NODETITLE"));
	        	e.printStackTrace(System.err);
   	    }
      
        System.out.println("Added new musthave: "+sMusthave);
        return;
    }
}