package com.iw.ui.conceptalerts;

import com.oculustech.layout.*;
import com.iw.ui.*;
import com.iw.system.*;
import com.iw.tools.JSortTable;
import com.iw.tools.DefaultSortTableModel;
import com.iw.tools.SwingWorker;
import com.iw.tools.SortTableModel;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.regex.*;

public class ManageAlerts extends JPanel implements ActionListener, MouseListener, KeyListener, ChangeListener {
    // add alert attributes
    protected ITSAdministrator admin;
    protected Component glass;

    // add alert widgets
    protected JSortTable alertTable;
    protected JLabel groupLabel;
    protected JComboBox groupList;
    protected JSortTable groupTable;
    protected JTextArea notesArea;
    protected JTextArea groupnotesArea;
    protected JButton saveButton;
    protected JButton runButton;
    protected JButton closeButton;
    protected JButton deleteButton;

    protected JTabbedPane tabPane;

    public JInternalFrame MDIframe;

    private FrequencyDrop[] items;

    // change detection
    private Hashtable htChanges = new Hashtable();
    private int iTextArea = -1;
    private boolean bFlip = false;

    public ManageAlerts() { }
    public ManageAlerts(ITSAdministrator admin) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = admin.jdp;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Manage Alerts", true, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(675, 480);
        MDIframe.setBackground(Color.lightGray);
        this.admin = admin;

        MDIframe.setFrameIcon(admin.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        items = new FrequencyDrop[4];
        items[0] = new FrequencyDrop("Never", "-1");
        items[1] = new FrequencyDrop("Daily", "1");
        items[2] = new FrequencyDrop("Weekly", "7");
        items[3] = new FrequencyDrop("Monthly", "30");

        User u = ITS.getUser();
        Vector vMyAlerts = new Vector();
        try { vMyAlerts = admin.its.getMyAlerts(u); }
        catch (Exception e) { e.printStackTrace(System.out); }

        final TableCellRenderer freqRenderer = new FreqRenderer(items);
        alertTable = new JSortTable() {
            public TableCellRenderer getCellRenderer(int row, int column) {
                if (column == 1) return freqRenderer;
                return super.getCellRenderer(row, column);
            }
        };

        alertTable.setShowHorizontalLines(false);
        alertTable.setShowVerticalLines(false);
        alertTable.setColumnSelectionAllowed(true);
        alertTable.setCellSelectionEnabled(true);
        alertTable.addMouseListener(this);
        buildTable(alertTable, vMyAlerts);

        groupLabel = new JLabel("Please select a project or group:");
        groupLabel.setToolTipText("Please select a project or group.");

        // retrieve the list of LDAP groups that this user is in
        Vector vGroups = new Vector();
        try { vGroups = admin.its.getMyGroups(); }
        catch (Exception e) { e.printStackTrace(System.out); }

        groupList = new JComboBox(vGroups);
        groupList.setToolTipText("Please select a project or group.");
        groupList.addActionListener(this);

        groupTable = new JSortTable() {
            public TableCellRenderer getCellRenderer(int row, int column) {
                return freqRenderer;
            }
        };
        groupTable.setShowHorizontalLines(false);
        groupTable.setShowVerticalLines(false);
        groupTable.setColumnSelectionAllowed(true);
        groupTable.setCellSelectionEnabled(true);
        groupTable.addMouseListener(this);

        if (vGroups.size() > 0) {
            AlertGroup ag = (AlertGroup) vGroups.elementAt(0);
            Vector vGpAlerts = new Vector();
            try { vGpAlerts = admin.its.getMyAlerts(ag.getID()); }
            catch (Exception e) { e.printStackTrace(System.out); }

            buildTable(groupTable, vGpAlerts);
        }

        notesArea = new JTextArea();
        notesArea.setPreferredSize(new Dimension(notesArea.getPreferredSize().width,75));
        notesArea.addKeyListener(this);

        groupnotesArea = new JTextArea();
        groupnotesArea.setPreferredSize(new Dimension(groupnotesArea.getPreferredSize().width,75));
        groupnotesArea.addKeyListener(this);

        saveButton = new JButton("Save");
        saveButton.setToolTipText("Save alert changes.");
        saveButton.addActionListener(this);

        deleteButton = new JButton("Delete");
        deleteButton.setToolTipText("Remove the selected alert.");
        deleteButton.addActionListener(this);

        runButton = new JButton("Run");
        runButton.setToolTipText("Run the selected alert.");
        runButton.addActionListener(this);

        closeButton = new JButton("Close");
        closeButton.setToolTipText("Close this window.");
        closeButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    tabPane = layout.nestTabbedPane();
                    {
                        layout.nestBoxAsTab("My Alerts",OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                        {
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.add(new JScrollPane(alertTable));
                                layout.add(new JScrollPane(notesArea));
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Group Alerts",OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.nestGrid(2,1);
                                        {
                                            layout.add(groupLabel);
                                            layout.add(groupList);
                                            layout.parent();
                                        }
                                        layout.add(new JScrollPane(groupTable));
                                        layout.add(new JScrollPane(groupnotesArea));
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.addSpace(20);
                        layout.add(saveButton);
                        layout.addSpace(10);
                        layout.add(runButton);
                        layout.addSpace(10);
                        layout.add(deleteButton);
                        layout.addSpace(10);
                        layout.add(closeButton);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);
        tabPane.addChangeListener(this);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
    }

    public void stateChanged(ChangeEvent e) {
        bFlip = true;
        saveTextChanges(true);
        if (htChanges.size() > 0) {
            int i = JOptionPane.showConfirmDialog(null, "Some alert properties have changed.  Save changes?", "Save Changes?", JOptionPane.YES_NO_OPTION);

            if (i == 0) saveChanges(); // save changes
            else htChanges = new Hashtable();
        }
        bFlip = false;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(closeButton)) { MDIframe.dispose(); return; }
        if (e.getSource().equals(groupList)) {
            AlertGroup ag = (AlertGroup) groupList.getSelectedItem();
            Vector vGpAlerts = new Vector();
            try { vGpAlerts = admin.its.getMyAlerts(ag.getID()); }
            catch (Exception ex) { ex.printStackTrace(System.out); }

            buildTable(groupTable, vGpAlerts);
        }
        if (e.getSource().equals(deleteButton)) {
            try {
                JSortTable resultsTable = null;
                JTextArea resultsArea = null;

                if (tabPane.getSelectedIndex() == 0) {
                    resultsTable = alertTable;
                    resultsArea = notesArea;
                } else {
                    resultsTable = groupTable;
                    resultsArea = groupnotesArea;
                }

                int row = resultsTable.getSelectedRow();
                if (row < 0) {
                    JOptionPane.showMessageDialog(admin, "You must first select an alert to run.", "Error", JOptionPane.NO_OPTION);
                    return;
                }

                Alert a = (Alert) resultsTable.getModel().getValueAt(row, 0);
                admin.its.removeAlert(a);
                ((DefaultTableModel) resultsTable.getModel()).removeRow(row);

                JOptionPane.showMessageDialog(admin, "Alert "+a.getName()+" removed successfully!", "Information", JOptionPane.NO_OPTION);

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(admin, "Could not remove this alert, reason: "+ex.getMessage(), "Error", JOptionPane.NO_OPTION);
                ex.printStackTrace(System.out);
            }
        }
        if (e.getSource().equals(saveButton)) {
            saveChanges();
        }
        if (e.getSource().equals(runButton)) {
            SwingWorker aWorker = new SwingWorker(e) {

                protected void doNonUILogic() {
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    glass.setVisible(true);
                    try {
                        JSortTable resultsTable = null;
                        JTextArea resultsArea = null;

                        if (tabPane.getSelectedIndex() == 0) {
                            resultsTable = alertTable;
                            resultsArea = notesArea;
                        } else {
                            resultsTable = groupTable;
                            resultsArea = groupnotesArea;
                        }

                        int row = resultsTable.getSelectedRow();
                        if (row < 0) {
                            JOptionPane.showMessageDialog(admin, "You must first select an alert to run.", "Error", JOptionPane.NO_OPTION);
                            return;
                        }

                        Alert a = (Alert) resultsTable.getModel().getValueAt(row, 0);

                        // if the CQL in this alert is a narrative, open the narrative panel instead
                        if (a.getCQL().toUpperCase().indexOf("<NARRATIVE>") != -1) {
                            // SELECT <NARRATIVE> WHERE NARRATIVEID = X
                            Pattern p = Pattern.compile("\\s*NARRATIVEID\\s*=\\s*(.*?)\\ (.*)", Pattern.CASE_INSENSITIVE);
                            Matcher m = p.matcher(a.getCQL()); boolean bResult = m.find();

                            String sID = m.group(1); String sType = m.group(2);
                            if (sID.equals("")) { sID = sType; sType = "CACHE"; }


                            try {
                                Vector v = getConcepts(sID);

                                if (v.size() > 0) {
                                    NarrativeReportPanel ds = new NarrativeReportPanel (admin, v);
                                    ds.narrativeID = sID;
                                    ds.setVisible(true);
                                }
                            } catch (Exception e) { e.printStackTrace(System.out); }

                        } else {
                            SimpleDocSearch ds = new SimpleDocSearch(admin, a.getCQL());
                            ds.setVisible(true);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                }

                protected void doUIUpdateLogic() { }

                public void finished() {
                    super.finished();
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    glass.setVisible(false);
                }
            };
            aWorker.start();
        }
    }

    public Vector getConcepts(String documentID) {
        Vector v = new Vector();
        try { v = admin.its.getSimilarDocuments(documentID); }
        catch (Exception e) { e.printStackTrace(System.out); return new Vector(); }

        if (v.size() == 0) {
            JOptionPane.showMessageDialog(admin, "That alert did not return any similar documents.",
                "Information", JOptionPane.NO_OPTION);
            return new Vector();
        }

        return v;
    }

    public void saveTextChanges() { saveTextChanges(false); }
    public void saveTextChanges(boolean ask) {
        if (iTextArea != -1) {

            int i = 0;
            if (ask) {
                i = JOptionPane.showConfirmDialog(null, "Some the notes properties of this alert has changed.  Save changes?", "Save Changes?", JOptionPane.YES_NO_OPTION);
            }

            if (i == 0) { // save text changes
                JSortTable resultsTable = null; JTextArea resultsArea = null;
                int j = 0; if (bFlip) j = 1;

                if (tabPane.getSelectedIndex() == j) { resultsTable = alertTable; resultsArea = notesArea; }
                else { resultsTable = groupTable; resultsArea = groupnotesArea; }

                Alert a = (Alert) resultsTable.getModel().getValueAt(iTextArea, 0);
                a.setNotes(resultsArea.getText());

                try { admin.its.saveAlert(a); }
                catch (Exception e) {
                    e.printStackTrace(System.out);
                    JOptionPane.showMessageDialog(admin, "This alert could not be updated at this time.  Please see the log for more details.",
                        "Error", JOptionPane.NO_OPTION);
                }
            }
        }
        iTextArea = -1;
    }

    public void saveChanges() {
        JSortTable resultsTable = null;

        int j = 0; if (bFlip) j = 1;

        if (tabPane.getSelectedIndex() == j) { resultsTable = alertTable; }
        else { resultsTable = groupTable; }

        iTextArea = resultsTable.getSelectedRow();
        saveTextChanges(false);

        if (resultsTable.getSelectedRow() > -1) htChanges.put(new Integer(resultsTable.getSelectedRow()), "FreqDrop");

        Enumeration eH = htChanges.keys();
        while (eH.hasMoreElements()) {

            Integer i = (Integer) eH.nextElement();

            Alert a = (Alert) resultsTable.getModel().getValueAt(i.intValue(), 0);
            FrequencyDrop fd = (FrequencyDrop) resultsTable.getModel().getValueAt(i.intValue(), 1);

            a.setFreq(fd);

            try { admin.its.saveAlert(a); }
            catch (Exception e) {
                e.printStackTrace(System.out);
                JOptionPane.showMessageDialog(admin, "This alert could not be updated at this time.  Please see the log for more details.",
                    "Error", JOptionPane.NO_OPTION);
                htChanges.clear(); return;
            }
        }
        JOptionPane.showMessageDialog(admin, "Your alerts have been saved successfully.",
            "Error", JOptionPane.NO_OPTION);

        htChanges.clear();
    }

    public void buildTable(JSortTable table, Vector v) {
        DefaultSortTableModel dtm = new DefaultSortTableModel(v.size(), 2);

        table.setModel(dtm);
        table.setRowHeight(22);

        Vector v2 = new Vector();
        v2.add("Title");
        v2.add("Frequency");
        dtm.setColumnIdentifiers(v2);

        TableColumn freqColumn = table.getColumnModel().getColumn(1);
        freqColumn.setCellEditor(new FreqEditor());

        for (int i = 0; i < v.size(); i++) {
            Alert a = (Alert) v.elementAt(i);

            table.setValueAt(a, i, 0);
            table.setValueAt(a.getRunFrequency(), i, 1);

            FrequencyDrop fd = (FrequencyDrop) table.getValueAt(i, 1);
        }
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        saveTextChanges(true);

        Point p = ((MouseEvent) e).getPoint();
        JSortTable resultsTable = null; JTextArea resultsArea = null;

        if (tabPane.getSelectedIndex() == 0) { resultsTable = alertTable; resultsArea = notesArea; }
        else { resultsTable = groupTable; resultsArea = groupnotesArea; }

        int row = resultsTable.rowAtPoint(p);

        if ((row < 0) || (resultsTable == null)) { System.out.println("No table at point "+p+" row: "+row); return; }

        Alert a = (Alert) resultsTable.getModel().getValueAt(row, 0);

        resultsArea.setText(a.getNotes());
    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }

    public void keyPressed(KeyEvent evt) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent evt) {
        JTextArea resultsArea = null; JSortTable resultsTable = null;

        if (tabPane.getSelectedIndex() == 0) { resultsTable = alertTable; resultsArea = notesArea; }
        else { resultsTable = groupTable; resultsArea = groupnotesArea; }

        iTextArea = resultsTable.getSelectedRow();
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    private class FreqRenderer extends JComboBox implements TableCellRenderer {
        public FreqRenderer(Object[] items) {
            super(items);
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (value != null) {
                removeAllItems();
                addItem(value);
            }
            return this;
        }
    }

    public class FreqEditor extends AbstractCellEditor
            implements TableCellEditor, ActionListener {
        Object currentValue;
        JComboBox box;
        protected static final String EDIT = "edit";

        public FreqEditor() {
            box = new JComboBox(items);
            box.setActionCommand(EDIT);
            box.addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            saveTextChanges(true);

            JSortTable resultsTable = null; JTextArea resultsArea = null;

            if (tabPane.getSelectedIndex() == 0) { resultsTable = alertTable; resultsArea = notesArea; }
            else { resultsTable = groupTable; resultsArea = groupnotesArea; }

            int row = resultsTable.getSelectedRow();
            htChanges.put(new Integer(row), "JComboBox");

            resultsTable.getModel().setValueAt(box.getSelectedItem(), row, 1);
            currentValue = box.getSelectedItem();

            Alert a = (Alert) resultsTable.getModel().getValueAt(row, 0);
            resultsArea.setText(a.getNotes());
        }

        //Implement the one CellEditor method that AbstractCellEditor doesn't.
        public Object getCellEditorValue() {
            return currentValue;
        }

        //Implement the one method defined by TableCellEditor.
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column) {
            if (column == 0) currentValue = (String) value;
            else currentValue = ((FrequencyDrop) value);

            return box;
        }
    }
 }
