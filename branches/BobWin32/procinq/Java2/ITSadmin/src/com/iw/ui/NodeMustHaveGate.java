package com.iw.ui;

import com.iw.system.HashTree;
import com.iw.system.ITS;
import com.iw.system.ListMusthaveGateReport;
import com.iw.system.ModelMusthaveGateReport;
import com.iw.system.ModelReport;
import com.iw.system.MustHaveGateReport;
import javax.swing.JRadioButton;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.StringReader;
import java.util.Hashtable;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.dom4j.QName;

public class NodeMustHaveGate extends JInternalFrame implements ActionListener {

    private ITS its = null;
    private ITSAdministrator ITSframe = null;
    private ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JRadioButton rdbtnNewRadioButton;
    private JRadioButton rdbtnNewRadioButton_1;
    private JRadioButton rdbtnNewRadioButton_2;
    private JRadioButton rdbtnNewRadioButton_3;
    private JRadioButton rdbtnNewRadioButton_4;
    private JRadioButton rdbtnNewRadioButton_5;
    private JRadioButton rdbtnNewRadioButton_6;
    private JButton btnSend;
    private String corpusId;
    private String nodeId;
    private String musthavestype;
    private String musthavesscope;
    private String fromlevel;
    private String tolevel;
    private String level;
    private String textbyuser;
    private JTable table;
    private DefaultTableModel modelTable;

    public NodeMustHaveGate(ITSAdministrator frame, String corpusID, String nodeID) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);

        setTitle("Add Node Must Have Gate");
        setLocation(60, 60);
        setSize(525, 262);
        its = frame.its;
        ITSframe = frame;
        corpusId = corpusID;
        nodeId = nodeID;
        musthavestype = "1";
        musthavesscope = "1";
        fromlevel = "1";
        tolevel = "-1";

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        JLabel lblNodeMustHave = new JLabel("Node Must Have First Pass / Gate");
        lblNodeMustHave.setBounds(0, 0, 446, 15);
        getContentPane().add(lblNodeMustHave);

        rdbtnNewRadioButton = new JRadioButton("Use Direct Parent Must Have");
        rdbtnNewRadioButton.setBounds(20, 28, 234, 15);
        rdbtnNewRadioButton.setSelected(true);
        getContentPane().add(rdbtnNewRadioButton);

        rdbtnNewRadioButton_1 = new JRadioButton("Copy Parent Must Have from level");
        rdbtnNewRadioButton_1.setBounds(20, 47, 263, 15);
        getContentPane().add(rdbtnNewRadioButton_1);

        rdbtnNewRadioButton_2 = new JRadioButton("Use Taxonomy Must Have");
        rdbtnNewRadioButton_2.setBounds(20, 66, 205, 15);
        getContentPane().add(rdbtnNewRadioButton_2);

        rdbtnNewRadioButton_3 = new JRadioButton("Add user typed information");
        rdbtnNewRadioButton_3.setBounds(20, 85, 219, 15);
        getContentPane().add(rdbtnNewRadioButton_3);

        textField = new JTextField();
        textField.setBounds(291, 45, 28, 19);
        textField.setEnabled(false);
        textField.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40 || e.getKeyCode() == 45)) {
                    textField.setText(textField.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblParent = new JLabel("parent");
        lblParent.setBounds(330, 47, 56, 15);
        getContentPane().add(lblParent);

        textField_1 = new JTextField();
        textField_1.setBounds(247, 83, 139, 19);
        textField_1.setEnabled(false);
        getContentPane().add(textField_1);
        textField_1.setColumns(10);

        JSeparator separator = new JSeparator();
        separator.setBounds(61, 122, 164, 2);
        getContentPane().add(separator);

        rdbtnNewRadioButton_4 = new JRadioButton("To all Nodes Below Current Selection");
        rdbtnNewRadioButton_4.setBounds(20, 132, 294, 15);
        rdbtnNewRadioButton_4.setSelected(true);
        getContentPane().add(rdbtnNewRadioButton_4);

        rdbtnNewRadioButton.addActionListener(this);
        rdbtnNewRadioButton_1.addActionListener(this);
        rdbtnNewRadioButton_2.addActionListener(this);
        rdbtnNewRadioButton_3.addActionListener(this);

        rdbtnNewRadioButton_5 = new JRadioButton("To all direct children of current selection");
        rdbtnNewRadioButton_5.setBounds(20, 151, 319, 15);
        getContentPane().add(rdbtnNewRadioButton_5);

        rdbtnNewRadioButton_6 = new JRadioButton("To all nodes from Level");
        rdbtnNewRadioButton_6.setBounds(20, 170, 188, 15);
        getContentPane().add(rdbtnNewRadioButton_6);

        rdbtnNewRadioButton_4.addActionListener(this);
        rdbtnNewRadioButton_5.addActionListener(this);
        rdbtnNewRadioButton_6.addActionListener(this);

        textField_2 = new JTextField();
        textField_2.setBounds(211, 168, 28, 19);
        textField_2.setEnabled(false);
        textField_2.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    textField_2.setText(textField_2.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(textField_2);
        textField_2.setColumns(10);

        JLabel lblToLevel = new JLabel("to level");
        lblToLevel.setBounds(247, 170, 56, 15);
        getContentPane().add(lblToLevel);

        textField_3 = new JTextField();
        textField_3.setBounds(305, 168, 28, 19);
        textField_3.setEnabled(false);
        textField_3.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    textField_3.setText(textField_3.getText().replaceAll("[^0-9 -]", ""));
                }
            }
        });
        getContentPane().add(textField_3);
        textField_3.setColumns(10);

        JLabel lblNewLabel = new JLabel("below current selection");
        lblNewLabel.setBounds(340, 170, 178, 15);
        getContentPane().add(lblNewLabel);

        btnSend = new JButton("Send");
        btnSend.setBounds(392, 197, 117, 25);
        btnSend.addActionListener(this);
        getContentPane().add(btnSend);

        ButtonGroup groupFirstRadio = new ButtonGroup();
        groupFirstRadio.add(rdbtnNewRadioButton);
        groupFirstRadio.add(rdbtnNewRadioButton_1);
        groupFirstRadio.add(rdbtnNewRadioButton_2);
        groupFirstRadio.add(rdbtnNewRadioButton_3);

        ButtonGroup groupSecondRadio = new ButtonGroup();
        groupSecondRadio.add(rdbtnNewRadioButton_4);
        groupSecondRadio.add(rdbtnNewRadioButton_5);
        groupSecondRadio.add(rdbtnNewRadioButton_6);

        table = new JTable();
        modelTable = new DefaultTableModel(
                new Object[][]{},
                new String[]{
            "Node Id", "Node Name", "Links", "Signatures", "Must Haves", "Images"
        });
        table.setModel(modelTable);

        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == rdbtnNewRadioButton) {
            musthavestype = "1";
            textField.setEnabled(false);
            textField_1.setEnabled(false);
        }

        if (e.getSource() == rdbtnNewRadioButton_1) {
            musthavestype = "2";
            textField.setEnabled(true);
            textField_1.setEnabled(false);
        }

        if (e.getSource() == rdbtnNewRadioButton_2) {
            musthavestype = "3";
            textField.setEnabled(false);
            textField_1.setEnabled(false);
        }

        if (e.getSource() == rdbtnNewRadioButton_3) {
            musthavestype = "4";
            textField.setEnabled(false);
            textField_1.setEnabled(true);
        }

        if (e.getSource() == rdbtnNewRadioButton_4) {
            musthavesscope = "1";
            fromlevel = "1";
            tolevel = "-1";
            textField_2.setEnabled(false);
            textField_3.setEnabled(false);
        }

        if (e.getSource() == rdbtnNewRadioButton_5) {
            musthavesscope = "2";
            fromlevel = "1";
            tolevel = "1";
            textField_2.setEnabled(false);
            textField_3.setEnabled(false);
        }

        if (e.getSource() == rdbtnNewRadioButton_6) {
            musthavesscope = "3";
            textField_2.setEnabled(true);
            textField_3.setEnabled(true);
        }

        //send data for server
        //corpusId, nodeId, musthavestype, musthavesscope, fromlevel, tolevel, level, textbyuser    
        if (e.getSource() == btnSend) {
            try {
                if (validateData()) {
                    Hashtable htArgs = its.getArguments();
                    if (musthavestype.equals("2")) {
                        htArgs.put("level", textField.getText());
                    }

                    if (musthavestype.equals("4")) {
                        htArgs.put("textbyuser", textField_1.getText());
                    }

                    htArgs.put("nodeid", nodeId);
                    htArgs.put("musthavestype", musthavestype);
                    htArgs.put("musthavesscope", musthavesscope);
                    htArgs.put("fromlevel", textField_2.getText());
                    htArgs.put("tolevel", textField_3.getText());
                    htArgs.put("corpusid", corpusId);

                    org.dom4j.Document htResults = its.InvokeDomAPI("tsnode.TSMusthaveFirstPass", htArgs);
                    showNodesChanged(htResults);
                }
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
            }
        }
    }

    private boolean validateData() {
        int levelI = 0;
        int fromlevelI = 0;
        int tolevelI = 0;

        if (musthavestype.equals("2")) {
            if (!(textField.getText().length() > 0)) {
                JOptionPane.showMessageDialog(this, "parent level can not be empty");
                return false;
            } else {
                levelI = Integer.parseInt(textField.getText());
            }
        } else if (musthavestype.equals("4")) {
            if (!(textField_1.getText().length() > 0)) {
                JOptionPane.showMessageDialog(this, "add user typed information can not be empty");
                return false;
            }
        }

        if (musthavesscope.equals("3")) {
            if (!(textField_2.getText().length() > 0 || textField_3.getText().length() > 0)) {
                JOptionPane.showMessageDialog(this, "from level and to level can not be empty");
                return false;
            } else {
                fromlevelI = Integer.parseInt(textField_2.getText());
                tolevelI = Integer.parseInt(textField_3.getText());
            }

            if (!(tolevelI > -2)) {
                JOptionPane.showMessageDialog(this, "to level must be greater than -2");
                return false;
            }

            if (musthavestype.equals("2")) {
                if (!(levelI < fromlevelI && fromlevelI < tolevelI || tolevelI == -1)) {
                    JOptionPane.showMessageDialog(this, "to level must be greater than or equal to from level 1, from level must be greater than parent level");
                    return false;
                }
            }
        }
        return true;
    }

    private void showNodesChanged(org.dom4j.Document htResults) {
        JInternalFrame showChange = new JInternalFrame("Upgraded Nodes");
        showChange.setSize(675, 362);
        showChange.setResizable(false);
        showChange.setClosable(true);
        showChange.setIconifiable(true);
        showChange.setFrameIcon(ITSframe.iIndraweb);
        showChange.getContentPane().setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(1, 1, 662, 340);
        showChange.getContentPane().add(scrollPane);

        modelTable = new DefaultTableModel(
                new Object[][]{},
                new String[]{
            "Node Id", "Node Title", "Updated Value"
        }) {
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
        table.setModel(modelTable);
        scrollPane.setViewportView(table);
        //----------------------------------------------------------------------
        try {
            org.dom4j.Document nodes = htResults;
            JAXBContext jc = JAXBContext.newInstance(MustHaveGateReport.class);
            StringReader reader = new StringReader(nodes.getRootElement().element(new QName("RESULT")).asXML());

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            MustHaveGateReport nodesResult = (MustHaveGateReport) unmarshaller.unmarshal(reader);

            if (nodesResult.getData() != null) {
                for (ListMusthaveGateReport data : nodesResult.getData()) {
                    for (ModelMusthaveGateReport list : data.getList()) {
                        modelTable.addRow(new Object[]{list.getNodeId(), list.getNodeTitle(), data.getValueUpdated() == null ? "null" : data.getValueUpdated()});
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        //----------------------------------------------------------------------
        ITSframe.jdp.add(showChange);
        showChange.setVisible(true);
    }
}
