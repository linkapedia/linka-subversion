/*
 * Operations to sql
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
 */

package com.iw.tools.webdav.sqlite;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.jfree.util.Log;

import com.iw.tools.ConfClient;
import com.iw.tools.ManageResource;
import com.iw.tools.webdav.tools.*;

public class SqlServerAdmin {
	private static String RUTA_DB;
	private Connection con;

	static {
		try {
			RUTA_DB = ManageResource.getResource(
					"files/Webdav/bd_serverAdmin.sqlite").toString();
		} catch (Exception e) {
			Log.error("Error get resource sqlite:" + e.getMessage());
		}

	}

	/**
	 * Constructor define the connection
	 */
	public SqlServerAdmin() {
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:" + RUTA_DB);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * close the connection
	 */
	private void closeConnection() {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * insert a register into database
	 * 
	 * @param name
	 * @param hostName
	 * @param user
	 * @param password
	 * @param path
	 * @return true or false according to the result
	 */
	public boolean insert(String name, String hostName, String user,
			String password, String path) {
		try {
			Statement cliente = con.createStatement();
			cliente.executeUpdate("insert into info values ('" + name + "','"
					+ hostName + "','" + user + "','" + password + "','" + path
					+ "');");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			closeConnection();
		}
		return true;

	}

	/**
	 * delete a register into database
	 * 
	 * @param name
	 *            : delete register with parameter name
	 * @return true or false according to the result
	 */
	public boolean delete(String name) {
		try {
			Statement cliente = con.createStatement();
			cliente.executeUpdate("delete from info where name='" + name + "';");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			closeConnection();
		}
		return true;
	}

	/**
	 * edit a register into database search to name
	 * 
	 * @param name
	 * @param hostName
	 * @param user
	 * @param password
	 * @param path
	 * @return true or false according to the result
	 */
	public boolean edit(String name, String hostName, String user,
			String password, String path) {
		try {
			Statement cliente = con.createStatement();
			cliente.executeUpdate("update info set hostname='" + hostName
					+ "', user ='" + user + "', password ='" + password
					+ "', path='" + path + "'" + " where name='" + name + "';");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			closeConnection();
		}
		return true;
	}

	/**
	 * consult all registers into database
	 * 
	 * @return registers or null according to the result
	 */
	public ResultSet consult() {
		try {
			Statement cliente = con.createStatement();
			ResultSet res = cliente.executeQuery("select * from info");
			return res;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;

	}
}
