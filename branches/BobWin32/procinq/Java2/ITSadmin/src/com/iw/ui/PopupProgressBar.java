package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class PopupProgressBar extends JInternalFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 480318195043617842L;

	protected JProgressBar progress;

    private ITSAdministrator itsframe = null;

    public PopupProgressBar(ITSAdministrator frame, int start, int end) {
        super("Progress", false, false, false, false);
        build(frame,start,end);
    }
    public PopupProgressBar(ITSAdministrator frame,String title, int start, int end) {
    	super("Progress -> "+title, false, false, false, false);
    	build(frame, start, end);
    }
    private void build(ITSAdministrator frame, int start, int end){
        itsframe = frame;

        pack();
        setSize(275, 75);
        openAtCenter();

        if (end < start) {
            System.out.println("Warning! End value on progress bar is greater than start value!");
            end = start+1;
        }

        progress = new JProgressBar(start, end);
        progress.setStringPainted(true);

        getContentPane().setLayout(new BorderLayout());
        JPanel jpm = new JPanel();
        jpm.add(progress,BorderLayout.NORTH);
        jpm.add(new JLabel("Loading... please wait"),BorderLayout.CENTER);

        setFrameIcon(itsframe.iIndraweb);
        getContentPane().add(jpm);
        frame.jdp.add(this);
        
    }

    public void setProgress(int i) { progress.setValue(i); }

    public void openAtCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 2);
    }
}
