package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class XtmOut extends ExportElement {
    public String corpusID = null;
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {
        corpusID = c.getID();
        if (ppbin != null) this.ppb = ppbin;

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        File f = new File(c.getName().replaceAll(" ", "_")+".xtm");
        if (f.exists()) { throw new Exception("Error: File: "+f.getAbsolutePath()+" already exists."); }

        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<topicMap id=\""+c.getName().replaceAll(" ", "_")+c.getID()+"\" xmlns=\"http://www.topicmaps.org/xtm/1.0/\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" ORDER BY NODEID ASC", 1, 15000);

        out.write("<topic id=\"parent-child\">\n");
        out.write("   <baseName id=\"Parent-Child-Association\">\n");
        out.write("      <baseNameString>Hierarchy</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"parent\">\n");
        out.write("   <baseName id=\"Parents\">\n");
        out.write("      <baseNameString>Parents</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"child\">\n");
        out.write("   <baseName id=\"Children\">\n");
        out.write("      <baseNameString>Children</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"parentid\">\n");
        out.write("   <baseName id=\"Parent\">\n");
        out.write("      <baseNameString>Parent</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"size-obj\">\n");
        out.write("   <baseName id=\"Size\">\n");
        out.write("      <baseNameString>Size</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"niwp-obj\">\n");
        out.write("   <baseName id=\"Index\">\n");
        out.write("      <baseNameString>Index Within Parent</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"depth-obj\">\n");
        out.write("   <baseName id=\"Depth\">\n");
        out.write("      <baseNameString>Depth</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        out.write("<topic id=\"sig-obj\">\n");
        out.write("   <baseName id=\"Signatures\">\n");
        out.write("      <baseNameString>Signatures</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>\n");

        /*
        out.write("<topic id=\"child-of\">\n");
        out.write("   <baseName id=\"child-of5\">\n");
        out.write("      <baseNameString>Has Child</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("   <baseName id=\"parent-scope\">\n");
        out.write("      <scope>\n");
        out.write("         <topicRef xlink:href=\"#parent\" />\n");
        out.write("      </scope>\n");
        out.write("      <baseNameString>Has Child</baseNameString>\n");
        out.write("   </baseName>\n");
        out.write("</topic>");
        */

        Hashtable ht = new Hashtable();

        for (int i = 0; i < vNodes.size(); i++) {
            // *** WRITE NODE INFORMATION HERE
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);
            Vector vHave = getMustHaves(n, c, server);
            writeNode(n, signatures, out, vHave);
            ht.put(n.get("NODEID"), n.get("NODETITLE"));
        }

        for (int i = 0; i < vNodes.size(); i++) {
            // *** WRITE NODE RELATIONSHIP INFORMATION HERE
            Node n = (Node) vNodes.elementAt(i);
            out.write("<association>\n");
            out.write("   <instanceOf><topicRef xlink:href=\"#parent-child\"/></instanceOf>\n");
            out.write("   <member>\n");
            out.write("      <roleSpec>\n");
            out.write("         <topicRef xlink:href=\"#child\" />\n");
            out.write("      </roleSpec>\n");
            out.write("      <topicRef xlink:href=\"#"+generateID(n)+"\" />\n");
            out.write("   </member>\n");
            out.write("   <member>\n");
            out.write("      <roleSpec>\n");
            out.write("         <topicRef xlink:href=\"#parent\" />\n");
            out.write("      </roleSpec>\n");
            out.write("      <topicRef xlink:href=\"#"+generateID(n.get("PARENTID"), (String) ht.get(n.get("PARENTID")))+"\" />\n");
            out.write("   </member>\n");
            out.write("</association>\n");
        }

        out.write("</topicMap>\n");

        out.close();

        return f;
    }

    public Vector getMustHaves(ITSTreeNode n, Corpus c, ITS server) throws Exception {
        Vector output = new Vector();
        Vector input = server.getMusthaves(n.get("NODEID"));

        // get all thesauri associated with this corpus
        Vector thesauri = new Vector();

        try { thesauri = server.getThesauri(c); } catch (Exception e) { } // do nothing if none found

        // new ** thesaurus expand each of these must have terms if they are an exact match
        for (int i = 0; i < input.size(); i++) {
            String musthave = (String) input.elementAt(i);

            Vector thesaurusWords = server.getSynonyms(thesauri, musthave.trim().toLowerCase());

            output.add(musthave);

            for (int j = 0; j < thesaurusWords.size(); j++) {
                ThesaurusWord tw = (ThesaurusWord) thesaurusWords.elementAt(j);
                output.add(tw.getName());
            }
            System.out.println("Thesaurus expand resulted in "+thesaurusWords.size()+" new must-have terms.");
        }

        return output;
    }

    public void writeNode(ITSTreeNode n, Signatures s, Writer out, Vector vHave) throws IOException {
    	
        System.out.println("Exporting node: "+n.get("NODETITLE"));
    	
        out.write("<topic id=\""+generateID(n)+"\">\n");
        out.write("   <instanceOf><topicRef xlink:href=\"#node\" /></instanceOf>\n");
        out.write("   <baseName> <baseNameString>"+getEncodeName(n.get("NODETITLE"))+"</baseNameString> </baseName>\n");
        out.write("   <occurrence>\n");
        out.write("      <instanceOf>");
        out.write("         <topicRef xlink:href=\"#size-obj\"></topicRef>");
        out.write("      </instanceOf>");
        out.write("      <resourceData id=\"NodeSize\">"+getEncodeName(n.get("NODESIZE"))+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("   <occurrence>\n");
        out.write("      <instanceOf>");
        out.write("         <topicRef xlink:href=\"#parentid\"></topicRef>");
        out.write("      </instanceOf>");
        out.write("      <resourceData id=\"ParentId\">"+getEncodeName(n.get("PARENTID"))+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("   <occurrence>\n");
        out.write("      <instanceOf>");
        out.write("         <topicRef xlink:href=\"#niwp-obj\"></topicRef>");
        out.write("      </instanceOf>");
        out.write("      <resourceData id=\"NodeIndexWithinParent\">"+getEncodeName(n.get("NODEINDEXWITHINPARENT"))+"</resourceData>\n");
        out.write("   </occurrence>\n");
        
        out.write("   <occurrence>\n");
        out.write("      <instanceOf>");
        out.write("         <topicRef xlink:href=\"#niwp-obj\"></topicRef>");
        out.write("      </instanceOf>");
        out.write("      <resourceData id=\"MustHaveTerms\">"+getEncodeName(GetMustHave(vHave))+"</resourceData>\n");
        out.write("   </occurrence>\n");
        
        out.write("   <occurrence>\n");
        out.write("      <instanceOf>");
        out.write("         <topicRef xlink:href=\"#depth-obj\"></topicRef>");
        out.write("      </instanceOf>");
        out.write("      <resourceData id=\"DepthFromRoot\">"+getEncodeName(n.get("DEPTHFROMROOT"))+"</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("   <occurrence>\n");
        out.write("      <instanceOf>");
        out.write("         <topicRef xlink:href=\"#sig-obj\"></topicRef>");
        out.write("      </instanceOf>");
        out.write("      <resourceData id=\"Signatures\">" + getEncodeName(s.getTopicMapString()) + "</resourceData>\n");
        out.write("   </occurrence>\n");
        out.write("</topic>\n");

        nodesProcessed++;
        if (ppb != null) {
            long perc = (nodesProcessed * 100) / totalNodes;
            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
        }
    }

    public String generateID(Node n) {
        return generateID((String) n.get("NODEID"), (String) n.get("NODETITLE"));
    }
    public String generateID(String NodeID, String NodeTitle) {
        if (NodeTitle == null) {
            System.out.println("Node Title is null for: "+NodeID); NodeTitle = "X";
        }
        if (NodeTitle.length() > 15) NodeTitle = NodeTitle.substring(4,15);

        StringBuffer sb = new StringBuffer("top"+getEncodeName(NodeTitle)+"X-"+NodeID);
        StringBuffer sbout = new StringBuffer();

        for (int i = 0; i < sb.length(); i++) {
            //System.out.println("i: "+i+" (of "+sb.length()+")");
            if (Character.isLetterOrDigit(sb.charAt(i)))
                sbout.append(sb.charAt(i));
        }

        return sbout.toString();
    }
    
    public static String getEncodeName(String s) {
        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        s = s.replaceAll("\"", "&quot;");
        s = s.replaceAll(">", "&gt;");

        return s;
    }
 private String GetMustHave(Vector vHave){
    	
    	StringBuffer buffer = new StringBuffer();
        if (vHave.size() >0){
        	for (int i = 0; i < vHave.size(); i++) {
        		
        		buffer.append(vHave.elementAt(i).toString().trim());
        		if (i == 0 & vHave.size()>1){
        			buffer.append(" || ");
        		}
        		if (i !=0 & i !=vHave.size() -1 ){
        			buffer.append(" || ");
        		}
        		
            
            }
          }
        
        
    
    	// output must haves from data structure
    	
    return buffer.toString();
    }
    
} 