package com.iw.tools;

import java.io.IOException;
import java.util.*;

import org.xml.sax.*;
import org.dom4j.*;
import com.iw.ui.PopupProgressBar;
import com.iw.system.PDFelement;
import com.iw.system.ParagraphElement;
import com.iw.system.ITS;

public class PDFimportHandlerPass1
        implements ElementHandler {

    public Hashtable htElements = new Hashtable(); // all distinct pdf element objects
    public Vector vOrder = new Vector();

    private float lastY = -1;
    private String lastID = "";
    private ParagraphElement pe = null;
    private Vector vPDFelements = new Vector();

    // constructor
    public PDFimportHandlerPass1 () { super(); }

    public void onStart(ElementPath ep) {
    }

    public void onEnd(ElementPath ep) {
        if (ep.getPath().endsWith("TEXT")) {
            PDFelement pdf = new PDFelement(ep);

            pdf.example[0] = truncate(ep.getCurrent().getText());

            if (ep.getCurrent().getText().length() < 2) return;

            vPDFelements.add(pdf);
        } else if (ep.getPath().endsWith("PARAGRAPH")) {
            pe = new ParagraphElement(ep);

            for (int i = 0; i < vPDFelements.size(); i++) {
                PDFelement pdf = (PDFelement) vPDFelements.elementAt(i);
                pdf.setParagraph(pe);

                String ID = pdf.getID();
                if (allCaps(pdf.example[0])) ID = ID + "-allcaps";

                if (!ITS.getUser().sameLines()) {
                    if (lastY == pdf.Y) { ID = lastID; }
                    else { lastID = ID; lastY = pdf.Y; }
                }

                //System.out.println("ID: "+ID+" Text: "+pdf.example[0]+" Y: "+pdf.Y+" lastY: "+lastY+" lastID: "+lastID);

                if (!htElements.containsKey(ID)) {
                    //System.out.println("*********** DOES NOT CONTAIN: "+ID);
                    htElements.put(ID, pdf);
                    vOrder.add(ID);

                    //System.out.print(ID+" "+pdf);
                } else {
                    //System.out.println("*********** CONTAINS: "+ID);
                    PDFelement pdfFinal = (PDFelement) htElements.get(ID);
                    pdfFinal.addExample(pdf);

                    htElements.put(ID, pdfFinal);
                    //System.out.println("PUT "+ID+" "+pdf);
                }
            }
            //System.out.println(" ");
            vPDFelements.clear();
        }
    }

    private boolean allCaps(String s) {
        char[] cArr = s.toCharArray();

        for (int i = 0; i < cArr.length; i++) {
            int asc = (int) cArr[i];
            if ((asc > 96) && (asc < 123)) { return false; }
        }

        return true;
    }

    private String truncate(String s) {
        if (s.length() < 80) return s;
        return s.substring(0, 76)+"...";
    }
 }

