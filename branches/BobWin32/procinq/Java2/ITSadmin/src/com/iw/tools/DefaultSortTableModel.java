package com.iw.tools;

import java.util.*;
import javax.swing.table.*;

public class DefaultSortTableModel
        extends DefaultTableModel
        implements SortTableModel {

    private boolean bEditable = true;

    public DefaultSortTableModel() {}

    public DefaultSortTableModel(int rows, int cols) {
        super(rows, cols);
    }

    public DefaultSortTableModel(Object[][] data, Object[] names) {
        super(data, names);
    }

    public DefaultSortTableModel(Object[] names, int rows) {
        super(names, rows);
    }

    public DefaultSortTableModel(Vector names, int rows) {
        super(names, rows);
    }

    public DefaultSortTableModel(Vector data, Vector names) {
        super(data, names);
    }

    public void setEditable(boolean Editable) { bEditable = Editable; }

    public boolean isSortable(int col) {
        return true;
    }

    public boolean isCellEditable(int col) {
        return bEditable;
    }
    public boolean isCellEditable(int col, int row) {
        return bEditable;
    }

    public void sortColumn(int col, boolean ascending) {
        Collections.sort(getDataVector(),
                new ColumnComparator(col, ascending));
    }

    public void setValueAt(Object aValue, int row, int column) {
        Vector rowVector = (Vector)dataVector.elementAt(row);

        try {
            int num = Integer.parseInt(aValue.toString());
            rowVector.setElementAt(new Integer(num), column);
        } catch (NumberFormatException exp) {

            rowVector.setElementAt(aValue, column);
        }

        fireTableCellUpdated(row, column);
    }
}

