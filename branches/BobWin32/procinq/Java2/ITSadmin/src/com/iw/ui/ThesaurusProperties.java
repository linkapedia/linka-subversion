package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.tools.*;
import com.iw.tools.SwingWorker;

public class ThesaurusProperties extends OculusBox implements ActionListener {
    // widgets
    protected JTextField nameText;
    protected JButton saveButton;
    protected JButton cancelButton;

    // objects of interest
    private Thesaurus thesaurus;
    private ITSAdministrator ITSframe;
    private JInternalFrame frame;
    private Component glass;

    public ThesaurusProperties(ITSAdministrator ITSframe, Thesaurus thesaurus) {
        this.thesaurus = thesaurus; this.ITSframe = ITSframe;

        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        frame = new JInternalFrame("Edit Thesaurus Properties", false, true, true, false);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setFrameIcon(ITSframe.iIndraweb);

        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        nameText = new JTextField(thesaurus.getName());
        saveButton = new JButton("Save");
        saveButton.addActionListener(this);
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);

        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
            {
                layout.addSpace(10);
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(new JLabel("Thesaurus Name: "));
                    layout.add(nameText);
                    layout.parent();
                }
                layout.parent();
            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(saveButton);
            layout.addSpace(10);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(10);

        frame.setLocation(30, 70);
        glass = frame.getGlassPane();

        frame.setPreferredSize(new Dimension(450, 150));
        ITSframe.jdp.add(frame);

        frame.getRootPane().setDefaultButton(saveButton);

        frame.setContentPane(this);
        frame.pack();
        frame.show();
    }

    public void actionPerformed(ActionEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {
            public SortedList sl = null;

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                // close frame if cancel was selected
                if (e.getSource().equals(cancelButton)) frame.dispose();
                if (e.getSource().equals(saveButton)) {
                    try { ITSframe.its.saveThesaurusProps(thesaurus.getID(), nameText.getText()); }
                    catch (Exception e) {
                        JOptionPane.showMessageDialog(ITSframe, "Error: Could not save thesaurus properties.", "Error", JOptionPane.NO_OPTION);
                        e.printStackTrace(System.out);
                        return;
                    }
                    JOptionPane.showMessageDialog(ITSframe, "Thesaurus properties changed successfully.", "Information", JOptionPane.NO_OPTION);
                    frame.dispose();
                }

            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }
}

