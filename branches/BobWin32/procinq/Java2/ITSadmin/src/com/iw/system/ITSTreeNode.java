package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import com.iw.tools.PubliclyCloneable;
import com.iw.tools.NodeTransferringTree;

import java.util.*;
import java.io.Serializable;

import org.dom4j.Element;

public class ITSTreeNode extends com.iw.system.Node implements PubliclyCloneable, Serializable, Comparable {
    // pointer to the tree that this node belongs to.
    private NodeTransferringTree Tree;
    protected Hashtable htn = new Hashtable();

    // constructor(s)
    public ITSTreeNode () { super(); }
    public ITSTreeNode (HashTree ht) {
        htn = new Hashtable();

        Enumeration eN = ht.keys();
        while (eN.hasMoreElements()) {
            String key = (String) eN.nextElement();
            String val = (String) ht.get(key);

            htn.put(key, val);
        }

        if (!htn.containsKey("NODESIZE")) htn.put("NODESIZE", "0");
    }
    public ITSTreeNode (Hashtable ht) {
        if (htn == null) htn = new Hashtable();
        htn = ht;

        if (!htn.containsKey("NODESIZE")) htn.put("NODESIZE", "0");
    }
    public ITSTreeNode (Vector v) {
        if (htn == null) htn = new Hashtable();
        for (int i = 0; i < v.size(); i++) {
            VectorValue vv = (VectorValue) v.elementAt(i);
            htn.put(vv.getName(), vv.getValue());
        }
    }
    public ITSTreeNode (Iterator i) {
        if (htn == null) htn = new Hashtable();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            htn.put(e.getName(), e.getText());
        }
    }

    public NodeDocument getNodeDocument (Document d) {
        NodeDocument nd = new NodeDocument(d);

        if (htn != null) {
            // inherit fields from node
            Enumeration eN = htn.keys();
            while (eN.hasMoreElements()) {
                String key = (String) eN.nextElement();
                String val = (String) htn.get(key);

                nd.htnd.put(key, val);
            }
        }

        return nd;
    }

    // accessor function -- 5/13/2004 only one accessor function required
    public String get(String fieldName) {
        String s = (String) htn.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }
        return s.replaceAll(" <br> ", "\n");
    }
    // 1/25/2006, adding a second accessor method for TEF output
    public String getForTef(String fieldName) {
        String s = (String) htn.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }

        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        s = s.replaceAll(">", "&gt;");

        return s.replaceAll(" <br> ", "\n");
    }

    public void set(String Key, String Value) {
        if (Key.equals("ID")) { htn.put("NODEID", Value); } // some of these values are required by the SAX parser
        else if (Key.equals("BASENAMESTRING")) { htn.put("NODETITLE", Value); }
        else if (Key.equals("INSTANCEOF")) { htn.put("CORPUSID", Value); }
        else if (Key.equals("SIGNATURES")) { setSignatures(Value); }
        else { htn.put(Key, Value); }
    }

    // accessor functions
    public NodeTransferringTree getTree() { return Tree; }
    public void setTree(NodeTransferringTree ntt) { Tree = ntt; }
    public Hashtable getHash() { return htn; }
    public void setNodeTitle(String nodeTitle)
    {
    	htn.put("NODETITLE", nodeTitle);
    }
    public String toString() {
        if (htn.containsKey("NODETITLE")) { return (String) htn.get("NODETITLE"); }

        return "Untitled Node";
    }
    public Object clone() {
        ITSTreeNode n = new ITSTreeNode();
        n.htn = getHash();

        return n;
    }
}
