package com.iw.ui;

import org.dom4j.io.SAXReader;
import org.dom4j.*;
import org.xml.sax.*;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.*;
import java.awt.event.*;
import java.util.Iterator;
import java.util.List;

import com.iw.tools.*;
import com.iw.ui.ITSAdministrator;
import com.iw.system.Corpus;

import com.oculustech.layout.*;

import javax.swing.*;

public class TopicMapImport {
    protected JProgressBar progressBar;
    public static int numberNodes = 0;

    private JInternalFrame MDIframe;
    private File file = null;
    private ITSAdministrator itsadmin = null;

    public static void importTopicMap (File f, ITSAdministrator frame) throws Exception {
        System.setProperty("org.xml.sax.driver", "org.apache.xerces.parsers.SAXParser");

        if (!f.exists()) {
            JOptionPane.showMessageDialog(null, "File: "+f.getAbsolutePath()+" not found.", "Information", JOptionPane.NO_OPTION);
            return;
        }

        TopicSizeHandler counter = new TopicSizeHandler();

        // count number of nodes in the file
        XMLReader xmlReaderCount = XMLReaderFactory.createXMLReader();
        xmlReaderCount.setEntityResolver(counter);
        xmlReaderCount.setErrorHandler(counter);
        xmlReaderCount.setContentHandler(counter);
        xmlReaderCount.setDTDHandler(counter);

        FileReader r = new FileReader(f);
	    try { xmlReaderCount.parse(new InputSource(r)); }
        catch (SAXParseException e) { throw e; }

        r.close(); r = null;

        numberNodes = counter.Size;
        PopupProgressBar ppb = new PopupProgressBar(frame, 1, counter.Size);
        ppb.setName("Import "+f.getName()); ppb.pack(); ppb.show();

        FileInputStream fis = new FileInputStream(f);
        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);

        Element eTopicMap = doc.getRootElement();

        String sCorpusName = GoogleNews.clean(f.getName());
        int iCorpusID = Integer.parseInt(getNodeIDfromString(eTopicMap.attribute("id").getText()));
        Corpus c = frame.its.addCorpus(sCorpusName, iCorpusID);

        // loop through each topic in the topic map
        Iterator it = eTopicMap.elements("topic").iterator(); int loop = 0;
        while (it.hasNext()) {
            Element eTopic = (Element) it.next();
            String sID = getNodeIDfromString(eTopic.attribute("id").getText());
            String sName = getSource(eTopic.element("baseName")).replaceAll("\n", "").trim();

            com.iw.system.Node n = new com.iw.system.Node();
            n.set("NODEID", sID);
            n.set("NODETITLE", sName);
            n.set("CORPUSID", c.getID());

            int attributes = 0;

            Iterator it3 = eTopic.elements("occurrence").iterator();
            while (it3.hasNext()) {
                Element eOcc = (Element) it3.next();

                // iterate through each attribute of the node, if applicable
                Iterator it2 = eOcc.elements("resourceData").iterator();
                while (it2.hasNext()) {
                    Element eResource = (Element) it2.next();
                    String attributeName = eResource.attribute("id").getText();

                    if (attributeName.toLowerCase().equals("signatures")) {
                        if (!eResource.getText().trim().equals("")) n.setSignatures(eResource.getText());
                    } else {
                        attributes++;
                        n.set(attributeName.toUpperCase(), eResource.getText());
                    }
                }
            }

            // if the node has one or more attributes, add it.
            if (attributes > 0) {
                loop++; frame.its.addNodeFromTopicMap(n); ppb.setProgress(loop);
            }
        }
        ppb.setProgress(counter.Size);
        ppb.dispose();

        CorpusManagement DragAndDrop = new CorpusManagement(frame, c);

        DragAndDrop.setBounds(50, 50, 850, 550);
        DragAndDrop.setVisible(true);
        DragAndDrop.setDefaultCloseOperation(
                WindowConstants.DISPOSE_ON_CLOSE);

        frame.jdp.add(DragAndDrop);
        DragAndDrop.moveToFront();

        frame.jdp.setVisible(true);
    }

    public static String getSource (Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) { sb.append(((Text) obj).getText()); }
            if (obj instanceof Element) { sb.append(getSource((Element) obj)); }
        }

        return sb.toString();
    }

    public static String getNodeIDfromString (String str) {
        String sout = "";

        char ch;       // One of the characters in str.
        int i;         // A position in str, from 0 to str.length()-1.
        for (i = str.length()-1; i >= 0; i--) {
            ch = str.charAt(i);
            if (Character.isDigit(ch))
                sout = ch + sout;
            else i = -1;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static String clean(String str) {
        String sout = "";

        char ch;       // One of the characters in str.
        int i;         // A position in str, from 0 to str.length()-1.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch))
                sout = sout + Character.toLowerCase(ch);
        }

        //System.out.println("return: "+sout);
        return sout;
    }
}

