/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Mar 12, 2004
 * Time: 4:45:59 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui.conceptalerts;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.util.Vector;
import java.util.Hashtable;
import java.net.URLEncoder;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.ui.ITSAdministrator;
import com.iw.ui.PopupProgressBar;
import com.iw.system.NodeDocument;
import com.iw.system.ITSTreeNode;
import com.iw.system.Document;
import com.iw.tools.SwingWorker;
import com.iw.tools.JSortTable;
import com.iw.tools.DefaultSortTableModel;

public class NarrativePanel extends OculusBox implements ActionListener, ChangeListener {
    protected JLabel titleLabel;
    protected JTextField titleText;
    protected JTextArea narrativeText;
    protected JSortTable conceptTable;
    protected JButton okButton;
    protected JButton cancelButton;
    protected JButton classifyButton;
    protected JButton removeButton;
    protected JTabbedPane jTabbedPane1;

    private ConceptAlerts ca;
    private ITSAdministrator itsframe;
    private JInternalFrame frame;
    private Component glass;
    private Document document; // document identifier, only present if editing an existing narrative

    public NarrativePanel(ConceptAlerts ca, ITSAdministrator ITSframe) {
        this.itsframe = ITSframe; this.ca = ca;

        initFrame();
        frame.show();
    }

    public NarrativePanel(ConceptAlerts ca, ITSAdministrator ITSframe, Document d) {
        this.itsframe = ITSframe; this.ca = ca; this.document = d;

        Vector v;
        try { v = itsframe.its.CQL("SELECT <NODEDOCUMENT> WHERE DOCUMENTID = "+d.get("DOCUMENTID")); }
        catch (Exception e) {
            e.printStackTrace(System.out);
            JOptionPane.showMessageDialog(ITSframe, "Warning: This narrative does not currently contain any topics.",
                    "Warning", JOptionPane.NO_OPTION);
            v = new Vector();
        }

        initFrame();

        // populate initial fields
        if (v.size() > 0) fillConceptTable(v);
        titleText.setText(d.get("DOCTITLE"));
        narrativeText.setText(d.get("DOCUMENTSUMMARY"));

        frame.show();
    }

    public void initFrame () {
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        frame = new JInternalFrame("Narrative Editor ("+ca.getNarrative().getNarrativeName()+")", true, true, false, true);
        frame.setLocation(30, 30);
        //frame.setSize(675, 225);
        frame.setBackground(Color.lightGray);
        frame.setFrameIcon(itsframe.iIndraweb);
        glass = frame.getGlassPane();

        initComponents();

        frame.setContentPane(this);
        frame.pack();

        itsframe.jdp.add(frame);
    }

    private void initComponents() {
        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        jTabbedPane1 = new JTabbedPane();
        jTabbedPane1.addChangeListener(this);

        titleLabel = new JLabel("Title:");
        titleLabel.setToolTipText("Select the title of this narrative.");

        titleText = new JTextField();
        titleText.setToolTipText("Select the title of this narrative.");

        narrativeText = new JTextArea();
        narrativeText.setToolTipText("Insert the text from your narrative.");
        narrativeText.setLineWrap(true);

        conceptTable = new JSortTable();

        okButton = new JButton("OK");
        okButton.setToolTipText("Accept changes and close this window.");
        okButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setToolTipText("Reject changes since last save and close this window.");
        cancelButton.addActionListener(this);

        classifyButton = new JButton("Classify");
        classifyButton.setToolTipText("Classify this narrative and refresh the results in the concept tab.");
        classifyButton.addActionListener(this);

        removeButton = new JButton("Remove");
        removeButton.setToolTipText("Remove a topic result from this narrative.");
        removeButton.addActionListener(this);
        removeButton.setEnabled(false);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.addSpace(10);
                layout.nestContainer(jTabbedPane1);
                {
                    layout.nestBoxAsTab("Narrative",OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                    {
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.nestGrid(2,1);
                                {
                                    layout.add(titleLabel);
                                    layout.add(titleText);
                                    layout.parent();
                                }
                                layout.add(new JScrollPane(narrativeText));
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.nestBoxAsTab("Concepts",OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.add(new JScrollPane(conceptTable));
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.add(okButton);
                    layout.addSpace(10);
                    layout.add(cancelButton);
                    layout.addSpace(10);
                    layout.add(classifyButton);
                    layout.addSpace(10);
                    layout.add(new JSeparator());
                    layout.add(removeButton);
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);
    }

    public String getNodeTree(String NodeID) {
        Vector v = new Vector();
        String s = "";

        try { v = itsframe.its.getNodeTree(NodeID); }
        catch (Exception e) { e.printStackTrace(System.out); }

        for (int i = v.size()-1; i > 0; i--) {
            ITSTreeNode n = (ITSTreeNode) v.elementAt(i);
            if (i < v.size()-1) { s = s + " -> "; }
            s = s + n.get("NODETITLE");
        }

        return s;
    }

    public void fillConceptTable(Vector v) {
        if (v.size() == 0) {
            JOptionPane.showMessageDialog(itsframe, "Your narrative did not classify into any topics.",
                    "Information", JOptionPane.NO_OPTION);

            DefaultSortTableModel dtm = new DefaultSortTableModel();
            conceptTable.setModel(dtm);
            return;
        }

        // table model
        DefaultSortTableModel dtm = (DefaultSortTableModel) conceptTable.getModel();
        NodeDocument[] ndo = new NodeDocument[v.size()];
        String[] gnto = new String[v.size()];
        Integer[] ratings = new Integer[v.size()];

        for (int i = 0; i < v.size(); i++) {
            NodeDocument nd = (NodeDocument) v.elementAt(i);

            ndo[i] = nd; gnto[i] = getNodeTree(nd.get("NODEID"));
            if (nd.get("INDEXWITHINNODE").equals(""))
                ratings[i] = new Integer(5);
            else ratings[i] = new Integer(nd.get("INDEXWITHINNODE"));
        }

        // add columns
        dtm.addColumn("Topic Title", ndo);
        dtm.addColumn("Topic Context", gnto);
        dtm.addColumn("Rate", ratings);

        // dropdown values
        Integer[] values = new Integer[]{new Integer(1),new Integer(2),new Integer(3),new Integer(4),
                                         new Integer(5),new Integer(6),new Integer(7),new Integer(8),
                                         new Integer(9),new Integer(10)};

        TableColumnModel tcm = conceptTable.getColumnModel();
        TableColumn col = tcm.getColumn(2);
        col.setCellEditor(new NarrComboBoxEditor(values));
        col.setCellRenderer(new NarrComboBoxRenderer(values));

        tcm.getColumn(0).setPreferredWidth(200);
        tcm.getColumn(1).setPreferredWidth(325);

        dtm.sortColumn(0, true);
        conceptTable.setModel(dtm);
    }

    public String clean (String s) {
        // remove leading and trailing spaces
        s = s.trim();

        if (s.length() > 3900) { s = s.substring(0, 3900); }

        s = s.replaceAll("\n", " "); //s = s.replaceAll("'", "''");
        s = s.replaceAll("&", "and");

        return s;
    }
    public String cleanSum (String s) {
        // remove leading and trailing spaces
        s = s.trim();

        if (s.length() > 3900) { s = s.substring(0, 3900); }

        s = s.replaceAll("\n", " "); s = s.replaceAll("'", "''");
        s = s.replaceAll("&", "and");

        return s;
    }

    public void actionPerformed (ActionEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {
            boolean bSuccess = false;

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                if (e.getSource().equals(classifyButton)) {
                    String sDocTitle = clean(titleText.getText());
                    String sDocText = cleanSum(narrativeText.getText());

                    // hardcoded (for now) trouble status: 1 (products), 2 (people), 3 (projects)
                    if (document == null) {
                        try {
                            Hashtable htArgs = new Hashtable();
                            htArgs.put("GenreID", "241");
                            htArgs.put("RepositoryID", "99999");
                            htArgs.put("DocTitle", sDocTitle);
                            htArgs.put("DocumentSummary", sDocText);
                            htArgs.put("NarrativeId", ca.getNarrative().getNarrativeID());

                            document = itsframe.its.addDocument(URLEncoder.encode("NARRATIVE" + sDocTitle, "UTF-8"), htArgs);
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                        }

                    } // handle existing documents here

                    Vector v = new Vector();

                    try { v = itsframe.its.classifyNarrative(sDocTitle, sDocText);
                    } catch (Exception ex) { ex.printStackTrace(System.out); return; }

                    fillConceptTable(v);

                    if (v.size() > 0) bSuccess = true;
                }

                if (e.getSource().equals(removeButton)) {
                    DefaultSortTableModel dstm = (DefaultSortTableModel) conceptTable.getModel();
                    int[] i = conceptTable.getSelectedRows();
                    for (int loop = i.length - 1; loop >= 0; loop--) {
                        dstm.removeRow(i[loop]);
                    }

                    conceptTable.revalidate();
                }

                if (e.getSource().equals(cancelButton)) { frame.dispose(); }

                if (e.getSource().equals(okButton)) {
                    // check that a document was inserted before continuing
                    if (document == null) {
                        JOptionPane.showMessageDialog(itsframe, "Your narrative has not been classified and thus will not be saved.",
                            "Error", JOptionPane.NO_OPTION);
                        frame.dispose();
                        return;
                    }

                    // this document text may have been edited - resave the document
                    try {
                        document.set("DOCUMENTSUMMARY", cleanSum(narrativeText.getText()));
                        document.set("DOCTITLE", clean(titleText.getText()));

                        itsframe.its.editDocument(document);
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                        JOptionPane.showMessageDialog(itsframe, "Your narrative could not be saved: "+e.getMessage()+".",
                            "Error", JOptionPane.NO_OPTION);
                        return;
                    }

                    // remove existing relationships first
                    try { itsframe.its.deleteNodeDocuments(document.get("DOCUMENTID"), true); }
                    catch (Exception e) { e.printStackTrace(System.out); }

                    // get list of node doc relationships and insert for this document
                    PopupProgressBar ppb = new PopupProgressBar(itsframe, 1, conceptTable.getModel().getRowCount());
                    ppb.pack();
                    ppb.show();

                    for (int i = 0; i < conceptTable.getModel().getRowCount(); i++) {
                        NodeDocument nd = (NodeDocument) conceptTable.getModel().getValueAt(i, 0);
                        nd.set("DOCUMENTID",document.get("DOCUMENTID"));
                        nd.set("INDEXWITHINNODE", ((Integer) conceptTable.getModel().getValueAt(i, 2)).toString());
                        try { itsframe.its.addNodeDocument(nd, false); }
                        catch (Exception e) { e.printStackTrace(System.out); }

                        ppb.setProgress(i);
                    }

                    // purge concept cache, if applicable
                    try { itsframe.its.purgeConceptCache(document); }
                    catch (Exception e) { e.printStackTrace(System.out); }

                    try { ca.getAlerts(); } catch (Exception e) { e.printStackTrace(System.out); }
                    ppb.dispose();
                    frame.dispose();
                }

            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);

                if (bSuccess) {
                    JOptionPane.showMessageDialog(itsframe, "Narrative classified successfully.",
                      "Information", JOptionPane.NO_OPTION);
                }
            }
        };
        aWorker.start();
    }

    public void stateChanged(ChangeEvent e) {
        if (jTabbedPane1.getSelectedIndex() == 1) {
            removeButton.setEnabled(true);
        } else { removeButton.setEnabled(false); }
    }

    public class NarrComboBoxRenderer extends JComboBox implements TableCellRenderer {
        public NarrComboBoxRenderer(Integer[] items) { super(items); }

        public Component getTableCellRendererComponent (JTable table, Object value, boolean isSelected,
                                                        boolean hasFocus, int row, int column) {
            if (isSelected) {
                setForeground(table.getSelectionForeground());
                setBackground(table.getSelectionBackground());
            } else {
                setForeground(table.getForeground());
                setBackground(table.getBackground());
            }

            // Select the current value
            setSelectedItem(value);
            return this;
        }
    }

    public class NarrComboBoxEditor extends DefaultCellEditor {
        public NarrComboBoxEditor(Integer[] items) {
            super(new JComboBox(items));
            ((JComboBox) this.getComponent()).setSize(50, 10);
        }
    }
}
