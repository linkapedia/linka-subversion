/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Dec 9, 2003
 * Time: 1:35:52 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.iw.tools;

import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class RowColorJTable extends JSortTable
{
    //private CustomRenderer customRenderer = new CustomRenderer (-1,-1,null);
    private CustomRenderer customRenderer = null;
    TableModel tableModel = null;

    public RowColorJTable (int iCompareCol , int iComparator , Object oCompareValue)
    {
        super ();
        //System.out.println ("in RowColorJTable constructor iComparator [" + iComparator + "]");
        customRenderer = new CustomRenderer (iCompareCol , iComparator , oCompareValue);
    }

    public RowColorJTable ()
    {
        super ();
    }

    public void addCustomRenderer (int iCompareCol) {
        customRenderer.vCompareCol.add(new Integer(iCompareCol));
    }

    public TableCellRenderer getCellRenderer (int row , int column)
    {
        return customRenderer;
    }

    public void setModel (TableModel tableModel)
    {
        super.setModel (tableModel);
        this.tableModel = tableModel;
    }

    class CustomRenderer extends DefaultTableCellRenderer
    {

        public Vector vCompareCol = new Vector();
        int iComparator;
        Object oCompareValue = null;

        public CustomRenderer (int iCol , int iComparator , Object oCompareValue)
        {
            super ();
            this.vCompareCol.add(new Integer(iCol));
            this.iComparator = iComparator;
            this.oCompareValue = oCompareValue;
            //System.out.println ("in CustomRenderer constructor iCompareCol  [" + iCompareCol + "]" +
            //        " oCompareValue [" + oCompareValue + "]");
        }

        public CustomRenderer (Vector vCol , int iComp , Object oCompareValue)
        {
            super ();
            vCompareCol = vCol;
            iComparator = iComp;
            this.oCompareValue = oCompareValue;
            //System.out.println ("in CustomRenderer constructor iCompareCol  [" + iCompareCol + "]" +
            //        " oCompareValue [" + oCompareValue + "]");
        }

        int iCount = 0;

        public boolean containsColumn(int column) {
            for (int i = 0; i < vCompareCol.size(); i++) {
                if (((Integer) vCompareCol.elementAt(i)).intValue() == column) return true;
            }
            return false;
        }

        public Component getTableCellRendererComponent (JTable table , Object value ,
                                                        boolean isSelected , boolean hasFocus ,
                                                        int row , int column)
        {
            iCount++;
            super.getTableCellRendererComponent (table , value , isSelected , hasFocus , row , column);

            if (oCompareValue != null)
            {
                if (!containsColumn(column))
                {
                    Object oCellValue = tableModel.getValueAt (row , ((Integer) vCompareCol.elementAt(0)).intValue());
                    // System.out.println(" row [ "   + row + "] oCellValue  [" + oCellValue .toString() + "] .getClass().getName() " + oCellValue.getClass().getName()+ "]");
                    if (oCellValue != null && colorRuleFires (oCellValue , iComparator, oCompareValue))
                    {
                        //System.out.println ("oCellValue[" + oCellValue + "] TRUE row [" + row + "] column [" + column + "]");
                        this.setForeground (Color.lightGray);
                    }
                    else
                    {
                        //System.out.println ("oCellValue[" + oCellValue + "] FALSE row [" + row + "] column [" + column + "]");
                        this.setForeground (Color.black);
                    }

                }

                else  // col for progress bar
                {
/*
                    JProgressBar jprog = new JProgressBar();
                    jprog.setBorderPainted(true);
                    jprog.setMinimum(0);
                    jprog.setValue(50);
                    jprog.setMaximum(100);
                    return jprog;
*/

                    IndicatorCellRenderer jprog = new IndicatorCellRenderer(0,100);
                    //jprog.setBorderPainted(true);

                    if ((!(value instanceof Integer)) && ((String)value).trim().equals("")) return this;

                    //jprog.setC
                    //jprog.setBorderPainted(true);
                    jprog.setStringPainted(true);
                    jprog.setBackground(table.getBackground());
                    double dValueExact;

                    if (value instanceof Integer) { dValueExact = ((Integer) value).doubleValue(); }
                    else { dValueExact = Double.parseDouble(((String) value )); }

                    double dValueFloor = java.lang.Math.floor(dValueExact );
                    double ddiff = dValueExact - dValueFloor;
                    double dValueFinal = -1;
                    if ( ddiff > .5 )
                        dValueFinal = java.lang.Math.ceil(dValueExact );
                    else
                        dValueFinal = dValueFloor;

                    jprog.setValue((int) dValueFinal);
                    jprog.setValue((int) dValueFinal);

                    if (dValueFinal == 0) return new JLabel("None", JLabel.CENTER);
                    if (dValueFinal == -1) return new JLabel("Failed!", JLabel.CENTER);

                    return jprog;
                }
            }
            return this;
        }

        private boolean colorRuleFires (Object oCellValue , int iComparator , Object oCompareValue)
        {

            if (oCompareValue.getClass ().getName ().equals ("java.lang.String"))
            {
                if (iComparator == ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS.intValue ())
                    return oCellValue.equals (oCompareValue);
                else
                    System.out.println ("unknown string compare type for colorRuleFires() iComparator  [" + iComparator + "]");

            }
            else
                System.out.println ("unknown data type for for colorRuleFires() [" + oCompareValue.getClass ().getName () + "]");


            return false;

        }

    } // end class CustomTable
} // end class CustomRenderer


