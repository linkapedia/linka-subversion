package com.iw.ui;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.oculustech.layout.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.File;

import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.ui.explain.*;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;

public class NodeDocView extends JPanel implements ActionListener, ListSelectionListener, MouseListener {
    public int iButtonPressed;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;

    // Instance attributes used in this example
    private int documentWeight = 0;
    private int abstractWeight = 0;

    private JInternalFrame MDIframe;
    private JPanel openPanel;
    private File postedFile = null;

    // tools
    protected JLabel documentLabel;
    protected JLabel titleLabel;
    protected RowColorJTable resultsTable;
    protected JButton explainButton;
    protected JButton closeButton;

    // classify a document with the given path
    public NodeDocView(ITSAdministrator frame, File f) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;
        its = frame.its;
        documentWeight = 1;
        postedFile = f;

        Vector vNodeDocs = new Vector();
        try {
            vNodeDocs = its.classify(f, false);
        } catch (EmptyCorpusToUseList e) {
            JOptionPane.showMessageDialog(ITSframe, "Taxonomy list not set.  Please configure your classification " +
                    "preferences and try your classification again.",
                    "Error", JOptionPane.NO_OPTION);
            Preferences p = new Preferences(frame);
            p.show();

            return;
        }

        if (vNodeDocs.size() == 0) {
            JOptionPane.showMessageDialog(ITSframe, "No results were found to match your request.",
                    "Information", JOptionPane.NO_OPTION);
            return;
        }

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Classify Results", true, true, true, true);
        MDIframe.setLocation(80, 80);
        MDIframe.setSize(675, 380);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        documentLabel = new JLabel("Document Title: ");
        documentLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        titleLabel = new JLabel(f.getName());

        resultsTable = new RowColorJTable(2, ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS.intValue(), (Object) new String(""));
        resultsTable.addMouseListener(this);

        loadResults(f.getName(), ITSframe, vNodeDocs);

        explainButton = new JButton("Explain");
        explainButton.addActionListener(this);
        closeButton = new JButton("Close");
        closeButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(documentLabel);
                    layout.add(titleLabel);
                    layout.parent();
                }
                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(new JScrollPane(resultsTable));
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.add(explainButton);
                        layout.addSpace(10);
                        layout.add(closeButton);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(explainButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
        MDIframe.toFront();
    }

    // show node document relationships for a given document
    public NodeDocView(ITSAdministrator frame, Document d, User u) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        String sAbstractIndicator = "";
        if (u != null) {
            documentWeight = u.getDocumentWeight();
            abstractWeight = u.getAbstractWeight();
        }
        if (u != null && u.modeAbstractDocument())
            sAbstractIndicator = "(abstract) ";

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Classify " + sAbstractIndicator + "Results: " + d.get("DOCTITLE"), true, true, true, true);
        MDIframe.setLocation(80, 80);
        MDIframe.setSize(675, 380);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        documentLabel = new JLabel("Document Title: ");
        documentLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        titleLabel = new JLabel(d.get("DOCTITLE"));

        resultsTable = new RowColorJTable(2, ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS.intValue(), (Object) new String(""));
        resultsTable.addMouseListener(this);

        // load table with data depending upon invokation method
        // 0: related, 1: classify doc, 2: classify abstract

        try {
            if (u == null) {
                runRelatedNodes(d);
            } else {
                classify(d, u.modeClassifyDocument());
            }
        } catch (EmptyCorpusToUseList e) {
            JOptionPane.showMessageDialog(ITSframe, "Taxonomy list not set.  Please configure your classification " +
                    "preferences and try your classification again.",
                    "Error", JOptionPane.NO_OPTION);
            MDIframe.dispose();
            hide();
            return;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, "No results were found to match your request.", "Information", JOptionPane.NO_OPTION);
            MDIframe.dispose();
            return;
        }

        explainButton = new JButton("Explain");
        explainButton.addActionListener(this);
        closeButton = new JButton("Close");
        closeButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(documentLabel);
                    layout.add(titleLabel);
                    layout.parent();
                }
                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(new JScrollPane(resultsTable));
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.add(explainButton);
                        layout.addSpace(10);
                        layout.add(closeButton);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(explainButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
        MDIframe.toFront();
    }

    private void runRelatedNodes(Document d) throws Exception {
        String sCQL = "SELECT <NODEDOCUMENT> WHERE DOCUMENTID = " + d.get("DOCUMENTID");
        Vector vNodeDocs = its.CQL(sCQL);
        if (vNodeDocs.size() == 0) {
            throw new Exception("No results found.");
        }
        loadResults(d.get("DOCTITLE"), ITSframe, vNodeDocs);
    }

    private void classify(Document d, boolean bDocClassify) throws Exception {
        //System.out.println("start classify type [" + this.iType + "] with docid [" + d.get("DOCUMENTID") + "]");
        Vector vNodeDocs = its.classify(Integer.parseInt(d.get("DOCUMENTID")), d.get("DOCTITLE"), bDocClassify);

        if (vNodeDocs.size() == 0) {
            throw new Exception("No results found.");
        }
        loadResults(d.get("DOCTITLE"), ITSframe, vNodeDocs);
    }


    public void valueChanged(ListSelectionEvent event) {
    }

    public void actionPerformed(ActionEvent event) {
        MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            if (event.getSource().equals(closeButton)) {
                MDIframe.dispose();
            } else if (event.getSource().equals(explainButton)) {
                try {
                    SwingWorker aWorker = new SwingWorker(event) {
                        protected void doNonUILogic() {
                            try {
                                invokeExplain();
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();

                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        try {
            MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            if (((MouseEvent) e).isShiftDown() ||
                    ((MouseEvent) e).isControlDown() ||
                    ((MouseEvent) e).isAltDown()) {
                return;
            }
            Point p = ((MouseEvent) e).getPoint();
            int row = resultsTable.rowAtPoint(p);

            System.out.println("mouseClicked: " + row + " count " + ((MouseEvent) e).getClickCount());
            if (((MouseEvent) e).getClickCount() == 2) {
                ITSTreeNode n = its.getNodeProps(((NodeDocument) resultsTable.getModel().getValueAt(row, 0)).get("NODEID"));
                Corpus c = its.getCorpora(n.get("CORPUSID"));

                JDesktopPane jdesktoppane = ITSframe.jdp;
                CorpusManagement DragAndDrop = null;
                try {
                    DragAndDrop = new CorpusManagement(ITSframe, c, n);
                } catch (Exception ex) {
                    System.out.println("Could not create corpus management frame.");
                    ex.printStackTrace(System.out);
                    return;
                }

                DragAndDrop.setBounds(50, 50, 850, 550);
                DragAndDrop.setVisible(true);
                DragAndDrop.setDefaultCloseOperation(
                        WindowConstants.DISPOSE_ON_CLOSE);

                jdesktoppane.add(DragAndDrop);
                DragAndDrop.moveToFront();

                jdesktoppane.setVisible(true);
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally { MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); }
    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void invokeExplain() throws Exception {
        invokeExplain(postedFile);
    }

    public void invokeExplain(File f) throws Exception {
        boolean bClassifyDocument = true;
        if (abstractWeight > 0) {
            bClassifyDocument = false;
        }

        int row = resultsTable.getSelectedRow();
        if (row == -1)
            JOptionPane.showMessageDialog(ITSframe, "A topic (row) must be selected.", "Information", JOptionPane.NO_OPTION);

        NodeDocument nd = (NodeDocument) resultsTable.getModel().getValueAt(row, 0);

        OculusBox ob = new OculusBox();

// pop up explain frame is an internal MDI frame
        String sAbstractIndicator = "";
        if (!bClassifyDocument)
            sAbstractIndicator = "(abstract) ";
        JInternalFrame jifExplainDisplay = new JInternalFrame("Explain Report for Node " + nd.get("NODEID") +
                " Doc " + sAbstractIndicator + nd.get("DOCUMENTID"), true, true, true, true);
        jifExplainDisplay.setLocation(60, 60);
        jifExplainDisplay.setSize(600, 500);  // hbk control
//Dimension winsize = this.getSize(), screensize =
//    Toolkit.getDefaultToolkit().getScreenSize();
//setSize((int) (screensize.width / 1.5),  (int) (screensize.height / 1.25));
        jifExplainDisplay.setResizable(true);
        jifExplainDisplay.setBackground(Color.lightGray);
        jifExplainDisplay.setFrameIcon(ITSframe.iIndraweb);

        ExplainDisplayDataContainer explainDisplayDataContainer = null;

        try {
            if ((nd.get("DOCUMENTID") != null) && (!nd.get("DOCUMENTID").equals(""))) {
                explainDisplayDataContainer = its.getExplainData(
                        new Integer(nd.get("NODEID")).intValue(),
                        new Integer(nd.get("DOCUMENTID")).intValue(),
                        nd.get("NODETITLE"), nd.get("DOCTITLE"), its.getNodeCorpus(nd.get("NODEID")),
                        ITSframe, bClassifyDocument);
            } else {
                explainDisplayDataContainer = its.getExplainData(
                        new Integer(nd.get("NODEID")).intValue(),
                        nd.get("NODETITLE"), nd.get("DOCTITLE"),
                        its.getNodeCorpus(nd.get("NODEID")), ITSframe, f);
            }
        } catch (EmptyCorpusToUseList e) {
            JOptionPane.showMessageDialog(ITSframe, "Taxonomy list not set.  Please configure your classification " +
                    "preferences and try your 'explain' again.",
                    "Error", JOptionPane.NO_OPTION);
            Preferences p = new Preferences(ITSframe);
            p.show();

            return;
        }
//System.out.println("kick off explain node [" + nd.get("DOCUMENTID") + " ] doc [" + nd.get("DOCUMENTID")+ "]");


        if (explainDisplayDataContainer == null)
            return;

        ExplainDisplayPane explainDisplayPane = new ExplainDisplayPane(ITSframe, explainDisplayDataContainer, jifExplainDisplay);
        jifExplainDisplay.setContentPane(ob);
        jifExplainDisplay.show();
        jifExplainDisplay.getContentPane().add(explainDisplayPane);

        explainDisplayPane.setVisible(true);
        explainDisplayPane.show();

//jIntFMDI.setMaximum(true );
        ITSframe.jdp.add(jifExplainDisplay);
//jIntFMDI.setMaximum(true);
        jifExplainDisplay.show();
        jifExplainDisplay.setVisible(true);
        jifExplainDisplay.toFront();
    }

    public void loadResults(String DocTitle, ITSAdministrator itsadmin, Vector vDocs) throws Exception {
        if (vDocs.size() == 0) {
            return;
        }

        final DefaultSortTableModel dtm = new DefaultSortTableModel(vDocs.size(), 3);

        for (int i = 0; i < vDocs.size(); i++) {
            NodeDocument nd = (NodeDocument) vDocs.elementAt(i);

            dtm.setValueAt(nd, i, 0);
            dtm.setValueAt(nd.get("DOCSUMMARY"), i, 1);
            dtm.setValueAt(nd.get("SCORE1"), i, 2);
        }
        Vector v = new Vector();
        v.add("Topic Title");
        v.add("Gist");
        v.add("Score");
        dtm.setColumnIdentifiers(v);
        dtm.setEditable(false);

        resultsTable.setModel(dtm);

        dtm.sortColumn(2, false);
    }
}
