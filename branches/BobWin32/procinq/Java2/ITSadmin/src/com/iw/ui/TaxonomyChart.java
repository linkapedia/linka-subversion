/*
    TaxonomyChart.java

    This class generates a chart given a vector of topics.  Charts are generated based upon
    a pre-specified parameter, where the minimum and maximum dates in the range are specified by the user.
*/
package com.iw.ui;

import org.jfree.chart.*;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.renderer.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.data.*;
import org.jfree.data.time.*;
import org.jfree.util.SortOrder;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import java.util.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.text.NumberFormat;
import java.text.DecimalFormat;

import com.iw.system.*;
import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;
import com.oculustech.layout.*;

public class TaxonomyChart extends JInternalFrame implements ActionListener, FocusListener, MouseListener {
    // document chart attributes
    protected ITSAdministrator admin;
    protected Component glass;
    protected Vector documents;
    protected Corpus corpus;
    protected Vector[] nodeData = new Vector[10];

    private String toolName;
    private TaxonomyChart tc;

    // document tool widgets
    protected JLabel depthLabel;
    protected JTextField depthText;
    protected JLabel countLabel;
    protected JTextField countText;

    // static widgets
    protected JLabel meanLabel;
    protected JLabel meanText;
    protected JLabel sdLabel;
    protected JLabel sdText;

    protected JSortTable resultsTable;

    // two panels in a split pane
    JPanel toolPanel;
    ChartPanel chartPanel;

    public TaxonomyChart(ITSAdministrator admin, Corpus c, String Name) {
        super("Topics By " + Name, true, true, true, true);
        this.admin = admin;
        toolName = Name;
        corpus = c;
        setFrameIcon(admin.iIndraweb);

        createPanels();
        pack();

        tc = this;
    }

    // create both panels and set up a glass pane for hourglass operations
    public void createPanels() {
        glass = getGlassPane();

        toolPanel = createToolPanel();
        JPanel chartTablePanel = createChartPanel();

        JSplitPane splitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT,
                toolPanel, chartTablePanel);

        splitPane.setDividerLocation(180);
        splitPane.setOneTouchExpandable(false);

        getContentPane().add(splitPane, BorderLayout.CENTER);
    }

    // the tool panel contains the widgets for manipulating the chart
    public JPanel createToolPanel() {
        JPanel toolPanel = new JPanel();

        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        depthLabel = new JLabel("Depth:");
        depthLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        depthLabel.setToolTipText("Restrict topics to the given depth level.");

        depthText = new JTextField("2");
        depthText.setPreferredSize(new Dimension(100, 20));
        depthText.setToolTipText("Restrict topics to the given depth level.");
        depthText.addFocusListener(this);

        countLabel = new JLabel("Min Count:");
        countLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        countLabel.setHorizontalTextPosition(SwingConstants.RIGHT);
        countLabel.setToolTipText("Restrict results to topics with a minimum document count.");

        countText = new JTextField("0");
        countText.setPreferredSize(new Dimension(100, 20));
        countText.setToolTipText("Restrict results to topics with a minimum document count.");
        countText.addFocusListener(this);

        meanLabel = new JLabel("Mean:");
        meanLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        meanLabel.setHorizontalTextPosition(SwingConstants.RIGHT);

        meanText = new JLabel("N/A");
        meanText.setHorizontalAlignment(SwingConstants.RIGHT);
        meanText.setHorizontalTextPosition(SwingConstants.RIGHT);

        sdLabel = new JLabel("Std Deviation:");
        sdLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        sdLabel.setHorizontalTextPosition(SwingConstants.RIGHT);

        sdText = new JLabel("N/A");
        sdText.setHorizontalAlignment(SwingConstants.RIGHT);
        sdText.setHorizontalTextPosition(SwingConstants.RIGHT);

        layout.nestGrid(2, 4);
        {
            layout.add(depthLabel);
            layout.add(depthText, OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
            layout.add(countLabel);
            layout.add(countText, OculusLayout.ALIGNMENT_SPACE_STRETCHING, OculusLayout.NO_STRETCH);
            layout.add(meanLabel);
            layout.add(meanText);
            layout.add(sdLabel);
            layout.add(sdText);
            layout.parent();
        }

        toolPanel.add(ob);

        return toolPanel;
    }

    // initialize the chart panel - the chart panel must take a chart by default, so we provide an empty one
    public JPanel createChartPanel() {
        resultsTable = new JSortTable();

        chartPanel = new ChartPanel(ChartFactory.createAreaChart("", "", "",
                new DefaultCategoryDataset(), PlotOrientation.VERTICAL, false, false, false));
        resultsTable.addMouseListener(this);

        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.add(new JScrollPane(new JTextPane()),
                BorderLayout.CENTER);
        p.setMinimumSize(new Dimension(375, 0));
        p.setBorder(BorderFactory.createTitledBorder(
                "Topic Properties"));

        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(chartPanel);
            layout.addSpace(10);
            layout.add(new JScrollPane(resultsTable));
            layout.addSpace(10);
            layout.parent();
        }

        p.add(ob);

        rebuildChart();

        return p;
    }

    // rebuild the chart
    public void rebuildChart() {
        int count = -1;
        try {
            count = new Integer(countText.getText()).intValue();
        } catch (Exception e) {
            count = -1;
        }

        int depth = -1;
        try {
            depth = new Integer(depthText.getText()).intValue();
        } catch (Exception e) {
            depth = -1;
        }

        if (nodeData[depth] == null) {
            try {
                if (toolName.equals("Documents")) {
                    nodeData[depth] = admin.its.getNodesWithDocuments(corpus, depth);
                } else if (toolName.equals("Children")) {
                    nodeData[depth] = admin.its.getNodesWithChildren(corpus, depth);
                } else {
                    nodeData[depth] = admin.its.getNodesWithSignatures(corpus, depth);
                }

                if (nodeData[depth].size() == 0) {
                    JOptionPane.showMessageDialog(admin, "No topics were found in the taxonomy "+corpus.getName()+" at "+
                        "depth level "+depth+".", "Error", JOptionPane.NO_OPTION);
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace(System.out);
                JOptionPane.showMessageDialog(admin, e.getMessage(), "Error", JOptionPane.NO_OPTION);
                return;
            }
        }
        double mean = mean(nodeData[depth]);
        double sd = standardDeviation(nodeData[depth]);
        double sdmean1 = mean + sd;
        double sdmean2 = mean - sd;
        if (sdmean2 < 0) sdmean2 = 0.0;
        double sdmean3 = mean + (sd * 2);
        double sdmean4 = mean - (sd * 2);
        if (sdmean4 < 0) sdmean4 = 0.0;

        if (false) System.out.println("mean: " + mean + " sd: " + sd + " sdmean1: " + sdmean1 + " sdmean2: " + sdmean2);

        DecimalFormat df = new DecimalFormat("0.00");
        meanText.setText(df.format(mean));
        sdText.setText(df.format(sd));

        // rebuild grid
        rebuildGrid(nodeData[depth]);

        // build chart
        XYSeries xys1 = new XYSeries(toolName, false);
        XYSeries xys2 = new XYSeries("Mean", false);
        XYSeries xys3 = new XYSeries("Deviation High x1", false);
        XYSeries xys4 = new XYSeries("Deviation Low x1", false);
        XYSeries xys5 = new XYSeries("Deviation High x2", false);
        XYSeries xys6 = new XYSeries("Deviation Low x2", false);

        int loop = 0;
        for (int i = 0; i < nodeData[depth].size(); i++) {
            ITSTreeNode n = (ITSTreeNode) nodeData[depth].elementAt(i);
            if (new Integer(n.get("CLIENTID")).intValue() >= count) {
                loop++;
                xys1.add(new Double(loop + ".0").doubleValue(), new Double(n.get("CLIENTID") + ".0").doubleValue());
                xys2.add(new Double(loop + ".0").doubleValue(), mean);
                xys3.add(new Double(loop + ".0").doubleValue(), sdmean1);
                xys4.add(new Double(loop + ".0").doubleValue(), sdmean2);
                xys5.add(new Double(loop + ".0").doubleValue(), sdmean3);
                xys6.add(new Double(loop + ".0").doubleValue(), sdmean4);
            }
        }

        IntervalXYDataset dataset = new ITSdataset(xys1, toolName);

        DefaultTableXYDataset dataseta = new DefaultTableXYDataset();
        dataseta.addSeries(xys2);
        DefaultTableXYDataset datasetb = new DefaultTableXYDataset();
        datasetb.addSeries(xys3);
        DefaultTableXYDataset datasetc = new DefaultTableXYDataset();
        datasetc.addSeries(xys4);
        DefaultTableXYDataset datasetd = new DefaultTableXYDataset();
        datasetd.addSeries(xys5);
        DefaultTableXYDataset datasete = new DefaultTableXYDataset();
        datasete.addSeries(xys6);

        DateAxis domainAxis = new DateAxis("Topics");
        domainAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
        ValueAxis rangeAxis = new NumberAxis(toolName);

        // create plot...
        IntervalXYDataset data1 = (IntervalXYDataset) dataset;
        XYBarRenderer renderer1 = new XYBarRenderer(0.20);
        renderer1.setPaint(Color.BLUE);

        XYPlot plot = new XYPlot(data1, domainAxis, rangeAxis, renderer1);

        // create subplot 2...
        XYDataset data2A = (XYDataset) dataseta;
        XYItemRenderer renderer2A = new StandardXYItemRenderer();
        renderer2A.setSeriesPaint(0, new Color(87, 128, 22));
        plot.setSecondaryDataset(0, data2A);
        plot.setSecondaryRenderer(0, renderer2A);

        XYDataset data2B = (XYDataset) datasetb;
        XYItemRenderer renderer2B = new StandardXYItemRenderer();
        renderer2B.setSeriesPaint(0, Color.RED);
        renderer2B.fillDomainGridBand((Graphics2D) chartPanel.getGraphics(), plot, domainAxis, new Rectangle((int) sdmean2, (int) mean), sdmean2, mean);
        plot.setSecondaryDataset(1, data2B);
        plot.setSecondaryRenderer(1, renderer2B);
        plot.mapSecondaryDatasetToRangeAxis(1, new Integer(0));

        XYDataset data2C = (XYDataset) datasetc;
        XYItemRenderer renderer2C = new StandardXYItemRenderer();
        renderer2C.setSeriesPaint(0, Color.RED);
        plot.setSecondaryDataset(2, data2C);
        plot.setSecondaryRenderer(2, renderer2C);
        plot.mapSecondaryDatasetToRangeAxis(2, new Integer(1));

        XYDataset data2D = (XYDataset) datasetd;
        XYItemRenderer renderer2D = new StandardXYItemRenderer();
        renderer2D.setSeriesPaint(0, Color.BLACK);
        plot.setSecondaryDataset(3, data2D);
        plot.setSecondaryRenderer(3, renderer2D);
        plot.mapSecondaryDatasetToRangeAxis(3, new Integer(2));

        XYDataset data2E = (XYDataset) datasete;
        XYItemRenderer renderer2E = new StandardXYItemRenderer();
        renderer2E.setSeriesPaint(0, Color.BLACK);
        plot.setSecondaryDataset(4, data2E);
        plot.setSecondaryRenderer(4, renderer2E);
        plot.mapSecondaryDatasetToRangeAxis(4, new Integer(3));

        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.getDomainAxis().setVisible(false);

        //plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinesVisible(false);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinesVisible(false);
        plot.setRangeGridlinePaint(Color.white);

        JFreeChart chart = new JFreeChart(toolName + " by Topic", JFreeChart.DEFAULT_TITLE_FONT, plot, true);
        chart.setBorderPaint(Color.lightGray);

        chartPanel.setChart(chart);
    }

    private void rebuildGrid(Vector vNodes) {
        DefaultSortTableModel dtm = new DefaultSortTableModel(vNodes.size(), 3);

        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode itn = (ITSTreeNode) vNodes.elementAt(i);
            dtm.setValueAt(itn, i, 0);
            dtm.setValueAt(itn.get("NODEDESC"), i, 1);
            dtm.setValueAt(new Integer(itn.get("CLIENTID")), i, 2);
        }
        Vector v = new Vector();
        v.add("Node Title");
        v.add("Description");
        v.add("Count");
        dtm.setColumnIdentifiers(v);
        dtm.sortColumn(2, false);
        dtm.setEditable(false);

        resultsTable.setModel(dtm);

        resultsTable.getColumnModel().getColumn(0).setPreferredWidth(200);
        resultsTable.getColumnModel().getColumn(1).setPreferredWidth(300);
        resultsTable.getColumnModel().getColumn(2).setPreferredWidth(50);
    }

    public static double mean(Vector data) {
        // Calculate the mean
        double mean = 0;
        final int n = data.size();
        if (n < 2)
            return Double.NaN;
        for (int i = 0; i < n; i++) {
            ITSTreeNode node = (ITSTreeNode) data.elementAt(i);
            mean += new Double(node.get("CLIENTID") + ".0").doubleValue();
        }
        mean /= n;

        return mean;
    }

    public static double standardDeviation(Vector data) {
        // Calculate the mean
        double mean = 0;
        final int n = data.size();
        if (n < 2)
            return Double.NaN;
        for (int i = 0; i < n; i++) {
            ITSTreeNode node = (ITSTreeNode) data.elementAt(i);
            mean += new Double(node.get("CLIENTID") + ".0").doubleValue();
        }
        mean /= n;

        // calculate the sum of squares
        double sum = 0;
        for (int i = 0; i < n; i++) {
            ITSTreeNode node = (ITSTreeNode) data.elementAt(i);
            final double v = new Double(node.get("CLIENTID") + ".0").doubleValue() - mean;
            sum += v * v;
        }
        return Math.sqrt(sum / (n - 1));
    }

    // ACTION ITEMS: on each value change, rebuild the chart
    public void actionPerformed(ActionEvent e) {
        System.out.println("Action performed.");

        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                rebuildChart();
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
                System.out.println("Exit action performed.");
            }
        };
        aWorker.start();

    }

    public void focusLost(FocusEvent e) {
        glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        glass.setVisible(true);
        rebuildChart();
        glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        glass.setVisible(false);
     }

    public void focusGained(FocusEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {

                    if (((MouseEvent) e).isShiftDown() ||
                            ((MouseEvent) e).isControlDown() ||
                            ((MouseEvent) e).isAltDown()) {
                        return;
                    }
                    Point p = ((MouseEvent) e).getPoint();
                    int row = resultsTable.rowAtPoint(p);

                    if (((MouseEvent) e).getClickCount() == 2) {
                        ITSTreeNode n = (ITSTreeNode) resultsTable.getModel().getValueAt(row, 0);

                        JDesktopPane jdesktoppane = admin.jdp;
                        CorpusManagement DragAndDrop = null;
                        try {
                            //System.out.println("draganddrop "+corpus.getName()+" "+n.getTitle());
                            DragAndDrop = new CorpusManagement(admin, corpus, n);
                        } catch (Exception ex) {
                            System.out.println("Could not create corpus management frame.");
                            ex.printStackTrace(System.out);
                            return;
                        }

                        DragAndDrop.setBounds(50, 50, 850, 550);
                        DragAndDrop.setVisible(true);
                        DragAndDrop.setDefaultCloseOperation(
                                WindowConstants.DISPOSE_ON_CLOSE);

                        jdesktoppane.add(DragAndDrop);
                        DragAndDrop.moveToFront();

                        jdesktoppane.setVisible(true);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();

    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }
}
