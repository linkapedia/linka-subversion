package com.iw.ui.conceptalerts;

import com.oculustech.layout.*;
import com.iw.ui.*;
import com.iw.system.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

public class AddAlert extends JPanel implements ActionListener {
    // add alert attributes
    protected ITSAdministrator admin;
    protected Component glass;
    protected String CQL;

    // add alert widgets
    protected JLabel nameLabel;
    protected JTextField nameText;
    protected JLabel freqLabel;
    protected JComboBox freqList;
    protected JLabel notesLabel;
    protected JTextArea notesArea;
    protected JRadioButton whoRadio;
    protected JRadioButton whoRadio2;
    protected JComboBox groupList;
    protected JButton saveButton;
    protected JButton cancelButton;

    public JInternalFrame MDIframe;

    private Vector vGroups;

    public AddAlert() { }
    public AddAlert(ITSAdministrator admin, String CQL) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = admin.jdp;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Add An Alert", true, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(675, 480);
        MDIframe.setBackground(Color.lightGray);
        this.admin = admin; this.CQL = CQL;

        MDIframe.setFrameIcon(admin.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        nameLabel = new JLabel("Name");
        nameLabel.setToolTipText("Please choose a name for this alert.");

        nameText = new JTextField();
        nameText.setToolTipText("Please choose a name for this alert.");

        freqLabel = new JLabel("Frequency");
        freqLabel.setToolTipText("Please choose a frequency for this alert.");

        Object[] items = new Object[4];
        items[0] = new FrequencyDrop("Never", "-1");
        items[1] = new FrequencyDrop("Daily", "1");
        items[2] = new FrequencyDrop("Weekly", "7");
        items[3] = new FrequencyDrop("Monthly", "30");

        freqList = new JComboBox(items);
        freqList.setToolTipText("Please choose a frequency for this alert.");

        notesLabel = new JLabel("Notes");
        notesLabel.setToolTipText("Please choose a name for this alert.");

        notesArea = new JTextArea();
        notesArea.setToolTipText("Please choose a name for this alert.");

        whoRadio = new JRadioButton("Attach this alert to my account only");
        whoRadio.setSelected(true);
        whoRadio.setToolTipText("Attach this alert to my account only.");

        whoRadio2 = new JRadioButton("Attach this alert to a project or group");
        whoRadio2.setToolTipText("Attach this alert to a project or group.");

        //Create a ButtonGroup object, add buttons to the group
        ButtonGroup myButtonGroup = new ButtonGroup();
		myButtonGroup.add(whoRadio);
		myButtonGroup.add(whoRadio2);

        // retrieve the list of LDAP groups that this user is in
        vGroups = new Vector();
        try { vGroups = admin.its.getMyGroups(); }
        catch (Exception e) { e.printStackTrace(System.out); }

        groupList = new JComboBox(vGroups);

        saveButton = new JButton("Save Alert");
        saveButton.setToolTipText("Store this alert for future use.");
        saveButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setToolTipText("Cancel this action.");
        cancelButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestGrid(2,2);
                {
                    layout.add(nameLabel);
                    layout.add(nameText);
                    layout.add(freqLabel);
                    layout.add(freqList);
                    layout.parent();
                }
                layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(notesLabel);
                    layout.addSpace(32);
                    layout.add(new JScrollPane(notesArea));
                    layout.parent();
                }
                layout.nestGrid(2,2);
                {
                    layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                    {
                        layout.addSpace(60);
                        layout.parent();
                    }
                    layout.add(whoRadio);
                    layout.addSpace(1);
                    layout.nestBox(OculusLayout.HORIZONTAL,OculusLayout.TOP_JUSTIFY);
                    {
                        layout.add(whoRadio2);
                        layout.add(groupList);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(saveButton);
            layout.addSpace(10);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    public void actionPerformed(ActionEvent e) {
        // if cancel button is depressed, close the window with no changes
        if (e.getSource().equals(cancelButton)) { MDIframe.dispose(); return; }

        User u = ITS.getUser(); String DN = u.getID();

        // if save button is depressed save this alert to the server database
        if (whoRadio2.isSelected() && (groupList.getSelectedItem() == null)) {
            JOptionPane.showMessageDialog(this, "Error: Cannot save this alert, no alert group specified.",
                    "Error", JOptionPane.NO_OPTION);
            return;
        }
        if (whoRadio2.isSelected()) { DN = ((AlertGroup) groupList.getSelectedItem()).getID(); }

        String email = u.getEmail();
        if ((u.getEmail() == null) || (u.getEmail().equals("null"))) {
            email = "info@intellisophic.com"; DN = "CN=Nobody"; }

        Alert a = new Alert(DN, nameText.getText(), DN, email, CQL, notesArea.getText(),
                (FrequencyDrop) freqList.getSelectedItem());
        try { admin.its.saveAlert(a); }
        catch (Exception ex) { ex.printStackTrace(System.out); }

        MDIframe.dispose();
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }}
