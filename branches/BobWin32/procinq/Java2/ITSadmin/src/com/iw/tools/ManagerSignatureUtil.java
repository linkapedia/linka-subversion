/*
 * 
 * @autor Andres Felipe Restrepo
 * @version 1.0
 * class for check signatures
 */
package com.iw.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyStore.Builder;
import java.util.ArrayList;

import org.jfree.util.Log;

public class ManagerSignatureUtil {

	private File dictionary;
	private ArrayList<String> arrayArchive = new ArrayList<String>();;
	private static String PATH_DICTIONARY;
	
	static{
		try {
			PATH_DICTIONARY=ManageResource.getResource("files/Dictionary/words1.txt").toString();
		} catch (Exception e) {
			Log.error("Error get resource dictionary: "+e.getMessage());
		}
		
	}
	/**
	 *Constructor 
	 */
	public ManagerSignatureUtil(){}
	
	/**
	 * search signature in the dictionary
	 * @param signatures
	 * @return ArrayList : signatures not found
	 */
	public ArrayList<String> returnSignaturesNotFound(ArrayList<String> signatures){
		boolean ban;   //logic for search
		ArrayList<String> signaturesNotFound= new ArrayList<String>();
		loadArchiveToArraylist();
		//Search signature into of the array
    	for(String sig:signatures){
    		ban=false;
    		
    		for(String sig1:arrayArchive){
    			if(sig.equals(sig1)){
    				ban=true;
    				break;
    			}
    		}
        	
        	if(!ban){
        		signaturesNotFound.add(sig);
        	}	
        	
    	}
    	return signaturesNotFound;
	}
		
	
	/**
	 * Write in the file
	 * @param signatures: list the signatures to write
	 * @return
	 */
    public boolean writeInFile(ArrayList<String> signatures){
		FileWriter fw=null;
		PrintWriter pw = null;
		try {
			fw = new FileWriter(PATH_DICTIONARY,true);
			pw = new PrintWriter(fw);
			 for(String sig : signatures){
				 pw.print("\n".concat(sig));
			 }
			 return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pw.close();
				fw.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return false;

    	
    }
    /**
     * load dictionary to array
     */
	private void loadArchiveToArraylist(){
		dictionary = new File(PATH_DICTIONARY);
		FileReader fr = null;
		BufferedReader br=null;
		
		try {
			fr =new FileReader(dictionary);
			br= new BufferedReader(fr);
			
			String line="";
			
			while((line=br.readLine())!=null){
				arrayArchive.add(line);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}	
	}
	
}
