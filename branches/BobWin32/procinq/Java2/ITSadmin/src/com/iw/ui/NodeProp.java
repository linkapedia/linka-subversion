package com.iw.ui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import org.jfree.util.Log;

import com.iw.system.ITS;
import com.iw.system.ITSTreeNode;
import com.iw.tools.NodeTypeBean;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NodeProp extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 405953607284000861L;
	private static final List<NodeTypeBean> objects;
	private JCheckBox chckbxNewCheckBox;
	private JButton btnNewButton_1;
	private JCheckBox chckbxNewCheckBox_1;
	private JButton btnNewButton;
	private JComboBox comboBox;
	private JButton btnNewButton_2;
	private JButton btnOk;

	private ITSTreeNode n;

	static {
		objects = new ArrayList<NodeTypeBean>();
		LoadFileNodeType();
	}

	/**
	 * Create the panel.
	 */
	public NodeProp(JFrame parent, ITSTreeNode node, final ITS its) {
		super(parent, true);		
		try {
			this.n = its.getNodeProps(node.get("NODEID"));
		} catch (Exception e2) {
			System.err.println("Error load properties "+e2.getMessage());
		}
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				NodeProp.class.getResource("/itsimages/logo.gif")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Node properties to: " + node.get("NODETITLE") + ". " + "("
				+ node.get("NODEID") + ")");
		setResizable(false);
		setModal(true);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JSeparator separator = new JSeparator();
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.insets = new Insets(0, 0, 5, 5);
		gbc_separator.gridx = 1;
		gbc_separator.gridy = 1;
		getContentPane().add(separator, gbc_separator);

		chckbxNewCheckBox = new JCheckBox("List Node");
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.anchor = GridBagConstraints.WEST;
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox.gridx = 1;
		gbc_chckbxNewCheckBox.gridy = 2;
		getContentPane().add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);

		btnNewButton_1 = new JButton("Recursively");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					its.updateListNodeR(n.get("NODEID"));
				} catch (Exception e1) {
					System.err.println("Error updating NODETYPE: "
							+ e1.getMessage());
				}
			}
		});
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.anchor = GridBagConstraints.WEST;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_1.gridx = 3;
		gbc_btnNewButton_1.gridy = 2;
		getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);

		chckbxNewCheckBox_1 = new JCheckBox("Bridge Node");
		chckbxNewCheckBox_1.setEnabled(false);
		GridBagConstraints gbc_chckbxNewCheckBox_1 = new GridBagConstraints();
		gbc_chckbxNewCheckBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox_1.gridx = 1;
		gbc_chckbxNewCheckBox_1.gridy = 3;
		getContentPane().add(chckbxNewCheckBox_1, gbc_chckbxNewCheckBox_1);

		btnNewButton = new JButton("Find Bridge Node");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//find bridge node from current node
				try {
					setCursor(Cursor
							.getPredefinedCursor(Cursor.WAIT_CURSOR));
					its.findBridgeNodes(n.get("NODEID"));
				} catch (Exception e1) {
					System.err.println("Error process Find Bridge nodes "
							+ e1.getMessage());
				}finally{
					setCursor(Cursor
							.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
				
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 3;
		gbc_btnNewButton.gridy = 3;
		getContentPane().add(btnNewButton, gbc_btnNewButton);

		JLabel lblNewLabel = new JLabel("Node Type");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 4;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 3;
		gbc_comboBox.gridy = 4;
		getContentPane().add(comboBox, gbc_comboBox);
		comboBox.setPrototypeDisplayValue("XXXXXXXXXXXXX");

		btnNewButton_2 = new JButton("Recursively");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					its.updateNodeTypeR(n.get("NODEID"));
				} catch (Exception e1) {
					System.err.println("Error updating NODETYPE: "
							+ e1.getMessage());
				}
			}
		});
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_2.gridx = 4;
		gbc_btnNewButton_2.gridy = 4;
		getContentPane().add(btnNewButton_2, gbc_btnNewButton_2);

		btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chckbxNewCheckBox_1.isSelected()) {
					n.set("BRIDGENODE", "1");
				} else {
					n.set("BRIDGENODE", "0");
				}

				if (chckbxNewCheckBox.isSelected()) {
					n.set("LISTNODE", "1");
				} else {
					n.set("LISTNODE", "0");
				}

				if (comboBox.getSelectedItem() instanceof NodeTypeBean) {
					NodeTypeBean obj = (NodeTypeBean) comboBox
							.getSelectedItem();
					n.set("NODE_TYPE", obj.getId());
				} else {
					System.err.println("Error getting nodetype");
				}
				// save data
				try {
					its.editNode(n);
					JOptionPane.showMessageDialog(btnOk,
							"save data ok!: ");
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(btnOk,
							"Not save data, Error: " + e1.getMessage());
				}
			}
		});
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 5, 5);
		gbc_btnOk.gridx = 3;
		gbc_btnOk.gridy = 6;
		getContentPane().add(btnOk, gbc_btnOk);

		loadProperties();

		this.pack();
		this.openAtCenter();
		this.setVisible(true);

	}

	private void loadProperties() {
		if (n.get("BRIDGENODE").equals("1")) {
			chckbxNewCheckBox_1.setSelected(true);
		} else {
			chckbxNewCheckBox_1.setSelected(false);
		}
		if (n.get("LISTNODE").equals("1")) {
			chckbxNewCheckBox.setSelected(true);
		} else {
			chckbxNewCheckBox.setSelected(false);
		}
		String node_Type = n.get("NODETYPE");

		if (objects != null) {
			// load object in combopbox
			comboBox.removeAllItems();
			for (NodeTypeBean nAux : objects) {
				comboBox.addItem(nAux);
			}
			// select the name for the nodetype
			NodeTypeBean nSelected = null;
			if (node_Type != null && !node_Type.isEmpty()) {
				for (NodeTypeBean nAux : objects) {
					if (nAux.getId().equals(node_Type)) {
						nSelected = nAux;
						break;
					}
				}
			}
			if (nSelected != null) {
				comboBox.setSelectedItem(nSelected);
			}
		}
	}

	private void openAtCenter() {
		Dimension winsize = this.getSize(), screensize = Toolkit
				.getDefaultToolkit().getScreenSize();
		this.setLocation((screensize.width - winsize.width) / 2,
				(screensize.height - winsize.height) / 2);
	}

	private static void LoadFileNodeType() {
		File file = new File("node_type.txt");
		FileReader fr = null;
		BufferedReader br = null;
		String line;
		NodeTypeBean obj = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {
				if (line.equalsIgnoreCase(""))
					break;
				String[] l = line.split(",");
				obj = new NodeTypeBean();
				obj.setId(l[0]);
				obj.setName(l[1]);
				objects.add(obj);
			}
		} catch (FileNotFoundException e1) {
			System.err.println("Error get file " + e1.getMessage());
			Log.error("Error get file: ", e1);
		} catch (IOException e) {
			System.err.println("Error read file " + e.getMessage());
			Log.error("Error read file ", e);
		} finally {
			try {
				if (null != br) {
					br.close();
				}
				if (null != fr) {
					fr.close();
				}
			} catch (IOException e) {
				Log.error("Error clossing file");
			}
		}

	}
}
