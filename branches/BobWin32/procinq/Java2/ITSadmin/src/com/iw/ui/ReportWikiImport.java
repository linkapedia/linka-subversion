package com.iw.ui;

import com.iw.system.ITS;
import java.awt.Cursor;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ReportWikiImport extends JInternalFrame {

    private JTable table;
    private DefaultTableModel modelTable;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");

    public ReportWikiImport(ITSAdministrator frame) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);
        setSize(483, 330);
        setTitle("Report Wiki Import");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(8, 12, 457, 279);
        getContentPane().add(scrollPane);

        table = new JTable();
        modelTable = new DefaultTableModel(
                new Object[][]{},
                new String[]{
            "Id", "Start Point", "Name", "Crawl", "Status"
        });
        table.setModel(modelTable);

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
                setCursor(cursor);
                tableMouseClicked(evt);
                cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                setCursor(cursor);
            }
        });

        scrollPane.setViewportView(table);
        getContentPane().add(scrollPane);
        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            //String nodeid = null;
            int rowindex = table.getSelectedRow();
            //nodeid = table.getModel().getValueAt(rowindex, 0).toString();
            //editNode(nodeid);
        }
    }
}
