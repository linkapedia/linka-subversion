package com.iw.ui;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.iw.ui.conceptalerts.AddAlert;
import com.oculustech.layout.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;
import java.util.Hashtable;

import com.iw.tools.*;
import com.iw.tools.SwingWorker;

public class DocumentSearch extends JPanel implements ActionListener, ListSelectionListener, MouseListener {
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    protected Component glass;

    // Instance attributes used in this example
    protected JInternalFrame MDIframe;
    protected JPanel openPanel;
    protected User user;

    // tools
    protected JButton searchButton;
    protected JButton cancelButton;
    protected JLabel fulltextLabel;
    protected JTextField fulltextText;
    protected JLabel titleLabel;
    protected JTextField titleText;
    protected JLabel queryLabel;
    protected JTextField abstractText;
    protected JLabel topicLabel;
    protected JTextField topicText;
    protected JButton browseButton;
    protected JSortTable resultsTable;
    protected JButton openButton;
    protected JButton relatedButton;
    protected JButton propertiesButton;
    protected JButton documentButton;
    protected JLabel classLabel;
    protected JButton chartButton;
    protected JButton alertButton;

    public DocumentSearch () { }
    public DocumentSearch (ITSAdministrator frame) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        user = ITS.getUser();

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Document Search", true, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(675, 480);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        searchButton = new JButton("Search");
        searchButton.addActionListener(this);
        cancelButton = new JButton("Close");
        cancelButton.addActionListener(this);
        openButton = new JButton("Open");
        openButton.addActionListener(this);
        relatedButton = new JButton("Related");
        relatedButton.addActionListener(this);
        propertiesButton = new JButton("Properties");
        propertiesButton.addActionListener(this);
        documentButton = new JButton("Classify");
        documentButton.addActionListener(this);

        fulltextLabel = new JLabel("Full Text:");
        fulltextLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        fulltextLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        fulltextText = new JTextField();

        titleLabel = new JLabel("Title:");
        titleLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        titleText = new JTextField();

        queryLabel = new JLabel("Abstract:");
        queryLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        abstractText = new JTextField();

        topicLabel = new JLabel("Topics:");
        topicLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        topicText = new JTextField();

        browseButton = new JButton("...");
        browseButton.addActionListener(this);

        chartButton = new JButton("Chart");
        chartButton.addActionListener(this);
        chartButton.setToolTipText("Generate a bar chart with a historical perspective of this document set.");

        alertButton = new JButton("Alert");
        alertButton.addActionListener(this);
        alertButton.setToolTipText("Create an alert on this query.");

        resultsTable = new JSortTable();

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestGrid(2, 4);
                            {
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(fulltextLabel);
                                    layout.parent();
                                }
                                layout.add(fulltextText);
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(titleLabel);
                                    layout.parent();
                                }
                                layout.add(titleText);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(queryLabel);
                                    layout.parent();
                                }
                                layout.add(abstractText);
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.add(topicLabel);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.add(topicText);
                                    layout.add(browseButton);
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                            {
                                layout.add(new JScrollPane(resultsTable));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(searchButton);
                                        layout.addSpace(10);
                                        layout.add(cancelButton);
                                        layout.addSpace(10);
                                        layout.add(openButton);
                                        layout.addSpace(10);
                                        layout.add(relatedButton);
                                        layout.addSpace(10);
                                        layout.add(propertiesButton);
                                        layout.addSpace(10);
                                        //layout.add(new JSeparator());
                                        layout.add(documentButton);
                                        layout.addSpace(10);
                                        layout.add(chartButton);
                                        layout.addSpace(10);
                                        layout.add(alertButton);
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();

            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(searchButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    protected void loadResults() throws Exception {
        Vector vDocs = new Vector();

        try {
            vDocs = runSearch(fulltextText.getText(), titleText.getText(), "",
                    abstractText.getText(), topicText.getText());
        } catch (IllegalQuery e) {
            JOptionPane.showMessageDialog(ITSframe, "Sorry, your query syntax is invalid.  Please recheck your query " +
                    "syntax and try again.",
                    "Error", JOptionPane.NO_OPTION);
            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);
            return;
        }

        if (vDocs.size() == 0) {
            JOptionPane.showMessageDialog(ITSframe, "No documents were found to match your search criteria.",
                    "Information", JOptionPane.NO_OPTION);

            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);
            return;
        }

        DefaultSortTableModel dtm = new DefaultSortTableModel(vDocs.size(), 3);
        dtm.setEditable(false);
        for (int i = 0; i < vDocs.size(); i++) {
            Document d = (Document) vDocs.elementAt(i);
            dtm.setValueAt(d, i, 0);
            dtm.setValueAt(d.get("DOCUMENTSUMMARY"), i, 1);
            dtm.setValueAt(d.get("DOCURL"), i, 2);

        }
        Vector v = new Vector();
        v.add("Document Title");
        v.add("Abstract");
        v.add("Location");
        dtm.setColumnIdentifiers(v);

        resultsTable.setModel(dtm);
    }

    public void viewDocument() {
        try {
            //if ( jtableDocData.getRowCount() == 0 && jtfSearchString.getText().equals("") )
            //    jtfSearchString.setText("eye wash");
            //else if ( jtableDocData.getRowCount() == 0 && jtfSearchString.getText().equals("eye wash") )
            //    jtfSearchString.setText("football company which moved");


            int iSelectedRow = resultsTable.getSelectedRow();
            if (iSelectedRow < 0) {
                JOptionPane.showMessageDialog(ITSframe, "You must first select a document to view.", "Information", JOptionPane.NO_OPTION);
                return;
            }
            int row = resultsTable.getSelectedRow();
            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);

            String sCommand = "";
            if (ITS.getUser().getPathPrefix().equals("")) {
                sCommand = "cmd /c explorer \"" + d.get("DOCURL")+"\"";
            } else {
                sCommand = "cmd /c explorer \"" + ITS.getUser().getPathPrefix() + d.get("DOCURL") +"\"";
            }

            //System.out.println("start view doc with command [" + sCommand + "]");
            java.lang.Runtime.getRuntime().exec(sCommand);


        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            ITSframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void valueChanged(ListSelectionEvent event) {
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        final DocumentSearch ds = this;

        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    if (e.getSource().equals(cancelButton)) {
                        MDIframe.dispose();
                    } else if (e.getSource().equals(browseButton)) {
                        try {
                            TopicSearch ts = new TopicSearch(ITSframe, ds);
                            ts.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    } else if (e.getSource().equals(searchButton)) {
                        try {
                            loadResults();
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    } else if (e.getSource().equals(chartButton)) { // open chart
                        if (resultsTable.getRowCount() == 0) { return; }

                        Vector v = new Vector();
                        for (int i = 0; i < resultsTable.getRowCount(); i++) {
                            try { v.add((com.iw.system.Document) resultsTable.getModel().getValueAt(i, 0)); }
                            catch (ClassCastException e) { }
                        }
                        DocumentChart dc = new DocumentChart(ITSframe, v);
                        ITSframe.jdp.add(dc);
                        dc.setBounds(10, 30, 900, 580);
                        dc.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        dc.setVisible(true);
                        dc.moveToFront();
                        dc.show();
                    } else if (e.getSource().equals(alertButton)) { // open create alert dialog
                        String sCQL = buildCQL(fulltextText.getText(), titleText.getText(), "",
                                               abstractText.getText(), topicText.getText());

                        AddAlert aa = new AddAlert(ITSframe, sCQL);
                        aa.MDIframe.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        aa.setVisible(true);
                        aa.MDIframe.moveToFront();
                        aa.show();

                    } else if (e.getSource().equals(openButton)) {
                        viewDocument();
                    } else if (e.getSource().equals(propertiesButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row >= 0) {
                            com.iw.system.Document d = (com.iw.system.Document) resultsTable.getModel().getValueAt(row, 0);
                            DocumentProperties dp = new DocumentProperties(ITSframe, d);
                            dp.show();
                        }
                    } else if (e.getSource().equals(relatedButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row >= 0) {
                            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);
                            try {
                                NodeDocView ndv = new NodeDocView(ITSframe, d, null);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    } else if (e.getSource().equals(documentButton)) {
                        int row = resultsTable.getSelectedRow();
                        if (row >= 0) {
                            Document d = (Document) resultsTable.getModel().getValueAt(row, 0);
                            if (user.modeAbstractDocument() && d.get("DOCUMENTSUMMARY").equals("")) {
                                JOptionPane.showMessageDialog(ITSframe, "Sorry, that abstract is empty.", "Information", JOptionPane.NO_OPTION);
                                return;
                            }
                            try {
                                NodeDocView ndv = new NodeDocView(ITSframe, d, user);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public String buildCQL(String FullText, String Title, String Bibliography, String DocSummary, String Keywords)
    throws Exception {

        // retrieve all query data
        String fulltextb = FullText;
        String titleb = Title;
        String bibliographyb = Bibliography;
        String abstractbox = DocSummary;
        String nodetitle = Keywords;

        // remove trailing and leading spaces from the query
        fulltextb = fulltextb.trim();
        titleb = titleb.trim();
        bibliographyb = bibliographyb.trim();
        abstractbox = abstractbox.trim();
        nodetitle = nodetitle.trim();

        fulltextb = fulltextb.replaceAll("'", "''");
        titleb = titleb.replaceAll("'", "''");
        bibliographyb = bibliographyb.replaceAll("'", "''");
        abstractbox = abstractbox.replaceAll("'", "''");
        nodetitle = nodetitle.replaceAll("'", "''");

        // build CQL query string
        CQLControls cq = new CQLControls();

        String sCQL = "SELECT <DOCUMENT> WHERE ";
        // Build the CQL advanced query.  on a failure return error
        try {
            //load the country dropdown lists
            if (!fulltextb.equals("")) { //full text
                sCQL = sCQL + "(" + cq.BuildQuery(fulltextb, "FULLTEXT") + ")";
            }
            if (!titleb.equals("")) { // title
                if (sCQL.endsWith(")")) {
                    sCQL = sCQL + " AND ";
                }
                sCQL = sCQL + "(" + cq.BuildQuery(titleb, "DOCTITLE") + ")";
            }
            if (!bibliographyb.equals("")) { //bibliography
                if (sCQL.endsWith(")")) {
                    sCQL = sCQL + " AND ";
                }
                sCQL = sCQL + "(" + cq.BuildQuery(bibliographyb, "BIBLIOGRAPHY") + ")";
            }
            if (!abstractbox.equals("")) { //abstract
                if (sCQL.endsWith(")")) {
                    sCQL = sCQL + " AND ";
                }
                sCQL = sCQL + "(" + cq.BuildQuery(abstractbox, "DOCUMENTSUMMARY") + ")";
            }
            if (!nodetitle.equals("")) { //nodetitle
                if (sCQL.endsWith(")")) {
                    sCQL = sCQL + " AND ";
                }
                sCQL = sCQL + "(" + cq.BuildQuery(nodetitle, "NODETITLE", "=") + ")";
            }
        } catch (IllegalQuery iq) {
            throw iq;
        } catch (Exception e) {
            throw e;
        }

        return sCQL;
    }

    // return a vector of documents
    public Vector runSearch(String FullText, String Title, String Bibliography, String DocSummary, String Keywords)
            throws Exception {
        String sCQL = "";

        try {
            sCQL = buildCQL(FullText, Title, Bibliography, DocSummary, Keywords);
        } catch (IllegalQuery iq) {
            JOptionPane.showMessageDialog(ITSframe, "Sorry, your query syntax is invalid.  A field may not begin " +
                    "with a NOT and contain subsequent NOT operations within the same field.",
                    "Error", JOptionPane.NO_OPTION);
            DefaultSortTableModel dtm = new DefaultSortTableModel();
            resultsTable.setModel(dtm);

            throw iq;
        } catch (Exception e) { throw e; }

        //System.out.println("CQL: " + sCQL);
        return its.CQL(sCQL);
    }
}
