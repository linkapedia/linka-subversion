package com.iw.system;

import java.util.Vector;

public class Genre implements Comparable {
    // attributes
    private String GenreID;
    private String GenreName;
    private boolean GenreActive = false;
    private boolean bModified = false;

    // constructor(s)
    public Genre () { }
    public Genre (HashTree ht) {
        GenreID = (String) ht.get("GENREID");
        GenreName = (String) ht.get("GENRENAME");
        String Status = (String) ht.get("GENRESTATUS");

        if (Status.equals("1")) { GenreActive = true; }
    }

    public Genre (Vector v) {
        GenreID = (String) v.elementAt(0);
        GenreName = (String) v.elementAt(1);
        String Status = (String) v.elementAt(1);

        if (Status.equals("1")) { GenreActive = true; }
    }

    // accessor functions
    public String getID() { return GenreID; }
    public String getName() { return GenreName; }
    public boolean getActive() { return GenreActive; }
    public boolean isModified() { return bModified; }

    public void setID(String ID) { GenreID = ID; }
    public void setName(String Name) { GenreName = Name; }
    public void setActive(boolean Active) { GenreActive = Active; }
    public void setModified(boolean Modified) { bModified = Modified; }

    public String toString() { return GenreName; }
    public int compareTo(Object o) { return GenreName.toLowerCase().compareTo(o.toString().toLowerCase()); }
}
