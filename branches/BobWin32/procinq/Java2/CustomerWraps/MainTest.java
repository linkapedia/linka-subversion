// MainTest

import com.iw.system.*;

import java.util.*;
import java.io.*;

public class MainTest {
    public static void main(String args[]) throws Exception {
        System.out.println("This program will test the API functions used for the Saffron-Indraweb integration.\n");

        // for this test, specify a text file to represent a piece of unstructured data
        File f = new File("C:\\data.txt");
        Vector classifyResults = new Vector();

        // Step 1. Initialize
        ProcinQ server = new ProcinQ();
        server.setAPI("127.0.0.1");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("mpuscar", "racer9");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        // Step 3. Classify a piece of unstructured data against the database.
        //   The first parameter is the pointer to a file containing the unstructured data.
        //   The second parameter specifies whether the classification results should be
        //       persistently stored in the ProcinQ database.
        //   The third parameter represents the list of taxonomies (by ID) that the data is to
        //       be classified against.  Multiple corpora should be separated by commas.  To
        //       retrieve the list of available corpora, use the server.getCorpora() wrapper function.
        System.out.println("Attempting to classify: "+f.getAbsolutePath());
        try { classifyResults = server.classify(f, false, "100003"); }
        catch (SessionExpired se) {
            System.err.println("Session expired."); return; }
        catch (ClassifyError ce) {
            System.err.println("Classification failed.");
            ce.printStackTrace(System.err); return;
        }
        if (classifyResults.size() == 0) { System.err.println("Classification returned no results."); return; }
        System.out.println("Classification complete.\n");

        // Step 4. Loop through the classification results and print detailed concept information
        for (int i = 0; i < classifyResults.size(); i++) {
            NodeDocument nd = (NodeDocument) classifyResults.elementAt(i);

            // customer note: attributes of the ND object are dynamic.  See API for a base set of attributes.
            //                additional attributes can be added simply by adding fields to the database table.
            System.out.println("Result: "+nd.get("NODETITLE"));

            // finally, this function will give you the context of the concepts that were returned
            Vector nodeTree = server.getNodeTree(nd.get("NODEID"));

            for (int j = nodeTree.size()-1; j > 0; j--) {
                if (j == nodeTree.size()-1) System.out.print("   ");
                else System.out.print(" > ");

                Node n = (Node) nodeTree.elementAt(j);
                System.out.print(n.get("NODETITLE"));
            }

            System.out.println("\n");
        }

    }
}