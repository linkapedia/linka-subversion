package com.iw.system;

import java.util.*;
import java.io.*;
import java.net.*;

import com.iw.fcrawl.*;

public class InvokeAPI
{
	private String sTaxonomyServer;
	private String sAPIcall;	
	private Hashtable htArguments;
	private String SessionKey = null;

    final private String boundary = "ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
    final private String twoHyphens = "--";
    final private String lineEnd = "\r\n";

	// Accessor methods to private variables
	public String GetTaxonomyServer() { return sTaxonomyServer; }
	public String GetAPIcall() { return sAPIcall; }
	public String GetSessionKey() { return SessionKey; }
	public Hashtable GetAPIarguments() { return htArguments; }
	
	// Object constructors
	public InvokeAPI (String sAPIcall, Hashtable htArguments, String sTaxonomyServer) {
		
		this.sAPIcall = sAPIcall;
		this.sTaxonomyServer = sTaxonomyServer;
		this.htArguments = htArguments;
		
		if (htArguments.containsKey("SKEY")) { this.SessionKey = (String) htArguments.get("SKEY"); }
		
	}
	public InvokeAPI (String sAPIcall, Hashtable htArguments) {
		
		this.sAPIcall = sAPIcall;
		this.htArguments = htArguments;
		this.sTaxonomyServer = (String) htArguments.get("api");

		if (htArguments.containsKey("SKEY")) { this.SessionKey = (String) htArguments.get("SKEY"); }
	}
	
	public String CreateName (Hashtable htHash, String sName) {
		// If the tag already exists, append to it and make it unique
        int loop = 0;
		if (htHash.containsKey(sName)) {
			loop++; sName = sName + loop;
			sName = CreateName(htHash, sName);
		} 
		
		return sName;
	}
	
	// Send POST API call
    public HashTree Execute (String filename, String sURL) throws ClassifyException {
        return Execute(filename, sURL, false);
    }

	public HashTree Execute (String filename, String sURL, boolean bDebug) throws ClassifyException {
        FileInputStream fileInputStream = null;
	    HttpURLConnection httpURLConn = null;
        try {
			HashTree htOriginal = new HashTree();
			htOriginal.SetParent(null);
			HashTree htObject = htOriginal;

            // fields used to trap error information
            String TS_ERROR_CODE = "-1";
            String TS_ERROR_CODE_STRING = "";
            String TS_ERROR_TYPE = "";
            String TS_ERROR_DESC = "";
            String TS_EXCEPTION_GETMESSAGE = "";

			// Do a multi-part post
			String boundary="ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
		    String twoHyphens="--";
			String lineEnd="\r\n";


			DataOutputStream outStream;
			DataInputStream inStream;
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
	        int maxBufferSize = 1*1024*1024;

	        // create FileInputStream to read from a text (.txt) file (case insens.)
            boolean bTextFile = false;
            if ( filename.toLowerCase().trim().endsWith(".txt"))
                bTextFile = true;

			fileInputStream = new FileInputStream(new File(filename));

			sURL = sURL+sAPIcall;
			Enumeration et = htArguments.keys();
			
			// Build URL to call Taxonomy Server
			while (et.hasMoreElements()) {
				String sKey = (String) et.nextElement();
				String sValue = (String) htArguments.get(sKey);
			
				sURL = sURL+"&"+sKey+"="+sValue; 
				
				// Remove any apostraphies (because they are illegal) and change
				// spaces into + signs
				sURL = new String(UtilStrings.replaceStrInStr(sURL," ","+"));
				sURL = new String(UtilStrings.replaceStrInStr(sURL,"'",""));
			}

			//System.out.println(Thread.currentThread().getName() + " "+ new java.util.Date() + "Invoke API URL: "+sURL);
            URL theURL = new URL(sURL);
            httpURLConn  = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

			// Set up POST parameters
			StringBuffer params = new StringBuffer();
		    params.setLength(0);
			Enumeration e = htArguments.keys();
			params.append("fn="+sAPIcall);
			
            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream (httpURLConn.getOutputStream());

			outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\""+ filename + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable,maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            if ( bTextFile ) {
                outStream.writeBytes("<HTML>r\n");
                outStream.writeBytes("<HEAD>\r\n<TITLE>");
                int iIndexLastSlash1 = filename.lastIndexOf("\\");
                int iIndexLastSlash2 = filename.lastIndexOf("/");
                int iIndexLastSlash = Math.max (iIndexLastSlash1, iIndexLastSlash2);
                outStream.writeBytes(filename.substring(iIndexLastSlash+1));
                outStream.writeBytes("</TITLE>\r\n</HEAD>\r\n");
                outStream.writeBytes("<BODY>\r\n");
            }

            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable,maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
            if ( bTextFile ) {
                outStream.writeBytes("</BODY>\r\n</HTML>");
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close(); fileInputStream = null;
            outStream.flush (); outStream.close ();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream ();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();
			
			while ((sData = buf.readLine()) != null) {
				if (bDebug) System.out.println(sData);
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");
				
				String sTag1;
				
				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }
				
				String sTag2 = "";
				
				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) { 
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);
				
				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) && 
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
 					 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						sTag1 = new String (CreateName(htObject, sTag1));
						//out.println("<BR>Begin object: "+sTag1);

                        // **************************************************************
                        // TRAP ERRORS IF APPLICABLE
                        if (sTag1.equals("TS_ERROR_CODE")) {
                            TS_ERROR_CODE = UtilStrings.getStringBetweenThisAndThat
                                    (sData, "<TS_ERROR_CODE>", "</TS_ERROR_CODE>");
                        }
                        if (sTag1.equals("TS_ERROR_CODE_STRING")) {
                            TS_ERROR_CODE_STRING = UtilStrings.getStringBetweenThisAndThat
                                    (sData, "<TS_ERROR_CODE_STRING>", "</TS_ERROR_CODE_STRING>");
                        }
                        if (sTag1.equals("TS_ERROR_TYPE")) {
                            TS_ERROR_TYPE = UtilStrings.getStringBetweenThisAndThat
                                    (sData, "<TS_ERROR_TYPE>", "</TS_ERROR_TYPE>");
                        }
                        if (sTag1.equals("TS_ERROR_DESC")) {
                            TS_ERROR_DESC = UtilStrings.getStringBetweenThisAndThat
                                    (sData, "<TS_ERROR_DESC>", "</TS_ERROR_DESC>");
                        }
                        if (sTag1.equals("TS_EXCEPTION_GETMESSAGE")) {
                            TS_EXCEPTION_GETMESSAGE = UtilStrings.getStringBetweenThisAndThat
                                    (sData, "<TS_EXCEPTION_GETMESSAGE>", "</TS_EXCEPTION_GETMESSAGE>");
                        }

						if (sTag1.equals("TS_EXCEPTION_STACKTRACE")) {
							throw new ClassifyException(TS_ERROR_CODE, TS_ERROR_CODE_STRING, TS_ERROR_TYPE,
                                    TS_ERROR_DESC, TS_EXCEPTION_GETMESSAGE);
						}
				
						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1,htObject);
						
					}
				
					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2); 
						htObject = (HashTree) htObject.GetParent();
					}
				
					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String (UtilStrings.getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						//out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->"); 
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag); 
						
						// If the variable value contains a "CDATA", eliminate it here
						if (UtilStrings.getStrContains(sTag,"<![CDATA")) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}
						
						htObject.put(sTag1, sTag);
					}
				}
			}
			
			return htObject;
		}
        catch (ClassifyException ce) { throw ce; }
		catch (Exception except) {
			except.printStackTrace(System.out);
			return null;
		} finally {
            try {
                if (fileInputStream != null) { fileInputStream.close(); fileInputStream = null; } }
            catch (Exception e) { fileInputStream = null; }
            if (httpURLConn != null) { httpURLConn.disconnect(); httpURLConn = null; }
        }

	}
		
    // Send POST API call
    public HashTree PostExecute(File f) throws Exception {
        return PostExecute(f, true); }
    public HashTree PostExecute(File f, boolean bUseImportDir) throws Exception {
        try {
            HashTree htOriginal = new HashTree();
            htOriginal.SetParent(null);
            HashTree htObject = htOriginal;

            HttpURLConnection httpURLConn = null;
            DataOutputStream outStream;
            DataInputStream inStream;
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            // create FileInputStream to read from file
            FileInputStream fileInputStream = new FileInputStream(f);

            String sURL = sTaxonomyServer + sAPIcall;
            Enumeration et = htArguments.keys();

            //System.out.println("Tax server: "+sTaxonomyServer+" API: "+sAPIcall);

            // Build URL to call Taxonomy Server
            while (et.hasMoreElements()) {
                String sKey = (String) et.nextElement();
                String sValue = (String) htArguments.get(sKey);

                sURL = sURL + "&" + sKey + "=" + sValue;

                // Remove any apostraphies (because they are illegal) and change
                // spaces into + signs
                sURL = sURL.replaceAll(" ", "+");
                sURL = sURL.replaceAll("'", "");
            }

            URL theURL = new URL(sURL);
            httpURLConn = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream(httpURLConn.getOutputStream());

            outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\"" + f.getName() + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();
            outStream.flush();
            outStream.close();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                //api.Log.Log(sData);
                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = new String("");
                }

                // HACK! HACK! Ignore lines that start with..
                if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) &&
                        (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
                        (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
                        (!sTag1.equals("IndraXMLArgs")) &&
                        (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

                    // If s1Tag is populated but s2Tag is not, start the object
                    if ((!sTag1.equals("")) && (sTag2.equals(""))) {

                        sTag1 = new String(CreateName(htObject, sTag1));
                        //out.println("<BR>Begin object: "+sTag1);

                        if (sTag1.equals("TS_ERROR")) {
                            return htOriginal;
                        }
                        if (sTag1.equals("INTERNALSTACKTRACE")) {
                            throw new Exception("Internal stack trace");
                        }

                        HashTree htOldObject = htObject;
                        htObject = new HashTree();
                        htObject.SetParent(htOldObject);
                        htOldObject.put(sTag1, htObject);

                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if ((!sTag2.equals("")) && (sTag1.equals(""))) {
                        // out.println("<BR>End object: "+sTag2);
                        htObject = (HashTree) htObject.GetParent();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                        String sTag = new String(getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">"));
                        //out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
                        // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                        // If the variable value contains a "CDATA", eliminate it here
                        if (sTag.indexOf("<![CDATA") != -1) {
                            sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                        }

                        htObject.put(sTag1, sTag);
                    }
                }
                else
                {
                    //System.out.println("InvokeAPI ignoring XML line [" + sTag1 + "]" );
                }
            }

            return htObject;
        } catch (Exception except) {
            throw except;
        }

    }

    public HashTree Execute (boolean b1, boolean b2) { return Execute(); }

	// Execute the API call.   Returns success 0 for success, -1 for failure.
	public HashTree Execute () {
		String sURL = sTaxonomyServer+sAPIcall;
		Enumeration e = htArguments.keys();

		HashTree htOriginal = new HashTree();
		htOriginal.SetParent(null);
		HashTree htObject = htOriginal;
			
		// Build URL to call Taxonomy Server
		while (e.hasMoreElements()) {
			String sKey = (String) e.nextElement();
			String sValue = (String) htArguments.get(sKey);
			
			if (!sKey.equals("api")) {
				sURL = sURL+"&"+sKey+"="+sValue; 
			
				// Remove any apostraphies (because they are illegal) and change
				// spaces into + signs
				sURL = new String(UtilStrings.replaceStrInStr(sURL," ","+"));
				sURL = new String(UtilStrings.replaceStrInStr(sURL,"'",""));
			}
		}
		
		// Make a socket connection to the server
		try {
			long lStart = System.currentTimeMillis();

		 	URL myURL = new URL(sURL);
			// System.out.println(Thread.currentThread().getName() + " API call: "+sURL);
			HttpURLConnection httpCon = (HttpURLConnection) myURL.openConnection(); 
		
			if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) { 
				throw new Exception("Http error: " + httpCon.getResponseMessage());
			} 
		
			InputStream is = httpCon.getInputStream();
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String sData = new String();
			
			while ((sData = buf.readLine()) != null) {
				//System.out.println(sData);
				int iTag1Start = sData.indexOf("<");
				int iTag1End = sData.indexOf(">");
				int iTag2Start = sData.lastIndexOf("</");
				int iTag2End = sData.lastIndexOf(">");
				
				String sTag1;
				
				try { sTag1 = sData.substring(iTag1Start+1, iTag1End); }
				catch (Exception eTag) { sTag1 = ""; }
				
				String sTag2 = "";
				
				if (iTag1End != iTag2End) { sTag2 = sData.substring(iTag2Start+2, iTag2End); }

				// This is how we detect an END tag
				if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) { 
					sTag2 = new String(sTag1.substring(1));
					sTag1 = new String("");
				}

				Integer iVariance = new Integer(0);
				
				// HACK! HACK! Ignore lines that start with..
				if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?")) && 
					 (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT")) &&
					 (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT")) &&
 					 (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

					// If s1Tag is populated but s2Tag is not, start the object
					if ((!sTag1.equals("")) && (sTag2.equals(""))) {

						sTag1 = new String (CreateName(htObject, sTag1));
						//out.println("<BR>Begin object: "+sTag1); 

						if (sTag1.equals("TS_ERROR")) {
							httpCon.disconnect();
							return htOriginal;
						}
						if (sTag1.equals("INTERNALSTACKTRACE")) {
							httpCon.disconnect();
							return htOriginal;
						}
				
						HashTree htOldObject = htObject;
						htObject = new HashTree();
						htObject.SetParent(htOldObject);
						htOldObject.put(sTag1,htObject);
						
					}
				
					// if s2Tag is populated but s1Tag is not, end the object
					if ((!sTag2.equals("")) && (sTag1.equals(""))) {
						// out.println("<BR>End object: "+sTag2); 
						htObject = (HashTree) htObject.GetParent();
					}
				
					// if both s1Tag and s2Tag are populated, store the field into object started
					if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
						String sTag = new String (UtilStrings.getStringBetweenThisAndThat(sData, "<"+sTag1+">", "</"+sTag2+">"));
						//out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->"); 
						// out.println("<BR>Add tag: "+sTag1+" value: "+sTag); 
						
						// If the variable value contains a "CDATA", eliminate it here
						if (UtilStrings.getStrContains(sTag,"<![CDATA")) {
							sTag = new String (sTag.substring(9, (sTag.length()-3)));
						}
						
						htObject.put(sTag1, sTag);
					}
				}
			}
			
			httpCon.disconnect();
			
			return htObject;
		}
		catch (Exception except) {
			return null;
		}
	}


    // **********************************************
	public static String getStringBetweenThisAndThat (String s,
	// **********************************************
													  String s1,
													  String s2
													  )
	{
		return getStringBetweenThisAndThat (s, s1, 1, s2, 1 );
	}


    // **********************************************
	public static int getLocationOfNthOfThese (String s, String s1, int index1 )
	// **********************************************
	{
		int currentPointer = -1;
		int incrementAmount = 0;

        try {
            for (int i = 0; i < index1; i++ ) {
                String curString = s.substring( currentPointer + 1 );
                incrementAmount = curString.indexOf ( s1 );

                if ( incrementAmount == -1 )
                    return -1;
                else
                    currentPointer += incrementAmount + 1;
                }
        } catch ( Exception e )
        {
            return -1;
        }
		return currentPointer;

	}

	// **********************************************
	public static String getStringBetweenThisAndThat (String s,
	// **********************************************
													  String s1,
													  int index1,
													  String s2,
													  int index2 )
	{
		String returnStr = null;

		int startLoc = 0;
		if ( index1 > 0 ) {
			startLoc = getLocationOfNthOfThese (s, s1, index1 ) + 1 ;
			if (startLoc == -1) {
				//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
				return "";    // EARLY RETURN!!!!!!!!!!!!!
			}
		} else {
			System.err.println("invalid parms to getStringBetweenThisAndThat - zero not working - use get up to ... ");
            return null;
		}

		if ( index2 > 0 ) {
			int endLoc = 0;
			int totalLen = startLoc+s1.length();
			if (totalLen == -1) {
				//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
				return "";
			}

			endLoc = getLocationOfNthOfThese (s, s2, index2 ) ;
			if (endLoc == -1) {
				//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
				returnStr = s.substring ( startLoc + s1.length()-1 );
			}
			else
			{
				try {
					returnStr = s.substring ( startLoc + s1.length()-1, endLoc );
				}
				catch ( Exception e )
				{
					//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
					returnStr = "";	// EARLY RETURN!!!!!!!!!!!

				}
			}
		} else {
			returnStr = s.substring ( startLoc+1, s1.length());
		}

		//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + returnStr);

		return returnStr;
		/* test cases :
		// String s = "abcdefghijklmnopqrstuvwxyza23defghijklmnopqrstuvwxyz";

		not working : need a non zero len string to do start , 0
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 1);
		HKonLib.getStringBetweenThisAndThat (s, "e", 2, "n", 1);
		HKonLib.getStringBetweenThisAndThat (s, "a", 0, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "a", 1, "e", 1);
		HKonLib.getStringBetweenThisAndThat (s, "b", 1, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 1, "", 0);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 5, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 5);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 0);

		test getting a comment field's text - from text= to end ... how
		*/
	}
}