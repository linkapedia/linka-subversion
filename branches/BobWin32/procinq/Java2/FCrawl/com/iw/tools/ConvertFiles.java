package com.iw.tools;

import org.pdfbox.Main;
import org.textmining.text.extraction.WordExtractor;

import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

import com.iw.FCrawl;

public class ConvertFiles {
	// This is our MAIN function
	public static void main(String args[]) {
        String sLocation = "E:/Indraweb/Content/crawls/";

        convert(sLocation);
    }

    public static void convert(String sLocation) {
        File fDirectory = new File(sLocation);
		String FilePaths[] = fDirectory.list();

        System.out.println("Total files found (including directories) directly under "+sLocation+" is "+FilePaths.length);

		// loop through each file in this directory
		if (FilePaths != null) {
		for (int i=0; i<FilePaths.length; i++) {
			File f = new File(sLocation+"/"+FilePaths[i]);

			// if this is a directory and recurse is true, continue collecting
			if (f.isDirectory()) {
				convert(sLocation+"/"+FilePaths[i]);
			}
			if (!f.isDirectory() && convertThis(f.getName())) {
                if (f.getName().toLowerCase().endsWith(".pdf")) {
                    File newFile = new File (f.getAbsolutePath().replaceAll("pdffnord", "idrac")+".html");

                    System.out.println("Filtering from PDF: "+f.getAbsolutePath()+" to "+newFile.getAbsolutePath());
                    try { org.pdfbox.Main.PDFtoHTML(f.getAbsolutePath(), newFile.getAbsolutePath()); }
                    catch (Exception e) {
                        System.err.println(e.getMessage()+" trying to filter file "+ f.getAbsolutePath() +" .. skipping ...");
                        return;
                    }
                }
                if (f.getName().toLowerCase().endsWith(".doc")) {
                    File newFile = new File (f.getAbsolutePath().replaceAll("docfnord", "idrac")+".html");

                    System.out.println("Filtering from MSWORD: "+f.getAbsolutePath()+" to "+newFile.getAbsolutePath());
                    try { MSWORDtoHTML(f, newFile); }
                    catch (Exception e) {
                        System.err.println(e.getMessage()+" trying to filter file "+ f.getAbsolutePath() +" .. skipping ...");
                        return;
                    }
                }
			}
		}}

    }

    public static void MSWORDtoHTML(File WordFile, File OutFile) throws Exception {
        FileInputStream in = new FileInputStream (WordFile.getAbsolutePath());
        WordExtractor extractor = new WordExtractor();

        String s = extractor.extractText(in);
        FCrawl.wrapHTML(s, WordFile.getName(), OutFile);
    }

    public static boolean convertThis(String sFilename) {
        if ((sFilename.toLowerCase().endsWith(".pdf")) || (sFilename.toLowerCase().endsWith(".doc"))) { return true; }
        return false;
    }
}