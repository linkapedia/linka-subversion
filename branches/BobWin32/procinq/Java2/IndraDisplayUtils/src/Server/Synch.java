package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;
import com.iw.system.*;

public class Synch
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String Tablespace = (String) props.get ("tablespace", "RDATA");

		try {
			HTMLDocument Document = new HTMLDocument();

			HashTree htArguments = new HashTree(props);
			htArguments.put("Tablespace", Tablespace);
			InvokeAPI API = new InvokeAPI ("tsclassify.TSLoadBatchRun", htArguments);
			HashTree htResults = API.Execute(false, false);
				
			if (!htResults.containsKey("SUCCESS")) {
				throw new Exception(
					"Sorry, your synchronization process could not be completed at this time. "+
                       "Please check the ITS Log file for more information.");
			}
				
			String sSuccess = "Batch synchronization was completed successfully.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.Administer'>"+
							  "Click here to return to the menu</a>.";
			Document.WriteSuccess(out, sSuccess);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
