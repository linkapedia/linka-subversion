package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;
import com.iw.system.*;
import com.iw.classification.Data_WordsToNodes;

public class Purge
{
    public static void doPurge (PrintWriter out, String sCorpusID) {
		try {
            doPurge("unknown", out, sCorpusID);
		} catch (Exception e) { Log.LogError("error invalidating corpus [" + sCorpusID + "]", e, out); }
	}

    public static void doPurge (String sReason, PrintWriter out, String sCorpusID) {
		try {
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus(sReason, Integer.parseInt(sCorpusID));
		} catch (Exception e) { Log.LogError("error invalidating corpus [" + sCorpusID + "]", e, out); }
	}

	// Purge stale and unwanted items from the system
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			// Loop through each corpus, delete cache files in IndraHome pertaining to each corpus.
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tscorpus.TSListCorpora", htArguments);
			HashTree htResults = API.Execute(false, false);
			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}

			if (htResults.containsKey("CORPORA")) {
				HashTree htCorpora = (HashTree) htResults.get("CORPORA");
				Enumeration e2 = htCorpora.elements();
				String sFilePath = com.indraweb.execution.Session.cfg.getProp ("IndraHome")+"/corpussource/";
				
				while (e2.hasMoreElements()) {
					HashTree htCorpus = (HashTree) e2.nextElement();
					Purge.doPurge("Purge.handleTSapiRequest()", out, (String) htCorpus.get("CORPUSID"));
				}
			}
			
			// Delete all expired corpora
			htArguments = new HashTree(props);
			API = new InvokeAPI ("tspurge.TSPurgeCorpora", htArguments);
			htResults = API.Execute(false, false);
			
			// Delete all expired nodes
			htArguments = new HashTree(props);
			API = new InvokeAPI ("tspurge.TSPurgeNodes", htArguments);
			htResults = API.Execute(false, false);

			// Delete all expired documents
			htArguments = new HashTree(props);
			API = new InvokeAPI ("tspurge.TSPurgeDocuments", htArguments);
			htResults = API.Execute(false, false);
			
			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "All objects marked for deletion have been purged.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.Administer'>"+
							  "Click here to return to the menu</a>.";
			Document.WriteSuccess(out, sSuccess);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}	
	}
	
}
