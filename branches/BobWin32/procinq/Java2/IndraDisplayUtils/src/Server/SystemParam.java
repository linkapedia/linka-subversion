package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.URLEncoder;

import com.indraweb.execution.Session;
import HTML.*;
import Logging.*;
import com.iw.system.*;

public class SystemParam
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sSubmit = (String) props.get ("SUBMIT.x");	
		if (sSubmit != null) {
			HashTree htArguments = new HashTree(props);
			Enumeration eS = req.getParameterNames();

			// pass all parameters along to the API
			while (eS.hasMoreElements()) {
				String sK = (String) eS.nextElement();
				String sV = (String) req.getParameter(sK);
				
				if ((!sK.equals("template")) && 
					(!sK.equals("SUBMIT.x")) &&
					(!sK.equals("SUBMIT.y"))) {
					htArguments.put(URLEncoder.encode(sK, "UTF-8"), URLEncoder.encode(sV, "UTF-8"));
				}
			}
			InvokeAPI API = new InvokeAPI ("security.TSSetConfigParams", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			if (htResults.containsKey("SUCCESS")) {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "System parameters updated successfully.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Administer'>"+
								  "Click here to return to the menu</a>.";
				Document.WriteSuccess(out, sSuccess);	
			} else {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "One or more system parameters failed to update.  "+
								  "This is usually do to a permission failure. <BR>  Please "+
								  "contact your system administrator for more details.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Administer'>"+
								  "Click here to return to the menu</a>.";
				Document.WriteSuccess(out, sSuccess);	
			}
		} else {
			try {
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Change system parameters");
				Document.AddVariable("OPTION_CORPUS_LIST", req);
				
				Document.WriteTemplate(out, "header-nosearch.tpl");
				Document.WriteTemplate(out, "system-param-head.tpl");
				
				HashTree htArguments = new HashTree(props);
				htArguments.put("TYPE", "0");
				InvokeAPI API = new InvokeAPI ("security.TSGetConfigParams", htArguments);
				HashTree htResults = API.Execute(false, false);

				if (htResults.containsKey("CONFIGPARAMS")) {
					HashTree htConfig = (HashTree) htResults.get("CONFIGPARAMS");
					Enumeration hC = htConfig.keys();
					while (hC.hasMoreElements()) {
						String sK = (String) hC.nextElement();
						String sV = (String) htConfig.get(sK);
						Document.AddVariable("PARAMNAME", sK);
						Document.AddVariable("PARAMVALUE", sV);
						Document.WriteTemplate(out, "system-param-item.tpl");
					}
				}
				
				Document.WriteTemplate(out, "system-param-middle.tpl");

				htArguments = new HashTree(props);
				htArguments.put("TYPE", "1");
				API = new InvokeAPI ("security.TSGetConfigParams", htArguments);
				htResults = API.Execute(false, false);

				if (htResults.containsKey("CONFIGPARAMS")) {
					HashTree htConfig = (HashTree) htResults.get("CONFIGPARAMS");
					Enumeration hC = htConfig.keys();
					while (hC.hasMoreElements()) {
						String sK = (String) hC.nextElement();
						String sV = (String) htConfig.get(sK);
						Document.AddVariable("PARAMNAME", sK);
						Document.AddVariable("PARAMVALUE", sV);
						Document.WriteTemplate(out, "system-param-item.tpl");
					}
				}
				
				Document.WriteTemplate(out, "system-param-foot.tpl");
				Document.WriteFooter(out);
			}
			catch (Exception e) {
				HTMLDocument Document = new HTMLDocument();
				String sErrorMsg = e.getMessage();
				Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
				Log.LogError(e, out);
			}
		}
	}
}
