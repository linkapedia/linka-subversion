package Server;

import java.io.*;
import java.util.Vector;

import HTML.*;
import Logging.*;
import com.indraweb.execution.Session;

public class Execute {

	public static int CorpusExec(String sFilename, PrintWriter out) {
        try {
            Vector v = doCorpusImportCallout(sFilename);
        } catch(Exception except) {
			out.println("<!--"); except.printStackTrace(out); out.println("-->");
		}
		return 0;
	}

    private static Vector doCorpusImportCallout(String sFilename)
            throws Exception {
        String sOSHost = (String) java.lang.System.getProperties().get("os.name");
        String sExec = null;
        if (sOSHost.toLowerCase().indexOf("windows") >= 0) {
            sExec = "cmd /c perl \"" + Session.sIndraHome + "\\corpus-import\\corpusin.pl\" -filename " +
                    Session.sIndraHome + "\\corpus-import\\" + sFilename;
            api.Log.Log("corpus import sExec [" + sExec + "]");
        }
        else { // linux
            sExec = "exec perl \"" + Session.sIndraHome + "/corpus-import/corpusin.pl\" -filename " +
                    Session.sIndraHome + "\\corpus-import\\" + sFilename;
            api.Log.Log("corpus import sExec [" + sExec + "]");
            //throw new Exception("callout manager for linux not configured");
        }
        com.iw.calloutmgr.CalloutMgr callmgr = new com.iw.calloutmgr.CalloutMgr(
                sExec, true, com.iw.calloutmgr.CalloutMgr.IOUTPUTCAPTUREMODE_All, "Inserting");
        callmgr.execItSynchronous();
        return callmgr.getVecOutput();
    }
}