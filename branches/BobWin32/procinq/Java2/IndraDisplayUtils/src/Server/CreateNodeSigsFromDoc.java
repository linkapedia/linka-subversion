package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;


import java.io.*;
import java.util.*;

import com.iw.system.*;
import HTML.*;
import Logging.*;

public class CreateNodeSigsFromDoc{

    public static void handleTSapiRequest(APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
            throws Exception {
        String sContentType = (String) props.get("Content-type");
        String sFileName = (String) props.get("import");
        //String sFilePxath = (String) props.get("FilePath");

        try {

            boolean bPostMode = false;
            if (sContentType != null) {
                // Send classify request, read and display results
                if ((!sContentType.equals("text/html")) &&
                    (!sContentType.equals("text/plain"))) {
                    //Log.Log("Attempt to classify document of type: "+sContentType);

                    if ( (props.get("docidsource") == null || ((String) props.get("docidsource")).trim().equals(""))
                            &&
                        (props.get("docidabstract") == null || ((String) props.get("docidabstract")).trim().equals("")) )
                        throw new Exception("Error: HTML document required");
                }
                else
                    bPostMode = true;

                HashTree htArgs_api = new HashTree(props);   // passes only skey in
                //htArgs_TSCreateNodeSigsFromDoc.put("POST", "TRUE");
                passArgs ("post", props, htArgs_api, true);
                //passArgs ("parentid", props, htArgs_api);
                //passArgs ("corpusid", props, htArgs_api);
                //passArgs ("nodetitle", props, htArgs_api);
                //passArgs ("nodedesc", props, htArgs_api);
                passArgs ("nodeid", props, htArgs_api, true);
                passArgs ("docidsource", props, htArgs_api, false);
                passArgs ("docidabstract", props, htArgs_api, false);
                //passArgs ("indexwithinparent", props, htArgs_api);
                InvokeAPI API = new InvokeAPI("tsnode.TSCreateSigsFromDoc", htArgs_api);
                HashTree htResults = null;
                if (bPostMode)
                    htResults = API.PostExecute(new File(sFileName));
                else
                    htResults = API.Execute(false, false);

                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Create a Node From Document");
                Document.WriteTemplate(out, "header-admin.tpl");

                boolean bSuccess = false;
                String sUserMsg = "server transaction error";
                String sFail = (String) htResults.get("ERROR");
                if ( sFail != null )
                    sUserMsg = "ERROR: FAIL indicator in response from Create Node From Document call [" + sFail + "]";
                else
                {
                    String sSUCCESS = (String) htResults.get("SUCCESS");
                    if ( sSUCCESS == null )
                        sUserMsg = "Add Node Sigs failure :  [" + sFail + "]";
                    else
                        sUserMsg = "Successful Create Node Signature From a Document completed.";

                }

/*
                HashTree htClass = (HashTree) htResults.get("SIGTERMS");
                Vector vNodes = Sort(htClass);
                Enumeration eT = vNodes.elements();
*/


                // Loop through each result and print it, baby!
                out.println("<blockquote>" + sUserMsg + "<br></blockquote>");

                //Document.WriteTemplate(out, "classify-start.tpl");
/*
                while (eT.hasMoreElements()) {
                    HashTree htNodeBucket = (HashTree) eT.nextElement();
                    float fScore = new Float((String) htNodeBucket.get("SCORE1")).floatValue();

                    // Ensure threshold match
                    loop = loop + 1;
                    if (loop == 1) {
                    }
                    String sNodeTree
                            = Taxonomy.Search.BuildNodeTree(out, (String) htNodeBucket.get("NODEID"), props);
                    sNodeTree = "<LI> <a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID=" +
                            (String) htNodeBucket.get("NODEID") + "'><i><b>" +
                            (String) htNodeBucket.get("NODETITLE") + "</b></i></a> &nbsp; " +
                            "<BR>" + sNodeTree + "<BR>"+ (String) htNodeBucket.get("DOCSUMMARY") +"<P>";
                    Document.SetHash(htNodeBucket);
                    Document.AddVariable("NODETREE", sNodeTree);
                    Document.WriteTemplate(out, "search.tpl");
                }
*/
                Document.WriteTemplate(out, "search-end.tpl");
                Document.WriteFooter(out);

            } else {
                // Just display the classify page
                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Create a Node and Signatures from a Document");
                Document.WriteTemplate(out, "header-admin.tpl");
                //Document.WriteTemplate(out, "classify-post.tpl");
                Document.WriteTemplate(out, "createnodesigs-post.tpl");
                Document.WriteFooter(out);
            }
        } catch (Exception e) {
            HTMLDocument Document = new HTMLDocument();
            Log.LogError(e, out);
            String sErrorMsg = e.getMessage();
            Document.WriteError(out, "<BLOCKQUOTE><BR> error from CreateNodeSigsFromDoc " +  sErrorMsg + "</BLOCKQUOTE>");
        }
    }

    private static void passArgs ( String sKey, Hashtable ht1, Hashtable ht2, boolean bRequired) throws Exception
    {
        if ( bRequired && ht1.get (sKey) == null )
            throw new Exception ("missing parm in CreateNodeSigsFromDoc [" + sKey + "]");

        if ( ht1.get (sKey) != null && !((String) ht1.get (sKey)).trim().equals(""))
        {
            ht2.put(sKey, ht1.get (sKey));
            //System.out.println("passing sKey [" + sKey + "] ht1.get (sKey) [" + ht1.get (sKey)+ "]" );
        }

        //else
            //System.out.println("not passing sKey [" + sKey + "] ht1.get (sKey) [" + ht1.get (sKey)+ "]" );

    }
    private static void qs(Vector v) {

        Vector vl = new Vector();                      // Left and right sides
        Vector vr = new Vector();
        HashTree el;
        float key;                                    // key for splitting

        if (v.size() < 2) return;                      // 0 or 1= sorted

        HashTree htResult = (HashTree) v.elementAt(0);
        key = new Float((String) htResult.get("SCORE1")).floatValue();

        // Start at element 1
        for (int i = 1; i < v.size(); i++) {
            el = (HashTree) v.elementAt(i);
            if (new Float((String) el.get("SCORE1")).floatValue() < key)
                vr.addElement(el); // Add to right
            else
                vl.addElement(el);                     // Else add to left
        }

        qs(vl);                                        // Recursive call left
        qs(vr);                                        //    "        "  right
        vl.addElement(v.elementAt(0));

        addVect(v, vl, vr);
    }

    // Add two vectors together, into a destination Vector
    private static void addVect(Vector dest, Vector left, Vector right) {
        int i;

        dest.removeAllElements();                     // reset destination

        for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
        for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));

    }

    public static Vector Sort(HashTree htHash) {
        Vector vNodes = new Vector();
        Enumeration e = htHash.keys();

        // Fill the vector with all the results
        while (e.hasMoreElements()) {
            String s = (String) e.nextElement();
            if (s.startsWith("NODE")) {
                HashTree htResult = (HashTree) htHash.get(s);
                vNodes.addElement(htResult);
            }
        }

        // Sort this vector  - engine sorts - no need
        //qs(vNodes);
        HashTree[] htArr = new HashTree[vNodes.size()];
        vNodes.copyInto(htArr);
        java.util.Arrays.sort(htArr, new comparehtResults());
        Vector vReturn = new Vector();
        for (int i = htArr.length - 1; i >= 0; i--) {
            vReturn.addElement(htArr[i]);
        }

        return vReturn;
    }

    private static class comparehtResults
            implements java.util.Comparator {

        public int compare(Object htResult1, Object htResult2) {
            float key1 = new Float((String) ((HashTree) htResult1).get("SCORE1")).floatValue();
            float key2 = new Float((String) ((HashTree) htResult2).get("SCORE1")).floatValue();

            if (key1 < key2)
                return -1;
            else if (key1 > key2)
                return 1;
            else
                return 0;
        }

    }

}

