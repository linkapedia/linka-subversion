package Server;

public class FrequencyDrop {
    // User attributes
    private String Name;
    private String Value;

    // constructor(s)
    public FrequencyDrop (String Name, String Value) { this.Name = Name; this.Value = Value; }

    // accessor functions
    public String getName() { return Name; }
    public String getValue() { return Value; }

    public void setName(String Name) { this.Name = Name; }
    public void setValue(String Value) { this.Value = Value; }

    public String toString() { return Name; }
    public boolean equals(FrequencyDrop fd) { if (fd.getValue().equals(Value)) { return true; } return false; }
}
