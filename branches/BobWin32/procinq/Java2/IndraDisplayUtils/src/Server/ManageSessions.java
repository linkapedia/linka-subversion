package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Logging.*;

public class ManageSessions
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Manage User Sessions");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-admin.tpl");
			Document.WriteTemplate(out, "manage-sessions.tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
