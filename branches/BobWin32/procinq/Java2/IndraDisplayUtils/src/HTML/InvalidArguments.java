package HTML;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.lang.reflect.*;

import Logging.*;

public class InvalidArguments extends Exception {
	private int iReturnCode = -1;
	private String sErrorMessage = null;

	public InvalidArguments ( int iReturnCode ) { this.iReturnCode = iReturnCode; }
	public InvalidArguments ( int iReturnCode, String sErrorMessage ) {
		this.iReturnCode = iReturnCode;
		this.sErrorMessage = sErrorMessage;
	}

	public int getReturnCode() { return iReturnCode; }	
	public String getErrorMessage()	{ return sErrorMessage; }	
}