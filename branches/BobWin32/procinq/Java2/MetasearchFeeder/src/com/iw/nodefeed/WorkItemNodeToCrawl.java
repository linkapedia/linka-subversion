/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Jan 15, 2003
/ * Time: 12:19:40 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.nodefeed;

import com.iw.threadmgr.WorkItemGeneric;

import java.util.Hashtable;

public class WorkItemNodeToCrawl extends WorkItemGeneric
{
    int iNodeID = -1;
    Hashtable htResultData = null;

    public WorkItemNodeToCrawl ( int iNodeID_ )
    {
        iNodeID = iNodeID_;
    }
    public int getNodeID ()
    {
        return iNodeID;
    }
}
