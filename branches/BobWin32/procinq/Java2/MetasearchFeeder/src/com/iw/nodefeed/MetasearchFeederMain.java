
package com.iw.nodefeed;

import com.iw.nodefeed.helper.HashTree;
import com.iw.nodefeed.helper.InvokeAPI;
import com.iw.nodefeed.helper.Machine;
import com.iw.nodefeed.helper.DBConnectionJX;
import com.iw.threadmgr.WorkQManager;
import com.indraweb.execution.Session;
//import com.iw.nodefeed.helper.Repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Date;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import api.Log;

/*
   MetasearchFeederMain.class

   This is the Main routine used to both extract information from repositories and
   process any new information that is found accordingly.

	   UserId				-	Username into the Indraweb Taxonomy Server
	   Password				-   Password into the Indraweb Taxonomy Server

   Optional input arguments:
	   Corpora				-	A comma separated list indicating the corpora to classify
							    against.   Defaults to configuration file if not provided.
	   Genre				-	Specify the genre (folder) identifier for this document set
	   Config				-   Location of configuration file containing API and thread info
       Batch			    -   Run MetasearchFeederMain in BATCH mode?  Default is false
	   DocSumm				-   Create document summaries ? Default is true
	   ErrorRun				-	Re-run all documents in the problem queue (C:\problem-documents.log)

   Main.class work flow:

    1.  Read configuration file and retrieve API locations and thread information
    2.  Log into each ITS machine and retrieve a persistent session key identifier.
	3.	Invalidate the cache files on each of the API servers
    ...

    UPDATE: 10/15/2002
    Changed FCRAWL to be multi-threaded.  Begin classifying files as they are found, and run
    multiple classifications at the same time (threads is now an optional parameter).
    UPDATE: 01/13/2003
    major mod to convert this from fcrawl to a Node refresh/crawl

??? old -config "n:/FeederProject/ConfigFeeder.txt"  -userid hkon -password racer9 -dbip jdbc:oracle:thin:@207.103.213.109:1521:gaea -dbuser sbooks -dbpass indra9
*/

public class MetasearchFeederMain
{


    public static long lStartTime = -1;
    // This is our MAIN function
    public static void main(String args[]) throws Exception
    {
        lStartTime = System.currentTimeMillis();

        System.out.println("### START FEED NODES [" + new java.util.Date() + "]");
        // extract command line arguments and put into a hashtable
        HashTree htArgs = getArgHash ( args );

        // Throw an exception if any required parameters are missing
        if (
                (!htArgs.containsKey("db")) ||
                (!htArgs.containsKey("dbuser")) ||
                (!htArgs.containsKey("dbpass")) ||
                (!htArgs.containsKey("userid")) ||
                (!htArgs.containsKey("numnodes")) ||
                (!htArgs.containsKey("IndraHome")) ||
                (!htArgs.containsKey("password"))
        )
        {
            throw new Exception("INVALID ARGUMENT LIST.  Copyright Indraweb, Inc. 1998-2003 Usage: MetasearchFeederMain -userid " +
                    "-db jdbcstring -dbuser dbuser -dbpass dbpassword -userid userid -password password  -IndraHome indrahomefolder -numnodes numnodes_0forALL");
        }




        Session.initializeSession ( args, false );
        String sWhichDB = "SVR";

        // define container/manager for threads
        // NOTE :  there is no producer in this model
        WorkQManager workQAndMgr    = new WorkQManager(50, -1, -1, -1);


        // *************************
        // PRODUCER / array get from getNextNode
        // *************************
/*
        if ( false ) // may want to let the consumer run getnextnode directly ie no Q
        {
                ThreadProducer_MSGetNextNode[] arrProdThreadNodesForCrawl =
                defineProducerThreads ( htArgs, workQAndMgr );
        }
*/

        // *************************
        // CONSUMER / call TSMetasearchNode
        // *************************
		ThreadConsumer_GetNod_tsMeta[] arrConsThreadCallApiToCrawlAndScoreNode = null;
        try
        {
            int iMaxNumNodesDesiredProcessed = Integer.parseInt( (String) htArgs.get ("numnodes") );
            arrConsThreadCallApiToCrawlAndScoreNode =
                    defineConsumerThreads(workQAndMgr, htArgs, iMaxNumNodesDesiredProcessed, sWhichDB);
        } catch (Exception e)
        {
            api.Log.LogError("error creating consumer threads", e);
        }
        int iAvailableMachinecount = 0;
        if (arrConsThreadCallApiToCrawlAndScoreNode != null)
            iAvailableMachinecount = arrConsThreadCallApiToCrawlAndScoreNode.length;
        System.out.println(new Date() + " Done defining [" + iAvailableMachinecount + "] consumer threads");

        if (iAvailableMachinecount != 0)
        {
            System.out.println(new Date() + " [" + iAvailableMachinecount + "] API MACHINES AVAILABLE TO METASEARCH WITH - STARTING.");
            // start thread container manager runing
            workQAndMgr.setAndStartThreads(
                    null,
                    //arrProdThreadNodesForCrawl,
                    arrConsThreadCallApiToCrawlAndScoreNode,
                    true);  // synch

/*
            Vector vMasterThreadSet = workQAndMgr.getMasterThreadSet();
            for ( int i = 0; i < vMasterThreadSet.size(); i++ )
            {
                Thread t = (Thread) vMasterThreadSet.elementAt(i);
                boolean b = t.isAlive();
                //System.out.println("thread [" + i + "] from master set is alive [" + b + "]");
            }
*/

            System.out.println(new Date() + " Completed process feedNodesToCrawl_main workQAndMgr.getNumJobsRemoved() [" +
                     workQAndMgr.getNumJobsRemoved() + "]");
            System.exit(0);
        } else
        {
            System.out.println(new Date() + " ERROR - NO API MACHINES AVAILABLE TO METASEARCH WITH");
            System.exit(1);
        }
    }

    // Read and parse the FCRAWL configuration file.  Each line in the configuration file
    // must contain an API server and the number of corresponding threads to weave.
    // These two values are separated by a "tab" character.
    // Example:
    //    http://localhost:8101/	12

    public static Hashtable setMSCrawlerMachines ( Connection dbc )
            throws Exception
    {
        FileInputStream fis = null;
        try
        {
//            fis = new FileInputStream(f);
//            System.out.println("From config file [" + f.getPath() + "]");
//            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
//            String sData = new String();

            Hashtable htMachines = new Hashtable();

            if ( true )
            {
                String sSQL = "select machineURL, numthreads, numurlthreads from mscrawlers where active = 1";
                Statement stmt = dbc.createStatement();
                ResultSet rs = stmt.executeQuery(sSQL);

                while ( rs.next())
                {
                    String sAPI = rs.getString(1);
                    int iNumThreadsThisMachSimulCalls = rs.getInt(2);
                    int iNumThreadsWithinMachineURLCalls = rs.getInt(3);
                    // temporarily set these to the same thing
                    Machine m = new Machine(iNumThreadsThisMachSimulCalls,
                            iNumThreadsWithinMachineURLCalls, sAPI);

                    htMachines.put(sAPI, m);

                    System.out.println("Crawler machine [" + sAPI + "] " +
                            " iNumThreadsThisMachSimulCalls : " + iNumThreadsThisMachSimulCalls +
                            " iNumThreadsWithinMachineURLCalls : " + iNumThreadsWithinMachineURLCalls
                    );
                }
                rs.close();
                stmt.close();
            }
            else // debug mode hbk control feeder machines  set
            {
                System.out.println("WARNING !!!!!!!!!!!!!!!!:  using debug machine set");
                Machine m = new Machine(1, 1, "http://66.134.131.35/itsapi");
                htMachines.put("http://66.134.131.35/itsapi", m);
            }
            return htMachines;

        } catch (Exception e)
        {
            Log.LogError("Error setting up machine definitions from the DB.", e);
            return new Hashtable();
        }
    }

    // GetArgHash takes command line arguments and parses them into a Hash structure
    public static HashTree getArgHash ( String[] args)
    {
        HashTree htHash = new HashTree();

        for (int i = 0; i < args.length; i++)
        {
            if (args[i].startsWith("-"))
            {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-")))
                {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey, sVal);
            }
        }

        return htHash;
    }


    // this is the LOGIN method used in a multi-threaded environment
    public static boolean Login ( HashTree htArgs, Hashtable htMachines )
    {


        Enumeration e4 = htArgs.keys( );
        int i = 0;
        while ( e4.hasMoreElements())
        {
            Object oKey = e4.nextElement();
            Object oVal = htArgs.get ( oKey );
            System.out.println( i + ". in login [" + oKey + "] = [" + oVal + "]"  );
            i++;
        }


        boolean bAnyLoginSuccess = false;
        boolean bLoginSuccess = false;

        Enumeration eM = htMachines.keys();

        while (eM.hasMoreElements())
        {

            Machine m = null;
            String key = (String) eM.nextElement();
            try
            {
                m = (Machine) htMachines.get(key);
                HashTree htUser = new HashTree();

                System.out.println("Logging in (" + m.GetAPI() + ")");

                htArgs.put("api", m.GetAPI());
                htArgs.put("whichdb", "SVR");  // hbk

                // Log the user into the server
                InvokeAPI API = new InvokeAPI("security.TSLogin", htArgs);

                boolean bURLSuccess = true; // hope for the best
                HashTree htResults = null;
                try {
                    /***************** CALL API **************/
                    System.out.println("executing API call " );
                    htResults = API.Execute();            // login
                    System.out.println("return from API call " );
                    /***************** CALL API **************/
                } catch ( Exception e ) {
                    bURLSuccess = false;
                    com.indraweb.util.Log.logcr( "Exception from login mach [" +  m.GetAPI()  + "] emsg [" + e.getMessage() + "]"  );
                } catch ( Throwable t ) {
                    bURLSuccess = false;
                    com.indraweb.util.Log.logcr( "throwable from login mach [" +  m.GetAPI()  + "] emsg [" + t.getMessage() + "]"  );
                }

                // If there is no "subscriber" tag, an error has occured
                if ( bURLSuccess )
                {
                    if ( !htResults.containsKey("SUBSCRIBER") )
                    {
                        bLoginSuccess = false;
                    } else {
                    // Get user hash tree
                        htUser = (HashTree) htResults.get("SUBSCRIBER");
                        if ( !htUser.containsKey("KEY"))
                        {
                            System.out.println("LOGIN FAIL [" + m.GetAPI() + "] this machine will be removed from the active queue.");
                            htMachines.remove(key);
                        } else {
                            System.out.println("Login successful [" + m.GetAPI() + "]");
                            bAnyLoginSuccess = true;
                            // Login successful.  Get the session key and put into args for future arguments
                            String SKEY = (String) htUser.get("KEY");
                            m.SetSKEY(SKEY);
                            htMachines.put(key, m);

                            // This will be the "default" machine for fetching parameters
                            htArgs.put("api", m.GetAPI()); //redundant
                            htArgs.put("SKEY", SKEY);
                        }
                    }
                } else {   // else not even a URL sucess
                    bLoginSuccess = false;
                }
            } catch (Exception e)
            {
                System.out.println("ERROR LOGGING IN TO [" + m.GetAPI() + "] " + e.getMessage());
                htMachines.remove(key);
            }
        }
        return bAnyLoginSuccess;
    }

    private static ThreadProducer_MSGetNextNode[] defineProducerThreads(
            HashTree htArgs, WorkQManager workQAndMgr) throws Exception
    {
        ThreadProducer_MSGetNextNode[] arrProdThreadNodesForCrawl = new ThreadProducer_MSGetNextNode[1];

        ThreadProducer_MSGetNextNode prodThreadNodesForCrawl = new ThreadProducer_MSGetNextNode(
                "Only P thread",
                workQAndMgr,
                (String) htArgs.get("db"),
                (String) htArgs.get("dbuser"),
                (String) htArgs.get("dbpass"));

        arrProdThreadNodesForCrawl[0] = prodThreadNodesForCrawl;
        prodThreadNodesForCrawl.setName("prodThreadNodesForCrawl only thread this pool");
        return arrProdThreadNodesForCrawl;
    }


    private static ThreadConsumer_GetNod_tsMeta[] defineConsumerThreads(WorkQManager workQAndMgr,
                                                                                HashTree htArgs,
                                                                                int iMaxNumNodesDesiredProcessed,
                                                                                String sWhichDB
                                                                                ) throws Exception
    {

        Connection dbc = DBConnectionJX.getConnection (
                (String) htArgs.get("db"),
                (String) htArgs.get("dbuser"),
                (String) htArgs.get("dbpass")
            );

        Hashtable htMachines = // machine/thread count info stored in hash table
                setMSCrawlerMachines( dbc );
        dbc = DBConnectionJX.freeConnection ( dbc, (String) htArgs.get("db") ) ;

        // Step 2: Attempt to log in, exit on failure
        if (!Login(htArgs, htMachines))
        {
            throw new Exception("No crawler machines with successful login, no consumers, quitting...");
        }

        Enumeration eMachines2 = htMachines.elements();
        Vector vAllConsumerThreads = new Vector();

        int iMachIndex = 0;
        while (eMachines2.hasMoreElements())
        {
            Machine mach = (Machine) eMachines2.nextElement();
            System.out.println("creating this many threads [" + mach.GetNumberOfThreadsSimulMachineCalls() +
                    "] for this mach [" + mach.GetAPI() + "]");
            for (int iThreadIdx = 0; iThreadIdx < mach.GetNumberOfThreadsSimulMachineCalls(); iThreadIdx++)
            {
                String sMachThreadDesc = "M [" + mach.GetAPI().substring(0, 27) + "] " +
                        " TI [" + iThreadIdx + "] MI [" + iMachIndex + "]";
				ThreadConsumer_GetNod_tsMeta consThreadCallApiToCrawlAndScoreNode =
                        new ThreadConsumer_GetNod_tsMeta(
                                sMachThreadDesc,
                                workQAndMgr,
                                (String) htArgs.get("db"), // for getNextNode calls
                                (String) htArgs.get("dbuser"),
                                (String) htArgs.get("dbpass"),
                                mach,
                                iThreadIdx,
                                iMaxNumNodesDesiredProcessed
                        );

                mach.vThreads.addElement(consThreadCallApiToCrawlAndScoreNode);

                vAllConsumerThreads.addElement(consThreadCallApiToCrawlAndScoreNode);
                System.out.println("Node consumer thread initialized : " + sMachThreadDesc);
                consThreadCallApiToCrawlAndScoreNode.setName("tCallMSApi " + sMachThreadDesc);
            }
            iMachIndex++;
        }

        System.out.println("defined [" + iMachIndex + "] machines with max iMaxNumNodesDesiredProcessed [" + iMaxNumNodesDesiredProcessed + "]"
        );


		ThreadConsumer_GetNod_tsMeta[] arrConsThreadCallApiToCrawlAndScoreNode =
                new ThreadConsumer_GetNod_tsMeta[vAllConsumerThreads.size()];
        vAllConsumerThreads.copyInto(arrConsThreadCallApiToCrawlAndScoreNode);
        return arrConsThreadCallApiToCrawlAndScoreNode;
    }

}
/* test connections
        System.out.println("### TEST [" + new java.util.Date() + "]");
        System.out.println("### TEST [" + new java.util.Date() + "]");
        System.out.println("### TEST [" + new java.util.Date() + "]");
        System.out.println("### TEST [" + new java.util.Date() + "]");

        com.javaexchange.dbConnectionBroker.DbConnectionBroker dbcbToUse = new com.javaexchange.dbConnectionBroker.DbConnectionBroker (
                    "oracle.jdbc.driver.OracleDriver",
                    "jdbc:oracle:thin:@207.103.213.109:1521:gaea",
                    "sbooks",
                    "indra9",
                    1,   // min conns
                    4,  // max cnns
                    "c:/temp/db.log",
                    1 //maxConnTime: Time in days between connection resets. (Reset does a basic cleanup)
                );

        Connection dbc1 = dbcbToUse.getConnection() ;
        Connection dbc2 = dbcbToUse.getConnection() ;
        Connection dbc3 = dbcbToUse.getConnection() ;
        Connection dbc4 = dbcbToUse.getConnection() ;
        Connection dbc5 = dbcbToUse.getConnection() ;
        Connection dbc6 = dbcbToUse.getConnection() ;
        Connection dbc7 = dbcbToUse.getConnection() ;

        System.out.println("### TEST [" + new java.util.Date() + "]");
        System.out.println("### TEST [" + new java.util.Date() + "]");
        System.out.println("### TEST [" + new java.util.Date() + "]");
        System.out.println("### TEST [" + new java.util.Date() + "]");
        System.exit(1);
*/


