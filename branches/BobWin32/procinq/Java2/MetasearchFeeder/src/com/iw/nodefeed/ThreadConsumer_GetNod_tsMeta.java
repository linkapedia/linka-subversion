
/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Jan 14, 2003
 * Time: 4:08:55 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.nodefeed;

import api.Log;
import com.indraweb.util.UtilProfiling;
import com.iw.nodefeed.helper.HashTree;
import com.iw.nodefeed.helper.Machine;
import com.iw.threadmgr.ThreadGenericProdCons;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class ThreadConsumer_GetNod_tsMeta extends ThreadGenericProdCons {

    private Machine mach = null;
    private int iThreadIdx = -1;
    private String sMachThreadInfo = null;
    private int iMaxNumNodesDesiredProcessed = -1;
    static api.util.SynchronizedCounter synchCountSuccess = new api.util.SynchronizedCounter();
    static api.util.SynchronizedCounter synchCountFail = new api.util.SynchronizedCounter();
    static api.util.SynchronizedCounter synchCountWorkItemsFetched = new api.util.SynchronizedCounter();
    static api.util.SynchronizedCounter synchCountWorkItemsCompletedGoodOrBad = new api.util.SynchronizedCounter();
    static int lMaxTimeOutAtCrawler = 0;
    static final Hashtable htPendingNodeIDsIntegerToLongTimeSentOut = new Hashtable();

    public ThreadConsumer_GetNod_tsMeta(String sMachThreadInfo_,
            com.iw.threadmgr.WorkQManager workQ_,
            String sDB_OracleJDBC_,
            String sDB_user_,
            String sDB_pass_,
            Machine mach_,
            int iThreadIdx_,
            int iMaxNumNodesDesiredProcessed_) {
        super(sMachThreadInfo_, workQ_, sDB_OracleJDBC_, sDB_user_, sDB_pass_);
        mach = mach_;
        iThreadIdx = iThreadIdx_;
        sMachThreadInfo = sMachThreadInfo_;
        this.iMaxNumNodesDesiredProcessed = iMaxNumNodesDesiredProcessed_;

    }
    private static final Object oSynchKickoffCounter = new Object();
    private static int iKickoffCounter = 0;
    private Hashtable htWhichMachineWhichNode = new Hashtable();
    private static int iNumNodesRunCounterAcrossAllThreads = 0;
    private static final Object oSynch_iNumNodesRunCounterAcrossAllThreads = new Object();

    @Override
    public void run() {

        int iNodeIDCurrent = -1;
        Connection dbc = null;
        try {
            int iLoop = 0;
            while (true) {
                if (bStopLooping) {
                    break;
                }
                if (com.indraweb.util.UtilFile.bFileExists("/temp/IndraControlStopMetasearchFeeder.txt")) {
                    System.out.println("file present - stop loop in ThreadConsumer_GetNode_CallAPItsMeta /temp/IndraControlStopMetasearchFeeder.txt");
                    break;
                }
                WorkItemNodeToCrawl workItem = null;
                try {
                    dbc = com.iw.nodefeed.helper.DBConnectionJX.getConnection(sDB_OracleJDBC, sDB_user, sDB_pass);
                    try {
                        // ************************************
                        // GET WORK ITEM
                        // ************************************
                        iNodeIDCurrent = -1;
                        workItem = createWorkItem(sMachThreadInfo, sDesc, dbc);
                        if (workItem == null) {
                            System.out.println("C. [" + sDesc + "] workItem is null - producers should be done ");
                            break;  // should mean only that all producers are done
                        }
                        synchCountWorkItemsFetched.incrVal(1);
                        // ************************************
                        // NOW DELETE OUT OLD DB MONITOR CONTENT BEFORE RUNNING
                        // ************************************
                        deleteOldDBContent(workItem, dbc);
                    } finally {
                        // ************************************
                        // FREE DB CONN WHILE WE WAIT FOR API TO COMPLETE
                        // ************************************
                        dbc = com.iw.nodefeed.helper.DBConnectionJX.freeConnection(dbc, sDB_OracleJDBC);
                    }

                    // ************************************
                    // RUN API CALL TO MS AND SCORE
                    // ************************************
                    Hashtable htArgs = new Hashtable();
                    iNodeIDCurrent = workItem.getNodeID();
                    int iThreadCountPerAPICall_Remote = 1;
                    htArgs.put("nodeid", "" + iNodeIDCurrent);
                    htArgs.put("whichdb", "SVR");
                    //htArgs.put("NUMTHREAD", ""+mach.GetNumberOfThreadsSimulMachineCalls() );  // hbk numthreads hbk  9 10 15
                    htArgs.put("MACHTHREADINFO", sMachThreadInfo);  // hbk numthreads hbk  9 10 15
                    htArgs.put("NumThreadsExtURLCalls", "" + mach.GetNumThreadsWithinMachineURLCalls());  // hbk numthreads hbk  9 10 15

                    htArgs.put("post", "true");
                    //System.out.println ( "C thrd got workItem [" + Thread.currentThread().getName() + "]") ;

                    com.iw.nodefeed.helper.InvokeAPI API = null;
                    API = new com.iw.nodefeed.helper.InvokeAPI("tsmetasearchnode.TSMetasearchNode", htArgs, mach.GetAPI());   // hbk control 2003 03 05
                    //API = new com.iw.nodefeed.helper.InvokeAPI("tsother.TSTestWait", htArgs, mach.GetAPI());   // hbk control 2003 03 05
                    com.iw.nodefeed.helper.HashTree htResults = null;

                    synchronized (htPendingNodeIDsIntegerToLongTimeSentOut) {
                        Long LTimeStart = (Long) htPendingNodeIDsIntegerToLongTimeSentOut.get(new Integer(iNodeIDCurrent));
                        if (LTimeStart != null) {
                            System.out.println("why processing the same node twice [" + iNodeIDCurrent
                                    + "] other one started ms ago [" + (UtilProfiling.elapsedTimeMillis(LTimeStart.longValue()) + "]"));
                        }
                        htPendingNodeIDsIntegerToLongTimeSentOut.put(new Integer(iNodeIDCurrent),
                                new Long(System.currentTimeMillis()));
                        htWhichMachineWhichNode.put(new Integer(iNodeIDCurrent),
                                mach.GetAPI());
                    }
                    int iLocalKickoffCtr = -1;
                    synchronized (oSynchKickoffCounter) {
                        iKickoffCounter++;
                        iLocalKickoffCtr = iKickoffCounter;
                        int iMaxTimeNodeBeenOutAcrossThreads = 0;
                        int iMaxTimeNodeID = -1;
                        Enumeration e = htPendingNodeIDsIntegerToLongTimeSentOut.keys();
                        while (e.hasMoreElements()) {
                            Integer IKeyNodeID = (Integer) e.nextElement();
                            Long LTimeOut = (Long) htPendingNodeIDsIntegerToLongTimeSentOut.get(IKeyNodeID);
                            long lOutFor = System.currentTimeMillis() - LTimeOut.longValue();
                            if (lOutFor > iMaxTimeNodeBeenOutAcrossThreads) {
                                iMaxTimeNodeID = IKeyNodeID.intValue();
                                iMaxTimeNodeBeenOutAcrossThreads = (int) lOutFor;
                            }

                        }
                        System.out.println("\r\n**** KickOff #" + iKickoffCounter
                                + " time  [" + new java.util.Date() + "] "
                                + " N [" + iNodeIDCurrent + "] "
                                + "Thrd [" + iThreadCountPerAPICall_Remote + "] "
                                + "M [" + sMachThreadInfo + "] "
                                + "maxtimeout ms [" + iMaxTimeNodeBeenOutAcrossThreads + "] "
                                + "in node [" + iMaxTimeNodeID + "] "
                                + " maxmach [" + htWhichMachineWhichNode.get(new Integer(iMaxTimeNodeID)) + "]"
                                + " HS ["
                                + this.htToStringPrintLongAgeMSPerKey(htPendingNodeIDsIntegerToLongTimeSentOut, ";")
                                + "]");
                    }


                    boolean bError = false;
                    long lTimeStartAPICall = System.currentTimeMillis();
                    try {
                        // ************************************
                        // API EXECUTE
                        // ************************************
                        htResults = API.Execute();
                    } catch (Throwable e) {
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! " + new java.util.Date() + " caught throw from api execute N [" + iNodeIDCurrent + "] errmsg [" + e.getMessage() + "]"
                                + " stack [" + com.indraweb.util.Log.stackTraceToString(e) + "]");
                        com.indraweb.util.Log.NonFatalError(" !!!!!!!!!!!!!!!!!!! " + new java.util.Date() + " caught throw from api execute N [" + iNodeIDCurrent + "]", e);
                        bError = true;
                        throw e;
                    } finally {
                        htWhichMachineWhichNode.remove(new Integer(iNodeIDCurrent));
                        String sAPIThrowErrIndicator = "no NON XML err ";
                        String sErrHilite = "";
                        if (bError) {
                            sAPIThrowErrIndicator = "ERR!";
                            sErrHilite = " !!!!!!!!!!!!!!!!!!!!!!!! ";
                        }
                        String sRtnInfo = "--> Raw return " + sErrHilite + "[" + iLocalKickoffCtr + " N [" + iNodeIDCurrent
                                + "] API err [" + sAPIThrowErrIndicator
                                + "] ms [" + (System.currentTimeMillis() - lTimeStartAPICall)
                                + "] M [" + sMachThreadInfo
                                + "] HS [" + com.indraweb.utils.sets.UtilSets.htToStringPrintLongAgeMSPerKey(htPendingNodeIDsIntegerToLongTimeSentOut, ";") + "]"
                                + "[" + new java.util.Date() + "] ";
                        // RAW RETURN INDICATOR - CAN'T GET PAST HERE w/o going thru                        if ( bverb
                        //System.out.println(sRtnInfo);
                        synchronized (htPendingNodeIDsIntegerToLongTimeSentOut) {
                            htPendingNodeIDsIntegerToLongTimeSentOut.remove(new Integer(iNodeIDCurrent));
                        }

                    }

                    // ************************************
                    // NOW CHECK API CALL RESULTS
                    // ************************************
                    HashTree htRes_TSMETASEARCHNODE = (HashTree) htResults.get("TSMETASEARCHNODE");

                    if (htRes_TSMETASEARCHNODE == null) {
                        System.out.println("Error as per MS xml return : htRes_TSMETASEARCHNODE == null sMachThreadInfo [" + sMachThreadInfo + "]");
                        PrintWriter pw = new PrintWriter(System.out);
                        com.indraweb.utils.sets.UtilSets.XMLout(htResults, pw, 0);
                        pw.flush();
                        pw.close();
                        com.indraweb.util.clsUtils.pause_this_many_milliseconds(2000);
                        throw new Exception("fail " + sMachThreadInfo + " htRes_TSMETASEARCHNODE == null, gross fail this TSMETASEARCHNODE call, continuing");

                    }

                    Object o_DOCSOPENEDOK = htRes_TSMETASEARCHNODE.get("DOCSOPENEDOK");

                    if (o_DOCSOPENEDOK == null) {
                        throw new Exception("htRes_DOCSOPENEDOK == null, gross fail this TSMETASEARCHNODE call, continuing");
                    }
                    int iNum_DOCSOPENEDOK = Integer.parseInt(o_DOCSOPENEDOK.toString());

                    Object o_DOCSOPENEDNOTOK = htRes_TSMETASEARCHNODE.get("DOCSOPENEDNOTOK");
                    if (o_DOCSOPENEDNOTOK == null) {
                        throw new Exception("htRes_DOCSOPENEDOK == null, gross fail this TSMETASEARCHNODE call, continuing");
                    }
                    int iNum_DOCSOPENEDNOTOK = Integer.parseInt(o_DOCSOPENEDNOTOK.toString());

                    String sResult =
                            " DOCSOPENEDNOTOK [" + iNum_DOCSOPENEDNOTOK
                            + "] T [" + iThreadIdx + "]";

                    String sCOMPLETECODE = null;


                    synchronized (oSynchKickoffCounter) {
                        double dRate = com.indraweb.util.UtilProfiling.rateSincePerMinute(
                                MetasearchFeederMain.lStartTime, synchCountWorkItemsCompletedGoodOrBad.getVal());
                        synchCountWorkItemsCompletedGoodOrBad.incrVal(1);
                        if (iNum_DOCSOPENEDOK > 0) {
                            synchCountSuccess.incrVal(1);
                            System.out.println("\r\n$$$$$ NODE DONE OK # " + iLocalKickoffCtr
                                    + " [" + new java.util.Date() + "] "
                                    + " N [" + iNodeIDCurrent
                                    + "] SUCC [" + synchCountSuccess.getVal()
                                    + "] #items [" + synchCountWorkItemsFetched.getVal()
                                    + "] #done at all [" + synchCountWorkItemsCompletedGoodOrBad.getVal()
                                    + "] #done at all rate [" + dRate
                                    + "] #fails [" + synchCountFail.getVal()
                                    + "] DOCSOK [" + iNum_DOCSOPENEDOK
                                    + "] M [" + sMachThreadInfo + "] " + sResult);
                            sCOMPLETECODE = "0";
                        } else {
                            synchCountFail.incrVal(1);
                            System.out.println("\r\n!!!!! NODE FAIL # "
                                    + " [" + new java.util.Date() + "] "
                                    + " N [" + iNodeIDCurrent
                                    + "] SUCC [" + synchCountSuccess.getVal()
                                    + "] #items [" + synchCountWorkItemsFetched.getVal()
                                    + "] #done at all [" + synchCountWorkItemsCompletedGoodOrBad.getVal()
                                    + "] #done at all rate [" + dRate
                                    + "] #fails [" + synchCountFail.getVal()
                                    + "] DOCSOK [" + iNum_DOCSOPENEDOK
                                    + "] M [" + sMachThreadInfo + "] " + sResult + "\r\n");
                            sCOMPLETECODE = "1";
                        }
                    }

                    //else // CALL IT A FAIL IF NO DOCS OPENED
                    // doing DB insert here :
                    // ************************************
                    // NOW DO API CALL RESULT INSERTS
                    // ************************************
                    Vector vStringsCols = new Vector();
                    vStringsCols.addElement("NODEID");
                    vStringsCols.addElement("CRAWLER");
                    vStringsCols.addElement("COMPLETECODE");
                    vStringsCols.addElement("DOCSOPENEDOK");
                    vStringsCols.addElement("DATEOFSEARCH");
                    vStringsCols.addElement("THREADNAME");

                    Vector vStringsVals = new Vector();
                    vStringsVals.addElement("" + workItem.getNodeID());
                    String sAPIDB = mach.GetAPI();
                    sAPIDB = sAPIDB.substring(0, 24);
                    vStringsVals.addElement("'" + sAPIDB + "'");
                    vStringsVals.addElement(sCOMPLETECODE);
                    vStringsVals.addElement("" + iNum_DOCSOPENEDOK);
                    vStringsVals.addElement("SYSDATE");
                    vStringsVals.addElement("'" + Thread.currentThread().getName() + "'");

                    dbc = com.iw.nodefeed.helper.DBConnectionJX.getConnection(
                            sDB_OracleJDBC,
                            sDB_user,
                            sDB_pass);

                    String sSQLInsertRecord = com.indraweb.database.SQLGenerators.genSQLInsert("MSRESULTNODE", vStringsCols, vStringsVals);
                    //System.out.println("sSQLInsertRecord [" + sSQLInsertRecord + "]" );
                    com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLInsertRecord, dbc, true, -1);


                    // ************************************
                    // NOW CLOSENODE
                    // ************************************
                    //OK looks like all is done and went well ... let's close the node
                    CallableStatement cstmtClose = dbc.prepareCall("begin CloseNode (:1,:2); end;");
                    cstmtClose.setLong(1, iNodeIDCurrent);
                    cstmtClose.setString(2, sMachThreadInfo);
                    long lTimeStart = System.currentTimeMillis();
                    cstmtClose.executeUpdate();
                    //System.out.println("time in ExecuteCloseNode (sec) [" + UtilProfiling.elapsedTimeSeconds ( lTimeStart ) + "]");
                    cstmtClose.close();
                    cstmtClose = null;
                } catch (Throwable e) {
                    synchCountFail.incrVal(1);
                    Log.LogError("err in node [" + iNodeIDCurrent + "] M [" + sMachThreadInfo + "] com.iw.nodefeed.ThreadConsumer_GetNode_CallAPItsMeta run() loop continuing ", e);
                } finally {
                    synchronized (oSynch_iNumNodesRunCounterAcrossAllThreads) {
                        iNumNodesRunCounterAcrossAllThreads++; // may have same value as synchCountSuccess
                        if (iMaxNumNodesDesiredProcessed > 0
                                && iNumNodesRunCounterAcrossAllThreads > iMaxNumNodesDesiredProcessed) {
                            System.out.println("stop looping as iNumNodesRunCounterAcrossAllThreads [" + iNumNodesRunCounterAcrossAllThreads + "] processed max is [" + iMaxNumNodesDesiredProcessed + "]");
                            bStopLooping = true;
                        }
                        // this max counter may not be bulletproof - race conditions could set it # threads error ?
                    }
                    com.iw.nodefeed.helper.DBConnectionJX.freeConnection(dbc, sDB_OracleJDBC);
                }
                dbc = null;
            } // while
        }// try
        catch (Throwable e) {
            System.out.println("Fail on node [" + iNodeIDCurrent + "] continuing to next node : " + e.getMessage());
        }
    }
    private static final Object oSynchGetNextNode = new Object();

    private static WorkItemNodeToCrawl createWorkItem(String sMachThreadInfo,
            String sDesc,
            Connection dbc) throws Exception {
        WorkItemNodeToCrawl workItem = null;
        if (false)  // TEST STYLE GETNEXTNODE
            ; // workItem = (WorkItemNodeToCrawl) workQ.getWorkItem();
        else {
            int iNodeID = -1;
//            if ( true )   // hbk control
            synchronized (oSynchGetNextNode) {
                if (true) // hbk control feeder which nodeid to run
                {
                    iNodeID = ThreadProducer_MSGetNextNode.getNextNode(sDesc, dbc);
                } else {
                    iNodeID = 1129139;
                }
            }
//            else { // test mode - generate sequential nodeid values not real nodeid's
//                synchcountNodeID.incrVal ( 1 );
//                iNodeID = synchcountNodeID.getVal()+1;
//            }


            if (iNodeID == 0) // no work
            {
                System.out.println("mach [" + sMachThreadInfo + "] C. no work in the Node Crawl Queue returning null workitem");
                //com.indraweb.util.clsUtils.pause_this_many_milliseconds(1000 * 30);
            } else if (iNodeID == -1) // error getting work
            {
                System.out.println("mach [" + sMachThreadInfo + "] C. error getting Node Crawl Queue returning null workitem");
                //com.indraweb.util.clsUtils.pause_this_many_milliseconds(1000 * 30);
            } else {
                workItem =
                        //new WorkItemNodeToCrawl(getNextNode());  // hbk control uncomment for prod 2003 02 12
                        //new WorkItemNodeToCrawl( getNextNode_test() );
                        new WorkItemNodeToCrawl(iNodeID);
                // System.out.println("C. got my own work item [" + sDesc + "] node [" + workItem.getNodeID() + "]");
            }

        }
        return workItem;
    }

    private void deleteOldDBContent(
            WorkItemNodeToCrawl workItem, Connection dbc) throws Exception {
        String sSQLDeletePreNode = "delete from MSResultNode where nodeid = " + workItem.getNodeID();
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(
                sSQLDeletePreNode, dbc, true, -1);

        String sSQLDeletePreDoc = "delete from MSResultDocs where nodeid = " + workItem.getNodeID();
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(
                sSQLDeletePreDoc, dbc, true, -1);

        String sSQLDeletePreScores = "delete from MSResultScores where nodeid = " + workItem.getNodeID();
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(
                sSQLDeletePreScores, dbc, true, -1);

    }

    //private class dataRunStatratPerNodeID
    private String htToStringPrintLongAgeMSPerKey(Hashtable ht, String sDelim) {
        Enumeration e = ht.keys();
        StringBuilder sb = new StringBuilder();
        while (e.hasMoreElements()) {
            Object oKey = e.nextElement();
            Long LVal = (Long) ht.get(oKey);

            sb.append("N[").append(oKey).append("] ms [").append(System.currentTimeMillis() - LVal.longValue()).append("]").append(sDelim);
        }
        return sb.toString();
    }
} // class

