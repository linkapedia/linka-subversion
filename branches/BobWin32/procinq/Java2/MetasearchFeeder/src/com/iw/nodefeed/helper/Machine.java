package com.iw.nodefeed.helper;

import java.io.*;
import java.util.*;

// Generic Document object (without node relationship)

public class Machine
{
    // private variables
    private int NumThreadsSimulMachineCalls;
    private int NumThreadsWithinMachineURLCalls;
    private String API;
    private String SKEY;

    // classification threads are publically accessible
    public Vector vThreads = new Vector();

    // Accessor methods to private variables
    public int GetNumberOfThreadsSimulMachineCalls()
    {
        return NumThreadsSimulMachineCalls;
    }

    public int GetNumThreadsWithinMachineURLCalls()
    {
        return NumThreadsWithinMachineURLCalls;
    }

    public String GetAPI()
    {
        return API + "/ts?fn=";
    }

    public String GetSKEY()
    {
        return SKEY;
    }

    // Set methods for private variables
    public int SetNumberOfThreadsSimulMachineCalls(int iThreads)
    {
        NumThreadsSimulMachineCalls = iThreads;
        return NumThreadsSimulMachineCalls;
    }

    public String SetAPI(String sAPI)
    {
        API = sAPI;
        return API;
    }

    public String SetSKEY(String sKey)
    {
        SKEY = sKey;
        return SKEY;
    }

    // Object constructor(s)
    public Machine(int iNumThreads, int iNumThreadsWithinMachineURLCalls, String sAPI)
    {
        this.NumThreadsSimulMachineCalls = iNumThreads;
        this.NumThreadsWithinMachineURLCalls = iNumThreadsWithinMachineURLCalls;
        this.API = sAPI;
        this.API = sAPI;
    }

    public Machine(int iNumThreads, int iNumThreadsWithinMachineURLCalls, String sAPI, String sKey)
    {
        this.NumThreadsSimulMachineCalls = iNumThreads;
        this.NumThreadsWithinMachineURLCalls = iNumThreadsWithinMachineURLCalls;
        this.API = sAPI;
        this.SKEY = sKey;
    }

    public Machine()
    {
    }
}
