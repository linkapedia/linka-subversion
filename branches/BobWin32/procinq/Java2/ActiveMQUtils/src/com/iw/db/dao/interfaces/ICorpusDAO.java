package com.iw.db.dao.interfaces;

import com.iw.db.beans.Terms;
import java.util.List;

/**
 *
 * @author Alex
 */
public interface ICorpusDAO {

    public String getAffinityTermsByCorpusID(Integer corpusID) throws Exception;

    public List<Terms> getAllEnabledAffinityTerms() throws Exception;
}
