package com.iw.rest.resources;

import com.iw.activemq.beans.ConfigurationPackage;
import com.iw.activemq.procinq.TopicProducer;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.iw.application.context.AppServletContextListener;
import com.iw.rest.beans.ChangeStatusRequest;
import com.iw.rest.beans.Classifier;
import com.iw.rest.beans.Task;
import java.util.StringTokenizer;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import javax.xml.bind.JAXBElement;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

// Will map the resource to the URL classifiers
@Path("/classifiers")
public class ClassifiersResource {

    private static transient final Logger log = Logger.getLogger(ClassifiersResource.class);
    // Allows to insert contextual objects into the class,
    // e.g. ServletContext, Request, Response, UriInfo
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    // Return the list of classifiers for applications
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getClassifiersAsXML() {
        log.debug("ClassifiersResource::getClassifiersAsXML()");
        Document classifiersXMLDoc = null;
        String strListClassifiers = null;
        try {
            List<Classifier> classifiers = new ArrayList<Classifier>();
            classifiers.addAll(AppServletContextListener.getClassifierList().values());
            classifiersXMLDoc = DocumentHelper.createDocument();
            Element root = classifiersXMLDoc.addElement("classifiers");
            for (Classifier classifier : classifiers) {
                root.add(classifier.getClassifierASXMLElement());
            }
            strListClassifiers = classifiersXMLDoc.asXML();
        } catch (Exception e) {
            log.error("Error in ClassifiersResource::getClassifiersAsXML()", e);
        }
        return strListClassifiers;
    }

    // returns the number of classifiers
    // Use http://localhost:8080/itsapi/rest/classifiers/count
    // to get the total number of records
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String getClassifiersCount() {
        log.debug("ClassifiersResource::getClassifiersCount()");
        int classifiersCount = 0;
        if (AppServletContextListener.getClassifierList() != null) {
            classifiersCount = AppServletContextListener.getClassifierList().size();
        }
        return String.valueOf(classifiersCount);
    }

    // Defines that the next path parameter after classifiers is
    // treated as a parameter and passed to the ClassifierResources
    // Allows to type http://localhost:8080/itsapi/rest/classifiers/1
    // 1 will be treaded as parameter classifier and passed to ClassifierResource
    @Path("{classifierId}")
    public ClassifierResource getClassifier(@PathParam("classifierId") String classifierId) {
        log.debug("ClassifiersResource::getClassifier()");
        return new ClassifierResource(uriInfo, request, classifierId);
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("refresh")
    public String refreshClassifiers() {
        log.debug("ClassifiersResource::refreshClassifiers()");
        List<Classifier> classifiers = null;
        String strListClassifiers = null;
        if (AppServletContextListener.getClassifierList() != null) {
            ConfigurationPackage cfgPkg = new ConfigurationPackage("refresh-classifiers-list");
            try {
                TopicProducer.sendMessage(cfgPkg);
                if (AppServletContextListener.getClassifierList() != null) {
                    classifiers = new ArrayList<Classifier>();
                    classifiers.addAll(AppServletContextListener.getClassifierList().values());
                    Document classifiersXMLDoc = DocumentHelper.createDocument();
                    Element root = classifiersXMLDoc.addElement("classifiers");
                    for (Classifier classifier : classifiers) {
                        root.add(classifier.getClassifierASXMLElement());
                    }
                    strListClassifiers = classifiersXMLDoc.asXML();
                }
            } catch (Exception e) {
                log.error("Error in ClassifiersResource::refreshClassifiers()", e);
            }
        }
        return strListClassifiers;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("start")
    public String startClassifiers(@QueryParam("classifiers") String classifiers) {
        log.debug("startClassifiers(String)");
        ConfigurationPackage confPkg = null;
        confPkg = new ConfigurationPackage("start");
        try {
            if (!classifiers.equals("ALL")) {
                StringTokenizer stringTokenizer = new StringTokenizer(classifiers, ",");
                String ipAddress = null;
                while (stringTokenizer.hasMoreTokens()) {
                    ipAddress = stringTokenizer.nextToken();
                    confPkg.setDestination(ipAddress);
                    TopicProducer.sendMessage(confPkg);
                }
            }
        } catch (Exception e) {
            log.error("Error starting the classifiers", e);
        }
        return "Starting: " + classifiers;
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String changeClassifiersState(JAXBElement<ChangeStatusRequest> request) {
        log.debug("changeClassifiersState()");
        ChangeStatusRequest changeRequest = request.getValue();
        ConfigurationPackage confPkg = null;
        if (changeRequest != null) {
            try {
                //Validate the information received from the client are currently running servers.
                Task[] tasks = changeRequest.getTask();
                for (Task task : tasks) {
                    if (task != null && task.getAction() != null && !"".equals(task.getAction().trim())) {
                        if ("start".equals(task.getAction())) {
                            //Start the classifiers.
                            if (task.getIpAddress() != null && task.getIpAddress().length > 0) {
                                //See if we got a request for all the classifiers.
                                if ("ALL".equals(task.getIpAddress()[0])) {
                                    log.info("Going to start all the classifiers.");
                                    confPkg = new ConfigurationPackage("start");
                                    TopicProducer.sendMessage(confPkg);
                                } else {
                                    //We have to start each classifier separated.
                                    String[] ipAddresses = task.getIpAddress();
                                    log.info("Going to start "+ipAddresses.length+" classifiers.");
                                    for (String ip : ipAddresses) {
                                        confPkg = new ConfigurationPackage("start");
                                        confPkg.setDestination(ip);
                                        TopicProducer.sendMessage(confPkg);
                                    }
                                }
                            }
                        } else if ("stop".equals(task.getAction())) {
                            //Stop the classifiers.
                            if (task.getIpAddress() != null && task.getIpAddress().length > 0) {
                                //See if we got a request for all the classifiers.
                                if ("ALL".equals(task.getIpAddress()[0])) {
                                    log.warn("Going to stop all the classifiers.");
                                    confPkg = new ConfigurationPackage("stop");
                                    TopicProducer.sendMessage(confPkg);
                                } else {
                                    //We have to start each classifier separated.
                                    String[] ipAddresses = task.getIpAddress();
                                    log.warn("Going to stop "+ipAddresses.length+" classifiers.");
                                    for (String ip : ipAddresses) {
                                        confPkg = new ConfigurationPackage("stop");
                                        confPkg.setDestination(ip);
                                        TopicProducer.sendMessage(confPkg);
                                    }
                                }
                            }
                        } else {
                            return "<operation status='failed' reason='The request format is not valid. Cannot found valid task to execute.'>"
                                    + "<task received='"+task.getAction()+"' status='invalid'/></operation>";
                        }
                    } else {
                        return "<operation status='failed' reason='The request format is not valid. Cannot found task to execute.' />";
                    }
                }
            } catch (JMSException e) {
                log.error("An error ocurred in changeClassifiersState().", e);
                return "<operation status='failed' reason='Exception found'>"
                        + "<exception type='JMSException'>"+e+"</exception></operation>";
            } catch (NamingException e) {
                log.error("An error ocurred in changeClassifiersState().", e);
                return "<operation status='failed' reason='Exception found'>"
                        + "<exception type='NamingException'>"+e+"</exception></operation>";
            }
        } else {
            return "<operation status='failed' reason='The request format is not valid.' />";
        }
        return "<operation status='successful' />";
    }
}
