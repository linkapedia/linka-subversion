package com.iw.activemq.beans;

import com.yuxipacific.documents.beans.DocumentDescriptor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class wraps the information required to do a classification 
 * in the classifier through the ActiveMQ.
 * @author Alexander Cano
 * @since 29/07/2011
 * @version 1.0
 */
public class ClassificationPackage implements Serializable {

    private static final long serialVersionUID = -6494708859339463042L;
    private List<String> corpusIDList;
    private DocumentDescriptor documentDescriptor;

    /**
     * Method to get the List of CorpusID inside the package.
     * @return ArrayList of CorpusID inside the package
     * @see java.util.List
     */
    public List<String> getCorpusIDList() {
        return corpusIDList;
    }

    /**
     * Method to set the List of CorpusID into the package.
     * @param corpusIDList {@code List}
     */
    public void setCorpusIDList(List<String> corpusIDList) {
        this.corpusIDList = corpusIDList;
    }

    /**
     * Method to get the URI of the resource.
     * @return DocumentDescriptor representing the URI.
     */
    public DocumentDescriptor getDocumentDescriptor() {
        return documentDescriptor;
    }

    /**
     * Method to set the URI of the resource.
     * @param documentDescriptor String representing the URI.
     */
    public void setDocumentDescriptor(DocumentDescriptor documentDescriptor) {
        this.documentDescriptor = documentDescriptor;
    }

    public void setCorpusID(String corpusID) {
        if (corpusIDList != null) {
            corpusIDList.add(corpusID);
        } else {
            corpusIDList = new ArrayList<String>();
            corpusIDList.add(corpusID);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CorpusID: ").append(getCorpusIDList()).append(" / ");
        sb.append("Resource: ").append(getDocumentDescriptor());
        return sb.toString();
    }
}
