package com.iw.activemq.beans;

import java.io.Serializable;

/**
 * This class wraps the information required to make changes to the state of the
 * classifier using the ActiveMQ Topic
 * {@code ProcinQ.Classifiers.ConfigurationTopic}
 * 
 * @author Alexander Cano
 * @since 12/08/2011
 * @version 1.0
 */
public class ConfigurationPackage implements Serializable {
	private static final long serialVersionUID = 8509329339410812196L;

	// Constants
	private static final String DEFAULT_DESTINATION = "ALL";
	private String task;
	private String destination;

	public ConfigurationPackage() {
		this.task = "NONE";
		this.destination = ConfigurationPackage.DEFAULT_DESTINATION;
	}

	public ConfigurationPackage(String task) {
		this.task = task;
		this.destination = ConfigurationPackage.DEFAULT_DESTINATION;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
}
