package com.iw.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.textmining.text.extraction.WordExtractor;

/**
 *
 * @author Alex
 */
class MSWordUtils {

    private static final Logger log = Logger.getLogger(MSWordUtils.class);

    /**
     * Method to get the title from a Microsoft Word Document.
     * @param wordFile Path to the Microsoft Word Document.
     * @return DocTitle of the WordFile
     * FIXME: Find a way to get the doc title, in this moment is getting the fileName.
     */
    public static String getDocTitleFromWord(File wordFile) {
        log.debug("getDocTitleFromWord(String)");
        String docTitle = null;
        String filePath;
        try {
            filePath = wordFile.getAbsolutePath();
            if (filePath != null && filePath.lastIndexOf("/") > 0) {
                docTitle = filePath.substring(filePath.lastIndexOf(File.separator), filePath.length());
            }
        } catch (Exception e) {
            log.error("An exception occured in parsing the Word Document.", e);
        }
        return docTitle;
    }

    /**
     * 
     * @param filePath
     * @return 
     */
    public static String convertWordToHTML(String filePath) {
        log.debug("convertWordToHTML(String)");
        File wordFile;
        String contents = null;
        FileInputStream in = null;
        try {
            wordFile = new File(filePath);
            in = new FileInputStream(wordFile.getAbsolutePath());
            WordExtractor extractor = new WordExtractor();
            contents = extractor.extractText(in);
        } catch (IOException e) {
            log.error("An exception occured in parsing the Word Document.", e);
        } catch (Exception e) {
            log.error("An exception occured in parsing the Word Document.", e);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                log.error("An exception occured in closing resources.", e);
            }
        }
        return contents;
    }
}
