package com.iw.activemq.procinq;

import com.iw.activemq.beans.ClassificationPackage;
import com.iw.rest.beans.Classifier;
import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

/**
 * Class to handle all the comunication with the Queue.
 *
 * @author Alex
 */
public class QueueProducer {

    private static Logger log = Logger.getLogger(QueueProducer.class);

    /**
     * Method to send the messages to the classification queue.
     *
     * @param classifyPackage Package that contains the information of the job to be classified.
     * @throws IllegalArgumentException
     * @throws JMSException
     * @throws NamingException
     */
    public static void sendMessage(ClassificationPackage classifyPackage) throws IllegalArgumentException, JMSException, NamingException {
        log.debug("Producer::sendMessage(ClassificationPackage)");
        if (classifyPackage == null || (classifyPackage.getDocumentDescriptor() == null)) {
            log.error("Error in Producer::sendMessage(ClassificationPackage) No Package provided to send to the queue.");
            throw new IllegalArgumentException("No Package provided to send to the queue");
        }
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            InitialContext initCtx = new InitialContext();
            Context envContext = (Context) initCtx.lookup("java:comp/env");
            ConnectionFactory connectionFactory = (ConnectionFactory) envContext.lookup("jms/ConnectionFactory");
            connection = connectionFactory.createConnection();
            connection.start();
            //Non transacted session.
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer((Destination) envContext.lookup("jms/queue/ClassificationQueue"));
            //Disable persistence for this queue.
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            Message objMessage = session.createObjectMessage(classifyPackage);
            long sendMsgStarted = System.currentTimeMillis();
            producer.send(objMessage);
            long sendMsgEnded = System.currentTimeMillis();
            log.info("Message sent in: " + (sendMsgEnded - sendMsgStarted) + "ms");
        } catch (JMSException exception) {
            log.error("Error in Producer::sendMessage(ClassificationPackage)", exception);
            throw exception;
        } catch (NamingException exception) {
            log.error("Error in Producer::sendMessage(ClassificationPackage)", exception);
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (session != null) {
                session.close();
            }
            if (producer != null) {
                producer.close();
            }
        }
    }

    /**
     * Method to register a classifier into the server throught the queue.
     *
     * @param classifier Classifier object that contains basic information. Required Id, IpAddress.
     * @throws IllegalArgumentException
     * @throws JMSException
     * @throws NamingException
     */
    public static void registerClassifier(Classifier classifier) throws IllegalArgumentException, JMSException, NamingException {
        log.debug("Producer::registerClassifier(Classifier)");
        if (classifier == null || classifier.getId() == null || classifier.getId().trim().equals("")) {
            log.error("Error in Producer::registerClassifier(Classifier) No Package provided to send to the queue.");
            throw new IllegalArgumentException("No Package provided to send to the queue");
        }
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            InitialContext initCtx = new InitialContext();
            Context envContext = (Context) initCtx.lookup("java:comp/env");
            ConnectionFactory connectionFactory = (ConnectionFactory) envContext.lookup("jms/ConnectionFactory");
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer((Destination) envContext.lookup("jms/queue/RegistrationQueue"));
            Message objMessage = session.createObjectMessage(classifier);
            long sendMsgStarted = System.currentTimeMillis();
            producer.send(objMessage);
            long sendMsgEnded = System.currentTimeMillis();
            log.info("Message sent in: " + (sendMsgEnded - sendMsgStarted) + "ms");
            connection.close();
        } catch (JMSException exception) {
            log.error("Error in Producer::registerClassifier(Classifier)", exception);
            throw exception;
        } catch (NamingException exception) {
            log.error("Error in Producer::registerClassifier(Classifier)", exception);
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (session != null) {
                session.close();
            }
            if (producer != null) {
                producer.close();
            }
        }
    }
}