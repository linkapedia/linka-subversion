package com.iw.rest.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.iw.application.context.AppServletContextListener;
import com.iw.rest.beans.Classifier;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class ClassifierResource {

    private static final Logger log = Logger.getLogger(ClassifierResource.class);
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    String classifierId;

    public ClassifierResource(UriInfo uriInfo, Request request, String classifierId) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.classifierId = classifierId;
    }

    //Application integration 		
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getClassifierAsXML() {
        log.debug("ClassifierResource::getClassifierAsXML()");
        Classifier classifier = null;
        if (AppServletContextListener.getClassifierList() != null) {
            classifier = AppServletContextListener.getClassifierList().get(classifierId);
        }
        if (classifier == null) {
            throw new RuntimeException("Get: Classifier [" + classifierId + "] not found");
        }
        return classifier.asXML();
    }

    @DELETE
    public void deleteClassifier() {
        log.debug("ClassifierResource::deleteClassifier()");
        Classifier classifier = null;
        if (AppServletContextListener.getClassifierList() != null) {
            classifier = AppServletContextListener.getClassifierList().remove(classifierId);
        }
        if (classifier == null) {
            throw new RuntimeException("Delete: Classifier [" + classifierId + "] not found");
        }
    }
}
