package com.iw.rest.beans;

/**
 *
 * @author Alex
 */
public class Task {

    private String action;
    private String[] ipAddress;

    public String[] getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String[] ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
