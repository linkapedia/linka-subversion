package com.iw.application.context;

import com.indraweb.execution.Session;
import com.iw.activemq.beans.ConfigurationPackage;
import com.iw.activemq.procinq.QueueProducer;
import com.iw.activemq.procinq.TopicProducer;
import com.iw.bo.ConfigParamsBO;
import com.iw.camel.processors.ClassificationProcessor;
import com.iw.camel.processors.ConfigurationProcessor;
import com.iw.camel.processors.RegistrationProcessor;
import com.iw.license.IndraLicense;
import com.iw.rest.beans.Classifier;
import com.yuxipacific.services.cache.CacheHandler;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppServletContextListener implements ServletContextListener {

    private static final transient Logger log = LoggerFactory.getLogger(AppServletContextListener.class);
    private static CamelContext camelctx = null;
    private static Map<String, Classifier> classifierCollection = null;
    private static String machineType = null;
    private static ResourceBundle systemConfig = ResourceBundle.getBundle("system/configuration");
    private static final String[] cacheRegionNames = new String[]{"documents", "rejecteddocuments", "terms", "musthaves"};

    @Override
    public void contextInitialized(ServletContextEvent servletCtxEvt) {
        log.debug("AppServletContextListener::contextInitialized()");
        try {
            log.info("ServletContextListener started");

            log.info("Starting Cache System");
            CacheHandler.initCacheHandler(cacheRegionNames);

            //Initialize Camel Context from JNDI resources.
            camelctx = createCamelContext();

            //Identify if the machine is the server or a classifier.
            machineType = servletCtxEvt.getServletContext().getInitParameter("MachineType");
            log.info("Starting Tomcat on " + machineType);
            if (machineType != null && machineType.equals("Classifier")) {
                //Register the classifier against the server.
                //Set the running flag in true, because the classifier is starting
                Classifier.getLocalInstance().setRunning(true);
                //send the message to the server to register this machine as a classifier.
                QueueProducer.registerClassifier(Classifier.getLocalInstance());
                camelctx.addRoutes(new RouteBuilder() {

                    @Override
                    public void configure() throws Exception {
                        ClassificationProcessor classificationProcessor = new ClassificationProcessor();
                        ConfigurationProcessor configurationProcessor = new ConfigurationProcessor();
                        String strThreadNumber = systemConfig.getString("system.classifier.threads");
                        String strConsumerNumber = systemConfig.getString("system.classifier.consumers");
                        int threadNumber;
                        int consumerNumber;
                        if (strThreadNumber == null || strThreadNumber.isEmpty()) {
                            strThreadNumber = "1";
                        }
                        if (strConsumerNumber == null || strConsumerNumber.isEmpty()) {
                            strConsumerNumber = "1";
                        }
                        try {
                            threadNumber = Integer.valueOf(strThreadNumber);
                        } catch (NumberFormatException e) {
                            log.error("The format for thread number in the properties is wrong. " + e.getMessage(), e);
                            threadNumber = 1;
                        }
                        try {
                            consumerNumber = Integer.valueOf(strConsumerNumber);
                        } catch (NumberFormatException e) {
                            log.error("The format for consumer number in the properties is wrong. " + e.getMessage(), e);
                            consumerNumber = 1;
                        }
                        String extraParams = systemConfig.getString("system.classifier.camel.uri.extraparams");
                        if (extraParams == null) {
                            extraParams = "";
                        }

                        //Read from the queue to perform the classifications.
                        from("activemq:ClassificationQueue?concurrentConsumers=" + consumerNumber + extraParams).routeId("ClassificationQueue").threads(threadNumber).process(classificationProcessor).end();
                        //Read from the topic to perform the operations requested.
                        from("activemq:topic:ProcinQ.Classifiers.ConfigurationTopic").routeId("ConfigurationTopic").process(configurationProcessor).end();
                    }
                });
            } else {
                //This is the server, so we initialize the classifier list.
                AppServletContextListener.setClassifierList(new HashMap<String, Classifier>());
                //Start the process to listen for classifier registrations.
                camelctx.addRoutes(new RouteBuilder() {

                    @Override
                    public void configure() throws Exception {
                        RegistrationProcessor registrationProcessor = new RegistrationProcessor();
                        //Read from the queue to register new classifiers in the server.
                        from("activemq:RegistrationQueue").routeId("RegistrationQueue").process(registrationProcessor).end();
                    }
                });
            }

            initializeSystem(servletCtxEvt);

            //Start the camel context.
            camelctx.start();

            //Force the reload of the classificator to get them registered on the Server.
            if (machineType != null && machineType.trim().equals("Server")) {
                ConfigurationPackage confPkg = new ConfigurationPackage("register");
                TopicProducer.sendMessage(confPkg);
            }
        } catch (Exception e) {
            log.error("Error: ", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletCtxEvt) {
        log.debug("AppServletContextListener::contextDestroyed()");
        try {
            log.info("ServletContextListener destroyed");
            //Identify if the machine is the server or a classifier.
            machineType = servletCtxEvt.getServletContext().getInitParameter("MachineType");
            if (machineType != null && machineType.equals("Classifier")) {
                //Set the shutting down flag in true, because the classifier is stoping all tasks.
                Classifier.getLocalInstance().setRunning(true);
                Classifier.getLocalInstance().setShuttingDown(true);
                //Send message to the RegistrationQueue to eliminate this Classifier.
                QueueProducer.registerClassifier(Classifier.getLocalInstance());
            }
            if (camelctx != null) {
                camelctx.stop();
            }
            CacheHandler.shutdown();
        } catch (Exception ex) {
            log.error("Error: ", ex);
        }
    }

    public static CamelContext getCamelContext() {
        return camelctx;
    }

    public static void setCamelContext(CamelContext camelContext) {
        AppServletContextListener.camelctx = camelContext;
    }

    public static Map<String, Classifier> getClassifierList() {
        return classifierCollection;
    }

    public static void setClassifierList(Map<String, Classifier> classifierCollection) {
        AppServletContextListener.classifierCollection = classifierCollection;
    }

    public static String getMachineType() {
        return machineType;
    }

    public static void setMachineType(String machineType) {
        AppServletContextListener.machineType = machineType;
    }

    private static void initializeSystem(ServletContextEvent servletCtxEv) throws ServletException {
        log.debug("AppServletContextListener::initializeClassifier()");
        // If the session is already initialized, return.
        if (com.indraweb.execution.Session.GetbInitedSession()) {
            return;
        }

        Enumeration eParams = servletCtxEv.getServletContext().getInitParameterNames();
        Map<String, String> htprops = new HashMap<String, String>();
        while (eParams.hasMoreElements()) {
            String sParamName = (String) eParams.nextElement();
            String sParamValu = (String) servletCtxEv.getServletContext().getInitParameter(sParamName);
            htprops.put(sParamName, sParamValu);
        }

        Session.sIndraHome = (String) servletCtxEv.getServletContext().getInitParameter("IndraHome");
        log.info("ts::init() Set Session.sIndraHome to [" + Session.sIndraHome + "]");
        Session.cfg = new com.indraweb.execution.ConfigProperties(htprops);

        // added by MAP 6/24/04, create license object and store in session
        String licenseKey = Session.cfg.getProp("licenseKey", false, "0");
        log.info("License key (" + licenseKey + ") initializing...");

        if (licenseKey.equals("0")) {
            log.warn("Warning! No license key was specified, starting server in evaluation mode.");
        } else {
            try {
                licenseKey = com.indraweb.utils.license.LicenseUtils.licenseToBitmap(licenseKey);
            } catch (Exception e) {
                log.error("Fatal error! Invalid license key specified.", e);
                throw new ServletException("Fatal error! Invalid license key specified: " + licenseKey);
            }
        }
        Session.license = new IndraLicense(licenseKey);
        //Config Params from DB
        log.debug("AppServletContextListener::initializeSystem()");
        Map<String, String> dbParameters;
        ConfigParamsBO configParamsBO;
        try {
            configParamsBO = new ConfigParamsBO();
            dbParameters = configParamsBO.getConfigParamsAsMap();
            if (!dbParameters.isEmpty()) {
                log.debug("Found " + dbParameters.size() + " parameters to config the system.");
                Session.cfg.addToProperties(dbParameters);
                log.debug("Parameters sucessfully loaded in system environment.");
            } else {
                log.warn("No properties found to load in the system environment.");
            }
        } catch (Exception e) {
            log.error("An exception ocurred. ", e);
        }
        Session.SetbInitedSession(true);
    }

    /**
     * Method to initialize the camel context.
     * @return Initialized camel context.
     * @throws Exception 
     */
    private static CamelContext createCamelContext() throws Exception {
        CamelContext camelContext = new DefaultCamelContext();
        camelContext.disableJMX();
        InitialContext initCtx = new InitialContext();
        Context envContext = (Context) initCtx.lookup("java:comp/env");
        ActiveMQConnectionFactory activeMQConnectionFactory = (ActiveMQConnectionFactory) envContext.lookup("jms/ConnectionFactory");
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(10);
        pooledConnectionFactory.setMaximumActive(200);
        ActiveMQComponent activeMQComponent = ActiveMQComponent.activeMQComponent();
        activeMQComponent.setUsePooledConnection(true);
        activeMQComponent.setConnectionFactory(pooledConnectionFactory.getConnectionFactory());
        camelContext.addComponent("activemq", activeMQComponent);
        return camelContext;
    }
}
