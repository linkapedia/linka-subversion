package com.iw.rest.beans;

import com.iw.camel.processors.ClassificationProcessor;
import java.util.Map;


import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.iw.utils.TimeUtils;
import java.io.Serializable;
import java.net.InetAddress;
import org.apache.log4j.Logger;

public class Classifier implements Serializable {

    private static final transient Logger log = Logger.getLogger(Classifier.class);
    private static Classifier _classifier;
    private String id;
    private String ipAddress;
    private long starttime;
    private long worksProcessed;
    private long worksFailed;
    private long worksOnDB;
    private Map<String, String> information;
    private boolean isRunning = false;
    private boolean isShuttingDown = false;

    private Classifier() {
    }

    public Classifier(String id, String ipAddress) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.starttime = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Map<String, String> getInformation() {
        return information;
    }

    public void setInformation(Map<String, String> information) {
        this.information = information;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    public boolean isShuttingDown() {
        return isShuttingDown;
    }

    public void setShuttingDown(boolean isShuttingDown) {
        this.isShuttingDown = isShuttingDown;
    }

    public long getUptime() {
        return System.currentTimeMillis() - starttime;
    }

    public String getUptimeAsString() {
        return TimeUtils.millisToLongDHMS(getUptime());
    }

    public long getWorksFailed() {
        return worksFailed;
    }

    public void setWorksFailed(long worksfailed) {
        this.worksFailed = worksfailed;
    }

    public long getWorksProcessed() {
        return worksProcessed;
    }

    public void setWorksProcessed(long worksprocessed) {
        this.worksProcessed = worksprocessed;
    }

    public long getWorksOnDB() {
        return worksOnDB;
    }

    public void setWorksOnDB(long worksOnDB) {
        this.worksOnDB = worksOnDB;
    }

    public Element getClassifierASXMLElement() {
        Document document = null;
        Element root = null;
        if (this != null) {
            document = DocumentHelper.createDocument();
            root = document.addElement("classifier");
            root.addElement("id").addText(this.getId());
            root.addElement("ipAddress").addText(this.getIpAddress());
            root.addElement("isRunning").addText((this.isRunning()) ? "true" : "false");
            root.addElement("isShuttingDown").addText((this.isShuttingDown()) ? "true" : "false");
            root.addElement("uptime").addText(getUptimeAsString());
            root.addElement("workprocessed").addText(String.valueOf(getWorksProcessed()));
            root.addElement("worksfailed").addText(String.valueOf(getWorksFailed()));
            root.addElement("worksondb").addText(String.valueOf(getWorksOnDB()));
            Element info = null;
            if (this.getInformation() != null && !this.getInformation().isEmpty()) {
                info = root.addElement("info");
                for (String clave : this.getInformation().keySet()) {
                    info.addAttribute(clave, this.getInformation().get(clave));
                }
            }
        }
        return root;
    }

    public String asXML() {
        Document document = null;
        document = DocumentHelper.createDocument();
        Element root = document.addElement("classifier");
        root.addElement("id").addText(this.getId());
        root.addElement("ipAddress").addText(this.getIpAddress());
        root.addElement("isRunning").addText((this.isRunning()) ? "true" : "false");
        root.addElement("isShuttingDown").addText((this.isShuttingDown()) ? "true" : "false");
        root.addElement("uptime").addText(getUptimeAsString());
        root.addElement("workprocessed").addText(String.valueOf(getWorksProcessed()));
        root.addElement("worksfailed").addText(String.valueOf(getWorksFailed()));
        root.addElement("worksondb").addText(String.valueOf(getWorksOnDB()));
        Element info = null;
        if (this.getInformation() != null && !this.getInformation().isEmpty()) {
            info = root.addElement("info");
            for (String clave : this.getInformation().keySet()) {
                info.addAttribute(clave, this.getInformation().get(clave));
            }
        }
        return (document != null) ? document.asXML() : "";
    }

    public static Classifier getLocalInstance() {
        log.debug("Classifier::getLocalInstance()");
        String hostname = null;
        String ipAddress = null;
        if (_classifier == null) {
            try {
                hostname = InetAddress.getLocalHost().getHostName();
                log.debug("Classifier Hostname: " + hostname);
                ipAddress = InetAddress.getLocalHost().getHostAddress();
                log.debug("Classifier IpAddress: " + ipAddress);
                _classifier = new Classifier(hostname, ipAddress);
            } catch (Exception exception) {
                log.error("Error in Classifier::getLocalInstance()", exception);
            }
        } else {
            //Refress the counters and return the instance.
            _classifier.setWorksFailed(ClassificationProcessor.getWorksFailedCounter());
            _classifier.setWorksProcessed(ClassificationProcessor.getWorksOkCounter());
            _classifier.setWorksOnDB(ClassificationProcessor.getWorksOnDBCounter());
        }
        return _classifier;
    }
}
