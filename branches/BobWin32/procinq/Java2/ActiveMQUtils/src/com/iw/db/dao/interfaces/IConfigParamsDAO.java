package com.iw.db.dao.interfaces;

import com.iw.db.beans.ConfigParam;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alex
 */
public interface IConfigParamsDAO {

    public List<ConfigParam> getConfigParamsAsList() throws SQLException, Exception;

    public Map<String, String> getConfigParamsAsMap() throws SQLException, Exception;
}
