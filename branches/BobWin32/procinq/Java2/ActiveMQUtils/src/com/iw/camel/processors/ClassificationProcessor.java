package com.iw.camel.processors;

import com.iw.activemq.beans.ClassificationPackage;
import com.iw.application.context.AppServletContextListener;
import com.iw.db.beans.ClassificationResult;
import com.iw.db.mysql.MySQLHandler;
import com.iw.system.InvokeAPI;
import com.sun.jersey.api.NotFoundException;
import com.yuxipacific.documents.beans.DocumentDescriptor;
import com.yuxipacific.documents.network.FileDownloader;
import com.yuxipacific.documents.parsers.ParserProxy;
import com.yuxipacific.documents.storage.exceptions.download.DeniedDownloadException;
import com.yuxipacific.documents.storage.exceptions.download.FileSizeExceededException;
import com.yuxipacific.documents.storage.exceptions.parser.EmptyDocumentException;
import com.yuxipacific.documents.storage.exceptions.parser.NoParseableDocumentException;
import com.yuxipacific.documents.text.SentenceExtractorProxy;
import com.yuxipacific.documents.utils.FileUtils;
import com.yuxipacific.generic.html.utils.HTMLUtils;
import com.yuxipacific.security.utils.DigestUtils;
import com.yuxipacific.services.system.OSUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.*;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.tree.DefaultElement;

public class ClassificationProcessor implements Processor {

    private static Logger log = Logger.getLogger(ClassificationProcessor.class);
    private static volatile long worksOkCounter = 0;
    private static volatile long worksFailedCounter = 0;
    private static volatile long worksOnDBCounter = 0;
    private static final long worksThreshold;
    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("system/configuration");
    private static final Object extractionLock = new Object();
    private static final Object classificationLock = new Object();

    static {
        String worksThresholdStr;
        worksThresholdStr = systemConfig.getString("system.classifier.workthreshold");
        if (worksThresholdStr == null || worksThresholdStr.isEmpty()) {
            worksThresholdStr = "500";
        }
        worksThreshold = Long.parseLong(worksThresholdStr);
    }

    @Override
    public void process(Exchange exchange) {
        log.debug("ClassificationProcessor::process");
        String fileURI;
        DocumentDescriptor documentDescriptor;
        try {
            Message message = exchange.getIn();
            Object objToClassify = message.getBody();
            if (objToClassify instanceof ClassificationPackage) {
                log.info(".::  ClassificationPackage Received  ::.");
                long startTime = System.currentTimeMillis();
                ClassificationPackage classifyPackage = (ClassificationPackage) objToClassify;
                documentDescriptor = classifyPackage.getDocumentDescriptor();
                //Get the file we are going to feed to the classifier.
                fileURI = documentDescriptor.getSrcURL();
                log.info("Started the classification process for the file: " + fileURI);

                // Calls the function to classify this type of package
                log.debug("Performing the classification of Package with URI <" + fileURI + ">");
                List<String> corpusIDList = classifyPackage.getCorpusIDList();
                log.info("Getting " + corpusIDList + " CorpusID for URI");

                for (String corpusID : corpusIDList) {
                    try {
                        //We need to download, parse and extract the sentences of this file.
                        log.debug("Performing the document processing phase for the URL: <" + fileURI + ">");
                        documentDescriptor = getFileUsingDocDescriptor(corpusID, documentDescriptor);
                        if (!documentDescriptor.isRejected() || documentDescriptor.isValid()) {
                            log.info("Going to classify: " + fileURI + " using corpusID=" + corpusID);
                            classify(documentDescriptor, corpusID);
                            //Refresh work count for this classifier.
                            synchronized (classificationLock) {
                                worksOkCounter++;
                            }
                        }
                        if (worksOkCounter + worksFailedCounter > worksThreshold) {
                            log.info("The classifier has reached the configured job threshold {" + worksThreshold + "} going to close the message channel and restart the classifier.");
                            CamelContext ctx = AppServletContextListener.getCamelContext();
                            if (ctx != null) {
                                ctx.stop();
                            }
                            log.info("Going to restart the classifier. Waiting 2 minutes.");
                            OSUtils.restartHost();
                        }
                    } catch (NotFoundException ex) {
                        log.error("Warning: Site <" + fileURI + "> not found. " + ex.getMessage());
                        synchronized (classificationLock) {
                            worksFailedCounter++;
                        }
                    } catch (IOException ex) {
                        log.error("Warning: Cannot open the file <" + fileURI + ">. " + ex.getMessage());
                        synchronized (classificationLock) {
                            worksFailedCounter++;
                        }
                    } catch (Exception ex) {
                        log.error("Warning: The file <" + fileURI + "> does not contains any word in the taxonomies." + ex.getMessage());
                        synchronized (classificationLock) {
                            worksFailedCounter++;
                        }
                    }
                }
                long endTime = System.currentTimeMillis();
                log.info("Classification process total execution time: " + (endTime - startTime) + "ms");
            }
        } catch (Exception ex) {
            log.error("An exception ocurred handling a classification request. ", ex);
        }
    }

    /**
     * Method to call the API function to classify a file against a taxonomy.
     *
     * @param file
     * @param sCorpusID
     * @param documentURI
     * <p/>
     * @return
     * <p/>
     * @throws Exception
     */
    public void classify(DocumentDescriptor docDescriptor, String sCorpusID) throws IllegalArgumentException, Exception {
        log.debug("ClassificationProcessor::classify(DocumentDescriptor, String)");
        Map<String, String> APIArguments;
        File file;
        try {
            file = new File(docDescriptor.getExtractedDocURL());
            log.debug("Received file: " + file.getAbsolutePath());

            //Check if we have the file, if not, retry
            if (file == null || !file.exists() || !file.isFile()) {
                log.error("IO Error in classify. Document not provided for classification");
                throw new IllegalArgumentException("Document not provided for classification");
            }

            //Get execution parameter for the classification.
            APIArguments = getExecutionParams();
            APIArguments.put("Corpora", sCorpusID);
            APIArguments.put("DocTitle", docDescriptor.getTitle());
            APIArguments.put("URL", "file://" + docDescriptor.getExtractedDocURL());
            APIArguments.put("FulltextURL", "file://" + docDescriptor.getExtractedDocURL());

            //URL to calculate the relevance of a link.
            Base64 base64 = new Base64();
            String sWebURL = base64.encodeAsString(docDescriptor.getSrcURL().getBytes());
            APIArguments.put("sWebURL", sWebURL);


            // Invoke the API
            InvokeAPI API = new InvokeAPI("tsclassify.TSClassifyDoc", new Hashtable(APIArguments));
            log.debug("Going to call the API to classify");
            long startTimeAPICall = System.currentTimeMillis();
            Document doc = API.dExecute(file);
            long endTimeAPICall = System.currentTimeMillis();
            log.info("API call total execution time: " + (endTimeAPICall - startTimeAPICall) + "ms");
            if (doc != null) {
                Element elemRoot = doc.getRootElement();

                Element eDocs = elemRoot.element("CLASSIFICATIONRESULTSET");
                if (eDocs == null) {
                    log.warn("ClassificationResultSet tag does not exist in XML output");
                    log.info("The document {" + docDescriptor.getSrcURL() + "} was not classified because it didnt pass the classification process. ");
                    return;
                }

                log.debug("Going to retrieve the classifier results for the document.");
                //search in the nodes for the document score and the nodeID
                List<DefaultElement> nodeList = doc.selectNodes("//TSRESULT/CLASSIFICATIONRESULTSET/NODE");
                ClassificationResult clResult;
                String classificationScore01;
                String classificationScore02;
                String classificationScore03;
                String classificationScore04;
                String classificationScore05;
                String classificationScore06;
                String classificationScore07;
                String classificationScore08;
                String classificationScore09;
                String classificationScore10;
                String classificationScore11;
                String classificationScore12;
                String classificationNodeTitleMatchScore;
                if (nodeList != null && !nodeList.isEmpty()) {
                    log.info("Going to insert classification data for doc: " + docDescriptor.getSrcURL() + " in MYSQL");
                    long startDBInsert = System.currentTimeMillis();
                    for (DefaultElement defaultElement : nodeList) {
                        classificationScore01 = defaultElement.element("SCORE1").getText();
                        classificationScore02 = defaultElement.element("SCORE2").getText();
                        classificationScore03 = defaultElement.element("SCORE3").getText();
                        classificationScore04 = defaultElement.element("SCORE4").getText();
                        classificationScore05 = defaultElement.element("SCORE5").getText();
                        classificationScore06 = defaultElement.element("SCORE6").getText();
                        classificationScore07 = defaultElement.element("SCORE7").getText();
                        classificationScore08 = defaultElement.element("SCORE8").getText();
                        classificationScore09 = defaultElement.element("SCORE9").getText();
                        classificationScore10 = defaultElement.element("SCORE10").getText();
                        classificationScore11 = defaultElement.element("SCORE11").getText();
                        classificationScore12 = defaultElement.element("SCORE12").getText();
                        //New score nodetitlematch
                        classificationNodeTitleMatchScore = defaultElement.element("SCORE0").getText();

                        String nodeId = defaultElement.element("NODEID").getText();
                        clResult = new ClassificationResult();
                        clResult.setNodeID(Integer.parseInt(nodeId));
                        clResult.setRemoteURI(docDescriptor.getSrcURL());
                        clResult.setLocalURI(docDescriptor.getDstURL());
                        clResult.setParsedURI(docDescriptor.getParsedDocURL());
                        clResult.setExtractedURI(docDescriptor.getExtractedDocURL());
                        clResult.setDocTitle(docDescriptor.getTitle());
                        clResult.setScore01(Float.valueOf(classificationScore01));
                        clResult.setScore02(Float.valueOf(classificationScore02));
                        clResult.setScore03(Float.valueOf(classificationScore03));
                        clResult.setScore04(Float.valueOf(classificationScore04));
                        clResult.setScore05(Float.valueOf(classificationScore05));
                        clResult.setScore06(Float.valueOf(classificationScore06));
                        clResult.setScore07(Float.valueOf(classificationScore07));
                        clResult.setScore08(Float.valueOf(classificationScore08));
                        clResult.setScore09(Float.valueOf(classificationScore09));
                        clResult.setScore10(Float.valueOf(classificationScore10));
                        clResult.setScore11(Float.valueOf(classificationScore11));
                        clResult.setScore12(Float.valueOf(classificationScore12));
                        clResult.setPageRankScore(Float.valueOf(classificationNodeTitleMatchScore));

                        if (MySQLHandler.tableExists(sCorpusID) || MySQLHandler.createTableByCorpusID(sCorpusID)) {
                            if (MySQLHandler.insertClassificationResult(clResult, sCorpusID)) {
                                synchronized (classificationLock) {
                                    worksOnDBCounter++;
                                }
                            }
                        }
                    }
                    long endDBInsert = System.currentTimeMillis();
                    log.info("Insert call total execution time: " + (endDBInsert - startDBInsert) + "ms");
                }
            } else {
                log.warn("No Document Returned from classification");
            }
        } catch (NotFoundException ex) {
            log.error("Page not found for uri: " + ex.getLocalizedMessage());
            throw ex;
        } catch (IllegalArgumentException e) {
            log.error("No valid argument provided." + e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error("General Error in classify().", e);
            throw e;
        }
    }

    /**
     *
     * @return
     */
    private Map<String, String> getExecutionParams() {
        log.debug("getExecutionParams()");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("ExplainScores", "false");
        //Post Classification Results to the Database
        parameters.put("post", "false");
        //Dont generate DocSummary
        parameters.put("DocSumm", "false");
        parameters.put("WANTDOCSUMMARY", "false");
        return parameters;
    }

    /**
     * Method to handle the download, parsing and text extraction for a given @code{DocumentDescriptor}
     * <p/>
     * @param corpusID Taxonomy identifier to be used as part of the download path!
     * @param docDescriptor Object that contains the required information to perform the download of a give file.
     * <p/>
     * @return Full document descriptor with all the URLs (Download file location, Parsed file location, Extracted file location).
     */
    private static DocumentDescriptor getFileUsingDocDescriptor(String corpusID, DocumentDescriptor docDescriptor) {
        log.debug("getFileUsingDocDescriptor(String, DocumentDescriptor)");
        String downloadLocation;
        String documentContent;
        String extractedContent;
        String parsedLocation;
        String extractedLocation;
        try {
            StringBuilder baseLocationPath = new StringBuilder(systemConfig.getString("system.classificationfiles.location"));

            baseLocationPath.append(corpusID);
            baseLocationPath.append(File.separatorChar);
            //Send the document descriptor to the Download Proxy.
            //We need to download the file and store the metadata in the document descriptor.

            //Download the file
            downloadLocation = FileDownloader.download(new URL(docDescriptor.getSrcURL()), baseLocationPath.toString());
            docDescriptor.setDstURL(downloadLocation);

            log.info("The file [" + docDescriptor.getSrcURL() + "] has been downloaded to: [" + downloadLocation + "]");
            documentContent = ParserProxy.parse(new URL(docDescriptor.getSrcURL()));
            if (documentContent == null || documentContent.isEmpty() || "".equals(documentContent)) {
                log.error("The parser phase did not generate content.");
                throw new EmptyDocumentException("The parser phase did not generate content.");
            }

            //Write the parsed content to the file system.
            parsedLocation = FileUtils.writeFile(DigestUtils.getMD5(docDescriptor.getSrcURL()), baseLocationPath.toString() + "parsed" + File.separatorChar, documentContent);
            docDescriptor.setParsedDocURL(parsedLocation);
            log.info("The file [" + docDescriptor.getDstURL() + "] has been parsed to: [" + parsedLocation + "]");

            synchronized (extractionLock) {
                //Sentence Extractor
                extractedContent = SentenceExtractorProxy.extractFrom(documentContent);
                if (extractedContent == null || extractedContent.isEmpty() || "".equals(extractedContent)) {
                    log.error("The extraction phase did not generate content.");
                    throw new EmptyDocumentException("The extraction phase did not generate content.");
                }

                //We need to wrap the extracted content in a HTML file.
                extractedContent = HTMLUtils.wrapTextFileIntoHTML(docDescriptor.getTitle(), extractedContent);

                //Write the extracted content to the file system.
                extractedLocation = FileUtils.writeFile(DigestUtils.getMD5(docDescriptor.getSrcURL()), baseLocationPath.toString() + "extracted" + File.separatorChar, extractedContent);
                docDescriptor.setExtractedDocURL(extractedLocation);
                log.info("The file [" + docDescriptor.getParsedDocURL() + "] has been extracted to: [" + extractedLocation + "]");
            }
            return docDescriptor;
        } catch (FileNotFoundException ex) {
            log.error("An exception has ocurred. File Not Found: " + ex.getLocalizedMessage());
            docDescriptor.setRejected(true);
            docDescriptor.setRejectedReason(ex.getLocalizedMessage());
        } catch (DeniedDownloadException ex) {
            log.error("An exception has ocurred. Download Denied: " + ex.getLocalizedMessage());
            docDescriptor.setRejected(true);
            docDescriptor.setRejectedReason(ex.getLocalizedMessage());
        } catch (FileSizeExceededException ex) {
            log.error("An exception has ocurred. File size exceeded: " + ex.getLocalizedMessage());
            docDescriptor.setRejected(true);
            docDescriptor.setRejectedReason(ex.getLocalizedMessage());
        } catch (NoParseableDocumentException ex) {
            log.error("An exception has ocurred. No parseable document" + ex.getLocalizedMessage());
            docDescriptor.setRejected(true);
            docDescriptor.setRejectedReason(ex.getLocalizedMessage());
        } catch (EmptyDocumentException ex) {
            log.error("An exception has ocurred. Empty Document: " + ex.getLocalizedMessage());
            docDescriptor.setRejected(true);
            docDescriptor.setRejectedReason(ex.getLocalizedMessage());
        } catch (SocketTimeoutException ex) {
            log.error("An exception has ocurred. Connection Timeout: " + ex.getLocalizedMessage());
            docDescriptor.setRejected(true);
            docDescriptor.setRejectedReason(ex.getLocalizedMessage());
        } catch (IOException ex) {
            log.error("An exception has ocurred." + ex.getLocalizedMessage(), ex);
            docDescriptor.setRejected(true);
            docDescriptor.setRejectedReason(ex.getLocalizedMessage());
        } catch (Exception ex) {
            log.error("An exception has ocurred." + ex.getLocalizedMessage(), ex);
        }
        return docDescriptor;
    }

    public static long getWorksFailedCounter() {
        return worksFailedCounter;
    }

    public static long getWorksOkCounter() {
        return worksOkCounter;
    }

    public static long getWorksOnDBCounter() {
        return worksOnDBCounter;
    }
}
