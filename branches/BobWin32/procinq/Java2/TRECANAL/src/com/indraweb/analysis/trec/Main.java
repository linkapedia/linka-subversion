/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 19, 2002
 * Time: 7:27:29 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.analysis.trec;

import java.util.*;
import java.sql.Connection;
import java.io.PrintWriter;

import com.indraweb.util.UtilFile;

import com.indraweb.database.DBAccess_Central_connection;
import com.indraweb.util.*;
import com.indraweb.execution.Session;

public class Main {

    //public static int[] iArrQIDs = {403};
    private static String sFile = "g:/temp/t7_node434.txt";
    public static int[] iArrQIDs = {434};
    //public static int[] iArrQIDs = {413,416,420,429,434,435,441,444,447,449,450};
    //public static int[] iArrQIDs = {403,406,411,413,416,420,429,434,435,441,444,447,449,450};
    //public static int[] iArrQIDs = {447};

    public static void main ( String[] args )
    {
        Connection dbc = null;
        try {
            if ( !com.indraweb.execution.Session.GetbInitedSession() )
            {
                Hashtable htprops =	new	Hashtable();
                // Load	environment	from JRUN config application variables first
                Session.sIndraHome = "C:/Documents and Settings/Indraweb/IndraHome";
                htprops.put	( "ImportDir", "C:/TEMP" );
                htprops.put	( "AuthenticationSystem", "ActiveDirectory"	);
                htprops.put	( "MachineName", "indraweb-e5j05c");
                htprops.put	( "DBString_API_User", "sbooks");
                htprops.put	( "DBString_SVR_User", "sbooks");
                htprops.put	( "DBString_API_Pass", "racer9");
                htprops.put	( "DBString_SVR_Pass", "racer9");
                htprops.put	( "DBString_API_OracleJDBC", "jdbc:oracle:thin:@66.134.131.60:1521:client");
                htprops.put	( "DBString_SVR_OracleJDBC", "jdbc:oracle:thin:@66.134.131.37:1521:gaea");
                htprops.put	( "IndraHome", Session.sIndraHome);
                htprops.put	( "IndraFolderHome", Session.sIndraHome);		//}

                Session.cfg	= new com.indraweb.execution.ConfigProperties(htprops);
                Session.SetbInitedSession(true);

                dbc = api.MainTest.initializeDB("API");
            }
        }
        catch ( Exception e ) {
            com.indraweb.util.Log.log("err in test main", e);
        }

        int iProgramModel = 2;
        if ( iProgramModel == 1 ) // original detail reports - has error in top 10 reporting
        {
            try   // real code - not init
            {
                String sFileName_qrel = "C:/ausr/hkon/IndraWeb/technology/trec anal/qrel8_hk.txt";
                // int iNumRows = LoadQrelToDB.loadqreltodb(sFileName_qrel, dbc) ;
                //System.out.println("iNumRows loaded from :"+iNumRows );
                //testclass.verifyURLPiecesAre1To1WithFullURL(dbc);
                testclass.outputDQQueryResilt(dbc);
                int i = 1;
                if ( i == 1 )
                    System.exit(0);
                // STEP 2 CalculatePrecisionAndRecall.outputPandRNumbers(dbc);
                // STEP 3

                UtilFile.addLineToFileKill(sFile, "TREC PRECISION AND RECALL REPORT " + new java.util.Date() + "\r\n");
                long lStartwholeprogram = System.currentTimeMillis();
                for ( int iQidIdx = 0; iQidIdx < iArrQIDs.length; iQidIdx++ ) // loop thru Q id's
                {
                    long lStartThisQLoop = System.currentTimeMillis();
                    // PRECISION
                    if ( true )
                    {
                        long lStartthisQloop_precisionreport = System.currentTimeMillis();
                        UtilFile.addLineToFile(sFile , "QId\tQDesc\tDocumentid\tDoctitle\tDdocurl\tNodeTitle\tNodeid\tCorpusid\tScore1STR\tScore5COV\tScore6FRQ\tScore5*6\r\n" );
                        UtilFile.addLineToFile(sFile, "PRECISION REPORT\r\n" );

                        TrecCompareCounters tcc = TrecIwComparePrecision.outputPrecisionNumbers(TrecIwComparePrecision.iREPORTTYPE_PRECISION,
                                iQidIdx, iArrQIDs[iQidIdx], dbc, sFile);
                        UtilFile.addLineToFile(sFile ,tcc.toString() );
                        UtilFile.addLineToFile(sFile , "time in precision report this Qid [" + UtilProfiling.elapsedTimeMillis(lStartthisQloop_precisionreport) + "]\r\n" );
                    }

                    // RECALL
                    if ( true )
                    {
                        UtilFile.addLineToFile(sFile, "RECALL REPORT\r\n" );
                        long lStartthisQloop_recallreport = System.currentTimeMillis();
                        TrecCompareCounters tcc = TrecIwComparePrecision.outputPrecisionNumbers(
                                TrecIwComparePrecision.iREPORTTYPE_RECALL,iQidIdx, iArrQIDs[iQidIdx], dbc, sFile);
                        UtilFile.addLineToFile(sFile ,tcc.toString() );
                        UtilFile.addLineToFile(sFile , "time in recall report this Qid [" + UtilProfiling.elapsedTimeMillis(lStartthisQloop_recallreport) + "]\r\n" );
                        //TrecIwCompareReport.outputRecallNumbers(dbc);
                    }
                    UtilFile.addLineToFile(sFile , "time total this Qid [" + UtilProfiling.elapsedTimeMillis(lStartThisQLoop) + "]\r\n" );

                }
                UtilFile.addLineToFile(sFile , "time total this program [" + UtilProfiling.elapsedTimeMillis(lStartwholeprogram) + "]\r\n" );

            } catch ( Throwable t )
            {
                t.printStackTrace();
                System.out.println("top level throwable");
                com.indraweb.util.Log.NonFatalError("top level throwable", t );
            }
            int i =     0;
        }
        else if ( iProgramModel == 2)
        {

            try{

				com.indraweb.analysis.trec.ReportPRSummaryIterative.runSummaryReport_PR(403,
															  30,
															  false,
															  dbc,
                                                              new PrintWriter (System.out));
            } catch ( Throwable t )
            {
                t.printStackTrace();
                System.out.println("top level throwable");
                com.indraweb.util.Log.NonFatalError("top level throwable", t );
            }
            int i =     0;
        }

    }


}
