/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 20, 2002
 * Time: 7:30:34 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.analysis.trec;

import java.sql.Connection;

public class CalculatePrecisionAndRecall {

    static int[] iArrQIDs = {403,4031,406,411,413,416,420,429,434,435,441,444,447,449,450};
    public static void outputPandRNumbers ( Connection dbc )
        throws Exception
    {
        int iSizeOfIntersection = -1;  // |{{IWPS} Intersect {TrecPS}}|
        int iSizeOfIWalone = -1;  // |{TrecPS}|
        int iSizeOfTRECalone = -1;  // |{IWPS}|

        for ( int i = 0; i < iArrQIDs.length; i++ )
        {
            String sSQLIntersect = "	select documentif from trecqnode tqn, nodedocument nd, document d " +
                " where tqn.nodeid = nd.nodeid and  d.documentid = nd.documentid and " +
                " docurl like '\\\\66.134.131.60\\60gb db drive\\trec\\tars\\%' and questionid = " + iArrQIDs[i] +
                " intersect " +
                " select documentid from trecqrel where qestionid = " + iArrQIDs[i] + " and status = 1 ";
            com.indraweb.util.Log.logClearcr("sSQLIntersect :" + sSQLIntersect );

            String sSQLIWAlone= "	select count (*) from trecqnode tqn, nodedocument nd, document d where tqn.nodeid = nd.nodeid and  d.documentid = nd.documentid and docurl like '\\\\66.134.131.60\\60gb db drive\\trec\\tars\\%' and questionid = " +
                        iArrQIDs[i];
            com.indraweb.util.Log.logClearcr("sSQLIWAlone :" + sSQLIWAlone );

            String sSQLTrecAlone= "select count (*) from trecqrel where qestionid = " + iArrQIDs[i] + " and status = 1";
            com.indraweb.util.Log.logClearcr("sSQLTrecAlone:" + sSQLTrecAlone);

            iSizeOfIntersection = com.indraweb.database.JDBCIndra_Connection.executeSQLint(sSQLIntersect, dbc);
            iSizeOfIWalone = com.indraweb.database.JDBCIndra_Connection.executeSQLint(sSQLIWAlone, dbc);
            iSizeOfTRECalone = com.indraweb.database.JDBCIndra_Connection.executeSQLint(sSQLTrecAlone, dbc);

            com.indraweb.util.Log.logClearcr( i + ". Qid [" + iArrQIDs[i] +
                    "] iSizeOfIntersection [" + iSizeOfIntersection  +
                    "] iSizeOfIWalone [" + iSizeOfIWalone +
                    "] iSizeOfTRECalone [" + iSizeOfTRECalone + "]"
            );
            com.indraweb.util.Log.logClearcr( i + ". Qid [" + iArrQIDs[i] +
                    "] P (iSizeOfIntersection / iSizeOfIWalone) [" + (iSizeOfIntersection / iSizeOfTRECalone) +
                    "] R (iSizeOfIntersection / iSizeOfTRECalone) [" + (iSizeOfIntersection / iSizeOfTRECalone)
            );



;



        }



    }

}

/*
TrecPS = trec positive set =
 select documentid from trecqrel where qestionid = 403 and status = 1;    (21)

TNS = trec negative set =
 select documentid from trecqrel where qestionid = 403 and status = 0;    (1025)

IWPS = indraweb positive set =
 select d.documentid from trecqnode tqn, nodedocument nd, document d where tqn.nodeid = nd.nodeid and  d.documentid = nd.documentid and docurl like '\\66.134.131.60\60gb db drive\trec\tars\%' and questionid = 403 and nd.score1 > 0
*/