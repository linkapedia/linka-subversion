package com.linkapedia.mapreducejobs.behaviors.interfaces;

import com.linkapedia.mapreducejobs.beans.ClassifierInfo;

/**
 *
 * @author andres
 */
public interface ISentencesProcess {

    public ClassifierInfo generateSentences(String htmlString);
}
