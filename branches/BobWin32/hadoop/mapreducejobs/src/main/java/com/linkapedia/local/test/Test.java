/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.local.test;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.linkapedia.mapreducejobs.beans.ClassifierInfo;
import com.linkapedia.mapreducejobs.behaviors.concrete.SentenceRenditionBehavior;
import com.linkapedia.mapreducejobs.behaviors.concrete.SentenceExtractorBehavior;
import com.linkapedia.mapreducejobs.output.OutputTask;
import com.linkapedia.mapreducejobs.output.SentenceExtractionProcess;
import com.linkapedia.mapreducejobs.util.BlackList;
import com.linkapedia.mapreducejobs.util.BringFilesFromS3;
import com.linkapedia.mapreducejobs.util.EightyLegResults;
import com.linkapedia.mapreducejobs.util.ParserProxy;
import com.linkapedia.mapreducejobs.util.SentenceExtractorProxy;
import com.linkapedia.mapreducejobs.util.StoreUtil;
import com.linkapedia.mapreducejobs.util.TestUtil;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author andres
 */
public class Test {

    public static void main(String args[]) {
        System.out.println("\"***START TESTING BEHAVIORS***");
        classifyingSentencesFromEightyLocalFile("/home/andy/Descargas/369854_375434_a_1.80");
//        String inputPath = "/home/andres/Downloads/bad/";
//        String s3File = "550534_555411_a_10.zip";
//        classifyingSentencesFromLocalHtmls(inputPath);
//        classifyingSentencesFromS3(s3File);
//        generatingSentencesLocalFromS3();
//        gettingHtmlsFromEightyLegFile();
//        testTitle();
//        TestingJsoup();
//        getTitleRegularExpression();
        System.out.println("***FINISH TESTING BEHAVIORS***");
    }

    /**
     * Metodo que clasifica informacion a partir de htmls puestos en una carpeta
     * de entrada
     *
     * @param inputPath
     */
    public static void classifyingSentencesFromLocalHtmls(String inputPath) {
        File folder = new File(inputPath);
        File[] listOfFiles = folder.listFiles();
        OutputTask sentenceTask = new SentenceExtractionProcess();
        ClassifierInfo classifierSetences = null;
        ClassifierInfo renditionSentences = null;
        String html = null;
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println("***************** Getting the html *****************");
                html = getContentFromFile(file);
                try {
                    sentenceTask.setSentencesBehavior(new SentenceExtractorBehavior());
                    classifierSetences = sentenceTask.generateSentences(html);
                    if (classifierSetences != null && (!"".equals(classifierSetences.getSentences()) && classifierSetences.getSentences() != null)) {
                        StoreUtil.storeDynamoNodeDocument(classifierSetences.getclassifierScores(), "key", classifierSetences.getTitle());
                        StoreUtil.storeSentencesS3(classifierSetences.getSentences(), "key");
                        System.out.println("***************** Info Classified *****************");
                        System.out.println(classifierSetences.getclassifierScores());
                        System.out.println("");
                    }
                } catch (Exception ex) {
                    System.out.println("Error...");
                }
                try {
                    sentenceTask.setSentencesBehavior(new SentenceRenditionBehavior());
                    renditionSentences = sentenceTask.generateSentences(html);
                    if (renditionSentences != null && !"".equals(renditionSentences.getSentences()) && renditionSentences.getSentences() != null) {
                        StoreUtil.storeSentencesRenditionS3(renditionSentences.getSentences(), "key");
                        System.out.println("");
                    }

                } catch (Exception ex) {
                    System.out.println("Error... in rendition");
                }
            }
        }
    }

    /**
     * Metodo que clasifica informacion a partir de un 80 leg que esta en S3 de
     * amazon
     *
     * @param line
     */
    public static void classifyingSentencesFromS3(String line) {
        BringFilesFromS3 bfs = new BringFilesFromS3();
        HashMap<String, byte[]> results;
        OutputTask sentenceTask = new SentenceExtractionProcess();
        ClassifierInfo classifierSetences = null;
        ClassifierInfo renditionSentences = null;
        try {
            results = bfs.getZipContentFromS3(line);
            if (results != null) {
                Set<String> keySet = results.keySet();
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        try {
                            sentenceTask.setSentencesBehavior(new SentenceExtractorBehavior());
                            classifierSetences = sentenceTask.generateSentences(content);
                            if (classifierSetences != null && !"".equals(classifierSetences.getSentences()) && !"".equals(classifierSetences.getTitle())) {
                                StoreUtil.storeDynamoNodeDocument(classifierSetences.getclassifierScores(), "key", classifierSetences.getTitle());
                                StoreUtil.storeSentencesS3(classifierSetences.getSentences(), "key");
                                System.out.println("***************** Info Classified *****************");
                                System.out.println(classifierSetences.getclassifierScores());
                                System.out.println("");
                            }

                        } catch (Exception ex) {
                            System.out.println("Error...");
                        }
                        try {
                            sentenceTask.setSentencesBehavior(new SentenceRenditionBehavior());
                            renditionSentences = sentenceTask.generateSentences(content);
                            if (renditionSentences != null && !"".equals(renditionSentences.getSentences())) {
                                StoreUtil.storeSentencesRenditionS3(renditionSentences.getSentences(), "key");
                            }

                        } catch (Exception ex) {
                            System.out.println("Error... in rendition");
                        }

                    }
                }
            }

        } catch (Exception ex) {
        }
    }

    /**
     * Metodo que clasifica informacion a partir de un 80 leg que esta en el
     * sistema local de archivos
     *
     * @param file
     */
    public static void classifyingSentencesFromEightyLocalFile(String file) {

        File fileToProcess = new File(file);
        HashMap<String, byte[]> results;
        OutputTask sentenceTask = new SentenceExtractionProcess();
        BlackList blackList = new BlackList();
        ClassifierInfo classifierSentences;
        try {
            ByteArrayInputStream bai = new ByteArrayInputStream(TestUtil.readBytes(fileToProcess));
            try {
                results = EightyLegResults.readFile(bai);
                Set<String> keySet = results.keySet();
                System.out.println("Number of htmls : " + keySet.size());
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            if (!blackList.isInBlackList(_key)) {
                                try {
                                    System.out.println("Url working on: " + _key);
                                    sentenceTask.setSentencesBehavior(new SentenceExtractorBehavior());
                                    classifierSentences = sentenceTask.generateSentences(content);
                                    if (classifierSentences != null && !"".equals(classifierSentences.getSentences()) && !"".equals(classifierSentences.getTitle()) && classifierSentences.getclassifierScores() != null) {
                                        System.out.println("***************** Info Classified *****************");
                                        System.out.println("Title: " + classifierSentences.getTitle());
                                        System.out.println("\nSentences: " + classifierSentences.getSentences());
                                        System.out.println("\nScore: " + classifierSentences.getclassifierScores());
                                        StoreUtil.storeDynamoNodeDocumentTemp(classifierSentences.getclassifierScores(), "key", classifierSentences.getTitle());
                                    }
                                } catch (Exception ex) {
                                    System.out.println("Error...");
                                }
                            } else {
                                System.out.println("Url: " + _key + " Banned...");
                            }
                        }
                    }
                }


            } catch (Exception ex) {
                System.out.println("Error... " + ex.getMessage());
            }
        } catch (IOException ex) {
            System.out.println("Error... " + ex.getMessage());
        }


    }

    /**
     * Metodo que obtiene los htmls a partir de un 80 leg file local y los
     * almacena en una carpeta
     */
    public static void gettingHtmlsFromEightyLegFile() {
        System.out.println("Generating htmls in local");
        String inputPath = "/home/andy/Descargas/550534_555411_a_10.80";
        String outputPath = "/home/andres/Downloads/bad";
        File fileToProcess = new File(inputPath);
        try {
            ByteArrayInputStream bai = new ByteArrayInputStream(TestUtil.readBytes(fileToProcess));
            try {
                HashMap<String, byte[]> results = EightyLegResults.readFile(bai);
                Set<String> keySet = results.keySet();
                System.out.println("Number of htmls : " + keySet.size());
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            byte[] byteContent = content.getBytes("UTF-8");
                            String htmlContent = ParserProxy.parse(results.get(_key));
                            String title = ParserProxy.getTitleFromFile(byteContent);
                            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(_key.toString());
                            TestUtil.write(content, outputPath, md5);
                        }
                    } else {
                        System.out.println("Exception: The content for the url: " + _key + " is null or empty.");
                    }
                }
                System.out.println("Files generated...");


            } catch (Exception ex) {
                System.out.println("Error... " + ex.getMessage());
            }

        } catch (IOException ex) {
            System.out.println("Error... " + ex.getMessage());
        }
    }

    /**
     * Metodo que a partir de un archivo de texto con las rutas de los 80 leg
     * files los descarga del s3 y obtiene el contenido de estos, incluyendo las
     * sentencias pero no los clasifica
     */
    public static void generatingSentencesLocalFromS3() {
        String inputPath = "/home/andy/Descargas/miPrueba.txt";
        BringFilesFromS3 bfs = new BringFilesFromS3();
        HashMap<String, byte[]> results;
        File file = new File(inputPath);
        String docsToGenerated = getContentFromFile(file);
        String[] legs = docsToGenerated.split("\n");
        for (int i = 0; i < legs.length; i++) {
            try {
                results = bfs.getZipContentFromS3(legs[i]);
                if (results != null) {
                    Set<String> keySet = results.keySet();
                    for (String _key : keySet) {
                        if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                            String content = new String((byte[]) results.get(_key), "UTF-8");
                            if ((content != null) && (!content.trim().isEmpty())) {
                                byte[] byteContent = content.getBytes("UTF-8");
                                String htmlContent = ParserProxy.parse(byteContent);
                                String title = ParserProxy.getTitleFromFile(byteContent);
                                if ((htmlContent != null) && (!htmlContent.trim().isEmpty())) {
                                    System.out.println("Getting Sentences");
                                    String sentences = null;
                                    try {
                                        System.out.println("***************** Calling Reverb *****************");
                                        sentences = SentenceExtractorProxy.extractFrom(content, true);
                                        if ((sentences != null) && (!"".equals(sentences))) {
                                            //TODO:Store the sentences
                                        }
                                        System.out.println("***************** Ending Reverb *****************");
                                    } catch (Exception ex) {
                                        Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
            }
        }
    }

    private static boolean saveImage(BufferedImage image, String name) throws IOException {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("image/jpeg");
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bao);
        ByteArrayInputStream bai = new ByteArrayInputStream(bao.toByteArray());
        boolean result = StoreUtil.storeS3(bai, name + ".jpg", objectMetadata);
        return result;
    }

    public static void testTitle() {
        System.out.println("Testing titles...");
        String inputPath = "/home/andres/Downloads/maloTemp.html";
        File fileToProcess = new File(inputPath);
        try {
            byte[] byteContent = TestUtil.readBytes(fileToProcess);
            String content = new String(byteContent, "UTF-8");
            String hmtlContent = ParserProxy.parse(byteContent);
            String title = ParserProxy.getTitleFromFile(byteContent);
            System.out.println("Finish...");
        } catch (Exception ex) {
            System.out.println("Error... " + ex.getMessage());
        }
    }

    public static void getTitleRegularExpression() {
        System.out.println("Generating htmls in local");
        String inputPath = "/home/andres/Downloads/451723_455432_a_5850.80";
        String title = "";
        String outputPath = "/home/andres/Downloads/bad";
        File fileToProcess = new File(inputPath);
        try {
            ByteArrayInputStream bai = new ByteArrayInputStream(TestUtil.readBytes(fileToProcess));
            try {
                HashMap<String, byte[]> results = EightyLegResults.readFile(bai);
                Set<String> keySet = results.keySet();
                System.out.println("Number of htmls : " + keySet.size());

                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            byte[] byteContent = content.getBytes("UTF-8");
                            Pattern p = Pattern.compile("<head>.*?<title>(.*?)</title>.*?</head>", Pattern.DOTALL);
                            Matcher m = p.matcher(content);
                            while (m.find()) {
                                title = m.group(1);
                                System.out.println("testing...");
                            }
                            System.out.println("Title: " + title);
                            String htmlContent = ParserProxy.parse(results.get(_key));
                            String titleTemp = ParserProxy.getTitleFromFile(byteContent);
                            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(_key.toString());
                            TestUtil.write(content, outputPath, md5);
                        }
                    } else {
                        System.out.println("Exception: The content for the url: " + _key + " is null or empty.");
                    }
                }
                System.out.println("Files generated...");
            } catch (Exception ex) {
                System.out.println("Error... " + ex.getMessage());
            }

        } catch (IOException ex) {
            System.out.println("Error... " + ex.getMessage());
        }
    }

    public static void TestingJsoup() {
        System.out.println("Testing Jsoup");
        File input;
        try {
            input = new File("/home/andres/Downloads/malo01.html");
            String html = Jsoup.connect("url").get().html();
            Document doc = Jsoup.parse(input, "UTF-8");
            Elements elemets = doc.getElementsByTag("META");
            String temp = doc.getElementsByTag("title").text();
            Element link = doc.select("a").first();

            String title = getMetaTag(doc, "title");
            String description = getMetaTag(doc, "description");
            System.out.println("Finish...");
        } catch (IOException ex) {
        }

    }

    public static String getMetaTag(Document document, String attr) {
        Elements elements = document.select("meta[name=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) {
                return s;
            }
        }
        elements = document.select("meta[property=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) {
                return s;
            }
        }
        return null;
    }

    public static String getContentFromFile(File file) {
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        DataInputStream dis = null;
        StringBuilder sb = new StringBuilder();
        try {
            fis = new FileInputStream(file);
            // Here BufferedInputStream is added for fast reading.
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            // dis.available() returns 0 if the file does not have more lines.
            while (dis.available() != 0) {
                // this statement reads the line from the file and print it to
                // the console.
                //System.out.println(dis.readLine());
                sb.append(dis.readLine());
                sb.append("\n");
            }

            // dispose all the resources after using them.
            fis.close();
            bis.close();
            dis.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
    //    public static void TestingImages(String line) {
//        ImageInfo ii = null;
//        OutputTask imageTask = new ImageExtractionProcess();
//        BringFilesFromS3 bfs = new BringFilesFromS3();
//        HashMap<String, byte[]> results;
//
//        try {
//            results = bfs.getZipContentFromS3(line);
//            if (results != null) {
//                Set<String> keySet = results.keySet();
//                for (String _key : keySet) {
//                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
//                        String content = new String((byte[]) results.get(_key), "UTF-8");
//                        if ((content != null) && (!content.trim().isEmpty())) {
//                          i = imageTask.generateImages(_key);
//                            if (ii != null) {
//                                //site image
//                                BuilderImage builder = new BuilderImage(ii.getImage(),
//                                        BuilderImage.SITE_IMAGE);
//                                BufferedImage image = builder.build();
//
//                                //digest image
//                                BuilderImage builderDigest = new BuilderImage(ii.getImage(), BuilderImage.DIGEST_IMAGE);
//                                BufferedImage imageDigest = builderDigest.build();
//                                try {
//                                    System.out.println("SAVE IMAGE IN S3: " + saveImage(image, DigestUtils.getMD5(_key.toString())));
//                                    System.out.println("SAVE IMAGE_MCROP IN S3: " + saveImage(imageDigest, DigestUtils.getMD5(_key.toString()) + "_mcrop"));
//                                } catch (Exception e) {
//                                    System.out.println("Error wtiting images on S3: " + e.getMessage());
//                                }
//                            }
//                        }
//                    }
//                }
//
//            }
//        } catch (Exception ex) {
//        }
//    }
}
