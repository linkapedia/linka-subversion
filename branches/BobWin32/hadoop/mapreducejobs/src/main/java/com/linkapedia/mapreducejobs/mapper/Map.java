package com.linkapedia.mapreducejobs.mapper;


import com.linkapedia.mapreducejobs.util.BlackList;
import com.linkapedia.mapreducejobs.util.BringFilesFromS3;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Map extends Mapper<LongWritable, Text, Text, Text> {

    private Text url = new Text();
    private Text html = new Text();

    @Override
    public void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
        String line = value.toString();
        HashMap<String, byte[]> results;
        BlackList blackList = new BlackList();
        BringFilesFromS3 bfs = new BringFilesFromS3();
        try {
            results = bfs.getZipContentFromS3(line);
            if (results != null) {
                Set<String> keySet = results.keySet();
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            if(!blackList.isInBlackList(_key)){                           
                            this.url.set(_key);
                            this.html.set(content);
                            System.out.println("Sending {" + content.length() + "} chars for url: " + _key);
                            context.write(this.url, this.html);
                            }
                        } else {
                            System.err.println("There is no content to send to the reducer for url: " + _key);
                        }
                    } else {
                        System.out.println("Exception: The content for the url: " + _key + " is null or empty.");
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error: " + e.getLocalizedMessage());
        }
    }
}