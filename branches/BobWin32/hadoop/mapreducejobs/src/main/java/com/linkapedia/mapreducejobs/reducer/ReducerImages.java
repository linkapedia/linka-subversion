package com.linkapedia.mapreducejobs.reducer;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.intellisophic.linkapedia.generic.utils.DigestUtils;
import com.linkapedia.mapreducejobs.output.ImageExtractionProcess;
import com.linkapedia.mapreducejobs.output.OutputTask;
import com.linkapedia.mapreducejobs.util.StoreUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


/**
 *
 * @author andres
 */
public class ReducerImages extends Reducer<Text, Text, NullWritable, NullWritable> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, NullWritable, NullWritable>.Context context) throws IOException, InterruptedException {
//        OutputTask imageTask = new ImageExtractionProcess();
//        ImageInfo ii = null;
//
//        for (Text val : values) {
//            System.out.println("Received {" + val.toString().length() + "} chars for url: " + key.toString());
//
//            ii = imageTask.generateImages(key.toString());
//           if (ii != null) {
//                //site image
//                BuilderImage builder = new BuilderImage(ii.getImage(),
//                        BuilderImage.SITE_IMAGE);
//                BufferedImage image = builder.build();
//
//                //digest image
//                BuilderImage builderDigest = new BuilderImage(ii.getImage(), BuilderImage.DIGEST_IMAGE);
//                BufferedImage imageDigest = builderDigest.build();
//                try {
//                    System.out.println("SAVE IMAGE IN S3: " + saveImage(image, DigestUtils.getMD5(key.toString())));
//                    System.out.println("SAVE IMAGE_MCROP IN S3: " + saveImage(imageDigest, DigestUtils.getMD5(key.toString()) + "_mcrop"));
//                } catch (Exception e) {
//                    System.out.println("Error wtiting images on S3: " + e.getMessage());
//                }
//            }
//        }
    }
    
       /**
     *
     * @param image
     * @param name
     * @return
     * @throws IOException
     */
    private boolean saveImage(BufferedImage image, String name) throws IOException {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("image/jpeg");
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bao);
        ByteArrayInputStream bai = new ByteArrayInputStream(bao.toByteArray());
        boolean result = StoreUtil.storeS3(bai, name + ".jpg", objectMetadata);
        return result;
    }
}