package com.linkapedia.mapreducejobs.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class StringUtils {

    private static final Logger log = LoggerFactory.getLogger(StringUtils.class);
    private static String[] defaultFilters = new String[]{" ,||,", " \\.||.", " n't||n't", " \",||\",", " \\?||?"};
    protected static final String DEFAULT_SEPARATOR = "\\|\\|";

    public static String escapeString(String strToEscape, String[] filters) {
        log.debug("escapeString(String, String[])");
        String filterSplit[] = null;
        if (strToEscape == null || strToEscape.trim().isEmpty()) {
            return null;
        }
        for (String filter : filters) {
            filterSplit = filter.split(DEFAULT_SEPARATOR);
            strToEscape = strToEscape.replaceAll(filterSplit[0], filterSplit[1]);
        }
        return strToEscape;
    }

    public static String escapeString(String strToEscape) {
        log.debug("escapeString(String)");
        return escapeString(strToEscape, defaultFilters);
    }

    public static String setTitle(String sentences) {

        StringBuilder titleSb = new StringBuilder();
        String[] words = sentences.split(" ");
        for (int i = 0; i < words.length; i++) {
            titleSb.append(words[i]);
            titleSb.append(" ");
            if (i == 8) {
                break;
            }
        }
        return titleSb.toString().trim();

    }

    public static String getTitle(String content) {
        StringBuilder title = new StringBuilder();
        Pattern p = Pattern.compile("<head>.*?<title>(.*?)</title>.*?</head>", Pattern.DOTALL);
        Matcher m = p.matcher(content);
        while (m.find()) {
            title.append(m.group(1));
        }
        String[] titleLines = title.toString().split("(\r\n|\n)");
        title.setLength(0);
        for (String line : titleLines) {
            title.append(line.trim());
        }
        return title.toString();
    }
}
