/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreduce.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author billbravo
 */
public class ZipUtils {

    public static final int BUFFER_SIZE = 2048;

    public static void extractToOutputStream(InputStream source, OutputStream destination) throws IOException {
        BufferedOutputStream bos;
        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(source));

        while (( zis.getNextEntry()) != null) {
            int count;
            byte data[] = new byte[BUFFER_SIZE];
            bos = new BufferedOutputStream(destination, BUFFER_SIZE);
            while ((count = zis.read(data, 0, BUFFER_SIZE)) != -1) {
                bos.write(data, 0, count);
            }
            bos.flush();
            bos.close();
        }
        
        zis.close();
    }
}
