package com.linkapedia.mapreducejobs.mapper;


import com.linkapedia.mapreduce.utils.ZipUtils;
import com.linkapedia.mapreducejobs.util.EightyLegResults;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class EightyLegMap
        extends Mapper<NullWritable, BytesWritable, Text, Text> {

    private Text url = new Text();
    private Text html = new Text();

    @Override
    public void map(NullWritable key, BytesWritable value, Context context)
            throws IOException, InterruptedException {
        HashMap<String, byte[]> results;
        ByteArrayInputStream bis = new ByteArrayInputStream(value.getBytes());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        try {
           ZipUtils.extractToOutputStream(bis, bos);
           IOUtils.closeQuietly(bis);
           bis = new ByteArrayInputStream(bos.toByteArray());
           results = EightyLegResults.readFile(bis); 
            
            if (results != null) {
                Set<String> keySet = results.keySet();
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            this.url.set(_key);
                            this.html.set(content);
                            System.out.println("Sending {" + content.length() + "} chars for url: " + _key);
                            context.write(this.url, this.html);
                        } else {
                            System.err.println("There is no content to send to the reducer for url: " + _key);
                        }
                    } else {
                        System.err.println("Exception: The content for the url: " + _key + " is null or empty.");
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error: " + e.getLocalizedMessage());
        } finally {
            IOUtils.closeQuietly(bis);
            IOUtils.closeQuietly(bos);
        }
    }
}