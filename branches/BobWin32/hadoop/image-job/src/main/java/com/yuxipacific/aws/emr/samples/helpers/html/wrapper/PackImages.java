/**
 * @author andres
 * @verion 0.1 BETA
 * <p/>
 * manage a list<ImageInfo> wrapper
 */
package com.yuxipacific.aws.emr.samples.helpers.html.wrapper;

import com.yuxipacific.aws.emr.samples.helpers.html.beans.ImageInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class PackImages implements List<ImageInfo>, Cloneable {

    private List<ImageInfo> elements;

    public PackImages() {
        this.elements = new ArrayList<ImageInfo>();
    }

    public PackImages(Collection<ImageInfo> elements) {
        this.elements = new ArrayList<ImageInfo>(elements);
    }

    public PackImages(List<ImageInfo> elements) {
        this.elements = elements;
    }

    public PackImages(ImageInfo... elements) {
        this(Arrays.asList(elements));
    }

    @Override
    public PackImages clone() {
        List<ImageInfo> contents = new ArrayList<ImageInfo>();
        for (ImageInfo e : elements) {
            contents.add(e.clone());
        }
        return new PackImages(contents);
    }

    //list methods
    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public Iterator<ImageInfo> iterator() {
        return elements.iterator();
    }

    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return elements.toArray(a);
    }

    @Override
    public boolean add(ImageInfo e) {
        return elements.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return elements.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return elements.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends ImageInfo> c) {
        return elements.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends ImageInfo> c) {
        return elements.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return elements.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return elements.retainAll(c);
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @Override
    public ImageInfo get(int index) {
        return elements.get(index);
    }

    @Override
    public ImageInfo remove(int index) {
        return elements.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return elements.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return elements.lastIndexOf(o);
    }

    @Override
    public ListIterator<ImageInfo> listIterator() {
        return elements.listIterator();
    }

    @Override
    public ListIterator<ImageInfo> listIterator(int index) {
        return elements.listIterator(index);
    }

    @Override
    public List<ImageInfo> subList(int fromIndex, int toIndex) {
        return elements.subList(fromIndex, toIndex);
    }

    @Override
    public ImageInfo set(int index, ImageInfo element) {
        return elements.set(index, element);
    }

    @Override
    public void add(int index, ImageInfo element) {
        elements.add(index, element);
    }
}
