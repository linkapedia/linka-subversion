/**
 * @author andres
 * @verion 0.1 BETA
 */
package com.yuxipacific.aws.emr.samples.helpers.io;

import com.yuxipacific.aws.emr.samples.helpers.html.beans.ImageInfo;
import com.yuxipacific.aws.emr.samples.helpers.html.wrapper.PackImages;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

public class UtilIO {

    private static final Logger log = Logger.getLogger(UtilIO.class);

    /**
     * write images into file system with correct format
     *
     * @param list
     * @param file
     * @param createFolders
     * @param format
     * @param quality : only to jpg format
     * @throws Exception
     */
    public static void writeImages(PackImages list, File file, boolean createFolders, String format, Float quality) throws Exception {
        log.debug("writeImages(PackImages list, File file, boolean, String, Float)");
        if (!file.exists()) {
            if (createFolders) {
                log.info("Creating directory to save the image");
                file.mkdirs();
            } else {
                log.error("The file(folder) not exits");
                return;
            }

        }
        if (!format.equalsIgnoreCase("PNG")
                && (!format.equalsIgnoreCase("JPG"))) {
            log.error("Not support format only JPG or PNG");
            return;
        }
        BufferedImage image;
        OutputStream os = null;
        for (ImageInfo ii : list) {
            try {
                os = new FileOutputStream(file.getAbsolutePath()
                        + File.separator + ii.getId() + "."
                        + format.toLowerCase());
                // save image with the correct format
                if (format.equalsIgnoreCase("JPG")) {
                    try {
                        image = UtilsTransform.convertToJPG(ii.getImage(),
                                quality.floatValue());
                    } catch (Exception e) {
                        image = UtilsTransform.convertToJPG(ii.getImage());
                    }
                    if (image != null) {
                        ImageIO.write(image, "jpg", os);
                    }
                } else if (format.equalsIgnoreCase("PNG")) {
                    image = UtilsTransform.convertToPNG(ii.getImage());
                    if (image != null) {
                        ImageIO.write(image, "png", os);
                    }
                }
                log.info("save image ok!");
            } catch (Exception e) {
                log.error("error save image");
            }
        }
    }

    /**
     *
     * @param image
     * @param file
     * @param createFolders
     * @param format
     * @param quality
     * @throws Exception
     */
    public static void writeImages(ImageInfo image, File file, boolean createFolders, String format, Float quality) throws Exception {
        PackImages list = new PackImages();
        list.add(image);
        writeImages(list, file, createFolders, format, quality);
    }

    /**
     *
     * @param image
     * @param file
     * @throws Exception
     */
    public static void writeImages(ImageInfo image, File file) throws Exception {
        writeImages(image, file, true, "jpg", null);
    }
}
