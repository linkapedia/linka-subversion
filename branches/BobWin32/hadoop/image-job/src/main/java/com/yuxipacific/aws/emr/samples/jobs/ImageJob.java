package com.yuxipacific.aws.emr.samples.jobs;

import com.yuxipacific.aws.emr.samples.helpers.io.ImgBytesWritable;
import com.yuxipacific.aws.emr.samples.jobs.mapper.Map;
import com.yuxipacific.aws.emr.samples.jobs.reducer.ReduceImage;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class ImageJob
{
  public static void main(String[] args)
    throws Exception
  {
    Configuration conf = new Configuration();

    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
    if (otherArgs.length != 3) {
      System.err.println("Usage: ImageJob <in> <out>");
      System.exit(2);
    }
    Job job = new Job(conf, "ImageJob");
    job.setJarByClass(ImageJob.class);

    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(Text.class);

    job.setOutputKeyClass(NullWritable.class);
    job.setOutputValueClass(BytesWritable.class);

    job.setMapperClass(Map.class);
    job.setReducerClass(ReduceImage.class);

    FileInputFormat.addInputPath(job, new Path(otherArgs[1]));
    FileOutputFormat.setOutputPath(job, new Path(otherArgs[2]));

    MultipleOutputs.addNamedOutput(job, "seq", SequenceFileOutputFormat.class, NullWritable.class, ImgBytesWritable.class);

    job.waitForCompletion(true);
  }
}