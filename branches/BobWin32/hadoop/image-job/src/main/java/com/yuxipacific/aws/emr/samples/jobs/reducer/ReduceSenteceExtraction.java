package com.yuxipacific.aws.emr.samples.jobs.reducer;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.intellisophic.linkapedia.amazon.services.S3.bo.ManageBucket;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeDocumentTemp;
import com.intellisophic.linkapedia.api.handlers.DocumentNodeAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeDocumentAPIHandler;
import com.intellisophic.linkapedia.generic.utils.DigestUtils;
import com.yuxipacific.aws.emr.samples.helpers.ParserClassifierFile;
import com.yuxipacific.aws.emr.samples.helpers.reverb.SentenceExtractorProxy;
import com.yuxipacific.aws.emr.samples.helpers.sockets.SocketClient;
import com.yuxipacific.aws.emr.samples.helpers.tika.FileUtils;
import com.yuxipacific.aws.emr.samples.helpers.tika.ParserProxy;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ReduceSenteceExtraction extends Reducer<Text, Text, NullWritable, Text> {

    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("classifier/config");
    protected static final String SENTENCE_BUCKET = systemConfig.getString("sentencejob.bucket");

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        System.out.println("Inside the reducer.");
        ManageBucket mb = new ManageBucket(SENTENCE_BUCKET);
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentType("text/plain");
        boolean dataReceived = false;
        for (Text val : values) {
            dataReceived = true;
            System.out.println(new StringBuilder().append("Received {").append(val.toString().length()).append("} chars for url: ").append(key.toString()).toString());
            String html = val.toString();
            if ((!"".equals(html)) && (html != null)) {
                StringBuilder sb = new StringBuilder();
                byte[] htmlContent = html.getBytes("UTF-16LE");
                String title = ParserProxy.getTitleFromFile(htmlContent);
                String content = ParserProxy.parse(htmlContent);

                sb.append("Title: ");
                sb.append(title);
                sb.append("\n");
                sb.append(" Content: ");
                if ((title != null) && (!title.trim().isEmpty()) && (content != null) && (!content.trim().isEmpty())) {
                    try {
                        System.out.println("Getting Sentences");
                        String sentences = SentenceExtractorProxy.extractFrom(content, true);
                        if ((sentences != null) && (!"".equals(sentences))) {
                            sb.append(sentences);
                            InputStream inputStream = FileUtils.convertStringToStream(sb.toString());

                            System.out.println("Call the Classifier");
                            try {
                                String classifierInfoResponse = SocketClient.sendAndReciveData(sentences);
                                if (classifierInfoResponse != null && !"".equals(classifierInfoResponse)) {
                                    System.out.println("Sending to dynamo");


                                    List<NodeDocumentTemp> nodeDocuments = ParserClassifierFile.classifierInfoToNodeDocumentTemp(classifierInfoResponse, title, key.toString(), DigestUtils.getMD5(key.toString()));
                                    if (nodeDocuments != null && !nodeDocuments.isEmpty()) {
                                        System.out.println("NodeDocuments Transformed: " + nodeDocuments.size());
                                        for (NodeDocumentTemp node : nodeDocuments) {
                                            System.out.println("Save NodeDocument{" + node.getNodeID() + "} in Dynamo");
                                            NodeDocumentAPIHandler.saveNodeDocumentTemp(node);
                                        }
                                    }

//                                    List<NodeDocument> nodeDocuments = ParserClassifierFile.classifierInfoToNodeDocument(classifierInfoResponse, title, key.toString(), DigestUtils.getMD5(key.toString()));
//                                    if (nodeDocuments != null && !nodeDocuments.isEmpty()) {
//                                        System.out.println("NodeDocuments Transformed: " + nodeDocuments.size());
//                                        for (NodeDocument node : nodeDocuments) {
//                                            System.out.println("Save NodeDocument{" + node.getNodeID() + "} in Dynamo");
//                                            NodeDocumentAPIHandler.saveNodeDocument(node);
//                                        }
//                                    }
//
//                                    List<DocumentNode> documentNodes = ParserClassifierFile.classifierInfoToDocumentNode(classifierInfoResponse, title, key.toString(), DigestUtils.getMD5(key.toString()));
//                                    if (documentNodes != null) {
//                                        System.out.println("DocumentNodes Transformed: " + documentNodes.size());
//                                        for (DocumentNode node : documentNodes) {
//                                            System.out.println("Save DocumentNode{" + node.getNodeID() + "} in Dynamo");
//                                            DocumentNodeAPIHandler.saveDocumentNode(node);
//                                        }
//                                    }

                                }
                            } catch (UnknownHostException e) {
                                System.err.println(new StringBuilder().append("Host not found ").append(e.getMessage()).toString());
                            } catch (Exception e) {
                                System.err.println(new StringBuilder().append("Error Classifing ").append(e.getMessage()).toString());
                            }

                            try {
                                boolean storeS3;
                                storeS3 = mb.saveData(new StringBuilder().append(systemConfig.getString("sentencejob.path.output")).append(DigestUtils.getMD5(key.toString())).toString(), inputStream, meta);
                                if (storeS3 == true) {
                                    context.write(NullWritable.get(), new Text(sentences));
                                } else {
                                    System.err.println(new StringBuilder().append("can not store sentence file in s3 ").append(key.toString()).toString());
                                }

                            } catch (Exception ex) {
                                System.err.println(new StringBuilder().append("Error Storing sentence file in S3: ").append(ex.getMessage()).toString());
                            }

                        } else {
                            System.err.println(new StringBuilder().append("There is not sentences for the url: ").append(key.toString()).toString());
                        }
                    } catch (Exception ex) {
                        System.err.println(new StringBuilder().append("Error creating the title file: ").append(ex.getMessage()).toString());
                    }
                } else {
                    System.err.println(new StringBuilder().append("There is not title/content for the url: ").append(key.toString()).toString());
                }
            } else {
                System.err.println(new StringBuilder().append("There is not html for the url: ").append(key.toString()).toString());
            }
        }
        if (!dataReceived) {
            System.err.println("No data received from Mapper.");
        }
    }
}