/**
 * @author andres
 * @verion 0.1 BETA
 */
package com.yuxipacific.aws.emr.samples.helpers.html.filters;

import java.util.ArrayList;
import java.util.List;

public class PreFilter {

    //num max of images to return
    private int num;
    //these words must be within <img ...
    private List<String> wordsIn;
    //these words should not be within <img ...
    private List<String> wordsOut;

    public PreFilter() {
        num = 0;
        wordsIn = new ArrayList<String>();
        wordsOut = new ArrayList<String>();
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public List<String> getWordsIn() {
        return wordsIn;
    }

    public void setWordsIn(List<String> wordsIn) {
        this.wordsIn = wordsIn;
    }

    public List<String> getWordsOut() {
        return wordsOut;
    }

    public void setWordsOut(List<String> wordsOut) {
        this.wordsOut = wordsOut;
    }
}
