package com.yuxipacific.aws.emr.samples.helpers.html.beans;

import java.awt.image.BufferedImage;
import org.apache.log4j.Logger;

public class ImageInfo implements Cloneable {

    private static final Logger log = Logger.getLogger(ImageInfo.class);
    private String id;
    private BufferedImage image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    @Override
    public ImageInfo clone() {
        log.debug("clone()");
        ImageInfo clone = null;
        try {
            clone = (ImageInfo) super.clone();
        } catch (CloneNotSupportedException e) {
            log.error("Clone method not supported {" + e.getLocalizedMessage() + "}");
        }
        return clone;
    }
}
