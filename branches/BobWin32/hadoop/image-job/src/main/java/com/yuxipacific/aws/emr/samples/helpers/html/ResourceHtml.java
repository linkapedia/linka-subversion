/**
 * @author andres
 * @verion 0.3 BETA
 * <p/>
 * manage image to html documents
 */
package com.yuxipacific.aws.emr.samples.helpers.html;

import com.yuxipacific.aws.emr.samples.helpers.html.beans.ImageInfo;
import com.yuxipacific.aws.emr.samples.helpers.html.filters.PreFilter;
import com.yuxipacific.aws.emr.samples.helpers.html.filters.UtilFilters;
import com.yuxipacific.aws.emr.samples.helpers.html.wrapper.PackImages;
import com.yuxipacific.utils.ImageUtils;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ResourceHtml {

    private static final Logger log = Logger.getLogger(ResourceHtml.class);
    private static final int TIMEOUT_READ = 30000;
    private static final int TIMEOUT_CONNECTION = 30000;
    public static final int TIME_OUT_JSOUP = 30000;

    /**
     * parser InputStream in document
     * <p/>
     * @param is
     * @param charset
     * @param baseUri
     * @param pf
     * @return
     * @throws Exception
     */
    public static PackImages getHtmlImages(InputStream is, String charset,
            String baseUri, PreFilter pf) throws Exception {
        log.debug("getHtmlImages(InputStream, String ,String, PreFilter)");
        if ((is == null) || (pf == null)) {
            throw new InvalidParameterException("Some parameters are NULL");
        }
        PackImages list = null;
        Document doc;
        try {
            doc = Jsoup.parse(is, charset, baseUri);
            list = getPackImagesFromDoc(doc, pf);
        } catch (Exception e) {
            log.error("Error parser page from InputStream: " + e.getMessage());
        }
        return list;
    }

    /**
     * parser file in document
     * <p/>
     * @param file
     * @param charset
     * @param baseUri
     * @param pf
     * @return
     * @throws Exception
     */
    public static PackImages getHtmlImages(File file, String charset, String baseUri, PreFilter pf) throws Exception {
        log.debug("getHtmlImages(File, String ,String, PreFilter)");
        if ((file == null) || (pf == null)) {
            throw new InvalidParameterException("Some parameters are NULL");
        }
        PackImages list = null;
        Document doc;
        try {
            doc = Jsoup.parse(file, charset, baseUri);
            list = getPackImagesFromDoc(doc, pf);
        } catch (Exception e) {
            log.error("Error parser page from file: " + e.getMessage());
        }
        return list;
    }

    /**
     * parser url in document
     * <p/>
     * @param url
     * @param pf
     * @return
     * @throws Exception
     */
    public static PackImages getHtmlImages(URL url, PreFilter pf)
            throws Exception {
        log.debug("getHtmlImages(URL, String ,String, PreFilter)");
        if ((url == null) || (pf == null)) {
            throw new InvalidParameterException("Some parameters are NULL");
        }
        UtilFilters utilFilter = new UtilFilters();
        PackImages list = null;
        Document doc;
        try {
            // get elements
            doc = Jsoup.connect(url.toExternalForm()).userAgent("Mozilla")
                    .timeout(TIME_OUT_JSOUP).get();
            // if the page need redirect
            Elements elemets = doc.getElementsByTag("META");
            String meta;
            if ((elemets != null) && (!elemets.isEmpty())) {
                for (Element el : elemets) {
                    meta = el.attr("HTTP-EQUIV");
                    if ((meta != null) && (meta.equalsIgnoreCase("REFRESH"))) {
                        url = new URL(utilFilter.getRedirectURL(el));
                        doc = Jsoup.connect(url.toExternalForm())
                                .userAgent("Mozilla").get();
                        break;
                    }
                }
            }
            list = getPackImagesFromDoc(doc, pf);
        } catch (Exception e) {
            log.info("Error get page from url:" + e.getMessage() + " "
                    + url.toExternalForm());
            throw e;
        }
        return list;
    }

    /**
     * get images from a document already parser
     * <p/>
     * @param doc
     * @param pf
     * @return
     */
    private static PackImages getPackImagesFromDoc(Document doc, PreFilter pf)
            throws Exception {
        log.debug("getPackImagesFromDoc(Document, PreFilter)");
        PackImages list = new PackImages();
        UtilFilters utilFilter = new UtilFilters();
        Elements elements = doc.getElementsByTag("img");
        if (elements.isEmpty()) {
            throw new Exception("The html file not have images");
        }
        int num = pf.getNum();
        Map<String, URL> mapURLS = new LinkedHashMap<String, URL>();
        String urlImage = null;
        int i = 1;
        String elementString;
        // get the images from html
        log.info("Go to get images");
        for (Element element : elements) {
            try {
                // get absolute path of the image
                urlImage = element.attr("abs:src");
                elementString = element.toString();
                // filter
                if (utilFilter.wordsIn(pf, elementString)
                        && (utilFilter.wordsOut(pf, elementString))) {
                    mapURLS.put(urlImage, new URL(urlImage));
                }
                // get a limit number images (filter)
                if (num != 0) {
                    if (i == num) {
                        break;
                    }
                }
                i++;
            } catch (Exception e) {
                log.error("Error build url: " + e.getMessage() + " " + urlImage);
            }
        }
        if (mapURLS.isEmpty()) {
            throw new Exception("Any image to compare");
        }

        // get images in a BufferedImage
        BufferedImage bi;
        // this is to calculate time to get all images
        long n = System.currentTimeMillis();
        ImageInfo ii;
        InputStream is = null;
        HttpURLConnection con = null;
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] b = new byte[4000];
        int length;
        log.info("Get images in a BufferedImage");
        for (URL resource : mapURLS.values()) {
            try {
                bao.reset();
                con = (HttpURLConnection) resource.openConnection();
                con.setRequestProperty("Range", "bytes=" + 30 + "-");
                con.setReadTimeout(TIMEOUT_READ);
                con.setConnectTimeout(TIMEOUT_CONNECTION);
                // my user agent (use any user agent)
                con.setRequestProperty(
                        "User-Agent",
                        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.168 Safari/535.19");

                con.connect();
                System.out.println("ALGO: " + con.getResponseMessage());
                ImageInputStream in = ImageIO.createImageInputStream(con.getInputStream());
                try {
                    final Iterator readers = ImageIO.getImageReaders(in);
                    if (readers.hasNext()) {
                        ImageReader reader = (ImageReader) readers.next();
                        try {
                            reader.setInput(in);
                            System.out.println("HEIGHT: " + reader.getHeight(0));
                            System.out.println("WIDTH: " + reader.getWidth(0));

                        } finally {
                            reader.dispose();
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error: "+e);
                } finally {
                    if (in != null) {
                        in.close();
                    }
                }





                ImageUtils imageUtil = new ImageUtils(con.getInputStream());




                System.out.println("HEIGHT: " + imageUtil.getHeight());
                System.out.println("WIDTH: " + imageUtil.getWidth());
                System.out.println("MIME_TYPE: " + imageUtil.getMimeType());


                /*is = con.getInputStream();
                
                
                 while ((length = is.read(b)) != -1) {
                 bao.write(b, 0, length);
                 }
                 bi = ImageIO.read(new ByteArrayInputStream(bao.toByteArray()));
                 if (bi != null) {
                 ii = new ImageInfo();
                 ii.setId(UUID.randomUUID().toString() + System.nanoTime());
                 ii.setImage(bi);
                 list.add(ii);
                 }
                 log.info("Get image ok! " + resource.toString());*/
            } catch (Exception e) {
                log.error("Get image error! " + e.getMessage()
                        + resource.toString());
            } finally {
                // close resources
                if (is != null) {
                    is.close();
                }
                if (con != null) {
                    con.disconnect();
                }
            }
        }
        log.info("TIME IN SECONDS (HTML IMAGES): " + (System.currentTimeMillis() - n) / 1000);
        return list;
    }
}
