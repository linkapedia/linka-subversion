/**
 * @author andres
 * @verion 0.3 BETA
 * <p/>
 * some method to manage filters
 */
package com.yuxipacific.aws.emr.samples.helpers.html.filters;

import com.yuxipacific.aws.emr.samples.helpers.html.beans.ImageInfo;
import com.yuxipacific.aws.emr.samples.helpers.html.wrapper.PackImages;
import java.awt.image.BufferedImage;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;

public class UtilFilters {

    private static final Logger log = Logger.getLogger(UtilFilters.class);

    /**
     * get url in meta tag to redirect page if is necessary
     * <p/>
     * @param el
     * @return
     */
    public String getRedirectURL(Element el) {
        log.debug("getRedirectURL(Element)");
        String content = el.attr("content");
        String url = "";
        Pattern pattern = Pattern.compile("\\d+\\;url\\=(.*)");
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            url = matcher.group(1);
        }
        return url;
    }

    /**
     * these words have to be in text <img ... <p/>
     * <
     * p/> < p/> @param list
     * @param resource
     * @return
     */
    public boolean wordsIn(PreFilter pf, String resource) {
        log.debug("wordsIn(PreFilter, String)");
        List<String> list = pf.getWordsIn();
        if ((list == null) || (list != null) && (list.isEmpty())) {
            return true;
        }
        boolean result = true;
        for (String word : list) {
            if (!StringUtils.containsIgnoreCase(resource, word)) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * these words do not have to be in text <img ... <p/>
     * <
     * p/> < p/> @param list
     * @param resource
     * @return
     */
    public boolean wordsOut(PreFilter pf, String resource) {
        log.debug("wordsOut(PreFilter, String)");
        List<String> list = pf.getWordsOut();
        if ((list == null) || (list != null) && (list.isEmpty())) {
            return true;
        }
        boolean result = true;
        for (String word : list) {
            if (StringUtils.containsIgnoreCase(resource, word)) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * get a better BufferedImage with some filters
     * <p/>
     * @param list
     * @param filter
     * @return
     */
    public ImageInfo getBetterImageFilter(PackImages list, FilterImages filter) throws Exception {
        log.debug("getBetterImageFilter(PackImages list, FilterImages filter)");
        if (list == null || filter == null) {
            throw new InvalidParameterException("List or filter = NULL");
        }
        int height, width;
        double divRatio = 0;
        double divRatio1 = 0;
        PackImages bettersImages = new PackImages();
        boolean ratioCompare = (filter.getBetter_ratiox() != 0 && filter.getBetter_ratioy() != 0);
        if (ratioCompare) {
            divRatio = filter.getBetter_ratiox() / filter.getBetter_ratioy();
            divRatio1 = filter.getBetter_ratioy() / filter.getBetter_ratiox();
        }
        BufferedImage image;
        for (ImageInfo ii : list) {
            try {
                image = ii.getImage();
                if (image == null) {
                    continue;
                }
                height = image.getHeight();
                width = image.getWidth();
                double div = (double) width / (double) height;
                boolean maxHeight = (filter.getMax_height() == 0 ? true
                        : (height > filter.getMax_height() ? false : true));
                boolean minHeight = (filter.getMin_height() == 0 ? true
                        : (height < filter.getMin_height() ? false : true));
                boolean maxWidth = (filter.getMax_width() == 0 ? true
                        : (width > filter.getMax_width() ? false : true));
                boolean minWidth = (filter.getMin_width() == 0 ? true
                        : (width < filter.getMin_width() ? false : true));
                // compare if the image pass filter to size
                if (maxHeight && minHeight && maxWidth && minWidth) {
                    // compare the better image ratio
                    if (ratioCompare) {
                        if ((div >= divRatio1) && (div <= divRatio)) {
                            bettersImages.add(ii);
                        }
                    } else {
                        bettersImages.add(ii);
                    }
                }
            } catch (Exception e) {
                log.error("Error getting image: " + e.getMessage());
            }
        }
        return getLargestImage(bettersImages);
    }

    /**
     * get the image more big
     * <p/>
     * @param pi
     * @return
     */
    private ImageInfo getLargestImage(PackImages pi) {
        log.info("--Get largest image from the list--");
        ImageInfo better = null;
        boolean init = true;
        int sum;
        int may = 0;
        for (ImageInfo ii : pi) {
            sum = ii.getImage().getHeight() + ii.getImage().getWidth();
            if (init) {
                better = ii;
                may = sum;
                init = false;
            } else {
                if (sum > may) {
                    better = ii;
                    may = sum;
                }
            }
        }
        return better;
    }
}
