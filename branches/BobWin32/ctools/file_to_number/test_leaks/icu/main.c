#include "trans.h"
const char *REMOVE_ACCENTS = "NFD;[:Nonspacing Mark:] Remove;NFC;Lower";
void test_memory_buffer()
{
	const char*_test="ñoño carlos prueba";
	BUFFER_STATUS _status;
	u16_char_buffer *_buffer=create_u16_char_buffer(_test,&_status);
	delete_u16_char_buffer(_buffer);
}
void test_memory_transliterator()
{
	const char *_test="ñoño que pasà";
	BUFFER_STATUS _status;
	TRANSFORMATION_STATUS _transformation_status;
	u16_char_buffer *_id=create_u16_char_buffer(REMOVE_ACCENTS,&_status);
	u16_char_buffer *_test_buffer=create_u16_char_buffer(_test,&_status);
	transformation *_transformation=create_transformation(_id,&_transformation_status);
	transform(_transformation,_test_buffer,&_transformation_status);
	delete_transformation(_transformation);
	delete_u16_char_buffer(_test_buffer);	
}
int main(int argc,char **argv)
{
	test_memory_buffer();
	test_memory_transliterator();
	u_cleanup();
	return 0;
}

