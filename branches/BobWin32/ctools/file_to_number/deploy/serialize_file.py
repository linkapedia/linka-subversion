#!/usr/bin/python
import struct
from sets import Set
DATA_DIRECTORY="static_data/"
def read_file(file_name):
	fd=open(file_name,"r")
	read_lines=fd.readlines()
	fd.close()
	new_read_lines=[line.strip() for line in read_lines if(len(line.strip())>0)]
	return new_read_lines
def serialize_words_file():
	read_lines=read_file(DATA_DIRECTORY+"words.txt")	
	fd=open("words.data","wb")
	word_in_file=len(read_lines)
	fd.write(struct.pack("<I",word_in_file))
	print "********* words in file %d *******"%(word_in_file)	
	for line in read_lines:
		data=line.split(",")	
		data[1]=data[1].strip()
		data[0]=int(data[0])
		offset=len(data[1])+1
#WORID,SIZE,WORD
		binary_format="<IB"+str(offset)+"s"
		binary_data=struct.pack(binary_format,data[0],offset,data[1])
		fd.write(binary_data)
	fd.close()
def serialize_node_sig_terms():
	read_lines=read_file(DATA_DIRECTORY+"node_sig_vector.txt")
	fd=open("node_sig_vector.data","wb")
	padding="emn"
	padding2="aluz"
	node_sig_term_size=len(read_lines)
	print "********* node sig term in file %d *******"%(node_sig_term_size)	
	fd.write(struct.pack("<I",node_sig_term_size))
	for line in read_lines:
		line=line.strip()
		data=line.split(",")
		size=(len(data)-2)/2
		node_type=int(data[1])
		node_id=int(data[0])	
#NODE_TYPE,PADING,NODEID,SIZE,TERM_WEIGHT_VECTOR,PADING2
		binary_format="<B3sII"
		fd.write(struct.pack(binary_format,node_type,padding,node_id,size))
		binary_format="<If"
		data=data[2:]
		map_node_terms={}
		for i in range(0,size):
			term_id=int(data[i*2])
			weight=float(data[(i*2)+1])
			if term_id in map_node_terms:
				map_node_terms[term_id].append(struct.pack(binary_format,term_id,weight))
			else:
				map_node_terms[term_id]=[struct.pack(binary_format,term_id,weight)]
		keys=sorted(map_node_terms.keys())
		for key in keys:
			term_node_list=map_node_terms[key]
			for term_node in term_node_list:
				fd.write(term_node)
		fd.write(struct.pack("<4s",padding2))
	fd.close()
def parse_code_list_node(file_name):
	read_lines=read_file(file_name)
	data_mapping={}
	for line in read_lines:
		line=line.replace(":",",")
		line=line.strip()
		data=line.split(",")
		data_mapping[data[0].strip()]=data[1:]
	return data_mapping
def parse_word_to_binary(words,sort=False):
	data=""
	words=[int(word) for word in words]
	if(sort):
		words=sorted(words)
	for word in words:
		data=data+struct.pack("<I",word)
	return data
def create_code_list(key,mapping):
	size=0
	code_list=""
	if key in mapping:
		code_list=mapping[key]
		size=len(code_list)
		code_list=parse_word_to_binary(code_list)
	return (size,code_list)
def serialize_list_node():
	binary_format="<III"
	mapping_code_list_node=parse_code_list_node(DATA_DIRECTORY+"code_list_node.txt")
	mapping_code_list_tax=parse_code_list_node(DATA_DIRECTORY+"code_list_tax.txt")
	keys=mapping_code_list_node.keys()+mapping_code_list_tax.keys()
	keys=Set(keys)
	records_len=len(keys)
	fd=open("list_node.data","wb")
	fd.write(struct.pack("<I",records_len))
	nextWord=False
	for key in keys:
		size_code_list_node,code_list_node=create_code_list(key,mapping_code_list_node)
		size_code_list_tax,code_list_tax=create_code_list(key,mapping_code_list_tax)
		fd.write(struct.pack(binary_format,int(key),size_code_list_node,size_code_list_tax))
		fd.write(code_list_node)
		fd.write(code_list_tax)	
	fd.close()
def serialize_phrases():
	binary_format="<I4s"
	lines=read_file(DATA_DIRECTORY+"phrases.txt")
	fd=open("phrases.data","wb")
	print "******************* phrases in file %d *****************"%(len(lines))
	fd.write(struct.pack(binary_format,len(lines),"emna"))
	for line in lines:	
		data=line.split(",")
		phrase_id=int(data[0])
		data=data[1:]
		fd.write(struct.pack("<II",phrase_id,len(data)))
		fd.write(parse_word_to_binary(data))
	fd.flush()
	fd.close()
def serialize_unfiltered_nodes():
	lines=read_file(DATA_DIRECTORY+"unfiltered_node_id.txt")
	print "******************* unfiltered nodes in file %d *****************"%(len(lines))
	fd=open("unfiltered_nodes.data","wb")
	fd.write(struct.pack("<I",len(lines)))
	[fd.write(struct.pack("<I",int(word))) for word in lines]
	fd.flush()
	fd.close()
def serialize_node_musthave_terms():
	lines=read_file(DATA_DIRECTORY+"node_musthave.txt")
	print "******************* node musthave in file %d *****************"%(len(lines))
	fd=open("node_musthave.data","wb")
	fd.write(struct.pack("<I",len(lines)))
	for line in lines:
		data=line.split(",")
		term_id=int(data[0])
		data=data[1:]
		fd.write(struct.pack("<II",term_id,len(data)))
		fd.write(parse_word_to_binary(data))
	fd.flush()
	fd.close()
def main():
	#print "*********** Generating Word File ****************"
	#serialize_words_file()
	#print "*********** Generating Node Sig Terms ****************"
	#serialize_node_sig_terms()
	print "*********** Generating List Node ****************"
	serialize_list_node()
	#print "*********** Generating Phrases ****************"
#	serialize_phrases()
#	print "*********** Generating Unfiltered Nodes ****************"
	#serialize_unfiltered_nodes()
	#print "*********** Generating Node Musthave terms ****************"
	#serialize_node_musthave_terms()
if __name__=="__main__":
	main()
