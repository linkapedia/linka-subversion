//*************************************************************************************************************
//*************************************************************************************************************
// File: memory_management.c
//*************************************************************************************************************
//*************************************************************************************************************

#include "memory_management.h"
#include "time.h"

#ifdef WIN32
#include <stdio.h>
#include <wchar.h>
#else
#include "stdio.h"
#endif

//*************************************************************************************************************
//*************************************************************************************************************
// Function: file_size
//*************************************************************************************************************
//*************************************************************************************************************

int file_size(int _fd)
{
	struct stat _stat;
	if(fstat(_fd,&_stat)<0)
	{
		return -1;
	}
	return _stat.st_size;
}

//*************************************************************************************************************
//*************************************************************************************************************
// Function: map_file
//*************************************************************************************************************
//*************************************************************************************************************

void *map_file(const char *_file_name,memory_page_buffer **_buffer,unsigned char _write)
{
	int _fd        = -1;
	int _file_size = -1;

#ifndef WIN32
	int _flags=O_RDWR;
#else
	int _flags=O_RDWR|O_BINARY;
#endif

	memory_page_list *_page = NULL;

	if ( _write == 0 )
	{
#ifndef WIN32
		_flags = O_RDONLY;
#else
		_flags = O_RDONLY|O_BINARY;
#endif
	}
	_fd = open( _file_name, _flags );

	if ( _fd < 0 )
		return NULL;

	_file_size = file_size( _fd );

	if( _file_size < 0 )	
	{
		close(_fd);
		return NULL;
	}
	_page = create_memory_page_list( _file_size, _fd );

	close(_fd);

	if ( _page == NULL )
		return NULL;

	*_buffer = add_page_to_buffer( *_buffer, _page );

	return _page->addr;
}

//*************************************************************************************************************
//*************************************************************************************************************
// Function: add_page_to_buffer
//*************************************************************************************************************
//*************************************************************************************************************

memory_page_buffer *add_page_to_buffer(memory_page_buffer *_buffer,memory_page_list *_page)
{
	memory_page_buffer *_new_buffer=_buffer;	
	if(_new_buffer)
	{
		_new_buffer->page_list_size=_new_buffer->page_list_size+1;
		_new_buffer->memory_size=_new_buffer->memory_size+_page->memory_size;
		_page->next=_new_buffer->page;	
		_new_buffer->page=_page;
	}
	else
	{
		_new_buffer=(memory_page_buffer *)malloc(sizeof(memory_page_buffer));
		if(!_new_buffer)
		{
			return NULL;
		}
		_new_buffer->page=_page;	
		_new_buffer->page_list_size=1;
		_new_buffer->memory_size=_page->memory_size;
	}
	return _new_buffer;	
}

//*************************************************************************************************************
//*************************************************************************************************************
// Function: free_memory_page_buffer
//*************************************************************************************************************
//*************************************************************************************************************

void free_memory_page_buffer(memory_page_buffer *_buffer)
{
	memory_page_list *_page_current=_buffer->page;
	memory_page_list *_page_next=_page_current;
	while(_page_next)
	{
		_page_next=_page_current->next;
		free(_page_current->addr);
		free(_page_current);
		_page_current=_page_next;
	}
	free(_buffer);
}

//*************************************************************************************************************
//*************************************************************************************************************
// Function: free_front_page_from_buffer
//*************************************************************************************************************
//*************************************************************************************************************

void free_front_page_from_buffer(memory_page_buffer *_buffer)
{
	memory_page_list *_memory_page_list=_buffer->page;
	_buffer->page=_memory_page_list->next;
	_buffer->memory_size=_buffer->memory_size-_memory_page_list->memory_size;
	free(_memory_page_list->addr);
	free(_memory_page_list);
}

//*************************************************************************************************************
//*************************************************************************************************************
// Function: create_page
//*************************************************************************************************************
//*************************************************************************************************************

memory_page_list *create_page()
{
	memory_page_list *_memory_page_list = create_memory_page_list( PAGE_SIZE, 0 );

	return _memory_page_list;
}

//*************************************************************************************************************
//*************************************************************************************************************
// Function: create_memory_page_list
//*************************************************************************************************************
//*************************************************************************************************************

memory_page_list *create_memory_page_list( unsigned int _page_size, int _fd )
{
	void *_page_memory = NULL;

	memory_page_list *_memory_page_list = NULL;

	_page_memory = malloc(_page_size);

	if ( _page_memory == NULL )
		return NULL;
	
	if ( _fd > 0 )
		_read( _fd, _page_memory, _page_size );

	_memory_page_list = (memory_page_list *)malloc(sizeof(memory_page_list));

	if ( _memory_page_list == NULL )
		return NULL;

	_memory_page_list->memory_size = _page_size;
	_memory_page_list->addr        = _page_memory;
	_memory_page_list->next        = NULL;

	return _memory_page_list;
}

//*************************************************************************************************************
//*************************************************************************************************************
// Function: realloc_memory_page
//*************************************************************************************************************
//*************************************************************************************************************

memory_page_list *realloc_memory_page( memory_page_list *_memory_page_list, unsigned int _new_page_size )
{
	void *_new_page        = NULL;
	void *_old_memory_page = _memory_page_list->addr;

	_new_page = realloc( _old_memory_page, _new_page_size );
	
	printf("************** memory map  realloc size %d***************\n",_memory_page_list->memory_size);

	if ( _new_page == NULL )
		return NULL;

	_memory_page_list->addr        = _new_page;
	_memory_page_list->memory_size = _new_page_size;

	return _memory_page_list;
}

//*************************************************************************************************************
//*************************************************************************************************************
//*************************************************************************************************************
//*************************************************************************************************************

