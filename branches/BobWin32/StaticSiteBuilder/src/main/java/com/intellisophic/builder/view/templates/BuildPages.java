/*
 * Build the pages per node
 * @authors Andres F. Restrepo A.
 *
 * @version 0.7
 * Date : JUN 20, 2012.
 */
package com.intellisophic.builder.view.templates;

import com.intellisophic.builder.business.InterestCircleInfo;
import com.intellisophic.builder.business.TreeMenuBO;
import com.intellisophic.builder.business.beans.Node;
import com.intellisophic.builder.business.context.AppServletContextListener;
import com.intellisophic.builder.services.ftp.FtpSync;
import com.intellisophic.builder.services.mongo.MongoHandler;
import com.intellisophic.builder.view.beans.ViewLink;
import com.intellisophic.builder.view.beans.ViewNode;
import com.yuxipacific.utils.StringUtils;
import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.jdom.Element;

public class BuildPages {
    // Attributes of the parameter constructor

    private static final Logger LOG = Logger.getLogger(BuildPages.class);
    private static final ResourceBundle RB = ResourceBundle.getBundle("system/config");
    private static final boolean ISFTPENABLE = Boolean.valueOf(RB.getString("ftp.enabled"));
    private static final String PATHOUTPUT;
    private static final HashMap<String, FtpSync> MAPSYNCFILE = new HashMap<String, FtpSync>();
    private static final List<Timer> TIMERSYNCLIST = new ArrayList<Timer>();
    private static final long TIMESYNC;
    //num links by page
    private static int NUM_LINKS_BY_PAGE = 8;

    static {
        PATHOUTPUT = ISFTPENABLE == false ? RB.getString("velocity.pages.output") : RB.getString("ftp.sync.tempDir");
        TIMESYNC = (Long.valueOf(RB.getString("ftp.sync.timer")) * 1000);

    }

    public BuildPages() {
    }

    /**
     * method start to create pages from a node
     *
     * @param taxonomy :name of the taxonomy(create root file with this name)
     * @param tree :map what contain 3 levels from a node
     * @param viewNode :info from node to build
     */
    public static void createPages(String taxonomy, Map<Integer, ArrayList<Element>> tree, ViewNode viewNode) {
        LOG.debug("createPages()");
        LOG.debug("Memory free: " + Runtime.getRuntime().freeMemory());
        StringWriter writer = new StringWriter();
        File directoryTaxonomy = null;
        VelocityContext context = null;
        Template template = null;
        VelocityEngine engine = null;
        LOG.debug(viewNode.getPath());
        LOG.debug(viewNode.getNodeDesc());
        LOG.debug(viewNode.getNodeId());
        LOG.debug(viewNode.getNodeTitle());
        LOG.debug(viewNode.getNodeMustHaves());
        //configure template
        try {
            engine = getEngine();
            template = engine.getTemplate(RB.getString("velocity.templates.name"));
            context = new VelocityContext();
            boolean flag = false;
            String nodeServer = RB.getString("nodejs.server");
            String sentenceServer = RB.getString("system.builder.sentence.server");
            //create taxonomy directory and copy generic resources into that 
            try {
                directoryTaxonomy = createFolderTaxonomy(taxonomy);
                File fileResource = new File(directoryTaxonomy, RB.getString("velosity.template.resource.name"));
                if (!fileResource.exists()) {
                    copyDirectory(new File(RB.getString("velocity.templates.resources.src")), directoryTaxonomy);
                }
                File directoryResourceGlobal = new File(PATHOUTPUT, RB.getString("velosity.template.siteresource.name"));
                if (!directoryResourceGlobal.exists()) {
                    copyDirectory(new File(RB.getString("velocity.templates.siteresources.src")), new File(PATHOUTPUT));
                }
                flag = true;
            } catch (Exception e) {
                LOG.error("Exception createFolderTaxonomy or copyDirectory" + e.getMessage());
                flag = false;
            }
            if (!flag) {
                LOG.error("not created structure to taxonomy");
            } else {
                // create logic to build template a html files
                //logic to the bridge node
                Map<Long, String> mapDesc = viewNode.getBridgeChildsInfo();
                Map<String, String> mapDescription = new HashMap<String, String>();
                String desc = "";
                if (mapDesc != null) {
                    for (long s : mapDesc.keySet()) {
                        desc = mapDesc.get(s);
                        if (desc == null || desc.trim().length() == 0) {
                            desc = "false";
                        }
                        mapDescription.put(String.valueOf(s), desc);
                        LOG.info("Description from Build Page " + desc + " Key " + s);
                    }
                }

                if (viewNode.isBridgeNode()) {
                    context.put("bridgeNode", "true");
                    context.put("childsMap", mapDescription);
                } else {
                    context.put("bridgeNode", "false");
                }

                context.put("taxonomy", StringUtils.encodeFileName(taxonomy));
                context.put("taxonomyName", org.apache.velocity.util.StringUtils.capitalizeFirstLetter(StringUtils.encodeFileName(taxonomy)));
                context.put("pathRootNode", AppServletContextListener.getNodeCache().getNodeByNodeID(viewNode.getRootNode().getId()).getPathToParent() + ".html");


                //logic to build list
                List<Element> level1 = tree.get(TreeMenuBO.LEVEL1);
                List<Element> level2 = tree.get(TreeMenuBO.LEVEL2);
                List<Element> level3 = tree.get(TreeMenuBO.LEVEL3);
                List<Element> level4 = tree.get(TreeMenuBO.LEVEL4);
                
                


                List<ViewLink> listLinks = viewNode.getLinks();
                //set id to the links
                LOG.info("set id for the links");
                int i = 1;
                for (ViewLink vli : listLinks) {
                    vli.setId(UUID.randomUUID().toString() + System.nanoTime() + i);
                    vli.setIdDefaultImage(String.valueOf(viewNode.getNodeId()));
                    i++;
                }

                //this map contain the path and content to create each page
                Map<String, StringWriter> mapWriters = new HashMap<String, StringWriter>();
                if (!listLinks.isEmpty()) {
                    try {
                        //create images to the links
                        sendPackImages(directoryTaxonomy, listLinks, viewNode);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                    int numLinkByPage = 0;
                    try {
                        //logic to infinite scroll
                        numLinkByPage = Integer.parseInt(RB.getString("builder.linksbypage"));
                        if (numLinkByPage < 1 || numLinkByPage > 50) {
                            numLinkByPage = NUM_LINKS_BY_PAGE;
                        }
                        if (listLinks.size() > numLinkByPage) {
                            //split arraylist
                            mapWriters = getPagesInfiniteScroll(listLinks.subList(numLinkByPage, listLinks.size()),
                                    viewNode, numLinkByPage,
                                    StringUtils.encodeFileName(taxonomy), sentenceServer);
                            //the first page only have the first links
                            listLinks = listLinks.subList(0, numLinkByPage);
                        }
                    } catch (Exception e) {
                        LOG.error("Error creating pages infinite scroll: " + e.getMessage());
                    }
                }

                //logic to build the menu
                Long currentId = null;
                Long parentId = null;
                Node grandfatherNode = null;
                Node myFather = null;
                boolean showLevel1 = true;
                boolean hasChild = true;
                currentId = viewNode.getNodeId();
                parentId = viewNode.getParentId();
                if ((level3 == null) || ((level3 != null) && (level3.isEmpty()))) {
                    level3 = level2;
                    level2 = level1;
                    level1 = level4;
                    myFather = AppServletContextListener.getNodeCache().getNodeByNodeID(parentId);
                    grandfatherNode = myFather.getParent();
                    changeOrderToPath(level1, grandfatherNode.getId());
                    changeOrderToPath(level2, parentId);
                    changeOrderToPath(level3, currentId);
                    hasChild = false;
                } else {
                    changeOrderToPath(level1, parentId);
                    changeOrderToPath(level2, currentId);
                }

                //root node
                if ((level1 == null) || (level1.isEmpty())) {
                    showLevel1 = false;
                }

                context.put("showLevel1", showLevel1);
                context.put("level1", level1);
                context.put("level2", level2);
                context.put("level3", level3);
                context.put("hasChild", hasChild);

                String pathNode = viewNode.getPath();
                if (pathNode.startsWith("/")) {
                    pathNode = pathNode.replaceFirst("/", "");
                }
                pathNode = pathNode.replaceAll("/", " > ");

                //cInterest--
                Random ram = new Random(System.currentTimeMillis());
                int num = ram.nextInt(2) + 4;
                List<InterestCircleInfo> listInterest = InterestCircleInfo.getRamdomElementsByTax(num, String.valueOf(viewNode.getRootNode().getCorpusID()));
                context.put("listInterest", listInterest);


                //Get the site name in the webserver
                //Needed for the references to the resource
                context.put("siteName", RB.getString("webserver.linkapedia.foldername"));
                LOG.debug(viewNode.getNodeId());
                context.put("nodeid", viewNode.getNodeId());
                context.put("nodeTitle", viewNode.getNodeTitle());
                context.put("secondPage", StringUtils.encodeFileName(viewNode.getNodeTitle()));
                context.put("nodeDesc", viewNode.getNodeDesc());
                context.put("nodePath", pathNode);
                context.put("musthaves", viewNode.getNodeMustHaves());
                context.put("currentId", viewNode.getNodeId());
                context.put("linksImages", RB.getString("builder.classify.folder"));
                context.put("parentId", viewNode.getParentId());
                context.put("links", listLinks);
                context.put("nodeServer", nodeServer);
                context.put("serverSentences", sentenceServer);


                template.merge(context, writer);
                //write one or more pages (write infinite scroll pages if is necessary)
                mapWriters.put(viewNode.getPath() + ".html", writer);
                Iterator<Map.Entry<String, StringWriter>> iter = mapWriters.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry<String, StringWriter> entry = iter.next();
                    try {
                        if (ISFTPENABLE) {
                            synchronized (MAPSYNCFILE) {
                                FtpSync ftpSync = MAPSYNCFILE.get(directoryTaxonomy.getAbsolutePath());
                                File newFileEntry = new File(entry.getKey());
                                ftpSync.addNewEntry(newFileEntry, entry.getValue().toString().getBytes("UTF-8"));
                            }
                        } else {
                            createPage(entry.getValue(), entry.getKey());
                        }
                        LOG.info("Create page successfully");
                    } catch (Exception e) {
                        LOG.error("Not create page to nodeid: " + viewNode.getNodeId() + " " + e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Class: Builder Method: createPages velocity Exception - general exception", e);
        }
        LOG.info("Finished!");
    }

    /**
     * configure the template
     *
     * @return
     * @throws Exception
     */
    private static VelocityEngine getEngine() throws Exception {
        Properties p = new Properties();
        p.setProperty("resource.loader", "file");
        p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
        p.setProperty("file.resource.loader.path", RB.getString("velocity.templates.src"));
        //Charset properties
        p.setProperty("input.encoding", "UTF-8");
        p.setProperty("output.encoding", "UTF-8");
        VelocityEngine engine = new VelocityEngine();
        engine.init(p);
        return engine;
    }

    /**
     * build a StringWriter and path to create the other pages(infinite scroll
     * pages)
     *
     * @param links
     * @param nodeTitle
     */
    private static Map<String, StringWriter> getPagesInfiniteScroll(List<ViewLink> links, ViewNode node,
            int numToSplit, String tax, String serverSen) throws Exception {

        Map<String, StringWriter> map = new HashMap<String, StringWriter>();
        StringWriter writer = null;
        List<ViewLink> listAux = null;
        VelocityContext context = null;
        Template template = null;
        VelocityEngine engine = null;
        engine = getEngine();
        template = engine.getTemplate(RB.getString("velocity.templates.name.pages"));
        context = new VelocityContext();
        int cont = 2;
        int min = 0;
        int max = min + numToSplit;
        if (max >= links.size()) {
            max = links.size();
        }
        context.put("taxonomy", tax);
        context.put("siteName", RB.getString("webserver.linkapedia.foldername"));
        context.put("linksImages", RB.getString("builder.classify.folder"));
        context.put("serverSentences", serverSen);
        context.put("nodeid", node.getNodeId());


        while (min < links.size()) {
            writer = new StringWriter();
            listAux = links.subList(min, max);
            //logic with the .vm 
            context.put("links", listAux);
            template.merge(context, writer);
            map.put(node.getPath() + "-" + cont + ".html", writer);
            min = max;
            max = min + numToSplit;
            if (max >= links.size()) {
                max = links.size();
            }
            cont++;
        }
        return map;
    }

    /**
     * save data into mongodb
     *
     * @param taxonomyDir
     * @param links
     * @throws Exception
     */
    private static void sendPackImages(File taxonomyDir, List<ViewLink> links, ViewNode n) throws Exception {
        LOG.info("Send data to Queue (Image Process)");
        File fileImages = new File(taxonomyDir, RB.getString("builder.classify.folder"));
        if (!fileImages.exists()) {
            fileImages.mkdir();
        }
        //write data into mongodb
        if (links != null && n != null) {
            MongoHandler.saveDataImages(links, n);
        }
    }

    /**
     * change id to first element in the list
     *
     * @param list
     * @param id
     */
    private static void changeOrderToPath(List<Element> list, Long id) {
        if (list == null) {
            return;
        }
        String elementId = "";
        String sElementId = String.valueOf(id);
        Element aux;
        for (int i = 0; i < list.size(); i++) {
            Element e = list.get(i);
            elementId = e.getAttributeValue("id");
            if (elementId.equalsIgnoreCase(sElementId)) {
                //change position
                aux = list.get(0);
                list.set(0, e);
                list.set(i, aux);
                break;
            }
        }
    }

    /**
     * create folder to the taxonomy and delete if exists
     *
     * @param taxonomy
     * @return
     * @throws Exception
     */
    private static File createFolderTaxonomy(String taxonomy) throws Exception {
        //create root folder and delete if the folder exists
        FtpSync ftpSync = null;
        String filePath = PATHOUTPUT;
        filePath += StringUtils.encodeFileName(taxonomy).toLowerCase();
        File directoryTaxonomy = new File(filePath);
        if (!ISFTPENABLE) {
            if (!directoryTaxonomy.exists()) {
                directoryTaxonomy.mkdir();
            }
            return directoryTaxonomy;
        }
        synchronized (MAPSYNCFILE) {
            ftpSync = MAPSYNCFILE.get(filePath);
            if (ftpSync == null) {
                ftpSync = new FtpSync();
                ftpSync.setTaxonomy(taxonomy);
                MAPSYNCFILE.put(directoryTaxonomy.getAbsolutePath(), ftpSync);
                Timer timer = new Timer();
                long currentTime = System.currentTimeMillis() + TIMESYNC;
                timer.schedule(ftpSync, new Date(currentTime), TIMESYNC);
                TIMERSYNCLIST.add(timer);
            }
        }
        return directoryTaxonomy;
    }

    /**
     * Create structure SEO into FileSystem
     *
     * @param writer text template
     * @param nodeTitle
     * @param directory path to create the file
     * @return
     */
    private static void createPage(StringWriter writer, String directory) throws Exception {
        LOG.debug("createPage(StringWritter, String, String)");
        //create complete directory to this node
        LOG.debug("directory: " + directory);
        OutputStreamWriter osw = null;
        BufferedWriter fbw = null;
        try {
            File f = new File(RB.getString("velocity.pages.output") + directory);
            if (f.exists()) {
                f.delete();
            } else {
                f.getParentFile().mkdirs();
            }
            //write content into html page
            osw = new OutputStreamWriter(
                    new FileOutputStream(f, true), "UTF-8");
            fbw = new BufferedWriter(osw);
            fbw.write(writer.toString());
            fbw.flush();
        } finally {
            if (fbw != null) {
                fbw.close();
            }
            if (osw != null) {
                osw.close();
            }
        }
    }

    /**
     * Copy generic folder to taxonomy directory
     *
     * @param sourceLocation directory(Origin)
     * @param targetLocation directory(target)
     * @throws IOException
     */
    private static void copyDirectory(File sourceLocation, File targetLocation) throws IOException {
        LOG.debug("copyDirectory(File, File)");
        InputStream in = null;
        OutputStream out = null;
        try {
            if (sourceLocation.isDirectory()) {
                if (!targetLocation.exists()) {
                    targetLocation.mkdir();
                }
                String[] children = sourceLocation.list();
                for (int i = 0; i < children.length; i++) {
                    copyDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
                }
            } else {
                in = new FileInputStream(sourceLocation);
                out = new FileOutputStream(targetLocation);
                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                in = null;
                out = null;
            }
        } catch (Exception e) {
            LOG.error("An exception ocurred while making the copy of the files.", e);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
                LOG.error("An exception ocurred while closing the input streams.", e);
            }
        }
    }
}
