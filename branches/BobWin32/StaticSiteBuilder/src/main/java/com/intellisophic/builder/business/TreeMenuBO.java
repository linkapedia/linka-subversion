package com.intellisophic.builder.business;

import com.intellisophic.builder.services.exceptions.DataNotFoundException;
import java.util.*;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

/**
 *
 * @author Xander Kno
 */
public class TreeMenuBO {

    private static final Logger log = Logger.getLogger(TreeMenuBO.class);
    private static Document fullTreeMenu = null;
    public static final int LEVEL1 = 1;
    public static final int LEVEL2 = 2;
    public static final int LEVEL3 = 3;
    public static final int LEVEL4 = 4;

    public static Map<Integer, ArrayList<Element>> getTreeMenuForByNodeID(Long nodeID) throws DataNotFoundException, Exception {
        log.debug("getTreeMenuForNode(Map)");
        Map<Integer, ArrayList<Element>> partialTree = new HashMap<Integer, ArrayList<Element>>();
        Document finalTreeMenu = getFullTreeMenu();
        try {
            if (finalTreeMenu != null) {
                Element element = (Element) XPath.selectSingleNode(finalTreeMenu, "/root/item[@id='" + nodeID + "']");
                if (element != null) {
                    partialTree.put(LEVEL1, getFirstLevelItems(element, finalTreeMenu));
                    partialTree.put(LEVEL2, getSecondLevelItems(element, finalTreeMenu));
                    partialTree.put(LEVEL3, getThirdLevelItems(element, finalTreeMenu));
                    partialTree.put(LEVEL4, getFourthLevelItems(element, finalTreeMenu));
                } else {
                    throw new DataNotFoundException("Node ID not found in menu xml structure.");
                }
            } else {
                throw new DataNotFoundException("XML tree menu not found.");
            }
        } catch (DataNotFoundException e) {
            log.error("An exception has ocurred. " + e.getLocalizedMessage());
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred. " + e.getLocalizedMessage());
            throw e;
        }
        return partialTree;
    }
    
    /**
     * get grandfathers from a node
     * @param e
     * @param xdoc
     * @return 
     */
    private static ArrayList<Element> getFourthLevelItems(Element e,Document xdoc) throws Exception{
        ArrayList<Element> arrayToreturn = new ArrayList<Element>();
        String parentidElement = e.getAttributeValue("parent_id");
        //my father
        Element element = (Element) XPath.selectSingleNode(xdoc, "/root/item[@id='" + parentidElement + "']");
        if(element==null){
            return null;
        }
        parentidElement = element.getAttributeValue("parent_id");
        String parentidParents = "";
        Element root = xdoc.getRootElement();
        List<Element> elements = root.getChildren("item");
        Iterator<Element> iter = elements.iterator();
        while (iter.hasNext()) {
            Element eNow = iter.next();
            String id = eNow.getAttributeValue("id");
            if (id.equals(parentidElement)) {
                parentidParents = eNow.getAttributeValue("parent_id");
                break;
            }
        }
        //not have grandfathers
        if (parentidParents.equals("")) {
            return null;
        }
        iter = elements.iterator();
        while (iter.hasNext()) {
            Element eNow = iter.next();
            String parentid = eNow.getAttributeValue("parent_id");
            if (parentid.equals(parentidParents)) {
                arrayToreturn.add(eNow);
            }
        }

        if (arrayToreturn.isEmpty()) {
            return null;
        }
        return arrayToreturn;
    }

    private static ArrayList<Element> getFirstLevelItems(Element e, Document xdoc) throws Exception {
        ArrayList<Element> arrayToreturn = new ArrayList<Element>();
        String parentidElement = e.getAttributeValue("parent_id");
        String parentidParents = "";
        Element root = xdoc.getRootElement();
        List<Element> elements = root.getChildren("item");
        Iterator<Element> iter = elements.iterator();
        while (iter.hasNext()) {
            Element eNow = iter.next();
            String id = eNow.getAttributeValue("id");
            if (id.equals(parentidElement)) {
                parentidParents = eNow.getAttributeValue("parent_id");
                break;
            }
        }
        if (parentidParents.equals("")) {
            return null;
        }
        iter = elements.iterator();
        while (iter.hasNext()) {
            Element eNow = iter.next();
            String parentid = eNow.getAttributeValue("parent_id");
            if (parentid.equals(parentidParents)) {
                arrayToreturn.add(eNow);
            }
        }

        if (arrayToreturn.isEmpty()) {
            return null;
        }
        return arrayToreturn;
    }

    private static ArrayList<Element> getSecondLevelItems(Element e, Document xdoc) throws Exception {

        ArrayList<Element> arrayToreturn = new ArrayList<Element>();
        String parentidElement = e.getAttributeValue("parent_id");
        Element root = xdoc.getRootElement();
        List<Element> elements = root.getChildren("item");
        Iterator<Element> iter = elements.iterator();
        while (iter.hasNext()) {
            Element eNow = iter.next();
            String parentID = eNow.getAttributeValue("parent_id");
            if (parentID.equals(parentidElement)) {
                arrayToreturn.add(eNow);
            }
        }
        if (arrayToreturn.isEmpty()) {
            return null;
        }
        return arrayToreturn;
    }

    private static ArrayList<Element> getThirdLevelItems(Element e, Document xdoc) throws Exception {

        ArrayList<Element> arrayToreturn = new ArrayList<Element>();
        String idElement = e.getAttributeValue("id");
        Element root = xdoc.getRootElement();
        List<Element> elements = root.getChildren("item");
        Iterator<Element> iter = elements.iterator();
        while (iter.hasNext()) {
            Element eNow = iter.next();
            String parentID = eNow.getAttributeValue("parent_id");
            if (parentID.equals(idElement)) {
                arrayToreturn.add(eNow);
            }
        }
        if (arrayToreturn.isEmpty()) {
            return null;
        }
        return arrayToreturn;
    }

    public static Document getFullTreeMenu() {
        return TreeMenuBO.fullTreeMenu;
    }

    public static void setFullTreeMenu(Document fullTreeMenu) {
        if (fullTreeMenu != null) {
            TreeMenuBO.fullTreeMenu = fullTreeMenu;
        }
    }
}
