package com.intellisophic.builder.data.pool;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.apache.log4j.Logger;

public abstract class ObjectPool<T> {

    private static final Logger log = Logger.getLogger(ObjectPool.class);
    private long expirationTime;
    private final HashMap<T, Long> locked, unlocked;
    private int poolSize;
    private static final Object lock = new Object();

    public ObjectPool() {
        expirationTime = 30000; // 30 seconds
        poolSize = 15;
        locked = new HashMap<T, Long>(poolSize);
        unlocked = new HashMap<T, Long>(poolSize);
    }

    protected abstract T create();

    public abstract boolean validate(T o);

    public abstract void expire(T o);

    public synchronized T checkOut() {
        log.debug("checkOut()");
        long now = System.currentTimeMillis();
        T t;
        synchronized (lock) {
            if (unlocked.size() > 0) {
                Set<T> e = unlocked.keySet();
                Iterator<T> it = e.iterator();
                while (it.hasNext()) {
                    t = it.next();
                    if ((now - unlocked.get(t)) > expirationTime) {
                        // object has expired
                        it.remove();
                        expire(t);
                        t = null;
                    } else {
                        if (validate(t)) {
                            it.remove();
                            synchronized (locked) {
                                locked.put(t, now);
                            }
                            return (t);
                        } else {
                            // object failed validation
                            it.remove();
                            expire(t);
                            t = null;
                        }
                    }
                }
            }
            // no objects available, create a new one
            t = create();
            synchronized (locked) {
                locked.put(t, now);
            }
        }
        log.debug("Connections opened: " + locked.size());
        return (t);
    }

    public synchronized void checkIn(T t) {
        log.debug("checkIn()");
        synchronized (lock) {
            locked.remove(t);
            log.debug("Free connections: " + unlocked.size());
            unlocked.put(t, System.currentTimeMillis());
        }
    }
}
