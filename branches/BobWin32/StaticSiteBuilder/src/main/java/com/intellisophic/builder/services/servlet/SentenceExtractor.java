package com.intellisophic.builder.services.servlet;


import com.intellisophic.builder.view.beans.SummaryObject;
import com.yuxipacific.utils.SentenceServletUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 *
 * Class to get sentences from a link
 */
public class SentenceExtractor extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(SentenceExtractor.class);
    private static final String SENTENCES_USER = "1";
    private static final String SENTENCES = "2";

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();

        List<SummaryObject> list = null;
        String nodeId = "";
        String fileName = "";
        //jsonp
        String callback = "";
        String sentencesType = "";

        try {
            // Search in the cache the sentences.
            // if not found in the cache search in the NAS
            // build json object with the information

            //get parameters
            nodeId = request.getParameter("nodeId");
            fileName = request.getParameter("fileName");
            callback = request.getParameter("callback");
            if (callback != null) {
                SentenceServletUtils.enabledJsonp(callback);
            }
            sentencesType = request.getParameter("sentenceType");

            Long nodeIdL = SentenceServletUtils.validateNodeID(nodeId);
            
            LOG.debug("nodeId: "+nodeId);
            LOG.debug("fileName: "+fileName);
            LOG.debug("sentencesType: "+sentencesType);

            if ((nodeIdL != null)
                    && (SentenceServletUtils.validateSentenceType(sentencesType))
                    && (SentenceServletUtils.validateFileName(fileName))) {
                LOG.debug("get all sentences");
                try {
                    list = SentenceServletUtils.getSentences(fileName, nodeIdL);
                } catch (Exception e) {
                    LOG.error("Error get sentences: "+ e.getMessage());
                }

                if (list != null) {
                    if (sentencesType.equals(SENTENCES_USER)) {
                        LOG.debug("Show sentences for the user");
                        out.write(SentenceServletUtils.getMsgSentencesJsonUser(list).toString());
                    } else if (sentencesType.equals(SENTENCES)) {
                        LOG.debug("Show sentences for the debug");
                        out.write(SentenceServletUtils.getMsgSentencesJsonAll(list).toString());
                    } else {
                        LOG.debug("Show sentences for the debug");
                        out.write(SentenceServletUtils.getMsgSentencesJsonAll(list).toString());
                    }
                } else {
                    out.write(SentenceServletUtils.getErrorSentences());
                }
            } else {
                LOG.error("Missing parameters");
                out.write(SentenceServletUtils.getErrorParameters());
            }

        } catch (Exception e) {
            LOG.error("Error in the server: " + e.getMessage());
        } finally {
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
