package com.intellisophic.builder.business.beans;

import java.io.Serializable;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author Alex
 */
public class Node implements Serializable {

    /**
     * NodeID (From DB)
     */
    private Long id;
    /**
     * NodeTitle (From DB)
     */
    private String title;
    /**
     * ParentID references a NodeID. (From DB)
     */
    private Node parent;
    /**
     * NodeDescription (From DB)
     */
    private String description;
    /**
     * Index Within Parent: Used to sort the nodes.
     */
    private String indexWithinParent;
    /**
     * Taxonomy Identifier.
     */
    private Long corpusID;
    /**
     * String containing the path from the node to the root node. (Calculated)
     */
    private String _pathToParent;

    public Node() {
        this.id = null;
        this.title = null;
        this.parent = null;
    }

    /**
     * Method to transform a single node in a XML representation.
     *
     * @return Node Structure as XML.
     */
    public String asXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<item id='");
        sb.append(getId());
        sb.append("' parent_id='");
        sb.append(getParent().getId());
        sb.append("' index='");
        sb.append(getIndexWithinParent());
        sb.append("'><content><name href='");
        sb.append(StringEscapeUtils.escapeXml(getPathToParent()));
        sb.append(".html'>");
        sb.append(StringEscapeUtils.escapeXml(getTitle().trim()));
        sb.append("</name></content></item>");
        return sb.toString();
    }

    public String getIndexWithinParent() {
        return indexWithinParent;
    }

    public void setIndexWithinParent(String indexWithinParent) {
        this.indexWithinParent = indexWithinParent;
    }

    public String getPathToParent() {
        return _pathToParent;
    }

    public void setPathToParent(String _pathToParent) {
        this._pathToParent = _pathToParent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCorpusID() {
        return corpusID;
    }

    public void setCorpusID(Long corpusID) {
        this.corpusID = corpusID;
    }
}
