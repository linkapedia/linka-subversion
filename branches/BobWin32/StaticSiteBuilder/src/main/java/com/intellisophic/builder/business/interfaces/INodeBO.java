package com.intellisophic.builder.business.interfaces;

import com.intellisophic.builder.business.beans.Node;
import java.util.List;

/**
 *
 * @author Alex
 */
public interface INodeBO {
    public List<Node> getNodesByCorpusID(Long corpusID) throws Exception;

    public Node getRootNodeByCorpusID(Long corpusID) throws Exception;
}
