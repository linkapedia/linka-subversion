package com.intellisophic.builder.data.interfaces;

import com.intellisophic.builder.business.beans.Node;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Alex
 */
public interface INodeDAO {
    public List<Node> getNodesByCorpusID(Long corpusID) throws SQLException, Exception;

    public Node getRootNodeByCorpusID(Long corpusID) throws SQLException, Exception;
}
