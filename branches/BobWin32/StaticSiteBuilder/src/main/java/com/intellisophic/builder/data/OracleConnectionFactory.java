package com.intellisophic.builder.data;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class OracleConnectionFactory {

    private static Logger log = Logger.getLogger(OracleConnectionFactory.class);

    public OracleConnectionFactory() {
    }

    public Connection createConnection() throws SQLException, ClassNotFoundException, NamingException {
        log.debug("createConnection()");
        Connection connection = null;
                
        try {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            OracleDataSource ds = (OracleDataSource) envCtx.lookup("jdbc/LinkapediaOracleDB");
            connection = ds.getConnection();
        } catch (NamingException e) {
            log.error("Error in createConnection", e);
            throw e;
        } catch (SQLException e) {
            log.error("Error in createConnection", e);
            throw e;
        }
        return connection;
    }
}
