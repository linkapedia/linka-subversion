package com.intellisophic.builder.services.cache.jcs;

import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.access.exception.InvalidArgumentException;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class JCSMustHaveCache {
    private static final Logger log = Logger.getLogger(JCSMustHaveCache.class);
    private JCS cache = null;
    private static final String MUSTHAVE_CACHE_IDENTIFIER = "mustHaveCache";

    /**
     * Class contructor used to initialize the cache.
     */
    public JCSMustHaveCache() {
        log.debug("MustHave Cache Constructor.");
        try {
            // Load the cache
            cache = JCS.getInstance(MUSTHAVE_CACHE_IDENTIFIER);
            log.info("MustHave Cache Initialized.");
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method that allows to add new must have in the cache.
     * @param nodeID Node Identifier.
     * @param mustHave Must haves of the given node
     */
    public void addMustHave(Long nodeID, String mustHave) {
        log.debug("addMustHave(Long, String)");
        try {
            if (nodeID == null || nodeID.longValue()<0) {
                throw new InvalidArgumentException("The object to store can not be null or negative");
            }
            if (mustHave == null || mustHave.trim().equals("")) {
                throw new InvalidArgumentException("The object properties can not be null or empty in " + nodeID);
            }
            cache.put(nodeID, mustHave);
        } catch (InvalidArgumentException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        } catch (CacheException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        }
    }

    /**
     * Method to return the must haves for a given nodeID.
     * @param nodeID Node Identifier.
     * @return Must haves of the given node, null if not found.
     */
    public String getMustHave(Long nodeID) {
        log.debug("getMustHave(Long)");
        return (String) cache.get(nodeID);
    }

    /**
     * Method to remove a must have from the cache for given node.
     * @param nodeID Node Identifier.
     */
    public void removeMustHave(Long nodeID) {
        log.debug("removeMustHave(Long)");
        try {
            cache.remove(nodeID);
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method to close the Cache when the application shutdown.
     */
    public static void shutdown() {
        CompositeCacheManager compositeCacheManager = CompositeCacheManager.getInstance();
        compositeCacheManager.release();
    }
}
