package com.intellisophic.builder.business.beans;

/**
 *
 * @author Alex
 */
public class BuildRequest {
    private String[] ipAddress;
    private Long corpusID;

    public long getCorpusID() {
        return corpusID;
    }

    public void setCorpusID(long corpusID) {
        this.corpusID = corpusID;
    }

    public String[] getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String[] ipAddress) {
        this.ipAddress = ipAddress;
    }
}
