package com.intellisophic.builder.business;

import com.intellisophic.builder.business.beans.Node;
import com.intellisophic.builder.business.interfaces.INodeBO;
import com.intellisophic.builder.data.NodeDAO;
import com.intellisophic.builder.data.interfaces.INodeDAO;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class NodeBO implements INodeBO {
    private static final Logger log = Logger.getLogger(NodeBO.class);
    private INodeDAO nodeDAO;

    public NodeBO() {
        nodeDAO = new NodeDAO();
    }

    @Override
    public List<Node> getNodesByCorpusID(Long corpusID) throws Exception {
        log.debug("getNodesByCorpusID()");
        List<Node> nodeList = null;
        try {
            nodeList = getNodeDAO().getNodesByCorpusID(corpusID);
        } catch (Exception exception) {
            log.error("Error in getNodesByCorpusID().", exception);
        }
        return nodeList;
    }

    public INodeDAO getNodeDAO() {
        return nodeDAO;
    }

    public void setNodeDAO(INodeDAO nodeDAO) {
        this.nodeDAO = nodeDAO;
    }

    @Override
    public Node getRootNodeByCorpusID(Long corpusID) throws Exception {
        log.debug("getRootNodeByCorpusID()");
        Node node = null;
        try {
            node = getNodeDAO().getRootNodeByCorpusID(corpusID);
        } catch (Exception exception) {
            log.error("Error in getRootNodeByCorpusID().", exception);
        }
        return node;
    }
}
