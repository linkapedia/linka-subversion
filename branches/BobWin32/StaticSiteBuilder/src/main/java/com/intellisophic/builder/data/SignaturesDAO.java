package com.intellisophic.builder.data;

import com.intellisophic.builder.business.beans.SignatureObject;
import com.intellisophic.builder.data.interfaces.ISignaturesDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class SignaturesDAO extends OracleConnectionFactory implements ISignaturesDAO {

    private static final Logger LOG = Logger.getLogger(SignaturesDAO.class);

    @Override
    public Map<Long, List<SignatureObject>> getAllSignatures() throws SQLException, Exception {
        LOG.debug("get all signatures");
        Map<Long, List<SignatureObject>> allSignatures = new HashMap<Long, List<SignatureObject>>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = createConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT NODEID,SIGNATUREWORD,SIGNATUREOCCURENCES,LANG FROM SIGNATURE");
            Long nodeID = null;
            String word = "";
            int ocurrences;
            String lang = "";
            SignatureObject sobj = null;
            List<SignatureObject> list = null;
            while (rs.next()) {
                nodeID = rs.getLong("NODEID");
                word = rs.getString("SIGNATUREWORD");
                ocurrences = rs.getInt("SIGNATUREOCCURENCES");
                lang = rs.getString("LANG");
                sobj = new SignatureObject();
                sobj.setWord(word);
                sobj.setOcurrences(ocurrences);
                sobj.setLang(lang);
                if (allSignatures.containsKey(nodeID)) {
                    allSignatures.get(nodeID).add(sobj);
                } else {
                    list = new ArrayList<SignatureObject>();
                    list.add(sobj);
                }
                allSignatures.put(nodeID, list);
            }
        } catch (SQLException e) {
            LOG.error("SQL Error in getAllSignatures()", e);
            throw e;
        } catch (Exception e) {
            LOG.error("General Error in getAllSignatures()", e);
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                LOG.error("General Error in getAllSignatures() closing resources", e);
                throw e;
            }
        }
        return allSignatures;
    }

    @Override
    public Map<Long, List<SignatureObject>> getSignaturesByCorpusID(Long corpusID) throws SQLException, Exception {
        LOG.debug("get signatures by corpusID");
         Map<Long, List<SignatureObject>> allSignatures = new HashMap<Long, List<SignatureObject>>();
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        try {
            conn = createConnection();
            pst = conn.prepareStatement( "SELECT N.NODEID, S.SIGNATUREWORD, S.SIGNATUREOCCURENCES, S.LANG FROM NODE N, SIGNATURE S WHERE N.NODEID = S.NODEID AND N.CORPUSID = ?");
            pst.setLong(1, corpusID);
            rs = pst.executeQuery();           
            Long nodeID = null;
            String word = "";
            int ocurrences;
            String lang = "";
            SignatureObject sobj = null;
            List<SignatureObject> list = null;
            while (rs.next()) {
                nodeID = rs.getLong("NODEID");
                word = rs.getString("SIGNATUREWORD");
                ocurrences = rs.getInt("SIGNATUREOCCURENCES");
                lang = rs.getString("LANG");
                sobj = new SignatureObject();
                sobj.setWord(word);
                sobj.setOcurrences(ocurrences);
                sobj.setLang(lang);
                if (allSignatures.containsKey(nodeID)) {
                    allSignatures.get(nodeID).add(sobj);
                } else {
                    list = new ArrayList<SignatureObject>();
                    list.add(sobj);
                }
                allSignatures.put(nodeID, list);
            }
        } catch (SQLException e) {
            LOG.error("SQL Error in getAllSignatures()", e);
            throw e;
        } catch (Exception e) {
            LOG.error("General Error in getAllSignatures()", e);
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pst != null) {
                    pst.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                LOG.error("General Error in getAllSignatures() closing resources", e);
                throw e;
            }
        }
        return allSignatures;
    }
}
