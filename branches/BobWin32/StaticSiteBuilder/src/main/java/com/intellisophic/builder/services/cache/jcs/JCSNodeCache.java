package com.intellisophic.builder.services.cache.jcs;

import com.intellisophic.builder.business.beans.Node;
import java.util.List;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.access.exception.InvalidArgumentException;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class JCSNodeCache {

    private static final Logger log = Logger.getLogger(JCSNodeCache.class);
    private JCS nodeCache = null;
    private JCS rootNodeCache = null;
    private static final String NODE_CACHE_IDENTIFIER = "nodeCache";
    private static final String ROOT_CACHE_IDENFIFIER = "rootNodeCache";

    /**
     * Class default constructor.
     */
    public JCSNodeCache() {
        log.debug("Node Cache Constructor.");
        try {
            // Load the cache
            nodeCache = JCS.getInstance(NODE_CACHE_IDENTIFIER);
            rootNodeCache = JCS.getInstance(ROOT_CACHE_IDENFIFIER);
            log.info("Node Cache Initialized.");
        } catch (CacheException ex) {
            log.error("An exception ocurred.", ex);
        }
    }

    /**
     * Method that allows to add new node in the cache.
     *
     * @param node {@code Node} to store
     * @see com.intellisophic.builder.business.beans.Node
     */
    public void addNode(Node node) {
        log.debug("addNode(Node)");
        try {
            if (node == null) {
                throw new InvalidArgumentException("The object to store can not be null");
            }
            if (node.getId() == null || node.getId() <= 0) {
                throw new InvalidArgumentException("The object properties can not be null or negative in " + node.getId());
            }
            nodeCache.put(node.getId(), node);
        } catch (InvalidArgumentException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        } catch (CacheException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        }
    }

    /**
     * Method that allows to add new nodes in the cache.
     *
     * @param nodeList
     * @see java.util.List
     * @see com.intellisophic.builder.business.beans.Node
     */
    public void addNodes(List<Node> nodeList) {
        log.debug("addNodes(List<Node>)");
        try {
            for (Node node : nodeList) {
                addNode(node);
            }
        } catch (Exception ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        }
    }

    /**
     * Method to return the node for a given nodeID.
     *
     * @param nodeID Node Identifier.
     * @return Node for the given nodeID, null if not found.
     */
    public Node getNode(Long nodeID) {
        log.debug("getNode(Long)");
        return (Node) nodeCache.get(nodeID);
    }

    /**
     * Method to remove a Node from the cache given the nodeID.
     *
     * @param nodeID Node Identifier.
     */
    public void removeNode(Long nodeID) {
        log.debug("removeNode(Long)");
        try {
            nodeCache.remove(nodeID);
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method that allows to add new root node in the cache.
     *
     * @param rootNode Must haves of the given node
     */
    public void addRootNode(Node rootNode) {
        log.debug("addRootNode(Node)");
        try {
            if (rootNode == null) {
                throw new InvalidArgumentException("The object to store can not be null");
            }
            if (rootNode.getCorpusID() == null || rootNode.getCorpusID() <= 0) {
                throw new InvalidArgumentException("The object properties can not be null or negative in " + rootNode.getCorpusID());
            }
            rootNodeCache.put(rootNode.getCorpusID(), rootNode);
        } catch (InvalidArgumentException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        } catch (CacheException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        }
    }

    /**
     * Method to return the root node for a given a corpusID.
     *
     * @param corpusID Node Identifier.
     * @return Node for the given nodeID, null if not found.
     */
    public Node getRootNode(Long corpusID) {
        log.debug("getRootNode(Long)");
        return (Node) rootNodeCache.get(corpusID);
    }

    /**
     * Method to remove a Node from the cache given the corpusID.
     *
     * @param corpusID Node Identifier.
     */
    public void removeRootNode(Long corpusID) {
        log.debug("removeRootNode(Long)");
        try {
            rootNodeCache.remove(corpusID);
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method to close the Cache when the application shutdown.
     */
    public static void shutdown() {
        CompositeCacheManager compositeCacheManager = CompositeCacheManager.getInstance();
        compositeCacheManager.release();
    }
}
