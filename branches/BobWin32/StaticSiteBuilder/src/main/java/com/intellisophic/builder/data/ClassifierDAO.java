package com.intellisophic.builder.data;

import com.intellisophic.builder.business.beans.CorpusObject;
import com.intellisophic.builder.data.interfaces.IClassifierDAO;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.sql.*;
import java.util.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class ClassifierDAO extends MySQLConnectionFactory implements IClassifierDAO {

    private static final Logger log = Logger.getLogger(ClassifierDAO.class);
    private static final ResourceBundle systemResource = ResourceBundle.getBundle("system/config");
    private static final ResourceBundle sqlResource = ResourceBundle.getBundle("database/sql");
    private static final ResourceBundle sqlConfig = ResourceBundle.getBundle("database/config");
    private static final int DEFAULT_RESULT_LIMIT = 100;
    private Connection connection = null;

    public ClassifierDAO(Connection conn) {
        this.connection = conn;
    }

    /**
     * Method to get the data from the classifier db by corpus id.
     *
     * @param corpusID Identifier of the taxonomy.
     * @return {@code Map<Long, List<CorpusObject>>} Map containing the info from the db [Key=NodeID,Value=List<CorpusObject>]
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public Map<Long, List<CorpusObject>> getClassifierDataForCorpusID(Long corpusID) throws SQLException, Exception {
        log.debug("getClassifierDataForCorpusID()");
        if (connection == null) {
            throw new Exception("The connection can not be null.");
        }
        Map<Long, List<CorpusObject>> classifierResultComplete = new HashMap<Long, List<CorpusObject>>();
        List<CorpusObject> classifierResult;
        CorpusObject corpusObj;
        Statement stmt = null;
        ResultSet rs = null;
        String query;
        String docTitle;
        String limit;
        try {
            limit = systemResource.getString("builder.linksperpage");
            if (limit != null && !"-1".equals(limit.trim())) {
                //The parameter has been set.
                query = sqlResource.getString("CLASSIFIER_FILTERED_QUERY");
                query = query.replace("%%limit%%", limit);
            } else {
                query = sqlResource.getString("CLASSIFIER_DEFAULT_QUERY");
            }
            query = query.replace("%%tablename%%", systemResource.getString("classifier.table.prefix") + corpusID);
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            Long nodeID = null;
            while (rs.next()) {
                corpusObj = new CorpusObject();
                nodeID = rs.getLong("NODEID");
                corpusObj.setNodeID(nodeID);
                corpusObj.setURI(rs.getString("URI"));
                corpusObj.setWebCacheURI(rs.getString("WEBCACHEURI"));
                docTitle = rs.getString("DOCTITLE");
                if (docTitle == null) {
                    docTitle = "";
                }
                corpusObj.setDocTitle(docTitle);
                corpusObj.setScore01(rs.getFloat("SCORE1"));
                corpusObj.setScore02(rs.getFloat("SCORE2"));
                corpusObj.setPagerank(rs.getFloat("PAGERANK"));

                //Save data in map where the key is the NodeID.
                //We have to check if the map already contains the nodeID.
                if (classifierResultComplete.containsKey(nodeID)) {
                    //Retrieve the list to add the new element.
                    classifierResult = classifierResultComplete.get(nodeID);
                    classifierResult.add(corpusObj);
                } else {
                    //If the element does not exist, we must create the new list and add the element.
                    classifierResult = new ArrayList<CorpusObject>();
                    classifierResult.add(corpusObj);
                }
                //Update the Map with the new List for the NodeID
                classifierResultComplete.put(nodeID, classifierResult);
            }
        } catch (CommunicationsException e) {
            log.error("Error in getClassifierDataForCorpusID (ConnectionFailure). " + e.getLocalizedMessage());
            throw e;
        } catch (SQLException e) {
            log.error("Error in getClassifierDataForCorpusID (SQLFailure). " + e.getLocalizedMessage());
            throw e;
        } catch (Exception e) {
            log.error("Error in getClassifierDataForCorpusID. " + e.getLocalizedMessage());
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                log.error("General Error in getClassifierDataForCorpusID() closing resources");
                throw e;
            }
        }
        return classifierResultComplete;
    }

    /**
     * Method to get the data from the classifier db using the corpus and node IDs.
     *
     * @param corpusID Taxonomy identifier
     * @param nodeID Node identifier.
     * @return {@code List<CorpusObject>} List containing the information for the node from db.
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public List<CorpusObject> getClassifierDataForNodeId(Long corpusID, Long nodeID) throws SQLException, Exception {
        log.debug("getClassifierDataForNodeId(Long, Long)");
        if (connection == null) {
            throw new Exception("The connection can not be null.");
        }
        List<CorpusObject> classifierResult;
        CorpusObject corpusObj;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query;
        String docTitle;
        String strResultLimit;
        int resultLimit;
        try {
            strResultLimit = sqlConfig.getString("database.results.limit");
            if (strResultLimit == null || strResultLimit.isEmpty()) {
                log.warn("The property 'database.results.limit' was not found using the default value (" + DEFAULT_RESULT_LIMIT + ")");
                resultLimit = new Integer(DEFAULT_RESULT_LIMIT);
            } else {
                log.debug("The limit for the query results was set to: " + strResultLimit);
                resultLimit = new Integer(strResultLimit);
            }
            classifierResult = new ArrayList<CorpusObject>();
            query = sqlResource.getString("CLASSIFIER_BYNODE_QUERY");
            query = query.replace("%%tablename%%", systemResource.getString("classifier.table.prefix") + corpusID);
            pstmt = connection.prepareStatement(query);
            pstmt.setLong(1, nodeID);
            rs = pstmt.executeQuery();

            String URL;
            String webCacheURI;
            int counter = 0;
            while (rs.next()) {
                corpusObj = new CorpusObject();
                corpusObj.setNodeID(nodeID);
                URL = rs.getString("URI");
                corpusObj.setURI(URL);
                webCacheURI = rs.getString("WEBCACHEURI");
                corpusObj.setWebCacheURI(webCacheURI);
                docTitle = rs.getString("DOCTITLE");
                if (docTitle == null) {
                    docTitle = "";
                }
                corpusObj.setDocTitle(docTitle);
                corpusObj.setScore01(rs.getFloat("SCORE1"));
                corpusObj.setScore02(rs.getFloat("SCORE2"));
                corpusObj.setPagerank(rs.getFloat("PAGERANK"));
                classifierResult.add(corpusObj);
                counter++;
                if (counter >= resultLimit) {
                    break;
                }
            }
        } catch (CommunicationsException e) {
            log.error("Error in getClassifierDataForCorpusID (ConnectionFailure). ", e);
            throw e;
        } catch (SQLException e) {
            log.error("Error in getClassifierDataForCorpusID (SQLFailure). ", e);
            throw e;
        } catch (Exception e) {
            log.error("Error in getClassifierDataForCorpusID. ", e);
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (Exception e) {
                log.error("General Error in getClassifierDataForNodeId() closing resources");
                throw e;
            }
        }
        return classifierResult;
    }

    /**
     * Method to get the list of nodes id of the nodes that contain information.
     *
     * @param corpusID Taxonomy identifier
     * @return {@code List<String>} List containing the information for the taxonomy
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public List<Long> getClassifiedNodesByCorpusID(Long corpusID) throws SQLException, Exception {
        log.debug("getClassifiedNodesByCorpusID(Long)");
        if (getConnection() == null) {
            throw new Exception("The connection can not be null.");
        }
        List<Long> classifierResult;
        Statement stmt = null;
        ResultSet rs = null;
        String query;
        Long nodeID;
        try {
            classifierResult = new ArrayList<Long>();
            query = sqlResource.getString("CLASSIFIER_BYNODE_CLASSIFIED_QUERY");
            query = query.replace("%%tablename%%", systemResource.getString("classifier.table.prefix") + corpusID);
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                nodeID = rs.getLong("NODEID");
                classifierResult.add(nodeID);
            }
        } catch (CommunicationsException e) {
            log.error("Error in getClassifiedNodesByCorpusID (ConnectionFailure). " + e.getLocalizedMessage());
            throw e;
        } catch (SQLException e) {
            log.error("Error in getClassifiedNodesByCorpusID (SQLFailure). " + e.getLocalizedMessage());
            throw e;
        } catch (Exception e) {
            log.error("Error in getClassifiedNodesByCorpusID. " + e.getLocalizedMessage());
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                log.error("General Error in getClassifiedNodesByCorpusID(Long) closing resources");
                throw e;
            }
        }
        return classifierResult;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
