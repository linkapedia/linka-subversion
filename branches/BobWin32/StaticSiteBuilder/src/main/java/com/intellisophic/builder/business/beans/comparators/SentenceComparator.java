package com.intellisophic.builder.business.beans.comparators;

import com.intellisophic.builder.view.beans.SummaryObject;
import java.util.Comparator;

/**
 *
 * @author andres
 */
public class SentenceComparator implements Comparator<SummaryObject> {

    @Override
    public int compare(SummaryObject o1, SummaryObject o2) {
        double totalScore1 = o1.getScoreByNodeTitle() + o1.getScoreByMustHaves() + o1.getScoreBySignatures();
        double totalScore2 = o2.getScoreByNodeTitle() + o2.getScoreByMustHaves() + o2.getScoreBySignatures();
        return (totalScore1 < totalScore2) ? 1 : (totalScore1 > totalScore2) ? -1 : 0;
    }
}
