package com.intellisophic.builder.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author andres
 */
public class InterestCircleInfo {

    private String corpusId;
    private String taxonomyName;
    private String interestCircleName;
    private String author;
    private String imageName;
    private String followers;
    private static List<InterestCircleInfo> listInfo;

    static {
        listInfo = new ArrayList<InterestCircleInfo>();
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }   
    
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getInterestCircleName() {
        return interestCircleName;
    }

    public void setInterestCircleName(String interestCircleName) {
        this.interestCircleName = interestCircleName;
    }

    public String getTaxonomyName() {
        return taxonomyName;
    }

    public void setTaxonomyName(String taxonomyName) {
        this.taxonomyName = taxonomyName;
    }

    synchronized public static void addElement(InterestCircleInfo cIbterest) {
        listInfo.add(cIbterest);
    }

    public static List<InterestCircleInfo> getAllElements() {
        return listInfo;
    }

    synchronized public static List<InterestCircleInfo> getRamdomElementsByTax(int number, String tax) {
        List<InterestCircleInfo> listAux = new ArrayList<InterestCircleInfo>();
        List<InterestCircleInfo> listToReturn = new ArrayList<InterestCircleInfo>();
        for (InterestCircleInfo ic : listInfo) {
            if (ic.getCorpusId().equals(tax)) {
                listAux.add(ic);
            }
        }
        
        if(listAux.size()<=number){
            return listAux;
        }
        Random ram = new Random(System.currentTimeMillis());
        for (int i = 0; i < number; i++) {           
            int r =ram.nextInt(listAux.size());
            listToReturn.add(listAux.get(r));
            listAux.remove(r);
        }
        
        return listToReturn;

    }
}
