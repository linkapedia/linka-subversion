package com.intellisophic.builder.view.beans;

import com.intellisophic.builder.business.beans.Node;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author Andrés Restrepo
 */
public class ViewNode {

    private Long nodeId;
    private String nodeTitle;
    private String nodeDesc;
    private List<ViewLink> links;
    private String nodeMustHaves;
    private String path;
    private Long parentId;
    private String pathPrefix;
    private int _nodeIndex;
    private Node rootNode;
    
    private boolean bridgeNode;
    private Map<Long,String> bridgeChildsInfo;

    public int getNodeIndex() {
        return _nodeIndex;
    }

    public void setNodeIndex(int nodeIndex) {
        _nodeIndex = nodeIndex;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeTitle() {
        return nodeTitle;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }

    public List<ViewLink> getLinks() {
        return links;
    }

    public void setLinks(List<ViewLink> links) {
        this.links = links;
    }

    public String getNodeMustHaves() {
        return nodeMustHaves;
    }

    public void setNodeMustHaves(String nodeMustHaves) {
        this.nodeMustHaves = nodeMustHaves;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPathPrefix() {
        return pathPrefix;
    }

    public void setPathPrefix(String pathPrefix) {
        this.pathPrefix = pathPrefix;
    }

    public String getNodeDesc() {
        return nodeDesc;
    }

    public void setNodeDesc(String nodeDesc) {
        this.nodeDesc = nodeDesc;
    }

    public Node getRootNode() {
        return rootNode;
    }

    public void setRootNode(Node rootNode) {
        this.rootNode = rootNode;
    }

    public boolean isBridgeNode() {
        if(getLinks()!=null){
            bridgeNode = getLinks().isEmpty();
        } else {
            bridgeNode = true;
        }
        return bridgeNode;
    }

    public void setBridgeNode(boolean bridgeNode) {
        this.bridgeNode = bridgeNode;
    }

    public Map<Long, String> getBridgeChildsInfo() {
        return bridgeChildsInfo;
    }

    public void setBridgeChildsInfo(Map<Long, String> bridgeChildsInfo) {
        this.bridgeChildsInfo = bridgeChildsInfo;
    }

    public String asXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<item id='");
        sb.append(getNodeId());
        sb.append("' parent_id='");
        sb.append(getParentId());
        sb.append("' index='");
        sb.append(getNodeIndex());
        sb.append("'><content><name href='");
        if (getPathPrefix() != null && !getPathPrefix().trim().equals("")) {
            sb.append(StringEscapeUtils.escapeXml("/" + getPathPrefix() + "/" + getPath()));
        } else {
            sb.append(StringEscapeUtils.escapeXml(getPath()));
        }
        sb.append(".html'>");
        sb.append(StringEscapeUtils.escapeXml(getNodeTitle().trim()));
        sb.append("</name></content></item>");
        return sb.toString();
    }
}
