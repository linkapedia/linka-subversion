package com.yuxipacific.utils.tika;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;

/**
 *
 * @author Xander Kno
 */
public class MimeTypeDetector {

    private static final Logger log = Logger.getLogger(MimeTypeDetector.class);

    /**
     * Detect and return MIME Type of the given file.
     *
     * @param file java.io.File to detect MIME Type
     * @return MIME Type of the file as String
     */
    public static String getMimeType(File file) {

        String mimeType = null;

        try {
            // Creating new Instance of org.apache.tika.Tika
            Tika tika = new Tika();

            // Detecting MIME Type of the File 
            mimeType = tika.detect(file);

        } catch (FileNotFoundException e) {
            log.error("Specified File not found.", e);
        } catch (IOException e) {
            log.error("Specified File not found.", e);
        }

        // returning detected MIME Type
        return mimeType;

    }

    /**
     * Detect and return MIME Type of the given file.
     *
     * @param filePath full path of the file to detect MIME Type
     * @return MIME Type of the file as String
     */
    public static String getMimeType(String filePath) {

        // Creating new instance of java.io.File for given file path
        File f = new File(filePath);

        // Getting and returning MIME Type of the file
        return getMimeType(f);

    }
}
