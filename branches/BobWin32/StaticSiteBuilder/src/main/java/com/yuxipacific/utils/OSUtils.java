package com.yuxipacific.utils;

/**
 * Class to check the properties of the system.
 * @author Alex
 */
public final class OSUtils {

    private static String OS = null;
    private static String Architecture = null;

    public static String getOsName() {
        if (OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    public static String getOsArchitecture() {
        if (Architecture == null) {
            Architecture = System.getProperty("os.arch");
        }
        return Architecture;
    }

    public static boolean isWindows() {
        //windows
        return (getOsName().toLowerCase().indexOf("win") >= 0);

    }

    public static boolean isMac() {
        //Mac
        return (getOsName().toLowerCase().indexOf("mac") >= 0);

    }

    public static boolean isUnix() {
        //linux or unix
        return (getOsName().toLowerCase().indexOf("nix") >= 0 || getOsName().toLowerCase().indexOf("nux") >= 0);
    }
}
