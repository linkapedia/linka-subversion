package com.yuxipacific.utils.tika;

import java.io.File;
import java.io.FileInputStream;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;

/**
 *
 * @author Xander Kno
 */
public class FileHandler {

    private static final Logger log = Logger.getLogger(FileHandler.class);
    private static final ContentHandler contenthandler = new BodyContentHandler(-1);

    /**
     *
     * @param file
     * @return
     */
    public static Metadata getMetadata(File file) {
        log.debug("getMetadataFromFile(File)");
        if (file == null) {
            log.error("The file cannot be null.");
            return null;
        }
        if (!file.isFile() || !file.exists()) {
            log.warn("The file {" + file.getAbsolutePath() + "} does not exist or it's not a file.");
            return null;
        }
        FileInputStream is = null;
        Metadata metadata = null;
        try {
            is = new FileInputStream(file);
            metadata = new Metadata();
            metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
            ParseContext pc = new ParseContext();
            Parser parser = new AutoDetectParser();
            parser.parse(is, contenthandler, metadata, pc);
            is.close();
            is = null;
        } catch (Exception e) {
            log.error("An exception occured in getting the Document metadata for file: " + file.getAbsolutePath(), e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                    is = null;
                } catch (Exception e) {
                    log.error("An exception occured in getting the Document metadata for file: " + file.getAbsolutePath(), e);
                }
            }
        }
        return metadata;
    }

    /**
     *
     * @param file
     * @return
     */
    public static String getContents(File file) {
        log.debug("getContents(File)");
        if (file == null) {
            log.error("The file cannot be null.");
            return null;
        }
        if (!file.isFile() || !file.exists()) {
            log.warn("The file {" + file.getAbsolutePath() + "} does not exist or it's not a file.");
            return null;
        }
        String documentContent = null;
        try {
            Tika tika = new Tika();
            documentContent = tika.parseToString(file);
        } catch (Exception e) {
            log.error("An exception ocurred. ", e);
        }
        return documentContent;
    }
}
