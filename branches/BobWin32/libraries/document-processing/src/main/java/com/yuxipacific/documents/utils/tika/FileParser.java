package com.yuxipacific.documents.utils.tika;

import java.io.File;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;

/**
 *
 * @author Xander Kno
 */
public class FileParser {

    private static final Logger log = Logger.getLogger(FileParser.class);

    public static String parse(File file) {
        log.debug("parse(File)");
        String documentContent = null;
        try {
            Tika tika = new Tika();
            documentContent = tika.parseToString(file);
        } catch (Exception e) {
            log.error("An exception ocurred. ", e);
        }
        return documentContent;
    }
}
