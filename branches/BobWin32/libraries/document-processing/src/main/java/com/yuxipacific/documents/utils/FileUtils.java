package com.yuxipacific.documents.utils;

import com.yuxipacific.documents.storage.exceptions.parser.EmptyDocumentException;
import com.yuxipacific.documents.utils.tika.MimeTypeDetector;
import java.io.*;
import java.net.URL;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Alex
 */
public class FileUtils {

    private static final Logger log = Logger.getLogger(FileUtils.class);
    private static final String DEFAULT_HTML_TYPE = "text/html";
    private static final Object fileLocker = new Object();

    /**
     * Method to handle the writes of files on disk.
     *
     * @param fileName File name
     * @param filePath Path where the file should be written.
     * @param fileContent Content of the file.
     * @return Full file path on disk.
     * @throws IOException
     * @throws EmptyDocumentException
     */
    public static synchronized String writeFile(String fileName, String filePath, String fileContent) throws IOException, EmptyDocumentException {
        log.debug("writeFile(String, String, String)");
        FileOutputStream fos = null;
        String _tmpLocalPath = null;
        File parentFolders;
        if (fileContent == null || fileContent.isEmpty()) {
            throw new EmptyDocumentException("To create the file the content must be provided.");
        }
        try {
            if (fileName != null && !"".equals(fileName.trim()) && fileContent != null && !"".equals(fileContent.trim())) {
                _tmpLocalPath = filePath + fileName;
                log.debug("Creating file: " + _tmpLocalPath);
                synchronized (fileLocker) {
                    log.debug("Synchronized File Write.");
                    //Create the parent folders if needed.
                    parentFolders = new File(_tmpLocalPath);
                    parentFolders.getParentFile().mkdirs();
                    // Create file 
                    fos = new FileOutputStream(_tmpLocalPath);
                    try {
                        fos.getChannel().lock();
                        log.debug("File lock acquired");
                    } catch (Exception e) {
                        log.error("Cannot lock the file. " + e.getLocalizedMessage());
                        fileLocker.wait();
                    }
                    fos.write(fileContent.getBytes("UTF-8"));
                    fos.flush();
                    //Close the output stream
                    fos.close();
                    log.debug("Released Lock");
                    fileLocker.notify();
                }
            }
        } catch (Exception ex) {
            log.error("An exception ocurred: ", ex);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    log.error("An exception ocurred: ", e);
                }
            }
        }
        return _tmpLocalPath;
    }

    /**
     * Method to read a text file
     *
     * @param file File to read from.
     * @return File content.
     */
    public static String readTextFile(File file) {
        log.debug("readTextFile(File)");
        StringBuilder fileContent = null;
        String strLine;
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        BufferedReader br = null;

        try {
            fileContent = new StringBuilder();
            fis = new FileInputStream(file);
            // Here BufferedInputStream is added for fast reading.
            bis = new BufferedInputStream(fis);
            br = new BufferedReader(new InputStreamReader(bis));
            while ((strLine = br.readLine()) != null) {
                fileContent.append(strLine);
            }

            // dispose all the resources after using them.
            fis.close();
            bis.close();
            br.close();
            fis = null;
            bis = null;
            br = null;
        } catch (FileNotFoundException ex) {
            log.error("The file was not found.", ex);
        } catch (IOException ex) {
            log.error("There was a exception reading the file.", ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (bis != null) {
                    bis.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                log.error("An exception ocurred closing the streams.", e);
            }
        }
        return fileContent.toString();
    }

    /**
     * Method to get the file name without the extension.
     *
     * @param file File to get the name from.
     * @return File name.
     */
    public static String getFileNameWithoutExtension(File file) {
        log.debug("getFileNameWithoutExtension(File)");
        String fileName = null;
        try {
            if (file != null && file.exists()) {
                if (file.isFile()) {
                    fileName = file.getAbsolutePath();
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));
                    fileName = fileName.substring(fileName.lastIndexOf(File.separator) + 1, fileName.length());
                    log.info("File Title: " + fileName);
                }
            }
        } catch (Exception e) {
            log.error("An exception ocurred", e);
        }
        return fileName;
    }

    /**
     * Method to get the Mime Type of the given file.
     *
     * @param file File to get the mime type from.
     * @return Mime type.
     */
    public static String getFileMimeType(File file) {
        log.debug("getFileMimeType(File)");
        return MimeTypeDetector.getMimeType(file);
    }

    /**
     * Method to get the Mime Type of the given file.
     *
     * @param is Input stream to get the mime type.
     * @return Mime type.
     */
    public static String getFileMimeType(InputStream is) {
        log.debug("getFileMimeType(InputStream)");
        return MimeTypeDetector.getMimeType(is);
    }

    /**
     * Method to get the doc title from a given file.
     * @param file File to get the doc title from.
     * @return Document Title.
     */
    public static String getDocTitleFromFile(File file) {
        log.debug("getFileTitle(File)");
        if (file == null) {
            log.error("The file cannot be null.");
            return null;
        }
        if (!file.isFile() || !file.exists()) {
            log.warn("The file {" + file.getAbsolutePath() + "} does not exist or it's not a file.");
            return null;
        }
        return getMetadata(file).get(Metadata.TITLE);
    }

    /**
     * Method to get the full document content from a given file.
     * @param file File to get the content from.
     * @return Full document content. 
     */
    public static String getFullDocumentContent(File file) {
        log.debug("getFullDocumentContent(File)");
        return getContents(file);
    }

    /**
     * Method to get the metadata from a given file.
     * @param file File to get the metadata from.
     * @return File metadata Object.
     */
    public static Metadata getMetadata(File file) {
        log.debug("getMetadata(File)");
        ParseContext context = new ParseContext();
        Detector detector = new DefaultDetector();
        Parser parser = new AutoDetectParser(detector);
        context.set(Parser.class, parser);
        Metadata metadata = new Metadata();
        InputStream input = null;

        try {
            metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
            URL url = file.toURI().toURL();
            input = TikaInputStream.get(url, metadata);
            MediaType mediaType = detector.detect(input, metadata);
            log.debug("File type detected for: " + file.getName() + " is: " + mediaType.getType() + ", the subtype is: " + mediaType.getSubtype());

            ContentHandler handler = new BodyContentHandler(-1);
            if ("xhtml+xml".equals(mediaType.getSubtype())) {
                log.debug("Changing mimetype to html");
                metadata.set(Metadata.CONTENT_TYPE, DEFAULT_HTML_TYPE);
                new HtmlParser().parse(input, handler, metadata, new ParseContext());
            } else {
                parser.parse(input, handler, metadata, context);
            }
        } catch (SAXException ex) {
            log.error("An exception has ocurred: ", ex);
        } catch (TikaException ex) {
            log.error("An exception has ocurred: ", ex);
        } catch (IOException ex) {
            log.error("An exception has ocurred: ", ex);
        } finally {
            try {
                input.close();
            } catch (IOException ex) {
                log.error("An exception has ocurred: ", ex);
            }
        }
        return metadata;
    }

    /**
     * Method to get the file contents.
     * @param file File to get the content from.
     * @return File Content.
     */
    public static String getContents(File file) {
        log.debug("getContents(File)");
        if (file == null) {
            log.error("The file cannot be null.");
            return null;
        }
        if (!file.isFile() || !file.exists()) {
            log.warn("The file {" + file.getAbsolutePath() + "} does not exist or it's not a file.");
            return null;
        }
        String documentContent = null;
        try {
            Tika tika = new Tika();
            documentContent = tika.parseToString(file);
        } catch (Exception e) {
            log.error("An exception ocurred. ", e);
        }
        return documentContent;
    }
}
