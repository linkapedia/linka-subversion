package com.yuxipacific.documents.parsers;

import com.yuxipacific.documents.storage.exceptions.parser.NoParseableDocumentException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.apache.log4j.Logger;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Xander Kno
 */
public class FileParser {

    private static final Logger log = Logger.getLogger(FileParser.class);
    private static final String DEFAULT_HTML_TYPE = "text/html";
    private static final ParseContext context = new ParseContext();
    private static final Detector detector = new DefaultDetector();
    private static final Parser parser = new AutoDetectParser(detector);
    private static final Metadata metadata = new Metadata();

    static {
        context.set(Parser.class, parser);
    }

    public synchronized static String parse(File file) throws NoParseableDocumentException {
        log.debug("parse(File)");
        InputStream input = null;
        String docContent = null;

        try {
            metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
            URL url = file.toURI().toURL();
            input = TikaInputStream.get(url, metadata);
            MediaType mediaType = detector.detect(input, metadata);
            log.debug("File type detected for: " + file.getName() + " is: " + mediaType.getType() + ", the subtype is: " + mediaType.getSubtype());

            ContentHandler handler = new BodyContentHandler(-1);
            if ("xhtml+xml".equals(mediaType.getSubtype())) {
                log.debug("Changing mimetype to html");
                metadata.set(Metadata.CONTENT_TYPE, DEFAULT_HTML_TYPE);
                new HtmlParser().parse(input, handler, metadata, new ParseContext());
            } else {
                parser.parse(input, handler, metadata, context);
            }

            docContent = handler.toString();
        } catch (SAXException ex) {
            log.error("An exception has ocurred: ", ex);
            throw new NoParseableDocumentException(ex);
        } catch (TikaException ex) {
            log.error("An exception has ocurred: ", ex);
            throw new NoParseableDocumentException(ex);
        } catch (IOException ex) {
            log.error("An exception has ocurred: ", ex);
            throw new NoParseableDocumentException(ex);
        } finally {
            try {
                input.close();
            } catch (IOException ex) {
                log.error("An exception has ocurred: ", ex);
            }
        }
        return docContent;
    }
}