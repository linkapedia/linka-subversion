package com.yuxipacific.documents.storage.exceptions.download;

/**
 *
 * @author Xander Kno
 */
public class DeniedDownloadException extends Exception {

    /**
     * Creates a new instance of
     * <code>DeniedDownloadException</code> without detail message.
     */
    public DeniedDownloadException() {
    }

    /**
     * Constructs an instance of
     * <code>DeniedDownloadException</code> with the specified detail message.
     * <p/>
     * @param msg the detail message.
     */
    public DeniedDownloadException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of
     * <code>DeniedDownloadException</code> with the specified throwable object.
     * <p/>
     * @param cause the cause of the exception.
     */
    public DeniedDownloadException(Throwable cause) {
        super(cause);
    }
}
