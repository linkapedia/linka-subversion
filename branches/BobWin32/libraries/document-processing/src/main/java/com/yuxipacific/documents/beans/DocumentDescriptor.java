package com.yuxipacific.documents.beans;

import java.io.Serializable;

/**
 *
 * @author Xander Kno
 */
public class DocumentDescriptor implements Serializable {

    /**
     * Source URL for the document.
     */
    private String srcURL = null;
    /**
     * Storage URI of the document in the local file system.
     */
    private String dstURL = null;
    /**
     * Storage URI of the parsed version of the document.
     */
    private String parsedDocURL = null;
    /**
     * Storage URL of the extracted version of the document.
     */
    private String extractedDocURL = null;
    /**
     * Handles the document title returned from Google API through Json!
     */
    private String title = null;
    /**
     * Identifies if the file was rejected or not by the download or parsing
     * process to avoid to send the file to the classifier.
     */
    private boolean rejected = false;
    /**
     * The reason of the rejection, if exists.
     */
    private String rejectedReason = null;

    public DocumentDescriptor() {
    }

    /**
     * Storage URI of the document in the local file system
     * <p/>
     * @return Local file system path.
     */
    public String getDstURL() {
        return dstURL;
    }

    public void setDstURL(String dstURL) {
        this.dstURL = dstURL;
    }

    public String getParsedDocURL() {
        return parsedDocURL;
    }

    public void setParsedDocURL(String parsedDocURL) {
        this.parsedDocURL = parsedDocURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSrcURL() {
        return srcURL;
    }

    public void setSrcURL(String srcURL) {
        this.srcURL = srcURL;
    }

    public String getExtractedDocURL() {
        return extractedDocURL;
    }

    public void setExtractedDocURL(String extractedDocURL) {
        this.extractedDocURL = extractedDocURL;
    }

    public boolean isRejected() {
        return rejected;
    }

    public void setRejected(boolean rejected) {
        this.rejected = rejected;
    }

    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DocumentDescriptor{srcURL=").append(srcURL).
                append(", dstURL=").append(dstURL).append(", parsedDocURL=").append(parsedDocURL).
                append(", extractedDocURL=").append(extractedDocURL).
                append(", rejected=").append(rejected).
                append(", rejectedReason=").append(rejectedReason).append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DocumentDescriptor other = (DocumentDescriptor) obj;
        if ((this.srcURL == null) ? (other.srcURL != null) : !this.srcURL.equals(other.srcURL)) {
            return false;
        }
        if ((this.dstURL == null) ? (other.dstURL != null) : !this.dstURL.equals(other.dstURL)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.srcURL != null ? this.srcURL.hashCode() : 0);
        hash = 53 * hash + (this.dstURL != null ? this.dstURL.hashCode() : 0);
        return hash;
    }

    public boolean isValid() {
        if (this.rejected) {
            return false;
        }
        if (this.srcURL == null || this.srcURL.isEmpty()) {
            return false;
        }
        if (this.dstURL == null || this.dstURL.isEmpty()) {
            return false;
        }
        return true;
    }
}
