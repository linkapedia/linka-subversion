package com.yuxipacific.generic.html.utils;

import org.apache.log4j.Logger;

/**
 * Class to handle all the basic HTML operations
 * <p/>
 * @author Xander Kno
 */
public class HTMLUtils {

    private static final Logger log = Logger.getLogger(HTMLUtils.class);
    private static final String HTML_TRASITIONAL_HEADER = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    private static final String HTML_OPEN_TAG = "<html>";
    private static final String HTML_CLOSE_TAG = "</html>";
    private static final String HEAD_OPEN_TAG = "<head>";
    private static final String HEAD_CLOSE_TAG = "</head>";
    private static final String BODY_OPEN_TAG = "<body>";
    private static final String BODY_CLOSE_TAG = "</body>";
    private static final String TITLE_OPEN_TAG = "<title>";
    private static final String TITLE_CLOSE_TAG = "</title>";
    private static final String TITLE_REPLACEMENT_KEY = "CAm4nfcHjJt3LU1RoTJ4";
    private static final String BODY_REPLACEMENT_KEY = "mc3o3rJattk7kzkXveTD";

    public static String wrapTextFileIntoHTML(String title, String content) {
        log.debug("wrapTextFileIntoHTML(String, String)");
        if (title == null || title.isEmpty() || content == null || content.isEmpty()) {
            return null;
        }
        String htmlTemplate = getHTMLFileBasicTemplate();
        htmlTemplate = htmlTemplate.replace(TITLE_REPLACEMENT_KEY, title);
        htmlTemplate =htmlTemplate.replace(BODY_REPLACEMENT_KEY, content);
        return htmlTemplate;
    }

    private static String getHTMLFileBasicTemplate() {
        log.debug("getHTMLFileBasicTemplate()");
        StringBuilder sb = new StringBuilder();
        sb.append(HTML_TRASITIONAL_HEADER);
        sb.append(HTML_OPEN_TAG);
        sb.append(HEAD_OPEN_TAG);
        sb.append(TITLE_OPEN_TAG);
        sb.append(TITLE_REPLACEMENT_KEY);
        sb.append(TITLE_CLOSE_TAG);
        sb.append(HEAD_CLOSE_TAG);
        sb.append(BODY_OPEN_TAG);
        sb.append(BODY_REPLACEMENT_KEY);
        sb.append(BODY_CLOSE_TAG);
        sb.append(HTML_CLOSE_TAG);
        return sb.toString();
    }
}
