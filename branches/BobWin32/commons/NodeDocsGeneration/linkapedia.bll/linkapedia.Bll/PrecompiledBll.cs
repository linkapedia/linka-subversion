﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using linkapedia.Dao;
using linkapedia.Documents;
using linkapedia.FileUtils;
using linkapedia.InterfacesAndModels;
using linkapedia.Models;
using linkapedia.Rules;
using linkapedia.StringUtils;
using log4net;

namespace linkapedia.Bll
{
    public class PrecompiledBll : IPrecompiledBll
    {
        private readonly ILog log = LogManager.GetLogger(typeof(PrecompiledBll));
        private IRules Rules { get; set; }
        private IProvideFileUtils FileUtils { get; set; }
        private IProvideStringUtils StringUtils { get; set; }
        private IProvideDataForDocuments DataProviderForDocuments { get; set; }
        private IProvideDataForNodeDocuments DataProviderForNodeDocuments { get; set; }
        Stopwatch sw = new Stopwatch();
        private int currentId = 0;
        Dictionary<string, int> dictionaryWords = new Dictionary<string, int>();
        Dictionary<string, int> dictionaryPhrases = new Dictionary<string, int>();

        public bool ProcessWordTable(string fullName)
        {
            log.Warn("BegDictionaryin to get the words");
            List<string> termsWithOutRules = DataProviderForDocuments.GetTerms();
            log.Info("Terms to process:" + termsWithOutRules.Count);
            log.Warn("terms obtanied");
            List<string> termsDistincWithOutRules = termsWithOutRules.Distinct().ToList();
            sw.Start();
            string term = null;
            for (int i = 0; i < termsDistincWithOutRules.LongCount(); i++)
            {
                try
                {
                    if (term == "united")
                    {
                        Console.WriteLine(term);
                    }
                    term = Rules.ApplyRules(termsDistincWithOutRules[i]);

                    if (!string.IsNullOrEmpty(term) && !dictionaryWords.ContainsKey(term))
                    {

                        dictionaryWords.Add(term, currentId);
                        currentId++;
                    }
                    Console.WriteLine("rules words: " + i);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            sw.Stop();
            Console.WriteLine("Time appliyn the rules: " + sw.Elapsed);
            log.Warn("Time appliyn the rules: " + sw.Elapsed);
            sw.Reset();

            log.Info("Words obatined finished");
            log.Info("Distinc names applied");
            FileUtils.WriteFile(fullName, dictionaryWords);
            log.Info("Current id " + currentId);
            return true;
        }


        public bool ProcessPhrasesTable(string fullName)
        {

            log.Warn("Beginning to get phrases");
            List<string> phrasesWithoutRules = DataProviderForDocuments.GetPhrases();
            log.Info("Phrases to process" + phrasesWithoutRules.Count);
            log.Info("Phrases obtained");
            List<string> phrasesDistincWithOutRules = phrasesWithoutRules.Distinct().ToList();
            sw.Start();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < phrasesDistincWithOutRules.LongCount(); i++)
            {
                string[] termsFromPhrase = phrasesDistincWithOutRules[i].Split(' ');

                string term;
                int value = 0;
                foreach (var word in termsFromPhrase)
                {
                    term = Rules.ApplyRules(word);
                    if (!string.IsNullOrEmpty(term) && dictionaryWords.ContainsKey(term))
                    {
                        value = dictionaryWords[term];
                        sb.Append(value.ToString());
                        sb.Append(",");
                        Console.WriteLine("rules phrases: " + i);
                    }
                }
                if (sb.Length > 1)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                if (!dictionaryPhrases.ContainsKey(sb.ToString()))
                {
                    dictionaryPhrases.Add(sb.ToString(), currentId);
                    currentId++;
                }
                sb.Clear();

            }
            sw.Stop();
            Console.WriteLine("Time appliyn the rules: " + sw.Elapsed);
            log.Warn("Time appliyn the rules: " + sw.Elapsed);
            sw.Reset();
            log.Info("phrases obatined finished");
            log.Info("Distinc names applied");
            FileUtils.WriteFile(fullName, dictionaryPhrases);
            log.Info("Current id " + currentId);
            return true;
        }

        public bool ProcessNodeSignaturesTable(string fullpath)
        {
            List<SignatureTerm> signatureTerms = DataProviderForNodeDocuments.GetSignatureTerms();
            List<NodeSignatureOuputElement> nodeSignatureOuputElements = new List<NodeSignatureOuputElement>();
            string term;
            int value = 0;
            decimal weight;
            StringBuilder sb;
            foreach (var signatureTerm in signatureTerms)
            {
                if (!string.IsNullOrEmpty((signatureTerm.Term)))
                {
                    try
                    {
                        weight = (decimal)signatureTerm.Ocurrences / (decimal)signatureTerm.NodeSize;
                    }
                    catch (DivideByZeroException e)
                    {
                        log.Error("Error divide by zero",e);
                        weight = 0;

                    }
                    if (StringUtils.IsAPhrase(signatureTerm.Term))
                    {
                        string[] words = signatureTerm.Term.Split(' ');
                        sb = new StringBuilder();


                        foreach (var word in words)
                        {
                            term = Rules.ApplyRules(word);
                            if (!string.IsNullOrEmpty(term) && dictionaryWords.ContainsKey(term))
                            {
                                value = dictionaryWords[term];
                                sb.Append(value);
                                sb.Append(',');
                            }
                        }
                        if (sb.Length > 1)
                        {
                            sb.Remove(sb.Length - 1, 1);
                        }
                        term = sb.ToString();

                        if (dictionaryPhrases.ContainsKey(term))
                        {
                            value = dictionaryPhrases[term];
                            var element = new NodeSignatureOuputElement { NodeId = signatureTerm.NodeId, TermValue = value, TypeId = signatureTerm.NodeType, Weight = weight };
                            nodeSignatureOuputElements.Add(element);
                        }
                    }
                    else
                    {
                        term = Rules.ApplyRules(signatureTerm.Term);
                        if (dictionaryWords.ContainsKey(term))
                        {
                            value = dictionaryWords[term];
                            var element = new NodeSignatureOuputElement
                                              {
                                                  NodeId = signatureTerm.NodeId,
                                                  TermValue = value,
                                                  TypeId = signatureTerm.NodeType,
                                                  Weight = weight
                                              };
                            nodeSignatureOuputElements.Add(element);
                        }
                    }


                }
            }
            log.Warn("********************agrupando por nodes *********************");
            sb = new StringBuilder();
            var q = from n in nodeSignatureOuputElements
                    group n by n.NodeId;
            foreach (var group in q)
            {
                Console.WriteLine("Node: " + group.Key);
                sb.Append(group.Key);
                var first = true;
                foreach (var c in group)
                {
                    if (first)
                    {
                        sb.Append("," + c.TypeId + ",");
                        first = false;
                    }

                    sb.Append(c.TermValue + "," + c.Weight.ToString("0.####").Replace(',', '.'));
                    sb.Append(",");
                }
                sb.Remove(sb.Length - 1, 1);
                //sb.Append("]");
                sb.Append("\n");
            }
            Console.WriteLine(sb.ToString());
            FileUtils.WriteFile(fullpath, sb.ToString());


            return true;
        }

        public bool ProcessNodeMustHaveTable(string fullpath)
        {
            List<MustHaveTerm> mustHaveTerms = DataProviderForNodeDocuments.GetMustHaveTerms();
            List<NodeMustHaveOuputElement> nodeMustHaveOuputElements = new List<NodeMustHaveOuputElement>();
            string term;
            int value = 0;
            StringBuilder sb;
            foreach (var mustHaveTerm in mustHaveTerms)
            {
                if (!string.IsNullOrEmpty((mustHaveTerm.Term)))
                {

                    if (StringUtils.IsAPhrase(mustHaveTerm.Term))
                    {
                        string[] words = mustHaveTerm.Term.Split(' ');
                        sb = new StringBuilder();


                        foreach (var word in words)
                        {
                            term = Rules.ApplyRules(word);
                            if (!string.IsNullOrEmpty(term) && dictionaryWords.ContainsKey(term))
                            {
                                value = dictionaryWords[term];
                                sb.Append(value);
                                sb.Append(',');
                            }
                        }
                        if (sb.Length > 1)
                        {
                            sb.Remove(sb.Length - 1, 1);
                        }
                        
                    
                    term = sb.ToString();

                        if (dictionaryPhrases.ContainsKey(term))
                        {
                            value = dictionaryPhrases[term];
                            var element = new NodeMustHaveOuputElement { NodeId = mustHaveTerm.NodeId, TermValue = value };
                            nodeMustHaveOuputElements.Add(element);
                        }
                    }
                    else
                    {
                        term = Rules.ApplyRules(mustHaveTerm.Term);
                        if (dictionaryWords.ContainsKey(term))
                        {
                            value = dictionaryWords[term];
                            var element = new NodeMustHaveOuputElement
                            {
                                NodeId = mustHaveTerm.NodeId,
                                TermValue = value

                            };
                            nodeMustHaveOuputElements.Add(element);
                        }
                    }

                }
            }
            log.Warn("********************agrupando*********************");
            sb = new StringBuilder();
            var q = from n in nodeMustHaveOuputElements
                    group n by n.NodeId;
            foreach (var group in q)
            {
                Console.WriteLine("Node: " + group.Key);
                sb.Append(group.Key + ",");
                foreach (var c in group)
                {
                    sb.Append(c.TermValue);
                    sb.Append(",");
                }
                sb.Remove(sb.Length - 1, 1);
                //sb.Append("]");
                sb.Append("\n");
            }
            Console.WriteLine(sb.ToString());
            FileUtils.WriteFile(fullpath, sb.ToString());

            log.Warn("********************agrupando por Terms *********************");
            sb.Clear();
            var p = from n in nodeMustHaveOuputElements
                    group n by n.TermValue;
            foreach (var group in p)
            {
                Console.WriteLine("Term: " + group.Key);
                sb.Append(group.Key + ",");
                foreach (var c in group)
                {
                    Console.WriteLine("NodeId: " + c.NodeId);
                    sb.Append(c.NodeId);
                    sb.Append(",");
                }
                sb.Remove(sb.Length - 1, 1);
                // sb.Append("]");
                sb.Append("\n");
            }

            Console.WriteLine(sb.ToString());
            fullpath = "c:\\temp\\codelistTax.txt";
            FileUtils.WriteFile(fullpath, sb.ToString());

            return true;
        }

        public bool ProcessUnfilteredNodeTable(string fullpatth)
        {
            log.Warn("Creating table UnfilteredNode");
            List<string> unfilteredNodeList = DataProviderForNodeDocuments.GetUnfilteredNodeList();
            log.Info("Unfiltered node Count: " + unfilteredNodeList.Count);
            StringBuilder sb = new StringBuilder();
            foreach (var node in unfilteredNodeList)
            {
                sb.Append(node);
                sb.Append("\n");
            }
            FileUtils.WriteFile(fullpatth, sb.ToString());

            return true;
        }

        public bool ProcessCodeListNode(string fullPath)
        {
            List<CorpusAffinityTerm> corpusAffinityTerms = DataProviderForNodeDocuments.GetCorpusAffinityTerms();
            List<string> IndividualTerms = new List<string>();
            string[] arrTerms = null;
            foreach (var t in corpusAffinityTerms)
            {
                arrTerms = t.Term.ToLower().Split(',');
                IndividualTerms.AddRange(arrTerms);

            }
            IndividualTerms = IndividualTerms.Distinct().ToList();
            List<CodeListNode> codeListNodes = new List<CodeListNode>();

            foreach (var individualTerm in IndividualTerms)
            {
                CodeListNode node = new CodeListNode();
                node.CorpusIds = new List<int>();

                node.Term = individualTerm;
                string[] corpusIndividualTermsArr = null;
                foreach (var corpusAffinityTerm in corpusAffinityTerms)
                {

                    corpusIndividualTermsArr = corpusAffinityTerm.Term.ToLower().Split(',');
                    for (int i = 0; i < corpusIndividualTermsArr.Length; i++)
                    {
                        if (individualTerm == corpusIndividualTermsArr[i])
                        {

                            node.CorpusIds.Add(corpusAffinityTerm.CorpusId);

                        }
                    }
                }
                codeListNodes.Add(node);
            }
            Console.WriteLine("finaliza logica...");

            foreach (var listNode in codeListNodes)
            {
                listNode.NodeIds = DataProviderForNodeDocuments.GetNoBridgeNodesRelatedToCorpus(listNode.CorpusIds);
            }
            StringBuilder sb = new StringBuilder();
            StringBuilder sbPhrase = new StringBuilder();

            string term = null;
            int value = 0;
            foreach (var codeListNode in codeListNodes)
            {


                if (StringUtils.IsAPhrase(codeListNode.Term))
                {
                    string[] words = codeListNode.Term.Split(' ');
                    sbPhrase = new StringBuilder();


                    foreach (var word in words)
                    {
                        term = Rules.ApplyRules(word);
                        if (!string.IsNullOrEmpty(term) && dictionaryWords.ContainsKey(term))
                        {
                            value = dictionaryWords[term];
                            sbPhrase.Append(value);
                            sbPhrase.Append(',');
                        }
                    }
                    if (sbPhrase.Length > 1)
                    {
                        sbPhrase.Remove(sbPhrase.Length - 1, 1);
                    }

                    term = sbPhrase.ToString();

                    if (dictionaryPhrases.ContainsKey(term))
                    {
                        sb.Append(dictionaryPhrases[term] + ",");
                    }
                }
                else
                {

                    term = Rules.ApplyRules(codeListNode.Term);
                    if (dictionaryWords.ContainsKey(term))
                    {
                        sb.Append(dictionaryWords[term] + ",");
                    }
                }



                foreach (var nodeid in codeListNode.NodeIds)
                {

                    sb.Append(nodeid);
                    sb.Append(",");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append("\n");
            }
            FileUtils.WriteFile(fullPath, sb.ToString());

            return true;
        }
    }
}