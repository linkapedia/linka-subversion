﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.Bll
{
    public interface IPrecompiledBll
    {
        bool ProcessWordTable(string fullPath);
        bool ProcessPhrasesTable(string fullPath);
        bool ProcessNodeSignaturesTable(string fullpath);
        bool ProcessNodeMustHaveTable(string fullpath);
        bool ProcessUnfilteredNodeTable(string fullpath);
        bool ProcessCodeListNode(string fullPath);
    }
}
