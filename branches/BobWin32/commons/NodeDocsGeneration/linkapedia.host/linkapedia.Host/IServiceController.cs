﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace linkapedia.Host
{
    public interface IServiceController
    {
        void Start();
        void Stop();
    }
}
