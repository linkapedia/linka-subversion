﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using linkapedia.Bll;
using log4net;

namespace linkapedia.Host
{
    public class ServiceController : IServiceController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ServiceController));
        private IPrecompiledBll PrecompiledBll { get; set; }
        public bool IsStopped { get; set; }
        public bool HasError { get; set; }

        public ServiceController()
        {
            log.Debug("ServiceController created");
            IsStopped = false;
            HasError = false;
        }

        public void Start()
        {
            log.Debug("Starting Service Controller...");
            Run();
        }

        public void Stop()
        {
            log.Debug("Stopping Service Controller...");
            IsStopped = true;

        }

        public void Run()
        {
            log.Warn("Run service controller");

            try
            {
                log.Info("Calling the thread");
                Thread localThread = new Thread(Execute);
                localThread.Start();

            }
            catch (Exception ex)
            {

                log.Error("Erron on run  service controller" + ex.Message);
            }

            log.Info("Ending...");
        }

        public void Execute()
        {
            log.Info("Executing...");
            PrecompiledBll.ProcessWordTable("c:\\temp\\words.txt");
            PrecompiledBll.ProcessPhrasesTable("c:\\temp\\phrases.txt");
            PrecompiledBll.ProcessNodeSignaturesTable("c:\\temp\\nodeSigVector.txt");
            PrecompiledBll.ProcessNodeMustHaveTable("c:\\temp\\nodeMustHaveVector.txt");
            PrecompiledBll.ProcessUnfilteredNodeTable("c:\\temp\\unfilteredNodeId.txt");
            PrecompiledBll.ProcessCodeListNode("c:\\temp\\codeListNode.txt");
            log.Warn("Finalizo proceso");
        }   
    }
}
