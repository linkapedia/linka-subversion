﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace linkapedia.Host
{
    public  interface IServiceCoordinator
    {
        void Start();
        void Stop();
    }
}
