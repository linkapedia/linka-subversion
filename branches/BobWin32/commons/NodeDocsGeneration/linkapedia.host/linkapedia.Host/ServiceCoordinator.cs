﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace linkapedia.Host
{
    public  class ServiceCoordinator:IServiceCoordinator
    {
        private readonly ILog log = LogManager.GetLogger(typeof (ServiceCoordinator));
        public IServiceController ServiceController { get; set; }

        public ServiceCoordinator()
        {
            log.Debug("ServiceCoordinator created");
        }

        public void Start()
        {
            //throw new Exception();
            log.Debug("Starting Service Coordinator...");
            ServiceController.Start();
        }

        public void Stop()
        {
            log.Debug("Stopping Service Coordinator...");
        }
    }
}
