﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using Spring.Context;
using Spring.Context.Support;
using Topshelf;
using Topshelf.Configuration;
using Topshelf.Configuration.Dsl;

namespace linkapedia.Host
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));
        public static IApplicationContext IoC { get; private set; }

        static void Main(string[] args)
        {
            InitBootstrapLogger();

            try
            {
                InitContainer();
                RunConfiguration cfg = RunnerConfigurator.New(x =>
                {

                    x.AfterStoppingServices(h => Log.Info("All Services have been stopped"));

                    x.ConfigureService<IServiceCoordinator>(s =>
                    {
                        s.Named("PrecompiledSteps");
                        s.HowToBuildService(name => (IServiceCoordinator)IoC.GetObject("ServiceCoordinator"));
                        s.WhenStarted(tc => tc.Start());
                        s.WhenStopped(tc => tc.Stop());
                    });

                    x.RunAsLocalSystem();

                    x.SetDescription("Service -preconfiled files");
                    x.SetDisplayName("preconfiled files");
                    x.SetServiceName("preconfiled files");
                });

                Runner.Host(cfg, args);
            }
            catch (Exception exception)
            {
                Log.Error("Error: " + exception.Message);  
                //throw;
            }

         
           
        }

        private static void InitContainer()
        {
            IoC = ContextRegistry.GetContext();
            Log.Info("Inversion of Control Container (IoC) Initialized Successfully");
        }

        private static void InitBootstrapLogger()
        {
            string configurationFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PrecompiledLog4Net.config");
            var configurationFile = new FileInfo(configurationFilePath);
            XmlConfigurator.ConfigureAndWatch(configurationFile);
            Log.InfoFormat("BootstrapLogger Initialized Successfully from configuration file {0}", configurationFilePath);
        }
    }
}
