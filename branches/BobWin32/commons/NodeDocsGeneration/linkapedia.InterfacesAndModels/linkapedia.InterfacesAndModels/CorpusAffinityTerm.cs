﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.InterfacesAndModels
{
    public class CorpusAffinityTerm
    {
        public Int32 CorpusId { get; set; }
        public string Term { get; set; }
    }
}
