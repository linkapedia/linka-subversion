﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.InterfacesAndModels
{
    public class CodeListNode
    {
        public string Term { get; set; }
        public List<int> CorpusIds { get; set; }
        public List<string> NodeIds { get; set; }
    }
}
