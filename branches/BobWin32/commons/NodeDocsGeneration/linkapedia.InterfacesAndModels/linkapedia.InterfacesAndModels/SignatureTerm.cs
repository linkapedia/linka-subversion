﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.Models
{
    public class SignatureTerm
    {
        public Int32 NodeId { get; set; }
        public Int32 NodeSize { get; set; }
        public Int16 NodeType { get; set; }
        public String Term { get; set; }
        public Int32 Ocurrences { get; set; }
    }
}
