﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.InterfacesAndModels
{
   public class MustHaveTerm
    {
       public Int32 NodeId {get; set;}
       public String Term { get; set; }
    }
}
