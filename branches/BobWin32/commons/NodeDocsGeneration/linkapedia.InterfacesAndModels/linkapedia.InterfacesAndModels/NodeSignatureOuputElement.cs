﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.InterfacesAndModels
{
   public  class NodeSignatureOuputElement
    {
        public int NodeId { get; set; }
        public decimal Weight { get; set; }
        public int TypeId { get; set; }
        public int TermValue { get; set; }
    }
}
