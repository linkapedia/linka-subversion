﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using linkapedia.InterfacesAndModels;
using linkapedia.Models;
using log4net;

namespace linkapedia.Dao
{
    public class DataProvider : IProvideData
    {
        private readonly ILog log = LogManager.GetLogger(typeof(DataProvider));

        public List<string> GetDataList(string sql)
        {
            var conn = new OracleConnection();
            string connectionString = ConfigurationManager.AppSettings["OracleConnectionString"];
            var dataList = new List<string>();
            OracleDataReader oracleDataReader;
            conn.ConnectionString = connectionString;
            try
            {
                conn.Open();

                var command = conn.CreateCommand();
                command.CommandText = sql;
                oracleDataReader = command.ExecuteReader();
                while (oracleDataReader.Read())
                {
                    if (!oracleDataReader.IsDBNull(0))
                    {
                        dataList.Add(oracleDataReader[0].ToString());


                    }

                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);

            }
            finally
            {
                conn.Dispose();
            }


            return dataList;
        }

        public List<Node> GetNodeList(string sql)
        {
            var conn = new OracleConnection();
            string connectionString = ConfigurationManager.AppSettings["OracleConnectionString"];
            var nodeList = new List<Node>();
            OracleDataReader oracleDataReader;
            conn.ConnectionString = connectionString;
            try
            {
                conn.Open();

                var command = conn.CreateCommand();
                command.CommandText = sql;
                oracleDataReader = command.ExecuteReader();

                while (oracleDataReader.Read())
                {
                    Node node=new Node();
                    if (!oracleDataReader.IsDBNull(0))
                    {
                        node.NodeId = Convert.ToInt32(oracleDataReader[0]);
                    }
                    if (!oracleDataReader.IsDBNull(1))
                    {
                        node.NodeSize = Convert.ToInt32(oracleDataReader[1]);
                    }
                    if (!oracleDataReader.IsDBNull(2))
                    {
                        node.NodeType = Convert.ToInt16(oracleDataReader[2]);
                    }

                    nodeList.Add(node);


                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);

            }
            finally
            {
                conn.Dispose();
            }


            return nodeList;
        }

        public List<SignatureTerm> GetSignatureTermList(string sql)
        {
            var conn = new OracleConnection();
            string connectionString = ConfigurationManager.AppSettings["OracleConnectionString"];
            var sigantureTermList = new List<SignatureTerm>();
            OracleDataReader oracleDataReader;
            conn.ConnectionString = connectionString;
            try
            {
                conn.Open();

                var command = conn.CreateCommand();
                command.CommandText = sql;
                oracleDataReader = command.ExecuteReader();

                while (oracleDataReader.Read())
                {
                    var signatureTerm = new SignatureTerm();
                    if (!oracleDataReader.IsDBNull(0))
                    {
                        signatureTerm.NodeId = Convert.ToInt32(oracleDataReader[0]);
                    }
                    if (!oracleDataReader.IsDBNull(1))
                    {
                        signatureTerm.NodeSize = Convert.ToInt32(oracleDataReader[1]);
                    }
                    if (!oracleDataReader.IsDBNull(2))
                    {
                        signatureTerm.NodeType = Convert.ToInt16(oracleDataReader[2]);
                    }

                    if (!oracleDataReader.IsDBNull(3))
                    {
                        signatureTerm.Term = Convert.ToString(oracleDataReader[3]);
                    }
                    if (!oracleDataReader.IsDBNull(4))
                    {
                        signatureTerm.Ocurrences = Convert.ToInt32(oracleDataReader[4]);
                    }

                    sigantureTermList.Add(signatureTerm);


                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);

            }
            finally
            {
                conn.Dispose();
            }


            return sigantureTermList;
        }

        public List<MustHaveTerm> GetMustHaveTermList(string sql)
        {
            var conn = new OracleConnection();
            string connectionString = ConfigurationManager.AppSettings["OracleConnectionString"];
            var mustHaveTermList = new List<MustHaveTerm>();
            OracleDataReader oracleDataReader;
            conn.ConnectionString = connectionString;
            try
            {
                conn.Open();

                var command = conn.CreateCommand();
                command.CommandText = sql;
                oracleDataReader = command.ExecuteReader();

                while (oracleDataReader.Read())
                {
                    var mustHaveTerm = new MustHaveTerm();
                    if (!oracleDataReader.IsDBNull(0))
                    {
                        mustHaveTerm.NodeId = Convert.ToInt32(oracleDataReader[0]);
                    }
                    if (!oracleDataReader.IsDBNull(1))
                    {
                        mustHaveTerm.Term = Convert.ToString(oracleDataReader[1]);
                    }


                    mustHaveTermList.Add(mustHaveTerm);


                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);

            }
            finally
            {
                conn.Dispose();
            }


            return mustHaveTermList;
        }

        public List<CorpusAffinityTerm> GetCorpusAffinityTerms(string sql)
        {
            var conn = new OracleConnection();
            string connectionString = ConfigurationManager.AppSettings["OracleConnectionString"];
            var corpusAffinityTerms = new List<CorpusAffinityTerm>();
            OracleDataReader oracleDataReader;
            conn.ConnectionString = connectionString;
            try
            {
                conn.Open();

                var command = conn.CreateCommand();
                command.CommandText = sql;
                oracleDataReader = command.ExecuteReader();

                while (oracleDataReader.Read())
                {
                    var corpusAffinityTerm = new CorpusAffinityTerm();
                    if (!oracleDataReader.IsDBNull(0))
                    {
                        corpusAffinityTerm.CorpusId = Convert.ToInt32(oracleDataReader[0]);
                    }
                    if (!oracleDataReader.IsDBNull(1))
                    {
                        corpusAffinityTerm.Term = Convert.ToString(oracleDataReader[1]);
                    }


                    corpusAffinityTerms.Add(corpusAffinityTerm);


                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);

            }
            finally
            {
                conn.Dispose();
            }


            return corpusAffinityTerms;
        }
    }
}
