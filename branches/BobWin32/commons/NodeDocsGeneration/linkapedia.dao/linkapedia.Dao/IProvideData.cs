﻿using System;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using linkapedia.InterfacesAndModels;
using linkapedia.Models;

namespace linkapedia.Dao
{
    public interface IProvideData
    {
        List<string> GetDataList(string sql);
        List<Node> GetNodeList(string sql);
        List<SignatureTerm> GetSignatureTermList(string sql);
        List<MustHaveTerm> GetMustHaveTermList(string sql);
        List<CorpusAffinityTerm> GetCorpusAffinityTerms(string sql);
    }
}
