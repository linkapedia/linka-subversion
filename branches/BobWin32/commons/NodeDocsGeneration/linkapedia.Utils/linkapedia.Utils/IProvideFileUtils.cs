﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.FileUtils
{
    public interface IProvideFileUtils
    {
        void WriteFile(string path, Dictionary<string, int> lines);
        void WriteFile(string path, string content);
    }
}
