﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.StringUtils
{
    class StringOperations:IProvideStringUtils
    {
        public bool IsAPhrase(string phrase)
        {
            phrase = phrase.TrimEnd(' ');
            string[] words = SplitWords(phrase, ' ');

            if (words.Length > 1)
                return true;
            return false;
        }
        public string[] SplitWords(String words, char c)
        {
            return words.Split(c);
        }
        public string[] SplitWords(String words, char[] c)
        {
            return words.Split(c);
        }
    }
}
