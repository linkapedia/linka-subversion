﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.StringUtils
{
    public interface IProvideStringUtils
    {
        bool IsAPhrase(string phrase);
        string[] SplitWords(String words, char c);

        string[] SplitWords(String words, char[] c);

    }
}
