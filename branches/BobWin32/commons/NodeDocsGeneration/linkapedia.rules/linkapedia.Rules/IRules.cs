﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.Rules
{
    public interface IRules
    {
        string ApplyRules(string word);
    }
}
