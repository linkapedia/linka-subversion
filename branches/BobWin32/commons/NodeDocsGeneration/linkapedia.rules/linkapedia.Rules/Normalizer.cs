﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace linkapedia.Rules
{
    class Normalizer
    {
        public static  string Normalize (string word)
        {

            Regex reg = new Regex("[^a-zA-Z0-9 ]");
            string textoSinAcentos = reg.Replace(word, "");
            return textoSinAcentos.ToLower();
        }
    }
}
