﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.Rules
{
    public class Rules:IRules
    {
        private PorterStemmer stemmer = new PorterStemmer();
        public string ApplyRules(string word)
        {
            
            if(!string.IsNullOrEmpty(word))
            {
                return stemmer.stemTerm(Normalizer.Normalize(word));
            }
            return null;
        }
       
    }
}
