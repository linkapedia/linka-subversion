﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.FileUtils
{
    public class FileOperations : IProvideFileUtils
    {
        public void WriteFile(string path, Dictionary<string, int> lines)
        {

            try
            {

                StreamWriter file = new StreamWriter(path);
                foreach (KeyValuePair<string, int> pair in lines)
                {
                    Console.WriteLine("{0}, {1}",
                    pair.Key,
                    pair.Value);
                    file.WriteLine(pair.Value + "," + pair.Key);
                }
                file.Flush();
                file.Close();

            }
            catch (Exception ex)
            {
            }

        }

        public void WriteFile(string path, string content)
        {
            try
            {
                StreamWriter file = new StreamWriter(path);
                file.WriteLine(content);
                file.Flush();
                file.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void WriteFileBytesRepresentation(string path, Dictionary<string, int> lines)
        {
            //cantidad de ids(bytes)idpalabrasidpalabras

            try
            {

                StreamWriter file = new StreamWriter(path);
                foreach (KeyValuePair<string, int> pair in lines)
                {
                    Console.WriteLine("{0}, {1}",
                                      pair.Key,
                                      pair.Value);
                    Console.WriteLine(getBytesRepresentation(lines.Count) + getBytesRepresentation(pair.Value));
                    file.WriteLine(pair.Value + "," + pair.Key);

                }
                file.Flush();
                file.Close();

            }
            catch (Exception ex)
            {
            }
        }



        private string getBytesRepresentation(int number)
        {
            StringBuilder sb = new StringBuilder();
            byte[] intBytes = BitConverter.GetBytes(number);
            foreach (var intByte in intBytes)
            {
                sb.Append(intByte);
            }
            return sb.ToString();
        }
    }
}
