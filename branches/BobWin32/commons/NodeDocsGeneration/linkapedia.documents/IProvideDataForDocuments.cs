﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.Documents
{
   public interface IProvideDataForDocuments
   {
       List<string> GetTerms();
       List<string> GetPhrases();
   }
}
