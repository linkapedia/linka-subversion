﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using linkapedia.Dao;
using linkapedia.StringUtils;
using log4net;


namespace linkapedia.Documents
{
    class DataProviderForDocuments : IProvideDataForDocuments
    {
        private IProvideData DataProvider { get; set; }
        private IProvideStringUtils StringUtils { get; set; }
        private RowData rowData = null;
        private readonly ILog log = LogManager.GetLogger(typeof(DataProviderForDocuments));


        public List<string> GetTerms()
        {
            log.Info("Starting to get the terms");
           
            var terms = new List<string>();

            try
            {
                if (rowData == null)
                {
                    SetRowData();
                }

                terms.AddRange(GetTermsFromCorpus());
                terms.AddRange(GetTermsFromSignature());
                terms.AddRange(GetTermsFromMustHaves());
            }
            catch (Exception ex)
            {               
               log.Error(ex.Message);
            }

            log.Info("Getting the terms was successfull!");
            return terms;


        }

        public List<string> GetPhrases()
        {
            var phrases = new List<string>();

            try
            {
                if (rowData == null)
                {
                    SetRowData();
                }
                phrases.AddRange(GetPhrasesFromCorpus());
                phrases.AddRange(GetPhrasesFromSignatures());
                phrases.AddRange(GetPhrasesFromMustHaves());
            }
            catch (Exception ex)
            {
                
                log.Error(ex.Message);
            }
           
            return phrases;
        }

        private void SetRowData()
        {
            log.Info("Getting row data");
            var dataFromCorpus = new List<string>();
            var dataFromSignature = new List<string>();
            var dataFromMustHave = new List<string>();

            try
            {
                List<string> activeNodes = GetDataActiveNodes();

                dataFromCorpus.AddRange(GetDataFromCorpus());

                long cont = 0;
                long step = 1000;

                for (int i = 0; i < activeNodes.LongCount(); i += (int)step)
                {
                    if (cont + 1000 > activeNodes.LongCount())
                    {
                        step = activeNodes.LongCount() - cont;
                    }

                    dataFromSignature.AddRange(GetDataFromSignature(activeNodes.GetRange(i, (int)step)));
                    cont += step;
                    log.Info("Getting Terms from signature, node count: " + cont);
                }

                cont = 0;
                step = 1000;
                for (int i = 0; i < activeNodes.LongCount(); i += (int)step)
                {
                    if (cont + 1000 > activeNodes.LongCount())
                    {
                        step = activeNodes.LongCount() - cont;
                    }

                    dataFromMustHave.AddRange(GetDataFromMustHave(activeNodes.GetRange(i, (int)step)));
                    cont += step;
                    log.Info("Getting Terms from MustHave, node count: " + cont);
                }

                rowData = new RowData();

                rowData.dataFromCorpus = dataFromCorpus;
                rowData.dataFromSignature = dataFromSignature;
                rowData.datafromMustHaves = dataFromMustHave;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            log.Info("Getting row data was succesfull!");

        }

        private List<string> GetDataActiveNodes()
        {
            log.Info("Getting the active nodes");
            List<string> activeNodes;

            try
            {
                string sql = "select NODEID FROM CORPUS INNER JOIN NODE ON" +
                             " corpus.corpusid = node.corpusid WHERE corpus.corpusactive = 1";
                //+ " and rownum<1000";

                activeNodes = DataProvider.GetDataList(sql);

            }
            catch (Exception ex)
            {
                throw ex;

            }
            log.Info("Getting the active rows was succesfull");
            return activeNodes;
        }
        private List<string> GetDataFromCorpus()
        {
            Console.WriteLine("Getting terms from corpus");
            var terms = new List<string>();

            try
            {

                string sql = "select distinct corpusterms FROM CORPUS WHERE corpus.corpusactive = 1";

                terms = DataProvider.GetDataList(sql);

            }
            catch (Exception ex)
            {
             
                throw ex;
            }

            return terms;
        }
        private List<string> GetDataFromSignature(List<string> nodes)
        {
            Console.WriteLine("Getting terms from signature");
            var terms = new List<string>();

            try
            {

                String nodesToLookFor = "";
                nodesToLookFor += "(";
                foreach (var node in nodes)
                {
                    nodesToLookFor += node + ",";
                }
                nodesToLookFor = nodesToLookFor.Remove(nodesToLookFor.LastIndexOf(','));
                nodesToLookFor += ")";
                String sql = "SELECT DISTINCT SIGNATUREWORD FROM NODE " +
                                       "INNER JOIN SIGNATURE ON SIGNATURE.NODEID=NODE.NODEID " +
                                       "WHERE NODE.NODEID IN " + nodesToLookFor;


                terms = DataProvider.GetDataList(sql);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return terms;

        }
        private List<string> GetDataFromMustHave(List<string> nodes)
        {
            Console.WriteLine("Getting terms from MustHave");
            var terms = new List<string>();

            try
            {
                String nodesToLookFor = "";
                nodesToLookFor += "(";
                foreach (var node in nodes)
                {
                    nodesToLookFor += node + ",";
                }
                nodesToLookFor = nodesToLookFor.Remove(nodesToLookFor.LastIndexOf(','));
                nodesToLookFor += ")";
                String sql = "SELECT DISTINCT MUSTWORD FROM NODE " +
                                       "INNER JOIN MUSTHAVE ON MUSTHAVE.NODEID=NODE.NODEID " +
                                       "WHERE NODE.NODEID IN " + nodesToLookFor;


                terms = DataProvider.GetDataList(sql);

            }
            catch (Exception ex)
            {

                throw ex;
            }


            return terms;
        }
        private List<string> GetTermsFromCorpus()
        {
            var terms=new List<string>();
            string[] words;
            char[] charsToSplit = { ' ', ',' };

            try
            {
                foreach (string term in rowData.dataFromCorpus)
                {
                    words = StringUtils.SplitWords(term, charsToSplit);
                    terms.AddRange(words);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
          

            return terms;
        }  
        private List<string> GetTermsFromSignature()
        {
            var terms = new List<string>();
            string[] words;

            try
            {
                foreach (string term in rowData.dataFromSignature)
                {
                    words = StringUtils.SplitWords(term, ' ');
                    terms.AddRange(words);
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            return terms;
        }
        private List<string> GetTermsFromMustHaves()
        {
            var terms = new List<string>();
            string[] words;

            try
            {
                foreach (string term in rowData.datafromMustHaves)
                {
                    words = StringUtils.SplitWords(term, ' ');
                    terms.AddRange(words);
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           

            return terms;
        }
        private List<string> GetPhrasesFromCorpus()
        {
            var phrases = new List<string>();
            string[] words;

            try
            {
                foreach (string phrase in rowData.dataFromCorpus)
                {
                    words = StringUtils.SplitWords(phrase, ',');
                    foreach (var word in words)
                    {
                        if (StringUtils.IsAPhrase(word))
                            phrases.Add(word);
                    }

                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            return phrases;
        }
        private List<string> GetPhrasesFromSignatures()
        {
            var phrases = new List<string>();

            try
            {
                foreach (string phrase in rowData.dataFromSignature)
                {
                    if (StringUtils.IsAPhrase(phrase))
                        phrases.Add(phrase);
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           

            return phrases;
        }
        private List<string> GetPhrasesFromMustHaves()
        {
            var phrases = new List<string>();

            try
            {
                foreach (string phrase in rowData.datafromMustHaves)
                {
                    if (StringUtils.IsAPhrase(phrase))
                        phrases.Add(phrase);
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }

          

            return phrases;
        }  
 


    }
}
