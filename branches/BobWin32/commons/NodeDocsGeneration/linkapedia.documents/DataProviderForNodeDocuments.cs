﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using linkapedia.Dao;
using linkapedia.InterfacesAndModels;
using linkapedia.Models;
using log4net;

namespace linkapedia.Documents
{
    class DataProviderForNodeDocuments : IProvideDataForNodeDocuments
    {
        private IProvideData DataProvider { get; set; }
        private readonly ILog log = LogManager.GetLogger(typeof(DataProviderForDocuments));
        private List<string> activeNodes = null;

        public List<SignatureTerm> GetSignatureTerms()
        {
            var signatureTerms=new List<SignatureTerm>();
            try
            {
                if (activeNodes == null)
                {
                    SetNoBridgeActiveNodes();
                }
                signatureTerms = GetTermsFromSignature();
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
         
            return signatureTerms;
        }

        public List<MustHaveTerm> GetMustHaveTerms()
        {
            var mustHaveTerms= new List<MustHaveTerm>();
            try
            {
                if (activeNodes == null)
                {
                    SetNoBridgeActiveNodes();
                }
                mustHaveTerms = GetTermsFromMustHave();
            }
            catch (Exception ex)
            {
                
               log.Fatal(ex.Message);
            }
           
            return mustHaveTerms;
        }

        public List<string> GetUnfilteredNodeList()
        {
             var unfilteredNodes=new List<string>();
            try
            {
                unfilteredNodes = GetNoBridgeNoCorpusTermsActiveNodes();
            }
            catch (Exception ex)
            {
                
               log.Fatal(ex.Message);
            }
           
            return unfilteredNodes;
        }

        public List<CorpusAffinityTerm> GetCorpusAffinityTerms()
        {
            var corpusAffinityTerms = new List<CorpusAffinityTerm>();
            try
            {

                string sql = "select CORPUSID,CORPUSTERMS FROM CORPUS "
                            + "WHERE corpus.corpusactive = 1 and corpusterms is not null ";
                //    + " and rownum<1000";
                corpusAffinityTerms = DataProvider.GetCorpusAffinityTerms(sql);
            }
            catch (Exception ex)
            {

                log.Fatal(ex.Message);
            }

            return corpusAffinityTerms;
        }

        public List<string> GetNoBridgeNodesRelatedToCorpus(List<int> corpusList)
        {
            var nodes = new List<string>();
            log.Info("Getting the No Bridge Nodes Related To Corpus");


            try
            {
                string corpusToLookFor = "";
                corpusToLookFor += "(";
                foreach (var corpus in corpusList)
                {
                    corpusToLookFor += corpus + ",";
                }
                corpusToLookFor = corpusToLookFor.Remove(corpusToLookFor.LastIndexOf(','));
                corpusToLookFor += ")";



                string sql = "select NODE.NODEID FROM CORPUS"
                             + " INNER JOIN NODE ON corpus.corpusid = node.corpusid and node.bridgenode=0"
                             + " WHERE CORPUS.CORPUSID in " + corpusToLookFor;
                   // + " and rownum<1000";

                nodes = DataProvider.GetDataList(sql);

            }
            catch (Exception ex)
            {
                throw ex;

            }

            log.Info("Getting the No Bridge Nodes Related To Corpus");
            return nodes;
        }

        private List<string> GetNoBridgeNoCorpusTermsActiveNodes()
        {
            var unfilteredNodes = new List<string>();
            log.Info("Getting the UnfilteredNodeList ");
            

            try
            {
                string sql = "select NODEID FROM CORPUS "
                            + "INNER JOIN NODE ON corpus.corpusid = node.corpusid and node.bridgenode=0"
                            + "WHERE corpus.corpusactive = 1 and corpusterms is null ";
                //    + " and rownum<1000";

                unfilteredNodes = DataProvider.GetDataList(sql);

            }
            catch (Exception ex)
            {
                throw ex;

            }

            log.Info("Getting the unfilteredNodes nodes was succesfull");
            return unfilteredNodes;
        }

        private void SetNoBridgeActiveNodes()
        {
            log.Info("Getting the active nodes");
            activeNodes = new List<string>();

            try
            {
                string sql = "select NODEID,NODESIZE,NODETYPE FROM CORPUS INNER JOIN NODE ON" +
                             " corpus.corpusid = node.corpusid and node.bridgenode=0" +
                             " WHERE corpus.corpusactive = 1 ";
                  //  + " and rownum<1000";

                activeNodes = DataProvider.GetDataList(sql);

            }
            catch (Exception ex)
            {
                throw ex;

            }

            log.Info("Getting the active nodes was succesfull");

        }

        private List<SignatureTerm> GetTermsFromSignature()
        {

            var signatureTerms = new List<SignatureTerm>();

            long cont = 0;
            long step = 1000;
            try
            {
                for (int i = 0; i < activeNodes.LongCount(); i += (int)step)
                {
                    if (cont + 1000 > activeNodes.LongCount())
                    {
                        step = activeNodes.LongCount() - cont;
                    }

                    signatureTerms.AddRange(GetDataTermsFromSignature(activeNodes.GetRange(i, (int)step)));
                    cont += step;
                    log.Info("Getting Terms from signature, node count: " + cont);
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

 

            return signatureTerms;

        }

        private List<SignatureTerm> GetDataTermsFromSignature(List<string> nodes)
        {
            Console.WriteLine("Getting terms from signature");
            var terms = new List<SignatureTerm>();

            try
            {

                String nodesToLookFor = "";
                nodesToLookFor += "(";
                foreach (var node in nodes)
                {
                    nodesToLookFor += node + ",";
                }
                nodesToLookFor = nodesToLookFor.Remove(nodesToLookFor.LastIndexOf(','));
                nodesToLookFor += ")";
                String sql = "SELECT   NODE.NODEID,NODE.NODESIZE,NODE.NODETYPE,SIGNATUREWORD,SIGNATUREOCCURENCES FROM NODE " +
                                       "INNER JOIN SIGNATURE ON SIGNATURE.NODEID=NODE.NODEID " +
                                       "WHERE NODE.NODEID IN " + nodesToLookFor;


                terms = DataProvider.GetSignatureTermList(sql);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return terms;

        }

        private List<MustHaveTerm> GetTermsFromMustHave()
        {

            var mustHaveTerms = new List<MustHaveTerm>();

            long cont = 0;
            long step = 1000;

            try
            {

                for (int i = 0; i < activeNodes.LongCount(); i += (int)step)
                {
                    if (cont + 1000 > activeNodes.LongCount())
                    {
                        step = activeNodes.LongCount() - cont;
                    }

                    mustHaveTerms.AddRange(GetDataTermsFromMustHave(activeNodes.GetRange(i, (int)step)));
                    cont += step;
                    log.Info("Getting Terms from mustHave, node count: " + cont);
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            return mustHaveTerms;

        }

        private List<MustHaveTerm> GetDataTermsFromMustHave(List<string> nodes)
        {
            Console.WriteLine("Getting terms from MustHave");
            var terms = new List<MustHaveTerm>();

            try
            {

                String nodesToLookFor = "";
                nodesToLookFor += "(";
                foreach (var node in nodes)
                {
                    nodesToLookFor += node + ",";
                }
                nodesToLookFor = nodesToLookFor.Remove(nodesToLookFor.LastIndexOf(','));
                nodesToLookFor += ")";
                String sql = "SELECT NODEID,MUSTWORD FROM MUSTHAVE " +
                                       "WHERE NODEID IN " + nodesToLookFor;


                terms = DataProvider.GetMustHaveTermList(sql);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return terms;

        }


  
    }
}
