﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using linkapedia.InterfacesAndModels;
using linkapedia.Models;

namespace linkapedia.Documents
{
   public interface IProvideDataForNodeDocuments
   {
       List<SignatureTerm> GetSignatureTerms();
       List<MustHaveTerm> GetMustHaveTerms();
       List<string> GetUnfilteredNodeList();
       List<CorpusAffinityTerm> GetCorpusAffinityTerms();
       List<string> GetNoBridgeNodesRelatedToCorpus(List<int> corpus);


   }
}
