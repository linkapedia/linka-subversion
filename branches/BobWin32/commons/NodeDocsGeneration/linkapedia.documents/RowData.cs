﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linkapedia.Documents
{
    class RowData
    {
        public List<string> dataFromCorpus { get; set; }
        public List<string> dataFromSignature { get; set; }
        public List<string> datafromMustHaves { get; set; }
    }
}
