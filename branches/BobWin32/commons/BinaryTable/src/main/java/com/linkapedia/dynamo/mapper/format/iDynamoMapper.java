/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.format;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import java.util.List;

/**
 *
 * @author andres
 */
public interface iDynamoMapper {
    
      List<Doc_Node> getDocNodes(List<DocumentNode> documentNodes);
}
