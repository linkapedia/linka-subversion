/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.format;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public class DocumentNodeToDocNodeMapper implements iDynamoMapper {

    private iMapToDocNode mapper;

    public DocumentNodeToDocNodeMapper(iMapToDocNode mapper) {

        this.mapper = mapper;
    }

    public List<Doc_Node> getDocNodes(List<DocumentNode> documentNodes) {
        List<Doc_Node> docNodes = new ArrayList<Doc_Node>();
        boolean sortOrder = true;
        String previousId;
        Doc_Node doc;
        Map<String, Float> unsortMap = new HashMap<String, Float>();
        if (!documentNodes.isEmpty()) {
            previousId = documentNodes.get(0).getDocID();
            for (int counter = 0; counter < documentNodes.size(); counter++) {  
                System.out.println("****Mapping document node:" + documentNodes.get(counter)+ "****");
                if (previousId.equalsIgnoreCase(documentNodes.get(counter).getDocID())) {
                    unsortMap.put(documentNodes.get(counter).getNodeID(), documentNodes.get(counter).getScore11());
                    if (documentNodes.size() - 1 == counter) {
                        doc = mapper.formatDocNode(unsortMap, previousId, sortOrder, documentNodes.get(counter));
                        docNodes.add(doc);
                    }
                } else {
                    if (!unsortMap.isEmpty()) {
                        doc = mapper.formatDocNode(unsortMap, previousId, sortOrder, documentNodes.get(counter));
                        docNodes.add(doc);
                    }
                    previousId = documentNodes.get(counter).getDocID();
                    unsortMap.clear();
                    if (previousId.equalsIgnoreCase(documentNodes.get(counter).getDocID())) {
                        unsortMap.put(documentNodes.get(counter).getNodeID(), documentNodes.get(counter).getScore11());
                    }
                }
            }
        }
        return docNodes;

    }
}
