/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.data;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import java.util.List;

/**
 *
 * @author andres
 */
public interface IDataOperations {
    List<DocumentNode> getData()throws Exception ;
    void saveData(List<Doc_Node> nodesToInsert);
}
