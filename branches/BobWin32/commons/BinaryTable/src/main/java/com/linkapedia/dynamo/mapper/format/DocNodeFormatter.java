/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.format;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.linkapedia.dynamo.mapper.beans.JsonObjectDocNodeContent;
import com.linkapedia.dynamo.utils.iTextOperations;
import com.mycompany.binarytable.App;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONObject;

/**
 *
 * @author andres
 */
public class DocNodeFormatter implements iMapToDocNode {

    private iTextOperations textUtil;
    private static Date dateTime = new Date();

    public DocNodeFormatter(iTextOperations textUtil) {
        this.textUtil = textUtil;
    }

    public Doc_Node formatDocNode(Map<String, Float> unsortMap, String previousId, boolean sortDirection, DocumentNode node) {
        Doc_Node doc = null;
        Map<String, Float> sortedMapAsc = sortByComparator(unsortMap, sortDirection);
        doc = new Doc_Node();
        doc.setDocId(previousId);
        //App.write("DocId: "+ doc.getDocId() + "\n" + "Content: " + convertToJson(sortedMapAsc, node).toString() , doc.getDocId());
        try {
            doc.setNodeList(textUtil.compressString(convertToJson(sortedMapAsc, node).toString()));

        } catch (Exception ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        return doc;
    }

    private JSONObject convertToJson(Map<String, Float> sortedMap, DocumentNode node) {
        JsonObjectDocNodeContent content = new JsonObjectDocNodeContent();
        content.setUrl(node.getURL());
        if (node.getTimeStamp() == 0) {
            content.setTimeStamp(dateTime.getTime());
        } else {
            content.setTimeStamp(node.getTimeStamp());
        }

        List<String> nodeId = new ArrayList<String>();
        for (Map.Entry<String, Float> entry : sortedMap.entrySet()) {
            nodeId.add(entry.getKey());
        }
        content.setNodes(nodeId);
        JSONObject jsonObject = JSONObject.fromObject(content);
        return jsonObject;

    }

    private Map<String, Float> sortByComparator(Map<String, Float> unsortMap, final boolean order) {
        List<Map.Entry<String, Float>> list = new LinkedList<Map.Entry<String, Float>>(unsortMap.entrySet());
        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Float>>() {
            public int compare(Map.Entry<String, Float> o1,
                    Map.Entry<String, Float> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Float> sortedMap = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}
