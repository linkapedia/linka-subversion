package com.mycompany.updatenodedocumentwithdate;

/**
 * Hello world!
 *
 */
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBWrapperAbs;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.handlers.NodeDocumentAPIHandler;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
public class App 
{
    private static final ResourceBundle config = ResourceBundle.getBundle("config");
    public static void main( String[] args )
    {
        try {
            long defaultDate = getADate();
            DynamoDBWrapperAbs.env = config.getString("environment");
            List<NodeDocument> nodes = DataHandlerFacade.FindNodeDocumentsWithNoTimeStamp();
            try {
                DataHandlerFacade.updateNodeDocumenttNodeWithATimeStamp(nodes, defaultDate);
            } catch (Exception ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ParseException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
       System.out.println("Sucessfully Finished! ");
    }
    
    private static long getADate() throws ParseException{
        String str_date="01/01/2013";
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date date = (Date)formatter.parse(str_date); 
        long longDate=date.getTime();
        return longDate;
    }
}
