/**
 * @author: Andres R
 * get connection with the resource
 */
package com.linkapedia.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.linkapedia.generateimage.GenerateImageProperties;

public class ConnectionFactory {

	/**
	 * get connection from oracle
	 * @param prop
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Connection getConnection(Properties prop) throws ClassNotFoundException, SQLException{
		Connection con =null;
		Class.forName("oracle.jdbc.driver.OracleDriver");
		String url=prop.getProperty(GenerateImageProperties.JDBC_URL);
		String user=prop.getProperty(GenerateImageProperties.JDBC_USER);
		String password =prop.getProperty(GenerateImageProperties.JDBC_PASSWORD);
		con=DriverManager.getConnection(url,user,password);
		return con;
	}
}
