/**
 * @author: Andres R
 * util images. create jpeg, scale image etc
 */
package com.linkapedia.generateimage;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class ImageUtils {
	private static final int BOX_HEIGHT_DEFAULT = 160;
	private static final int BOX_WIDTH_DEFAULT = 240;
	private static Log LOG = LogFactory.getLog(ImageUtils.class);
	private static ByteArrayOutputStream bao = null;
	private static BufferedImage transparent = null;

	// use one bao and reset to each request
	static {
		bao = new ByteArrayOutputStream();
		try {
			InputStream is = ImageUtils.class.getClassLoader().getResourceAsStream("images/background.png");
			transparent = ImageIO.read(is);
		} catch (Exception e) {
			LOG.error("Not get transparent Image: " + e.getMessage());
		}
	}

	/**
	 * save image in the FileSystem
	 * 
	 * @param fileName
	 * @param img
	 * @throws Exception
	 */
	public static void saveImage(String fileName, BufferedImage img)
			throws Exception {

		LOG.debug("ImageUtils: saveImage(String, BufferedImage)");
		if (img != null) {
			ImageIO.write(img, "jpg", new FileOutputStream(fileName));
			LOG.info("Save image ok!: " + fileName);
		} else {
			LOG.error("Image == null.");
		}

	}

	/**
	 * create a composite image  from one image
	 * @param image
	 * @param prop
	 * @return
	 * @throws IOException
	 */
	public static BufferedImage getCompositeImage(BufferedImage image,
			Properties prop) throws IOException {
		BufferedImage imageComposite = image;
		BufferedImage imagemini =null;
		int heightImage = imageComposite.getHeight();
		int widthImage = imageComposite.getWidth();
		int boxHeight;
		int boxWidth;
		try {
			boxHeight = Integer.parseInt(prop
					.getProperty(GenerateImageProperties.BOX_HEIGHT));
			boxWidth = Integer.parseInt(prop
					.getProperty(GenerateImageProperties.BOX_WIDTH));
		} catch (Exception e) {
			LOG.info("Not set box height or width");
			boxHeight = BOX_HEIGHT_DEFAULT;
			boxWidth = BOX_WIDTH_DEFAULT;
		}
		imageComposite = scaleImage(image, boxWidth, boxHeight, null,
				false);

		if ((heightImage <= boxHeight) && (widthImage <= boxWidth)) {
			imagemini = image;
		} else if ((heightImage > boxHeight) && (widthImage > boxWidth)) {
			imagemini = scaleImage(image, boxWidth, boxHeight,
					null,true);
		}else{
			imagemini = scaleImage(image, boxWidth, boxHeight,
					null,true);
		}

		int heightInterImage = imagemini.getHeight();
		int widthInterImage = imagemini.getWidth();

		Graphics g = imageComposite.getGraphics();
		g.drawImage(transparent, -6, -6, boxWidth + 10, boxHeight + 10,
				null);
		g.drawImage(imagemini, (boxWidth - widthInterImage) / 2,
				(boxHeight - heightInterImage) / 2, widthInterImage,
				heightInterImage, null);

		ImageIO.write(imageComposite, "jpg", new FileOutputStream(new File(
				"/home/andres/example.jpg")));

		return imageComposite;

	}

	/**
	 * change height and width
	 * 
	 * @param img
	 * @param width
	 * @param height
	 * @param background
	 * @return
	 */
	public static BufferedImage scaleImage(BufferedImage img, int width,
			int height, Color background, boolean correctScale) {
		LOG.debug("ImageUtils: scaleImage(BufferedImage, int, int)");
		int imgWidth = img.getWidth();
		int imgHeight = img.getHeight();
		// scale image if is possible
		if (correctScale) {
			if (imgWidth * height < imgHeight * width) {
				width = imgWidth * height / imgHeight;
			} else {
				height = imgHeight * width / imgWidth;
			}
		}

		// create new image with the new height and width
		BufferedImage newImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g = newImage.createGraphics();
		try {
			// set parameters
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			g.setBackground(background);
			g.clearRect(0, 0, width, height);
			g.drawImage(img, 0, 0, width, height, null);
		} finally {
			g.dispose();
		}
		return newImage;
	}

	/**
	 * convert image to jpeg and change quality
	 * 
	 * @param image
	 * @param precision
	 * @param quality
	 * @throws IOException
	 */
	public static BufferedImage convertToJPEGImage(BufferedImage image,
			int precision, float quality) throws IOException {

		LOG.debug("ImageUtils: convertToJPEGImage(BufferedImage, int)");
		bao.reset();
		BufferedImage imager = null;
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(bao);
		JPEGEncodeParam jpegEncodeParam = encoder
				.getDefaultJPEGEncodeParam(image);
		jpegEncodeParam.setDensityUnit(JPEGEncodeParam.DENSITY_UNIT_DOTS_INCH);
		jpegEncodeParam.setXDensity(precision);
		jpegEncodeParam.setYDensity(precision);
		jpegEncodeParam.setQuality(quality, true);
		encoder.encode(image, jpegEncodeParam);
		imager = ImageIO.read(new ByteArrayInputStream(bao.toByteArray()));

		return imager;
	}

	/**
	 * 
	 * @param bao
	 * @param properties
	 * @throws Exception
	 */
	public static void sendFtp(File file, Properties properties)
			throws Exception {
		FTPClient ftpClient = new FTPClient();
		String directory = "";
		if (properties.getProperty("FTPDIR") == null) {
			directory = "./";
		} else {
			directory = properties.getProperty("FTPDIR");
		}
		directory += file.getName().toLowerCase();
		ftpClient.connect(properties.getProperty("FTPIP"));
		boolean isLogin = ftpClient.login(properties.getProperty("USERNAME"),
				properties.getProperty("PASSWORD"));
		if (isLogin) {
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.storeFile(directory, new FileInputStream(file));

			ftpClient.logout();
			LOG.info("Send file ok!");
		} else {
			LOG.info("Not login ftp!");
		}
		ftpClient.disconnect();
	}
}
