package com.linkapedia.generateimage;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class StringUtils {

	private static Log LOG = LogFactory.getLog(StringUtils.class);

	public static String encodeFileName(String fileNameToEncode) {
		LOG.debug("encodeFileName(String)");
		if (fileNameToEncode != null) {
			fileNameToEncode = StringEscapeUtils.unescapeXml(fileNameToEncode);
			fileNameToEncode = fileNameToEncode.toLowerCase();
			fileNameToEncode = fileNameToEncode.trim();
			fileNameToEncode = fileNameToEncode.replaceAll(" ", "-");
			fileNameToEncode = fileNameToEncode.replaceAll(",", "-");
			fileNameToEncode = fileNameToEncode.replaceAll(":", "-");
			fileNameToEncode = fileNameToEncode.replaceAll("á", "a");
			fileNameToEncode = fileNameToEncode.replaceAll("é", "e");
			fileNameToEncode = fileNameToEncode.replaceAll("í", "i");
			fileNameToEncode = fileNameToEncode.replaceAll("ó", "o");
			fileNameToEncode = fileNameToEncode.replaceAll("ú", "u");
			fileNameToEncode = fileNameToEncode.replaceAll("ñ", "n");
		}
		return fileNameToEncode;
	}
}
