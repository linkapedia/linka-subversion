/**
 * @author: Andres R
 * access to database and get images
 */
package com.linkapedia.db;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.linkapedia.bean.PackImages;
import com.linkapedia.generateimage.GenerateImageProperties;
import com.linkapedia.generateimage.ImageUtils;

public class ImageUtilsDB {

	private static Log LOG = LogFactory.getLog(ImageUtilsDB.class);

	private Connection con;
	private Properties prop;

	/**
	 * Initialize connection and pass properties
	 * 
	 * @param prop
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ImageUtilsDB(Properties prop) throws ClassNotFoundException,
			SQLException {
		LOG.debug("ImageUtilsDB: Constructor(Properties)");
		this.prop = prop;
		LOG.debug("Get connection to oracle");
		this.con = ConnectionFactory.getConnection(prop);
	}

	/**
	 * get images from one corpusid
	 * 
	 * @param corpusId
	 * @return
	 */
	private List<PackImages> getImagesCorpus(String corpusId,
			Float imageQuality, int width, int height) {
		LOG.debug("ImageUtilsDB: getCorpusImagesByCorpus(String)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT CORPUSID, IMAGE FROM CORPUSIMAGES "
				+ "WHERE CORPUSID=?";
		List<PackImages> list = new ArrayList<PackImages>();
		InputStream is = null;
		BufferedImage bi = null;
		BufferedImage biThumnail = null;
		BufferedImage biComposite = null;
		PackImages pi = null;
		String id = null;
		try {
			ps = con.prepareStatement(query);
			ps.setLong(1, Long.parseLong(prop.getProperty("CORPUSID")));
			rs = ps.executeQuery();
			int index = 1;
			while (rs.next()) {
				try {
					pi = new PackImages();
					id = rs.getString("CORPUSID");
					LOG.info("Get image " + index + " from corpusid: " + id);
					is = rs.getBlob("IMAGE").getBinaryStream();
					bi = ImageIO.read(is);
					bi = ImageUtils.convertToJPEGImage(bi, 72, imageQuality);
					pi.setId("" + index);
					biComposite = ImageUtils.getCompositeImage(bi, prop);
					pi.setImage(biComposite);
					biThumnail = ImageUtils.scaleImage(bi, width, height, null,true);
					pi.setImageThumnail(biThumnail);
					list.add(pi);
					index++;
					LOG.info("get image ok!");
				} catch (Exception e) {
					LOG.error("ImageUtilsDB: getCorpusImagesByCorpus(String) "
							+ "reading images" + e.getMessage() + "corpusid: "
							+ id + " index: " + index);
					continue;
				}
			}
		} catch (Exception e) {
			LOG.error("ImageUtilsDB: getCorpusImagesByCorpus(String) "
					+ "parsing propeties " + e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					LOG.error("ImageUtilsDB: getCorpusImagesByCorpus(String)"
							+ " close resulset");
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					LOG.error("ImageUtilsDB: getCorpusImagesByCorpus(String) "
							+ "close prepareStatement");
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LOG.error("ImageUtilsDB: getCorpusImagesByCorpus(String) "
							+ "close InputStream");
				}
			}
		}
		return list;
	}

	/**
	 * 
	 * @param nodeId
	 * @param recursively
	 * @param zos
	 * @param files
	 * @param corpusId
	 */
	public void copyImages(String nodeId, boolean recursively,
			ZipOutputStream zos, Map<String, File> files, String corpusId) {
		List<String> listNodes;
		try {
			listNodes = getNodes(nodeId, recursively);
		} catch (SQLException e1) {
			LOG.error("Not get nodes" + e1.getMessage());
			return;
		}
		float imageQuality;
		int num;
		int width;
		int height;

		try {
			imageQuality = Float.parseFloat(prop
					.getProperty(GenerateImageProperties.QUALITY));
			num = Integer.parseInt(prop
					.getProperty(GenerateImageProperties.NUM_IMAGES));
			width = Integer.parseInt(prop
					.getProperty(GenerateImageProperties.WIDTH));
			height = Integer.parseInt(prop
					.getProperty(GenerateImageProperties.HEIGHT));
		} catch (Exception e) {
			LOG.error("ImageUtilsDB: getNodeImagesByNode(String, boolean) "
					+ "error parse properties " + e.getMessage());
			return;
		}
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		List<PackImages> imagec = getImagesCorpus(corpusId, imageQuality,
				width, height);
		for (String node : listNodes) {
			List<PackImages> imagesNode = new ArrayList<PackImages>();
			imagesNode = getImagesNode(node, imageQuality, num, width, height);
			if ((imagesNode != null) && (imagesNode.isEmpty())) {
				// get images from the taxonomy
				PackImages pack = getImageCorpusRamdom(imagec);
				if (pack != null) {
					pack.setId(node);
					imagesNode.add(pack);
				}
			}
			for (PackImages in : imagesNode) {
				if (zos != null) {
					try {
						bao.reset();
						LOG.info("create folder in zip node image to "
								+ in.getId());
						zos.putNextEntry(new ZipEntry(files.get("FOLDER")
								.getName()
								+ File.separator
								+ in.getId()
								+ ".jpg"));
						ImageIO.write(in.getImage(), "jpg", bao);
						zos.write(bao.toByteArray());
						zos.closeEntry();

						bao.reset();
						LOG.info("create folder Thumnail in zip node image to "
								+ in.getId());
						zos.putNextEntry(new ZipEntry(files.get("FOLDER")
								.getName()
								+ File.separator
								+ files.get("FOLDERTHUMNAIL").getName()
								+ File.separator + in.getId() + ".jpg"));
						ImageIO.write(in.getImageThumnail(), "jpg", bao);
						zos.write(bao.toByteArray());
						zos.closeEntry();
					} catch (Exception e) {
						LOG.error("App: sendImages(List<PackImages>, List<PackImages>,"
								+ " Map<String, File> error creating zip image node "
								+ e.getMessage());
					}
				} else {
					try {
						LOG.info("Saving node image to " + in.getId());
						ImageUtils.saveImage(files.get("FOLDER")
								+ File.separator + in.getId() + ".jpg",
								in.getImage());
						LOG.info("Saving node Thumnail image to " + in.getId());
						ImageUtils.saveImage(files.get("FOLDERTHUMNAIL")
								+ File.separator + in.getId() + ".jpg",
								in.getImageThumnail());
					} catch (Exception e) {
						LOG.error("App: saveImages(List<PackImages>, List<PackImages>,"
								+ " Map<String, File> error save image node "
								+ e.getMessage());
					}
				}
			}
		}
	}

	private PackImages getImageCorpusRamdom(List<PackImages> packI) {
		PackImages pack = null;
		int ram = 0;
		if ((packI != null) && (!packI.isEmpty())) {
			ram = (int) (Math.random() * (packI.size()));
			pack = packI.get(ram);
		}
		return pack;
	}

	/**
	 * 
	 * @param nodeId
	 * @param recursively
	 * @param files
	 * @param corpusId
	 */
	public void copyImages(String nodeId, boolean recursively,
			Map<String, File> files, String corpusId) {
		copyImages(nodeId, recursively, null, files, corpusId);

	}

	/**
	 * 
	 * @param nodeid
	 * @param recursively
	 * @return
	 * @throws SQLException
	 */
	private List<String> getNodes(String nodeid, boolean recursively)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query;
		List<String> list = new ArrayList<String>();

		// if we need only one specify node
		try {
			if (!recursively) {
				list.add(nodeid);
			} else {
				query = "SELECT NODEID FROM NODE START WITH NODEID = ? "
						+ "CONNECT BY PRIOR NODEID = PARENTID";
				ps = con.prepareStatement(query);
				ps.setLong(1, Long.parseLong(nodeid));
				rs = ps.executeQuery();
				String nodeIdAux;
				while (rs.next()) {
					nodeIdAux = rs.getString("NODEID");
					list.add(nodeIdAux);
				}
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
		}
		return list;

	}

	/**
	 * get images from one node
	 * 
	 * @param nodeid
	 * @param imageQuality
	 * @param num
	 * @param width
	 * @param height
	 * @return
	 */
	private List<PackImages> getImagesNode(String nodeid, float imageQuality,
			int num, int width, int height) {
		LOG.debug("ImageUtilsDB: getImagesNode(String,float,int,int,int)");
		PreparedStatement ps = null;
		String query = "SELECT NODEID, IMAGE FROM NODEIMAGES "
				+ "WHERE NODEID=? AND ROWNUM <= ?";
		ResultSet rs = null;
		int index = 1;
		InputStream is = null;
		BufferedImage bi = null;
		BufferedImage biThumnail = null;
		BufferedImage biComposite = null;
		PackImages pi = null;
		String id = null;
		List<PackImages> list = new ArrayList<PackImages>();
		try {
			ps = con.prepareStatement(query);
			ps.setLong(1, Long.parseLong(nodeid));
			ps.setLong(2, num);
			rs = ps.executeQuery();
			while (rs.next()) {
				try {
					pi = new PackImages();
					id = rs.getString("NODEID");
					LOG.info("Get image " + index + " from nodeid: " + id);
					is = rs.getBlob("IMAGE").getBinaryStream();
					bi = ImageIO.read(is);
					bi = ImageUtils.convertToJPEGImage(bi, 72, imageQuality);
					if (index > 1) {
						pi.setId(id + "-" + index);
					} else {
						pi.setId(id);
					}
					biComposite = ImageUtils.getCompositeImage(bi, prop);
					pi.setImage(biComposite);
					biThumnail = ImageUtils.scaleImage(bi, width, height, null,true);
					pi.setImageThumnail(biThumnail);
					list.add(pi);
					index++;
					LOG.info("get image ok!");
				} catch (Exception e) {
					LOG.error("ImageUtilsDB: getImagesNode"
							+ "(String, float,int,int,int) " + "reading images"
							+ e.getMessage() + "nodeid: " + id + " index: "
							+ index);
					continue;
				}
			}
		} catch (Exception e) {
			LOG.error("Exception" + e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					LOG.error("ImageUtilsDB: getImagesNode"
							+ "(String, float,int,int,int)"
							+ " error close resulset " + e.getMessage());
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					LOG.error("ImageUtilsDB: getImagesNode"
							+ "(String, float,int,int,int) "
							+ "error close prepareStatement " + e.getMessage());
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LOG.error("ImageUtilsDB: getImagesNode"
							+ "(String, float,int,int,int) "
							+ "error close InputStream " + e.getMessage());
				}
			}
		}
		return list;
	}

	/**
	 * 
	 * @param node
	 * @return
	 * @throws SQLException
	 */
	public String getCorpusidFromNode(String node) throws SQLException {
		LOG.debug("ImageUtilsDB: getCorpusidFromNode(String)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT CORPUSID FROM NODE WHERE NODEID = ? ";
		try {
			ps = con.prepareStatement(query);
			ps.setString(1, node);
			rs = ps.executeQuery();
			String corpusId = null;
			;
			if (rs.next()) {
				corpusId = rs.getString("CORPUSID");
			} else {
				LOG.info("uncaught corpusId");
			}
			return corpusId;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
		}
	}

	/**
	 * 
	 * @param corpus
	 * @return
	 * @throws SQLException
	 */
	public String getCorpusNameFromCorpus(String corpus) throws SQLException {
		LOG.debug("ImageUtilsDB: getCorpusNameFromCorpus(String)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT CORPUS_NAME FROM CORPUS WHERE CORPUSID = ? ";
		try {
			ps = con.prepareStatement(query);
			ps.setString(1, corpus);
			rs = ps.executeQuery();
			String corpusName = null;
			;
			if (rs.next()) {
				corpusName = rs.getString("CORPUS_NAME");
			} else {
				LOG.info("uncaught corpus name");
			}
			return corpusName;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
		}
	}

	/**
	 * 
	 * @param corpusid
	 * @return
	 */
	public String getRootNode(String corpusid) throws SQLException {
		LOG.debug("ImageUtilsDB: getRootNode(String)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "SELECT NODEID FROM NODE WHERE CORPUSID = ? "
				+ "AND PARENTID=-1";
		try {
			ps = con.prepareStatement(query);
			ps.setString(1, corpusid);
			rs = ps.executeQuery();
			String rootid = null;
			;
			if (rs.next()) {
				rootid = rs.getString("NODEID");
			} else {
				LOG.info("uncaught root node");
			}
			return rootid;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
		}

	}

	/**
	 * close connection. only have one connection per instance
	 * 
	 * @return
	 */
	public boolean close() {
		LOG.debug("ImageUtilsDB: close()");
		if (con != null) {
			try {
				con.close();
				LOG.debug("Close connection ok!");
				return true;
			} catch (SQLException e) {
				LOG.error("Error closing connection");
				return false;
			}
		}
		return false;
	}
}
