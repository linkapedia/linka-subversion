package com.linkapedia.admintool.services.s3.bo.beans;

import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */

public class SelectedFiles{
    private static final Logger log = Logger.getLogger(SelectedFiles.class);
    
    private List<String> resources;

    public List<String> getResources() {
        return resources;
    }

    public void setResources(List<String> resource) {
        this.resources = resource;
    }
}