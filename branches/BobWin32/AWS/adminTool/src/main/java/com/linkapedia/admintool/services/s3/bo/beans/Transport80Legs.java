package com.linkapedia.admintool.services.s3.bo.beans;

import java.util.List;
import org.apache.log4j.Logger;

/**
 * return this object in json to paint in the web page
 * @author andres
 */
public class Transport80Legs{
    private static final Logger log = Logger.getLogger(Transport80Legs.class);

    private boolean truncated;
    private String marker;
    private List<Object80Legs> keys;

    public boolean isTruncated() {
        return truncated;
    }

    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public List<Object80Legs> getKeys() {
        return keys;
    }

    public void setKeys(List<Object80Legs> keys) {
        this.keys = keys;
    }
}