package com.linkapedia.admintool.services.s3.bo;

import com.linkapedia.admintool.services.s3.bo.beans.SelectedFiles;
import com.linkapedia.admintool.services.s3.bo.beans.Transport80Legs;
import java.security.InvalidParameterException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class Facade {

    private static final Logger log = Logger.getLogger(Facade.class);
    private static final int MAX_KEYS = 1000; // max is 1000

    /**
     * get all files in the bucket
     * @return 
     */
    public static Transport80Legs getFiles() {
        log.debug("Facade: getFiles()");
        return S3Access.getFiles("", null, MAX_KEYS, null);
    }
    
    /**
     * use this to the search bar
     * @param prefix
     * @return 
     */
    public static Transport80Legs getFiles(String prefix){
        log.debug("Facade: getFiles(String)");
        return S3Access.getFiles(prefix, null, MAX_KEYS, null);
    }
    
    /**
     * get next items (use for the pagination)
     * @param marker
     * @return 
     */
    public static Transport80Legs getNextPage(String marker){
        log.debug("Facade: getNextPage(String)");
         return S3Access.getFiles("", null, MAX_KEYS, marker);
    }

    /**
     * save files into s3
     * @param data
     * @throws InvalidParameterException 
     */
    public static void process(SelectedFiles data) throws InvalidParameterException {
        log.debug("Facade: process(String)");
        if (data == null) {
            throw new InvalidParameterException("the object data is null");
        } else {
            if(data.getResources().isEmpty()){
                log.info("The resources selected is Empty");
                return;
            }
            List<String> files = data.getResources();
            if (S3Access.saveFile(files)) {
                log.info("File saved ok!");
            } else {
                log.info("Error saving file");
            }
        }
    }
}
