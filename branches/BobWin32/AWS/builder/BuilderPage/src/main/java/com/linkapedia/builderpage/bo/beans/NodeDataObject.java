package com.linkapedia.builderpage.bo.beans;

/**
 * Node Object, object with the required data to build menu,bridgenode.
 *
 * @author juanidrobo
 */
public class NodeDataObject {

    private String nodeId;
    private String name;
    private String encodeName;
    // position, is used to order by comparator.
    private Integer position;
    private String description;
    private String imageSrc;

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public NodeDataObject(String nodeId, String name) {
        this.nodeId = nodeId;
        this.name = name;
    }

    public NodeDataObject() {
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEncodeName() {
        return encodeName;
    }

    public void setEncodeName(String encodeName) {
        this.encodeName = encodeName;
    }
}
