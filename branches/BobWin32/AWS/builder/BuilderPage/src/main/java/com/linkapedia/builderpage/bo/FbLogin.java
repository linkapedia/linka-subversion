package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.api.beans.UserFb;
import com.intellisophic.linkapedia.api.beans.enums.UserRole;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.util.KISSmetrics;
import com.linkapedia.builderpage.util.ManageResourcesUtils;
import com.linkapedia.builderpage.util.SEOUtils;
import com.linkapedia.builderpage.util.UtilEnviroment;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import net.sf.json.JSONObject;
/**
 *
 * @author juanidrobo
 */
public class FbLogin {

    private static final Logger log = Logger.getLogger(FbLogin.class);
    String tokenFb;
    User user;
    com.restfb.types.User userRestFb;
    UserFb userFb;
    HttpSession session;
    String nextUrl; //next url after login

    public JSONObject FacebookLogin(String token, HttpSession sess, String next) {

        nextUrl = next;
        session = sess;
        tokenFb = token;
        String urlToredirect = null;
        JSONObject loginStatus = new JSONObject();

        KISSmetrics kISSmetrics = null;
        HashMap<String, String> dataKissMetrics = new HashMap<String, String>();
        try {
            String kissMetricToken = UtilEnviroment.getKissmetricId();
            kISSmetrics = new KISSmetrics(kissMetricToken);

        } catch (Exception ex) {
            log.info("error getting the KissMetric Token from config");
        }

        try {



            FacebookClient facebookClient = new DefaultFacebookClient(tokenFb);

            userRestFb = facebookClient.fetchObject("me", com.restfb.types.User.class);

            //getting the user from UserFb 
            userFb = DataHandlerFacade.getUserFb(userRestFb.getId());

            if (userFb != null) {
                log.info("The User FbId " + userFb.getFbId() + " is registered in the UserFb Table");
                user = DataHandlerFacade.getUser(userFb.getUserId());
                if (user != null) {
                    log.info("The email " + user.getEmail() + " is registered in the User Table");
                    this.updateUser();
                    urlToredirect = this.signIn();
                    if (kISSmetrics != null) {
                        kISSmetrics.identify(user.getUserId());
                        loginStatus.put("login","loginWithFacebook");
                    }

                } else {
                    log.error("The userID " + userFb.getUserId() + " is NOT registered in the User Table");
                }
            } else {

                // I have to wait for the user to be registered in User table,
                // because Dynamo returns the inserted user with the userId assigned
                // then register in the Facebook Table
                this.register();
                this.registerFb();

                urlToredirect = this.signIn();

                if (kISSmetrics != null) {
                    loginStatus.put("login","createAccount");
                }
            }

        } catch (com.restfb.exception.FacebookOAuthException e) {

            log.error(e.getMessage());
        } catch (com.restfb.exception.FacebookJsonMappingException e) {

            log.error(e.getMessage());
        } catch (IllegalArgumentException e) {

            log.error(e.getMessage());
        } catch (Exception e) {

            log.error("Exception: " + e.getMessage());
        } finally {
            loginStatus.put("url",urlToredirect);
            return loginStatus;
        }
    }

    private User register() throws IOException {
        try {
            user = new User();
            Properties fbCredentials = new Properties();

            String name = userRestFb.getFirstName();
            if (userRestFb.getMiddleName() != null) {
                name += " " + userRestFb.getMiddleName();
            }

            user.setFirstName(name);
            user.setLastName(userRestFb.getLastName());
            user.setEmail(userRestFb.getEmail());
            user.setUserRole(UserRole.REGISTERED);
            fbCredentials.put("fbToken", tokenFb);
            fbCredentials.put("fbId", userRestFb.getId());
            user.setFacebookCredentials(fbCredentials);
            Date dateTime = new Date();
            long timeStamp = dateTime.getTime();
            user.setTimeStamp(timeStamp);
            DataHandlerFacade.saveUser(user);
            return user;
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return null;
    }

    private void registerFb() {
        log.info("Save Facebook Data!");
        userFb = new UserFb();
        userFb.setUserId(user.getUserId());
        userFb.setFbId(user.getFacebookCredentials().getProperty("fbId"));
        try {
            DataHandlerFacade.saveUserFb(userFb);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
    }

    private void updateUser() throws Exception {
        log.info("Update User Facebook Credentials in User table");
        Properties fbCredentials = new Properties();
        String name = userRestFb.getFirstName();
        if (userRestFb.getMiddleName() != null) {
            name += " " + userRestFb.getMiddleName();
        }
        fbCredentials.put("fbToken", tokenFb);
        fbCredentials.put("fbId", userFb.getFbId());
        user.setFirstName(name);
        user.setLastName(userRestFb.getLastName());
        user.setEmail(userRestFb.getEmail());
        user.setFacebookCredentials(fbCredentials);
        user.setUserRole(UserRole.REGISTERED);
        DataHandlerFacade.updateUser(user);
    }

    private String signIn() throws IOException {
        log.info("Signing the user!");
        String signInUrl = null;
        session.setAttribute("user", user);
        if (!nextUrl.isEmpty()) {
            signInUrl = nextUrl;
        }

        if (nextUrl.equals("RootNodeId")) {
            try {
                NodeStatic rootNode = DataHandlerFacade.getNode(UtilEnviroment.getRootIdByEnv());
                String[] childrenIds = rootNode.getChildrenIDs().split("\\|\\|");
                Random rnd = new Random(System.currentTimeMillis());
                int random = rnd.nextInt(childrenIds.length);

                signInUrl = UtilEnviroment.getContext() + "/page/" + SEOUtils.encodeFileName(rootNode.getTitle()) + "/" + childrenIds[random];
            } catch (Exception ex) {
                log.error("Exception Signing ex: " + ex.getMessage());
            }
        }
        return signInUrl;
    }
}
