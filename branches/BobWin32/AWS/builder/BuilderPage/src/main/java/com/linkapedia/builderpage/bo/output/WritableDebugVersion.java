package com.linkapedia.builderpage.bo.output;

import com.linkapedia.builderpage.bo.beans.DebugVersionObject;
import com.linkapedia.builderpage.bo.comparators.SentenceComparator;
import com.linkapedia.core.DocumentSentence;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class WritableDebugVersion implements IWritable<List<DocumentSentence>> {

    private static final Logger LOG = Logger.getLogger(WritableDebugVersion.class);
    private HttpServletResponse res;
    private PrintWriter pw;
    private String callback;

    /**
     *
     * @param res
     * @param callback
     * @throws IOException
     */
    public WritableDebugVersion(final HttpServletResponse res, String callback) throws IOException {
        this.res = res;
        this.res.setContentType("application/json;charset=UTF-8");
        this.pw = this.res.getWriter();
        this.callback = callback;
    }

    public boolean write(List<DocumentSentence> o) {
        JSONObject result = new JSONObject();
        if (o == null || o.isEmpty()) {
            LOG.error("printJson list = null.");
            result.put("status", "0");
            result.put("message", "Sentence Extract Version Missing");
        } else {
            result.put("status", "1");
            result.put("message", "ok");
            JSONArray ja = new JSONArray();
            SentenceComparator sc = new SentenceComparator();
            Collections.sort(o, sc);
            JSONObject jObj;
            for (DocumentSentence obj : o) {
                jObj = new JSONObject();
                jObj.put("sentence", StringEscapeUtils.escapeHtml4(obj.getSentence()));
                jObj.put("scoreNodeTitle", obj.getScoreByNodeTitle());
                jObj.put("scoreMH", obj.getScoreByMustHaves());
                jObj.put("scoreSig", obj.getScoreBySignatures());
                jObj.put("signatures", obj.getSignatures());
                jObj.put("musthaves", obj.getMustHaves());
                ja.add(jObj);
            }
            result.put("sentences", ja);
        }
        pw.println(callback + "(" + result.toString() + ");");
        return true;
    }
}
