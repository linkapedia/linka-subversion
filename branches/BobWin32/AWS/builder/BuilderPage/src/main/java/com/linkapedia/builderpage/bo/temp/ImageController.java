package com.linkapedia.builderpage.bo.temp;

import com.amazonaws.services.s3.model.ObjectListing;
import com.intellisophic.linkapedia.amazon.services.S3.bo.ManageBucket;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class ImageController {

    public static ManageBucket mbNode;
    public static ManageBucket mbTaxonomy;
    private static final int TAXONOMY = 1;
    private static final int NODE = 2;
    private static final Logger log = Logger.getLogger(ImageController.class);

    static {
        log.debug("ImageController: init the buckets");
        mbNode = new ManageBucket("nodeimages");
        mbTaxonomy = new ManageBucket("taxonomyimages");
    }

    /**
     * get file numbers 1= taxonomy, 2 = node fixme: search a good way to do
     * this without a ManageBucket by default
     *
     * @param key
     * @param type
     * @return
     */
    public static int getNumFiles(String key, int type) {
        log.debug("ImageController: getNumFiles");
        int imagesN = 0;
        ObjectListing list = null;
        if (type == TAXONOMY) {
            list = mbTaxonomy.listFiles(key, null, 1000, null);
        } else if (type == NODE) {
            list = mbNode.listFiles(key, null, 1000, null);
        } else {
            log.error("ImageController: parameter type not found");
            return 0;
        }
        if (list != null) {
            imagesN = list.getObjectSummaries().size();
        }
        return imagesN;
    }
}
