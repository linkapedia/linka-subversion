package com.linkapedia.builderpage.bo.beans;

/**
 * object for pages with links
 *
 * @author andres
 */
public class LinkObject {

    private String title;
    private String encodeTitle;
    private String digest;
    private String fileName;
    private String link;
    private String cols;   //masonry configuration
    private String imageSrc;

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCols() {
        return cols;
    }

    public void setCols(String cols) {
        this.cols = cols;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public String getEncodeTitle() {
        return encodeTitle;
    }

    public void setEncodeTitle(String encodeTitle) {
        this.encodeTitle = encodeTitle;
    }
}
