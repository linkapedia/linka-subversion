package com.linkapedia.builderpage.bo.beans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juanidrobo
 */
public class PaginatorUserFeedObject {

    private List<String> eventsList = new ArrayList<String>();
    private Integer nextIndexObject;
    private boolean end;

    public Integer getNextIndexObject() {
        return nextIndexObject;
    }

    public void setNextIndexObject(Integer nextIndexObject) {
        this.nextIndexObject = nextIndexObject;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public List<String> getEventsList() {
        return eventsList;
    }

    public void setEventList(String event) {
        this.eventsList.add(event);
    }

    public void setEvents(List<String> eventsList) {
        this.eventsList = eventsList;
    }
}
