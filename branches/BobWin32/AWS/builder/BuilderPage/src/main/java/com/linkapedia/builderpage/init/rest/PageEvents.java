package com.linkapedia.builderpage.init.rest;

import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollectionElement;
import com.intellisophic.linkapedia.api.beans.User;
import com.linkapedia.page.events.Node;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import org.apache.log4j.Logger;

/**
 *
 * @author juanidrobo
 */
@Path("/page-events")
public class PageEvents {

    private static final Logger log = Logger.getLogger(PageEvents.class);

    @Context
    @GET
    @Path("/joinNode/{nodeId}")
    public String joinNode(@PathParam(value = "nodeId") final String nodeId, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: joinNode");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = Node.join(user, nodeId);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @GET
    @Path("/unjoinNode/{nodeId}")
    public String unjoinNode(@PathParam(value = "nodeId") final String nodeId, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: unjoinNode");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = Node.unjoin(user, nodeId);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @GET
    @Path("/followUser/{userId}")
    public String followUser(@PathParam(value = "userId") final String userId, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: followUser");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = com.linkapedia.page.events.User.follow(user, userId);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @GET
    @Path("/unFollowUser/{userId}")
    public String unFollowUser(@PathParam(value = "userId") final String userId, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: unfollowUser");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = com.linkapedia.page.events.User.unFollow(user, userId);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @POST
    @Path("/createCollection")
    @Consumes("application/json")
    @Produces("application/json")
    public InterestCollection createCollection(InterestCollection interestCollection, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: createCollection");
        InterestCollection response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            if (interestCollection.getInterestCollectionName() == null || interestCollection.getInterestCollectionName().trim().isEmpty()) {
                return null;
            }
            response = com.linkapedia.page.events.Collection.createInterestCollection(user, interestCollection);
            return response;
        }
        return null;
    }

    @Context
    @POST
    @Path("/addInterestCollection")
    @Consumes("application/json")
    public String addInterestCollection(InterestCollectionElement interestCollectionElement, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: addInterestCollection");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = com.linkapedia.page.events.Collection.addInterestCollection(user, interestCollectionElement);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @POST
    @Path("/removeInterestCollection")
    @Consumes("application/json")
    public String removeInterestCollection(InterestCollectionElement interestCollectionElement, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: removeInterestCollection");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = com.linkapedia.page.events.Collection.removeInterestCollection(user, interestCollectionElement);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @POST
    @Path("/updateInterestCollection")
    @Consumes("application/json")
    public String updateInterestCollection(InterestCollection interestCollection, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: updateInterestCollection");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = com.linkapedia.page.events.Collection.updateInterestCollection(user, interestCollection);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @GET
    @Path("/makePublicInterestCollection/{interestCollectionId}")
    public String makePublicInterestCollection(@PathParam(value = "interestCollectionId") final String interestCollectionId, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: makePublicInterestCollection");
        boolean response;
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            response = com.linkapedia.page.events.Collection.makePublicInterestCollection(user, interestCollectionId);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @POST
    @Path("/getInterestCollectionFollowedByUser")
    @Produces("application/json")
    public List<InterestCollection> getInterestCollectionFollowedByUser(@Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: getInterestCollectionFollowedByUser");
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        List<InterestCollection> interestCollections = null;
        if (user != null) {
            interestCollections = com.linkapedia.page.events.Collection.getInterestCollectionFollowedByUser(user);
        }
        return interestCollections;
    }

    @Context
    @GET
    @Path("/getInterestCollectionOwnedByUser")
    @Produces("application/json")
    public List<InterestCollection> getInterestCollectionOwnedByUser(@Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: getOwnedInterestCollection");
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        List<InterestCollection> interestCollectionsOwned = null;
        if (user != null) {
            interestCollectionsOwned = com.linkapedia.page.events.Collection.getInterestCollectionOwnedByUser(user);
        }
        return interestCollectionsOwned;
    }

    @Context
    @GET
    @Path("/followCollection/{interestCollectionId}")
    public String followInterestCollection(@PathParam(value = "interestCollectionId") final String interestCollectionId, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: followInterestCollection");
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        boolean response;
        if (user != null) {
            response = com.linkapedia.page.events.Collection.followInterestCollection(user, interestCollectionId);
            return Boolean.toString(response);
        }
        return "false";
    }

    @Context
    @GET
    @Path("/unFollowCollection/{interestCollectionId}")
    public String unFollowInterestCollection(@PathParam(value = "interestCollectionId") final String interestCollectionId, @Context HttpServletRequest req) throws Exception {
        log.debug("PageEvents: unFollowInterestCollection");
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        boolean response;
        if (user != null) {
            response = com.linkapedia.page.events.Collection.unFollowInterestCollection(user, interestCollectionId);
            return Boolean.toString(response);
        }
        return "false";
    }
}
