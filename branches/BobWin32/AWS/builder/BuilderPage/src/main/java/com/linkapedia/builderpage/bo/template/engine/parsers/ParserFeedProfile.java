package com.linkapedia.builderpage.bo.template.engine.parsers;

import com.intellisophic.linkapedia.api.beans.UserFeed;
import com.linkapedia.builderpage.util.SEOUtils;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.apache.velocity.tools.config.DefaultKey;

/**
 * class to parser the data from event table This class is a velocity tool
 * @FIXME: this is only a class to test.
 * we need think in other architecture to do this
 * @author andres
 */
@DefaultKey("ParserFeedProfile")
public class ParserFeedProfile {

    private static final String MESSAGE_FOLLOWING = "started following";
    private static final String MESSAGE_FOLLOWED = "is being followed by";
    private static final String MESSAGE_UNFOLLOWING = "stopped following";
    private static final String MESSAGE_UNFOLLOWED = "stopped being followed by";
    private static final String MESSAGE_JOIN_PREFIX = "joined the";
    private static final String MESSAGE_JOIN_SUFIX = "interest comunity";
    private static final String MESSAGE_UNJOIN = "unjoined the";
    private static final String MESSAGE_REGISTER = "joined Knomor";
    private static final String MESSAGE_CREATE_COLLECTION_PREFIX = "created the";
    private static final String MESSAGE_CREATE_COLLECTION_SUFIX = "interest collection";
    private static final String MESSAGE_ADD_COLLECTION_PREFIX = "added the document";
    private static final String MESSAGE_ADD_COLLECTION_CONNECTOR = "to the";
    private static final String MESSAGE_ADD_COLLECTION_SUFIX = "interest collection";
    private static final String MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_PREFIX = "added the document";
    private static final String MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_MIDDLE1 = "that belongs to the topic";
    private static final String MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_MIDDLE2 = "that you are following, in the";
    private static final String MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_MIDDLE3 = "interest collection";
    private static final String MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_SUFIX = "'s reason to add the document:";
    private static final String MESSAGE_MAKE_PUBLIC_INTEREST_COLLECTION_PREFIX = "make public";
    private static final String MESSAGE_MAKE_PUBLIC_INTEREST_COLLECTION_SUFIX = "interest collection";
    private static final String MESSAGE_FOLLOWING_COLLECTION_PREFIX = "started following";
    private static final String MESSAGE_FOLLOWING_COLLECTION_SUFIX = "interest collection";
    private static final Logger log = Logger.getLogger(ParserFeedProfile.class);

    public String parser(UserFeed userFeed) {
        if (userFeed == null) {
            log.error("ParserFeedProfile: userFeed can't be null");
            return "";
        }
        JSONObject json = JSONObject.fromObject(userFeed.getData());

        //show the time ago with the server data
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String nowAsString = df.format(new Date(userFeed.getTimeStamp()));

        //specific the parser by event (read the correct data)
        if (json.get("TYPE").equals("FOLLOW")) {
            return parserFollow(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("FOLLOWED")) {
            return parserFollowed(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("UNFOLLOW")) {
            return parserUnFollow(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("UNFOLLOWED")) {
            return parserUnFollowed(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("JOIN")) {
            return parserJoin(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("UNJOIN")) {
            return parserUnJoin(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("REGISTER")) {
            return parserRegister(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("CREATE_COLLECTION")) {
            return parserCreateCollection(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("ADD_INTEREST_COLLECTION")) {
            return parserAddInterestCollection(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION")) {
            return parserAddFollowedTopicInterestCollection(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("MAKE_COLLECTION_PUBLIC")) {
            return parserMakePublicInterestCollection(json, userFeed.getUserName(), nowAsString);
        } else if (json.get("TYPE").equals("FOLLOW_COLLECTION")) {
            return parserFollowInterestCollection(json, userFeed.getUserName(), nowAsString);
        } else {
            log.info("The Event type is not support now: " + json.get("TYPE"));
        }

        return "";
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserFollow(JSONObject json, String userName, String time) {
        String message = "";
        String imageUrl = "http://graph.facebook.com/" + json.get("followedUserFbId") + "/picture?type=small";
        message = "<p class='show'><span>" + userName + " " + MESSAGE_FOLLOWING + "</span>"
                + "<a href='ProfileCreation?userId=" + json.get("followedUserId") + "'><img src='" + imageUrl + "'/>"
                + "<span class=\"orange\">" + json.getString("followedUserName") + "&nbsp;&nbsp;&nbsp;</span></a>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserUnFollow(JSONObject json, String userName, String time) {
        String message = "";
        String imageUrl = "http://graph.facebook.com/" + json.get("followedUserFbId") + "/picture?type=small";
        message = "<p class='show'><span>" + userName + " " + MESSAGE_UNFOLLOWING + "</span>"
                + "<a href='ProfileCreation?userId=" + json.get("followedUserId") + "'><img src='" + imageUrl + "'/>"
                + "<span class=\"orange\">" + json.getString("followedUserName") + "&nbsp;&nbsp;&nbsp;</span></a>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserFollowed(JSONObject json, String userName, String time) {
        String message = "";
        String imageUrl = "http://graph.facebook.com/" + json.get("followingUserFbId") + "/picture?type=small";
        message = "<p class='show'><span>" + userName + " " + MESSAGE_FOLLOWED + "</span>"
                + "<a href='ProfileCreation?userId=" + json.get("followingUserId") + "'><img src='" + imageUrl + "'/>"
                + "<span class=\"orange\">" + json.getString("followingUserName") + "&nbsp;&nbsp;&nbsp;</span></a>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserUnFollowed(JSONObject json, String userName, String time) {
        String message = "";
        String imageUrl = "http://graph.facebook.com/" + json.get("followingUserFbId") + "/picture?type=small";
        message = "<p class='show'><span>" + userName + " " + MESSAGE_UNFOLLOWED + "</span>"
                + "<a href='ProfileCreation?userId=" + json.get("followingUserId") + "'><img src='" + imageUrl + "'/>"
                + "<span class=\"orange\">" + json.getString("followingUserName") + "&nbsp;&nbsp;&nbsp;</span></a>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserJoin(JSONObject json, String userName, String time) {
        String message = "";
        message = "<p class='show'><span>" + userName + " " + MESSAGE_JOIN_PREFIX + "</span><a href='" + UtilEnviroment.getContext() + "/page/" + SEOUtils.encodeFileName((String) json.get("nodeTitle")) + "/" + json.get("nodeId") + "' class=\"head_btn2\">"
                + "<span class=\"blue\">" + json.get("nodeTitle") + "</span></a>"
                + "<span>" + MESSAGE_JOIN_SUFIX + "&nbsp;&nbsp;&nbsp;</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserUnJoin(JSONObject json, String userName, String time) {
        String message = "";
        message = "<p class='show'><span>" + userName + " " + MESSAGE_UNJOIN + "</span><a href='" + UtilEnviroment.getContext() + "/page/" + SEOUtils.encodeFileName((String) json.get("nodeTitle")) + "/" + json.get("nodeId") + "' class=\"head_btn2\">"
                + "<span class=\"blue\">" + json.get("nodeTitle") + "</span></a>"
                + "<span>" + MESSAGE_JOIN_SUFIX + "&nbsp;&nbsp;&nbsp;</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserRegister(JSONObject json, String userName, String time) {
        String message = "";
        message = "<span>" + userName + " " + MESSAGE_REGISTER + "</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserCreateCollection(JSONObject json, String userName, String time) {
        String message = "";
        message = "<p class='show'>" + "<span class='textUserFeedRight'>" + userName + " " + MESSAGE_CREATE_COLLECTION_PREFIX + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/collection/" + SEOUtils.encodeFileName((String) json.get("interestCollectionName")) + "/" + json.get("interestCollectionId") + "' class=\"\">"
                + "<span class=\"blue linkUserFeed\">" + json.get("interestCollectionName") + "</span></a>"
                + "<span class='textUserFeedLeft textUserFeedRight'>" + MESSAGE_CREATE_COLLECTION_SUFIX + "</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserAddInterestCollection(JSONObject json, String userName, String time) {
        String message = "";
        message = "<p class='show'>" + "<span class='textUserFeedRight'>" + userName + " " + MESSAGE_ADD_COLLECTION_PREFIX + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/digest/" + SEOUtils.encodeFileName((String) json.get("docTitle")) + "/" + json.get("nodeId") + "/" + json.get("docId") + "' class=\"\">"
                + "<span class=\"blue linkUserFeed\">" + json.get("docTitle") + "</span></a>"
                + "<span class='textUserFeedLeft textUserFeedRight'>" + MESSAGE_ADD_COLLECTION_CONNECTOR + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/collection/" + SEOUtils.encodeFileName((String) json.get("interestCollectionName")) + "/" + json.get("interestCollectionId") + "' class=\"\">"
                + "<span class=\"blue linkUserFeed\">" + json.get("interestCollectionName") + "</span></a>"
                + "<span class='textUserFeedLeft textUserFeedRight'>" + MESSAGE_ADD_COLLECTION_SUFIX + "&nbsp;&nbsp;&nbsp;</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserAddFollowedTopicInterestCollection(JSONObject json, String userName, String time) {
        String message = "";
        String messageUserReason = (String)(json.get("userReason")==null?"":json.get("userReason"));
        message = "<p class='show'>" + "<span class='textUserFeedRight'>" + userName + " " + MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_PREFIX + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/digest/" + SEOUtils.encodeFileName((String) json.get("docTitle")) + "/" + json.get("nodeId") + "/" + json.get("docId") + "' class=\"\">"
                + "<span class=\"blue linkUserFeed\" >" + json.get("docTitle") + "</span></a>"
                + "<span class='textUserFeedLeft'>" + MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_MIDDLE1 + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/page/" + SEOUtils.encodeFileName((String) json.get("nodeName")) + "/" + json.get("nodeId") + "' class=\"head_btn2\">"
                + "<span class=\"blue\">" + json.get("nodeName") + "</span></a><br><br>"
                + "<span class='textUserFeedRight'>" + MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_MIDDLE2 + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/collection/" + SEOUtils.encodeFileName((String) json.get("interestCollectionName")) + "/" + json.get("interestCollectionId") + "' class=\"\">"
                + "<span class=\"blue linkUserFeed\">" + json.get("interestCollectionName") + "</span></a>"
                + "<span class='textUserFeedLeft'>" + MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_MIDDLE3 + "&nbsp;&nbsp;&nbsp;</span>"
                + "<span>" + userName + " " + MESSAGE_ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION_SUFIX + "</span>"
                + "<span>" + messageUserReason+ "&nbsp;&nbsp;&nbsp;</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserMakePublicInterestCollection(JSONObject json, String userName, String time) {
        String message = "";
        message = "<p class='show'>" + "<span class='textUserFeedRight'>" + userName + " " + MESSAGE_MAKE_PUBLIC_INTEREST_COLLECTION_PREFIX + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/collection/" + SEOUtils.encodeFileName((String) json.get("interestCollectionName")) + "/" + json.get("interestCollectionId") + "' class=\"\">"
                + "<span class=\"blue linkUserFeed\">" + json.get("interestCollectionName") + "</span></a>"
                + "<span class='textUserFeedLeft'>" + MESSAGE_MAKE_PUBLIC_INTEREST_COLLECTION_SUFIX + "&nbsp;&nbsp;&nbsp;</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }

    /**
     * TODO: test case for this method
     *
     * @param json //data to extract the info
     * @param userName //userName the event owner
     * @param time // time to show the timeago
     * @return
     */
    private String parserFollowInterestCollection(JSONObject json, String userName, String time) {
        String message = "";
        message = "<p class='show'>" + "<span class='textUserFeedRight'>" + userName + " " + MESSAGE_FOLLOWING_COLLECTION_PREFIX + "</span>"
                + "<a href='" + UtilEnviroment.getContext() + "/collection/" + SEOUtils.encodeFileName((String) json.get("interestCollectionName")) + "/" + json.get("interestCollectionId") + "' class=\"\">"
                + "<span class=\"blue linkUserFeed\">" + json.get("interestCollectionName") + "</span></a>"
                + "<span class='textUserFeedLeft'>" + MESSAGE_FOLLOWING_COLLECTION_SUFIX + "&nbsp;&nbsp;&nbsp;</span>"
                + "<span>-&nbsp;&nbsp;<abbr class=\"timeago\" title=" + time + "></abbr></span></p>";
        return message;
    }
}
