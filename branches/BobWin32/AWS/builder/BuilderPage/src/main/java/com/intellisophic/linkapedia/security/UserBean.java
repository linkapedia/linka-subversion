package com.intellisophic.linkapedia.security;

import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.api.beans.UserFb;
import com.intellisophic.linkapedia.api.beans.enums.UserRole;
import com.intellisophic.linkapedia.api.exceptions.ObjectAlreadyExistsException;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Bancolombia
 */
public class UserBean {

    private static final Logger log = Logger.getLogger(UserBean.class);
    /**
     * Creates a new instance of UserBean
     */
    private String email;
    private String firstName;
    private String lastName;
    private String fbId;
    private String fbToken;

    public UserBean() {
    }

    public String save() {
        log.debug("save()");
        FacesContext ctx = FacesContext.getCurrentInstance();
        try {
            if (getEmail() != null && !getEmail().isEmpty() && getFirstName() != null && !getFirstName().isEmpty() && getLastName() != null && !getLastName().isEmpty()) {
                User user = new User();
                user.setEmail(getEmail());
                user.setFirstName(getFirstName());
                user.setLastName(getLastName());
                user.setUserRole(UserRole.REGISTERED);
                if (!"".equals(getFbId())) {
                    Properties fbCredentials = new Properties();
                    fbCredentials.put("fbToken", getFbToken());
                    fbCredentials.put("fbId", getFbId());
                    user.setFacebookCredentials(fbCredentials);
                    UserFb userFb = new UserFb();
                    userFb.setFbId(getFbId());
                    //userFb.setEmail(getEmail());
                    DataHandlerFacade.saveUserFb(userFb);
                }
                DataHandlerFacade.saveUser(user);
                return "success";
            }
        } catch (ObjectAlreadyExistsException ex) {
            log.error("The user object already Exists.", ex);
            ctx.addMessage("Error", new FacesMessage("Error: The user Email is already registered"));
            return "error";
        } catch (Exception ex) {
            log.error("An exception has ocurred while trying to save the user.", ex);
            ctx.addMessage("Error", new FacesMessage("Error: " + ex.getMessage()));
            return "error";
        }
        return "error";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firsName) {
        this.firstName = firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }

    public String getFbToken() {
        return fbToken;
    }
}
