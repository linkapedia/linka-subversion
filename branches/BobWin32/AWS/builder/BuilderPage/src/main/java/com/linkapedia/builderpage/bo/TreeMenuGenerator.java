package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import com.linkapedia.builderpage.bo.comparators.NodeDataComparator;
import com.linkapedia.builderpage.util.TreeMenuUtils;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TreeMenuGenerator {

    private static final Logger log = Logger.getLogger(TreeMenuGenerator.class);
    private LinkedList<NodeDataObject> level1;
    private LinkedList<NodeDataObject> level2;
    private LinkedList<NodeDataObject> level3;
    //special level only if the node not have children
    private LinkedList<NodeDataObject> level0;
    public static final String TREE_LEVEL1 = "level1";//parent
    public static final String TREE_LEVEL2 = "level2";//me
    public static final String TREE_LEVEL3 = "level3";//child
    public static final String TREE_LEVEL0 = "level0";//grandparent
    private boolean hasChild;
    private boolean showLevel1;

    /**
     * constructor this initialize the tree for one node
     *
     * @param node
     */
    public TreeMenuGenerator(NodeStatic node) {
        level1 = null;
        level2 = null;
        level3 = null;
        level0 = null;
        generateMenu(node);
    }

    /**
     * logic to generate the menu
     *
     * @param node
     */
    private void generateMenu(NodeStatic node) {
        //in a case the parent are the grandfathers
        log.debug("Generating menu...");

        //build the levels in a good structure
        level1 = TreeMenuUtils.getList(node.getParentIDs(), node.getParentNames(), node.getParentIndexWithinParent());//parents
        level2 = TreeMenuUtils.getList(node.getSiblingIDs(), node.getSiblingNames(), node.getSiblingIndexWithinParent());//siblings
        level3 = TreeMenuUtils.getList(node.getChildrenIDs(), node.getChildrenNames(), node.getChildrenIndexWithinParent());//sons
        level0 = TreeMenuUtils.getList(node.getGrandParentIDs(), node.getGrandParentNames(), node.getGrandParentIndexWithinParent());//grandparents


        if (UtilEnviroment.isDev() || UtilEnviroment.isKnomor() || UtilEnviroment.isLocal()) {
            //logic to conect with nodelink table.
            LinkNodeTreeMenuGenerator menuLinkObject = new LinkNodeTreeMenuGenerator(node);
            if (level1 == null) {
                level1 = menuLinkObject.generateLevel(LinkNodeTreeMenuGenerator.TREE_LEVEL1);
            }
            if (level2 == null) {
                //search siblings by others parents
                level2 = menuLinkObject.generateLevel(LinkNodeTreeMenuGenerator.TREE_LEVEL2);
            }
            if (level0 == null) {
                level0 = menuLinkObject.generateLevel(LinkNodeTreeMenuGenerator.TREE_LEVEL0);
            }
        }
        //sort each level by IndexWithinParent
        NodeDataComparator com = new NodeDataComparator();
        if (level1 != null) {
            Collections.sort(level1, com);
        }
        if (level2 != null) {
            Collections.sort(level2, com);
        }
        if (level3 != null) {
            Collections.sort(level3, com);
        }
        if (level0 != null) {
            Collections.sort(level0, com);
        }

        //specific logic (to mark the route)    
        //add the first nodes in the levels 
        /**
         * LEVEL 1
         */
        NodeDataObject aux = null;
        if (level1 == null) {
            //I need to verify that father exists
            //Fixme compare by "1-", please! ask andres R.
            if (node.getParentID() != null && !TreeMenuUtils.isRootNode(node) && !TreeMenuUtils.isTaxonomyRootNode(node)) {
                level1 = new LinkedList<NodeDataObject>();
                aux = new NodeDataObject(String.valueOf(node.getParentID()), node.getParentName());
                level1.addFirst(aux);
            }
        } else {
            //move the parents to first position
            if (TreeMenuUtils.isRootNode(node)) {
                //add the multiple parent
                level1.addAll(0, LinkNodeTreeMenuGenerator.getParentNodeLinkToMenu(node.getId()));
            } else {
                //move the father to the first position
                aux = new NodeDataObject(String.valueOf(node.getParentID()), node.getParentName());
                level1.addFirst(aux);
            }
        }

        //if the current node not have siblings
        /**
         * LEVEL 2
         */
        if (level2 == null) {
            level2 = new LinkedList<NodeDataObject>();
            aux = new NodeDataObject(String.valueOf(node.getId()), node.getTitle());
            level2.addFirst(aux);
        } else {
            //move the father to the first position
            aux = new NodeDataObject(String.valueOf(node.getId()), node.getTitle());
            level2.addFirst(aux);
        }

        /**
         * LEVEL 0
         */
        //if the grandfather not have siblings
        if (level0 == null) {
            //I need to verify that grandfather exists
            //Fixme compare by "1-", please! ask andres R.
            if (node.getGrandParentID() != null && !TreeMenuUtils.isRootNode(node) && !TreeMenuUtils.isTaxonomyRootNode(node)) {
                level0 = new LinkedList<NodeDataObject>();
                aux = new NodeDataObject(String.valueOf(node.getGrandParentID()), node.getGrandParentName());
                level0.addFirst(aux);
            }
        } else {
            //move the parents to first position
            if (TreeMenuUtils.isRootNode(node)) {
                //add the multiple grandparent
                List<NodeDataObject> data = LinkNodeTreeMenuGenerator.getGrandParentNodeLinkToMenu(node.getId());
                if (data == null || data.isEmpty()) {
                    level0 = null;
                } else {
                    level0.addAll(0, data);
                }
            } else {
                //move the father to the first position
                NodeStatic grandParent;
                try {
                    grandParent = DataHandlerFacade.getNode(node.getParentID());
                    if (TreeMenuUtils.isRootNode(grandParent)) {
                        level0.addAll(0, LinkNodeTreeMenuGenerator.getParentNodeLinkToMenu(grandParent.getId()));
                    } else {
                        aux = new NodeDataObject(String.valueOf(grandParent.getParentID()), grandParent.getParentName());
                        level0.addFirst(aux);
                    }
                } catch (Exception e) {
                    log.error("Exception getting grandparent", e);
                }
            }
        }
    }

    /**
     * get LinkedList with the level information
     *
     * @param level
     * @return
     */
    public LinkedList<NodeDataObject> getLevel(String level) {
        log.info("Get " + level);
        if (level.equals(TREE_LEVEL1)) {
            return level1;
        } else if (level.equals(TREE_LEVEL2)) {
            return level2;
        } else if (level.equals(TREE_LEVEL3)) {
            return level3;
        } else if (level.equals(TREE_LEVEL3)) {
            return level3;
        } else if (level.equals(TREE_LEVEL0)) {
            return level0;
        } else {
            log.error("The level is not found");
            return null;
        }
    }

    /**
     * build the menu with the business logic set level 0 in level 3 if is null
     * show or not show level 1
     */
    public void buildMenu() {
        showLevel1 = true;
        hasChild = true;
        if (level3 == null || level3.isEmpty()) {
            level3 = level2;
            level2 = level1;
            //we need show the grandparents
            level1 = this.getLevel(TreeMenuGenerator.TREE_LEVEL0);
            hasChild = false;
        }
        if (level1 == null || level1.isEmpty()) {
            showLevel1 = false;
        }
    }

    /**
     * use this in the template
     *
     * @return
     */
    public boolean isFather() {
        return hasChild;
    }

    /**
     * use this in the template
     *
     * @return
     */
    public boolean isRoot() {
        return showLevel1;
    }
}
