/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import java.util.LinkedList;

/**
 *
 * @author alexander
 */
public class BreadCrumbs {
    private int childs=0;
    LinkedList<NodeDataObject> childrens;
    NodeStatic parent;
    
    public BreadCrumbs(NodeStatic paren){        
        parent = paren;
    }
    
    public int getTotalChilds(){
        return childs;
    }
    
    public LinkedList<NodeDataObject> getChilds(){
        return childrens;
    }
    
    public void setChilds(LinkedList<NodeDataObject> child){
        childrens = child;
        if(child != null){
            childs = child.size();
        }
    }
    
    public NodeStatic getParent(){
        return parent;
    }
}   
