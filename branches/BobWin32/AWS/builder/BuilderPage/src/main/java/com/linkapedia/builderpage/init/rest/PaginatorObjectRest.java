package com.linkapedia.builderpage.init.rest;

import com.linkapedia.builderpage.bo.PaginatorGenerator;
import com.linkapedia.builderpage.bo.beans.PaginatorLinkObject;
import com.linkapedia.builderpage.bo.beans.PaginatorNodeObject;
import com.linkapedia.builderpage.bo.beans.PaginatorUserFeedObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;

/**
 *
 * @author juanidrobo
 */
@Path("/paginator")
public class PaginatorObjectRest {

    private static final Logger log = Logger.getLogger(PaginatorObjectRest.class);

    @GET
    @Path("/getLinkObjects/{nodeId}/{init}/{nObjects}") //Init is the index to start counting the Nobjects to retrieve
    @Produces(MediaType.APPLICATION_JSON)
    public PaginatorLinkObject getLinkObjects(@PathParam(value = "nodeId") final String nodeId, @PathParam(value = "init") final Integer init, @PathParam(value = "nObjects") final Integer nObjects) throws Exception {
        log.debug("PaginatorLinkObjectREST: getObjects");
        PaginatorLinkObject paginatorLinkObject = null;
        try {
            paginatorLinkObject = PaginatorGenerator.getPaginatorLinkObject(nodeId, init, nObjects);
        } catch (Exception ex) {
            log.error("Error in PaginatorLinkObjectRest, exception: " + ex.getMessage());
        }
        return paginatorLinkObject;


    }

    @GET
    @Path("/getNodeObjects/{nodeId}/{init}/{nObjects}") //Init is the index to start counting the Nobjects to retrieve
    @Produces(MediaType.APPLICATION_JSON)
    public PaginatorNodeObject getNodeObjects(@PathParam(value = "nodeId") final String nodeId, @PathParam(value = "init") final Integer init, @PathParam(value = "nObjects") final Integer nObjects) throws Exception {

        log.debug("PaginatorNodeObjectREST: getObjects");
        PaginatorNodeObject paginatorNodeObject = null;
        try {
            paginatorNodeObject = PaginatorGenerator.getPaginatorNodeObject(nodeId, init, nObjects);
        } catch (Exception ex) {
            log.error("Error in PaginatorNodeObjectRest, exception: " + ex.getMessage());
        }
        return paginatorNodeObject;
    }

    @GET
    @Path("/getUserFeedObjects/{userId}/{init}/{nObjects}") //Init is the index to start counting the Nobjects to retrieve
    @Produces(MediaType.APPLICATION_JSON)
    public PaginatorUserFeedObject getUserFeedObjects(@PathParam(value = "userId") final String userId, @PathParam(value = "init") final Integer init, @PathParam(value = "nObjects") final Integer nObjects) throws Exception {

        log.debug("PaginatorNodeObjectREST: getObjects");
        PaginatorUserFeedObject paginatorUserFeedObject = null;
        try {
            paginatorUserFeedObject = PaginatorGenerator.getPaginatorUserFeedObject(userId, init, nObjects);
        } catch (Exception ex) {
            log.error("Error in PaginatorNodeObjectRest, exception: " + ex.getMessage());
        }
        return paginatorUserFeedObject;
    }
}
