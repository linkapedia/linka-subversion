package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.UserFeed;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.beans.LinkObject;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import com.linkapedia.builderpage.bo.beans.PaginatorLinkObject;
import com.linkapedia.builderpage.bo.beans.PaginatorNodeObject;
import com.linkapedia.builderpage.bo.beans.PaginatorUserFeedObject;
import com.linkapedia.builderpage.bo.comparators.NodeDataComparator;
import com.linkapedia.builderpage.bo.template.engine.parsers.ParserFeedProfile;
import com.linkapedia.builderpage.util.BuilderUtil;
import com.linkapedia.builderpage.util.SEOUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author juanidrobo
 */
public class PaginatorGenerator {

    private static final Logger log = Logger.getLogger(FbLogin.class);

    public static PaginatorLinkObject getPaginatorLinkObject(final String nodeId, final Integer init, final Integer nObjects) throws Exception {
        PaginatorLinkObject paginatorLinkObject = new PaginatorLinkObject();
        NodeStatic node = DataHandlerFacade.getNode(nodeId);
        List<NodeDocument> documents = null;
        List<LinkObject> links = null;
        List<LinkObject> linksToReturn = null;

        if (node == null) {
            log.info("Error getting NodeStatic:");
            return paginatorLinkObject;
        }
        documents = DataHandlerFacade.getNodeDocumentsByNodeId(nodeId);
        if (documents == null) {
            log.info("Error getting the documents:");
            return paginatorLinkObject;
        }

        BuilderUtil.sorterDocuments(documents);
        links = BuilderUtil.getLinks(documents, node);
        links = BuilderUtil.removeLinkObjectWithoutDigest(links);
        int nObjects1 = (init + nObjects >= links.size()) ? links.size() : init + nObjects;
        try {
            linksToReturn = links.subList(init, nObjects1);
            BuilderUtil.calculateColumns(linksToReturn);
            BuilderUtil.calculateLinksImageSrc(linksToReturn);
            BuilderUtil.calculateTitle(linksToReturn, 5);
            paginatorLinkObject.setLinkObjectList(linksToReturn);
        } catch (IndexOutOfBoundsException e) {
            log.error("Error with init and nObjects parameters.", e);
            paginatorLinkObject.setLinkObjectList(links);
            nObjects1 = links.size();
        } catch (IllegalArgumentException e) {
            log.error("Error with init and nObjects parameters.", e);
            paginatorLinkObject.setLinkObjectList(links);
            nObjects1 = links.size();
        }

        paginatorLinkObject.setNextIndexObject(nObjects1);
        if (nObjects1 == links.size()) {
            paginatorLinkObject.setEnd(true);
        }
        return paginatorLinkObject;

    }

    public static PaginatorNodeObject getPaginatorNodeObject(final String nodeId, final Integer init, final Integer nObjects) throws Exception {
        PaginatorNodeObject paginatorNodeObject = new PaginatorNodeObject();
        NodeStatic node = DataHandlerFacade.getNode(nodeId);
        //get clildren in a NodeStatic object for get the description.
        paginatorNodeObject.setCorpusIdReverse(node.getTaxonomyID().toString());
        int totalSize = 0;

        if (node.getChildrenIDs() != null) {
            String[] childrenData = node.getChildrenIDs().split("\\|\\|");
            String[] childrenIndexWithinParent = node.getChildrenIndexWithinParent().split("\\|\\|");
            List<NodeDataObject> childNodes = new ArrayList<NodeDataObject>();
            totalSize = childrenData.length;

            for (int i = 0; i < totalSize; i++) {
                NodeDataObject childNode = new NodeDataObject();
                childNode.setNodeId(childrenData[i]);
                childNode.setPosition(Integer.parseInt(childrenIndexWithinParent[i]));
                childNodes.add(childNode);
            }
            NodeDataComparator com = new NodeDataComparator();
            Collections.sort(childNodes, com);

            if (init + nObjects < totalSize) {
                childNodes = childNodes.subList(init, init + nObjects);
            } else {
                childNodes = childNodes.subList(init, totalSize);
            }

            String[] childrenNodeList = new String[childNodes.size()];
            for (int i = 0; i < childNodes.size(); i++) {
                childrenNodeList[i] = childNodes.get(i).getNodeId();
            }
            List<NodeStatic> children = DataHandlerFacade.getNodesByNodeIDs(childrenNodeList);

            for (NodeStatic var : children) {
                NodeDataObject nodeDataObject = new NodeDataObject();
                nodeDataObject.setNodeId(var.getId());
                nodeDataObject.setName(var.getTitle());
                nodeDataObject.setEncodeName(SEOUtils.encodeFileName(var.getTitle()));
                nodeDataObject.setDescription(var.getDescription());

                paginatorNodeObject.setNodeObjectList(nodeDataObject);
            }

            paginatorNodeObject.setNextIndexObject(init + nObjects);

        }

        paginatorNodeObject.setObjectList(BuilderUtil.setDeafultDescription(paginatorNodeObject.getNodeObjectList(), nodeId, true));
        BuilderUtil.calculateNodesImageSrc(paginatorNodeObject.getNodeObjectList(), node.getTaxonomyID());

        if (init + nObjects >= totalSize) {
            paginatorNodeObject.setEnd(true);
        }
        return paginatorNodeObject;
    }

    public static PaginatorUserFeedObject getPaginatorUserFeedObject(final String userId, final Integer init, final Integer nObjects) throws Exception {
        PaginatorUserFeedObject paginatorUserFeedObject = new PaginatorUserFeedObject();
        List<UserFeed> userFeedList = DataHandlerFacade.getUserFeed(userId);
        int totalSize = 0;
        if (userFeedList != null && !userFeedList.isEmpty()) {

            totalSize = userFeedList.size();

            if (init + nObjects < totalSize) {
                userFeedList = userFeedList.subList(init, init + nObjects);
            } else {
                userFeedList = userFeedList.subList(init, totalSize);
                paginatorUserFeedObject.setEnd(true);
            }

            paginatorUserFeedObject.setNextIndexObject(init + nObjects);

        }

        ParserFeedProfile parserFeedProfile = new ParserFeedProfile();
        for (UserFeed userFeed : userFeedList) {
            paginatorUserFeedObject.setEventList(parserFeedProfile.parser(userFeed));
        }

        return paginatorUserFeedObject;
    }
}
