package com.linkapedia.builderpage.bo.beans;

import java.util.List;

/**
 *
 * @author andres
 */
public class InfoVersionObject {

    private String docDigest="";
    private String url;
    private String title;
    private List<NodeRelated> nodesRelated;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public class NodeRelated {

        private String id;
        private String title;
        private String encodeTitle;
        private int weight;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public String getEncodeTitle() {
            return encodeTitle;
        }

        public void setEncodeTitle(String encodeTitle) {
            this.encodeTitle = encodeTitle;
        }
    }

    public String getDocDigest() {
        return docDigest;
    }

    public void setDocDigest(String docDigest) {
        this.docDigest = docDigest;
    }

    public List<NodeRelated> getNodesRelated() {
        return nodesRelated;
    }

    public void setNodesRelated(List<NodeRelated> nodesRelated) {
        this.nodesRelated = nodesRelated;
    }
}
