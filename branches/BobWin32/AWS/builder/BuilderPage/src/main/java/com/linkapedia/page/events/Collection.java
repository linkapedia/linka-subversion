package com.linkapedia.page.events;

import com.intellisophic.linkapedia.api.beans.FollowedUser;
import com.intellisophic.linkapedia.api.beans.FollowingInterestCollection;
import com.intellisophic.linkapedia.api.beans.FollowingUser;
import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollectionElement;
import com.intellisophic.linkapedia.api.beans.InterestCollectionFollowed;
import com.intellisophic.linkapedia.api.beans.NodeInterestCollection;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.util.Date;
import java.util.List;

/**
 *
 * @author juanidrobo
 */
public class Collection {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Node.class);

    public static InterestCollection createInterestCollection(User user, InterestCollection interestCollection) {
        try {
            Date dateTime = new Date();
            long timeStamp = dateTime.getTime();
            interestCollection.setTimeStamp(timeStamp);
            interestCollection.setOwnerId(user.getUserId());
            interestCollection.setOwnerName(user.getFirstName());
            if (interestCollection.getIsPrivate()) {
                interestCollection.setIsPrivate(Boolean.TRUE);
            } else {
                interestCollection.setIsPrivate(Boolean.FALSE);
            }

            DataHandlerFacade.saveInterestCollection(interestCollection);

            FollowingInterestCollection followingInterestCollection = new FollowingInterestCollection();
            followingInterestCollection.setUserId(user.getUserId());
            followingInterestCollection.setInterestCollectionId(interestCollection.getInterestCollectionId());
            followingInterestCollection.setUserName(user.getFirstName());
            followingInterestCollection.setUserFbId(user.getFacebookCredentials().getProperty("fbId"));

            InterestCollectionFollowed interestCollectionFollowed = new InterestCollectionFollowed();
            interestCollectionFollowed.setInterestCollectionId(interestCollection.getInterestCollectionId());
            interestCollectionFollowed.setUserId(user.getUserId());
            dateTime = new Date(); //get different timestamp
            timeStamp = dateTime.getTime();
            interestCollectionFollowed.setTimeStamp(timeStamp);
            interestCollectionFollowed.setInterestCollectionName(interestCollection.getInterestCollectionName());

            DataHandlerFacade.saveFollowingInterestCollection(followingInterestCollection);
            DataHandlerFacade.saveInterestCollectionFollowed(interestCollectionFollowed);


        } catch (Exception ex) {
            log.error("Exception in createCollection : " + ex.getMessage());
            return null;
        }

        return interestCollection;
    }

    public static boolean addInterestCollection(User user, InterestCollectionElement interestCollectionElement) {

        InterestCollection interestCollection = null;
        try {
            interestCollection = DataHandlerFacade.getInterestCollection(interestCollectionElement.getInterestCollectionId());
            if (user.getUserId().equals(interestCollection.getOwnerId())) {

                NodeInterestCollection nodeInterestCollection = new NodeInterestCollection();
                nodeInterestCollection.setInterestCollectionId(interestCollectionElement.getInterestCollectionId());
                nodeInterestCollection.setNodeId(interestCollectionElement.getNodeId());

                Date dateTime = new Date();
                long timeStamp = dateTime.getTime();
                interestCollectionElement.setTimeStamp(timeStamp);

                //this is for the Events, not saving this attribute to dynamo
                interestCollectionElement.setInterestCollectionName(interestCollection.getInterestCollectionName());

                DataHandlerFacade.addInterestCollection(interestCollectionElement);
                DataHandlerFacade.saveNodeInterestCollection(nodeInterestCollection);

            } else {
                log.error("The user trying to add collection is not the owner");
                return false;
            }

        } catch (Exception ex) {
            log.error("Exception in addInterestCollection : " + ex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean removeInterestCollection(User user, InterestCollectionElement interestCollectionElement) {

        InterestCollection interestCollection = null;
        int totalDigest = 0;
        
        try {
            interestCollection = DataHandlerFacade.getInterestCollection(interestCollectionElement.getInterestCollectionId(),true);
            totalDigest = interestCollection.getInterestCollectionElements().size();
            
            if (user.getUserId().equals(interestCollection.getOwnerId()) && totalDigest > 1) {
                NodeInterestCollection nodeInterestCollection = new NodeInterestCollection();
                nodeInterestCollection.setInterestCollectionId(interestCollectionElement.getInterestCollectionId());
                nodeInterestCollection.setNodeId(interestCollectionElement.getNodeId());

                DataHandlerFacade.removeInterestCollection(interestCollectionElement);
                DataHandlerFacade.removeNodeInterestCollection(nodeInterestCollection);
            } else {
                log.error("The user trying to remove collection is not the owner");
                return false;
            }

        } catch (Exception ex) {
            log.error("Exception in removeInterestCollection : " + ex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean updateInterestCollection(User user, InterestCollection interestCollection) {
        InterestCollection interestCollectionFromDynamo;
        try {
            interestCollectionFromDynamo = DataHandlerFacade.getInterestCollection(interestCollection.getInterestCollectionId(), true);
            if (interestCollectionFromDynamo != null) {
                if (interestCollectionFromDynamo.getOwnerId().equals(user.getUserId())) {

                    //just let edit the description and name
                    interestCollectionFromDynamo.setInterestCollectionDescription(interestCollection.getInterestCollectionDescription());
                    interestCollectionFromDynamo.setInterestCollectionName(interestCollection.getInterestCollectionName());
                    DataHandlerFacade.updateInterestCollection(interestCollectionFromDynamo);



                } else {
                    log.error("The user trying to update collection is not the owner");
                    return false;
                }
            } else {
                log.error("The InterestCollection trying to update does NOT exist");
                return false;
            }
        } catch (Exception ex) {
            log.error("error updating interest collection :" + ex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean makePublicInterestCollection(User user, String interestCollectionId) {
        try {
            InterestCollection interestCollection = DataHandlerFacade.getInterestCollection(interestCollectionId);
            if (user.getUserId().equals(interestCollection.getOwnerId())) {

                DataHandlerFacade.setPublicInterestCollection(interestCollection);
            } else {
                log.error("The user trying to make public the collection is not the owner");
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception in makePublicInterestCollection : " + ex.getMessage());
            return false;
        }

        return true;
    }

    public static List<InterestCollection> getInterestCollectionFollowedByUser(User user) {
        List<InterestCollection> interestCollectionsFollowed = null;
        try {
            interestCollectionsFollowed = DataHandlerFacade.getInterestCollectionFollowedByUserId(user.getUserId());

        } catch (Exception ex) {
            log.error("Exception in getInterestCollectionFollowedByUser : " + ex.getMessage());
        }
        return interestCollectionsFollowed;
    }

    public static List<InterestCollection> getInterestCollectionOwnedByUser(User user) {
        List<InterestCollection> interestCollections = null;
        try {
            interestCollections = DataHandlerFacade.getInterestCollectionOwnedByUserId(user.getUserId());
        } catch (Exception ex) {
            log.error("Exception in getInterestCollectionFollowedByUser : " + ex.getMessage());
        }

        return interestCollections;
    }

    public static boolean followInterestCollection(User user, String interestCollectionId) {
        try {
            InterestCollection ic = DataHandlerFacade.getInterestCollection(interestCollectionId);

            Date dateTime = new Date();
            long timeStamp = dateTime.getTime();

            FollowingInterestCollection fic = new FollowingInterestCollection();
            fic.setInterestCollectionId(ic.getInterestCollectionId());
            fic.setUserId(user.getUserId());
            fic.setUserName(user.getFirstName());
            fic.setUserFbId(user.getFacebookCredentials().getProperty("fbId"));

            InterestCollectionFollowed icf = new InterestCollectionFollowed();

            icf.setTimeStamp(timeStamp);
            icf.setInterestCollectionId(interestCollectionId);
            icf.setUserId(user.getUserId());

            //this is for the Events, not saving this attribute to dynamo
            icf.setInterestCollectionName(ic.getInterestCollectionName());


            DataHandlerFacade.saveFollowingInterestCollection(fic);
            DataHandlerFacade.saveInterestCollectionFollowed(icf);

        } catch (Exception ex) {
            log.error("Exception in followInterestCollection : " + ex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean unFollowInterestCollection(User user, String interestCollectionId) {
        try {
            InterestCollection ic = DataHandlerFacade.getInterestCollection(interestCollectionId);

            if (!user.getUserId().equals(ic.getOwnerId())) {

                FollowingInterestCollection fic = new FollowingInterestCollection();
                fic.setInterestCollectionId(ic.getInterestCollectionId());
                fic.setUserId(user.getUserId());

                InterestCollectionFollowed icf = new InterestCollectionFollowed();
                icf.setInterestCollectionId(ic.getInterestCollectionId());
                icf.setUserId(user.getUserId());


                DataHandlerFacade.deleteInterestCollectionFollowed(icf);
                DataHandlerFacade.deleteFollowingInterestCollection(fic);
            } else {
                log.error("The user trying to unfollow the collection is the owner, the owner cannot unfollow their own collection");
                return false;
            }

        } catch (Exception ex) {
            log.error("Exception in unFollowInterestCollection : " + ex.getMessage());
            return false;
        }
        return true;
    }
}
