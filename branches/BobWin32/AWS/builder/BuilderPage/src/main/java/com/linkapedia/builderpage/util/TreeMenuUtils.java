package com.linkapedia.builderpage.util;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TreeMenuUtils {

    private static final Logger log = Logger.getLogger(TreeMenuUtils.class);
    private static final String IS_ROOT_NODE = "-1";
    private static final String IS_TAXONOMY_ROOT_NODE = "-2";

    /**
     * this method require a order. ids, names, etc
     *
     * @param separator
     * @param values
     * @return
     */
    public static LinkedList<NodeDataObject> getListSeperator(String separator, String... values) {
        log.debug("Get list levels");
        List<String[]> valuesList = new ArrayList<String[]>();
        List<String> entireObject = new ArrayList<String>();
        for (String value : values) {
            if (value == null) {
                return null;
            } else {
                valuesList.add(value.split(separator));
            }
        }
        //get number of field
        int fields = valuesList.get(0).length;
        NodeDataObject nodeDataObject = null;
        LinkedList<NodeDataObject> listToReturn = new LinkedList<NodeDataObject>();
        for (int k = 0; k < fields; k++) {
            entireObject.clear();
            for (String[] reg : valuesList) {
                entireObject.add(reg[k]);
            }
            //set the properties. if we need more attributes add here
            nodeDataObject = new NodeDataObject();
            nodeDataObject.setNodeId(entireObject.get(0));
            nodeDataObject.setName(entireObject.get(1));
            nodeDataObject.setPosition(Integer.parseInt(entireObject.get(2)));
            listToReturn.add(nodeDataObject);
        }
        return listToReturn;
    }

    public static LinkedList<NodeDataObject> getList(String... values) {
        return getListSeperator("\\|\\|", values);
    }

    public static boolean isRootNode(NodeStatic node) {
        if (node == null) {
            log.error("Node can't be null");
            return false;

        }
        if (node.getParentID().equals(IS_ROOT_NODE)) {
            return true;
        }
        //FIXME after new datamigration
        if (node.getParentID().equals("1-")) {
            return true;
        }
        return false;
    }

    public static boolean isTaxonomyRootNode(NodeStatic node) {
        if (node == null) {
            log.error("Node can't be null");
            return false;

        }
        if (node.getParentID().equals(IS_TAXONOMY_ROOT_NODE)) {
            return true;
        }
        return false;
    }

    public static List<NodeDataObject> getSiblings(NodeStatic node, String ignoreId) {
        if (node == null || ignoreId == null) {
            log.error("Missing parameters: getSiblings");
        }
        LinkedList<NodeDataObject> siblings = null;
        siblings = TreeMenuUtils.getList(node.getSiblingIDs(), node.getSiblingNames(), node.getSiblingIndexWithinParent());
        if (siblings != null) {
            for (int i = 0; i < siblings.size(); i++) {
                if (siblings.get(i).getNodeId().equals(ignoreId)) {
                    siblings.remove(i);
                }
            }
        }
        return siblings;

    }

    /**
     * return the children but ignore one id
     *
     * @param node
     * @param ignoreId
     * @return
     */
    public static LinkedList<NodeDataObject> getChildren(NodeStatic node, String ignoreId) {
        if (node == null || ignoreId == null) {
            log.error("Missing parameters: getChildren");
        }
        LinkedList<NodeDataObject> children = null;
        children = TreeMenuUtils.getList(node.getChildrenIDs(), node.getChildrenNames(), node.getChildrenIndexWithinParent());
        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i).getNodeId().equals(ignoreId)) {
                    children.remove(i);
                }
            }
        }
        return children;
    }
}
