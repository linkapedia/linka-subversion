package com.linkapedia.builderpage.init;

import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.BuildPageBo;
import com.linkapedia.builderpage.bo.BuilderPageController;
import com.linkapedia.builderpage.bo.output.WritablePage;
import com.linkapedia.builderpage.security.ControlMessageAPI;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author andres
 */
public class ProfileCreation extends HttpServlet {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfileCreation.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(true);
        User currentuser = new User();
        if (session.getAttribute("user") != null) {
            currentuser = (User) session.getAttribute("user");
        }

        //transport the parameters by all layers
        Map<String, Object> parameters = new HashMap<String, Object>();
        String userId = null;
        ControlMessageAPI apiMessage = null;
        try {
            userId = request.getParameter("userId");
            apiMessage = new ControlMessageAPI(response, "");
            //validate nodeId
            if (userId == null || "".equals(userId)) {
                apiMessage.messageMissingParameters();
            } else {
                //start process
                User user = null;
                try {
                    user = DataHandlerFacade.getUser(userId);
                } catch (Exception ex) {
                    log.error("Error getting the user", ex);
                }
                BuildPageBo bp = new BuildPageBo();
                WritablePage writable = new WritablePage(response);
                bp.addObserver(writable);
                boolean isOk;
                BuilderPageController bpc = new BuilderPageController(bp);
                isOk = bpc.createProfilePage(currentuser, user, parameters);
                if (!isOk) {
                    if (UtilEnviroment.isLocal() || UtilEnviroment.isDev()) {
                        apiMessage.messageErrorPageCreation();
                    } else {
                        response.sendRedirect(UtilEnviroment.getContext() + "/home");
                    }
                }
            }
        } finally {
            if (apiMessage != null) {
                apiMessage.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
