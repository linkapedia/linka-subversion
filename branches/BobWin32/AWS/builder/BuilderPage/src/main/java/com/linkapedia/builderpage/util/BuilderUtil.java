package com.linkapedia.builderpage.util;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodb.AmazonDynamoDBClient;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.SentencesGenerator;
import com.linkapedia.builderpage.bo.beans.LinkObject;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import com.linkapedia.builderpage.bo.comparators.DocumentComparator;
import com.linkapedia.builderpage.bo.comparators.NodeDataComparator;
import com.linkapedia.builderpage.bo.comparators.SentenceComparator;
import com.linkapedia.builderpage.bo.temp.ImageController;
import com.linkapedia.core.DocumentSentence;
import com.linkapedia.core.DomainBlacklist;
import com.linkapedia.persistence.dao.DynamoDomainBlacklistDao;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * util class to build a good objects
 *
 * @author andres
 */
public class BuilderUtil {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BuilderUtil.class);

    /**
     * get documents(links) in a good object to manipulate
     *
     * @param documents
     * @param node
     * @return a emply collections if not found documents
     */
    public static List<LinkObject> getLinks(List<NodeDocument> documents, NodeStatic node) {
        log.debug("BuilderUtil: getLinks");
        List<LinkObject> links = new ArrayList<LinkObject>();
        String digest = "";
        LinkObject linkObj = null;
        int limitDocument = 0;
        for (NodeDocument nodeDoc : documents) {
            digest = "";
            
            /**
             * mode this code to the datalayer
             */
            DomainBlacklist domainBlackList = new DomainBlacklist();
            DynamoDomainBlacklistDao dao = new DynamoDomainBlacklistDao();
            BasicAWSCredentials credentials = new BasicAWSCredentials(
                    "AKIAIIRFFKMHLXAUXG7A",
                    "gwZXL3SX2QrCONvwxO9CDj8tNlnPyRS35jQhmBrr");
            AmazonDynamoDBClient client = new AmazonDynamoDBClient(credentials);
            dao.setDynamoClient(client);
            domainBlackList.setDomainBlacklistDao(dao);
            if (domainBlackList.isBlacklisted(nodeDoc.getDocURL())) {
                continue;
            }

            List<DocumentSentence> listSummaries = SentenceUtil.getAllSentencesWithScore(
                    node, nodeDoc.getDocID());

            if (listSummaries != null && listSummaries.size() >= 1) {

                sorterSentences(listSummaries);
                digest = SentencesGenerator.getDigest(listSummaries);

                //must have not found in the sentences
                if (digest == null || digest.isEmpty()) {
                    //select the sentence with the bigger score > 0
                    if (listSummaries.get(0).getScoreByMustHaves()
                            + listSummaries.get(0).getScoreByNodeTitle()
                            + listSummaries.get(0).getScoreBySignatures() > 0) {
                        digest = listSummaries.get(0).getSentence();
                    }
                }
            }

            //set fields for the link
            linkObj = new LinkObject();
            linkObj.setTitle(StringUtil.sanitizeString(nodeDoc.getDocTitle()));
            linkObj.setEncodeTitle(SEOUtils.encodeFileName(nodeDoc.getDocTitle()));
            linkObj.setDigest(StringUtil.sanitizeString(digest));
            linkObj.setFileName(nodeDoc.getDocID());
            linkObj.setLink(nodeDoc.getDocURL());
            links.add(linkObj);
        }
        return links;
    }

    /**
     * set column number by the text size this is used in the template
     *
     * @param links
     */
    public static void calculateColumns(List<LinkObject> links) {
        log.debug("BuilderUtil: calculateColumns");
        String digest;
        for (LinkObject l : links) {
            digest = l.getDigest();
            if (digest == null || digest.equals("")) {
                digest = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.notcontent.document");
                l.setDigest(digest);
            }
            //calculate the columns
            if (digest.toCharArray().length < 300) {
                l.setCols("col1");
            } else if (digest.toCharArray().length < 650) {
                l.setCols("col2");
            } else {
                //truncate the digests
                digest = digest.substring(0, 650);
                digest = digest.concat(" ...");
                l.setDigest(digest);
                l.setCols("col2");
            }

        }
    }

    /**
     * set the correct image for the links
     *
     * @param links
     * @param nodeid
     */
    public static void calculateLinksImageSrc(List<LinkObject> links) {
        String urlDocument;
        int n = 0;
        for (LinkObject lo : links) {
            urlDocument = "https://s3.amazonaws.com/documentimage/" + lo.getFileName() + ".jpg";
            if (checkURL(urlDocument)) {
                lo.setImageSrc(urlDocument);
                continue;
            }
            //load default image
            lo.setImageSrc(null);
        }
    }

    public static String calculateLinksImageSrc(String id) {
        String urlDocument = urlDocument = "https://s3.amazonaws.com/documentimage/" + id + ".jpg";
        if (checkURL(urlDocument)) {
            return urlDocument;
        } else {
            return null;
        }
    }

    public static void calculateLinksImageSrc(LinkObject link) {
        List<LinkObject> links = new ArrayList<LinkObject>();
        links.add(link);
    }

    /**
     * set title from the digest if not found title
     *
     * @param links
     * @param number number of words in the digest to build the title
     */
    public static void calculateTitle(List<LinkObject> links, int number) {
        log.info("BuilderUtil: calculateTitle");
        if (number <= 0) {
            log.info("Parameter number is not valid!");
            return;
        }
        String digestTemp;
        List<String> words;
        int top;
        String title;
        for (LinkObject l : links) {
            digestTemp = l.getDigest();
            if (l.getTitle() == null || l.getTitle().equals("")) {
                title = "";
                digestTemp = digestTemp.replaceAll("\\s+", " ");
                digestTemp = digestTemp.trim();
                words = new ArrayList<String>(Arrays.asList(digestTemp.split(" ")));
                top = (words.size() <= number) ? words.size() : number;
                for (int n = 0; n < top; n++) {
                    title += words.get(n) + " ";
                }
                l.setTitle(title);
            }
        }
    }

    /**
     * set the correct image for the bridgeNodes
     *
     * @param nodes
     * @param nodeid
     */
    public static void calculateNodesImageSrc(List<NodeDataObject> nodes, String taxonomyId) {
        int imageNumber;
        String urlDefault = "https://s3.amazonaws.com/resourcesweb/newdev/images/notImage.jpg";
        String urlImage;
        Random ram = null;
        int n = 0;

        for (NodeDataObject node : nodes) {

            imageNumber = getNumberToRamdom(node.getNodeId() + "/images", 2);
            if (imageNumber > 0) {
                ram = new Random(System.currentTimeMillis());
                n = ram.nextInt(imageNumber);
                urlImage = "https://s3.amazonaws.com/nodeimages/" + node.getNodeId() + "/images/" + node.getNodeId() + "_" + n + ".jpg";
                node.setImageSrc(urlImage);
                continue;
            }

            imageNumber = getNumberToRamdom(taxonomyId + "/images", 1);
            if (imageNumber > 0) {
                ram = new Random(System.currentTimeMillis());
                n = ram.nextInt(imageNumber);
                urlImage = "https://s3.amazonaws.com/taxonomyimages/" + taxonomyId + "/images/" + taxonomyId + "_" + n + ".jpg";
                node.setImageSrc(urlImage);
                continue;
            }

            //search the correct taxonomy for the link nodes(ProcinQ)
            NodeStatic nodeStatic = null;
            try {
                nodeStatic = DataHandlerFacade.getNode(node.getNodeId());
            } catch (Exception e) {
                log.error("Error getting nodestatic", e);
            }
            if (nodeStatic != null) {
                String taxId = nodeStatic.getTaxonomyID();
                imageNumber = getNumberToRamdom(taxId + "/images", 1);
                if (imageNumber > 0) {
                    ram = new Random(System.currentTimeMillis());
                    n = ram.nextInt(imageNumber);
                    urlImage = "https://s3.amazonaws.com/taxonomyimages/" + taxId + "/images/" + taxId + "_" + n + ".jpg";
                    node.setImageSrc(urlImage);
                    continue;
                }
            }

            //set default image if not found image
            node.setImageSrc(urlDefault);


        }
    }

    /**
     * check the resource if exits
     *
     * @param uri
     * @return
     */
    private static boolean checkURL(String uri) {
        URL url = null;
        HttpURLConnection con;
        try {
            url = new URL(uri);
            con = (HttpURLConnection) url.openConnection();
            con.connect();
            if (con.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
                return false;
            }
            return true;
        } catch (MalformedURLException ex) {
            log.error("Error MalformedURLException: checkURL", ex);
        } catch (IOException ex) {
            log.error("Error IO: checkURL", ex);
        }
        return false;
    }

    /**
     * set default description to the NodeDataObject
     *
     * @param list
     * @param description
     * @param useNodeTitle
     * @return
     */
    public static List<NodeDataObject> setDeafultDescription(List<NodeDataObject> list, String description, boolean useNodeTitle) {
        log.debug("BuilderUtil: setDeafultDescription");
        //get clildren in a NodeStatic object for get the description.
        for (NodeDataObject var : list) {
            if (var.getDescription() == null || var.getDescription().trim().isEmpty()) {
                if (useNodeTitle) {
                    var.setDescription(var.getName());
                } else {
                    var.setDescription(description);
                }
            }
        }
        return list;

    }

    /**
     * name encode to show in the url
     *
     * @param list
     * @return
     */
    public static LinkedList<NodeDataObject> setNameEncode(LinkedList<NodeDataObject> list) {
        if (list == null) {
            return null;
        }

        for (NodeDataObject node : list) {
            node.setEncodeName(SEOUtils.encodeFileName(node.getName()));
        }
        return list;
    }

    /**
     * add some attributes to the NodeDataObject from nodestatic
     *
     * @param children
     */
    public static void setBridgeNodeAttributes(List<NodeDataObject> children) {
        log.debug("BuilderUtil: setBridgeNodeAttributes");
        for (NodeDataObject var : children) {
            try {
                NodeStatic node = DataHandlerFacade.getNode(var.getNodeId());
                if (node != null) {
                    var.setDescription(node.getDescription());
                }
            } catch (Exception ex) {
                log.error("NodeStatic is null", ex);
            }
        }

    }

    /**
     * order the documents
     *
     * @param documents
     */
    public static void sorterDocuments(List<NodeDocument> documents) {
        DocumentComparator docc = new DocumentComparator();
        Collections.sort(documents, docc);
    }

    /**
     * order to the nodes
     *
     * @param children
     */
    public static void sorterNodeObject(List<NodeDataObject> children) {
        NodeDataComparator com = new NodeDataComparator();
        Collections.sort(children, com);
    }

    /**
     * order to the sentences
     *
     * @param listSummaries
     */
    public static void sorterSentences(List<DocumentSentence> listSummaries) {
        SentenceComparator com = new SentenceComparator();
        Collections.sort(listSummaries, com);
    }

    /**
     * get file numbers 1= taxonomy, 2 = node
     *
     * @param key
     * @param type
     * @return
     */
    public static int getNumberToRamdom(String key, int type) {
        int imageTaxonomyNumber = ImageController.getNumFiles(key, type);
        return imageTaxonomyNumber;
    }

    /**
     * remove links null or empty
     *
     * @param links
     * @return
     */
    public static List<LinkObject> removeLinkObjectWithoutDigest(List<LinkObject> links) {
        String digest = "";
        List<LinkObject> temp = new ArrayList<LinkObject>();

        for (LinkObject l : links) {
            digest = l.getDigest();
            if (digest == null || digest.isEmpty()) {
                continue;
            }
            temp.add(l);
        }
        return temp;
    }
}
