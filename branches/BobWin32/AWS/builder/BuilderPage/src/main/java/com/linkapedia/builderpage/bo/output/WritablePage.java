package com.linkapedia.builderpage.bo.output;

import com.linkapedia.builderpage.bo.BuildPageBo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Observable;
import java.util.Observer;
import javax.servlet.http.HttpServletResponse;

/**
 * paint the page in the browser
 *
 * @author andres
 */
public class WritablePage implements Observer, IWritable<String> {

    private HttpServletResponse res;
    private PrintWriter pw;

    public WritablePage(final HttpServletResponse res) throws IOException {
        this.res = res;
        this.res.setContentType("text/html;charset=UTF-8");
        this.pw = this.res.getWriter();
    }

    public void update(Observable o, Object arg) {
        if (o instanceof BuildPageBo) {
            BuildPageBo result = (BuildPageBo) o;
            write(result.getPageString());
        }
    }

    public boolean write(String o) {
        pw.println(o);
        pw.flush();
        pw.close();
        return true;
    }
}
