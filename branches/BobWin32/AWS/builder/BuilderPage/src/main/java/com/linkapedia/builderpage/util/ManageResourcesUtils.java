package com.linkapedia.builderpage.util;

import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class ManageResourcesUtils {

    private static final Logger LOG = Logger.getLogger(ManageResourcesUtils.class);
    private static Properties prop;

    /**
     * load the properties file
     *
     * @return
     */
    public static Properties getProperties() {
        if (prop == null) {
            prop = new Properties();
            try {
                prop.load(ManageResourcesUtils.class.getClassLoader().getResourceAsStream("system/config.properties"));
            } catch (Exception ex) {
                LOG.error("Error creating prop object " + ex.getMessage());
            }
        }
        return prop;
    }
}
