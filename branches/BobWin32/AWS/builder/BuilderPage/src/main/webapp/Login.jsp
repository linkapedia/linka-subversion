<%@page import="com.linkapedia.builderpage.util.ManageResourcesUtils"%>
<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%
    String redirect_uri = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facebook.redirect_uri");  
    String clientId = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facebook.client_id");
%>
<f:view>
    <html>
        <head>
            <title>Login</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="css/external.css">
            <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" charset="utf-8"></script>
            <script type="text/javascript" src="http://localhost:8093/sitebeta4/js/facebook.js" charset="utf-8"></script>


        </head>
        <body>

            <div id="fb-root"></div>
            <script>FacebookInit(<%=clientId%>)</script>


            <a id="fbLogin" href="#" onclick="login()"><img src="img/facebook_login_img.png" /></a> 

            <h:form  id="frmLogin"  styleClass="box login">
                <fieldset class="boxBody">
                    <label>Username</label>
                    <h:inputText value="#{LoginBean.login}" id="login" 
                                 requiredMessage="Please enter your name" 
                                 validatorMessage="Invalid e-mail address format"
                                 required="true">   
                        <f:validateRegex pattern="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                    </h:inputText>
                    <label><a href="#" class="rLink" tabindex="5">Forget your password?</a>Password</label>
                    <h:inputSecret value="#{LoginBean.password}" id="password" 
                                   required ="true" requiredMessage="Please enter your password"
                                   validatorMessage="password must be at least 3 characters">
                        <f:validateLength minimum="3" />

                    </h:inputSecret>
                </fieldset>
                <footer>
                    <a id="fbLogin" href="faces/Loginfb.jsp"><img src="img/facebook_login_img.png" /></a> 
                        <h:commandButton value="Login" action="#{LoginBean.singin}" styleClass="btnLogin"  />
                        <h:messages style="color:red;"  />
                </footer>
            </h:form>
        </body>
    </html>

</f:view>