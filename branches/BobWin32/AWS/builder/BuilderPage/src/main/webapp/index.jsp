<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<f:view>
    <html>
        <head>
            <title>Index</title>
            <meta charset="UTF-8">
            <style type="text/css">
                <!--
                body {
                    font: 100% Arial, Helvetica, sans-serif;
                    background: #666666;
                    margin: 0;
                    padding: 0;
                    text-align: center;
                    color: #000000;
                    background-image: url(http://semanticmarketingsystems.com/background.jpg);
                    background-repeat: repeat-x;
                    font-size: 10pt;
                }
                .oneColElsCtr #container {
                    width: 60%;
                    -webkit-border-radius: 12px;
                    border-radius: 12px;
                    background: #FFFFFF;
                    margin: 0 auto; /* the auto margins (in conjunction with a width) center the page */
                    border: 0px;
                    text-align: left;
                    margin-top: 20%;
                }
                .oneColElsCtr #mainContent {
                    padding: 0 20px;
                }
                h1 {
                    font-family: Futura, Arial, Helvetica, sans-serif;
                    font-size: 18pt;
                    font-weight: bold;
                    color: #333333;
                }
                -->
            </style>

            <script>

                function isChecked()
                {
                    //alert (12);
                    if (Agreement.checked == true)
                    {

                        document.getElementById('formLogin:btnlogin').disabled = false;
                    }
                    else
                    {

                        document.getElementById('formLogin:btnlogin').disabled = true;
                    }
                }
            </script>
        </head>
        <body class="oneColElsCtr" onload="document.getElementById('formLogin:btnlogin').disabled = true">
            <h:form  id="formLogin"  styleClass="box login">
                <div id="container" style="background-color: rgba(255, 255, 255, .7)">
                    <div id="mainContent">
                        <h1>&nbsp;</h1>
                        <h1>Confidentiality Agreement</h1>
                        <p>By accessing this site, you acknowledge and agree that the content you are about to view is highly confidential and is not to be disclosed to any other party without the express written consent of SEMMX, Inc. or until SEMMX, Inc. makes the content available to the general public.</p>

                        <label>
                            <input type="checkbox"  name="Agreement" id="Agreement" onchange="isChecked()" />
                            I agree</label>
                        <p>
                            <label>Username</label>
                            <h:inputText value="#{IndexBean.login}" id="login" 
                                         requiredMessage="Please enter your name"        
                                         required="true">   
                            </h:inputText>
                        </p>
                        <p>
                            <label>Password

                            </label>
                            <h:inputSecret value="#{IndexBean.password}" id="password" 
                                           required ="true" requiredMessage="Please enter your password"
                                           validatorMessage="password must be at least 3 characters">
                                <f:validateLength minimum="3" />

                            </h:inputSecret>
                        </p>
                        <p>
                        <footer>
                            <h:commandButton value="Login" action="#{IndexBean.singin}" styleClass="btnLogin" id="btnlogin" />
                            <h:messages style="color:red;"  />
                        </footer>
                        </p>

                        <p>&nbsp;</p>
                        <h2>&nbsp;</h2>
                        <!-- end #mainContent --></div>
                    <!-- end #container --></div>

            </h:form>
        </body>
    </html>

</f:view>