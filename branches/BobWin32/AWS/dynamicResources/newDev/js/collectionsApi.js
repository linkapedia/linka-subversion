/**
    API client to the collections
    @autor: Andres Restrepo
    @autor: Juan Idrobo

**/
var COLLECTIONAPI = function(){

	//object with the info
	var _data={};
	
	var _cookieExpireDays=360;
    
    	var maxNumberOfUsersToShowFollowingCollection=10//default

    	/** 
    	* save a document in a collection
    	**/
	function saveDocumentCollection(collectionId){
 		console.log("saveDocumentCollection(collectionId) saving a document in a collection");
	    	if(collectionId==undefined){
			console.log("saveDocumentCollection(collectionId): can't get collectionId");
			return;
	    	}
		//object to the rest
            json={};
            json.interestCollectionId=collectionId;
            json.docId=_data.documentId;
            json.nodeId=_data.nodeId;
            json.docTitle=_data.documentTitle;
            json.digestText=_data.documentDigest;
            json.nodeName=_data.nodeName;
            json.order=0;
            json.userReason = $("#userReason").val();
            $('#btnSaveDocumentCollection').prop("disabled", true);
	      COLLECTION_DIALOG.messages(3,"Saving the document, please wait");
            $.ajax({
            	type : 'POST',
                	contentType : 'application/json',
                	url : CONTEXT_LINKAPEDIA+'/rest/page-events/addInterestCollection',
                	dataType : 'json',
                	data : JSON.stringify(json),
                	success : function(data) {
		    		if(data!=undefined && data==true){
			    		_kmq.push(['record', 'ADD DOCUMENT TO COLLECTION']);  //KISSMETRIC EVENT
			    		COLLECTION_DIALOG.messages(1,"Success. document saved");
		            	$(".fancybox-close").click();
		    		}else{
		            	console.log("error saving data");
		            	$('#btnSaveDocumentCollection').prop("disabled", false);
			    		COLLECTION_DIALOG.messages(2,"Error saving the document");
		    		}
                	},
                	error: function(){
                  	console.log("error saving data");
                    	$('#btnSaveDocumentCollection').prop("disabled", false);
		    		COLLECTION_DIALOG.messages(2,"Error saving the document");
                	}
            });
    	}
    	/**  
    	* save a collection with a document
    	**/
    	function saveCollection(){
    		console.log("saveCollection() saving a collection and call saveDocumentCollection()");
            var json = {};
            json.interestCollectionName = $('#txtCollectionName').val();
            json.interestCollectionDescription = $('#txtCollectionDescription').val();
            json.isPrivate = $('#chkPublicCollection').attr('checked') ? true : false;

            //validate fields
            if(json.interestCollectionName==undefined || json.interestCollectionName.trim()==""){
                COLLECTION_DIALOG.messages(3,"Plase write a name");
                return;
            }
	    	COLLECTION_DIALOG.messages(3,"Saving the collection, please wait");
	    	$('#btnSaveCollection').prop("disabled", true);
            $.ajax({
            	type : 'POST',
                	contentType : 'application/json',
                	url : CONTEXT_LINKAPEDIA+'/rest/page-events/createCollection',
                	dataType : 'json',
                	data : JSON.stringify(json),
                	success : function(data) {
                  	COLLECTION_DIALOG.messages(1,"Success. collection saved");
		    		$('#btnSaveCollection').prop("disabled", false);
                    	_kmq.push(['record', 'CREATE COLLECTION']);  //KISSMETRIC EVENT
                    	$.cookie('lastCollectionId', data.interestCollectionId, { expires: _cookieExpireDays, path: '/' });
		    		saveDocumentCollection(data.interestCollectionId);
                	},
                	error : function(){
                  	console.log("Error saving the collection");
		    		COLLECTION_DIALOG.messages(2,"Error saving the collection");
		    		$('#btnSaveCollection').prop("disabled", false);
                	}
            });
    	}
    	
	return{
      	//set the data from external resource
        	setObject: function(data){
            	_data=data;
        	},
        	setMaxNumberOfUsersToShowFollowingCollection: function(maxNumber) {
            	maxNumberOfUsersToShowFollowingCollection = maxNumber;
        	},
        	/** save funcionality 
		*   type =1: save a document
		*   type =2: save a collection and a document
		**/
		save : function(type){
			if(type==1){
				var collectionId = $("#slcCollectionsId").find('option:selected').attr('cod');
            		if(collectionId==undefined || collectionId==""){
					console.log("saveDocumentCollection(collectionId): can't get collectionId");
					COLLECTION_DIALOG.messages(3,"Select one collection");
                  		return;
            		}else{
					$.cookie('lastCollectionId', collectionId, { expires: _cookieExpireDays, path: '/' });
				}
				saveDocumentCollection(collectionId);
			}else if(type==2){
				saveCollection();
			}
		},
		//load my collections
        	loadCollections: function(){
            	console.log("loadCollections() loading collections");
            	$("#slcCollectionsId").empty();
            	$('#collectionLoadImage').show();
            	$.ajax({
                		type : 'GET',
                		contentType : 'application/json',
                		url : CONTEXT_LINKAPEDIA+'/rest/page-events/getInterestCollectionOwnedByUser',
                		success : function(data) {
		              	//parse data
		              	if(data!=undefined && data.length>0){
		                  	for(i=0;i<data.length;i++){
		                      		//add to comboBox
							var collId = $.cookie('lastCollectionId');
		                      		if(collId!=undefined){
		                        		if(collId==data[i].interestCollectionId){
		                              		$("#slcCollectionsId").prepend("<option cod='"+data[i].interestCollectionId+"'>"+data[i].interestCollectionName+"</option>");
		                              		continue;
		                          		}
		                      		}
		                      		$("#slcCollectionsId").append("<option cod='"+data[i].interestCollectionId+"'>"+data[i].interestCollectionName+"</option>");
		                  	}
		              	}else{
		                  	console.log("the server return undefined");
						//show create collection
						COLLECTION_DIALOG.firstTimeDialog();
		              	}
		              	$('#collectionLoadImage').hide();
                		},
                		error:function(){
		              	console.log("Error getting collections");
					COLLECTION_DIALOG.messages(2,"Error getting collections");
		              	$('#collectionLoadImage').hide();
                		}
            	});            
        	},
		followCollection: function(collectionId) {
            	if (!lock) {
                		lock = true;
                		$.ajax({
                    		url: CONTEXT_LINKAPEDIA+'/rest/page-events/followCollection/' + collectionId,
                    		success: function(response) {
                        		if (JSON.parse(response)) {
		                      		_kmq.push(['record', 'FOLLOW COLLECTION']);  //KISSMETRIC EVENT
		                      		$('#follow_collection').removeClass();
				                  $('#follow_collection').addClass("unfollowButton");    
				                  $('#followDate').text("Following today");
				                  $('#follow_collection').attr("onclick", "COLLECTIONAPI.unFollowCollection(\""  + collectionId + "\")");
				                  $('#follow_collection').text("(Leave)");
				                  var count = $('#followers_count').text();
		                      		count++;
		                      		$('#followers_count').text(count);
                            			if ($('#collection_followers').size() < maxNumberOfUsersToShowFollowingCollection)
                            			{
                                			$('#user_itself_following').show();
                            			}
                        		}
                        		lock = false;
                    		},
                    		error: function(response) {
                        		lock = false;
                    		}
                		});
            	}
        	},
        	unFollowCollection: function(collectionId) {
            	if (!lock) {
		          	lock = true;
		          	$.ajax({
		              	url: CONTEXT_LINKAPEDIA+'/rest/page-events/unFollowCollection/' + collectionId,
		              	success: function(response) {
		                  	if (JSON.parse(response)) {
		                      		_kmq.push(['record', 'UNFOLLOW COLLECTION']);  //KISSMETRIC EVENT 
		                      		$('#follow_collection').removeClass();
		                     		$('#follow_collection').addClass("back"); 
		                      		$('#followDate').text("");
		                      		$('#follow_collection').attr("onclick", "COLLECTIONAPI.followCollection(\""  + collectionId + "\")");
		                      		$('#follow_collection').text("FOLLOW");
		                      		var count = $('#followers_count').text();
		                      		count--;
		                      		$('#followers_count').text(count);
		                       		$('#user_itself_following').hide();
		                  	}
		                  	lock = false;
		              	},
		              	error: function(response) {
		                  	lock = false;
		              	}
		          	});
            	}
        	},
         	makePublicInterestCollection: function(collectionId) {
            	if (!lock) {
                		lock = true;
                		$.ajax({
                    		url: CONTEXT_LINKAPEDIA+'/rest/page-events/makePublicInterestCollection/' + collectionId,
                    		success: function(response) {
                        		if (JSON.parse(response)) {
                             			_kmq.push(['record', 'MAKE PUBLIC COLLECTION']);  //KISSMETRIC EVENT
                             			$('.make_public').hide();
                        		}
                        		lock = false;
                    		},
                    		error: function(response) {
                        		lock = false;
                    		}
                		});
            	}

        	},
		removeDocumentCollection:function(collectionId,docId,nodeId,that){
            	var json = {};
            	json.interestCollectionId=collectionId;
            	json.docId=docId;
	        	json.nodeId=nodeId;
	        	var objToRemove = that;
            	$(objToRemove).hide();
            	$(objToRemove).next('img').show();
            	$.ajax({
                		type : 'POST',
                		contentType : 'application/json',
                		url : CONTEXT_LINKAPEDIA+'/rest/page-events/removeInterestCollection',
                		dataType : 'json',
                		data : JSON.stringify(json),
                		success : function(data) {
		  		    	if(data!=undefined && data==true){
		                  	_kmq.push(['record', 'REMOVE DOCUMENT FROM COLLECTION']);  //KISSMETRIC EVENT         
		  			    	//remove the portlet
		  			    	console.log("removing document: "+data);
		  			    	//$(objToRemove).parent().remove();
		                  	$(objToRemove).parent().parent().fadeOut('slow',function(){
				                	$(objToRemove).parent().parent().remove();
				                	var container = $('#digests');
				                	container.masonry( 'reload' ); 
		                  	});
		  		    	}else{
							$("#dialog").dialog({
								resizable:false,
								buttons:{
									"ok":function(e){
										$(this).dialog("close");
									}
								},
								modal:true,
								close:function(ev, ui){
									$(objToRemove).show();
									$(objToRemove).next('img').hide();
								}
							});	
						}
                		},
                		error: function(){
                    		console.log("error removing document");
                    		$(objToRemove).next('img').hide();
                    		$(objToRemove).show();
                		}
            	});
		},
         	//update a collection
        	updateCollection: function(){
            	//save data
            	var json = {};
            	json.interestCollectionId=$('#interestCollectionId').val();
            	json.interestCollectionName = $('#editText1').val();
            	json.interestCollectionDescription = $('#editText2').val();

            	//validate fields
            	if(json.interestCollectionName==undefined || json.interestCollectionName.trim()==""){
                		alert("Please write a name for the collection");
                		return;
            	}
            	$.ajax({
                		type : 'POST',
                		contentType : 'application/json',
                		url : CONTEXT_LINKAPEDIA+'/rest/page-events/updateInterestCollection',
                		dataType : 'json',
                		data : JSON.stringify(json),
                		success : function(data) {
		              	if (data){
		                  	_kmq.push(['record', 'UPDATE COLLECTION']);  //KISSMETRIC EVENT
		                  	$('#interestCollectionText1 span').text($('#editText1').val());
		                  	$('#interestCollectionText2 span').text($('#editText2').val());
		              	}         
                		},
                		error : function(){
                  		console.log("Error saving a collection");   
                		}
            	});       
        	}
    	};
}();
