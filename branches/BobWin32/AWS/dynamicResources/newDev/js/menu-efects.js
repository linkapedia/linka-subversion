$(document).ready(function(){
	reziseTopMenu();
	initializeEvents();
});

function reziseTopMenu(){
	var menuLevels = $('#menu > div').filter(".wrap");
	var currentpage = $("#containTopMenu").attr("currentpage");
	var sizeHeader = $("#header").height();
	var sizeMenu = $("#menu").height();
 	
	if(currentpage == "homepage"){
		$("#homepage").css("margin-top",(sizeHeader + sizeMenu + 3));
		if(menuLevels.length == 3){
			$("#buttonCloseOpenMenu").css("margin-top","-4px");
		}else{
			$("#buttonCloseOpenMenu").css("margin-top","34px");
		}
	}else if(currentpage == "profilepage"){
		$(".content,.clearfix").css("margin-top","34px");
	}else if(currentpage == "collectionpage"){
		$("#content").css("margin-top","34px");
	}else if(currentpage == "bridgepage"){
		$("#content").css("margin-top",(sizeHeader + sizeMenu + 3));
		if(menuLevels.length > 2){
			$("#buttonCloseOpenMenu").css("margin-top","-8px");	
		}else{
			$("#buttonCloseOpenMenu").css("margin-top","37px");
		}
	}else if(currentpage == "linkpage"){
		$("#content").css("margin-top",(sizeHeader + sizeMenu + 3));
		$("#buttonCloseOpenMenu").css("margin-top","-8px");
	}else if(currentpage == "digestpage"){
		$("#content").css("margin-top",(sizeHeader + sizeMenu + 3));
		$("#buttonCloseOpenMenu").css("margin-top","-8px");
	}else{
		$("#content").css("margin-top",sizetopmenu);
	}
}

function initializeEvents(){
	$(window).scroll(function(){
		$("#buttonCloseOpenMenu").unbind("click");
		retractAndExpandMenu();	
	});
}

function retractAndExpandMenu(){
	var menuLevels = $('#menu > div').filter(".wrap");
	var scroll = $(window).scrollTop();
	var menuResized = false;
	var showhide = false;
	var time = "fast";
	
	if(menuLevels.length > 2){
		if(scroll < 500){
			if($('#menu').css('top') == "-52px"){
				$('#menu').css('top', '-52px').animate({ top: 34 }, time);
			}
			
			
		}else{
			$("#buttonCloseOpenMenu").click(function(e){
				e.preventDefault();
				if(showhide){
					$('#menu').css('top', '34px').animate({ top: -52 }, time);
					showhide = false;
				}else{
					$('#menu').css('top', '-52px').animate({ top: 34 }, time);
					showhide = true;
				}
			});
			
			if($('#menu').css('top') == "34px"){
				$('#menu').css('top', '34px').animate({ top: -52 }, time);	
			}
		}
	}
}
