/**
	COLLECTION DIALOGS
	@autor: Andres Restrepo
**/

/**
	load dialog "document save" in a collection
**/
var COLLECTION_DIALOG = function() {
	var frmDocument = "<div id='frmSaveDocument'>"+
			      "<h3 id='lblDocumentTitle'></h3>"+
			      "<img id='imgDocumentImage' src='' alt=''>"+
			      "<textarea id='txtDocumentUserReason' rows='4' cols='60' placeholder='What do you like about this article?'></textarea>"+
			      "<div id='divAuxSaveTo'>"+
				"<h1 id='btnSaveTo'>SAVE TO</h1>"+
			      "</div>"+
			      "<a id='btnCreateNew' href='javascript:void(0)' onclick='COLLECTION_DIALOG.showSaveCollection()'>create new</a>"+
			      "<a id='btnOldCollection' href='javascript:void(0)' onclick='COLLECTION_DIALOG.hideSaveCollection()'>show other collections</a>"+
			      "<select id='slcCollectionsId' name='collections'></select>"+
			      "<img id='collectionLoadImage' src='https://s3.amazonaws.com/resourcesweb/newdev/images/loadingCollections.gif'>"+
			      "<div id='frmSaveCollection'></div>"+
			      "<span id='messagesCollectionDialog'></span>"+
			      "<a id='btnSaveDocumentCollection' onclick='COLLECTIONAPI.save(1)' href='javascript:void(0)' class='save_b'>"+
				"<span>SAVE</span>"+
			      "</a>"+
			      "<a id='btnSaveCollection' onclick='COLLECTIONAPI.save(2)' href='javascript:void(0)' class='save_b'>"+
				"<span>SAVE</span>"+
			      "</a>"+
			      "</div>";

	var frmCollection = "<input id='txtCollectionName' type='text' name='collectionName'>"+
			        "<textarea id='txtCollectionDescription' rows='2' cols='50' placeholder='What is this interest collection about?'></textarea>"+
			        "<div id='divAuxPrivateCollection'>"+
				  "<input id='chkPublicCollection' type='checkbox' value='Keep this interest collection private'>"+
				  "Keep this interest collection private"+
			        "</div>";
	var _title;
	var _imageSrc;
	var _fileds={};
	
	/**
	* constructor with the configuration and init the fancybox
	**/
	function init(){
		/**
		* link page
		**/
		$(".saveDocumentLink").fancybox({
			openEffect   : 'none',
		    	closeEffect  : 'none',
		    	fitToView    : false,
		    	autoSize     : false,
		    	autoResize   : false,
		    	scrollOutside: true,

		    	afterLoad   : function() {
				//build the dialog	
				var imageUrl = this.element.parent().parent().parent().find(".linkImage").attr("src");
				var title = this.element.parent().parent().parent().find(".LinkTitle").text();

				//objects necesary
				var dataObject = {};
				dataObject.documentId=this.element.attr('data-docid');				
				dataObject.documentTitle = title;
				dataObject.nodeId=$('#nodeId').val();
		       	dataObject.nodeName=$('#nodeTitle').val();
				var littleDigest = this.element.parent().parent().parent().find(".body_text").text();
				if(littleDigest.length>300){
					littleDigest=littleDigest.substring(0,299);
				}
				dataObject.documentDigest=littleDigest;				
				COLLECTIONAPI.setObject(dataObject);

				//save the fields (data necessary in the dialog)
				_fileds.userName=$('#userName').val();
				_fileds.node=dataObject.nodeName;
				_fileds.parentNodeName=$('#nodeParentName').val();

				if(imageUrl == undefined){
					imageUrl= 'https://s3.amazonaws.com/resourcesweb/newdev/images/no_image.png';
				}
	
				//set dialog to the fancybox
			    	this.content = frmDocument;
				
				// set the attributes
				_title = title;
				_imageSrc=imageUrl;

				COLLECTIONAPI.loadCollections();
		    },
		    afterShow: function(){
				$('#lblDocumentTitle').text(_title);
				$('#imgDocumentImage').attr("src", _imageSrc);
		    }
		});
		/**
		*  digest page
		**/
		$(".saveDocumentDigest").fancybox({
			openEffect   : 'none',
		    	closeEffect  : 'none',
		    	fitToView	 : false,
		    	autoSize	 : false,
		    	autoResize   : false,
		    	scrollOutside: true,

		    	afterLoad   : function() {
				//build the dialog
			    	var imageUrl = $('.article_pic').find("img").attr("src");
				var title = $('.article_details').find("h1").text();;

				//object necesary to the object
				var dataObject = {};
				dataObject.documentTitle = title;
				dataObject.documentId= $('#docId').val();
				dataObject.nodeId=$('#nodeId').val();
				dataObject.nodeName=$('#nodeTitle').val();
				var littleDigest = $('.article_details').find(".body_text").find("p").text();
				if(littleDigest.length>300){
					littleDigest=littleDigest.substring(0,299);
				}
				dataObject.documentDigest=littleDigest;

				COLLECTIONAPI.setObject(dataObject);

				//save the fields (data necessary in the dialog)
				_fileds.userName=$('#userName').val();
				_fileds.node=dataObject.nodeName;
				_fileds.parentNodeName=$('#nodeParentName').val();

				if(imageUrl == undefined){
					imageUrl= 'https://s3.amazonaws.com/resourcesweb/newdev/images/no_image.png';
				}

				//set dialog to the fancybox
			    	this.content = frmDocument;
				
				// set the attributes
				_title = title;
				_imageSrc=imageUrl;

				COLLECTIONAPI.loadCollections();
		    },
		    afterShow: function(){
		    		$('#lblDocumentTitle').text(_title);
				$('#imgDocumentImage').attr("src", _imageSrc);
		    }
		});

	}

	return{
		/**
		* Initialize the fancybox event
		**/
		config: function(){
			init();
		},
		/**
		*  show save Collection
		**/
		showSaveCollection : function(){
			$('#slcCollectionsId').hide();
			$('#btnOldCollection').show();
			$('#btnSaveDocumentCollection').hide();
			$('#btnSaveCollection').show();
			$('#btnCreateNew').hide();
			$('#frmSaveCollection').append(frmCollection).hide().fadeIn('slow', function(){
				$('#txtCollectionName').val(_fileds.userName+"'s "+_fileds.node+" "+_fileds.parentNodeName+" Collection");
			});
		},
		/**
		*  hide save Collection
		**/
		hideSaveCollection : function(){
			$('#txtCollectionName').val("");
			$('#frmSaveCollection').fadeOut('slow',function(){
				$(this).empty();
				$('#btnCreateNew').show();
				$('#btnOldCollection').hide();
				$('#btnSaveDocumentCollection').show();
				$('#btnSaveCollection').hide();
				$('#slcCollectionsId').show();
				COLLECTIONAPI.loadCollections();
			});
		},
		/**
		*  show this if the user not have collections
		**/
		firstTimeDialog : function(){
			$('#slcCollectionsId').hide();
			$('#btnOldCollection').hide();
			$('#btnCreateNew').hide();
			$('#btnSaveDocumentCollection').hide();
			$('#btnSaveCollection').show();
			$('#frmSaveCollection').append(frmCollection).hide().fadeIn('slow', function(){
				$('#txtCollectionName').val(_fileds.userName+"'s "+_fileds.node+" "+_fileds.parentNodeName+" Collection");
			});
		},
		/**
		* show the notification area
		* type=1 : success
		* type=2 : error
		* typr=3 : notification
		**/
		messages: function(type, message){
			if(type==1){
			    $('#messagesCollectionDialog').css("color","green");
			    $('#messagesCollectionDialog').text(message);
			}else if(type==2){
			    $('#messagesCollectionDialog').css("color","red");
			    $('#messagesCollectionDialog').text(message);
			}else if(type==3){
			    $('#messagesCollectionDialog').css("color","blue");
			    $('#messagesCollectionDialog').text(message);
			}
		}	
	};
}();

