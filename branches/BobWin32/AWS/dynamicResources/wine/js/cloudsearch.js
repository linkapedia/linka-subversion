/*
 * Client app to comunicate with the cloud search
 * FIXME: change _taxonomy by taxonomyId
 * Andres Restrepo.
 * version: 1.1 ALPHA
 */
var CONFIG_SEARCH = function() {
	//private members
	var _taxonomy;
	//proxy
	var _URL = 'http://ec2-23-21-89-211.compute-1.amazonaws.com/CloudSearch.php';
	//taxonomy results
	var _PAGINATION_SECTION_1 = 16;
	// another taxonomies results
	var _PAGINATION_SECTION_2 = 16;
	// 1: taxonomy nodes, 2: another taxonomies default 0:  infinitescroll not activated
	var _INFINITE_SCROLL = 0

	// search control
	var _CURRENT_TERM;
	var _CONTROL_SIZE = 0;
	var _QUERY;
	var _QUERY2;
	var _USE_QUERY2 = true;

	//flow control
	var _LOCK = false;
	var _FINISHED = true;
	var _CANCEL_PAINT = false;

	//funcionality control
	var _DELAY_SEARCH = 500;
	var _delayTimer

	// control in the scroll (pagination)
	var _START = 0;
	var _END = _PAGINATION_SECTION_1;

	//view control
	var _selectorsToHidden = [];
	var _selectorParent;
	var _COLOR_MARK = "red";
	var _GENERAL_SITE_NAME = "KNOMOR";

	//private methods

	/**
	 *   select data with the _CURRENT_TERM
	 */
	var markData = function() {
		$(".resultCloudSearch").each(function(index, value) {
			try {
				//title
				var data = "";
				var selector;
				selector = $(".titleTextSearch");
				data = $(value).find(selector).text();
				data = transformMarkData(data);
				$(value).find(selector).html(data);

				//description
				selector = $(".descriptionTextSearch");
				data = $(value).find(selector).text();
				data = transformMarkData(data);
				$(value).find(selector).html(data);

				//path
				selector = $(".pathTextSearch");
				data = $(value).find(selector).text();
				data = transformMarkData(data);
				$(value).find(selector).html(data);

			} catch(ex) {
				console.log("error marking data" + ex);
			}
		});
		
		//select the style for the terms marked
		var selectorMark = $(".markResult");
		$("#content_search").find(selectorMark).css('color', _COLOR_MARK);
		
		$(".resultCloudSearch").click(function(){
			_kmq.push(['record', 'Search Page Result Click']);  //KISSMETRIC EVENT
		});
	}
	/**
	 * <span class="markResult" ...
	 */
	var transformMarkData = function(source) {
		source = source.replace(new RegExp(_CURRENT_TERM, "i"), "<span class='markResult'>" + _CURRENT_TERM + "</span>");
		return source;
	}
	/**
	 * mark one term
	 */
	var markTerm = function(term) {
		term = "<span class='markResult'>" + term + "</span>";
		return term;
	}
	/**
	 * create a template to show the result
	 */
	var createTemplate = function() {
		var contentSearch = $("#content_search");
		if (contentSearch.length == 0) {
			_selectorParent.append($("<div id='content_search'></div>"));
			contentSearch = $("#content_search");
		} else {
			contentSearch.children().remove();
		}
		//create the container
		contentSearch.ready(function() {
			contentSearch.append($("<br><div id='section1_title' class='titleSearch' ></div>"));
			contentSearch.append($("<a href='javascript:void(0);' class='clearButton' onclick='CONFIG_SEARCH.clear();'>CLEAR </a>"));
			contentSearch.append($("<div id='section1_result'><br></div><br>"));
			contentSearch.append($("<div id='section2_title'class='titleSearch' ></div>"));
			contentSearch.append($("<div id='section2_result'></div><br>"));
			contentSearch.append($("<div id='section3_result'></div>"));
			contentSearch.append('<div id="wrapper_infinite_scroll_loading">' +
								 '<div id="infinite_scroll_loading">' +
							     '<img  src="https://s3.amazonaws.com/resourcesweb/newdev/images/infinite.gif" />' +
								 '</div>' +
								 '</div>');
		});
	}
	/*
	 * get the structure to append in the html
	 */
	var getPorletNode = function(data) {
		console.log("getPorletNode");
		var dataHtml = "";
		for ( i = 0; i < data.length; i++) {
			dataHtml += "<div>";
			dataHtml += "<a href='"+CONTEXT_LINKAPEDIA+"/PageCreation?nodeId=" + data[i].nodeId + "'>";
			dataHtml += "<div class='resultCloudSearch clearfix'> <img src='https://s3.amazonaws.com/nodeimages/" + data[i].nodeId + "/images/" + data[i].nodeId + "_0.jpg' class='pic'" + "onerror=\"this.src='https://s3.amazonaws.com/resourcesweb/dev/site-resources/images/thumbnail/notImage.jpg'\" >";
			dataHtml += "<h3 class='titleTextSearch'>" + data[i].nodeName + "</h3>";
			dataHtml += "<p class='descriptionTextSearch'> " + data[i].nodeDesc + "</p>";
			dataHtml += "<h6 class='pathTextSearch' style='clear: both;'> FOUND IN:" + data[i].path + "</h6>";
			dataHtml += "</div>";
			dataHtml += "</a>";
			dataHtml += "</div>";
		}
		return dataHtml;
	};

	/**
	 * hidden the selector to show in the html
	 */
	var hiddenSelectors = function() {
		$.each(_selectorsToHidden, function(index, value) {
			try {
				value.hide();
			} catch(ex) {
				console.log("error hidden data");
			}
		});
	}
	/**
	 * paint the section 3: google
	 */
	var googleSectionPaint = function() {
		$("#section3_result").html("<a target='_blank'  href='https://www.google.com/search?q=" + _CURRENT_TERM + "'>SEARCH for " + _CURRENT_TERM + " on Google</a>");
		$("#section3_result > a").click(function(){
			_kmq.push(['record', 'Search Page One Click']);  //KISSMETRIC EVENT
		});
	}
	/**
	 * control the paint in the web
	 */
	var paintControl = function() {
		_LOCK = false;
		if (_CANCEL_PAINT) {
			_CANCEL_PAINT = false;
			return false;
		}
		return true;
	}
	/*
	 * paint the porlets in the web page
	 */
	var paint = function(data, section) {
		console.log("paint for the section" + section);
		if (paintControl()) {
			console.log(data);
			hiddenSelectors();
			var dataHtml;
			if (section == 1) {
				dataHtml = $(getPorletNode(data));
				dataHtml.css({
					opacity : 0
				});
				$("#section1_result").append(dataHtml);
				dataHtml.animate({
					opacity : 1
				}, {
					duration : 3000
				});
				//set the title
				$("#section1_title").html(markTerm(_CURRENT_TERM) + " IN " + _taxonomy);

			} else if (section == 2) {
				dataHtml = $(getPorletNode(data));
				dataHtml.css({
					opacity : 0
				});

				//set the title
				if (_taxonomy == undefined) {
					// put the data in the section 1 because is one query
					$("#section1_title").html(markTerm(_CURRENT_TERM) + " IN " + _GENERAL_SITE_NAME);
					$("#section1_result").append(dataHtml);
				} else {
					$("#section2_title").html(markTerm(_CURRENT_TERM) + " IN OTHER AREAS");
					$("#section2_result").append(dataHtml);
				}
				dataHtml.animate({
					opacity : 1
				}, {
					duration : 3000
				});
			}
			markData();

			googleSectionPaint();
		}
	}
	/*
	 * paint a error message in the web page
	 */
	var paintError = function(message) {
		console.log("paintError");
		if (paintControl()) {
			console.log(message);
		}
		googleSectionPaint();
	}
	/*
	 * paint not found result
	 */
	var paintNotfound = function(section) {
		console.log("paintNotfound");
		if (paintControl()) {
			console.log("term not found: " + _CURRENT_TERM);
			hiddenSelectors();
			if (section == 1) {
				$("#section1_title").text("No matches for " + _CURRENT_TERM + " in " + _taxonomy);
			} else if (section == 2) {
				if (_taxonomy == undefined) {
					// put the data in the section 1 because is one query
					$("#section1_title").text("No matches for " + _CURRENT_TERM + " in " + _GENERAL_SITE_NAME);
				} else {
					$("#section2_title").text("No matches for " + _CURRENT_TERM + " in " + "OTHER AREAS");
				}
			}
			googleSectionPaint();
		}

	}
	/*
	 * load more nodes in taxonomy section
	 */
	var moreNodesSection1 = function() {
		if (!_LOCK) {
			_LOCK = true;
		} else {
			return;
		}
		console.log("moreNodesSection1");
		_START = _END;
		_END = _END + _PAGINATION_SECTION_1;
		var query = _QUERY + "&start=" + _START + "&size=" + _PAGINATION_SECTION_1;
		$.ajax({
			url : _URL,
			dataType : "jsonp",
			data : query,
			success : function(data) {
				if (data == undefined) {
					console.log("ajax call return data:" + data);
					paintError("the server not return data");
					return;
				}
				//parse data
				var d = [];
				try {
					var hitsObject = data.hits;
					var hits = hitsObject.hit;
					var found = hitsObject.found;
					var objectResult;
					for ( k = 0; k < hits.length; k++) {
						objectResult = hits[k];
						d.push({
							nodeId : objectResult.id,
							nodeName : objectResult.data.nodename[0],
							nodeDesc : objectResult.data.nodedesc[0],
							path : objectResult.data.path[0],
							category : objectResult.data.taxonomyname[0]
						});
					}
				} catch(err) {
					console.log("Error parsing result" + err);
					paintError("Error: the data structure not is correct");
					return;
				}

				if (d.length > 0) {
					_CONTROL_SIZE += d.length;
					paint(d, 1);
					//if is all data
					if (found == _CONTROL_SIZE) {
						_START = 0;
						_END = _PAGINATION_SECTION_2;
						_CONTROL_SIZE = 0;
						if(_USE_QUERY2){
							start(_QUERY2, 2);
						}else{
							_INFINITE_SCROLL = 0;
							paintNotfound(3);
						}
					}
				} else {
					_START = 0;
					_END = _PAGINATION_SECTION_2;
					_CONTROL_SIZE = 0;
					if(_USE_QUERY2){
						start(_QUERY2, 2);
					}else{
						_INFINITE_SCROLL = 0;
						paintNotfound(3);
					}
				}
				$("#infinite_scroll_loading").hide();
			}
		});

	}
	/*
	 * load more nodes in another area section
	 */
	var moreNodesSections2 = function() {
		if (!_LOCK) {
			_LOCK = true;
		} else {
			return;
		}
		console.log("moreNodesSection2");
		_START = _END;
		_END = _END + _PAGINATION_SECTION_2;
		var query = _QUERY2 + "&start=" + _START + "&size=" + _PAGINATION_SECTION_2;
		$.ajax({
			url : _URL,
			dataType : "jsonp",
			data : query,
			success : function(data) {
				if (data == undefined) {
					console.log("ajax call return data:" + data);
					paintError("the server not return data");
					return;
				}
				//parse data
				var d = [];
				try {
					var hitsObject = data.hits;
					var hits = hitsObject.hit;
					var found = hitsObject.found;
					var objectResult;
					for ( k = 0; k < hits.length; k++) {
						objectResult = hits[k];
						d.push({
							nodeId : objectResult.id,
							nodeName : objectResult.data.nodename[0],
							nodeDesc : objectResult.data.nodedesc[0],
							path : objectResult.data.path[0],
							category : objectResult.data.taxonomyname[0]
						});
					}
				} catch(err) {
					console.log("Error parsing result" + err);
					paintError("Error: the data structure not is correct");
					return;
				}
				if (d.length > 0) {
					_CONTROL_SIZE += d.length;
					paint(d, 2);
					//if is all data
					if (found == _CONTROL_SIZE) {
						_CONTROL_SIZE = 0;
						_INFINITE_SCROLL = 0;
					}
				} else {
					_INFINITE_SCROLL = 0;
				}
				$("#infinite_scroll_loading").hide();
			}
		});
	}
	/*
	 * get data from cloudsearch
	 */
	var start = function(query, section, executeQuery2) {
		var s = section;
		if(typeof(executeQuery2)==='undefined') executeQuery2 = true;
		$.ajax({
			url : _URL,
			dataType : "jsonp",
			data : query,
			success : function(data) {
				if (data == undefined) {
					console.log("ajax call return data:" + data);
					paintError("the server not return data");
					return;
				}
				//parse data
				var d = [];
				try {
					var hitsObject = data.hits;
					var hits = hitsObject.hit;
					var found = hitsObject.found;
					var objectResult;
					for ( k = 0; k < hits.length; k++) {
						objectResult = hits[k];
						d.push({
							nodeId : objectResult.id,
							nodeName : objectResult.data.nodename[0],
							nodeDesc : objectResult.data.nodedesc[0],
							path : objectResult.data.path[0],
							category : objectResult.data.taxonomyname[0]
						});
					}
				} catch(err) {
					console.log("Error parsing result" + err);
					paintError("Error: the data structure not is correct");
					return;
				}

				//manage the sections in the web page
				if (s == 1) {
					if (d.length > 0) {
						_CONTROL_SIZE += d.length;
						paint(d, 1);
						//if is all data
						if (found == _CONTROL_SIZE) {
							//finished all data for section 1
							//call data section 2
							_CONTROL_SIZE = 0;
							if(executeQuery2){
								start(_QUERY2, 2);
							}
						} else {
							_INFINITE_SCROLL = 1;
						}
					} else {
						//search in another taxonomies recursively
						console.log("Not found results in this taxonomy");
						paintNotfound(1);
						if(executeQuery2){
							start(_QUERY2, 2);
						}
					}
				} else if (s == 2) {
					if (d.length > 0) {
						_CONTROL_SIZE += d.length;
						paint(d, 2);
						//if is all data
						if (found == _CONTROL_SIZE) {
							//finished infinite scroll
							_INFINITE_SCROLL = 0;
						} else {
							_INFINITE_SCROLL = 2;
						}
					} else {
						console.log("Not found result in anothers taxonomies");
						_INFINITE_SCROLL = 0;
						paintNotfound(2);
					}
				}
			},
			error : function() {
				console.log("Error getting data from cloudsearch in ajax call");
				paintError("Error: the resource is unavailable");
			}
		});
	};

	//public methods
	return {
		setTaxonomy : function(taxonomy) {
			_taxonomy = taxonomy;
		},

		isExecuteQuery2 : function(param){
			_USE_QUERY2 = param;
		},

		/*
		 * consult the data from the text field
		 */
		search : function() {
			console.log("search: searching data");

			var text = $("#searchtxt").val();
			if (_LOCK) {
				_CANCEL_PAINT = true;
			}

			// verify is the user is typing
			clearTimeout(_delayTimer);
			_delayTimer = setTimeout(function() {
				//search if >= 3 characters
				if (text.length < 3) {
					clearTimeout(_delayTimer);
					CONFIG_SEARCH.clear();
					return;
				}
				CONFIG_SEARCH.configureInfinite();
				//init the template if is undefined
				createTemplate();
				//initialize variables
				_LOCK = false;
				_FINISHED = false;
				_START = 0;
				_END = _PAGINATION_SECTION_1;
				_INFINITE_SCROLL = 0;
				_CURRENT_TERM = text;
				_CONTROL_SIZE = 0;

				_QUERY = "bq=(and nodename:'" + text + "|" + text + "*' taxonomyname:'" + _taxonomy + "')" + "&size=" + _PAGINATION_SECTION_1 + "&return-fields=nodename,nodedesc,path,taxonomyname&rank=taxonomyname";
				_QUERY2 = "bq=(and nodename:'"+ text + "|" + text + "*' (not taxonomyname:'" + _taxonomy + "'))" + "&size=" + _PAGINATION_SECTION_2 + "&return-fields=nodename,nodedesc,path,taxonomyname&rank=taxonomyname";

				//validate _taxonomy
				if (_taxonomy == undefined) {
					console.log("Taxonomy not found, search in all taxonomies");
					start(_QUERY2, 2);
				} else {
					start(_QUERY, 1, _USE_QUERY2);
				}

				clearTimeout(_delayTimer);

			}, _DELAY_SEARCH);

		},

		/*
		 * infinite scroll controller
		 */
		callInfinite : function() {
			if (_INFINITE_SCROLL == 1) {
				$("#infinite_scroll_loading").show();
				moreNodesSection1();
			} else if (_INFINITE_SCROLL == 2) {
				$("#infinite_scroll_loading").show();
				moreNodesSections2();
			}
		},

		/*
		 * hidden the selectors to show the result data
		 */
		setSelectorsToHidden : function(selectors) {
			_selectorsToHidden = selectors;
		},

		/**
		 * select the parent to put the data with the result
		 */
		setSelectorParent : function(selector) {
			_selectorParent = selector;
		},

		/**
		 * execute this in the init plugin
		 * configure my infinite scroll and disabled anothers events
		 */
		configureInfinite : function() {
			//configute my event
			$(window).scroll(function() {
				if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
					if (!_FINISHED) {
						CONFIG_SEARCH.callInfinite();
					}
				}
			});
		},

		/*
		 * clean the search app
		 */
		clear : function() {
			$.each(_selectorsToHidden, function(index, value) {
				try {
					value.show();
				} catch(ex) {
					console.log("error hidden data");
				}
			});
			var contentSearch = $("#content_search");
			contentSearch.children().remove();
			//unlock my infinite scroll
			_LOCK = false;
			_FINISHED = true;
		}
	};
}();
