function FacebookInit(clientId)
{
                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : clientId, // App ID
                        channelUrl : 'https://s3.amazonaws.com/resourcesweb/newdev/helpers/channel.html', // Channel File
                        status     : true, // check login status
                        cookie     : true, // enable cookies to allow the server to access the session
                        xfbml      : true  // parse XFBML
                    });
                    
                    FB.getLoginStatus(function(response) {
                        if (response.status === 'connected') {
                            //alert ("connected")
                        } else if (response.status === 'not_authorized') {
                            //alert("not_authorized");
                           // login();
                        } else {
                           // alert("not_logged_in");
                           // login();
                        }
                    });

                    // Additional init code here

                };
                
    

                // Load the SDK Asynchronously
                (function(d){
                    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement('script'); js.id = id; js.async = true;
                    js.src = "//connect.facebook.net/en_US/all.js";
                    ref.parentNode.insertBefore(js, ref);
                }(document));
                
}
            function login(next) {
				
				if (next==null)
				{
				next=document.URL;
				//alert (next);
				}
                    FB.login(function(response) {
                        if (response.authResponse) {
                            ///alert ("connected");
                            //alert (response.authResponse.accessToken);
                            //alert (response.authResponse.userID);
                             window.location=CONTEXT_LINKAPEDIA+"/FacebookLoginClient?tokenFb="+response.authResponse.accessToken+"&next="+escape(next);
                        } else {
                            //alert ("cancelled");
                        }
                    }, {scope: 'email'});
                }
