/*
 *Andres restrepo
 *
 * logic to use the search call CloudSearch ajax
 * Note: we need a order in the category
 */
 var URL = 'http://ec2-23-21-89-211.compute-1.amazonaws.com/CloudSearch.php';
 var ban = true;
 var taxonomyName;
 function configureSearch(currentTax){
    taxonomyName=currentTax;
    var siteName="linkapedia";
    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        //logic to build the head taxonomy name
        _renderMenu: function( ul, items ) {
            var self = this,
            currentCategory = "";
            var style = "style='font-size:18px; text-decoration:underline; color: gray;float: right; margin-top: 19px;'";
            //add link "more" in the first search
            if(ban){
                ul.append("<a href='javascript:void(0);' onclick=\"loadNewData();\" "+style+">☉</a>");
            }else{
                //add link "remove" in the second search
                ul.append("<a href='javascript:void(0);' onclick=\"loadData();\" "+style+">⇇</a>");
            }
            var myBan = true;
            $.each( items, function( index, item ) {
                //use myBan to fix the style with button in the first result category
                if ( item.category != currentCategory ) {
                    if(myBan){
                        ul.append( "<center><li style=\"width:216px;\" class='ui-autocomplete-category'>" + item.category + "</li></center>" );
                        myBan =false;
                    }else{
                        ul.append( "<center><li class='ui-autocomplete-category'>" + item.category + "</li></center>" );
                    }
                    currentCategory = item.category;
                }
                self._renderItem( ul, item );
            });
        },
        _renderItem : function( ul, item ) {
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<img width= 38px height= 38px class='iconSearch' src=\"https://s3.amazonaws.com/nodeimages/"+item.nodeId+"/thumbnails/"+item.nodeId+"_0.jpg\" onerror=\"this.src='https://s3.amazonaws.com/resourcesweb/dev/site-resources/images/thumbnail/notImage.jpg';\" class='ui-state-default'>")
            .append( "<a class=\"itemSearch\"'><div class=\"search-nodeTitle\"> <b><i>" + item.nodeName + "</i></b></div>" + "<div class=\"search-url\">"+item.path + ".</div><div class=\"search-summary\"> ... </div></a>" )
            .appendTo( ul );
        }

    });
    loadData();
}

function loadData(){
    ban = true;
    $( "#searchtxt" ).catcomplete({
        delay: 500,
        minLength: 3,
        source: function( request, response ) {
            var text = $("#searchtxt").val();
            text = replaceAll(text," ","+");
            $.ajax({
                url: URL,
                dataType: "jsonp",
                data: "bq=(and nodename:'"+text+"*' taxonomyname:'"+taxonomyName+"')&return-fields=nodename,path,taxonomyname&rank=taxonomyname",
                success: function( data ) {
                        //parse and return data
                        var d =[];
                        if (data!=undefined){
                            try{
                            var hitsObject = data.hits;
                                var hits = hitsObject.hit;
                                var objectResult;
                                
                                for(k=0;k<hits.length;k++){
                                    objectResult = hits[k];

                                    d.push({
                                        nodeId:objectResult.id,
                                        nodeName:objectResult.data.nodename[0],
                                        path:objectResult.data.path[0],
                                        category:objectResult.data.taxonomyname[0]
                                    });
                                }
                            }catch(err){
                                console.log("Error parsing result");
                            }
                        }
                        response(d);
                    }
                });
        },
        focus: function( event, ui ) {
            $( "#searchtxt" ).val( ui.item.nodeName );
            return false;
        },
        select: function( event, ui ) {
            $( "#searchtxt" ).val( ui.item.nodeName );
                //alert("One moment. I need to create the code in this part")
                window.location.href="?nodeId="+ui.item.nodeId;
                return false;
        }
    });
    $( "#searchtxt" ).catcomplete("search");
}

//load a the data to search in all taxonomies
function loadNewData(){
    ban = false;
    $( "#searchtxt" ).catcomplete({
        source: function( request, response ) {
            var text = $("#searchtxt").val();
            text = replaceAll(text," ","+");
            $.ajax({
                url: URL,
                dataType: "jsonp",
                data: "bq=nodename:'"+text+"*'&return-fields=nodename,path,taxonomyname&rank=taxonomyname",
                success: function( data ) {
                    //parse and return data
                    var d =[];
                    if (data!=undefined){
                        try{
                            var hitsObject = data.hits;
                            var hits = hitsObject.hit;
                            var objectResult;
                            for(k=0;k<hits.length;k++){
                                objectResult = hits[k];
                                    d.push({
                                    nodeId:objectResult.id,
                                    nodeName:objectResult.data.nodename[0],
                                    path:objectResult.data.path[0],
                                    category:objectResult.data.taxonomyname[0]
                                });
                            }
                        }catch(err){
                            console.log("Error parsing data");
                        }
                    }
                    response(d);
                }
            });
        },
        focus: function( event, ui ) {
            $( "#searchtxt" ).val( ui.item.nodeName );
            return false;
        },
        select: function( event, ui ) {
            $( "#searchtxt" ).val( ui.item.nodeName );
                //alert("One moment. I need to create the code in this part")
                window.location.href="?nodeId="+ui.item.nodeId;
                return false;
        }
    });
    $( "#searchtxt" ).catcomplete("search");
}   

//utilities
function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
