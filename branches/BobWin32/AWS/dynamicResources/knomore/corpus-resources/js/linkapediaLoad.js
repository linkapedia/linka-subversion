/*
 *Andres restrepo
 *
 *This file contain all configuration to plugins jquery in the pages
 */

 //load ramdom images for the links nodes
function imageLinkrandom(nodeId){
    var randomNumber;
    var newSrcTemp;
    $('.floatLeft,.floatRight,.floatNone').each(function() {
        if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
            // image was broken, replace with your new image
            console.log('Image 1 not found');
            randomNumber = randomToN(4); 
            newSrcTemp = 'https://s3.amazonaws.com/nodeimages/' + nodeId + '/images/' + nodeId + '_' + randomNumber + '.jpg';
            $(this).attr("src",newSrcTemp).error(function(){
                console.log('Image 2 not found');
                $(this).attr("src","https://s3.amazonaws.com/resourcesweb/dev/site-resources/images/notImage.jpg");
                console.log('Loading default image');
            });     
        }
    });
}

function randomToN(maxVal){
    var randVal = Math.random()*maxVal;
    return typeof floatVal=='undefined'?Math.round(randVal):randVal.toFixed(floatVal);
}

function load(){
    //init tree menu carrousel
    // initialize scrollable
    $(".scrollable").scrollable();
    
    //load jmasonry...
    var container = $('#container');
    container.imagesLoaded( function(){
        container.masonry({
            itemSelector : '.porlet,.box'
        });
    });
    $( "#dialog" ).dialog({
        autoOpen: false,
        show: "blind",
        hide: "explode",
        resizable: false,
        modal: true
    });

    $( "#dialogDebugSentences" ).dialog({
        autoOpen: false,
        show: "blind",
        hide: "explode",
        resizable: false,
        draggable: false,
        maxWidth:500,
        maxHeight:500,
        height: 500,
        width:500,
        modal: true
    });

    $( "#dialogInfoSentences" ).dialog({
        autoOpen: false,
        show: "blind",
        hide: "explode",
        resizable: false,
        draggable: false,
        maxWidth:600,
        maxHeight:600,
        height: 600,
        width:600,
        modal: true
    });

    $( "#loadingAjax" ).dialog({
        autoOpen: false,
        closeOnEscape: false,
        open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        close: function(event, ui){ $(".ui-dialog-titlebar-close").show(); },
        show: "blind",
        hide: "explode",
        resizable: false,
        draggable: false,
        maxWidth:170,
        maxHeight:150,
        height: 150,
        width:170,
        modal: true
    });

    $(".cInterest").click(function() {
        $( "#dialog" ).dialog( "open" );
        return false;
    });
    
    var $container = $('#container');
    $container.infinitescroll({
        navSelector  : '#page-nav',    // selector for the paged navigation 
        nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
        itemSelector : '.box',     // selector for all items you'll retrieve
        localMode:true,
        animate:true,
        loading: {
            finishedMsg: 'No more pages to load.',
            img: 'http://i.imgur.com/6RMhx.gif'
        }
    },
    // trigger Masonry as a callback
    function( newElements ) {
        // hide new items while they are loading
        var $newElems = $( newElements ).css({
            opacity: 0
        });
        // ensure that images load before adding to masonry layout
        $newElems.imagesLoaded(function(){
            // show elems now they're read
            $newElems.animate({
                opacity: 1
            });
            $container.masonry( 'appended', $newElems, true );
        });
    });

    $('#container').css('visibility','visible');

}