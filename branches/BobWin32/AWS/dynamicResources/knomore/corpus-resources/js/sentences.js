/*
 *Andres restrepo
 *
 *This file contain all logic to get sentenses
 */

 var call = "SentenceExtracted";
 function getDebugVersion(corpusId,nodeId,fileName){
    //call ajax to get result
    $.ajax({
        url: call,
        dataType: "jsonp",
        data: "corpusId="+corpusId+"&nodeId="+nodeId+"&fileName="+fileName+"&version=1",
        success: function( data ) {
            var status = data.status;
            var message = data.message;
            var sentences = data.sentences;
            var string = "";
            if(status == 0){
                string = "<div><b>"+message+"</b></div>";
            }else{
            //parse and return data
                string = "<div style='padding:10px;'>";
                for (i = 0; i < sentences.length; i++){
                    var sentence=sentences[i].sentence;
                    var scoreNodeTitle = sentences[i].scoreNodeTitle;
                    var scoreMH = sentences[i].scoreMH;
                    var scoreSig = sentences[i].scoreSig;
                    var signatures = sentences[i].signatures;
                    var musthaves = sentences[i].musthaves;
                    //build html <div> and append
                    string+="<br>";
                    string+="<p>"+sentence+"<br>";
                    string+="sscore1(Node title):&nbsp;"+scoreNodeTitle+"<br>";
                    string+="score2(Musthaves):&nbsp;"+scoreMH+"<br>";
                    string+="score3(signatures):&nbsp;"+scoreSig+"<br>";
                    var total = scoreNodeTitle + scoreMH + scoreSig;
                    string+="Total:&nbsp;"+total+"<br>";
                    string+=" Musthaves:&nbsp;"+musthaves+"<br>";
                    string+=" Signatures:&nbsp;"+signatures+"<br>";
                    string+="</p><br>"; 
                }
                string+="</div>";
            }


            $('#dialogDebugSentences').children("div").remove();
            $('#dialogDebugSentences').append(string);
            $('#dialogDebugSentences').dialog( "open" );

        },beforeSend: function(){
            $("#loadingAjax").dialog( "open" );
        },complete: function(){
            $("#loadingAjax").dialog( "close" );       
        },error: function(){
            console.log("Error getting result debug version");
            $("#loadingAjax").dialog( "close" );  
        }
    });
}

function getInfoVersion(corpusId,nodeId,fileName,title,link,imageNumber){
      //call ajax to get result
      $.ajax({
        url: call,
        dataType: "jsonp",
        data: "corpusId="+corpusId+"&nodeId="+nodeId+"&fileName="+fileName+"&version=2",
        success: function( data ) {
            //parse and return data
            var string = "<div style='padding:10px;'>";
            //object to cloud tag
            var settings =  {"size":{
                "grid" : 16
            },
            "options" : {
                "color" : "random-dark",
                "printMultiplier" : 3
            },
            "font" : "Futura, Helvetica, sans-serif",
            "shape" : "square"};

            var docDigest=data.docDigest;
            var nodes = data.nodes;
            //build html <div> and append
            string+="<br>";
            var idImage ="porletImage_"+imageNumber;
            string+="<center><img src="+$("#"+idImage).attr("src")+" WIDTH=190 HEIGHT=110 /></center>";
            string+="<p><h3>Title</h3></p><br>";
            string+="<p>"+title+"</p><br><br>";
            string+="<p><h3>Link</h3></p><br>";
            string+="<p>"+link+"</p><br><br>";
            string+="<p><h3>Digest</h3></p><br>";
            string+="<p>"+docDigest+"</p><br><br>";
            string+="<p><h3>Related Nodes</h3><br>";
            string+="<center><div id=myCloudTag style=\"height:300px;width:300px;\">";
            if (nodes!=undefined){
                //build a cloud tag divs
                for (i = 0; i < nodes.length; i++){
                    string+="<span data-weight='"+nodes[i].weight+"'>"+nodes[i].title+"</span>";
                }
            }
            //div of the cloud tag
            string+="</div></center>";
            //div general
            string+="</div>";

            $('#dialogInfoSentences').children("div").remove();
            $('#dialogInfoSentences').append(string);
            $('#dialogInfoSentences').dialog( "open" );

            //init cloudTag
            $( "#myCloudTag" ).awesomeCloud( settings );

        },beforeSend: function(){
            $("#loadingAjax").dialog( "open" );
        },complete: function(){
            $("#loadingAjax").dialog( "close" );       
        }
    });
}  