package com.intellisophic.linkapedia.migration.exec;

import com.intellisophic.linkapedia.amazon.services.dynamodb.exceptions.DynamoDBAnnotationException;
import com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.DynamoDBHelper;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.datasources.mysql.MySQLParamHandler;
import com.intellisophic.linkapedia.migration.services.MigrationServices;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class MySQLMigrationApp {

    private static final Logger log = LoggerFactory.getLogger(MySQLMigrationApp.class);

    public static void main(String[] args) {
        boolean waitUserPassword = false;
        String username = null;
        String password = null;
        String hostname = null;
        String dbname = null;
        String strCorpusID = null;
        Long corpusID = -1L;
        boolean createTable = false;
        boolean dropTable = false;
        if (args != null && args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                String param = args[i];
                if (param.startsWith("-u") && (i + 1) <= args.length && !args[i + 1].startsWith("-")) {
                    username = args[++i];
                    System.out.println("Username: " + username);
                } else if (param.startsWith("-u") && (i + 1) <= args.length) {
                    System.out.println("No username especified.");
                    System.exit(0);
                }
                if (param.startsWith("-p") && param.length() > 2) {
                    password = param.substring(2, param.length());
                    System.out.println("Password: " + password);
                } else if (param.startsWith("-p")) {
                    System.out.println("Waiting for password...");
                    waitUserPassword = true;
                }
                if (param.startsWith("-h") && (i + 1) < args.length && !args[i + 1].startsWith("-")) {
                    hostname = args[++i];
                    System.out.println("Hostname: " + hostname);
                } else if (param.startsWith("-h") && (i + 1) <= args.length) {
                    System.out.println("No hostname especified.");
                    System.exit(0);
                }
                if (param.startsWith("-d") && (i + 1) < args.length && !args[i + 1].startsWith("-")) {
                    dbname = args[++i];
                    System.out.println("Database: " + dbname);
                } else if (param.startsWith("-d") && (i + 1) <= args.length) {
                    System.out.println("No database especified.");
                    System.exit(0);
                }
                if (param.startsWith("-c") && (i + 1) < args.length && !args[i + 1].startsWith("-")) {
                    strCorpusID = args[++i];
                    System.out.println("Corpus: " + strCorpusID);
                    if (strCorpusID != null && !strCorpusID.isEmpty()) {
                        try {
                            corpusID = Long.parseLong(strCorpusID);
                        } catch (NumberFormatException e) {
                            System.out.println("No valid format for corpus.");
                            System.exit(0);
                        }
                    }
                } else if (param.startsWith("-c") && (i + 1) <= args.length) {
                    System.out.println("No corpus especified.");
                    System.exit(0);
                }

                //Create tables
                if (param.equals("--create-tables")) {
                    createTable = true;
                    log.debug("Going to create the tables required");
                }
                if (param.equals("--drop-tables")) {
                    dropTable = true;
                    if (createTable && dropTable) {
                        log.debug("Going to create the tables required after the drop table command finishes.");
                    } else {
                        log.warn("Going to drop the tables");
                    }
                }
            }
            if (waitUserPassword) {
                password = waitForUserPassword();
                System.out.println("Password received from input: " + password);
                if (password == null || password.isEmpty()) {
                    log.error("No password especified.");
                    System.out.println("No password especified.");
                    System.exit(0);
                }
            }
            boolean nodeDocument = false;
            boolean documentNode = false;
            try {
                nodeDocument = DynamoDBHelper.tableExistForClass(NodeDocument.class);
                documentNode = DynamoDBHelper.tableExistForClass(DocumentNode.class);
            } catch (DynamoDBAnnotationException dae) {
                log.error("DynamoDBAnnotationException -- main", dae);
                return;
            }

            if (dropTable) {
                System.out.println("Dropping Tables...");
                if (nodeDocument) {
                    DynamoDBHelper.deleteTable(NodeDocument.class);
                } else {
                    log.warn("The Table NodeDocument does not exists.");
                    System.out.println("The Table NodeDocument does not exists.");
                }
                if (documentNode) {
                    DynamoDBHelper.deleteTable(DocumentNode.class);
                } else {
                    log.warn("The Table DocumentNode does not exists.");
                    System.out.println("The Table DocumentNode does not exists.");
                }
            }
            if (createTable) {
                if (dropTable) {
                    System.out.println("Re-creating Tables...");
                } else {
                    System.out.println("Creating Tables...");
                }
                if (!nodeDocument) {
                    DynamoDBHelper.createTableFromClass(NodeDocument.class);
                } else {
                    log.warn("The Table NodeDocument already exists.");
                    System.out.println("The Table NodeDocument already exists.");
                }
                if (!documentNode) {
                    DynamoDBHelper.createTableFromClass(DocumentNode.class);
                } else {
                    log.warn("The Table DocumentNode already exists.");
                    System.out.println("The Table DocumentNode already exists.");
                }
            }

            if (dropTable && !createTable) {
                log.error("No destination tables found on DynamoDB");
                System.exit(0);
            }

            if (hostname != null && !hostname.isEmpty() && dbname != null && !dbname.isEmpty() && username != null && !username.isEmpty() && password != null && !password.isEmpty() && corpusID != null && !corpusID.equals(-1L)) {
                try {
                    MySQLParamHandler.addProperty("hostname", hostname);
                    MySQLParamHandler.addProperty("dbname", dbname);
                    MySQLParamHandler.addProperty("username", username);
                    MySQLParamHandler.addProperty("password", password);
                    MigrationServices services = new MigrationServices();
                    services.migrateFromMySQLToDynamoDB(corpusID);
                    System.out.println("Done...");
                } catch (Exception e) {
                    log.error("Exception: ", e);
                }
            } else {
                System.out.println("Missing parameters.");
                System.exit(0);
            }
        } else {
            System.out.println("No params especified.");
            System.out.println("Usage: mysql-tool -u <db-username> -p[db-password] -h <hostname> -d <db-name> -c <corpus-id> | --create-tables|[--drop-tables]");
        }
    }

    private static String waitForUserPassword() {
        //Create a scanner object  
        //Scanner is a predefined class in java that will be use to scan text  
        //System.in is mean, we will receive input from standard input stream  
        Scanner readUserInput = new Scanner(System.in);
        System.out.println("Password: ");

        //WAIT FOR USER INPUT  
        String userPassword = readUserInput.nextLine();

        return userPassword;

    }
}
