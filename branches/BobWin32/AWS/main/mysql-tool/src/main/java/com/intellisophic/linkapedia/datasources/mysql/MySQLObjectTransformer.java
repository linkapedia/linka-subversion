package com.intellisophic.linkapedia.datasources.mysql;

import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.generic.utils.DigestUtils;
import com.intellisophic.linkapedia.generic.utils.NumericUtils;
import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class MySQLObjectTransformer {

    private static final Logger log = LoggerFactory.getLogger(MySQLObjectTransformer.class);
    public static final String NODEDOCUMENT = "NODEDOCUMENTTABLE";
    public static final String DOCUMENTNODE = "DOCUMENTNODETABLE";

    public static Object transform(CorpusInfo corpusInfo, String objectToTransform) {
        log.debug("transform(CorpusInfo, String)");
        if (objectToTransform.equals(NODEDOCUMENT)) {
            return getNodeDocument(corpusInfo);
        } else if (objectToTransform.equals(DOCUMENTNODE)) {
            return getDocumentNode(corpusInfo);
        }
        return null;
    }

    private static NodeDocument getNodeDocument(CorpusInfo corpusInfo) {
        log.debug("getNodeDocument(CorpusInfo)");
        NodeDocument node = null;
        if (corpusInfo == null) {
            return node;
        }
        node = new NodeDocument();
        node.setNodeID(NumericUtils.revertLongAsString(corpusInfo.getNodeID()));
        node.setDocID(DigestUtils.getMD5(corpusInfo.getURI()));
        node.setDocURL(corpusInfo.getURI());
        node.setPagerank(corpusInfo.getPagerank());
        node.setScore01(corpusInfo.getScore01());
        node.setScore02(corpusInfo.getScore02());
        node.setScore03(corpusInfo.getScore03());
        node.setScore04(corpusInfo.getScore04());
        node.setScore05(corpusInfo.getScore05());
        node.setScore06(corpusInfo.getScore06());
        node.setScore07(corpusInfo.getScore07());
        node.setScore08(corpusInfo.getScore08());
        node.setScore09(corpusInfo.getScore09());
        node.setScore10(corpusInfo.getScore10());
        node.setScore11(corpusInfo.getScore11());
        node.setScore12(corpusInfo.getScore12());
        node.setDocTitle(corpusInfo.getDocTitle());
        node.setUserScore(new Float(0.0f));
        return node;
    }

    private static DocumentNode getDocumentNode(CorpusInfo corpusInfo) {
        log.debug("getDocumentNode(CorpusInfo)");
        DocumentNode document = null;
        if (corpusInfo == null) {
            return document;
        }
        document = new DocumentNode();
        document.setDocID(DigestUtils.getMD5(corpusInfo.getURI()));
        document.setNodeID(NumericUtils.revertLongAsString(corpusInfo.getNodeID()));
        document.setTitle(corpusInfo.getDocTitle());
        document.setURL(corpusInfo.getURI());
        document.setScore01(corpusInfo.getScore01());
        document.setScore02(corpusInfo.getScore02());
        document.setScore03(corpusInfo.getScore03());
        document.setScore04(corpusInfo.getScore04());
        document.setScore05(corpusInfo.getScore05());
        document.setScore06(corpusInfo.getScore06());
        document.setScore07(corpusInfo.getScore07());
        document.setScore08(corpusInfo.getScore08());
        document.setScore09(corpusInfo.getScore09());
        document.setScore10(corpusInfo.getScore10());
        document.setScore11(corpusInfo.getScore11());
        document.setScore12(corpusInfo.getScore12());
        document.setPagerank(corpusInfo.getPagerank());
        document.setLastFound(corpusInfo.getTimestamp());
        return document;
    }
}
