/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.datasources.mysql;

import java.util.Properties;

/**
 *
 * @author Xander Kno
 */
public class MySQLParamHandler {

    private static final Properties properties = new Properties();

    public static boolean isValidDBConfig() {
        if (properties.containsKey("hostname") && properties.containsKey("dbname") && properties.containsKey("username") && properties.containsKey("password")) {
            return true;
        }
        return false;
    }

    public static void addProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }
}
