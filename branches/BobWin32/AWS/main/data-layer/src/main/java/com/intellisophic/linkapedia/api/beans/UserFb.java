package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;


/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "UserFb", hashKey = "fbId", hashKeyType = "S")
@DynamoDBTable(tableName = "UserFb")
public class UserFb implements Serializable {

    /**
     * unique/key/fbId
     */
    private String fbId;
    private String userId;

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @DynamoDBHashKey(attributeName = "fbId")
    public String getFbId() {
        return fbId;
    }

    @DynamoDBAttribute(attributeName = "userId")
    public String getUserId() {
        return userId;
    }
}
