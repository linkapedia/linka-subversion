package com.intellisophic.linkapedia.api.beans;

import java.io.Serializable;

/**
 * use this to save data im memcache
 *
 * @author andres
 */
public class DocNodeWrapper implements Serializable{

    private String docId;
    private byte[] data;

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
