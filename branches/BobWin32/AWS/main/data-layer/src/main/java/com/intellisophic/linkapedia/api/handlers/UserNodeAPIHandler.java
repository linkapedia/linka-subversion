package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.InterestCollectionFollowed;
import com.intellisophic.linkapedia.api.beans.UserNode;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class UserNodeAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(UserNodeAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(UserNodeAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<UserNode> getUserNodesByUserId(String userId) throws Exception {
        log.info("getUserNodesByUserId - userId: " + userId);
        List<UserNode> returnList = new ArrayList<UserNode>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(userId);
            if (cacheObj != null) {
                return (List<UserNode>) cacheObj;
            } else {
                PaginatedQueryList<UserNode> paginatedQueryList = getDynamoDBMapper().query(
                        UserNode.class, new DynamoDBQueryExpression(new AttributeValue().withS(userId)));

                for (UserNode n : paginatedQueryList) {
                    if (n != null) {
                        returnList.add(n);
                    }
                }
                cache.set(userId, returnList, ONE_HOUR);

            }
        } catch (Exception ex) {
            log.error("Exception in getUserNodesByUserId - " + userId + " - " + ex.getMessage());
            throw ex;
        }

        return returnList;
    }

    public static UserNode getUserNodesByUserIdAndNodeId(String userId, String nodeId) throws Exception {
        UserNode userNode = null;
        Object cacheObj = null;
        try {
            cacheObj = cache.get("userNode" + userId + nodeId);
            if (cacheObj != null) {
                return (UserNode) cacheObj;
            } else {
                userNode = getDynamoDBMapper().load(UserNode.class, userId, nodeId);
                if (userNode != null) {
                    cache.set("userNode" + userId + nodeId, userNode, ONE_HOUR);
                } else {
                    return null;
                }
            }
        } catch (Exception ex) {
            log.error("Exception in getUserNodesByUserIdAndNodeId: " + ex.getMessage());
            throw ex;
        }
        return userNode;
    }

    public static int getUserNodesCount(String userId) throws Exception {
        log.info("getUserNodesCount - userId: " + userId);
        List<UserNode> userNodeList = new ArrayList<UserNode>();
        Object cacheObj = null;
        int count = 0;
        try {
            cacheObj = cache.get(userId);
            if (cacheObj != null) {
                userNodeList = (List<UserNode>) cacheObj;

            } else {
                userNodeList = getUserNodesByUserId(userId);

            }
            count = userNodeList.size();
        } catch (Exception ex) {
            log.error("Exception in getUserNodesCount - userId: " + userId + " - " + ex.getMessage());
            throw ex;

        }

        return count;
    }

    //this method use the events table
    public static void saveUserNode(UserNode userNode) throws Exception {
        log.info("saveUserNode - UserNode");
        try {
            getDynamoDBMapper().save(userNode);
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(userNode.getUserId());
            userEvents.registerEvent(userNode, UserFeedAPIHandler.JOIN);
            cache.delete(userNode.getUserId()); //delete from cache to ensure the data consistence
        } catch (Exception ex) {
            log.error("Exception in saveUserNode: " + ex.getMessage());
            throw ex;
        }
    }

    //this method use the events table
    public static void deleteUserNode(UserNode userNode) throws Exception {
        log.info("deleteUserNode - UserNode");
        try {
            getDynamoDBMapper().delete(userNode);
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(userNode.getUserId());
            userEvents.registerEvent(userNode, UserFeedAPIHandler.UNJOIN);
            cache.delete(userNode.getUserId());
        } catch (Exception ex) {
            log.error("Exception in deleteUserNode: " + ex.getMessage());
            throw ex;
        }
    }
}
