package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.FollowedUser;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class FollowedUserAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(FollowedUserAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(FollowedUserAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<FollowedUser> getFollowerUsersByFollowedUserId(String followedUserId) throws Exception {
        log.info("getFollowerUsersByFollowedUserId - followedUserId: " + followedUserId);
        List<FollowedUser> returnList = new ArrayList<FollowedUser>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(followedUserId);
            if (cacheObj != null) {
                return (List<FollowedUser>) cacheObj;
            } else {
                PaginatedQueryList<FollowedUser> result = getDynamoDBMapper().query(
                        FollowedUser.class, new DynamoDBQueryExpression(new AttributeValue(followedUserId)));

                for (FollowedUser followedUser : result) {
                    if (followedUser != null) {
                        returnList.add(followedUser);
                    }
                }
                cache.set(followedUserId, returnList, ONE_HOUR);
            }
        } catch (Exception ex) {
            log.error("Exception in getFollowerUsersByFollowedUserId - followedUserId: " + followedUserId + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;

    }

    public static int getFollowersCount(String followedUserId) throws Exception {

        log.info("getNodeUsersCount - followedUserId: " + followedUserId);

        List<FollowedUser> followedUsersList = new ArrayList<FollowedUser>();
        Object cacheObj = null;
        int count;

        try {
            cacheObj = cache.get(followedUserId);
            if (cacheObj != null) {
                followedUsersList = (List<FollowedUser>) cacheObj;

            } else {
                followedUsersList = getFollowerUsersByFollowedUserId(followedUserId);
            }
            count = followedUsersList.size();
        } catch (Exception ex) {
            log.error("Exception in getFollowersCount - followedUserId: " + followedUserId + " - " + ex.getMessage());
            throw ex;
        }

        return count;
    }

    //this method use the events table
    public static void saveFollowedUser(FollowedUser followedUser) throws Exception {
        log.info("saveFollowedUser - FollowedUser");
        try {
            getDynamoDBMapper().save(followedUser);
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(followedUser.getFollowedUserId());
            userEvents.registerEvent(followedUser, UserFeedAPIHandler.FOLLOWED);
            cache.delete(followedUser.getFollowedUserId()); //delete from cache to ensure the data consistence
        } catch (Exception ex) {
            log.error("Exception in saveFollowedUser: " + ex.getMessage());
            throw ex;
        }
    }

    //this method use the events table
    public static void deleteFollowedUser(FollowedUser followedUser) throws Exception {
        log.info("deleteNodeUser - FollowedUser");
        try {
            getDynamoDBMapper().delete(followedUser);
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(followedUser.getFollowedUserId());
            userEvents.registerEvent(followedUser, UserFeedAPIHandler.UNFOLLOWED);
            cache.delete(followedUser.getFollowedUserId());
        } catch (Exception ex) {
            log.error("Exception in deleteFollowedUser: " + ex.getMessage());
            throw ex;
        }

    }
}
