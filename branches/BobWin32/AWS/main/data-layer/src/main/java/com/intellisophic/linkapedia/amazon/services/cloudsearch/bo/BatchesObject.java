package com.intellisophic.linkapedia.amazon.services.cloudsearch.bo;

import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;

/**
 * save the documents in a array (Json Format)
 *
 * @author andres
 *
 */
public class BatchesObject {

    private static final Logger log = Logger.getLogger(BatchesObject.class);
    private JSONArray arr;
    private static final long MEGABYTE = 1024L * 1024L;
    private static double MAX_MEGABYTE_BATCHES = 4.9;   //5MB
    private static double MAX_MEGABYTE_DOCUMENT = 0.9; //1MB
    //manage status code
    public static final int STATUS_BIG_FILE = 1;
    public static final int STATUS_BIG_BATCHES = 2;
    public static final int STATUS_OK = 3;
    //with more big than 1MB
    private static List<String> nodesWithError;

    static {
        nodesWithError = new ArrayList<String>();
    }

    public BatchesObject() {
        arr = new JSONArray();
    }

    /**
     * control data for insert in cloud search
     *
     * @param data
     * @return a status code
     */
    public int addObject(CloudSearchObjectAWS data) {
        log.debug("BatchesObject: addObject(CloudSearchObjectAWS)");
        long sizeArr = arr.toString().getBytes().length / MEGABYTE;
        long sizeData = data.toSdf().toString().getBytes().length / MEGABYTE;
        
        log.info("Size batches: "+sizeArr+" MB.");
        log.info("Size current data: "+sizeData+" MB");

        //validate the size for the document
        if (sizeData > MAX_MEGABYTE_DOCUMENT) {
            log.info("The document is more big than 1MB: " + data.getNodeId());
            addErrorNode(data.getNodeId());
            return STATUS_BIG_FILE;
        }
        //validate the size for the Batches
        if (sizeArr + sizeData > MAX_MEGABYTE_BATCHES) {
            log.info("Batches is more big than 5MB");
            return STATUS_BIG_BATCHES;
        }
        arr.add(data.toSdf());
        return STATUS_OK;
    }

    /**
     * Save nodes that can't save in cloud search
     *
     * @param id
     */
    private synchronized static void addErrorNode(String id) {
        nodesWithError.add(id);
    }

    /**
     * Get nodes that can't save in cloud search
     *
     * @return
     */
    public static List<String> getErrorNodes() {
        return nodesWithError;
    }

    /**
     * reset the arr when is big > 5MB
     */
    public void reset() {
        arr.clear();
    }

    /**
     * return the complete document to send to Cloud Search
     *
     * @return
     */
    @Override
    public String toString() {
        return arr.toString();
    }
}
