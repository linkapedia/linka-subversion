package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author Xander Kno
 */
@AmazonDynamoDB(tableName = "NodeDocument", hashKey = "nodeID", hashKeyType = "S", rangeKey = "docID", rangeKeyType = "S")
@DynamoDBTable(tableName = "NodeDocument")
public class NodeDocument implements Serializable {

    private String nodeID;
    /**
     * MD5 Hash of the docURL.
     */
    private String docID;
    private String docTitle;
    private String docURL;
    private Float score01;
    private Float score02;
    private Float score03;
    private Float score04;
    private Float score05;
    private Float score06;
    private Float score07;
    private Float score08;
    private Float score09;
    private Float score10;
    private Float score11;
    private Float score12;
    private Float pagerank;
    private Float userScore;
    private boolean modified;
    private long timeStamp;

    public void setNodeID(String nodeID) {
        this.nodeID = nodeID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public void setDocTitle(String docTitle) {
        this.docTitle = docTitle;
    }

    public void setDocURL(String docURL) {
        this.docURL = docURL;
    }

    public void setScore01(Float score01) {
        this.score01 = score01;
    }

    public void setScore02(Float score02) {
        this.score02 = score02;
    }

    public void setScore03(Float score03) {
        this.score03 = score03;
    }

    public void setScore04(Float score04) {
        this.score04 = score04;
    }

    public void setScore05(Float score05) {
        this.score05 = score05;
    }

    public void setScore06(Float score06) {
        this.score06 = score06;
    }

    public void setScore07(Float score07) {
        this.score07 = score07;
    }

    public void setScore08(Float score08) {
        this.score08 = score08;
    }

    public void setScore09(Float score09) {
        this.score09 = score09;
    }

    public void setScore10(Float score10) {
        this.score10 = score10;
    }

    public void setScore11(Float score11) {
        this.score11 = score11;
    }

    public void setScore12(Float score12) {
        this.score12 = score12;
    }

    public void setPagerank(Float pagerank) {
        this.pagerank = pagerank;
    }

    public void setUserScore(Float userScore) {
        this.userScore = userScore;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @DynamoDBHashKey(attributeName = "nodeID")
    public String getNodeID() {
        return nodeID;
    }

    @DynamoDBRangeKey(attributeName = "docID")
    public String getDocID() {
        return docID;
    }

    @DynamoDBAttribute(attributeName = "docTitle")
    public String getDocTitle() {
        return docTitle;
    }

    @DynamoDBAttribute(attributeName = "docURL")
    public String getDocURL() {
        return docURL;
    }

    @DynamoDBAttribute(attributeName = "score01")
    public Float getScore01() {
        return score01;
    }

    @DynamoDBAttribute(attributeName = "score02")
    public Float getScore02() {
        return score02;
    }

    @DynamoDBAttribute(attributeName = "score03")
    public Float getScore03() {
        return score03;
    }

    @DynamoDBAttribute(attributeName = "score04")
    public Float getScore04() {
        return score04;
    }

    @DynamoDBAttribute(attributeName = "score05")
    public Float getScore05() {
        return score05;
    }

    @DynamoDBAttribute(attributeName = "score06")
    public Float getScore06() {
        return score06;
    }

    @DynamoDBAttribute(attributeName = "score07")
    public Float getScore07() {
        return score07;
    }

    @DynamoDBAttribute(attributeName = "score08")
    public Float getScore08() {
        return score08;
    }

    @DynamoDBAttribute(attributeName = "score09")
    public Float getScore09() {
        return score09;
    }

    @DynamoDBAttribute(attributeName = "score10")
    public Float getScore10() {
        return score10;
    }

    @DynamoDBAttribute(attributeName = "score11")
    public Float getScore11() {
        return score11;
    }

    @DynamoDBAttribute(attributeName = "score12")
    public Float getScore12() {
        return score12;
    }

    @DynamoDBAttribute(attributeName = "pagerank")
    public Float getPagerank() {
        return pagerank;
    }

    @DynamoDBAttribute(attributeName = "userscore")
    public Float getUserScore() {
        return userScore;
    }

    @DynamoDBIgnore
    public boolean isModified() {
        return modified;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NodeDocument{" + "nodeID=").append(nodeID).append(", docID=").append(docID).append(", docTitle=").append(docTitle).append(", score01=").append(score01).append(", score02=").append(score02).append(", pagerank=").append(pagerank).append(", userScore=").append(userScore).append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.nodeID != null ? this.nodeID.hashCode() : 0);
        hash = 29 * hash + (this.docID != null ? this.docID.hashCode() : 0);
        hash = 29 * hash + (this.docTitle != null ? this.docTitle.hashCode() : 0);
        hash = 29 * hash + (this.docURL != null ? this.docURL.hashCode() : 0);
        hash = 29 * hash + (this.score01 != null ? this.score01.hashCode() : 0);
        hash = 29 * hash + (this.score02 != null ? this.score02.hashCode() : 0);
        hash = 29 * hash + (this.pagerank != null ? this.pagerank.hashCode() : 0);
        hash = 29 * hash + (this.userScore != null ? this.userScore.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NodeDocument other = (NodeDocument) obj;
        if (this.nodeID != other.nodeID && (this.nodeID == null || !this.nodeID.equals(other.nodeID))) {
            return false;
        }
        if ((this.docID == null) ? (other.docID != null) : !this.docID.equals(other.docID)) {
            return false;
        }
        if ((this.docTitle == null) ? (other.docTitle != null) : !this.docTitle.equals(other.docTitle)) {
            return false;
        }
        if ((this.docURL == null) ? (other.docURL != null) : !this.docURL.equals(other.docURL)) {
            return false;
        }
        if (this.score01 != other.score01 && (this.score01 == null || !this.score01.equals(other.score01))) {
            return false;
        }
        if (this.score02 != other.score02 && (this.score02 == null || !this.score02.equals(other.score02))) {
            return false;
        }
        if (this.pagerank != other.pagerank && (this.pagerank == null || !this.pagerank.equals(other.pagerank))) {
            return false;
        }
        if (this.userScore != other.userScore && (this.userScore == null || !this.userScore.equals(other.userScore))) {
            return false;
        }
        return true;
    }
}
