package com.intellisophic.linkapedia.generic.services.xcache;

/**
 *
 * @author juanidrobo
 */
public interface ICache {

    public Object get(String id);

    public void set(String id, Object obj, int lastingSecondsCache);

    public void delete(String Id);

}
