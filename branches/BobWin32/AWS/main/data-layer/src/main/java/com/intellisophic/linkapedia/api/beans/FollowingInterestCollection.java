/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "FollowingInterestCollection", hashKey = "interestCollectionId", hashKeyType = "S", rangeKey = "userId", rangeKeyType = "S")
@DynamoDBTable(tableName = "FollowingInterestCollection")
public class FollowingInterestCollection implements Serializable {

    private String interestCollectionId;
    private String userId;
    private String userName;
    private String userFbId;

    @DynamoDBHashKey(attributeName = "interestCollectionId")
    public String getInterestCollectionId() {
        return interestCollectionId;
    }

    @DynamoDBRangeKey(attributeName = "userId")
    public String getUserId() {
        return userId;
    }

    @DynamoDBAttribute(attributeName = "userName")
    public String getUserName() {
        return userName;
    }

    @DynamoDBAttribute(attributeName = "userFbId")
    public String getUserFbId() {
        return userFbId;
    }

    public void setInterestCollectionId(String interestCollectionId) {
        this.interestCollectionId = interestCollectionId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserFbId(String userFbId) {
        this.userFbId = userFbId;
    }
}
