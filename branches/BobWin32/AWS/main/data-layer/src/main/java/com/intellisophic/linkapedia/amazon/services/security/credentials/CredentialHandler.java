package com.intellisophic.linkapedia.amazon.services.security.credentials;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class CredentialHandler {

    private static final Logger log = Logger.getLogger(CredentialHandler.class);
    private static AWSCredentials credentials;
    private static ClientConfiguration config;
    private static final String CREDENTIAL_FILE_NAME = "AwsCredentials.properties";
    private static final int DEFAULT_MAX_ERROR_RETRY = 10;

    public static AWSCredentials getCredentials() {
        if (credentials == null) {
            try {
                credentials = new PropertiesCredentials(CredentialHandler.class.getClassLoader().getResourceAsStream(CREDENTIAL_FILE_NAME));
            } catch (Exception e) {
                log.debug("An exception has ocurred while reading credential information.", e);
            }
        }
        return credentials;
    }

    public static ClientConfiguration getConfiguration() {
        if (config == null) {
            config = new ClientConfiguration();
            config.setMaxErrorRetry(DEFAULT_MAX_ERROR_RETRY);
        }
        return config;
    }
}
