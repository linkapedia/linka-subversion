package com.intellisophic.linkapedia.generic.services.cache;

import java.util.HashMap;
import java.util.Map;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class CacheHandler {

    private static final Logger log = LoggerFactory.getLogger(CacheHandler.class);
    private static final Map<String, CacheObject> cacheHandlerMap = new HashMap<String, CacheObject>();

    private CacheHandler() {
        log.debug("CacheHandler Initialized");
    }

    public static void initCacheHandler(String cacheIdentifiers[]) {
        log.debug("CacheHandler Initialized");
        CacheObject objectCache;
        for (String cacheIdentifier : cacheIdentifiers) {
            objectCache = new CacheObject(cacheIdentifier);
            cacheHandlerMap.put(cacheIdentifier, objectCache);
            log.info("Initialized Cache for: {}", cacheIdentifier);
        }
    }

    public static void initCacheHandler(String cacheIdentifier) {
        log.debug("CacheHandler Initialized");
        cacheHandlerMap.put(cacheIdentifier, new CacheObject(cacheIdentifier));
        log.info("Initialized Cache for: {}", cacheIdentifier);
    }

    public static void addCachedObjectHandler(String cacheIdentifier, CacheObject cachedObjectHandler) {
        log.debug("addCachedObjectHandler(String, CacheLogicAbstract)");
        cacheHandlerMap.put(cacheIdentifier, cachedObjectHandler);
    }

    public static CacheObject getCachedObjectHandler(String cacheIdentifier) {
        log.debug("getCachedObjectHandler(String)");
        return cacheHandlerMap.get(cacheIdentifier);
    }

    public static int getCachedObjectHandlerCount() {
        log.debug("getCachedObjectHandlerCount()");
        return cacheHandlerMap.size();
    }

    /**
     * Method to close the Cache when the application shutdown.
     */
    public static void shutdown() {
        CompositeCacheManager compositeCacheManager = CompositeCacheManager.getInstance();
        compositeCacheManager.release();
        compositeCacheManager.shutDown();
    }
}
