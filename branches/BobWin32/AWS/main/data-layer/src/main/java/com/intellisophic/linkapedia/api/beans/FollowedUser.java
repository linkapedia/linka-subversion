/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "FollowedUser", hashKey = "followedUserId", hashKeyType = "S", rangeKey = "followingUserId", rangeKeyType = "S")
@DynamoDBTable(tableName = "FollowedUser")
public class FollowedUser implements Serializable {

    /**
     * unique/key/followedUserId
     */
    private String followedUserId;
    private String followingUserId;
    private String followingUserName;
    private String followingUserLastName;
    private String followingUserFbId;
    private Long timeStamp;

    @DynamoDBHashKey(attributeName = "followedUserId")
    public String getFollowedUserId() {
        return followedUserId;
    }

    @DynamoDBRangeKey(attributeName = "followingUserId")
    public String getFollowingUserId() {
        return followingUserId;
    }

    @DynamoDBAttribute(attributeName = "followingUserName")
    public String getFollowingUserName() {
        return followingUserName;
    }

    @DynamoDBAttribute(attributeName = "followingUserLastName")
    public String getFollowingUserLastName() {
        return followingUserLastName;
    }

    @DynamoDBAttribute(attributeName = "followingUserFbId")
    public String getFollowingUserFbId() {
        return followingUserFbId;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setFollowingUserFbId(String followingUserFbId) {
        this.followingUserFbId = followingUserFbId;
    }

    public void setFollowedUserId(String followedUserId) {
        this.followedUserId = followedUserId;
    }

    public void setFollowingUserId(String followingUserId) {
        this.followingUserId = followingUserId;
    }

    public void setFollowingUserName(String followingUserName) {
        this.followingUserName = followingUserName;
    }

    public void setFollowingUserLastName(String followingUserLastName) {
        this.followingUserLastName = followingUserLastName;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
