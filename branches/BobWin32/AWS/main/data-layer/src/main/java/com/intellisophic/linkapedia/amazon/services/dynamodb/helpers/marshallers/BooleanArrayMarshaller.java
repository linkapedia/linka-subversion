package com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshaller;
import com.intellisophic.linkapedia.generic.utils.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class BooleanArrayMarshaller implements DynamoDBMarshaller<Boolean[]> {

    private static final Logger log = Logger.getLogger(BooleanArrayMarshaller.class);
    private static final String AMAZON_FIELD_SEPARATOR = "||";
    private static final String AMAZON_FIELD_SEPARATOR_REGEX = "\\|\\|";

    @Override
    public String marshall(Boolean[] values) {
        log.debug("marshall(Boolean[])");
        StringBuilder sb = new StringBuilder();
        for (Boolean value : values) {
            sb.append(value).append(AMAZON_FIELD_SEPARATOR);
        }
        return StringUtils.rCharTrim(sb, AMAZON_FIELD_SEPARATOR);
    }

    @Override
    public Boolean[] unmarshall(Class<Boolean[]> clazz, String obj) {
        log.debug("unmarshall(Class<Boolean[]>, String)");
        String[] values = obj.split(AMAZON_FIELD_SEPARATOR_REGEX);
        Boolean[] returnValues = new Boolean[values.length];
        for (int i = 0; i < returnValues.length; i++) {
            String value = values[i];
            if ("true".equals(value)) {
                returnValues[i] = true;
            } else if ("false".equals(value)) {
                returnValues[i] = false;
            }
        }
        return returnValues;
    }
}
