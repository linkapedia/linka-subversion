package com.intellisophic.linkapedia.api.handlers;

import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.api.exceptions.ObjectNotFoundException;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class UserAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(UserAPIHandler.class);
    // public static final String USER_CACHE_IDENTIFIER = "users";
    //private static volatile CacheObject userCache = initializeCache();
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(UserAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    private UserAPIHandler() {
    }

    /*private static CacheObject initializeCache() {
     CacheHandler.initCacheHandler(USER_CACHE_IDENTIFIER);
     return CacheHandler.getCachedObjectHandler(USER_CACHE_IDENTIFIER);
     }*/
    public static User getUser(String userId) throws IllegalArgumentException, ClassNotFoundException {
        log.debug("getUser(String)");
        if (userId == null || userId.trim().isEmpty()) {
            throw new IllegalArgumentException("The user id {userId} can not be null");
        }
        User user = null;
        user = (User) cache.get(userId);
        if (user != null) {
            return user;
        } else {

            user = getDynamoDBMapper().load(User.class, userId);
            if (user != null) {
                cache.set(userId, user, ONE_HOUR);
            } else {
                log.warn("No user found for id {" + userId + "}");
            }
            return user;
        }
    }

    public static void saveUser(User user) throws IllegalArgumentException, ClassNotFoundException {
        log.debug("saveUser(User)");
        if (user == null) {
            throw new IllegalArgumentException("The user to be saved can not be null");
        }
        getDynamoDBMapper().save(user);
        UserFeedAPIHandler userEvents = new UserFeedAPIHandler(user.getUserId());
        userEvents.registerEvent(user, UserFeedAPIHandler.REGISTER);
        cache.set(user.getUserId(), user, ONE_HOUR);

    }

    public static void deleteUser(User user) throws IllegalArgumentException {
        log.debug("deleteUser(User)");
        if (user == null) {
            throw new IllegalArgumentException("The user to be deleted can not be null");
        }
        getDynamoDBMapper().delete(user);
        cache.delete(user.getUserId());

        //user = null;
    }

    public static void deleteUsers(List<User> users) {
        log.debug("deleteUsers(List<User>)");
        getDynamoDBMapper().batchDelete(users);
    }

    public static void updateUser(User user) throws IllegalArgumentException, ObjectNotFoundException {
        log.debug("updateUser(User)");
        if (user == null) {
            throw new IllegalArgumentException("The user to be updated can not be null");
        }
        User userCache = (User) cache.get(user.getUserId());
        if (userCache != null && !userCache.equals(user)) {
            getDynamoDBMapper().save(user);
            cache.set(user.getUserId(), user, ONE_HOUR);
        } else if (userCache == null) {
            User userFromDynamo = getDynamoDBMapper().load(User.class, user.getUserId());
            if (userFromDynamo != null && !userFromDynamo.equals(user)) {
                getDynamoDBMapper().save(user);
                cache.set(user.getUserId(), user, ONE_HOUR);
            } else {
                log.debug("The user is up to dated already!");
                // throw new ObjectNotFoundException("the user object to be updated doesn't exist.");

            }
        }
    }

    public static void deleteCacheUser(User user) {
        cache.delete(user.getUserId());
    }
}
