package com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshaller;
import com.intellisophic.linkapedia.api.beans.enums.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class UserRoleEnumMarshaller implements DynamoDBMarshaller<UserRole> {

    private static final Logger log = LoggerFactory.getLogger(UserRoleEnumMarshaller.class);

    @Override
    public String marshall(UserRole userRole) {
        log.debug("marshall(UserRole)");
        return userRole.getUserRole();
    }

    @Override
    public UserRole unmarshall(Class<UserRole> clazz, String userRole) {
        log.debug("unmarshall(Class<UserRole>, String)");
        for (UserRole userRoles : UserRole.values()) {
            if (userRoles.getUserRole().equals(userRole)) {
                return userRoles;
            }
        }
        log.warn("UserRole {" + userRole + "} not found. Returning default value.");
        //Return default userrole.
        return UserRole.GUEST;
    }
}
