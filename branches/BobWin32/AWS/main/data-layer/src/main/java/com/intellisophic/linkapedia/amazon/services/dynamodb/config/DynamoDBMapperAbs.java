package com.intellisophic.linkapedia.amazon.services.dynamodb.config;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMapper;

/**
 *
 * @author Xander Kno
 */
public abstract class DynamoDBMapperAbs extends DynamoDBClientAbs {

    //protected static final DynamoDBMapper dynamoMapper = new DynamoDBMapper(getDynamoDBClient());
    protected static final DynamoDBWrapperAbs dynamoDBWrapper=new DynamoDBWrapperAbs(getDynamoDBClient());
   
    /**
     * The only information needed to create a mapper is a dynamo client
     * initialized using the security credentials consisting of the AWS Access Key ID
     * and Secret Access Key.
     *
     * @see com.amazonaws.services.dynamodb.AmazonDynamoDBClient
     * @see com.amazonaws.services.dynamodb.datamodeling.DynamoDBMapper
     */
    protected static DynamoDBMapper getDynamoDBMapper() {
        return dynamoDBWrapper;
    }

}
