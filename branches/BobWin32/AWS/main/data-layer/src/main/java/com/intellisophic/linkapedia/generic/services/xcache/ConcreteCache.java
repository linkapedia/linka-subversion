package com.intellisophic.linkapedia.generic.services.xcache;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeoutException;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.utils.AddrUtil;

/**
 *
 * @author juanidrobo
 */
public class ConcreteCache implements ICache {

    private String cacheIdentifier = null;
    public static String env = "newdev";
    private static MemcachedClient client;
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConcreteCache.class);
    private static ResourceBundle cacheConfig;
    private static String cacheUrl = null;

    public ConcreteCache(String cacheId) {
        cacheIdentifier = cacheId;
    }

    public static void cacheConfig(String url) {
        if (cacheUrl == null) {
            cacheUrl = url;
        } else if (!url.equals(cacheUrl)) {
            cacheUrl = url;
            shutdown();
            client = null;
        }
    }

    private static MemcachedClient getCacheClient() {
        if (client == null) {
            cacheConfig = ResourceBundle.getBundle("system/cache/configMemcached");
            log.info("Memcached client is initializing");
            try {
                MemcachedClientBuilder builder;
                List<InetSocketAddress> connections = null;
                if (cacheUrl == null) {
                    if ("newdev".equals(ConcreteCache.env)) {
                        cacheUrl = cacheConfig.getString("org.linkapedia.xcache.connections.dev");
                    } else {
                        cacheUrl = cacheConfig.getString("org.linkapedia.xcache.connections");
                    }
                }

                connections = AddrUtil.getAddresses(cacheUrl);
                builder = new XMemcachedClientBuilder(connections);
                client = builder.build();

            } catch (IOException ex) {
                log.error(ex.getMessage());
                return client;
            }

            log.info("Memcached client is loaded");

        }

        return client;
    }

    @Override
    public Object get(String id) {

        Object obj = null;
        try {

            obj = getCacheClient().get(id);

            log.info("getting cache id:" + id);
            obj = getCacheClient().get(cacheIdentifier + id);


        } catch (TimeoutException ex) {

            log.error(ex.getMessage());
        } catch (InterruptedException ex) {

            log.error(ex.getMessage());
        } catch (MemcachedException ex) {

            log.error(ex.getMessage());
        }
        return obj;

    }

    @Override
    public void set(String id, Object obj, int lastingSecondsCache) {

        log.info("setting cache id:" + id);
        try {
            getCacheClient().set(cacheIdentifier + id, lastingSecondsCache, obj);
            //getCacheClient().touch(id, lastingSecondsCache);
        } catch (TimeoutException ex) {

            log.error(ex.getMessage());
        } catch (InterruptedException ex) {

            log.error(ex.getMessage());
        } catch (MemcachedException ex) {

            log.error(ex.getMessage());
        }

    }

    @Override
    public void delete(String id) {

        log.info("Deleting cache id:" + id);
        try {
            getCacheClient().delete(cacheIdentifier + id);
        } catch (TimeoutException ex) {

            log.error(ex.getMessage());
        } catch (InterruptedException ex) {

            log.error(ex.getMessage());
        } catch (MemcachedException ex) {

            log.error(ex.getMessage());
        }

    }

    public static void shutdown() {
        try {
            client.shutdown();
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }
}
