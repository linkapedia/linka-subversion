/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.generic.services.xcache;

/**
 *
 * @author juanidrobo
 */
public class ConcreteCacheCreator extends CacheCreator{
  
    
    @Override
    protected ICache factoryMethod(String cacheId) {
        return new ConcreteCache(cacheId);
    }




}
