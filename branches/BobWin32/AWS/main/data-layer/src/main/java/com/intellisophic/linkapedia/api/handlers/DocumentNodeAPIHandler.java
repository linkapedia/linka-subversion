package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.ComparisonOperator;
import com.amazonaws.services.dynamodb.model.Condition;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author juanidrobo
 */
public class DocumentNodeAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(DocumentNodeAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(DocumentNodeAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<DocumentNode> getDocumentNodesByDocumentId(String docId) throws Exception {
        log.info("getDocumentNodesByDocumentId - " + docId);
        List<DocumentNode> returnList = new ArrayList<DocumentNode>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(docId);
            if (cacheObj != null) {
                return (List<DocumentNode>) cacheObj;
            } else {
                PaginatedQueryList<DocumentNode> result = getDynamoDBMapper().query(
                        DocumentNode.class, new DynamoDBQueryExpression(new AttributeValue(docId)));

                for (DocumentNode doc : result) {
                    if (doc != null) {
                        returnList.add(doc);
                    }
                }
                cache.set(docId, returnList, ONE_HOUR);
            }
        } catch (Exception ex) {
            log.error("Exception in getDocumentNodesByDocumentId - " + docId + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;

    }

    public static DocumentNode getDocumentNode(String nodeId, String docId) throws Exception {
        log.info("getDocumentNode - " + nodeId + " " + docId);
        log.debug("getNode(Long)");
        DocumentNode documentNode = null;
        if (nodeId == null || docId == null) {
            throw new IllegalArgumentException("some parameters can't be null");
        }
        documentNode = (DocumentNode) cache.get(nodeId + docId);
        if (documentNode != null) {
            return documentNode;
        } else {
            documentNode = getDynamoDBMapper().load(DocumentNode.class, docId, nodeId);
            if (documentNode != null) {
                documentNode.setModified(false);
                cache.set(nodeId + docId, documentNode, ONE_HOUR);
            } else {
                log.warn("No node found for docId {" + docId + "}");
            }
            return documentNode;
        }

    }
    
    /**
     *  Find DocumentNodes With Score01= 0 
     *  Author: Sahar Ebadi
     * @param 
     * @return returnList
     */
    public static List<DocumentNode> FindDocumentNodesWithScoreOneValueEqualToZero() throws Exception {
        log.info("FindDocumentNodesWithScoreOneValueEqualToZero - ");
        List<DocumentNode> returnList = new ArrayList<DocumentNode>();
        try {
                Condition scanFilterCondition = new Condition()
                        .withComparisonOperator(ComparisonOperator.EQ)
                        .withAttributeValueList(new AttributeValue().withN("0"));
                Map<String, Condition> conditions = new HashMap<String, Condition>();
                conditions.put("score01", scanFilterCondition);
                DynamoDBScanExpression scanExpression=new DynamoDBScanExpression();
                scanExpression.addFilterCondition("score01", scanFilterCondition);
                PaginatedScanList<DocumentNode> result = getDynamoDBMapper().scan(DocumentNode.class, scanExpression);
                for (DocumentNode doc : result) {
                            if (doc != null) {
                                returnList.add(doc);
                            }
                }
        } catch (Exception ex) {
            log.error("Exception in FindDocumentNodesWithScoreOneValueEqualToZero - " + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;
    }
    
     public static List<DocumentNode> ScanDocumentNode() throws Exception {
        log.info("FindDocumentNodesWithScoreOneValueEqualToZero - ");
        List<DocumentNode> returnList = new ArrayList<DocumentNode>();
        try {
                Condition scanFilterCondition = new Condition()
                        .withComparisonOperator(ComparisonOperator.NOT_NULL);
//                for testing porpose, is faster to return a small portion of table                
//                Condition scanFilterCondition = new Condition()
//                            .withComparisonOperator(ComparisonOperator.BEGINS_WITH)
//                            .withAttributeValueList(new AttributeValue().withS("aaae"));
                // untill here
                Map<String, Condition> conditions = new HashMap<String, Condition>();
                conditions.put("docID", scanFilterCondition);
                DynamoDBScanExpression scanExpression=new DynamoDBScanExpression();
                scanExpression.addFilterCondition("docID", scanFilterCondition);
                PaginatedScanList<DocumentNode> result = getDynamoDBMapper().scan(DocumentNode.class, scanExpression);
                for (DocumentNode doc : result) {
                            if (doc != null) {
                                returnList.add(doc);
                            }
                }
        } catch (Exception ex) {
            log.error("Exception in ScanDocumentNode - " + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;
    }

    public static void saveDocumentNode(DocumentNode documentNode) {
        //TODO cache
        getDynamoDBMapper().save(documentNode);
        
    }
    /**
     *  delete DocumentNode (used to delete DocumentNodes With Score01= 0) 
     *  Author: Sahar Ebadi
     * @param documentNode
     * @return 
     */
    public static void deleteDocumentNode(DocumentNode documentNode) {
        //TODO cache
        getDynamoDBMapper().delete(documentNode);
    }
}
