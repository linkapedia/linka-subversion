package com.intellisophic.linkapedia.api.handlers;

import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.UserFb;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class UserFbAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(UserAPIHandler.class);
    // public static final String USER_CACHE_IDENTIFIER = "users";
    //private static volatile CacheObject userCache = initializeCache();
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(UserFbAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    private UserFbAPIHandler() {
    }

    public static void saveUserFb(UserFb userFb) throws IllegalArgumentException {
        log.debug("saveUserFb (UserFb)");
        if (userFb == null) {
            log.debug("The userFb to be saved can not be null");
            throw new IllegalArgumentException("The user to be saved can not be null");
        }
        UserFb userFbFromDynamo = getDynamoDBMapper().load(UserFb.class, userFb.getFbId());
        if (userFbFromDynamo == null) {
            //user.setPassword(DigestUtils.getHashWithSalt(user.getPassword()));
            getDynamoDBMapper().save(userFb);
            cache.set(userFb.getFbId(), userFb, ONE_HOUR);
        } else {
            log.debug("The user is already saved so it wont be saved again.");

        }


    }

    public static UserFb getUserFb(String fbId) throws IllegalArgumentException {
        log.debug("getUserFb(String)");
        if (fbId == null) {
            throw new IllegalArgumentException("The user fbId {fbId} can not be null");
        }
        UserFb userFb = null;
        userFb = (UserFb) cache.get(fbId);
        if (userFb != null) {
            return userFb;
        } else {

            userFb = getDynamoDBMapper().load(UserFb.class, fbId);
            if (userFb != null) {
                cache.set(fbId, userFb, ONE_HOUR);
            } else {
                log.warn("No user found for fbId {" + fbId + "}");
            }
            return userFb;
        }


    }
}
