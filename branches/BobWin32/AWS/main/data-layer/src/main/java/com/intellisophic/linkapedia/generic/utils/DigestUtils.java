package com.intellisophic.linkapedia.generic.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class DigestUtils {
    private static final Logger log = LoggerFactory.getLogger(DigestUtils.class);

    /**
     * Method that allow to convert the text in a MD5 hash form. <p />
     * <p/>
     * @param text text to be converted to MD5
     * @return MD5 hash of the given text.
     */
    public static String getMD5(String text) {
        log.debug("getMD5(String)");
        StringBuilder sb = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());
            byte byteData[] = md.digest();
            sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            log.error("The Algorithm that you are trying to use does not exist.", e);
        } catch (Exception e) {
            log.error("An exception has ocurred.", e);
        }
        return (sb == null) ? "" : sb.toString();
    }

    /**
     * Hash a password using the OpenBSD bcrypt scheme
     * <p/>
     * @param text the text to hash
     * @return	the hashed password
     */
    public static String getHashWithSalt(String text) {
        log.debug("getHashWithSalt(String)");
        return BCrypt.hashpw(text, BCrypt.gensalt());
    }

    /**
     * Method that allow the validation of the two hashed strings
     * <p/>
     * @param plaintext	the plaintext password to verify
     * @param hashed	the previously-hashed password
     * @return true if both passwords match. false otherwise.
     */
    public static boolean checkPassword(String plaintext, String hashed) {
        log.debug("checkPassword(String)");
        return BCrypt.checkpw(plaintext, hashed);
    }
}