package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollectionElement;
import com.intellisophic.linkapedia.api.beans.InterestCollectionFollowed;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class InterestCollectionFollowedAPIHandler extends DynamoDBMapperAbs {
    
    private static final Logger log = LoggerFactory.getLogger(InterestCollectionFollowedAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(InterestCollectionFollowedAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;
    
    public static List<InterestCollection> getInterestCollectionFollowedByUserId(String userId) throws Exception {
        log.info("getInterestCollectionFollowedByUserId - userId: " + userId);
        List<InterestCollection> returnList = new ArrayList<InterestCollection>();
        
        Object cacheObj = null;
        try {
            cacheObj = cache.get(userId);
            if (cacheObj != null) {
                return (List<InterestCollection>) cacheObj;
            } else {
                PaginatedQueryList<InterestCollectionFollowed> paginatedQueryList = getDynamoDBMapper().query(
                        InterestCollectionFollowed.class, new DynamoDBQueryExpression(new AttributeValue().withS(userId)));
                
                for (InterestCollectionFollowed icf : paginatedQueryList) {
                    if (icf != null) {
                        returnList.add(DataHandlerFacade.getInterestCollection(icf.getInterestCollectionId()));
                    }
                }
                cache.set(userId, returnList, ONE_HOUR);
                
            }
        } catch (Exception ex) {
            log.error("Exception in getInterestCollectionFollowedByUserId - " + userId + " - " + ex.getMessage());
            throw ex;
        }
        
        return returnList;
    }
    
    public static List<InterestCollection> getInterestCollectionOwnedByUserId(String userId) throws Exception {
        return getInterestCollectionOwnedByUserId(userId, false);
    }

    //we do not use cache in this method because the data is supplied by getInterestCollectionFollowedByUserId
    public static List<InterestCollection> getInterestCollectionOwnedByUserId(String userId, boolean retrieveElements) throws Exception {
        log.info("getInterestCollectionFollowedByUserId - userId: " + userId);
        List<InterestCollection> returnList = new ArrayList<InterestCollection>();
        List<InterestCollection> icsFollowed = null;
        
        try {
            icsFollowed = getInterestCollectionFollowedByUserId(userId);
            for (InterestCollection ic : icsFollowed) {
                if (ic.getOwnerId().equals(userId)) {
                    if (retrieveElements) {
                        List<InterestCollectionElement> interestCollectionElements = InterestCollectionElementAPIHandler.getInterestCollectionElement(ic.getInterestCollectionId());
                        ic.setInterestCollectionElements(interestCollectionElements);
                    }
                    returnList.add(ic);
                }
            }
            
        } catch (Exception ex) {
            log.error("Exception in getInterestCollectionOwnedByUserId - " + userId + " - " + ex.getMessage());
            throw ex;
        }
        
        return returnList;
    }
    
    public static int getInterestCollectionFollowedCount(String userId) throws Exception {
        log.info("getInterestCollectionFollowedCount - userId: " + userId);
        List<InterestCollection> interestCollections = new ArrayList<InterestCollection>();
        Object cacheObj = null;
        int count = 0;
        try {
            cacheObj = cache.get(userId);
            if (cacheObj != null) {
                interestCollections = (List<InterestCollection>) cacheObj;
                
            } else {
                interestCollections = getInterestCollectionFollowedByUserId(userId);
                
            }
            count = interestCollections.size();
            
        } catch (Exception ex) {
            log.error("Exception in getInterestCollectionFollowedCount - userId: " + userId + " - " + ex.getMessage());
            throw ex;
            
        }
        
        return count;
    }
    
    public static void saveInterestCollectionFollowed(InterestCollectionFollowed interestCollectionFollowed) throws Exception {
        log.info("saveInterestCollectionFollowed - InterestCollectionFollowed");
        try {
            getDynamoDBMapper().save(interestCollectionFollowed);
            
            cache.delete(interestCollectionFollowed.getUserId()); //delete from cache to ensure the data consistence

            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(interestCollectionFollowed.getUserId());
            userEvents.registerEvent(interestCollectionFollowed, UserFeedAPIHandler.FOLLOW_COLLECTION);
            
        } catch (Exception ex) {
            log.error("Exception in saveInterestCollectionFollowed: " + ex.getMessage());
            throw ex;
        }
    }
    
    public static void deleteInterestCollectionFollowed(InterestCollectionFollowed interestCollectionFollowed) throws Exception {
        log.info("deleteInterestCollectionFollowed - InterestCollectionFollowed");
        try {
            getDynamoDBMapper().delete(interestCollectionFollowed);
            cache.delete(interestCollectionFollowed.getUserId()); //delete from cache to ensure the data consistence
            cache.delete("interestCollectionFollowed" + interestCollectionFollowed.getUserId() + interestCollectionFollowed.getInterestCollectionId());
        } catch (Exception ex) {
            log.error("Exception in deleteInterestCollectionFollowed: " + ex.getMessage());
            throw ex;
        }
    }
    
    public static void updateInterestCollectionFollowed(String userId) throws Exception {
        log.info("updateInterestCollectionFollowed - String");
        try {
            
            cache.delete(userId); //delete from cache because the interestColection has been updated
        } catch (Exception ex) {
            log.error("Exception in updateInterestCollectionFollowed: " + ex.getMessage());
            throw ex;
        }
    }
    
    public static InterestCollectionFollowed getInterestCollectionFollowedByUserIdAndCollectionId(String userId, String collectionId) throws Exception {
        InterestCollectionFollowed icf = null;
        Object cacheObj = null;
        try {
            cacheObj = cache.get("interestCollectionFollowed" + userId + collectionId);
            if (cacheObj != null) {
                return (InterestCollectionFollowed) cacheObj;
            } else {
                icf = getDynamoDBMapper().load(InterestCollectionFollowed.class, userId, collectionId);
                if (icf != null) {
                    cache.set("interestCollectionFollowed" + userId + collectionId, icf, ONE_HOUR);
                } else {
                    return null;
                }
            }
        } catch (Exception ex) {
            log.error("Exception in getInterestCollectionFollowedByUserIdAndCollectionId: " + ex.getMessage());
            throw ex;
        }
        return icf;
    }
}
