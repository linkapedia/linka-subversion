package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;


/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "NodeLink", hashKey = "nodeStaticLinkId", hashKeyType = "S", rangeKey = "nodeLinkId", rangeKeyType = "S")
@DynamoDBTable(tableName = "NodeLink")
public class NodeLink implements Serializable {

    private String nodeLinkId;
    private String title;
    private String nodeStaticLinkId;
    private String nodeParentStaticId;

    public void setNodeStaticLinkId(String nodeStaticLinkId) {
        this.nodeStaticLinkId = nodeStaticLinkId;
    }

    public void setNodeParentStaticId(String nodeParentStaticId) {
        this.nodeParentStaticId = nodeParentStaticId;
    }

    public void setNodeLinkId(String nodeLinkId) {
        this.nodeLinkId = nodeLinkId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @DynamoDBHashKey(attributeName = "nodeStaticLinkId")
    public String getNodeStaticLinkId() {
        return nodeStaticLinkId;
    }

    @DynamoDBRangeKey(attributeName = "nodeLinkId")
    public String getNodeLinkId() {
        return nodeLinkId;
    }

    @DynamoDBAttribute(attributeName = "title")
    public String getTitle() {
        return title;
    }

    @DynamoDBAttribute(attributeName = "nodeParentStaticId")
    public String getNodeParentStaticId() {
        return nodeParentStaticId;
    }
}
