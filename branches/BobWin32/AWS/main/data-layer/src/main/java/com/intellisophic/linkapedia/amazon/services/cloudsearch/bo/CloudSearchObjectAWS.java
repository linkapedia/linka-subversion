package com.intellisophic.linkapedia.amazon.services.cloudsearch.bo;

import com.intellisophic.linkapedia.api.beans.CloudSearchInfo;
import java.io.Serializable;
import net.sf.json.JSONObject;

/**
 * Each document is represented with this information
 * <p/>
 * @author andres
 * <p/>
 */
public class CloudSearchObjectAWS extends CloudSearchInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4823200866874382818L;
    private CloudSearchInfo fields;

    /**
     * constructor with one sdf
     * @param cloud 
     */
    public CloudSearchObjectAWS(CloudSearchInfo cloud) {
        this.fields = cloud;
    }

    /**
     * set head parameters of this document
     * <p/>
     * @param type
     * @param id
     * @param version
     */
    public void setHeadParameter(String type, String id, long version) {
        this.setType(type);
        this.setId(id);
        this.setVersion(version);
    }

    /**
     * get the object json of the object
     * <p/>
     * @return JsonObject with the information
     */
    public JSONObject toSdf() {
        JSONObject jsonObj = new JSONObject();
        JSONObject jsonObjFields = new JSONObject();

        // head of the document
        jsonObj.put("type", this.getType());
        jsonObj.put("id", this.getId());
        jsonObj.put("version", this.getVersion());
        jsonObj.put("lang", fields.getLang());

        // fields
        jsonObjFields.put("nodename", fields.getNodeName());
        jsonObjFields.put("nodedesc", fields.getNodeDesc());
        jsonObjFields.put("path", fields.getNodePath());
        jsonObjFields.put("taxonomyid", fields.getCorpusId());
        jsonObjFields.put("taxonomyname", fields.getCorpusName());
        jsonObjFields.put("public", fields.isNodePublic());

        jsonObj.put("fields", jsonObjFields);

        return jsonObj;
    }
}
