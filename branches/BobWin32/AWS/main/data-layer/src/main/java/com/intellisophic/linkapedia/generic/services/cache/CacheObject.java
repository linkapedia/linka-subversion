package com.intellisophic.linkapedia.generic.services.cache;

import com.intellisophic.linkapedia.generic.services.cache.interfaces.Cacheable;
import java.io.Serializable;
import java.util.ResourceBundle;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.access.exception.InvalidArgumentException;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class CacheObject implements Cacheable {

    private static final Logger log = Logger.getLogger(CacheObject.class);
    private JCS cacheInternalObject = null;
    private static ResourceBundle cacheMessages = ResourceBundle.getBundle("system/cache/messages");

    private CacheObject() {
    }

    /**
     * Contructor
     *
     * @param cacheIdentifier Unique cache identifier.
     */
    public CacheObject(String cacheIdentifier) {
        log.debug(cacheMessages.getString("cache." + cacheIdentifier + ".constructor"));
        try {
            // Load the cache
            if (cacheInternalObject == null) {
                cacheInternalObject = JCS.getInstance(cacheIdentifier);
                log.info(cacheMessages.getString("cache." + cacheIdentifier + ".initializecomplete"));
            } else {
                log.info(cacheMessages.getString("cache." + cacheIdentifier + ".cachealreadyloaded"));
            }
        } catch (CacheException ex) {
            log.error(cacheMessages.getString("cache.generic.exception"), ex);
        }
    }

    /**
     * Method to return the object for a given objectKey.
     * <p/>
     * @param objectKey unique key of the object on the cache.
     * @return Object stored on the cache for the given objectKey.
     */
    @Override
    public Object getObject(Serializable objectKey) {
        log.debug("getObject(Serializable)");
        return cacheInternalObject.get(objectKey);
    }

    /**
     * Method that allows to add new object in the cache.
     * <p/>
     * @param objectKey unique key of the object on the cache.
     * @param objectValue Object to be stored on the cache.
     */
    @Override
    public void setObject(Serializable objectKey, Object objectValue) {
        log.debug("setObject(Serializable, Serializable)");
        try {
            if (objectKey == null || ((objectKey instanceof String) && ((String) objectKey).isEmpty())) {
                throw new InvalidArgumentException("No valid key specified to store the object");
            }
            if (objectKey == null || ((objectKey instanceof Integer) && ((Integer) objectKey).intValue() <= 0)) {
                throw new InvalidArgumentException("No valid key specified to store the object");
            }
            if (objectValue == null) {
                throw new InvalidArgumentException("The object to store can not be null or empty for " + objectKey);
            }
            cacheInternalObject.put(objectKey, objectValue);
        } catch (InvalidArgumentException ex) {
            log.error(cacheMessages.getString("cache.generic.exception"), ex);
        } catch (CacheException ex) {
            log.error(cacheMessages.getString("cache.generic.exception"), ex);
        }
    }

    /**
     * Method to remove an object from the cache for a given objectKey.
     * <p/>
     * @param objectKey Object to be deleted from the cache.
     */
    @Override
    public void deleteObject(Serializable objectKey) {
        log.debug("deleteObject(Serializable)");
        try {
            cacheInternalObject.remove(objectKey);
        } catch (CacheException ex) {
            log.error(cacheMessages.getString("cache.generic.exception"), ex);
        }
    }

    /**
     * Method to validate that the object for the given
     *
     * @param objectKey
     * @return true if the object is already stored on the cache.
     */
    @Override
    public boolean objectExists(Serializable objectKey) {
        log.debug("objectExists(Serializable)");
        return (cacheInternalObject.get(objectKey) != null);
    }

    public void init() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
