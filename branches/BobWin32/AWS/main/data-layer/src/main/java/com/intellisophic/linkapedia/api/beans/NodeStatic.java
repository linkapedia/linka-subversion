package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;
import java.util.Date;

/**
 * Class to handle the static information of a given node identified by it's Id.
 * <p/>
 * @author Xander Kno
 */

@AmazonDynamoDB(tableName = "NodeStatic", hashKey = "ID", hashKeyType = "S")
@DynamoDBTable(tableName = "NodeStatic")
public class NodeStatic implements Serializable {

    private String id;
    private String title;
    private String description;
    private Integer size;
    private String parentID;
    private Integer indexWithinParent;
    private Integer depthFromRoot;
    private String linkNodeID;
    private boolean listNode;
    private Integer type;
    private boolean bridgeNode;
    private boolean modified;
    private Date modificationDate;
    //New Fields
    /**
     * Comma separated list of signatures for this node.
     */
    private String signatures;
    /**
     * Comma separated list of must haves for this node.
     */
    private String musthaves;
    /**
     * Comma separated list of weights for the signatures.
     */
    private String signatureWeights;
    /**
     * Name of the parent of this node. (Parent->Node->Title)
     */
    private String parentName;
    /**
     * Comma separated list of siblings id's.
     */
    private String siblingIDs;
    /**
     * Comma separated list of sibling names.
     */
    private String siblingNames;
    /**
     * Comma separated list of node siblings index within parent.
     */
    private String siblingIndexWithinParent;
    /**
     * Comma separated list of grandparent id's.
     */
    private String grandParentIDs;
    /**
     * Comma separated list of granParent names.
     */
    private String grandParentNames;
    /**
     * Comma separated list of node granParents index within parent.
     */
    private String grandParentIndexWithinParent;
    /**
     * Comma separated list of node children id's.
     */
    private String childrenIDs;
    /**
     * Comma separated list of node children names.
     */
    private String childrenNames;
    /**
     * Comma separated list of node children index within parent.
     */
    private String childrenIndexWithinParent;
    /**
     * Taxonomy ID. (CorpusID)
     */
    private String taxonomyID;
    /**
     * Taxonomy Name.
     */
    private String taxonomyName;
    /**
     * Comma separated list of parent id's
     */
    private String parentIDs;
    /**
     * Comma separated list of node parent index within parent.
     */
    private String parentIndexWithinParent;
    /**
     * Comma separated list of parent names.
     */
    private String parentNames;
    /**
     * Grandparent name
     */
    private String grandParentName;
    /**
     * Grandparent ID
     */
    private String grandParentID;
    /**
     * Field used to store the path from the current node till the root node.
     */
    private String path;

    @DynamoDBAttribute(attributeName = "depthFromRoot")
    public Integer getDepthFromRoot() {
        return depthFromRoot;
    }

    public void setDepthFromRoot(Integer depthFromRoot) {
        this.depthFromRoot = depthFromRoot;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        this.setModified(true);
    }

    @DynamoDBHashKey(attributeName = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "indexWithinParent")
    public Integer getIndexWithinParent() {
        return indexWithinParent;
    }

    public void setIndexWithinParent(Integer indexWithinParent) {
        this.indexWithinParent = indexWithinParent;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "linkNodeID")
    public String getLinkNodeID() {
        return linkNodeID;
    }

    public void setLinkNodeID(String linkNodeID) {
        this.linkNodeID = linkNodeID;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "parentID")
    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "parentName")
    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "size")
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "bridgeNode")
    public boolean isBridgeNode() {
        return bridgeNode;
    }

    public void setBridgeNode(boolean bridgeNode) {
        this.bridgeNode = bridgeNode;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "listNode")
    public boolean isListNode() {
        return listNode;
    }

    public void setListNode(boolean listNode) {
        this.listNode = listNode;
        this.setModified(true);
    }

    @DynamoDBAttribute(attributeName = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
        this.setModified(true);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NodeStatic{" + "title=").append(title);
        sb.append(", description=").append(description);
        sb.append(", parentID=").append(parentID);
        sb.append(", listNode=").append(listNode);
        sb.append(", type=").append(type);
        sb.append(", bridgeNode=").append(bridgeNode);
        if (this.isModified()) {
            sb.append(", isModified=").append("Yes");
        } else {
            sb.append(", isModified=").append("No");
        }
        sb.append('}');
        return sb.toString();
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @DynamoDBIgnore
    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
        this.setModificationDate(new Date());
    }

    @DynamoDBAttribute(attributeName = "childrenIDs")
    public String getChildrenIDs() {
        return childrenIDs;
    }

    public void setChildrenIDs(String childrenIDs) {
        this.childrenIDs = childrenIDs;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "childrenNames")
    public String getChildrenNames() {
        return childrenNames;
    }

    public void setChildrenNames(String childrenNames) {
        this.childrenNames = childrenNames;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "childrenIndexWithinParent")
    public String getChildrenIndexWithinParent() {
        return childrenIndexWithinParent;
    }

    public void setChildrenIndexWithinParent(String childrenIndexWithinParent) {
        this.childrenIndexWithinParent = childrenIndexWithinParent;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "musthaves")
    public String getMusthaves() {
        return musthaves;
    }

    public void setMusthaves(String musthaves) {
        this.musthaves = musthaves;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "siblingIDs")
    public String getSiblingIDs() {
        return siblingIDs;
    }

    public void setSiblingIDs(String siblingIDs) {
        this.siblingIDs = siblingIDs;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "siblingNames")
    public String getSiblingNames() {
        return siblingNames;
    }

    public void setSiblingNames(String siblingNames) {
        this.siblingNames = siblingNames;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "siblingIndexWithinParent")
    public String getSiblingIndexWithinParent() {
        return siblingIndexWithinParent;
    }

    public void setSiblingIndexWithinParent(String siblingIndexWithinParent) {
        this.siblingIndexWithinParent = siblingIndexWithinParent;
    }

    @DynamoDBAttribute(attributeName = "signatureWeights")
    public String getSignatureWeights() {
        return signatureWeights;
    }

    public void setSignatureWeights(String signatureWeights) {
        this.signatureWeights = signatureWeights;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "signatures")
    public String getSignatures() {
        return signatures;
    }

    public void setSignatures(String signatures) {
        this.signatures = signatures;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "taxonomyID")
    public String getTaxonomyID() {
        return taxonomyID;
    }

    public void setTaxonomyID(String taxonomyID) {
        this.taxonomyID = taxonomyID;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "taxonomyName")
    public String getTaxonomyName() {
        return taxonomyName;
    }

    public void setTaxonomyName(String taxonomyName) {
        this.taxonomyName = taxonomyName;
        setModified(true);
    }

    @DynamoDBAttribute(attributeName = "grandParentIDs")
    public String getGrandParentIDs() {
        return grandParentIDs;
    }

    public void setGrandParentIDs(String granParentIDs) {
        this.grandParentIDs = granParentIDs;
    }

    @DynamoDBAttribute(attributeName = "grandParentIndexWithinParent")
    public String getGrandParentIndexWithinParent() {
        return grandParentIndexWithinParent;
    }

    public void setGrandParentIndexWithinParent(String granParentIndexWithinParent) {
        this.grandParentIndexWithinParent = granParentIndexWithinParent;
    }

    @DynamoDBAttribute(attributeName = "grandParentNames")
    public String getGrandParentNames() {
        return grandParentNames;
    }

    public void setGrandParentNames(String granParentNames) {
        this.grandParentNames = granParentNames;
    }

    @DynamoDBAttribute(attributeName = "parentIDs")
    public String getParentIDs() {
        return parentIDs;
    }

    public void setParentIDs(String parentIDs) {
        this.parentIDs = parentIDs;
    }

    @DynamoDBAttribute(attributeName = "parentIndexWithinParent")
    public String getParentIndexWithinParent() {
        return parentIndexWithinParent;
    }

    public void setParentIndexWithinParent(String parentIndexWithinParent) {
        this.parentIndexWithinParent = parentIndexWithinParent;
    }

    @DynamoDBAttribute(attributeName = "parentNames")
    public String getParentNames() {
        return parentNames;
    }

    public void setParentNames(String parentNames) {
        this.parentNames = parentNames;
    }

    @DynamoDBAttribute(attributeName = "grandParentID")
    public String getGrandParentID() {
        return grandParentID;
    }

    public void setGrandParentID(String grandParentID) {
        this.grandParentID = grandParentID;
    }

    @DynamoDBAttribute(attributeName = "grandParentName")
    public String getGrandParentName() {
        return grandParentName;
    }

    public void setGrandParentName(String grandParentName) {
        this.grandParentName = grandParentName;
    }

    @DynamoDBIgnore
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
