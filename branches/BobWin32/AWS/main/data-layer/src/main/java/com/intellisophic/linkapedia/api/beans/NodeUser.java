package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "NodeUser", hashKey = "nodeId", hashKeyType = "S", rangeKey = "userId", rangeKeyType = "S")
@DynamoDBTable(tableName = "NodeUser")
public class NodeUser implements Serializable {

    /**
     * unique/key/nodeId
     */
    private String nodeId;
    private String userId;
    private String userName;
    private String userLastName;
    private String userFbId;
    private Long timeStamp;

    @DynamoDBHashKey(attributeName = "nodeId")
    public String getNodeId() {
        return nodeId;
    }

    @DynamoDBRangeKey(attributeName = "userId")
    public String getUserId() {
        return userId;
    }

    @DynamoDBAttribute(attributeName = "userName")
    public String getUserName() {
        return userName;
    }

    @DynamoDBAttribute(attributeName = "userLastName")
    public String getUserLastName() {
        return userLastName;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public Long getTimeStamp() {
        return timeStamp;
    }

    public String getUserFbId() {
        return userFbId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public void setUserFbId(String userFbId) {
        this.userFbId = userFbId;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
