/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.FollowingInterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollectionElement;
import com.intellisophic.linkapedia.api.beans.NodeInterestCollection;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.NodeUser;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author juanidrobo
 */
public class InterestCollectionElementAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = Logger.getLogger(InterestCollectionElementAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(InterestCollectionElementAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static void addInterestCollection(InterestCollectionElement interestCollectionElement) throws Exception {
        log.debug("addInterestCollection (InterestCollectionElement)");
        try {
            getDynamoDBMapper().save(interestCollectionElement);
            InterestCollection ic = DataHandlerFacade.getInterestCollection(interestCollectionElement.getInterestCollectionId());


            //save NodeInterestCollection
            NodeInterestCollection nodeInterestCollection = new NodeInterestCollection();
            nodeInterestCollection.setInterestCollectionId(ic.getInterestCollectionId());
            nodeInterestCollection.setNodeId(interestCollectionElement.getNodeId());
            float score = 0;
            nodeInterestCollection.setScore(score); //not yet sure what to put in score;
            NodeInterestCollectionAPIHandler.saveNodeInterestCollection(nodeInterestCollection);

            cache.delete(ic.getInterestCollectionId()); //delete from cache to ensure the data consistence


            //register event to the creator of the collection
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(ic.getOwnerId());
            userEvents.registerEvent(interestCollectionElement, UserFeedAPIHandler.ADD_INTEREST_COLLECTION);

            //Register events to the users following the collection
            List<FollowingInterestCollection> followingInterestCollection = DataHandlerFacade.getFollowingInterestCollectionByCollectionId(interestCollectionElement.getInterestCollectionId());
            for (FollowingInterestCollection fic : followingInterestCollection) {
                if (!fic.getUserId().equals(ic.getOwnerId())) {
                    userEvents.setUserToShow(fic.getUserId());
                    userEvents.registerEvent(interestCollectionElement, UserFeedAPIHandler.ADD_INTEREST_COLLECTION);
                }
            }

            //Register events to the users following the topic
            List<NodeUser> nodeUsers = DataHandlerFacade.getNodeUsersByNodeId(interestCollectionElement.getNodeId());
            if (nodeUsers.size() > 0) {
                NodeStatic nodeStatic = DataHandlerFacade.getNode(interestCollectionElement.getNodeId());
                interestCollectionElement.setNodeName(nodeStatic.getTitle());
                for (NodeUser nu : nodeUsers) {
                    if (!nu.getUserId().equals(ic.getOwnerId())) {
                        userEvents.setUserToShow(nu.getUserId());
                        userEvents.registerEvent(interestCollectionElement, UserFeedAPIHandler.ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION);
                    }
                }
            }


        } catch (Exception ex) {
            log.error("Exception in addInterestCollection: " + ex.getMessage());
            throw ex;
        }
    }

    public static List<InterestCollectionElement> getInterestCollectionElement(String interestCollectionId) throws Exception {
        log.debug("getInterestCollectionElement (String)");
        List<InterestCollectionElement> returnList = new ArrayList<InterestCollectionElement>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(interestCollectionId);
            if (cacheObj != null) {
                return (List<InterestCollectionElement>) cacheObj;
            } else {
                PaginatedQueryList<InterestCollectionElement> result = getDynamoDBMapper().query(
                        InterestCollectionElement.class, new DynamoDBQueryExpression(new AttributeValue(interestCollectionId)));

                for (InterestCollectionElement interestCollectionList : result) {
                    if (interestCollectionList != null) {
                        returnList.add(interestCollectionList);
                    }
                }
                cache.set(interestCollectionId, returnList, ONE_HOUR);

            }
        } catch (Exception ex) {
            log.error("Exception in getInterestCollectionElement - interestCollectionId: " + interestCollectionId + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;
    }

    public static void removeInterestCollection(InterestCollectionElement interestCollectionList) throws Exception {
        log.debug("removeInterestCollection (InterestCollectionList)");
        try {
            getDynamoDBMapper().delete(interestCollectionList);

            //remove NodeInterestCollection
            NodeInterestCollection nodeInterestCollection = new NodeInterestCollection();
            nodeInterestCollection.setInterestCollectionId(interestCollectionList.getInterestCollectionId());
            nodeInterestCollection.setNodeId(interestCollectionList.getNodeId());
            float score = 0;
            nodeInterestCollection.setScore(score); //not yet sure what to put in score;
            NodeInterestCollectionAPIHandler.removeNodeInterestCollection(nodeInterestCollection);
            cache.delete(interestCollectionList.getInterestCollectionId()); //delete from cache to ensure the data consistence

        } catch (Exception ex) {
            log.error("Exception in removeInterestCollection: " + ex.getMessage());
            throw ex;
        }
    }
}
