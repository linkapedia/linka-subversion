package com.intellisophic.linkapedia.api.handlers;

import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.FollowedUser;
import com.intellisophic.linkapedia.api.beans.FollowingInterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollectionElement;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author juanidrobo
 */
public class InterestCollectionAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = Logger.getLogger(InterestCollectionAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(InterestCollectionAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static void saveInterestCollection(InterestCollection interestCollection) throws Exception {
        log.debug("saveInterestCollection (InterestCollection)");
        if (interestCollection == null) {
            log.debug("The InterestCollection to be saved can not be null");
            throw new IllegalArgumentException("The InterestCollection to be saved can not be null");
        }
        try {

            getDynamoDBMapper().save(interestCollection);

            cache.set(interestCollection.getInterestCollectionId(), interestCollection, ONE_HOUR);


            //register event to the creator of the collection
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(interestCollection.getOwnerId());
            userEvents.registerEvent(interestCollection, UserFeedAPIHandler.CREATE_COLLECTION);

            if (!interestCollection.getIsPrivate()) {
                //Register event to all users following the creator of collection
                List<FollowedUser> followedUsers = DataHandlerFacade.getFollowerUsersByFollowedUserId(interestCollection.getOwnerId());
                for (FollowedUser fu : followedUsers) {

                    userEvents.setUserToShow(fu.getFollowingUserId());
                    userEvents.registerEvent(interestCollection, UserFeedAPIHandler.CREATE_COLLECTION);

                }
            }

        } catch (Exception ex) {
            log.error("Exception in saveInterestCollection: " + ex.getMessage());
            throw ex;
        }
    }

    public static InterestCollection getInterestCollection(String interestCollectionId) throws Exception {
        return getInterestCollection(interestCollectionId, false);
    }

    public static InterestCollection getInterestCollection(String interestCollectionId, boolean retrieveElements) throws Exception {
        log.debug("getInterestCollection (String)");
        InterestCollection interestCollection = null;
        Object cacheObj = null;
        try {
            cacheObj = cache.get(interestCollectionId);
            if (cacheObj != null) {
                interestCollection = (InterestCollection) cacheObj;
                if (retrieveElements) {
                    interestCollection.setInterestCollectionElements(InterestCollectionElementAPIHandler.getInterestCollectionElement(interestCollectionId));
                }

                return interestCollection;
            } else {
                interestCollection = getDynamoDBMapper().load(InterestCollection.class, interestCollectionId);
                if (retrieveElements) {
                    interestCollection.setInterestCollectionElements(InterestCollectionElementAPIHandler.getInterestCollectionElement(interestCollectionId));
                }
                cache.set(interestCollectionId, interestCollection, ONE_HOUR);
                return interestCollection;
            }
        } catch (Exception ex) {
            log.error("Exception in getInterestCollection: " + ex.getMessage());
            throw ex;
        }

    }

    public static void setPublicInterestCollection(InterestCollection interestCollection) throws Exception {
        log.debug("setPublicInterestCollection (InterestCollection)");
        try {
            interestCollection.setIsPrivate(Boolean.FALSE);
            getDynamoDBMapper().save(interestCollection);

            cache.set(interestCollection.getInterestCollectionId(), interestCollection, ONE_HOUR);

            //Register event to all users following the creator of collection
            UserFeedAPIHandler userEvents = new UserFeedAPIHandler(interestCollection.getOwnerId());
            List<FollowedUser> followedUsers = DataHandlerFacade.getFollowerUsersByFollowedUserId(interestCollection.getOwnerId());
            for (FollowedUser fu : followedUsers) {
                userEvents.setUserToShow(fu.getFollowingUserId());
                userEvents.registerEvent(interestCollection, UserFeedAPIHandler.MAKE_COLLECTION_PUBLIC);
            }

        } catch (Exception ex) {
            log.error("Exception in setPublicInterestCollection: " + ex.getMessage());
            throw ex;
        }


    }

    public static void updateInterestCollection(InterestCollection interestCollection) throws Exception {
        log.debug("updateInterestCollection (InterestCollection)");
        if (interestCollection == null) {
            log.debug("The InterestCollection to be saved can not be null");
            throw new IllegalArgumentException("The InterestCollection to be saved can not be null");
        }
        try {
            getDynamoDBMapper().save(interestCollection);
            cache.set(interestCollection.getInterestCollectionId(), interestCollection, ONE_HOUR);

            //update the interestCollectionFollowed of all the follwers of the collection.
            List<FollowingInterestCollection> followingInterestCollection = DataHandlerFacade.getFollowingInterestCollectionByCollectionId(interestCollection.getInterestCollectionId());
            for (FollowingInterestCollection fic : followingInterestCollection) {
                DataHandlerFacade.updateInterestCollectionFollowed(fic.getUserId());
            }
            //update the interestCollectionFollowed of all the follwers of the user.
            List<FollowedUser> followedUsers = DataHandlerFacade.getFollowerUsersByFollowedUserId(interestCollection.getOwnerId());
            for (FollowedUser followedUser : followedUsers) {
                DataHandlerFacade.updateInterestCollectionFollowed(followedUser.getFollowingUserId());
            }   

            //update the nodeInterestCollection of all the collections related
           for (InterestCollectionElement ice : interestCollection.getInterestCollectionElements())
           {
               DataHandlerFacade.updateNodeInterestCollection(ice.getNodeId());
           }

        } catch (Exception ex) {
            log.error("Exception in updateInterestCollection: " + ex.getMessage());
            throw ex;
        }
    }
}
