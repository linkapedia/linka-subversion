package com.intellisophic.linkapedia.migration.facade;

import com.intellisophic.linkapedia.generic.utils.NumericUtils;
import com.intellisophic.linkapedia.generic.utils.StringUtils;
import com.intellisophic.linkapedia.migration.bo.CorpusBO;
import com.intellisophic.linkapedia.migration.bo.MustHaveBO;
import com.intellisophic.linkapedia.migration.bo.NodeBO;
import com.intellisophic.linkapedia.migration.bo.SignatureBO;
import com.intellisophic.linkapedia.migration.bo.interfaces.ICorpusBO;
import com.intellisophic.linkapedia.migration.bo.interfaces.IMustHaveBO;
import com.intellisophic.linkapedia.migration.bo.interfaces.INodeBO;
import com.intellisophic.linkapedia.migration.bo.interfaces.ISignatureBO;
import com.intellisophic.linkapedia.migration.bo.models.Corpus;
import com.intellisophic.linkapedia.migration.bo.models.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class TaxonomyFacade {

    private static final String AMAZON_FIELD_SEPARATOR = "||";
    private static final Logger log = Logger.getLogger(TaxonomyFacade.class);
    private INodeBO nodeBO;
    private IMustHaveBO mustHaveBO;
    private ISignatureBO signatureBO;
    private ICorpusBO corpusBO;

    public TaxonomyFacade() {
        nodeBO = new NodeBO();
        mustHaveBO = new MustHaveBO();
        signatureBO = new SignatureBO();
        corpusBO = new CorpusBO();
    }

    public Node getNode(Long nodeId) throws Exception {
        log.debug("getNode(Long)");
        return getNodeBO().getNode(nodeId);
    }

    public boolean saveNode(Node node) throws Exception {
        log.debug("saveNode(Node)");
        return getNodeBO().saveNode(node);
    }

    public boolean deleteNode(Node node) throws Exception {
        log.debug("deleteNode(Node)");
        return getNodeBO().deleteNode(node.getId());
    }

    public List<Node> getNodesByNodeId(Long nodeId, boolean isForAmazon, boolean isRecursive) throws Exception {
        log.debug("getNodesByCorpusId(Long)");
        List<Node> completeNodesList = null;
        if (!isForAmazon) {
            return getNodeBO().getNodesByNodeId(nodeId,isRecursive);
        } else {
            List<Node> nodeList = getNodeBO().getNodesByNodeId(nodeId,isRecursive);
            if (nodeList != null && !nodeList.isEmpty()) {
                completeNodesList = new ArrayList<Node>();
                List<Node> childrenList = null;
                List<Node> grandParentList = null;
                List<Node> siblingList = null;
                List<Node> parentList = null;
                Map<String, Long> signatureList = null;
                StringBuilder parentIDs = new StringBuilder();
                StringBuilder parentNames = new StringBuilder();
                StringBuilder parentIndexWithinParent = new StringBuilder();
                StringBuilder childrenIDs = new StringBuilder();
                StringBuilder childrenNames = new StringBuilder();
                StringBuilder childrenIndexWithinParent = new StringBuilder();
                StringBuilder siblingIDs = new StringBuilder();
                StringBuilder siblingNames = new StringBuilder();
                StringBuilder siblingIndexWithinParent = new StringBuilder();
                StringBuilder grandParentIDs = new StringBuilder();
                StringBuilder grandParentNames = new StringBuilder();
                StringBuilder grandParentIndexWithinParent = new StringBuilder();
                StringBuilder signatures = new StringBuilder();
                StringBuilder signatureWeights = new StringBuilder();
                String musthaves = null;
                Corpus corpusInfo = null;
                Node parentNode = null;
                Node granParentNode = null;
                for (Node node : nodeList) {
                    
                   
                    //Reset the strings
                    parentIDs.setLength(0);
                    parentNames.setLength(0);
                    parentIndexWithinParent.setLength(0);
                    childrenIDs.setLength(0);
                    childrenNames.setLength(0);
                    childrenIndexWithinParent.setLength(0);
                    siblingIDs.setLength(0);
                    siblingNames.setLength(0);
                    siblingIndexWithinParent.setLength(0);
                    grandParentIDs.setLength(0);
                    grandParentNames.setLength(0);
                    grandParentIndexWithinParent.setLength(0);
                    signatures.setLength(0);
                    signatureWeights.setLength(0);
                    //Build the path.
                    node.setPath(getPathToRootForNode(node));

                    //get children.
                    childrenList = getNodeBO().getNodesByParentID(node.getId());
                    for (Node child : childrenList) {
                        //set the correct children list. Pointing to NodeSatic Nodes
                        childrenIDs.append(NumericUtils.revertLongAsString(child.getLinkNodeId())).append(AMAZON_FIELD_SEPARATOR);
                        childrenNames.append(child.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                        childrenIndexWithinParent.append(child.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                    }
                    node.getExtraValues().put("childrenIDs", StringUtils.rCharTrim(childrenIDs, AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("childrenNames", StringUtils.rCharTrim(childrenNames, AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("childrenIndexWithinParent", StringUtils.rCharTrim(childrenIndexWithinParent, AMAZON_FIELD_SEPARATOR));

                    //We get the parents of the current node
                    if (!node.getParentId().equals(-1L)) {
                        parentNode = getNodeBO().getNode(node.getParentId());
                        node.getExtraValues().put("parentName", parentNode.getTitle());
                        if (parentNode != null && !parentNode.getParentId().equals(-1L)) {
                            parentList = getNodeBO().getNodesByParentID(parentNode.getParentId());
                            for (Node parent : parentList) {
                                if (!parent.getId().equals(node.getParentId())) {
                                    parentIDs.append(NumericUtils.revertLongAsString(parent.getLinkNodeId())).append(AMAZON_FIELD_SEPARATOR);
                                    parentNames.append(parent.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                                    parentIndexWithinParent.append(parent.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                                }
                            }
                            node.getExtraValues().put("parentIDs", StringUtils.rCharTrim(parentIDs, AMAZON_FIELD_SEPARATOR));
                            node.getExtraValues().put("parentNames", StringUtils.rCharTrim(parentNames, AMAZON_FIELD_SEPARATOR));
                            node.getExtraValues().put("parentIndexWithinParent", StringUtils.rCharTrim(parentIndexWithinParent, AMAZON_FIELD_SEPARATOR));
                        }
                    }

                    //Search for grantparents if no children found
                    if ((childrenList == null || childrenList.isEmpty()) && !node.getParentId().equals(-1L)) {
                        //We get the parent of the current node
                        parentNode = getNodeBO().getNode(node.getParentId());
                        if (parentNode != null && !parentNode.getParentId().equals(-1L)) {
                            //We need to retrieve the granparent of the current node.
                            granParentNode = getNodeBO().getNode(parentNode.getParentId());

                            if (granParentNode != null && granParentNode.getParentId().equals(-1L)) {
                                node.getExtraValues().put("grandParentID", NumericUtils.revertLongAsString(granParentNode.getLinkNodeId()).toString());
                                node.getExtraValues().put("grandParentName", granParentNode.getTitle());
                            } else {
                                if (granParentNode != null && !granParentNode.getParentId().equals(-1L)) {
                                    grandParentList = getNodeBO().getNodesByParentID(granParentNode.getParentId());
                                    for (Node grandParent : grandParentList) {
                                        if (!grandParent.getId().equals(granParentNode.getId())) {
                                            grandParentIDs.append(NumericUtils.revertLongAsString(grandParent.getLinkNodeId())).append(AMAZON_FIELD_SEPARATOR);
                                            grandParentNames.append(grandParent.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                                            grandParentIndexWithinParent.append(grandParent.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                                        } else {
                                            node.getExtraValues().put("grandParentID", NumericUtils.revertLongAsString(grandParent.getLinkNodeId()).toString());
                                            node.getExtraValues().put("grandParentName", grandParent.getTitle());
                                        }
                                    }
                                    node.getExtraValues().put("grandParentIDs", StringUtils.rCharTrim(grandParentIDs, AMAZON_FIELD_SEPARATOR));
                                    node.getExtraValues().put("grandParentNames", StringUtils.rCharTrim(grandParentNames, AMAZON_FIELD_SEPARATOR));
                                    node.getExtraValues().put("grandParentIndexWithinParent", StringUtils.rCharTrim(grandParentIndexWithinParent, AMAZON_FIELD_SEPARATOR));
                                }
                            }
                        }
                    }

                    //get musthaves
                    musthaves = getMustHaveBO().getMustHavesByNodeID(node.getId());
                    node.getExtraValues().put("musthaves", StringUtils.rCharTrim(musthaves, AMAZON_FIELD_SEPARATOR));

                    //get siblings if node id <> -1
                    if (!node.getParentId().equals(-1L)) {
                        siblingList = getNodeBO().getNodesByParentID(node.getParentId());
                        for (Node sibling : siblingList) {
                            if (!sibling.getId().equals(node.getId())) {
                                siblingIDs.append(NumericUtils.revertLongAsString(sibling.getLinkNodeId())).append(AMAZON_FIELD_SEPARATOR);
                                siblingNames.append(sibling.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                                siblingIndexWithinParent.append(sibling.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                            }
                        }
                        node.getExtraValues().put("siblingIDs", StringUtils.rCharTrim(siblingIDs, AMAZON_FIELD_SEPARATOR));
                        node.getExtraValues().put("siblingNames", StringUtils.rCharTrim(siblingNames, AMAZON_FIELD_SEPARATOR));
                        node.getExtraValues().put("siblingIndexWithinParent", StringUtils.rCharTrim(siblingIndexWithinParent, AMAZON_FIELD_SEPARATOR));
                    }

                    //get signatures.
                    signatureList = getSignatureBO().getSignatureByNodeID(node.getId());
                    for (String signature : signatureList.keySet()) {
                        signatures.append(signature).append(AMAZON_FIELD_SEPARATOR);
                        signatureWeights.append(signatureList.get(signature)).append(AMAZON_FIELD_SEPARATOR);
                    }
                    node.getExtraValues().put("signatures", StringUtils.rCharTrim(signatures, AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("signatureWeights", StringUtils.rCharTrim(signatureWeights, AMAZON_FIELD_SEPARATOR));

                    //get Taxonomy Name
                    corpusInfo = getCorpusBO().getCorpus(node.getCorpusId());
                    node.getExtraValues().put("taxonomyID", StringUtils.rCharTrim(String.valueOf(NumericUtils.revertLongAsString(node.getCorpusId())), AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("taxonomyName", StringUtils.rCharTrim(corpusInfo.getName(), AMAZON_FIELD_SEPARATOR));
                    completeNodesList.add(node);
                }
            }
        }
        return completeNodesList;
    }

    public List<Node> getNodesByCorpusId(Long corpusID, boolean isForAmazon) throws Exception {
        log.debug("getNodesByCorpusId(Long)");
        List<Node> completeNodesList = null;
        if (!isForAmazon) {
            return getNodeBO().getNodesByCorpusId(corpusID);
        } else {
            List<Node> nodeList = getNodeBO().getNodesByCorpusId(corpusID);
            if (nodeList != null && !nodeList.isEmpty()) {
                completeNodesList = new ArrayList<Node>();
                List<Node> childrenList = null;
                List<Node> grandParentList = null;
                List<Node> siblingList = null;
                List<Node> parentList = null;
                Map<String, Long> signatureList = null;
                StringBuilder parentIDs = new StringBuilder();
                StringBuilder parentNames = new StringBuilder();
                StringBuilder parentIndexWithinParent = new StringBuilder();
                StringBuilder childrenIDs = new StringBuilder();
                StringBuilder childrenNames = new StringBuilder();
                StringBuilder childrenIndexWithinParent = new StringBuilder();
                StringBuilder siblingIDs = new StringBuilder();
                StringBuilder siblingNames = new StringBuilder();
                StringBuilder siblingIndexWithinParent = new StringBuilder();
                StringBuilder grandParentIDs = new StringBuilder();
                StringBuilder grandParentNames = new StringBuilder();
                StringBuilder grandParentIndexWithinParent = new StringBuilder();
                StringBuilder signatures = new StringBuilder();
                StringBuilder signatureWeights = new StringBuilder();
                String musthaves = null;
                Corpus corpusInfo = null;
                Node parentNode = null;
                Node granParentNode = null;
                for (Node node : nodeList) {
                    //Reset the strings
                    parentIDs.setLength(0);
                    parentNames.setLength(0);
                    parentIndexWithinParent.setLength(0);
                    childrenIDs.setLength(0);
                    childrenNames.setLength(0);
                    childrenIndexWithinParent.setLength(0);
                    siblingIDs.setLength(0);
                    siblingNames.setLength(0);
                    siblingIndexWithinParent.setLength(0);
                    grandParentIDs.setLength(0);
                    grandParentNames.setLength(0);
                    grandParentIndexWithinParent.setLength(0);
                    signatures.setLength(0);
                    signatureWeights.setLength(0);
                    //Build the path.
                    node.setPath(getPathToRootForNode(node));

                    //get children.
                    childrenList = getNodeBO().getNodesByParentID(node.getId());
                    for (Node child : childrenList) {
                        childrenIDs.append(NumericUtils.revertLongAsString(child.getId())).append(AMAZON_FIELD_SEPARATOR);
                        childrenNames.append(child.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                        childrenIndexWithinParent.append(child.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                    }
                    node.getExtraValues().put("childrenIDs", StringUtils.rCharTrim(childrenIDs, AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("childrenNames", StringUtils.rCharTrim(childrenNames, AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("childrenIndexWithinParent", StringUtils.rCharTrim(childrenIndexWithinParent, AMAZON_FIELD_SEPARATOR));

                    //We get the parents of the current node
                    if (!node.getParentId().equals(-1L)) {
                        parentNode = getNodeBO().getNode(node.getParentId());
                        node.getExtraValues().put("parentName", parentNode.getTitle());
                        if (parentNode != null && !parentNode.getParentId().equals(-1L)) {
                            parentList = getNodeBO().getNodesByParentID(parentNode.getParentId());
                            for (Node parent : parentList) {
                                if (!parent.getId().equals(node.getParentId())) {
                                    parentIDs.append(NumericUtils.revertLongAsString(parent.getId())).append(AMAZON_FIELD_SEPARATOR);
                                    parentNames.append(parent.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                                    parentIndexWithinParent.append(parent.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                                }
                            }
                            node.getExtraValues().put("parentIDs", StringUtils.rCharTrim(parentIDs, AMAZON_FIELD_SEPARATOR));
                            node.getExtraValues().put("parentNames", StringUtils.rCharTrim(parentNames, AMAZON_FIELD_SEPARATOR));
                            node.getExtraValues().put("parentIndexWithinParent", StringUtils.rCharTrim(parentIndexWithinParent, AMAZON_FIELD_SEPARATOR));
                        }
                    }

                    //Search for grantparents if no children found
                    if ((childrenList == null || childrenList.isEmpty()) && !node.getParentId().equals(-1L)) {
                        //We get the parent of the current node
                        parentNode = getNodeBO().getNode(node.getParentId());
                        if (parentNode != null && !parentNode.getParentId().equals(-1L)) {
                            //We need to retrieve the granparent of the current node.
                            granParentNode = getNodeBO().getNode(parentNode.getParentId());

                            if (granParentNode != null && granParentNode.getParentId().equals(-1L)) {
                                node.getExtraValues().put("grandParentID", NumericUtils.revertLongAsString(granParentNode.getId()).toString());
                                node.getExtraValues().put("grandParentName", granParentNode.getTitle());
                            } else {
                                if (granParentNode != null && !granParentNode.getParentId().equals(-1L)) {
                                    grandParentList = getNodeBO().getNodesByParentID(granParentNode.getParentId());
                                    for (Node grandParent : grandParentList) {
                                        if (!grandParent.getId().equals(granParentNode.getId())) {
                                            grandParentIDs.append(NumericUtils.revertLongAsString(grandParent.getId())).append(AMAZON_FIELD_SEPARATOR);
                                            grandParentNames.append(grandParent.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                                            grandParentIndexWithinParent.append(grandParent.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                                        } else {
                                            node.getExtraValues().put("grandParentID", NumericUtils.revertLongAsString(grandParent.getId()).toString());
                                            node.getExtraValues().put("grandParentName", grandParent.getTitle());
                                        }
                                    }
                                    node.getExtraValues().put("grandParentIDs", StringUtils.rCharTrim(grandParentIDs, AMAZON_FIELD_SEPARATOR));
                                    node.getExtraValues().put("grandParentNames", StringUtils.rCharTrim(grandParentNames, AMAZON_FIELD_SEPARATOR));
                                    node.getExtraValues().put("grandParentIndexWithinParent", StringUtils.rCharTrim(grandParentIndexWithinParent, AMAZON_FIELD_SEPARATOR));
                                }
                            }
                        }
                    }

                    //get musthaves
                    musthaves = getMustHaveBO().getMustHavesByNodeID(node.getId());
                    node.getExtraValues().put("musthaves", StringUtils.rCharTrim(musthaves, AMAZON_FIELD_SEPARATOR));

                    //get siblings if node id <> -1
                    if (!node.getParentId().equals(-1L)) {
                        siblingList = getNodeBO().getNodesByParentID(node.getParentId());
                        for (Node sibling : siblingList) {
                            if (!sibling.getId().equals(node.getId())) {
                                siblingIDs.append(NumericUtils.revertLongAsString(sibling.getId())).append(AMAZON_FIELD_SEPARATOR);
                                siblingNames.append(sibling.getTitle()).append(AMAZON_FIELD_SEPARATOR);
                                siblingIndexWithinParent.append(sibling.getIndexWithinParent()).append(AMAZON_FIELD_SEPARATOR);
                            }
                        }
                        node.getExtraValues().put("siblingIDs", StringUtils.rCharTrim(siblingIDs, AMAZON_FIELD_SEPARATOR));
                        node.getExtraValues().put("siblingNames", StringUtils.rCharTrim(siblingNames, AMAZON_FIELD_SEPARATOR));
                        node.getExtraValues().put("siblingIndexWithinParent", StringUtils.rCharTrim(siblingIndexWithinParent, AMAZON_FIELD_SEPARATOR));
                    }

                    //get signatures.
                    signatureList = getSignatureBO().getSignatureByNodeID(node.getId());
                    for (String signature : signatureList.keySet()) {
                        signatures.append(signature).append(AMAZON_FIELD_SEPARATOR);
                        signatureWeights.append(signatureList.get(signature)).append(AMAZON_FIELD_SEPARATOR);
                    }
                    node.getExtraValues().put("signatures", StringUtils.rCharTrim(signatures, AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("signatureWeights", StringUtils.rCharTrim(signatureWeights, AMAZON_FIELD_SEPARATOR));

                    //get Taxonomy Name
                    corpusInfo = getCorpusBO().getCorpus(corpusID);
                    node.getExtraValues().put("taxonomyID", StringUtils.rCharTrim(String.valueOf(NumericUtils.revertLongAsString(node.getCorpusId())), AMAZON_FIELD_SEPARATOR));
                    node.getExtraValues().put("taxonomyName", StringUtils.rCharTrim(corpusInfo.getName(), AMAZON_FIELD_SEPARATOR));
                    completeNodesList.add(node);
                }
            }
        }
        return completeNodesList;
    }

    public List<Long> getChildrenByNodeId(Long id) throws Exception {
        log.debug("getChildrenByNodeId(Long)");
        return getNodeBO().getChildrenByNodeId(id);
    }

    public Corpus getCorpus(Long corpusID) throws Exception {
        log.debug("getCorpus(Long)");
        return getCorpusBO().getCorpus(corpusID);
    }

    public List<Corpus> getAllCorpus() throws Exception {
        log.debug("getAllCorpus()");
        return getCorpusBO().getAllCorpus();
    }

    /**
     * Method to get the path {URL} from the given node till the top of the
     * tree.
     *
     * @param currentNode Node to find the path to.
     * @return path separated by "/" from the root to the currentNode. Optimized
     * to use StringBuilder (03/29/2012)
     */
    public String getPathToRootForNode(Node currentNode) {
        log.debug("getPathToRootForNode(Node)");
        StringBuilder strPathToRoot;
        if (currentNode == null) {
            return "";
        }
        strPathToRoot = new StringBuilder();
        try {
            //Make the current node the parent.
            Node parentNode = currentNode;
            Long parentId;

            log.debug("Going to get the hierarchy for node {" + currentNode.getId() + "}");

            strPathToRoot.insert(0, parentNode.getTitle()).insert(0, "/");
            parentId = parentNode.getParentId();
            parentNode = getNodeBO().getNode(parentId);

            while (parentNode != null) {
                strPathToRoot.insert(0, parentNode.getTitle()).insert(0, "/");
                parentId = parentNode.getParentId();
                if (!parentId.equals(-1L)) {
                    parentNode = getNodeBO().getNode(parentId);
                    if (parentNode != null) {
                        log.debug("Parent found: " + parentNode.getId());
                    } else {
                        log.debug("Parent not found: This is the root node");
                    }
                } else {
                    log.debug("Root node reached.");
                    parentNode = null;
                }
            }
        } catch (Exception e) {
            log.error("An exception has ocurred while building the node path.", e);
        }
        return strPathToRoot.toString();
    }

    public IMustHaveBO getMustHaveBO() {
        return mustHaveBO;
    }

    public void setMustHaveBO(IMustHaveBO mustHaveBO) {
        this.mustHaveBO = mustHaveBO;
    }

    public INodeBO getNodeBO() {
        return nodeBO;
    }

    public void setNodeBO(INodeBO nodeBO) {
        this.nodeBO = nodeBO;
    }

    public ISignatureBO getSignatureBO() {
        return signatureBO;
    }

    public void setSignatureBO(ISignatureBO signatureBO) {
        this.signatureBO = signatureBO;
    }

    public ICorpusBO getCorpusBO() {
        return corpusBO;
    }

    public void setCorpusBO(ICorpusBO corpusBO) {
        this.corpusBO = corpusBO;
    }
}
