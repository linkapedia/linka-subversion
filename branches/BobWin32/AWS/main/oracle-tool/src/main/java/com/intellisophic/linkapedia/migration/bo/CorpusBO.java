package com.intellisophic.linkapedia.migration.bo;

import com.intellisophic.linkapedia.migration.bo.interfaces.ICorpusBO;
import com.intellisophic.linkapedia.migration.bo.models.Corpus;
import com.intellisophic.linkapedia.migration.dao.CorpusDAO;
import com.intellisophic.linkapedia.migration.dao.interfaces.ICorpusDAO;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class CorpusBO implements ICorpusBO {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(CorpusBO.class);
    private ICorpusDAO corpusDAO;

    public CorpusBO() {
        corpusDAO = new CorpusDAO();
    }

    public Corpus getCorpus(Long corpusID) throws Exception {
        log.debug("getCorpus(Long)");
        return getCorpusDAO().getCorpus(corpusID);
    }

    public List<Corpus> getAllCorpus() throws Exception {
        log.debug("getAllCorpus()");
        return getCorpusDAO().getAllCorpus();
    }

    public ICorpusDAO getCorpusDAO() {
        return corpusDAO;
    }

    public void setCorpusDAO(ICorpusDAO corpusDAO) {
        this.corpusDAO = corpusDAO;
    }
}
