package com.intellisophic.linkapedia.migration.exec;

import com.intellisophic.linkapedia.migration.services.TaxonomyServices;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * TODO: move this to Amazon tool project
 * @author andres
 */
public class AWSUpdateApp {

    private static final Logger log = Logger.getLogger(AWSUpdateApp.class);

    public static void main(String[] args) {
        //AWS utilities updating
        if (args.length == 0) {
            log.info("Missing parameter: please put the service");
            printHelp();
            return;
        }
        if (args[0].equalsIgnoreCase("-h")) {
            printHelp();
            return;
        }
        if (args[0].equals("cloudsearch-update")) {
            try {
                if (args[1].equalsIgnoreCase("-n")) {
                    cloudSearchUpdate(args[2]);
                } else {
                    log.info("Parameter not found in this service");
                }
            } catch (IndexOutOfBoundsException e) {
                log.info("Sorry! missing parameters for this service", e);
            }
        }
        if (args[0].equals("linknodes-migrate")) {
            String nodeId = null;
            boolean isRecursive = false;
            List<String> arguments = Arrays.asList(args);
            try {
                if (arguments.contains("-n")) {
                    nodeId = args[arguments.indexOf("-n") + 1];
                } else {
                    log.info("Parameter required not found (-n nodeId) in this service");
                    return;
                }
                if (arguments.contains("--r")) {
                    isRecursive = true;
                }

                linkNodesMigrate(nodeId, String.valueOf(isRecursive));



            } catch (IndexOutOfBoundsException e) {
                log.info("Sorry! missing parameters for this service", e);
            }
        }
        log.info("The process is finished!");
    }

    /**
     * cloud search update service
     *
     * @param params
     */
    private static void cloudSearchUpdate(String... params) {
        log.debug("AWSUpdateApp: cloudSearchUpdate");
        log.info("Start to populate the cloud search with dynamoDB info:");
        TaxonomyServices services = new TaxonomyServices();

        try {
            Long nodeId = Long.parseLong(params[0]);
            services.populateCloudSearch(nodeId);
        } catch (NumberFormatException e) {
            log.info("please set a number in a nodeId", e);
        }

    }

    private static void linkNodesMigrate(String... params) {
        TaxonomyServices services = new TaxonomyServices();
        try {
            Long nodeId = Long.parseLong(params[0]);
            Boolean isRecursive = Boolean.parseBoolean(params[1]);
            services.migrateLinkNodesFromOracleToDynamoDB(nodeId, isRecursive);
        } catch (NumberFormatException e) {
            log.info("please set a number in a nodeId", e);
        }
    }

    /**
     * print help
     */
    private static void printHelp() {
        log.info("cloudsearch-update: set one nodeId(oracle) and pupulate the cloud search recursively: require -n nodeId");
        log.info("-h: help");
    }
}
