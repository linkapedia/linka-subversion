package com.intellisophic.linkapedia.migration.dao;

import com.intellisophic.linkapedia.datasources.oracle.OracleTemplate;
import com.intellisophic.linkapedia.migration.dao.interfaces.IMustHaveDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class MustHaveDAO extends OracleTemplate implements IMustHaveDAO {

    private static final Logger log = Logger.getLogger(MustHaveDAO.class);

    public String getMustHavesByNodeID(Long nodeID) throws SQLException, Exception {
        log.debug("getMustHavesByNodeID(Long)");
        StringBuilder musthaves = new StringBuilder();
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("SELECT MUSTWORD FROM MUSTHAVE WHERE NODEID = ?");
            pst.setLong(1, nodeID);
            rs = pst.executeQuery();
            while (rs.next()) {
                musthaves.append(rs.getString("MUSTWORD")).append("||");
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on getMustHavesByNodeID(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getMustHavesByNodeID(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return musthaves.toString();
    }
}
