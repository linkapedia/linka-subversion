package com.intellisophic.linkapedia.migration.bo.models;

import java.util.Date;

/**
 *
 * @author Xander Kno
 */
public class Corpus {

    private Long ID;
    private String name;
    private String description;
    private Float rocf1;
    private Float rocf2;
    private Float rocf3;
    private Float rocc1;
    private Float rocc2;
    private Float rocc3;
    private boolean active;
    private Integer runFrecuency;
    private Date lastRun;
    private Date lastSync;
    private Integer rocSettingID;
    private boolean affinity;
    private String terms;
    //aditional fields.
    //TODO Look for another way to handle this field. This field does not feel natural in this class.
    private Long rootNodeID;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAffinity() {
        return affinity;
    }

    public void setAffinity(boolean affinity) {
        this.affinity = affinity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastRun() {
        return lastRun;
    }

    public void setLastRun(Date lastRun) {
        this.lastRun = lastRun;
    }

    public Date getLastSync() {
        return lastSync;
    }

    public void setLastSync(Date lastSync) {
        this.lastSync = lastSync;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRocSettingID() {
        return rocSettingID;
    }

    public void setRocSettingID(Integer rocSettingID) {
        this.rocSettingID = rocSettingID;
    }

    public Float getRocc1() {
        return rocc1;
    }

    public void setRocc1(Float rocc1) {
        this.rocc1 = rocc1;
    }

    public Float getRocc2() {
        return rocc2;
    }

    public void setRocc2(Float rocc2) {
        this.rocc2 = rocc2;
    }

    public Float getRocc3() {
        return rocc3;
    }

    public void setRocc3(Float rocc3) {
        this.rocc3 = rocc3;
    }

    public Float getRocf1() {
        return rocf1;
    }

    public void setRocf1(Float rocf1) {
        this.rocf1 = rocf1;
    }

    public Float getRocf2() {
        return rocf2;
    }

    public void setRocf2(Float rocf2) {
        this.rocf2 = rocf2;
    }

    public Float getRocf3() {
        return rocf3;
    }

    public void setRocf3(Float rocf3) {
        this.rocf3 = rocf3;
    }

    public Integer getRunFrecuency() {
        return runFrecuency;
    }

    public void setRunFrecuency(Integer runFrecuency) {
        this.runFrecuency = runFrecuency;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public Long getRootNodeID() {
        return rootNodeID;
    }

    public void setRootNodeID(Long rootNodeID) {
        this.rootNodeID = rootNodeID;
    }
}
