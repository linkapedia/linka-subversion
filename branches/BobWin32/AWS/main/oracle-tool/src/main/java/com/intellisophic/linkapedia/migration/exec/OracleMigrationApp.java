package com.intellisophic.linkapedia.migration.exec;

import com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.DynamoDBHelper;
import com.intellisophic.linkapedia.generic.services.cache.CacheHandler;
import com.intellisophic.linkapedia.migration.services.TaxonomyServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OracleMigrationApp {

    private static final Logger log = LoggerFactory.getLogger(OracleMigrationApp.class);

    /**
     * Method to call the execution of the oracle-tool jar Examples
     * <code>
     *  java -jar oracle-tool.jar --migrate --target=[Taxonomy|Node|ALL] --corpusid=<corpusid> [--create-tables=[tablename1,tablename2,...,|All] --drop-tables=[tablename1,tablename2,...,|All]]
     *  java -jar oracle-tool.jar -m -t [Taxonomy|Node|ALL] -c <corpusid> [--create-tables=[tablename1,tablename2,...,|ALL] --drop-tables=[tablename1,tablename2,...,|ALL]]
     * </code>
     * <p/>
     * @param args
     */
    public static void main(String[] args) {
        Long corpusID = null;
        String command = null;
        //Taxonomy or Nodes.
        Target target = Target.UNDEFINED;
        String param = null;
        boolean createTables = false;
        boolean dropTables = false;
        String[] tablesForCreation = null;
        String[] tablesForDeletion = null;

        try {
            if (args != null && args.length > 0) {
                for (int i = 0; i < args.length; i++) {
                    param = args[i];
                    if (param.equals("--migrate")) {
                        command = "migrate";
                    }
                    if (param.startsWith("--target") || param.equals("-t")) {
                        String strTarget = null;
                        if (param.startsWith("--target")) {
                            strTarget = param.substring(param.indexOf("=") + 1, param.length());
                        } else if (param.equals("-t")) {
                            strTarget = args[++i];
                        }
                        if (strTarget.equals("Taxonomy")) {
                            target = Target.TAXONOMY;
                        } else if (strTarget.equals("Node")) {
                            target = Target.NODE;
                        } else if (strTarget.equalsIgnoreCase("all")) {
                            target = Target.ALL;
                        } else {
                            System.err.println("Target Not Specified.");
                            log.error("No target was specified.");
                            System.exit(0);
                        }
                    }
                    if (param.startsWith("--corpusid") || param.equals("-c")) {
                        String strCorpusID = null;
                        if (param.startsWith("--corpusid")) {
                            strCorpusID = param.substring(param.indexOf("=") + 1, param.length());
                        } else if (param.equals("-c")) {
                            strCorpusID = args[++i];
                        }
                        try {
                            corpusID = Long.valueOf(strCorpusID);
                        } catch (NumberFormatException numberFormatException) {
                            log.error("The corpus identifier {" + strCorpusID + "} is not valid");
                            System.out.println("The corpus identifier {" + strCorpusID + "} is not valid");
                            System.exit(0);
                        }
                    }
                    if (param.startsWith("--create-tables")) {
                        //Create the required tables to store the data.
                        if (param.contains("=")) {
                            String tables = param.substring(param.indexOf("=") + 1, param.length());
                            if (tables != null && !tables.isEmpty() && tables.contains(",")) {
                                tablesForCreation = tables.split("\\,");
                            } else if (tables != null && !tables.isEmpty() && !tables.contains(",")) {
                                tablesForCreation = new String[]{tables};
                            } else {
                                log.error("The user didnt specify the tables to be created.");
                                System.err.println("The user didnt specify the tables to be created.");
                                System.exit(0);
                            }
                            createTables = true;
                        } else {
                            log.error("The user didnt specify the tables to be created.");
                            System.err.println("The user didnt specify the tables to be created.");
                            System.exit(0);
                        }

                    }
                    if (param.startsWith("--drop-tables")) {
                        //Drop the tables from DynamoDB.
                        if (param.contains("=")) {
                            String tables = param.substring(param.indexOf("=") + 1, param.length());
                            if (tables != null && !tables.isEmpty() && tables.contains(",")) {
                                tablesForDeletion = tables.split("\\,");
                            } else if (tables != null && !tables.isEmpty() && !tables.contains(",")) {
                                tablesForDeletion = new String[]{tables};
                            } else {
                                log.error("The user didnt specify the tables to be deleted.");
                                System.err.println("The user didnt specify the tables to be deleted.");
                                System.exit(0);
                            }
                            dropTables = true;
                        } else {
                            log.error("The user didnt specify the tables to be deleted.");
                            System.err.println("The user didnt specify the tables to be deleted.");
                            System.exit(0);
                        }
                    }
                }
            }

            if (!target.equals(Target.UNDEFINED) || createTables || dropTables) {
                if (dropTables) {
                    for (String tableName : tablesForDeletion) {
                        if (DynamoDBHelper.tableExist(tableName)) {
                            DynamoDBHelper.deleteTable(tableName);
                        } else {
                            log.debug("The table {" + tableName + "} does not exist on Amazon DynamoDB.");
                            System.out.println("The table {" + tableName + "} does not exist on Amazon DynamoDB.");
                        }
                    }
                }
                if (createTables) {
                    for (String tableName : tablesForCreation) {
                        if (!DynamoDBHelper.tableExist(tableName)) {
                            Class<?> clazz = Class.forName("com.intellisophic.linkapedia.api.beans." + tableName);
                            DynamoDBHelper.createTableFromClass(clazz);
                        } else {
                            log.debug("The table {" + tableName + "} does not exist on Amazon DynamoDB.");
                            System.out.println("The table {" + tableName + "} does not exist on Amazon DynamoDB.");
                        }
                    }
                }
                if (command != null && command.equals("migrate")) {
                    TaxonomyServices services = new TaxonomyServices();
                    if (target.equals(Target.NODE) || target.equals(Target.ALL)) {
                        services.migrateNodesFromOracleToDynamoDB(corpusID);
                    }
                    if (target.equals(Target.TAXONOMY) || target.equals(Target.ALL)) {
                        services.migrateTaxonomyFromOracleToDynamoDB();
                    }
                }
            } else {
                log.error("The command {" + command + "} is not valid");
                System.out.println("The command {" + command + "} is not valid");
            }

            System.out.println("Done...");
        } catch (Exception ex) {
            log.error("An exception has ocurred.", ex);
        } finally {
            CacheHandler.shutdown();
        }
    }

    public enum Target {

        ALL("all"), TAXONOMY("Taxonomy"), NODE("Node"), UNDEFINED("Undefined");
        private String target;

        Target(String target) {
            this.target = target;
        }

        public String getTarget() {
            return this.target;
        }
    }
}
