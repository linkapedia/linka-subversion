package com.intellisophic.linkapedia.migration.bo;

import com.intellisophic.linkapedia.migration.bo.interfaces.IMustHaveBO;
import com.intellisophic.linkapedia.migration.dao.MustHaveDAO;
import com.intellisophic.linkapedia.migration.dao.interfaces.IMustHaveDAO;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class MustHaveBO implements IMustHaveBO {

    private static final Logger log = Logger.getLogger(MustHaveBO.class);
    private IMustHaveDAO mustHaveDAO;

    public MustHaveBO() {
        mustHaveDAO = new MustHaveDAO();
    }

    public String getMustHavesByNodeID(Long nodeID) throws Exception {
        log.debug("getMustHavesByNodeID(Long)");
        String mustHaveList = getMustHaveDAO().getMustHavesByNodeID(nodeID);
        return mustHaveList;
    }

    public IMustHaveDAO getMustHaveDAO() {
        return mustHaveDAO;
    }

    public void setMustHaveDAO(IMustHaveDAO mustHaveDAO) {
        this.mustHaveDAO = mustHaveDAO;
    }
}
