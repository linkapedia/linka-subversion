package com.intellisophic.linkapedia.migration.dao;

import com.intellisophic.linkapedia.datasources.oracle.OracleTemplate;
import com.intellisophic.linkapedia.migration.dao.interfaces.ISignatureDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class SignatureDAO extends OracleTemplate implements ISignatureDAO {

    private static final Logger log = Logger.getLogger(SignatureDAO.class);

    public Map<String, Long> getSignatureByNodeID(Long nodeID) throws SQLException, Exception {
        log.debug("getSignatureByNodeID(Long)");
        Map<String, Long> signatures = new HashMap<String, Long>();
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("SELECT SIGNATUREWORD, SIGNATUREOCCURENCES FROM SIGNATURE WHERE NODEID = ?");
            pst.setLong(1, nodeID);
            rs = pst.executeQuery();
            while (rs.next()) {
                signatures.put(rs.getString("SIGNATUREWORD"), rs.getLong("SIGNATUREOCCURENCES"));
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on getSignatureByNodeID(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getSignatureByNodeID(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return signatures;
    }
}
