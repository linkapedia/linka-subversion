package com.intellisophic.linkapedia.datasources.cloudsearch;

import com.intellisophic.linkapedia.api.beans.CloudSearchInfo;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.generic.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class CloudSearchObjectTransformer {

    private static final Logger log = LoggerFactory.getLogger(CloudSearchObjectTransformer.class);

    public static CloudSearchInfo transform(NodeStatic node) {
        log.debug("transform(NodeStatic)");
        CloudSearchInfo info = new CloudSearchInfo();
        info.setCorpusId(node.getTaxonomyID());
        info.setCorpusName(node.getTaxonomyName());
        info.setNodeDesc(node.getDescription());
        info.setNodeId(StringUtils.revert(node.getId()));
        info.setNodeName(node.getTitle());
        info.setNodePath(node.getPath());
        info.setNodePublic(true);
        return info;
    }
}
