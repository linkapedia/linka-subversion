package com.yuxipacific.zip.toolkit.unzippages;

import com.yuxipacific.zip.toolkit.ZipUtils;
import java.io.File;
import java.util.ResourceBundle;
import org.apache.camel.Exchange;
import org.apache.camel.Main;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.GenericFile;
import org.apache.log4j.Logger;

/**
 * Class that handles the logic to start and stop the UnzipListener process.
 *
 */
public class App {
    
    private static final Logger log = Logger.getLogger(App.class);
    private static final ResourceBundle rb = ResourceBundle.getBundle("system/config");
    private Main main;
    
    public static void main(String[] args) throws Exception {
        App example = new App();
        example.boot();
    }
    
    public void boot() throws Exception {
        log.debug("boot()");
        // create a Main instance
        main = new Main();
        // enable hangup support so you can press ctrl + c to terminate the JVM
        main.enableHangupSupport();
        // add routes
        main.addRouteBuilder(new MyRouteBuilder());

        // run until you terminate the JVM
        System.out.println("Starting Camel. Use ctrl + c to terminate the JVM.\n");
        main.run();
    }
    
    private static class MyRouteBuilder extends RouteBuilder {
        
        @Override
        public void configure() throws Exception {
            from("file:" + rb.getString("builder.ftp.home") + "?delete=true&delay=3600").process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    Object object = exchange.getIn().getBody();
                    if (object instanceof GenericFile) {
                        GenericFile file = (GenericFile) object;
                        System.out.println("Archivo encontrado: " + file.getAbsoluteFilePath());
                        ZipUtils.unZipFile(file.getAbsoluteFilePath(), rb.getString("apache.http.home"));
                        //Delete the zip file from the folder.
                        File zipFile = (File) file.getFile();
                        if(zipFile!=null && zipFile.exists()){
                            zipFile.delete();
                        }
                    }
                }
            }).end();
        }
    }
}
