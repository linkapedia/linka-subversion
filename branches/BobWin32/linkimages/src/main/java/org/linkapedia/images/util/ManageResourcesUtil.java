package org.linkapedia.images.util;

import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * 
 * @author andres
 */
public class ManageResourcesUtil {

	private static final Logger LOG = Logger
			.getLogger(ManageResourcesUtil.class);
	private static Properties prop;

	/**
	 * load the properties file
	 * 
	 * @return
	 */
	public static Properties getProperties() {
		if (prop == null) {
			prop = new Properties();
			try {
				prop.load(ManageResourcesUtil.class.getClassLoader()
						.getResourceAsStream("system_linkimages/config.properties"));
			} catch (Exception ex) {
				LOG.error("Error creating prop object " + ex.getMessage());
			}
		}
		return prop;
	}
}