package org.linkapedia.images.beans;

import java.awt.image.BufferedImage;
import java.io.Serializable;

import org.apache.log4j.Logger;

/**
 * 
 * @author andres this bean is to transport the images in the app
 */
public class ImageInfo implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5449131332523368691L;

	private static final Logger LOG = Logger.getLogger(ImageInfo.class);

	private String id;
	private BufferedImage image; // remove this in future revision

	private int height;
	private int width;
	private String type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ImageInfo clone() {
		ImageInfo clone = null;
		try {
			clone = (ImageInfo) super.clone();
		} catch (CloneNotSupportedException e) {
			LOG.error("Not close support");
		}
		return clone;
	}
}
