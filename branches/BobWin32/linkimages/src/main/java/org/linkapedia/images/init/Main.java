package org.linkapedia.images.init;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.linkapedia.images.io.UtilIO;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.bo.BuilderImage;
import org.linkapedia.images.bo.ImageSelection;
import org.linkapedia.images.filters.FilterImages;
import org.linkapedia.images.filters.FilterResources;
import org.linkapedia.images.filters.config.DefaultFilterImages;
import org.linkapedia.images.filters.config.DefaultFilterResources;
import org.linkapedia.images.resources.ResourceHtml;
import org.linkapedia.images.wrapper.PackImages;

/**
 * Test
 * 
 */
public class Main {
	private static final Logger log = Logger.getLogger(Main.class);

	public static void main(String[] args) throws IOException {
		URL url = new URL("http://us.battle.net/wow/en/");
		PackImages list = null;
		ImageInfo ii = null;
		FilterImages filter = new DefaultFilterImages();
		FilterResources pfWeb = new DefaultFilterResources();

		try {
			list = ResourceHtml.getHtmlImages(url, pfWeb);
		} catch (Exception e1) {
			log.error("Error getting images from url: " + url.toString()+ e1.getMessage());
		}

		try {
			UtilIO.writeImages(list, new File("/home/andres/testedImages/"),
					true, "jpg", new Float(0.25));
		} catch (Exception e) {
			log.error("Error wtiting images on disk"+ e.getMessage());
		}

		if (list == null || list.isEmpty()) {
			log.info("Not found good images");
		} else {
			ii = list.get(0);
			BuilderImage builder = new BuilderImage(ii.getImage(),
					BuilderImage.SITE_IMAGE);
			BufferedImage image = builder.build();
			
			BuilderImage builderDigest = new BuilderImage(ii.getImage(), BuilderImage.DIGEST_IMAGE);
			BufferedImage imageDigest = builderDigest.build();
			
			
			ii.setImage(image);
			try {
				UtilIO.writeImages(ii, new File(
						"/home/andres/testedImages/better"), true, "jpg",
						new Float(0.25));
			} catch (Exception e) {
				log.error("Error wtiting better image on disk"+ e.getMessage());
			}
			
			ii.setImage(imageDigest);
			try {
				UtilIO.writeImages(ii, new File(
						"/home/andres/testedImages/bettera"), true, "jpg",
						new Float(0.25));
			} catch (Exception e) {
				log.error("Error wtiting better image on disk"+ e);
			}
		}

		/*
		 * try { ii = ImageSelection.getBetterImageFilter(list, filter); } catch
		 * (Exception e) { log.error("Error getting better image"); }
		 * 
		 * if (ii != null) { BuilderImage builder = new BuilderImage(); ii =
		 * builder.buildSiteImage(ii); try { UtilIO.writeImages(ii, new File(
		 * "/home/andres/testedImages/better"), true, "jpg", new Float(0.25)); }
		 * catch (Exception e) { log.error("Error wtiting better image on disk",
		 * e); } }
		 */

	}

}