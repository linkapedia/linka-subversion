package org.linkapedia.images.bo;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.linkapedia.images.util.ImageCreation;
import org.linkapedia.images.util.ManageResourcesUtil;

import com.sun.image.codec.jpeg.ImageFormatException;

public class BuilderImage {

	private static final Logger log = Logger.getLogger(BuilderImage.class);
	private static final int DEFAULT_BOXH = 100;
	private static final int DEFAULT_BOXW = 100;

	public static final int SITE_IMAGE = 1;
	public static final int DIGEST_IMAGE = 2;

	private BufferedImage image;
	private int type;

	public BuilderImage(BufferedImage image, int type) {
		this.image = image;
		this.type = type;
	}

	public BufferedImage build() {
		if (!validate()) {
			return null;
		}
		int boxh;
		int boxw;
		boxh = getBoxH();
		boxw = getBoxW();
		return build(boxw, boxh);
	}

	public BufferedImage build(int boxw, int boxh) {
		if (!validate()) {
			return null;
		}
		if (this.type == SITE_IMAGE) {
			return buildSiteImage(boxw, boxh);
		} else if (this.type == DIGEST_IMAGE) {
			return buildDigestImage(boxw, boxh);
		} else {
			log.error("Type unsupported");
			return null;
		}
	}

	private BufferedImage buildSiteImage(int boxw, int boxh) {
		log.debug("BuilderImage: buildSiteImage(int,int)");

		BufferedImage background = ImageCreation.scaleImage(image, boxh, boxw,
				false);
		background = ImageCreation.createLayerTransparent(background);

		BufferedImage mini = null;
		if ((this.image.getHeight() <= boxh) && (this.image.getWidth() <= boxw)) {
			mini = this.image;
		} else {
			mini = ImageCreation.scaleImage(image, boxh, boxw, true);
		}
		background = ImageCreation.createCompositeImage(background, mini,
				mini.getHeight(), mini.getWidth(),
				(boxw - mini.getWidth()) / 2, (boxh - mini.getHeight()) / 2);
		try {
			background = ImageCreation
					.convertToJPG(background, new Float(0.25));
		} catch (ImageFormatException e) {
			log.error("Error building jpg image: ", e);
		} catch (IOException e) {
			log.error("Error building jpg image: ", e);
		}
		return background;
	}

	public BufferedImage buildDigestImage(int boxw, int boxh) {
		log.debug("BuilderImage: buildDigestImage(int,int)");
		BufferedImage dest = this.image;

		int width = dest.getWidth();
		int height = dest.getHeight();

		if (width > boxw && height > boxh) {
			// crop middle
			dest = dest.getSubimage((width - boxw) / 2, (height - boxh) / 2,
					boxw, boxh);
		} else {
			return null;
		}
		return dest;

	}

	/**
	 * utilities
	 */

	private Integer getBoxH() {
		Integer boxh = DEFAULT_BOXH;
		try {
			if (this.type == SITE_IMAGE) {
				boxh = Integer.parseInt(ManageResourcesUtil.getProperties()
						.getProperty("org.linkapedia.images.siteimage.boxh",
								String.valueOf(DEFAULT_BOXH)));
			} else if (this.type == DIGEST_IMAGE) {
				boxh = Integer.parseInt(ManageResourcesUtil.getProperties()
						.getProperty("org.linkapedia.images.digestimage.boxh",
								String.valueOf(DEFAULT_BOXH)));
			} else {
				log.error("Type unsupported");
			}
		} catch (NumberFormatException e) {
			log.error("Error getting parameter BOXH from properties ", e);
		}
		return boxh;
	}

	private int getBoxW() {
		Integer boxw = DEFAULT_BOXW;
		try {
			if (this.type == SITE_IMAGE) {
				boxw = Integer.parseInt(ManageResourcesUtil.getProperties()
						.getProperty("org.linkapedia.images.siteimage.boxw",
								String.valueOf(DEFAULT_BOXW)));
			} else if (this.type == DIGEST_IMAGE) {
				boxw = Integer.parseInt(ManageResourcesUtil.getProperties()
						.getProperty("org.linkapedia.images.digestimage.boxw",
								String.valueOf(DEFAULT_BOXW)));
			} else {
				log.error("Type unsupported");
			}
		} catch (NumberFormatException e) {
			log.error("Error getting parameter BOXH from properties ", e);
		}
		return boxw;
	}

	private boolean validate() {
		if (this.image == null) {
			log.error("Please set image, can't be null");
			return false;
		}
		return true;
	}
}