package org.linkapedia.images.filters.config;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.linkapedia.images.filters.FilterResources;
import org.linkapedia.images.util.ManageResourcesUtil;

public class DefaultFilterResources extends FilterResources {
	private static final Logger log = Logger
			.getLogger(DefaultFilterResources.class);

	public DefaultFilterResources() {
		Properties prop = ManageResourcesUtil.getProperties();
		if (prop == null) {
			log.error("Error creating DefaultFilterResources, properties not load");
			return;
		}
		// set the parameters with the properties
		int num;
		List<String> wordsIn;
		List<String> wordsOut;
		try {
			num = Integer.parseInt(prop
					.getProperty("org.linkapedia.images.default.maxnumber"));
			setNum(num);
		} catch (NumberFormatException e) {
			log.error("Error setting DefaultFilterResources", e);
		}catch(Exception e){
			log.error("Error not found the property");
		}
		String wordsInTemp = prop
				.getProperty("org.linkapedia.images.default.words.in");
		String wordsOutTemp = prop
				.getProperty("org.linkapedia.images.default.words.out");
		wordsIn = Arrays.asList(wordsInTemp.split(","));
		wordsOut = Arrays.asList(wordsOutTemp.split(","));
		setWordsIn(wordsIn);
		setWordsOut(wordsOut);
	}
}