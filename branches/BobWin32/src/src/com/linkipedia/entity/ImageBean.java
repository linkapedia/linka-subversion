package com.linkipedia.entity;

public class ImageBean {
	
	private int nodeId;
	private String imageLink1;
	private String imageLink2;
	
	

	public ImageBean(int nodeId, String imageLink1, String imageLink2) {
		this.nodeId = nodeId;
		this.imageLink1 = imageLink1;
		this.imageLink2 = imageLink2;
	}
	public String getImageLink1() {
		return imageLink1;
	}
	public void setImageLink1(String imageLink1) {
		this.imageLink1 = imageLink1;
	}
	public String getImageLink2() {
		return imageLink2;
	}
	public void setImageLink2(String imageLink2) {
		this.imageLink2 = imageLink2;
	}
	public ImageBean() {

	}
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	
	
	

}
