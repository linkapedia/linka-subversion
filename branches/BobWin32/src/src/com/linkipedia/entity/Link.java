package com.linkipedia.entity;

import java.util.ArrayList;
import java.util.List;

import com.linkipedia.dao.LinkDao;

public class Link {

	static LinkDao linkDao = new LinkDao();

	private int nodeid;
	private String nodetitle;
	private List<Link> children = new ArrayList<Link>();
	private Link parent;

	private int depthfromroot;
	private Boolean containCoffee;

	public Boolean isContainCoffee() {
		return containCoffee;
	}

	public void setContainCoffee(boolean containCoffee) {
		this.containCoffee = containCoffee;
	}

	private List<Link> getLinkPath(Link link) {
		List<Link> path;
		if (link.getParent() == null) {
			path = new ArrayList<Link>();
		} else {
			path = getLinkPath(link.getParent());
		}
		path.add(link);
		return path;
	}

	public List<Link> getPath() {
		return getLinkPath(this);
	}

	public String getLink() {
		return nodeid + ".html";
	}

	public List<Link> getChildren() {
		return children;
	}

	public void setChildren(List<Link> children) {
		this.children = children;
	}

	public Link(int nodeid, String nodetitle, int depthfromroot) {
		super();
		this.nodeid = nodeid;
		this.nodetitle = nodetitle;
		this.depthfromroot=depthfromroot;
	}

	public Link(int nodeid, String nodetitle, List<Link> children, String link) {
		super();
		this.nodeid = nodeid;
		this.nodetitle = nodetitle;
		this.children = children;

	}

	public int getNodeid() {
		return nodeid;
	}

	public void setNodeid(int nodeid) {
		this.nodeid = nodeid;
	}

	public String getNodetitle() {
		return nodetitle;
	}

	public void setNodetitle(String nodetitle) {
		this.nodetitle = nodetitle;
	}

	/**
	 * @return the parent
	 */
	public Link getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(Link parent) {
		this.parent = parent;
	}

	public static List<Link> getLinksGraph() {

		List<Link> grapgh = linkDao.getLev1Links();
		for (Link link : grapgh) {
			link.addChildren();
			
			//Ticket #20 Ensure results contain the word "coffee"
			linkDao.wipeOutLinks(link);
		}
		//Ticket #20 Ensure results contain the word "coffee"
		linkDao.removeCurrentLinks(grapgh);
		return grapgh;
	}
	
	private void addChildren() {
		children = linkDao.getChildren(this);
		if (children != null)
			for (Link link : children) {
				link.setParent(this);
				link.addChildren();
			}
	}

	/**
	 * @return the depthfromroot
	 */
	public int getDepthfromroot() {
		return depthfromroot;
	}

	/**
	 * @param depthfromroot the depthfromroot to set
	 */
	public void setDepthfromroot(int depthfromroot) {
		this.depthfromroot = depthfromroot;
	}

}
