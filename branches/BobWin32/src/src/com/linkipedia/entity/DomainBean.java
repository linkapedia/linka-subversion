package com.linkipedia.entity;

import java.util.List;

import com.linkipedia.dao.SearchDocumentDao;

public class DomainBean {
    
    private int domain_id;
    private String domain_name;
    static SearchDocumentDao searchDao=new SearchDocumentDao();
    
    public DomainBean(int domain_id, String domain_name) {
        super();
        this.domain_id = domain_id;
        this.domain_name = domain_name;
    }
     
    public int getDomain_id() {
        return domain_id;
    }
    public void setDomain_id(int domain_id) {
        this.domain_id = domain_id;
    }
    public String getDomain_name() {
        return domain_name;
    }
    public void setDomain_name(String domain_name) {
        this.domain_name = domain_name;
    }

	public static List<DomainBean> getDomains() {
		
		return searchDao.getDomains();
	}
    
	public static List<DomainBean> getDomainsWoDotNet() {
		
		return searchDao.getDomainsWoDotNet();
	}
    

}
