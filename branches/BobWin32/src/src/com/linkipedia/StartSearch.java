package com.linkipedia;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.linkipedia.crawl.ClientBase;
import com.linkipedia.crawl.ClientBaseImpl;
import com.linkipedia.crawl.UrlCrawlResult;
import com.linkipedia.dao.SearchDocumentDao;
import com.linkipedia.entity.Concepts;
import com.linkipedia.entity.GoogleCrawlResult;

public class StartSearch {

	/**
	 * @param args
	 */
	private static Properties props = null;
	private static Logger logger = Logger.getLogger(StartSearch.class);
	private static int produceTaskSleepTime = 2;
	private static int consuskSleepTime = 2000;
	private static int produceTaskMaxNumber = 10;

	public static void main(String[] args) {
		
//		ThreadPoolExecutor threadPool = new ThreadPoolExecutor(30, 40, 30,
//				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(3),
//				new ThreadPoolExecutor.DiscardOldestPolicy());
		
		ExecutorService threadPool = Executors.newFixedThreadPool(100); 
		
		SearchDocumentDao dao = new SearchDocumentDao();
		List<Concepts> list = dao.getAllConcepts();
		//for (Concepts concept : list) {
			List<GoogleCrawlResult> googleCrawlResultList = dao.getNoscoreGoogleCrawlResults();
			System.out.println(googleCrawlResultList.size());
			if (googleCrawlResultList != null && googleCrawlResultList.size() > 0){
				for (int i = 0; i < googleCrawlResultList.size(); i++) {
					String url = googleCrawlResultList.get(i).getUrl();
					int nodeId = googleCrawlResultList.get(i).getNodeid();
					if (url != null && url.length() > 0 && !url.toLowerCase().endsWith(".pdf")) {
						UrlCrawlResult ucr = new UrlCrawlResult(nodeId,url, dao);
						threadPool.execute(ucr);
					}
				}
			}
		//}
		
		threadPool.shutdown();
	
	}
	
	public void runCrawl(){
		ExecutorService executor = Executors.newFixedThreadPool(100); 
		
	}
	
}
