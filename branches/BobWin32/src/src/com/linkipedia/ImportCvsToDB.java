package com.linkipedia;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.csvreader.CsvReader;
import com.linkipedia.dao.Dao;

/**
 * @author Administrator
 * @version 1.0
 */
public class ImportCvsToDB extends Dao {

	private static Logger logger = Logger.getLogger(ImportCvsToDB.class);

	/**
	 * Close the Connection
	 * 
	 * @param Connection
	 */

	public boolean batchInsertResult(String fileName, String tableName) {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean success = false;
		List<String[]> list = readFileByLines(fileName);
		String[] pars = list.get(0);
		int length = pars.length;
		String pars_ = "";
		String parements_ = "";

		for (int i = 0; i < length; i++) {
			String a = pars[i].replace("\"", "");
			if (i != pars.length) {
				pars_ = pars_ + a + ",";
			}
		}
		int l = pars_.length();
		String parements = pars_.substring(0, l - 1);

		for (int j = 0; j < length; ++j) {
			parements_ = parements_ + ",?";
		}
		parements_ = parements_.substring(1);

		try {
			connection = getConn();
			connection.setAutoCommit(false);

			ps = connection.prepareStatement("INSERT INTO " + tableName + "("
					+ parements + ") values(" + parements_ + ");");
			System.out.println("INSERT INTO " + tableName + "(" + parements
					+ ") values(" + parements_ + ")");
			for (int k = 1; k < list.size(); k++) {
				String[] a = list.get(k);
				int length_ = a.length;
				if (length_ == length) {
					for (int i = 0; i < length_; i++) {
						String temp = a[i].replace("\"", "");
						ps.setObject(i + 1, temp);
						System.out.println(i + 1 + " " + " " + temp);
					}
					ps.addBatch();
				} else {
					System.out.println("Have some unvalid symbol,skip this line!");
				}

			}
			ps.executeBatch();
			connection.commit();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			close(rs, ps, connection);
		}

		return success;
	}

	public List<String[]> readFileByLines(String csvFilePath) {
		List<String[]> csvList = new ArrayList<String[]>();
		try {
			CsvReader reader = new CsvReader(csvFilePath, ',', Charset
					.forName("SJIS"));
			// reader.readHeaders(); // 跳过表头 如果需要表头的话，不要写这句。
			while (reader.readRecord()) { 
				csvList.add(reader.getValues());
			}
			reader.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return csvList;
	}

	public static void main(String[] args) throws Exception {
		// 18496330,18496569,""Giunta,
		// Hertha""," ",2670,"Brands and Companies (Coffee)"
		// String fileName = args[0];
		// String tableName = args[1];
		// System.out.println(fileName + tableName);
		ImportCvsToDB d = new ImportCvsToDB();

		// boolean b = d.batchInsertResult(fileName,tableName);
		// System.out.println(b);
		// d.batchInsertResult("F:/task/ASTD/Linkipedia/svcFilemodify/Brands_and_Companies__Coffee__Topics___Source.csv",
		// "t_source");
		//d.batchInsertResult("F:/task/ASTD/Linkipedia/temp/Brands_and_Companies__Coffee__Topics___Metadata.csv",
		//				"t_metadata");
		//d.readFileByLines("F:/task/ASTD/Linkipedia/Brands_and_Companies__Coffee__Topics___Source.csv");
		
		/**
		 * import another csv:CORPUSID = 2671
		 */
		//d.batchInsertResult("C:/Documents and Settings/Administrator/桌面/LinkPediaToday/Global_Industries__Coffee__Topic___Metadata.csv","t_metadata");
		//d.batchInsertResult("C:/Documents and Settings/Administrator/桌面/LinkPediaToday/Global_Industries__Coffee__Topics___Must_Haves.csv","t_musthave");
		//d.batchInsertResult("C:/Documents and Settings/Administrator/桌面/LinkPediaToday/Global_Industries__Coffee__Topics___Signatures.csv","t_signatures");
		//d.batchInsertResult("C:/Documents and Settings/Administrator/桌面/LinkPediaToday/Global_Industries__Coffee__Topics___Source.csv","t_source");
		/**
		 * import another csv:CORPUSID = 2669
		 */
		//d.batchInsertResult("C:/Documents and Settings/Administrator/桌面/LinkPediaToday/Coffee Taxonomy Topic & Metadata.csv","t_metadata");
		//d.batchInsertResult("C:/Documents and Settings/Administrator/桌面/LinkPediaToday/Coffee Taxonomy Topics & Must Haves.csv","t_musthave");
		d.batchInsertResult("C:/Documents and Settings/Administrator/桌面/LinkPediaToday/Coffee Taxonomy Topics & Signatures.csv","t_signatures");
	}
}
