package com.linkipedia;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.linkipedia.constant.Constants;
import com.linkipedia.dao.LinkDao;
import com.linkipedia.dao.SearchDocumentDao;
import com.linkipedia.entity.Concepts;
import com.linkipedia.entity.DomainBean;
import com.linkipedia.entity.GoogleCrawlResult;
import com.linkipedia.entity.Link;
import com.linkipedia.entity.Signatures;
import com.linkipedia.util.PropertiestUtils;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class StartFreeMarker

{

	private static freemarker.template.Configuration cfg;
	private static Logger logger = Logger.getLogger(StartFreeMarker.class);
	private static Properties props = null;
	private static String filePath;

	private void init() throws Exception {
		try {
			props = PropertiestUtils.loadConfigFile();
			cfg = new freemarker.template.Configuration();
			// 设置FreeMarker的模版文件位置

			cfg.setDirectoryForTemplateLoading(new File(props
					.getProperty(Constants.FREEMARKER_TEMPLATE)));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			filePath = props.getProperty(Constants.FREEMARKER_GENERATEFILEPATH);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void process_resultpages(Map globalRoot, List<Link> links,
			Template t) throws Exception {

		for (Link link : links) {
			FileOutputStream fos = new FileOutputStream(filePath
					+ String.valueOf(link.getNodeid()) + ".html");
			System.out.println("Creating " + link.getLink());

			Writer out = new OutputStreamWriter(fos, "UTF-8");

			globalRoot.put("link", link);
			globalRoot.put("title", "Linkipedia " + link.getNodetitle());
			globalRoot.put("header", link.getNodetitle());
			globalRoot.put("keywords", link.getNodetitle());
			Concepts concept = new Concepts(link.getNodeid());
			// description
			List<Signatures> signatures = concept.getSignatures();
			StringBuilder sb = new StringBuilder();
			for (Signatures sig : signatures) {
				sb.append(sig.getNodeTitle());
				sb.append(",");
			}
			String description = "";
			if (sb.length() > 1)
				description = sb.substring(1, sb.length() - 1);
			globalRoot.put("description", description);
			// search results
			Map<String, List<GoogleCrawlResult>> results = concept.getResults();
			globalRoot.put("results", results);

			t.process(globalRoot, out);
			if (link.getChildren() != null && link.getChildren().size() > 0)
				process_resultpages(globalRoot, link.getChildren(), t);
		}

	}

	private void process_homepage(Map globalRoot, List<Link> links, Template t)
			throws Exception {

		FileOutputStream fos = new FileOutputStream(filePath + "index.html");
		Writer out = new OutputStreamWriter(fos, "UTF-8");

		if (logger.isInfoEnabled())
			logger.info("Creating Homepage.");

		globalRoot.put("title", "Linkipedia Home");
		globalRoot.put("header", "Linkipedia Home");
		globalRoot.put("keywords", "Linkipedia");
		globalRoot.put("description", "Linkipedia Home");

		t.process(globalRoot, out);

		if (logger.isInfoEnabled())
			logger.info("Homepage created.");
	}

	public void process() throws Exception, IOException {
		if (logger.isInfoEnabled())
			logger.info("Process will generate html files!");


		init();
		Template t = cfg
				.getTemplate(props.getProperty(Constants.HTML_TEMPLATE));

		Template leftTemplate = cfg.getTemplate(props
				.getProperty(Constants.HTML_LEFT_TEMPLATE));

		Map globalRoot = new HashMap();
		List<Link> links = Link.getLinksGraph();
		globalRoot.put("links", links);

		 processLeft(globalRoot, links, leftTemplate);
		 String left = getLeft();
		 globalRoot.put("left_content", left);


		List<DomainBean> domains = DomainBean.getDomains();
		globalRoot.put("domainbeanlist", domains);

		globalRoot.put("homepage", "true");
		process_homepage(globalRoot, links, t);

//		globalRoot.put("homepage", "false");
//		hf.process_resultpages(globalRoot, links, t);
		if (logger.isInfoEnabled())
			logger.info("Generate html files done!");
	}

	public static void main(String[] args) throws Exception {
		new StartFreeMarker().process();

		 
	}

	private void processLeft(Map globalRoot, List<Link> links, Template t)
			throws TemplateException, IOException {
		FileOutputStream fos = new FileOutputStream(filePath + "left.html");
		System.out.println("Creating left content.");

		Writer out = new OutputStreamWriter(fos, "UTF-8");
		t.process(globalRoot, out);

	}

	private String getLeft() throws IOException {
		FileReader f = new FileReader(filePath + "left.html");
		BufferedReader r = new BufferedReader(f);
		StringBuilder sb = new StringBuilder();
		String temp;
		while ((temp = r.readLine()) != null) {
			sb.append(temp);
		}
		return sb.toString();
	}




}
