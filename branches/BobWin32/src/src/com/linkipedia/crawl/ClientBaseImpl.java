package com.linkipedia.crawl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class ClientBaseImpl implements ClientBase {

	private static Logger logger = Logger.getLogger(ClientBaseImpl.class);

	private String doRequest(String url, String body) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httpreq = new HttpPost(url);
		HttpEntity requestEntity = null;
		String s = null;
		try {
			requestEntity = new StringEntity(body);
		} catch (UnsupportedEncodingException e) {
			// TODO
			e.printStackTrace();
		}
		httpreq.setEntity(requestEntity);
		httpreq.setHeader("Content-Type", "application/xml");
		try {
			HttpResponse response = httpclient.execute(httpreq);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				s = EntityUtils.toString(entity);
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}

	private String doRequest(String url) throws ClientProtocolException, IOException {
		String s = null;
		
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpreq = new HttpGet(url);

			httpreq.setHeader("Content-Type", "application/xml");
			// httpreq.setHeader("Keep-Alive", "300");
			// httpreq.setHeader("Connection", "keep-alive");
//			 httpreq.setHeader("User-Agent",
//			 "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10 ( .NET CLR 3.5.30729)");

			HttpResponse response = httpclient.execute(httpreq);
			HttpEntity entity = response.getEntity();
			if (entity != null) 
				s = EntityUtils.toString(entity);
			
		
		
		return s;
	}

//	private String generateUrlforSearch(String keyword, int numberPerPage,
//			String siteDomian) {
//		// FIXME,the url is http://www.google.com.hk domain
//		return "http://www.google.com.hk/search?as_q="
//				+ keyword
//				+ "&hl=en&newwindow=1&num="
//				+ numberPerPage
//				+ "&btnG=Google+%E6%90%9C%E7%B4%A2&as_epq=&as_oq=&as_eq=&lr=lang_en&cr=&as_ft=i&as_filetype=&as_qdr=all&as_occt=any&as_dt=i&as_sitesearch="
//				+ siteDomian + "&as_rights=";
//	}

	private String generateUrlforSearch(String keyword, int numberPerPage,
			String siteDomian, int start) {
		return "http://www.google.com/search?q="
		+ keyword.trim()
		+ "+site:"
		+ siteDomian.trim()
		+ "&num="
		+ numberPerPage
		+ "&hl=en&lr=lang_en&newwindow=1&as_qdr=all&tbs=lr:lang_1en&prmd=lifdmvs&ei=faKiTKDeDIHSsAPYn8X6Bg&start="
		+ start + "&sa=N";
		//+ start + "&as_ft=e&as_filetype=pdf&sa=N";

	}

//	private String generateUrlforVedioSearch(String keyword, int numberPerPage,
//			String siteDomian) {
//		// http://www.google.com.hk/search?hl=zh-CN&q=coffee+site%3Ayoutube.com&tbs=vid%3A1&tbo=p&source=vgc&num=5
//		// TODO Auto-generated method stub
//		return "http://www.google.com.hk/search?hl=zh-CN&q=" + keyword
//				+ "+site%3A" + siteDomian
//				+ "&tbs=vid%3A1&tbo=p&source=vgc&num=" + numberPerPage;
//	}

//	private String generateUrlforVedioSearch(String keyword, int numberPerPage,
//			String siteDomian, int start) {
//		// http://www.google.com.hk/search?hl=zh-CN&q=coffee+site%3Ayoutube.com&tbs=vid%3A1&tbo=p&source=vgc&num=5
//		return "http://www.google.com/search?q="
//				+ keyword
//				+ "site:"
//				+ siteDomian
//				+ "&num="
//				+ numberPerPage
//				+ "&start="
//				+ start + "&sa=N";
//	}
	
	private String generateUrlforVedioSearch(String keyword, int numberPerPage,
			String siteDomian, int start) {
		// http://www.google.com.hk/search?hl=zh-CN&q=coffee+site%3Ayoutube.com&tbs=vid%3A1&tbo=p&source=vgc&num=5
		return "http://www.google.com/search?q="
		+ keyword
		+ "+site:"
		+ siteDomian
		+ "&num="
		+ numberPerPage
		+ "&hl=en&lr=lang_en&newwindow=1&tbs=lr:lang_1en,vid:1&ei=nKSiTO7fKoSCsQOh2sH6Bg&start="
		+ start + "&sa=N";
	}

	public String getContentFromUrl(String keyword, int numberPerPage,
			String siteDomian, int start, boolean isVedio) throws ClientProtocolException, IOException {
		String url = null;
		String content = null;
		if (isVedio) {
			url = generateUrlforVedioSearch(keyword, numberPerPage, siteDomian,
					start);
		} else {
			url = generateUrlforSearch(keyword, numberPerPage, siteDomian,
					start);
		}
		if (logger.isInfoEnabled())
			logger.info("Will crawl the url: " + url);
		content = doRequest(url);
		return content;
	}

	public String getContentFromUrl(String url) throws ClientProtocolException, IOException {
		// url = url.replace("&amp;", "&").replace("", " ");
		return doRequest(url);
	}

	public String doRequestFromUrl(String url) throws Exception {
		// System.out.println(url+"   ===========");
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpreq = new HttpGet(url);
		String s = null;
		httpreq.setHeader("Content-Type", "application/xml");
		// httpreq.setHeader("Keep-Alive", "300");
		// httpreq.setHeader("Connection", "keep-alive");
		// httpreq.setHeader("User-Agent",
		// "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10 ( .NET CLR 3.5.30729)");
		try {
			HttpResponse response = httpclient.execute(httpreq);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				s = EntityUtils.toString(entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {

		}
		return s;
	}

	public static void main(String[] args) {
		ClientBaseImpl c = new ClientBaseImpl();
		//http://www.google.com.hk/search?q=coffee+site:.org&num=10&as_qdr=all&start=0&&as_ft=e&as_filetype=pdf&sa=N
		System.out.println(c.generateUrlforSearch("coffee", 10, ".org", 0));

	}

}
