package com.linkipedia.crawl;

import java.util.ArrayList;
import java.util.List;

import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import com.linkipedia.dao.ImageBeanDao;
import com.linkipedia.entity.ImageBean;
import com.linkipedia.util.LinkiPediaUtils;

public class ImageCrawl {

	private static MatchResult getMatchGroup(String input, String patternStr) {
		try {
			MatchResult result = null;
			PatternCompiler compiler = new Perl5Compiler();
			Pattern pattern = compiler.compile(patternStr);
			PatternMatcher matcher = new Perl5Matcher();
			if (matcher.contains(input, pattern)) {
				result = matcher.getMatch();
			}
			if (result != null)
				return result;
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ImageBean parseContentToImageBean(String keyword, int nodeId) {
		ClientBaseImpl client = new ClientBaseImpl();
		keyword = LinkiPediaUtils.formatGooelKeyword(keyword);
		String tempUrl = "http://www.google.com/images?as_q="
				+ keyword
				+ "&um=1&hl=en&newwindow=1&btnG=Google+%E6%90%9C%E7%B4%A2&as_epq=&as_oq=&as_eq=&as_sitesearch=&as_st=y&tbs=isch:1,isz:m,itp:photo,iar:w,ift:jpg";
		String content = null;
		List<String> list = new ArrayList<String>();
		ImageBean imageBean = null;
		try {
			content = client.doRequestFromUrl(tempUrl);
			String[] contents = null;
			if (content != null && content.length()> 0)
				contents = content.split("<td align=left valign=bottom width=23% style=\"padding-top:1px\">");
			if (contents != null && contents.length > 0) {
				for (int i = 1; i < contents.length; i++) {
					String temp = contents[i];
					MatchResult matchResult = getMatchGroup(temp,
							"(http.*?\\.jpg)");
					if (matchResult != null) {
						//TODO
						list.add(matchResult.group(0).replace("%2520", "%20"));
					}
					if (list.size() == 2) {
						break;
					}
				}
			}
			if (list != null && list.size() == 2) {
				//TODO
				imageBean = new ImageBean(nodeId,list.get(0),list.get(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return imageBean;
	}

	public static void main(String[] args) {
		ImageBeanDao  dao = new ImageBeanDao();
		ImageCrawl i = new ImageCrawl();
		ImageBean iimage = i.parseContentToImageBean("coffee", 2);
		dao.insertImageBean(iimage);
		System.out.println(iimage.getImageLink1()+"  "+ iimage.getImageLink2());
	}

}
