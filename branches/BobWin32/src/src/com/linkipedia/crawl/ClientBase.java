package com.linkipedia.crawl;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

public interface ClientBase {
	//public String doRequest(String url, String body);
	//public String doRequest(String url);
	//public String generateUrlforSearch(String keyword,int numberPerPage,String siteDomian,int start);
	//public String generateUrlforSearch(String keyword,int numberPerPage,String siteDomian);
	//public String generateUrlforVedioSearch(String keyword,int numberPerPage,String siteDomian);
	//public String generateUrlforVedioSearch(String keyword,int numberPerPage,String siteDomian,int start);
	public String getContentFromUrl(String url) throws ClientProtocolException, IOException;
	public String getContentFromUrl(String keyword, int numberPerPage,
			String siteDomian, int start,boolean isVedio) throws ClientProtocolException, IOException;


}
