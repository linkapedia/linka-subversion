package com.yuxipacific.aws.emr.samples.helpers;

import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeDocumentTemp;
import com.intellisophic.linkapedia.generic.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bancolombia
 */
public class ParserClassifierFile {
    
       /**
     *
     * @param content
     * @param title
     * @param url
     * @param docId
     * @return
     */
    public static List<NodeDocumentTemp> classifierInfoToNodeDocumentTemp(String content, String title, String url, String docId) {
        List<NodeDocumentTemp> docs = new ArrayList<NodeDocumentTemp>();
        try {
            if (!"".equals(content) && content != null) {

                String[] lines = content.split("\\r?\\n");
                for (int i = 0; i < lines.length; i++) {
                    if (i > 0) {
                        NodeDocumentTemp doc = new NodeDocumentTemp();
                        String[] line;
                        line = lines[i].split(",");
                        doc.setNodeID(StringUtils.revert(line[0]));
                        doc.setDocID(docId);
                        doc.setDocTitle(title);
                        doc.setDocURL(url);
                        doc.setScore01(convert(line[9]));
                        doc.setScore02(convert(line[1]));
                        doc.setScore03(0.0f);
                        doc.setScore04(convert(line[3]));
                        doc.setScore05(convert(line[2]));
                        doc.setScore06(convert(line[4]));
                        doc.setScore07(convert(line[5]));
                        doc.setScore08(convert(line[6]));
                        doc.setScore09(0.0f);
                        doc.setScore10(0.0f);
                        doc.setScore11(convert(line[6]));
                        doc.setScore12(0.0f);
                        doc.setPagerank(convert(line[8]));
                        docs.add(doc);
                    }
                }
            }
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        return docs;
    }
    
    
    
    
    

    /**
     *
     * @param content
     * @param title
     * @param url
     * @param docId
     * @return
     */
    public static List<NodeDocument> classifierInfoToNodeDocument(String content, String title, String url, String docId) {
        List<NodeDocument> docs = new ArrayList<NodeDocument>();
        try {
            if (!"".equals(content) && content != null) {

                String[] lines = content.split("\\r?\\n");
                for (int i = 0; i < lines.length; i++) {
                    if (i > 0) {
                        NodeDocument doc = new NodeDocument();
                        String[] line;
                        line = lines[i].split(",");
                        doc.setNodeID(StringUtils.revert(line[0]));
                        doc.setDocID(docId);
                        doc.setDocTitle(title);
                        doc.setDocURL(url);
                        doc.setScore01(convert(line[9]));
                        doc.setScore02(convert(line[1]));
                        doc.setScore03(0.0f);
                        doc.setScore04(convert(line[3]));
                        doc.setScore05(convert(line[2]));
                        doc.setScore06(convert(line[4]));
                        doc.setScore07(convert(line[5]));
                        doc.setScore08(convert(line[6]));
                        doc.setScore09(0.0f);
                        doc.setScore10(0.0f);
                        doc.setScore11(convert(line[6]));
                        doc.setScore12(0.0f);
                        doc.setPagerank(convert(line[8]));
                        docs.add(doc);
                    }
                }
            }
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        return docs;
    }

    /**
     *
     * @param content
     * @param title
     * @param url
     * @param docId
     * @return
     */
    public static List<DocumentNode> classifierInfoToDocumentNode(String content, String title, String url, String docId) {
        List<DocumentNode> docs = new ArrayList<DocumentNode>();
        try {
            if (!"".equals(content) && content != null) {

                String[] lines = content.split("\\r?\\n");
                for (int i = 0; i < lines.length; i++) {
                    if (i > 0) {
                        DocumentNode doc = new DocumentNode();
                        String[] line;
                        line = lines[i].split(",");
                        doc.setNodeID(StringUtils.revert(line[0]));
                        doc.setDocID(docId);
                        doc.setTitle(title);
                        doc.setURL(url);
                        doc.setScore01(convert(line[9]));
                        doc.setScore02(convert(line[1]));
                        doc.setScore03(0.0f);
                        doc.setScore04(convert(line[3]));
                        doc.setScore05(convert(line[2]));
                        doc.setScore06(convert(line[4]));
                        doc.setScore07(convert(line[5]));
                        doc.setScore08(convert(line[6]));
                        doc.setScore09(0.0f);
                        doc.setScore10(0.0f);
                        doc.setScore11(convert(line[6]));
                        doc.setScore12(0.0f);
                        doc.setPagerank(convert(line[8]));
                        docs.add(doc);
                    }
                }
            }
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        return docs;
    }

    /**
     *
     * @param data
     * @return
     */
    private static float convert(String data) {
        float value;
        try {
            value = Float.parseFloat(data);
        } catch (NumberFormatException e) {
            value = 0;
        }
        return value;
    }
}
