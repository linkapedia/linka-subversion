package com.yuxipacific.aws.emr.samples.jobs.reducer;

import com.yuxipacific.aws.emr.samples.helpers.io.ImgBytesWritable;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.bo.BuilderImage;
import org.linkapedia.images.bo.ImageSelection;
import org.linkapedia.images.filters.FilterImages;
import org.linkapedia.images.filters.FilterResources;
import org.linkapedia.images.filters.config.DefaultFilterImages;
import org.linkapedia.images.filters.config.DefaultFilterResources;
import org.linkapedia.images.io.UtilIO;
import org.linkapedia.images.resources.ResourceHtml;
import org.linkapedia.images.wrapper.PackImages;

public class ReduceImage extends Reducer<Text, Text, NullWritable, ImgBytesWritable> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, NullWritable, ImgBytesWritable>.Context context) throws IOException, InterruptedException {

        PackImages list = null;
        ImageInfo ii = null;
        FilterImages filter = new DefaultFilterImages();
        FilterResources pfWeb = new DefaultFilterResources();
        for (Text val : values) {
            System.out.println("Received {" + val.toString().length() + "} chars for url: " + key.toString());

            try {
                URL url = new URL(key.toString());
                list = ResourceHtml.getHtmlImages(url, pfWeb);
            } catch (Exception e1) {
                System.out.println("Error getting images from url: " + key.toString() + " " + e1.getMessage());
            }

            try {
                UtilIO.writeImages(list, new File("/home/andres/testedImages/"),
                        true, "jpg", new Float(0.25));
            } catch (Exception e) {
                System.out.println("Error wtiting images on disk: " + e.getMessage());
            }

            try {
                ii = ImageSelection.getBetterImageFilter(list, filter);
            } catch (Exception e) {
                System.out.println("Error getting better image: " + e.getMessage());
            }
//            if (ii != null) {
//                BuilderImage builder = new BuilderImage();
//                ii = builder.buildSiteImage(ii);
//                try {
//                    UtilIO.writeImages(ii, new File(
//                            "/home/andres/testedImages/better"), true, "jpg",
//                            new Float(0.25));
//                } catch (Exception e) {
//                    System.out.println("Error wtiting better image on disk: " + e.getMessage());
//                }
//            }
        }
    }
}