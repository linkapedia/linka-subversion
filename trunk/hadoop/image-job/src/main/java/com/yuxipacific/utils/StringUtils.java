package com.yuxipacific.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class StringUtils {

    private static final Logger log = LoggerFactory.getLogger(StringUtils.class);
    private static String[] defaultFilters = new String[]{" ,||,", " \\.||.", " n't||n't", " \",||\",", " \\?||?"};
    protected static final String DEFAULT_SEPARATOR = "\\|\\|";

    public static String escapeString(String strToEscape, String[] filters) {
        log.debug("escapeString(String, String[])");
        String filterSplit[] = null;
        if (strToEscape == null || strToEscape.trim().isEmpty()) {
            return null;
        }
        for (String filter : filters) {
            filterSplit = filter.split(DEFAULT_SEPARATOR);
            strToEscape = strToEscape.replaceAll(filterSplit[0], filterSplit[1]);
        }
        return strToEscape;
    }

    public static String escapeString(String strToEscape) {
        log.debug("escapeString(String)");
        return escapeString(strToEscape, defaultFilters);
    }
}
