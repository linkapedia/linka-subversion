/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.linkapedia.mapreducejobs.util.TestUtil;
import com.linkapedia.mapreducejobs.util.TitleExtractionUtil;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Ignore;

/**
 *
 * @author andy
 */
public class TitleExtractorTest {

    public TitleExtractorTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    public void getKnownTitle() {
        String sentences = null;
        String htmlString = null;
        String title = null;
        String url = "http://www.unirel.vt.edu/web/templates/";
        try {
            sentences = TestUtil.readFileContent("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/sentences/virginia.txt");
            htmlString = TestUtil.readFileContent("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/html/virginia.html");
            title = TitleExtractionUtil.titleProcess(htmlString, sentences, url);

        } catch (IOException ex) {
            System.out.println("Exception ex: " + ex.getMessage());
        }

        assertEquals("The title must be the same", "Music at Virginia Tech Department of Music Virginia Tech", title);

    }

    @Test
    @Ignore
    public void getKnownTitleWithDiferentUrl() {
        String sentences = null;
        String htmlString = null;
        String title = null;
        String url = "http://www.urldeprueba.com";
        try {
            sentences = TestUtil.readFileContent("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/sentences/virginia.txt");
            htmlString = TestUtil.readFileContent("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/html/virginia.html");
            title = TitleExtractionUtil.titleProcess(htmlString, sentences, url);

        } catch (IOException ex) {
            System.out.println("Exception ex: " + ex.getMessage());
        }

        assertEquals("The title must be the same", "Music at Virginia Tech Department of Music Virginia Tech", title);

    }

    @Test
    @Ignore
    public void getTitleWithEmptySentences() {
        String sentences = "";
        String htmlString = null;
        String title = null;
        String url = "http://www.unirel.vt.edu/web/templates/";
        try {
            htmlString = TestUtil.readFileContent("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/html/virginia.html");
            title = TitleExtractionUtil.titleProcess(htmlString, sentences, url);

        } catch (IOException ex) {
            System.out.println("Exception ex: " + ex.getMessage());
        }

        assertEquals("The title must be the same", "Music at Virginia Tech Department of Music Virginia Tech", title);

    }

    @Test
    @Ignore
    public void getTitleWithBadHtml() {
        String sentences = "";
        String htmlString = null;
        String title = null;
        String url = "http://www.unirel.vt.edu/web/templates/";
        try {
            htmlString = TestUtil.readFileContent("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/html/d80c7a06113805ee59948e7377db7130.html");
            title = TitleExtractionUtil.titleProcess(htmlString, sentences, url);

        } catch (IOException ex) {
            System.out.println("Exception ex: " + ex.getMessage());
        }

        assertEquals("The title must be the same", "kill the page!", title);

    }

    @Test
    @Ignore
    public void getTitleWithPlainText() {
        String sentences = "";
        String htmlString = null;
        String title = null;
        String url = "http://www.unirel.vt.edu/web/templates/";
        try {
            htmlString = TestUtil.readFileContent("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/html/b9a36d3075144daa68178c1718859d2d.html");
            title = TitleExtractionUtil.titleProcess(htmlString, sentences, url);

        } catch (IOException ex) {
            System.out.println("Exception ex: " + ex.getMessage());
        }
          assertEquals("The title must be the same", "kill the page!", title);


    }
}