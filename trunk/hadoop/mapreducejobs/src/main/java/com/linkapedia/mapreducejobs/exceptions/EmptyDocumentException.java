/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.exceptions;

/**
 *
 * @author Bancolombia
 */
public class EmptyDocumentException extends Exception {
    
    public EmptyDocumentException(){
    }
    
    public EmptyDocumentException(String msg){
        super(msg);
    }
    
}
