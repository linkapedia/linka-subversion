/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Sahar Ebadi
 */
public class TEXUtil {
    
    public static Map sortByComparator(Map unsortMap) {
        List list = new LinkedList(unsortMap.entrySet());
        // sort list based on comparator
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });
        // put sorted list into map again
        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
    
    public static void printMap(Map<String, Integer> map) {
        int count = 0;
        for (Map.Entry entry : map.entrySet()) {
            //            if((count==0) && (entry.getValue().toString().equals("26"))){
//            System.out.println("title" + count + ": " + entry.getKey() + ", fontSize: " + entry.getValue() + "px");//}
            count++;
            if (count == 2) {
                break;
            }
        }
    }
    
    public static int getMaxValue(Map map){
        int maximumValue=0;
        Set entrySet = map.entrySet();
        for(Iterator itr=entrySet.iterator();itr.hasNext();){
            Map.Entry entry = (Map.Entry) itr.next();
            if((Integer)entry.getValue()>maximumValue){
                maximumValue = (Integer)entry.getValue();
            }
        }
        return maximumValue;
    }

    public static Map.Entry getHighestScoreInMap(Map<String, Integer> map) {
        Set keySet = map.keySet();
        Iterator it = keySet.iterator();
        String title = null;
        Map.Entry entry = null;
        if (it.hasNext()) {
            entry = (Map.Entry) it.next();
            title = entry.getKey().toString();
        }
        return entry;
    }
}
