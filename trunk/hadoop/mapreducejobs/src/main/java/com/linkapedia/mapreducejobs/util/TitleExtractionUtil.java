/**
 * This is the process of Title extraction which accepts an HTMLString and sentences(produced by Reverb) and returns a title as the output.
 * these process consists of the following processes:
 * font size features
 * frequency features
 * evaluation function
 * 
 * @author Sahar Ebadi
 */
package com.linkapedia.mapreducejobs.util;

import com.linkapedia.mapreducejobs.behaviors.concrete.SentenceExtractorBehavior;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * These are the methods used for title extraction including font size and
 * frequency features
 *
 * @author Sahar Ebadi
 */
public class TitleExtractionUtil {

    public static  String titleProcess(String htmlString, String sentences, String url) {
        Map<String, Integer> fontSizeTitles = new HashMap<String, Integer>();
        Map<String, Integer> frequencyTitles = new HashMap<String, Integer>();
        Map<String, Double> finalTitles = new HashMap<String, Double>();
        String finalTitle = null;;
        Document doc = null;
        Map<String, Integer> sortedMap = null;
        if ((!"".equals(htmlString)) && (htmlString != null)) {
            try {
//                System.out.println("***************** Start title extraction _ titleProcess*****************");
               
                doc = Jsoup.parse(htmlString);
                String HTMLtext = null;
                try {
                    fontSizeTitles = new HashMap<String, Integer>();
                    if (!doc.toString().isEmpty() && !doc.toString().equalsIgnoreCase("") && doc != null) {
                        fontSizeTitles = TEXFontFeatures.getFontSizeTitles(doc, url);
                    }

                    if (!sentences.isEmpty() && !sentences.equalsIgnoreCase("") && sentences != null) {
                        frequencyTitles = TEXFrequencyFeatures.getFrequencyTitles(sentences, fontSizeTitles);// if we dont have fontSizeTitles in getFrecu() it will search the whole sentences
                    }

//                    System.out.println(frequencyTitles.toString());
                    if (frequencyTitles.isEmpty()) {
                        System.out.println("setences: ");
                        System.out.println(sentences.toString());
                    }
                    // add the U(X) function
                    finalTitles =  TEXEvaluationFunction.getTitlesScore(fontSizeTitles, frequencyTitles, sentences, htmlString, url);
//                    finalTitles.putAll(fontSizeTitles);
                    
                    Map<String, Double> sortedFinalTitle = TEXUtil.sortByComparator(finalTitles);
                    Set entrySet = sortedFinalTitle.entrySet();
                    Iterator it = entrySet.iterator();
                    Map.Entry entry;
                    if (it.hasNext()) {
                        try {
                            entry = (Map.Entry) it.next();
                            finalTitle = entry.getKey().toString();
                            //testing porpuse
                            Object finalTitleScore = entry.getValue();
//                            Double finalTitleScore = score. ;
                            System.out.println(" score: "+finalTitleScore.toString());
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                } catch (Exception ex) {
                    Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (Exception ex) {
                Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        finalTitle = TEXPostProcessing.perform(finalTitle);
        
        return finalTitle;

    }
    public static String OnlyDomain(String text) throws URISyntaxException{
        String cleanedURL=null;
            URI uri = new URI(text.replace(" ", ""));
            cleanedURL = uri.getHost();
        return cleanedURL;
    }
}
