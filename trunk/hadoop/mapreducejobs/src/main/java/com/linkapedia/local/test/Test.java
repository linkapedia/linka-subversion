/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.local.test;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.Doc_NodeTemp;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.handlers.Doc_NodeAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeAPIHandler;
import com.linkapedia.mapreducejobs.beans.ClassifierInfo;
import com.linkapedia.mapreducejobs.behaviors.concrete.SentenceRenditionBehavior;
import com.linkapedia.mapreducejobs.behaviors.concrete.SentenceExtractorBehavior;
import com.linkapedia.mapreducejobs.output.OutputTask;
import com.linkapedia.mapreducejobs.output.SentenceExtractionProcess;
import com.linkapedia.mapreducejobs.util.BringFilesFromS3;
import com.linkapedia.mapreducejobs.util.DomainBlackList;
import com.linkapedia.mapreducejobs.util.EightyLegResults;
import com.linkapedia.mapreducejobs.util.FileUtils;
import com.linkapedia.mapreducejobs.util.ParserProxy;
import com.linkapedia.mapreducejobs.util.SentenceExtractorProxy;
import com.linkapedia.mapreducejobs.util.StoreUtil;
import com.linkapedia.mapreducejobs.util.TestUtil;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author andres Clase con las pruebas de integracion para probar el workflow
 * de sentencias y de imagenes en map reduce
 */
public class Test {

    public static void main(String args[]) {
        System.out.println("\"***START TESTING BEHAVIORS***");
       // getNodeStatic("04076981");
        getDocNodeTemp("e66b156a0ba00a6440f40efa3c40cc67");
        getDocNode("137ab6c49581fe843b38a4da2c1803b6");
        classifyingSentencesFromEightyLocalFile("/home/yuxi-leader/Documentos/testing_map_reduce/00032d65-233b-4307-a8f8-697b9f95b486.80");
        cleanSentencesFromLocal("/home/andy/Descargas/", "/home/andy/Descargas/output");
        String s3File = "550534_555411_a_10.zip";
        classifyingSentencesFromLocalHtmls("/home/andy/Documentos/Docs/testing_map_reduce/title_extractor/html/");
        classifyingSentencesFromS3(s3File);
        generatingSentencesLocalFromS3();
        gettingHtmlsFromEightyLegFile();
        testTitle();
        TestingJsoup();
        getTitleRegularExpression();
        System.out.println("***FINISH TESTING BEHAVIORS***");
    }

    public static void getDocNodeTemp(String id) {
        try {
            Doc_NodeTemp doc = Doc_NodeAPIHandler.getDoc_NodesByDocumentIdTemp(id);
            String nodeList = FileUtils.decompressString(doc.getNodeList());
            String sentences = FileUtils.decompressString(doc.getSentences());
            String[] sentencesArray = sentences.split("(\r\n|\n)");
            System.out.println("item has been RETRIVED as: docId:" + doc.getDocId() + ", nodeList:" + nodeList + " Sentences: " + sentences);
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void getNodeStatic(String id) {
        try {
            NodeStatic node_static = NodeAPIHandler.getNode(id);
            System.out.println("Title: " + node_static.getTitle());


        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void getDocNode(String id) {
        try {
            Doc_Node doc_node = Doc_NodeAPIHandler.getDoc_NodesByDocumentId(id);
            String nodeList = FileUtils.decompressString(doc_node.getNodeList());
            String sentencesContent = FileUtils.decompressString(doc_node.getSentences());
            List<String> sentences = new ArrayList<String>(Arrays.asList(sentencesContent.split("(\r\n|\n)")));
            System.out.println("item has been RETRIVED as: docId:" + doc_node.getDocId() + ", nodeList:" + nodeList);
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void cleanSentencesFromLocal(String inputPath, String outputPath) {
        File folder = new File(inputPath);
        File[] listOfFiles = folder.listFiles();
        String dirtyContent = null;
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println("***Getting the content of the sentence file***");
                dirtyContent = getContentFromFile(file);
                String normalized_string = dirtyContent.replaceAll("[\u0000-\u001f]", "");//remove strange characters (unicode)
                normalized_string = normalized_string.replaceAll("[^\\x20-\\x7e]", "");//remove strange characters ()
                TestUtil.write(normalized_string, outputPath, file.getName());

            }
        }
    }

    /**
     * Metodo que clasifica informacion a partir de htmls puestos en una carpeta
     * de entrada
     *
     * @param inputPath
     */
    public static void classifyingSentencesFromLocalHtmls(String inputPath) {
        File folder = new File(inputPath);
        File[] listOfFiles = folder.listFiles();
        OutputTask sentenceTask = new SentenceExtractionProcess();
        ClassifierInfo classifierSetences = null;
        ClassifierInfo renditionSentences = null;
        String html = null;
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println("***************** Getting the html *****************");
                html = getContentFromFile(file);
                try {
                    sentenceTask.setSentencesBehavior(new SentenceExtractorBehavior());
                    classifierSetences = sentenceTask.generateSentences(html, "url");
                    if (classifierSetences != null && (!"".equals(classifierSetences.getSentences()) && classifierSetences.getSentences() != null)) {
                        StoreUtil.storeDynamoNodeDocument(classifierSetences.getclassifierScores(), "url", classifierSetences.getTitle());
                        StoreUtil.storeSentencesS3(classifierSetences.getSentences(), "url");
                        System.out.println("***************** Info Classified *****************");
                        System.out.println(classifierSetences.getclassifierScores());
                        System.out.println("");
                    }
                } catch (Exception ex) {
                    System.out.println("Error...");
                }
//                try {
//                    sentenceTask.setSentencesBehavior(new SentenceRenditionBehavior());
//                    renditionSentences = sentenceTask.generateSentences(html, "url");
//                    if (renditionSentences != null && !"".equals(renditionSentences.getSentences()) && renditionSentences.getSentences() != null) {
//                        StoreUtil.storeSentencesRenditionS3(renditionSentences.getSentences(), "url");
//                        System.out.println("");
//                    }
//
//                } catch (Exception ex) {
//                    System.out.println("Error... in rendition");
//                }
            }
        }
    }

    /**
     * Metodo que clasifica informacion a partir de un 80 leg que esta en S3 de
     * amazon
     *
     * @param line
     */
    public static void classifyingSentencesFromS3(String line) {
        BringFilesFromS3 bfs = new BringFilesFromS3();
        HashMap<String, byte[]> results;
        OutputTask sentenceTask = new SentenceExtractionProcess();
        ClassifierInfo classifierSetences = null;
        ClassifierInfo renditionSentences = null;
        try {
            results = bfs.getZipContentFromS3(line);
            if (results != null) {
                Set<String> keySet = results.keySet();
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        try {
                            sentenceTask.setSentencesBehavior(new SentenceExtractorBehavior());
                            classifierSetences = sentenceTask.generateSentences(content, "key");
                            if (classifierSetences != null && !"".equals(classifierSetences.getSentences()) && !"".equals(classifierSetences.getTitle())) {
                                StoreUtil.storeDynamoNodeDocument(classifierSetences.getclassifierScores(), "key", classifierSetences.getTitle());
                                StoreUtil.storeSentencesS3(classifierSetences.getSentences(), "key");
                                System.out.println("***************** Info Classified *****************");
                                System.out.println(classifierSetences.getclassifierScores());
                                System.out.println("");
                            }

                        } catch (Exception ex) {
                            System.out.println("Error...");
                        }
                        try {
                            sentenceTask.setSentencesBehavior(new SentenceRenditionBehavior());
                            renditionSentences = sentenceTask.generateSentences(content, "key");
                            if (renditionSentences != null && !"".equals(renditionSentences.getSentences())) {
                                StoreUtil.storeSentencesRenditionS3(renditionSentences.getSentences(), "key");
                            }

                        } catch (Exception ex) {
                            System.out.println("Error... in rendition");
                        }

                    }
                }
            }

        } catch (Exception ex) {
        }
    }

    /**
     * Metodo que clasifica informacion a partir de un 80 leg que esta en el
     * sistema local de archivos
     *
     * @param file
     */
    public static void classifyingSentencesFromEightyLocalFile(String file) {
        File fileToProcess = new File(file);
        HashMap<String, byte[]> results;
        OutputTask sentenceTask = new SentenceExtractionProcess();
        //  BlackList blackList = new BlackList();
        ClassifierInfo classifierSentences;
        try {
            ByteArrayInputStream bai = new ByteArrayInputStream(TestUtil.readBytes(fileToProcess));
            try {
                results = EightyLegResults.readFile(bai);
                Set<String> keySet = results.keySet();
                System.out.println("Number of htmls : " + keySet.size());
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            if (!DomainBlackList.isInBlackList(_key)) {
                                try {
                                    System.out.println("Url working on: " + _key);
                                    sentenceTask.setSentencesBehavior(new SentenceExtractorBehavior());
                                    classifierSentences = sentenceTask.generateSentences(content, _key);
                                    if (classifierSentences != null && !"".equals(classifierSentences.getSentences()) && !"".equals(classifierSentences.getTitle()) && classifierSentences.getclassifierScores() != null) {
                                        System.out.println("***************** Info Classified *****************");
                                        System.out.println("Title: " + classifierSentences.getTitle());
                                        System.out.println("\nSentences: " + classifierSentences.getSentences());
                                        System.out.println("\nScore: " + classifierSentences.getclassifierScores());
                                        String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(_key.toString());
                                        // StoreUtil.storeSentencesS3(classifierSentences.getSentences(),_key);
                                        //StoreUtil.storeDynamoNodeDocument(classifierSentences.getclassifierScores(), _key, classifierSentences.getTitle());
                                        StoreUtil.storeDynamoNodeDocumentTemp(classifierSentences.getclassifierScores(), _key, classifierSentences.getTitle());
                                        //StoreUtil.storeDynamoDoc_Node(classifierSentences.getclassifierScores(), _key.toString(), classifierSentences.getTitle(),classifierSentences.getSentences());
                                        StoreUtil.storeDynamoDoc_NodeTemp(classifierSentences.getclassifierScores(), _key.toString(), classifierSentences.getTitle(), classifierSentences.getSentences());
                                        //StoreUtil.storeLocal(classifierSentences.getSentences(), "/home/yuxi-leader/Documentos/testing_map_reduce/output/", md5);
                                        System.out.println("Data Saved...");

                                    }
                                } catch (Exception ex) {
                                    System.out.println("Error...");
                                }
                            } else {
                                System.out.println("Url: " + _key + " Banned...");
                            }
                        }
                    }
                }


            } catch (Exception ex) {
                System.out.println("Error... " + ex.getMessage());
            }
        } catch (IOException ex) {
            System.out.println("Error... " + ex.getMessage());
        }


    }

    /**
     * Metodo que obtiene los htmls a partir de un 80 leg file local y los
     * almacena en una carpeta
     */
    public static void gettingHtmlsFromEightyLegFile() {
        System.out.println("Generating htmls in local");
        String inputPath = "/home/andy/Documentos/Docs/testing_map_reduce/369854_375434_a_1.80";
        String outputPath = "/home/andy/Documentos/Docs/testing_map_reduce/htmls/";
        File fileToProcess = new File(inputPath);
        try {
            ByteArrayInputStream bai = new ByteArrayInputStream(TestUtil.readBytes(fileToProcess));
            try {
                HashMap<String, byte[]> results = EightyLegResults.readFile(bai);
                Set<String> keySet = results.keySet();
                System.out.println("Number of htmls : " + keySet.size());
                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            byte[] byteContent = content.getBytes("UTF-8");
                            String htmlContent = ParserProxy.parse(results.get(_key));
                            String title = ParserProxy.getTitleFromFile(byteContent);
                            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(_key.toString());
                            TestUtil.write(content, outputPath, md5 + ".html");
                        }
                    } else {
                        System.out.println("Exception: The content for the url: " + _key + " is null or empty.");
                    }
                }
                System.out.println("Files generated...");


            } catch (Exception ex) {
                System.out.println("Error... " + ex.getMessage());
            }

        } catch (IOException ex) {
            System.out.println("Error... " + ex.getMessage());
        }
    }

    /**
     * Metodo que a partir de un archivo de texto con las rutas de los 80 leg
     * files los descarga del s3 y obtiene el contenido de estos, incluyendo las
     * sentencias pero no los clasifica
     */
    public static void generatingSentencesLocalFromS3() {
        String inputPath = "/home/andy/Descargas/miPrueba.txt";
        BringFilesFromS3 bfs = new BringFilesFromS3();
        HashMap<String, byte[]> results;
        File file = new File(inputPath);
        String outputPath = "/home/andres/Downloads/bad";
        String docsToGenerated = getContentFromFile(file);
        String[] legs = docsToGenerated.split("\n");
        for (int i = 0; i < legs.length; i++) {
            try {
                results = bfs.getZipContentFromS3(legs[i]);
                if (results != null) {
                    Set<String> keySet = results.keySet();
                    for (String _key : keySet) {
                        if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                            String content = new String((byte[]) results.get(_key), "UTF-8");
                            if ((content != null) && (!content.trim().isEmpty())) {
                                byte[] byteContent = content.getBytes("UTF-8");
                                String htmlContent = ParserProxy.parse(byteContent);
                                String title = ParserProxy.getTitleFromFile(byteContent);
                                if ((htmlContent != null) && (!htmlContent.trim().isEmpty())) {
                                    System.out.println("Getting Sentences");
                                    String sentences = null;
                                    try {
                                        System.out.println("***************** Calling Reverb *****************");
                                        sentences = SentenceExtractorProxy.extractFrom(content, true);
                                        if ((sentences != null) && (!"".equals(sentences))) {
                                            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(_key.toString());
                                            TestUtil.write("Title : " + title + "\nKey: " + _key + "\nSentences : " + sentences, outputPath, md5);
                                        }
                                        System.out.println("***************** Ending Reverb *****************");
                                    } catch (Exception ex) {
                                        Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
            }
        }
    }

    private static boolean saveImage(BufferedImage image, String name) throws IOException {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("image/jpeg");
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bao);
        ByteArrayInputStream bai = new ByteArrayInputStream(bao.toByteArray());
        boolean result = StoreUtil.storeS3(bai, name + ".jpg", objectMetadata);
        return result;
    }

    public static void testTitle() {
        System.out.println("Testing titles...");
        String inputPath = "/home/andres/Downloads/maloTemp.html";
        File fileToProcess = new File(inputPath);
        try {
            byte[] byteContent = TestUtil.readBytes(fileToProcess);
            String content = new String(byteContent, "UTF-8");
            String hmtlContent = ParserProxy.parse(byteContent);
            String title = ParserProxy.getTitleFromFile(byteContent);
            System.out.println("Finish...");
        } catch (Exception ex) {
            System.out.println("Error... " + ex.getMessage());
        }
    }

    public static void getTitleRegularExpression() {
        System.out.println("Generating htmls in local");
        String inputPath = "/home/andres/Downloads/451723_455432_a_5850.80";
        String title = "";
        String outputPath = "/home/andres/Downloads/bad";
        File fileToProcess = new File(inputPath);
        try {
            ByteArrayInputStream bai = new ByteArrayInputStream(TestUtil.readBytes(fileToProcess));
            try {
                HashMap<String, byte[]> results = EightyLegResults.readFile(bai);
                Set<String> keySet = results.keySet();
                System.out.println("Number of htmls : " + keySet.size());

                for (String _key : keySet) {
                    if ((results.get(_key) != null) && (((byte[]) results.get(_key)).length > 0)) {
                        String content = new String((byte[]) results.get(_key), "UTF-8");
                        if ((content != null) && (!content.trim().isEmpty())) {
                            byte[] byteContent = content.getBytes("UTF-8");
                            Pattern p = Pattern.compile("<head>.*?<title>(.*?)</title>.*?</head>", Pattern.DOTALL);
                            Matcher m = p.matcher(content);
                            while (m.find()) {
                                title = m.group(1);
                                System.out.println("testing...");
                            }
                            System.out.println("Title: " + title);
                            String htmlContent = ParserProxy.parse(results.get(_key));
                            String titleTemp = ParserProxy.getTitleFromFile(byteContent);
                            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(_key.toString());
                            TestUtil.write(content, outputPath, md5);
                        }
                    } else {
                        System.out.println("Exception: The content for the url: " + _key + " is null or empty.");
                    }
                }
                System.out.println("Files generated...");
            } catch (Exception ex) {
                System.out.println("Error... " + ex.getMessage());
            }

        } catch (IOException ex) {
            System.out.println("Error... " + ex.getMessage());
        }
    }

    public static void TestingJsoup() {
        System.out.println("Testing Jsoup");
        File input;
        try {
            input = new File("/home/andres/Downloads/malo01.html");
            String html = Jsoup.connect("url").get().html();
            Document doc = Jsoup.parse(input, "UTF-8");
            Elements elemets = doc.getElementsByTag("META");
            String temp = doc.getElementsByTag("title").text();
            Element link = doc.select("a").first();

            String title = getMetaTag(doc, "title");
            String description = getMetaTag(doc, "description");
            System.out.println("Finish...");
        } catch (IOException ex) {
        }

    }

    public static String getMetaTag(Document document, String attr) {
        Elements elements = document.select("meta[name=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) {
                return s;
            }
        }
        elements = document.select("meta[property=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) {
                return s;
            }
        }
        return null;
    }

    public static String getContentFromFile(File file) {
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        DataInputStream dis = null;
        StringBuilder sb = new StringBuilder();
        try {
            fis = new FileInputStream(file);
            // Here BufferedInputStream is added for fast reading.
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            // dis.available() returns 0 if the file does not have more lines.
            while (dis.available() != 0) {
                // this statement reads the line from the file and print it to
                // the console.
                //System.out.println(dis.readLine());
                sb.append(dis.readLine());
                sb.append("\n");
            }

            // dispose all the resources after using them.
            fis.close();
            bis.close();
            dis.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
