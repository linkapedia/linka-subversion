/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.beans;

/**
 *
 * @author andres
 */
public class ClassifierInfo {
    
    
    private String title;
    private String sentences;
    private String classifierScores;
    
    public void setTitle(String value){
        title = value;
    }
    
    public String getTitle(){
        return title;
    }
    
    public void setSentences(String value){
        sentences = value;     
    }
    
    public String getSentences(){
        return sentences;
    }
    
    public void setclassifierScores(String value){
        classifierScores = value;
    }
    
    public String getclassifierScores(){
        return classifierScores;
    }
    
    
    
}
