/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Sahar Ebadi
 */
public class TEXFrequencyFeatures {
    
    public static Map<String, Integer> getFrequencyTitles(String textPage, Map fontSizeTitles) {// return a Map with all the words in page/all the title sentences(coming from fontSizeTitles)
        String[] pageContent = textPage.split(" ");
        String[] titleWords;
        String title;
        Map<String, Integer> wordFrequencyMap = new HashMap<String, Integer>();
        Map<String, Integer> sortedWordFrequencyMap = new HashMap<String, Integer>();

        if (fontSizeTitles.isEmpty()) {
            wordFrequencyMap = frequencyCounter(pageContent, "", null, wordFrequencyMap);
        } else {
            Set entrySet = fontSizeTitles.entrySet();
            int countTitle = 0;
            for (Iterator it = entrySet.iterator(); it.hasNext();) {
                if (countTitle > 1) {
                    break;
                }
                Map.Entry entry = (Map.Entry) it.next();
                String noCleanTitle = entry.getKey().toString();
                title = TEXDataCleaning.trimSpaces(TEXDataCleaning.trimSpecialChar(noCleanTitle));
                titleWords = title.split(" ");
                for (int count = 0; count < titleWords.length; count++) {
                    wordFrequencyMap.putAll(frequencyCounter(pageContent, titleWords[count], titleWords, wordFrequencyMap));
                }
                countTitle++;
            }
        }
        sortedWordFrequencyMap = TEXUtil.sortByComparator(wordFrequencyMap);
        return sortedWordFrequencyMap;
    }

    public static Map<String, Integer> frequencyCounter(String[] pageContent, String titleWord, String[] title, Map wordFrequencyMap) { // gives back a map with frequency of each word(in title /in whole page)
        String word;
        if (titleWord.isEmpty() && title == null) {// if the font size title is null then get the Freq for each word in the page 
            for (int i = 0; i < pageContent.length; i++) {
                if (TEXDataCleaning.isNotBlackList(pageContent[i]) && (!pageContent[i].isEmpty() && !pageContent[i].equalsIgnoreCase(""))) {
                    pageContent[i] = TEXDataCleaning.trimSpaces(TEXDataCleaning.trimSpecialChar(pageContent[i]));
                    word = pageContent[i];
                    Integer count = (Integer) wordFrequencyMap.get(word.toLowerCase());
                    if (count == null) {
                        wordFrequencyMap.put(word.toLowerCase(), 1);
                    } else {
                        wordFrequencyMap.put(word.toLowerCase(), count + 1);
                    }
                }
            }
        } else { // else find the Frq only for words found by font size features
            if (TEXDataCleaning.isNotBlackList(titleWord) && (!wordFrequencyMap.containsKey(titleWord.toLowerCase()))) {
                Integer count = (Integer) wordFrequencyMap.get(titleWord.toLowerCase());
                if (count == null) {
                    wordFrequencyMap.put(titleWord.toLowerCase(), 1);
                }
                for (int i = 0; i < pageContent.length; i++) {
                    if (titleWord.equalsIgnoreCase(pageContent[i])) {
                        count = (Integer) wordFrequencyMap.get(titleWord.toLowerCase());
                        wordFrequencyMap.put(titleWord.toLowerCase(), count + 1);
                    }
                }
            }
        }
        return wordFrequencyMap;
    }
}
