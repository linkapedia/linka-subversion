/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author linkapedia_leader
 */
public class DomainBlackList {

    static List<String> blackList = new ArrayList<String>();

    static {
        blackList.add("mickopedia.org");
        blackList.add("wikipedia.org");
        blackList.add("ajevonline.org");
        blackList.add("wapipedia.org");
    }

    public static boolean isInBlackList(String url) {
        boolean response = false;
        for (String domain : blackList) {
            if (url.contains(domain)) {
                response = true;
                break;
            }
        }

        return response;

    }
}
