/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sahar Ebadi
 */
public class TEXDataCleaning {
    
    public static String[] removeBlackListWords(String[] wordsInTitle){
        List<String> listWordsInTitle = new ArrayList<String>();
        for(String word: wordsInTitle){
            if(isNotBlackList(word)){
                listWordsInTitle.add(word);
            }
        }
        String[] copyWordsInTitle = new String[listWordsInTitle.size()];
        copyWordsInTitle = listWordsInTitle.toArray(copyWordsInTitle);
        return copyWordsInTitle;
    }
    
    public static boolean isNotBlackList(String word) {
        boolean result = true;
        String[] blackList = {"your","did","would","day","special","whole","specially","especial","especially","not","till","all","should","but","about","a.m.","like","p.m.","other","there","here","according","week","except","who","when","where","always","usually","never","towards","people","person","without","last","case","untill","now","then","still","others","something","some","good","bad","for", "and", "the", "para","...","was","were","with","from","are","have","has","which","that","its","has","have","this","that","been","also","may","might","such","into","onto","who","how","both","other","not","became","become","came","come","over","same","within","out","their","them","she","her","his","you","mine","more","adr","new","between","own","will","since","because","had","amongst","among","needs","need","must","some","few","many","much","lot","little","total","sir","madam","boy","girl","people","than","via","one","these","those","year","before","after"};
        //String[] negativeList = {"view","views","comment","like","page not found","error", "Forbidden,"..}; 
        if (word.length() < 3) {
            result = false;
        } else if (word.equalsIgnoreCase(" ")) {
            result = false;
        } else {
            for (String blackWord : blackList) {
                if (word.equalsIgnoreCase(blackWord)) {
                    result = false;
                }
            }
        }
        return result;
    }
    
    public static String trimSpecialChar(String text) {// removes special character and replace them with space ***NOTE: NOT FOR HTML before Parsing 
        text = text.trim().replaceAll("(?i)(\\.com|\\.org|\\.edu|\\.gov|www\\.|\\.info|\\.hu|\\.co|\\.uk|\\.us|\\.de|\\.net|\\.se|\\.ir)|[\\*|\\|\\/|#|_|~|¿|!|¦|<|>|»|«]", " ");// - hazf shod bekhatere kalamati mese e-commerce.
        return text;
    }
    
    public static String trimSpaces(String text) {
        text = text.trim().replaceAll("[\\s|\\r|\\t|\\n]", " ");
        text = text.trim().replaceAll("\\s+", " ");
        return text;
    }
    public static String trimSpaceCharacter(String text){
        text = text.replace("^.+?(\\*|\\|\\/|#|_|-|~|¿|!|¦|<|>|»|«|\\s+)", "");
        return text;
    }
    public static String removeOnlyDomainDate(String text, String url){// removes titles which only contain domain name or domain name + date
        String domain=null;
        text = text.toLowerCase();
        String finalTitle = "kill the page!";
        try{
        String cleanedUrl = url.replace(" ", "").replace("^","%");
        URI uri = new URI(cleanedUrl);
        domain = uri.getHost().toLowerCase();
        String[] title = text.trim().split(" ");
        for(String word : title){
            if(!trimSpaceCharacter(word).isEmpty()){
                if(!domain.contains(word) && wordIsADate(word)==false){
                    finalTitle = text;
                    break;
                }
            }
        }
        }catch(Exception e){
            //just pass as a bad title finalTitle = "kill the page!";
        }
        return finalTitle;
    }
   public static String removeDate(String finalTitle){//remove the title if it only includes a time expression
        //^((\d{2}|\d{4}|mah)+(\s+|\\)+(\d{2}|\d{2}|mah)+(\s+|\\))
        boolean startsWithDate = finalTitle.matches("^(?i)((\\d{1,2}|\\d{4}|(Jan|Feb|Mar|Apr|May|June|July|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December))+\\s*(\\s+|-|_|\\/|\\\\|:|,|\\.)\\s*(\\d{1,2}|\\d{4}|(Jan|Feb|Mar|Apr|May|June|July|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December))+\\s*(\\s*|-|_|\\/|\\\\|:|,|\\.)\\s*(\\d{1,2}|\\d{4}|(Jan|Feb|Mar|Apr|May|June|July|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December))*)");
        if(startsWithDate==true){
            finalTitle = "Kill the page!";
        }
        return finalTitle;
    }
   public static boolean wordIsADate(String word){//if the word given could  be a part of a date returns true other wise false
        boolean isADate = word.matches("(?i)((\\d{1,2}|\\d{4})|(Jan|Feb|Mar|Apr|May|June|July|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December))?(\\s*|-|_|\\/|\\\\|:|,|\\.)?");
        return isADate;
    }

}
