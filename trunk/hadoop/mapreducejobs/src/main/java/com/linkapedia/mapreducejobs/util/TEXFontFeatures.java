/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Sahar Ebadi
 */
public class TEXFontFeatures {
    
    public static Map getFontSizeTitles(Document doc, String url) {
        String title;
        Elements title2;
        Map<String, Integer> textSizeObj = new HashMap<String, Integer>();
        int fontSize = 0;
        try {
            title = doc.title();
            if(title!=null && title.isEmpty()){
                // Extract the text between the two title elements
                String docCopy = doc.toString();
                Pattern p = Pattern.compile("(?i)(<title.*?>)(.+?)(</title>)");
                Matcher m = p.matcher(docCopy);
                if (m.find()) {
                    title = m.group(1);  // The matched substring
                }
            }
            if (title != null && !title.equals("")) {
                textSizeObj = putInList(textSizeObj, title, getGeneralSize("title"), url);
            }

            title2 = doc.getElementsByTag("title");// for the future MR we can go through all the title and h1 and h2 tags, Not only the first one.
            if (title2.first() != null && !title2.first().text().equals("")) {
                textSizeObj = putInList(textSizeObj, title2.first().text(), getGeneralSize("title"), url);
            }

            Elements h1Element = doc.getElementsByTag("h1");
            if (h1Element.first() != null && !h1Element.first().text().equals("")) {
                textSizeObj = putInList(textSizeObj, h1Element.first().text(), getGeneralSize("h1"), url);
            }

            Elements h2Element = doc.getElementsByTag("h2");
            if (h2Element.first() != null && !h2Element.first().text().equals("")) {
                textSizeObj = putInList(textSizeObj, h2Element.first().text(), getGeneralSize("h2"), url);
            }

            Elements fontSizeEls = doc.select("[style*=font-size]");
            textSizeObj = getAllFontSizeElmnts(fontSizeEls, fontSize, textSizeObj, url);
        } catch (Exception e) {
            e.getMessage();
        }
        //System.out.println(textSizeObj.toString());
        textSizeObj = findBestTitle(textSizeObj);
        return textSizeObj;
    }
    
    public static Map<String, Integer> findBestTitle(Map feasibleTitles) {
        Map<String, Integer> treeMap = new TreeMap<String, Integer>(feasibleTitles);
        Map<String, Integer> sortedMap = new HashMap<String, Integer>();
        sortedMap = TEXUtil.sortByComparator(feasibleTitles);
        TEXUtil.printMap(sortedMap);
        return sortedMap;
    }
    
    public static Map putInList(Map textSizeObj, String noTag, int fontSize, String url) {
            noTag = TEXDataCleaning.trimSpaces(TEXDataCleaning.trimSpecialChar(noTag));// to remove mre than one spaces from titles.
            if(!TEXPostProcessing.isABadTitle(noTag, url)){
                if (fontSize != 0 && !noTag.equals("")) {
                    if (textSizeObj.get(noTag) != null) {
                        int preElmSize = (Integer) textSizeObj.get(noTag);
                        if (fontSize > preElmSize) {
                            textSizeObj.put(noTag, fontSize);
                            //                    System.out.println("notag: "+noTag);
                            //                    System.out.println("fontsize: "+fontSize+", text: "+noTag+", element:"+element);
                        }
                    } else {
                        textSizeObj.put(noTag, fontSize);
                    }
                }
            }
        return textSizeObj;
    }
    
    // to convert all difreent fontsizes to pt
    public static Map getAllFontSizeElmnts(Elements fontSizeEls, int fontSize, Map textSizeObj, String url) {
        for (Element element : fontSizeEls) {
            Pattern pattern = Pattern.compile("font-size:(.*?);");
            Matcher matcher = pattern.matcher(element.toString());
            if (matcher.find()) {
                fontSize = getGeneralSize(matcher.group(1));
                for (int childIndex = 0; childIndex < element.childNodeSize(); childIndex++) {
                    String noTag = Jsoup.parse(element.childNode(childIndex).toString()).text();
                    putInList(textSizeObj, noTag, fontSize, url);
                }
            }
        }
        return textSizeObj;
    }
    
    public static int getGeneralSize(String fontSize) {
        int convertedFontSize = 8;
        if (fontSize.equalsIgnoreCase("9px") || fontSize.equalsIgnoreCase("6pt") || fontSize.equalsIgnoreCase("60%") || fontSize.equalsIgnoreCase("h6")) {
            convertedFontSize = 9;
        } else if (fontSize.equalsIgnoreCase("10px") || fontSize.equalsIgnoreCase("7pt") || fontSize.equalsIgnoreCase("65%") || fontSize.equalsIgnoreCase("h6")) {
            convertedFontSize = 10;
        } else if (fontSize.equalsIgnoreCase("11px") || fontSize.equalsIgnoreCase("8pt") || fontSize.equalsIgnoreCase("70%") || fontSize.equalsIgnoreCase("h6")) {
            convertedFontSize = 11;
        } else if (fontSize.equalsIgnoreCase("12px") || fontSize.equalsIgnoreCase("9pt") || fontSize.equalsIgnoreCase("75%") || fontSize.equalsIgnoreCase("80%") || fontSize.equalsIgnoreCase("h6")) {
            convertedFontSize = 12;
        } else if (fontSize.equalsIgnoreCase("13px") || fontSize.equalsIgnoreCase("10pt") || fontSize.equalsIgnoreCase("85%") || fontSize.equalsIgnoreCase("90%") || fontSize.equalsIgnoreCase("h6")) {
            convertedFontSize = 13;
        } else if (fontSize.equalsIgnoreCase("14px") || fontSize.equalsIgnoreCase("11pt") || fontSize.equalsIgnoreCase("95%") || fontSize.equalsIgnoreCase("h5")) {
            convertedFontSize = 14;
        } else if (fontSize.equalsIgnoreCase("15px") || fontSize.equalsIgnoreCase("12pt") || fontSize.equalsIgnoreCase("100%") || fontSize.equalsIgnoreCase("h4")) {
            convertedFontSize = 15;
        } else if (fontSize.equalsIgnoreCase("16px") || fontSize.equalsIgnoreCase("13pt") || fontSize.equalsIgnoreCase("105%") || fontSize.equalsIgnoreCase("115%") || fontSize.equalsIgnoreCase("110%") || fontSize.equalsIgnoreCase("h4")) {
            convertedFontSize = 16;
        } else if (fontSize.equalsIgnoreCase("18px") || fontSize.equalsIgnoreCase("14pt") || fontSize.equalsIgnoreCase("120%") || fontSize.equalsIgnoreCase("h3")) {
            convertedFontSize = 18;
        } else if (fontSize.equalsIgnoreCase("20px") || fontSize.equalsIgnoreCase("15pt") || fontSize.equalsIgnoreCase("125%") || fontSize.equalsIgnoreCase("h2")) {
            convertedFontSize = 20;
        } else if (fontSize.equalsIgnoreCase("h1")) {
            convertedFontSize = 24;
        } else if (fontSize.equalsIgnoreCase("title")) {
            convertedFontSize = 26;
        }

        return convertedFontSize;
    }
}
