/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sahar Ebadi
 */
public class TEXEvaluationFunction {
    
    public static Map getTitlesScore(Map fontSizeTitles, Map frequencyTitles, String sentences,String htmlString, String url){
        Map titleScoreMap = new HashMap<String,Double>();
        BigDecimal totalScore = new BigDecimal("0.00");
        if(!fontSizeTitles.isEmpty()){// get the score for each title in the titles of fontSizeTitles
            Set entrySet = fontSizeTitles.entrySet();
            for(Iterator itr=entrySet.iterator();itr.hasNext();){//for each title extracted by fontsize features
                Map.Entry entry = (Map.Entry) itr.next();
                String fontTitle = entry.getKey().toString();
                int titleFontSize = (Integer) entry.getValue();
                String[] wordsInTitle = fontTitle.split(" ");
                totalScore = getScoreForSentence(frequencyTitles, fontSizeTitles, titleFontSize, wordsInTitle);
                titleScoreMap.put(fontTitle, totalScore);
             }
        }else{// if fontSizeTitles is null means we want to score each sentence in the page or sentences
            String[] sentencesSplited = sentences.split("[\\r\\n]+");
            if(sentencesSplited.length>3){
                System.out.println("real freq extracion is starting...");
            }
            String feasibleTitle;
            for(String sentence : sentencesSplited){//for each sentence in page
                feasibleTitle = sentence;
                int titleFontSize = 0;// we want to neutralize the effect of font size in the formula since we are only taking into account the frequency feature
                if(!TEXPostProcessing.isABadTitle(feasibleTitle, url)){
                    if(!feasibleTitle.isEmpty() && !feasibleTitle.equalsIgnoreCase("kill the page!")){
                        String[] wordsInSentence = sentence.split(" ");
                        totalScore = getScoreForSentence(frequencyTitles, fontSizeTitles, titleFontSize, wordsInSentence);
                        titleScoreMap.put(feasibleTitle, totalScore);
                    }
                }else{
                    titleScoreMap.put("kill the page!", totalScore);
                }
            }
        }
    return titleScoreMap;
    }
    
    public static BigDecimal getScoreForSentence( Map frequencyTitles, Map fontSizeTitles, int titleFontSize, String[] wordsInSentence){
        Map titleScoreMap = new HashMap<String,Double>();
        BigDecimal totalScore = new BigDecimal("0.00");
        int maxFontSize = TEXUtil.getMaxValue(fontSizeTitles);
        int maxFreqInPage = TEXUtil.getMaxValue(frequencyTitles);
        BigDecimal maxBound = new BigDecimal("1.0000000001");
        BigDecimal minBound = new BigDecimal("0.00");
        String[] cleanedWordsInSentence = TEXDataCleaning.removeBlackListWords(wordsInSentence);
        int numWordsInSentence= cleanedWordsInSentence.length;
        for(String word : cleanedWordsInSentence){// for each word in the specific sentence find the score
            int freqValue=0;
            BigDecimal scoreForWord = new BigDecimal("0.00");
            if(!frequencyTitles.isEmpty()&& frequencyTitles.containsKey(word.toLowerCase())){
                 freqValue = (Integer)frequencyTitles.get(word.toLowerCase());
            }
            scoreForWord = getScore(titleFontSize, maxFontSize, freqValue, maxFreqInPage, numWordsInSentence);
            totalScore = totalScore.add(scoreForWord);// add all the wordScores in this sentence and get the sentence score(totalScore)
            if(totalScore.compareTo(maxBound)>0||totalScore.compareTo(minBound)<0){
                System.out.println("################# ERRRORR: score out of bound: "+totalScore);
            }
        }
        return totalScore;
    }
    public static BigDecimal getScore(int fontsizeValueInt, int maxFontSizeInt ,int freqValueInt, int maxFreqInPageInt, int numWordsInTitleInt){
        BigDecimal fontsizeValue = new BigDecimal(fontsizeValueInt);
        BigDecimal maxFontSize = new BigDecimal(maxFontSizeInt);
        BigDecimal freqValue = new BigDecimal(freqValueInt);
        BigDecimal maxFreqInPage = new BigDecimal(maxFreqInPageInt);
        BigDecimal numWordsInTitle = new BigDecimal(numWordsInTitleInt);
        BigDecimal w_FontSize = new BigDecimal("0.7");
        BigDecimal w_Frequency = new BigDecimal("0.3");
        BigDecimal score=new BigDecimal("0.00").setScale(20, BigDecimal.ROUND_HALF_UP);
        BigDecimal wordW = new BigDecimal("1.00");
        try{
            BigDecimal font = getScoreForEachFeature(fontsizeValue,maxFontSize,w_FontSize);
            BigDecimal fre = getScoreForEachFeature(freqValue, maxFreqInPage, w_Frequency);
            BigDecimal fontPlusFre = font.add(fre);
            wordW = wordW.divide(numWordsInTitle,20, RoundingMode.HALF_UP);
            score = fontPlusFre.multiply(wordW);
//        score = (((fontsizeValue/maxFontSize)*w_FontSize)+((freqValue/maxFreqInPage)*w_Frequency))*(((BigDecimal)1)/numWordsInTitle);
        }catch(Exception e){
        e.getMessage();
        }
        return score;
    }
    
    public static BigDecimal getScoreForEachFeature(BigDecimal value, BigDecimal maxValue, BigDecimal weightOfFeature){
        BigDecimal ScoreForOneFeature = new BigDecimal("0.00");
        if(maxValue.compareTo(BigDecimal.ZERO)>0 && value.compareTo(BigDecimal.ZERO)!=0){// we cant have 0/1
            ScoreForOneFeature = value.divide(maxValue,20, RoundingMode.HALF_UP);
            ScoreForOneFeature = ScoreForOneFeature.multiply(weightOfFeature);
        }
        return ScoreForOneFeature;
    }

}
