package com.linkapedia.mapreducejobs.util;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.intellisophic.linkapedia.amazon.services.S3.bo.ManageBucket;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class BringFilesFromS3 {

    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("classifier/config");
    private static final String FILE_BUCKET = systemConfig.getString("classifier.mapper.file.bucket");
    private static ManageBucket bucketIn;

    public BringFilesFromS3() {
        bucketIn = new ManageBucket(FILE_BUCKET);
    }

    public InputStream getFile(String filePath) {
        InputStream file = null;
        try {
            file = bucketIn.getData(systemConfig.getString("classifier.mapper.file.input") + filePath);
        } catch (Exception e) {
            System.err.println("Error getting ZIP " + filePath + e);
        }
        return file;
    }

    public HashMap<String, byte[]> getZipContentFromS3(String data) throws Exception {
        HashMap results = null;
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentType("text/plain");

        InputStream file = getFile(data);
        if (file == null) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipInputStream zin = new ZipInputStream(file);

        ZipEntry ze = null;
        byte[] buffer = new byte[9000];
        try {
            while ((ze = zin.getNextEntry()) != null) {
                out.reset();
                int len;
                while ((len = zin.read(buffer)) != -1) {
                    out.write(buffer, 0, len);
                }
                out.flush();
                int temp = out.toByteArray().length;
                System.out.println("ZipSize: " + temp + " bytes");

                ByteArrayInputStream bai = new ByteArrayInputStream(out.toByteArray());
                results = EightyLegResults.readFile(bai);
            }
        } catch (IOException ex) {
            ByteArrayInputStream bai;
            System.err.println("Error reading the ZIP -> " + data + ex);
            return null;
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
                if (zin != null) {
                    zin.close();
                }
            } catch (IOException ioe) {
                System.err.println("Error closing resources" + ioe);
            }
        }
        return results;
    }
}