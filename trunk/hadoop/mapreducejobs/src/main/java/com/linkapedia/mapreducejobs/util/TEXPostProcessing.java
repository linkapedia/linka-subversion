/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.mapreducejobs.util;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Sahar Ebadi
 */
public class TEXPostProcessing {
    public static String perform(String finalTitle) {
        finalTitle = finalTitle.trim();
        finalTitle = checkForKillingList(finalTitle);
        finalTitle = adjustTitleLenght(finalTitle);
        finalTitle = removeDate(finalTitle);
        finalTitle = removeOnlyNumbers(finalTitle);
        finalTitle = trimEndOfTitle(finalTitle);
        return finalTitle;
    }
    
    public static String trimSpaceAndCharacter(String text){
        String pattern = "(^.*)((_|-|<|>|»|«|\\*|\\s)+$)";  //"(^.*)([#|_|-|~|¿|!|¦|<|>|»|«|\\*|\\|\\/|\\s]+$)"
        String newtext = text.replaceAll(pattern, "$1");  //$1 represent the captured group
        return newtext;
    }
    public static String trimEndOfTitle(String text){// this is to ommit any character or space with the lengh of one or two from th end of title
        String pattern = "(^.*)((\\s+.{1,2})\\s*+$)";
        String newtext = text.replaceAll(pattern, "$1");
        return newtext.trim();
    }
    
    private static boolean isInKillingList(String finalTitle) {// return true if the title: 1)it includes a black list word/s (e.g: "Not Found Page"), 2)it is exactly equal to a blavk list word/s (e.g: "About") , this is case _INsensitive
        boolean isInKillingList;
        isInKillingList = finalTitle.matches("(?i)(.*(No width|No width\\/height causes infinite loops|monthly archive|Service surchargé|Service overloaded|Not Found|Execution permission cannot be acquired|The page cannot be found|Untitled Page|Untitled Document|google search|Privacy Policy|bad request|Forbidden|XML-RPC server accepts POST requests only|Gateway Time-Out|Bad Gateway|Server Error|Request-URL Too Large|No Content|303 See Other|Request Time-Out|409 Conflict|410 Gone|415 Unsupported Media Type|501 Not Implemented|HTTP Version not supported|401 Unauthorized|403 Forbidden/Access Denied|Request Timeout|Internal Error|501 Not Implemented|Service Unavailable|Connection Refused by Host|File Contains No Data|Bad File Request|Failed DNS Lookup|Host Unavailable|Unable to Locate Host|Network Connection Refused by the Server|login page|login|home.html|index.html|index.htm|Internal Server Error|Error|last update|under construction|Erreur|404|The page cannot be displayed).*|home|About|Terms of Use|terms of conditions|Terms And Conditions|Main|contact|contact us|email us|about us|download|downloads|Document Moved|FAQ|Sitemap|Explore More|News|Discounts|Discount|welcome)");// this is case _Insensitive (?i)
        return isInKillingList;
    }
    private static String checkForKillingList(String finalTitle) {// removes the WholeTitle if: 1)it includes a black list word/s (e.g: "Not Found Page"), 2)it is exactly equal to a blavk list word/s (e.g: "About") , this is case _INsensitive
        boolean killTheTitle;
        if(isInKillingList(finalTitle)){
            finalTitle = "Kill the page!";
        }
        return finalTitle;
    }
    
    public static boolean isOnlyDate(String finalTitle){//finds if the title only includes a time expression
        //^((\d{2}|\d{4}|mah)+(\s+|\\)+(\d{2}|\d{2}|mah)+(\s+|\\))
        boolean isDate = finalTitle.matches("^(?i)((\\d{1,2}|\\d{4}|(Jan|Feb|Mar|Apr|May|June|July|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December))+\\s*(\\s+|-|_|\\/|\\\\|:|,|\\.)\\s*(\\d{1,2}|\\d{4}|(Jan|Feb|Mar|Apr|May|June|July|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December))+\\s*(\\s*|-|_|\\/|\\\\|:|,|\\.)\\s*(\\d{1,2}|\\d{4}|(Jan|Feb|Mar|Apr|May|June|July|Aug|Sept|Oct|Nov|Dec|January|February|March|April|May|June|July|August|September|October|November|December))*)");
        return isDate;
    }
    public static String removeDate(String finalTitle){//remove the title if it only includes a time expression
        if(isOnlyDate(finalTitle)){
            finalTitle = "Kill the page!";
        }
        return finalTitle;
    }

    public static boolean isOnlyNumbers(String text){// find titles like: 404
        boolean isOnlyNumbers = text.matches("^\\d*$");
        return isOnlyNumbers;
    }
    public static String removeOnlyNumbers(String text){// to remove titles like: 404
        if(isOnlyNumbers(text)){
            text = "Kill the page!";
        }
        return text;
    }
    private static String adjustTitleLenght(String finalTitle){
        String[] titleInWords = finalTitle.split(" ");
        String shortTitle = ""; 
        if(titleInWords.length>10){
            for(int i=0;i<10;i++){
                shortTitle = shortTitle+titleInWords[i]+" ";
            }
            return shortTitle;
        }else{
            return finalTitle;
        }
    }
    
    public static boolean isABadTitle(String feasibleTitle,String url){// return true is it is a bad title (includes only date, only number or only domain or domain+date or includes blacklist words)
        boolean isABadTitle = false;
        try {
            if(TEXDataCleaning.removeOnlyDomainDate(feasibleTitle,url).equalsIgnoreCase("kill the page!")){
                isABadTitle = true;
            }
            if(isInKillingList(feasibleTitle) || isOnlyDate(feasibleTitle) || isOnlyNumbers(feasibleTitle) ){
                isABadTitle = true;
            }
        } catch (Exception ex) {
            Logger.getLogger(TEXPostProcessing.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isABadTitle;
    }
}
