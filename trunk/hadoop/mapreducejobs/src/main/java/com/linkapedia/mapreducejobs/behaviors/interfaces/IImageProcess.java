
package com.linkapedia.mapreducejobs.behaviors.interfaces;

import org.linkapedia.images.beans.ImageInfo;


/**
 *
 * @author andres
 */
public interface IImageProcess {
    
    public ImageInfo generateImage(String key);
}
