package com.linkapedia.mapreducejobs.behaviors.concrete;

import com.linkapedia.mapreducejobs.beans.ClassifierInfo;
import com.linkapedia.mapreducejobs.behaviors.interfaces.ISentencesProcess;
import com.linkapedia.mapreducejobs.util.ParserProxy;
import com.linkapedia.mapreducejobs.util.SentenceExtractorProxy;
import com.linkapedia.mapreducejobs.util.SocketClient;
import com.linkapedia.mapreducejobs.util.StringUtils;
import com.linkapedia.mapreducejobs.util.TitleExtractionUtil;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andres
 */
public class SentenceExtractorBehavior implements ISentencesProcess {

   private static final ResourceBundle systemConfig = ResourceBundle.getBundle("classifier/config");
    private static final int MIN_SETENCES ;
    private static final int MAX_SENTENCES ;
    
    static{
        MIN_SETENCES = Integer.parseInt(systemConfig.getString("classifier.min.setences"));
        MAX_SENTENCES =  Integer.parseInt(systemConfig.getString("classifier.max.setences"));
    }

    
    public ClassifierInfo generateSentences(String htmlString,String url) {
        ClassifierInfo response = new ClassifierInfo();
        String classifierInfoResponse = null;
        StringBuilder sentencesToScore = new StringBuilder();
        if ((!"".equals(htmlString)) && (htmlString != null)) {
            try {
                System.out.println("Getting Sentences...");
                String sentences = null;
                try {
                    System.out.println("***************** Calling Reverb *****************");
                    sentences = SentenceExtractorProxy.extractFrom(htmlString, true);
                    System.out.println("***************** Ending Reverb *****************");
                } catch (Exception ex) {
                    Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
                }
                if ((sentences != null) && (!"".equals(sentences))) {
                    String[] sentencesArray = sentences.split("(\r\n|\n)");
                    if (sentencesArray.length > MIN_SETENCES) {
                        if (sentencesArray.length > MAX_SENTENCES) {
                            for (int i = 0; i <= MAX_SENTENCES; i++) {
                                sentencesToScore.append(sentencesArray[i]);
                                sentencesToScore.append("\n");
                            }
                            sentences = sentencesToScore.toString();
                        }
                        String title = TitleExtractionUtil.titleProcess(htmlString, sentences, url);
                        if (title == null || "".equals(title)) {
                            title = StringUtils.setTitle(sentences);
                        }

                        response.setTitle(title);
                        response.setSentences(sentences);

                        System.out.println("Calling the Classifier...");
                        try {
                            classifierInfoResponse = SocketClient.sendAndReciveData(response.getTitle() + "\n" + response.getSentences());
                            if ((classifierInfoResponse != null) && (!classifierInfoResponse.trim().isEmpty())) {
                                int linesCount = new StringTokenizer(classifierInfoResponse, "\r\n").countTokens();
                                if (linesCount > 1) {
                                    response.setclassifierScores(classifierInfoResponse);
                                }
                            }

                        } catch (IOException ex) {
                            Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

            } catch (Exception ex) {
                Logger.getLogger(SentenceExtractorBehavior.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return response;
    }
}
