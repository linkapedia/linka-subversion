package com.linkapedia.mapreducejobs.util;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.Doc_NodeTemp;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeDocumentTemp;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.handlers.NodeAPIHandler;
import com.intellisophic.linkapedia.generic.utils.StringUtils;
import com.linkapedia.mapreducejobs.beans.JsonObjectDocNodeContent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONObject;

/**
 *
 * @author Bancolombia
 */
public class ParserClassifierFile {

    /**
     *
     * @param content
     * @param title
     * @param url
     * @param docId
     * @return
     */
    static Date dateTime = new Date();

    public static Doc_Node classifierInfoToDoc_Node(String content, String title, String url, String docId, String sentences) {
        Doc_Node doc = null;
        long timeStamp = dateTime.getTime();
        String[] lines = content.split("\\r?\\n");
        if (lines.length > 1) {
            boolean ASC = true;
            boolean DESC = false;
            float roc;
            Map<String, Float> unsortMap = new HashMap<String, Float>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < lines.length; i++) {
                String[] line;
                if (i > 0) {
                    line = lines[i].split(",");
                    roc = convert(line[9]);
                    if (roc >= 50.0) {
                        if (line[0] != null && line[0].length() != 0) {
                            sb.append(line[0]);
                            sb.reverse();
                            unsortMap.put(sb.toString(), convert(line[6]));
                            sb.setLength(0);
                        }
                    }
                }
            }
            if (!unsortMap.isEmpty()) {
                Map<String, Float> sortedMapAsc = sortByComparator(unsortMap, ASC);
                doc = new Doc_Node();
                doc.setDocId(docId);
                try {
                    doc.setNodeList(FileUtils.compressString(convertToJson(sortedMapAsc, url, timeStamp, title).toString()));
                    doc.setSentences(FileUtils.compressString(sentences));
                } catch (IOException ex) {
                    System.out.println("Exception Compressing the content of Doc_Node " + ex.getMessage());
                } catch (Exception ex) {
                    System.out.println("Exception generating content of doc node" + ex.getMessage());
                }
            }
        }
        return doc;
    }
    
       public static Doc_NodeTemp classifierInfoToDoc_Nodetemp(String content, String title, String url, String docId, String sentences) {
     
        Doc_NodeTemp doc = null;
        long timeStamp = dateTime.getTime();
        String[] lines = content.split("\\r?\\n");
        if (lines.length > 1) {
            boolean ASC = true;
            boolean DESC = false;
            float roc;
            Map<String, Float> unsortMap = new HashMap<String, Float>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < lines.length; i++) {
                String[] line;
                if (i > 0) {
                    line = lines[i].split(",");
                    roc = convert(line[9]);
                    //if (roc >= 50.0) {
                        if (line[0] != null && line[0].length() != 0) {
                            sb.append(line[0]);
                            sb.reverse();
                            unsortMap.put(sb.toString(), convert(line[6]));
                            sb.setLength(0);
                        }
                    //}
                }
            }
            if (!unsortMap.isEmpty()) {
                Map<String, Float> sortedMapAsc = sortByComparator(unsortMap, ASC);
                doc = new Doc_NodeTemp();
                doc.setDocId(docId);
                try {
                    doc.setNodeList(FileUtils.compressString(convertToJson(sortedMapAsc, url, timeStamp, title).toString()));
                    doc.setSentences(FileUtils.compressString(sentences));
                } catch (IOException ex) {
                    System.out.println("Exception Compressing the content of Doc_Node " + ex.getMessage());
                } catch (Exception ex) {
                    System.out.println("Exception generating content of doc node" + ex.getMessage());
                }
            }
        }
        return doc;
    }

    private static JSONObject convertToJson(Map<String, Float> sortedMap, String url, long timeStamp, String title) {
        JsonObjectDocNodeContent content = new JsonObjectDocNodeContent();
        content.setUrl(url);
        content.setTimeStamp(timeStamp);
        content.setTitle(title);

        List<String> nodeIds = new ArrayList<String>();
        for (Map.Entry<String, Float> entry : sortedMap.entrySet()) {
            nodeIds.add(entry.getKey());
        }
        content.setNodes(nodeIds);
        JSONObject jsonObject = JSONObject.fromObject(content);
        System.out.println(jsonObject.toString());
        return jsonObject;

    }

    public static List<NodeDocumentTemp> classifierInfoToNodeDocumentTemp(String content, String title, String url, String docId) {
        List<NodeDocumentTemp> docs = new ArrayList<NodeDocumentTemp>();
        try {
            if (!"".equals(content) && content != null) {
                String[] lines = content.split("\\r?\\n");
                float roc;
                for (int i = 0; i < lines.length; i++) {
                    if (i > 0) {
                        String[] line;
                        line = lines[i].split(",");
                        roc = convert(line[9]);
                         if (roc >= 50.0) {
                        NodeDocumentTemp doc = new NodeDocumentTemp();
                        doc.setNodeID(StringUtils.revert(line[0]));
                        doc.setDocID(docId);
                        doc.setDocTitle(title);
                        doc.setDocURL(url);
                        doc.setScore01(roc);//roc
                        doc.setScore02(convert(line[1]));//doc frecuency
                        doc.setScore03(0.0f);//
                        doc.setScore04(convert(line[3]));//sumary frecuency
                        doc.setScore05(convert(line[2]));//doc coverage
                        doc.setScore06(convert(line[4]));//sumary coverage
                        doc.setScore07(convert(line[5]));//fxc
                        doc.setScore08(convert(line[6]));//fxcx15
                        doc.setScore09(0.0f);
                        doc.setScore10(0.0f);
                        doc.setScore11(convert(line[6]));//fxcx20
                        doc.setScore12(0.0f);
                        doc.setPagerank(convert(line[8]));//binary score
                        long timeStamp = dateTime.getTime();
                        doc.setTimeStamp(timeStamp);
                        docs.add(doc);
                         }
                    }
                }
            }
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        return docs;
    }

    /**
     *
     * @param content
     * @param title
     * @param url
     * @param docId
     * @return
     */
    public static List<NodeDocument> classifierInfoToNodeDocument(String content, String title, String url, String docId) {
        List<NodeDocument> docs = new ArrayList<NodeDocument>();
        try {
            if (!"".equals(content) && content != null) {
                String[] lines = content.split("\\r?\\n");
                float roc;
                for (int i = 0; i < lines.length; i++) {
                    if (i > 0) {
                        String[] line;
                        line = lines[i].split(",");
                        roc = convert(line[9]);
                        if (roc >= 50.0) {
                            NodeDocument doc = new NodeDocument();
                            doc.setNodeID(StringUtils.revert(line[0]));
                            doc.setDocID(docId);
                            doc.setDocTitle(title);
                            doc.setDocURL(url);
                            doc.setScore01(convert(line[9]));
                            doc.setScore02(convert(line[1]));
                            doc.setScore03(0.0f);
                            doc.setScore04(convert(line[3]));
                            doc.setScore05(convert(line[2]));
                            doc.setScore06(convert(line[4]));
                            doc.setScore07(convert(line[5]));
                            doc.setScore08(convert(line[6]));
                            doc.setScore09(0.0f);
                            doc.setScore10(0.0f);
                            doc.setScore11(convert(line[6]));
                            doc.setScore12(0.0f);
                            doc.setPagerank(convert(line[8]));
                            long timeStamp = dateTime.getTime();
                            doc.setTimeStamp(timeStamp);
                            docs.add(doc);
                        }
                    }
                }
            }
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        return docs;
    }

    /**
     *
     * @param content
     * @param title
     * @param url
     * @param docId
     * @return
     */
    public static List<DocumentNode> classifierInfoToDocumentNode(String content, String title, String url, String docId) {
        List<DocumentNode> docs = new ArrayList<DocumentNode>();
        try {
            if (!"".equals(content) && content != null) {
                String[] lines = content.split("\\r?\\n");
                float roc;
                for (int i = 0; i < lines.length; i++) {
                    if (i > 0) {
                        String[] line;
                        line = lines[i].split(",");
                        roc = convert(line[9]);
                        if (roc >= 50.0) {
                            DocumentNode doc = new DocumentNode();
                            doc.setNodeID(StringUtils.revert(line[0]));
                            doc.setDocID(docId);
                            doc.setTitle(title);
                            doc.setURL(url);
                            doc.setScore01(convert(line[9]));
                            doc.setScore02(convert(line[1]));
                            doc.setScore03(0.0f);
                            doc.setScore04(convert(line[3]));
                            doc.setScore05(convert(line[2]));
                            doc.setScore06(convert(line[4]));
                            doc.setScore07(convert(line[5]));
                            doc.setScore08(convert(line[6]));
                            doc.setScore09(0.0f);
                            doc.setScore10(0.0f);
                            doc.setScore11(convert(line[6]));
                            doc.setScore12(0.0f);
                            doc.setPagerank(convert(line[8]));
                            long timeStamp = dateTime.getTime();
                            doc.setTimeStamp(timeStamp);
                            docs.add(doc);
                        }
                    }
                }
            }
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        return docs;
    }

    /**
     *
     * @param data
     * @return
     */
    private static float convert(String data) {
        float value;
        try {
            value = Float.parseFloat(data);
        } catch (NumberFormatException e) {
            value = 0;
        }
        return value;
    }

    private static Map<String, Float> sortByComparator(Map<String, Float> unsortMap, final boolean order) {
        List<Map.Entry<String, Float>> list = new LinkedList<Map.Entry<String, Float>>(unsortMap.entrySet());
        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Float>>() {
            public int compare(Map.Entry<String, Float> o1,
                    Map.Entry<String, Float> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());
                }
            }
        });
        // Maintaining insertion order with the help of LinkedList
        Map<String, Float> sortedMap = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}
