package com.linkapedia.mapreducejobs.util;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.intellisophic.linkapedia.amazon.services.S3.bo.ManageBucket;
import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.Doc_NodeTemp;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeDocumentTemp;
import com.intellisophic.linkapedia.api.handlers.Doc_NodeAPIHandler;
import com.intellisophic.linkapedia.api.handlers.DocumentNodeAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeDocumentAPIHandler;
import com.intellisophic.linkapedia.generic.utils.DigestUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 *
 * @author andres
 */
public class StoreUtil {

    private static ManageBucket storeImages;
    private static ManageBucket storeSenteces;
    private static ManageBucket storeTripes;
    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("classifier/config");

    static {
        storeImages = new ManageBucket(systemConfig.getString("documentimage.bucket"));
        storeSenteces = new ManageBucket(systemConfig.getString("documentextracted.bucket"));
        storeTripes = new ManageBucket(systemConfig.getString("documenttriplesextracted.bucket"));

    }

    public static boolean storeDynamoNodeDocumentTemp(String infoToStore, String key, String title) {

        boolean stored = false;
        try {
            List<NodeDocumentTemp> nodeDocumentsTemp = ParserClassifierFile.classifierInfoToNodeDocumentTemp(infoToStore, title, key, DigestUtils.getMD5(key));
            if (nodeDocumentsTemp != null && !nodeDocumentsTemp.isEmpty()) {
                System.out.println("NodeDocuments Transformed: " + nodeDocumentsTemp.size());
                for (NodeDocumentTemp node : nodeDocumentsTemp) {
                    System.out.println("Save NodeDocument{" + node.getNodeID() + "} in Dynamo");
                    NodeDocumentAPIHandler.saveNodeDocumentTemp(node);
                    stored = true;
                }
            }

        } catch (Exception ex) {
            stored = false;
        }
        return stored;
    }

    public static boolean storeDynamoNodeDocument(String infoToStore, String key, String title) {

        boolean stored = false;
        try {
            List<NodeDocument> nodeDocuments = ParserClassifierFile.classifierInfoToNodeDocument(infoToStore, title, key, DigestUtils.getMD5(key));
            if (nodeDocuments != null && !nodeDocuments.isEmpty()) {
                System.out.println("NodeDocuments Transformed: " + nodeDocuments.size());
                for (NodeDocument node : nodeDocuments) {
                    System.out.println("Save NodeDocument{" + node.getNodeID() + "} in Dynamo");
                    NodeDocumentAPIHandler.saveNodeDocument(node);
                    List<NodeDocument> test=   NodeDocumentAPIHandler.getNodeDocumentsByNodeId(node.getNodeID());
                }
            }

        } catch (Exception ex) {
            stored = false;
        }
        return stored;
    }


    public static boolean storeDynamoDoc_Node(String infoToStore, String key, String title,String sentences) {
        boolean stored = false;

        try {
            Doc_Node doc = ParserClassifierFile.classifierInfoToDoc_Node(infoToStore, title, key, DigestUtils.getMD5(key),sentences);
            if (doc != null) {
                System.out.println("Save NodeDocument{" + doc.getDocId() + "} in Dynamo");
                Doc_NodeAPIHandler.saveDoc_Node(doc);
            }

        } catch (Exception ex) {
            stored = false;
        }
        return stored;

    }
    
        public static boolean storeDynamoDoc_NodeTemp(String infoToStore, String key, String title,String sentences) {
        boolean stored = false;

        try {
            Doc_NodeTemp doc = ParserClassifierFile.classifierInfoToDoc_Nodetemp(infoToStore, title, key, DigestUtils.getMD5(key),sentences);
            if (doc != null) {
                System.out.println("Save NodeDocument{" + doc.getDocId() + "} in Dynamo");
                Doc_NodeAPIHandler.saveDoc_NodeTemp(doc);
            }

        } catch (Exception ex) {
            stored = false;
        }
        return stored;

    }

    public static boolean storeSentencesS3(String infoToStore, String key) {
        boolean storeS3 = false;
        InputStream inputStream = FileUtils.convertStringToStream(infoToStore);
        storeS3 = storeSenteces.saveData(new StringBuilder().append(systemConfig.getString("documentextracted.path.output")).append(DigestUtils.getMD5(key)).toString(), inputStream);
        return storeS3;


    }

    public static boolean storeSentencesRenditionS3(String infoToStore, String key) {
        boolean storeS3 = false;
        InputStream inputStream = FileUtils.convertStringToStream(infoToStore);
        storeS3 = storeTripes.saveData(new StringBuilder().append(systemConfig.getString("documenttriplesextracted.path.output")).append(DigestUtils.getMD5(key)).toString(), inputStream);
        return storeS3;


    }

    public static boolean storeS3(InputStream infoToStore, String fileName, ObjectMetadata meta) {
        boolean storeS3 = false;
        storeS3 = storeImages.saveData(systemConfig.getString("documentimage.path.output") + fileName, infoToStore, meta);
        return storeS3;
    }

    public static boolean storeLocal(String content, String path, String filename) {
             FileWriter fileWriter = null;
        try {
            File folder = new File(path);
            if (!folder.exists()) {
                folder.mkdir();
            }

            File newTextFile = new File(path + "/" + filename);
            fileWriter = new FileWriter(newTextFile);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
        } finally {
            try {
                fileWriter.close();
            } catch (IOException ex) {
            }
        }
        return false;
    }
}
