/* ng-infinite-scroll - v1.0.0 - 2013-02-23 - http://binarymuse.github.io/ngInfiniteScroll/ */
angular.module('infinite-scroll', [])
	.directive('infiniteScroll', function($rootScope, $window, $timeout) {
		return {
			link: function(scope, elem, attrs) {
				var checkWhenEnabled, handler, scrollEnabled;
				$window = angular.element($window);
				scrollEnabled = true;
				checkWhenEnabled = false;
				if (attrs.infiniteScrollDisabled != null) {
					scope.$watch(attrs.infiniteScrollDisabled, function(value) {
						scrollEnabled = !value;
						if (scrollEnabled && checkWhenEnabled) {
							checkWhenEnabled = false;
							return handler();
						}
					});
				}
				handler = function() {
					var elementBottom, remaining, shouldScroll, windowBottom;
					windowBottom = $window.height() + $window.scrollTop();
					elementBottom = elem.offset().top + elem.height();
					remaining = elementBottom - windowBottom;
					var distance = attrs.infiniteScrollDistance ? parseInt(attrs.infiniteScrollDistance) : 0;
					shouldScroll = remaining <= $window.height() * distance;
					if (shouldScroll && scrollEnabled) {
						if ($rootScope.$$phase) {
							return scope.$eval(attrs.infiniteScroll);
						}
						else {
							return scope.$apply(attrs.infiniteScroll);
						}
					}
					else if (shouldScroll) {
						return checkWhenEnabled = true;
					}
				};
				$window.on('scroll', handler);
				scope.$on('$destroy', function() {
					return $window.off('scroll', handler);
				});
				return $timeout((function() {
					if (attrs.infiniteScrollImmediateCheck) {
						if (scope.$eval(attrs.infiniteScrollImmediateCheck)) {
							return handler();
						}
					}
					else {
						return handler();
					}
				}), 0);
			}
		};
	})
