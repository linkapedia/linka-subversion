angular.module('Linkapedia', ['jt_AJS', 'ngSanitize', 'infinite-scroll'])

	.config(function($routeProvider, $httpProvider, $provide, $locationProvider) {
		$routeProvider
			.when('/', { templateUrl: "partials/home.html", controller: 'HomeCtrl' } )
			.when('/page/:topic1/:topic2/:tid', { templateUrl: "partials/topic_docs.html", controller: 'TopicCtrl' } )
			.when('/digest/:title/:tid/:did', { templateUrl: "partials/digest.html", controller: 'DigestCtrl' } )
			.when('/search', { templateUrl: "partials/search.html", controller: 'SearchCtrl' } )
			.when('/search/:query', { templateUrl: "partials/search.html", controller: 'SearchCtrl' } )
			.when('/settings', { templateUrl: "partials/settings.html", controller: 'SettingsCtrl' } )
			.when('/login', { templateUrl: "login.html", controller: 'LoginCtrl' } )
			.otherwise({redirectTo: '/'});

		$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		
		$locationProvider.hashPrefix('!');
		//$locationProvider.html5Mode(true);
	})

	.run(['$rootScope', function($rootScope) {
		var _getTopScope = function() {
		  return $rootScope;
		  //return angular.element(document).scope();
		};

		$rootScope.ready = function() {
		  var $scope = _getTopScope();
		  $scope.status = 'ready';
		  if(!$scope.$$phase) $scope.$apply();
		};
		$rootScope.loading = function() {
		  var $scope = _getTopScope();
		  $scope.status = 'loading';
		  if(!$scope.$$phase) $scope.$apply();
		};
		$rootScope.$on('$routeChangeStart', function() {
		  _getTopScope().loading();
		});
	  }])

	/*.config(function(AnalyticsProvider, function() {
        // initial configuration
        AnalyticsProvider.setAccount('UA-39745928-4');
        // track all routes (or not)
        AnalyticsProvider.trackPages(true);
        AnalyticsProvider.setDomainName('interestplace.com');
    }))*/

	.service('DebugLog', function() {
		this.logTxt = '';

		this.log = function(txt, dateTime) {
			if (dateTime) {
				txt = txt + ', ' + dateTime.toLocaleTimeString();
			}
			this.logTxt = txt + "<br>" + this.logTxt; // most recent first
		}

		this.benchmark = function(reps, lbl, start, stop, desc) {
			var elapsed = (stop - start) / 1000;
			this.log('<b>' + reps + ' ' + lbl + '</b> in ' + elapsed.toFixed(1) + ' secs., ' + desc, stop);
		}

	})



	.service('Settings', function(jt_AJS_LocalStorage) {
		this.settingsKey = 'Linkapedia_REST_API_demo';
		var srvc_Settings = this;

		this.reset = function(saveIt) {
			srvc_Settings.vals = {
				'topicPhotos': true,
				'topicLinks': 'show',
				'docLinkPhoto': true,
				'knowMore': 'Google',
				'bcSortDrop': '',
				'bcAutoOpen': true
			};
			if (saveIt) {
				jt_AJS_LocalStorage.set(srvc_Settings.settingsKey, srvc_Settings.vals);
			}
		}

		this.reset();
		jt_AJS_LocalStorage.get(this.settingsKey).then(
			function(settings) {
				for (var prop in settings) {
					srvc_Settings.vals[prop] = settings[prop];
				}
			}
		)

		this.setPref = function(prefName, prefValue) {
			//console.log('setPref', prefName, prefValue, srvc_Settings.settingsKey, srvc_Settings.vals);
			delete srvc_Settings.vals.bcDropSort; // temporary; remove unused setting
			srvc_Settings.vals[prefName] = prefValue;
			jt_AJS_LocalStorage.set(srvc_Settings.settingsKey, srvc_Settings.vals);
		}

		this.CSS = function(prefName, prefValue, matchTxtYes, matchTxtNo) {
			//console.log('CSS', prefName, typeof srvc_Settings.vals[prefName], typeof prefValue,srvc_Settings.vals[prefName] == prefValue);
			return (srvc_Settings.vals[prefName] == prefValue) ? (matchTxtYes || 'active') : (matchTxtNo || '');
		}

	})



	.service('AppAlerts', function($rootScope, $timeout, DebugLog) {
		this.list = [];
		this.loading = {};
		this.numReqs = 0;
		var wasLoading = false;
		var srvc_Alerts = this;

		this.init = function(clrAll) {
			window.clearTimeout(srvc_Alerts.timer);
			$timeout(function() {
				srvc_Alerts.list = [];
				if (clrAll) {
					srvc_Alerts.loading = {};
				}
			}, 0);
		}

		$rootScope.$on('$routeChangeSuccess', function() {
			srvc_Alerts.init();
		});

		this.alert = function(msgBold, msgTxt, typeAlert) {
			window.clearTimeout(srvc_Alerts.timer);
			DebugLog.log(msgBold + ' ' + msgTxt, new Date());
			srvc_Alerts.list.unshift({"msgBold": msgBold, "msgTxt": msgTxt, "typeAlert": typeAlert || 'alert-error'});
			srvc_Alerts.timer = setTimeout(function() {
				srvc_Alerts.init(true); // clear alerts/"Loading..."
			}, 10000); // 10 sec.
		}

		this.setLoading = function(req) {
			srvc_Alerts.loading[req] = 1;
			if (!wasLoading) { // start "Loading..." benchmark
				srvc_Alerts.start = new Date();
				srvc_Alerts.numReqs = 0;
				wasLoading = true;
			}
		}

		this.clrLoading = function(req) {
			delete srvc_Alerts.loading[req];
		}

		this.loadingOn = function() {
			var isOn = false;
			for (var prop in srvc_Alerts.loading) {
				isOn = true;
				break; // one is enough
			}
			if (wasLoading && !isOn) { // "Loading..." benchmark
				DebugLog.benchmark(srvc_Alerts.numReqs, 'reqs', srvc_Alerts.start, new Date(), 'Loading...');
				wasLoading = false;
			}
			return isOn;
		}

	})



	.service('REST_API', function($http, AppAlerts, DebugLog, $timeout) {
		this.REST_url = "http://linkapedia-rest-dev.elasticbeanstalk.com";
		var srvc_REST_API = this;
		this.get = function(req, callBack, useCache, retry) {
			retry = retry || 0; // internal re-try counter
			if (typeof useCache == "undefined") {
				useCache = true;
			}

			function tryAgain() {
				if (retry < 2) { // try three times
					$timeout(function() { // re-try w/o cache after 2 sec.
						srvc_REST_API.get(req, callBack, false, retry+1);
					}, 2000);
				}
			}

			AppAlerts.setLoading(req);
			AppAlerts.numReqs += 1;
			//console.log('REST_API', req);
			$http({method: 'GET', url: this.REST_url + req, orig_req: req, cache: useCache})
				.success(function(data, status, headers, config) {
					AppAlerts.clrLoading(req);
					//console.log('REST_API.success', data, status, headers, config);
					if (typeof data == "object") {
						callBack(data, config);
					}
					else {
						AppAlerts.alert('UNEXPECTED REPLY:', data + '<br>url: ' + req);
						tryAgain();
					}
				})
				.error(function(data, status, headers, config) {
					AppAlerts.clrLoading(req);
					AppAlerts.alert('ERROR!', status + '<br>url: ' + req);
					console.log("ERROR!", data, status, headers, config);
					tryAgain();
				});
		}
	})



	.service('TopicSrvc', function(REST_API, Settings, AppAlerts, $q) {
		this.titleCache = {};
		this.topicQueue = {};
		this.reqQueue = {};
		this.numHandlers = 0;
		this.parentMap = {};
		this.rootNodeId = '31947981';
		var srvc_Topics = this;

		this.init = function() {
			var d = $q.defer();
			if (srvc_Topics.initialized) {
				d.resolve();
			}
			else {
				srvc_Topics.initialized = true;
				srvc_Topics.get(srvc_Topics.rootNodeId, function(aTopic) { // load root node
					srvc_Topics.rootTaxonomy = aTopic.children;
					var num = 0;
					angular.forEach(aTopic.children, function(taxonomy) { // pre-cache top level
						num += 1;
						srvc_Topics.get(taxonomy.id, function() {
							num -= 1;
							if (num == 0) { // all requests completed
								d.resolve();
							}
						});
					});
				});
			}
			return d.promise;
		}

		this.get = function(topic_id, callBack) {
			REST_API.get('/topics/' + topic_id + '/info', function(aTopic, config) {
				srvc_Topics.titleCache[aTopic.id] = aTopic.title;
				var mapParents = parseInt(aTopic.parentId) < 1;
				angular.forEach(aTopic.children, function(topic, key) {
					//console.log(typeof topic, topic, key);
					srvc_Topics.titleCache[topic.id] = topic.title;
					if (mapParents) {
						srvc_Topics.parentMap[topic.id] = topic_id;
					}
				});
				if (mapParents) {
					//console.log('mapParents', topic_id, aTopic.parentId, parseInt(aTopic.parentId), srvc_Topics.parentMap);
					if (srvc_Topics.parentMap[topic_id]) {
						aTopic.parentId = srvc_Topics.parentMap[topic_id];
						aTopic.isTaxonomy = true;
					}
					else if (topic_id != srvc_Topics.rootNodeId) { // ignore root node
						AppAlerts.alert('ERROR:', 'No parent for ' + topic_id);
						console.log('mapParents', topic_id, aTopic.parentId, parseInt(aTopic.parentId));
					}
				}
				if (callBack) {
					callBack(aTopic, config);
				}
			});
		}

		this.topicTitle = function(topic_id, doc) {
			var doLookup = doc ? ((Settings.vals.topicLinks == 'show') || (doc.topicLinks == 'show')) : true;
			//console.log('topicTitle', topic_id, doLookup, doc);
			if (doLookup) {
				if (srvc_Topics.titleCache[topic_id]) {
					return srvc_Topics.titleCache[topic_id];
				}
				else {
					if (srvc_Topics.topicQueue[topic_id] != 1) {
						srvc_Topics.topicQueue[topic_id] = 1;
						srvc_Topics.handleQueue();
					}
					return topic_id;
				}
			}
			else return topic_id;
		}

		this.handleQueue = function(reset) {
			var qDepth = 0;
			var maxDepth = 500;
			var maxHandlers = 10;

			function getOne(callBack) {
				var gotOne = false;
				if (qDepth < maxDepth) {
					qDepth++;
					//console.log('handleQueue', qDepth, srvc_Topics.numHandlers);
					//console.log('handleQueue', srvc_Topics.topicQueue);
					for (var topic_id in srvc_Topics.topicQueue) {
						gotOne = true;
						delete srvc_Topics.topicQueue[topic_id];
						if (srvc_Topics.titleCache[topic_id] || srvc_Topics.reqQueue[topic_id]) getOne(callBack);
						else {
							srvc_Topics.reqQueue[topic_id] = 1;
							srvc_Topics.get(topic_id, function(aTopic) {
								delete srvc_Topics.reqQueue[topic_id];
								getOne(callBack);
							});
						}
						break; // just one!
					}
				}
				if (!gotOne) callBack();
			}

			if (srvc_Topics.numHandlers < maxHandlers) {
				srvc_Topics.numHandlers = srvc_Topics.numHandlers + 1;
				getOne(function() {
					srvc_Topics.numHandlers = srvc_Topics.numHandlers - 1;
				});
			}
		}

	})



	.service('BreadCrumbs', function(TopicSrvc, DebugLog) {
		this.breadCrumbs = [];
		this.newCrumbs = [];
		this.visited = {}; // topic pages viewed during session
		var srvc_BreadCrumbs = this;
		var bc_start; // benchmark
		var topBreadCrumbs = 39904581;

		this.addCrumb = function(crumb, reset) {
			if (reset) {
				this.newCrumbs = [crumb];
			}
			else {  // at beginning
				this.newCrumbs.unshift(crumb);
			}
		}

		this.build = function(topic_id, reset, callBack) {
			//console.log('bcBuild', topic_id);
			if (reset) {
				bc_start = new Date();
			}
			TopicSrvc.get(topic_id, function(aTopic) {

				function zInt(val) {
					var num = parseInt(val);
					return isNaN(num) ? 0 : num;
				}
								
				srvc_BreadCrumbs.addCrumb(aTopic, reset);
				if (aTopic.id != topBreadCrumbs && aTopic.parentId && (zInt(aTopic.parentId) > 0)) {										
					srvc_BreadCrumbs.build(aTopic.parentId, false, callBack);
				}
				else {
					srvc_BreadCrumbs.breadCrumbs = srvc_BreadCrumbs.newCrumbs;
					var crumbs = [];
					for (var i=1; i<srvc_BreadCrumbs.breadCrumbs.length; i++) {
						crumbs[i] = srvc_BreadCrumbs.title( srvc_BreadCrumbs.breadCrumbs[i] );
					}
					DebugLog.benchmark(srvc_BreadCrumbs.breadCrumbs.length, 'breadCrumbs', bc_start, new Date(), crumbs.join(' / '));
					if (callBack) callBack();
				}
			});
		}

		this.viewPage = function(topic_id) {
			this.visited[topic_id] = 1;
		}

		this.title = function(bc) {
			return bc ? bc.title || bc.name : "undefined";
		}

	})



	.controller('AppCtrl', function($scope, $routeParams, $location, AppAlerts, BreadCrumbs, TopicSrvc, REST_API, Settings, $timeout) {
		$scope.appVersion = Linkapedia_version; // set in index.php
		$scope.imgServerUrl = 'http://nodeimages.s3.amazonaws.com/';
		$scope.appAlerts = AppAlerts;
		$scope.crumbNav = BreadCrumbs;
		$scope.topicSrvc = TopicSrvc;
		$scope.settings = Settings;
		$scope.scrnSmall = false;
		$scope.scrollDistance = 2;
		var ctrl_App = this;
		var _scope = $scope;

		$scope.knowMore = function() {
			var crumbs = '';
			for (var i=1; i<BreadCrumbs.breadCrumbs.length; i++) {
				crumbs = crumbs + ' "' + BreadCrumbs.title( BreadCrumbs.breadCrumbs[i] ) + '"';
				//console.log("crumbs=", crumbs);
			}
			return encodeURIComponent($.trim(crumbs.toLowerCase()));
		}

		$scope.imgPath = function(midSt, tid) {
			tid = tid || $routeParams.tid; // 'tid' only from Search results
			return tid + (midSt || '/images/') + tid;
		}

		$scope.logoBar = $('#logoBar');

		this.chkWinSize = function() {
			var winH = $(window).height();
			_scope.scrollDistance = (winH > 900) ? 1 : ((winH < 500) ? 3 : 2);

			var winW = $(window).width();
			_scope.scrnSmall = (winW < 640) || (winH < 640);

			//console.log("chkWinSize", ctrl_App.tnFixed, $('#logoBar').height());
			$timeout(function() {
				_scope.logoBar.css('position', ctrl_App.tnFixed ? 'fixed' : 'relative');
				$('body').css("marginTop", ctrl_App.tnFixed ? _scope.logoBar.height() + 'px' : 0);
			}, 0);
		}
		$(window).resize(ctrl_App.chkWinSize);

		$scope.initPage = function(isFixed) {
			ctrl_App.tnFixed = isFixed;
			ctrl_App.chkWinSize();
		}

		$scope.ifPathNot = function(path) {
			return $location.path() != path;
		}

		$scope.addSearchResults = function(results) {; // global so they "stick"
			_scope.searchData.searchResults.push.apply(_scope.searchData.searchResults, results.topics);
			_scope.searchData.nextPage = parseInt(results.page) + 1;
			_scope.searchData.totPages = parseInt(results.totalPages);
		}

		$scope.clrSearchResults = function(qVal, clearPath, callBack) {
			_scope.searchData = { query: qVal ? qVal : '', searchResults: [], nextPage: 1, totPages:2 };
			if (clearPath) {
				$location.path("/search");
			}
			if (callBack) callBack();
		}
		$scope.clrSearchResults(); // self-execute to initialize!

	})



	.controller('TopicCtrl', function($scope, $routeParams, TopicSrvc, BreadCrumbs, REST_API, Settings, DebugLog, $timeout, $location, $window) {
		DebugLog.log('TopicCtrl=' + $routeParams.tid, new Date());			
		$scope.initPage(true);
		//$('body').removeClass('homePage');
		//console.log($routeParams.topic1, $routeParams.topic2, $routeParams.tid);
		$scope.pageDocs = [];
		$scope.taxTopics = [];
		BreadCrumbs.viewPage($routeParams.tid); // flag as visited
		$scope.busy = false;
		this.pageSize = 25;
		this.nextPage = 1;
		this.totPages = 2;
		this.initReady = false;
		var ctrl_Topics = this;
		var _scope = $scope;				

		$scope.haveRelated = function(doc) {
			if (Settings.vals.topicLinks != 'hide') {
				for (var i=0; i<doc.topics.length; i++) {
					if (doc.topics[i] != $routeParams.tid) {
						return true;
					}
				}
			}
			return false;
		}

		$scope.notSelf = function(topic) {
			return topic != $routeParams.tid;
		}

		$scope.totRelated = function(doc) {
			var tot = 0;
			for (var i=0; i<doc.topics.length; i++) {
				if ($scope.notSelf(doc.topics[i])) {
					tot += 1;
				}
			}
			return tot;
		}

		$scope.topicPhotos = function() {
			Settings.setPref('topicPhotos', !Settings.vals.topicPhotos); // toggle
		}

		$scope.loadDocuments = function() {
			//console.log('loadDocuments', ctrl_Topics.initReady, ctrl_Topics.nextPage, ctrl_Topics.totPages);
			if (ctrl_Topics.initReady && (ctrl_Topics.nextPage < ctrl_Topics.totPages)) {
				$scope.busy = true;
				var start = new Date();
				REST_API.get('/topics/' + $routeParams.tid + '/documents?limit=' + ctrl_Topics.pageSize + '&page=' + ctrl_Topics.nextPage, function(docs, config) {
					var parts = config.orig_req.split('/');
					if (parts[2] == $routeParams.tid) {
						DebugLog.benchmark(docs.documents.length, 'documents', start, new Date(), config.orig_req);
						ctrl_Topics.nextPage = parseInt(docs.page) + 1;
						ctrl_Topics.totPages = parseInt(docs.totalPages);
						if (docs.documents.length == 0) {
							//Analytic.trackPage('topicpage');							
							_kmq.push(['record', 'Bridge Node Page']);
							setStylesBridgePage();
							setAds('topics');
							refreshAddThis();
							_scope.taxTitle = _scope.pageCrumb.title || docs.name;
							_scope.taxTopics = _scope.pageCrumb.children;
						}
						else {
							//Analytic.trackPage('linkpage');
							_kmq.push(['record', 'Link Page']);
							setStylesLinkPage();
							setAds('link');
							refreshAddThis();
							_scope.pageDocs.push.apply(_scope.pageDocs, docs.documents);
						}
						_scope.busy = false;
					}
					else {
						DebugLog.benchmark(docs.documents.length, 'documents', start, new Date(), '<b>cached, not displayed:</b> ' + config.orig_req);
					}
				});
			}
			//Analytics.trackTrans();
			$('.boxLink').wookmark({
				  align: 'left',
				  autoResize: true,
				  container: $('.digests'),
				  itemWidth: 0,
				  offset: 8,
				  resizeDelay: 50,
				  flexibleWidth: 0,
				  onLayoutChanged: undefined
			});
			$('.digests').trigger('refreshWookmark');
		}

		TopicSrvc.init().then(function() {
			BreadCrumbs.build($routeParams.tid, true, function() {
				TopicSrvc.get($routeParams.tid, function(pageCrumb) {
					_scope.pageCrumb = pageCrumb;
					ctrl_Topics.initReady = true;
					_scope.loadDocuments();
				});
			});
		});

	})



	.controller('DigestCtrl', function($scope, $routeParams, BreadCrumbs, REST_API, TopicSrvc) {
		$scope.initPage(true);
		//$('body').removeClass('homePage');
		var _scope = $scope;
		//console.log($routeParams.title, $routeParams.tid, $routeParams.did);
		$scope.imgUrlDigest = "http://documentimage.s3.amazonaws.com/" + $routeParams.did + ".jpg";
		TopicSrvc.init().then(function() {
			BreadCrumbs.build($routeParams.tid, true, function() {
				TopicSrvc.get($routeParams.tid, function(pageCrumb) {
					_scope.pageCrumb = pageCrumb;
					REST_API.get('/topics/' + $routeParams.tid + '/documents/' + $routeParams.did + '/info', function(digest) {
						$scope.docDigest = digest;
					});
				});
			});
		});
		/*Analytic.trackPage('digestpage');
		Analytics.trackTrans();*/
		setAds('digest');
		refreshAddThis();
	})



	.controller('SearchCtrl', function($scope, REST_API, AppAlerts, BreadCrumbs, $routeParams) {
		$scope.initPage(false);
		//$('body').removeClass('homePage');
		$scope.busy = false;
		this.pageSize = 40;
		var ctrl_Search = this;
		var _scope = $scope;

		$scope.searchAlerts = [];

		$scope.closeAlert = function(idx) {
			$scope.searchAlerts.splice(idx, 1);
		}

		$scope.queryEncode = function() {
			return encodeURIComponent($scope.searchData.query);
		}

		$scope.loadSearch = function() {
			//console.log('searchData', _scope.searchData);
			if ((_scope.searchData.query != '') && (_scope.searchData.nextPage < _scope.searchData.totPages)) {
				$scope.busy = true;
				REST_API.get('/topics?query=' + $scope.queryEncode() + '&limit=' + ctrl_Search.pageSize + '&page=' + $scope.searchData.nextPage, function(results) {
					_scope.addSearchResults(results);
					if (results.topics.length == 0) {
						_scope.searchAlerts.unshift(_scope.searchData.query); // most recent first!
					}
					else {
						_scope.searchAlerts = [];
					}
					_scope.busy = false;
				});
			}
		}

		$scope.searchStart = function() {
			$scope.clrSearchResults($scope.searchData.query, true, $scope.loadSearch);
		}

		if ($routeParams.query) {
			$scope.clrSearchResults($routeParams.query, false, $scope.loadSearch);
		}
	})



	.controller('SettingsCtrl', function($scope, DebugLog, TopicSrvc, Settings) {
		$scope.initPage(false);
		//$('body').removeClass('homePage');
		$scope.debugLog = DebugLog;
		//console.log(TopicSrvc.topicQueue, TopicSrvc.titleCache);
		console.log('settings:', Settings.vals);
		$scope.opSelected = function(val) {
		}

		$scope.setKnowMore = function(selTag) {
			Settings.setPref('knowMore', Settings.vals.knowMore)
		}

	})



	.controller('HomeCtrl', function($scope, TopicSrvc) {
		/*Analytic.trackPage('homepage');
		Analytics.trackTrans();*/
		_kmq.push(['record', 'Viewed Homepage']);
		//$('body').addClass('homePage');		
		TopicSrvc.init().then(function() {
			// no need to wait?
		});
	})



	.controller('LoginCtrl', function($scope) {
		$scope.initPage(false);
		//$('body').removeClass('homePage');
	})
	
	.directive('Adsense', function() {
	  return {
		restrict: 'A',
		transclude: true,
		replace: true,
		template: '<div ng-transclude></div>',
		link: function ($scope, element, attrs) {}
	  }
	})

	.directive('bcMagic', function(BreadCrumbs, Settings, $timeout) {
		return function(scope, element, attrs) {

			scope.colClass = '';

			scope.keepOnScrn = function() {
				//console.log('keepOnScrn');
				$timeout(function() {
					var mW = menu.width(),
						winW = $(window).width(),
						mPos = element.offset();
					//console.log('keepOnScrn', element.hasClass('open'), mPos.left, mW, menu.width(), winW);
					if (element.hasClass('open') && (mPos.left + mW > winW)) {
						var newLeft = winW - (mPos.left + mW) - 10; // 10px right margin
						//console.log('newLeft', newLeft, winW, mPos, mW);
						if (winW - mW < 0) {
							newLeft = -mPos.left;
						}
						$(menu).css("left", newLeft);
					}
					else {
						$(menu).css("left", 0); // restore default
					}
				}, 0);
			}

			function makeColumns() {
				//console.log("makeColumns");
				var cols = [];
				if (scope.bc.children.length < 13) { // revert to single column defaults
					menu.width("auto");
				}
				if (menu[0].style.width == "auto") { // also set by CSS media query
					cols = [scope.bc.children];
				}
				else {
					var numCols = Math.round(menu.width() / 260); // 260 from CSS: #CrumbNav .dropdown-menu div.ddMultiCol
					var numRows = Math.ceil(scope.bc.children.length / numCols);
					for (var i=0; i<numCols; i++) {
						var idx = i * numRows;
						cols[i] = scope.bc.children.slice(idx, idx + numRows);
					}
				}
				scope.colClass = (cols.length > 1) ? 'ddMultiCol' : '';
				scope.ddmCols = cols;
			}

			function colsOnScrn() {
				makeColumns();
				scope.keepOnScrn();
			}

			//console.log('bcMagic', scope.$first, scope.$last, scope.bc.children.length);
			$timeout(function() {
				/*if (scope.$first) {
					$(element).addClass("active");
					$('a.dropdown-toggle', element).html('<i class="foundicon-cloud"></i></span>').attr('title', "Top");
				}*/
				$('.itemMenuA').dropdownHover();
				/*if (scope.$last && (scope.bc.children.length > 0) && Settings.vals.bcAutoOpen) {
					$(element).addClass("open");
				}*/
			}, 0);
			var menuItems = scope.bc.children;
			if (Settings.vals.bcSortDrop) {
				menuItems.sort(function(a, b) {
					return a.title.localeCompare(b.title);
				});
			}
			var menu = angular.element($('.dropdown-menu', element)[0]);
			makeColumns();
			element.on('click', colsOnScrn);
			$(window).resize(colsOnScrn);
			scope.nextCrumb = BreadCrumbs.breadCrumbs[scope.$index+1];
		}
	})


	.directive('bcmlDecorate', function(BreadCrumbs) {
		return function(scope, element, attrs) {
			if (BreadCrumbs.visited[scope.topic.id]) {
				element.addClass('topicViewed');
			}
			if (scope.nextCrumb && (scope.topic.id == scope.nextCrumb.id)) {
				element.addClass('active');
			}
		}
	})


	.directive('oneSearchResult', function(BreadCrumbs) {
		return function(scope, element, attrs) {
			if (BreadCrumbs.visited[scope.result.id]) {
				element.addClass('topicViewed');
			}
			scope.resultUrl = "#/page/" + seoFilter(scope.result.taxonomy.name) + '/' + seoFilter(scope.result.title) + '/' + scope.result.id;
		}
	})


	.directive('setTopicPhotos', function(Settings) {
		return function(scope, element, attrs) {

			function setPhotoList() {
				if (Settings.vals.topicPhotos) {
					var photoList = [];
					for (var i=0; i<5; i++) {
						photoList[i] = scope.imgServerUrl + scope.imgPath() + '_' + i + '.jpg';
					}
					scope.topicPhotoList = photoList;
					$('#MorePhotos').addClass('showIt');
				}
				else {
					$('#MorePhotos').removeClass('showIt');
				}
			}

			setPhotoList();
			scope.$watch('settings.vals.topicPhotos', function(newValue, oldValue) {
				if (newValue != oldValue) {
					setPhotoList();
				}
			});
		}
	})


	.directive('focusIfEmpty', function() {
		return function(scope, element, attrs) {
			if (scope.$eval(attrs.ngModel) == '') {
				element.focus();
			}
		}
	})


	.directive('btnsTopEnd', function($timeout) {
		return function(scope, element, attrs) {
			var btnIsOn = false;
			$(window).scroll( function() {
				window.clearTimeout(element.timerTop);
				$timeout(function() {
					if ($(window).scrollTop() > 50) {
						if (!btnIsOn) {
							btnIsOn = true;
							$(element).css('right', $('#adSideR').css('display')=='block' ? '180px' : '10px').addClass('showIt');
						}
						element.timerTop = window.setTimeout(function() {
							btnIsOn = false;
							$(element).removeClass('showIt');
						}, 3000); // 3 sec.
					}
					else if (btnIsOn) {
						btnIsOn = false;
						$(element).removeClass('showIt');
						// unbind up/down button event handlers?
					}
				}, 0);
			});

		}
	})


	.directive('toTop', function() {
		return function(scope, element, attrs) {
			element.on('click', function(event) {
				$(window).scrollTop(0);
				return false;
			})
		}
	})


	.directive('toEnd', function() {
		return function(scope, element, attrs) {
			element.on('click', function(event) {
				$(window).scrollTop($(document).height());
				return false;
			})
		}
	})


	.directive('pgDown', function() {
		return function(scope, element, attrs) {
			element.on('click', function(event) {
				$(window).scrollTop($(window).scrollTop() + $(window).height());
				return false;
			})
		}
	})


	.directive('pgUp', function() {
		return function(scope, element, attrs) {
			element.on('click', function(event) {
				$(window).scrollTop($(window).scrollTop() - $(window).height());
				return false;
			})
		}
	})


	.directive('showRelated', function(Settings) {
		return function(scope, element, attrs) {
			//console.log('showRelated', scope[attrs.showRelated], Settings.vals);
			$(element).css('cursor', 'pointer');
			if ((Settings.vals.topicLinks == 'show') || (scope[attrs.showRelated].topicLinks == 'show')) {
				$(element).css('display', 'inline');
			}
			else {
				$(element).css('display', 'none');
			}
		}
	})


	.directive('clickRelated', function() {
		return function(scope, element, attrs) {
			$(element).css('cursor', 'pointer');
			element.on('click', function(event) {
				//console.log('parent', $(element).parent()[0]);
				scope[attrs.clickRelated].topicLinks = 'show';
				scope.$apply( function() {
					$('span[show-related]', $(element).parent()[0]).css('display', 'inline');
				});
				return false;
			})
		}
	})


	.directive('backButton', function() {
		return function(scope, element, attrs) {
			$(element).attr('title', 'Back');
			element.on('click', function(event) {
				window.history.back();
				return false;
			})

		}
	})


	.directive('showOnLoad', function($timeout) {
		return function(scope, element, attrs) {
			$(element).css('display', 'none');
			element.on('load', function(event) {
				//console.log('showOnLoad', element, element.attr('src'));
				$timeout(function() {
					$(element).css('display', 'inline');
					if (scope.imgLoaded) {
						scope.imgLoaded();
					}
				}, 0);
			})

		}
	})


	.directive('docLinkPhoto', function(Settings, $timeout) {
		return function(scope, element, attrs) {
			//console.log('docLinkPhoto', Settings.vals.docLinkPhoto, attrs);
			if (Settings.vals.docLinkPhoto) {
				$timeout(function() {
					element[0].src = attrs.docLinkPhoto;
				}, 0);
			}
		}
	})


	.directive('topicPhotoUrl', function() {
		return function(scope, element, attrs) {
			var tid = scope.result ? scope.result.id : null; // only from search.html

			function setTopicPhotoUrl() {
				var narrow = $(window).width() < 400;
				var newUrl = scope.imgServerUrl + scope.imgPath(narrow ? '/thumbnails/' : '/images/', tid) + (narrow ? '_0.jpg' : '_0_nocrop.jpg');
				$(element)[0].src = newUrl;
			}

			setTopicPhotoUrl();
			$(window).resize(setTopicPhotoUrl); // need this?
		}
	})


	.directive('tileRows', function($timeout) {
		return function(scope, element, attrs) {
			var tilesRendered = false;
			var tileScan_id = false;

			scope.setRows = function() { // called first by .directive('jtRepeatDone'
				tilesRendered = true;
				tileScan_id = Math.random();
				var row = 0;
				var prev = 0;
				var last = 0;

				function scanRows(sid) {
					row++;
					//console.log('setRows', row, sid, tileScan_id);
					$timeout(function() {
						$('.well', $(element).offsetParent()).each(function(idx) {
							if (sid != tileScan_id) {
								//console.log('tileScan_id2',sid);
								return false;
							}
							var pos = $(this).position();
							var newRow = pos.top > prev;
							//console.log('each', idx, newRow, prev, pos.top);
							prev = pos.top;
							if (newRow && (idx > last)) {
								$(this).css('clear', 'left');
								last = idx;
								scanRows(sid);
								return false;
							}
						});
					}, 100);
				}

				$('.well', $(element).offsetParent()).css('clear', 'none');
				scanRows(tileScan_id);
			}

			scope.imgLoaded = function() { // called by .directive('showOnLoad'
				//console.log('imgLoaded', tilesRendered, tileScan_id);
				if (tilesRendered) {
					scope.setRows();
				}
			}

			$(window).resize(function() {
				if (tilesRendered) {
					scope.setRows();
				}
			});
		}
	})


	.directive('adSide', function($http, AppAlerts) {
		return function(scope, element, attrs) {
			var vuPort = $('#pageVuPort');

			function hideAdColumn() {
				element.css('display', 'none');
				vuPort.css('width', 'auto');
			}

			function setAdColumn() {
				if (scope.scrnSmall) {
					hideAdColumn();
				}
				else {
					vuPort.css('width', $(window).width() - parseInt($(vuPort).css('margin-left')) - 200 ); // 160 + 10 + 30
					element.css('display', 'block');
					$http({method: 'GET', url: "ad_server/?size=160x600"})
						.success(function(data, status, headers, config) {
							element.html(data);
						})
						.error(function(data, status, headers, config) {
							AppAlerts.alert('ERROR!', status + '<br>url: ' + config.url);
						});
				}
			}

			setAdColumn();
			$(window).resize(setAdColumn);
			scope.$on('$routeChangeStart', hideAdColumn);

		}
	})


	.directive('adBottom', function($http, AppAlerts) {
		return function(scope, element, attrs) {

			function setAdBottom() {
				if (scope.scrnSmall || attrs.adBottom) {
					$http({method: 'GET', url: "ad_server/?size=320x50"})
						.success(function(data, status, headers, config) {
							element.html(data);
						})
						.error(function(data, status, headers, config) {
							AppAlerts.alert('ERROR!', status + '<br>url: ' + config.url);
						});
				}
				else {
					element.html('');
				}
			}

			setAdBottom();
			$(window).resize(setAdBottom); // need this?
		}
	})


	.filter('seo_path', function() {
		return function(path) { // remove invalid url characters
			return seoFilter(path);
		}
	})
	
seoFilter = function(path) { // remove invalid url characters
	var words = path ? path.toLowerCase().replace(/[^a-zA-Z0-9-_.\s]/g, '').split(' ') : ['seo_error'];
	return words.join('-');
}

setStylesLinkPage = function(){
	$('#customTitle').text('Know more about ');
	$('#topTitle').addClass('top_title_link');
	$('#joinButton').show();
}

setStylesBridgePage = function(){
	$('#customTitle').text('Topics in ');
}

setAds = function(page){
	var contAds = $('#insertAfterAds');
	var html = "";
	contAds.empty();
	if(page == 'topics'){
			   $('.topicsAds').remove();
			   html += '<div class="topicsAds pub_container pub_container_multi" original="http://pagead2.googlesyndication.com/pagead/show_ads.js">' +
					   '<code type="text/javascript">' +
					   '<!--' +
					   'google_ad_client = "ca-pub-1595717843634953";' +                       
					   'google_ad_slot = "5096023225";' +
					   'google_ad_width = 160;' +
                       'google_ad_height = 600;' +
					   '//-->' +
					   '</code>' +
					   '</div>';
	}else if(page == 'link'){
				html += '<div class="linkAdsLeft pub_container pub_container_multi" original="http://pagead2.googlesyndication.com/pagead/show_ads.js">' +
					   '<code type="text/javascript">' +
					   '<!--' +
                       'google_ad_client = "ca-pub-1595717843634953";' +                            
                       'google_ad_slot = "9665823623";' +
                       'google_ad_width = 300;' +
                       'google_ad_height = 600;' +
                       '//-->' +
					   '</code>' +
					   '</div>';
				html += '<div class="linkAdsTop pub_container pub_container_multi" original="http://pagead2.googlesyndication.com/pagead/show_ads.js">' +
						   '<code type="text/javascript">' +
						   '<!--' +
						   'google_ad_client = "ca-pub-1595717843634953";' +                            
						   'google_ad_slot = "3619290029";' +
						   'google_ad_width = 468;' +
						   'google_ad_height = 15;' +
						   '//-->' +
						   '</code>' +
						   '</div>';
	}else if(page == 'digest'){
				html = '<div class="digestsAdsTop pub_container pub_container_multi" original="http://pagead2.googlesyndication.com/pagead/show_ads.js">' +
								   '<code type="text/javascript">' +
								   '<!--' +
								   'google_ad_client = "ca-pub-1595717843634953";' +                            
								   'google_ad_slot = "4956422424";' +
								   'google_ad_width = 200;' +
								   'google_ad_height = 90;' +
								   '//-->' +
								   '</code>' +
								   '</div>';
				contAds.after(html);
				jQuery('div.pub_container_multi').lazyLoadAd();
				html = '<div class="digestsAdsLeft pub_container pub_container_multi" original="http://pagead2.googlesyndication.com/pagead/show_ads.js">' +
								   '<code type="text/javascript">' +
								   '<!--' +
								   'google_ad_client = "ca-pub-1595717843634953";' +                            
								   'google_ad_slot = "6572756426";' +
								   'google_ad_width = 234;' +
								   'google_ad_height = 60;' +
								   '//-->' +
								   '</code>' +
								   '</div>';
				$('#insertLeftAds').append(html);
				jQuery('div.digestsAdsLeft').lazyLoadAd();
				return;
	}
	contAds.after(html);
	jQuery('div.pub_container_multi').lazyLoadAd({forceLoad:true});
}

refreshAddThis = function(){
	if (window.addthis){
		window.addthis = null;
		window._adr = null;
		window._atc = null;
		window._atd = null;
		window._ate = null;
		window._atr = null;
		window._atw = null; 		
	}
	addthis_share.url = location.href;
	$.getScript( "http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-506b79ce40993638");
}

function googleAnalytics(){
	
}
