<?php
error_reporting(E_ALL);
session_start();

include_once dirname(__FILE__)."/incl/SessionData.class.php";

$app_session = new SessionData();

if (isset($_REQUEST['theme'])) {
	$app_session->set('theme', $_REQUEST['theme']);
}
if (isset($_REQUEST['skin'])) {
	$app_session->set('skin', $_REQUEST['skin']);
}
if (isset($_REQUEST['goto'])) {
	$uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	header('Location: http://'.$_SERVER['HTTP_HOST'].$uri);
	exit;
}

$theme = $app_session->get('theme');

function isSelected($val) {
	global $theme;
	return ($val == $theme) ? ' selected="selected"' : '';
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Linkapedia Branding - CSS Themes and Skins</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta charset="utf-8" />

<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
<style>
body{margin:30px;}
select {width: auto;}

.form-horizontal .control-group {margin-bottom:10px;}
.form-horizontal .controls {margin-left: 160px;}
legend {margin-bottom:5px;}
</style>

<script>
var skin_urls = [
	"",
	"http://www.wingo.com/semmx/test_1.css",
	"http://www.wingo.com/semmx/test_2.css",
	"http://www.wingo.com/semmx/test_dark.css",
	"http://www.wingo.com/semmx/slide_nav.css"
];

function setSkin(idx) {
	document.PAGEFORM.skin.value = skin_urls[idx];
	return false;
}
</script>

</head>
<body>

<h1>Linkapedia Branding - CSS Themes and Skins</h1>

<form action="branding.php" method="POST" name="PAGEFORM" class="form-horizontal">

<fieldset>
	<legend>Choose Theme and Skin</legend>

	<div class="control-group">
		<label class="control-label" for="theme">Bootstrap Theme:&nbsp;</label>
		<select name="theme">
			<option<?= isSelected('default') ?>>default</option>
			<option<?= isSelected('readable') ?>>readable</option>
			<option<?= isSelected('slate') ?>>slate</option>
			<option<?= isSelected('spacelab') ?>>spacelab</option>
			<option<?= isSelected('spruce') ?>>spruce</option>
			<option<?= isSelected('flatly') ?>>flatly</option>
			<option<?= isSelected('amelia') ?>>amelia</option>
			<option<?= isSelected('superhero') ?>>superhero</option>
			<option<?= isSelected('united') ?>>united</option>
		</select>
	</div>

	<div class="control-group">
		<label class="control-label" for="skin">Skin:&nbsp;</label>
		<input type="text" name="skin" class="input-xxlarge" placeholder="stylesheet url" value="<?= $app_session->get('skin') ?>">
	</div>

	<div class="control-group">
		<div class="controls">
			<label>
				<a href="" onclick="return setSkin(0)">clear</a> |
				<a href="" onclick="return setSkin(1)">test_1.css</a> |
				<a href="" onclick="return setSkin(2)">test_2.css</a> |
				<a href="" onclick="return setSkin(3)">test_dark.css</a> |
				<a href="" onclick="return setSkin(4)">slide_nav.css</a>
			</label>

			<label class="checkbox">
				<input type="checkbox" name="goto"> Go to Linkapedia
			</label>

		<button type="submit" class="btn">Save</button>
		</div>
	</div>

</fieldset>
</form>


<legend>Instructions</legend>

<ul>
	<li>Choose a Bootstrap Theme (required).</li>
	<li>Copy/paste the url of a "Skin" stylesheet (optional) - Examples are provided; click the links to use them (<a href="http://www.wingo.com/semmx/test_1.css" target="_blank">test_1.css</a>, <a href="http://www.wingo.com/semmx/test_2.css" target="_blank">test_2.css</a>, <a href="http://www.wingo.com/semmx/test_dark.css" target="_blank">test_dark.css</a> or <a href="http://www.wingo.com/semmx/slide_nav.css" target="_blank">slide_nav.css</a>) or create your own and make it visible to the web (a public Dropbox folder, for example).  Click "clear" and "Save" with no skin url to see the unmodified theme.</li>
	<li>Click the checkbox to "Go to Linkapedia" (optional, not recommended).</li>
	<li>Click "Save" to set a PHP session variable, saving your choices for use by the application.  The session variable allows the theme and skin parameters to be removed from the url, though it's still possible to set them that way: <a href="index.php?theme=slate&skin=http://www.wingo.com/semmx/slide_nav.css" target="_blank">index.php?theme=slate&skin=http://www.wingo.com/semmx/slide_nav.css</a><br>
		(NOTE: index.php grabs the theme and skin parameters, saves them to the session variable and reloads itself without the params visible; subsequent page reloads will re-use the session variable choices.)</li>
	<li>The application can run in a separate browser tab (recommended); if you do this, switch to that tab and reload the page to see the new theme and/or skin.</li>
	<li>Modify the skin stylesheet and reload the application page (over and over!).  The session values don't change so there is no need to re-run this page unless you want to change the theme or skin url.</li>
</ul>

<legend>Background</legend>

<p>There are four levels of CSS used by this application:</p>

<ol>
	<li>A Bootstrap "theme"; the default is plain Bootstrap.  The others listed were found at <a href="http://www.bootstrapcdn.com/#tab_bootswatch" target="_blank">bootstrapcdn.com</a>.  Many other sources exist for Bootstrap themes:
		<a href="http://bootstrapthemes.com/" target="_blank">bootstrapthemes.com</a>,
		<a href="http://bootswatch.com/#gallery" target="_blank">bootswatch.com</a>,
		<a href="https://wrapbootstrap.com/" target="_blank">wrapbootstrap.com</a>,
		etc.  And tools exist for creating your own:
		<a href="http://pikock.github.io/bootstrap-magic/" target="_blank">Bootstrap Magic</a>,
		<a href="http://stylebootstrap.info/" target="_blank">StyleBootstrap</a>,
		<a href="http://www.boottheme.com/" target="_blank">BootTheme</a>,
		<a href="http://www.bootstrapthemeroller.com/" target="_blank">Bootstrap ThemeRoller</a>,
		<a href="http://paintstrap.com/" target="_blank">PaintStrap</a>
		and <a href="https://www.google.com/search?q=bootstrap+theme+generator" target="_blank">more</a>.
	</li>
	<li><a href="css/default_bcdd.css" target="_blank">theme_bcdd.css</a> - To support the breadcrumb multi-column dropdown menus ("bcdd"), a few CSS styles must be copied with slightly modified selectors: ".dropdown-menu&gt;" is changed to "#CrumbNav .dropdown-menu " (trailing space instead of "&gt;").  These files are created automatically by parsing the specified theme.</li>
	<li><a href="css/style.css" target="_blank">style.css</a> - This file contains structural elements specific to this application, including responsive design features.</li>
	<li>skin.css - This optional file allows all of the above CSS to be redefined and overridden.  With great power comes great responsibility!  Many things are possible, including breaking the application...</li>
</ol>

<p>Branding, or making the site look unique for different customers, consists of choosing a Bootstrap theme and providing an optional skin.css file.</p>

<legend>Disclaimers</legend>

<p>This is an effort to provide a means for a visual designer to independently modify the appearance of the application.  It is a "sandbox project" for exploring ideas. We may discover that changes to the HTML are needed to achive better customization. In production, the theme and skin choices would likely come from a config file (simple but not yet implemented).</p>

<p>I am not a visual designer.  The example skins are offered as "how to", not as recommendations of color or style.  They are very simple and don't work well with all themes.  The best results will be achieved by making each skin for a specific theme.</p>

</body>
</html>