/**
* Api Rest Linkapedia
* Create  by Alexander Agudelo
* This js require jquery
* @module REST
*/

Rest = function(){
	this.uri_rest = "http://linkapedia-rest-dev.elasticbeanstalk.com";
	this.limit = 16;
	this.totalpages = null;	
}

/**
 * Returns basic information about a taxonomy
 * Return Json Object
 */ 
 
Rest.prototype.getTaxonomyInfo = function(options){
	try{		
		if(options && options.taxonomyId && options.success && options.error){
			this.getData(this.uri_rest + "/taxonomies/" + options.taxonomyId + "/info", options.success, options.error);	
		}else{
			console.error("Error: missing arguments options={taxonomyId:value, success:function(data){}, error:function(err){}}");
		}		
	}catch(err){
		return options.error("Rest.data.getTaxonomyInfo: " + err.message);							
	}
}

/**
 * Returns basic information about a topic.
 */

Rest.prototype.getTopicInfo = function(options){
	try{
		var that = this;
		if(options && options.topicId && options.success && options.error){
			this.getData(this.uri_rest + "/topics/" + options.topicId +"/info", 
			function(data){				
				that.setDescriptionChildrens(data, 0, data.children.length, function(data2){
					return options.success(data2);
				});
			}, 
			function(err){
				return options.error(err);
			});	
		}else{
			console.error("Error: missing arguments options={topicId:value, success:function(data){}, error:function(err){}}");
		}		
	}catch(err){		
		return options.error("Rest.data.getTopicInfo: " + err.message);
	}
} 

/**
 * Get and set description for children topics
 * Return json data updated 
 */
 
Rest.prototype.setDescriptionChildrens = function(data, cont, limit, success, error){
	try{
		var that = this;		
		if(cont < limit){			
			this.getData(this.uri_rest + "/topics/" + data.children[cont].id +"/info", 
			function(data2){
				data.children[cont].img = data.img = "https://s3.amazonaws.com/nodeimages/" + data.children[cont].id + "/images/" + data.children[cont].id + "_0_nocrop.jpg";
				if(data2.description.trim() != ""){
					data.children[cont].description = data2.description;
				}else{
					data.children[cont].description = data2.title;
				}
				that.setDescriptionChildrens(data, ++cont, limit, success, error);
			}, 
			function(err){
				return error("Rest.setDescriptionChildrens: " + err);
			});
		}else{
			return success(data);
		}
	}catch(err){
		return error("Rest.setDescriptionChildrens: " + err.message);
	}
}

/**
 * Get Documents page by page 
 * Return Json Object
 */

Rest.prototype.getDocumentsByPage = function(options){
	try{
		if(options && options.topicId && options.page && options.success && options.error){
			var that = this;			
			this.getData(this.uri_rest + "/topics/" + options.topicId + "/documents?limit=" + this.limit +"&page=" + options.page, 
			function(data){
				that.totalpages = data.totalPages;
				if(parseInt(options.page) > that.totalpages){
					return options.error("No more pages");
				}
				return options.success(data);
			}, 
			function(err){
				return options.error(err);
			});
		}else{
			console.error("Error: missing arguments options={topicId:value, page:value, success:function(data){}, error:function(err){}}");
		}		
	}catch(err){
		options.error("Rest.getDocumentsByPage: " + err.message);		
	}
} 
 
/**
 * Get Categories 
 */ 

Rest.prototype.getCategoriesInfo = function(options){
	try{
		if(options && options.topicId && options.success, options.error){
			this.getData(this.uri_rest + "/categories/" + options.topicId +"/info", 
			function(data){
				return options.success(data);
			}, 
			function(err){
				return options.error(err);
			});
		}else{
			console.error("Error: missing arguments options={topicId:value, success:function(data){}, error:function(err){}}");
		}
	}catch(err){
		options.error("Rest.getCategories: " + err.message);		
	}
} 
 
/**
 * Get data from Rest Amazon Service 
 * Return Json Object
 */
 
Rest.prototype.getData = function(urlRequest, success, error){
	try{		
		$.ajax({			
			url : urlRequest,
			dataType : "json",		
			success : function(data){				
				return success(data);
			},
			error : function(jqXHR, textStatus, errorThrown){
				return error("HTTP Status " + jqXHR.status + " - " + jqXHR.statusText);				
			}
		});
	}catch(err){
		return error(err.message);
	}
}

