angular.module('Linkapedia', [])

.config(function($routeProvider, $locationProvider, $httpProvider) {	
	$routeProvider
		.when('/', {templateUrl:"templates/home.html", controller:'HomeCtrl'})
		.when('/page/:taxName/:topicName/:topicId', {templateUrl:"../../templates/bridgenode.html", controller:'BridgeNode'})
		.otherwise({redirectTo: '/'});		
		
		$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		//$locationProvider.html5Mode(true);
})

.controller('AppCtrl', function($scope) {
	console.log('Que cosa');
})

.controller('HomeCtrl', function($scope, $routeParams, $location) {
	
})

.controller('BridgeNode', function($scope, $routeParams, $location) {	
	var topics = new Rest();
	topics.getBreadCrumbs($routeParams.topicId,
		function(data) {
			var size = data.length;
			var sizegroup = 7;
			var groups = [];
			var totalgroups = (data.childrens / sizegroup) >> 0;
			var temp = [];
			for(i=0; i<size; i++){
				data[i].encodetitle = encodeFileName(data[i].title);
				data[i].taxonomy.encodename = encodeFileName(data[i].taxonomy.name);
				
				if(data[i]['childrens'].length > sizegroup){			
					while(data[i]['childrens'].length > sizegroup){					
						for(b=0; b<sizegroup; b++){
							temp.push(data[i].childrens[b]);
						}
						groups.push(temp);
						temp=[];
						data[i]['childrens'].splice(0, sizegroup);
					}
				}else{
					groups.push(data[i]['childrens']);
				}
				groups.push(data[i]['childrens']);
				data[i]['childrens'] = groups;
			}				
															
			$scope.breadCrumbs = data;			
			$scope.$apply();			
			$('.dropdown-toggle').dropdownHover();				
		},
		function(err) {			
			console.error(err);
		}
	);
	$('#relojLoading').show();
	topics.isBridgeNode($routeParams.topicId, function(isOrNot){		
		if(isOrNot){
			topics.getTopicInfo({
				topicId : $routeParams.topicId,
				success : function(data) {
					$('#relojLoading').hide();
					$scope.title = data.title;
					$scope.textDescription = data.description;	
										
					var size = data.children.length;
					data.taxonomy.encodename = encodeFileName(data.taxonomy.name);
					for(i=0; i<size; i++){
						data.children[i].encodetitle = encodeFileName(data.children[i].title);						
					}
												
					$scope.topics = data;
					$scope.$apply();
					
					$('#topicTitle').show();					
					
					$($("html > .imgtopic")).imagesLoaded(function(){
					$('.boxBridgeNode').wookmark({
						  align: 'left',
						  autoResize: true,
						  container: $('#bridgeNodeContent'),
						  itemWidth: 270,				  
						  offset: 10,
						  resizeDelay: 50,
						  flexibleWidth: 0,
						  onLayoutChanged: undefined
						});	
						bridgeNodeEffect();					
					});
				},
				error : function(err) {			
				}
			});
		}else{
			topics.getDocumentsByPage({
				topicId:$routeParams.topicId,
				page:1,
				success:function(docs){
					$scope.docs = docs;
					$('#relojLoading').hide();
					$('#Linkpage').show();
					$("#digests > .box").css("width", "270px");
					$("#digests > .linkImage").error(function(){
						$(this).css("display", "none");
						//$(this).attr("src", "https://s3.amazonaws.com/resourcesweb/newdev/images/notImage.jpg");
					});
					$scope.$apply();
					$($("html > .linkImage")).imagesLoaded(function(){
						$('.box').wookmark({
						  align: 'left',
						  autoResize: true,
						  container: $('#digests'),
						  itemWidth: 0,
						  offset: 8,
						  resizeDelay: 50,
						  flexibleWidth: 0,
						  onLayoutChanged: undefined
						});						
					});					
				},
				error:function(err){
					console.error(err);
				}
			});								
		}
	},
	function(err){
		console.error(err);
	});			
})
