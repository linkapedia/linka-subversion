<?php
header('Content-type: text/html');
header('Cache-Control: no-cache');

$adSize = isset($_GET['size']) ? $_GET['size'] : "160x600";
$adList = [];
$dir = 'size'.$adSize; // directory name based on $adSize
$files = scandir($dir);
foreach ($files as $file) {
	$relPath = $dir.'/'.$file;
	if (!is_dir($relPath)) {
		$adList[] = $relPath; // collect file names
	}
}
echo trim(file_get_contents($adList[mt_rand(0, count($adList)-1)])); // return content of random file
?>