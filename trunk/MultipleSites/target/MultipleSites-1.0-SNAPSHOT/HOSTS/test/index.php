<?php
error_reporting(E_ALL);
session_start();

include_once dirname(__FILE__)."/incl/SessionData.class.php";

$app_session = new SessionData();

$Linkapedia_version = "v2013Jul.10b";

if (isset($_GET['theme'])) {
	$app_session->set('theme', $_GET['theme']);
}
if (isset($_GET['skin'])) {
	$app_session->set('skin', $_GET['skin']);
}
if (isset($_GET['theme']) || isset($_GET['skin'])) {
	$uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	header('Location: http://'.$_SERVER['HTTP_HOST'].$uri);
	exit;
}

$theme = "default";
$urlTheme = $cssDefault = "//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css";
$themePref = $app_session->get('theme');
if ($themePref) {
	$theme = $themePref;
	$urlTheme = ($theme == "default") ? $cssDefault : "//netdna.bootstrapcdn.com/bootswatch/2.3.2/".$theme."/bootstrap.min.css";
}

/************ BEGIN: (for each theme!) re-define Bootstrap 'dropdown-menu' features for multi-column ************/
	function cssCopy($var) {
		$pos1 = strpos($var, ".dropdown-menu>li");
		$pos2 = strpos($var, ".dropdown-menu>.active>a");
		return ($pos1 === 0) || ($pos2 === 0) || ($pos1 === 1) || ($pos2 === 1);
	}

$bcdd = "css/".$theme."_bcdd.css";
if (!file_exists($bcdd)) {
	$ch = curl_init("http:".$urlTheme);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$css = curl_exec($ch);
	//$curlError = curl_error($ch);
	//$curlInfo = curl_getinfo($ch);
	curl_close($ch);
	//echo 'curl_error=[' . $curlError . "]<br>\n";
	//foreach ($curlInfo as $curlKey => $curlValue) echo $curlKey . ' = ' . $curlValue . "<br>\n";

	$lines = explode('}', $css);
	$keep = array_filter($lines, "cssCopy");
	$css = implode("\n}\n", $keep)."\n}\n"; // must add last one!
	$lines = explode('{', $css);
	$css = implode("{\n", $lines);
	file_put_contents($bcdd, str_replace(".dropdown-menu>", "#CrumbNav .dropdown-menu ", $css));
}
/************ END: (for each theme!) re-define Bootstrap 'dropdown-menu' features for multi-column ************/
?>
<!DOCTYPE html>
<html>
<head>
	<title>Linkapedia Browser Demo using REST API and AngularJS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta charset="utf-8" />

<link href="<?= $urlTheme ?>" rel="stylesheet">
<link href="<?= $bcdd ?>" rel="stylesheet">
<link href="foundation_icons_general/stylesheets/general_foundicons.css" rel="stylesheet" type="text/css">
<link href="css/style.css?v=<?= $Linkapedia_version ?>" rel="stylesheet" type="text/css">
<?php
$skin = $app_session->get('skin');
if ($skin) {?>
<link href="<?= $skin ?>" rel="stylesheet" type="text/css"><?php
}
?>
</head>
<body ng-app="Linkapedia" ng-controller="AppCtrl" jt-view-scroll>

<div id="logoBar">
	<a href="#/"><img src="images/logo.png" alt="" width="152" height="28"></a>

	<div class="btn-group pull-right">
		<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="btn_Menu">
			Menu
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="#/search"><i class="foundicon-search"></i> Search</a></li>
			<li class="muted"><a href="#/history" onClick="return false;"><i class="foundicon-compass"></i> History</a></li>
			<li><a href="#/settings" onClick="return false;"><i class="foundicon-settings"></i> Settings</a></li>
			<li class="divider"></li>
			<li><a href="" jt-full-screen><i class="foundicon-photo"></i> Full screen</a></li>
			<li class="divider"></li>
			<li><a href="#/login"><i class="foundicon-unlock"></i> Login</a></li>
			<li class="divider"></li>
			<li><a href="#aboutInfo" role="button" class="wingo_logo" data-toggle="modal"><i class="foundicon-idea"></i> About</a></li>
		</ul>
	</div>

	<a href="#/search" class="btn btn-primary pull-right btnPadR" title="Search"><i class="foundicon-search"></i></a><br class="clearBoth">

</div>

<div id="CrumbNav">
	<ul class="nav nav-pills pageMargins ng-cloak">
		<li ng-repeat="bc in crumbNav.breadCrumbs" class="dropdown" bc-magic>
			<a class="dropdown-toggle ng-cloak" role="button" data-toggle="dropdown" href="#" ng-show="bc.children.length>0">{{ bc.title }}</a>
			<span ng-show="bc.children.length==0" class="bcNoLink muted">{{ bc.title }}</span>
			<div class="dropdown-menu" role="menu" ng-show="bc.children.length>0">
				<div ng-repeat="column in ddmCols" ng-class="colClass" jt-repeat-done="keepOnScrn()">
					<ul>
						<li ng-repeat="topic in column" role="presentation" bcml-decorate><a href="#/page/{{bc.title | seo_path}}/{{topic.title | seo_path}}/{{topic.id}}" role="menuitem">{{topic.title}}<span class="badge"><i class="foundicon-checkmark"></i></span></a></li>
					</ul>
				</div>
			</div>
		</li>
	</ul>
</div>

<div ng-repeat="alert in appAlerts.list" class="alert pageMargins ng-cloak" ng-class="alert.typeAlert">
	<!-- <button type="button" class="close" id="btnTagAlert">&times;</button> -->
	<b ng-bind-html="alert.msgBold"></b>
	<span ng-bind-html="alert.msgTxt"></span>
</div>

<div class="pageMargins ng-view" id="pageVuPort"></div>

<p id="footer">
	&copy; 2013 Linkapedia | Mobile | <a href="http://www.interestplace.com/" target="_top">Desktop</a>
</p>

<div id="Loading" ng-show="appAlerts.loadingOn()">Loading...</div>

<div id="btns_Top_End" btns-top-end>
	<a href="" class="btn btn-warning" title="top of page" to-top><i class="foundicon-up-arrow"></i> Top</a><br>
	<a href="" class="btn btn-warning upDown" title="page up" pg-up><i class="foundicon-up-arrow"></i></a><a href="" class="btn btn-warning upDown" title="page down" pg-down><i class="foundicon-down-arrow"></i></a><br>
	<a href="" class="btn btn-warning" title="end of page" to-end>End <i class="foundicon-down-arrow"></i></a><br>
</div>


<script type="text/ng-template" id="login.html">
<h2><i class="foundicon-unlock"></i> Login</h2>

<p><i>Not yet implemented...</i></p>
</script>


<div id="aboutInfo" class="modal hide fade" ng-include src="'partials/about.html'"></div>


<script type="text/javascript">
'use strict';
var Linkapedia_version = "<?= $Linkapedia_version ?>";
</script>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.6/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.6/angular-sanitize.min.js"></script>

<script src="js/jt_AJS.js"></script>
<script src="js/ng-infinite-scroll_2.js"></script>
<script src="js/linkapedia.js?v=<?= $Linkapedia_version ?>"></script>

</body>
</html>