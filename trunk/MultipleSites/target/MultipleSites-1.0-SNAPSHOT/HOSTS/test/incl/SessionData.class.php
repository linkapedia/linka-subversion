<?php
/**
 * SessionData.class.php - manage session data using single key
 *
 * @created 22 Feb 2012
 * @author	Joseph Oster, wingo.com
 */

class SessionData {
	public $errMsg;
	private $props = array();
	private $version = 100;
	private $key = 'LinkapediaSession';

	public function instance() {
		// return existing instance or create/"save" new instance
		global $SessionData_instance__;
		if (!$SessionData_instance__) $SessionData_instance__ = new SessionData();
		return $SessionData_instance__;
	}

	public function __construct() {
		if ( isset($_SESSION[$this->key]) && ($_SESSION[$this->key]['version'] == $this->version) ) {
			$this->props = $_SESSION[$this->key];
		}
	}

	public function clearAll() {
		$this->props = array();
		unset($_SESSION[$this->key]);
	}

	public function set($name, $value) {
		$this->props['version'] = $this->version;
		$this->props[$name] = $value;
		$_SESSION[$this->key] = $this->props;
	}

	public function get($name) {
		return isset($this->props[$name]) ? $this->props[$name] : '';
	}

	public function login() {
		$email = isset($_POST['email']) ? trim($_POST['email']) : '';
		$password = isset($_POST['password']) ? trim($_POST['password']) : '';
		if ( ($email != '') || ($password != '') ) {
			// XML login request here
			global $xml_server_url;
			$url = $xml_server_url."login.cgi?email=".$_POST['email'].'&password='.$_POST['password'].xmlSuffix();
			$xmlSt = file_get_contents($url);
			$xml = simplexml_load_string($xmlSt);
			if (strpos($xmlSt, '<error><row><row error=') !== false) {
				$this->errMsg = $xml->row->row->attributes()->error;
			}
			else {
				//print_r($xml);
				//echo 'user='.$xml->user->row->attributes()->user.'<br />';
				//echo 'vendor='.$xml->user->row->attributes()->vendor.'<br />';
				//echo 'vendorname='.$xml->user->row->attributes()->vendorname.'<br />';
				$this->set('user', (string)$xml->user->row->attributes()->user );
				$this->set('vendor', (string)$xml->user->row->attributes()->vendor );
				$this->set('vendorName', (string)$xml->user->row->attributes()->vendorname );
				$this->set('token', (string)$xml->user->row->attributes()->token );
				$this->set('settings', (string)$xml->user->row->attributes()->settings );
				if ($xml->user->row->attributes()->admin_staff == 'True') {
					$this->set('admin_staff', 1);
				}
				return true;
			}
		}
		else $this->errMsg = 'missing Username/password';
		return false;
	}

	public function isLoggedIn() {
		return isset($this->props['user']) && ($this->props['user'] > 0);
	}

	public function logout() {
		$this->clearAll();
	}

}
?>