/**************************************
	module server(init application)
	Andres Rpo
***************************************/

//load modules
var express = require('express');
var SearchProvider = require('./myModules/connectionMongo').SearchProvider;
var app = express.createServer();
var apiKey='andres89';

// Configuration
app.configure(function() {
	app.use(express.bodyParser());
	app.enable("jsonp callback");
	
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

var searchProvider = new SearchProvider('10.3.8.96', 27017);

// Routes
app.get('/getNodes', function(req, res) {
	debugger;
	console.log("new Request");
	console.log(req.body);
	res.header('Content-Type', 'application/json');
	res.header('Charset', 'utf-8');  
	textSearch=req.param('text');
    searchProvider.getAllItems(textSearch, function( error, docs) {
		res.send(docs);
		res.end();		
    });		
});

//this method is to save documents in the collection
app.get('/saveDocuments', function(req, res) {
	debugger;
	res.header('Content-Type', 'application/json');
	res.header('Charset', 'utf-8');  
	tax=req.param('taxonomy');
	node=req.param('node');
	uri=req.param('URI');
	key=req.param('key');
	if(key!=apiKey){
		res.send('({"status": "Error (Not Auth)"});'); 
	}else{
		searchProvider.save({
			taxonomy: tax,
			node: node,
			URI : uri
		}, function(status) {
			if(status==1){
				res.send('({"status": "ok"});'); 
			}else{
				res.send('({"status": "Document already exits"});'); 
			}
			res.end();
		});
	}
});
app.post('/saveDocuments',function(req,resp)
{
	var strJson=req.body;
	var size=strJson.length;
	var index=0;
	var errorDocument=0;
	var sucessDocument=0;
	function callback(status)
	{
		index++;
		if(status==1)
		{
			sucessDocument+=1;
		}
		else
		{
			errorDocument+=1;
		}
		console.log("Document Insert Status %d",status);
		if((index+1)==size)
		{
			resp.write("Document Insert Success: "+sucessDocument+"\n");
			resp.write("Document Inset Error "+errorDocument+"\n");
			resp.end();
		}
	}
	key=req.param('key');
	if(key!=apiKey){
		resp.send('({"status": "Error (Not Auth)"});');
		resp.end();
	}else{
		for(var i=0;i<size;i++)
		{
			console.log(strJson[i]);
			searchProvider.save(strJson[i],callback);
		}
	}
});
console.log("Server Listen For 8881");
app.listen(8881);

