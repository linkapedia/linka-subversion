/*****************************************
	module to manage mongodb connections
	Andres Rpo
******************************************/

//load modules (mongodb)
var Db = require('mongodb').Db;
var Server = require('mongodb').Server;

//specify params to the connections
var databaseName='searchTest';
var collectionName='search';

//constructor (get connections)
SearchProvider = function(host, port) {
  this.db= new Db(databaseName, new Server(host, port, {auto_reconnect: true}, {}));
  this.db.open(function(){});
};

 //private method
 function getCollection(callback)
  {
	this.db.collection(collectionName, function(error, coll) {
		if( error ){
			callback(error);
		}else{
			callback(null, coll);
		}
	});
  }
  
  //check if documents already exits into database
  //private method
  function validateToSave(criteria,callback){
	getCollection.call(this,function(error, docs){
		if( error ){
			callback(error);
		}else{
			docs.find(criteria).toArray(function(err,r){
				if(r.length!=0){
					callback(0);
				}else{
					callback(1);
				}
			});
		}
	});
}

//get all items from collectionName
SearchProvider.prototype.getAllItems = function(text,callback) {
    getCollection.call(this,function(error, docs) {
    if( error ){
		callback(error);
    }else{
		exp=new RegExp('^'+text,'i');
		docs.find({node:exp}).toArray(function(err, r) {
			callback(null,r);
		});
	}
  });
};

//save documents to collectionName
SearchProvider.prototype.save = function(document, callback) {
	var that=this;
	getCollection.call(this,function(error, docs) {
      if( error ){ 
		callback(error);
      }else {
		validateToSave.call(that,document,function(response){
			if(response==1){
				docs.insert(document, function() {
					callback(1);
				});
			}else{
				callback(0);
			}
		});
      }
    });
};

//export to use require()
exports.SearchProvider = SearchProvider;