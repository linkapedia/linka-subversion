/**
 * @author: Andres R
 * principal process
 */
package com.linkapedia.generateimage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.linkapedia.db.ImageUtilsDB;

public class App {

	private static Log LOG = LogFactory.getLog(App.class);

	public static void main(String[] args) {
		LOG.info("START!");
		ImageUtilsDB iudb = null;
		Properties properties = new Properties();
		String nodeId = null;
		String corpusId = null;
		boolean help = false;
		if (args.length == 0) {
			LOG.info("Mixing Options");
			printHelp();
			return;
		}
		try {
			properties.load(App.class.getClassLoader().getResourceAsStream(
					"conf.properties"));
			// save parameters in the properties
			help = parseOptions(properties, args);
			nodeId = properties.getProperty("NODEID");
			corpusId = properties.getProperty("CORPUSID");
		} catch (Exception ex) {
			LOG.error("App: main(String[]) parsing properties "
					+ ex.getMessage());
		}
		// incorrect parameters
		if (help) {
			printHelp();
			return;
		}
		if (nodeId == null) {
			LOG.info("Parameter -n not found");
			return;
		}

		try {
			iudb = new ImageUtilsDB(properties);
		} catch (ClassNotFoundException e) {
			LOG.error("App: main(String[]) creating connection "
					+ e.getMessage());
			return;
		} catch (SQLException e) {
			LOG.error("App: main(String[]) creating connection "
					+ e.getMessage());
			return;
		}
		// set other parameters
		boolean t = false;
		try {
			t = setCorpusName(properties, iudb);
		} catch (SQLException e1) {
			LOG.error("SQLException " + e1.getMessage());
		}
		if (!t) {
			return;
		}
		boolean isFtp = properties.getProperty("FTPIP") != null;
		File tempZip = null;
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		Map<String, File> files;
		files = getStructureFiles(properties);
		if (!isFtp) {
			try {
				createDirectories(files, properties);
			} catch (Exception e) {
				LOG.error("App: main(String[]) creating directories "
						+ e.getMessage());
				return;
			}
		} else {
			if (!isFtpValid(properties)) {
				LOG.error("App: main(String[]) Ftp Options are Wrong");
				printHelp();
				return;
			} else {
				try {
					tempZip = new File("./images_"
							+ StringUtils.encodeFileName(properties
									.getProperty("CORPUSNAME")) + ".zip");
					if (tempZip.exists()) {
						tempZip.delete();
					}
					tempZip.createNewFile();
					tempZip.deleteOnExit();
					fos = new FileOutputStream(tempZip);
					zos = new ZipOutputStream(fos);
				} catch (Exception e) {
					LOG.debug("not create zip file " + e.getMessage());
					return;
				}
			}
		}
		// get corpusid from node
		if (corpusId == null) {
			LOG.info("get corpusid from node");
			try {
				corpusId = iudb.getCorpusidFromNode(nodeId);
			} catch (SQLException e) {
				LOG.error("Not get corpusid from nodeid please set -c");
			}
		}

		// get images by node
		if (nodeId.equalsIgnoreCase("all")) {
			if (corpusId != null) {
				String rootNodeId = null;
				// get root node: parent node = -1
				try {
					rootNodeId = iudb.getRootNode(corpusId);
				} catch (SQLException e) {
					LOG.error("App: main(String[]) getting root node "
							+ e.getMessage());
				}
				if (rootNodeId != null) {
					if (isFtp) {
						iudb.copyImages(rootNodeId, true, zos, files, corpusId);
					} else {
						iudb.copyImages(rootNodeId, true, files, corpusId);
					}
				}
			} else {
				LOG.error("Parameter -n all requiere corpusId");
				return;
			}
		} else {
			if (properties.getProperty("RECURSIVELY") != null) {
				LOG.info("Starting get node RECURSIVELY");
				if (isFtp) {
					iudb.copyImages(nodeId, true, zos, files, corpusId);
				} else {
					iudb.copyImages(nodeId, true, files, corpusId);
				}
			} else {
				LOG.info("Starting get node not RECURSIVELY");
				if (isFtp) {
					iudb.copyImages(nodeId, true, zos, files, corpusId);

				} else {
					iudb.copyImages(nodeId, true, files, corpusId);
				}
			}
		}

		// close connection database
		iudb.close();
		if (isFtp) {
			try {
				zos.flush();
				zos.close();
				ImageUtils.sendFtp(tempZip, properties);
			} catch (Exception e) {
				LOG.error("Not send by ftp: " + e.getMessage());
			}
		}

		LOG.info("FINISHED!");

	}

	/**
	 * 
	 * @param prop
	 * @param iudb
	 * @return
	 * @throws SQLException
	 */

	private static boolean setCorpusName(Properties prop, ImageUtilsDB iudb)
			throws SQLException {
		if (prop.getProperty("CORPUSID") != null) {
			prop.put("CORPUSNAME",
					iudb.getCorpusNameFromCorpus(prop.getProperty("CORPUSID")));
		} else {
			if (prop.getProperty("NODEID") != null) {
				if (!prop.getProperty("NODEID").equalsIgnoreCase("all")) {
					String idaux = iudb.getCorpusidFromNode(prop
							.getProperty("NODEID"));
					prop.put("CORPUSNAME", iudb.getCorpusNameFromCorpus(idaux));
				} else {
					LOG.info("not set corpus name");
					return false;
				}
			} else {
				LOG.info("not set corpus name");
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param properties
	 * @return
	 */
	private static boolean isFtpValid(Properties properties) {
		if (properties.get("USERNAME") == null) {
			return false;
		}
		if (properties.get("PASSWORD") == null) {
			return false;
		}
		return true;
	}

	private static Map<String, File> getStructureFiles(Properties properties) {
		Map<String, File> map = new HashMap<String, File>();
		// path to save images
		File path = new File(
				properties.getProperty(GenerateImageProperties.DEFAULTPATH));
		// folder to save images
		File imageFolder = new File(path,
				properties.getProperty(GenerateImageProperties.FOLDER));
		// folder to save thumnail
		File thumnailFolder = new File(imageFolder,
				properties.getProperty(GenerateImageProperties.FOLDERTHUMNAIL));
		map.put("PATH", path);
		map.put("FOLDER", imageFolder);
		map.put("FOLDERTHUMNAIL", thumnailFolder);

		return map;

	}

	/**
	 * create directories into file system
	 * 
	 * @param map
	 * @param properties
	 * @throws IOException
	 */
	private static void createDirectories(Map<String, File> map,
			Properties properties) throws IOException {

		// create directory to save images
		File path = map.get("PATH");
		if (!path.exists()) {
			path.mkdirs();
		}
		// create folder to save images
		File imageFolder = map.get("FOLDER");
		if (!imageFolder.exists()) {
			imageFolder.mkdir();
		} else {
			if (properties.getProperty("REMOVE") != null) {
				LOG.info("Clean Directory");
				FileUtils.cleanDirectory(imageFolder);
			}
		}
		// create folder to save thumnail images taxonomy
		File thumnailFolderTax = map.get("FOLDERTHUMNAIL");
		if (!thumnailFolderTax.exists()) {
			thumnailFolderTax.mkdir();
		}

	}

	/**
	 * save parameters into properties
	 * 
	 * @param properties
	 * @param args
	 * @return true to show help or false if the parameters are ok
	 */

	private static boolean parseOptions(Properties properties, String[] args) {
		String option = null;
		int i = 0;
		while (i < args.length) {
			option = GenerateImageProperties.OPTIONMAPPING.get(args[i]);
			if (option != null) {
				if (option.equals("HELP")) {
					return true;
				}
				if (args[i].startsWith("--")) {
					properties.put(option, option);
					i++;
				} else if ((i + 1) < args.length) {
					properties.put(option, args[i + 1]);
					i += 2;
				} else {
					LOG.error("Please specify param " + args[i]);
					return false;
				}
			} else {
				LOG.error("paremeters not found:" + args[i]);
				return true;
			}
		}
		return false;
	}

	/**
	 * Show help. (parameters and examples)
	 */
	private static void printHelp() {
		LOG.info("-c: CORPUSID");
		LOG.info("-n: NODEID - put \"ALL\" TO GET ALL NODES");
		LOG.info("-num: NUM IMAGES TO RETURN");
		LOG.info("--r: RECURSIVELY FROM NODE");
		LOG.info("--h: HELP");
		LOG.info("--MODE FTP--");
		LOG.info("-f: FTP SERVER");
		LOG.info("-u: USERNAME");
		LOG.info("-p: PASSWORD");
		LOG.info("-fp: DIRECTORY FTP");
		LOG.info("-dport: DPORT");
		LOG.info("Example mode ftp: java -jar GenerateImages.jar -c 2669 -n all -f 10.3.8.70 -u andres -p andres89 -fp ./ ");
		LOG.info("--MODE COPY DIRECTORY--");
		LOG.info("-path: PATH TO COPY IMAGES");
		LOG.info("--remove: REMOVE DIRECTORY IMAGES IF EXISTS AND START PROCESS");
		LOG.info("Example mode copy directory: java -jar GenerateImages.jar -n 1111 --r -path /var/www/html/linkapedia/coffee/");

	}
}
