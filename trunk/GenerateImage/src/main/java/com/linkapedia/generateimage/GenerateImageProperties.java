/**
 * @author: Andres R
 * manage properties
 */
package com.linkapedia.generateimage;

import java.util.HashMap;
import java.util.Map;

public class GenerateImageProperties {
	public static final String JDBC_URL = "jdbc.url";
	public static final String JDBC_USER = "jdbc.user";
	public static final String JDBC_PASSWORD = "jdbc.password";
	public static final String DEFAULTPATH = "image.defaultPath";
	public static final String FOLDER = "image.folder";
	public static final String FOLDERTHUMNAIL = "image.folderThumbnail";
	public static final String WIDTH = "image.resizeWidth";
	public static final String HEIGHT = "image.resizeHeight";
	public static final String QUALITY = "image.quality";
	public static final String NUM_IMAGES ="image.numimages";
	public static final String BOX_FOLDER ="box.folder";
	public static final String BOX_HEIGHT ="box.height";
	public static final String BOX_WIDTH ="box.width";
	
	public static final Map<String, String> OPTIONMAPPING = new HashMap<String, String>();

	static {
		OPTIONMAPPING.put("-c", "CORPUSID");
		OPTIONMAPPING.put("-n","NODEID");
		OPTIONMAPPING.put("-num","image.numimages");
		OPTIONMAPPING.put("--r", "RECURSIVELY");
		OPTIONMAPPING.put("--h", "HELP");

		//FTP options
		OPTIONMAPPING.put("-f", "FTPIP");
		OPTIONMAPPING.put("-u", "USERNAME");
		OPTIONMAPPING.put("-p", "PASSWORD");
		OPTIONMAPPING.put("-fp", "FTPDIR");
		OPTIONMAPPING.put("-dport", "DPORT");
		
		//copy directory options
		OPTIONMAPPING.put("-path", "image.defaultPath");
		OPTIONMAPPING.put("--remove", "REMOVE");
	}
}
