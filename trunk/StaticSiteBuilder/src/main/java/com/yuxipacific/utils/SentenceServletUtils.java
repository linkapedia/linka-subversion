package com.yuxipacific.utils;

import com.intellisophic.builder.business.beans.SignatureObject;
import com.intellisophic.builder.business.beans.comparators.SentenceComparator;
import com.intellisophic.builder.business.context.AppServletContextListener;
import com.intellisophic.builder.view.beans.SummaryObject;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author andres
 */
public class SentenceServletUtils {

    private static final Logger LOG = Logger.getLogger(SentenceServletUtils.class);
    private static Properties prop;
    private static boolean jsonp = false;
    private static String nameCallback;

    static {
        prop = new Properties();
        try {
            prop.load(SentenceServletUtils.class.getClassLoader().getResourceAsStream("system/config.properties"));
        } catch (Exception ex) {
            LOG.error("Not load properties file to use in the servlet: " + ex.getMessage());
        }
    }

    public static void enabledJsonp(String nameCallback) {
        SentenceServletUtils.jsonp = true;
        SentenceServletUtils.nameCallback = nameCallback;
    }

    /**
     * validate data from servlet nodeId
     *
     * @param nodeid
     * @return
     */
    public static Long validateNodeID(String nodeid) {
        Long nodeIdL;
        if (nodeid != null && !nodeid.equals("")) {
            try {
                nodeIdL = Long.parseLong(nodeid);
                return nodeIdL;
            } catch (Exception e) {
                LOG.error("Error get nodeid Long: " + e.getMessage());
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * validate data from servlet taxId
     *
     * @param taxid
     * @return
     */
    public static boolean validateSentenceType(String type) {
        if (type != null && !type.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * validate data from servlet fileName
     *
     * @param fileName
     */
    public static boolean validateFileName(String fileName) {
        if (fileName != null && !fileName.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    //filers this sentences to show only better sentences.
    public static String getMsgSentencesJsonUser(List<SummaryObject> list) throws Exception {
        JSONArray ja = new JSONArray();
        JSONObject jObj;
        SentenceComparator sc = new SentenceComparator();
        Collections.sort(list, sc);
        int i = 0;
        for (SummaryObject obj : list) {
            jObj = new JSONObject();
            if (obj.getScoreByMustHaves() + obj.getScoreByNodeTitle() + obj.getScoreBySignatures() > 0) {
                jObj.put("sentence", obj.getSentence());
                ja.put(jObj);
                i++;
                if(i==20){
                    break;
                }
            }

        }
        return validateJsonp(ja.toString());
    }

    /**
     * get sentences in a json format
     *
     * @param list
     * @return
     * @throws Exception
     */
    public static String getMsgSentencesJsonAll(List<SummaryObject> list) throws Exception {
        JSONArray ja = new JSONArray();
        JSONObject jObj;
        SentenceComparator sc = new SentenceComparator();
        Collections.sort(list, sc);
        for (SummaryObject obj : list) {
            jObj = new JSONObject();
            jObj.put("sentence", obj.getSentence());
            jObj.put("scoreNodeTitle", obj.getScoreByNodeTitle());
            jObj.put("scoreMH", obj.getScoreByMustHaves());
            jObj.put("scoreSig", obj.getScoreBySignatures());
            jObj.put("signatures", obj.getSignatures());
            jObj.put("musthaves", obj.getMustHaves());
            ja.put(jObj);
        }
        return validateJsonp(ja.toString());
    }

    /**
     * message error: not sentenses found
     *
     * @return
     */
    public static String getErrorSentences() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("msg", "Not have sentences");
            obj.put("code", "1");
        } catch (JSONException ex) {
            LOG.error(ex.getMessage());
            return "Error Json data";
        }
        return validateJsonp(obj.toString());
    }

    /**
     * message error: missing parameters
     *
     * @return
     */
    public static String getErrorParameters() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("msg", "Missing Parameters");
            obj.put("code", "0");
        } catch (JSONException ex) {
            LOG.error(ex.getMessage());
            return "Error Json data";
        }
        return validateJsonp(obj.toString());
    }


    /**
     * get sentenses to one link
     *
     * @param fileName
     * @param tax
     * @return
     * @throws Exception
     */
    public static List<SummaryObject> getSentences(String fileName, Long nodeId) throws Exception {
        LOG.debug("getSentences(String, Long)");
        LOG.debug("get NodeTitle");
        String nodeTitle = AppServletContextListener.getNodeCache().getNodeByNodeID(nodeId).getTitle();
        LOG.debug("get MustHaves");
        String mustHaves = AppServletContextListener.getMustHaveCache().getMustHaveByNodeID(nodeId);
        LOG.debug("get Signatures");
        List<SignatureObject> signatures = AppServletContextListener.getSignatureCache().getSignaturesByNodeID(nodeId);
        List<SummaryObject> summaryList = null;
        String[] mustHavesL = null;
        try {
            File f = new File(fileName);
            if (mustHaves != null) {
                mustHavesL = mustHaves.split(",");
            }
            summaryList = DocUtils.getScores(f, nodeTitle, mustHavesL, signatures);
            return summaryList;
        } catch (Exception ex) {
            LOG.error("Can't get the file: " + ex.getMessage());
            return null;
        }

    }

    /**
     * set jsonp function
     *
     * @param json
     * @return
     */
    private static String validateJsonp(String json) {
        StringBuilder sb = new StringBuilder();
        if (SentenceServletUtils.jsonp) {
            sb.append(nameCallback).append("(").append(json).append(");");
            return sb.toString();
        } else {
            return json;
        }
    }
}
