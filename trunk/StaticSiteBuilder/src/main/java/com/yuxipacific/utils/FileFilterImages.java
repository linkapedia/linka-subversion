/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yuxipacific.utils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

/**
 *
 * @author andres
 */
public class FileFilterImages extends OutputStream {

    private RandomAccessFile ramdonFile = null;

    public FileFilterImages(File file, String value) throws Exception {
        long sizeFile = file.length();
        long sizeValue = value.getBytes().length;
        long seekValue = sizeFile - sizeValue;
        if (seekValue < 0) {
            seekValue = 0;
        }
        this.ramdonFile = new RandomAccessFile(file, "rw");
        this.ramdonFile.seek(seekValue);
    }

    @Override
    public void write(int b) throws IOException {
        this.ramdonFile.write(b);
    }

    @Override
    public void write(byte b[], int off, int len) throws IOException {
        this.ramdonFile.write(b, off, len);
    }

    public void write(byte b[]) throws IOException {
        this.ramdonFile.write(b);
    }
}
