package com.yuxipacific.utils;

import com.yuxipacific.utils.tika.FileHandler;
import com.yuxipacific.utils.tika.MimeTypeDetector;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.tika.metadata.Metadata;

/**
 *
 * @author Alex
 */
public class FileUtils {

    private static final Logger log = Logger.getLogger(FileUtils.class);
    private static ResourceBundle rb = ResourceBundle.getBundle("system/config");

    /**
     *
     * @param fileName
     * @param fileContent
     * @return
     */
    public static boolean writeFile(String fileName, String fileContent) {
        log.debug("writeFile(File, String)");
        boolean _tmpExito = false;
        String filePath = null;
        FileOutputStream fos = null;
        try {
            filePath = rb.getString("log.file.location");
            if (fileName != null && !"".equals(fileName.trim()) && fileContent != null && !"".equals(fileContent.trim())) {
                // Create file 
                fos = new FileOutputStream(filePath + fileName);
                fos.write(fileContent.getBytes("UTF-8"));
                fos.flush();
                //Close the output stream
                fos.close();
                _tmpExito = true;
            }
        } catch (Exception ex) {
            log.error("An exception ocurred: ", ex);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                    fos = null;
                } catch (Exception e) {
                    log.error("An exception ocurred: ", e);
                }
            }
        }
        return _tmpExito;
    }

    /**
     *
     * @param file
     * @return
     */
    public static String readTextFile(File file) {
        log.debug("readTextFile(File)");
        StringBuilder fileContent = null;
        String strLine = null;
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        BufferedReader br = null;

        try {
            fileContent = new StringBuilder();
            fis = new FileInputStream(file);
            // Here BufferedInputStream is added for fast reading.
            bis = new BufferedInputStream(fis);
            br = new BufferedReader(new InputStreamReader(bis));
            while ((strLine = br.readLine()) != null) {
                fileContent.append(strLine);
            }

            // dispose all the resources after using them.
            fis.close();
            bis.close();
            br.close();
            fis = null;
            bis = null;
            br = null;
        } catch (FileNotFoundException ex) {
            log.error("The file was not found.", ex);
        } catch (IOException ex) {
            log.error("There was a exception reading the file.", ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (bis != null) {
                    bis.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                log.error("An exception ocurred closing the streams.", e);
            }
        }
        return fileContent.toString();
    }

    public static String getFileNameWithoutExtension(File file) {
        log.debug("getFileNameWithoutExtension(File)");
        String fileName = null;
        try {
            if (file != null && file.exists()) {
                if (file.isFile()) {
                    fileName = file.getAbsolutePath();
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));
                    fileName = fileName.substring(fileName.lastIndexOf(File.separator) + 1, fileName.length());
                    log.info("File Title: " + fileName);
                }
            }
        } catch (Exception e) {
            log.error("An exception ocurred", e);
        }
        return fileName;
    }

    public static String getFileMimeType(File file) {
        log.debug("getFileMimeType(File)");
        return MimeTypeDetector.getMimeType(file);
    }

    public static String getDocTitleFromFile(File file) {
        log.debug("getFileTitle(File)");
        if (file == null) {
            log.error("The file cannot be null.");
            return null;
        }
        if (!file.isFile() || !file.exists()) {
            log.warn("The file {" + file.getAbsolutePath() + "} does not exist or it's not a file.");
            return null;
        }
        return FileHandler.getMetadata(file).get(Metadata.TITLE);
    }

    public static String getFullDocumentContent(File file) {
        log.debug("getFullDocumentContent(File)");
        return FileHandler.getContents(file);
    }
}
