package com.yuxipacific.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;

/**
 * Class to handle basic operations on zip files.
 *
 * @author Alex
 */
public class ZipUtils {

    private static final Logger log = Logger.getLogger(ZipUtils.class);

    /**
     * Method to unzip a .zip file.
     *
     * @param zipFileName Name of the zip file.
     * @param destination Destination folder path.
     * @throws Exception
     */
    public static void unZipFile(String zipFileName, String destination) throws Exception {
        log.debug("unZipFile(String)");
        log.info("Unzipping File <" + zipFileName + "> on " + destination);
        ZipFile zipFile = new ZipFile(zipFileName);
        Enumeration files = zipFile.entries();
        File file = null;
        FileOutputStream fos = null;
        InputStream eis = null;

        while (files.hasMoreElements()) {
            try {
                ZipEntry entry = (ZipEntry) files.nextElement();
                eis = zipFile.getInputStream(entry);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;

                file = new File(destination + File.separator + entry.getName());

                if (entry.isDirectory()) {
                    file.mkdirs();
                    continue;
                } else {
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                }

                fos = new FileOutputStream(file);

                while ((bytesRead = eis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                eis.close();
                eis = null;
                fos.close();
                fos = null;
            } catch (IOException e) {
                log.error("Error unzipping file", e);
                continue;
            } finally {
                try {
                    if (fos != null) {
                        fos.close();
                    }
                    if (eis != null) {
                        eis.close();
                    }
                } catch (Exception e) {
                    log.error("An exception ocurred while closing the input streams.", e);
                }

            }
        }
    }

    /**
     * Zip up a directory
     *
     * @param directory Directory that will be zipped.
     * @param zipName Name of the generated zip file.
     * @throws IOException
     */
    public static void zipDir(String directory, String zipName) throws IOException {
        log.debug("zipDir(String,String)");
        // create a ZipOutputStream to zip the data to
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipName));
        String path = "";
        zipDir(directory, zos, path);
        // close the stream
        zos.close();
    }

    /**
     * Zip up a directory
     *
     * @param directory Directory that will be zipped.
     * @param zipName Name of the generated zip file.
     * @param folderName Name of the Folder that will be included in the zip.
     * @throws IOException
     */
    public static void zipDir(String directory, String zipName, String folderName) throws IOException {
        log.debug("zipDir(String,String)");
        // create a ZipOutputStream to zip the data to
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipName));
        if (folderName == null) {
            folderName = "";
        } else {
            folderName += "/";
        }
        zipDir(directory, zos, folderName);
        // close the stream
        zos.close();
    }

    /**
     * Zip up a directory path
     *
     * @param directory Directory that will be zipped.
     * @param zos Input stream generated based on the zip file name.
     * @param path Name of the Folder that will be included in the zip.
     * @throws IOException
     */
    public static void zipDir(String directory, ZipOutputStream zos, String path) throws IOException {
        log.debug("zipDir(String, ZipOutputStream,String)");
        log.info("Creating zip File <" + directory + "> for " + path);
        File zipDir = new File(directory);
        // get a listing of the directory content
        String[] dirList = zipDir.list();
        byte[] readBuffer = new byte[2156];
        int bytesIn = 0;
        // loop through dirList, and zip the files
        for (int i = 0; i < dirList.length; i++) {
            File f = new File(zipDir, dirList[i]);
            if (f.isDirectory()) {
                String filePath = f.getPath();
                zipDir(filePath, zos, path + f.getName() + "/");
                continue;
            }
            FileInputStream fis = new FileInputStream(f);
            try {
                zos.putNextEntry(new ZipEntry(path + f.getName()));
                bytesIn = fis.read(readBuffer);
                while (bytesIn != -1) {
                    zos.write(readBuffer, 0, bytesIn);
                    bytesIn = fis.read(readBuffer);
                }
            } finally {
                fis.close();
            }
        }

    }

    /**
     *
     * @param filenames
     * @param zipFileName
     * @param destination
     */
    public static void createZipFile(String[] filenames, String zipFileName, String destination) {
        log.debug("createZipFile()");

        // Create a buffer for reading the files
        byte[] buf = new byte[1024];

        try {
            // Create the ZIP file
            String outFilename = destination + zipFileName;
            log.info("Packing pages on file: " + outFilename);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFilename)));

            // Compress the files
            for (int i = 0; i < filenames.length; i++) {
                FileInputStream in = new FileInputStream(filenames[i]);

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(filenames[i]));

                // Transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                // Complete the entry
                out.closeEntry();
                in.close();
            }

            // Complete the ZIP file
            out.close();
        } catch (IOException e) {
            log.error("Error creating the zip file", e);
        }
    }
}
