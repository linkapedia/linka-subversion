package com.intellisophic.builder.business.beans;

import java.io.Serializable;

/**
 *
 * @author andres
 */
public class SignatureObject implements Serializable{
    private String word;
    private int ocurrences;
    private String lang;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getOcurrences() {
        return ocurrences;
    }

    public void setOcurrences(int ocurrences) {
        this.ocurrences = ocurrences;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

}
