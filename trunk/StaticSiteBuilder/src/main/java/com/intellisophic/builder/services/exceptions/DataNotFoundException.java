package com.intellisophic.builder.services.exceptions;

/**
 *
 * @author Alex
 */
public class DataNotFoundException extends Exception {

    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException() {
    }
}
