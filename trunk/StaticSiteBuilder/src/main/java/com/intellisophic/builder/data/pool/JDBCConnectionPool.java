package com.intellisophic.builder.data.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class JDBCConnectionPool extends ObjectPool<Connection> {

    private String dsn, usr, pwd;
    private static final Logger log = Logger.getLogger(JDBCConnectionPool.class);

    public JDBCConnectionPool(String driver, String dsn, String usr, String pwd) {
        super();
        try {
            Class.forName(driver).newInstance();
        } catch (Exception e) {
            log.error("An exception ocurred creating the connection pool.", e);
        }
        this.dsn = dsn;
        this.usr = usr;
        this.pwd = pwd;
    }

    @Override
    protected Connection create() {
        try {
            return (DriverManager.getConnection(dsn, usr, pwd));
        } catch (SQLException e) {
            log.error("An exception ocurred creating the connection.", e);
            return (null);
        }
    }

    @Override
    public void expire(Connection o) {
        try {
            ((Connection) o).close();
        } catch (SQLException e) {
            log.error("An exception ocurred closing the connection.", e);
        }
    }

    @Override
    public boolean validate(Connection o) {
        try {
            return (!((Connection) o).isClosed());
        } catch (SQLException e) {
            log.error("An exception ocurred validating the connection.", e);
            return (false);
        }
    }
}
