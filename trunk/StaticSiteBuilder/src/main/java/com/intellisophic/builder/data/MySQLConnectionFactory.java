package com.intellisophic.builder.data;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

public class MySQLConnectionFactory {

    private static Logger log = Logger.getLogger(MySQLConnectionFactory.class);
    private static String DB_NAME = "database.name";
    private static String DB_USERNAME = "database.username";
    private static String DB_PASSWORD = "database.password";
    private static String HOST_IP_ADDRESS = "database.host.ip";
    private static final ResourceBundle resource = ResourceBundle.getBundle("database/config");

    /**
     * 
     */
    public MySQLConnectionFactory() {
        resource.getString(DB_NAME);
    }

    /**
     * 
     * @param ipAddress
     * @return
     * @throws CommunicationsException
     * @throws Exception 
     */
    public static Connection createConnectionByIP(String ipAddress) throws CommunicationsException, Exception {
        log.debug("createConnectionByIP(String)");
        Connection conn = null;
        if (ipAddress == null || ipAddress.trim().equals("")) {
            ipAddress = resource.getString(HOST_IP_ADDRESS);
        }
        try {
            conn = createConnection("jdbc:mysql://" + ipAddress + "/" + resource.getString(DB_NAME), resource.getString(DB_USERNAME), resource.getString(DB_PASSWORD));
        } catch (CommunicationsException ex) {
            log.error("An exception ocurred. Cannot find database in host {" + ipAddress + "}. " + ex.getLocalizedMessage());
            throw ex;
        } catch (Exception ex) {
            log.error("An exception ocurred. " + ex.getLocalizedMessage());
            throw ex;
        }
        return conn;
    }

    /**
     * 
     * @param dbURL
     * @param user
     * @param password
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    private static Connection createConnection(String dbURL, String user, String password) throws SQLException, ClassNotFoundException {
        log.debug("MySQLConnectionFactory::createConnection(String,String,String)");
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(dbURL, user, password);
    }
}
