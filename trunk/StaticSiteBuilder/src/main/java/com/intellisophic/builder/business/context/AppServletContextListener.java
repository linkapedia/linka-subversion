package com.intellisophic.builder.business.context;

import com.intellisophic.builder.business.camel.PageBuildProcessor;
import com.intellisophic.builder.services.cache.MustHaveCache;
import com.intellisophic.builder.services.cache.NodeCache;
import com.intellisophic.builder.services.cache.SignatureCache;
import com.intellisophic.builder.services.cache.jcs.JCSDocumentCache;
import com.intellisophic.builder.services.cache.jcs.JCSMustHaveCache;
import com.intellisophic.builder.services.cache.jcs.JCSNodeCache;
import com.intellisophic.builder.services.cache.jcs.JCSSignatureCache;
import java.util.ResourceBundle;
import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class AppServletContextListener implements ServletContextListener {

    private static CamelContext camelctx = null;
    private static final transient Logger log = Logger.getLogger(AppServletContextListener.class);
    private static final ResourceBundle rb = ResourceBundle.getBundle("system/config");
    private static MustHaveCache mustHaveCache = new MustHaveCache();
    private static SignatureCache signatureCache = new SignatureCache();
    private static NodeCache nodeCache = new NodeCache();
    private String[] corpusIds = {"2669", "1000002", "1000003"};

    @Override
    public void contextInitialized(ServletContextEvent servletCtxEv) {
        log.debug("contextInitialized()");

        for (int i = 0; i < corpusIds.length; i++) {
            //init cache
            log.debug("init cache for nodeId "+corpusIds[i]);
            AppServletContextListener.getNodeCache().initialize(Long.parseLong(corpusIds[i]));
            AppServletContextListener.getMustHaveCache().initialize(Long.parseLong(corpusIds[i]));
            AppServletContextListener.getSignatureCache().initialize(Long.parseLong(corpusIds[i]));
        }

        try {
            log.info("ServletContextListener started");
            camelctx = new DefaultCamelContext();
            camelctx.disableJMX();
            InitialContext initCtx = new InitialContext();
            Context envContext = (Context) initCtx.lookup("java:comp/env");
            ConnectionFactory connectionFactory = (ConnectionFactory) envContext.lookup("jms/ConnectionFactory");
            //Add the ConnectionFactory from ActiveMQ to the CamelContext.
            camelctx.addComponent("activemq", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
            camelctx.addRoutes(new RouteBuilder() {
                //URL Formats
                //ftp://[username@]hostname[:port]/directoryname[?options]
                //sftp://[username@]hostname[:port]/directoryname[?options]
                //ftps://[username@]hostname[:port]/directoryname[?options]

                @Override
                public void configure() throws Exception {
                    boolean isFTPEnabled = Boolean.valueOf(rb.getString("ftp.enabled"));
                    if (isFTPEnabled) {
                        //Read the zip file in the folder and sent it to the ftp server on the website.
                        from("file:" + rb.getString("zip.pages.output.final") + "?move=.done&delay=3600").to(rb.getString("ftp.website.url"));
                    } else {
                        log.info("No FTP configuration found. No camel Required. Local Webserver");
                    }
                }
            });
            camelctx.addRoutes(new RouteBuilder() {

                @Override
                public void configure() throws Exception {
                    PageBuildProcessor pageBuildProcessor = new PageBuildProcessor();
                    String strThreadNumber = rb.getString("system.classifier.threads");
                    String strConsumerNumber = rb.getString("system.classifier.consumers");
                    int threadNumber;
                    int consumerNumber;
                    if (strThreadNumber == null || strThreadNumber.isEmpty()) {
                        strThreadNumber = "1";
                    }
                    if (strConsumerNumber == null || strConsumerNumber.isEmpty()) {
                        strConsumerNumber = "1";
                    }
                    try {
                        threadNumber = Integer.valueOf(strThreadNumber);
                    } catch (NumberFormatException e) {
                        log.error("The format for thread number in the properties is wrong. " + e.getMessage(), e);
                        threadNumber = 1;
                    }
                    try {
                        consumerNumber = Integer.valueOf(strConsumerNumber);
                    } catch (NumberFormatException e) {
                        log.error("The format for consumer number in the properties is wrong. " + e.getMessage(), e);
                        consumerNumber = 1;
                    }
                    //Read from the queue to perform the classifications.
                    from("activemq:PageCreationQueue?concurrentConsumers=" + consumerNumber).routeId("PageCreationQueue").threads(threadNumber).process(pageBuildProcessor).end();
                }
            });
            //Start the camel context.
            camelctx.start();
        } catch (Exception e) {
            log.error("Error Starting Context: ", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletCtxEv) {
        log.debug("contextDestroyed()");
        try {
            if (camelctx != null) {
                camelctx.stop();
            }
            // Shutdown the cache system.
            JCSDocumentCache.shutdown();
            JCSMustHaveCache.shutdown();
            JCSNodeCache.shutdown();
            JCSSignatureCache.shutdown();
        } catch (Exception e) {
            log.error("Error Stopping Context: ", e);
        }
    }

    public static MustHaveCache getMustHaveCache() {
        return mustHaveCache;
    }

    public static void setMustHaveCache(MustHaveCache mustHaveCache) {
        AppServletContextListener.mustHaveCache = mustHaveCache;
    }

    public static NodeCache getNodeCache() {
        return nodeCache;
    }

    public static void setNodeCache(NodeCache nodeCache) {
        AppServletContextListener.nodeCache = nodeCache;
    }

    public static void setSignatureCache(SignatureCache signatureCache) {
        AppServletContextListener.signatureCache = signatureCache;
    }

    public static SignatureCache getSignatureCache() {
        return signatureCache;
    }
}
