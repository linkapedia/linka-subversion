package com.intellisophic.builder.services.cache;

import com.intellisophic.builder.business.beans.SignatureObject;
import com.intellisophic.builder.data.SignaturesDAO;
import com.intellisophic.builder.data.interfaces.ISignaturesDAO;
import com.intellisophic.builder.services.cache.jcs.JCSSignatureCache;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class SignatureCache {
    
        private static final Logger log = Logger.getLogger(SignatureCache.class);
    private JCSSignatureCache signatureCache = null;
    private boolean isFirstRun = true;

    /**
     * Class default constructor
     */
    public SignatureCache() {
        signatureCache = new JCSSignatureCache();
    }

    /**
     * Method to load the signatures from the Oracle DB into the Builder.
     *
     * @param corpusID
     * @return Map<Long, List<SignatureObject>>
     */
    public Map<Long, List<SignatureObject>> initialize(Long corpusID) {
        log.debug("initialize(Long)");
        ISignaturesDAO signaturesDao;
        Map<Long, List<SignatureObject>> signatureItems = null;
        //Check if the object is already in the cache.
        if (isFirstRun) {
            isFirstRun = false;
            log.info("Going to populate/refresh signatures cache for corpusID: " + corpusID);
            signaturesDao = new SignaturesDAO();
            try {
                signatureItems = signaturesDao.getSignaturesByCorpusID(corpusID);
                if (signatureItems != null && !signatureItems.isEmpty()) {
                    Set<Long> nodeIDs = signatureItems.keySet();
                    for (Long nodeID : nodeIDs) {
                        //Fill the cache with data.
                        signatureCache.addSignature(nodeID, signatureItems.get(nodeID));
                    }
                }
            } catch (Exception e) {
                log.error("Error in initialize(Long).", e);
            }
        } else {
            log.info("No Need to refresh the cache, already in the latest version");
        }
        return signatureItems;
    }

    /**
     * Method to get the signatures of a given node associated with a nodeID.
     *
     * @param nodeID Node Identifier.
     * @return signatures for the given node.
     */
    public List<SignatureObject> getSignaturesByNodeID(Long nodeID) {
        log.debug("getSignaturesByNodeID(Long)");
        return signatureCache.getSignatures(nodeID);
    }
}
