package com.intellisophic.builder.data.interfaces;

import java.sql.SQLException;
import java.util.Map;

/**
 *
 * @author Alex
 */
public interface IMustHaveDAO {

    public Map<Long, String> getAllMustHaves() throws SQLException, Exception;

    public Map<Long, String> getMustHavesByCorpusID(Long corpusID) throws SQLException, Exception;
}
