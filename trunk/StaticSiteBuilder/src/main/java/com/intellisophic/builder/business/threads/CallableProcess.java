package com.intellisophic.builder.business.threads;

import com.intellisophic.builder.business.BuilderProcessor;
import com.intellisophic.builder.business.beans.CorpusObject;
import com.intellisophic.builder.business.beans.PageObject;
import com.intellisophic.builder.business.beans.comparators.CorpusObjectComparator;
import com.intellisophic.builder.data.ClassifierDAO;
import com.intellisophic.builder.data.interfaces.IClassifierDAO;
import com.intellisophic.builder.data.pool.JDBCConnectionPool;
import com.yuxipacific.activemq.beans.QueueProducer;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import javax.jms.JMSException;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class CallableProcess implements Callable<Void> {

    private static final Logger log = Logger.getLogger(CallableProcess.class);
    private String[] ipAddresses = null;
    private long corpusID = 0;
    private long nodeID = 0;
    private static final CorpusObjectComparator corpusObjComparator = new CorpusObjectComparator();

    /**
     * Method to return the information of one node from all the classifiers.
     *
     * @return
     * @throws Exception
     */
    @Override
    public Void call() {
        log.debug("call");
        List<CorpusObject> _tmpFullNodeList = null;
        List<CorpusObject> _tmpNodeList;
        IClassifierDAO classifierDAO;
        try {
            if (ipAddresses != null && ipAddresses.length > 0) {
                Connection conn;
                JDBCConnectionPool connectionPool;
                for (String ipAddress : ipAddresses) {
                    connectionPool = BuilderProcessor.getConnectionPoolByIp().get(ipAddress);
                    conn = connectionPool.checkOut();
                    classifierDAO = new ClassifierDAO(conn);
                    try {
                        _tmpNodeList = classifierDAO.getClassifierDataForNodeId(getCorpusID(), getNodeID());
                        if (_tmpNodeList != null && !_tmpNodeList.isEmpty()) {
                            log.debug("First set of info found for node " + getNodeID() + ". Adding info.");
                            if (_tmpFullNodeList == null) {
                                _tmpFullNodeList = new ArrayList<CorpusObject>();
                            }
                            _tmpFullNodeList.addAll(_tmpNodeList);
                        } else {
                            log.debug("No information found for node: " + getNodeID() + " on server: " + ipAddress);
                        }
                    } catch (Exception e) {
                        log.error("An exception has ocurred in call(). ", e);
                        log.error("An exception ocurred getting data from mysql, returning conn to the pool. ");
                    }
                    connectionPool.checkIn(conn);
                }

                //Sort the list by score1, pagerank.
                if (_tmpFullNodeList != null) {
                    synchronized (corpusObjComparator) {
                        Collections.sort(_tmpFullNodeList, corpusObjComparator);
                    }
                }
                sendJobToTheQueue(_tmpFullNodeList);
            }
        } catch (Exception e) {
            log.error("An exception has ocurred in call().", e);
        }
        return null;
    }

    public long getCorpusID() {
        return corpusID;
    }

    public void setCorpusID(long corpusID) {
        this.corpusID = corpusID;
    }

    public String[] getIpAddresses() {
        return ipAddresses;
    }

    public void setIpAddresses(String[] ipAddresses) {
        this.ipAddresses = ipAddresses;
    }

    public long getNodeID() {
        return nodeID;
    }

    public void setNodeID(long nodeID) {
        this.nodeID = nodeID;
    }

    /**
     *
     * @param pageCreationPayload
     */
    public void sendJobToTheQueue(List<CorpusObject> pageCreationPayload) {
        log.debug("sendJobToTheQueue(List<CorpusObject>)");
        if (pageCreationPayload == null || pageCreationPayload.isEmpty()) {
            log.error("The page creation payload does not contains any data.");
            return;
        }
        try {
            PageObject tmpPage = new PageObject();
            log.debug("Node {" + nodeID + "} ready for queue and page creation");
            tmpPage.setNodes(pageCreationPayload);
            tmpPage.setNodeId(nodeID);
            tmpPage.setCorpusID(corpusID);
            log.debug("Sending " + tmpPage + " to the queue for creation.");
            QueueProducer.sendMessage(tmpPage);
        } catch (IllegalArgumentException e) {
            log.error("Error in sendJobToTheQueue, IllegalArgumentException. ", e);
        } catch (JMSException e) {
            log.error("Error in sendJobToTheQueue, JMSException. ", e);
        } catch (NamingException e) {
            log.error("Error in sendJobToTheQueue, NamingException. ", e);
        }
    }
}
