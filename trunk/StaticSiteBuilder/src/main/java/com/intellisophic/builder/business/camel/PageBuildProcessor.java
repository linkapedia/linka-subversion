package com.intellisophic.builder.business.camel;

import com.intellisophic.builder.business.TreeMenuBO;
import com.intellisophic.builder.business.beans.CorpusObject;
import com.intellisophic.builder.business.beans.Node;
import com.intellisophic.builder.business.beans.PageObject;
import com.intellisophic.builder.business.beans.SignatureObject;
import com.intellisophic.builder.business.beans.comparators.SentenceComparator;
import com.intellisophic.builder.business.context.AppServletContextListener;
import com.intellisophic.builder.view.beans.SummaryObject;
import com.intellisophic.builder.view.beans.ViewLink;
import com.intellisophic.builder.view.beans.ViewNode;
import com.intellisophic.builder.view.templates.BuildPages;
import com.yuxipacific.utils.DigestUtils;
import com.yuxipacific.utils.DocUtils;
import com.yuxipacific.utils.FileUtils;
import com.yuxipacific.utils.StringUtils;
import java.io.File;
import java.util.*;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 *
 * @author Alex
 */
public class PageBuildProcessor implements Processor {
    
    private static final Logger log = Logger.getLogger(PageBuildProcessor.class);
    private static final ResourceBundle systemResource = ResourceBundle.getBundle("system/config");
    
    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("process(Exchange)");
        Object page = exchange.getIn().getBody();
        if (page instanceof PageObject) {
            PageObject pageObject = (PageObject) page;
            log.debug("Received message to create page for node: " + pageObject.getNodeId());
            //In this point the list of CorpusObjects in the page are related to one node.
            //Transform the CorpusObject into ViewNodes
            ViewNode viewNode = transformCorpusObjectsIntoViewNode(pageObject.getNodeId(), pageObject.getCorpusID(), pageObject.getNodes());
            generateStaticHtmlForViewNode(viewNode);
        }
    }

    /**
     *
     * @param nodeID
     * @param corpusObjectList
     * @return
     */
    private ViewNode transformCorpusObjectsIntoViewNode(Long nodeID, Long corpusID, List<CorpusObject> corpusObjectList) {
        log.debug("transformCorpusObjectsIntoViewNode(Long, Long, List)");
        List<ViewLink> _tmpNodeLinks;
        ViewNode viewNode = null;
        log.info("Going to transform " + corpusObjectList.size() + " nodes.");
        log.debug("Transforming node: " + nodeID + " from Node to ViewNode.");
        Node _tmpNode = AppServletContextListener.getNodeCache().getNodeByNodeID(nodeID);
        Node rootNode;
        String docTitle;
        if (_tmpNode != null) {
            try {
                log.debug("Direct hit {node in cache}");
                viewNode = new ViewNode();
                viewNode.setNodeId(nodeID);
                _tmpNodeLinks = new ArrayList<ViewLink>();
                List<SummaryObject> listSummaryObject = new ArrayList<SummaryObject>();
                File fileSentence = null;
                if (corpusObjectList != null && !corpusObjectList.isEmpty()) {
                    File localFile;
                    String mustHavesFromCache;
                    List<SignatureObject> signatures = null;
                    String docSummary = null;
                    ViewLink viewLink;
                    for (CorpusObject corpusObject : corpusObjectList) {
                        viewLink = new ViewLink();
                        docTitle = corpusObject.getDocTitle();
                        //TODO this is to test remove /home/andres/yuxi/builderFolder/nas in production
                        //localFile = new File("/home/andres/yuxi/builderFolder/nas" + corpusObject.getWebCacheURI());
                        localFile = new File(corpusObject.getWebCacheURI());
                        log.debug("Web Cache URI:" + corpusObject.getWebCacheURI());
                        String md5Name = DigestUtils.getMD5(corpusObject.getURI());
                        fileSentence = new File(localFile.getParentFile().getParent(), "extracted" + File.separator + md5Name);
                        
                        
                        if (docTitle == null || "".equals(docTitle.trim())) {
                            docTitle = FileUtils.getDocTitleFromFile(localFile);
                        }
                        viewLink.setDocTitle(docTitle);
                        viewLink.setUri(corpusObject.getURI());
                        //Add here the logic to calculate the docsummary.
                        log.debug("Getting must haves for node: " + nodeID);
                        mustHavesFromCache = AppServletContextListener.getMustHaveCache().getMustHaveByNodeID(nodeID);
                        signatures = AppServletContextListener.getSignatureCache().getSignaturesByNodeID(nodeID);
                        
                        if (mustHavesFromCache != null) {
                            String[] mustHaves = mustHavesFromCache.split(",");

                            //String docContent = FileUtils.getFullDocumentContent(localFile);
                            log.debug("Getting sentences score");
                            try {
                                //TODO create a cache to save this
                                listSummaryObject = DocUtils.getScores(fileSentence, _tmpNode.getTitle(), mustHaves, signatures);  
                            } catch (Exception e) {
                                log.error("I CAN'T TO GET THE SENCENCE SCORE -> " + e.getMessage());
                            }
                        }else{
                            log.error("Not get musthaves from cache");
                        }
                        //logic to extract doc summary
                        int numSen = 1;
                        String summaryDefault = systemResource.getString("builder.summary.notgenerated");;
                        docSummary = "";
                        //sort the list
                        SentenceComparator com = new SentenceComparator();
                        Collections.sort(listSummaryObject, com);
                        for (SummaryObject so : listSummaryObject) {
                            if (so.getScoreByMustHaves() + so.getScoreByNodeTitle() + so.getScoreBySignatures() == 0) {
                                break;
                            }
                            if (numSen > 2) {
                                break;
                            }
                            docSummary += " " + so.getSentence();
                            numSen++;
                        }
                        if (!docSummary.equals("")) {
                            viewLink.setDocSummary(docSummary);
                        } else {
                            viewLink.setDocSummary(summaryDefault);
                        }
                        viewLink.setPathFileSentence(fileSentence.getAbsolutePath());
                        viewLink.setPathFile(localFile.getAbsolutePath());
                        viewLink.setLinkMd5(md5Name);

                        //TODO: Remove this, this is temporal
                        viewLink.setScore01(corpusObject.getScore01());
                        viewLink.setScore02(corpusObject.getScore02());
                        viewLink.setPagerank(corpusObject.getPagerank());
                        _tmpNodeLinks.add(viewLink);
                    }
                }
                viewNode.setLinks(_tmpNodeLinks);
                viewNode.setNodeMustHaves(AppServletContextListener.getMustHaveCache().getMustHaveByNodeID(nodeID));

                // This is the parent of the current node, It could be diferent than the root node.
                viewNode.setParentId(_tmpNode.getParent().getId());
                // This is the root node
                rootNode = AppServletContextListener.getNodeCache().getRootNodeByCorpusID(corpusID);
                viewNode.setRootNode(rootNode);
                viewNode.setNodeTitle(_tmpNode.getTitle());
                viewNode.setNodeDesc(_tmpNode.getDescription());
                //Path from the node to the parent including .html
                viewNode.setPath(calculatePathForNode(_tmpNode));
                viewNode.setNodeIndex(Integer.parseInt(_tmpNode.getIndexWithinParent()));
                //Set the path prefix for the site: ie, /linkapedia/taxonomyroot/nodetitle/.***
                String pathPrefix = systemResource.getString("webserver.linkapedia.foldername");
                if (pathPrefix != null) {
                    viewNode.setPathPrefix(pathPrefix);
                }
                log.debug("Node {" + nodeID + "} successfully transformed to ViewNode.");
            } catch (Exception ex) {
                log.error("An exception ocurred.", ex);
            }
        } else {
            log.warn("Node with id=" + nodeID + " was not found in the NodeCache using CorpusID");
        }
        return viewNode;
    }
    
    private String calculatePathForNode(Node currentNode) {
        log.debug("calculatePathForNode(Node)");
        String strPathToRoot = "";
        if (currentNode == null) {
            return strPathToRoot;
        }
        //Make the current node the parent.
        Node parentNode = currentNode;
        Long parentId;
        
        log.debug("Going to get the hierarchy for node {" + currentNode.getId() + "}");
        
        strPathToRoot = "/" + StringUtils.encodeFileName(parentNode.getTitle()) + strPathToRoot;
        parentId = parentNode.getParent().getId();
        parentNode = AppServletContextListener.getNodeCache().getNodeByNodeID(parentId);
        
        while (parentNode != null) {
            strPathToRoot = "/" + StringUtils.encodeFileName(parentNode.getTitle()) + strPathToRoot;
            parentId = parentNode.getParent().getId();
            parentNode = AppServletContextListener.getNodeCache().getNodeByNodeID(parentId);
            if (parentNode != null) {
                log.debug("Parent found: " + parentNode.getId());
            } else {
                log.debug("Parent not found: This is the root node");
            }
        }
        return strPathToRoot;
    }

    /**
     *
     * @param corpusID
     * @param elements
     */
    public void generateStaticHtmlForViewNode(ViewNode viewNode) {
        log.debug("generateStaticHtml(ViewNode)");
        try {
            //Get the Tree for the menu.
            log.debug("Create menu for node: " + viewNode.getNodeId());
            Map<Integer, ArrayList<Element>> nodeTreeMenu = TreeMenuBO.getTreeMenuForByNodeID(viewNode.getNodeId());
            log.debug("Tree Generated");
            //Get the parent node of the taxonomy from the cache.
            Node rootNode = AppServletContextListener.getNodeCache().getNodeByNodeID(viewNode.getRootNode().getId());
            if (rootNode != null) {
                //Create The Files For this Taxonomy.
                log.info("Setting up the parameters for the Velocity Engine.");
                log.info("Creating page for node: " + viewNode.getNodeId());
                //Validate if we need to retrieve the child information for this page.
                if (viewNode.isBridgeNode()) {
                    //We need to get the childs of this node.
                    List<Element> childsForBridge = nodeTreeMenu.get(TreeMenuBO.LEVEL3);
                    log.debug("Bridge Node Id: " + viewNode.getNodeId());
                    Long nodeId;
                    Node _tmpChild;
                    Map<Long, String> bridgeChildsInfo = new HashMap<Long, String>();
                    for (Element nodeAsXML : childsForBridge) {
                        nodeId = nodeAsXML.getAttribute("id").getLongValue();
                        _tmpChild = AppServletContextListener.getNodeCache().getNodeByNodeID(nodeId);
                        bridgeChildsInfo.put(nodeId, _tmpChild.getDescription());
                    }
                    viewNode.setBridgeChildsInfo(bridgeChildsInfo);
                }
                BuildPages.createPages(rootNode.getTitle(), nodeTreeMenu, viewNode);
            } else {
                log.warn("Parent Node not found for taxonomy");
            }
        } catch (Exception e) {
            log.error("Error in generateStaticHtml().", e);
        }
    }
}
