
package com.intellisophic.builder.business.interfaces;

/**
 *
 * @author andres
 */
public interface INotifyWorkerImages {
    
    public void onFinish(String t);
}
