package com.intellisophic.builder.data;

import com.intellisophic.builder.data.interfaces.IMustHaveDAO;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class MustHavesDAO extends OracleConnectionFactory implements IMustHaveDAO {
    private static final Logger log = Logger.getLogger(MustHavesDAO.class);

    /**
     * Method to query all the must haves.
     * @return Map<Long,String> Object that contains the Key=NodeID, Value=Must word list comma separated.
     * @throws SQLException
     * @throws Exception 
     */
    @Override
    public Map<Long, String> getAllMustHaves() throws SQLException, Exception {
        log.debug("getAllMustHaves()");
        Map<Long, String> allMustHaves = new HashMap<Long, String>();
        StringBuilder mustHaveList = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = createConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT NODEID,MUSTWORD FROM MUSTHAVE");
            while (rs.next()) {
                Long nodeID = rs.getLong("NODEID");
                String mustHave = rs.getString("MUSTWORD");
                if (allMustHaves.containsKey(nodeID)) {
                    mustHaveList = new StringBuilder(allMustHaves.get(nodeID));
                    mustHaveList.append(",");
                    mustHaveList.append(mustHave);
                } else {
                    mustHaveList = new StringBuilder(mustHave);
                }
                allMustHaves.put(nodeID, mustHaveList.toString());
            }
            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
        } catch (SQLException e) {
            log.error("SQL Error in getAllMustHaves()", e);
            throw e;
        } catch (Exception e) {
            log.error("General Error in getAllMustHaves()", e);
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                log.error("General Error in getAllMustHaves() closing resources", e);
                throw e;
            }
        }
        return allMustHaves;
    }

    /**
     * Method that query the must haves for a taxomony.
     * @param corpusID Taxonomy Id
     * @return Map<Long,String> Object that contains the Key=NodeID, Value=Must word list comma separated.
     * @throws SQLException
     * @throws Exception 
     */
    @Override
    public Map<Long, String> getMustHavesByCorpusID(Long corpusID) throws SQLException, Exception {
        log.debug("getMustHavesByCorpusID()");
        Map<Long, String> mustHaves = new HashMap<Long, String>();
        StringBuilder mustHaveList = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = createConnection();
            pst = conn.prepareStatement("SELECT N.NODEID, M.MUSTWORD FROM NODE N, MUSTHAVE M WHERE N.NODEID = M.NODEID AND N.CORPUSID = ?");
            pst.setLong(1, corpusID);
            rs = pst.executeQuery();
            while (rs.next()) {
                Long nodeID = rs.getLong("NODEID");
                String mustHave = rs.getString("MUSTWORD");
                if (mustHaves.containsKey(nodeID)) {
                    mustHaveList = new StringBuilder(mustHaves.get(nodeID));
                    mustHaveList.append(",");
                    mustHaveList.append(mustHave);
                } else {
                    mustHaveList = new StringBuilder(mustHave);
                }
                mustHaves.put(nodeID, mustHaveList.toString());
            }
            rs.close();
            rs = null;
            pst.close();
            pst = null;
            conn.close();
            conn = null;
        } catch (SQLException e) {
            log.error("SQL Error in getMustHavesByCorpusID()", e);
            throw e;
        } catch (Exception e) {
            log.error("General Error in getMustHavesByCorpusID()", e);
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pst != null) {
                    pst.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                log.error("General Error in getMustHavesByCorpusID() closing resources", e);
                throw e;
            }
        }
        return mustHaves;
    }
}
