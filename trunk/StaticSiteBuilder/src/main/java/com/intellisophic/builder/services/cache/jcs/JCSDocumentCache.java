package com.intellisophic.builder.services.cache.jcs;

import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.access.exception.InvalidArgumentException;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class JCSDocumentCache {
    private static final Logger log = Logger.getLogger(JCSDocumentCache.class);
    private JCS contentCache = null;
    private JCS titleCache = null;
    private static final String CONTENT_CACHE_IDENTIFIER = "documentCache";
    private static final String TITLE_CACHE_IDENFIFIER = "titleCache";

    /**
     * Class contructor used to initialize the cache.
     */
    public JCSDocumentCache() {
        log.debug("Document Cache Constructor.");
        try {
            // Load the cache
            contentCache = JCS.getInstance(CONTENT_CACHE_IDENTIFIER);
            titleCache = JCS.getInstance(TITLE_CACHE_IDENFIFIER);
            log.info("Document Cache Initialized.");
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method that allows to add new document content in the cache.
     * @param path Route to the document on disk.
     * @param content Content parsed of the document.
     */
    public void addDocumentContent(String path, String content) {
        log.debug("addDocumentContent(String, String)");
        try {
            if (path == null) {
                throw new InvalidArgumentException("The object to store can not be null");
            }
            if (content == null || content.trim().equals("")) {
                throw new InvalidArgumentException("The object properties can not be null or empty in " + path);
            }
            contentCache.put(path, content);
        } catch (InvalidArgumentException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        } catch (CacheException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        }
    }

    /**
     * Method to return the document content for a given Path.
     * @param documentPath Path to the document.
     * @return Content of the given document.
     */
    public String getDocumentContent(String documentPath) {
        log.debug("getDocumentContent(String)");
        return (String) contentCache.get(documentPath);
    }

    /**
     * Method to remove a document content from the cache for a path.
     * @param documentPath Path to the document.
     */
    public void removeDocumentContent(String documentPath) {
        log.debug("removeDocumentContent(String)");
        try {
            contentCache.remove(documentPath);
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method that allows to add new document titles in the cache.
     * @param path Route to the document on disk.
     * @param title title parsed of the document.
     */
    public void addDocumentTitle(String path, String title) {
        log.debug("addDocumentTitle(String, String)");
        try {
            if (path == null) {
                throw new InvalidArgumentException("The object to store can not be null");
            }
            if (title == null || "".equals(title.trim())) {
                throw new InvalidArgumentException("The object properties can not be null or empty in " + path);
            }
            titleCache.put(path, title);
        } catch (InvalidArgumentException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        } catch (CacheException ex) {
            log.error("An exception ocurred: " + ex.getMessage(), ex);
        }
    }

    /**
     * Method to return the document title for a given Path.
     * @param documentPath Path to the document.
     * @return title of the given document.
     */
    public String getDocumentTitle(String documentPath) {
        log.debug("getDocumentTitle(String)");
        return (String) titleCache.get(documentPath);
    }

    /**
     * Method to remove a document from the cache for a path.
     * @param documentPath Path to the document.
     */
    public void removeDocumentTitle(String documentPath) {
        log.debug("removeDocumentTitle(String)");
        try {
            titleCache.remove(documentPath);
        } catch (CacheException ex) {
            log.error("An exception ocurred", ex);
        }
    }

    /**
     * Method to close the Cache when the application shutdown.
     */
    public static void shutdown() {
        CompositeCacheManager compositeCacheManager = CompositeCacheManager.getInstance();
        compositeCacheManager.release();
    }
}
