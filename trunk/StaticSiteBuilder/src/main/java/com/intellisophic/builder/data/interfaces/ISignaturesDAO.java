/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.builder.data.interfaces;

import com.intellisophic.builder.business.beans.SignatureObject;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public interface ISignaturesDAO {

    public Map<Long, List<SignatureObject>> getAllSignatures() throws SQLException, Exception;

    public Map<Long, List<SignatureObject>> getSignaturesByCorpusID(Long corpusID) throws SQLException, Exception;
}
