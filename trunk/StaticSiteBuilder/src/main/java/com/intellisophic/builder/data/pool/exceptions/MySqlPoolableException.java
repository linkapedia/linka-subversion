package com.intellisophic.builder.data.pool.exceptions;

/**
 *
 * @author Xander Kno
 */
public class MySqlPoolableException extends Exception {
    public MySqlPoolableException(final String msg, Exception e) {
        super(msg, e);
    }
}