package com.intellisophic.builder.data.interfaces;

import com.intellisophic.builder.business.beans.CorpusObject;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alex
 */
public interface IClassifierDAO {

    /**
     * Method to get the data from the classifier db by corpus id.
     *
     * @param corpusID Identifier of the taxonomy.
     * @return {@code Map<Long, List<CorpusObject>>} Map containing the info
     * from the db [Key=NodeID,Value=List<CorpusObject>]
     * @throws SQLException
     * @throws Exception
     */
    public Map<Long, List<CorpusObject>> getClassifierDataForCorpusID(Long corpusID) throws SQLException, Exception;

    /**
     * Method to get the data from the classifier db using the corpus and node
     * IDs.
     *
     * @param corpusID Taxonomy identifier
     * @param nodeID Node identifier.
     * @return {@code List<CorpusObject>} List containing the information for
     * the node from db.
     * @throws SQLException
     * @throws Exception
     */
    public List<CorpusObject> getClassifierDataForNodeId(Long corpusID, Long nodeID) throws SQLException, Exception;

    /**
     * Method to get the list of nodes id of the nodes that contain information.
     *
     * @param corpusID Taxonomy identifier
     * @return {@code List<String>} List containing the information for the taxonomy
     * @throws SQLException
     * @throws Exception
     */
    public List<Long> getClassifiedNodesByCorpusID(Long corpusID) throws SQLException, Exception;
}
