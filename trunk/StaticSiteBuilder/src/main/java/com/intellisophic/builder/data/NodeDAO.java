package com.intellisophic.builder.data;

import com.intellisophic.builder.business.beans.Node;
import com.intellisophic.builder.data.interfaces.INodeDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class NodeDAO extends OracleConnectionFactory implements INodeDAO {

    private static final Logger log = Logger.getLogger(NodeDAO.class);

    /**
     * Method to retrieve the all the nodes of a given taxonomy.
     *
     * @param corpusID
     * @return
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public List<Node> getNodesByCorpusID(Long corpusID) throws SQLException, Exception {
        log.debug("getNodeByCorpusID(Long)");
        List<Node> nodeList = new ArrayList<Node>();
        Node node;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = createConnection();
            //TODO Move this queries to properties file
            pst = conn.prepareStatement("SELECT NODEID,NODETITLE,NODEDESC,PARENTID,NODEINDEXWITHINPARENT FROM NODE WHERE CORPUSID = ? ORDER BY PARENTID ASC, NODETITLE ASC");
            pst.setLong(1, corpusID);
            rs = pst.executeQuery();
            Node parent;
            while (rs.next()) {
                node = new Node();
                node.setId(rs.getLong("NODEID"));
                node.setTitle(rs.getString("NODETITLE"));
                node.setDescription(rs.getString("NODEDESC"));
                node.setCorpusID(corpusID);
                parent = new Node();
                parent.setId(rs.getLong("PARENTID"));
                node.setParent(parent);
                node.setIndexWithinParent(rs.getString("NODEINDEXWITHINPARENT"));
                nodeList.add(node);
            }
        } catch (SQLException e) {
            log.error("SQL Error in getNodeByCorpusID()");
            throw e;
        } catch (Exception e) {
            log.error("General Error in getNodeByCorpusID()");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pst != null) {
                    pst.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                log.error("General Error in getNodeByCorpusID() closing resources");
                throw e;
            }
        }
        return nodeList;
    }

    /**
     * Method to return the root node of a given taxonomy
     *
     * @param corpusID Taxonomy Identifier.
     * @return Taxonomy root node
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public Node getRootNodeByCorpusID(Long corpusID) throws SQLException, Exception {
        log.debug("getRootNodeByCorpusID(Long)");
        Node node = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = createConnection();
            //TODO Move this queries to properties file
            pst = conn.prepareStatement("SELECT NODEID,NODETITLE,NODEDESC,PARENTID,CORPUSID,NODEINDEXWITHINPARENT FROM NODE WHERE CORPUSID = ? AND PARENTID = -1");
            pst.setLong(1, corpusID);
            rs = pst.executeQuery();
            Node parent;
            if (rs.next()) {
                node = new Node();
                node.setId(rs.getLong("NODEID"));
                node.setTitle(rs.getString("NODETITLE"));
                node.setDescription(rs.getString("NODEDESC"));
                node.setCorpusID(corpusID);
                parent = new Node();
                parent.setId(rs.getLong("PARENTID"));
                node.setParent(parent);
                node.setIndexWithinParent(rs.getString("NODEINDEXWITHINPARENT"));
            }
            rs.close();
            rs = null;
            pst.close();
            pst = null;
            conn.close();
            conn = null;
        } catch (SQLException ex) {
            log.error("SQL Error in getRootNodeByCorpusID()", ex);
            throw ex;
        } catch (Exception ex) {
            log.error("General Error in getRootNodeByCorpusID()", ex);
            throw ex;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pst != null) {
                    pst.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                log.error("General Error in getRootNodeByCorpusID() closing resources");
                throw e;
            }
        }
        return node;
    }
}
