package com.iw.system;

public class DataIntegrityViolation extends Exception {
    public DataIntegrityViolation() { super(); }
    public DataIntegrityViolation(String s) { super(s); }
}
