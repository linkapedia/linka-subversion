package com.iw.bo.interfaces;

import com.iw.db.beans.ConfigParam;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alex
 */
public interface IConfigParamsBO {

    public List<ConfigParam> getConfigParamsAsList() throws Exception;

    public Map<String, String> getConfigParamsAsMap() throws Exception;
}
