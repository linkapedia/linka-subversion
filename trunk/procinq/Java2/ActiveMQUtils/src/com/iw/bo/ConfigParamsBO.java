package com.iw.bo;

import com.iw.bo.interfaces.IConfigParamsBO;
import com.iw.db.beans.ConfigParam;
import com.iw.db.dao.ConfigParamsDAO;
import com.iw.db.dao.interfaces.IConfigParamsDAO;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class ConfigParamsBO implements IConfigParamsBO {

    private static final Logger log = Logger.getLogger(ConfigParam.class);
    private IConfigParamsDAO configDAO;

    public ConfigParamsBO() {
        configDAO = new ConfigParamsDAO();
    }

    @Override
    public List<ConfigParam> getConfigParamsAsList() throws Exception {
        log.debug("getConfigParamsAsList()");
        return getConfigDAO().getConfigParamsAsList();
    }

    @Override
    public Map<String, String> getConfigParamsAsMap() throws Exception {
        return getConfigDAO().getConfigParamsAsMap();
    }

    public IConfigParamsDAO getConfigDAO() {
        return configDAO;
    }

    public void setConfigDAO(IConfigParamsDAO configDAO) {
        this.configDAO = configDAO;
    }
}
