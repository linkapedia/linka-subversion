package com.iw.camel.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

import com.iw.application.context.AppServletContextListener;
import com.iw.rest.beans.Classifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegistrationProcessor implements Processor {

    private static final transient Logger log = LoggerFactory.getLogger(RegistrationProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("RegistrationProcessor::process(Exchange)");
        Message message = exchange.getIn();
        Object registrationObj = message.getBody();
        if (registrationObj instanceof Classifier) {
            Classifier classifierToRegister = (Classifier) registrationObj;
            log.info("RegMessage Received, Register Classifier with ID: " + classifierToRegister.getId());
            if(classifierToRegister.isRunning() && classifierToRegister.isShuttingDown()){
                log.info("The classifier ["+classifierToRegister.getId()+"] is shutting down, eliminate Registration from List");
                AppServletContextListener.getClassifierList().remove(classifierToRegister.getId());
            } else {
                AppServletContextListener.getClassifierList().put(classifierToRegister.getId(), classifierToRegister);
            }
        }
    }
}
