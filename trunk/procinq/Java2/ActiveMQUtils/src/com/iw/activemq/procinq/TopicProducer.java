package com.iw.activemq.procinq;

import com.iw.activemq.beans.ConfigurationPackage;
import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

public class TopicProducer {

    private static Logger log = Logger.getLogger(TopicProducer.class);

    public static void sendMessage(ConfigurationPackage confPackage) throws IllegalArgumentException, JMSException, NamingException {
        log.debug("Producer::sendMessage(ConfigurationPackage)");
        if (confPackage == null || (confPackage.getTask() != null && confPackage.getTask().trim().equals(""))) {
            log.error("Error in Producer::sendMessage(ConfigurationPackage) No Package provided to send to the topic.");
            throw new IllegalArgumentException("No Package provided to send to the topic");
        }
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            InitialContext initCtx = new InitialContext();
            Context envContext = (Context) initCtx.lookup("java:comp/env");
            ConnectionFactory connectionFactory = (ConnectionFactory) envContext.lookup("jms/ConnectionFactory");
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer((Destination) envContext.lookup("jms/topic/ConfigTopic"));
            Message objMessage = session.createObjectMessage(confPackage);
            long sendMsgStarted = System.currentTimeMillis();
            producer.send(objMessage);
            long sendMsgEnded = System.currentTimeMillis();
            log.info("Message sent in: " + (sendMsgEnded - sendMsgStarted) + "ms");
        } catch (JMSException exception) {
            log.error("Error in Producer::sendMessage(ConfigurationPackage)", exception);
            throw exception;
        } catch (NamingException exception) {
            log.error("Error in Producer::sendMessage(ConfigurationPackage)", exception);
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (session != null) {
                session.close();
            }
            if (producer != null) {
                producer.close();
            }
        }
    }
}