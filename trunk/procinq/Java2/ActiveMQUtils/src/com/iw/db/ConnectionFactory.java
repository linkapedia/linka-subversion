package com.iw.db;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class ConnectionFactory {

    public static final String ORACLE = "Oracle";
    public static final String MYSQL = "MySQL";
    private static final Logger log = Logger.getLogger(ConnectionFactory.class);
    private static Context initCtx = null;
    private static OracleDataSource oracleDS = null;
    private static DataSource mysqlDataSource = null;

    static {
        try {
            initCtx = new InitialContext();
        } catch (NamingException ex) {
            log.error("An exception ocurred while initializing context.", ex);
        }
    }

    /**
     *
     * @param dbType
     * @return
     */
    public static Connection createConnection(String dbType) {
        log.debug("createConnection()");
        Connection conn = null;
        try {
            if (dbType.equals(ORACLE)) {
                conn = getOracleConnection();
            } else if (dbType.equals(MYSQL)) {
                conn = getMySQLConnection();
            }
        } catch (Exception e) {
            log.error("Error in createConnection().", e);
        }
        return conn;
    }

    /**
     *
     * @return
     */
    private static Connection getOracleConnection() {
        log.debug("getOracleConnection()");
        Connection connection = null;
        try {
            if (oracleDS == null) {
                log.info("Oracle Datasource Initialized.");
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                oracleDS = (OracleDataSource) envCtx.lookup("jdbc/LinkapediaOracleDB");
            }
            connection = oracleDS.getConnection();
        } catch (NamingException e) {
            log.error("Error in getConnection", e);
        } catch (SQLException e) {
            log.error("Error in getConnection", e);
        }
        return connection;
    }

    /**
     *
     * @return @throws Exception
     */
    private static Connection getMySQLConnection() throws Exception {
        log.debug("getMySQLConnection()");
        Connection connection = null;
        try {
            if (mysqlDataSource == null) {
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                mysqlDataSource = (DataSource) envCtx.lookup("jdbc/LinkapediaMySQLDB");
            }
            connection = mysqlDataSource.getConnection();
        } catch (NamingException e) {
            log.error("Error in getConnection", e);
            throw e;
        } catch (SQLException e) {
            log.error("Error in getConnection", e);
            throw e;
        }
        return connection;
    }
}
