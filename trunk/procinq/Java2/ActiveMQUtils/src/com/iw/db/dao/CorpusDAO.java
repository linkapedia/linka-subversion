package com.iw.db.dao;

import com.iw.db.beans.Terms;
import com.iw.db.dao.interfaces.ICorpusDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class CorpusDAO implements ICorpusDAO {

    private static final Logger log = Logger.getLogger(CorpusDAO.class);
    private Connection conn = null;

    public CorpusDAO(Connection conn) {
        this.conn = conn;
    }

    private CorpusDAO() {
    }

    @Override
    public String getAffinityTermsByCorpusID(Integer corpusID) throws Exception {
        log.debug("getAffinityTermsByCorpusID(Integer)");
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query;
        String strTerms = null;
        try {
            query = "SELECT corpusterms FROM corpus WHERE corpusaffinity IS NOT NULL AND corpusaffinity <> 0 AND corpusterms IS NOT NULL AND corpusid = ?";
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, corpusID);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                strTerms = rs.getString("corpusterms");
            }
            rs.close();
            rs = null;
            pstmt.close();
            pstmt = null;
        } catch (SQLException ex) {
            log.error("An exception ocurred", ex);
            throw ex;
        } catch (Exception ex) {
            log.error("An exception ocurred", ex);
            throw ex;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (Exception ex) {
                log.error("An exception ocurred closing db resources", ex);
            }
        }
        return strTerms;
    }

    @Override
    public List<Terms> getAllEnabledAffinityTerms() throws Exception {
        log.debug("getAllEnabledAffinityTerms()");
        Statement stmt = null;
        ResultSet rs = null;
        String query;
        List<Terms> terms = new ArrayList<Terms>();
        Terms _tmpTerms;
        try {
            query = "SELECT corpusid, corpusaffinity, corpusterms FROM corpus WHERE corpusaffinity IS NOT NULL AND corpusaffinity <> 0 AND corpusterms IS NOT NULL";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                _tmpTerms = new Terms();
                _tmpTerms.setCorpusID(rs.getInt("corpusid"));
                _tmpTerms.setTerms(rs.getString("corpusterms"));
                terms.add(_tmpTerms);
            }
            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
        } catch (SQLException ex) {
            log.error("An exception ocurred", ex);
            throw ex;
        } catch (Exception ex) {
            log.error("An exception ocurred", ex);
            throw ex;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            } catch (Exception ex) {
                log.error("An exception ocurred closing db resources", ex);
            }
        }
        return terms;
    }
}
