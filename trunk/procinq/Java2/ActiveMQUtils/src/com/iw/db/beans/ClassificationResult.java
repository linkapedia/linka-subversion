package com.iw.db.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Class created to handle the classification results and save them in the database.
 *
 * @author Alex
 */
public class ClassificationResult implements Serializable {

    private int nodeID;
    private String remoteURI;
    private String localURI;
    private String parsedURI;
    private String extractedURI;
    private String docTitle;
    private Date timestamp;
    private float score01;
    private float score02;
    private float score03;
    private float score04;
    private float score05;
    private float score06;
    private float score07;
    private float score08;
    private float score09;
    private float score10;
    private float score11;
    private float score12;
    private float pageRankScore;

    public String getDocTitle() {
        return docTitle;
    }

    public void setDocTitle(String docTitle) {
        this.docTitle = docTitle;
    }

    public int getNodeID() {
        return nodeID;
    }

    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getLocalURI() {
        return localURI;
    }

    public void setLocalURI(String localURI) {
        this.localURI = localURI;
    }

    public String getRemoteURI() {
        return remoteURI;
    }

    public void setRemoteURI(String remoteURI) {
        this.remoteURI = remoteURI;
    }

    public String getParsedURI() {
        return parsedURI;
    }

    public void setParsedURI(String parsedURI) {
        this.parsedURI = parsedURI;
    }

    public String getExtractedURI() {
        return extractedURI;
    }

    public void setExtractedURI(String extractedURI) {
        this.extractedURI = extractedURI;
    }

    public float getScore01() {
        return score01;
    }

    public void setScore01(float score01) {
        this.score01 = score01;
    }

    public float getScore02() {
        return score02;
    }

    public void setScore02(float score02) {
        this.score02 = score02;
    }

    public float getScore03() {
        return score03;
    }

    public void setScore03(float score03) {
        this.score03 = score03;
    }

    public float getScore04() {
        return score04;
    }

    public void setScore04(float score04) {
        this.score04 = score04;
    }

    public float getScore05() {
        return score05;
    }

    public void setScore05(float score05) {
        this.score05 = score05;
    }

    public float getScore06() {
        return score06;
    }

    public void setScore06(float score06) {
        this.score06 = score06;
    }

    public float getScore07() {
        return score07;
    }

    public void setScore07(float score07) {
        this.score07 = score07;
    }

    public float getScore08() {
        return score08;
    }

    public void setScore08(float score08) {
        this.score08 = score08;
    }

    public float getScore09() {
        return score09;
    }

    public void setScore09(float score09) {
        this.score09 = score09;
    }

    public float getScore10() {
        return score10;
    }

    public void setScore10(float score10) {
        this.score10 = score10;
    }

    public float getScore11() {
        return score11;
    }

    public void setScore11(float score11) {
        this.score11 = score11;
    }

    public float getScore12() {
        return score12;
    }

    public void setScore12(float score12) {
        this.score12 = score12;
    }

    public float getPageRankScore() {
        return pageRankScore;
    }

    public void setPageRankScore(float pageRankScore) {
        this.pageRankScore = pageRankScore;
    }
}
