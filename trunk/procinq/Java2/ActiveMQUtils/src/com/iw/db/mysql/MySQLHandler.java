package com.iw.db.mysql;

import com.iw.db.ConnectionFactory;
import com.iw.db.beans.ClassificationResult;
import java.sql.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class MySQLHandler {

    private static final Logger log = Logger.getLogger(MySQLHandler.class);
    private static final String TABLE_DDL = "create table corpus%%corpusid(nodeid int , uri varchar(255) , webcacheuri varchar(255), doctitle varchar(255), timestamp timestamp , score1 float,score2 float,score3 float,score4 float,score5 float,score6 float,score7 float,score8 float,score9 float,score10 float,score11 float,score12 float,pagerank float) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    private static final String INSERT_SQL = "INSERT INTO corpus%%corpusid(nodeid, uri, webcacheuri, doctitle, timestamp, score1,score2,score3,score4,score5,score6,score7,score8,score9,score10,score11,score12,pagerank) VALUES (?,?,?,?,now(),?,?,?,?,?,?,?,?,?,?,?,?,?);";
    private static final String tablePrefix = "corpus";

    /**
     * This method allows the creation of a mysql table based in the corpusID
     * <p/>
     * @param corpusID Taxonomy ID to create the DB.
     */
    public static boolean createTableByCorpusID(String corpusID) {
        log.debug("createTableByCorpusID(String)");
        boolean wasSuccessful = false;
        Connection conn = null;
        Statement stmt = null;
        try {
            String tableDDL = TABLE_DDL.replace("%%corpusid", corpusID);
            conn = ConnectionFactory.createConnection(ConnectionFactory.MYSQL);
            stmt = conn.createStatement();
            stmt.executeUpdate(tableDDL);
            wasSuccessful = true;
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
        } catch (Exception exception) {
            log.error("Error in createTableByCorpusID(String).", exception);
            wasSuccessful = false;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException exception) {
                log.error("Error in createTableByCorpusID(String) {Closing Resources}", exception);
            }
        }
        return wasSuccessful;
    }

    /**
     * Method that allows the user to check if the table for this corpusID already Exists
     * <p/>
     * @param corpusID String containing the corpusID (Taxonomy ID)
     * @return
     */
    public static boolean tableExists(String corpusID) {
        log.debug("tableExists(String)");
        Connection conn = null;
        ResultSet rs = null;
        String tableName = null;
        boolean tableExists = false;
        try {
            conn = ConnectionFactory.createConnection(ConnectionFactory.MYSQL);
            rs = getTables(conn, new String[]{"TABLE"});
            while (rs.next()) {
                tableName = rs.getString("TABLE_NAME");
                if (tableName.equalsIgnoreCase(tablePrefix + corpusID)) {
                    tableExists = true;
                }
            }
            rs.close();
            rs = null;
            conn.close();
            conn = null;
        } catch (Exception e) {
            log.error("Error in tableExists(String)", e);
            tableExists = false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException exception) {
                log.error("Error in tableExists(String) {Closing Resources}", exception);
            }
        }
        return tableExists;
    }

    public static ResultSet getTables(Connection conn, String[] types) {
        log.debug("getTables(Connection,String[])");
        DatabaseMetaData metadata;
        ResultSet rs = null;
        try {
            metadata = conn.getMetaData();
            rs = metadata.getTables(null, null, null, types);
        } catch (Exception e) {
            log.error("Error in getTables(Connection,String[])", e);
        }
        return rs;
    }

    public static boolean insertClassificationResult(ClassificationResult clResult, String sCorpusID) {
        log.debug("insertClassificationResult(ClassificationResult)");
        boolean wasSuccessful = false;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            String insertSQL = INSERT_SQL.replace("%%corpusid", sCorpusID);
            conn = ConnectionFactory.createConnection(ConnectionFactory.MYSQL);
            stmt = conn.prepareStatement(insertSQL);
            stmt.setInt(1, clResult.getNodeID());
            stmt.setString(2, clResult.getRemoteURI());
            stmt.setString(3, clResult.getLocalURI());
            stmt.setString(4, clResult.getDocTitle());
            stmt.setFloat(5, clResult.getScore01());
            stmt.setFloat(6, clResult.getScore02());
            stmt.setFloat(7, clResult.getScore03());
            stmt.setFloat(8, clResult.getScore04());
            stmt.setFloat(9, clResult.getScore05());
            stmt.setFloat(10, clResult.getScore06());
            stmt.setFloat(11, clResult.getScore07());
            stmt.setFloat(12, clResult.getScore08());
            stmt.setFloat(13, clResult.getScore09());
            stmt.setFloat(14, clResult.getScore10());
            stmt.setFloat(15, clResult.getScore11());
            stmt.setFloat(16, clResult.getScore12());
            stmt.setFloat(17, clResult.getPageRankScore());
            stmt.execute();
            wasSuccessful = true;
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
        } catch (Exception exception) {
            log.error("Error in insertClassificationResult(ClassificationResult).", exception);
            wasSuccessful = false;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException exception) {
                log.error("Error in insertClassificationResult(ClassificationResult) {Closing Resources}", exception);
            }
        }
        return wasSuccessful;
    }
}
