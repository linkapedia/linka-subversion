package com.iw.db.dao;

import com.iw.db.dao.interfaces.IMustHaveDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Class to handle the Must have information from db.
 *
 * @author Alex
 */
public class MustHaveDAO implements IMustHaveDAO {

    private static final Logger log = Logger.getLogger(MustHaveDAO.class);
    private Connection conn = null;

    private MustHaveDAO() {
    }

    public MustHaveDAO(Connection conn) {
        this.conn = conn;
    }

    /**
     * Method to return the MustHaves Associated with a Node!
     *
     * @param nodeID Node Identifier
     * @return {@code String} A must have list comma separated of the given node.
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public String getMustHavesByNodeID(Integer nodeID) throws SQLException, Exception {
        log.debug("getMustHavesByNodeID(Integer)");
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder _tmpMustHaveList = new StringBuilder();
        String query;
        try {
            query = "SELECT m.nodeid, m.mustword, m.lang FROM musthave m WHERE m.nodeid = ?";
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, nodeID);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                _tmpMustHaveList.append(rs.getString("mustword"));
                _tmpMustHaveList.append(",");
            }
            rs.close();
            rs = null;
            pstmt.close();
            pstmt = null;
        } catch (Exception ex) {
            log.error("An exception ocurred: ", ex);
            throw ex;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (SQLException ex) {
                log.error("An exception ocurred: ", ex);
            }
        }
        return _tmpMustHaveList.toString();
    }

    /**
     * Method to return all the MustHaves Associated with a Node within the given Corpus!
     *
     * @param corpusID Taxonomy/Corpus Identifier.
     * @return  {@code Map<Integer, MustHave>} A map containing the must haves associated with each node of the given corpus ID. <p><b>Note:</b>The Key of the map is the node ID, the Value is a comma
     * separated list of must haves.</p>
     * @see java.util.Map
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public Map<Integer, String> getMustHavesByCorpusID(Integer corpusID) throws SQLException, Exception {
        log.debug("getMustHavesByCorpusIDAsList(Integer)");
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Map<Integer, String> tmpMustHavesMap = new HashMap<Integer, String>();
        String query;
        String tmpMustHaves;
        int tmpNodeId;
        try {
            query = "SELECT m.nodeid, m.mustword, m.lang FROM node n, musthave m WHERE m.nodeid = n.nodeid AND n.corpusid = ?";
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, corpusID);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                tmpNodeId = rs.getInt("nodeid");
                if (tmpMustHavesMap.containsKey(tmpNodeId)) {
                    tmpMustHaves = tmpMustHavesMap.get(tmpNodeId);
                    tmpMustHaves = tmpMustHaves + "," + rs.getString("mustword");
                    tmpMustHavesMap.put(tmpNodeId, tmpMustHaves);
                } else {
                    tmpMustHavesMap.put(tmpNodeId, rs.getString("mustword"));
                }
            }
            rs.close();
            rs = null;
            pstmt.close();
            pstmt = null;
        } catch (Exception ex) {
            log.error("An exception ocurred: ", ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (SQLException ex) {
                log.error("An exception ocurred closing db resources", ex);
            }
        }
        return tmpMustHavesMap;
    }
}