package com.iw.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 *
 * @author Alex
 */
public class PDFUtils {

    private static final Logger log = Logger.getLogger(PDFUtils.class);

    public static String convertPDFToHTML(String pdfFileName) throws IOException {
        log.debug("convertPDFToHTML(String)");
        PDFParser parser;
        String parsedText = null;
        PDFTextStripper pdfStripper;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        File file = new File(pdfFileName);
        if (!file.isFile()) {
            log.warn("File " + pdfFileName + " does not exist.");
            return null;
        }
        try {
            parser = new PDFParser(new FileInputStream(file));
        } catch (IOException e) {
            log.error("Unable to open PDF Parser. " + e.getMessage());
            return null;
        }
        try {
            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            pdfStripper.setStartPage(1);
            parsedText = pdfStripper.getText(pdDoc);
        } catch (Exception e) {
            log.error("An exception occured in parsing the PDF Document.", e);
            return null;
        } finally {
            try {
                if (pdDoc != null) {
                    pdDoc.close();
                }
                if (cosDoc != null) {
                    cosDoc.close();
                }
            } catch (Exception e) {
                log.error("Error convertPDFToHTML(String).", e);
            }
        }
        return parsedText;
    }

    public static void convertPDFToHTML(File src, File dst) throws IOException {
        log.debug("convertPDFToHTML(File,File)");
        PDFParser parser;
        String parsedText;
        PDFTextStripper pdfStripper;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        Writer output = null;
        if (!src.isFile()) {
            log.warn("File " + src.getName() + " does not exist.");
            return;
        }
        try {
            parser = new PDFParser(new FileInputStream(src));
        } catch (IOException e) {
            log.error("Unable to open PDF Parser. " + e.getMessage());
            return;
        }
        try {
            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            pdfStripper.setStartPage(1);
            parsedText = pdfStripper.getText(pdDoc);

            //Write the parsed contents to the dest file.
            output = new BufferedWriter(new FileWriter(dst));
            output.write(parsedText);

        } catch (Exception e) {
            log.error("An exception occured in parsing the PDF Document.", e);
        } finally {
            try {
                if (pdDoc != null) {
                    pdDoc.close();
                }
                if (cosDoc != null) {
                    cosDoc.close();
                }
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (Exception e) {
                log.error("An exception occured in parsing the PDF Document.", e);
            }
        }
    }

    public static void convertPDFToHTML(File src, String dstPath) throws IOException {
        log.debug("convertPDFToHTML(File, String)");
        PDFParser parser;
        String parsedText;
        PDFTextStripper pdfStripper;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        Writer output = null;
        if (!src.isFile()) {
            log.warn("File " + src.getName() + " does not exist.");
            return;
        }

        try {
            parser = new PDFParser(new FileInputStream(src));
        } catch (IOException e) {
            log.error("Unable to open PDF Parser. " + e.getMessage());
            return;
        }
        try {
            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            pdfStripper.setStartPage(1);
            parsedText = pdfStripper.getText(pdDoc);

            //Write the parsed contents to the dest file.
            output = new BufferedWriter(new FileWriter(new File(dstPath)));
            output.write(parsedText);

        } catch (Exception e) {
            log.error("An exception occured in parsing the PDF Document.", e);
        } finally {
            try {
                if (pdDoc != null) {
                    pdDoc.close();
                }
                if (cosDoc != null) {
                    cosDoc.close();
                }
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (Exception e) {
                log.error("An exception occured in parsing the PDF Document.", e);
            }
        }
    }

    public static String getDocTitleFromPDF(File pdfFile) throws IOException {
        log.debug("getDocTitleFromPDF(File)");
        String docTitle = null;
        PDDocument doc = null;
        try {
            doc = PDDocument.load(pdfFile);
            PDDocumentInformation info = doc.getDocumentInformation();
            docTitle = info.getTitle();
            String pdfFileName = pdfFile.getName();
            if (docTitle == null || "".equals(docTitle)) {
                if (pdfFileName.lastIndexOf("/") > 0) {
                    docTitle = pdfFileName.substring(pdfFileName.lastIndexOf("/"), pdfFileName.length());
                }
            }
        } catch (Exception e) {
            log.error("An exception occured in getting the PDF Document Title.", e);
        } finally {
            if (doc != null) {
                doc.close();
            }
        }
        return docTitle;
    }
}
