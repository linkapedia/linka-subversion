package com.iw.utils;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Alex
 */
public class StringUtils {

    private static final Logger log = Logger.getLogger(StringUtils.class);

    public static String encodeFileName(String fileNameToEncode) {
        log.debug("encodeFileName(String)");
        if (fileNameToEncode != null) {
            fileNameToEncode = StringEscapeUtils.unescapeXml(fileNameToEncode);
            fileNameToEncode = fileNameToEncode.toLowerCase();
            fileNameToEncode = fileNameToEncode.trim();
            fileNameToEncode = fileNameToEncode.replaceAll(" ", "-");
            fileNameToEncode = fileNameToEncode.replaceAll(",", "-");
            fileNameToEncode = fileNameToEncode.replaceAll(":", "-");
            fileNameToEncode = fileNameToEncode.replaceAll("á", "a");
            fileNameToEncode = fileNameToEncode.replaceAll("é", "e");
            fileNameToEncode = fileNameToEncode.replaceAll("í", "i");
            fileNameToEncode = fileNameToEncode.replaceAll("ó", "o");
            fileNameToEncode = fileNameToEncode.replaceAll("ú", "u");
            fileNameToEncode = fileNameToEncode.replaceAll("ñ", "n");
        }
        return fileNameToEncode;
    }

    /**
     * Method to remove 
     * @param words
     * @return 
     */
    public static String[] sanitizeWords(String[] words) {
        log.debug("sanitizeWords(String[])");
        String[] wordsSanitized = new String[words.length];
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            word = word.replaceAll(",", "");
            word = word.replaceAll("\\.", "");
            word = word.replaceAll(";", "");
            wordsSanitized[i] = word;
        }
        return wordsSanitized;
    }
}