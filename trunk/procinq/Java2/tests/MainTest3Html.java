/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Dec 14, 2003
 * Time: 4:14:13 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.iw.metasearch;

import com.indraweb.html.HTMLMemDoc;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.execution.Session;
import com.indraweb.util.UtilNet;
import com.indraweb.network.InputStreamsGen;

import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.ChangedCharSetException;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import java.io.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Vector;

public class MainTest3Html
{


    private static int iCallCounter = 0;
    public static void main (String[] args)
    {
        try
        {
            System.out.println ("start MainTest3Html");
            System.out.println (" getText () [" + getText () + "]");
            System.out.println ("done MainTest3Html");
        } catch ( Exception e )
        {
            e.printStackTrace ();
            e.printStackTrace ();
        }
    }

    public static String getText () throws Exception
    {
        //yes URL url = new URL ("http://www.google.com");
        //no URL url = new URL ("http://www.google.com/search?num=100&hl=en&lr=lang_en&ie=UTF-8&oe=UTF-8&safe=off&q=patrick+mehr+lexington%2C+ma&btnG=Google+Search");
        //no URL url = new URL ("http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=twist&btnG=Google+Search");
        // yahoo yes URL url = new URL ("http://phone.people.yahoo.com/py/psPhoneSearch.py?srch=bas&D=1&FirstName=patrick&LastName=mehr&City=lexington&State=ma&searchFor=Telephone&Search=Search");
        // switch boartd
        //URL url = new URL ("http://www.switchboard.com/bin/cginbr.dll?ID=11640112&ABS=0&FUNC=MORE&TYPE=1008&MEM=1&L=kon&T=lexington&S=MA&QV=02F9A78755914A3FF5CF3203O01F77BE5B23CD43F78303203O03F92C3DB93CD43F1C303203");
/*
        URL url = new URL ("http://www.google.com/search?num=100&hl=en&lr=lang_en"+
         //"&ie=UTF-8&oe=UTF-8&safe=off&q=patrick+mehr+lexington%2C+ma&btnG=Google+Search");
         "&q=patrick+mehr+lexington%2C+ma");
*/
        String sURL = "http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=patrick+mehr+lexington%2C+ma&btnG=Google%20Search";
        //String sURL = "http://www.google.com/search?hl=en&ie=UTF-8&oe=UTF-8&q=patrick+mehr+lexington%2C+ma";
        //String sURL = "http://phone.people.yahoo.com/py/psPhoneSearch.py?srch=bas&D=1&FirstName=patrick&LastName=mehr&City=lexington&State=ma&searchFor=Telephone&Search=Search";
        URL url = new URL (sURL);

/*
        HttpURLConnection myURL = (HttpURLConnection) url.openConnection ();
        myURL.setRequestMethod ("GET");
            myURL.connect ();


        BufferedReader reader = new BufferedReader (new
                InputStreamReader (myURL.getInputStream () , "8859_1"));
*/

       UrlHtmlAttributes urlhrmlAttr = new UrlHtmlAttributes (sURL);

        InputStream is = InputStreamsGen.timedSocketConnect ( url, urlhrmlAttr.getsURL(), 500000);


        //InputStream is = InputStreamsGen.getDirectInputStreamFromURL(urlhrmlAttr, 500000, false);
        BufferedReader reader = new BufferedReader (new
                InputStreamReader (is, "8859_1"));

 //       HttpURLConnection myURL = (HttpURLConnection) url.openConnection ();
/*        myURL.setRequestMethod ("GET");
            myURL.connect ();*/
/*
        BufferedReader reader = new BufferedReader (new
                InputStreamReader (myURL.getInputStream () , "8859_1"));
*/



        StringBuffer buffer = new StringBuffer ();

        int read;

        while (( read = reader.read () ) != -1)
        {
            buffer.append ((char) read);
        }

        //String html = "<html><body>Hello & Bye<BR>test</BR><br> ...</br><BR>Google.co.in offered in: software</BR><BR>line   jjjj</BR><BR> manoj & &lt; test</BR></body></html>";
        String html = buffer.toString ();


        final StringBuffer buf = new StringBuffer (100000);
        InputStreamReader rd = null;
        //File ficle = null;
        HTMLDocument doc = null;
        HTMLEditorKit kit = null;
        StringReader sr = null;
        try
        {
            //file = new File ("testing.html");
// Create an HTML document that appends all text to buf
            iCallCounter = 0;
            doc = new HTMLDocument ()
            {
                public HTMLEditorKit.ParserCallback getReader (int pos)
                {

                    System.out.println ("in getreader");
                    return new HTMLEditorKit.ParserCallback ()
                    {
// This method is whenever text is encountered in the HTML file
                        public void handleText (char[] cArrData , int iPos)
                        {
                            iCallCounter++;
                            String sLine = new String(cArrData );
                            System.out.println("iCallCounter [" + iCallCounter +
                                    "] iPos [" + iPos +
                                    "] sLine [" + sLine + "]" );
                            buf.append (cArrData);
                            buf.append ('\n');
                        }


                        public void flush () throws BadLocationException
                        {
                        }

                        public void handleComment (char[] data , int pos)
                        {
                        }

                        public void handleStartTag (HTML.Tag tag , MutableAttributeSet a , int pos)
                        {
                            System.out.println("iCallCounter [" + iCallCounter +
                                                            "] tag.toString()  [" + tag.toString()
                                                            );
                                                }

                        public void handleEndTag (HTML.Tag t , int pos)
                        {
                        }

                        public void handleSimpleTag (HTML.Tag t , MutableAttributeSet a , int pos)
                        {
                        }

                        public void handleError (String errorMsg , int pos)
                        {
                        }

                        public void handleEndOfLineString (String eol)
                        {
                        }

                    };
                }
            };

// Create a reader on the HTML content

// Parse the HTML

            sr = new StringReader (html);
            kit = new HTMLEditorKit ();
            doc.putProperty ("IgnoreCharsetDirective" , new Boolean (true));


            System.out.println ("before read");
            kit.read (sr , doc , 0);
        } catch ( ChangedCharSetException e1 )
        {

            System.out.println ("Exception " + e1);
            sr.close ();
// reload html
            sr = new StringReader (html);
// remove any loaded stuff from document
            try
            {
                doc.remove (0 , doc.getLength ());
            } catch ( BadLocationException e )
            {
            }
// ignore charset
            doc.putProperty ("IgnoreCharsetDirective" , new Boolean (true));
// retry load
            try
            {
                kit.read (sr , doc , 0);
            } catch ( BadLocationException e2 )
            {


            } catch ( Exception e )
            {
            }


        } catch ( Exception e )
        {
            System.out.println (e);
        }




// Return the text
        return buf.toString ();
    }
}
