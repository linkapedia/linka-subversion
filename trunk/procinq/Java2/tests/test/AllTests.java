package test;

import api.emitxml.EmitGenXML_ErrorInfo;


/**
 * Test cases for all API Classes
 */

public class AllTests
{
	public static void runAllTests ()
		throws Exception
	{
		int iFailCount = 0;
		
/*		try  		{ 	 TestGetCorpusProps.test1(); } 	catch ( Exception e ) { 
			//System.out.println ( EmitGenXML_ErrorInfo.genXML_TestingException ( "unit test 1 fail", e, null ) ); 
			iFailCount++;
		}
		try  		{ 	 TestGetCorpusProps.test2(); } 	catch ( Exception e ) { 
			//System.out.println ( EmitGenXML_ErrorInfo.genXML_TestingException ( "unit test 2 fail", e, null )); 
			iFailCount++;
		}
		try  		{ 	 TestGetCorpusProps.test3(); } 	catch ( Exception e ) { 
			//System.out.println ( EmitGenXML_ErrorInfo.genXML_TestingException ( "unit test 3 fail", e, null)); 
			iFailCount++;
		}

*/		
		
		if ( iFailCount > 0 )
			throw new Exception ("unit tests fail with [" + iFailCount + "] failures"); 
	} 
}
