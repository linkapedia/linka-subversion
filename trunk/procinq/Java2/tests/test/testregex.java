package test;

import java.util.Vector;
import java.util.Hashtable;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.io.*;
import java.text.SimpleDateFormat;
import java.sql.Date;

// This is the repository class for type Filesystem.   This class is assumed to be
//  an NT filesystem.   Please use the UNIXFilesystem class for UNIX based filesystems.

public class testregex
{
    // Private variable htVectors contains an entry for each corpus containing
    //   documents tagged to be reindexed through Verity
    public static Vector vHyperlinks = new Vector ();

    private static Hashtable htVisited = new Hashtable ();
    private static Hashtable htFileLocations = new Hashtable ();
    private static String Key = "";

    //public Hashtable getFileLocations() { return htFileLocations; }

    private static void o (Matcher m, int iLoop)
    {
        int iCount=-1;
        System.out.println(iLoop + ". m.groupCount()[" + (iCount = m.groupCount()) + "]" );
        for ( int i = 0; i < iCount; i++)
        {
            String sText = m.group (i);
            System.out.println(iLoop + ". group [" + i + ". sText [" + sText+ "]" );

        }

    }
    private static void o (String s)
    {
        System.out.println(s);
    }
    private static void o (String sPre, String s)
    {
        System.out.println("pre [" + sPre + "] s [" + s + "]" );

    }

    public static void main (String[] args)
    {

   /*     String s =
                "<a href=\"http://www.SierraTradingPost1.com/\"><img src=\"http://www.SierraTradingPost1.com/Assets/images/imgHomeLogo.gif\" alt=\"Sierra Trading Post\" border=\"0\"></a>" +
                "<a href=\"\"http://www.SierraTradingPost2.com/\"><img src=\"http://www.SierraTradingPost2.com/Assets/images/imgHomeLogo.gif\" alt=\"Sierra Trading Post\" border=\"0\"></a>"
                ;
*/
        String s = "a@1.2,\r\nb@1.2.3,c@1.2.3";
                                       //   href=\"\"http://www.SierraTradingPost2.com/
        //Pattern p = Pattern.compile ("\\s*href\\s*=\\s*\"*?([^>\" ]+)\"*?.*?>{1}?(.*?)(</a>{1}?)" , Pattern.CASE_INSENSITIVE);
        //Pattern p = Pattern.compile ("href=\"*?([^>\" ]+)" , Pattern.CASE_INSENSITIVE);
        Pattern patSplitCRLF = Pattern.compile ("[\n||\r]");
        String[] sArrLines = patSplitCRLF.split(s);
        for ( int i = 0; i < sArrLines.length; i++)
        {
            o(""+i, sArrLines[i]);
        }

        Pattern p = Pattern.compile ("href=\"*?([^>\" ]+)" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher (s);
        boolean bResult = m.find ();

        int iLoop = -1;
        while (bResult)
        {
            iLoop++;
            System.out.println("new match");
            o (m, iLoop);

/*
                    if ((!sURL.toLowerCase().startsWith("http")) && (!sURL.startsWith("/"))) {
                        if ((getDirectory(rootURL)).endsWith("/")) {
                            sURL = getDirectory(rootURL)+sURL;
                        } else { sURL = getDirectory(rootURL)+"/"+sURL; }
                    }
                    if (sURL.startsWith("/")) {
                        if ((getBase(rootURL)).endsWith("/")) {
                            sURL = getBaseWithoutSlash(rootURL)+sURL;
                        } else { sURL = getBase(rootURL)+sURL; }
                    }

                    // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
                    int iPound = sURL.indexOf("#");
                    if (iPound != -1) { sURL = sURL.substring(0, iPound); }

                    try {
                        URI u = new URI(sURL);
                        u = u.normalize();
                        sURL = u.toString();
                    } catch (Exception e) {
                        System.out.println("Could not normalize URL: "+sURL);
                    }
                    // for a link to be added it must pass the following qualifications:
                    //  a) it must not be an image link
                    //  b) it must contain the URL keyword (if applicable) and
                    //  c) it must not have been already visited
                    //  d) remove all null characters (ascii: 0) from the string
                    sURL = sURL.replaceAll(new Character((char)0).toString(), "");

                    if (hasKey() && (sURL.indexOf(Key) != -1)) {
                        if (!alreadyVisited(sURL) && (!isImage(sURL))) {
                            vHyperlinks.add(sURL); markVisited(sURL); }
                    }
*/
            bResult = m.find ();
        }

    }


    // given a URL, retrieve the HTML and return it
    private String getData (String sURL)
    {
        long lStart = System.currentTimeMillis ();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer ("");

        try
        {
            URL myURL = new URL (sURL);
            httpCon = null;
            //httpCon = (HttpURLConnection) myURL.openConnection();
            //httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");

            if (httpCon.getResponseCode () != HttpURLConnection.HTTP_OK)
            {
                throw new Exception ("Http error: " + httpCon.getResponseCode () + " " + httpCon.getResponseMessage ());
            }

            InputStream is = httpCon.getInputStream ();
            DataInputStream dis = new DataInputStream (new BufferedInputStream (is));

            //System.out.println("Get: "+URL+" ("+httpCon.getContentType()+")");

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "html";
        } catch ( Exception e )
        {
            long lEnd = System.currentTimeMillis () - lStart;
            System.out.println ("HTTP/File Write failure after " + lEnd + " ms. : " + e.toString ());
            e.printStackTrace (System.out);
            return "";
        } finally
        {
            httpCon.disconnect ();
        }
        return null;

    }

    // extract all HTML hyperlinks from a given document and return in a vector
    public static void extractFrameTextLinks (String page , String rootURL)
    {
        int lastPosition = 0;           // position of "http:" substring in page

        Pattern p = Pattern.compile ("\\s*<frame (.*?)src\\s*=\\s*\"(.*?)\"" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher (page);
        boolean bResult = m.find ();

        while (bResult)
        {
            String sURL = m.group (2);
            if (( !sURL.toLowerCase ().startsWith ("http") ) && ( !sURL.startsWith ("/") ))
            {
                if (( getDirectory (rootURL) ).endsWith ("/"))
                {
                    sURL = getDirectory (rootURL) + sURL;
                }
                else
                {
                    sURL = getDirectory (rootURL) + "/" + sURL;
                }
            }
            if (sURL.startsWith ("/"))
            {
                if (( getBase (rootURL) ).endsWith ("/"))
                {
                    sURL = getBaseWithoutSlash (rootURL) + sURL;
                }
                else
                {
                    sURL = getBase (rootURL) + sURL;
                }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf ("#");
            if (iPound != -1)
            {
                sURL = sURL.substring (0 , iPound);
            }

            try
            {
                URI u = new URI (sURL);
                u = u.normalize ();
                sURL = u.toString ();
            } catch ( Exception e )
            {
                System.out.println ("Could not normalize URL: " + sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll (new Character ((char) 0).toString () , "");

            if (hasKey () && ( sURL.indexOf (getKey ()) != -1 ))
            {
                if (!alreadyVisited (sURL) && ( !isImage (sURL) ))
                {
                    System.out.println ("Found in frame: " + sURL);
                    vHyperlinks.add (sURL);
                    markVisited (sURL);
                }
            }
            bResult = m.find ();
        }
    }


    // extract all HTML hyperlinks from a given document and return in a vector
    public static void extractHyperTextLinks (String page , String rootURL)
    {
        int lastPosition = 0;           // position of "http:" substring in page

        Pattern p = Pattern.compile ("\\s*href\\s*=\\s*\"*?([^>\" ]+)\"*?.*?>{1}?(.*?)(</a>{1}?)" , Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher (page);
        boolean bResult = m.find ();

        while (bResult)
        {
            String sURL = m.group (1);

            if (( !sURL.toLowerCase ().startsWith ("http") ) && ( !sURL.startsWith ("/") ))
            {
                if (( getDirectory (rootURL) ).endsWith ("/"))
                {
                    sURL = getDirectory (rootURL) + sURL;
                }
                else
                {
                    sURL = getDirectory (rootURL) + "/" + sURL;
                }
            }
            if (sURL.startsWith ("/"))
            {
                if (( getBase (rootURL) ).endsWith ("/"))
                {
                    sURL = getBaseWithoutSlash (rootURL) + sURL;
                }
                else
                {
                    sURL = getBase (rootURL) + sURL;
                }
            }

            // if the link has a # (pound) in it, it's just an anchor.  truncate everything after and including the pound
            int iPound = sURL.indexOf ("#");
            if (iPound != -1)
            {
                sURL = sURL.substring (0 , iPound);
            }

            try
            {
                URI u = new URI (sURL);
                u = u.normalize ();
                sURL = u.toString ();
            } catch ( Exception e )
            {
                System.out.println ("Could not normalize URL: " + sURL);
            }
            // for a link to be added it must pass the following qualifications:
            //  a) it must not be an image link
            //  b) it must contain the URL keyword (if applicable) and
            //  c) it must not have been already visited
            //  d) remove all null characters (ascii: 0) from the string
            sURL = sURL.replaceAll (new Character ((char) 0).toString () , "");

            if (hasKey () && ( sURL.indexOf (Key) != -1 ))
            {
                if (!alreadyVisited (sURL) && ( !isImage (sURL) ))
                {
                    vHyperlinks.add (sURL);
                    markVisited (sURL);
                }
            }
            bResult = m.find ();
        }
    }

    // return the URL directory that this file was found in on the web server
    public static String getDirectory (String URL)
    {
        while (URL.endsWith ("/"))
        {
            return URL;
        }
        int index = URL.lastIndexOf ('/');

        return URL.substring (0 , index);
    }

    // return the base URL (host)
    public static String getBase (String URL)
    {
        int index = URL.indexOf ('/' , 7);

        return URL.substring (0 , index);
    }

    public static String getBaseWithoutSlash (String URL)
    {
        int index = URL.indexOf ('/' , 7);

        return URL.substring (0 , index - 1);
    }

    // mark or check if page has already been spidered
    public static void markVisited (String URL)
    {
        htVisited.put (URL.toLowerCase () , "1");
    }

    public static boolean alreadyVisited (String URL)
    {
        if (htVisited.containsKey (URL.toLowerCase ()))
        {
            return true;
        }
        return false;
    }

    // setKey sets the keyword field.  This allows us to ensure matches are only found from within the directory of the starting point.
    public static String setKey (String sKey)
    {
        if (!sKey.endsWith ("/"))
        {
            sKey = getDirectory (sKey) + "/";
        }
        if (sKey.startsWith ("http://"))
        {
            int index = sKey.indexOf ('/' , 7);
            if (index == -1)
            {
                return "";
            }
            sKey = sKey.substring (index);
        }
        Key = sKey;
        return Key;
    }

    // these functions determine whether or not to check the URL for a specific key directory
    public static String getKey ()
    {
        return Key;
    }

    public static boolean hasKey ()
    {
        if (!Key.equals (""))
        {
            return true;
        }
        return false;
    }

    // look at the document extension, make sure it's not an image file
    public static boolean isImage (String URL)
    {
        if (URL.toLowerCase ().endsWith (".jpg"))
        {
            return true;
        }
        if (URL.toLowerCase ().endsWith (".gif"))
        {
            return true;
        }
        if (URL.toLowerCase ().endsWith (".jpeg"))
        {
            return true;
        }
        if (URL.toLowerCase ().endsWith (".bmp"))
        {
            return true;
        }
        if (URL.toLowerCase ().endsWith (".wav"))
        {
            return true;
        }
        if (URL.toLowerCase ().endsWith (".css"))
        {
            return true;
        }
        if (URL.toLowerCase ().endsWith (".xml"))
        {
            return true;
        }
        if (URL.indexOf ("javascript") != -1)
        {
            return true;
        }
        if (URL.indexOf ("mailto") != -1)
        {
            return true;
        }
        if (URL.indexOf ("..") != -1)
        {
            System.out.println ("Found URL: " + URL + " has relative paths, discarding..");
            return true;
        }
        return false;
    }

    // Specific web crawl routine to retrieve new objects on the web site that have
    //  changed since DateLastCapture.
    public void GetNewObjects (String sURL , String sDateLastCapture , boolean bRecurse)
    {
        System.out.println ("Looking for classification candidates in " + sURL);

        // get all the documents already run from the database, they should be
        // run first to ensure that they have not changed.
        Hashtable htOldDocuments = null;//hbk

        // initialize directory information
        vHyperlinks.add (sURL);
        setKey (sURL);

        while (vHyperlinks.size () > 0)
        {
            String sURLelement = (String) vHyperlinks.firstElement ();
            boolean bClassifyIt = true;

            // if the URL is an existing document (already in our database), do not retrieve it
            // unless it has changed since our last run
/*
                if (htOldDocuments.containsKey(sURLelement)) {
                    String sDate = doHeadRequest(sURLelement);  // YYYYMMDDHHMISS
                    String sLast = (String) htOldDocuments.get(sURLelement);  // YYYYMMDDHHMISS

                    if (sDate.compareTo(sLast) < 0) { bClassifyIt = false; }
                }
*/

            String sData = getData (sURLelement);
            if (bRecurse) extractHyperTextLinks (sData.replaceAll ("\n" , " ").replaceAll ("\r" , " ") , sURLelement);
/*
                if (bClassifyIt) {
                    iCrawlCounter++;
                    synchronized (this) { System.out.println("Added new document: "+sURLelement);
                        AddNewDocument(sURLelement); }
                }
*/

            vHyperlinks.remove (sURLelement);
            //try { Thread.sleep(250); } catch (Exception e) { System.out.println("Could not pause."); } // let's try not to annoy the system administrators :)
        }

        System.out.println (htFileLocations.size () + " new web pages found on web site " + sURL);
        System.out.println ("");
        System.out.flush ();

        //try { CloseConnection(); } catch (Exception e) {}
    }

    // These three functions are repository specific and must be overridden

    // HEAD request the URL and return the last modified date, if applicable, style: 1976-01-05 13:50:08
/*
        public String doHeadRequest (String URL) {
            HttpURLConnection httpCon = null;

            try {
                URL myURL = new URL(URL);
                httpCon = myURL.openConnection();
                httpCon.setRequestMethod("HEAD");

                if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new Exception("Http error: " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
                }

                long changed = httpCon.getLastModified();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmss");
                return sdf.format(new Date(changed));

            } catch (Exception e) {
                System.out.println("HTTP HEAD request failure: "+e.toString());
                return "";
            }
            finally { httpCon.disconnect(); }
        }

*/

}

