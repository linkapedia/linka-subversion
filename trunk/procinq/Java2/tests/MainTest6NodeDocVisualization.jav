package com.iw.metasearch;

import java.sql.*;
import java.io.*;
import java.util.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


import com.indraweb.util.*;
import com.indraweb.encyclopedia.*;
import com.iw.classification.*;
import com.iw.scoring.NodeForScore;
import com.indraweb.execution.*;
import com.indraweb.database.JDBCIndra;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.ir.clsStemAndStopList;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.webelements.IndraURL;
import com.indraweb.utils.datasets.DataTables;
import Server.VectorTree;
import com.iw.system.HashTree;
import HTML.HTMLDocument;

import org.apache.log4j.*;
import org.dom4j.io.XMLWriter;
import org.dom4j.io.SAXReader;
import org.dom4j.Document;
import com.indraweb.ir.texttranslate.translators.TextTranslator_BabelFish;
import com.indraweb.ir.texttranslate.Data_TextTranslate;
import api.APIProps;

public class MainTest6NodeDocVisualization
{

    public static void main (String[] args)
    {

/*

        Object[][] oArrArrTableData = new Object[10][5];
        int i3 = oArrArrTableData.length;
        int j3 = oArrArrTableData[0].length;
        System.out.println("oArrArrTableData.length:" + i3 + " oArrArrTableData[0].length :" + j3);
        int i = oArrArrTableData.length;
        int j = oArrArrTableData[0].length;
          Object[] oArr = new Object[5];
        for ( int iRow = 0; iRow < 10; iRow++)
        {
            for ( int iCol = 0; iCol < 10; iCol++)
            {
                System.out.println("setting row [" + iRow + "] col [" + iCol + "] to [" + iRow+":"+iCol + "]" );
                oArrArrTableData[iRow][iCol] = iRow+":"+iCol;
            }
        }
        oArr[0] = "0";
        oArr[1] = "1";
        oArr[2] = "2";
        oArr[3] = "3";
        oArr[4] = "4";
        for ( int i2 = 0; i2 < oArrArrTableData.length; i2++)
        {
            oArrArrTableData[i2] =oArr;
        }

        i3 = oArrArrTableData.length;
        j3 = oArrArrTableData[0].length;
        System.out.println("oArrArrTableData.length:" + i3 + " oArrArrTableData[0].length :" + j3);
        System.out.println("oArrArrTableData.length:" + i3 + " oArrArrTableData[0].length :" + j3);
*/

/*  performance test ht strings vs ints
        Hashtable htTest = new Hashtable();
        long ltstart = System.currentTimeMillis();
        for ( int i = 0; i < 100000; i++)                                ec
        {
            //htTest.put (new Integer(i),  new Integer(i));
            htTest.put (""+i,  ""+i);
        }
        for ( int i = 0; i < 100000; i++)
        {
            Object o = htTest.get (""+i);
        }

        System.out.print("elapsed [" + (System.currentTimeMillis()- ltstart + "]" ));
        System.out.print("elapsed [" + (System.currentTimeMillis()- ltstart + "]" ));
        System.exit(1);

*/




        System.out.println ("STATE 1");
        System.out.println ("in new main");
        String sOSHost = (String) java.lang.System.getProperties ().get ("os.name");
        if (sOSHost.toLowerCase ().indexOf ("windows") >= 0)
        {
            Session.sIndraHome = "C:/Program Files/ITS";
        }
        else
        {
            Session.sIndraHome = "/tmp/IndraHome";
        }

        Connection dbc = null;
        try
        {
            if (false)     // 2003 07 30
            {
                try
                {
                    String sWhichDB = "API";
                    dbc = localInitSession (sWhichDB);
                    DocForScore docForScore = new DocForScore (
                            "file:///C:/Program Files/ITS/CorpusSource/classificationtest/43kim0.woti-doc.htm" ,
                            "file:///C:/Program Files/ITS/CorpusSource/classificationtest/43kim0.woti-doc.htm" ,
                            true ,
                            false ,
                            "hktest" ,
                            "no summary" ,
                            true ,
                            null ,
                            true ,
                            false
                    );
                } catch ( Exception e )
                {
                    e.printStackTrace ();
                }
                System.exit (0);
            }
            if (false) // classify one or a few 2002 11 17  // fn: batch false
            {
                String sWhichDB = "SVR";
                dbc = localInitSession (sWhichDB);
                int iLoop = 0;
                while (iLoop < 1)
                {
                    System.out.println ("\r\n\r\n\r\n\r\n\r\n********** MAINTEST testTSMetasearchNode iLoop  [" + iLoop + "]");
                    //com.indraweb.util.UtilFile.addLineToFile("c:/t.t", new java.util.Date () + " testTSMetasearchNode iLoop  [" + iLoop + "]\r\n");
                    System.out.flush ();
                    testTSMetasearchNode (dbc); // 2003 01 25
                    iLoop++;
                }
                System.exit (0);
            }

            if (false) // test property override 2003
            {
                APIProps apiprops_override = null;
                System.out.println ("found /temp/PropertyOverride.txt");
                apiprops_override = new api.APIProps ("/temp/PropertyOverride.txt");
                System.out.println ("found2 /temp/PropertyOverride.txt");
                Enumeration e = apiprops_override.keys ();
                while (e.hasMoreElements ())
                {
                    String sKey = (String) e.nextElement ();
                    String sValue = (String) apiprops_override.get (sKey);
                    System.out.println ("setting [" + sKey + "] to [" + sValue + "]");
                }
                System.exit (0);
            }
/*
            //String sWhichDB = "SVR";
            String sWhichDB = "API";
            System.out.println("STATE 2");
            dbc = localInitSession ( sWhichDB );
            System.out.println("STATE 3");
            if ( true )  // hbk control 2003 06 08 remove for prod.
                System.out.println("STATE 4");
                patchWebCrawlCache( dbc );
*/
            if (false)
            {  // hbk control 2003 06 08 remove for prod. {
                System.out.println ("STATE 4");
                loadWebCrawlCache (dbc);
                System.out.println ("STATE 5");
            }
            if (false)
            { // 2003 09 04 TSGetROCData
                dbc = localInitSession ("API");
                Session.stopList = new clsStemAndStopList (Session.cfg.getProp ("IndraHome") + Session.cfg.getProp ("StopListFile"));
                testTSGetROCData (dbc);
                System.exit (0);
            }
            if (false)
            { // classify one or a few 2003 12 01// fn: batch false
                dbc = localInitSession ("API");
                Session.stopList = new clsStemAndStopList (Session.cfg.getProp ("IndraHome") + Session.cfg.getProp ("StopListFile"));
                testTSCLassify2 (dbc);
                //testTSCLassify(dbc);
                System.exit (0);
            }
/*
            if (false) // 2003 12 02
            { // test DOM4j
                //OutputStream out  = System.out;
            FileOutputStream fos = new FileOutputStream("c:/temp/temp.txt");
           com.iw.ui.explain.serializetest.Dom4jTest.serializetoXML(fos , "dummyhk");
            FileInputStream fis = new FileInputStream ("c:/temp/temp.txt");
           org.dom4j.Document doc = com.iw.ui.explain.serializetest.Dom4jTest.deserializeFromXMLin(fis);
           com.iw.ui.explain.serializetest.Dom4JTest3Iterator.iterateTest(doc );

                    System.exit (0);
            }
*/
/*
            if (false) // 2003 12 02
            { // test DOM4j tables
                Dom4jTestTableXML2Way.test ();
                System.exit (0);
            }
            if (false ) // 2003 12 03
            { // test DOM4j tables
                TestXMLEncoder.test ();
                System.exit (0);
            }
*/
            if (false)
            { // classify one or a few 2003 12 06// fn: batch false
                dbc = localInitSession ("API");
                Session.stopList = new clsStemAndStopList (Session.cfg.getProp ("IndraHome") + Session.cfg.getProp ("StopListFile"));
                testTSCLassifyGUIExplainStyle (dbc);
                //testTSCLassify(dbc);
                System.exit (0);
            }
            if (true)
            { // classify one or a few 2003 12 06// fn: batch false
                testDom4JHtmlRead();
                System.exit (0);
            }

            if (false)
            { // test oracle language multiligual 2003 10 21 //
                dbc = localInitSession ("API");
                Session.stopList = new clsStemAndStopList (Session.cfg.getProp ("IndraHome") + Session.cfg.getProp ("StopListFile"));
                // test if oracle can store funny char's  : testOracleMultilingual (dbc);
                if ( false) testCorpusSigDataMigrate (dbc);
                if ( true ) testLanguageTranslate (dbc);
                System.exit (0);
            }

            if (false ) // test oracle doc conversion 2003 11 19
            {
                dbc = localInitSession ("API");
                Session.stopList = new clsStemAndStopList (Session.cfg.getProp ("IndraHome") + Session.cfg.getProp ("StopListFile"));
                testURLToOracleStreamConvert(dbc);
                //testTSCLassify(dbc);
                System.exit (0);
            }


            if (false )   // 2003 10 20 query time test
            { // classify one or a few 2003 10 11 // fn: batch false
                api.Log.Log ("entering query time test" );
                dbc = localInitSession ("API");

                String sClearCache = "ALTER SYSTEM FLUSH SHARED_POOL";
                Statement stmtCAcheClear = dbc.createStatement();
                stmtCAcheClear.executeUpdate (sClearCache);
                dbc.commit();
                stmtCAcheClear.close();
                api.Log.Log ("completed cache flush" );

                String sSQLnew = "select distinct DOCUMENT.DOCUMENTID, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, DOCUMENT.DOCURL, DOCUMENT.DOCUMENTSUMMARY, DOCUMENT.DATELASTFOUND, DOCUMENT.VISIBLE, DOCUMENT.SHORT, DOCUMENT.AUTHOR, DOCUMENT.SOURCE, DOCUMENT.CITATION, DOCUMENT.SORTDATE, DOCUMENT.SHOWDATE, DOCUMENT.SUBMITTEDBY, DOCUMENT.REVIEWDATE, DOCUMENT.REVIEWEDBY, DOCUMENT.REVIEWSTATUS, DOCUMENT.COMMENTS, DOCUMENT.UPDATEDDATE, DOCUMENT.UPDATEDBY, DOCUMENT.FULLTEXT, contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') from DOCUMENT, DOCUMENTSECURITY where DOCUMENT.DOCUMENTID = DOCUMENTSECURITY.DOCUMENTID and DOCUMENTSECURITY.SECURITYID in( 0 ) and ( contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') > 0) order by 22";
                long lTime2 = testQuerySpeed (sSQLnew, 150, dbc, "new way");

                stmtCAcheClear = dbc.createStatement();
                stmtCAcheClear.executeUpdate (sClearCache);
                dbc.commit();
                stmtCAcheClear.close();
                api.Log.Log ("completed cache flush" );

                String sSQLOld = "select distinct * from ( select DOCUMENT.DOCUMENTID, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, DOCUMENT.DOCURL, DOCUMENT.DOCUMENTSUMMARY, DOCUMENT.DATELASTFOUND, DOCUMENT.VISIBLE, DOCUMENT.SHORT, DOCUMENT.AUTHOR, DOCUMENT.SOURCE, DOCUMENT.CITATION, DOCUMENT.SORTDATE, DOCUMENT.SHOWDATE, DOCUMENT.SUBMITTEDBY, DOCUMENT.REVIEWDATE, DOCUMENT.REVIEWEDBY, DOCUMENT.REVIEWSTATUS, DOCUMENT.COMMENTS, DOCUMENT.UPDATEDDATE, DOCUMENT.UPDATEDBY, DOCUMENT.FULLTEXT, contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') from DOCUMENT, DOCUMENTSECURITY where DOCUMENT.DOCUMENTID = DOCUMENTSECURITY.DOCUMENTID and DOCUMENTSECURITY.SECURITYID in( 0 ) and ( contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') > 0)  order by contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)')    ) ";
                long lTime1 = testQuerySpeed (sSQLOld, 150, dbc, "old way");
                api.Log.Log ("query time test result : lTime1 [" + lTime1 + " lTime2 [" + lTime2 + "]" );
                //testTSCLassify(dbc);
                System.exit (0);
            }

            if (false)// 2003 10 11
            {
                testDocOpen ();
                System.exit (0);
            }

/*            if ( false ) { // TSapplyROC 2003 09 05
                dbc = localInitSession ( "API" );
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                testApplyROC(dbc);
                System.exit(0);
            }*/



            if (false)  // hbk control 2002 12 25 remove for prod.
                memNewIntOrStringSameOnesTest ();
            if (false)  // hbk control 2002 12 25 remove for prod.
                memIArrTest ();
            if (false)  // hbk control 2002 12 25 remove for prod.
                memRandomStringsArrTest ();
            if (false) // corpus file copies with word scramblig
                scrambleCorpusWordsAndNodeIDs ();
            if (false)
                testDataStructLoader (dbc);
            if (false)
                classifySpeedLoopTestCompareModes (dbc);
            // 2003 03 26
            if (false) // classify one or a few 2002 11 17  // fn: batch false
                testTSThesaurusSynch (dbc);
            if (false) // classify one or a few 2002 11 17  // fn: batch false
                testTSRefreshNode2 (dbc); // 2003 01 25
            // 2003 03 22
            if (true)
            {  // 2003 07 28 // explain ?
                dbc = localInitSession ("API");
                Session.stopList = new clsStemAndStopList (Session.cfg.getProp ("IndraHome") + Session.cfg.getProp ("StopListFile"));
                testExplain (dbc);
            }
            if (false)   // uses post execute to tsclassify
                testExplainMorePureAsAPICall (dbc);
            if (false)
            {
                dbc = localInitSession ("API");
                com.indraweb.util.Log.log ("engine test logger");
                testLogger ();
                System.exit (1);
            }

            if (false) //
            {
                // tsinstall corpus	BIO	test
                // executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TS`orpus&CorpusFolder=C:\\temp\\temp\\biologicalandchemical\\001\\002\\002&CorpusName=test hkon&CorpusActive=-1&skey=1",	dbc	);
                dbc = localInitSession ("API");
                executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus" +
                        //"&CorpusFolder=C:/Program Files/ITS/CorpusSource/corpusingest/001&CorpusName=test hkon 5&CorpusActive=-1&skey=1" , dbc);
                        "&CorpusFolder=E:/applications/temp/testcnf/IndraWebCorpusLoad/Wuhan/001&CorpusName=test hkon 202&CorpusActive=-1&skey=1" , dbc);
                //"&CorpusFolder=C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/001/003&CorpusName=test hkon new&CorpusActive=-1&skey=1",	dbc	);
                //"&CorpusFolder=C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/001&CorpusName=test hkon new&CorpusActive=-1&skey=1",	dbc	);
                //                C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/001
                // C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/00
                System.exit(0);
            }


            System.exit (0);

        } catch ( Throwable t )
        {
            api.Log.Log ("maintest throwable [" + t.getMessage () + "]");
            t.printStackTrace();
            api.Log.Log ("maintest throwable [" + t.getMessage () + "]");
        }
        try
        {

            /*
                if ( false ) // ftp test 2002 11 02   // fn:
                {
                    try {
                        com.indraweb.classification.BatchClassFileMgr.testFtpConnection2 ("shell.theworld.com", "hkon2", "xxxx"); // prep.ai.mit.edu
                    }
                    catch ( Exception e ) {
                        com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
                    }
                    System.exit(1);
                }
            */

            // SET UP ENV SINCE NOT COMING THRU JRUN

//			com.indraweb.util.Log.logClear("testtest\r\n");
            //com.indraweb.util.UtilFile.addLineToFile("c:/temp/temp.txt", new java.util.Date()	+ ": done db init  \r\n");
            // create corpus
            //executeAPI (
            //	"http://207.103.213.118:8100/servlet/ts?fn=tscorpus.TSCreateCorpus&ClientID=0&"	+
            //	"CorpusName=(henry and mike)%20Kon%20Test%20Corpus%20Name&CorpusDesc=&CorpusPath=pathunknown&RocF1=1&RocF2=2&RocF3=3&RocC1=1&RocC2=2&RocC3=3");
            // edit	corpus
            //executeAPI ("http://207.103.213.118:8100/servlet/ts?fn=tsdocument.TSGetNodeDocument&CorpusID=1&NodeID=90411");
            //				"http://207.103.213.118:8100/servlet/ts?fn=tscorpus.TSEditCorpusProps&ClientID=0&CorpusID=3&" +
            //				"CorpusName=UpdatedCorpName&CorpusPath=updatedCorpusPath&RocF1=10");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&CorpusID=2");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSGetCorpusProps&CorpusID=1");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&UserID=");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/water.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/worldbook/a1_html/000200.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/worldbook/a1_html/000200.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.gulflink.osd.mil/cement_factory_ii/cement_factory_ii_s03.htm#C.%20Cement%20Factory%20Location");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.gulflink.osd.mil/cement_factory_ii/cement_factory_ii_s03.htm#C.%20Cement%20Factory%20Location");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.gulflink.osd.mil/cement_factory_ii/cement_factory_ii_s03.htm#C.%20Cement%20Factory%20Location");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/iraqoil.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&URL=/corpussource/classificationtest/DepletedUranium.html");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&URL=/classifyfiles/westNileVirus.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&URL=/classifyfiles/westNileVirus.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");

            //			executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&url=/corpussource/classificationtest/iraqoil.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&url=/corpussource/classificationtest/iraqoil.html");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/test2.html&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsnode.TSNodeTitleSearch&Keywords=TIME");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://www.zyvex.com/nano/");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsnode.TSAddNode&NodeID=200021&ParentID=90192&CorpusID=1&ClientID=1&NodeTitle=hkon test node&WordsAndFreqs=word1:1;word2:2;word3:3&NodeSize=40&NodeIndexInParent=20");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsnode.TSAddNode&ParentID=90192&CorpusID=1&NodeTitle=hkon test node&WordsAndFreqs=word1:1;word2:2;word3:3&NodeSize=40&NodeIndexInParent=20");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/iraqoil.html&userID=hkontestpageb&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&maxnumoutput=20&post=false&url=http://www.rpi.org/news/press_1998_0721b.htm");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.lassp.cornell.edu/GraduateAdmissions/greene/greene.html&maxnumoutput=5&post=false");


            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/3799.html&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/3799.html&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/3799.html&post=true");


            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage");

            // with	name from ARES
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=\\\\66.134.131.38\\public\\corpussource&CorpusName=HKTEST&CorpusActive=1");
            // without name
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=C:\\Documents	and	Settings\\Indraweb\\IndraHome\\corpussource\\merckManualCrawl\\corpusRoot\\001");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome/corpussource/merckManualCrawl/corpusRoot/001/001&CorpusName=HKTEST");

            //			executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=C:/Documents and Settings\\Indraweb/IndraHome/corpussource/merckManualCrawl/corpusRoot/001");

            //executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome/corpussource/TechTarget/001&CorpusName=Tech Target&CorpusActive=-1&whichdb=SVR&publisherid=5");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome/corpussource/TechTarget/001&CorpusName=Tech Target&CorpusActive=-1");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage");
            // 9728.html executeAPI	("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/9728.html");  // hbk 2002
            // hbk control - which url for debug classification
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://www.zyvex.com/nano/");  // hbk 2002
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://www.worldviewsoftware.com/ea.html");
            // executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/water.html&userID=hkontestpage");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusActive=1&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome\\corpussource\\Child Psychology\\Child Psychology\\001");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hbkTest&ExplainScores=true&ExplainScoresShowDocWords=true&NodeToScoreExplain=95634");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=95634&explainscoresshowdocwords=false");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false");
            //	executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings/Indraweb/IndraHome\\corpussource/merckManualCrawl/corpusRoot/001/001&corpusname=HKTEST5&CorpusActive=-1");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=2");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");


            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn="+
            //			"tsclassify.TSClassifyDoc&URL=http://66.134.131.38/hkon/DepletedUranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=true&numscorestoexplain=1");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/for(henry and mike).htm");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/for(henry and mike).htm&explainscores=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/for(henry and mike).htm&explainscores=true&nodetoscoreexplain=1129429");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/hoeytest.html&explainscores=true&explainscoresshowdocwords=true&numscorestoexplain=5");
            // hk smallest corpus
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome_default\\corpussource\\chem smallest hk\\001&CorpusName=chem war small	hbk	test&CorpusActive=1", "C:/Documents	and	Settings/IndraWeb/IndraHome_default");

            // tsinstall corpus	merck
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome_default/corpussource/merckManualCrawl/corpusRoot/001&CorpusName=merck	hbk	test&CorpusActive=1&skey=1", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // 2002	05 16
            // tsinstall corpus	test warfare
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome_default\\corpussource\\chemical warfare HKTEST\\001&CorpusName=chem war hbk 7&CorpusActive=-1&skey=1",	"C:/Documents and Settings/IndraWeb/IndraHome_default");

            // 2002	05 16
            // tsclassify uranium, post	= false
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium.html&post=true&sorpora=1&SKEY=-220364972",	"C:/Documents and Settings/IndraWeb/IndraHome_default");

            // tsclassify uranium, post	= true,	summary	and	title
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium2.html&post=true&SKEY=-220364972&doctitle=test hbk test	doc	title&docsummary=test hbk test doc summary", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // tsclassify uranium, post	= true,	no summary and title
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium2.html&post=true&SKEY=-220364972&corpora=1", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // classify	uranium, post =	false
            // D:\IndraWeb\SourceSafe_IndrawebRoot\Java2\TS_API_project
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium.html&post=true&SKEY=-151158519", "C:/Documents	and	Settings/IndraWeb/IndraHome_default");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/t.html&post=false&SKEY=1892478463", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // 2002	06 26
            // tsinstall corpus	software_engineering2
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\indrahome\\corpussource\\software_engineering_3\\001&CorpusName=test	hkon&CorpusActive=-1&skey=1", dbc );


            // 2002	07 04
            // tsinstall corpus	test warfare
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome\\corpussource\\chemical warfare HKTEST\\001&CorpusName=test hkon&CorpusActive=-1&skey=1", dbc );

            // 2002	07 22
            // tsinstall corpus	SWE	test
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome\\corpussource\\SWESave\\001\\001&CorpusName=test hkon&CorpusActive=-1&skey=1",	dbc	);

            // 2002	08 09
            // tsinstall corpus	BIO	test
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\temp\\temp\\biologicalandchemical\\001\\002\\002&CorpusName=test hkon&CorpusActive=-1&skey=1",	dbc	);



            // 2002	09 18
            /*
			String s = "file:///C:/Documents and Settings/Indraweb/IndraHome/corpussource/classificationtest/depleteduranium.html";
			//UrlHtmlAttributes	uh = new UrlHtmlAttributes ( "file:///C:/t.html");
			UrlHtmlAttributes uh = new UrlHtmlAttributes ( s);

			long ltgetATopicFromMinInputs_start	= System.currentTimeMillis();

			Topic t	= new Topic	  // inside	getATopicFromMinInputs	 2
			(
			uh,
			true,
			dbc, //	fix	this
			20000,
			null,
			false,
			false,
			Session.cfg.getPropInt ("socketTimeoutMSWebDocs"),
			true,
			true
			);
			Enumeration	ewords = t.htWordsDocOnly_noDescendants.keys();
			while (	ewords.hasMoreElements() )
			{
			String sKey	= (String) ewords.nextElement();
			//com.indraweb.util.Log.log	("word [" +	sKey + "] count	[" + t.htWordsDocOnly_noDescendants.get	( sKey ) + "]\r\n")	;
			}

			System.exit(1);
			*/

            // 2002 11 11 test file put for FTP BATCH CLASSIFY
            if (false) // batch classify ftp test
            {
/*
                for ( int i = 0; i < 6; i++ )
                {
                   api.tsclassify.BatchClassifyTempFileMgr.addOneDocClassResultToTempFile (8, i + ". batch class test string");
                }
*/
            }

            if (false) //  tsqa.TSQATrec2  v2
            {
/*
				String sURLTrecQA2	= "http://localhost:8105/servlet/ts?fn=tsqa.TSQATrec2&skey=1" +
									  "&NumTopRecs=30" +
									  "&qid=403"+
									  "" ;

				executeAPI (sURLTrecQA2,  dbc );
				System.exit(0);
*/
            }

            if (false) //  tsqa.TSQATrec v.1
            {
/*
				String sURLTrecQA2	= "http://localhost:8105/servlet/ts?fn=tsqa.TSQATrec" +
									  "&skey=1" +
									  //"&outputfile=/temp/temp/temp/test/QaRpt.txt&qid=403,406";
									  "&outputfile=/temp/hk/QaRpt33.txt" +
									  "&qid=403"+
									  //"&REPORTONLY=RECALL" +
									  "&NumTopRecs=10" +
									  //"&REPORTONLY=BOTH";
									  "" ;

				String sURLTrecQA	= "http://localhost:8105/servlet/ts?fn=tsqa.TSQATrec&outputfile=/temp/hk/QaRpt35.txt&qid=406&skey=19&NumTopRecs=10";


				executeAPI (sURLTrecQA,  dbc );
*/
            }


            if (false) // classify a few 2002 11 02  // fn: batch true
            {
/*
				try
				{

					// 2002	07 22
					// tsclassify uranium, post	= true
					//String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

                    com.indraweb.execution.Session.cfg.setProp("outputToScreen", "true");
                    String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" +
						"fn=tsclassify.TSClassifyDoc" +
						//"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
						//"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
						//"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
						//"&URL=/corpussource/classificationtest/hkmp4.html" +
						//"&URL=/corpussource/classificationtest/DepletedUranium.html" +
						//"&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" +
						"&REPOSITORYID=11"+
						//"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
						"&SKEY=-220364972" +
						"&batch=true" +
						"&post=true" +
						//"&URL=/corpussource/classificationtest/FT911-1.HTM" +
						//"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" +
						"&URL=/corpussource/classificationtest/FT911-133.HTM" +
						"&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" +
						//"&ExplainScores=true"	+
						//"&NodeToScoreExplain=16881" +
						"" ;

					//com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
					Data_WordsToNodes.verifyCacheUpToDate(dbc, false);
					executeAPI (sURLClassify,  dbc );
					//Data_WordsToNodes.initForceDataStructReload  (dbc, true);
					//executeAPI (sURLClassify,  dbc );

				}
				catch ( Exception e )
				{
					com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
				}
*/
            }


            if (false)
            {


                // 2002	10 14
                // tsrefreshnode
                String sURLRefreshNode = "http://66.134.131.40:8100/servlet/ts?" +
                        "fn=tsclassify.TSNodeRefresh" +
                        "&NODEID=208899" +
                        //"&NODEID=421517" +
                        "&REPOSITORYID=10" +
                        //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
                        //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
                        //"&URL=/corpussource/classificationtest/DepletedUranium.html" +
                        //"&URL=/corpussource/classificationtest/DepletedUranium2.html"	+
                        //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
                        //"&URL=/corpussource/classificationtest/hkmp4.html" +
                        //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                        "&ExplainScores=true" +
                        //"&NodeToScoreExplain=2521" +
                        //"&dburl=hkon/.../corpussource/classificationtest/hkmp4.html" +
                        "&SKEY=-220364972" +
                        "&post=true" +
                        "&corpora=100040";


                executeAPI (sURLRefreshNode , dbc);
            }

            // TEST	FOLDER
            /*
			String[] sArrFileNames = com.indraweb.util.UtilFileEnumerator.listFilesInFolderNonRecursive_returnFQName
			("C:/Documents and Settings/IndraWeb/IndraHome/corpussource/classificationtest/testcase");
			testbed	  (dbc,	sArrFileNames );
			*/


            // 2002	07 08
            // tsrebucket corpus
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSRebucket&CorpusID=1&skey=1", dbc );

            // CQL QUERY
            //executeAPI ("http://localhost:8101/servlet/ts?fn=tscql.TSCql&query=SELECT%20<DOCUMENT>%20WHERE%20DOCUMENTID%3D123&SKEY=234234", dbc);

            com.indraweb.util.Log.logClear ("DONE MAINTEST DONE\r\n");
            System.exit (0);


        } catch ( Throwable e )
        {
            System.out.println ("error in main.java:" + e.getMessage ());
            com.indraweb.util.Log.FatalError ("error in main.java" , e);
        }
    }

    private static void testLogger ()
    {
        Logger logger = Logger.getLogger ("log");
        String sFileLogCfg = "C:/Program Files/ITS/ITSLogConfig.txt";
        PropertyConfigurator.configure (sFileLogCfg);

        int ix = 0;
        while (true)
        {
            logger.info (ix + ". in 1 loop");
            try
            {
                throw new Exception ("hk except");
            } catch ( Exception e )
            {
                logger.fatal (ix + ". in loop" , e);
            }
            com.indraweb.util.clsUtils.pause_this_many_milliseconds (14000);
            ix++;
            if (ix == 1000)
                break;
            ;
        }
        logger.info ("Exiting application.");
    }


    private static void executeAPI (String surlAPI , Connection dbc)
            throws Throwable
    {
        String sParmAndValuesOnly = com.indraweb.util.UtilStrings.getAllAfterLastOfThis (surlAPI , "?");

        //split	all	input args into	a vector , so CorpusID=3&PublisherID=1&CorpusShortName=HKTestCorpusShortName
        // gets	split into a vector	with elements :	CorpusID=3,	PublisherID=1, ...
        Vector vParmEqValues = com.indraweb.util.UtilStrings.splitByStrLen1 (sParmAndValuesOnly , "&");
        api.APIProps props = new api.APIProps ();
        Enumeration e = vParmEqValues.elements ();
        while (e.hasMoreElements ())
        {
            String sParmValue = (String) e.nextElement ();  // get e.g.,	"CorpusID=3"
            Vector vParmAndValue = com.indraweb.util.UtilStrings.splitByStrLen1 (sParmValue , "=");
            if (vParmAndValue.size () != 2)  // split into	a vec of  "CorpusID" as	element	0 and "3" as element 1
                throw new Exception ("invalid input	parm [" + sParmValue + "]\r\n");

            String sParm = (String) vParmAndValue.elementAt (0);
            String sValue = (String) vParmAndValue.elementAt (1);
            //System.out.println("program adding param [" + sParm + "] value [" + sValue + "]" );
            props.put (sParm , sValue);
        }

        PrintWriter out = new PrintWriter (System.out);
        // Log.log ("log test");
        // out.println("output test");
        //java.net.URL url = new java.net.URL("http://www.gulflink.osd.mil/declassdocs/cia/19970825/970613_010191_dec_txt_0001.html");
        ////InputStream	is = url.openStream();
        //byte[] barr =	new	byte[5000];
        //is.read(barr);

        try
        {
            api.APIHandler.doGetMine (null , null , props , out , dbc , true);
        } finally
        {
            out.flush ();
        }
    }


    // as cut and pasted from ts.java for local	use
    public static Connection initializeDB (String sWhichDB)
    {
        try
        {
            Connection dbc = api.statics.DBConnectionJX.getConnection (sWhichDB , "Maintest.initializeDB");
            Hashtable htargs = new Hashtable ();

            String sSQL = "	select ParamName, ParamValue from ConfigParams ";
            Statement stmt = dbc.createStatement ();
            ResultSet rs = stmt.executeQuery (sSQL);

            while (rs.next ())
            {
                String sK = rs.getString (1);
                String sV = rs.getString (2);

                if (sV == null)
                {
                    sV = "";
                }
                htargs.put (sK , sV);
            }

            Session.cfg.addToProperties (htargs);
            rs.close ();
            stmt.close ();
            return dbc;
        } catch ( Exception e )
        {
            api.Log.LogError("error	getting	DB connection internal stack trace" , e);
            return null;
        }
    }


    private static void testbed (Connection dbc , String[] sArrFileNames)
            throws Throwable
    {

        if (false)
        { // hbk control test bed
            sArrFileNames = new String[1];
            sArrFileNames[0] = "C:/Documents and Settings/IndraWeb/IndraHome/corpussource/classificationtest/hkmp4.html";
        }

        // *********************************************
        // TEST	BED
        // *********************************************
        String sArrTests_icfg_classification_DocCountMin[] = {"1", "2"};
        String sArrTests_icfg_classification_SigCountMin[] = {"2"};

        int iTestNum = 0;
        for ( int i = 0 ; i < sArrTests_icfg_classification_DocCountMin.length ; i++ )
        {
            for ( int j = 0 ; j < sArrTests_icfg_classification_SigCountMin.length ; j++ )
            {
                com.indraweb.util.Log.logClearcr ("************************************************");
                com.indraweb.util.Log.logClearcr ("*********************	START TEST " + ( iTestNum++ ));
                com.indraweb.util.Log.logClearcr ("************************************************");
                // now clear that stuff	out
                // delete and verify
                String SQLcountpre = "select count (*) from	document where docurl like '%classificationtest%'";
                long lCntpre = com.indraweb.database.JDBCIndra_Connection.ExecuteQueryAgainstDBlong (SQLcountpre , dbc);
                String SQL1 = "delete from document	where docurl like '%classificationtest%'";
                com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB (SQL1 , dbc , true , -1);
                String SQLcountverify = "select	count (*) from document	where docurl like '%classificationtest%'";
                long lCntVerify = com.indraweb.database.JDBCIndra_Connection.ExecuteQueryAgainstDBlong (SQLcountverify , dbc);
                com.indraweb.util.Log.logClearcr ("DB doc cnt docs like '%classificationtest%' test from	" + lCntpre + "	to " + lCntVerify);

                if (lCntVerify > 0)
                    throw new Exception ("lCntVerify >	0 ");

                Session.cfg.setProp ("cfg_classification_DocCountMin" ,
                        sArrTests_icfg_classification_DocCountMin[i]);
                Session.cfg.setProp ("cfg_classification_SigCountMin" ,
                        sArrTests_icfg_classification_SigCountMin[j]);

                //com.indraweb.util.Log.logClearcr("SIG	WORDS PRE INIT docMin [" +
                //sArrTests_icfg_classification_DocCountMin[i]	+ "] sigmin	[" +
                //sArrTests_icfg_classification_SigCountMin[j]	+ "] " +
                //" ["	+ Data_WordsToNodes.getWordsList()+	"]");
                com.iw.classification.Data_WordsToNodes.verifyCacheUpToDate (dbc ,
                        Session.cfg.getPropBool ("AffinityNodeTitlesOnly")
                );

                //com.indraweb.util.Log.logClearcr("SIG	WORDS POST INIT	[" +
                //sArrTests_icfg_classification_DocCountMin[i]	+ "] sigmin	[" +
                //sArrTests_icfg_classification_SigCountMin[j]	+ "] " +
                //" ["	+ Data_WordsToNodes.getWordsList()+	"]");



                // ***********************************
                // RUN TESTS IN	HERE
                long lStartAPI = System.currentTimeMillis (); //	start file loop
                for ( int k = 0 ; k < sArrFileNames.length ; k++ )
                {

                    if (sArrFileNames[k].indexOf ("/classificationtest") < 0)
                        throw new Exception ("premised on files	being in or	under 'C:/Documents	and	Settings/IndraWeb/IndraHome/corpussource/classificationtest'");
                    String sFileTail = "/corpussource/" + UtilStrings.getAllAfterLastOfThis (sArrFileNames[k] , "/classificationtest");
                    String sURLClassify = "http://66.134.131.40:8100/servlet/ts?" +
                            "fn=tsclassify.TSClassifyDoc" +
                            "&URL=" + sFileTail +
                            //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                            //"&ExplainScores=true"	+
                            //"&NodeToScoreExplain=2521" +
                            "&dburl=hkon/" + sFileTail +
                            "&SKEY=-220364972" +
                            "&post=true";

                    //com.indraweb.util.Log.logClear ( "sURLclassify:" + sURLClassify +	"\r\n");
                    executeAPI (sURLClassify , dbc);
                }
                com.indraweb.util.Log.logcr ("time	(ms) in	executeAPI : " + com.indraweb.util.UtilProfiling.elapsedTimeMillis (lStartAPI));
                // RUN TESTS IN	HERE
                // ***********************************
                com.indraweb.util.Log.logClearcr ("Data_WordsToNodes.gethtWordsToVecOfNodeCounts().size():" +
                        Data_WordsToNodes.gethtWordsToVecOfNodeCounts ().size ());

                String SQL = null;
                com.indraweb.util.Log.logClearcr ("getting	DB results ...");
                SQL = "select score1, count	(*)	from nodedocument nd, document d where nd.documentid = d.documentid	" +
                        " and d.docurl like	'%classificationtest%' group by	score1 ";

                //com.indraweb.util.Log.logClearcr("starting sql to	get	nodedoc	count for score	= "	+ iArrScoreValues[k] + ")");
                Vector vDBResults = com.indraweb.database.JDBCIndra_Connection.ExecuteDBQueryVecOfVecsReturned (SQL , dbc , -1);
                com.indraweb.util.Log.logClearcr ("# docs scored \t" + sArrFileNames.length);
                com.indraweb.util.Log.logClearcr ("DocMin\t" + sArrTests_icfg_classification_DocCountMin[i] +
                        "\tSigMin\t" + sArrTests_icfg_classification_SigCountMin[j]);
                if (vDBResults != null && vDBResults.size () > 0)
                {
                    Vector v1I_Score = (Vector) vDBResults.elementAt (0);
                    Vector v2I_Count = (Vector) vDBResults.elementAt (1);
                    for ( int k = 0 ; k < v2I_Count.size () ; k++ )
                    {
                        com.indraweb.util.Log.logClearcr ("Score\t" + v1I_Score.elementAt (k) +
                                "\tCount\t" + v2I_Count.elementAt (k));

                    }
                }
                else
                {
                    com.indraweb.util.Log.logClearcr ("db final : no doc NODEDOCUMENT records for these docs");
                }

            }
            //com.indraweb.util.Log.logClear ( "****** START CLASSIFY [" + iz +	" ]************\r\n");
            // doc parm	= 1, sig parm =	1

            //	if (iz == 0	)
            //	{
            //			com.indraweb.util.Log.logClear ( "****** done first	classify - keyread ************* r\n");
            //			com.indraweb.util.UtilKeyReads.read();
            //	}


        }


    }

    static private void memIArrTest ()
    {

        System.gc ();
        long lmempre = Runtime.getRuntime ().freeMemory ();
        String s = "lmempre [" + lmempre + "]";
        log1 (s);
        runScopable_memIArrTest ();
        clsUtils.waitForKeyboardInput ("at hk wait 6");
        clsUtils.waitForKeyboardInput ("at hk wait 7");
    }

    static private void runScopable_memIArrTest ()
    {

        System.gc ();
        long lmempre = Runtime.getRuntime ().freeMemory ();
        // make an array of int's to represent word relations ...
        int i_xDimLen = 1000;
        int i_yDimLen = 1000;

        int[][] iArrTest = new int[i_xDimLen][i_yDimLen];

        for ( int i = 0 ; i < i_xDimLen ; i++ )
        {
            for ( int j = 0 ; j < i_yDimLen ; j++ )
            {
                iArrTest[i][j] = ( i * j );
            }
        }

        log1 ("num arr cells - written [" + ( i_xDimLen * i_yDimLen ) + "]");
        System.gc ();
        long lmempost = Runtime.getRuntime ().freeMemory ();
        String s = new java.util.Date () +
                " num arr cells - written [" + ( i_xDimLen * i_yDimLen ) + "] "
                + " mem pre [" + ( lmempre ) + "]"
                + " mem post [" + ( lmempost ) + "]"
                + " mem diff [" + ( lmempre - lmempost ) + "]"
                ;
        log1 (s);
    }


    private static void memRandomStringsArrTest () throws Exception
    {
        System.gc ();
        long lmempre = Runtime.getRuntime ().freeMemory ();

        int iNumWords = 1000000;
        int iMinLen = 6;
        int iEveryXReset = 1000000;

        log1 ("\r\n ********** START " + new java.util.Date ()
                + "\r\niNumWords  [" + ( iNumWords ) + "]"
                + " iMinLen  [" + ( iMinLen ) + "]"
                + " iEveryXReset  [" + ( iEveryXReset ) + "]"
                + " lmempre  [" + ( lmempre ) + "]"
        );

        String[] sArr = new String[iNumWords];

        if (iNumWords % iEveryXReset != 0)
            throw new Exception ("iNumWords % iNumUnique != 0");

        long lmempre_pregc = Runtime.getRuntime ().freeMemory ();
        System.gc ();
        long lmempre_postgc = Runtime.getRuntime ().freeMemory ();
        Random rand = null;
        for ( int kWIdx = 0 ; kWIdx < iNumWords ; kWIdx++ )
        {
            if (kWIdx % 10000 == 0)
            {
/*
                System.gc();
                long lmemmid= Runtime.getRuntime().freeMemory();
*/
                String s = "kWIdx [" + kWIdx + "] "
//                            + " lmemMid [" + lmemmid + "] \r\n"
                        ;
                log1 (s);

            }

            if (kWIdx % iEveryXReset == 0)
            {
                rand = new Random ((long) 1000);
                //System.out.println("new rand at kWIdx [" + kWIdx  + "] ");
            }
            int iLen = com.indraweb.util.UtilRandom.getRandomInt_Min0_MaxSizeMinus1 (rand , 15);

            if (iLen < iMinLen)
                iLen = iMinLen;

            char[] carr = new char[iLen];

            for ( int j = 0 ; j < iLen ; j++ )
            {
                int ichar = 48 + com.indraweb.util.UtilRandom.getRandomInt_Min0_MaxSizeMinus1
                        (rand , 42);
                char c = (char) ichar;
                carr[j] = c;
            }

            String s2 = new String (carr);
/*
            htAll.put ( new Integer ( kWIdx ), s );
            htUnique.put ( s, s );
*/
            sArr[kWIdx] = s2;
            if (kWIdx % 10000 == 0)
                log1 (kWIdx + ". " + s2);

            //System.out.println("new rand at kWIdx [" + kWIdx  + "] s [" + s + "]");
        }

        System.gc ();
        long lmempost_postgc = Runtime.getRuntime ().freeMemory ();

/*
        clsUtils.waitForKeyboardInput("at testwait 0 " +
                        "\r\nhtunique.size() [" + htUnique.size() + "] " +
                        "\r\nhtAll.size() [" + htAll.size() + "] " +
                        "\r\nlmempre_pregc [" + lmempre_pregc + "] " +
                        "\r\nlmempre_postgc[" + lmempre_postgc+ "] " +
                        "\r\nlmempost_pregc [" + lmempost_pregc + "] " +
                        "\r\nlmempost_postgc [" + lmempost_postgc + "] " +
                        "\r\ngap [" + ( lmempost_postgc - lmempre_postgc ) + "] "
        );
*/
/*
        htUnique = null;
        htAll = null;
*/
        System.gc ();
        long lmempost = Runtime.getRuntime ().freeMemory ();

        String s =
                " lmempre [" + ( lmempre ) + "]"
                + " lmempost  [" + ( lmempost ) + "]"
                + " lmempre - lmempost [" + ( lmempre - lmempost ) + "]"
                ;
        log1 (s);

        clsUtils.waitForKeyboardInput ("hk wait 10 ");
        clsUtils.waitForKeyboardInput ("hk wait 10 ");
        System.exit (1);

    }


    private static void log1 (String s)
    {
        System.out.println (s);
        UtilFile.addLineToFile ("/tmp/t.t" , s + "\r\n");
    }

    private static void memNewIntOrStringSameOnesTest ()
    {

        if (false) // INT test
        {
            System.gc ();
            long lmempre = Runtime.getRuntime ().freeMemory ();
            String s =
                    " lmempre [" + ( lmempre ) + "]"
                    ;
            log1 (s);

            int iArrSize = 1000000;
            Integer[] iarr = new Integer[iArrSize];
            for ( int i = 1 ; i < iArrSize ; i++ )
            {
                if (i % 10000 == 0)
                    System.out.println (" i = " + i);

                iarr[i] = new Integer (1);
            }
            System.gc ();
            long lmempost = Runtime.getRuntime ().freeMemory ();
            String s2 =
                    " lmempost[" + ( lmempost ) + "]"
                    ;
            log1 (s2);
            clsUtils.waitForKeyboardInput ("at hkwait 15 " +
                    "\r\ndiff [" + ( lmempre - lmempost ) + "] ");
        }
        if (true) // String test
        {
            System.gc ();
            long lmempre = Runtime.getRuntime ().freeMemory ();
            String s =
                    "String lmempre [" + ( lmempre ) + "]"
                    ;
            log1 (s);

            int iArrSize = 1000000;
            String[] sarr = new String[iArrSize];
            for ( int i = 1 ; i < iArrSize ; i++ )
            {
                if (i % 10000 == 0)
                    System.out.println (" i = " + i);

                sarr[i] = new String ("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
            }
            System.gc ();
            long lmempost = Runtime.getRuntime ().freeMemory ();
            String s2 =
                    "String lmempost[" + ( lmempost ) + "]"
                    ;
            log1 (s2);
            clsUtils.waitForKeyboardInput ("at hkwait 15 " +
                    "\r\ndiff [" + ( lmempre - lmempost ) + "] ");
        }

    }

    private static void testDataStructLoader (Connection dbc) throws Throwable
    {
        try
        {

            UtilFile.addLineToFileKill ("c:/t.t" , "AT TOP OF testDataStructLoader \r\n");
            //com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            long lStartTime = System.currentTimeMillis ();
            System.gc ();
            long lmempreDSbuild = Runtime.getRuntime ().freeMemory ();

            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.verifyCacheUpToDate (dbc , true);

            System.gc ();
            long lmempostDSbuild = Runtime.getRuntime ().freeMemory ();
            // int iCountWordsDistinct = Data_WordsToNodes.htWordsToVecOfNodeCounts.size();
            // int iCountWordsTotal = Data_WordsToNodes.htIntToSAllWordsRedundant.size();
            int iCountNodes = Data_NodeIDsTo_NodeForScore.getInstance ().getMainHT_htNodeIDToNodeForScore ().size ();

            // ?print word info at end of test

            // print word info at end of test
            int iCountNumNFSsPrinted = 0;
            Hashtable htNodeIDToNodeForScore = Data_NodeIDsTo_NodeForScore.getInstance ().getMainHT_htNodeIDToNodeForScore ();
            Enumeration eNFSs = htNodeIDToNodeForScore.elements ();
            while (eNFSs.hasMoreElements ())
            {
                iCountNumNFSsPrinted++;
                if (iCountNumNFSsPrinted > 2)
                    break;
                NodeForScore nfs = (NodeForScore) eNFSs.nextElement ();
                String[] sarr = nfs.getSarr (null , false , false);
                int[] iarr = nfs.getiarr ();
                long lNodeid = nfs.getNodeID ();
                if (sarr != null)
                {
                    for ( int i = 0 ; i < sarr.length ; i++ )
                    {
                        log1 (i + ". nodeid:" + lNodeid + " word [" + sarr[i] + "] cnt [" + iarr[i] + "]");
                    }
                }
                else
                    System.out.println ("scarr == null");

            }
            System.out.println ("completed Data_WordsToNodes.verifyCacheUpToDate ");
//                    System.out.println("iCountWordsDistinct  [" + iCountWordsDistinct + "]");
            //System.out.println("iCountWordsTotal [" + iCountWordsTotal + "]");
            System.out.println ("iCountNodes [" + iCountNodes + "]");
            System.out.println ("iNumtimesNCConstructorCalled [" + NodeCount.iNumNCConstructors + "]");
            System.out.println ("iNumtimesNFSConstructorCalled [" + NodeForScore.iNumNFSConstruct + "]");
            System.out.println ("iNumtimesSCConstructorCalled [" + StringAndCount.iNumtimesSCConstructorCalled + "]");

            String sOldWayFile = Session.sIndraHome + "/IndraControl_LoadMemOldWay.txt";
            if (UtilFile.bFileExists (sOldWayFile))
            {
                System.out.println ("running the old way");
            }
            else
                System.out.println ("running the new way");

            System.out.println ("total time ms [" + ( System.currentTimeMillis () - lStartTime ) + "]");
            System.out.println ("" +
                    "\r\nlmempostDSbuild  [" + lmempostDSbuild + "] " +
                    "\r\ndiff [" + ( lmempreDSbuild - lmempostDSbuild ) + "] ");
            //Data_WordsToNodes.printDataStructContents_dumpAllNodes(false);
            // Data_WordsToNodes.printDataStructAffinityContents_WordToNodes();
            clsUtils.waitForKeyboardInput ("at hkwait 2 ");
            clsUtils.waitForKeyboardInput ("at hkwait 3 ");
            // Data_WordsToNodes.initForceDataStructReload  (dbc, false);

            // executeAPI (sURLClassify,  dbc );
//                    executeAPI (sURLClassify,  dbc );
//                    executeAPI (sURLClassify,  dbc );

            com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit (0);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testDataStructLoader( Connection dbc)

    private static void testTSCLassify (Connection dbc) throws Throwable
    {
        try
        {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

/*
            String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" +
                "fn=tsclassify.TSClassifyDoc" +
                //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
                //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
                //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
                //"&URL=/corpussource/classificationtest/hkmp4.html" +
                "&URL=/corpussource/classificationtest/DepletedUranium.html" +
                "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" +
                "&REPOSITORYID=11"+
                //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                "&SKEY=-220364972" +
                "&batch=false" +
                    "&post=true" +
                    //"&post=false" +
                //"&URL=/corpussource/classificationtest/FT911-1.HTM" +
                //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" +
                //"&URL=/corpussource/classificationtest/FT911-133.HTM" +
//						"&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" +
                //"&ExplainScores=true"	+
                //"&NodeToScoreExplain=16881" +
                   // "&nodeset=421445,421446"
                //"&nodeset=2521,2523"   +
                "" ;
*/

            //String sFile = "00002821.htm.html";   // 0 bytes
            //String sFile = "82345233.html";   // 0 bytes
            // hbkhbkhbk
            // String sFile = "plat092898.htm";
            //String sFile = "plat092898_preTagRmoved.htm";
            //String sFile = "plat092898_preTagRmoved_mpfilter.html";
            //String sFile = "schiz4.htm";
            //String sFile = "plat092898.htm";



            //String sFile = "schizophrenia.htm";
            //String sFile = "43kim0.woti-doc.htm";
            //String sFile = "temp.html";
            //String sFile = "DepletedUranium.html";
            //String sFile = "background node 207970 corpus 16.html";
            //String sFile = "TreatingSchizophrenia.html";
            //String sFile = "Corpus 16 Node 209038 DOD-FDA discussions.html";
            //String sFile = "Corpus 16 Node 209038 and corpus 17 node 250004.html";
            //String sFile = "node 1386237 ability.html";
            //String sFile = "60867527.htm";
            //String sFile = "temp.txt";
            //String sFile = "347.html";
            String sFile = "Corpus16Node207974 GulfWarIllnesses.html";

            //     DepletedUranium.html" +

            String sURL = "/corpussource/classificationtest/" + sFile ;
            api.Log.Log (" Classification sURL [" + sURL  + "]" );
            String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?" +
                    "fn=tsclassify.TSClassifyDoc" +
                    "&URL=" + sURL+
                    "&dburl=dburl/" + sURL +
                    "&REPOSITORYID=11" +
                    "&SKEY=-220364972" +
                    "&batch=false" +
                    //"&post=true" +
                    "&post=true" +
                    "&docsumm=true" +
                    //"&docsummary=passed4 in docsummary test" +
                    //"&doctitle=passe  d in doctitle test" +

                    //2003 09 22 "&nodetoscoreexplain=209038" +
                    // "&explainscores=true" +
                    "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            com.indraweb.util.Log.log ("START CLASSIFY 1\r\n");
            executeAPI (sURLClassify , dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            clsUtils.waitForKeyboardInput ("at hkwait 3 ");
            com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit (0);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSCLassifyAPICall
    private static void testTSCLassify2 (Connection dbc) throws Throwable
    {
        try
        {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

/*
            String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" +
                "fn=tsclassify.TSClassifyDoc" +
                //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
                //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
                //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
                //"&URL=/corpussource/classificationtest/hkmp4.html" +
                "&URL=/corpussource/classificationtest/DepletedUranium.html" +
                "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" +
                "&REPOSITORYID=11"+
                //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                "&SKEY=-220364972" +
                "&batch=false" +
                    "&post=true" +
                    //"&post=false" +
                //"&URL=/corpussource/classificationtest/FT911-1.HTM" +
                //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" +
                //"&URL=/corpussource/classificationtest/FT911-133.HTM" +
//						"&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" +
                //"&ExplainScores=true"	+
                //"&NodeToScoreExplain=16881" +
                   // "&nodeset=421445,421446"
                //"&nodeset=2521,2523"   +
                "" ;
*/

            //String sFile = "00002821.htm.html";   // 0 bytes
            //String sFile = "82345233.html";   // 0 bytes
            // hbkhbkhbk
            // String sFile = "plat092898.htm";
            //String sFile = "plat092898_preTagRmoved.htm";
            //String sFile = "plat092898_preTagRmoved_mpfilter.html";
            //String sFile = "schiz4.htm";
            //String sFile = "plat092898.htm";



            //String sFile = "schizophrenia.htm";
            //String sFile = "43kim0.woti-doc.htm";
            //String sFile = "temp.html";
            //String sFile = "DepletedUranium.html";
            //String sFile = "background node 207970 corpus 16.html";
            //String sFile = "TreatingSchizophrenia.html";
            //String sFile = "Corpus 16 Node 209038 DOD-FDA discussions.html";
            //String sFile = "Corpus 16 Node 209038 and corpus 17 node 250004.html";
            //String sFile = "node 1386237 ability.html";
            //String sFile = "60867527.htm";
            //String sFile = "temp.txt";
            //String sFile = "347.html";
            String sFile = "Corpus16Node207974 GulfWarIllnesses.html";

            //     DepletedUranium.html" +

            String sURL = "/corpussource/classificationtest/" + sFile ;
            api.Log.Log (" Classification sURL [" + sURL  + "]" );
            String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?" +
                    "fn=tsclassify.TSClassifyDoc" +
                    "&docidsource=604046" +
                    "&skey=484464469" +
                    "&explainscores=false" +
                    "&POST=false" +
                    "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            com.indraweb.util.Log.log ("START CLASSIFY 1\r\n");
            executeAPI (sURLClassify , dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            clsUtils.waitForKeyboardInput ("at hkwait 3 ");
            com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit (0);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSCLassifyAPICall


    private static void testTSCLassifyGUIExplainStyle (Connection dbc) throws Throwable
    {
        try
        {

            String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?" +
                    "fn=tsclassify.TSClassifyDoc" +
                    //"&DocIDSource=41293" +
                    //"&DocIDSource=702471" +
                    "&DocIDSource=41293" +  // testTSCLassifyGUIExplainStyle
                    "&post=false" +
                    "&SKEY=-220364972" +
                    "&explainscores=true" +
                    //"&nodetoscoreexplain=95634" +
                    "&nodetoscoreexplain=1402331" +

                    //"&docsummary=passed4 in docsummary test" +
                    //"&doctitle=passe  d in doctitle test" +

                    //2003 09 22 "&nodetoscoreexplain=209038" +
                    // "&explainscores=true" +
                    "";

            //tsclassify.TSClassifyDoc&documentid=41293&post=false&SKEY=478186727

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            com.indraweb.util.Log.log ("START CLASSIFY 1\r\n");
            executeAPI (sURLClassify , dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            clsUtils.waitForKeyboardInput ("at hkwait 3 ");
            com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit (0);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSCLassifyAPICall


    private static void testTSThesaurusSynch (Connection dbc) throws Throwable
    {
        try
        {
            String sURLClassify =
                    "http://localhost:8080/servlet/ts?fn=tsthesaurus.thesaurusoracle.TSThesaurusSynch&skey=23434"
                    + "&DBName=client"
                    + "&UserDB=sbooks"
                    + "&PassDB=racer9";

            executeAPI (sURLClassify , dbc);
            clsUtils.waitForKeyboardInput ("at hkwait 3 ");
            com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit (0);
        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSCLassifyAPICall


    private static void testExplainMorePureAsAPICall (Connection dbc) throws Throwable
    {
        try
        {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

/*
            String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" +
                "fn=tsclassify.TSClassifyDoc" +
                //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
                //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
                //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
                //"&URL=/corpussource/classificationtest/hkmp4.html" +
                "&URL=/corpussource/classificationtest/DepletedUranium.html" +
                "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" +
                "&REPOSITORYID=11"+
                //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                "&SKEY=-220364972" +
                "&batch=false" +
                    "&post=true" +
                    //"&post=false" +
                //"&URL=/corpussource/classificationtest/FT911-1.HTM" +
                //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" +
                //"&URL=/corpussource/classificationtest/FT911-133.HTM" +
//						"&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" +
                //"&ExplainScores=true"	+
                //"&NodeToScoreExplain=16881" +
                   // "&nodeset=421445,421446"
                //"&nodeset=2521,2523"   +
                "" ;
*/
            String sURLClassify = "http://66.134.131.40:8100/servlet/ts?" +
                    "fn=tsclassify.TSClassifyDoc" +
                    "&URL=/corpussource/classificationtest/DepletedUranium.html" +
                    "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" +
                    "&REPOSITORYID=11" +
                    "&SKEY=-220364972" +
                    "&batch=false" +
                    "&post=false" +
                    "&nodetoscoreexplain=208995" +
                    "&explainscores=true" +
                    "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            executeAPI (sURLClassify , dbc);
            clsUtils.waitForKeyboardInput ("at hkwait 3 ");
            com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit (0);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSCLassifyAPICall

    private static void testTSRefreshNode2 (Connection dbc) throws Throwable
    {
        try
        {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

            com.indraweb.execution.Session.cfg.setProp ("outputToScreen" , "true");
            String sURLNodeRefresh = "http://localhost:8080/servlet/ts?" +
                    "fn=tsrefreshnode.TSMetasearchNode" +
                    "&REPOSITORYID=11" +
                    //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                    "&SKEY=-220364972" +
                    //"&batch=false" +
                    "&post=true" +
                    //"&URL=/corpussource/classificationtest/FT911-1.HTM" +
                    //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" +
                    //"&URL=/corpussource/classificationtest/FT911-133.HTM" +
//						"&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" +
                    //"&ExplainScores=true"	+
                    //"&NodeToScoreExplain=16881" +
                    "&nodeid=2521" +
                    "&whichdb=SVR" +
                    //"&nodeid=2521"   +
                    "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            executeAPI (sURLNodeRefresh , dbc);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSRefreshNode2

    private static Connection localInitSession (String sWhichDB)
    {
        if (!com.indraweb.execution.Session.GetbInitedSession ())
        {
//				com.indraweb.util.UtilFile.addLineToFile("/temp/temp.txt", new java.util.Date() +	": doing MainTest init session  \r\n");
            Hashtable htprops = new Hashtable ();
            // Load	environment	from JRUN config application variables first
            String sOSHost = (String) java.lang.System.getProperties ().get ("os.name");
            if (sOSHost.toLowerCase ().indexOf ("windows") >= 0)
            {
                Session.sIndraHome = "C:/Program Files/ITS";
            }
            else
            {
                Session.sIndraHome = "/tmp/IndraHome";
            }

            Session.sLogNameDetail = Session.sIndraHome + "/logs/LogDetail.txt";

            htprops.put ("ImportDir" , "C:/TEMP");
            htprops.put ("AuthenticationSystem" , "ActiveDirectory");
            htprops.put ("MachineName" , "indraweb-e5j05c");
            htprops.put ("DBString_API_User" , "sbooks");
            htprops.put ("DBString_SVR_User" , "sbooks");
            htprops.put ("DBString_API_Pass" , "racer9");
            htprops.put ("DBString_SVR_Pass" , "indra9");

            //String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:client";  // :client :schering :gaea
            //String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:client";  // :client :schering :gaea
            //String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:client";  // :client :schering :gaea
            String sDB = "jdbc:oracle:thin:@66.134.131.37:1521:perseus";  // :client :schering :gaea
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            System.out.println ("using perseus!");
            htprops.put ("DBString_API_OracleJDBC" , sDB);
            System.out.println ("DBString_API_OracleJDBC:" + sDB);
            //htprops.put	( "DBString_API_OracleJDBC", "jdbc:oracle:thin:@207.103.213.109:1521:client");
            //htprops.put	( "DBString_SVR_OracleJDBC", "jdbc:oracle:thin:@207.103.213.109:1521:gaea");
            htprops.put ("DBString_SVR_OracleJDBC" , "jdbc:oracle:thin:@207.103.213.109:1521:gaea");
            htprops.put ("IndraHome" , Session.sIndraHome);
            htprops.put ("IndraFolderHome" , Session.sIndraHome);		//}

            htprops.put ("BatchClassFTPHost" , "shell.theworld.com");		//}
            htprops.put ("BatchClassFTPUser" , "hkon2");		//}
            htprops.put ("BatchClassFTPPass" , "xxxx");		//}
            htprops.put ("BatchClassFTPRemoteDir" , "/usr/tmp");		//}
            htprops.put ("outputToFile" , "true");
            htprops.put ("outputToScreen" , "true");

            Session.cfg = new com.indraweb.execution.ConfigProperties (htprops);
            Session.SetbInitedSession (true);
            Session.cfg.setProp ("outputToScreen" , "true");

            /*
            System.out.println(
                    " API DB [" + htprops.get ( "DBString_API_OracleJDBC") + "]"   +
                    " SVR DB [" + htprops.get ( "DBString_SVR_OracleJDBC") + "]"
            );
            */

        }

        //com.indraweb.util.UtilFile.addLineToFile("c:/temp/temp.txt", new java.util.Date()	+ ": done chech	or init	session	 \r\n");
        return initializeDB (sWhichDB);


    }

    private static void scrambleCorpusWordsAndNodeIDs ()
    {
        try
        {
            for ( int i = 20 ; i < 20 ; i++ )
            {
                Data_WordsToNodes.corpusFileMangler (
                        "C:/Documents and Settings/IndraWeb/IndraHome/corpussource/SigWordCountsFile_300012.txt" ,
                        "C:/Documents and Settings/IndraWeb/IndraHome/corpussource/SigWordCountsFile_300012" + i + ".txt" ,
                        i);
                System.out.println ("completed i = " + i + "[" +
                        "SigWordCountsFile_300012" + i + ".txt" + "]");

            }
        } catch ( Throwable t )
        {
            api.Log.LogFatal ("error 123" , t);
            System.out.println ("error 123");
            System.exit (1);
        }

        System.out.println ("completed ok");
        System.exit (1);
    }

    private static void classifySpeedLoopTestCompareModes (Connection dbc) throws Throwable
    {
        try
        {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";


            String sURLClassify = "http://66.134.131.40:8100/servlet/ts?" +
                    "fn=tsclassify.TSClassifyDoc" +
                    //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
                    //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
                    //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
                    //"&URL=/corpussource/classificationtest/hkmp4.html" +
                    "&URL=/corpussource/classificationtest/DepletedUranium.html" +
                    "&REPOSITORYID=11" +
                    //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                    //"&ExplainScores=true"	+
                    //"&NodeToScoreExplain=2521" +
                    "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" +
                    "&SKEY=-220364972" +
                    "&batch=true" +
                    "&post=true";

            String sURLClassify_nodocsumm = sURLClassify + "&docsumm=false";

            long lStart1 = System.currentTimeMillis ();
            executeAPI (sURLClassify_nodocsumm , dbc);
            long lEnd1 = System.currentTimeMillis ();
            com.indraweb.util.Log.logClear ("strat 3 : " +
                    ( lEnd1 - lStart1 + "\r\n" ));
            System.exit (0);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    }

    private static void testTSMetasearchNode (Connection dbc) throws Throwable
    {
        try
        {
            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

            //int iNodeID = ThreadProducer_MSGetNextNode.getNextNode("main test getting node", dbc);
            int iNodeID = 1323509;   //hbk control 203 03 04 which single node local debug a
            com.indraweb.execution.Session.cfg.setProp ("outputToScreen" , "true");
            com.indraweb.execution.Session.cfg.setProp ("cfg_classification_CommaDelimCorporaToUse" ,
                    "100003");
            com.indraweb.execution.Session.cfg.setProp ("proxyhostname" , "none");
            com.indraweb.execution.Session.cfg.setProp ("proxyhostport" , "none");
            com.indraweb.execution.Session.cfg.setProp ("useproxyWithHttp" , "false");
            com.indraweb.execution.Session.cfg.setProp ("useproxyWithSockets" , "false");

            String sURLNodeRefresh =
                    "http://localhost:8080/servlet/ts?fn=tsmetasearchnode.TSMetasearchNode" +
                    "&post=true" +
                    "&nodeid=" + iNodeID +
                    "&whichdb=SVR" +
                    "&machthreadinfo=maintestdebug" +
                    "&NUMTHREAD=1";    // hbk numthreads hbk

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            executeAPI (sURLNodeRefresh , dbc);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSRefreshNode2

    private static void testExplain (Connection dbc) throws Throwable
    {
        try
        {
/*
            String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" +
                "fn=tsclassify.TSClassifyDoc" +
                "&URL=/corpussource/classificationtest/DepletedUranium.html" +
                "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" +
                "&REPOSITORYID=11"+
                "&SKEY=-220364972" +
                "&batch=false" +
                "&post=true" +
                "" ;
*/

            //String sFileName = "schiz4.htm";   //1386876
            //int iNodeIdToExplain = 1386876;   //

            //String sFileName = "DepletedUranium.html"; // 1386240
            String sFileName = "Corpus11_Node95634_gulfTopic.html"; // 1386240
            int iNodeIdToExplain = 95634;   //
            String sNodeID = "" + iNodeIdToExplain;
            HashTree htArguments = new HashTree ();
            htArguments.put ("fn" , "tsclassify.TSClassifyDoc");
            htArguments.put ("URL" , "/corpussource/classificationtest/" + sFileName);
            htArguments.put ("dburl" , "/corpussource/classificationtest/" + sFileName);
            htArguments.put ("REPOSITORYID" , "11");
            htArguments.put ("SKEY" , "-220364972");
            htArguments.put ("batch" , "false");
            htArguments.put ("post" , "false");
            htArguments.put ("nodetoscoreexplain" , sNodeID);
            htArguments.put ("explainscores" , "true");


            if (true) // switch which mode - post or not
            {
                Enumeration et = htArguments.keys ();

                // Build URL to call Taxonomy Server
                String sURLExplain = "http://localhost:80/itsapi/ts?";
                int i = 0;
                while (et.hasMoreElements ())
                {
                    String sKey = (String) et.nextElement ();
                    String sValue = (String) htArguments.get (sKey);

                    if (i == 0)
                        sURLExplain = sURLExplain + sKey + "=" + sValue;
                    else
                        sURLExplain = sURLExplain + "&" + sKey + "=" + sValue;

                    // Remove any apostraphies (because they are illegal) and change
                    // spaces into + signs
                    sURLExplain = new String (com.indraweb.util.UtilStrings.replaceStrInStr (sURLExplain , " " , "+"));
                    sURLExplain = new String (com.indraweb.util.UtilStrings.replaceStrInStr (sURLExplain , "'" , ""));
                    i++;
                }
                System.out.println ("SURL explain [" + sURLExplain + "]");

                executeAPI (sURLExplain , dbc);
            }
            else
            {


                PrintWriter out = new PrintWriter (System.out);
                Vector vResults = PostExecute (out , "C:/temp/temp/temp/plat092898.htm" ,
                        "tsclassify.TSClassifyDoc" ,
                        htArguments , "http://localhost:80/itsapi/ts?fn=");


                if (vResults.size () < 4)
                {
                    throw new Exception ("Sorry, no classification results found " +
                            "for the topic identifier that was specified. ");
                }
                VectorTree vExplain = (VectorTree) vResults.elementAt (2);

                HTMLDocument Document = new HTMLDocument ();

                String sNodeTitle = (String) vExplain.elementAt (1);
                if (sNodeTitle.length () > 50)
                {
                    sNodeTitle = sNodeTitle.substring (0 , 47) + "...";
                }

                Document.AddVariable ("NODEID" , (String) vExplain.elementAt (0));
                Document.AddVariable ("NODETITLE" , sNodeTitle);
                Document.AddVariable ("DOCURL" , (String) vExplain.elementAt (2));
                Document.AddVariable ("DOCTITLE" , (String) vExplain.elementAt (3));

                // retrieve document frequency and coverage
                Vector vClassResults = (VectorTree) vResults.elementAt (4);
                Enumeration eC = vClassResults.elements ();
                while (eC.hasMoreElements ())
                {
                    try
                    {
                        Vector v = (Vector) eC.nextElement ();

                        if (sNodeID.equals ((String) v.elementAt (1)))
                        {
                            Document.AddVariable ("SC1" , (String) v.elementAt (3));
                            //Document.AddVariable("SFQ", (String) v.elementAt(8));
                        }
                    } catch ( Exception e )
                    {
                    }
                }

                Document.AddVariable ("SFQ" , "scaled");


                // get frq data
                String sData = (String) vExplain.elementAt (( vExplain.size () - 5 ));
                out.println ("<!-- frq: " + sData + " elementAt: " + ( vExplain.size () - 4 ) + "-->");
                //System.out.println("<!-- cov: "+sData+" elementAt: "+(vExplain.size()-4)+"-->");
                int iStart = sData.lastIndexOf ("[");
                int iEnd = sData.lastIndexOf ("]");
                Document.AddVariable ("FRQ" , sData.substring (iStart + 1 , iEnd));

                // get cov data
                sData = (String) vExplain.elementAt (( vExplain.size () - 4 ));
                out.println ("<!-- cov: " + sData + " -->");
                iStart = sData.lastIndexOf ("[");
                iEnd = sData.lastIndexOf ("]");
                Document.AddVariable ("COV" , sData.substring (iStart + 1 , iEnd));

                Document.AddVariable ("Title" , "Explain a Document");
                Document.WriteTemplate (out , "header-admin.tpl");
                Document.WriteTemplate (out , "explain/explain-head.tpl");

                // Loop through each explanation .. but filter out things that aren't explanations
                for ( int loop = 6 ; loop < vExplain.size () - 6 ; loop++ )
                {
                    sData = (String) vExplain.elementAt (loop);

                    // split into readable results..
                    //0. wd- [summary] d1cnt [8] d2cnt [0] incr [0.0]
                    iStart = sData.indexOf ("[");
                    iEnd = sData.indexOf ("]");
                    Document.AddVariable ("SIGNATURE" ,
                            sData.substring (iStart + 1 , iEnd));

                    sData = sData.substring (iEnd + 1);
                    iStart = sData.indexOf ("[");
                    iEnd = sData.indexOf ("]");
                    Document.AddVariable ("SOURCE" ,
                            sData.substring (iStart + 1 , iEnd));

                    sData = sData.substring (iEnd + 1);
                    iStart = sData.indexOf ("[");
                    iEnd = sData.indexOf ("]");
                    Document.AddVariable ("DOC" ,
                            sData.substring (iStart + 1 , iEnd));

                    Document.WriteTemplate (out , "explain/explain-result.tpl");
                }

            } // if as to which mode to run
        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }

    } // private static void testTSCLassifyAPICall

    public static Vector PostExecute (PrintWriter out , String filename ,
                                      String sAPIcall , HashTree htArguments , String sTaxonomyServer)
    {
        try
        {
            VectorTree vOriginal = new VectorTree ();
            vOriginal.SetParent (null);
            VectorTree vObject = vOriginal;

            // Do a multi-part post
            String boundary = "ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
            String twoHyphens = "--";
            String lineEnd = "\r\n";

            HttpURLConnection httpURLConn = null;
            DataOutputStream outStream;
            DataInputStream inStream;
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            // create FileInputStream to read from file
            //String sFn = com.indraweb.execution.Session.cfg.getProp("ImportDir")+filename;
            String sFn = filename;
            FileInputStream fileInputStream = new FileInputStream (new File (sFn));


            String sURL = sTaxonomyServer + sAPIcall;
            Enumeration et = htArguments.keys ();

            // Build URL to call Taxonomy Server
            while (et.hasMoreElements ())
            {
                String sKey = (String) et.nextElement ();
                String sValue = (String) htArguments.get (sKey);

                sURL = sURL + "&" + sKey + "=" + sValue;

                // Remove any apostraphies (because they are illegal) and change
                // spaces into + signs
                sURL = new String (com.indraweb.util.UtilStrings.replaceStrInStr (sURL , " " , "+"));
                sURL = new String (com.indraweb.util.UtilStrings.replaceStrInStr (sURL , "'" , ""));
            }

            URL theURL = new URL (sURL);
            httpURLConn = (HttpURLConnection) theURL.openConnection ();

            httpURLConn.setRequestMethod ("POST");
            httpURLConn.setRequestProperty ("Connection" , "Keep-Alive");
            httpURLConn.setDoOutput (true);
            httpURLConn.setUseCaches (false);
            httpURLConn.setRequestProperty ("Accept-Charset" , "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty ("Accept-Language" , "en");
            httpURLConn.setRequestProperty ("Content-type" , "multipart/form-data; boundary=" + boundary);

            // Set up POST parameters
            StringBuffer params = new StringBuffer ();
            params.setLength (0);
            Enumeration e = htArguments.keys ();
            params.append ("fn=" + sAPIcall);

            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream (httpURLConn.getOutputStream ());

            outStream.writeBytes (twoHyphens + boundary + lineEnd);
            outStream.writeBytes ("Content-Disposition: form-data; name=\"import\";" + " filename=\"" + filename + "\"" + lineEnd);
            outStream.writeBytes (lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available ();
            bufferSize = Math.min (bytesAvailable , maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read (buffer , 0 , bufferSize);
            while (bytesRead > 0)
            {
                outStream.write (buffer , 0 , bufferSize);
                bytesAvailable = fileInputStream.available ();
                bufferSize = Math.min (bytesAvailable , maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read (buffer , 0 , bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes (lineEnd);
            outStream.writeBytes (twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close ();
            outStream.flush ();
            outStream.close ();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream ();
            BufferedReader buf = new BufferedReader (new InputStreamReader (is));
            String sData = new String ();

            while (( sData = buf.readLine () ) != null)
            {
                //System.out.println("sData [" + sData+ "]" );

                int iTag1Start = sData.indexOf ("<");
                int iTag1End = sData.indexOf (">");
                int iTag2Start = sData.lastIndexOf ("</");
                int iTag2End = sData.lastIndexOf (">");

                String sTag1;

                try
                {
                    sTag1 = sData.substring (iTag1Start + 1 , iTag1End);
                } catch ( Exception eTag )
                {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End)
                {
                    sTag2 = sData.substring (iTag2Start + 2 , iTag2End);
                }

                // This is how we detect an END tag
                if (( !sTag1.equals ("") ) && ( sTag1.charAt (0) == '/' ))
                {
                    sTag2 = new String (sTag1.substring (1));
                    sTag1 = new String ("");
                }

                Integer iVariance = new Integer (0);

                // HACK! HACK! Ignore lines that start with..
                if (( !sTag1.equals ("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?") ) &&
                        ( !sTag1.equals ("TSRESULT") ) && ( !sTag2.equals ("TSRESULT") ) &&
                        ( !sTag1.equals ("APIGETS") ) && ( !sTag1.equals ("CALLCOUNT") ) &&
                        ( !sTag1.equals ("CLASSLOAD") ) && ( !sTag1.equals ("TIMEOFCALL_MS") ))
                {

                    // If s1Tag is populated but s2Tag is not, start the object
                    if (( !sTag1.equals ("") ) && ( sTag2.equals ("") ))
                    {

                        if (sTag1.equals ("TS_ERROR"))
                        {
                            httpURLConn.disconnect ();
                            return vOriginal;
                        }
                        if (sTag1.equals ("INTERNALSTACKTRACE"))
                        {
                            httpURLConn.disconnect ();
                            return vOriginal;
                        }
                        VectorTree vOldObject = vObject;
                        vObject = new VectorTree ();
                        vObject.SetParent (vOldObject);
                        vOldObject.addElement (vObject);

                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if (( !sTag2.equals ("") ) && ( sTag1.equals ("") ))
                    {
                        // out.println("<BR>End object: "+sTag2);
                        vObject = (VectorTree) vObject.GetParent ();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if (( !sTag2.equals ("") ) && ( !sTag1.equals ("") ))
                    {
                        String sTag = new String (com.indraweb.util.UtilStrings.getStringBetweenThisAndThat (sData , "<" + sTag1 + ">" , "</" + sTag2 + ">"));
                        //out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
                        // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                        // If the variable value contains a "CDATA", eliminate it here
                        if (com.indraweb.util.UtilStrings.getStrContains (sTag , "<![CDATA"))
                        {
                            sTag = new String (sTag.substring (9 , ( sTag.length () - 3 )));
                        }

                        vObject.addElement (sTag);
                    }
                }
            }

            return vObject;
        } catch ( Exception except )
        {
            except.printStackTrace();
            HTML.HTMLDocument Document = new HTML.HTMLDocument ();
            Logging.Log.LogError (except , out);
            Document.WriteError (out , except.toString ());
            return null;
        }

    } // postExecute

/*
IndraXMLargs 0. url:file:///c:\Documents and Settings\Indraweb\IndraHome/classifyfiles/DepletedUranium.html
IndraXMLargs 1. postedfilename:c:\Documents and Settings\Indraweb\IndraHome/classifyfiles/DepletedUranium.html
IndraXMLargs 2. showscores:true
IndraXMLargs 3. explainscores:true
IndraXMLargs 4. nodetoscoreexplain:208995
IndraXMLargs 5. fn:tsclassify.TSClassifyDoc
IndraXMLargs 6. skey:1591484286
IndraXMLargs 7. post:false
IndraXMLargs 8. content-type:text/plain
IndraXMLargs 9. import:DepletedUranium.html
*/

    private static void loadWebCrawlCache (Connection dbc)
    {
        try
        {
            String sDirFile = "C:/home/admin/fcrawl/temp16.txt";
            System.out.println ("STATE 6");
            Enumeration e = com.indraweb.util.UtilFile.enumFileLines (sDirFile);
            System.out.println ("STATE 7");
            int iLineNum1Based = 0;
            int iMaxUrlSize = -1;
            HashSet hsSetURLsDoneAlready = new HashSet ();
            int iNumDups = 0;
            while (e.hasMoreElements ())
            {
                iLineNum1Based++;
                String sLine = (String) e.nextElement ();
                Vector v = com.indraweb.util.UtilStrings.splitByStrLen1 (sLine , "\t");

                if (v.size () != 2)
                    throw new Exception ("Line " + iLineNum1Based + " size <>2 ");
                String sDocURL = (String) v.elementAt (0);
                if (hsSetURLsDoneAlready.contains (sDocURL.toLowerCase ()))
                {
                    iNumDups++;
                    System.out.print ("found (case ins.) dup #" + iNumDups + "line " + iLineNum1Based + " url [" + sDocURL.toLowerCase () + "]");
                    continue;
                }
                hsSetURLsDoneAlready.add (sDocURL.toLowerCase ()); //save doc for dup check
                if (sDocURL.length () > iMaxUrlSize)
                    iMaxUrlSize = sDocURL.length ();
                System.out.println ("STATE 8.3");
                String sDocFile = (String) v.elementAt (1);

                String sSQL = "insert into WebCrawlCache " +
                        " (REPOSITORYID ,DOCURL , DOCCACHEFILE , STATUS , DATELASTTOUCHED ) " +
                        " values (15, '" + sDocURL + "', '" + sDocFile + "', 1, sysdate )";

                System.out.print ("SQL line " + iLineNum1Based + ":" + sSQL);
                com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB (sSQL , dbc , false , false , -1);
                if (iLineNum1Based % 100 == 0)
                    dbc.commit ();
                System.out.println ("line " + iLineNum1Based + " inserted iMaxUrlSize " + iMaxUrlSize);
            } // while
            System.out.println ("STATE 9");
            dbc.commit ();
        } catch ( Exception e )
        {
            e.printStackTrace ();
        }

    }

    // to add third col and keep file location and file cache info separate
    private static void xpatchWebCrawlCache (Connection dbc) throws Exception
    {
        String sSQL = "select DOCCACHEFILE from webcrawlcache";
        Statement stmt = dbc.createStatement ();
        ResultSet rslt = stmt.executeQuery (sSQL);

        while (rslt.next ())
        {
            String sDOCCACHEFILE = rslt.getString (1);
            System.out.println ("sDOCCACHEFILE [" + sDOCCACHEFILE + "]");
        }

        rslt.close ();
        stmt.close ();
    }

    private static void testTSGetROCData (Connection dbc) throws Throwable
    {
        try
        {
            String sURLgetroc = "http://207.103.213.104:8080/itsharvester/ts?" +
                    "fn=tsclassify.TSGetROCdata" +
                    "&corpusid=100040" +
                    "&DirectoryPath=c:/temp/" +
                    "&SKEY=-220364972" +
                    "";
            executeAPI (sURLgetroc , dbc);
            com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit (0);

        } catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError ("in classify test nonfatal" , e);
        }


    }


    public static void testDocOpen () throws Throwable
    {
        String sURL = "file:///C:/Program Files/ITS/CorpusSource/classificationtest/347.html";
        Connection dbc = localInitSession ("API");
        //testTSCLassify(dbc);
        UrlHtmlAttributes uh = new UrlHtmlAttributes (sURL);

        IndraURL iURL = new IndraURL (sURL , null , null , null , 1 , 0);
        long ltgetATopicFromMinInputs_start = System.currentTimeMillis ();

        Topic t = new Topic// inside	getATopicFromMinInputs	 2
                (
                        uh ,
                        true ,
                        dbc , //	fix	this
                        20000 ,
                        iURL ,
                        false ,
                        false ,
                        Session.cfg.getPropInt ("socketTimeoutMSWebDocs") ,
                        true ,
                        true ,
                        true ,
                        true
                );

        Enumeration ewords = t.htWordsDocOnly_noDescendants.keys ();
        while (ewords.hasMoreElements ())
        {
            String sKey = (String) ewords.nextElement ();
            com.indraweb.util.Log.log ("doc has word [" + sKey + "] count	[" + t.htWordsDocOnly_noDescendants.get (sKey) + "]\r\n");
        }

        System.exit (1);

    }
    private static int iCallCount_testQuerySpeed = 1;
    public static long testQuerySpeed (String sSQL, int iNumRecsToPull, Connection dbc, String sComment ) throws Throwable
    {
        long lTimeStart  = System.currentTimeMillis();
        api.Log.Log (new java.util.Date() + "start SQL test [" + sComment + "] call number [" + iCallCount_testQuerySpeed    + "]" );
        long lStart = System.currentTimeMillis();
        Statement stmt = null;;
		ResultSet rs = null;
		int iLoop = 0;
		String DBResult;
        long lStartFetchLoop = -1;
		try {
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);
			Vector outVecReturnData = new Vector();
			long l = -1;

			ResultSetMetaData rsMD = rs.getMetaData();
            lStartFetchLoop = System.currentTimeMillis();
			while ( rs.next() ) {
				for ( int i = 0; i < rsMD.getColumnCount(); i++ ) {
						outVecReturnData.addElement (rs.getObject ( i + 1 )) ;
				}
            iLoop++;
                if ( iLoop == iNumRecsToPull )
                    break;
			}

		}

		catch(Throwable  e)
		{
			api.Log.LogFatal( "error throwable [" + sSQL	 + "]");
            //throw e;
		}
		finally
		{
			if ( rs != null )
				rs.close();
			if ( stmt != null )
				stmt.close();
		}


        long lDur = com.indraweb.util.UtilProfiling.elapsedTimeMillis (lStart);
        long lDurFetchOnly = com.indraweb.util.UtilProfiling.elapsedTimeMillis (lStartFetchLoop);
        api.Log.Log (new java.util.Date() + "done SQL test [" + sComment + "] call number [" +
                iCallCount_testQuerySpeed    +
                "] lDur [" + lDur + "] ms " +
                "] lDurFetchOnly [" + lDurFetchOnly + "] ms " +
                "] iNumRecsToPull [" + iNumRecsToPull + "] ms "
        );
        iCallCount_testQuerySpeed++;
        return lDur;

    }





/*
    private static int testApplyROC(Connection dbc) throws Exception
    {
        //api.tsclassify.TSApplyROC.doApply("100040", dbc , true);
        return 0;

    }
*/

    public static void testLanguageTranslate (Connection dbc) throws Throwable
    {
        String sOut = TextTranslator_BabelFish.translateText("capabilities", new Integer (2), new Integer (14), true);
        System.exit(1);
        String sURLTranslateCorpus = "http://207.103.213.104:8080/itsharvester/ts?" +
                "fn=multilingual.TSTranslateCorpus" +
                "&CORPUSID=50" +
                "&LANGFROM=2" +
                "&LANGTO=14";

        System.out.println ("pre executeapi");
        executeAPI (sURLTranslateCorpus , dbc);
        System.out.println ("post executeapi");
        //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
        //executeAPI (sURLClassify,  dbc );
        clsUtils.waitForKeyboardInput ("at hkwait 3 ");
        com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
        System.exit (0);

    }
    public static void testCorpusSigDataMigrate (Connection dbc) throws Throwable
    {
        //api.multilingual.TSMigrateCorpusLanguageData.doMigrate(100004, dbc);
        api.multilingual.TSMigrateCorpusLanguageData.doMigrate(50, dbc);
    }


    public static void testURLToOracleStreamConvert(Connection dbc)
    {

        //OutputStream out = System.out;
        //String documentid = "950585";  // just as a test, but any document id will do
        //String documentid = "41293";  // \\66.134.131.60\fdrive\GulfWarDocs\data\4547.html
        //String documentid = "739571";  // http://www.oehha.ca.gov/air/chronic_rels/pdf/sulfuric.pdf
        String documentid = "936894";  // http://ist-socrates.berkeley.edu/~bsp/caucasus/newsletter/2003-03ccan.pdf




        String s = new String();
        CallableStatement cs = null;
        try {
            cs = dbc.prepareCall ("begin GETTEXTSTREAM (:1, :2); end;");
            cs.setString(1, documentid);
            cs.registerOutParameter(2, Types.CLOB);
            cs.execute();

            Clob c = cs.getClob(2);
            //System.out.println("<LENGTH>"+c.length()+"</LENGTH>");
            //System.out.println("<PARTIAL>"+c.getSubString(1,2000)+"</PARTIAL>");
            //System.out.println("<PARTIAL>"+c.getSubString(1,2000)+"</PARTIAL>");
        } catch (Exception e) {
            api.Log.LogError(e);

        } finally {
            if (cs != null) { try { cs.close(); } catch (Exception e) {} cs = null; }
        }

    }

    private static void testDom4JHtmlRead()    throws Exception
    {
      // Transmit the request document
        URL u = new URL("http://www.google.com");
        SAXReader reader = new SAXReader();
        Document document = reader.read(u);
        System.out.print("document.getName() [" + document.getName() + "]");

    }

    private static void test3DNodeDocNodeVisualization(Connection dbc)
    {
        try
        {
            String sSQL = "select nodeid, documentid from nodedocument nd, node n " +
            " where n.nodeid = nd.nodeid and " +
                    "n.corpusid = 100011"
                    +
                 "";

            Statement stmt = dbc.createStatement ();
            ResultSet rs = stmt.executeQuery (sSQL);


            Hashtable htCounter = new Hashtable();
            while (rs.next ())
            {
                int iNodeid = rs.getInt(1);
                int iDocid = rs.getInt(2);
                UtilSets.hash_increment_count_for_string(htCounter, ""+iNodeid+":"+iDocid, 1);
            }

            rs.close ();
            stmt.close ();

            Enumeration enumKeys = htCounter.keys();
            int i = 0;
            while ( enumKeys.hasMoreElements() )
            {
                String sNodeDoc = (String) enumKeys.nextElement();
                System.out.println(i + "\t"+sNodeDoc.replace(':','\t')+ "\t"+(String) htCounter.get(sNodeDoc) );
                i++;
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }

    }

}
