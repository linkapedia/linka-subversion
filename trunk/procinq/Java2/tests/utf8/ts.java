
import java.util.*;
import java.io.*;
import java.sql.*;
import java.lang.reflect.*;

import javax.servlet.*;
import javax.servlet.http.*;

import api.Log;
import api.TSException;
import api.TSDBException;
import api.TSNoSuchMethodException;
import api.TSNoSuchObjectException;
import api.util.MultiPartMisc;

import com.oreilly.servlet.multipart.*;
import com.indraweb.util.UtilFile;

import api.SerialProps;

import com.indraweb.execution.Session;

import com.indraweb.ir.clsStemAndStopList;

/**
 *	Main servlet class for IndraWeb Taxonomy Server API
 *	All rights reserved.
 *	(c)IndraWeb.com,Inc. 2001-2002
 *
 *	@authors hkon, mpuscar
 *
 *	@param	req	Contains the name-value pairs:
 *						fn=functionname
 *						parm1=parameterValue1
 *						parm2=parameterValue2 ...
 *
 *	@param	res	response is written as XML to the output stream of res

 *	@return	xml via output stream
 */

public class ts extends HttpServlet {
    // This is a "convenience method" so, after overriding it there is no need to call the super()
    // This function takes the place of the initializeSession method in the engine project.

    public void init()
            throws ServletException {
        // If the session is already initialized, return.

        if (com.indraweb.execution.Session.GetbInitedSession()) return;

        // Load environment from JRUN config application variables first
        Enumeration eParams = getServletContext().getInitParameterNames();
        Hashtable htprops = new Hashtable();

        while (eParams.hasMoreElements()) {
            String sParamName = (String) eParams.nextElement();
            String sParamValu = (String) getServletContext().getInitParameter(sParamName);

            htprops.put(sParamName, sParamValu);
/*
            UtilFile.addLineToFile("c:/t2.t", new java.util.Date() +
                 " : in ts init() 0.4 ; paramname [" + sParamName + "] " +
                 " ; paramval [" + sParamValu + "]\r\n");
*/
        }
        Session.sIndraHome = (String) getServletContext().getInitParameter("IndraHome");
        System.out.println("ts init set Session.sIndraHome to [" + Session.sIndraHome + "]" );
        Session.cfg = new com.indraweb.execution.ConfigProperties(htprops);

        Session.SetbInitedSession(true);

        api.Log.Log("TS SERVER INITED");

        return;
    }


    public void doSerialize(HttpServletRequest req,
                            HttpServletResponse res)
            throws ServletException, IOException {
        api.APIProps apiprops = new api.APIProps();
        res.setContentType("text/xml; charset=UTF-8");
        //PrintWriter out = res.getWriter();
        SerialProps sDataP;

        try {
            ObjectInputStream objin = new ObjectInputStream(req.getInputStream());
            Object o = objin.readObject();
            sDataP = (SerialProps) o;
            File f = sDataP.GetFile();
            if (f.exists()) {
                f.delete();
            }

            String sFileName = Session.cfg.getProp("ImportDir") + "/" + f.getName();
            FileOutputStream fos = new FileOutputStream(sFileName);
            fos.write(sDataP.bArr);
            fos.flush();
            fos.close();

            sDataP.Props.put("URL", "file:///" + sFileName);

            doGet(req, res, sDataP.Props);

        } catch (Exception e) {
            api.Log.LogError("error 2", e);
            //TSException tse = new TSException(api.emitxml.EmitGenXML_ErrorInfo.ERR_TS_ERROR, e.getMessage());
            //api.emitxml.EmitGenXML_ErrorInfo.emitException("", tse, res.3er());
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        api.Log.Log ("do post 1 ");

        if (request.getContentType().equals("application/x-java-serialized-object")) {
            doSerialize(request, response);
            return;
        }
        if (!request.getContentType().substring(0, 19).equals("multipart/form-data")) {
            doGet(request, response);
        } else {
            MultipartParser mpr = new MultipartParser(request, 1000000000);
            response.setContentType("text/xml; charset=UTF-8");
            //PrintWriter outx = response.getWriter();
            Part p = null;
            String sFilename = null;
            api.APIProps apiprops = new api.APIProps();

            try {
                // get indrahome from jrun config
                String sIndraHome = Session.sIndraHome;
                if (sIndraHome == null)
                    throw new Exception("IndraHomeFolder not specified in jrun config");

                String sPathclassificationFiles = sIndraHome + "/classifyfiles/";
                if (!com.indraweb.util.UtilFile.bDirExists(sPathclassificationFiles)) {
                    TSException tse = new TSException(api.emitxml.EmitGenXML_ErrorInfo.ERR_TS_ERROR, "classification files folder not found [" + sPathclassificationFiles + "]");
                    //api.emitxml.EmitGenXML_ErrorInfo.emitException("", tse, out);
                    Log.LogFatal("Failure in processing MIME attachment", tse);
                }

                // loop through each part that was sent
                while ((p = mpr.readNextPart()) != null) {
                    String sParmName = p.getName();
                    String sParmValue = new String("");
                    if (p.isParam()) {
                        ParamPart pp = (ParamPart) p;
                        sParmValue = pp.getStringValue();
                    }
                    if (p.isFile()) {
                        FilePart fp = (FilePart) p;

                        // Write content type and file name into props
                        apiprops.put("Content-type", fp.getContentType());
                        sParmValue = fp.getFileName();

                        if (fp.getFileName() != null) {
                            // Write file out to temp directory here
                            sFilename = sPathclassificationFiles + fp.getFileName();
                            File fFile = new File(sFilename);

                            int fileloop = 0;
                            while (fFile.exists()) {
                                fileloop++;
                                sFilename = sFilename + fileloop;
                                fFile = new File(sFilename);
                            }

                            InputStream is = fp.getInputStream();
                            FileOutputStream fo = new FileOutputStream(fFile);
                            int c;

                            while ((c = is.read()) != -1) {
                                fo.write(c);
                            }

                            // close streams
                            is.close();
                            fo.close();

                            apiprops.put("PostedFilename", sFilename);
                            if ( com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebug_tsjava_LogTempFileNames.txt") )
                                 api.Log.Log("sFilename [" + sFilename + "]" );
                                apiprops.put("URL", "file:///" + sFilename);
                        }
                    }
                    if (sParmValue != null) {
                        apiprops.put(sParmName, sParmValue);
                    }
                }
                doGet(request, response, apiprops);
            }
                    // If an error occurs attempting to invoke the class, or it does not exist, print the main page
            catch (Exception e) {
                api.Log.LogFatal("Failure in receiving MIME attached HTTP call.", e);
                Log.LogError("caught within ts.java.doPost(), re-throwing", e);
                //api.emitxml.EmitGenXML_ErrorInfo.emitException("caught within ts.java.doPost(), re-throwing", e, response.getWriter());
                throw new ServletException("General exception in ts.java Post see api.log ", e);
            } finally { // cleanup extraneous files
                if (sFilename != null) {
                    File f = new File(sFilename);
                    if (f.exists() && !com.indraweb.util.UtilFile.bFileExists("/temp/IndraDebugLeaveClassifyTempFiles.txt") ) {
                        f.delete();
                    } else {
                        api.Log.Log ("file not existing to delete or IndraDebugLeaveClassifyTempFiles.txt switch set not to [" + sFilename + "]" );
                    }
                }
            }
        }
    }

    // *******************************************
    public void doGet(HttpServletRequest req,
                      HttpServletResponse res)
            throws ServletException, IOException {
        api.Log.Log ("do get 0 ");
        Properties props = new Properties();
        api.APIProps apiprops = new api.APIProps(props);
        doGet(req, res, apiprops);
    }  // doGet

    public void doGet(HttpServletRequest req,
                      HttpServletResponse res,
                      api.APIProps apiprops)
            throws ServletException, IOException {

        api.Log.Log ("do get 1 ");
        Connection dbc = null;
        String sWhichDB = "API";
        try {
            res.setContentType("text/xml; charset=UTF-8");


            //PrintWriter out = res.getWriter();

            Enumeration e4 = req.getParameterNames();
            while (e4.hasMoreElements()) {
                String sParmName = (String) e4.nextElement();
                String sISO = req.getParameter(sParmName);
                byte bytes[] = new byte[sISO.length()];
                sISO.getBytes(0, sISO.length(), bytes, 0);
                String sParmValue = (new String(bytes,"UTF-8"));
                apiprops.put(sParmName, sParmValue);
            }

            try {
                //System.out.println("ts.java has sWhichDB [" + sWhichDB + "] pre DB APIDBInterface.getConnection.");
                sWhichDB = (String) apiprops.get("whichdb", "API");
            } catch (Exception e) {
                System.out.println("in exception [" + Log.stackTraceToString ( e ) + "]" );
                System.out.println("USING API AS ERROR DEFAULT" );
                sWhichDB = "API";
            }

            // System.out.println("Thread : " + Thread.currentThread().getName() + " ts.java has sWhichDB [" + sWhichDB + "] pre DB APIDBInterface.getConnection.");



            // don't init a dbc for tsmetasearch - dont want to waste them
            String sFn = (String) apiprops.get("fn", true);
            //System.out.println("sFn = [" + sFn + "]" );
            //System.out.println("ts getting DBC for sWhichDB  [" + sWhichDB + "]");
            dbc = api.util.APIDBInterface.getConnection(sWhichDB);
            if (dbc == null) {
                throw new Exception("Database connection is NULL DB [" + sWhichDB + "]");
            }

            // instantiate security
            api.security.DocumentSecurity.getSecurityHash(dbc);

            //else
            // System.out.println("IT IS tsmetasearchnode.TSMetasearchNode so not initing DBC " );



            Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));

            // if a proxy has been specified in the configuration, it is set here
            if (Session.cfg.getProp("proxyHost") != null) {
                String sHost = Session.cfg.getProp("proxyHost");
                if (!sHost.toLowerCase().equals("none")) {
                    SetProxyProperties(Session.cfg.getProp("proxyHost"), Session.cfg.getProp("proxyPort"));
                    //api.Log.Log("### Note: api will be using a proxy [" + Session.cfg.getProp("proxyHost") + "]");
                } //else
                  //  api.Log.Log("### Note: api will not be using a proxy.");
            } //else
              //  api.Log.Log("### Note: api will not be using a proxy.");


            api.APIHandler.doGetMine(req, res, apiprops, res.getOutputStream() , dbc, true);
        } catch (Exception e) {
            api.Log.Log ("pre main error");
            e.printStackTrace();
            api.Log.Log ("post main error");
            Log.LogError("caught within ts.java.doGet(), re-throwing", e);
            //api.emitxml.EmitGenXML_ErrorInfo.emitException("caught within ts.java.doGet(), re-throwing", e, res.getWriter());
            throw new ServletException("General exception in ts.java Get see api.log ", e);
        } finally {
            try {
                dbc = api.util.APIDBInterface.freeConnection(dbc);
            } catch (Exception e) {
                api.Log.LogError("error freeing dbc", e);
            }

        }
        //try { dbc.close(); } catch (Exception e) { api.Log.LogError(e); }
    } // public void doGet(HttpServletRequ	est req,

    public void destroy() {
        try {
            Iterator It = api.util.APIDBInterface.getDBsUsed();
            while (It.hasNext()) {
                String sDB = (String) It.next();
                api.Log.Log("SERVER SHUTDOWN - DBdestroy [" + sDB + "]");
                api.statics.DBConnectionJX.destroy(sDB);
            }
        } catch (Exception e) {
            api.Log.LogError("error on destroy", e);
        }

    }

    public void SetProxyProperties(String proxyHost, String proxyPort) {
        //api.Log.Log("### Note: api will be using a proxy: "+proxyHost+":"+proxyPort);

        System.getProperties().put("proxySet", "true");
        System.getProperties().put("proxyHost", proxyHost);
        System.getProperties().put("proxyPort", proxyPort);
    }

} // public class ts extends HttpServlet


