import java.util.*;
import java.io.*;
import java.sql.*;

import java.util.Iterator;
import oracle.jdbc.driver.OracleConnection;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ExecutionContext;

public class RebuildIndexes {
   public static void RebuildIndexes(int i)
	   throws Exception { 

	// define the execution context
	ExecutionContext ec = DefaultContext.getDefaultContext().getExecutionContext();
    Connection conn = DefaultContext.getDefaultContext().getConnection();	
	i++;
	
	// Rebuild the indicies
	long lStartadd = System.currentTimeMillis();
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Recreating indicies from NODEDOCUMENT table.') };
    #sql [ec] { commit };

	#sql [ec] { alter table NODEDOCUMENT add PRIMARY KEY (NODEID,DOCUMENTID) };
		 
	long lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();

	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Primary key index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

	#sql [ec] { create index DOCONNODEDOC on NODEDOCUMENT (DOCUMENTID) TABLESPACE DDATA };

	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();

	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('DOCONNODEDOC index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };
		 
	#sql [ec] { create index NODEDOCUMENTNODEID on NODEDOCUMENT (NODEID) TABLESPACE DDATA };

	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();

	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('NODEDOCUMENTNODEID index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

	#sql [ec] { create index NOTIFICATION on NODEDOCUMENT (DATESCORED) TABLESPACE DDATA };
		 
	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();
	
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('NOTIFICATION index built in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

    Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery("analyze table document estimate statistics");
	rs.close(); stmt.close();
		 
	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();
	
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Estimated statistics on document table in '||:lEndadd||' ms.') };
    #sql [ec] { commit };

    Statement stmt2 = conn.createStatement();
    ResultSet rs2 = stmt2.executeQuery("analyze table nodedocument estimate statistics");
	rs2.close(); stmt2.close();
		 
	lEndadd = System.currentTimeMillis() - lStartadd;
	lStartadd = System.currentTimeMillis();
	
	#sql [ec] { insert into FCRAWL_LOG (LOG) values ('Estimated statistics on node document table in '||:lEndadd||' ms.') };
    #sql [ec] { commit };
    }
}