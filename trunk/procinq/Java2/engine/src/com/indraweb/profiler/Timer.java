/**
 * Timer.java
 *
 *
 * @author 
 * @version
 */
package com.indraweb.profiler;

public class Timer {
    private long startTime;
    private long totalTime;
    private int count = 0;
    private String name;
    private boolean running = false;
    private int iNumStarts = 0;
    private int iNumStops = 0;

    public Timer(String name) {
        this.name = name;
        this.startTime = 0;
        this.totalTime = 0;
    }

    public int compareTo(Timer t) {
        if (t == this) {
            return 0;
        }

        return (int) (totalTime - t.getTotal());
    }

    public String getName() {
        return this.name;
    }

    public boolean isRunning() {
        return running;
    }

    public void start() {
        startTime = System.currentTimeMillis();
        count++;
        running = true;
        iNumStarts++;
    }

    public void stop() {
        running = false;
        totalTime += (System.currentTimeMillis() - startTime);
        startTime = 0;

        Timer oldTimer = (Timer) Profiler.getTimer(name, false);
        if (oldTimer != null
                && oldTimer != this) {
            oldTimer.add(this);
        }
        iNumStops++;
    }

    public long getTotal() {
        return totalTime;
    }

    public int getCount() {
        return count;
    }

    private synchronized void add(Timer timer) {
        totalTime += timer.totalTime;
        count += timer.count;
    }
}
