
package com.indraweb.util;

import java.io.*;
import java.util.*;

	// 1 ------------------------------------------------------------------------------
	// public int FI_CountLinesInFile (String str_FName, int int_includeComments) {
	// ------------------------------------------------------------------------------
	// 2 ------------------------------------------------------------------------------
	// private int FI_LoadFile (String str_FNameToLoad, boolean bool_careAboutcomments) {
	// ------------------------------------------------------------------------------
	// 3 ------------------------------------------------------------------------------
	// public String FI_GetBracketedParameterFromFile (String str_FName, String str_ParmName) {
	// ------------------------------------------------------------------------------

// loads a file into memory : comments and non comments separated
// line counter in a file, w/ or w/o comments
public class FileImage
{
	public String[] m_strA_nonCommentLines;
	public String[] m_strA_CommentLines;
		
	public int int_linesInFileTotal = 0;
	public int int_linesInFileComment = 0;
	public int int_linesInFileNonComment = 0;
	

	void FileImage (String str_FNameToLoad, boolean bool_careAboutcomments) {
		// count num lines in file
		// load file to memory for faster acces
		// can also work from disk directly for parm files via 
		FI_CountLinesInFile (str_FNameToLoad, 1) ;
		FI_LoadFile (str_FNameToLoad, bool_careAboutcomments);
	}
	
	// 1 ------------------------------------------------------------------------------
	public int FI_CountLinesInFile (String str_FName, int int_includeComments) {
	// ------------------------------------------------------------------------------

		try { 
			BufferedReader in = new BufferedReader(new FileReader(str_FName));		
			
			while (in.ready()) {
				String textline;
				textline = in.readLine().trim();
				
				int_linesInFileTotal = 0;
				int_linesInFileNonComment = 0;

				int_linesInFileTotal++;
				if (textline.substring (0,1).equals("#") ||  textline.equals (""))  {
					int_linesInFileComment++;
				}
				else {
					int_linesInFileNonComment++;
				}	
			}
		    in.close();
			
			if (int_includeComments != 0 ) 
				return int_linesInFileTotal;
			else
				return int_linesInFileComment ;
			
		} 
		
		catch (FileNotFoundException e) {
			Log.FatalError ("FileNotFoundException in Class FileImage method FI_CountLinesInFile file [" + 
				str_FName + "]"); 
			return -1 ;
		}
		catch (IOException  e) {
			Log.FatalError ("IOException in Class FileImage method FI_CountLinesInFile file [" + 
				str_FName + "]"); 
			return -1 ;
		}		
		
	}
	
	// 2 ------------------------------------------------------------------------------
	 private int FI_LoadFile (String str_FNameToLoad, boolean bool_careAboutcomments) {
	// ------------------------------------------------------------------------------
		// based on code from line counter above 
		try { 
			if  (int_linesInFileComment + int_linesInFileComment == 0 ) 
				return 0;
			
			m_strA_CommentLines = new String[int_linesInFileComment];
			m_strA_nonCommentLines = new String[int_linesInFileNonComment];
			
			BufferedReader in = new BufferedReader(new FileReader(str_FNameToLoad));		
			
			while (in.ready()) {
				String textline;
				textline = in.readLine().trim();

				int_linesInFileTotal++;
				if (textline.substring (0,1).equals("#") ||  textline.equals (""))  {
					m_strA_CommentLines[int_linesInFileComment] = textline;
					int_linesInFileComment++;
				}
				else {
					m_strA_nonCommentLines[int_linesInFileNonComment] = textline;					
					int_linesInFileNonComment++;
				}	
			}
		    in.close();
			
			if (bool_careAboutcomments == true ) 
				return int_linesInFileTotal;
			else
				return int_linesInFileComment ;
			
		} 
		
		catch (FileNotFoundException e) {
			Log.FatalError ("FileNotFoundException in Class FileImage method FI_CountLinesInFile file [" + 
				str_FNameToLoad + "]"); 
			return -1 ;
		}
		catch (IOException  e) {
			Log.FatalError ("IOException in Class FileImage method FI_CountLinesInFile file [" + 
				str_FNameToLoad + "]"); 
			return -1 ;
		}		
	
	}

	public static int getFileParmInt	 ( String str_FName, String str_ParmName, int whichOne )
	{
		int i = -1;
		try {
			
			i = Integer.parseInt (FI_GetParm (str_FName, str_ParmName, whichOne));
		}
		catch ( Exception e ) 
		{
			Log.FatalError ("getFileParmInt [" + str_FName + "] [" + str_ParmName+ "] [" + whichOne  + "]"  );	
		} 
		return i;
	} 
	 
	 
	// 3 ------------------------------------------------------------------------------
	public static String FI_GetParm (String str_FName, String str_ParmName, int whichOne) {
	// ------------------------------------------------------------------------------

		try { 

			
			BufferedReader in = new BufferedReader(new FileReader(str_FName));		
			
			int thisOne = 0;  // counter for lists of same-named parms in parm files 
			while (in.ready()) {
				String textline;
				textline = in.readLine().trim();

				if (!textline.trim().equals ("") && !textline.substring (0,1).equals("#"))  {
					if (textline.startsWith(str_ParmName) )  
						if (whichOne == thisOne) {
						    in.close();
							return textline.substring(textline.indexOf("[")+1, textline.indexOf("]")).trim();
						}
						else
							thisOne++;
				}	
			}
		    in.close();

			Log.FatalError ("Parameter not found in Class FileImage method FI_GetParm file [" + 
				str_FName + "]" + " Parameter [" + str_ParmName + "] instance [" + whichOne + "]"); 
			
		} 
		
		catch (Exception e) {
			Log.FatalError ("error in getting "+
								"parameter ["+str_ParmName+"] "+
								"from file ["+str_FName+"] "+
								"index which one - zero based ["+whichOne+"]", e);
		}
		
		return "" ;   // parm not found
	}	 
	 
	public static String FI_GetBracketedParameterFromFile (String str_FName, String str_ParmName) {
		return FI_GetParm ( str_FName, str_ParmName, 0);
	}
}
