package com.indraweb.util;

public class UtilFormat
{
	//---------------------------------------------------------------
	/** Direct exception information to the proper output stream.

	 *  @param	Throwable	Exception
	 */
	public static void displayError(Throwable e)
	{
		System.out.println() ;
		System.out.println("Throwable Error:" + e.getMessage()) ;
	}
		
	//---------------------------------------------------------------
	/** Direct exception information to the proper output stream.

	 *  @param	Throwable	Exception
	 */
	public static void displayError(Exception e)
	{
		System.out.println() ;
		System.out.println("Exception Error:" + e.getMessage()) ;
	}
		
	//---------------------------------------------------------------
	/** Add spaces to the right of a given string until the field is
	 *  greater than or equal to the specified width.

	 *  @see	#PadLeft(java.lang.String, int)
	 *  @param	String Value	- String to be padded.
	 *  @param	int Precision	- Width to pad Value to.
	 *  @return	String			- String that has been padded to the specified width.
	 */
	public static String padRight(String Value, int Precision)
	{
		int	Index ;
		StringBuffer	buffer = new StringBuffer(128) ;
		
		buffer.append(Value) ;
		for (Index = Value.length() ; Index < Precision ; Index++)
			buffer.append(' ') ;
		
		return buffer.toString() ;
	}
	
	//---------------------------------------------------------------
	/** Add spaces to the left of a given string until the field is
	 *  treater than or equal to the specified width.

	 *  @see	#PadRight(java.lang.String, int)
	 *  @param	String Value	- String to be padded.
	 *  @param	int Precision	- Width to pad value to.
	 *  @return	String			- String that has been padded to the specified width.
	 */
	public static String padLeft(String Value, int Precision)
	{
		int	Index ;
		StringBuffer	buffer = new StringBuffer(128) ;
		
		for (Index = Value.length() ; Index < Precision ; Index++)
			buffer.append(' ') ;
		buffer.append(Value) ;
		
		return buffer.toString() ;
	}
	
	
	
	//---------------------------------------------------------------
	/** Convert a double to a string with 2 digit to the right
	 *  of the decimal point and a total width of 16.

	 *  @param	double value	- Value to be formatted.
	 *  @return	String			- String containing formatted value.
	 *  @exception	java.lang.Exception
	 */
	public static String formatDouble(double Value)
		throws java.lang.Exception
	{
		return formatDouble(Value) ;
	}
	
	
	//---------------------------------------------------------------
	public static String currentSimpleSortTime(java.util.Date date)
	{
		java.text.SimpleDateFormat	dateFormatter	= new java.text.SimpleDateFormat("yyyyMMdd") ;
		return dateFormatter.format(date) ;
	}
	
}
