package com.indraweb.util;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import com.indraweb.html.*;

public class TreeNodeGeneric
{

	protected	long			lNodeid		= -1;
	private		int				level			= -1; // 0-based 
	protected	TreeNodeGeneric parent			= null;
	protected	long			lParentNodeID	= -1;
				Vector			vChildren		= new Vector(); // vector of children
	private		int				seqAmongSibs	= -1;
	protected	String			nodeName		= null;
	protected	String			sParmInfo		= null; // misc info as "field1 [a] field2 [b] ... 
		
	// -------- constructors 
	public TreeNodeGeneric ( )  
	{
	} 
		
	public TreeNodeGeneric ( TreeNodeGeneric parent_ )  
	{
		parent = parent_;
	} 
		
	public String getNodeName()
	{
		return nodeName;
	} 
	public long getNodeID()
	{
		return lNodeid;
	} 

	public void setNodeID( long lNodeID_)
	{
		lNodeid = lNodeID_;
	} 
	public void setNodeName (String nodeName_)
	{
		nodeName = nodeName_;
	} 

	public void setParentNodeID( long lParentNodeID_)
	{
		lParentNodeID = lParentNodeID_;
	} 
	public long getParentNodeID( )
	{
		return lParentNodeID ;
	} 

	public void setChildVector( Vector vChilden_ )
	{
		vChildren = vChilden_;
	} 
	public Vector getChildVector( )
	{
		return vChildren ;
	} 
	
	public void setParameterInfo( String sParmInfo_ )
	{
		sParmInfo = sParmInfo_;
	} 
	public String getParameterInfo(  )
	{
		return sParmInfo;
	} 
	// ***************************************************************************
	// VERTICAL STUFF - PARENTS, LEVELS
	// ***************************************************************************

	// -------- parent get set
	public synchronized void setParent ( TreeNodeGeneric parent_, boolean resetSequenceAmongSibs)  
	{
		parent = parent_;
		if ( resetSequenceAmongSibs ) // b/c child may know it's sequence to be sorted later with putChildrenInTheirStatedOrderSequenceInefficient
		{
			if ( parent_ != null )
				this.seqAmongSibs = parent_.getNumChildren();
			else
				this.seqAmongSibs = -1; // rot - parent = null;
		}
	} 
		
	public TreeNodeGeneric getParent ( )  
	{
		return parent;
	} 
		
		
	public int getLevel ( )  
	{
		TreeNodeGeneric localParent = parent;
		int level = 0;
		while ( localParent != null)
		{
			localParent = localParent.getParent();
			level++;
		}
		return level;
	} 
		
	// ***************************************************************************
	// HORIZONTAL STUFF - CHILDREN, SIBLINGS
	// ***************************************************************************

	public synchronized void addChild ( TreeNodeGeneric child_)  
	{
		child_.setParent ( this, true );
		vChildren.addElement ( child_ );
	} 
		
	public int getNumChildren ( )  
	{
		return vChildren.size();
	} 
	
	public TreeNodeGeneric getChild ( int i )  
	{
		return ( TreeNodeGeneric) vChildren.elementAt ( i );
	} 
	
	public boolean isLeaf ( )  
	{
		return ( vChildren.size() == 0 );
	} 
	
	public int getSequenceAmongSiblings()
	{
		return 	seqAmongSibs;
	} 

	public void setSequenceAmongSiblings ( int seqAmongSibs_ )
	{
		seqAmongSibs = seqAmongSibs_;
	} 

	
	
	
	
	
	public static String printTree ( TreeNodeGeneric ttg, StringBuffer sb, String indentStr )
	{
		
		sb.append ( indentStr + ttg.printMe() + "\r\n" );
		int uBound = ttg.getNumChildren();
		for (int i = 0; i < uBound; i++ )
		{
			printTree ( ttg.getChild ( i ) ,sb, indentStr + " " ) ;
		} 
		return sb.toString();
	} 


	// ***************************************************************************
	// UTILITIES : PRINT, NAVIGATE NODE PATH, EXPLICATE NODE PATH
	// ***************************************************************************

	
	
	public String printMe ( )
	{
		Vector v = new Vector();
		TreeNodeGeneric tg = this;
		while (tg != null)
		{
			v.addElement( new Integer (tg.getSequenceAmongSiblings()+1)); 
			tg = tg.getParent();
		} 
		StringBuffer sb = new StringBuffer();
		
		for (int i = v.size()-1; i >= 0; i--)
		{
			sb.append( ((Integer) v.elementAt ( i )).toString() + ".");	
			
		} 
		
		return sb + " " + nodeName;
	} 
	
	public static void getNodesAtLevel ( TreeNodeGeneric root, int level, Vector nodeVecAtLevel )
	{
		if (root.getLevel() == level )
		{
			nodeVecAtLevel.addElement ( root );	
		} 
		else
		{
			int uBound = root.getNumChildren();
			for (int i = 0; i < uBound; i++ )
			{
				getNodesAtLevel ( root.getChild ( i ), level, nodeVecAtLevel );
			} 
		} 

	} 
	
	public static void putChildrenInTheirStatedOrderSequenceInefficient_thisNodeOnly ( TreeNodeGeneric t )
	{
		// get set of kids
		Enumeration e = t.getChildVector().elements();
		TreeNodeGeneric[] tArr = new TreeNodeGeneric [ t.getChildVector().size() ];

		while ( e.hasMoreElements() ) 
		{
			TreeNodeGeneric t2 = (TreeNodeGeneric) e.nextElement();
			tArr [ t2.getSequenceAmongSiblings() ] = t2;
		}
		
		Vector vChildrenNew = new Vector();
		for ( int i = 0; i < tArr.length; i++ )
		{
			if ( tArr[i] == null )
			{
				Log.FatalError ("gap in child sequences in TreeNodeGeneric.putChildrenInTheirStatedOrderSequenceInefficient" );
			} 
			else
			{
				vChildrenNew.addElement ( t ) ;
			}
		} 
		
		
	}

	public static Vector getDFSNodesInVector ( TreeNodeGeneric t )
	{
		Vector vDFSOrderNodes = new Vector();	
		recurseDFSAccreter (vDFSOrderNodes, t );
		return vDFSOrderNodes;
		
		
	}
	
	private static void recurseDFSAccreter ( Vector v, TreeNodeGeneric t )
	{	
		v.addElement ( t );			
		for ( int i = 0; i < t.getChildVector().size(); i++ )
		{
			recurseDFSAccreter ( v, ( TreeNodeGeneric ) t.getChildVector().elementAt ( i ) );
		} 
	} 

	public TreeNodeGeneric walkToFindRoot ()
	{	
		TreeNodeGeneric t = this;
		if ( parent != null )
		{
			while ( true ) 
			{	
				if ( t.parent != null )
					t = t.parent;
				else
					break;
			}
		}
		return t;
	} 

	
	public int getNodeDepth ()
	{	
		int iDepth = 0;
		TreeNodeGeneric t = this;
		if ( parent != null )
		{
			while ( true ) 
			{	
				if ( t.parent != null )
					t = t.parent;
				else
					break;
				
				iDepth++;

			}
		}
		return iDepth;
	} 
	
	public String getParentSibIndexPath ()
	{	
		StringBuffer sb = new StringBuffer();
	
		TreeNodeGeneric t = this;
		while ( true ) 
		{	
			sb.insert ( 0, t.getSequenceAmongSiblings() + "." );
			
			if ( t.parent != null )
				t = t.parent;
			else
				break;
		}
		return sb.toString();
	} 

	
	
	
//	public static void ( insert
	
//TreeGeneric treeNodeParent	
	

} 
	
	
