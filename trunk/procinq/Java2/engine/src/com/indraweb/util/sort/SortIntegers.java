package com.indraweb.util.sort;

import java.util.*;

public class SortIntegers
{
	public static void sortIntVec ( Vector v )   throws Exception
	{
		com.indraweb.util.sort.Sort.sort ( v, new comp() );
	} 
	
	public static void sortIntArr ( Integer[] IArr )     throws Exception
	{
		com.indraweb.util.sort.Sort.sort ( IArr, new comp() );
	} 
	
	
	static class comp implements IComparator
	{
		public int compare ( Object o1, Object o2 )
		{
			if ( ((Integer) o1).intValue() < ((Integer) o2).intValue() )
				return -1;
			else if ( ((Integer) o1).intValue() > ((Integer) o2).intValue() )
				return 1;
			else 
				return 0;
		}
		
	}
	
	
	
	
}
