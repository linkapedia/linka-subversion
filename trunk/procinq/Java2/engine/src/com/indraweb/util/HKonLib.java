//HKonLib.java
// a set of functions reusable across projects

package com.indraweb.util;

import com.indraweb.execution.*;
import java.io.* ;				// for print  
import java.util.*;				// for hashtable				   

public class HKonLib
{

	public static boolean  hkdebug_level_routine_entry_exit = false;


	//---------r--------------------------------------------------------------------
	// METHOD print
	//-----------------------------------------------------------------------------
	public static void print (String StrToPrint) {
		Log.log	( StrToPrint, true );
	}


	//-----------------------------------------------------------------------------
	// METHODPauseForKeyboardInput
	//-----------------------------------------------------------------------------
	public static void PauseForKeyboardInput () {
		Log.log ( "!!!!!!! at keyboard wait in PauseForKeyboardInput");	
		try 
		{
			System.in.read();
		} 
		catch (IOException e) 
		{
			Log.FatalError ("error in keyread", e );
		}
	}
	
	
	// ---------------------------------------------------------
	public static int max (int a, int b) { 
		if (a > b) return a;
		else return b;
	} 
	// ---------------------------------------------------------
	
	// ---------------------------------------------------------
	public static int min (int a, int b) { 
		if (a < b) return a;
		else return b;
	} 
	// ---------------------------------------------------------
	
	
	

	
	

	
	
	
	public static String myOutValue (String s, int i) 
	{
		return s+" ["+i+"] ";	
	} 
	public static String  myOut (String s, long l) 
	{
		return s+" ["+l+"] ";	
	} 
	public static String  myOut (String s, double d) 
	{
		return s+" ["+d+"] ";	
	} 
	
	
	public static String readKeyboard( String prompt, int max )
	{
		try  // "are you sure ..."
		{
			Log.log ( "!!!!!!! at keyboard wait in readKeyboard [" + prompt + "]");	
			byte[] bArr = new byte[ max ];
			int readchar = System.in.read ( bArr );
			return new String ( bArr );
		}
		catch ( Exception e ) 
		{	
			Log.printStackTrace_mine (81, e);
			return null;
		}				
		
	} 
	
}

