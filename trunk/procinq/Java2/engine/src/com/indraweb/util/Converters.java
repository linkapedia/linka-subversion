package com.indraweb.util;

import java.util.*;

public class Converters
{

	
	// -------------------------------------------------------
	public static String[] cvtVecToStrArr ( Vector v, boolean reverseOrderTrue )
	// -------------------------------------------------------
	{
		String[] sarr = new String [ v.size() ];
		Enumeration e = v.elements();
		int i = 0;
		if ( !reverseOrderTrue ) 
		{
			while ( e.hasMoreElements() )
			{
				sarr[i] = ( String ) e.nextElement();	
				i++;
			} 
		} 
		else
		{
			while ( e.hasMoreElements() )
			{
				sarr[ sarr.length - ( i  + 1 ) ] = ( String ) e.nextElement();	
				i++;
			} 
		} 
		
		return sarr;
	} // public static Object[] cvtVecToArr ( Vector v )
	
	// -------------------------------------------------------
	public static Hashtable cvtArrToHT ( Object[] oArr )
	// -------------------------------------------------------
	{
		Hashtable ht = new Hashtable();
		if ( oArr.length == 0 ) 
			return ht;
		
		for ( int i = 0; i < oArr.length; i++)
		{
			if ( oArr[i] != null )
				ht.put ( oArr[i], oArr[i] );
		} 
		return ht;
	} 
	
	// -------------------------------------------------------
	public static Vector dedupVector ( Vector v ) 
	// -------------------------------------------------------
	{
		Hashtable ht = new Hashtable();
		
		Enumeration e = v.elements();
		
		while ( e.hasMoreElements() )
		{
			Object o = e.nextElement() ;
			ht.put ( o, o );	
		} 
		
		Vector v2 = new Vector();
		e = ht.elements();
		while ( e.hasMoreElements() )
		{
			v2.addElement ( e.nextElement() );	
		} 
		return v2;
	} 
}
