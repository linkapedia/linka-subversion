package com.indraweb.util;

import java.util.*;
import java.net.*;
import java.io.*;
import com.indraweb.execution.*;

public class UtilNet
{
	
	private static boolean bProxyHttpOn = false;
	private static boolean bProxySocketOn = false;

	public static boolean getProxyHttpOn ()
	{
		return bProxyHttpOn;
	}
	public static boolean getProxySocketOn ()
	{
		return bProxySocketOn;
	}
			
	public static void setHttpProxyOnOff ( boolean bOnIsTrue)
	{
		bProxyHttpOn = bOnIsTrue;
		if ( bOnIsTrue )
		{
            String proxyhostname = Session.cfg.getProp ( "proxyhostname" );
			String proxyhostport = Session.cfg.getProp ( "proxyhostport" );
            //Log.logClear("setting http proxy on proxyhostname [" + proxyhostname + "] proxyhostport  [" + proxyhostport + "]\r\n");
			Properties props = System.getProperties();
			props.put ( "http.proxySet", "true" );
			props.put ( "http.proxyHost", proxyhostname );
			props.put ( "http.proxyPort", proxyhostport );
			System.setProperties ( props );
		}
            else
		{
			//Log.logClear("setting http proxy off\r\n");
			Properties props = System.getProperties();
			props.put ( "http.proxySet", "false" );
			System.setProperties ( props );
		}
	}	
	
	public static void setSocketProxyOnOff ( boolean bOnIsTrue)
	{
		bProxySocketOn = bOnIsTrue;
		if ( bOnIsTrue )
		{
			String proxyhostname = Session.cfg.getProp ( "proxyhostname" );	
			String proxyhostport = Session.cfg.getProp ( "proxyhostport" );	
			//Log.log("setting socket proxy on proxyhostname [" + proxyhostname + "] port [" + proxyhostport+ "]\r\n");
			Properties props = System.getProperties();
			
			// MS VM
			props.put("firewallSet", "true");
			props.put("firewallHost", proxyhostname);
			props.put("firewallPort", proxyhostport);

			// Sun VM
			props.put ( "proxySet", "true" );
			props.put ( "proxyHost", proxyhostname ); 
			props.put ( "proxyPort", proxyhostport );
			System.setProperties ( props );
			
			
		}
		else
		{
			//Log.log("setting socket proxy off\r\n");
			Properties props = System.getProperties();
			props.put("firewallSet", "false"); // MS
			props.put ( "proxySet", "false" ); // sun
			System.setProperties ( props );
		}
	}	
	
	
	public static void setProxyOnOff ( boolean bOnIsTrue )
	{
		setHttpProxyOnOff ( bOnIsTrue );		
		setSocketProxyOnOff ( bOnIsTrue );		
	}

    private static String sHostNameCache = null;
    private static Integer ISytnchHostNAme = new Integer(0);
    public static String getLocalHostName() throws Exception
    {

        if (sHostNameCache  == null )
        {
            synchronized(ISytnchHostNAme)
            {
                if (sHostNameCache  == null ) // race
                {
                    InetAddress addr = InetAddress.getLocalHost();
                    byte[] ipAddr = addr.getAddress();
                    sHostNameCache = addr.getHostName();
                }
            }
        }
        return sHostNameCache;
    }
}

