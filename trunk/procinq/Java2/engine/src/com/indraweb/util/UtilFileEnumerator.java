

package com.indraweb.util;

import java.util.*;
import java.io.*;
import com.indraweb.gfs.*;

public class UtilFileEnumerator
{

	// -------------------------------------------------------------------------------
	public static String[] listFilesInFolderNonRecursive ( String folderName, 
														   boolean bSort,
														   String sContraint,
														   int iConstrainMode ) throws Exception
	// -------------------------------------------------------------------------------
	{
		File fTopicFolder = new File ( folderName );
		String[] saFileList = fTopicFolder.list () ;
		if ( iConstrainMode > 0 ) 
		{
			Vector v = new Vector();
			for ( int i = 0; i < saFileList.length; i++ )
			{
				if ( iConstrainMode == iMODE_CONSTRAIN_NAME_CONTAINS )
				{
					if ( UtilStrings.getStrContains( saFileList[i], sContraint ) )
						v.addElement(saFileList[i]);
				}
				else if (iConstrainMode == iMODE_CONSTRAIN_NAME_ENDSWITH )
				{
					if ( saFileList[i].endsWith ( sContraint ) )
						v.addElement(saFileList[i]);
				}
				else
					throw new Exception ("unknown contrain mode");
			} 
			saFileList = UtilSets.convertVectorTo_StringArray(v);
		} 

		if ( bSort ) 
			GFSSort.sortArrayStrings ( saFileList );
	
		return saFileList;
	} 
	
	
	public static final int iMODE_CONSTRAIN_NAME_NONE = 0;
	public static final int iMODE_CONSTRAIN_NAME_CONTAINS = 1;
	public static final int iMODE_CONSTRAIN_NAME_ENDSWITH = 2;
	// -------------------------------------------------------------------------------
	public static String[] listFilesInFolderNonRecursive_returnFQName ( String folderName  ) throws Exception
	// -------------------------------------------------------------------------------
	{
		return listFilesInFolderNonRecursive_returnFQName ( folderName, "" , null);
	} 

	// -------------------------------------------------------------------------------
	public static String[] listFilesInFolderNonRecursive_returnFQName ( String folderName, String prePendEGFileWhackWhack, String sDummyWayToRestrictForQuick ) throws Exception
	// -------------------------------------------------------------------------------
	{
		File fTopicFolder = new File ( folderName );
		String[] saFileList = fTopicFolder.list () ;
		String finalFolderName = null;
		if ( !folderName.endsWith("/") ) 
			folderName = folderName + "/"; 
		if ( prePendEGFileWhackWhack == null )
			prePendEGFileWhackWhack = "";
		
		Vector v = new Vector();
		for ( int i = 0; i < saFileList.length; i++ )
		{
			saFileList[i] = prePendEGFileWhackWhack + folderName + saFileList[i]	;
			if ( sDummyWayToRestrictForQuick == null || sDummyWayToRestrictForQuick.equals ("") )
			{
				v.addElement ( saFileList[i] );
			} 
			else
			{
				if ( saFileList[i].indexOf ( sDummyWayToRestrictForQuick ) >= 0 )
				{
					v.addElement ( saFileList[i] );
				} 
			}
		} 
		String [] sArrReturn = UtilSets.convertVectorTo_StringArray ( v );
		return sArrReturn;
	} 

	// -------------------------------------------------------------------------------
	public static String[] listFilesInFolderNonRecursive_fileNameMustContain ( String folderName, String fileWildCard ) throws Exception
	// -------------------------------------------------------------------------------
	{
		File fTopicFolder = new File ( folderName );
		iwFileFilter iwf = new iwFileFilter ( fileWildCard );
		String[] saFileList = fTopicFolder.list ( iwf ) ;
		return saFileList;
	} 
	
	// -------------------------------------------------------------------------------
	static class iwFileFilter implements FilenameFilter
	// -------------------------------------------------------------------------------
	{
		String fileFilter = null;
			
		public iwFileFilter ( String fileFilter_ )
		{
			fileFilter = fileFilter_;
		}
			
		public boolean accept ( File f, String file)
		{
			if 	( UtilStrings.getStrContains ( file, fileFilter ) )
				return true;
			else
				return false;
		} 
	} 


	
	
	
	
	/**
	 * Given a dir and a file spec criteria ... 
	 * enumerate thru the file names contained
	 * returns an enumeration of file names is it?
	 */
	
	// ---------------------------------------------------------------------------	
	public static Enumeration enumerateFilesInDir (String dir, 
	// -------------------------------------------------------------------------------
												   String fileSpec_startsWith, 
												   int fileSpecType, 
												   boolean includeDirectories,
												   int maxWanted
												   ) 
	{
		
		// include subdir's
		// what level subdir
		// filespec
		
		
		// enumerateFilesInDir_fileSpecTypeStartsWith = 1;
		
		try {
			
			java.io.File dirFile = new java.io.File ( dir );
			
			if ( !dirFile.isDirectory () ) { 
				Log.FatalError ("enumerateFilesInDir : not a dir " + dir ) ; 
				return null;
			} 
			
			Vector result = new Vector ();
			String fileList[]	= dirFile.list();		

			for ( int i = 0; i < fileList.length; i++ ) 
			{ 
				java.io.File tempFile = new java.io.File( dir + "/"+ fileList[ i ] );
				String curFileOrDirName = tempFile.getName();
				String fileNameOnly = curFileOrDirName.substring( dir.length() + 1 );
				if ( !tempFile.isDirectory() || includeDirectories == true ) 
				{
					if ( fileSpecType == 1 ) // enumerateFilesInDir_fileSpecTypeStartsWith
					{ 
						if (fileNameOnly.startsWith ( fileSpec_startsWith ) ) {
							result.addElement (tempFile) ;
						}
					} 
				}
			}
			return ( Enumeration ) result;
		} catch ( Exception e ) { 
			Log.FatalError ( "in enumerateFilesInDir " + dir +","+ fileSpec_startsWith, e );
			return null;
		} 
		
	}


	
	
	
	
	
	
	
	// ---------------------------------------------------------------------------	
	public static void enumerateFilesInFolder_Recursive ( Vector vFileFQPathAccumulator,
															String dir, 
	// -------------------------------------------------------------------------------
															int maxWanted,
															String sConstrainContains,
															String sConstrainEndsWith
															
														) 
	{
		
		
		try {
			
			//UtilFile.addLineToFile("c:/t.t","in 1.0 enumerateFilesInFolder_Recursive  " + 
			//								" dir [" + dir + "]" + 
			//								//" fileSpec_startsWith [" + fileSpec_startsWith + "]" + 
			//								" \r\n");
			java.io.File dirFile = new java.io.File ( dir );
			
			if ( !dirFile.isDirectory () ) { 
				Log.FatalError ("enumerateFilesInFolder_Recursive : not a dir " + dir ) ; 
				return;
			} 
			
			String fileList[]	= dirFile.list();
			

			for ( int i = 0; i < fileList.length; i++ ) 
			{ 
				String sFilePath = dir + "/"+ fileList[ i ];			
				java.io.File tempFile = new java.io.File( dir + "/"+ fileList[ i ] );
				String curFileOrDirName = tempFile.getName();

				//UtilFile.addLineToFile("c:/t.t","in 1.1 enumerateFilesInFolder_Recursive middle  " + 
												
				//							" dir [" + dir + "]" + 
				//							" curFileOrDirName [" + curFileOrDirName.length() + "]" + 
				//							" dirlen [" + dir.length() + "]" + 
				//							" \r\n");
				
				if ( tempFile.isDirectory() ) 
				{
					enumerateFilesInFolder_Recursive ( vFileFQPathAccumulator, 	
												   dir + "/"+ fileList[ i ],
												   maxWanted,
												   sConstrainContains, 
												   sConstrainEndsWith);
				}
				else
				{
					boolean bInclude = false;
					if ( sConstrainContains == null && sConstrainEndsWith == null ) 
					{
						bInclude = true;
					}
					// contains
					else if ( sConstrainContains != null && sConstrainEndsWith == null ) 
					{
						if ( UtilStrings.getStrContains ( sFilePath, sConstrainContains ) ) 
							bInclude = true;
					} 
					// ends with 
					else if ( sConstrainContains == null && sConstrainEndsWith != null ) 
					{
						if ( sFilePath.toLowerCase().endsWith ( sConstrainEndsWith.toLowerCase() )) 
							bInclude = true;
					} 
					// both
					else if ( sConstrainContains != null && sConstrainEndsWith != null ) 
					{
						if ( UtilStrings.getStrContains ( sFilePath, sConstrainContains ) 
							&& sFilePath.toLowerCase().endsWith ( sConstrainEndsWith.toLowerCase() )) 
							bInclude = true;
						
					} 
						
					if ( bInclude )
						vFileFQPathAccumulator.addElement ( sFilePath  );
					
				}
				if (maxWanted  > 0 &&  vFileFQPathAccumulator.size() >= maxWanted )
					return;
			}
			if ( maxWanted  > 0 && vFileFQPathAccumulator.size() >= maxWanted )
				return;
		} catch ( Exception e ) { 
			Log.FatalError ( "exception leaving enumerateFilesInFolder_Recursive " + dir  , e );
		} 
		finally
		{
			//UtilFile.addLineToFile("c:/t.t","exiting non exception enumerateFilesInFolder_Recursive  " + " \r\n");
		} 
		
	}

	
	
}
