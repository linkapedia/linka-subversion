//clsUtils.java
package com.indraweb.util;

public class clsUtils {
    public static void waitForKeyboardInput() {
        waitForKeyboardInput("Keyboard Wait");
    }

    public static void waitForKeyboardInput(String s) {
        try {
            System.out.println("!!!!!!! at keyboard wait in waitForKeyboardInput [" + s + "]");
            System.in.read();
            System.out.print("Keyboard wait over [" + s + "] \r\n");
        } catch (Exception e) {
            System.out.println(" Exception in waitForKeyboardInput " + e.getMessage());
        }
    }

    // -------------------------------------------------------------------
    public static void pause_this_many_milliseconds(long x) {
        // -------------------------------------------------------------------

        try {
            Thread.sleep(x);
            // int debugme = 0;
            return;
        } catch (InterruptedException e) {
            Log.FatalError("ERROR IN pause_this_many_milliseconds\n");
        }

    }
}
