package com.indraweb.network;

import java.io.*;
import java.net.*;
import com.indraweb.html.*;
import com.indraweb.util.*;

public class InputStreamsGen {

    /**
     * 
     * @param urlHtmlAttribs
     * @param iTimeOutMS
     * @param bVerboseOnError
     * @return
     * @throws Exception 
     */
    public static InputStream getDirectInputStreamFromURL(UrlHtmlAttributes urlHtmlAttribs,
            int iTimeOutMS,
            boolean bVerboseOnError)
            throws Exception {
        InputStream is = null;
        String surl = urlHtmlAttribs.getsURL();
        try {
            if (!surl.toLowerCase().startsWith("file://") && !surl.toLowerCase().startsWith("http://")) {
                surl = "file:///" + surl;
            }

            URL url = new URL(surl);
            long tStartConnectionOpen = System.currentTimeMillis();
            boolean forceURLForIntelliseek = true && UtilStrings.getStrContains(surl, "207.103.213.105:9000");// was from configparams Session.cfg.getProp ("IntelliseekSource")  ) ;


            if (forceURLForIntelliseek || surl.toLowerCase().startsWith("file://")) {
                URLConnection urlCon = url.openConnection();
                is = urlCon.getInputStream();
                is = new BufferedInputStream(is);
            } else {
                is = timedSocketConnect(url, urlHtmlAttribs.getsURL(), iTimeOutMS);
                is = new BufferedInputStream(is);
                if ((System.currentTimeMillis() - tStartConnectionOpen) > 20000) {
                    Log.log("long timedSocketConnect URL [" + surl + "] in [" + (System.currentTimeMillis() - tStartConnectionOpen) + "] ms\r\n");
                }
            }
        } catch (InterruptedIOException e) {
            Log.NonFatalError("getDirectInputStreamFromURL InterruptedIOException msg [" + e.getMessage() + "] on url [" + urlHtmlAttribs.getsURL() + "]\r\n");
            return null;
        } catch (Exception e) {
            if (bVerboseOnError) {
                Log.NonFatalError("getDirectInputStreamFromURL Exception msg [" + e.getMessage() + "] on url [" + urlHtmlAttribs.getsURL() + "]\r\n");
            }
            return null;
        } finally {
        }
        return is;
    }

    /**
     * 
     * @param url
     * @param sURL_
     * @param iTimedSocketTimeout
     * @return
     * @throws Exception 
     */
    public static InputStream timedSocketConnect(URL url, String sURL_, int iTimedSocketTimeout) throws Exception {

        String sRemote_address = url.getHost();
        String sRemote_path = url.getFile();
        int iRremote_port = url.getPort();
        if (iRremote_port == -1) {
            iRremote_port = 80;
        }

        Socket connection = TimedSocket.getSocket(sRemote_address, iRremote_port, iTimedSocketTimeout, sURL_);
        connection.setSoTimeout(iTimedSocketTimeout);

        PrintStream pout = new PrintStream(connection.getOutputStream());
        if (UtilNet.getProxySocketOn()) {
            String s = "GET " + sURL_ + " HTTP/1.0\n";
            pout.print(s);
        } else {
            String s = "GET " + sRemote_path + " HTTP/1.0\n";
            pout.print(s);
        }
        String s = "Host: " + sRemote_address + "\n";
        pout.print(s);

        s = "User-Agent: Mozilla/4.0 (compatible; MSIE 4.01; Windows 98)" + "\n\n";
        pout.print(s);

        return connection.getInputStream();
    }
}
