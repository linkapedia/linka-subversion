package com.indraweb.gfs;

import java.util.*;

public class GFSSort
{

    public GFSSort()
    {
    }

    public static void sort(Vector vec, IComparator c)
    {
	    Object[] arr = new Object[vec.size()];
        vec.copyInto(arr);
	    sort(arr, c);
	    for (int i = 0; i <arr.length; i++)
        {
            vec.setElementAt(arr[i], i);
    	}
    }

	private static Comparator_StringArray comparatorInstance_StrArray = null;
    public static void sortArrayStrings (String[] a)
    {
        Object aux[] = (Object[])a.clone();
		if ( comparatorInstance_StrArray == null )
			comparatorInstance_StrArray = new Comparator_StringArray();
        mergeSort(aux, a, 0, a.length, comparatorInstance_StrArray);
    }

    public static void sort(Object[] a, IComparator c)
    {
        Object aux[] = (Object[])a.clone();
        mergeSort(aux, a, 0, a.length, c);
    }


    private static void mergeSort(Object src[], Object dest[],
                                  int low, int high, IComparator c) 
    {
	    int length = high - low;

	    // Insertion sort on smallest arrays
	    if (length < 7)
        {
	        for (int i=low; i<high; i++)
		    for (int j=i; j>low && c.compare(dest[j-1], dest[j]) > 0; j--)
		        swap(dest, j, j-1);
	        return;
        }

        // Recursively sort halves of dest into src
        int mid = (low + high)/2;
        mergeSort(dest, src, low, mid, c);
        mergeSort(dest, src, mid, high, c);

        // If list is already sorted, just copy from src to dest.  This is an
        // optimization that results in faster sorts for nearly ordered lists.
        if (c.compare(src[mid-1], src[mid]) <= 0)
        {
           System.arraycopy(src, low, dest, low, length);
           return;
        }

        // Merge sorted halves (now in src) into dest
        for(int i = low, p = low, q = mid; i < high; i++)
        {
            if (q>=high || p<mid && c.compare(src[p], src[q]) <= 0)
                dest[i] = src[p++];
            else
                dest[i] = src[q++];
        }
    }

    private static void swap(Object x[], int a, int b)
    {
	    Object t = x[a];
	    x[a] = x[b];
	    x[b] = t;
    }

} 