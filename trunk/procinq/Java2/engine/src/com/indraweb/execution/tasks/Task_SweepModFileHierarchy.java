package com.indraweb.execution.tasks;

import com.indraweb.util.Log;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilFileEnumerator;
import com.indraweb.util.UtilProfiling;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class Task_SweepModFileHierarchy {


    /**
     * remove font tags from all html files in a folder and down - corpus ingest preprocess
     * for SW Eng.
     */

    private static int iCallCount = -1;

    // ******************************
    public static void processFile(String sFQPathAndFilename,
                                   boolean bDeleteTempFile)
            // ******************************
            throws Exception {
        //Log.logClear ( "processFile :" + sFQPathAndFilename + "\r\n");
        iCallCount++;
        if (!UtilFile.bFileExists(sFQPathAndFilename))
            throw new Exception("file missing : " + sFQPathAndFilename);
        //Log.log (iCallCount + ". iLoop [" + iLoop + "] file [" + sFQPathAndFilename + "] \r\n" );

        String sRenamedFileName = getRenameFileName(sFQPathAndFilename);
        if (!UtilFile.bFileExists(sRenamedFileName))  // don't resave an already mod'd folder
        {
            UtilFile.renameFile(sFQPathAndFilename, sRenamedFileName);
        } else
            Log.logClear("error? skipping rename for folder [" + sFQPathAndFilename + "]\r\n");

        String sSourceString = UtilFile.getFileAsString(sRenamedFileName);

        Hashtable htTagsToDiscard = new Hashtable();
        htTagsToDiscard.put("font", "font"); // BE SURE LOWER CASE ONLY!!
        htTagsToDiscard.put("/font", "/font"); // BE SURE LOWER CASE ONLY!!
        htTagsToDiscard.put("sub", "sub"); // BE SURE LOWER CASE ONLY!!
        htTagsToDiscard.put("/sub", "/sub"); // BE SURE LOWER CASE ONLY!!
        htTagsToDiscard.put("sup", "sup"); // BE SURE LOWER CASE ONLY!!
        htTagsToDiscard.put("/sup", "/sup"); // BE SURE LOWER CASE ONLY!!

        //String sSourceString = UtilFile.getFileAsString (sFQPathAndFilename); // remove
        String sModifiedString = modStringRemoveTags(sSourceString, htTagsToDiscard);
        //if ( !sModifiedString.equals(sSourceString))
        //{
        //	Log.logClear ("changing [" + sSourceString + "] to [" +
        //				  sModifiedString + "]\r\n");
        //}

        //System.out.println ("MOD [" + sSourceString + "] to [" + sModifiedString + "]" );
        // done mods - be sure no pre-exist file, write mod'd string to disk file
        UtilFile.deleteFile(sFQPathAndFilename); //if present from a prior run
        UtilFile.addLineToFile(sFQPathAndFilename, sModifiedString);
        //UtilFile.deleteFile ( "c:/temp/temp.txt" ); //if present from a prior run // remove
        //UtilFile.addLineToFile( "c:/temp/temp.txt", sModifiedString	);// remove

        if (!UtilFile.bFileExists(sRenamedFileName))  // don't resave an already mod'd folder
        {
            throw new Exception("renamed file not there to del [" + sRenamedFileName + "\r\n");
        }

        if (bDeleteTempFile) {
            UtilFile.deleteFile(sRenamedFileName);
            if (UtilFile.bFileExists(sRenamedFileName))  // don't resave an already mod'd folder
            {
                throw new Exception("renamed file won't del [" + sRenamedFileName + "\r\n");
            }
        }
    }

    // ******************************
    private static String getRenameFileName(String sFQPathAndFilename)
            // ******************************
            throws Exception {
        iCallCount++;
        // GET RENAME NAME
        String sFileNameNoExt = UtilFile.getFileNameNoExtFromFQFileName(sFQPathAndFilename);
        String sPath = UtilFile.getDirFromFQFileName(sFQPathAndFilename);
        String sFileExtFromFQFileName = UtilFile.getFileExtFromFQFileName(sFQPathAndFilename);


        String sRenamedName = sPath + "/" + sFileNameNoExt + "_sav" + "." +
                sFileExtFromFQFileName;
        // RENAME AND PASS STRINGS TO MODOFIERS
        return sRenamedName;
    }

    // ******************************
    private static String modStringRemoveTags(String sSource, Hashtable htTagsToDiscard)
            // ******************************
            throws Exception {
        StringBuffer sbTarget = new StringBuffer();
        final int iOUTSIDE_A_TAG = 1;
        final int iINSIDE_A_TAG = 2;

        int iPtLeftBrace = 0;
        int iPtRtBrace = 0;
        int iCurrentPointer = 0;
        int iState = iOUTSIDE_A_TAG;
        int iLoopCount = -1;

        boolean bDone = false;
        String sNextBlock = null;
        while (true) {
            iLoopCount++;
            switch (iState) {
                case iOUTSIDE_A_TAG:
                    iPtLeftBrace = sSource.indexOf('<', iCurrentPointer);
                    if (iPtLeftBrace == -1) {
                        try {
                            sNextBlock = sSource.substring(iCurrentPointer, sSource.length() - 2);
                            sbTarget.append(sNextBlock);  // keep non tags
                        } catch (Exception e) {
                            Log.logClear("SWEEP MOD FILE HIERARCHY: tail in file ok \r\n");
                        }
                        bDone = true;
                        break;
                        //System.out.println ("keep block [" + sNextBlock + "]\r\n");

                    }
                    if (iCurrentPointer < iPtLeftBrace) {
                        sNextBlock = sSource.substring(iCurrentPointer, iPtLeftBrace);
                        //System.out.println ("keep block [" + sNextBlock + "]\r\n");
                        sbTarget.append(sNextBlock);  // keep non tags
                    }
                    iCurrentPointer = iPtLeftBrace + 1;
                    iState = iINSIDE_A_TAG;
                    break;

                case iINSIDE_A_TAG:
                    iPtRtBrace = sSource.indexOf('>', iCurrentPointer);
                    if (iPtRtBrace == -1) {
                        throw new Exception("no closing brace");
                    }
                    sNextBlock = sSource.substring(iCurrentPointer - 1, iPtRtBrace + 1);
                    if (testTag(sNextBlock, htTagsToDiscard)) {
                        //System.out.println (iLoopCount + ". keep tag [" + sNextBlock + "]\r\n");
                        sbTarget.append(sNextBlock);  // keep tags
                    }
                    //else
                    //	System.out.println ("remove tag [" + sNextBlock + "]\r\n");

                    iCurrentPointer = iPtRtBrace + 1;
                    iState = iOUTSIDE_A_TAG;
                    break;

                default:
                    throw new Exception("invalid state");
            }
            if (bDone)
                break;
        }
        return sbTarget.toString();
    }

    // filter which to keep
    private static boolean testTag(String s, Hashtable htTagsToDiscard) {

        //Log.logClear("testing tag [" + s + "]\r\n");
        s = s.toLowerCase();
        char[] cArrTag = s.toCharArray();
        int iPtNonWhite = -1;
        for (int i = 1; ; i++) {
            if (!Character.isWhitespace(cArrTag[i])) {
                iPtNonWhite = i;
                break;
            }
        }
        //Log.logClear("iPtNonWhite [" + iPtNonWhite + "]\r\n");
        boolean bPasses = true;
        Enumeration e = htTagsToDiscard.keys();
        while (e.hasMoreElements()) {
            String sTestTag = (String) e.nextElement();
            if (s.substring(iPtNonWhite).toLowerCase().startsWith(sTestTag)) {
                bPasses = false;
                break;
            } else
                continue;
        }

        return bPasses;
    }


}