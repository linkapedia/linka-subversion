package com.indraweb.execution;

import java.util.Hashtable;
import com.indraweb.encyclopedia.Encyclopedia;
import com.indraweb.encyclopedia.Topic;
import com.indraweb.util.*;


public class ThreadInfo
{
	
	// ----  THREAD CLASS/STATIC CHARACTERISTICS
	private static Hashtable htRunningThreads = new Hashtable(); 
	private static String delim = "::";
	// ----  THREAD CLASS/STATIC CHARACTERISTICS
	
	// ----  THREAD OBJECT       CHARACTERISTICS
	private long threadStartTime = -1;
	private String functionTrace = "";
	private String initThreadInfo = null;
	
	// ----  THREAD OBJECT       CHARACTERISTICS
	
	// ---- CONSTRUCTORS
	
	public ThreadInfo ( String initThreadInfo_ )
	{
		initThreadInfo = initThreadInfo_;
		functionTrace = delim + initThreadInfo_;	
		threadStartTime = System.currentTimeMillis();
	}


		
	static public synchronized long getDurationThusFar (  )
	{
		
		ThreadInfo ti = ( ThreadInfo ) htRunningThreads.get ( Thread.currentThread() );
		return ti.getDurationThusFar();
	}
	
	static public synchronized String getInitThreadInfo (  )
	{
		String sReturn = null;
		if ( htRunningThreads == null )
			return "not even a thread HT yet\r\n";
		ThreadInfo ti = ( ThreadInfo ) htRunningThreads.get ( Thread.currentThread() );
		if ( ti != null )
			sReturn = ti.initThreadInfo + ":" ;
		else
			sReturn = "noTIinfo";
		String sMachine = null;
		if ( Session.cfg != null )
			sMachine = Session.cfg.getProp ("MachineName");
		else
			sMachine = "Machine [ThreadInfo.java : cfg not inited]";
			
		sReturn = sMachine + ":" + sReturn;
		return sReturn;
	}
	
	// ---- RUNNING LIST
	
	static public synchronized void addThreadToRunningList ( String initStateInfo )
	{
		htRunningThreads.put (   Thread.currentThread(), new ThreadInfo ( initStateInfo ) ) ;
	}
	static public synchronized void addThreadToRunningList ( Thread t, String initStateInfo )
	{
		htRunningThreads.put (   t, new ThreadInfo ( initStateInfo ) ) ;
	}

    static public synchronized void removeThreadRunningList ()
	{
		htRunningThreads.remove ( Thread.currentThread() );
	}

}



/*
code from PC

package ....servlet;

import java.util.*;

public class ThreadInfo // inner class to hold thread descriptor info
{
	private String		sessionId;
	private String		requestId;
	private int			hashCode;
	private String		serviceName;
	private String		userName;
	private String		threadStartDateTime;
	private long		threadStartTimeMillis;
	static	Hashtable	htCurrentlyRunningThreads = new Hashtable();

	static synchronized void addThreadToRunningList ( String sessionId, String requestId, String userName, String serviceName )
	{
		if ( htCurrentlyRunningThreads.get ( Thread.currentThread() ) != null ) 
		{
			System.out.println ( "PortConServer.putThreadInfo() : WARNING (thread name error?): thread already Running : [sessionId, requestId, userName, serviceName] [" + sessionId+","+requestId+","+userName+","+serviceName + " log files may contain incorrect information, system unaffected otherwise." );
		} 
		else 
		{
			ThreadInfo ti = new ThreadInfo ( sessionId, requestId, userName, serviceName);
			htCurrentlyRunningThreads.put ( Thread.currentThread(), ti );
		}
	} 
	
	static synchronized void removeThreadFromRunningList ( )
	{
		if ( htCurrentlyRunningThreads.get ( Thread.currentThread() ) == null ) {
			System.out.println ( "PortConServer.removeThreadInfo() : WARNING (thread name error?): thread not Running - log files may contain incorrect information, system unaffected otherwise ["+ ThreadInfo.getThreadInfoWithTime() );
		} 
		else
		{
			htCurrentlyRunningThreads.remove ( Thread.currentThread() );
		} 
	} 
	
	public ThreadInfo 
		( 
			String sessionId_,
			String requestId_, 
			String userName_ ,		
			String serviceName_ 
		)
	{
		sessionId				= sessionId_				;
		requestId				= requestId_				;
		serviceName				= serviceName_				;
		userName				= userName_					;
		threadStartDateTime		= (new Date()).toString().substring(0, 19)		; // thread start time
		threadStartTimeMillis	= System.currentTimeMillis();
	} 

	//					ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );

	
	// BASE DATA ELEMENTS
	public static String getThreadUserName ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.userName;
		else
			return null;
	} 

	public static String getThreadServiceName ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.serviceName;
		else
			return null;
	} 

	public static String getThreadSessionId ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.sessionId ;
		else
			return null;
	} 

	public static String getThreadRequestId ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.requestId ;
		else
			return null;
	} 
	// END BASE DATA ELEMENTS

	public static String getThreadInfo ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.userName+".["+ti.requestId+"]."+ti.serviceName+"."+ti.sessionId;
		else
			return null;
	} 

	public static String getThreadInfoShort ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.getThreadUserName() + "." + ti.getThreadRequestId() ;
		else
			return null;
	} 

	public static String getThreadInfoWithTime ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.userName+".["+ti.requestId+"].["+ti.serviceName+"] " + 
				"Started@ ["+ti.threadStartDateTime+"] time ms ["+ ( System.currentTimeMillis()  - ti.threadStartTimeMillis )+"]" ;
		else
			return null;
	} 

	public static String getThreadNameReqIDElapsed ()
	{
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.userName+".["+ti.requestId+"] time ms\t["+ ( System.currentTimeMillis()  - ti.threadStartTimeMillis )+"]" ;
		else
			return null;
	} 

    public static String getThreadKey() 
    {
		ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( Thread.currentThread() );
		if (ti != null)
			return ti.userName+".["+ti.requestId+"]."+ti.sessionId;
		else
			return null;	
    }


	static public synchronized String getRunningRequests()
	{
		// TO DO: synch so that no other thread removes in midst of this
		try 
		{
			Enumeration en = htCurrentlyRunningThreads.keys() ;

			StringBuffer sb = new StringBuffer();
		
			int numPendingRequests = 0;
			while ( en.hasMoreElements( ) ) 
			{
				Thread t = ( Thread ) en.nextElement();

				ThreadInfo ti = ( ThreadInfo ) htCurrentlyRunningThreads.get ( t );

				if ( !( (String) ti.userName + "." + ti.requestId).equals (getThreadUserName() + "." + getThreadRequestId() )  )
				{
					sb.append ( " " + ti.userName + "." + ti.requestId + " " );
					numPendingRequests++;
				}

			}

			return " Pending [" + numPendingRequests + "] : [" + sb.toString() + "]" ;
	
		} catch ( Throwable e ) {
			System.out.println( "ThreadInfo.getRunningRequests() : WARNING: throwable error in getting pending server requests " );
			e.printStackTrace();
		} 
		return null;
	}



}  // 	class ThreadInfo
*/