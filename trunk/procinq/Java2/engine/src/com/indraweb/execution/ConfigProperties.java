package com.indraweb.execution;

import com.indraweb.util.Log;
import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.clsUtils;
import java.util.Properties;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

public class ConfigProperties {

    private static final Logger log = Logger.getLogger(ConfigProperties.class);
    int iNumPropsSet = 0;
    String[] args;
    public Properties pConfig = new Properties();
    private Vector vConfigIndexKeeper = new Vector();

    class nameValuePair {

        String name = null;
        String value = null;

        public nameValuePair(String name_, String value_) {
            name = name_;
            value = value_;
        }
    }

    public ConfigProperties(String sCfgFileName_) {
        addToProperties(sCfgFileName_, true);
    }

    public ConfigProperties(String[] args_) {
        addToProperties(args_);
    }

    public ConfigProperties(Map ht_) {
        addToProperties(ht_);
    }

    // **********************************************************
    public static String getFromArgs(String[] args, String sParmToGet, boolean bRequired) {
        boolean bFoundParm = false;
        String sReturn = null;
        for (int i = 0; i < args.length; i += 2) {
            if (!args[i].startsWith("-")) {
                Log.FatalError(" argument type specifier [" + args[i] + "] not valid, exiting");
            } else {
                String sWhichProp = args[  i].substring(1).trim();
                if (sWhichProp.equals(sParmToGet)) {
                    bFoundParm = true;
                    String sPropValue = args[ i + 1];
                    if (sPropValue.startsWith("-")) {
                        Log.FatalError("property can't start with '-' [" + sPropValue + "] for property [" + sWhichProp + "]");
                    }

                    sReturn = sPropValue;
                }
            }
        }
        if (bFoundParm && bRequired) {
            Log.FatalError("parm [" + sParmToGet + "] not found in argument list\r\n");
        }

        return sReturn;
    }

    // **********************************************************
    public String getProp(String sPropName) {
        return (getProp(sPropName, true, null));
    }

    public String getProp(String sPropName, boolean bRequired, String sDefault) {
        String s = null;
        s = getPropCommon(sPropName, bRequired, sDefault);
        return s;
    }

    // **********************************************************
    public int getPropInt(String sPropName) {
        return (getPropInt(sPropName, true, null));

    }

    public int getPropInt(String sPropName, boolean bRequired, String sDefault) {
        String s = null;
        s = getPropCommon(sPropName, bRequired, sDefault);
        return (Integer.parseInt(s));
    }

    // **********************************************************
    public double getPropDbl(String sPropName) {
        return getPropDbl(sPropName, true, null);
    }

    public double getPropDbl(String sPropName, boolean bRequired, String sDefault) {
        String s = null;
        s = getPropCommon(sPropName, bRequired, sDefault);
        return (Double.valueOf(s).doubleValue());
    }

    // **********************************************************
    public long getPropLng(String sPropName) {
        return getPropLng(sPropName, true, null);

    }

    public long getPropLng(String sPropName, boolean bRequired, String sDefault) {
        String s = null;
        s = getPropCommon(sPropName, bRequired, sDefault);
        return (Long.valueOf(s).longValue());
    }

    // **********************************************************
    public boolean getPropBool(String propName) // **********************************************************
    {
        return getPropBool(propName, true, null);
    }
    static int iCallCtr = 0;
    // **********************************************************

    public boolean getPropBool(String propName, boolean bRequired, String sDefault) // **********************************************************
    {
        String s = null;

        iCallCtr++;
        //UtilFile.addLineToFile("/temp/temp.txt", iCallCtr + ". getPropBool propName [" + propName + "] bRequired [" + bRequired + "] sDefault [" + sDefault  + "]\r\n" );
        s = getPropCommon(propName, bRequired, sDefault);

        try {
            if (!(s.toLowerCase().equals("false") || s.toLowerCase().equals("true"))) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("ERROR: unable to get boolean property correctly, setting to false [" + propName + "]");
            return false;
        }

        return (Boolean.valueOf(s).booleanValue());
    }

    public Properties getProps() {
        return pConfig;
    }
    private static Hashtable htPropsActuallyUsed = new Hashtable();

    // **********************************************************
    private String getPropCommon(String propName, boolean bRequired, String sDefault) // **********************************************************
    {
        htPropsActuallyUsed.put(propName, propName);
        String s = (String) pConfig.get(propName);
        //com.indraweb.util.Log.logClearcr ( "property fetch [" + propName + "] = [" + s + "]" );
        if (s == null) {
            if (bRequired && (sDefault == null || sDefault.trim().equals(""))) {
                Exception e = new Exception("caller looking for property with Name = null : "
                        + "from in getPropCommon fatal about to be called : property not set [" + propName + "]");
                Log.NonFatalError("STACKTRACE FOR getPropCommon : " + Log.stackTraceToString(e));
                //clsUtils.pause_this_many_milliseconds ( 5000 );
                return null;
                //Log.FatalError ( "property not set [" + propName + "]" );
            } else // return default
            {
                return sDefault;
            }
        } else {
            return s;
        }

    }

    // **********************************************************
    public Object getPropDefault(String propName, boolean bRequired, Object oDefault) // **********************************************************
    {
        Object oReturn = pConfig.get(propName);
        if (oReturn == null) {
            if (bRequired) {
                Exception e = new Exception("caller looking for property Object with Name = null : "
                        + " from in getPropCommon fatal about to be called : property not set [" + propName + "]");
                Log.log("STACKTRACE FOR getPropCommon : " + Log.stackTraceToString(e));
                //clsUtils.pause_this_many_milliseconds ( 5000 );
                //Log.FatalError ( "property not set [" + propName + "]" );
                return null;
            } else // return default
            {
                return oDefault;
            }
        } else {
            return oReturn;
        }
    }

    // **********************************************************
    public void addToProperties(String sCfgFileName_, boolean okIfThereAlready) // **********************************************************
    {
        try {
            System.out.println("**** setting properties from config file [" + sCfgFileName_ + "] ****");
            Enumeration e = UtilFile.enumFileLines(sCfgFileName_);
            String line = null;
            while (e.hasMoreElements()) {
                line = ((String) e.nextElement()).trim();
                try {
                    if (!line.trim().startsWith("#") && UtilStrings.getStrContains(line, "[")) {
                        String sWhichProp = UtilStrings.getsUpToNthOfThis_1based(
                                line, "[", 1).trim();

                        if (!sWhichProp.equals("")) {
                            String sPropValue = UtilStrings.getStringBetweenThisAndThat(
                                    line, "[", "]");

                            String sExistingParmValue = (String) pConfig.get(sWhichProp);
                            if (!okIfThereAlready && sExistingParmValue != null) {
                                Log.FatalError("FATAL SYSOUT EXIT received two config values for [" + sWhichProp + "]");
                                throw new Exception("FATAL SYSOUT EXIT received two config values for [" + sWhichProp + "]");
                            }

                            setProp(sWhichProp, sPropValue, okIfThereAlready);
                        }
                    }
                } catch (Exception e2) {
                    Log.FatalError("invalid config line [" + line + "] in cfg file [" + sCfgFileName_ + "]");
                }
            }
        } catch (Throwable t) {
            Log.FatalError("fail in addToProperties", t);
        }
    }

    /**
     * 
     * @param htargs 
     */
    public void addToProperties(Map<String, String> htargs) {
        log.debug("addToProperties(Map<String,String>)");
        Set<String> keys = htargs.keySet();
        for (String sK : keys) {
            String sV = htargs.get(sK);
            if(sV==null || sV.isEmpty()){
                log.warn("The parameter value for {"+sK+"} is Empty or Null");
                sV = "";
            }
            log.debug("Adding property=" + sK + " with value=" + sV);
            setProp(sK, sV);
        }
    }

    // **********************************************************
    public void addToProperties(String[] args) {
        for (int i = 0; i < args.length; i += 2) {
            if (!args[i].startsWith("-")) {
                System.out.println(" argument type specifier [" + args[i] + "] not valid, exiting");
            } else {
                String sWhichProp = args[  i].substring(1).trim();
                if (!sWhichProp.equals("")) {
                    String sPropValue = args[ i + 1];
                    if (sPropValue.startsWith("-")) {
                        System.out.println("property value can't start with '-' [" + sPropValue + "] for property [" + sWhichProp + "]");
                    }

                    setProp(sWhichProp, sPropValue);
                    System.out.println("setting from args [" + sWhichProp + "] value [" + sPropValue + "]");

                }
            }
        }
    }

    // **********************************************************
    public synchronized void setProp(String sPropName, String sPropValue) {
        setProp(sPropName, sPropValue, true);
    }

    // **********************************************************
    public synchronized void setProp(String sPropName, String sPropValue, boolean bOKifTherealready) {
        String sPropValueAlreadyThere = (String) pConfig.get(sPropName);

        if ((sPropValueAlreadyThere) != null && !bOKifTherealready) {
            clsUtils.waitForKeyboardInput("property already set [" + sPropName + "]\r\n");
        }

        if (sPropName.equals("outputToScreen")) {
            // hbk control 2003 03 08
            if (com.indraweb.util.UtilFile.bDirExists("c:/ausr/hkon")) {
                sPropValue = "true";
            }
        }

        if (sPropName.equals("outputToFile")) {
            // hbk control 2003 03 08
            if (com.indraweb.util.UtilFile.bDirExists("c:/ausr/hkon")) {
                sPropValue = "true";
            }
        }

        iNumPropsSet++;
        pConfig.put(sPropName, sPropValue);
        nameValuePair nv = new nameValuePair(sPropName, sPropValue);
        // don't double count this if being set again
        if ((sPropValueAlreadyThere) == null) {
            vConfigIndexKeeper.addElement(nv);
        }
    }   // private Properties overWriteCfgPropWithProgrmArgProps ( String[] args  )
}
