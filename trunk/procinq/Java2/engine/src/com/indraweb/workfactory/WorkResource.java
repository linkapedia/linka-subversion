package com.indraweb.workfactory;

import java.util.*;
import com.indraweb.util.*;

public class WorkResource
{
	protected Vector vOneWorkResourceUnit = null;
	
	public void WorkResource ( Vector vOneWorkResourceUnit_ )
	{
		vOneWorkResourceUnit = vOneWorkResourceUnit_;	
	} 
	
	public Vector getvOneWorkResourceUnit ( )
	{
		return vOneWorkResourceUnit;	
	}
	public void closeResources ( )
	{
		Log.FatalError ("must be overridden");
	}
}
