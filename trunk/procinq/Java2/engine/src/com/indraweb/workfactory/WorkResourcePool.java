package com.indraweb.workfactory;

import java.util.*;
import com.indraweb.util.*;
import com.indraweb.execution.*;

public class WorkResourcePool extends Vector
{
	// each vector element will be 
	
	public int getNumResourcesRemaining()
	{
		return ( (Vector) this).size();
	}
	
	public synchronized WorkResource getWorkResource()
	{
		while (  ( (Vector) this).size() == 0 )
		{
			try {
				wait();
			} catch ( Exception e )
			{
				Log.printStackTrace_mine ( 92, e ) ;
			}
		} 
		WorkResource vWorkResource = (WorkResource) ( (Vector) this).elementAt ( 0 );
		( (Vector) this).removeElementAt ( 0 );
		// Log.log (	"removing work unit, remaining [" + getNumWorkResourcesLeft() + "] \r\n", 
		// 								true);
		return vWorkResource;  // a vector of work objects, e.g. database connections
	}
	
	public synchronized int getNumWorkResourcesLeft()
	{
		return ( (Vector) this).size();
	}
	
	public synchronized void addReturnWorkResource ( WorkResource wRes_ )
	{
		( (Vector) this).addElement( wRes_ );
		notify();
	}

	public void closeResources()
	{
		Enumeration e = ((Vector) this).elements();
		while ( e.hasMoreElements() )
		{
			WorkResource wRes = ( WorkResource ) e.nextElement();	
			wRes.closeResources();
		} 
		
		
	} 
	
	
}
