package com.indraweb.workfactory;

import java.util.*;
import com.indraweb.execution.*;
import com.indraweb.util.*;

public class WorkerThread  extends Thread
{
	WorkTask			wTask		= null;  
	WorkResourcePool	wResPool	= null;
	WorkResource		wResource	= null;
	boolean				stopMe		= false;
	long				startTime	= -0;
	int					iThreadID	= -1;
	int					iThreadSlot = -1;
	
	public WorkerThread ( WorkTask wTask_, 
						  WorkResourcePool wResPool_, 
						  int iThreadID_, 
						  int iThreadSlot_ )
	{
		wTask		= wTask_;
		wResPool	= wResPool_;
		iThreadID	= iThreadID_;
		iThreadSlot	= iThreadSlot_;
	} 

	// constructor public WorkerThread ( WorkTask wu, WorkProgressThisThread wptt );
	void makeMeStopNoMatterWhatImDoing ()
	{
		stopMe = true;
	}
	
	public WorkTask getWorkTask()
	{
		return wTask	;
	} 
	
	public void run ()
	{
		try 
		{
			ThreadInfo.addThreadToRunningList ( "Thrd [" + iThreadSlot + "." + iThreadID + "]" );
			startTime = System.currentTimeMillis();
			wResource = wResPool.getWorkResource();
			wTask.doMyWorkSynchronous ( wResource );
			//Log.log ( "WorkerThread.run() done call to wTask.doMyWorkSynchronous \r\n");
			wTask.setState_completed( WorkTask.wuState_COMPLETE_SUCCESS ) ;
		} 
		catch ( Exception e )
		{
			Log.printStackTrace_mine (91, e);	
			wTask.setState_completed( WorkTask.wuState_COMPLETE_FAILED ) ;
		} 
		finally 
		{
			wResPool.addReturnWorkResource ( wResource );
			// Log.log ( "WorkerThread.run() done call to wResPool.addReturnWorkResource\r\n");
		} 
	} 
} 
// --------------------------------------------------------

	
	



