package com.indraweb.encyclopedia;

/**
 * Class DocForScore_Min - A doc representation for scoring Language & Environments: JRE 1.1, 1.2
 *
 * @author hkon
 * @version 1.0
 * @since November 4, 2001
 * @see	Topic History: November 4, 2001 Created
 */
import com.indraweb.ir.IRStringHandlers;
import com.indraweb.ir.Thesaurus;
import com.indraweb.ir.PhraseParms;
import com.indraweb.ir.ParseParms;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import com.indraweb.util.UtilTitleAndSigStringHandlers;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.execution.Session;
import com.indraweb.webelements.IndraURL;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.sql.Connection;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 * DocForScore contains information pertaining to a document and its metadata. This is used by classifiers in generating node-document scores. <p> Data contained includes Document Title, Document Size
 * (unique word count), Terms (words and phrases) and Weights, and Document Summary.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 */
public class DocForScore extends TextObject {
    // corpus doc true vs web or other doc - corpus docs have signatures
    // URL for a db insert

    private static final Logger log = Logger.getLogger(DocForScore.class);
    private static final ResourceBundle rb = ResourceBundle.getBundle("system/configuration");
    private String URLRemoteForDBInsert = null;
    // title
    private String sDocTitle = null;
    private String sDocTitleStemmed = null;
    // words
    private Hashtable htWordsNCount = null;
    private Hashtable htWordsNCount_stemmed = null;
    private StringAndCount[] scarr = null;
    private StringAndCount[] scarrStemmed = null;
    public boolean bFileOpenedOk = false;
    public int iNumBytesOfDocsToUse = -1;
    private Topic topic = null;
    private Hashtable htCorpusID_to_TitleAndBodyStringThesExpanders = new Hashtable();
    private Hashtable htCorpusID_to_htWordCountFromThesExpansions = new Hashtable();
    // summary if preset by api call from user
    private String sDocSummary = null;
    Vector vIntegerSEsFindingThisURL = null;
    public int iWeight = -1; // word count multiuplier for doc blending
    private Vector vDFSBlendedDocs = null; // word count multiuplier for doc blending
    private int iDocID = -1; // set just pre scoring by TSClassify
    private ParseParms parseparms = null; // set just pre scoring by TSClassify
    private final Boolean BSynchStringStem = false;
    private String sDocWebURL;
    private Hashtable htCache_strCorpusSet_to_vecOfWordsWithTitleWordsFirst_ThesExpandedMaybe = new Hashtable();

    /**
     *
     * @param sURL_
     * @param URLRemoteForDBInsert
     * @param fullWebURL
     * @param bFatalIfCantOpenDoc
     * @param bStemmedAlso
     * @param sDocTitleToUse
     * @param sProp_DocSummary
     * @param bWantPhrases
     * @param iURL
     * @param bTakeFirstLineAsTitleIfNoTitleTag
     * @param bVerbose
     * @param iWeight_
     * @param parseparms
     * @param sText
     * <p/>
     * @throws Exception
     */
    public DocForScore(String sURL_,
            String URLRemoteForDBInsert,
            String fullWebURL,
            boolean bFatalIfCantOpenDoc,
            boolean bStemmedAlso,
            String sDocTitleToUse,
            String sProp_DocSummary,
            boolean bWantPhrases,
            IndraURL iURL,
            boolean bTakeFirstLineAsTitleIfNoTitleTag,
            boolean bVerbose,
            int iWeight_,
            ParseParms parseparms,
            String sText)
            throws Exception {
        log.debug("DocForScore(Constructor)");
        if (iURL != null) {
            // will be null in classify mode, not in metasearch
            vIntegerSEsFindingThisURL = iURL.getVecIntegerSEsFindingThisURL();
        }
        this.sDocTitle = sDocTitleToUse;
        this.sDocSummary = sProp_DocSummary;
        this.iWeight = iWeight_;
        this.URLRemoteForDBInsert = URLRemoteForDBInsert;
        this.parseparms = parseparms;
        sDocWebURL = fullWebURL;
        sURL = sURL_;

        try {
            if (log.isDebugEnabled()) {
                log.debug("DocTitle: " + sDocTitleToUse);
                log.debug("DocSummary: " + sProp_DocSummary);
                log.debug("Weight: " + iWeight);
                log.debug("URLRemoteForDBInsert: " + URLRemoteForDBInsert);
                log.debug("sURL: " + sURL);
            }
            try {
                if (!sURL.toLowerCase().startsWith("file:") && !sURL.toLowerCase().startsWith("http:")) {
                    log.debug("File does not have the prefix http or file");
                    sURL = "file:///" + rb.getString("system.classificationfiles.location") + sURL;
                    log.debug("Adding prefix to sURL: " + sURL);
                }

                UrlHtmlAttributes uh = new UrlHtmlAttributes(sURL, sText);
                topic = new Topic(
                        uh,
                        true,
                        null, // fix this
                        iURL,
                        false,
                        // phrase scoring false,
                        10000,
                        true,
                        bWantPhrases,
                        bTakeFirstLineAsTitleIfNoTitleTag,
                        false,
                        iWeight,
                        sDocTitleToUse,
                        null, // text not known in adcvance
                        true,
                        true,
                        new PhraseParms(),
                        parseparms,
                        -1,
                        null, // wordfreq not needed
                        null);

                if (topic == null) {
                    log.warn(" topic == null in DFS url [" + uh.getsURL() + "]");
                }
            } catch (Throwable t) {
                throw new Exception(Thread.currentThread().getName() + "throwable from getATopicFromMinInputs 2 in DocForScore " + " errmsg [" + t.getMessage() + "]");
            }

            bFileOpenedOk = true;
            htWordsNCount = topic.htWordsDocOnly_noDescendants;
            //int iTopicUniqueWordCount = topic.getTopicUniqueWordCount();
            if (htWordsNCount.isEmpty()) {
                throw new Exception("unable to open file, or otherwise: unique word count is 0 in doc [" + sURL + "]");
            }

            // POST PARSING HERE - COPY NON STEMMED INTO STEMMED
            if (bStemmedAlso) {
                htWordsNCount_stemmed = com.indraweb.util.stem.UtilStem.getStemmed_ht(htWordsNCount);
            }
            if (sDocTitle == null) {
                sDocTitle = topic.m_DocumentTitle;
            }

            // GET DOC DocSummary (not NodeDoc)
            if (topic.sbAllTextInOne != null && topic.sbAllTextInOne.length() > 0) {
                String sFirstX00CharsInDoc = topic.sbAllTextInOne.substring(0, com.indraweb.util.UtilSets.min(topic.sbAllTextInOne.length(), 300));
                // GET TITLE FROM DOC TXT IF NECESSARY
                if (sDocTitle == null || sDocTitle.equals("")) {
                    int iIndexNThSpace = com.indraweb.util.UtilStrings.getLocationOfNthOfThese(sFirstX00CharsInDoc, " ", 6);
                    if (iIndexNThSpace == -1) {
                        sDocTitle = sFirstX00CharsInDoc.substring(0, com.indraweb.util.UtilSets.min(40, sFirstX00CharsInDoc.length() - 1));
                    } else {
                        sDocTitle = sFirstX00CharsInDoc.substring(0, iIndexNThSpace);
                    }
                    sDocTitle = com.indraweb.util.UtilStrings.cleanStringWhiteSpaceToSpaces(sDocTitle, true);
                    if (sDocSummary == null || sDocSummary.equals("")) {
                        sDocSummary = sFirstX00CharsInDoc.substring(0, com.indraweb.util.UtilSets.min(180, sFirstX00CharsInDoc.length() - 1));
                    }
                } else {
                    if (sDocSummary == null || sDocSummary.equals("")) {
                        sDocSummary = sFirstX00CharsInDoc + "...";
                    }
                }
                sDocSummary = com.indraweb.util.UtilStrings.cleanStringWhiteSpaceToSpaces(sDocSummary, true);
                if (sDocSummary.startsWith(sDocTitle)) {
                    sDocSummary = sDocSummary.substring(sDocTitle.length()).trim();
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
            if (bFatalIfCantOpenDoc) {
                log.error(Thread.currentThread().getName() + " DocForScore constructor can't open doc URLRemoteForDBInsert [" + URLRemoteForDBInsert + "] sURL_ [" + sURL_ + "]\r\n");
            } else {
                log.warn(Thread.currentThread().getName() + " DocForScore constructor can't open doc URLRemoteForDBInsert [" + URLRemoteForDBInsert + "] sURL_ [" + sURL_ + "]\r\n");
            }
        } finally {
            vDFSBlendedDocs = new Vector();
            vDFSBlendedDocs.addElement(this);
        }
    }

    /**
     *
     * @param sURL_
     * @param fullWebURL
     * @param bFatalIfCantOpenDoc
     * @param bStemmedAlso
     * @param sDocTitleToUse
     * @param sProp_DocSummary
     * @param bWantPhrases
     * @param iURL
     * @param bTakeFirstLineAsTitleIfNoTitleTag
     * @param bVerbose
     * @param iWeight_
     * @param parseparms
     * @param sText
     * <p/>
     * @throws Exception
     */
    public DocForScore(String sURL_,
            String fullWebURL,
            boolean bFatalIfCantOpenDoc,
            boolean bStemmedAlso,
            String sDocTitleToUse,
            String sProp_DocSummary,
            boolean bWantPhrases,
            IndraURL iURL,
            boolean bTakeFirstLineAsTitleIfNoTitleTag,
            boolean bVerbose,
            int iWeight_,
            ParseParms parseparms,
            String sText)
            throws Exception {

        if (iURL != null) {
            // will be null in classify mode, not in metasearch
            vIntegerSEsFindingThisURL = iURL.getVecIntegerSEsFindingThisURL();
        }
        this.sDocTitle = sDocTitleToUse;
        this.sDocSummary = sProp_DocSummary;
        this.iWeight = iWeight_;
        sURL = sURL_;
        this.parseparms = parseparms;
        sDocWebURL = fullWebURL;

        try {
            try {
                if (!sURL.toLowerCase().startsWith("file:") && !sURL.toLowerCase().startsWith("http:")) {
                    sURL = "file:///" + Session.sIndraHome + sURL;
                }

                UrlHtmlAttributes uh = new UrlHtmlAttributes(sURL, sText);
                topic = new Topic(
                        uh,
                        true,
                        null, // fix this
                        iURL,
                        false,
                        // phrase scoring false,
                        10000,
                        true,
                        bWantPhrases,
                        bTakeFirstLineAsTitleIfNoTitleTag,
                        false,
                        iWeight,
                        sDocTitleToUse,
                        null, // text not known in adcvance
                        true,
                        true,
                        new PhraseParms(),
                        parseparms,
                        -1,
                        null, // wordfreq not needed
                        null);
                if (topic == null) {
                    log.warn(" topic == null in DFS url [" + uh.getsURL() + "]");
                }
            } catch (Throwable t) {
                throw new Exception(Thread.currentThread().getName() + "throwable from getATopicFromMinInputs 2 in DocForScore errmsg [" + t.getMessage() + "]");
            }

            bFileOpenedOk = true;
            htWordsNCount = topic.htWordsDocOnly_noDescendants;
            if (htWordsNCount.isEmpty()) {
                throw new Exception("unable to open file, or otherwise: unique word count is 0 in doc [" + sURL + "]");
            }

            // POST PARSING HERE - COPY NON STEMMED INTO STEMMED
            if (bStemmedAlso) {
                htWordsNCount_stemmed = com.indraweb.util.stem.UtilStem.getStemmed_ht(htWordsNCount);
            }
            if (sDocTitle == null) {
                sDocTitle = topic.m_DocumentTitle;
            }

            // GET DOC DocSummary (not NodeDoc)
            if (topic.sbAllTextInOne != null && topic.sbAllTextInOne.length() > 0) {
                String sFirstX00CharsInDoc = topic.sbAllTextInOne.substring(0, com.indraweb.util.UtilSets.min(topic.sbAllTextInOne.length(), 300));
                // GET TITLE FROM DOC TXT IF NECESSARY
                if (sDocTitle == null || sDocTitle.equals("")) {
                    int iIndexNThSpace = com.indraweb.util.UtilStrings.getLocationOfNthOfThese(sFirstX00CharsInDoc, " ", 6);
                    if (iIndexNThSpace == -1) {
                        sDocTitle = sFirstX00CharsInDoc.substring(0, com.indraweb.util.UtilSets.min(40, sFirstX00CharsInDoc.length() - 1));
                    } else {
                        sDocTitle = sFirstX00CharsInDoc.substring(0, iIndexNThSpace);
                    }
                    sDocTitle = com.indraweb.util.UtilStrings.cleanStringWhiteSpaceToSpaces(sDocTitle, true);
                    if (sDocSummary == null || sDocSummary.equals("")) {
                        sDocSummary = sFirstX00CharsInDoc.substring(0, com.indraweb.util.UtilSets.min(180, sFirstX00CharsInDoc.length() - 1));
                    }
                } else {
                    if (sDocSummary == null || sDocSummary.equals("")) {
                        sDocSummary = sFirstX00CharsInDoc + "...";
                    }
                }
                sDocSummary = com.indraweb.util.UtilStrings.cleanStringWhiteSpaceToSpaces(sDocSummary, true);
                if (sDocSummary.startsWith(sDocTitle)) {
                    sDocSummary = sDocSummary.substring(sDocTitle.length()).trim();
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
            if (bFatalIfCantOpenDoc) {
                log.error(Thread.currentThread().getName() + " DocForScore constructor can't open doc URLRemoteForDBInsert [" + URLRemoteForDBInsert + "] sURL_ [" + sURL_ + "]\r\n");
            } else {
                log.warn(Thread.currentThread().getName() + " DocForScore constructor can't open doc URLRemoteForDBInsert [" + URLRemoteForDBInsert + "] sURL_ [" + sURL_ + "]\r\n");
            }
        } finally {
            vDFSBlendedDocs = new Vector();
            vDFSBlendedDocs.addElement(this);
        }
    }

    /**
     *
     * @return
     */
    public Topic getTopic() {
        return topic;
    }

    // ******************************************
    public StringAndCount[] getSCarr(boolean bStemmed)
            // ******************************************
            throws Exception {
        if (scarr == null) {
            synchronized (this) {
                if (scarr == null) // avoid race
                {
                    scarr = StringAndCount.convertHTWordCounts_ToSortedStrandCount(getHtWordsNCount(bStemmed), false);
                    if (bStemmed) {
                        scarrStemmed = com.indraweb.util.stem.UtilStem.getStemmed_SCArr(scarr, false);
                    }
                }
            }
        }

        if (bStemmed) {
            return scarrStemmed;
        } else {
            return scarr;
        }
    }

    // ******************************************
    public Hashtable getHtWordsNCount(boolean bStemmed) // ******************************************
    {
        if (!bStemmed) {
            if (htWordsNCount == null) {
                log.error("ht not iniitalized in DocForScore.java");
            }
            return htWordsNCount;
        } else {
            if (htWordsNCount_stemmed == null) {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    log.error("ht stemmed not iniitalized in DocForScore.java", e);
                }
            }
            return htWordsNCount_stemmed;

        }
    }

    /**
     *
     * @param bStemmed
     * <p/>
     * @return
     */
    public Enumeration getEnumWords_notThesExpanded(boolean bStemmed) {
        if (bStemmed) {
            return htWordsNCount_stemmed.keys();
        } else {
            return htWordsNCount.keys();
        }

    }

    // ******************************************
    // get terms into a vector for return
    // and also populate a few items up top :
    public Vector getEnumDocWordsWithTitleWordsFirst_populateThesExpansionTermsPerCorpusOrCorpusSet(int iMinOccurencesInclusive,
            boolean bStemmed,
            boolean bLower,
            Hashtable htCorpusIDsForThesaurusExpansion,
            Connection dbc) throws Exception {
        String sCacheString_htCorpusIDsRepresented = "none";
        if (htCorpusIDsForThesaurusExpansion != null) {
            sCacheString_htCorpusIDsRepresented = UtilStrings.convertHTKeysToString(htCorpusIDsForThesaurusExpansion, ",", true);
        }

        String sCacheKey = sCacheString_htCorpusIDsRepresented + ":" + iMinOccurencesInclusive + ":" + bStemmed + ":" + bLower;
        Vector vReturn = (Vector) htCache_strCorpusSet_to_vecOfWordsWithTitleWordsFirst_ThesExpandedMaybe.get(sCacheKey);

        if (vReturn == null) {
            vReturn = new Vector();


            // ***************
            // STEP 1 FIRST DO TITLE - add all delim split title words into the vReturn and
            // htTitleWordsAlreadyEmitted objects
            // ***************

            String sTitleMaybeStemmed = this.getTitle(bStemmed);
            Hashtable htTitleWordsAlreadyEmitted = new Hashtable();
            Vector vTitleWordsMaybeStemmed = UtilStrings.splitByStrLenLong(sTitleMaybeStemmed, parseparms.getDelimiters());

            // shared routine here and with title thesaurus get section
            accumulateWords(vTitleWordsMaybeStemmed, htTitleWordsAlreadyEmitted, vReturn, bLower, bStemmed);

            // ***************
            // STEP 2 optional NOW DO Doc TITLE THESAURUS TERMS IF DESIRED
            // ***************
            // title expansions also included in step 4 below - but here for
            if (htCorpusIDsForThesaurusExpansion != null) {
                String sTitleNotStemmedNonStopRemovedYesLowCase = getTitle(false);
                if (bStemmed) {
                    sTitleNotStemmedNonStopRemovedYesLowCase =
                            IRStringHandlers.convertNormalizeString_ElimStopStemOption(
                            sTitleNotStemmedNonStopRemovedYesLowCase,
                            parseparms.getDelimiters(),
                            false,
                            bStemmed);
                }

                Enumeration eCorpusIDsForThesaurusExpansion = htCorpusIDsForThesaurusExpansion.elements();
                while (eCorpusIDsForThesaurusExpansion.hasMoreElements()) {
                    Integer ICorpusID = (Integer) eCorpusIDsForThesaurusExpansion.nextElement();
                    Thesaurus thesaurusThisCorpus = Thesaurus.cacheGetThesaurus(bStemmed, ICorpusID.intValue(), true, dbc);
                    Vector vDocTitleExpansionAltrenativesThisCorpus = thesaurusThisCorpus.getMultiWordExpansions(sTitleNotStemmedNonStopRemovedYesLowCase);
                    if (vDocTitleExpansionAltrenativesThisCorpus != null) {
                        Enumeration eTitleExpansionAltrenativesThisCorpus = vDocTitleExpansionAltrenativesThisCorpus.elements();

                        while (eTitleExpansionAltrenativesThisCorpus.hasMoreElements()) {
                            String sTitleExpansionMaybeStemmed = (String) eTitleExpansionAltrenativesThisCorpus.nextElement();
                            vTitleWordsMaybeStemmed = UtilStrings.splitByStrLenLong(sTitleExpansionMaybeStemmed, parseparms.getDelimiters());
                            accumulateWords(vTitleWordsMaybeStemmed, htTitleWordsAlreadyEmitted,
                                    vReturn, bLower, bStemmed);
                        }
                    }
                }
            } // if want (title) thesaurus expansion

            // ***************
            // STEP 3 THEN DO BODY
            // ***************
            // get the appropriate ht of words to us
            Hashtable htStemmedOrNot = null;
            if (bStemmed) {
                htStemmedOrNot = htWordsNCount_stemmed;
            } else {
                htStemmedOrNot = htWordsNCount;
            }

            Enumeration e = htStemmedOrNot.keys();
            while (e.hasMoreElements()) {
                String sWord = (String) e.nextElement();
                Integer ICount = (Integer) htStemmedOrNot.get(sWord);

                if (ICount.intValue() >= iMinOccurencesInclusive) {
                    if (sWord == null || sWord.trim().equals("")) {
                        continue;
                    }
                    if (bLower) {
                        sWord = sWord.toLowerCase();
                    }

                    if (htTitleWordsAlreadyEmitted.get(sWord) == null) {
                        vReturn.addElement(sWord);
                    }
                }
            }

            // ***************
            // STEP 4 optional NOW DO BODY THESAURUS TERMS IF DESIRED
            // ***************
            if (htCorpusIDsForThesaurusExpansion != null) {
                String sBody = topic.sbAllTextInOne.toString(); // (NotStemmedNonStopRemovedYesLowCase??)
                sBody = IRStringHandlers.convertNormalizeString_ElimStopStemOption(
                        sBody,
                        parseparms.getDelimiters(),
                        false,
                        bStemmed);

                // thesaurus words are not yet restricted to min count rule
                Enumeration eCorpusIDsForThesaurusExpansion = htCorpusIDsForThesaurusExpansion.elements();
                while (eCorpusIDsForThesaurusExpansion.hasMoreElements()) {
                    Integer ICorpusID = (Integer) eCorpusIDsForThesaurusExpansion.nextElement();
                    Thesaurus thes = Thesaurus.cacheGetThesaurus(bStemmed, ICorpusID.intValue(),
                            true, null);
                    Vector vThesTermsFromBodyExpansionThisCorpus = thes.getMultiWordExpansions(sBody);
                    // cumulate the thesaurus content into the doc main content by corpusid


                    Vector vWordsOneExpansion = null;
                    StringBuilder sbCacheByCorpus_ThesExpansText = new StringBuilder();
                    Hashtable htWordCountFromThesExpansionsThisCorpus = new Hashtable();
                    if (vThesTermsFromBodyExpansionThisCorpus != null) {
                        Enumeration eThesTermsFromBodyExpansionThisCorpus = vThesTermsFromBodyExpansionThisCorpus.elements();

                        boolean bAny = false;
                        while (eThesTermsFromBodyExpansionThisCorpus.hasMoreElements()) {
                            bAny = true;
                            String sBodyOneExpansion_MaybeStemmed = (String) eThesTermsFromBodyExpansionThisCorpus.nextElement();
                            vWordsOneExpansion = UtilStrings.splitByStrLenLong(
                                    sBodyOneExpansion_MaybeStemmed, parseparms.getDelimiters());

                            // INCR THESAURUS TERMS
                            UtilSets.hash_increment_count_for_VecOfstrings(htWordCountFromThesExpansionsThisCorpus, vWordsOneExpansion, 1, true, bStemmed);
                            //if ( bStemmed )
                            //  UtilSets.hash_increment_count_for_string(htWordsNCount_stemmed, sBodyOneExpansion_MaybeStemmed, 1);

                            //UtilSets.hash_increment_count_for_string(htWordsNCount, sBodyOneExpansion_MaybeStemmed, 1);

                            accumulateWords(vWordsOneExpansion, htTitleWordsAlreadyEmitted,
                                    vReturn, bLower, bStemmed);

                            // the "body" text above will include also the title words expanded
                            sbCacheByCorpus_ThesExpansText.append(sBodyOneExpansion_MaybeStemmed).append(" ");

                        }

                        if (bAny) {
                            htCorpusID_to_htWordCountFromThesExpansions.put(ICorpusID + ":" + bStemmed, htWordCountFromThesExpansionsThisCorpus);
                            htCorpusID_to_TitleAndBodyStringThesExpanders.put(ICorpusID + ":" + bStemmed, sbCacheByCorpus_ThesExpansText.toString());
                        }

                    }
                }
            } // if want (title) thesaurus expansion


            // CACHE RESULT
            htCache_strCorpusSet_to_vecOfWordsWithTitleWordsFirst_ThesExpandedMaybe.put(sCacheKey, vReturn);
        }
        return vReturn;
    } // public Enumeration getEnumDocWordsWithTitleWordsFirst_populateThesExpansionTermsPerCorpusOrCorpusSet ( int iMinOccurencesInclusive,

    private static void accumulateWords(Vector vTitleWordsMaybeStemmed,
            Hashtable htTitleWordsAlreadyEmitted,
            Vector vReturn,
            boolean bLower,
            boolean bStemmed) {
        Enumeration eTitleMaybeStemmed = vTitleWordsMaybeStemmed.elements();

        // automatic to include title words in the enum

        // remove stop words
        Hashtable htStop = null;
        if (bStemmed) {
            htStop = com.indraweb.execution.Session.stopList.getHTStopWordListStemmed();
        } else {
            htStop = com.indraweb.execution.Session.stopList.getHTStopWordList();
        }

        while (eTitleMaybeStemmed.hasMoreElements()) {
            String sTitlewordMaybeStemmed = (String) eTitleMaybeStemmed.nextElement();
            if (sTitlewordMaybeStemmed == null || sTitlewordMaybeStemmed.trim().equals("")) {
                continue;
            }

            if (bLower) {
                sTitlewordMaybeStemmed = sTitlewordMaybeStemmed.toLowerCase();
            }

            if (htStop.get(sTitlewordMaybeStemmed) == null) {
                if (htTitleWordsAlreadyEmitted.get(sTitlewordMaybeStemmed) == null) {
                    vReturn.addElement(sTitlewordMaybeStemmed);
                    htTitleWordsAlreadyEmitted.put(sTitlewordMaybeStemmed, sTitlewordMaybeStemmed);
                }
            }
        }
    }

    // ******************************************
    public String getTitle(boolean bStemOn) // ******************************************
    {
        String sDocTitleReturn = sDocTitle;
        if (bStemOn) {
            if (sDocTitleStemmed == null) {
                synchronized (BSynchStringStem) {
                    if (sDocTitleStemmed == null) {
                        sDocTitleStemmed = UtilTitleAndSigStringHandlers.convertTitleToTitleForTitleMatch(sDocTitle, bStemOn, parseparms.getDelimiters());
                    }
                }
            }
            sDocTitleReturn = sDocTitleStemmed;
        }

        return (sDocTitleReturn != null) ? sDocTitleReturn.trim() : "";
    }

    // ******************************************
    public String getURL() // ******************************************
    {
        return sURL;
    }

    // ******************************************
    public String getsThesaurusExpandedStrings(int iCorpusID, boolean bStemmed) // ******************************************
    {
        return (String) htCorpusID_to_TitleAndBodyStringThesExpanders.get(iCorpusID + ":" + bStemmed);
    }

    // ******************************************
    public Hashtable gethtWordCountThesExpansion(int iCorpusID, boolean bStemmed) // ******************************************
    {
        return (Hashtable) htCorpusID_to_htWordCountFromThesExpansions.get(iCorpusID + ":" + bStemmed);
    }

    public String getDocSumm() {
        return sDocSummary;
    }

    /**
     * Assumes the this (sic) document is already weighted - so for example if weights were 6 parts this to 5 parts incoming ... assumes docs already have the 6 and 5 weightings.
     */
    public void blendInAnotherDoc(DocForScore dfsBeingAdded,
            boolean bStemmed, // eg replace, concat, concat weighted etc.
            boolean bVerbose) {
        if (bVerbose) {
            printme("pre blend vDFSBlendedDocs.size() [" + vDFSBlendedDocs.size() + "] <- vDFSBlendedDocs.size() ");
        }
        if (bVerbose) {
            dfsBeingAdded.printme("dfsBeingAdded.printme # [" + vDFSBlendedDocs.size() + "] <- vDFSBlendedDocs.size() ");
        }
        // components to consider
        // 1 words in HT's
        // 2 total unique word counts (may never be materialized - may read out of ht each score - if so OK
        // 3 title info nothing yet (not in abstracts)
        // 4 phrasing - (phrase connect to stem)
        // 5 full text representation


        // 1 WORDS non stemmed first
        // getting non stemmed even if stemming on - simulates a real doc - may be not needed
/*
         * { // scoping safety ! Hashtable htWordsNCountDFSPassedIn = dfsBeingAdded.getHtWordsNCount(false); Enumeration enumeration = htWordsNCountDFSPassedIn.keys(); while (
         * enumeration.hasMoreElements() ) { String sWord = (String) enumeration.nextElement(); Integer ICount = (Integer) htWordsNCountDFSPassedIn.get(sWord); UtilSets.hash_increment_count_for_string
         * ( htWordsNCount, sWord, ICount.intValue() );
         *
         * }
         * }
         */

        // 1b WORDS stemmed if needed
        if (bStemmed) {
            Hashtable htWordsNCountDFSPassedIn_stem = dfsBeingAdded.getHtWordsNCount(bStemmed);
            Enumeration enumeration_Stem = htWordsNCountDFSPassedIn_stem.keys();
            while (enumeration_Stem.hasMoreElements()) {
                String sWord = (String) enumeration_Stem.nextElement();
                Integer ICount = (Integer) htWordsNCountDFSPassedIn_stem.get(sWord);
                UtilSets.hash_increment_count_for_string(htWordsNCount_stemmed, sWord, ICount.intValue());
            }
        }

        // 2 fulltext appendage
        vDFSBlendedDocs.addElement(dfsBeingAdded);
        if (bVerbose) {
            printme("post blend doc");
        }

    }
    DocForScore[] dfsArrBlended = null;

    public DocForScore[] getArrDFSBlendedDocs() {
        if (dfsArrBlended == null) {
            dfsArrBlended = new DocForScore[vDFSBlendedDocs.size()];
            vDFSBlendedDocs.copyInto(dfsArrBlended);
        }
        return dfsArrBlended;

    }

    public void printme(String sDesc) {
        StringBuilder sbhtWordsNCount = new StringBuilder();
        Enumeration enumhtWords = htWordsNCount.keys();
        int iKeyIdx = 0;
        while (enumhtWords.hasMoreElements()) {
            iKeyIdx++;
            String sKey = (String) enumhtWords.nextElement();
            Integer ICount = (Integer) htWordsNCount.get(sKey);
            sbhtWordsNCount.append("  ").append(iKeyIdx).append(". doc word [").append(sKey).append("] ICount [").append(ICount).append("]\r\n");
        }

        log.debug("-- DFS summary START [" + sDesc + "]");
        log.debug("   0 URLRemoteForDBInsert [" + URLRemoteForDBInsert + "]");
        log.debug("   1 vDFSBlendedDocs.size() [" + vDFSBlendedDocs.size() + "]");
        log.debug("   2 sDocTitle [" + sDocTitle + "]");
        log.debug("   2.5 iDocID [" + iDocID + "]");
        log.debug("   3 sDocTitleStemmed  [" + sDocTitleStemmed + "]");
        log.debug("   4 htWordsNCount.size() [" + htWordsNCount.size() + "]");
        log.debug("   4.5 htWordsNCount  elemnts() [" + sbhtWordsNCount + "]");
        //log.debug( ("   5 htWordsNCount_stemmed.size() [" + htWordsNCount_stemmed.size()  + "]" );
        //log.debug( ("   6 scarr.length [" + scarr.length + "]" );
        //log.debug( ("   7 scarrStemmed.length [" + scarrStemmed.length + "]" );
        log.debug("   8 htWordsNCount.size() [" + htWordsNCount.size() + "]");
        log.debug("   9 this.getDocWordsEffectivePostBlend() [" + this.getDocWordsEffectivePostBlend() + "]");
        log.debug("   10 iNumBytesOfDocsToUse [" + iNumBytesOfDocsToUse + "]");
        log.debug("   11 htCorpusID_to_TitleAndBodyStringThesExpanders.size() [" + htCorpusID_to_TitleAndBodyStringThesExpanders.size() + "]");
        log.debug("   12 htCorpusID_to_htWordCountFromThesExpansions.size() [" + htCorpusID_to_htWordCountFromThesExpansions.size() + "]");
        log.debug("   13 iWeight [" + iWeight + "]");
        log.debug("   14 sDocSummary  [" + sDocSummary + "]");
        log.debug("   15 topic.sbAllTextInOne.toString() [" + topic.sbAllTextInOne.toString() + "]");
        // log.debug( ("   16 topic.getTopicUniqueWordCount()  [" + topic.getTopicUniqueWordCount() + "]" );
        log.debug("   17 topic.htWordsDocOnly_noDescendants.size()   [" + topic.htWordsDocOnly_noDescendants.size() + "]");
        log.debug("-- DFS summary END [" + sDesc + "]");
    }

    public int getWeight() {
        return iWeight;
    }

    public void setDocID(int iDocID) {
        this.iDocID = iDocID;
    }

    public int getDocID() {
        return iDocID;
    }

    public int getDocWordsEffectivePostBlend() {
        boolean bVerbose = false; // hbk 2004 03 23
        int iNumWordsReturn = 0;
        DocForScore[] dfsArr = this.getArrDFSBlendedDocs();
        for (int iDfsIdx = 0; iDfsIdx < dfsArr.length; iDfsIdx++) {
            DocForScore dfs = dfsArr[iDfsIdx];
            if (dfs != null) {
                iNumWordsReturn += dfs.getWeight() * dfs.htWordsNCount.size();
                if (bVerbose) {
                    log.debug("calculate dfs.getDocWordsEffectivePostBlend "
                            + "   dfsArr.length [" + dfs.getArrDFSBlendedDocs().length + "] "
                            + "   iDfsIdx [" + iDfsIdx + "] "
                            + "   iNumWordsReturn accumulator [" + iNumWordsReturn + "] "
                            + "   dfs.getWeight() [" + dfs.getWeight() + "] "
                            + "   dfs.htWordsNCount.size() [" + dfs.htWordsNCount.size() + "] "
                            + "");
                }
            }
        }

        return iNumWordsReturn;
    }

    public ParseParms getParseParms() {
        return parseparms;
    }

    public String getsDocWebURL() {
        return sDocWebURL;
    }

    public void setsDocWebURL(String sDocWebURL) {
        this.sDocWebURL = sDocWebURL;
    }
}