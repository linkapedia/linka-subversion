package com.indraweb.encyclopedia;

/**
 * TextObject is the superclass that any text object in the system extends.
 * <p>
 * Extended by NodeForScore and DocForScore.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 */
public class TextObject
{
    protected String sURL = null;
    public String getURL()
    {
        return sURL ;
    }

}
