	package com.indraweb.ir;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.File;
import java.io.IOException;

import com.indraweb.util.*;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.ScoredObject;
import com.aliasi.chunk.ConfidenceChunker;
import com.aliasi.chunk.NBestChunker;
import com.aliasi.chunk.Chunk;
import com.aliasi.lm.TokenizedLM;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;

public class PhraseExtractor
{
	private static int iCallCounter = 0; // hbk control 
	// **********************************************************
	public static int buildPhrases (String s,
									 Hashtable htWordsDocOnly_noDescendants,
									  int iPhraseLenMin,
									  int iPhraseRepeatMinInclusive,
									  String sDelimiters,
									  int iMaxPhraseLen,
									  int iCountMultiplier,
                                     boolean bPhraseWordsCanStartWithNonLetters
) // since phrases getting squashed by single
	// **********************************************************
	{
        //api.Log.Log("Entering the phrase build segment for node with length "+s.length());

        // start with HK's phrase code
        // *************************************************************************************************
        iMaxPhraseLen = iMaxPhraseLen +1 ; // error - spec -
        iCallCounter++;
		// walk the array one word at a time creating a htWordToVecLocations of locations per word -
		// htWordToVecLocations contains for each word a vector of indexes where it occurs
		// then walk the array (maybe enum the HT is better) for a loop of phrase resolving ... 
		// keep track of which words done so far no need to do same word multiple times
		// don't consider phrases starting with stop words
		Hashtable htWordsToVecLocs = new Hashtable();
		//Hashtable htPunctuationCharacters = UtilStrings.gethtCharactersFromString( sPunctuation	);

        String phraseOptions = com.indraweb.execution.Session.corpusInstallOptions;

		Vector vStrings = com.indraweb.util.UtilStrings.splitByStrLenLong (s.toLowerCase(), sDelimiters );
		vStrings.addElement("dummyendword_elimTailConditions");
		String[] sArr = new String[vStrings.size()];
		vStrings.copyInto ( sArr );

        boolean bVerbose = com.indraweb.util.UtilFile.bFileExists ( "/temp/IndraLingpipeVerbose.txt" );

		// make a HT of words to vector of locations for fast access in resolving
		for ( int i = 0; i < sArr.length; i++ ) {
			Vector vWordLocs = (Vector) htWordsToVecLocs.get ( sArr[i] );
			if ( vWordLocs == null ) {
				vWordLocs = new Vector();	
				htWordsToVecLocs.put( sArr[i], vWordLocs );
			}
			vWordLocs.addElement (new Integer(i)); // keep next word for fast compare next time
		} 
		
		// OK have the hashtable of words to vec of integer locations
		// now walk each word to resolve all matching phrases
		Hashtable htWordLocsInPhrasesALready = new Hashtable(); // keeps a list of locs where a word is already in a phrase, avoids finding States of America as a Phrase when United States of America is the real phrase
		Hashtable htPhrasesAccumulateTotal = new Hashtable(); // keeps a list of locs where a word is already in a phrase, avoids finding States of America as a Phrase when United States of America is the real phrase
		
		int iEnumIndex = -1;
		Enumeration e = vStrings.elements();
		while ( e.hasMoreElements() )	
		{
			iEnumIndex++;
			String sWord = (String) e.nextElement();

			boolean b1 = ( com.indraweb.execution.Session.stopList.getHTStopWordList().get ( sWord.toLowerCase() ) == null );
			boolean b2 = ( htWordLocsInPhrasesALready.get ( new Integer (iEnumIndex) ) == null );
				
			if ( b1 && b2 ) 
			{
				Vector vWordLocs = (Vector) htWordsToVecLocs.get(sWord);

				if ( vWordLocs.size() >= iPhraseRepeatMinInclusive )
				{ // no point checking out a phrase occuring only once
					int[] iArrWordLocs = com.indraweb.util.UtilSets.convertVectorTo_intArray ( vWordLocs );
					//not certain yet : for ( int iArrIndex = 0; iArrIndex < iArrWordLocs.length; iArrIndex++ )
					{
						//if ( htWordLocsInPhrasesALready.get ( new Integer (iArrWordLocs[iArrIndex])) == null)
						resolvePhrasesStartingThisWord
                            (
								iEnumIndex, //note sure yet : iArrWordLocs[iArrIndex], 
								iArrWordLocs, 
								sArr,
								htPhrasesAccumulateTotal,
								htWordLocsInPhrasesALready,
								iMaxPhraseLen,
								iPhraseLenMin,
								iCountMultiplier,
								bPhraseWordsCanStartWithNonLetters
                            );
						//htPhraseStartWordsDonealready.put ( sWord, sWord );
					} 
				} 
			}
		}

        // done - accumulate into relevant hastables
		Enumeration e2 = htPhrasesAccumulateTotal.keys	();
		while ( e2.hasMoreElements() )
		{
			String sPhrase = (String) e2.nextElement();
			int iPhraseCount = ((Integer) htPhrasesAccumulateTotal.get ( sPhrase )).intValue();
			//Log.logClear ("testing phrase [" + sPhrase + "] count [" + iPhraseCount + "] LT min [" + iPhraseRepeatMinInclusive * iCountMultiplier + "]\r\n");
			if ( iPhraseCount < iPhraseRepeatMinInclusive * iCountMultiplier )
				htPhrasesAccumulateTotal.remove (sPhrase);
			else
			{
                if (phraseOptions.substring(0, 1).equals("1")) {
                    //api.Log.Log("phrase option (A) - Dr. Kon algorithm - is set");
                    UtilSets.hash_increment_count_for_string (htWordsDocOnly_noDescendants, sPhrase , iPhraseCount );  // MULTIPLIER ALREADY APPLIED
                }
			}
		} // build phrases

        //api.Log.Log("After HK phrase extraction, total phrases: "+htPhrasesAccumulateTotal.size());

        // ******************************************************************************

        if (phraseOptions.substring(1, 2).equals("1")) {
            //api.Log.Log("phrase option (B) - Lingpipe - is set");

            // now use the Lingpipe phrase technology
            File modelFile = new File("C:\\Program Files\\ITS\\models\\ne-en-news-muc6.AbstractCharLmRescoringChunker");

            Object o = null;
            try { o = AbstractExternalizable.readObject(modelFile); }
            catch (ClassNotFoundException cnfe) { api.Log.LogError("Could not instantiate the lingpipe class", cnfe); }
            catch (IOException io) { api.Log.LogError("Cannot open model file "+modelFile.getAbsolutePath(), io); }

            if (o != null) {
                ConfidenceChunker chunker = (ConfidenceChunker) o;
                //NBestChunker chunker2 = (NBestChunker) o;

                char[] cs = s.toCharArray();
                Iterator it = chunker.nBestChunks(cs, 0, cs.length, 50);
                //Iterator it2 = chunker2.nBest(cs,0,cs.length,50);

                for (int j = 0; it.hasNext(); ++j) {
                    Chunk chunk = (Chunk) it.next();
                    double conf = Math.pow(2.0, chunk.score());
                    int start = chunk.start();
                    int end = chunk.end();
                    String phrase = clean(s.substring(start, end));
                    phrase = phrase.trim();

                    boolean bCommonWord = true;
                    Pattern pat = Pattern.compile("(\\w+)");
                    Matcher mat = pat.matcher(phrase);
                    while (mat.find()) {
                        if (bCommonWord)
                            bCommonWord = com.indraweb.execution.Session.stopList.getHTStopWordList().containsKey ( mat.group().toLowerCase() );
                        // if (commonword(mat.group()) { }
                    }

                    if (meetsCriteria(phrase) && (!bCommonWord) && (!htPhrasesAccumulateTotal.containsKey(phrase.toLowerCase().trim()))) {
                        htPhrasesAccumulateTotal.put(phrase.toLowerCase().trim(), new Integer(2));
                        UtilSets.hash_increment_count_for_string (htWordsDocOnly_noDescendants, phrase.toLowerCase().trim() , 1);
                        if (bVerbose)
                            com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputLingpipe.txt", "\r\n\r\n" +
                                new java.util.Date() + "\r\n (Best Chunks) phrase: "+phrase.toLowerCase().trim()+" score: "+chunk.score());
                    } else {
                        if (false)
                            com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputLingpipe.txt", "\r\n\r\n" +
                                new java.util.Date() + "\r\n ** DID NOT MEET CRITERIA ** (Best Chunks) phrase: "+phrase.toLowerCase()+" score: "+chunk.score());
                    }
                }
            }
        }

        // ************ capitalization phrase routine

        if (phraseOptions.substring(2, 3).equals("1")) {
            //api.Log.Log("phrase option (C) - Lingpipe Capitalization- is set");

            IndoEuropeanTokenizerFactory tokenizerFactory = new IndoEuropeanTokenizerFactory();
            TokenizedLM backgroundModel = new TokenizedLM(tokenizerFactory, 3);
            backgroundModel.train(s);

            ScoredObject[] coll = backgroundModel.collocations(3, 1, 100);
            for (int i = 0; i < coll.length; ++i) {
                double score = coll[i].score();
                String[] toks = (String[]) coll[i].getObject();
                String phrase = clean(report_filter(toks));
                phrase = phrase.trim();

                boolean bCommonWord = true;
                Pattern pat = Pattern.compile("(\\w+)");
                Matcher mat = pat.matcher(phrase);
                while (mat.find()) {
                    if (bCommonWord)
                        bCommonWord = com.indraweb.execution.Session.stopList.getHTStopWordList().containsKey ( mat.group().toLowerCase() );
                    // if (commonword(mat.group()) { }
                }

                if ((meetsCriteria(phrase)) && (!bCommonWord) && (!htPhrasesAccumulateTotal.containsKey(phrase.toLowerCase().trim()))) {
                    htPhrasesAccumulateTotal.put(phrase.toLowerCase().trim(), new Integer(2));
                    UtilSets.hash_increment_count_for_string (htWordsDocOnly_noDescendants, phrase.toLowerCase().trim() , 1);
                    if (bVerbose)
                            com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputLingpipe.txt", "\r\n\r\n" +
                                new java.util.Date() + "\r\n (Capitalization) phrase: "+phrase.toLowerCase().trim()+" score: "+score);
                } else {
                    if (false)
                            com.indraweb.util.UtilFile.addLineToFile("/temp/IndraOutputLingpipe.txt", "\r\n\r\n" +
                                new java.util.Date() + "\r\n ** DID NOT MEET CRITERIA ** (Capitalization) phrase: "+phrase.toLowerCase()+" score: "+score);
                }
            }

            tokenizerFactory = null;
            backgroundModel = null;
            coll = null;
        }

        //api.Log.Log("After HK + LINGPIPE phrase extraction, total phrases: "+htPhrasesAccumulateTotal.size());

        return htPhrasesAccumulateTotal.size();
	}

    private static String clean (String phrase) {
        phrase = phrase.toLowerCase().trim();
        phrase = phrase.replaceAll("the ", "");
        phrase = phrase.replaceAll("^an ", "");
        phrase = phrase.replaceAll("^and ", "");
        phrase = phrase.replaceAll("^a ", "");
        phrase = phrase.replaceAll("^of ", "");
        //phrase = phrase.replaceAll("\\(", "");
        //phrase = phrase.replaceAll("\\)", "");
        //phrase = phrase.replaceAll("  ", " ");
        if (phrase.endsWith(".")) phrase = phrase.substring(0, phrase.length()-1);

        String rphrase = "";
        char letters[] = phrase.toCharArray();
        for (int i = 0; i < letters.length; i++) {
            if ((int) letters[i] >= 32 && (int) letters[i] <= 122) rphrase = rphrase + letters[i];
        }
        return rphrase;
    }

    private static boolean meetsCriteria(String phrase) {
        phrase = phrase.toLowerCase();

        // if the phrase contains a period that is not preceded by a single character, return false
        if (phrase.indexOf(".") != -1) {
            String[] words = phrase.split("\\.");

            for (int i = 0; i < words.length-1; i++) {
                String word = words[i];
                String[] morewords = word.split(" ");

                String lastword = morewords[((morewords.length)-1)];
                if (lastword.length() > 2) return false;
            }
        }

        // phrase must contain at least one letter
        Pattern pat = Pattern.compile("[a-z]");
        Matcher mat = pat.matcher(phrase);
        if (!mat.find()) return false;

        // phrase may not start with a hyphen
        if (phrase.startsWith("-")) return false;

        // phrase may not contain a semicolon
        if (phrase.indexOf(";") != -1) return false;

        // phrase must contain at least once space
        if (phrase.indexOf(" ") == -1) return false;

        pat = null; mat = null; phrase = null;

        return true;
    }

	private static String report_filter(String[] toks) {
        String accum = "";
        for (int j = 0; j < toks.length; ++j) {
            if (nonCapWord(toks[j])) return "";
            accum += " " + toks[j];
        }
        return accum;
    }

    private static boolean nonCapWord(String tok) {
        if (!Character.isUpperCase(tok.charAt(0)))
            return true;
        for (int i = 1; i < tok.length(); ++i)
            if (!Character.isLowerCase(tok.charAt(i)))
                return true;
        return false;
    }
	// resolve the phrases given the original string, a set of locations where some single word matches
	// issues :
	// 1)	word matces occuring within prior phrases must be thrown out, e.g., don't find both
	//		united states of america and states of americal as phrases
	// 2)	what if two phrases descend from the same word ... united states of ... and united front ...

	// returns the set of sets locations where matches occur

	//private static int found = 0; // hbk control
	//private static int icall = 0; // hbk control
	private static Hashtable[] resolvePhrasesStartingThisWord (
										 int isArrIndexAnchor,
										 int[] iArrWordLocs, // locations of the start word of interest
										 String[] sArr,
										 Hashtable htPhrasesAccumulateTotal,
										 Hashtable htWordLocsInPhrasesALready,
										 int iMaxPhraseLen,
										 int iMinPhraseLen,
										 int iCountMultiplier,
										 boolean bbPhraseWordsCanStartWithNonLetters
										 )
	{
		int iNumPhrases = 0;
		// for each replicated occurence of a string see how long the word sequence matches are
		// check word two and see if same etc.
		// set up hashtables to contain for each iWordsOutIndex size ...
		//Log.logClear ( "in resolvePhrasesStartingThisWord " + isArrIndexAnchor + ". start word [" + sArr [ isArrIndexAnchor ] + "]\r\n" );
		//for ( int i= 0; i < iArrWordLocs.length; i++ )
			//Log.logClear ( "word occurs at loc [" + iArrWordLocs[i] + "]\r\n" );

        //api.Log.Log (icall++ + ". ");
		Hashtable[] htArrPerWordsOutIndexSetIntLocVecMatches = new Hashtable[iMaxPhraseLen];
		for ( int i = 0; i < iMaxPhraseLen; i++ )
		{
			htArrPerWordsOutIndexSetIntLocVecMatches[i] = new Hashtable();
		}
		// maybe want to start at end of sarr here ?
 		for ( int jArrWordLocsIndex = 0; jArrWordLocsIndex < iArrWordLocs.length; jArrWordLocsIndex++ )
		{
			if ( isArrIndexAnchor != iArrWordLocs [ jArrWordLocsIndex ] )
			{
				for ( int iWordsOutIndex = 1;
					  iArrWordLocs [ jArrWordLocsIndex ] + iWordsOutIndex < sArr.length	 &&
					  isArrIndexAnchor + iWordsOutIndex < sArr.length &&
						iWordsOutIndex <= iMaxPhraseLen ;
					  iWordsOutIndex++)
				{
					String sWord1 = sArr [ isArrIndexAnchor + iWordsOutIndex ];
					String sWord2 = sArr [ iArrWordLocs [ jArrWordLocsIndex ] + iWordsOutIndex ];
					if ( !sWord1.equals (sWord2 ) || iWordsOutIndex == iMaxPhraseLen -1)
					{
						iWordsOutIndex = iWordsOutIndex -1;
						///counter++;
						//elim trailing stop words
						int iLocLastNonStopInCommon = -1;
						// walk the list and compare
						for ( iLocLastNonStopInCommon = isArrIndexAnchor + iWordsOutIndex ;
							  iLocLastNonStopInCommon >= isArrIndexAnchor;
							  iLocLastNonStopInCommon--)
						{
							String sWordAtLoc = sArr [ iLocLastNonStopInCommon ];
							Hashtable htStops = com.indraweb.execution.Session.stopList.getHTStopWordList();
							if ( htStops.get ( sWordAtLoc.toLowerCase() ) == null )
								break;
						}
						iWordsOutIndex = iLocLastNonStopInCommon - isArrIndexAnchor + 1; // don't want tail/trailing stop words

						// HAVE A PHRASE
						if
							( ( iWordsOutIndex >= iMinPhraseLen )
							 &&
							  phraseQualifies ( sArr,
												isArrIndexAnchor,
												isArrIndexAnchor + iWordsOutIndex,
												bbPhraseWordsCanStartWithNonLetters) )
							 // if "phrase len" > 1
						{
							Integer JArrWordLocsIndex = new Integer ( iArrWordLocs[ jArrWordLocsIndex] );
							htArrPerWordsOutIndexSetIntLocVecMatches[iWordsOutIndex].put ( JArrWordLocsIndex,
																						   JArrWordLocsIndex );

							String[] sArrPhrase = (String[]) com.indraweb.util.UtilSets.getStrArraySlice ( sArr,
																		  isArrIndexAnchor,
																		  isArrIndexAnchor + iWordsOutIndex  );

							String sPhrase = com.indraweb.util.UtilStrings.convertSArrToString ( sArrPhrase, " " );
							int iPreCount = UtilSets.hash_get_count_for_string (htPhrasesAccumulateTotal, sPhrase );


							//Log.logClear ( "counter [" + counter + "] found [" + found + "] count [" + (1+iPreCount) + "] th occurofphrase " +
											 //" at locs [" + isArrIndexAnchor + "] to [" + (isArrIndexAnchor+iWordsOutIndex) + "] " +
											 //" and locs [" + iArrWordLocs [ jArrWordLocsIndex ] + "] to [" + (iArrWordLocs [ jArrWordLocsIndex ]+iWordsOutIndex) + "] " +
											 //"[" + sPhrase + "]\r\n" );
							// be sure to count the first time / anchor
							if ( iPreCount == 0 )
								com.indraweb.util.UtilSets.hash_increment_count_for_string ( htPhrasesAccumulateTotal, sPhrase, iCountMultiplier );

                            //api.Log.Log ("keeping /bumping count on phrase [" + sPhrase + "]" );
							com.indraweb.util.UtilSets.hash_increment_count_for_string ( htPhrasesAccumulateTotal, sPhrase, iCountMultiplier );
							// mark all words within SOURCE phrase as done - found a phrase for
							for ( int iWordLocInPhrase = isArrIndexAnchor; iWordLocInPhrase < isArrIndexAnchor + iWordsOutIndex; iWordLocInPhrase++ )
								htWordLocsInPhrasesALready.put ( new Integer ( iWordLocInPhrase ), new Integer ( iWordLocInPhrase ) );
							// mark all words within TARGET phrase as done - found a phrase for
							for ( int iWordLocInPhrase = iArrWordLocs [ jArrWordLocsIndex ]; iWordLocInPhrase < (iArrWordLocs [ jArrWordLocsIndex ] + iWordsOutIndex); iWordLocInPhrase++ )
								htWordLocsInPhrasesALready.put ( new Integer ( iWordLocInPhrase ), new Integer ( iWordLocInPhrase ) );

							iNumPhrases++;
						}
						break;  // found longest word match > 1 this start point
					}
				} // for words out
			} // if not at the anchor word location
		} // for word location index
		//if (
		// I need a set of locations where there is a phrase match of X words
		// maybe keep a vector of hashtables - vector index represents number of words
		// matching - hashtables contgain starting locations of matches of that length
		// maybe keep it redundant at first and in a consolidation sweep starting
		//	at vector end sweep backwards and throw away all shorter phrase locations if they've already been found.

		// ISSUE: how to tell when we've already covered a spot for phrases ?
		//

		//System.out.println ( "found [" + iNumPhrases + "] phrases" );
		return htArrPerWordsOutIndexSetIntLocVecMatches;
	}

	public static String cleanPhrase ( String sPhrase, HashSet hsPunctChars )
	{

//        if ( sPhrase.indexOf("torq") >= 0 )
//            api.Log.Log ("clean phrase [" + sPhrase + "]" );
		// FIRST CLEAN TRAILING PUNCTUATION
		char[] cArrPhrase = sPhrase.toCharArray();
		int iNumCharsToLoseAtEnd = 0;
        //api.Log.Log ("xxx" + UtilSets.hsToStr( hsPunctChars,"\r\n",false));
		for ( int i = cArrPhrase.length-1; i > 0; i-- )
		{

			Character CPhraseFromEnd = new Character ( cArrPhrase[i] );
			if ( !hsPunctChars.contains(CPhraseFromEnd)  )
				break;
			else
            {
                //api.Log.Log ("CPhraseFromEnd [" + CPhraseFromEnd + "]" );
				iNumCharsToLoseAtEnd++;
            }
		}

		char[] cArrPhraseCleaned = new char[cArrPhrase.length - iNumCharsToLoseAtEnd];
		for ( int i = 0; i < cArrPhraseCleaned.length; i++ )
		{
			cArrPhraseCleaned[i] = cArrPhrase[i];
		}

		// now clean leading stop words

		return new String(cArrPhraseCleaned);
	}

	private static boolean phraseQualifies ( String[] sArrAllText,
											 int iStart,
											 int iEnd,
											 boolean bPhraseWordsCanStartWithNonLetters)
	{
		boolean bPhraseQualifies = true;
		int iNumTwoCharsOrLess = 0;

		for ( int i = iStart; i < iEnd; i++ )
		{
			String s = sArrAllText[i];
			if ( !bPhraseWordsCanStartWithNonLetters )
			{
				char c = s.toCharArray()[0];
				if ( !Character.isLetter ( c ) )
				{
					bPhraseQualifies = false;
					break;
				}
			}
			if ( s.length() <= 2 )
				iNumTwoCharsOrLess++;
		}

		double dRatioSmallWords = ((double) iNumTwoCharsOrLess /
									(double) (iEnd - iStart + 1 ) );
		if ( dRatioSmallWords > .6 )
			bPhraseQualifies = false;

		//if ( bPhraseQualifies == false )  // hbk control - comment if false section
		//{
			//String[] sPhrase = (String[]) UtilSets.getStrArraySlice ( sArrAllText, iStart, iEnd );
			//com.indraweb.util.Log.logClear ( "disqualified phrase [" + UtilStrings.convertSArrToString ( sPhrase, " " ) + "]\r\n");
		//}

		return bPhraseQualifies;
	}
}
