package com.indraweb.ir;

import java.util.*;

public class IRStringHandlers
{
	// ******************************************
	public static String convertNormalizeString_ElimStopStemOption ( String s_, 
												   String sDelim_,
												   boolean bRemoveStops,
												   boolean bStem )
	// ******************************************
	{
        Hashtable htStopWords = null;

		if ( bRemoveStops ) 
			htStopWords = com.indraweb.execution.Session.stopList.getHTStopWordList();

		StringTokenizer	st	= null;
		try 
		{
			st	= new  StringTokenizer ( s_.toLowerCase() + " " , sDelim_ );
		}
		catch ( Exception e ) 
		{
			com.indraweb.util.Log.FatalError ("tokenizing exception [" + s_ + "]",  e ) ;
		}
			
		String word = null;
		StringBuffer sbReturn = new StringBuffer();
		
		com.indraweb.util.stem.Porter porter = null;
		if ( bStem )
			porter = com.indraweb.util.stem.Porter.getInstance();
		
		int i = 0;
		while ( st.hasMoreElements() )
		{
			try 
			{
				word = ( String ) st.nextElement ();

				word = word.trim();

				// step remove
				if ( bRemoveStops && htStopWords.get ( word ) != null )
					continue;
				
				// stem
				if ( bStem )
					word = porter.stem ( word );
				
				if ( i == 0 )
					sbReturn.append ( word.toLowerCase()  );
				else
					sbReturn.append ( " " + word.toLowerCase());
					
				i++;
			}
			catch ( Exception e )
			{
				com.indraweb.util.Log.FatalError ( "error in removeDelims_StopStemOptional [" + s_ + "]", e ); 
			} 
		}
		return sbReturn.toString();
	}  // private String removeStringDelims_StopWordsStemOptional ( String s_, 
	
	
	
}
