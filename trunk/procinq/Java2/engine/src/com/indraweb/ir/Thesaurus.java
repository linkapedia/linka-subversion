package com.indraweb.ir;

import com.indraweb.util.Log;
import com.indraweb.util.UtilSets;
import com.indraweb.util.UtilStrings;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

/**
 * implements a thesaurus as a set of equivalent/substitute words for any given word
 *
 * animal : BEAST, brute, creature
 * drink : imbibe, quaff, sip, sup, swallow, toss
 *
 * options/issues considered at design :
 *
 * 1 use codes for words - benefit in storage potentially, slower in execution due to
 * indirection of lookup
 *
 * 2 are equivalence sets always reflexive/symmetric?, eg if animal -> beast does beast -> animal
 * will there be benefits in the design if this is the case - it will reduce the number of synonym set
 * vectors by a factor of n where n is the average number of synonyms per word - looks like
 * a factor of 3 or 4.
 *
 * 3 This is a place where noun-verb distinctions could add lift
 *
 * 4 Synonyms are fairly simple to add for coverage and frequency scores. A little trickier for
 * title-to-title matches
 *
 * 5 Would have to write a separate title-title match algorithm (score 14) to leverage thesaurus
 *
 * 6 use getExpandedWordCountHT to get a synonym-expanded hashtable of words and counts
 *
 * 7 Still need to define interfaces for a stemmed thesaurus function, perhaps as a boolean on
 * function calls already defined
 *
 * 8 phrases can by synonymed
 *
 * 9 user relevance feedback
 * <p/>
 * select THESAURUSID, WORDANCHOR, tw1.THESAURUSWORD, WORDRELATED, tw2.THESAURUSWORD
 * from thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2
 * where
 * WORDANCHOR = tw1.WORDID and
 * WORDRELATED = tw2.WORDID ;
 * <p/>
 * select THESAURUSID, WORDANCHOR, tw1.THESAURUSWORD, WORDRELATED, tw2.THESAURUSWORD
 * from thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2
 * where
 * WORDANCHOR = tw1.WORDID and
 * WORDRELATED = tw2.WORDID and
 * THESAURUSID = 5;
 * <p/>
 * 4/2004 : select tw1.THESAURUSWORD, tw2.THESAURUSWORD
 * from corpusthesaurus ct, thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2
 * where
 * ct.corpusID = 100011
 * and ct.thesaurusID = tr.thesaurusID
 * and tr.WORDANCHOR = tw1.WORDID
 * and tr.WORDRELATED = tw2.WORDID ;
 * <p/>
 * SQL> desc thesaurusrelations;
 * Name Null? Type
 * ----------------------------------------- -------- ----------------------------
 * THESAURUSID NOT NULL NUMBER(5)
 * WORDANCHOR NOT NULL NUMBER(12)
 * WORDRELATED NOT NULL NUMBER(12)
 * RELATIONSHIP NOT NULL NUMBER(2)
 * <p/>
 * SQL> desc corpusthesaurus;
 * Name Null? Type
 * ----------------------------------------- -------- ----------------------------
 * CORPUSID NOT NULL NUMBER(9)
 * THESAURUSID NOT NULL NUMBER(5)
 * <p/>
 * SQL> desc THESAURUSWORD
 * Name Null? Type
 * ----------------------------------------- -------- ----------------------------
 * WORDID NOT NULL NUMBER(12)
 * THESAURUSWORD NOT NULL VARCHAR2(60)
 * <p/>
 * SQL> desc THESAURUS;
 * Name Null? Type
 * ----------------------------------------- -------- ----------------------------
 * THESAURUSID NOT NULL NUMBER(5)
 * THESAURUSNAME VARCHAR2(30)
 * <p/>
 * <
 * p/>
 * <
 * p/>
 * select THESAURUSID, WORDANCHOR, tw1.THESAURUSWORD, WORDRELATED, tw2.THESAURUSWORD
 * from thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2
 * where
 * tr.thesaurusid = 5 and
 * WORDANCHOR = tw1.WORDID and
 * WORDRELATED = tw2.WORDID ;
 * <p/>
 * and (
 * tw1.THESAURUSWORD = 'product' or tw2.THESAURUSWORD = 'nce')
 * <p/>
 * <
 * p/>
 *
 */
public class Thesaurus {

    /**
     * keyed by a concat of id, corpusorthesaurus, and stemmed y/n
     */
    private static boolean bVerbose = false;
    private static Hashtable htThesaurusCache = new Hashtable();
    /**
     * Synonym data structure - hashtable of vectors, keyed by word
     */
    private Hashtable htSyns_sTerm_to_hsSynTerm = new Hashtable();
    private Hashtable ht2MultiWordKey_StringFirstWordKey_KeyWord_tohsListLensStartingThisWord = new Hashtable();
    private String sDelimitersText = null;

    // *******************************************
    public Thesaurus(boolean bStemmed,
            // *******************************************
            int iID_CorpusOrThesaurus,
            boolean bCorpusIDTrueThesaurusIDFalse,
            java.sql.Connection dbc)
            throws Exception {
        api.Log.Log(" new thesaurus iID_CorpusOrThesaurus [" + iID_CorpusOrThesaurus + "]");
        sDelimitersText = ParseParms.getDelimitersAll_Default();
        fillSyns(bStemmed, iID_CorpusOrThesaurus, bCorpusIDTrueThesaurusIDFalse, dbc);
        String sCacheLookup = iID_CorpusOrThesaurus + ":" + bStemmed + ":" + bCorpusIDTrueThesaurusIDFalse;
        if (bVerbose) {
            api.Log.Log("1 adding cached thesaurus sCacheLookup  [" + sCacheLookup + "]");
        }
        htThesaurusCache.put(sCacheLookup, this);
    }

    private void fillSyns(boolean bStemmed, int iIDCorpusOrThesaurus, boolean bCorpusIDTrueThesaurusIDFalse, Connection dbc) throws Exception {
        // get set of thesauri for this corpora

        api.Log.Log("in fillSyns for thes db connection iIDCorpusOrThesaurus [" + iIDCorpusOrThesaurus + "] bCorpusIDTrueThesaurusIDFalse [" + bCorpusIDTrueThesaurusIDFalse + "]");
        String sql = null;
        if (bCorpusIDTrueThesaurusIDFalse) {
            sql = "select tw1.THESAURUSWORD, tw2.THESAURUSWORD "
                    + " from corpusthesaurus ct, thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2  "
                    + " where "
                    + " ct.corpusID = " + iIDCorpusOrThesaurus + " and "
                    + " ct.thesaurusID = tr.thesaurusID and "
                    + " tr.WORDANCHOR = tw1.WORDID and "
                    + " tr.WORDRELATED = tw2.WORDID ";
        } else {
            sql = "select tw1.THESAURUSWORD, tw2.THESAURUSWORD "
                    + " from thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2 "
                    + " where "
                    + " tr.thesaurusID = " + iIDCorpusOrThesaurus + " and "
                    + " tr.WORDANCHOR = tw1.WORDID and "
                    + " tr.WORDRELATED = tw2.WORDID ";
        }
        if (bVerbose) {
            api.Log.Log("thesaurus sql [" + sql + "]");
        }

        Statement stmt = null;;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sql);

            String sTerm = null;;
            String sTermSyn = null;
            int iCountSyns = 0;
            while (rs.next()) {

                sTerm = rs.getString(1).toLowerCase();
                sTermSyn = rs.getString(2).toLowerCase();

                if (bStemmed) {
                    sTerm = com.indraweb.ir.IRStringHandlers.convertNormalizeString_ElimStopStemOption(sTerm, sDelimitersText, false, bStemmed);
                    sTermSyn = com.indraweb.ir.IRStringHandlers.convertNormalizeString_ElimStopStemOption(sTermSyn, sDelimitersText, false, bStemmed);
                }
                iCountSyns++;
                if (false && bVerbose) {
                    api.Log.Log("thes: add : " + sTerm.trim() + ":" + sTermSyn.trim());
                }
                addSynonym(sTerm.trim(), sTermSyn.trim());
            }
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }

    /**
     * Add a synonym for a word
     */
    // *******************************************
    public void addSynonym(String sTermAntecedent, String sWordSyn) // *******************************************
    {
        // this is called once for each row of :
        /*
         * select tw1.THESAURUSWORD, tw2.THESAURUSWORD
         * from corpusthesaurus ct, thesaurusrelations tr, THESAURUSWORD tw1, THESAURUSWORD tw2
         * where
         * ct.corpusID = 100011
         * and ct.thesaurusID = tr.thesaurusID
         * and tr.WORDANCHOR = tw1.WORDID
         * and tr.WORDRELATED = tw2.WORDID ;
         */
        if (bVerbose) {
            api.Log.Log("inserting syn term [" + sTermAntecedent + "] [" + sWordSyn + "]\r\n");
        }

        HashSet hs3SynonymsInner_WordKey_WordEqualValue = (HashSet) htSyns_sTerm_to_hsSynTerm.get(sTermAntecedent);

        if (hs3SynonymsInner_WordKey_WordEqualValue == null) {
            hs3SynonymsInner_WordKey_WordEqualValue = new HashSet();
        }

        hs3SynonymsInner_WordKey_WordEqualValue.add(sWordSyn);
        // water -> h20
        // water -> melte
        htSyns_sTerm_to_hsSynTerm.put(sTermAntecedent, hs3SynonymsInner_WordKey_WordEqualValue);

        // now get the multiword expansion hashtable primed

        Vector vsSplitTermAntecedent = UtilStrings.splitByStrLenLong(sTermAntecedent, sDelimitersText);
        if (vsSplitTermAntecedent != null && vsSplitTermAntecedent.size() > 0) {
            String sLeftWord = (String) vsSplitTermAntecedent.elementAt(0);
            HashSet hsListLensStartingThisWord = (HashSet) ht2MultiWordKey_StringFirstWordKey_KeyWord_tohsListLensStartingThisWord.get(sLeftWord);
            if (hsListLensStartingThisWord == null) {
                hsListLensStartingThisWord = new HashSet();
                //api.Log.Log ("adding new multiword lookup [" + sLeftWord + "] of [" + vsSplitTermAntecedent.size() + "] on [" + sTermAntecedent  + "]\r\n");
            } else {
                //api.Log.Log ("adding repeat multiword lookup [" + sLeftWord + "] of [" + vsSplitTermAntecedent.size() + "] on [" + sTermAntecedent  + "]\r\n");
            }
            Integer Isize = new Integer(vsSplitTermAntecedent.size());
            hsListLensStartingThisWord.add(Isize);
            ht2MultiWordKey_StringFirstWordKey_KeyWord_tohsListLensStartingThisWord.put(
                    sLeftWord, hsListLensStartingThisWord);
        }
    } // add synonym
    /**
     * Finds all expansion terms for the term passed in and its subterms
     */
    int iCallCount_getMultiWordExpansions = 0;
//	public Vector getMultiWordExpansions ( String sSourceText, boolean bExpandFullSourceTextAsWellAsElements )
//	{
//        return getMultiWordExpansions(sSourceText, null, bExpandFullSourceTextAsWellAsElements);
//    }

    public Vector getMultiWordExpansions(String sSourceText) {
        //long lStartgetMultiWordExpansions = System.currentTimeMillis();
        //iCallCount_getMultiWordExpansions++;
        //if ( bVerbose )
        //api.Log.Log ("getMultiWordExpansions call# [" + iCallCount_getMultiWordExpansions +
        //        "] of term [" + sSourceText  + "]" );

        Vector vsSplitSourceText = UtilStrings.splitByStrLenLong(sSourceText,
                sDelimitersText);
        //vsSplitSourceText.addElement ( sSourceText );
        // see if this word is in the multiword syn key lookup table
        String[] sArrSplitSourceText = new String[vsSplitSourceText.size()];
        vsSplitSourceText.copyInto(sArrSplitSourceText);
        Vector vReturn = null;

        // check each individual word from the source string to see if there's a possible expansion
        for (int iSourceWordPos = 0; iSourceWordPos < sArrSplitSourceText.length; iSourceWordPos++) {

            //if ( bVerbose )
            // api.Log.Log("getMultiWordExpansions checking [" + sArrSplitSourceText [ iSourceWordPos ] + "]" );

            HashSet hsSizesOfAntecedentTermVectorsStartingWithThisWord = (HashSet) ht2MultiWordKey_StringFirstWordKey_KeyWord_tohsListLensStartingThisWord.get(sArrSplitSourceText[ iSourceWordPos]);

            if (bVerbose && hsSizesOfAntecedentTermVectorsStartingWithThisWord != null) {
                api.Log.Log("hsSizesOfAntecedentTermVectorsStartingWithThisWord " + UtilSets.hsToStr(hsSizesOfAntecedentTermVectorsStartingWithThisWord, ",", false));
            }

            if (hsSizesOfAntecedentTermVectorsStartingWithThisWord != null) {
                Iterator eSizesOfAntecedentTermVectorsStartingWithThisWord = hsSizesOfAntecedentTermVectorsStartingWithThisWord.iterator();
                while (eSizesOfAntecedentTermVectorsStartingWithThisWord.hasNext()) {
                    Integer ISizeOfAntecedentTermVectorsStartingWithThisWord = (Integer) eSizesOfAntecedentTermVectorsStartingWithThisWord.next();
                    // optimization - skip ahead to end of matching process -
                    // if a possible expansion, see if it matches all the way down

                    int iSizeOfAntecedentTermVectorsStartingWithThisWord = ISizeOfAntecedentTermVectorsStartingWithThisWord.intValue();
                    StringBuffer sb = new StringBuffer();
                    boolean bHitEndOfWholeString = false;
                    if (iSizeOfAntecedentTermVectorsStartingWithThisWord > 1) {
                        for (int iSubWordIndex = iSourceWordPos;
                                iSubWordIndex < iSourceWordPos + iSizeOfAntecedentTermVectorsStartingWithThisWord;
                                iSubWordIndex++) {
                            if (iSubWordIndex == sArrSplitSourceText.length) {
                                // ran up against right side boundary in expansion test
                                // e.g., string is 1 "time love money" and 2 "love twisted people" -> "time2"
                                // then the test for an expansion of 1 by 2 would run against the wall.
                                bHitEndOfWholeString = true;
                                break;
                            } else {
                                if (iSubWordIndex == iSourceWordPos) {
                                    sb.append(sArrSplitSourceText[ iSubWordIndex]);
                                } else {
                                    try {
                                        sb.append(" " + sArrSplitSourceText[ iSubWordIndex]);
                                    } catch (Exception e) {
                                        Log.FatalError(
                                                "sTermAntecedent [" + sSourceText + "] "
                                                + "sArrSplitTermLeftSide.length [" + sArrSplitSourceText.length + "] "
                                                + "iSubWordIndex [" + iSubWordIndex + "] "
                                                + "iSourceWordPos [" + iSourceWordPos + "] "
                                                + "sb.toString() [" + sb.toString() + "]\r\n", e);
                                    }
                                }
                            }
                        }
                    } else {
                        sb.append(sArrSplitSourceText[ iSourceWordPos]);
                    }

//                    if ( bVerbose )
//                        api.Log.Log ("testing iSourceWordPos [" + iSourceWordPos+
//                                "] sb.toString() [" + sb.toString() + "]" );

                    if (bHitEndOfWholeString == false) // don't bother looking if we hit the end of the string - see comment above
                    {
                        HashSet hsSyn = getSynWholeWord(sb.toString());
                        // if the possible expansion turned out to be real
                        if (hsSyn != null) {
                            if (vReturn == null) {
                                vReturn = new Vector();
                            }

                            // put the 1 or more expanion terms in the output set
                            Iterator iter = hsSyn.iterator();
                            while (iter.hasNext()) {
                                String sExpansionTerm = (String) iter.next();

//                                if ( bVerbose )  api.Log.Log ("got sExpansionTerm  [" + sExpansionTerm  + "] from key sb.toString() [" + sb.toString() + "]" );
                                vReturn.addElement(sExpansionTerm);
                            }
                        }
                    }
                }
            }  //if ( vListLensStartingThisWord != null )
        }
        //api.Log.Log ( "thes time " + (System.currentTimeMillis() - lStartgetMultiWordExpansions) + " for text len [" + sArrSplitSourceText.length + "]");
        return vReturn;
    } // get multiword expansions

    /**
     * Add a synonym for a word
     */
    //public void addSynonyms ( String sWord, Vector vSyns )
    //{
    //}
    /**
     * Delete a synonym for a word
     */
    //public void removeSynonym ( String sWord, String sSyn )
    //{
    //}
    /**
     * Delete all synonyms for a word
     */
    //public void clearSynonyms ( String sWord )
    //{
    //}
    public HashSet getSynWholeWord(String sWordOrTermNotToBeBroken) {
        return (HashSet) htSyns_sTerm_to_hsSynTerm.get(sWordOrTermNotToBeBroken);
    }

    /**
     * invalidate the static cache entry
     */
    public static void cacheInvalidate(
            int iID_CorpusOrThesaurus,
            boolean bIDType_CorpusTrue_ThesaurusFalse) {
        api.Log.Log("invalidating thesaurus cache ID [" + iID_CorpusOrThesaurus + "] "
                + " CorpusTrue_ThesaurusFalse [" + bIDType_CorpusTrue_ThesaurusFalse + "]");
        //remove stemmed first
        String sCacheLookup = iID_CorpusOrThesaurus + ":" + "true" + ":" + bIDType_CorpusTrue_ThesaurusFalse;
        //api.Log.Log  ("removing stemmed cached thesaurus sCacheLookup  [" + sCacheLookup  + "]" );
        htThesaurusCache.remove(sCacheLookup);
        //remove non stemmed
        sCacheLookup = iID_CorpusOrThesaurus + ":" + "false" + ":" + bIDType_CorpusTrue_ThesaurusFalse;
        htThesaurusCache.remove(sCacheLookup);
        //api.Log.Log  ("removing non stemmed cached thesaurus sCacheLookup  [" + sCacheLookup  + "]" );
    }

    /**
     * get a cached thesaurus entry - by corpus or by thesaurus
     */
    public static Thesaurus cacheGetThesaurus(
            boolean bStemmed,
            int iID_CorpusOrThesaurus,
            boolean bCorpusIDTrueThesaurusIDFalse,
            java.sql.Connection dbc)
            throws Exception {
        String sCacheLookup = iID_CorpusOrThesaurus + ":" + bStemmed + ":" + bCorpusIDTrueThesaurusIDFalse;
        Thesaurus thes = (Thesaurus) htThesaurusCache.get(sCacheLookup);
        if (thes == null) {
            thes = new Thesaurus(bStemmed, iID_CorpusOrThesaurus, bCorpusIDTrueThesaurusIDFalse, dbc);
            if (bVerbose) {
                api.Log.Log("2 adding cached thesaurus sCacheLookup  [" + sCacheLookup + "]");
            }
            htThesaurusCache.put(sCacheLookup, thes);
        }
        return thes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("THES : first HT keys [").append(UtilSets.convertHTKeysToStrCommaList(htSyns_sTerm_to_hsSynTerm)).append("]");
        sb.append("hs synx for address [").append(UtilSets.hsToStr((HashSet) htSyns_sTerm_to_hsSynTerm.get("address"), ",", false)).append("]");
        sb.append("THES : second HT keys [").append(UtilSets.convertHTKeysToStrCommaList(htSyns_sTerm_to_hsSynTerm)).append("]");
        return sb.toString();

    }
}
