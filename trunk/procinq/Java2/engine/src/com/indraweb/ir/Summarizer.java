package com.indraweb.ir;

import com.indraweb.encyclopedia.Topic;
import com.indraweb.execution.Session;
import com.indraweb.util.Log;
import com.indraweb.util.UtilSets;
import java.util.Enumeration;
import java.util.Vector;

public class Summarizer {

    //private static boolean bDebug = false;
    private static int[] findSnippetNuggetStart(char[] cArrTextAllInOne,
            int iStartPoint,
            int iNUMWORDSBACKANDFOR_SNIPPET,
            int iNUMWORDSBACKANDFOR_NUGGET,
            boolean bUseNuggets) {

        //api.Log.Log ("findSnippetNuggetStart [" + cArrTextAllInOne.length + "] iStartPoint [" + iStartPoint + "] str [" +
        //        showBreakInText (new String (cArrTextAllInOne).toString () , iStartPoint) + "]");
        int iNumWordsBack = 0;
        boolean bGotSnippet = false;
        int itextStartSnippet = -1;
        int itextStartNugget = -1;
        for (int itextStart = iStartPoint; itextStart >= 0; itextStart--) {
            if (cArrTextAllInOne[itextStart] == ' ') {
                iNumWordsBack++;
                if (bGotSnippet == false && iNumWordsBack == iNUMWORDSBACKANDFOR_SNIPPET) {
                    bGotSnippet = true;
                    itextStartSnippet = itextStart;
                    if (!bUseNuggets) {
                        break;
                    }
                }
                if (iNumWordsBack == iNUMWORDSBACKANDFOR_NUGGET) {
                    itextStartNugget = itextStart;
                    break;
                }
                if (itextStart > 0) {
                    itextStart--;
                }
                while (itextStart > 0 && cArrTextAllInOne[itextStart] == ' ') {
                    itextStart--;
                }
                //api.Log.Log ("itextStart [" + itextStart + "] cArrTextAllInOne[itextStart] [" + cArrTextAllInOne[itextStart] + "]");
                //itextStart++;

            }
        }
        // in case
        if (itextStartNugget < 0) {
            itextStartNugget = 0;
        }
        if (itextStartSnippet < 0) {
            itextStartSnippet = 0;
        }
        // hbk timer timerloopstart.stop();
        int[] iArrResult = new int[2];
        iArrResult[0] = itextStartSnippet;
        //api.Log.Log ("findSnippetNuggetStart result [" + showBreakInText (new String (cArrTextAllInOne).toString () , iArrResult[0]) + "]" );
        iArrResult[1] = itextStartNugget;
        return iArrResult;


    } //private static int[] findSnippetNuggetStart(char[] cArrTextAllInOne,

    private static int[] findSnippetNuggetEnd(char[] cArrTextAllInOne,
            int iStartPoint,
            int iNUMWORDSBACKANDFOR_SNIPPET,
            int iNUMWORDSBACKANDFOR_NUGGET,
            boolean bUseNuggets) {
        int iNumWordsForeward = 0;
        boolean bGotSnippet = false;
        int iTextEndSnippet = -1;
        int iTextEndNugget = -1;

        for (int itextEnd = iStartPoint; itextEnd < cArrTextAllInOne.length; itextEnd++) {
            if (cArrTextAllInOne[itextEnd] == ' ') {
                iNumWordsForeward++;
                if (bGotSnippet == false && iNumWordsForeward == iNUMWORDSBACKANDFOR_SNIPPET) {
                    bGotSnippet = true;
                    iTextEndSnippet = itextEnd;
                    if (!bUseNuggets) {
                        break;
                    }
                }
                if (iNumWordsForeward == iNUMWORDSBACKANDFOR_NUGGET) {
                    iTextEndNugget = itextEnd;
                    break;
                }
            }
        }
        if (iTextEndNugget == -1) {
            iTextEndNugget = cArrTextAllInOne.length;
        }
        if (iTextEndSnippet == -1) {
            iTextEndSnippet = cArrTextAllInOne.length;
        }
        // hbk timer timerloopstart.stop();
        int[] iArrResult = new int[2];
        iArrResult[0] = iTextEndSnippet;
        iArrResult[1] = iTextEndNugget;
        return iArrResult;


    }  // private static int[] findSnippetNuggetEnd(char[] cArrTextAllInOne,

    // snippet is the 5-10 word item, nugget is more like paragraphs
//    private static int iCallCounter = 0;
//    private static final int iCallCounterModToProfile = 1;
    public static SummarizerResult createSummary3_snippet(long lNodeID,
            String sURL,
            Topic t,
            int iMaxLenSummary,
            int iMaxNumSnippets,
            Vector vTitleOrSigWords,
            String sNodeTitle,
            boolean bUseNuggets)
            throws Exception {
        //iCallCounter++;
        //if ( iCallCounter % iCallCounterModToProfile == 0 )
        //     lStartTime = System.currentTimeMillis();
        int iNumSnippetsInSummary = 0;
        int[] iArrFoundHitForTerm = null;
        int jSigTermIndex = 0;

        //api.Log.Log (iCallCounter+ ". in createSummary3_snippet");
        if (Session.stopList == null) {
            throw new Exception("Session.stopList == null");
        }
        SummarizerResult sumResult = new SummarizerResult();
        final int iNUMWORDSBACKANDFOR_SNIPPET = 4;
        final int iNUMWORDSBACKANDFOR_NUGGET = 20;

        // hbk timer com.indraweb.profiler.Timer timer = com.indraweb.profiler.Profiler.getTimer("method insertOrUpdateNodeDocInfotoDB->createSummary2", true);
        // hbk timer try
        // hbk timer {
        // hbk timer com.indraweb.profiler.Timer timertop = com.indraweb.profiler.Profiler.getTimer("method insertOrUpdateNodeDocInfotoDB->createSummary2->top", true);
        // Vector vTitleOrSigWords = DBAccess_Central_OutOfBand_connection.getSigTermsForNode ( lNodeID, dbc );
        boolean bWantDebugStrings = false;

        // title
        Vector vTitleWords = UtilSets.convertStringToStrVec_Tokenizer(sNodeTitle, " ", true);
        for (int i = 0; i < vTitleWords.size(); i++) {
            vTitleWords.setElementAt(((String) vTitleWords.elementAt(i)).toLowerCase(), i);
        }

        // combined sig title words (may be title and sig word overlap esp at header now)
        vTitleOrSigWords = UtilSets.concatenateVectors(vTitleWords, vTitleOrSigWords);
        UtilSets.deDupVector(vTitleOrSigWords);
        UtilSets.removeNumbersFromStringVector(vTitleOrSigWords);
        vTitleOrSigWords = UtilSets.getVectorSlice(vTitleOrSigWords, 0, 20);

        try {
            String sAllText = t.sbAllTextInOne.toString();
            //com.indraweb.util.Log.logClearcr("sAllText:" + sAllText); // hbk control comment for production
            String sAllTextLower = sAllText.toLowerCase();
            //api.Log.Log ("SummarizerResult createSummary3_snippet sAllText [" + sAllText + "]");
            char[] cArrTextAllInOne = sAllText.toCharArray();

            int iTotalCharsIncluded = 0;
            int iTotalNumSnippetsSoFar = 0;
            int iNUM_SIG_WORDSTO_CONSIDER = 20;

            // COLLECT TEXT SNIPPETS
            int iSigWordWrapPoint = UtilSets.min(iNUM_SIG_WORDSTO_CONSIDER, vTitleOrSigWords.size());
            int[] iArrMvngPointPerTerm_snippet = new int[iSigWordWrapPoint];
            // hbk timer timertop.stop();
            // hbk timer com.indraweb.profiler.Timer tim    erloop = com.indraweb.profiler.Profiler.getTimer("method insertOrUpdateNodeDocInfotoDB->createSummary2 loop", true);
            //api.Log.Log ("set of terms for summ gen [" + UtilSets.vToStr (vTitleOrSigWords) + "]");


            Vector vIntRangesStartStopAlternating = new Vector();

            iArrFoundHitForTerm = new int[vTitleOrSigWords.size()];
            for (int i = 0; i < iArrFoundHitForTerm.length; i++) {
                iArrFoundHitForTerm[i] = 0;
            }
            // will wrap ...
            //api.Log.Log ("createSummary3_snippet - sAllText is [" + sAllText + "]" );
            for (jSigTermIndex = 0; true; jSigTermIndex++) {
                int jPostMod = jSigTermIndex % iSigWordWrapPoint;

                String sTitleOrSigWord = ((String) vTitleOrSigWords.elementAt(jPostMod)).toLowerCase();


                if (sTitleOrSigWord != null && Session.stopList.getHTStopWordList().get(sTitleOrSigWord.toLowerCase()) == null) {
                    //api.Log.Log ("--- jSigTermIndex [" + jSigTermIndex + "] sTitleOrSigWord [" + sTitleOrSigWord + "]");
                    int iWordIndexRelToMovingPointer = sAllTextLower.substring(
                            iArrMvngPointPerTerm_snippet[jPostMod]).indexOf(
                            sTitleOrSigWord);
                    if (iWordIndexRelToMovingPointer >= 0) {
                        iArrMvngPointPerTerm_snippet[jPostMod] += (iWordIndexRelToMovingPointer + 1);
                        //api.Log.Log ("incrementing pointer for term [" + jPostMod + "] [" + vTitleOrSigWords.elementAt((jPostMod)) + "] to [" + iArrMvngPointPerTerm_snippet[jPostMod] + "]" );
                        //Log.logClear ("sig word [" + ( j % iNUM_SIG_WORDS_TO_CONSIDER ) + "] [" + sTitleOrSigWord + "] YES found\r\n");
                        //Vector vSigLine

                        // WALK BACK TO SNIPPET AND NUGGET START POINT
                        // hbk timer com.indraweb.profiler.Timer timerloopstart = com.indraweb.profiler.Profiler.getTimer("method insertOrUpdateNodeDocInfotoDB->createSummary2 loop -> start", true);
                        int iTextStart_snippet = -1;
                        int iTextStart_nugget = -1;
                        int[] iArrTextStart_snipNug = findSnippetNuggetStart(cArrTextAllInOne,
                                iArrMvngPointPerTerm_snippet[jPostMod],
                                iNUMWORDSBACKANDFOR_SNIPPET,
                                iNUMWORDSBACKANDFOR_NUGGET,
                                bUseNuggets);
                        iTextStart_snippet = iArrTextStart_snipNug[0];
                        iTextStart_nugget = iArrTextStart_snipNug[1];

                        // ONLY FINISH IF NOT PAST END OF DOC
                        if (iArrMvngPointPerTerm_snippet[jPostMod] + 2 < cArrTextAllInOne.length) {
                            // NOW WALK FORWARD TO SNIPPET END POINT
                            int iTextEnd_snippet = -1;
                            int iTextEnd_nugget = -1;
                            int[] iArrTextEnd_snipNug = findSnippetNuggetEnd(cArrTextAllInOne,
                                    iArrMvngPointPerTerm_snippet[jPostMod],
                                    iNUMWORDSBACKANDFOR_SNIPPET,
                                    iNUMWORDSBACKANDFOR_NUGGET,
                                    bUseNuggets);
                            iTextEnd_snippet = iArrTextEnd_snipNug[0];
                            if (bUseNuggets) {
                                iTextEnd_nugget = iArrTextEnd_snipNug[1];
                            }

                            if (iTextEnd_snippet < 0 || iTextEnd_snippet <= iTextStart_snippet) {
                                Log.NonFatalError("itextEnd no good snip \r\n");
                            }

                            if (bUseNuggets && (iTextEnd_nugget < 0 || iTextEnd_nugget <= iTextStart_nugget)) {
                                Log.NonFatalError("itextEnd no good nug \r\n");
                            }

                            // GOT START AND END INT POINTS NOW MAKE VECTOR TO ELIM LEAD AND TAIL STOP WORDS
                            /*
                             * Vector vStrSnippetVector = UtilSets.convertStringToStrVec_Tokenizer (
                             * sOriginalSnippetSubString, " ", false);
                             *
                             * boolean[] bKeep = new boolean[ vStrSnippetVector.size() ];
                             *
                             * Hashtable htStopWords = Session.stopList.getHTStopWordList();
                             * // find first word to keep
                             * int iFirstToKeep = 0;
                             * for ( int i2 = 0 ; i2 < bKeep.length; i2++ )
                             * {
                             * if ( htStopWords.get ( vStrSnippetVector.elementAt(i2)) == null )
                             * {
                             * iFirstToKeep = i2;
                             * break;
                             * }
                             * }
                             *
                             * // find last word to keep (will keep all in between)
                             * int iLastToKeep = -1;
                             * for ( int i2 = bKeep.length-1 ; i2 >= 0; i2-- )
                             * {
                             * if ( htStopWords.get ( vStrSnippetVector.elementAt(i2)) == null )
                             * {
                             * iLastToKeep = i2;
                             * break;
                             * }
                             * }
                             *
                             * // now construct the final snippet
                             * StringBuffer sbFinalSnippet = new StringBuffer();
                             * for ( int i2 = 0 ; i2 < bKeep.length; i2++ )
                             * {
                             * if ( i2 >= iFirstToKeep && i2 <= iLastToKeep)
                             * {
                             * //if ( sTitleOrSigWord.equals ( vStrSnippetVector.elementAt(i2) ) )
                             * //vStrSnippetVector.setElementAt(((String) vStrSnippetVector.elementAt(i2)).toUpperCase(), i2);
                             * sbFinalSnippet.append ( vStrSnippetVector.elementAt(i2) + " " );
                             * }
                             * }
                             */
                            // String sFinalSnippetTrim = sbFinalSnippet.toString().trim();

                            // DEBUG DATA
                            if (bWantDebugStrings) {
                                /*
                                 * String sTitleIndicator = null;
                                 * String sFinalSnippet = sbFinalSnippet.toString();
                                 * if (( j % iSigWordWrapPoint ) < iNumTitleWords )
                                 * sTitleIndicator = ":TITLE:";
                                 * else
                                 * sTitleIndicator = ":SIG:";
                                 * sumResult.vStrDocSnippets_debug.addElement ("... [index" + (j % iSigWordWrapPoint) + sTitleIndicator +
                                 * ":" + vTitleOrSigWords.elementAt( (j % iSigWordWrapPoint)) +
                                 * "] " + sbFinalSnippet);
                                 */
                            }

                            try {
                                String sOriginalSnippetSubString = sAllText.substring(iTextStart_snippet, iTextEnd_snippet).trim();

                                //api.Log.Log ("vIntRangesStartStopAlternating [" + UtilSets.vToStr(vIntRangesStartStopAlternating) + "]" );
                                if (!isTextRangeAlreadyCovered(
                                        vIntRangesStartStopAlternating,
                                        iTextStart_snippet, iTextEnd_snippet, 0.50,
                                        sOriginalSnippetSubString,
                                        sTitleOrSigWord)) {

                                    //Log.logClear ( "snipnugppoints : " + iTextStart_snippet + ":" + iTextEnd_snippet +
                                    //			   ":" + iTextStart_nugget + ":" + iTextEnd_nugget + "\r\n" );
                                    //Log.logClearcr ("sig ot title word [" + sTitleOrSigWord +
                                    //"] sOriginalSnippetSubString:["+sOriginalSnippetSubString +
                                    //"] iTextStart_snippet, iTextEnd_snippet [" +
                                    //iTextStart_snippet + ","
                                    //+ iTextEnd_snippet+ "]");

                                    iNumSnippetsInSummary++;
                                    iArrFoundHitForTerm[ jPostMod]++;
                                    vIntRangesStartStopAlternating.addElement(new Integer(iTextStart_snippet));
                                    vIntRangesStartStopAlternating.addElement(new Integer(iTextEnd_snippet));
                                    sumResult.vStrDocSnippets.addElement(sOriginalSnippetSubString);

                                    if (bUseNuggets) {
                                        String sOriginalNugget = sAllText.substring(iTextStart_nugget, iTextEnd_nugget).trim();
                                        sumResult.vStrDocNuggets.addElement(sOriginalNugget);
                                    }

                                    iTotalCharsIncluded += (sOriginalSnippetSubString.length() + 4); // + 4 for "... "
                                    iTotalNumSnippetsSoFar++;
                                    if (iTotalCharsIncluded > iMaxLenSummary) {
                                        break;
                                    }
                                    if (iTotalNumSnippetsSoFar == iMaxNumSnippets) {
                                        break;
                                    }
                                } //if this block not covered
                            } catch (Exception e) {
                                Log.NonFatalError("in snippet/nugget final - caught ok", e);
                            }

                            // hbk timer timerloopend.stop();
                        } else {
                            break; // reached doc end
                        }
                    }  // if (iWordIndexRelToMovingPointer < 0 )
                } // if sig term not null
                if (((int) jSigTermIndex / iSigWordWrapPoint) > 5) {
                    break;
                }

            } // for ( int jSigTermIndex = 0;

            // hbk timer timerloop.stop();

            // Log.logClear ( " found [" + iNumFound + "] out of [" + vTitleOrSigWords.size() + "]\r\n");
            //Log.logClear("docsummary w/data [" + sArr_NodeTitle_Dirty_Clean_DocTitle[1] +"]\r\n");
            //Log.logClear("docsummary [" + sArr_NodeTitle_Dirty_Clean_DocTitle[2] +"]\r\n");


            if (false) // hbk control // print snippet and nugget results
            {
                Log.logClear("docsummary [" + sumResult.getDocSummary(false) + "]\r\n\r\n\r\n");
                Enumeration e = sumResult.vStrDocNuggets.elements();
                while (e.hasMoreElements()) {
                    String sNugget = (String) e.nextElement();
                    Log.logClear("nugget [" + sNugget + "]\r\n\r\n");
                }
                //Log.logClear("docsummary [" + sArr_NodeTitle_Dirty_Clean_DocTitle[2] +"]\r\n");
            }

            return sumResult;


        } catch (Exception e) {
            Log.NonFatalError("bad summary 2", e);
        } finally {
//            if ( iCallCounter % iCallCounterModToProfile == 0 )
//            {
//                StringBuffer sbHitCounts = new StringBuffer();
//                for ( int i = 0; i < iArrFoundHitForTerm.length; i++)
//                {
//                    sbHitCounts.append(i + ". " + iArrFoundHitForTerm[i] + ";" );
//                }
//                //api.Log.Log ("1 completed docsummary in ms [" +(System.currentTimeMillis()-lStartTime)+ "]" );
//                api.Log.Log ("2 sumResult.getDocSummary(false) [" + sumResult.getDocSummary (false) + "]");
//                api.Log.Log ("3 sumResult.getDocSummary(true) [" + sumResult.getDocSummary (true) + "]");
//                api.Log.Log ("4 jSigTermIndex at end [" + jSigTermIndex  + "]");
//                api.Log.Log ("5 vTitleOrSigWords.size() [" + vTitleOrSigWords.size() + "]");
//                api.Log.Log ("6 hit distribution [" + sbHitCounts.toString() + "]");
//                api.Log.Log ("7 iNumSnippetsInSummary [" + iNumSnippetsInSummary + "]");
//                api.Log.Log ("8 iTotalSummLenNotWithElipses  [" + iTotalSummLenNotWithElipses + "]");
            //}
        }

        return null;
        // hbk timer }
        // hbk timer finally
        // hbk timer {
        // hbk timer timer.stop();
        // hbk timer }

    }  // public static String[] createSummary3_nugget ( long lNodeID,

    public static boolean isTextRangeAlreadyCovered(Vector vIntRangesAlreadyCoveredStartStopAlternating,
            int iStartThis,
            int iStopThis,
            double dThresholdFraction,
            String sSnippet,
            String sTitleOrSigWord)
            throws Exception {
        //api.Log.Log ("--- overlap check checking for term [" + sTitleOrSigWord+ "] this start [" + iStartThis+ "] to [" + iStopThis + "] vs already ranges [" + UtilSets.vToStr(vIntRangesAlreadyCoveredStartStopAlternating)+ "] on snippet [" + sSnippet + "]");
//        if ( UtilFile.bFileExists ( "/temp/IndraDebugSummaryOldStyle_noOverlapCheck.txt" ) )
//        {
//            api.Log.Log ("exists : debug summary mode : /temp/IndraDebugSummaryOldStyle_noOverlapCheck.txt");
//            return false;
//        }
        int iRangeSizeThis = iStopThis - iStartThis;
        if (iRangeSizeThis < 0) {
            throw new Exception("invalid range size in text summary generator");
        }

        int iOverlapSizeAccumulator = 0;
        Enumeration enumIntsAlreadyCoveredStartStopAlternating = vIntRangesAlreadyCoveredStartStopAlternating.elements();
        while (enumIntsAlreadyCoveredStartStopAlternating.hasMoreElements()) {
            int iStartItemAlreadyCovered = ((Integer) enumIntsAlreadyCoveredStartStopAlternating.nextElement()).intValue();
            int iStopItemAlreadyCovered = ((Integer) enumIntsAlreadyCoveredStartStopAlternating.nextElement()).intValue();

            int iOverlapStart = UtilSets.max(iStartThis, iStartItemAlreadyCovered);
            int iOverlapStop = UtilSets.min(iStopThis, iStopItemAlreadyCovered);
            int iOverLapSize = iOverlapStop - iOverlapStart;
            if (iOverLapSize < 0) // redundant I know
            {
                iOverLapSize = 0;
            }

            iOverlapSizeAccumulator += iOverLapSize;
//            api.Log.Log ("overlap check inner " +
//                    " iStartItemAlreadyCovered [" + iStartItemAlreadyCovered + "] " +
//                    " iStopItemAlreadyCovered [" + iStopItemAlreadyCovered + "] " +
//                    " iStartThis[" + iStartThis+ "] " +
//                    " iStopThis [" + iStopThis + "] " +
//                    " iOverLapSize [" + iOverLapSize + "] "
//            );


        }
        double dOverlapFractionActual = (double) ((double) iOverlapSizeAccumulator / (float) iRangeSizeThis);
        boolean bReturn = false;
        if (dOverlapFractionActual > dThresholdFraction) {
            bReturn = true;
            //api.Log.Log ("flagging and rejecting text as dOverlapFractionActual  [" + dOverlapFractionActual  + "] > dThresholdFraction  [" + dThresholdFraction + "] repeat for doc summ [" + sSnippet + "]" );
            //api.Log.Log ("debugme" );
        }

//        api.Log.Log ("repeat range checking for term [" + sTitleOrSigWord+ "] this start [" + iStartThis+ "] to [" + iStopThis + "] vs already ranges [" + UtilSets.vToStr(vIntRangesAlreadyCoveredStartStopAlternating)+ "] on snippet [" + sSnippet + "]");
//        api.Log.Log ("overlap check final " +
//                "1 iOverlapSizeAccumulator [" + iOverlapSizeAccumulator + "] " +
//                "2 iRangeSizeThis [" + iRangeSizeThis + "] " +
//                "3 iStartThis[" + iStartThis+ "] " +
//                "4 iStopThis [" + iStopThis + "] " +
//                "5 dOverlapFractionActual [" + dOverlapFractionActual + "] " +
//                "6 dThresholdFraction [" + dThresholdFraction + "] "
//        );


        return bReturn;

    }
}
