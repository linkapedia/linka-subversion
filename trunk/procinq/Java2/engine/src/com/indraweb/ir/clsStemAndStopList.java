//clsStopList.java

package com.indraweb.ir;

import com.indraweb.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.Hashtable;


public class clsStemAndStopList {
// to do : uncomment for atlsei 	private static atlsei.IR  x = new atlsei.IR ();

    public static String m_fStemParagraph(String paragraphtoStem) {

        return paragraphtoStem;
    }

    public static String m_fStemWord(String wordToStem) {

        return wordToStem;
    }


    public static Hashtable getHTStopWordList() {
        if (hshtStopList == null) {
            Thread.dumpStack();
            Log.FatalError("hbk null HT");
        }

        if (hshtStopList.size() == 0) {
            Thread.dumpStack();
            throw new RuntimeException("hash table of stop words not initialized");
        }

        return hshtStopList;
    }

    // STEMMED
    public static Hashtable getHTStopWordListStemmed() {
        if (hshtStopListStemmed == null) {
            hshtStopListStemmed = new Hashtable();
            com.indraweb.util.stem.Porter porter = null;
            porter = new com.indraweb.util.stem.Porter();

            Enumeration enumStopWordsNonStemmed = hshtStopList.keys();

            while (enumStopWordsNonStemmed.hasMoreElements()) {
                String s = (String) enumStopWordsNonStemmed.nextElement();
                hshtStopListStemmed.put(porter.stem(s), porter.stem(s));
            }
        }
        return hshtStopListStemmed;
    }


    // STOP LIST BELOW
    private static Hashtable hshtStopList = new Hashtable();
    private static Hashtable hshtStopListStemmed = null;

    Integer I = new Integer(1);

    public clsStemAndStopList(String filename
                              ) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));

            String word_to_add_to_stoplist;

            //com.indraweb.util.stem.Porter porter = null;

            while (in.ready()) {
                word_to_add_to_stoplist = in.readLine();
                if (word_to_add_to_stoplist != null)
                    hshtStopList.put(word_to_add_to_stoplist, I);

            }
            in.close();


            return;
        } catch (FileNotFoundException e) {
            Log.FatalError("FileNotFoundException in Class clsStopList method clsStopList (String filename) file [" +
                    filename + "]", e);
        } catch (java.io.IOException e) {
            Log.FatalError("IOException in Class clsStopList method clsStopList (String filename) file [" +
                    filename + "]", e);
        }

        return;
        //BufferedInputStream bisStopFile = new BufferedInputStream (clsUtils.);

    }
}
