package com.indraweb.signatures;

import java.util.*;
import java.io.*;
import com.indraweb.util.*;
import com.indraweb.execution.*;

public class TestIWvsBNCBasedRemoval
{
	/**
	 * class takes an input file generated by WriteSigWordFreqsFile
	 * and after having ranks added in excel as a function of range 
	 * of count values 
	 * 
	 */
	
	static int IMAX_BNC = 10536; // max count value in sample set 
	static int IMAX_IW = 10540;
	
	public static void TestIWvsBNCBasedRemoval()
	{
		String sFileName= Session.sIndraHome + "/WordFrequencies/TestSigsWithRank_2.txt";
		LineOfSigData[] arrLineOfSigData = fill ( sFileName );

		outputCutOffFile ( Session.sIndraHome + "/WordFrequencies/Compare.txt",
						   arrLineOfSigData);
		
	} // 
	
	private static LineOfSigData[] fill ( String sFileName ) 
	{

		BufferedReader in = null;
		Vector vLineOfSigData = new Vector();
		String textline = null;
		int iLineNum1Based = 1;
		try 
		{ 
			in = new BufferedReader(new FileReader(sFileName));		
				
			while (in.ready()) 
			{
				textline = in.readLine() ; 
				vLineOfSigData.addElement ( new LineOfSigData ( textline ) );
				iLineNum1Based++;
			}
			in.close();
			
			LineOfSigData[] arrLineOfSigData = new LineOfSigData[vLineOfSigData.size()];
			vLineOfSigData.copyInto(arrLineOfSigData);
			
			return arrLineOfSigData;
			
		} 
		catch ( Exception  e ) 
		{
			Log.FatalError ("Exception in enumFileLines [" + textline + "] iLineNum1Based [" + iLineNum1Based + "]\r\n", e ); 
		}		
		return null;
	} 
	
	private static void outputCutOffFile (	String sFileName, 
											LineOfSigData[] arrLineOfSigData )
	{
		PrintWriter pfile = null;
		try
		{
			com.indraweb.util.UtilFile.deleteFile (sFileName);
			pfile = new PrintWriter(new BufferedWriter(new FileWriter(sFileName, true)))	;	
				
			for ( int i = 0; i < arrLineOfSigData.length; i++ )
			{
				
				if ( arrLineOfSigData[i].iSigOccur > 1)
				{
						
					boolean bRemove_BNC = false;
					boolean bRemove_IW = false;
				
					if ( (double) (arrLineOfSigData[i].iBNCAbsRank / (double) IMAX_BNC) > .8 ) 
						bRemove_BNC = true;
					if ( ((double) arrLineOfSigData[i].iIWAbsRank / (double) IMAX_IW) > .8 ) 
						bRemove_IW = true;
				
					String sLineOri = arrLineOfSigData[i].getOriLine();
					if ( bRemove_BNC || bRemove_IW )
					{
						pfile.write (sLineOri + "\t" + 
								 "remove" + "\t" +
								 bRemove_BNC + "\t" +
								 bRemove_IW + "\r\n"
								 );
					} 
					else
					{
						//pfile.write (sLineOri + "\t" + 
								 //"keep" + "\t" +
								 //bRemove_BNC + "\t" +
								 //bRemove_IW + "\r\n"
								 //);
					}
				} // if sig count > 1
			} 
		}
		catch ( Exception e )
		{
			Log.FatalError ("testMsg", e);
		} 
		finally 
		{
			if (pfile	!= null )
				pfile.close();
			} 

	}
}
