package com.indraweb.signatures;

import java.sql.*;
import java.io.*;
import java.util.*;
import com.indraweb.util.*;
import com.indraweb.execution.Session;
import com.indraweb.database.*;
/**
 * class created to see the effect of using corpus-based sig elimination from counts 
 * 
 * 
 */
public class TestSigFiltersOnExistingDBSigs
{
	
	public static void testSigsCountBasedRemoval (  Connection dbc)
		throws Exception
	{
		
		String SQL = "select nodetitle, s.nodeid, signatureword, SIGNATUREOCCURENCES from signature s, node n  " +
					 //" where rownum < 10000 and n.nodeid = s.nodeid " ;
					 " where n.corpusid = 100012 and n.nodeid =  s.nodeid order by s.nodeid DESC " ;
					 //" where n.nodeid = s.nodeid " ;
		
		Log.log ("sql in testSigsForCorpus [" + SQL + "]\r\n" );
		Statement stmt = null;;
		ResultSet rs = null;
		PrintWriter pfile = null;

		Hashtable htWordFreqsICOUNTFILE_MODEL_BNC = null;
		Hashtable htWordFreqsICOUNTFILE_MODEL_IW = null;
		// BNC MODEL
		WordFrequencies wordfreqBNC = new WordFrequencies ( 
			Session.sIndraHome + "/WordFrequencies/" +  "SigWordFreqs_IWModel_Corpus5only.txt",
			0, 
			WordFrequencies.ICOUNTFILE_MODEL_BNC, true );
		htWordFreqsICOUNTFILE_MODEL_BNC = wordfreqBNC.gethtWordCounts_String_to_Integer();
		//wordfreqBNC.printWordsAndWeights ( "#BNC Sorted Stripped Word Count set", 
		//								   Session.sIndraHome + "/WordFrequencies/SigWordFreqs_BNCSorted.txt" ); 

		// INDRAWEB SIG MODEL 
		WordFrequencies wordfreqIW = new WordFrequencies ( 
			Session.sIndraHome + "/WordFrequencies/SigWordFreqs_IWModel_Corpus5only.txt", 
			0, 
			WordFrequencies.ICOUNTFILE_MODEL_IW, false);
		htWordFreqsICOUNTFILE_MODEL_IW = wordfreqIW.gethtWordCounts_String_to_Integer();
		
		
		
		// SEE WHICH OF IW SIG WORDS ARE IN BNC
		{
			String sFileName_IWSigsRelToBNC = Session.sIndraHome + "/WordFrequencies/StopWordCompares_BNCSigsInIW.txt"; 
			UtilFile.deleteFile(sFileName_IWSigsRelToBNC);
			PrintWriter pfile2 = new PrintWriter ( new BufferedWriter( new FileWriter( sFileName_IWSigsRelToBNC, true )));	
			pfile2.write ("words in BNC sig and if found in IW\r\n");
			Enumeration e = wordfreqBNC.gethtWordCounts_String_to_Integer().keys();
			int iNumFound = 0;
			int iNumNotFound = 0;
			while ( e.hasMoreElements() )
			{
				String sBNCSig = (String) e.nextElement();
				int iBNCCount = ((Integer)  wordfreqBNC.gethtWordCounts_String_to_Integer().get (sBNCSig)).intValue();
				if ( wordfreqIW.gethtWordCounts_String_to_Integer().get ( sBNCSig ) == null &&
					 Session.stopList.getHTStopWordList().get (sBNCSig) == null ) 
				{
					if ( iBNCCount> 500 ) 
						pfile2.write (sBNCSig + "\t" + "NOT found\t" + iBNCCount + "\r\n" );
					iNumNotFound++;
				}
				else
				{
					//pfile2.write (sIWSig + "\t" + "found\t" + iIWCount + "\r\n" );
					iNumFound++;
				}
			} 
			pfile2.write ("found [" +iNumFound +"] not found [" + iNumNotFound+ "]\r\n" );
			pfile2.close();
		
		}	

		try 
		{
			String sFileResults = Session.sIndraHome + "/WordFrequencies/" + "testSigsCountBasedRemoval.txt";
				
			com.indraweb.util.UtilFile.deleteFile (sFileResults);
			pfile = new PrintWriter(new BufferedWriter(new FileWriter(sFileResults, true)))	;	
			String	sWord = null;
			String	sNodeTitle = null;
			int		iNodeID = -1;
				
			stmt	= dbc.createStatement ();
			rs		= stmt.executeQuery ( SQL );
			Log.log ("post execute query\r\n" );
			
			int		i = 0;
			pfile.write ("Status\tNodeID\tNodeTopicPath\tNodeCorpusFilePath\tNode Title\tSig Word\tSig Count\tBNC count\tIW count\r\n");
			int iRecordIndex = 0;
			Hashtable htNodeIDToNodePath = new Hashtable();
			//while ( rs.next() && iRecordIndex < 10000  )   // hbk control 
			while ( rs.next() ) 
			{ // nodetitle, nodeid, signatureword, SIGNATUREOCCURENCES
				// RANDOMIZER  
				//int irand = UtilRandom.getRandomInt_Min0_MaxSizeMinus1 ( random, 1000 );
				//if ( irand <= 975 ) 
					//continue;
				
				iRecordIndex++;
				sNodeTitle = rs.getString(1);
				iNodeID = rs.getInt(2);
				
				// NODE FILTER 
				if ( (iNodeID + 3 ) % 1000 != 0 )
					continue;
				
				sWord = rs.getString(3);
				int iSIGNATUREOCCURENCES = rs.getInt(4);
				String sNodeCorpusFilePath = DBAccess_Central_connection.getNodePathCorpusfile ( (long) iNodeID, true, dbc );
				
				Integer INodeID = new Integer ( iNodeID );
				String sNodePath = (String) htNodeIDToNodePath.get ( INodeID ); 
				if ( sNodePath == null )
				{
					sNodePath = DBAccess_Central_connection.getNodeTitle ( true, 
																		   false, 
																		   //false,
																		   //false,
																		  (long) iNodeID, dbc );	
					htNodeIDToNodePath.put ( INodeID, sNodePath );
				} 
				
				Integer IWordCount_BNC = (Integer) htWordFreqsICOUNTFILE_MODEL_BNC.get ( sWord );
				Integer IWordCount_IW = (Integer) htWordFreqsICOUNTFILE_MODEL_IW.get ( sWord );

				String sFound;
				if ( IWordCount_BNC != null || IWordCount_IW != null )
					sFound = "FOUND";
				else
					sFound = "NOT FOUND";
				
				int iBNC = -1; if ( IWordCount_BNC == null ) iBNC = 0; else iBNC = IWordCount_BNC.intValue();
				int iIW = -1; if ( IWordCount_IW == null ) iIW = 0; else iIW = IWordCount_IW.intValue();
				
				pfile.write (	
								sFound + "\t" +
								iNodeID + "\t" + 
								sNodePath + "\t" +  
								sNodeCorpusFilePath + "\t" +  
								sNodeTitle + "\t" + 
								sWord + "\t" + 
								iSIGNATUREOCCURENCES + "\t" + 
								iBNC + "\t" + 
								iIW  
								+ "\r\n");
				
				if ( i % 1000 == 0 )
					Log.log("at DB row [" + i + "]\r\n");
				i++;
			}
		}
		catch ( Exception e )
		{
			Log.FatalError ("error in writeSigsFile()" , e);
			throw e;
		} 
		finally
		{
			if ( stmt != null )
				stmt.close();
			if ( rs != null )
				rs.close();
			if ( pfile != null )
				pfile.close();
		}
		
	} 
}
