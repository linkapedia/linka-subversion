package com.indraweb.signatures;

import java.util.*;
import com.indraweb.util.*;
import com.indraweb.execution.*;

public class LineOfSigData
{
	// from excel file in analysis to decide which sig filter to use - IW sigs (GB) vs BNC
		// 0 NodeID	
		// 1 NodePath	
		// 2 Node Title	
		// 3 Sig Word	
		// 4 Sig Occur	
		// 5 BNC count	
		// 6 BNC abs rank	
		// 7 IW count	
		// 8 IW Abs rank
		
	
			
		public int iNodeID	= -1;
		public String  sNodePath	= null;
		public String  sNodeTitle = null;	
		public String  sSigWord = null;	
		public int  iSigOccur = -1;	
		public int  iBNCcount = -1;
		public int  iBNCAbsRank = -1;	
		public int  iIWCount = -1;	
		public int  iIWAbsRank = -1;
	
	public LineOfSigData ( String line ) 
	{

		Vector vArrValues = com.indraweb.util.UtilStrings.splitByStrLen1 ( line, "\t" )	;
		
		iNodeID	= Integer.parseInt ( (String) vArrValues.elementAt ( 0 ) );
		sNodePath = (String) vArrValues.elementAt ( 1 );
		sNodeTitle = (String) vArrValues.elementAt ( 2 );	
		sSigWord =  (String) vArrValues.elementAt ( 3 );	
		iSigOccur =  Integer.parseInt (  (String) vArrValues.elementAt ( 4 ) );	
		iBNCcount =  Integer.parseInt (  (String) vArrValues.elementAt ( 5 ) );
		iBNCAbsRank = Integer.parseInt (  (String) vArrValues.elementAt ( 6 ) );
		iIWCount = Integer.parseInt (  (String) vArrValues.elementAt ( 7 ) );	
		iIWAbsRank = Integer.parseInt (  (String) vArrValues.elementAt ( 8 ) );
	} 
	
	public String getOriLine ()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("" + iNodeID  + "\t");
		sb.append(sNodePath  + "\t");
		sb.append(sNodeTitle + "\t");
		sb.append(sSigWord  + "\t");
		sb.append(iSigOccur  + "\t");
		sb.append("" + iBNCcount  + "\t");
		sb.append("" + iBNCAbsRank  + "\t");
		sb.append("" + iIWCount + "\t");
		sb.append("" + iIWAbsRank );
		
		return sb.toString();
	
	} 
}
