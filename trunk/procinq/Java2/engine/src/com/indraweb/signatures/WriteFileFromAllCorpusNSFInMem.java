/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 3, 2004
 * Time: 5:17:16 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.signatures;

import com.indraweb.util.UtilFile;
import com.iw.scoring.NodeForScore;
import com.iw.classification.SignatureFile;

import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Hashtable;


public class WriteFileFromAllCorpusNSFInMem
{
    public static synchronized void writeSigFileFromNFSHashInMem ( int iCorpusID,
                                                                   boolean bStem,
                                                                   Hashtable htCorpusIDToHSNodeIDs,
                                                                   Hashtable htNodeIDToNodeForScore
                                                                   ) throws Exception
    {
        long lStart = -1;

        // CREATE SIG WORD FREQ FILE to eliminate some words from sig file
        // if ( false  // hbk control true for production - debug aid for classification speed

        String sfnameSigWordCountsFile = SignatureFile.getFileName ( iCorpusID, bStem );

        boolean bFileExistsPre = UtilFile.bFileExists ( sfnameSigWordCountsFile );
        if ( bFileExistsPre )
            UtilFile.deleteFile ( sfnameSigWordCountsFile );

        PrintWriter pw = null;
        HashSet hsNodeIDsthisCorpus = ( HashSet ) htCorpusIDToHSNodeIDs.get ( new Integer ( iCorpusID ) );
        try
        {
            api.Log.Log ( "build sig file from mem [" + iCorpusID + "] [" + sfnameSigWordCountsFile + "]" );

            lStart = System.currentTimeMillis ();

            // loop thru nodeid, signatureword, SIGNATUREOCCURENCES
            int iNumReadsCompleted = 0;

            pw = new PrintWriter ( new BufferedWriter ( new FileWriter ( sfnameSigWordCountsFile, true ) ) );

            //api.Log.Log ("debug capped iNumNodesEncountered < 1000 ");
            //api.Log.Log ("debug capped iNumNodesEncountered < 1000  ");
            //while (rs.next () && iNumNodesEncountered <= 1000 )
            int iLoop = -1;
            if ( hsNodeIDsthisCorpus != null )
            {
                Iterator iterIntegerNodeIDs = hsNodeIDsthisCorpus.iterator ();
                api.Log.Log ( "genning sig file for [" + hsNodeIDsthisCorpus.size () + "] nodes." );
                while ( iterIntegerNodeIDs.hasNext () )
                {
                    iLoop++;
                    Integer INodeID = ( Integer ) iterIntegerNodeIDs.next ();
                    NodeForScore nfs = ( NodeForScore ) htNodeIDToNodeForScore.get ( INodeID );
                    nfs.emitMeToSigFile ( pw, bStem );

                    iNumReadsCompleted++;

                } // while ( rs.next() )
            } // if there are any nodes
            pw.write ( SignatureFile.CLOSESTRING );

        }
        catch ( Exception e )
        {
            api.Log.LogError( "exception file in createfile corpus [" + iCorpusID + "]\r\n", e );
        }
        finally
        {
            if ( pw != null )
                pw.close ();
        }
        long lEnd = System.currentTimeMillis ();
        api.Log.Log ( "done create sig file SUCCESS [" + iCorpusID + "] #nodes [" + hsNodeIDsthisCorpus.size() + "] ms [" + ( lEnd - lStart ) + "] [" + sfnameSigWordCountsFile + "]" + "" );


    }


}


