package com.indraweb.signatures;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

import com.indraweb.util.UtilStrings;
import com.indraweb.util.UtilStrings;
import com.indraweb.signatures.Signature;

import com.ms.util.ArraySort;

public class IntIDAndDoubleValue
{
	
	public int iColNodeID;
	public int iRowWordID;
 	public int iWordcountInNode; // word in doc count is initial app.
 	public double dWordFreqInNode; // word in doc frequency is initial app.
	
							
	public IntIDAndDoubleValue (int iColNodeID, 
								int iRowWordID, 
								int iWordcountInNode, 
								double dWordFreqInNode) 
	{
		this.iColNodeID = iColNodeID;
		this.iRowWordID = iRowWordID;
		this.iWordcountInNode = iWordcountInNode;
		this.dWordFreqInNode = dWordFreqInNode;
	}

	/**
	 * @param htIntIDAndDoubleValues hashtable of objects of this class type to be 
	 * sorted into an array 
	 * 
	 * @return sorted set
	 */
	// ***************************************************
	public static IntIDAndDoubleValue[] sortVecIntIDAndDoubleValues ( 
		Vector vIntIDAndDoubleValues )
	// ***************************************************
	{
		
		IntIDAndDoubleValue[] arrIntIDAndDoubleValues = new IntIDAndDoubleValue[vIntIDAndDoubleValues.size()];
		
		vIntIDAndDoubleValues.copyInto(arrIntIDAndDoubleValues);
		ArraySort.sort ( arrIntIDAndDoubleValues, new Compar ( Compar.DESC ) );
		return arrIntIDAndDoubleValues;
	} 
	
	/**
	 * @param htIntIDAndDoubleValues hashtable of objects of this class type to be 
	 * sorted into an array 
	 * 
	 * @return sorted set
	 */
	// ***************************************************
	public static void sortArrayIntIDAndDoubleValues ( 
		IntIDAndDoubleValue[] arrIntIDAndDoubleValues )
	// ***************************************************
	{
		ArraySort.sort ( arrIntIDAndDoubleValues, new Compar ( Compar.DESC ) );
	} 
	/**
	 * Class comparator for sorting this class  
	 * 
	 * @author HBK 2002 04 10 
	 */
	// ************************************************************************
	public static class Compar implements com.ms.util.Comparison
	// ************************************************************************
	{
		public final static int ASC  =  1;
		public final static int DESC = -1;
		int order = -2;
		
		// *****************************************
		public Compar ( int order_ )
		// *****************************************
		{
			order = order_;	
		} 

		// *****************************************
		public int compare  (Object o1, Object o2) {
		// *****************************************
			double dv1 = ((IntIDAndDoubleValue)o1).dWordFreqInNode;
			double dv2 = ((IntIDAndDoubleValue)o2).dWordFreqInNode;
			
			if ( dv1 < dv2 ) 
				return -1 * order ;
			else if ( dv2 < dv1 ) 
				return 1 * order ;
			else 
				return  compareOnIDAsBackup (o1, o2);
		} 

		// *****************************************
		public int compareOnIDAsBackup  (Object o1, Object o2) {
		// *****************************************
			
			int id1 = ((IntIDAndDoubleValue)o1).iColNodeID;
			int id2 = ((IntIDAndDoubleValue)o2).iColNodeID;
			if ( id1 < id2 ) 
				return -1 * order ;
			else if ( id1 > id2 ) 
				return 1 * order ;
			else
				return 0;
		} 
		
	}  // public  class comparisonClassStringAndCount

    public String toString()
    {
        StringBuffer sb = new StringBuffer ("iid [");
        sb.append("iColNodeID [" + iColNodeID + "] " );
        sb.append("iRowWordID [" + iRowWordID + "] " );
        sb.append("iWordcountInNode [" + iWordcountInNode + "] " );
        sb.append("dWordFreqInNode [" + dWordFreqInNode + "] " );
        sb.append("]");
        return sb.toString();

    }

    public static String toString (IntIDAndDoubleValue[] iidArr)
    {
        StringBuffer sb = new StringBuffer ("iidarr [\r\n");
        for (int i = 0; i < iidArr.length; i++)
        {
            IntIDAndDoubleValue iid = iidArr[i];
            sb.append(i + ". " + iid.toString() + "\r\n");
            sb.append(i + ". " + iid.toString() + "\r\n");
        }
        sb.append("]");
        return sb.toString();
    }
}
