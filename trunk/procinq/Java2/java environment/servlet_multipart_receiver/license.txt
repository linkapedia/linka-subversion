Copyright (C) 2001 by Jason Hunter <jhunter@servlets.com>.
All rights reserved. 

The source code, object code, and documentation in the com.oreilly.servlet
package is copyright and owned by Jason Hunter. 

Feel free to use the com.oreilly.servlet package in the development of any
non-commercial project. For this use you are granted a non-exclusive,
non-transferable limited license at no cost. (Yep, free.) 

If you are interested in using the package to benefit a commercial project,
you are granted the same license, provided that every person on the
development team for that project owns a copy of the book Java Servlet
Programming (O'Reilly) in its most recent edition. The most recent edition is
currently the 2nd Edition, available in association with Amazon.com at
http://www.amazon.com/exec/obidos/ASIN/0596000405/jasonhunter. 

Redistribution of the com.oreilly.servlet package is permitted provided that
the following conditions are met: 

1. You redistribute the package in object code form only (as Java .class files
or a .jar file containing the .class files) and only as part of a product that
uses the classes as part of its primary functionality. 

2. You reproduce the above copyright notice, this list of conditions, and the
following disclaimer in the documentation and/or other materials provided with
the distribution.  

To clarify, you may use the com.oreilly.servlet package to
build new software and may distribute the package in object code form as part
of this software. You may NOT distribute the package as part of a software
development kit, other library, or development tool without consent of the
copyright holder. Any modified form of the com.oreilly.servlet package is
bound by these same restrictions. (I don't want multiple versions making the
rounds.) 

Other sometimes cheaper license terms are available upon request; please write
to jhunter@servlets.com for more information. 

Note that the com.oreilly.servlet package is provided "as is" and the author
will not be liable for any damages suffered as a result of your use. (You
won't sue me.) Furthermore, you understand the package comes without any
technical support. 

You can always find the latest version of the com.oreilly.servlet package at
http://www.servlets.com. 


THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Thanks,
Jason Hunter <jhunter@servlets.com> 
