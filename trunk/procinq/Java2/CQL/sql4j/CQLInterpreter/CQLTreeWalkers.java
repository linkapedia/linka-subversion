/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Apr 27, 2004
 * Time: 12:47:45 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package sql4j.CQLInterpreter;

import sql4j.parser.*;

import java.util.Vector;
import java.util.HashSet;
import java.util.Hashtable;

import com.indraweb.utils.oracle.CustomFields;

public class CQLTreeWalkers
{
    public static void recurseUpperCaseColumnNames ( WhereCondition wc )
            throws Exception
    {
        if (wc instanceof AtomicWhereCondition)
        {
            AtomicWhereCondition awc = (AtomicWhereCondition) wc;
            Column col1 = awc.getColumn1();
            if ( col1 != null )
            {
                if ( col1.getTableName() != null )
                {
                    col1.setTableName ( col1.getTableName().toUpperCase() );
                    //api.Log.Log ("ucased tab1 [" + col1.getTableName() + "]" );
                }
                if ( col1.getColumnName() != null )
                {
                    col1.setColumnName( col1.getColumnName().toString().toUpperCase() );
                    //api.Log.Log ("ucased col1 [" + col1.getTableName() + "]" );
                }
            }
            Column col2 = awc.getColumn2();
            if ( col2 != null )
            {
                if ( col2.getTableName() != null )
                {
                    col2.setTableName ( col2.getTableName().toUpperCase() );
                    //api.Log.Log ("ucased tab2 [" + col2.getTableName() + "]" );
                }
                if ( col2.getColumnName() != null )
                {
                    col2.setColumnName( col2.getColumnName().toString().toUpperCase() );
                    //api.Log.Log ("ucased col2 [" + col2.getTableName() + "]" );
                }
            }
        }   // atomic where
        else if (wc instanceof CompoundWhereCondition)
        {
            CompoundWhereCondition cwc = (CompoundWhereCondition) wc;
            String sOp = cwc.getOperator ();
            if ( sOp.equals("AND") )
            {

                WhereCondition wcLeft = cwc.getLeft ();
                WhereCondition wcRight = cwc.getRight ();
                recurseUpperCaseColumnNames ( wcLeft );
                recurseUpperCaseColumnNames ( wcRight );
            }
        } // compound where
        else
            throw new Exception ("BQ wc instance of unknown type");

    }
    public static void recurseCollectFullTextWCs ( WhereCondition wc,
                                                   Vector vOutWhereConditions,
                                                   Hashtable htIndexes )
            throws Exception
    {

        boolean bMarkThis = false; // assume not
        if (wc instanceof AtomicWhereCondition)
        {
            if ( isFtLikeWC ( wc, htIndexes ) )
            {
                LikePredicate lp = (LikePredicate) wc;
                String sLikeTarget = lp.getPattern();
                if ( sLikeTarget.indexOf("%%") < 0 )
                    vOutWhereConditions.addElement(wc);
            }
        }   // atomic where
        else if (wc instanceof CompoundWhereCondition)
        {
            CompoundWhereCondition cwc = (CompoundWhereCondition) wc;
            String sOp = cwc.getOperator ();
            if ( sOp.equals("AND") )
            {

                WhereCondition wcLeft = cwc.getLeft ();
                WhereCondition wcRight = cwc.getRight ();
                recurseCollectFullTextWCs ( wcLeft, vOutWhereConditions, htIndexes);
                recurseCollectFullTextWCs ( wcRight, vOutWhereConditions, htIndexes);

            }

        } // compound where

        else
            throw new Exception ("BQ wc instance of unknown type");

        wc.setIfAllUnderAreNodeNegsConnectedByAnd(bMarkThis);
    }



    public static boolean isFtLikeWC ( WhereCondition wc,  Hashtable htIndexes)
    {
        boolean bReturn = false;
        if (wc instanceof AtomicWhereCondition)
        {
            AtomicWhereCondition awc = (AtomicWhereCondition) wc;
            String sAWCgetOp = awc.getOperator(); // will be null for like predicate
            String sAWCgetCol1 = awc.getColumn1().toString();

            if (awc instanceof LikePredicate)
            {
                LikePredicate lpRight = (LikePredicate) awc;
                String slpLabel = lpRight.getLabel();
                if (slpLabel.trim().toLowerCase().equals("like") )
                {
                    if ( CustomFields.isTableFieldIndexed(htIndexes, sAWCgetCol1.trim()))
                        bReturn = true;
                }
                // System.out.println(iCallCounter + ":" + iDepth + ":" +  sLeftRightOrStart + " lpRight  " +                                     " \r\nlpRight.getLabel() [" + lpRight.getLabel() +                                     "] \r\nawc.toString() [" + awc.toString() +                                     "] \r\nawc.getColumn1() [" + awc.getColumn1() +                                    "] \r\nawc.getColumn2() [" + awc.getColumn2() + "]\r\n");
            }
        }
        return bReturn;
    }
}
