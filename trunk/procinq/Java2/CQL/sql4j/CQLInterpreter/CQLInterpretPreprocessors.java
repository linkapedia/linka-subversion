/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Mar 14, 2003
 * Time: 5:00:04 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import java.util.Iterator;
import java.sql.Connection;

public class CQLInterpretPreprocessors
{
    static String convertNearToCodedLike ( String sQuery, Connection dbc ) throws Exception
    {
        Iterator e = CQLSchema.defineWhichColsAreTextIndexed( false, dbc ).iterator();
        while ( e.hasNext() )
        {
            String sCol = (String) e.next();
            sQuery = com.indraweb.util.UtilStrings.replaceStrInStr (
                sQuery, sCol              + " near '", sCol                + " LIKE '##NEARASLIKECODE");
            sQuery = com.indraweb.util.UtilStrings.replaceStrInStr (
                sQuery, sCol.toLowerCase()+ " near '", sCol.toLowerCase()  + " LIKE '##NEARASLIKECODE");
            sQuery = com.indraweb.util.UtilStrings.replaceStrInStr (
                sQuery, sCol              + " NEAR '", sCol                + " LIKE '##NEARASLIKECODE");
            sQuery = com.indraweb.util.UtilStrings.replaceStrInStr (
                sQuery, sCol.toLowerCase()+ " NEAR '", sCol.toLowerCase()  + " LIKE '##NEARASLIKECODE");
        }
        return sQuery;
    }
    static String convertNodeIDUnderToCodedLike ( String sQuery ) throws Exception
    {
        String sNODEIDUNDER = " NODEID UNDER ";
        sQuery = sQuery.replaceAll(" [nN][oO][dD][eE][iI][dD] [uU][nN][dD][eE][rR] ", sNODEIDUNDER);
        String sQueryUpperCase = sQuery.toUpperCase();

        // multipass converter of nodeid under clauses
        if ( sQuery == null ) {
            //sQuery = "select <DOCUMENT> WHERE   NODEID    under     \t   100000  and   DOCUMENTID = 200000";
/*
            sQuery = "select <DOCUMENT> WHERE   NODEID    under     \t   100000  " +
              " or nodeid    UNDER     \t   150000  and    DOCUMENTID = 200000";
*/
            sQuery = "select <DOCUMENT> WHERE   NODEID    under     \t   100000";
            sQuery = CQLInterpret.errorCheckAndNormalizeCQL( sQuery );
        }


        //String sQThvisSourceCase = sQuery;
        // System.out.println( "sQuery pre [" + sQuery + "]" );
        String sbQThisSourceCaseAccumulator = new String();
        int iCurStartPt = 0;
        while ( true )
        {
            int iRelOffset = sQuery.indexOf(sNODEIDUNDER, iCurStartPt );
            if ( iRelOffset < 0 )
                break;
            else
            {
                // ASSUMPTION : IF HAVE nodeid under x  WHERE x IS A DIGIT - ASSUMES THIS IS
                // A GENUINE NODEID UNDER     EG SELECT DOC WHERE FULLTEXT LIKE "NODEID UNDER 1" WOULD BREAK THIS
                int iLocIntNodeID = iRelOffset + sNODEIDUNDER.length();
                char cNodeIDFirstDigitIfReal = sQuery.charAt ( iLocIntNodeID );
                if ( Character.isDigit(cNodeIDFirstDigitIfReal )) // only if it was a real one
                {
                    // append to the accumulator all up to this instance of nodeid under
                    String sPreAmble = sQuery.substring ( 0, iRelOffset +1  ) ;

                    int iEndNodeIDExpression = com.indraweb.util.UtilStrings.findFirstNonDigit ( sQuery,
                            iRelOffset + sNODEIDUNDER.length()  ) ;
                    String sNodeIDUnderExpression = sQuery.substring(iRelOffset, iEndNodeIDExpression );
                    String sRemainder = sQuery.substring (iEndNodeIDExpression);
                    String sIntNodeid = sQuery.substring ( iLocIntNodeID , iEndNodeIDExpression );
                    String sNewNodeUnderExpression = "NODEID LIKE '##NODEIDUNDERASLIKECODE " +
                            sNodeIDUnderExpression.substring (sNODEIDUNDER.length()) + "'";
                    sQuery = sPreAmble + sNewNodeUnderExpression + sRemainder;

                    iCurStartPt = sPreAmble.length() + sNewNodeUnderExpression.length() ;
                    //String sTempStillInPlay = sQuery.substring ( iCurStartPt );
                    //System.out.println("sTempStillInPlay ["+ sTempStillInPlay  + "]");
                } else  {
                    iCurStartPt += iRelOffset + 8; // JUST SKIP THIS SINCE IT DOES NOT END IN A NUM
                }
            }  // WHILE // may be multiple nodeid unders

            iCurStartPt = 0;
            //sQThisSourceCase = sbQThisSourceCaseAccumulator.toString();

        } // for each string case

        //sQuery = com.indraweb.util.UtilStrings.replaceStrInStr (
          //  sQuery, " nodeid under "+ " near '", "NODEID +  LIKE '##NEARASLIKECODE");

        // System.out.println( "sQuery post under check [" + sQuery + "]" );
        return sQuery;
    } // static String convertNodeIDUnderToCodedLike ( String sQuery ) throws Exception


}
