/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: May 23, 2003
 * Time: 8:00:04 AM
 * helper class to ParseTreeAnalyzer
 * to decide about node negative intersect to minus of inverse processing
 */
package sql4j.CQLInterpreter;

import sql4j.parser.WhereCondition;
import sql4j.parser.CompoundWhereCondition;
import sql4j.parser.AtomicWhereCondition;
import sql4j.parser.LikePredicate;

public class NodeNegative
{
    /**
     * step 1 if an atomic node neg is on L (and not on rt) then swap to rt
     * step 2 if, after step 1, the right is a node neg then do double neg. w/
     * a minus (instead of intersect) and Not the not (sic)
     */
    static String analyzeNodeNegs_swapLR_MinusRHS(CompoundWhereCondition cwc)
    {
        //api.Log.Log ("in analyzeNodeNegs_swapLR_MinusRHS");
        String sConnector = "intersect";  // default  hbk 2003 09 29

        boolean bIsAwcNodeNeg_Left = isAtomicNodeNeg(cwc.getLeft(), false);
        boolean bIsAwcNodeNeg_Right = isAtomicNodeNeg(cwc.getRight(), false);

        if (!bIsAwcNodeNeg_Right && bIsAwcNodeNeg_Left)
        {
           // api.Log.Log ("in analyzeNodeNegs_swapLR_MinusRHS swapping");
            WhereCondition wcTempSwap = cwc.getLeft();
            cwc.setLeft(cwc.getRight());
            cwc.setRight(wcTempSwap);

            boolean bIsAwcNodeNeg_tempSwap = bIsAwcNodeNeg_Left;
            bIsAwcNodeNeg_Left = bIsAwcNodeNeg_Right;
            bIsAwcNodeNeg_Right = bIsAwcNodeNeg_tempSwap;
        } else {
           // api.Log.Log ("in analyzeNodeNegs_swapLR_MinusRHS NOT swapping");
        }

        if (bIsAwcNodeNeg_Right)
        {
            sConnector = "minus"; // hbk 2003 09 29
            //sConnector = "and";
            isAtomicNodeNeg(cwc.getRight(), true);
        }

        return sConnector;
    }


    static boolean analyzeNodeNegs_swapLR_OnRHSNodeNegAnd_LHSNot (CompoundWhereCondition cwc)
    {
        boolean bSwapped = false;
        //api.Log.Log ("in analyzeNodeNegs_swapLR_OnRHSNodeNegAnd_LHSNot");
        boolean bIsAwcNodeNeg_Left = cwc.getLeft().getIfAllUnderAreNodeNegsConnectedByAnd();
        boolean bIsAwcNodeNeg_Right = cwc.getRight().getIfAllUnderAreNodeNegsConnectedByAnd();

        //if (!bIsAwcNodeNeg_Right && bIsAwcNodeNeg_Left)
        if (!bIsAwcNodeNeg_Right && bIsAwcNodeNeg_Left)
        {
            //api.Log.Log ("swapping in analyzeNodeNegs_swapLR_OnRHSNodeNegAnd_LHSNot");
            WhereCondition wcTempSwap = cwc.getLeft();
            cwc.setLeft(cwc.getRight());
            cwc.setRight(wcTempSwap);
            bSwapped = true;

        } else {
            //api.Log.Log ("not swapping in analyzeNodeNegs_swapLR_OnRHSNodeNegAnd_LHSNot");
        }
        return bSwapped;

    }

    /**
     * converts some = to like etc
     */
    public static boolean isAtomicNodeNeg ( WhereCondition wc,
                                             boolean bActuallyCompleteTheSettingNegToPos )
    {

        boolean bReturn = false;
        if (wc instanceof AtomicWhereCondition)
        {
            AtomicWhereCondition awc = (AtomicWhereCondition) wc;
            String sAWCgetOp = awc.getOperator(); // will be null for like predicate
            String sAWCgetCol1 = awc.getColumn1().toString();

            if (awc instanceof LikePredicate)
            {
                LikePredicate lpRight = (LikePredicate) awc;
                String slpLabel = lpRight.getLabel();
                if (slpLabel.trim().toLowerCase().equals("not like") &&
                        sAWCgetCol1.trim().toLowerCase().equals("node.nodetitle"))
                {
                    bReturn = true;
                    if (bActuallyCompleteTheSettingNegToPos)
                        lpRight.setLabel("LIKE");

                }
                // System.out.println(iCallCounter + ":" + iDepth + ":" +  sLeftRightOrStart + " lpRight  " +                                     " \r\nlpRight.getLabel() [" + lpRight.getLabel() +                                     "] \r\nawc.toString() [" + awc.toString() +                                     "] \r\nawc.getColumn1() [" + awc.getColumn1() +                                    "] \r\nawc.getColumn2() [" + awc.getColumn2() + "]\r\n");
            } else if ((sAWCgetOp.equals("<>") || sAWCgetOp.equals("!=")) &&
                    (sAWCgetCol1.indexOf("NODETITLE") >= 0 || sAWCgetCol1.indexOf("NODEID") >= 0
                      ))
            {
                bReturn = true;
                if (bActuallyCompleteTheSettingNegToPos)
                    awc.setOperator("=");
            }
        }


        return bReturn;
    }


    private static boolean bDoIHaveDocAndNodeNeg ()
    {
       // boolean bLHSsideHasaDocAndPath
        return true;

    }
}
