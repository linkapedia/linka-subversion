
package sql4j.CQLInterpreter;

import sql4j.parser.*;
import sql4j.schema.*;
import sql4j.util.*;

import java.util.*;

public class BuildWhere_BaseQ
{

    boolean bContainsNodeTableOrCorpusTable;

    static int iCallCounter = 0;

    public static String buildWhere_BaseQRecurse (
            Columns cols_IDS ,
            Schema schema ,
            HashMap hmUnknownCols_StringColToStringSetTables ,
            WhereCondition wc ,
            int iDepth ,
            String sLeftRightOrStart
            // IWFullTextQueryContainer mpqc
            ) throws Exception
    {
/*
        if (sLeftRightOrStart.equals("Start"))
        {
            System.out.println("Start toString() : " + wc.toString());
        }
*/

        iCallCounter++;
        StringBuffer sbCQLSubquery = new StringBuffer ();

        // WALK TREE FOR SUBSUQUERIES
        // start by assuming that doc is in the select list
        // (7)
        if (wc instanceof AtomicWhereCondition)
        {
            AtomicWhereCondition awc = (AtomicWhereCondition) wc;

            // System.out.println("colsUSER_SEL_COLS_IDS [" + colsUSER_SEL_COLS_IDS + "]");
            String sAtomicSubPiece = buildAtomicBaseWherePiece(
                    cols_IDS ,
                    schema ,
                    hmUnknownCols_StringColToStringSetTables ,
                    awc,
                    cols_IDS.getTables()
            );
            // System.out.println(iCallCounter + ":" + iDepth + ":" + sLeftRightOrStart + " AtomicWhereCondition  " +                     " \r\ngetOperator() [" + awc.getOperator() +                     "] \r\nawc.toString() [" + awc.toString() +                     "] \r\nawc.getColumn1() [" + awc.getColumn1() +                     "] \r\nawc.getColumn2() [" + awc.getColumn2() + "]\r\n"             );
            sbCQLSubquery.append (sAtomicSubPiece);
        }   // atomic where
        else if (wc instanceof CompoundWhereCondition)
        {
            CompoundWhereCondition cwc = (CompoundWhereCondition) wc;
            //System.out.println(iCallCounter + ":" + iDepth + ":" + sLeftRightOrStart + " CompoundWhereCondition" + "\r\ncwc.getOperator() [" + cwc.getOperator() + "]\r\n" + " toString() [" + cwc.toString() + "]\r\n" );

            String sOp = cwc.getOperator ();
            // if ( sOp.equals("AND") && exists_anyNodeTableSpecific_Or_CorpusConstraintsBelow ( wc ) ) {

            if (sOp.equals ("AND") || sOp.equals ("OR"))
            {
                String sConnector = null;
                if (sOp.equals ("AND"))
                    //sConnector = NodeNegative.analyzeNodeNegs_swapLR_MinusRHS (cwc);
                    sConnector = "and";
                else if (sOp.equals ("OR"))
                //sConnector = "union"; // hbk 2003 09 29
                    sConnector = "or";

                //System.out.println(iCallCounter + ":" + iDepth + " wcLeft.getClass().getName() " + wcLeft.getClass().getName() + " wcLeft.toString() [" + wcLeft.toString() + " lp.getOperator [" + lp.getOperator() + "] " + " lp.getPattern [" + lp.getPattern() + "] "
                sbCQLSubquery.append ("(" + buildWhere_BaseQRecurse (cols_IDS , schema  ,
                        hmUnknownCols_StringColToStringSetTables ,
                        cwc.getLeft () , iDepth + 1 , "L") + ") ");
                sbCQLSubquery.append (sConnector);// union or intersection

                //System.out.println(iCallCounter + ":" + iDepth + " wcRight.getClass().getName() " + wcRight.getClass().getName() + " wcRight.toString() [" + wcRight.toString() + "]");
                sbCQLSubquery.append (" (" + buildWhere_BaseQRecurse (cols_IDS , schema ,
                        hmUnknownCols_StringColToStringSetTables ,
                        cwc.getRight () , iDepth + 1 , "R") + ")");


            }
            else
                throw new Exception ("BQ compound where condition not sOp.equals('AND') || sOp.equals('OR') [" + sOp + "]");


        } // compound where
        else
            throw new Exception ("BQ wc instance of unknown type");

        //api.Log.Log ("BQ sbCQLSubquery [" + sbCQLSubquery + "]");
        return sbCQLSubquery.toString ();

    }


    /**
     * ATOMIC
     * build select IDs from tables where atomic - for intersect and union construction
     */
    private static String buildAtomicBaseWherePiece (
            Columns cols_IDS , // may be ID's only
            Schema schema , // contains known col to table mappings
            HashMap hmUnknownCols_StringColToStringSetTables ,
            AtomicWhereCondition awc,
            Tables tablesUserSel_unresolved
            ) throws Exception
    {
/* hbk 2003 09 29
        //api.Log.Log ("awc.toPrint() [" + awc.toPrint() + "]" );
        api.Log.Log ("111 BQ cols_IDS 1 [" + cols_IDS + "]");
        cols_IDS = cols_IDS.getDeepCopy ();
        api.Log.Log ("112 BQ cols_IDS.toString() 2 [" + cols_IDS.toString () + "]");
        api.Log.Log ("112.5 BQ UserSelTables_unresolved.toString 2 [" + UserSelTables_unresolved.toString () + "]");
        cols_IDS.resetTables (schema); // (5)
*/

// RESOLVE UNKNOWN USER SEL COLS IDS TABS
/* hbk 2003 09 29
        sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown (// side effect - set tables in Cols arg
                cols_IDS ,
                hmUnknownCols_StringColToStringSetTables);
*/
/*            hbk 2003 09 29
        api.Log.Log ("113 BQ post resolve cols_IDS 3 [" + cols_IDS + "]");
        Tables tabs_FromIDcols = cols_IDS.getTables ();
        api.Log.Log ("114 BQ tabs_FromIDcols 4 [" + tabs_FromIDcols + "]");
*/
// (6)
        Columns colsBaseWhere = awc.getColumns ();
        //api.Log.Log ("115 BQ colsBaseWhere 5 [" + colsBaseWhere+ "]");
        sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown (// side effect - set tables in Cols arg
                colsBaseWhere,
                hmUnknownCols_StringColToStringSetTables,
                tablesUserSel_unresolved);
        //api.Log.Log ("116 BQ colsUCSUBTREE 6  [" + colsBaseWhere + "]");
        Tables tabsBaseWhere = colsBaseWhere.getTables ();
        //api.Log.Log ("117 BQ tabsBaseWhere 7 [" + tabsBaseWhere + "]");

// (7)
        Tables tabsUSER_SEL_COLS_IDS_TABS = cols_IDS.getTables (); // still known and unknown
        //api.Log.Log ("118 BQ tabsUSER_SEL_COLS_IDS_TABS 8 [" + tabsUSER_SEL_COLS_IDS_TABS + "]");

// (8) union 6 and 7
        Tables tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS = new Tables ();
        //api.Log.Log ("119 BQ tabsUCSUBTREE_TABS 9 [" + tabsBaseWhere + "]");

        //api.Log.Log ("120 BQ tabsUSER_SEL_COLS_IDS_TABS 10 [" + tabsUSER_SEL_COLS_IDS_TABS + "]");
        tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS.addAll (tabsBaseWhere);
        //api.Log.Log ("121 BQ tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS 11 [" + tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS + "]");
        tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS.addAll (tabsUSER_SEL_COLS_IDS_TABS);
        //api.Log.Log ("122 BQ tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS 12 [" + tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS + "]");
// (8.5)
        Tables tabsUSER_SEL_AND_SUBTREE_IMPLIED_JOINS_TABS = CQLInterpreterUtils.getExtendToIncludeJoinTabs(
                tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS, false);
        //api.Log.Log ("123 BQ tabsUSER_SEL_AND_SUBTREE_IMPLIED_JOINS_TABS 13 [" + tabsUSER_SEL_AND_SUBTREE_IMPLIED_JOINS_TABS + "]");

        Tables tabsAllThisWhereInclSelList = new Tables ();
        tabsAllThisWhereInclSelList.addAll (tabsJOIN_TABS_UCSUBTREE_AND_USER_SEL_COLS_IDS);
        tabsAllThisWhereInclSelList.addAll (tabsUSER_SEL_AND_SUBTREE_IMPLIED_JOINS_TABS);
        //api.Log.Log ("124 BQ tabsAllThisSubSelect [" + tabsAllThisWhereInclSelList + "]");

// RESOLVE UNKNOWN TABS in (8)
        sql4j.CQLInterpreter.CQLInterpreterUtils.resolveColumnsUnknown (// side effect - set tables in Cols arg
                cols_IDS ,
                hmUnknownCols_StringColToStringSetTables,
                tabsAllThisWhereInclSelList);

// System.out.println("BQ colsUSER_SEL_COLS_IDS_NEW_TAB_ASSIGNMENT resolved [" + colsUSER_SEL_COLS_IDS_NEW_TAB_ASSIGNMENT + "]");
// (9)
/* hbk 2003 09 29
        String sAllTabsJoins =
                sql4j.CQLInterpreter.CQLInterpreterUtils.buildJoinStrings (tabsAllThisSubSelect);
*/
// System.out.println("BQ sAllTabsJoins [" + sAllTabsJoins + "]");
        String sAWC = awc.toString ();
//System.out.println("BQ sAWC [" + sAWC + "]");
        Tables tabsFINAL_USER_SEL_AND_JOINS_RESOLVED = cols_IDS.getTables ();
        String sWC = awc.toString ();


        // api.Log.Log ("BQ sbFinal4[" + sAWC + "]");
        return sAWC;
    }
}

