/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Aug 23, 2004
 * Time: 8:59:35 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import com.indraweb.util.sort.IComparator;
import com.indraweb.gfs.Comparator_double;

public class Comparator_dataDetailRecord implements IComparator
{
	public int compare(Object o1, Object o2)
	{
        return Comparator_double.compare(((dataDetailRecord_DocScore) o1).dAggScore , ((dataDetailRecord_DocScore) o2).dAggScore );
	}
}
