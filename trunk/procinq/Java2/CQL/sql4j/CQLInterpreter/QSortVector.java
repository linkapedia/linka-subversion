/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Nov 13, 2002
 * Time: 2:59:45 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package sql4j.CQLInterpreter;

import java.util.Vector;

public class QSortVector {

    public static void qs(Vector v) {

       Vector vl = new Vector();                      // Left and right sides
       Vector vr = new Vector();
       dataDetailRecord_DocScore el;
       double key;                                    // key for splitting

       if (v.size() < 2) return;                      // 0 or 1= sorted

       dataDetailRecord_DocScore ddrds = (dataDetailRecord_DocScore) v.elementAt(0);
       key = ddrds.dAggScore;               // 1st element key

       // Start at element 1
       for (int i=1; i < v.size(); i++) {
          el = (dataDetailRecord_DocScore) v.elementAt(i);
          if (el.dAggScore >= key) vr.addElement(el);// Add to right
          else vl.addElement(el);                     // Else add to left
       }

       qs(vl);                                        // Recursive call left
       qs(vr);                                        //    "        "  right
       vl.addElement(v.elementAt(0));

       addVect(v, vl, vr);

     }

    private static void addVect( Vector dest, Vector left, Vector right ) {

	   int i;
	   dest.removeAllElements();                     // reset destination

	   for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
	   for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
	}



}
