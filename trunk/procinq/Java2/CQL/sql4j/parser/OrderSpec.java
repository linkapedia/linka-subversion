package sql4j.parser;

import sql4j.util.StringSet;

/**
 * Insert the type's description here.
 * Creation date: (12/17/2001 11:38:18 AM)
 * @author: Administrator
 */

public class OrderSpec {
    static StringSet ssetCols = new StringSet ();
	String order;
/**
 * OrderSpec constructor comment.
 */
public OrderSpec() {
	super();
}
	public OrderSpec(Column e1, String e2){
            //System.out.println("orderspec [" + e1 + "] [" + e2 + "]");
        try {
           if ( e2 == null )
            ssetCols.add(e1.toString());
           else
           {
                if (!( e2.toString().trim().toLowerCase().equals("asc") ||
                       e2.toString().trim().toLowerCase().equals("desc")))
                    throw new Exception ("invalid orderspec [" + e1 + "] [" + e2 + "]");
               else
                ssetCols.add(e1.toString() + " " + e2.toString());
           }

        }
        catch ( Exception e )
        {
            com.indraweb.util.Log.NonFatalError("error in order spec",e);
            System.out.println ("error in order spec");
        }
	}

    // violates multithreadednes ! HACK HACK HACK - couldn't find where the parser keeps the OrderSpec objects inside
    public static StringSet getCols()
    {
        StringSet ssColsReturn = ssetCols;
        if ( ssetCols.size() > 0 )
            ssetCols = new StringSet();

        return ssColsReturn;
    }
}