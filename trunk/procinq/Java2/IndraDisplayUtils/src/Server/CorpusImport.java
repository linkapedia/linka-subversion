package Server;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;

import HTML.*;
import Logging.*;

public class CorpusImport
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
        // Note: corpus import is now using HK's new execute class.
		int rval = 0;
		
		try {
			// is the content type correct?
			String sContentType = (String) props.get ("Content-type");	
			String sFileName = (String) props.get ("import");	
			
			if (!sContentType.equals("text/xml")) {
				throw new Exception("Sorry, the file you have selected is not a valid corpus file.");
			}

			Execute exec = new Execute();
			rval = exec.CorpusExec(sFileName, out);
		}
		catch (Exception except) {
			HTML.HTMLDocument Document = new HTML.HTMLDocument();
			Log.LogError(except, out);
			Document.WriteError(out, except.toString());
			return;
		}
		
		if (rval != -1) {
			HTMLDocument Document = new HTMLDocument();
			Document.WriteSuccess(out, "<!-- "+rval+" -->\n Your corpus import has completed successfully.<P>"+
								   "Click <a href='/servlet/Main?template=Taxonomy.CorpusAdminister'>here "+
								   "to return</a> to the Corpus Administration menu.");	
		}
	}

}
