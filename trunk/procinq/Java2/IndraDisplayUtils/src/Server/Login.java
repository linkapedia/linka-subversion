package Server;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.reflect.*;
import java.net.URLEncoder;

import HTML.*;
import Logging.APIProps;
import Logging.Log;
import com.iw.system.*;

public class Login
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sUserID = (String) props.get ("UserID");
		String sPassword = (String) props.get ("Password");
		String sPageToDisplay = (String) props.get ("Page");

        if (sPageToDisplay == null) sPageToDisplay = com.indraweb.execution.Session.cfg.getProp ("FrontPage", false, null);

		if ((sUserID == null) || (sPassword == null)) {
            if (sPageToDisplay == null) { sPageToDisplay = "Taxonomy.FrontPage"; }
			try {
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Server Login");
                Document.AddVariable("Page", sPageToDisplay);
                Document.AddVariable("Action", req.getServletPath());
				Document.WriteTemplate(out, "header-admin-login.tpl");
				Document.WriteTemplate(out, "login.tpl");
				Document.WriteFooter(out);
			} catch (Exception e) {
				HTMLDocument Document = new HTMLDocument();
				String sErrorMsg = e.getMessage();
				Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
				Log.LogError(e, out);
			}
		} else {
			try {
				// LoginName [sn=##USERNAME##,ou=users,dc=indraweb,dc=com]
				// UserID will only be a part of the name, the rest is stored in the config file.
				String sLoginName = com.indraweb.execution.Session.cfg.getProp ("LoginName");

				if (!com.indraweb.execution.Session.cfg.getProp("AuthenticationSystem").equals("ActiveDirectory")) {
					sUserID = com.indraweb.util.UtilStrings.replaceStrInStr(sLoginName, "##USERNAME##", sUserID);
				}

				// Call: Login
				HashTree htArguments = new HashTree();
				htArguments.put("UserID", sUserID);
				htArguments.put("Password", sPassword);
				InvokeAPI API = new InvokeAPI ("security.TSLogin", htArguments);
				HashTree htResults = API.Execute(false, false);

				// A success is indicated if there is no TSERROR tag in the return
				if (!htResults.containsKey("SUBSCRIBER")) {
					out.println("<!-- No Subscriber -->");
					throw new Exception("You have entered an invalid username, password combination.<P>"+
						"Please hit your <b>back</b> button and try again. (no subscriber)");
				}

				HashTree htUser = (HashTree) htResults.get("SUBSCRIBER");

				if (!htUser.containsKey("KEY")) {
					out.println("<!-- No Key -->");
					throw new Exception("<br>You have entered an invalid username, password combination.<P>"+
						"Please hit your <b>back</b> button and try again. (no key)");
				}

				String SKEY = (String) htUser.get("KEY");
				String sID = (String) htUser.get("ID");
				String sResultsPerPage = (String) htUser.get("RESULTSPERPAGE");
				String sScoreThreshold = (String) htUser.get("SCORETHRESHOLD");
                String sEmail = (String) htUser.get("EMAIL");
                String sName = (String) htUser.get("NAME");

				// Write the session information into the user's cookie
				Cookie cSessionCookie = new Cookie("SKEY", SKEY);
				cSessionCookie.setMaxAge(6592000);

				Cookie cSessionCookie2 = new Cookie("RESULTSPERPAGE", sResultsPerPage);
				cSessionCookie2.setMaxAge(6592000);

				sID = sID.replace(',','|');
				Cookie cSessionCookie3 = new Cookie("ID", sID);
				cSessionCookie3.setMaxAge(6592000);

				Cookie cSessionCookie4 = new Cookie("SCORETHRESHOLD", sScoreThreshold);
				cSessionCookie4.setMaxAge(6592000);

                Cookie cSessionCookie6 = new Cookie("EMAIL", "null");
                if (!sEmail.equals("null")) { cSessionCookie6 = new Cookie("EMAIL", sEmail); }
                cSessionCookie6.setMaxAge(6592000);

                Cookie cSessionCookie7 = new Cookie("NAME", "null");
                if (!sEmail.equals("null")) { cSessionCookie7 = new Cookie("NAME", sName); }
                cSessionCookie7.setMaxAge(6592000);

                ITS its = new ITS(SKEY);
                props.put("SKEY", SKEY);

				// Get the corpus list and write into the cookie
                Vector v = new Vector();
                its.getCorpora();
				String sOPTION_CORPUS_LIST = "";

				if (v.size() > 0) {
                    for (int i = 0; i < v.size(); i++) {
                        Corpus c = (Corpus) v.elementAt(i);

						if (new Integer(c.getActive()).intValue() == 1) {
							if (c.getName().length() > 25) { c.setName(c.getName().substring(0,22)+"..."); }
							sOPTION_CORPUS_LIST=sOPTION_CORPUS_LIST+"<option value="+c.getID()+"> "+URLEncoder.encode(c.getName(),"UTF-8") ;
						}
					}
					Cookie cSessionCookie5 = new Cookie("OPTION_CORPUS_LIST", URLEncoder.encode(sOPTION_CORPUS_LIST, "UTF-8"));
					cSessionCookie5.setMaxAge(6592000);
					res.addCookie(cSessionCookie5);
				}

				res.addCookie(cSessionCookie);
				res.addCookie(cSessionCookie2);
				res.addCookie(cSessionCookie3);
				res.addCookie(cSessionCookie4);
                res.addCookie(cSessionCookie6);

				if (sPageToDisplay == null) {
					Taxonomy.FrontPage.handleTSapiRequest(props, out, req, res);
				} else {
                    Class cProperties = Class.forName ("Logging.APIProps");
                    Class cHttpServletRequest = Class.forName ("javax.servlet.http.HttpServletRequest");
                    Class cHttpServletResponse = Class.forName ("javax.servlet.http.HttpServletResponse");
                    Class cPrintWriter = Class.forName ("java.io.PrintWriter");

					Class[] cArr = { cProperties, cPrintWriter, cHttpServletRequest, cHttpServletResponse };

					Class cTSapi = Class.forName (sPageToDisplay);
					Method method = cTSapi.getMethod( "handleTSapiRequest", cArr );

					Object[] oArr = { props, out, req, res};
					method.invoke ( cTSapi, oArr );
				}

				/*
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "Thank you. You are now successfully logged into the Taxonomy Server. "+
								  "Note if you are logging into the system from <br>a public terminal, please "+
								  "remember to log out when you are done.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.FrontPage'>"+
								  "Click here to begin your session</a>. " ;
				Document.WriteSuccess(out, sSuccess);
				*/

			} catch (Exception e) {
				HTMLDocument Document = new HTMLDocument();
				String sErrorMsg = e.getMessage();
				Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
				Log.LogError(e, out);
			}
		}

	}
}
