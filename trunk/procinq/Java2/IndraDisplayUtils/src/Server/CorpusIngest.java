package Server;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.oreilly.servlet.MultipartRequest;

import HTML.*;
import Logging.*;
import com.iw.system.*;

public class CorpusIngest
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		int rval = 0;
		
		try {
			String submit = (String) props.get("submit");
			
			if (submit != null) {
				// is the content type correct?
				String sFileLocation = (String) props.get ("filename");	
				String sCorpusName = (String) props.get ("corpusname");	
	
				// Call: Ingest the corpus located at "filename"
				HashTree htArguments = new HashTree(props);
				htArguments.put("CorpusFolder", sFileLocation);
				htArguments.put("CorpusName", sCorpusName);
				htArguments.put("CorpusActive", "1");
				InvokeAPI API = new InvokeAPI ("tscorpus.TSInstallCorpus", htArguments);
				HashTree htResults = API.Execute(false, false);
				
				HTMLDocument Document = new HTMLDocument();
				Document.WriteSuccess(out, "Your corpus ingestion is complete.  <P>"+
										   "Click <a href='/servlet/Main?template=Taxonomy.CorpusAdminister'>here "+
										   "</a> to go to the Corpus Administration menu.");	
			} else {
				HTMLDocument Document = new HTMLDocument();
				Document.WriteTemplate(out, "header-admin.tpl");
				Document.WriteTemplate(out, "import.tpl");
				Document.WriteFooter(out);
			}
		} catch (Exception except) {
			HTML.HTMLDocument Document = new HTML.HTMLDocument();
			Log.LogError(except, out);
			Document.WriteError(out, except.toString());
			return;
		}
	}
}
