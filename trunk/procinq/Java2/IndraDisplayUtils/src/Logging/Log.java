package Logging;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.indraweb.execution.Session;
import com.indraweb.execution.ThreadInfoTestAssertion;

// This class manages logging to the JRUN logs located in the $JRUN/logs directory
public class Log  { 

    private static Logger logger = Logger.getLogger("log");
    private static String loggerInitDummy = cfgLogger();
    private static String cfgLogger() {
        if ( Session.sIndraHome == null ) {
            System.out.println("Fatal Error: Session.sIndraHome == null in UI.cfgLogger");
        } else {
            PropertyConfigurator.configure(Session.sIndraHome + "/ITSLogConfig.txt");
            System.out.println("UI completed log cfg [" + Session.sIndraHome + "/ITSLogConfig.txt]");
        }
        return "1";
    }
	// Log output to JRUN logs
	public static void Log (String Message) {
        logger.info ("UI: "+Message);
    }

	// Log warning or fatal error to JRUN logs
	public static void LogError (String Message) { logger.error("UI WARNING: "+Message); }
	public static void LogError (String Message, Exception e) {
        logger.error (" UI EXCEPTION ERROR: "+Message, e);
        ThreadInfoTestAssertion.captureErrorPut("Log.LogError time [" + new java.util.Date() +
                "] UI EXCEPTION ERROR Logging.Log.Logerror() [" + stackTraceToString(e) + "]");
	}
	public static void LogError (String Message, Exception e, PrintWriter out) {
        logger.error (" UI EXCEPTION ERROR: "+Message, e);
        String sStack = stackTraceToString(e);
        out.println(new Date()+" <!-- ");
        out.println( sStack );
        out.println("--> ");
	}
	public static void LogError (Exception e) {
        logger.error (" UI EXCEPTION ERROR ", e);
        ThreadInfoTestAssertion.captureErrorPut("Logging.Log.LogError time [" + new java.util.Date() +
                "] UI EXCEPTION ERROR Logging.Log.Logerror() [" + stackTraceToString(e) + "]");
	}
	public static void LogError (Exception e, PrintWriter out) {
        logger.error (" UI EXCEPTION ERROR ", e);
        String sStack = stackTraceToString(e);
        ThreadInfoTestAssertion.captureErrorPut("Logging.Log.FatalError2 time [" + new java.util.Date() +
                "] UI EXCEPTION ERROR Logging.Log.Logerror() [" + sStack  + "]");
        out.println(new Date()+" <!-- ");
        out.println( sStack );
        out.println("--> ");
	}
	
	// Fatal error
	public static void LogFatal (String Message) { 
        logger.fatal(" UI FATAL ERROR: "+Message);
        ThreadInfoTestAssertion.captureErrorPut("Logging.Log.LogFatal time [" + new java.util.Date() +
                "] UI FATAL ERROR [" + Message + "]");
	}
	public static void LogFatal (String Message, Exception e) {
        logger.fatal(" UI FATAL ERROR: "+Message, e);
        logger.fatal(" UI FATAL ERROR: "+Message, e);
        ThreadInfoTestAssertion.captureErrorPut("Logging.Log.LogFatal time [" + new java.util.Date() +
                "] UI FATAL ERROR [" + Message + "] [" + stackTraceToString(e) + "]");
	}
	public static void LogFatal (String Message, Exception e, PrintWriter out) {
        logger.fatal(" UI FATAL ERROR ", e);
        logger.fatal(" UI FATAL ERROR ", e);
        String sStack = stackTraceToString(e);
        out.println(new Date()+" <!-- ");
        out.println( sStack );
        out.println("--> ");
        ThreadInfoTestAssertion.captureErrorPut("Logging.Log.LogFatal time [" + new java.util.Date() +
                "] UI FATAL ERROR [" + Message + "] [" + stackTraceToString(e) + "]");
	}
	public static void LogFatal (String Message, Throwable e) {
        logger.fatal(" UI FATAL ERROR: " + Message, e);
	}
	public static void LogFatal (String Message, Throwable e, PrintWriter out) {
        logger.error (" UI FATAL ERROR: " + Message, e);
        String sStack = stackTraceToString(e);
        out.println(new Date()+" <!-- ");
        out.println( sStack );
        out.println("--> ");
        ThreadInfoTestAssertion.captureErrorPut("Logging.Log.LogFatal time [" + new java.util.Date() +
                "] UI FATAL ERROR [" + Message + "] [" + stackTraceToString(e) + "]");
	}
	
	// Get the stacktrace of an exception into a string
	public static String stackTraceToString ( Throwable e ) {
		try {
			StringWriter sw = new StringWriter ( );
			PrintWriter pw = new PrintWriter ( sw );
			e.printStackTrace ( pw ) ;
			return "StackTRACE [" + sw.toString ( ) + " ]";
		} catch ( Exception e2 ) { return "bad stack2string"; }
	}
}
