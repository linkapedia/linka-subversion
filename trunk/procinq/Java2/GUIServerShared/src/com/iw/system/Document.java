package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import java.util.*;
import java.net.URLEncoder;
import java.io.PrintWriter;
import java.sql.*;

import com.iw.system.*;
import org.dom4j.Element;

public class Document implements Comparable {
    public Hashtable htd = new Hashtable();

    // constructor(s)
    public Document () {}
    public Document (HashTree ht) {
        htd = new Hashtable();

        Enumeration eN = ht.keys();
        while (eN.hasMoreElements()) {
            String key = (String) eN.nextElement();
            String val = (String) ht.get(key);

            htd.put(key, val);
        }
    }

    public Document (Vector v) {
        for (int i = 0; i < v.size(); i++) {
            VectorValue vv = (VectorValue) v.elementAt(i);
            htd.put(vv.getName(), vv.getValue());
        }
    }
    public Document (Iterator i) {
        if (htd == null) htd = new Hashtable();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            htd.put(e.getName(), e.getText());
        }
    }

    public Document (ResultSet rs ) throws Exception {
        ResultSetMetaData rsmd = rs.getMetaData();

        for (int i = 1; i <= rsmd.getColumnCount(); i++)
            if (rs.getString(i) != null)
                htd.put(rsmd.getColumnName(i), rs.getString(i));
            else htd.put(rsmd.getColumnName(i), "");
    }

    public static String FixDate (String Date) {
        if (Date.charAt(2) != '-') {
            // change from: 2003-07-24+07:55:35 to 24-07-2003+07:55:35
            return Date.substring(8, 10)+"-"+Date.substring(5, 7)+"-"+Date.substring(0, 4)+Date.substring(10);
        } else { return Date; }
    }

    public boolean isAuthorized(User u) {
        String sDocumentSecurity = get("DOCUMENTSECURITY");
        if (sDocumentSecurity == null) return false;

        return u.IsAuthorized(sDocumentSecurity);
    }

    // 04/28/04 -- use this function to enable DOC security on emit
    public boolean emitXML(PrintWriter out, User u, boolean WithDocWrap) {
        boolean bDocAuthorized = u.IsAuthorized(get("DOCUMENTSECURITY"));

        if (bDocAuthorized) {
            if (WithDocWrap) { out.println("<DOCUMENT>"); }

            if ( htd != null ) {
                Enumeration enumCustomColnames = htd.keys();
                while ( enumCustomColnames.hasMoreElements() )
                {
                    String sColName = (String) enumCustomColnames.nextElement();
                    out.println("   <"+sColName+"><![CDATA[" + htd.get(sColName) + "]]></"+sColName+">");
                }
            }

            if (WithDocWrap) { out.println("</DOCUMENT>"); }
        }
        return bDocAuthorized;
    }

    public static String getSQL(Connection dbc) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("DOCUMENT", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            if (loop == 1) sSQL = sSQL + "d." + inf.getName();
            else sSQL = sSQL + ", d." + inf.getName();
        }

        sSQL = sSQL + " from Document d";

        return sSQL;
    }
    public static String getSQLwithoutTable(Connection dbc) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("DOCUMENT", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            if (loop == 1) sSQL = sSQL + "d." + inf.getName();
            else sSQL = sSQL + ", d." + inf.getName();
        }

        return sSQL;
    }
    public static String getSQL(Connection dbc, String dateformat) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("DOCUMENT", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            String sField = "d."+inf.getName();
            if (inf.getType().equals("DATE")) {
                sField = "to_char("+sField+", '"+dateformat+"')";
            }

            if (loop == 1) sSQL = sSQL + "d." + sField;
            else sSQL = sSQL + ", d." + sField;
        }

        sSQL = sSQL + " from Document d";

        return sSQL;
    }
    public static String getSQLwithoutTable(Connection dbc, String dateformat) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("DOCUMENT", dbc);

        String sSQL = "select "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            String sField = "d."+inf.getName();
            if (inf.getType().equals("DATE")) {
                sField = "to_char("+sField+", '"+dateformat+"')";
            }

            if (loop == 1) sSQL = sSQL + "d." + sField;
            else sSQL = sSQL + ", d." + sField;
        }

        return sSQL;
    }
    public static String getGroupBy(Connection dbc) {
        Hashtable ht = com.iw.system.FieldData.getFieldData("DOCUMENT", dbc);

        String sSQL = "group by "; int loop = 0;
        Enumeration eH = ht.elements();

        while (eH.hasMoreElements()) {
            loop++; IndraField inf = (IndraField) eH.nextElement();

            if (loop == 1) sSQL = sSQL + "d." + inf.getName();
            else sSQL = sSQL + ", d." + inf.getName();
        }

        return sSQL;
    }

    // accessor function -- 5/13/2004 only one accessor function required
    public String get(String fieldName) {
        String s = (String) htd.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }
        return s.replaceAll(" <br> ", "\n");
    }

    public void set (String fieldName, String fieldValue) { htd.put(fieldName, fieldValue); }

    // accessor functions with encoding
    public String getEncodedField(String fieldName) throws Exception {
        return URLEncoder.encode(get(fieldName), "UTF-8"); }

    public String toString() {
        String s = get("DOCTITLE"); if (s == null) return super.toString();
        return s;
    }
    public int compareTo(Object o) {
        String s = get("DOCTITLE"); if (s == null) return super.toString().compareTo(o.toString());
        return s.compareTo(o.toString());
    }
}
