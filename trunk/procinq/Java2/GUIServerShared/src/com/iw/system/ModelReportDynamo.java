package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@XmlRootElement(name = "REPORT")
public class ModelReportDynamo {

    private String nodeCount;
    private List<ModelNodeReportDynamo> nodes;

    @XmlElement(type = String.class)
    public String getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(String nodeCount) {
        this.nodeCount = nodeCount;
    }

    @XmlElement(type = ModelNodeReportDynamo.class)
    public List<ModelNodeReportDynamo> getNodes() {
        return nodes;
    }

    public void setNodes(List<ModelNodeReportDynamo> nodes) {
        this.nodes = nodes;
    }
}
