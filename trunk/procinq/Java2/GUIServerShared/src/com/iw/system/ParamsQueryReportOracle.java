package com.iw.system;

/**
 *
 * @author andres
 */
public class ParamsQueryReportOracle extends BaseParamsQueryReport {

    //report 1 filter
    private String lessSignature;
    private String lessMusthave;
    private String moreSignature;
    private String moreMusthave;
    //report 2 filter
    private String lessDocuments;
    private String moreDocuments;

    public String getLessSignature() {
        return lessSignature;
    }

    public void setLessSignature(String lessSignature) {
        this.lessSignature = lessSignature;
    }

    public String getLessMusthave() {
        return lessMusthave;
    }

    public void setLessMusthave(String lessMusthave) {
        this.lessMusthave = lessMusthave;
    }

    public String getMoreSignature() {
        return moreSignature;
    }

    public void setMoreSignature(String moreSignature) {
        this.moreSignature = moreSignature;
    }

    public String getMoreMusthave() {
        return moreMusthave;
    }

    public void setMoreMusthave(String moreMusthave) {
        this.moreMusthave = moreMusthave;
    }

    public String getLessDocuments() {
        return lessDocuments;
    }

    public void setLessDocuments(String lessDocuments) {
        this.lessDocuments = lessDocuments;
    }

    public String getMoreDocuments() {
        return moreDocuments;
    }

    public void setMoreDocuments(String moreDocuments) {
        this.moreDocuments = moreDocuments;
    }
}