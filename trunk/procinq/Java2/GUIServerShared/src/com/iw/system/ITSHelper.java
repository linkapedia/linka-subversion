
package com.iw.system;

import com.iw.system.Signatures;
import com.iw.system.Signature;

import java.util.Vector;

public class ITSHelper
{
    public static Signatures vSignatureObjectsToSignaturesObject (Vector vSignatureObjects)
    {
        Signatures signatures = new Signatures();
        for ( int i = 0; i < vSignatureObjects.size(); i++ )
        {
            signatures.add ( (Signature) vSignatureObjects.elementAt(i));

        }
        return signatures;

    }
}
