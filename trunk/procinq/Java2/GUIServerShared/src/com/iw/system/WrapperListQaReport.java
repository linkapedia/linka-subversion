package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@XmlRootElement(name = "REPORT")
public class WrapperListQaReport {

    private List<QaReportModel> list;

    @XmlElement(type = QaReportModel.class)
    public List<QaReportModel> getList() {
        return list;
    }

    public void setList(List<QaReportModel> list) {
        this.list = list;
    }
}
