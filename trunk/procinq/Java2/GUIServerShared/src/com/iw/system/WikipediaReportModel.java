package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author andres
 */
public class WikipediaReportModel {

    private String requestId;
    private String timestamp;
    private String wikipediaStartPoint;
    private String corpus;
    private String depth;
    private List<String> messages;
    private String status;

    @XmlElement(type = String.class)
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @XmlElement(type = String.class)
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @XmlElement(type = String.class)
    public String getWikipediaStartPoint() {
        return wikipediaStartPoint;
    }

    public void setWikipediaStartPoint(String wikipediaStartPoint) {
        this.wikipediaStartPoint = wikipediaStartPoint;
    }

    @XmlElement(type = String.class)
    public String getCorpus() {
        return corpus;
    }

    public void setCorpus(String corpus) {
        this.corpus = corpus;
    }

    @XmlElement(type = String.class)
    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    @XmlElement(type = String.class)
    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    @XmlElement(type = String.class)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
