package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 24, 2003
 * Time: 1:35:47 PM
 */

/*
  <CORPUSID>10000002</CORPUSID>
  <CORPUS_NAME>Biological agents</CORPUS_NAME>
  <CORPUSDESC>null</CORPUSDESC>
  <ROCF1>0.0</ROCF1>
  <ROCF2>0.0</ROCF2>
  <ROCF3>0.0</ROCF3>
  <ROCC1>0.0</ROCC1>
  <ROCC2>0.0</ROCC2>
  <ROCC3>0.0</ROCC3>
  <CORPUSACTIVE>1</CORPUSACTIVE>
 */
import java.util.Vector;
import java.sql.ResultSet;
import java.io.PrintWriter;

public class Thesaurus {
    // User attributes
    private String ThesaurusID;
    private String ThesaurusName;
    private String DisplayName;

    // constructor(s)
    public Thesaurus (HashTree ht) {
        ThesaurusID = (String) ht.get("THESAURUSID");
        ThesaurusName = (String) ht.get("THESAURUSNAME");
        DisplayName = ThesaurusName+"    ";
    }

    public Thesaurus (Vector v) {
        ThesaurusID = (String) v.elementAt(0);
        ThesaurusName = (String) v.elementAt(1);
        DisplayName = ThesaurusName+"    ";
    }

    public Thesaurus (ResultSet rs) throws Exception {
        ThesaurusID = rs.getString(1);
        ThesaurusName = rs.getString(2);
    }

    // emit proper xml for this class
    public void emitXML(PrintWriter out) { emitXML(out, false); }
    public void emitXML(PrintWriter out, boolean WithWrap) {
        if (WithWrap) { out.println("<THESAURUS>"); }
        out.println("   <THESAURUSID>"+ThesaurusID+"</THESAURUSID>");
        out.println("   <THESAURUSNAME>"+ThesaurusName+"</THESAURUSNAME>");
        if (WithWrap) { out.println("</THESAURUS>"); }
    }

    // accessor functions
    public String getID() { return ThesaurusID; }
    public String getName() { return ThesaurusName; }

    public void setID(String ID) { ThesaurusID = ID; }
    public void setName(String Name) { ThesaurusName = Name; }

    public String toString() { return DisplayName; }
}
