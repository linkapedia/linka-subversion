package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@XmlRootElement(name = "CATEGORIES")
public class WrapperListCategoriesDomain{

    private List<CategoryDomain> list;

    @XmlElement(type = CategoryDomain.class)
    public List<CategoryDomain> getList() {
        return list;
    }

    public void setList(List<CategoryDomain> list) {
        this.list = list;
    }
}
