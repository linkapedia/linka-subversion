package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "REPORT")
public class WrapperListMusthaveReport {

    private List<MusthaveReportModel> list;

    @XmlElement(type = MusthaveReportModel.class)
    public List<MusthaveReportModel> getList() {
        return list;
    }

    public void setList(List<MusthaveReportModel> list) {
        this.list = list;
    }
}
