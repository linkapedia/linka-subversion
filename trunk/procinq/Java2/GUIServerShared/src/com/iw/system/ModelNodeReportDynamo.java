package com.iw.system;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author andres
 */
 public class ModelNodeReportDynamo {

    private String nodeId;
    private String nodeTitle;
    private String documents;
    private String images;

    @XmlElement(type = String.class)
    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @XmlElement(type = String.class)
    public String getNodeTitle() {
        return nodeTitle;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }

    @XmlElement(type = String.class)
    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    @XmlElement(type = String.class)
    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
