package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@XmlRootElement(name = "RESULT")
public class MustHaveGateReport {

    private List<ListMusthaveGateReport> data;

    @XmlElement(type = ListMusthaveGateReport.class)
    public List<ListMusthaveGateReport> getData() {
        return data;
    }
    
    public void setData(List<ListMusthaveGateReport> data) {
        this.data = data;
    }
}
