package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 24, 2003
 * Time: 1:35:47 PM
 */

/*
 <CORPUSID>10000002</CORPUSID>
 <CORPUS_NAME>Biological agents</CORPUS_NAME>
 <CORPUSDESC>null</CORPUSDESC>
 <ROCF1>0.0</ROCF1>
 <ROCF2>0.0</ROCF2>
 <ROCF3>0.0</ROCF3>
 <ROCC1>0.0</ROCC1>
 <ROCC2>0.0</ROCC2>
 <ROCC3>0.0</ROCC3>
 <CORPUSACTIVE>1</CORPUSACTIVE>
 */
import java.io.Serializable;
import java.util.Vector;

public class Corpus implements Comparable, Serializable {
    // User attributes

    private String CorpusID;
    private String CorpusName;
    private String CorpusDesc;
    private String CorpusActive;
    private String corpusDomain;
    private String corpusToClassify;
    private String kissmetricId;
    private String adsenseId;
    private String googleAnalyticsId;
    private String corpusAffinity;
    private String useGoogleAds;
    private String corpusTerms;
    private String tagline;
    private String domainName;
    private String googleSiteVerificationId;
    private String category;

    // constructor(s)
    public Corpus() {
    }

    public Corpus(HashTree ht) {
        CorpusID = (String) ht.get("CORPUSID");
        CorpusName = (String) ht.get("CORPUS_NAME");

        if (ht.containsKey("CORPUSDESC")) {
            CorpusDesc = (String) ht.get("CORPUSDESC");
        }
        if (ht.containsKey("CORPUSACTIVE")) {
            CorpusActive = (String) ht.get("CORPUSACTIVE");
        }
        if (ht.containsKey("CORPUSDOMAIN")) {
            corpusDomain = (String) ht.get("CORPUSDOMAIN");
        }
        if (ht.containsKey("CORPUSTOCLASSIFY")) {
            corpusToClassify = (String) ht.get("CORPUSTOCLASSIFY");
        }

        if (ht.containsKey("KISSMETRICID")) {
            kissmetricId = (String) ht.get("KISSMETRICID");
        }

        if (ht.contains("CATEGORY")) {
            category = (String) ht.get("CATEGORY");
        }
        
        if (ht.contains("GOOGLESITEVERIFICATIONID")) {
            googleSiteVerificationId = (String) ht.get("GOOGLESITEVERIFICATIONID");
        }

        if (ht.containsKey("ADSENSEID")) {
            adsenseId = (String) ht.get("ADSENSEID");
        }

        if (ht.containsKey("GOOGLEANALYTICSID")) {
            googleAnalyticsId = (String) ht.get("GOOGLEANALYTICSID");
        }

        if (ht.containsKey("CORPUSAFFINITY")) {
            this.corpusAffinity = ((String) ht.get("CORPUSAFFINITY"));
        }

        if (ht.containsKey("USEGOOGLEADS")) {
            this.useGoogleAds = ((String) ht.get("USEGOOGLEADS"));
        }

        if (ht.containsKey("TAGLINE")) {
            this.tagline = ((String) ht.get("TAGLINE"));
        }

        if (ht.containsKey("CORPUSTERMS")) {
            this.corpusTerms = ((String) ht.get("CORPUSTERMS"));
        }

        if (ht.containsKey("DOMAINNAME")) {
            this.domainName = ((String) ht.get("DOMAINNAME"));
        }
    }

    public Corpus(Vector v) {
        for (int i = 0; i < v.size(); i++) {
            VectorValue vv = (VectorValue) v.elementAt(i);

            if (vv.getName().equals("CORPUSID")) {
                CorpusID = vv.getValue();
            }
            if (vv.getName().equals("CORPUS_NAME")) {
                CorpusName = vv.getValue();
            }
            if (vv.getName().equals("CORPUSDESC")) {
                CorpusDesc = vv.getValue();
            }
            if (vv.getName().equals("CORPUSACTIVE")) {
                CorpusActive = vv.getValue();
            }
            if (vv.getName().equals("CORPUSDOMAIN")) {
                corpusDomain = vv.getValue();
            }
            if (vv.getName().equals("CORPUSTOCLASSIFY")) {
                corpusToClassify = vv.getValue();
            }


            if (vv.getName().equals("KISSMETRICID")) {
                kissmetricId = (String) vv.getValue();
            }

            if (vv.getName().equals("GOOGLESITEVERIFICATIONID")) {
                googleSiteVerificationId = (String) vv.getValue();
            }
            
            if (vv.getName().equals("CATEGORY")){
                category = (String) vv.getValue();
            }

            if (vv.getName().equals("ADSENSEID")) {
                adsenseId = (String) vv.getValue();
            }

            if (vv.getName().equals("GOOGLEANALYTICSID")) {
                googleAnalyticsId = (String) vv.getValue();
            }

            if (vv.getName().equals("CORPUSAFFINITY")) {
                this.corpusAffinity = vv.getValue();
            }

            if (vv.getName().equals("USEGOOGLEADS")) {
                this.useGoogleAds = vv.getValue();
            }

            if (vv.getName().equals("TAGLINE")) {
                this.tagline = vv.getValue();
            }

            if (vv.getName().equals("DOMAINNAME")) {
                this.domainName = vv.getValue();
            }

            if (!vv.getName().equals("CORPUSTERMS")) {
                continue;
            }
            this.corpusTerms = vv.getValue();
        }
    }

    // accessor functions
    public String getID() {
        return CorpusID;
    }

    public String getName() {
        return CorpusName;
    }

    public String getDescription() {
        return CorpusDesc;
    }

    public String getActive() {
        return CorpusActive;
    }

    public String getDomain() {
        return this.corpusDomain;
    }

    public String getCorpusToClassify() {
        return this.corpusToClassify;
    }

    public String getKissmetricId() {
        return (kissmetricId == null || kissmetricId.equals("null")) ? "" : kissmetricId;
    }

    public void setKissmetricId(String kissmetricId) {
        this.kissmetricId = kissmetricId;
    }

    public String getGoogleSiteVerificationId() {
        return (googleSiteVerificationId == null || googleSiteVerificationId.equals("null")) ? "" : googleSiteVerificationId;

    }

    public void setGoogleSiteVerificationId(String googleSiteVerificationId) {
        this.googleSiteVerificationId = googleSiteVerificationId;
    }
    
    public String getCategory(){
        return (category == null || category.equals("null")) ? "" : category;
    }
    
    public void setCategory(String category){
        this.category = category;
    }

    
    public String getAdsenseId() {
        return (adsenseId == null || adsenseId.equals("null")) ? "" : adsenseId;
    }

    public void setAdsenseId(String adsenseId) {
        this.adsenseId = adsenseId;
    }

    public String getGoogleAnalyticsId() {
        return (googleAnalyticsId == null || googleAnalyticsId.equals("null")) ? "" : googleAnalyticsId;
    }

    public void setGoogleAnalyticsId(String googleAnalyticsId) {
        this.googleAnalyticsId = googleAnalyticsId;
    }

    public String getAffinity() {
        return this.corpusAffinity;
    }

    public String getUseGoogleAds() {
        return this.useGoogleAds;
    }

    public String getTagLine() {
        return (tagline == null || tagline.equals("null")) ? "" : tagline;
    }

    public String getDomainName() {
        return (domainName == null || domainName.equals("null")) ? "" : domainName;
    }

    public String getTerms() {
        return this.corpusTerms;
    }

    public void setID(String ID) {
        CorpusID = ID;
    }

    public void setName(String Name) {
        CorpusName = Name;
    }

    public void setDescription(String Description) {
        CorpusDesc = Description;
    }

    public void setActive(String Active) {
        CorpusActive = Active;
    }

    public void setDomain(String domain) {
        corpusDomain = domain;
    }

    public void setCorpusClassify(String corpusToClassify) {
        this.corpusToClassify = corpusToClassify;
    }

    public void setAffinity(String affinity) {
        this.corpusAffinity = affinity;
    }

    public void setUseGoogleAds(String useGoogleAds) {
        this.useGoogleAds = useGoogleAds;
    }

    public void setTagLine(String tagLine) {
        this.tagline = tagLine;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public void setTerms(String terms) {
        this.corpusTerms = terms;
    }

    // generic set: used by SAX parser
    public void set(String Key, String Value) {
        if (Key.equals("ID")) {
            setID(Value);
        } else if (Key.equals("BASENAMESTRING")) {
            setName(Value);
        } else if (Key.equals("CORPUSDESC")) {
            setDescription(Value);
        } else if (Key.equals("CORPUSACTIVE")) {
            setActive(Value);
        } else if (Key.equals("CORPUSDOMAIN")) {
            setDomain(Value);
        } else if (Key.equals("CORPUSTOCLASSIFY")) {
            setCorpusClassify(Value);
        } else if (Key.equals("KISSMETRICID")) {
            setKissmetricId(Value);
        } else if (Key.equals("GOOGLESITEVERIFICATIONID")){
            setGoogleSiteVerificationId(Value);
        }else if(Key.equals("CATEGORY")){
            setCategory(Value);
        } else if (Key.equals("GOOGLEANALYTICSID")) {
            setGoogleAnalyticsId(Value);
        } else if (Key.equals("ADSENSEID")) {
            setAdsenseId(Value);
        } else if (Key.equals("CORPUSAFFINITY")) {
            setAffinity(Value);
        } else if (Key.equals("USEGOOGLEADS")) {
            setUseGoogleAds(Value);
        } else if (Key.equals("DOMAINNAME")) {
            setDomainName(Value);
        } else if (Key.equals("CORPUSTERMS")) {
            setTerms(Value);
        }
    }

    @Override
    public String toString() {
        return CorpusName;
    }

    @Override
    public int compareTo(Object o) {
        return CorpusName.compareTo(o.toString());
    }
}
