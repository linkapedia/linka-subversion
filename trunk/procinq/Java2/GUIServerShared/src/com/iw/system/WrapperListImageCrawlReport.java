package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@XmlRootElement(name = "REPORT")
public class WrapperListImageCrawlReport {

    private List<ImageCrawlReportModel> list;

    @XmlElement(type = ImageCrawlReportModel.class)
    public List<ImageCrawlReportModel> getList() {
        return list;
    }

    public void setList(List<ImageCrawlReportModel> list) {
        this.list = list;
    }
}
