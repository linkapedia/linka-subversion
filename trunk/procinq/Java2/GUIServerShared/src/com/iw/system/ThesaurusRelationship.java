package com.iw.system;

public class ThesaurusRelationship {
    // attributes
    private String RelationshipID;
    private String Relationship;

    // constructor
    public ThesaurusRelationship (String ID, String Name) {
        RelationshipID = ID; Relationship = Name;
    }

    // accessor functions
    public String getID() { return RelationshipID; }
    public String getRelationship() { return Relationship; }

    public String toString() { return Relationship; }
}
