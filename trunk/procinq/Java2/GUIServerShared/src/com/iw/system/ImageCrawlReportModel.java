package com.iw.system;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author andres
 */
public class ImageCrawlReportModel {

    private String requestId;
    private String timestamp;
    private String corpus;
    private String node;
    private String recursively;
    private String status;
    private String summary;

    @XmlElement(type = String.class)
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @XmlElement(type = String.class)
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @XmlElement(type = String.class)
    public String getCorpus() {
        return corpus;
    }

    public void setCorpus(String corpus) {
        this.corpus = corpus;
    }

    @XmlElement(type = String.class)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlElement(type = String.class)
    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    @XmlElement(type = String.class)
    public String getRecursively() {
        return recursively;
    }

    public void setRecursively(String recursively) {
        this.recursively = recursively;
    }

    @XmlElement(type = String.class)
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
