package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@XmlRootElement(name = "REPORT")
public class WrapperListReport {

    private List<WikipediaReportModel> list;

    @XmlElement(type = WikipediaReportModel.class)
    public List<WikipediaReportModel> getList() {
        return list;
    }

    public void setList(List<WikipediaReportModel> list) {
        this.list = list;
    }
}
