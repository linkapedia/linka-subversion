package com.iw.system;

/**
 *
 * @author andres
 */
public class BaseParamsQueryReport {

    private String fromPage;
    private String toPage;

    public String getFromPage() {
        return fromPage;
    }

    public void setFromPage(String fromPage) {
        this.fromPage = fromPage;
    }

    public String getToPage() {
        return toPage;
    }

    public void setToPage(String toPage) {
        this.toPage = toPage;
    }
}