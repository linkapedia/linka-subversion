package com.iw.system;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@XmlRootElement(name = "REPORT")
public class ModelReport {

    private String nodeCount;
    private List<ModelNodeReport> nodes;

    @XmlElement(type=String.class)
    public String getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(String nodeCount) {
        this.nodeCount = nodeCount;
    }

    @XmlElement(type=ModelNodeReport.class)
    public List<ModelNodeReport> getNodes() {
        return nodes;
    }

    public void setNodes(List<ModelNodeReport> nodes) {
        this.nodes = nodes;
    }
}
