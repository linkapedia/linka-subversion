package com.iw.guiservershared.externalexplain;

import com.iw.guiservershared.xmlserializejava.IXMLBeanSerializable;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;

import java.util.Vector;
import java.util.Enumeration;
import java.io.InputStream;


public class ExplainDisplayDataContainer implements IXMLBeanSerializable
{
    public static final String sExplainDataRenderType_TABLE = "RENDERTABLE";
    public static final String sExplainDataRenderType_PIECHART = "RENDERPIECHART";

    private Vector vExplainDisplayDataInstances = new Vector();

    private int iNodeID = -1;
    private int iDocID = -1;
    private String sNodeTitle= null;
    private String sDocTitle = null;

    public  ExplainDisplayDataContainer ()
    {
    }
    public  ExplainDisplayDataContainer (int iNodeID, int iDocID, String sNodeTitle, String sDocTitle)
    {
        this.iNodeID = iNodeID;
        this.iDocID = iDocID;
        this.sNodeTitle= sNodeTitle;
        this.sDocTitle = sDocTitle;
    }
    public  ExplainDisplayDataContainer (int iNodeID, String sNodeTitle, String sDocTitle)
    {
        this.iNodeID = iNodeID;
        this.iDocID = -1;
        this.sNodeTitle= sNodeTitle;
        this.sDocTitle = sDocTitle;
    }
    public Enumeration enumExplainDisplayDataInstances()
    {
        return vExplainDisplayDataInstances.elements();
    }
    public synchronized void addExplainDisplayDataInstance (ExplainDisplayDataInstance eddi)
    {
        vExplainDisplayDataInstances.addElement(eddi);
    }
    public synchronized void addExplainDisplayDataInstance (ExplainDisplayDataInstance eddi, int iTabIndex)
    {
        vExplainDisplayDataInstances.insertElementAt(eddi, iTabIndex);
    }
    public Object getSerializableForm()
    {
        // vector containins a string and a vector of data instances
        Vector vTopLevelSerializableForm =  new Vector();
        vTopLevelSerializableForm.addElement(this.getClass().getName());
        Vector vSetofSerializableFormInstances = new Vector();

        Enumeration enumXMLableInstances = vExplainDisplayDataInstances.elements();
        while ( enumXMLableInstances.hasMoreElements())
        {
            vSetofSerializableFormInstances.addElement(((IXMLBeanSerializable)
                    enumXMLableInstances.nextElement()).getSerializableForm());
        }
        vTopLevelSerializableForm.addElement(vSetofSerializableFormInstances);

        return vTopLevelSerializableForm;

    }
    public void setFromSerializableForm(Object oThisInSerializedForm ) throws Exception
    {
        Vector vTopLevelSerializableForm = (Vector) oThisInSerializedForm;
        vExplainDisplayDataInstances = new Vector();
        String sClassPath = (String) vTopLevelSerializableForm.elementAt(0);

        if ( !sClassPath.equals(this.getClass().getName()))
            throw new Exception ("class mismatch on deserialize me [" + this.getClass().getName() + "] vs XML [" + sClassPath + "]");

        Vector vOfExplainDisplayDataInstancesSerializableForm = (Vector) vTopLevelSerializableForm.elementAt(1);
        Enumeration enumEDDIsInSerializableForm = vOfExplainDisplayDataInstancesSerializableForm.elements();

        while ( enumEDDIsInSerializableForm.hasMoreElements())
        {
            Vector vDisplayDataInstanceInSerializedForm = (Vector) enumEDDIsInSerializableForm.nextElement();
            String sClassPathInstance = (String) vDisplayDataInstanceInSerializedForm.elementAt(0);
           // Object oTest = new ExplainDisplayDataInstanceTABLE();

            ExplainDisplayDataInstance eddi = null;
            // why did this not work ?
                    // ExplainDisplayDataInstance ExplainDisplayDataInstance = Class.forName(sClassPathInstance, true, this.getClass().getClassLoader());
            if ( sClassPathInstance.equals("com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE"))
            {
                eddi = new ExplainDisplayDataInstanceTABLE();
            }
            else
                throw new Exception ("unknown type for deserialization sClassPathInstance [" + sClassPathInstance  + "]");
            //ExplainDisplayDataInstance  eddi = (ExplainDisplayDataInstance)  (o);
            eddi.setFromSerializableForm(vDisplayDataInstanceInSerializedForm);
            vExplainDisplayDataInstances.addElement(eddi);
        }

    }
    public String getXMLVisualDebug ( int iDepthRecursionForIndent ) throws Exception
    {
        StringBuffer sb = new StringBuffer();

        Enumeration enumDataInstances = vExplainDisplayDataInstances.elements();
        String sIndent = new String(new byte[iDepthRecursionForIndent]);
        while ( enumDataInstances.hasMoreElements())
        {
            sb.append(sIndent+"<OBJECT>\r\n");
            sb.append(sIndent+" <CLASSNAME>" + this.getClass().getName() + "</CLASSNAME>\r\n");
            sb.append(sIndent+" <SIZE>" + this.vExplainDisplayDataInstances.size() + "</SIZE>\r\n");
            IXMLBeanSerializable xmlBeanSerializable =
                    (IXMLBeanSerializable) enumDataInstances.nextElement();
            sb.append(sIndent+xmlBeanSerializable.getXMLVisualDebug(iDepthRecursionForIndent+1)+ "\r\n");
            sb.append(sIndent+"</OBJECT>\r\n");
        }

        return sb.toString();

    }

    public boolean bValidateDeserializableForm (Object o) throws Exception
    {
/*
        String s = (String) vectorRepresentedDisplayDataInstance.elementAt(0);
        if ( s.equals(ExplainDisplayDataContainer.sExplainDataInstanceType_PIECHART)
        {


        }
        else if s.equals(ExplainDisplayDataContainer.sExplainDataInstanceType_TABLE)
        {


        }
        else {
            throw new Exception ("invalid DisplayDataInstance type [" + s + "]");
*/

        return true;
    }

    public int getNodeID(){return iNodeID;}
    public int getDocID(){return iDocID;}
    public String getDocTitle(){return sDocTitle;}
    public String getNodeTitle(){return sNodeTitle;}
    public Vector getvExplainDisplayDataInstances(){return vExplainDisplayDataInstances;}
    public int getNumReportElements (){return vExplainDisplayDataInstances.size() ;}
}
