/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Dec 1, 2003
 * Time: 5:12:48 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.iw.guiservershared.xmlserializejava;

import java.io.*;
import java.beans.XMLEncoder;
import java.beans.XMLDecoder;

/**
 * This class is the analog of XMLBeanSerializeHelper except oriented for more visual display
 * of XML output akin to traditional Indraweb XML output, i.e., not for object graph traversal but rather
 * row object emits that are better for visual viewing.
 */
public class XMLSerializeHelper
{

    public static void serializeToStream ( Object o, OutputStream out )
    {
        XMLEncoder e = new XMLEncoder(out);
        try {
            e.writeObject(o);
        } catch (Exception e2 ) {
            System.out.println("warning on object encode as xml");
            e2.printStackTrace();
        }
        e.close();
    }

    public static void serializeToFile ( Object o, String sFileNameFull ) throws FileNotFoundException, IOException
    {
        BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream (sFileNameFull));
        serializeToStream ( o, bout );
        bout.close();
    }

    public static String serializeToString ( Object o ) throws IOException
    {
        ByteArrayOutputStream bout = new ByteArrayOutputStream ();
        serializeToStream ( o, bout );
        String sRet = bout.toString();
        char[] cArrReplace = new char[1];
        cArrReplace[0] = (char) 10;
        String sReplace = new String(cArrReplace);
        //sRet = sRet.replace((char)10, (char)13));
        sRet = sRet.replaceAll(sReplace, "\r\n");
        byte[] barr = bout.toByteArray();
        for ( int i = 0; i < 100; i++)
        {
            //System.out.println(i + ". barr [" + barr[i] + "] char [" + (char) barr[i] + "]" );

        }
        //System.out.println("sRet [" + sRet  + "]" );
        bout.close();
        return sRet;
    }


    public static Object deserializeFromStream ( InputStream in )
            throws Exception
    {
        XMLDecoder d = new XMLDecoder(in);
        Object oRet = null;
        try {
            oRet = d.readObject();
        } catch ( Exception e2 )  {
            System.out.println("warning on object encode as xml");
            e2.printStackTrace();
        }
        d.close();
        return  oRet;
    }

    public static Object deserializeFromFile ( String sFileNameFull )
            throws Exception
    {
        BufferedInputStream bin = new BufferedInputStream(new FileInputStream (sFileNameFull));
        Object o = deserializeFromStream(bin);
        bin.close();
        return o;
    }
    public static Object deserializeFromString ( String sObjectXMLized)
            throws Exception
    {
        ByteArrayInputStream bin = new ByteArrayInputStream(sObjectXMLized.getBytes());
        Object o = deserializeFromStream(bin);
        bin.close();
        return o;
    }


    public static boolean testVerifyObjectSerialization ( Object o, String sTempFileNameFull1,
                                                          String sTempFileNameFull2)
            throws Exception
    {

        //serializeToFile(o, sTempFileNameFull1);
        //Object o2 = deserializeFromFile(sTempFileNameFull1);
        //serializeToFile(o2, sTempFileNameFull2);
        //String serialized1 = com.indraweb.util.UtilFile.getFileAsString(sTempFileNameFull1);
        //String serialized2 = com.indraweb.util.UtilFile.getFileAsString(sTempFileNameFull2);
        //return serialized1.equals(serialized2);
        return true;
    }




}
