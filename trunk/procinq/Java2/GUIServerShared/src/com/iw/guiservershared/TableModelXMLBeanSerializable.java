

package com.iw.guiservershared;

import com.iw.guiservershared.xmlserializejava.IXMLBeanSerializable;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.AbstractTableModel;
import java.util.Vector;
import java.util.Enumeration;
import java.io.*;
import java.sql.*;


import javax.swing.table.AbstractTableModel;
import javax.swing.*;


public class TableModelXMLBeanSerializable extends AbstractTableModel implements IXMLBeanSerializable
{

    String[] sArrColNamesFinal = null;
    Object[][] oArrArrData = new Object[0][0];
    String[] sArrColClassNames = null;


    // serializable data container - indexed by vector location
    Vector vSerializableTopLevelContainer = new Vector ();


    public int getColumnCount ()
    {
        return sArrColNamesFinal.length;
    }

    public int getRowCount ()
    {
        return oArrArrData.length;
    }

    public String getColumnName (int col)
    {
        return sArrColNamesFinal[col];
    }

    public String getColumnClassName (int col)
    {
        return sArrColClassNames[col];
    }

    public Object getValueAt (int row , int col)
    {
        try
        {
            return oArrArrData[row][col];
        } catch ( Exception e )
        {
            e.printStackTrace ();
        }
        return null;
    }

    /*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     */
    public Class getColumnClass (int c)
    {
        try
        {
            Object o = getValueAt (0 , c);
            if (o == null)
                return null;
            return getValueAt (0 , c).getClass ();
        } catch ( Exception e )
        {
            e.printStackTrace ();
        }
        return null;
    }

    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    public boolean isCellEditable (int row , int col)
    {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        return false;
/*
        if (col < 2)
        {
            return false;
        }
        else
        {
            return true;
        }
*/
    }

    /*
     * Don't need to implement this method unless your table's
     * data can change.
     */
    public void setValueAt (Object value , int row , int col)
    {

        if (!value.getClass ().equals (getColumnClass (col)))
            System.out.println ("ERROR in TableModelXMLBeanSerializable : class mismatch col ");
/*
        [" + col +
            "] current [" getColumnClass(col) + "] inserted [" + value.getClass() +
            "] inserted value toString() [" + value.toString() + "]" );
*/

        oArrArrData[row][col] = value;
    }

    // for readable xml output in a more streaming style with no data retention optional
    public void fillDataWithSQLCall (String sSQL ,
                                     Connection dbc ,
                                     String[] sArrColNamesFromCaller ,
                                     PrintWriter pw ,
                                     String sObjectNameInsteadOfTable ,
                                     String sObjectNameInsteadOfRow,
                                     boolean bBeanXML
                                     ) throws Exception
    {

        String sObjectNameInsteadOfRowFinal = "ROW";
        String sObjectNameInsteadOfTableFinal = "TABLE";
        //System.out.println ("in fillDataWithSQLCall ");
        Statement stmt = dbc.createStatement ();
        ResultSet rs = stmt.executeQuery (sSQL);

        ResultSetMetaData md = rs.getMetaData ();
        int iColCount = md.getColumnCount ();
        //String[] sArrColClassNames_metadata = new String[iColCount];
        boolean[] bArrConvertColToDoubleOrInteger = new boolean[iColCount];
        boolean[] bArrCDataNeeded = new boolean[iColCount];
        sArrColNamesFinal = new String[iColCount];
        sArrColClassNames = new String[iColCount];
        //sArrColClassNames = new String[iColCount];
        // GET USER COL NAMES IF PROVIDED
        if (sArrColNamesFromCaller == null)
        {
            for ( int i = 0 ; i < iColCount ; i++ )
                sArrColNamesFinal[i] = md.getColumnName (i + 1);
        }
        else
        {
            if (sArrColNamesFromCaller.length != iColCount)
                throw new Exception ("column count mismatch in TableModelXMLBeanSerializable.fillDataWithSQLCall for variable sArrColNamesFromCaller [" + sArrColNamesFromCaller.length + "] vs data [" + iColCount + "]");
            sArrColNamesFinal = sArrColNamesFromCaller;
        }

        // get col class names
        for ( int i = 0 ; i < iColCount ; i++ )
        {
            sArrColClassNames[i] = md.getColumnClassName (i + 1);
            if (sArrColClassNames[i].equals ("java.math.BigDecimal"))
                bArrConvertColToDoubleOrInteger[i] = true;
            else
                bArrConvertColToDoubleOrInteger[i] = false;
        }
        // set cdata needed boolean arr
        for ( int iCol = 0 ; iCol < iColCount ; iCol++ )
        {
            if (sArrColClassNames[iCol].equals ("java.lang.String"))
                bArrCDataNeeded[iCol] = true;
            else
                bArrCDataNeeded[iCol] = false;
        }
        // verify data types are within understood set
        for ( int i = 0 ; i < iColCount ; i++ )
        {
            if (sArrColClassNames[i].equals ("java.math.BigDecimal"))
                continue;
            if (sArrColClassNames[i].equals ("java.lang.String"))
                continue;
            throw new Exception ("function fillDataWithSQLCall not yet adapted to data type [" +
                    sArrColClassNames[i] + "]");
        }


        if (sObjectNameInsteadOfRow != null)
            sObjectNameInsteadOfRowFinal = sObjectNameInsteadOfRow;
        if (sObjectNameInsteadOfTable != null)
            sObjectNameInsteadOfTableFinal = sObjectNameInsteadOfTable;

        Vector vRows = new Vector ();
        if (!bBeanXML)
        {
            pw.println ("<" + "OBJECT_TABLEMODEL_XML" + ">");
            pw.println ("<" + sObjectNameInsteadOfTableFinal + ">");
        }
        while (rs.next ())
        {
            if (!bBeanXML)
                pw.println (" <" + sObjectNameInsteadOfRowFinal + ">");

            Object[] oArrRow = new Object[iColCount];
            for ( int iCol = 0 ; iCol < iColCount ; iCol++ )
            {
                Object cellvalue = rs.getObject (iCol + 1);

                if (bBeanXML && cellvalue != null && bArrConvertColToDoubleOrInteger[iCol])
                {
                    String sCellValStr = cellvalue.toString ();
                    if (sCellValStr.indexOf (".") >= 0)
                        cellvalue = new Double (cellvalue.toString ());
                    else
                        cellvalue = new Integer (cellvalue.toString ());

                }
                oArrRow[iCol] = cellvalue;
            }

            if (bBeanXML)
            {
                vRows.addElement (oArrRow);
            }
            else // emit nice looking row of data
            {
                for ( int iCol = 0 ; iCol < iColCount ; iCol++ )
                {
                    pw.print ("  <" + sArrColNamesFinal[iCol] + ">");
                    if (bArrCDataNeeded[iCol])
                        pw.print ("<![CDATA[" + oArrRow[iCol].toString () + "]]>");
                    else
                        pw.print (oArrRow[iCol].toString ());
                    pw.println ("</" + sArrColNamesFinal[iCol] + ">");
                }
                pw.println (" </" + sObjectNameInsteadOfRowFinal + ">");
            }
        }

        if (bBeanXML) // if not dumped into a table
        {
            oArrArrData = new Object[vRows.size ()][iColCount];

            int iRowCount = vRows.size ();
            for ( int i = 0 ; i < iRowCount ; i++ )
            {
                oArrArrData[i] = (Object[]) vRows.elementAt (i);
            }
        }
        if (!bBeanXML)
        {
            pw.println ("</" + sObjectNameInsteadOfTableFinal + ">");
            pw.println ("</" + "OBJECT_TABLEMODEL_XML" + ">");
        }

        rs.close ();
        stmt.close ();
    }

    private void printDebugData ()
    {
        int numRows = getRowCount ();
        int numCols = getColumnCount ();

        for ( int i = 0 ; i < numRows ; i++ )
        {
            System.out.print ("    row " + i + ":");
            for ( int j = 0 ; j < numCols ; j++ )
            {
                System.out.print ("  " + oArrArrData[i][j]);
            }
            System.out.println ();
        }
        System.out.println ("--------------------------");
    }

    public Object[][] getOArrArrData ()
    {
        return oArrArrData;
    }

    public void setOArrArrData (Object[][] oArrArrData)
    {
        this.oArrArrData = oArrArrData;
    }

    public void setColNames (String[] sArrColNames)
    {
        this.sArrColNamesFinal = sArrColNames;
    }

    public void setColClassNames (String[] sArrColClassNames)
    {
        this.sArrColClassNames = sArrColClassNames;
    }

    public String[] getColNames ()
    {
        return sArrColNamesFinal;
    }

    public void emitIWXMLData (OutputStream out) throws Exception
    {
        //System.out.println("in emitIWXMLData (OutputStream out)" );
        Vector vDataObjects = packageTheData ();
        XMLBeanSerializeHelper.serializeToStream (vDataObjects , out);
        emitIWXMLData (new PrintWriter (out));
    }

    public void emitIWXMLData (PrintWriter pwout) throws Exception
    {
        //System.out.println("in emitIWXMLData (PrintWriter pwout)" );

        Vector vDataObjects = packageTheData ();
        String svDataObjectsSerialized = XMLBeanSerializeHelper.serializeToString (vDataObjects);
        //System.out.println("svDataObjectsSerialized [" + svDataObjectsSerialized + "]" );
        pwout.write (svDataObjectsSerialized);
    }

    private Vector packageTheData ()
    {
        vSerializableTopLevelContainer = new Vector ();
        vSerializableTopLevelContainer.addElement (this.getClass ().getName ());
        vSerializableTopLevelContainer.addElement (sArrColNamesFinal);
        vSerializableTopLevelContainer.addElement (sArrColClassNames);
        vSerializableTopLevelContainer.addElement (oArrArrData);
        return vSerializableTopLevelContainer;

    }

    public void setFromIWXMLData (InputStream in) throws Exception
    {
        vSerializableTopLevelContainer = (Vector) XMLBeanSerializeHelper.deserializeFromStream (in);
        setColNames ((String[]) vSerializableTopLevelContainer.elementAt (1));
        setColClassNames ((String[]) vSerializableTopLevelContainer.elementAt (2));
        setOArrArrData ((Object[][]) vSerializableTopLevelContainer.elementAt (3));
    }

    public void setFromSerializableForm (Object oSerializableForm) throws Exception
    {
        throw new Exception ("Method not implemented (or meaningful) deprecated [setFromSerializableForm].");
    }

    public Object getSerializableForm ()
    {
        System.out.println ("Method not implemented (or meaningful) deprecated [getSerializableForm].");
        return null;
    }

    public String getXMLVisualDebug (int iDepthRecursionForIndent) throws Exception
    {
        StringBuffer sb = new StringBuffer ();

        Enumeration enumData = vSerializableTopLevelContainer.elements ();
        String sIndent = new String (new byte[iDepthRecursionForIndent]);
        while (enumData.hasMoreElements ())
        {
            sb.append (sIndent + "<OBJECT>\r\n");
            sb.append (sIndent + " <CLASSNAME>" + this.getClass ().getName () + "</CLASSNAME>\r\n");
            sb.append (sIndent + " <SIZE>" + this.vSerializableTopLevelContainer.size () + "</SIZE>\r\n");
            IXMLBeanSerializable xmlBeanSerializable =
                    (IXMLBeanSerializable) enumData.nextElement ();
            sb.append (sIndent + xmlBeanSerializable.getXMLVisualDebug (iDepthRecursionForIndent + 1) + "\r\n");
            sb.append (sIndent + "</OBJECT>\r\n");
        }

        return sb.toString ();

    }

    public boolean bValidateDeserializableForm (Object o) throws Exception
    {
        return true;
    }


}
