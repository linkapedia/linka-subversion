/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: May 24, 2004
 * Time: 4:36:22 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.iw.tools;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.HashSet;

public class Dom4jHelper
{
    public static org.dom4j.Document getDomFromstring (String s) throws Exception
    {
        boolean bDebug = false;
        org.dom4j.Document doc = null;
        try
        {
            ByteArrayInputStream bin = new ByteArrayInputStream(s.getBytes("UTF-8"));
            SAXReader xmlReader = new SAXReader(false);
            if (bDebug) {
                ErrorHandler eh = new ErrorHandler() {
                    public void fatalError (SAXParseException s) {
                        System.out.println("Fatal error parsing XML, line: "+s.getLineNumber() + ":" + s.getMessage()); }
                    public void error (SAXParseException s) {
                        System.out.println("Error parsing XML, line: "+s.getLineNumber() + ":" + s.getMessage()); }
                    public void warning (SAXParseException s) {
                        System.out.println("Warning parsing XML, line: "+s.getLineNumber() + ":" + s.getMessage()); }
                };
                xmlReader.setErrorHandler(eh);
            }
            InputSource ins = new InputSource(bin );
            ins.setEncoding("UTF-8");

            doc = xmlReader.read(ins);
            return doc;
        } catch ( Exception e )
        {
            throw new Exception ("error in getDomFromstring", e);
        }
    }

    public static int countTagsRecursive (String sTagName, org.dom4j.Element elem,
                                                           boolean bRecurse ) throws Exception
    {
        boolean bDebug = false;
        org.dom4j.Document doc = null;
        try
        {
            Iterator iterelems = elem.elements().iterator();
            int iCount = 0;
            while ( iterelems.hasNext())
            {
                Element elemnext = (Element) iterelems.next();
                String sElemName = elemnext.getName();
                if ( sElemName.equalsIgnoreCase(sTagName) )
                    iCount++;
                if ( bRecurse)
                    iCount += countTagsRecursive(sTagName, elemnext, bRecurse );
            }
            return iCount ;
        } catch ( Exception e )
        {
            throw new Exception ("error in getDomFromstring", e);
        }
    }

    public static boolean containsTag (String sTagName, Element element) throws Exception {
        if (element.getName().equalsIgnoreCase(sTagName)) return true;

        Iterator i = element.elements().iterator();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            if (containsTag(sTagName, e)) return true;
        }

        return false;
    }

    public static void getAllTagNames (HashSet hs, Element element, int iAtLevel, int iCurrentLevel) throws Exception {
        if ( iAtLevel == iCurrentLevel )
            hs.add(element.getName());
        else
        {
            Iterator i = element.elements().iterator();
            while (i.hasNext()) {
                Element e = (Element) i.next();
                getAllTagNames(hs, e, iAtLevel , iCurrentLevel+1);
            }
        }
    }
 }
