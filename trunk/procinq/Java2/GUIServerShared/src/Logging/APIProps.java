package Logging;

import java.util.Properties;
import java.io.PrintWriter;

public class APIProps extends Properties {
    // *********************************************************************
    public APIProps()
            // *********************************************************************
    {
        super();
    }

    // *********************************************************************
    public APIProps(Properties props)
            // *********************************************************************
    {
        super(props);
    }

    // *********************************************************************
    public Object get(String soKey, boolean bRequired)
            // *********************************************************************
            throws Exception {
        String sValue = (String) get(soKey.toLowerCase());
        if (bRequired && sValue == null) {
            throw new Exception("api props missing paramter [" + soKey.toString().toLowerCase() + "]");
        }
        return sValue;
    }

    // *********************************************************************
    public Object get(String soKey, String sDefault)
            // *********************************************************************
    {
        String sValue = (String) super.get(soKey.toLowerCase());
        if (sValue == null) {
            return sDefault;
        }
        return sValue;
    }

    // *********************************************************************
    public Object get(String soKey)
            // *********************************************************************
    {
        String sValue = (String) super.get(soKey.toLowerCase());

        /* Do not print output for parameters
		if (sValue != null && APIHandler.getEmittingXML() )		{
			APIHandler.getout().println ("<PARM>"+soKey.toLowerCase()+"="+sValue+"</PARM>");
		}
		*/
        return sValue;
    }


    // *********************************************************************
    public boolean getbool(String soKey, boolean bRequiredParm)
            // *********************************************************************
            throws Exception {
        return Boolean.valueOf((String) get(soKey.toLowerCase(), bRequiredParm)).booleanValue();
    }

    // *********************************************************************
    public boolean getbool(String soKey, String sDefault)
            // *********************************************************************
    {
        String sBool = (String) get(soKey.toLowerCase(), sDefault);
        boolean b1 = Boolean.valueOf(sBool).booleanValue();
        return b1;
    }

    // *********************************************************************
    public boolean getbool(String soKey)
            // *********************************************************************
    {
        return Boolean.valueOf((String) get(soKey.toLowerCase())).booleanValue();
    }

    // *********************************************************************
    public void put(String sKey, String sValue)
            // *********************************************************************
    {
        super.put(sKey.toLowerCase(), sValue);
    }

    // *********************************************************************
    public void remove(String soKey)
            // *********************************************************************
    {
        super.remove(soKey.toLowerCase());
    }

}
