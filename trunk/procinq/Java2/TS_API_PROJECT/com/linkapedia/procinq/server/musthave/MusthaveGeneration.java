package com.linkapedia.procinq.server.musthave;

import com.linkapedia.procinq.server.node.Node;
import java.util.List;
import java.util.Map;


public interface MusthaveGeneration {

    Map<Integer, List<String>> getMusthaves(List<Node> nodes
            ) throws MustHaveGenerationException;
}
