package com.linkapedia.procinq.server.musthave;

import com.linkapedia.procinq.server.node.Node;
import com.linkapedia.procinq.server.node.NodeDao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

public class MusthavesUtils {

    public static List<Node> getNodes(long nodeId, int toLevel, int from, int to,
            Connection connection) throws SQLException {
        List<Node> nodes;
        if (toLevel == -1) {
            nodes = NodeDao.getNodesByIdUntilLastLevelWithPagination(
                    nodeId, from, to, connection);
        } else {
            nodes = NodeDao.getNodesByIdUntilLevelWithPagination(
                    nodeId, toLevel, from, to, connection);
        }
        return nodes;
    }

    public static List<String> cleanTerms(List<String> terms) {
        String termAux;
        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < terms.size(); i++) {
            termAux = terms.get(i);
            termAux = termAux.replaceAll("(?i)(^\\s*(the)\\s+|\\s+(the)\\s+)", " ");
            termAux = termAux.replaceAll("(?i)\\bas\\b", " ");
            termAux = termAux.replaceAll("(?i)\\bbut\\b", " ");
            termAux = termAux.replaceAll("(?i)\\bso\\b", " ");
            termAux = termAux.replaceAll("(?i)\\bthen\\b", " ");
            termAux = termAux.replaceAll("(?i)\\bif\\b", " ");
            termAux = termAux.replaceAll("(?i)\\bthat\\b", " ");
            termAux = Pattern.compile("\\p{Punct}", 
                    Pattern.UNICODE_CHARACTER_CLASS).matcher(termAux).replaceAll(" ");
            termAux = termAux.trim();
            if (!termAux.isEmpty()) {
                resultList.add(termAux);
            }
        }
        return resultList;
    }
    
    public static List<String> cleanTermsPunct(List<String> terms) {
        String termAux;
        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < terms.size(); i++) {
            termAux = terms.get(i);
            termAux = Pattern.compile("\\p{Punct}", 
                    Pattern.UNICODE_CHARACTER_CLASS).matcher(termAux).replaceAll(" ");
            termAux = termAux.trim();
            if (!termAux.isEmpty()) {
                resultList.add(termAux);
            }
        }
        return resultList;
    }
    

    public static String removeTextInsidePharenthesisInNodeTitle(String title) {
        return title.replaceAll("(?i)(\\(.*?(\\)|$))", " ");
    }

    public static String removeStringWithDotAtEnd(String title) {
        return title.replaceAll("\\s.{1,3}\\.", " ");
    }

    public static List<String> removeDuplicateTerms(List<String> terms) {
        List<String> list = new ArrayList<>();
        Set<String> set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        set.addAll(terms);
        list.addAll(set);
        return list;
    }
}
