package com.linkapedia.procinq.server.musthave;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProccessStatusDao {

    public static Long saveStatusAndResturnRequestId(
            long timeStamp, String corpus, String node,
            int toLevel, String status, Connection connection)
            throws SQLException {

        String sql = "INSERT INTO SBOOKS.MUSTUHAVEPROCESSSTATUS (REQUESTID,TIMESTAMP,"
                + "CORPUS, NODE, TOLEVEL, STATUS) VALUES "
                + "(SEC_MUSTHAVEPROCESS.nextval,?,?,?,?,?)";
        PreparedStatement pstmt = null;
        ResultSet generatedKeys = null;
        try {
            pstmt = connection.prepareStatement(sql, new String[]{"REQUESTID"});
            pstmt.setLong(1, timeStamp);
            pstmt.setString(2, corpus);
            pstmt.setString(3, node);
            pstmt.setInt(4, toLevel);
            pstmt.setString(5, status);
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating imagecrawl process failed");
            }
            generatedKeys = pstmt.getGeneratedKeys();
            generatedKeys.next();
            return generatedKeys.getLong(1);
        } finally {
            generatedKeys.close();
            pstmt.close();
        }
    }

    public static void updateProcessStatus(long requestId, String status,
            Connection connection) throws SQLException {
        String sql = "UPDATE SBOOKS.MUSTUHAVEPROCESSSTATUS SET STATUS=? WHERE REQUESTID=?";
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, status);
            pstmt.setLong(2, requestId);
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }
}
