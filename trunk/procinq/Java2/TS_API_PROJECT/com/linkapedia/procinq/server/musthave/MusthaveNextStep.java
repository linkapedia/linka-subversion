package com.linkapedia.procinq.server.musthave;

import com.linkapedia.procinq.server.node.Node;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MusthaveNextStep {

    List<MusthaveGeneration> musthavesGeneration;

    public MusthaveNextStep(List<MusthaveGeneration> musthavesGeneration) {
        this.musthavesGeneration = musthavesGeneration;
    }

    public void insertMushaves(List<Node> nodes, Connection sqlConnection) {
        for (MusthaveGeneration musthaveGeneration : musthavesGeneration) {
            try {
                Map<Integer, List<String>> nodesMusthaves =
                        musthaveGeneration.getMusthaves(nodes);

                for (Map.Entry<Integer, List<String>> musthave : nodesMusthaves.entrySet()) {
                    int nodeId = musthave.getKey();
                    List<String> musthaves = musthave.getValue();
                    musthaves = MusthavesUtils.cleanTerms(musthaves);
                    for (String mustWord: musthaves){
                        try {
                            MustHaveDao.insertMusthave(nodeId, mustWord, sqlConnection);
                        } catch (SQLException ex) {
                        }
                    }
                }
            } catch (MustHaveGenerationException ex) {
            }
        }
    }
    
    
}
