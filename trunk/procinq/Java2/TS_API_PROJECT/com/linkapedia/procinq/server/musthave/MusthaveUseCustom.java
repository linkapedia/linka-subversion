package com.linkapedia.procinq.server.musthave;

import com.linkapedia.procinq.server.node.Node;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MusthaveUseCustom implements MusthaveGeneration {

    private Map<String, Object> filters;

    public MusthaveUseCustom(Map<String, Object> filters) {
        this.filters = filters;
    }

    @Override
    public Map<Integer, List<String>> getMusthaves(List<Node> nodes) throws MustHaveGenerationException {
        String customMusthave = (String) filters.get("CUSTOMMUSTHAVE");
        Map<Integer, List<String>> muthaves = new HashMap();
        for (Node node : nodes) {
            muthaves.put(node.getNodeId(), Arrays.asList(customMusthave));
        }
        return muthaves;
    }
}
