package com.linkapedia.procinq.server.node;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NodeDao {

    private static final String NODES_BY_ID_UNTIL_LAST_LEVEL = "SELECT NODEID, NODETITLE FROM NODE START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID";
    private static final String NODES_BY_ID_UNTIL_TO_LEVEL = "SELECT NODEID, NODETITLE FROM NODE WHERE LEVEL <= ? START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID";
    private static final String NODES_BY_ID_UNTIL_TO_LEVEL_WITH_PAGINATION = "SELECT * from (SELECT * FROM ("
            + "SELECT page.*, rownum as row_num FROM ("
            + "SELECT NODEID, NODETITLE, NODEDESC FROM NODE WHERE LEVEL <= ? START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID"
            + ") page) WHERE row_num BETWEEN ? AND ?)";
    private static final String NODES_BY_ID_UNTIL_LAST_LEVEL_WITH_PAGINATION = "SELECT * from (SELECT * FROM ("
            + "SELECT page.*, rownum as row_num FROM ("
            + "SELECT NODEID, NODETITLE, NODEDESC FROM NODE START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID"
            + ") page) WHERE row_num BETWEEN ? AND ?)";
    private static final String NODE_SEARCH_TERMS_BY_ID_UNTIL_LAST_LEVEL = "SELECT NODE.NODEID,"
            + "NODE.NODETITLE,"
            + "CORPUS.CORPUSTERMS,"
            + "LINKAPEDIA.GET_MUSTHAVE_WORDS(NODE.NODEID) AS NODE_MUSTWORDS,"
            + "NODE.NODEMUSTHAVEGATEINHERITANCE,"
            + "CASE"
            + " WHEN NODE.NODEMUSTHAVEGATEINHERITANCE IS NULL THEN (LINKAPEDIA.GET_MUSTHAVE_WORDS(NODE.NODEID))"
            + " WHEN NODE.NODEMUSTHAVEGATEINHERITANCE = 0 THEN (LINKAPEDIA.GET_MUSTHAVEGATE_WORDS(NODE.NODEID))"
            + " ELSE           (LINKAPEDIA.GET_MUSTHAVE_WORDS(("
            + "        SELECT PARENT_NODE.NODEID"
            + "        FROM NODE PARENT_NODE"
            + "        WHERE LEVEL = NODE.NODEMUSTHAVEGATEINHERITANCE"
            + "        START WITH PARENT_NODE.NODEID = NODE.PARENTID"
            + "        CONNECT BY PRIOR PARENT_NODE.PARENTID = "
            + "        PARENT_NODE.NODEID)))"
            + " END AS INHERITANCE_MUSTWORDS"
            + " FROM NODE"
            + " INNER JOIN CORPUS ON CORPUS.CORPUSID = NODE.CORPUSID"
            + " START WITH NODE.NODEID = ? CONNECT BY PRIOR "
            + "NODE.NODEID=NODE.PARENTID";
    private static final String NODE_SEARCH_TERMS_BY_ID_UNTIL_LEVEL = "SELECT NODE.NODEID,"
            + "NODE.NODETITLE,"
            + "CORPUS.CORPUSTERMS,"
            + "LINKAPEDIA.GET_MUSTHAVE_WORDS(NODE.NODEID) AS NODE_MUSTWORDS,"
            + "NODE.NODEMUSTHAVEGATEINHERITANCE,"
            + "CASE"
            + " WHEN NODE.NODEMUSTHAVEGATEINHERITANCE IS NULL THEN (LINKAPEDIA.GET_MUSTHAVE_WORDS(NODE.NODEID))"
            + " WHEN NODE.NODEMUSTHAVEGATEINHERITANCE = 0 THEN (LINKAPEDIA.GET_MUSTHAVEGATE_WORDS(NODE.NODEID))"
            + " ELSE           (LINKAPEDIA.GET_MUSTHAVE_WORDS(("
            + "        SELECT PARENT_NODE.NODEID"
            + "        FROM NODE PARENT_NODE"
            + "        WHERE LEVEL = NODE.NODEMUSTHAVEGATEINHERITANCE"
            + "        START WITH PARENT_NODE.NODEID = NODE.PARENTID"
            + "        CONNECT BY PRIOR PARENT_NODE.PARENTID = "
            + "        PARENT_NODE.NODEID)))"
            + " END AS INHERITANCE_MUSTWORDS"
            + " FROM NODE"
            + " INNER JOIN CORPUS ON CORPUS.CORPUSID = NODE.CORPUSID"
            + " WHERE LEVEL <= ? START WITH NODE.NODEID = ? CONNECT BY PRIOR "
            + "NODE.NODEID=NODE.PARENTID";
    private static final String UPDATE_IMAGE_INFO = "UPDATE NODE SET IMAGE_URL_SOURCE=?, IMAGE_URL=?, IMAGE_HASH=?, IMAGE_LINKAPEDIA_SITE=? WHERE NODEID = ?";
    private static final String GET_COUNT_WIKIPEDIA_IMAGES_UNTIL_LEVEL = "SELECT COUNT(*) FROM NODE WHERE IMAGE_URL LIKE '%wikipedia%' AND LEVEL <=? START WITH NODEID=? CONNECT BY PRIOR NODEID=PARENTID";
    private static final String GET_COUNT_WIKIPEDIA_IMAGES_UNTIL_LAST_LEVEL = "SELECT COUNT(*) FROM NODE WHERE IMAGE_URL LIKE '%wikipedia%' START WITH NODEID=? CONNECT BY PRIOR NODEID=PARENTID";
    private static final String GET_ROOT_NODE_BY_CORPUSID = "SELECT NODE.NODEID FROM NODE WHERE NODE.PARENTID=-1 and NODE.CORPUSID = ?";
    private static final String GET_NODE = "SELECT NODE.NODEID, NODE.PARENTID, NODE.NODETITLE, NODE.CORPUSID, NODE.NODEDESC FROM NODE WHERE NODE.NODEID = ?";
    private static final String SAVE_NODE_MUSTHAVES = "INSERT INTO MUSTHAVE (NODEID, MUSTWORD, LANG) VALUES (?,?,?)";
    private static final String SAVE_NODE_MUSTHAVES_GATE = "INSERT INTO NODEMUSTHAVEGATE (NODEID, MUSTWORD, LANG) VALUES (?,?,?)";
    private static final String UPDATE_NODES_INHERITANCE = "UPDATE NODE SET NODEMUSTHAVEGATEINHERITANCE=? WHERE NODEID = ?";
    private static final String UPDATE_NODES_DISCUSSIONID = "UPDATE NODE SET DISCUSSIONID = ? WHERE NODEID = ?";
    private static final String DELETE_NODE_MUST_HAVE = "delete from musthave where nodeid in (SELECT NODEID FROM NODE WHERE LEVEL <= ? START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID)";
    private static final String DELETE_NODE_MUST_HAVE_UNTIL_LAST_LEVEL = "delete from musthave where nodeid in (SELECT NODEID FROM NODE START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID)";
    private static final String DELETE_NODE_MUST_HAVE_GATE = "delete from nodemusthavegate where nodeid in (SELECT NODEID FROM NODE WHERE LEVEL <= ? START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID)";
    private static final String DELETE_NODE_MUST_HAVE_GATE_UNTIL_LAST_LEVEL = "delete from nodemusthavegate where nodeid in (SELECT NODEID FROM NODE START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID)";
    private static final String DELETE_NODES_FROM_LEVEL = "DELETE FROM NODE WHERE NODEID IN (SELECT NODEID FROM NODE WHERE LEVEL >= ? START WITH NODEID = ? CONNECT BY PRIOR NODEID = PARENTID)";
    private static final String GET_NODES_BY_PARENTID = "SELECT NODEID FROM NODE WHERE PARENTID = ?";
    private static int BATCH_UPDATER = 1000;

    public static List<Node> getNodesByIdUntilLevel(int id, int toLevel, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(NODES_BY_ID_UNTIL_TO_LEVEL);
            pstmt.setInt(1, toLevel);
            pstmt.setInt(2, id);
            resultNodes = pstmt.executeQuery();
            return createNodesFromResultSet(resultNodes);
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    public static void deleteNodesByIdFromLevel(int id, int fromLevel, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(DELETE_NODES_FROM_LEVEL);
            pstmt.setInt(1, fromLevel);
            pstmt.setInt(2, id);
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }

    public static List<Node> getNodesByIdUntilLastLevel(int id, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(NODES_BY_ID_UNTIL_LAST_LEVEL);
            pstmt.setInt(1, id);
            resultNodes = pstmt.executeQuery();
            return createNodesFromResultSet(resultNodes);
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    public static Map<String, String> getNodeSearchTermByIdUntilLastLevel(int id, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(NODE_SEARCH_TERMS_BY_ID_UNTIL_LAST_LEVEL);
            pstmt.setInt(1, id);
            resultNodes = pstmt.executeQuery();
            return createNodeSearchTermByResultSet(resultNodes);
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    public static Map<String, String> getNodeSearchTermByIdUntilLevel(int id, int toLevel, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(NODE_SEARCH_TERMS_BY_ID_UNTIL_LEVEL);
            pstmt.setInt(1, toLevel);
            pstmt.setInt(2, id);
            resultNodes = pstmt.executeQuery();
            return createNodeSearchTermByResultSet(resultNodes);
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    private static Map<String, String> createNodeSearchTermByResultSet(ResultSet rs) throws SQLException {
        int mustHaveInheritance;
        String nodeId;
        String nodeMusthave;
        String corpusTerm;
        String inheritanceMustHave;
        Map<String, String> nodeSearchTerm = new HashMap<>();
        while (rs.next()) {
            nodeId = rs.getString("NODEID");
            mustHaveInheritance = getMusthaveInheritance(rs);
            nodeMusthave = getFirstSearchTermFromResultSetByNameField(rs, "NODE_MUSTWORDS");
            corpusTerm = getFirstSearchTermFromResultSetByNameField(rs, "CORPUSTERMS");
            inheritanceMustHave = getFirstSearchTermFromResultSetByNameField(rs, "INHERITANCE_MUSTWORDS");
            if (nodeMusthave.trim().isEmpty()) {
                continue;
            }

            if (mustHaveInheritance == -1) {
                nodeSearchTerm.put(nodeId, formatSearchTerm(corpusTerm, nodeMusthave));
                continue;
            }
            nodeSearchTerm.put(nodeId, formatSearchTerm(inheritanceMustHave, nodeMusthave));
        }
        return nodeSearchTerm;

    }

    private static String formatSearchTerm(String searchTerm, String mustHave) {
        if (!searchTerm.isEmpty()) {
            return "\"" + mustHave + "\" "
                    + "\"" + searchTerm + "\"";
        }
        return "\"" + mustHave + "\"";

    }

    private static int getMusthaveInheritance(ResultSet rs) throws SQLException {
        if (rs.getString("NODEMUSTHAVEGATEINHERITANCE") == null) {
            return -1;
        }
        return rs.getInt("NODEMUSTHAVEGATEINHERITANCE");
    }

    private static String getFirstSearchTermFromResultSetByNameField(ResultSet rs, String fieldName) throws SQLException {
        if (rs.getString(fieldName) == null) {
            return "";
        }
        return rs.getString(fieldName).split("\\|\\|")[0];
    }

    private static List<Node> createNodesFromResultSet(ResultSet rs) throws SQLException {
        List<Node> nodes = new ArrayList<>();
        Node node = null;
        while (rs.next()) {
            node = new Node();
            node.setNodeId(rs.getInt("NODEID"));
            node.setNodeTitle(rs.getString("NODETITLE"));
            nodes.add(node);
        }
        return nodes;
    }

    public static void updateImageInfo(int nodeId, String urlImageSource, String urlImage, String imageHash, String imageLinkapediaUrl, Connection connection) throws SQLException {
        PreparedStatement pstmt = connection.prepareStatement(UPDATE_IMAGE_INFO);
        try {
            pstmt.setString(1, urlImageSource);
            pstmt.setString(2, urlImage);
            pstmt.setString(3, imageHash);
            pstmt.setString(4, imageLinkapediaUrl);
            pstmt.setInt(5, nodeId);
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }

    public static int getCountWikipediaImages(int nodeId, int toLevel, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(GET_COUNT_WIKIPEDIA_IMAGES_UNTIL_LEVEL);
            pstmt.setInt(1, toLevel);
            pstmt.setInt(2, nodeId);
            resultNodes = pstmt.executeQuery();
            resultNodes.next();
            return resultNodes.getInt(1);
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    public static int getCountWikipediaImages(int nodeId, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(GET_COUNT_WIKIPEDIA_IMAGES_UNTIL_LAST_LEVEL);
            pstmt.setInt(1, nodeId);
            resultNodes = pstmt.executeQuery();
            resultNodes.next();
            return resultNodes.getInt(1);
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    public static int getRootNode(int corpusId, Connection connection) throws SQLException {
        ResultSet resultSet = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(GET_ROOT_NODE_BY_CORPUSID);
            pstmt.setInt(1, corpusId);
            resultSet = pstmt.executeQuery();
            resultSet.next();
            return resultSet.getInt("NODEID");
        } finally {
            resultSet.close();
            pstmt.close();
        }
    }

    public static Node getNode(int nodeId, Connection connection) throws SQLException {
        ResultSet resultSet = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(GET_NODE);
            pstmt.setInt(1, nodeId);
            resultSet = pstmt.executeQuery();
            resultSet.next();
            return new Node(resultSet.getInt("NODEID"),
                    resultSet.getInt("PARENTID"),
                    resultSet.getString("NODETITLE"),
                    resultSet.getInt("CORPUSID"),
                    resultSet.getString("NODEDESC"));
        } finally {
            resultSet.close();
            pstmt.close();
        }
    }

    public static List<Node> getNodesByIdUntilLevelWithPagination(long id, int toLevel,
            int from, int to, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(NODES_BY_ID_UNTIL_TO_LEVEL_WITH_PAGINATION);
            pstmt.setInt(1, toLevel);
            pstmt.setLong(2, id);
            pstmt.setInt(3, from);
            pstmt.setInt(4, to);
            resultNodes = pstmt.executeQuery();
            List<Node> nodes = new ArrayList<>();
            Node node = null;
            while (resultNodes.next()) {
                node = new Node();
                node.setNodeId(resultNodes.getInt("NODEID"));
                node.setNodeTitle(resultNodes.getString("NODETITLE"));
                node.setDescription(resultNodes.getString("NODEDESC"));
                nodes.add(node);
            }
            return nodes;
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    public static List<Node> getNodesByIdUntilLastLevelWithPagination(long id,
            int from, int to, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultNodes = null;
        try {
            pstmt = connection.prepareStatement(NODES_BY_ID_UNTIL_LAST_LEVEL_WITH_PAGINATION);
            pstmt.setLong(1, id);
            pstmt.setInt(2, from);
            pstmt.setInt(3, to);
            resultNodes = pstmt.executeQuery();
            List<Node> nodes = new ArrayList<>();
            Node node = null;
            while (resultNodes.next()) {
                node = new Node();
                node.setNodeId(resultNodes.getInt("NODEID"));
                node.setNodeTitle(resultNodes.getString("NODETITLE"));
                node.setDescription(resultNodes.getString("NODEDESC"));
                nodes.add(node);
            }
            return nodes;
        } finally {
            resultNodes.close();
            pstmt.close();
        }
    }

    public static void insertNodesMusthaves(Map<Integer, List<String>> nodesTerms, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            connection.setAutoCommit(false);
            pstmt = connection.prepareStatement(SAVE_NODE_MUSTHAVES);
            List<String> terms = new ArrayList<>();
            int nodeId;
            for (Map.Entry<Integer, List<String>> entry : nodesTerms.entrySet()) {
                terms = entry.getValue();
                nodeId = entry.getKey();
                for (String musthave : terms) {
                    pstmt.setInt(1, nodeId);
                    pstmt.setString(2, musthave);
                    pstmt.setString(3, "EN");
                    pstmt.addBatch();
                }
            }
            pstmt.executeBatch();
            pstmt.clearBatch();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.setAutoCommit(true);
            pstmt.close();
        }
    }

    public static void insertNodesMusthavesGate(Map<Integer, String> nodesTerms, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            connection.setAutoCommit(false);
            pstmt = connection.prepareStatement(SAVE_NODE_MUSTHAVES_GATE);
            int nodeId;
            String musthaveGate;
            for (Map.Entry<Integer, String> entry : nodesTerms.entrySet()) {
                musthaveGate = entry.getValue();
                nodeId = entry.getKey();
                pstmt.setInt(1, nodeId);
                pstmt.setString(2, musthaveGate);
                pstmt.setString(3, "EN");
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.clearBatch();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.setAutoCommit(true);
            pstmt.close();
        }
    }

    public static void updateNodeInheritance(List<Integer> nodes, int inhertitance, Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            connection.setAutoCommit(false);
            pstmt = connection.prepareStatement(UPDATE_NODES_INHERITANCE);
            for (int nodeId : nodes) {
                pstmt.setInt(1, inhertitance);
                pstmt.setInt(2, nodeId);
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.clearBatch();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.setAutoCommit(true);
            pstmt.close();
        }
    }

    public static void deleteMustHaveByNodeIdUntilLevel(long nodeId, int toLevel, Connection con) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(DELETE_NODE_MUST_HAVE);
            pstmt.setInt(1, toLevel);
            pstmt.setLong(2, nodeId);
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }

    public static void deleteMustHaveByNodeIdLastLevel(long nodeId, Connection con) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(DELETE_NODE_MUST_HAVE_UNTIL_LAST_LEVEL);
            pstmt.setLong(1, nodeId);
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }

    public static void deleteMustHaveGateByNodeIdUntilLevel(long nodeId, int toLevel, Connection con) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(DELETE_NODE_MUST_HAVE_GATE);
            pstmt.setInt(1, toLevel);
            pstmt.setLong(2, nodeId);
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }

    public static void deleteMustHaveGateByNodeIdLastLevel(long nodeId, Connection con) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(DELETE_NODE_MUST_HAVE_GATE_UNTIL_LAST_LEVEL);
            pstmt.setLong(1, nodeId);
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }

    public static List<Node> getNodesByParentId(int parentId, Connection con) throws SQLException {
        List<Node> listReturn = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(GET_NODES_BY_PARENTID);
            pstmt.setLong(1, parentId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Node node = new Node();
                node.setNodeId(rs.getInt("NODEID"));
                listReturn.add(node);
            }
            return listReturn;
        } finally {
            rs.close();
            pstmt.close();
        }
    }

    public static void updateDiscussionId(List<Node> nodes, int discussionId,
            Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            connection.setAutoCommit(false);
            pstmt = connection.prepareStatement(UPDATE_NODES_DISCUSSIONID);
            int cont = 0;
            for (Node node : nodes) {
                pstmt.setInt(1, discussionId);
                pstmt.setInt(2, node.getNodeId());
                pstmt.addBatch();
                if (cont == BATCH_UPDATER) {
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            pstmt.executeBatch();
            pstmt.clearBatch();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.setAutoCommit(true);
            pstmt.close();
        }
    }
}
