package com.linkapedia.procinq.server.musthave;

import api.tsmusthave.RegexHandler;
import api.util.ConfServer;
import com.iw.db.ConnectionFactory;
import com.linkapedia.procinq.server.musthave.queue.MessageMusthaveProcess;
import com.linkapedia.procinq.server.node.Node;
import com.linkapedia.procinq.server.node.NodeDao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MusthaveProcessService {

    private static final int PAGINATION = 1000;

    public void start(MessageMusthaveProcess message) throws SQLException {
        Connection connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        ProccessStatusDao.updateProcessStatus(message.getRequestId(), "IN PROGRESS", connection);
        int from = 1;
        int to = PAGINATION;
        List<Node> nodes;
        deleteAllMusthaves(message, connection);
        do {
            nodes = getNodes(message, from, to, connection);
            Map<Integer, List<String>> musthaves = new HashMap<>();
            if (message.isUseTitleFull()) {
                Map<Integer, List<String>> musthaveFullTitle = getTermsFromNodeTitleFull(nodes);
                musthaves = musthaveFullTitle;
            }
            if (message.isUseDescription()) {
                Map<Integer, List<String>> termsFromNodeDescription = getTermsNodeDescription(nodes);
                joinObjectTerms(musthaves, termsFromNodeDescription);
            }
            NodeDao.insertNodesMusthaves(musthaves, connection);

            from = to + 1;
            to += PAGINATION;

        } while (!nodes.isEmpty());
        ProccessStatusDao.updateProcessStatus(message.getRequestId(), "FINISHED", connection);
    }

    private void deleteAllMusthaves(MessageMusthaveProcess message, Connection connection) throws SQLException {
        if (message.getToLevel() == -1) {
            NodeDao.deleteMustHaveByNodeIdLastLevel(message.getNodeId(), connection);
        } else {
            NodeDao.deleteMustHaveByNodeIdUntilLevel(message.getNodeId(), message.getToLevel(), connection);
        }
    }

    private List<Node> getNodes(MessageMusthaveProcess message, int from, int to, Connection connection) throws SQLException {
        List<Node> nodes;
        if (message.getToLevel() == -1) {
            nodes = NodeDao.getNodesByIdUntilLastLevelWithPagination(
                    message.getNodeId(), from, to, connection);
        } else {
            nodes = NodeDao.getNodesByIdUntilLevelWithPagination(
                    message.getNodeId(), message.getToLevel(), from, to, connection);
        }
        return nodes;
    }

    private void joinObjectTerms(Map<Integer, List<String>> map1, Map<Integer, List<String>> map2) {
        int nodeId;
        List<String> listAux;
        List<String> myList;
        for (Map.Entry<Integer, List<String>> entry : map2.entrySet()) {
            nodeId = entry.getKey();
            myList = entry.getValue();
            listAux = map1.get(nodeId);
            if (listAux == null) {
                map1.put(nodeId, myList);
            } else {
                listAux.addAll(myList);
                listAux = MusthavesUtils.removeDuplicateTerms(listAux);
                map1.put(nodeId, listAux);
            }
        }
    }

    private Map<Integer, List<String>> getTermsFromNodeTitleFull(List<Node> nodes) {
        Map<Integer, List<String>> nodesTerms = new HashMap<>();
        List<String> terms;
        for (Node node : nodes) {
            String node_title_filtered = MusthavesUtils.removeTextInsidePharenthesisInNodeTitle(node.getNodeTitle());
            terms = new ArrayList<>();
            terms.add(node_title_filtered);
            terms = MusthavesUtils.cleanTermsPunct(terms);
            nodesTerms.put(node.getNodeId(), terms);
        }
        return nodesTerms;
    }

    private Map<Integer, List<String>> getTermsNodeDescription(List<Node> nodes) {
        String stopRegex = "(\\band\\b|\\bor\\b|,|\\bin\\b|\\bis\\b|\\.|;|:|!|$|\\(|\\[)";
        Map<Integer, List<String>> nodesMusthaves = new HashMap<>();

        List<String> musthavesPatterns = Arrays.asList(ConfServer.getValue(
                "com.linkapedia.server.description.patterns.musthaves").split("\\|"));
        for (Node node : nodes) {
            if (node.getDescription() == null || node.getDescription().isEmpty()) {
                continue;
            }
            String normalizeTitle = Pattern.quote(node.getNodeTitle().trim());
            List<RegexHandler> list = new ArrayList<>();
            for (String musthavePattern : musthavesPatterns) {
                String regex = String.format("%s(\\S|\\s{0,})(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\b%s\\b(.*?)%s", normalizeTitle, musthavePattern, stopRegex);
                list.add(new RegexHandler(regex, 4));
            }

            String re = regexDesc(node.getDescription(), list, "");
            List<String> musthaves = new ArrayList<>();
            if (!re.isEmpty()) {
                musthaves.add(re);
                musthaves = MusthavesUtils.cleanTerms(musthaves);
                nodesMusthaves.put(node.getNodeId(), musthaves);
            }

        }
        return nodesMusthaves;
    }

    private String regexDesc(String a, List<RegexHandler> regex, String beforeResult) {
        Pattern pattern;
        String result = beforeResult;
        for (RegexHandler reg : regex) {
            try {
                pattern = Pattern.compile(reg.getRegex(), Pattern.CASE_INSENSITIVE);
                Matcher mat = pattern.matcher(a);
                if (mat.find()) {
                    result = mat.group(reg.getGroup());
                    result = result.trim();
                    if (result.isEmpty()) {
                        continue;
                    }
                    if (reg.getRegexStep2() != null) {
                        String auxResult = regexDesc(result, reg.getRegexStep2(), result);
                        if (auxResult.isEmpty()) {
                            continue;
                        } else {
                            return auxResult;
                        }
                    }
                    break;
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        if (result.matches(".*\\d.*")) {
            result = "";
        }
        if (5 < result.split("\\s+").length) {
            result = "";
        }
        return result;
    }
}
