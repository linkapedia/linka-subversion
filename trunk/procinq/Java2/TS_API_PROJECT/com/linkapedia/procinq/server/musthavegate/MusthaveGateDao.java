package com.linkapedia.procinq.server.musthavegate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MusthaveGateDao {

    private static final String DELETE_MUSTHAVESGATE_TO_LAST_LEVEL = "DELETE FROM NODEMUSTHAVEGATE mh "
            + "WHERE mh.NODEID IN (SELECT NODEID FROM NODE START WITH NODEID = ? "
            + "CONNECT BY PRIOR NODEID = PARENTID)";
    private static final String DELETE_MUSTHAVESGATE_TO_LEVEL = "DELETE FROM NODEMUSTHAVEGATE mh "
            + "WHERE mh.NODEID IN (SELECT NODEID FROM NODE "
            + "WHERE LEVEL <= ? START WITH NODEID = ? "
            + "CONNECT BY PRIOR NODEID = PARENTID)";

    public static void deleteMusthaveGateToLastLevel(Connection connection, int nodeid) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(DELETE_MUSTHAVESGATE_TO_LAST_LEVEL);
            pstmt.setInt(1, nodeid);
            pstmt.executeUpdate();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    public static void deleteMusthaveGateToLevel(Connection connection, int nodeid, int toLevel) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(DELETE_MUSTHAVESGATE_TO_LEVEL);
            pstmt.setInt(1, toLevel);
            pstmt.setInt(2, nodeid);
            pstmt.executeUpdate();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
}
