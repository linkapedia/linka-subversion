package com.linkapedia.procinq.server.musthave;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class MustHaveDao {
    
    private static final String INSERT_MUSTHAVE = "INSERT INTO MUSTHAVE (NODEID, MUSTWORD, LANG) VALUES (?,?,?)"; 
    
    public static void insertMusthave(int nodeId, String musthave, Connection connection) throws SQLException {
        PreparedStatement pstmt = connection.prepareStatement(INSERT_MUSTHAVE);
        try {
            pstmt.setInt(1, nodeId);
            pstmt.setString(2, musthave);
            pstmt.setString(3, "EN");
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }
}
