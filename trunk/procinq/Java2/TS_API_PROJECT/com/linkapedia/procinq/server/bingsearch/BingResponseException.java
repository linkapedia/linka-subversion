package com.linkapedia.procinq.server.bingsearch;

public class BingResponseException extends RuntimeException {

    public BingResponseException(String message) {
        super(message);
    }
}
