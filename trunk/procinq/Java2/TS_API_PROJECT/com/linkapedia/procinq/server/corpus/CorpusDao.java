package com.linkapedia.procinq.server.corpus;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CorpusDao {

    private static String QUERYIMAGESCORPUS = "SELECT IMAGE FROM CORPUSIMAGES WHERE CORPUSID = ?";
    private static String INSERT_CORPUS_IMAGE = "insert into CORPUSIMAGES(imageid, description,"
            + " url, corpusid, image) values(SEC_IMAGESCORPUS.nextval, ?, ?, ?, ?)";
    private static String GET_CORPUS = "SELECT CORPUSID, CORPUS_NAME FROM CORPUS WHERE CORPUSID = ?";

    public static List<byte[]> getImages(int id, Connection connection) throws SQLException {
        List<byte[]> images = new ArrayList<byte[]>();
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(QUERYIMAGESCORPUS);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                images.add(rs.getBytes("IMAGE"));
            }
            return images;
        } finally {
            rs.close();
            pstmt.close();
        }
    }

    public static void saveImage(int id, String title, String url, byte[] image, 
            Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(INSERT_CORPUS_IMAGE);
            pstmt.setString(1, title);
            pstmt.setString(2, url);
            pstmt.setInt(3, id);
            pstmt.setBinaryStream(4, new ByteArrayInputStream(image));
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }

    public static Corpus getCorpus(int corpusId, Connection connection) throws SQLException{
        ResultSet resultSet = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(GET_CORPUS);
            pstmt.setInt(1, corpusId);
            resultSet = pstmt.executeQuery();
            resultSet.next();
            return new Corpus(resultSet.getInt("CORPUSID"), 
                    resultSet.getString("CORPUS_NAME"));
                   
        } finally {
            resultSet.close();
            pstmt.close();
        }
    }
}
