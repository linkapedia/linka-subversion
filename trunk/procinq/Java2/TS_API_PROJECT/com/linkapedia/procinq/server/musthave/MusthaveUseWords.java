package com.linkapedia.procinq.server.musthave;

import com.linkapedia.procinq.server.node.Node;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MusthaveUseWords implements MusthaveGeneration {

    private Map<String, Object> filters;

    public MusthaveUseWords(Map<String, Object> filters) {
        this.filters = filters;
    }

    @Override
    public Map<Integer, List<String>> getMusthaves(List<Node> nodes)
            throws MustHaveGenerationException {
        int wordToPut = Integer.valueOf((String) this.filters.get("WORDTOPUT"));
        if (wordToPut < -1) {
            throw new MustHaveGenerationException("WORDTOPUT can't be < -1");
        }
        Map<Integer, List<String>> muthaves = new HashMap();
        for (Node node : nodes) {
            String nodeTitle = MusthavesUtils.
                    removeTextInsidePharenthesisInNodeTitle(node.getNodeTitle());
            nodeTitle = MusthavesUtils.removeStringWithDotAtEnd(nodeTitle);
            String wordToGet = "";
            String[] words = nodeTitle.split("\\s");
            if (words.length >= wordToPut && wordToPut != -1) {
                wordToGet = words[wordToPut - 1];
            } else {
                wordToGet = words[words.length - 1];
            }
            muthaves.put(node.getNodeId(), Arrays.asList(wordToGet));
        }
        return muthaves;
    }
}
