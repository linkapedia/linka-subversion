package com.linkapedia.procinq.server.musthave.queue;

import java.io.Serializable;

public class MessageMusthaveProcess implements Serializable {

    private long requestId;
    private long nodeId;
    private int toLevel;
    private boolean useDescription;
    private boolean useTitleFull;

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }

    public int getToLevel() {
        return toLevel;
    }

    public void setToLevel(int toLevel) {
        this.toLevel = toLevel;
    }

    public boolean isUseDescription() {
        return useDescription;
    }

    public void setUseDescription(boolean useDescription) {
        this.useDescription = useDescription;
    }

    public boolean isUseTitleFull() {
        return useTitleFull;
    }

    public void setUseTitleFull(boolean useTitleFull) {
        this.useTitleFull = useTitleFull;
    }

}
