package com.linkapedia.procinq.server.node;

public class Node {

    private int nodeId;
    private int parentId;
    private String nodeTitle;
    private int corpusId;
    private String description;

    public Node(int nodeId, int parentId, String nodeTitle, int corpusId, String description) {
        this.nodeId = nodeId;
        this.parentId = parentId;
        this.nodeTitle = nodeTitle;
        this.corpusId = corpusId;
        this.description = description;
    }

    public Node(int nodeId) {
        this.nodeId = nodeId;
    }

    public Node() {
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getNodeTitle() {
        return nodeTitle;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }

    public int getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(int corpusId) {
        this.corpusId = corpusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
