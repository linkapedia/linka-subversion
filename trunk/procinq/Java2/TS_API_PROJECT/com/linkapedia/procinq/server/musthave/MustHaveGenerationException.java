package com.linkapedia.procinq.server.musthave;

public class MustHaveGenerationException extends Exception {

    public MustHaveGenerationException() {
        super();
    }

    public MustHaveGenerationException(String message) {
        super(message);
    }

    public MustHaveGenerationException(String message, Throwable cause) {
        super(message, cause);
    }

    public MustHaveGenerationException(Throwable cause) {
        super(cause);
    }
}
