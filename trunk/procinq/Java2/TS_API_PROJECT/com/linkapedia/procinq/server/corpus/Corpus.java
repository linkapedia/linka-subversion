package com.linkapedia.procinq.server.corpus;

public class Corpus {

    private int corpusId;
    private String corpusName;

    public Corpus(int corpusId, String corpusName) {
        this.corpusId = corpusId;
        this.corpusName = corpusName;
    }

    public Corpus() {
    }

    public int getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(int corpusId) {
        this.corpusId = corpusId;
    }

    public String getCorpusName() {
        return corpusName;
    }

    public void setCorpusName(String corpusName) {
        this.corpusName = corpusName;
    }
}
