package com.linkapedia.procinq.server.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamUtil {

    public static byte[] readStream(InputStream stream, int streamLength) throws IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] byteBuffer = new byte[streamLength];
        int byteRead;
        try {
            while ((byteRead = stream.read(byteBuffer, 0, streamLength)) != -1) {
                bao.write(byteBuffer, 0, byteRead);
            }
            return bao.toByteArray();
        } finally {
            stream.close();
        }
    }
}
