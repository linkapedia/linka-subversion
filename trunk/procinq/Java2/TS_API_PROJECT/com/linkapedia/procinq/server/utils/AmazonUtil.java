package com.linkapedia.procinq.server.utils;

import org.apache.commons.lang.StringUtils;

public class AmazonUtil {

    private static final String FOLDER_IMAGES = "images";

    public static String getDynamoDBIdByNodeId(String nodeId) {
        return StringUtils.reverse(String.valueOf(nodeId));
    }

    public static String getImageLinkapediaUrl(String nodeId) {
        String url = "https://s3.amazonaws.com/nodeimages/";
        return url + buildKeyImage(nodeId);
    }

    public static String buildKeyImage(String id) {
        return buildKeyImage(id, 0);
    }
    
    public static String buildKeyImageMobile(String nodeId){
        return nodeId + "/" + FOLDER_IMAGES + "/" + nodeId + "_mobi";
    }

    public static String buildKeyImage(String id, int index) {
        return id + "/" + FOLDER_IMAGES + "/" + id + "_" + index + "_nocrop" + ".jpg";
    }
}
