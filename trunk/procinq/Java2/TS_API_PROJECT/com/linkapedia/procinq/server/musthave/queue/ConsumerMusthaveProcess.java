package com.linkapedia.procinq.server.musthave.queue;

import com.linkapedia.procinq.server.musthave.MusthaveProcessService;
import com.npstrandberg.simplemq.Message;
import com.npstrandberg.simplemq.MessageQueue;

public class ConsumerMusthaveProcess implements Runnable {

    private static final org.apache.log4j.Logger log =
            org.apache.log4j.Logger.getLogger(ConsumerMusthaveProcess.class);
    private int millis = 5000;
    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    @Override
    public void run() {
        MusthaveProcessService musthaveProcessService;
        MessageMusthaveProcess message = new MessageMusthaveProcess();
        Message retrieveMessage;
        while (true) {
            try {
                retrieveMessage = messageQueue.receiveAndDelete();
                if (retrieveMessage != null) {
                    message = (MessageMusthaveProcess) retrieveMessage.getObject();
                    musthaveProcessService = new MusthaveProcessService();
                    musthaveProcessService.start(message);
                }
            } catch (Exception e) {
                //error in the process a message
                log.error("Error procesing data from the queue requestId:"
                        + message.getRequestId(), e);
            }
            try {
                //TODO: manage the millis with a logic exponencial
                Thread.sleep(millis);
            } catch (InterruptedException ex) {
                log.error("A ConsumerMusthaveProcess CONSUMER IS DEAD, InterruptedException",
                        ex);
            }
        }
    }
}
