package com.linkapedia.procinq.server.musthave.queue;


import com.npstrandberg.simplemq.MessageInput;
import com.npstrandberg.simplemq.MessageQueue;

public class ProducerMusthaveProcess {

    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    public boolean sendMessage(MessageMusthaveProcess message) {
        MessageInput mi = new MessageInput();
        mi.setObject(message);
        return messageQueue.send(mi);
    }
}
