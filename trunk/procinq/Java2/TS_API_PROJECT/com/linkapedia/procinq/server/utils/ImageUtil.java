package com.linkapedia.procinq.server.utils;

import api.util.ConfServer;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.linkapedia.image.ImageBuilder;
import com.linkapedia.image.utils.ImageTypeDectector;
import java.io.IOException;

public class ImageUtil {

    public static byte[] getLinkapediaSiteImage(byte[] image) throws IOException {
        int widthCrop = Integer.parseInt(ConfServer.getValue("images.service.crop_width"));
        int heightCrop = Integer.parseInt(ConfServer.getValue("images.service.crop_height"));
        float qualityImage = Float.parseFloat(ConfServer.getValue("images.service.quality"));
        return ImageBuilder.buildLinkapediaSiteImage(image, widthCrop, heightCrop, qualityImage);
    }

    public static byte[] getLinkapediaMobileImage(byte[] image) throws IOException {
        float qualityImage = Float.parseFloat(ConfServer.getValue("images.service.quality"));
        return ImageBuilder.buildLinkapediaMobileImage(image, qualityImage);
    }

    public static ObjectMetadata createMetadataImage(byte[] image) {
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentType(getContentType(image));
        meta.setCacheControl("max-age=3888000");
        return meta;
    }

    private static String getContentType(byte[] image) {
        boolean isPng = ImageTypeDectector.isPngImage(image);
        if (isPng) {
            return "image/png";
        }
        return "image/jpeg";
    }
}
