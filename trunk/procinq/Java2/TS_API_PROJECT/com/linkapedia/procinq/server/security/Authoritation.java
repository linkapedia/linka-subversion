package com.linkapedia.procinq.server.security;

import com.iw.system.User;
import java.io.PrintWriter;

public class Authoritation {

    public static boolean isAuthorizedUser(int corpusid, String sKey, PrintWriter out) throws Exception {
        User user = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        return user.IsAuthorized(corpusid, out);
    }
}
