package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

// Given an IDRAC ID NUMBER, retrieve DOCUMENT INFORMATION
public class TSGetIdracInfo {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

		String sIdracNum = (String) props.get ("IdracNum", true);
        String sSQL = " select DocumentID, GenreID, DocTitle, DocURL, "+
					  " Country from DocumentCustom where IdracNumber = '"+sIdracNum+"'";
		Statement stmt = null; ResultSet rs = null;
			
		try {
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);
			rs.next();

			String sDocumentID = rs.getString(1);

			out.println ("<DOCUMENT> ");
			out.println ("   <DOCUMENTID>"+sDocumentID+"</DOCUMENTID>");
			out.println ("   <GENREID>"+rs.getString(2)+"</GENREID>");
			out.println ("   <DOCTITLE><![CDATA["+rs.getString(3)+"]]></DOCTITLE>");
			out.println ("   <DOCURL><![CDATA["+rs.getString(4)+"]]></DOCURL>");
			out.println ("   <COUNTRY>"+rs.getString(5)+"</COUNTRY>");
			out.println ("</DOCUMENT>");

        } catch (Exception e) {
			out.println ( "<DEBUG>No IDRAC information found for # "+sIdracNum+" </DEBUG>");
			//throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
		} finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
		}
	}
}
