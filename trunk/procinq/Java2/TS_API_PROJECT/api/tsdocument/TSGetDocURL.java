package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * This API call has been deprecated, and should no longer be used.  Please use TSGetDocProps for document information.
 * @note    This API call has been deprecated.
 */
public class TSGetDocURL {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE);
    }
}
