package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.net.*;
import java.util.Enumeration;
import java.util.zip.*;

import com.iw.system.User;

import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.results.*;
import api.security.*;

import com.indraweb.database.*;

/**
 * Retrieves ROC Validation Samples for user validation of classification.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  NodeID  Unique node identifier.
 *
 *  @note    http://ITSServer/servlet/ts?fn=tsdocument.TSGetROCDocument&SKEY=925959622&NodeID=90863

 *	@return	ITS DOCUMENT object.
 *  \verbatim
 <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <DOCUMENTS>
            <DOCUMENT>
                <NODEID>90863</NODEID>
                <DOCUMENTID>702741</DOCUMENTID>
                <FOLDERID>11</FOLDERID>
                <DOCTITLE><![CDATA[DEPLETED URANIUM]]></DOCTITLE>
                <DOCURL><![CDATA[C:\Documents and Settings\Administrator\Desktop\docs\DepletedUranium.html]]></DOCURL>
                <DOCSUMMARY><![CDATA[fuel. Is this true? How did these]]></DOCSUMMARY>
            <SCORE1>0</SCORE1>
            <SCORE2>-1</SCORE2>
            <SCORE3>0.02039234879073372</SCORE3>
            <SCORE4>0.019648102484502557</SCORE4>
            <SCORE5>0.1</SCORE5>
            <SCORE6>0.020392348790733716</SCORE6>
            <FTIMESC>0.0020392348790733716</FTIMESC>
            <DATESCORED>11/01/02 04:42</DATESCORED>
            <TYPE>0</TYPE>
            </DOCUMENT>
        </DOCUMENTS>
         <CLASSLOAD>Thu Mar 13 16:35:57 EST 2003</CLASSLOAD>
        <CALLCOUNT>2232</CALLCOUNT>
        <TIMEOFCALL_MS>10</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSGetROCDocument
{
	public static final void copyInputStream(InputStream in, OutputStream out)
		throws IOException 
	{
		byte[] buffer = new byte[1024];
	    int len;

	    while((len = in.read(buffer)) >= 0)
		out.write(buffer, 0, len);

	    in.close();
		out.close();
	}

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// Get input parameters.. GenreID is optional
		String sNodeID = (String) props.get ("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);	
		
		try {
			String sSQL = " select D.DocumentID, D.DocTitle, D.DocURL, N.DocSummary, "+
						  " N.Score1, N.Score2, N.Score3, N.Score4, N.Score5, N.Score6, "+
						  " (N.Score5 * N.Score6), "+
						  " to_char(N.DateScored, 'MM/DD/YY HH:MI'), D.GenreID from Document D, "+ 
						  " NodeDocument N where D.RepositoryID = 1 "+
						  " and D.DocumentId = N.DocumentID AND N.NodeID = "+sNodeID;
			
			ResultSet rs = null;
			Statement stmt = null;

			try {
				stmt = dbc.createStatement();
				rs = stmt.executeQuery (sSQL);			

			} 
			catch ( SQLException qe) { 
				EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
			}
			
			// Collect CLIENT data into a hash table
			int loop = 0;
			while ( rs.next() ) {
				loop++;
				if (loop == 1) { out.println ("<DOCUMENTS> "); }
				String iDocID = rs.getString(1);
				String sDocTitle = rs.getString(2);
				String sDocURL = rs.getString(3);
				String sDocSummary = rs.getString(4);
				String fScore1 = rs.getString(5);
				String fScore2 = rs.getString(6);
				String fScore3 = rs.getString(7);
				String fScore4 = rs.getString(8);
				String fScore5 = rs.getString(9);
				String fScore6 = rs.getString(10);
				String fc = rs.getString(11);
				String sDateScored = rs.getString(12);
				String slGenreID = rs.getString(13);

				out.println ("   <DOCUMENT> ");
				out.println ("	    <NODEID>"+sNodeID+"</NODEID>");
				out.println ("	    <DOCUMENTID>"+iDocID+"</DOCUMENTID>");
				out.println ("      <FOLDERID>"+slGenreID+"</FOLDERID>");
				out.println ("      <DOCTITLE><![CDATA["+sDocTitle+"]]></DOCTITLE>");
				out.println ("      <DOCURL><![CDATA["+sDocURL+"]]></DOCURL>");
				out.print ("      <DOCSUMMARY><![CDATA[");
				for (int k = 0; k < sDocSummary.length(); k++) {
					int ichar = (int) sDocSummary.charAt(k);
					if ((ichar != 13) && (ichar != 10)) { out.print(sDocSummary.charAt(k)); }
				}
				out.println ("]]></DOCSUMMARY>");
				out.println ("      <SCORE1>"+fScore1+"</SCORE1>");
				out.println ("      <SCORE2>"+fScore2+"</SCORE2>");
				out.println ("      <SCORE3>"+fScore3+"</SCORE3>");
				out.println ("      <SCORE4>"+fScore4+"</SCORE4>");
				out.println ("      <SCORE5>"+fScore5+"</SCORE5>");
				out.println ("      <SCORE6>"+fScore6+"</SCORE6>");
				out.println ("      <FTIMESC>"+fc+"</FTIMESC>");
				out.println ("      <DATESCORED>"+sDateScored+"</DATESCORED>");
				out.println ("		<TYPE>0</TYPE>");
				out.println ("   </DOCUMENT>");
			}
			rs.close(); 
			stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No documents found for Node Id "+sNodeID+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			} else { out.println ("</DOCUMENTS>"); }
		}
		
		catch ( TSException tse) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}