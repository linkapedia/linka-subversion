package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Document;
import com.iw.system.User;

/**
 * Get the list of documents classified in the given directory partial.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Folder  The whole or partial path of the watched folder.
 *  @param  Next (optional) Result to begin with.  Defaults to 1.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetDocumentsClassified
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

		String sDocumentFolder = (String) props.get ("Folder", true);
		String sNext = (String) props.get("Next", "1");
        String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // Ensure user is a member of the admin group
        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

		int istart = new Integer(sNext).intValue();
		int ifinish = istart + 99;
		
		try {
			// Select all documents from the database with <genreid> and <nodeid>
            String sSQL = Document.getSQL(dbc)+" where DocURL like '"+sDocumentFolder+"%'";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				if (loop == istart) { out.println("<DOCUMENTS>"); }
				
				if ((loop >= istart) && (loop <= ifinish)) {
                    Document d = new Document(rs);
                    d.emitXML(out, u, true);
				}
			}

			rs.close();
		    stmt.close();
		
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No information found.</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			} else { out.println("</DOCUMENTS>"); }
			out.println("<NUMRESULTS>"+loop+"</NUMRESULTS>");
		}
		
		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}
