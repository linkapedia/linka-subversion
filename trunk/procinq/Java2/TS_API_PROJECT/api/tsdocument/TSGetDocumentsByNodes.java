package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Document;
import com.iw.system.User;

/**
 * Get all documents associated within a given node list, grouped by count
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeList    A comma separated list of unique node identifiers.
 *  @param  GenreID (optional)  Limit document results to a specific genre or folder.
 *  @param  MaxResults (optional)   Maximum number of results to return.  Default is 500.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetDocumentsByNodes {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {
		String sNodeList = (String) props.get ("NodeList", true);
        String sGenreID = (String) props.get ("GenreID");
        String sMax = (String) props.get ("MaxResults", "500");
        int iMax = new Integer(sMax).intValue();

        String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // Ensure user is a member of the admin group
        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

        Statement stmt = null;
        ResultSet rs = null;
		try {
            String sSQL = Document.getSQLwithoutTable(dbc)+", count(*) from document d, nodedocument ND "+
                " where ND.NodeID in ("+sNodeList+") and d.DocumentID = ND.DocumentID ";
            if (sGenreID != null) { sSQL = sSQL + " and d.GENREID = "+sGenreID; }
            sSQL = sSQL + Document.getGroupBy(dbc) + " order by count(*) desc";

			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;

                Document d = new Document(rs);
                int iCount = rs.getInt(26);

                if (loop == 1) { out.println("<DOCUMENTS>"); }

                out.println ("   <DOCUMENT> ");
                d.emitXML(out, u, false);
                out.println ("      <COUNT>"+iCount+"</COUNT>");
				out.println ("   </DOCUMENT>");

                if (loop == iMax) break;
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No documents found in the topic identifier(s): "+sNodeList+" </DEBUG>");
                out.println("<NUMRESULTS>0</NUMRESULTS>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}
            out.println("</DOCUMENTS>");
            out.println("<NUMRESULTS>"+loop+"</NUMRESULTS>");
		}

		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
