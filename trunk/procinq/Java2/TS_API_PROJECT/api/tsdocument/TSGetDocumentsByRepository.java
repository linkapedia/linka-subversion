package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Document;
import com.iw.system.User;

/**
 * Get all documents within a given document repository.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  RID Unique repository ID.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetDocumentsByRepository {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sRID = (String) props.get ("RID", true);
        Statement stmt = null; ResultSet rs = null;
        String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // Ensure user is a member of the admin group
        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

		try {
            String sSQL = Document.getSQL(dbc, "YYYYMMDDHHMISS")+" where RepositoryID = "+sRID;
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
                if (loop == 1) { out.println("<DOCUMENTS>"); }

                Document d = new Document(rs);
                d.emitXML(out, u, true);
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No Documents were found.</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}

            out.println("</DOCUMENTS>");
		}

		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { if (stmt != null) { stmt.close(); stmt = null; } if (rs != null) { rs.close(); rs = null; } }
	}
}
