package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import api.results.*;

/**
 * @note    This API call has been deprecated.  Please use TSPrepareClientDocumentsForAnalysis
 */
public class TSPrepareDocumentsForAnalysis
{
	// TSPrepareDocumentsForAnalysis
	//
	// Take every Nth document from the server for the given node and insert it into the
	//  client database, where N = TOTAL DOCS / DOCS REQUESTED ..
	//
	// Do not count documents which have already been evaluated in the client database.
	
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sCount = (String) props.get ("Count", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
			
			// verify authorization to do this..
			if (!u.IsAdmin(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			// Call servlet running at server site.   
			String sTaxServer = com.indraweb.execution.Session.cfg.getProp("TaxServer");
			Vector vDocuments = new Vector();

			// Only call the server if one is available, and genre is not a local genre.
			if (!sTaxServer.equals("none")) {
				// Grab the zip file from the Taxonomy web server
				//String sURL = sTaxServer+"/cache/"+sNodeID+"--"+sGenreID+".zip";
				String sURL = sTaxServer+"/cache/"+sNodeID+"--null.zip";
				URL myURL = new URL(sURL); 
				HttpURLConnection httpCon = (HttpURLConnection) myURL.openConnection(); 
		
				if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) { 
					throw new Exception("Http error: " + httpCon.getResponseMessage());
				} 

				ZipInputStream zin = new ZipInputStream(httpCon.getInputStream());
				ZipEntry ze = zin.getNextEntry();

				HashTree htResults = InvokeAPI.Execute(zin);

				zin.close();

				// If this node contains signatures, build the signature list
				if (!htResults.containsKey("NODEDOCUMENTS")) {
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
				}

				HashTree htNodeDocuments = (HashTree) htResults.get("NODEDOCUMENTS");
				Enumeration e1 = htNodeDocuments.elements();
						
				while (e1.hasMoreElements()) {
					HashTree htDocResult = (HashTree) e1.nextElement();
					
					String iDocID = (String) htDocResult.get("DOCUMENTID");
					String slGenreID = (String) htDocResult.get("FOLDERID");
					String sDocTitle = (String) htDocResult.get("DOCTITLE");
					String sDocURL = (String) htDocResult.get("DOCURL");
					String sDocSummary = (String) htDocResult.get("DOCSUMMARY");
					String fScore1 = (String) htDocResult.get("SCORE1");					
					String fScore2 = (String) htDocResult.get("SCORE2");					
					String fScore3 = (String) htDocResult.get("SCORE3");					
					String fScore4 = (String) htDocResult.get("SCORE4");				
					String fScore5 = (String) htDocResult.get("SCORE5");					
					String fScore6 = (String) htDocResult.get("SCORE6");					
					String sDateScored = (String) htDocResult.get("DATESCORED");					

					NodeDocumentResult ndr 
						= new NodeDocumentResult(sNodeID, iDocID, slGenreID, 
												 new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocTitle,"'","''")),
												 new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocURL,"'","''")),
												 new String(com.indraweb.util.UtilStrings.replaceStrInStr(sDocSummary,"'","''")),
												 fScore1, fScore2, fScore3, 
												 fScore4, fScore5, fScore6,
												 sDateScored, "1");
					vDocuments.addElement(ndr);
				}
			}

			// MATH TIME.  We don't want all of the documents, only every Nth.
			//  So we must first calculate N
			float N = new Float(vDocuments.size()).floatValue() / new Float(sCount).floatValue();
			if (N < 1.0) { N = new Float(1.0).floatValue(); }
			float Ncounter = N; int iInsertCounter = 0;
			
			for (int i = 1; i <= vDocuments.size(); i++) {
				int iN = java.lang.Math.round (Ncounter);
				if (i == iN) { 
					InsertIntoClient(dbc, (NodeDocumentResult) vDocuments.elementAt(i-1), out);
					iInsertCounter++; Ncounter = Ncounter + N;
				}
			}

			if (iInsertCounter > 0) { 
				out.println("<SUCCESS>"+iInsertCounter+" documents inserted.</SUCCESS>");
			}
			
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
	public static boolean InsertIntoClient (Connection dbc, NodeDocumentResult ndr, PrintWriter out) 
	throws Exception {

		String sSQL = " select document_seq.nextval from dual";
		api.Log.Log("<SQL>"+sSQL+"</SQL>");
		Statement stmt = dbc.createStatement();	
		ResultSet rs = stmt.executeQuery (sSQL);
		rs.next();
			
		int iDocumentID = rs.getInt(1);
		rs.close(); stmt.close();
		
		// First: Insert into the Document table
		sSQL = "insert into document (documentid, genreid, doctitle, docurl, repositoryid) "+
		 	   "values ("+iDocumentID+", "+ndr.GetFolderID()+", '"+ndr.GetDocTitle()+"', "+
			   "'"+ndr.GetDocURL()+"', 1)";

		stmt = dbc.createStatement();	
		try { stmt.executeUpdate (sSQL); }
		catch (Exception e) { 
			// Document already exists -- Update repositoryid and get the real document identifer
		    stmt.close();
			sSQL = "select documentid from document where docurl = '"+ndr.GetDocURL()+"'";
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL); rs.next();
			iDocumentID = rs.getInt(1); rs.close();
			stmt.close();
			
			sSQL = "update document set repositoryid = 1 where documentid = "+iDocumentID;
			stmt = dbc.createStatement();	
			stmt.executeUpdate (sSQL);				
		}
	    stmt.close();
		
		// Second: Insert into the NodeDocument table
		sSQL = "insert into nodedocument (NodeID, DocumentID, Score1, Score2, Score3, "+
			   "Score4, Score5, Score6, DocSummary) values "+
			   "("+ndr.GetNodeID()+", "+iDocumentID+", "+ndr.GetScore1()+", "+ndr.GetScore2()+", "+
			   ndr.GetScore3()+", "+ndr.GetScore4()+", "+ndr.GetScore5()+", "+ndr.GetScore6()+", '"+
			   ndr.GetDocSummary()+"')";
		stmt = dbc.createStatement();	

		// If query statement failed, throw an exception
		try { stmt.executeUpdate (sSQL); }
		catch (Exception e) { 
			api.Log.LogError("Warning: insert failed ("+sSQL+")\r\n", e);
			return false;
		}
	    stmt.close();
		
		// Finally: Insert into the ROCValidation table
		sSQL = "insert into ROCValidation values ("+ndr.GetNodeID()+", "+iDocumentID+", 0)";
		stmt = dbc.createStatement();	

		// If query statement failed, throw an exception
		try { stmt.executeUpdate (sSQL); }
		catch (Exception e) { 
			api.Log.LogError("Warning: insert failed ("+sSQL+")\r\n", e);
			return false;
		}
	    stmt.close();
		
		return true;
	}
}
