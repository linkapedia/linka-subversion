package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.iw.system.User;
import com.iw.system.Document;

/**
 *  Returns a list of narratives in the database.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note  http://itsserver/servlet/ts?fn=tsdocument.TSGetNarratives&SKEY=993135977

 *	@return	a series of corpus objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <DOCUMENTS>
            <DOCUMENT>
                <DOCUMENTID>29517</DOCUMENTID>
                <DOCTITLE><![CDATA[Chemical Reactions]]></DOCTITLE>
                <DOCURL><![CDATA[http://66.134.131.62:8101/document/21k2/cc00.html]]></DOCURL>
                <COUNT>14</COUNT>
            </DOCUMENT>
 ...
  \endverbatim
 */
public class TSGetNarratives
{
	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception
	{

        //boolean bDebugging  = false;
        Statement stmt = null;
        ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = Document.getSQLwithoutTable(dbc)+" from Document d, Narrative N where N.DocumentID = D.DocumentID";
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			Hashtable htAuth = u.GetAuthorizedHash(out);

			while ( rs.next() ) {
                loop++;
                if (loop == 1) out.println("<DOCUMENTS>");
                Document d = new Document(rs);
                d.emitXML(out, u, true);
			}

            if (loop > 0) out.println("</DOCUMENTS>");

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No narratives found</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_CORPORA_THIS_DB);
			}
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            rs.close(); stmt.close(); rs = null; stmt = null;
        }

	}
}
