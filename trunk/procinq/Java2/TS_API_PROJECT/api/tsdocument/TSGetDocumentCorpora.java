package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * Get the distinct list of all corpora with any associations to the given document path
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Path    The URL or UNC that links uniquely to this document.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsdocument.TSAddDocument&SKEY=-132981656&DocUrl=www.test.com&DocTitle=test%20document&GenreID=0

 *	@return	SUCCESS tag if the relationship was added successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationship added successfully</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetDocumentCorpora {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDocumentPath = (String) props.get ("Path", true);

		try {
			String sSQL = " select CorpusID from Node where NodeID in "+
						  " (select nodeid from nodedocument N, document D where "+
						  "  N.DocumentID = D.DocumentID and D.DocUrl = '"+
						  sDocumentPath+"')";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				
				String sCorpusID = rs.getString(1);
		
				out.println ("   <DOCUMENT> ");
				out.println ("   <CORPUSID>"+sCorpusID+"</CORPUSID>");
				out.println ("   </DOCUMENT>");
			}
		
			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No information found for Document: "+sDocumentPath+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}
		}
		
		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}
