package api.tsdocument;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.Log;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 *  List all subject areas in the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSGetNarrativeTypes&SKEY=993135977

 *	@return	series of subject area objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NARRATIVETYPES>
        <NARRATIVETYPE>
            <NARRATIVETYPEID>3</NARRATIVETYPEID>
            <NARRATIVETYPENAME>Threats</NARRATIVETYPENAME>
        </NARRATIVETYPE>
 ...
 </TSRESULT>
  \endverbatim
 */
public class TSGetNarrativeTypes
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// Corpus ID is optional
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null; ResultSet rs = null;

		try {

			String sSQL = " select NarrativeID, Name from NARRATIVETYPE";
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				int iID = rs.getInt(1);
				String sName = rs.getString(2);

				loop = loop + 1;
				if (loop == 1) { out.println ("<NARRATIVETYPES>"); }
				out.println ("   <NARRATIVETYPE> ");
				out.println ("      <NARRATIVETYPEID>"+iID+"</NARRATIVETYPEID>");
				out.println ("      <NARRATIVETYPENAME>"+sName+"</NARRATIVETYPENAME>");
				out.println ("   </NARRATIVETYPE>");
			}
			if (loop > 0) {	out.println ("</NARRATIVETYPES>"); }
		} catch ( Exception e ) {
			Log.LogError(e);
		} finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
