package api;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;

public class APIProps extends Properties {
	private static final long serialVersionUID = 2329298979331759459L;

	// *********************************************************************
	public APIProps() {
		super();
	}

	// *********************************************************************
	public APIProps(Properties props) {
		super(props);
	}
	// *********************************************************************
	public APIProps(String sFileName) throws Exception {
		FileInputStream fis = new FileInputStream(sFileName);
		// java.util.Vector v =
		// com.indraweb.util.UtilFile.getVecOfFileStrings(sFileName);
		// for ( int i = 0; i < v.size(); i++ )
		// {
		// com.indraweb.util.Log.log( " apiprops file loader: [" +
		// v.elementAt(i) + "\r\n" );
		// }
		this.load(fis);
	}

	// *********************************************************************
	public Object get(String soKey, boolean bRequired) throws TSException {
		String sValue = (String) get(soKey.toLowerCase());
		if (bRequired && sValue == null) {
			throw new TSException(api.emitxml.EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS, "api props missing parameter [" + soKey.toString().toLowerCase() + "]");
		}
		return sValue;
	}

	// *********************************************************************
	public Object get(String soKey, String sDefault) throws TSException {
		Object sValue = super.get(soKey.toLowerCase());
		if (sValue == null) {
			/*
			 * Do not print output for parameters if (
			 * APIHandler.getEmittingXML() ) { APIHandler.getout().println
			 * ("<PARMDFLT>"+soKey.toLowerCase()+"="+sDefault+"</PARMDFLT>"); }
			 */
			return sDefault;
		}
		/*
		 * Do not print output for parameters else { if (sValue != null &&
		 * APIHandler.getEmittingXML() ) { APIHandler.getout().println
		 * ("<PARM>"+soKey.toLowerCase()+"="+sValue+"</PARM>"); } }
		 */
		return sValue;
	}

	// *********************************************************************
	public Object get(String soKey) {
		Object sValue = super.get(soKey.toLowerCase());

		/*
		 * Do not print output for parameters if (sValue != null &&
		 * APIHandler.getEmittingXML() ) { APIHandler.getout().println
		 * ("<PARM>"+soKey.toLowerCase()+"="+sValue+"</PARM>"); }
		 */
		return sValue;
	}

	// *********************************************************************
	public boolean getbool(String soKey, boolean bRequiredParm) throws TSException {
		return Boolean.valueOf((String) get(soKey.toLowerCase(), bRequiredParm)).booleanValue();
	}

	// *********************************************************************
	public boolean getbool(String soKey, String sDefault) throws TSException {
		String sBool = (String) get(soKey.toLowerCase(), sDefault);
		boolean b1 = Boolean.valueOf(sBool).booleanValue();
		return b1;
	}

	// *********************************************************************
	public int getint(String soKey, boolean bRequiredParm) throws TSException {
		return Integer.valueOf((String) get(soKey.toLowerCase(), bRequiredParm)).intValue();
	}

	// *********************************************************************
	public int getint(String soKey, String sDefault) throws TSException {
		String sInt = (String) get(soKey.toLowerCase(), sDefault);
		int i = Integer.valueOf(sInt).intValue();
		return i;
	}

	// *********************************************************************
	public double getdbl(String soKey, boolean bRequiredParm) throws TSException {
		return Double.valueOf((String) get(soKey.toLowerCase(), bRequiredParm)).doubleValue();
	}

	// *********************************************************************
	public double getdbl(String soKey, String sDefault) throws TSException {
		String sDbl = (String) get(soKey.toLowerCase(), sDefault);
		double d = Double.valueOf(sDbl).intValue();
		return d;
	}

	// *********************************************************************
	public boolean getbool(String soKey) {
		return Boolean.valueOf((String) get(soKey.toLowerCase())).booleanValue();
	}

	// *********************************************************************
	public void put(String sKey, String sValue) {
		super.put(sKey.toLowerCase(), sValue);
	}

	// *********************************************************************
	public void remove(String soKey) {
		super.remove(soKey.toLowerCase());
	}

	// *********************************************************************
	public static int getFromFile_int(String sPropFileName, String sPropName, boolean bRequiredParm) throws Exception {
		sPropName = sPropName.toLowerCase();
		APIProps apip = new APIProps(sPropFileName);
		return apip.getint(sPropName, bRequiredParm);
	}
	// *********************************************************************
	public static double getFromFile_double(String sPropFileName, String sPropName, boolean bRequiredParm) throws Exception {
		sPropName = sPropName.toLowerCase();
		APIProps apip = new APIProps(sPropFileName);
		return apip.getdbl(sPropName, bRequiredParm);
	}
	// *********************************************************************
	public static boolean getFromFile_bool(String sPropFileName, String sPropName, boolean bRequiredParm) throws Exception {
		sPropName = sPropName.toLowerCase();
		APIProps apip = new APIProps(sPropFileName);
		return apip.getbool(sPropName, bRequiredParm);
	}
	// *********************************************************************
	public static String getFromFile(String sPropFileName, String sPropName, boolean bRequiredParm) throws Exception {
		sPropName = sPropName.toLowerCase();
		APIProps apip = new APIProps(sPropFileName);
		return (String) apip.get(sPropName, bRequiredParm);
	}

	// *********************************************************************
	public String printContents() throws Exception {
		Enumeration<Object> enumeration = super.keys();
		StringBuffer sb = new StringBuffer();
		while (enumeration.hasMoreElements()) {
			String sKey = (String) enumeration.nextElement();
			String sVal = (String) super.get(sKey.toLowerCase());
			sb.append(" sKey [" + sKey + "] sVal [" + sVal + "]\r\n");
		}
		return sb.toString();
	}

	public APIProps getPropsCommonType() {
		Enumeration<Object> enumeration = super.keys();
		APIProps apiprops = new APIProps();
		while (enumeration.hasMoreElements()) {
			String sKey = (String) enumeration.nextElement();
			String sVal = (String) super.get(sKey.toLowerCase());
			apiprops.put(sKey, sVal);
		}
		return apiprops;

	}

}
