package api;

import HTML.HTMLDocument;
import Server.VectorTree;
import api.tsqa.MainTestMultiThreadClassifyCacheExerciser;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.encyclopedia.StringAndCount;
import com.indraweb.encyclopedia.Topic;
import com.indraweb.execution.Session;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.html.UtilHTML;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.Thesaurus;
import com.indraweb.ir.clsStemAndStopList;
import com.indraweb.ir.texttranslate.translators.TextTranslator_BabelFish;
import com.indraweb.profiler.Profiler;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.util.*;
import com.indraweb.utils.oracle.UtilClob;
import com.indraweb.webelements.IndraURL;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;
import com.iw.classification.Data_WordsToNodes;
import com.iw.classification.NodeCount;
import com.iw.classification.SignatureFile;
import com.iw.scoring.NodeForScore;
import com.iw.system.HashTree;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class MainTest {
    //public static int iLoopCounter = 0;

    public static long lTimeCounter = System.currentTimeMillis();
    //static String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:client";  // :client :schering :gaea
    //static String sDB = "jdbc:oracle:thin:@66.134.131.37:1521:perseus";  // :client :schering :gaea
    static String sDB = "jdbc:oracle:thin:@66.134.131.62:1521:medusa";  // :client :schering :gaea
    //static String sDB = "jdbc:oracle:thin:@66.134.131.60:1521:pandora";  // :client :schering :gaea
    //static String sDB = "jdbc:oracle:thin:@66.134.131.62:1522:hestia";  // :client :schering :gaea

    public static void main(String[] args) {

        String sFunnyChars = "<>/!@#$%^&*(){}[]\":';?/.,\\|+=-_~`";
        String sFunnyChars2 = "\u00AD";
        char c = (char) 173;
        char[] carr = new char[1];
        carr[0] = c;
        String s4 = new String(carr);
        //result.append("&#039;");
        System.out.println((int) sFunnyChars2.toCharArray()[0] + " " + s4);
        if (sFunnyChars.indexOf(sFunnyChars2) >= 0) {
            return;
        }


//        try {
//
//            fileConversion();
//            System.exit(0);
//        } catch ( Exception e )
//        {
//            e.printStackTrace();
//        }

        /*
         *
         * Object[][] oArrArrTableData = new Object[10][5]; int i3 = oArrArrTableData.length; int j3 = oArrArrTableData[0].length; System.out.println("oArrArrTableData.length:" + i3 + "
         * oArrArrTableData[0].length :" + j3); int i = oArrArrTableData.length; int j = oArrArrTableData[0].length; Object[] oArr = new Object[5]; for ( int iRow = 0; iRow < 10; iRow++) { for ( int
         * iCol = 0; iCol < 10; iCol++) { System.out.println("setting row [" + iRow + "] col [" + iCol + "] to [" + iRow+":"+iCol + "]" ); oArrArrTableData[iRow][iCol] = iRow+":"+iCol; } } oArr[0] =
         * "0"; oArr[1] = "1"; oArr[2] = "2"; oArr[3] = "3"; oArr[4] = "4"; for ( int i2 = 0; i2 < oArrArrTableData.length; i2++) { oArrArrTableData[i2] =oArr; }
         *
         * i3 = oArrArrTableData.length; j3 = oArrArrTableData[0].length; System.out.println("oArrArrTableData.length:" + i3 + " oArrArrTableData[0].length :" + j3);
         * System.out.println("oArrArrTableData.length:" + i3 + " oArrArrTableData[0].length :" + j3);
         */

        /*
         * performance test ht strings vs ints Hashtable htTest = new Hashtable(); long ltstart = System.currentTimeMillis(); for ( int i = 0; i < 100000; i++) ec { //htTest.put (new Integer(i), new
         * Integer(i)); htTest.put (tru(""+i, ""+i); } for ( int i = 0; i < 100000; i++) { Object o = htTest.get (""+i); }
         *
         * System.out.print("elapsed [" + (System.currentTimeMillis()- ltstart + "]" )); System.out.print("elapsed [" + (System.currentTimeMillis()- ltstart + "]" )); System.exit(1);
         *
         */




        System.out.println("STATE 1");
        System.out.println("in new main");
        String sOSHost = (String) java.lang.System.getProperties().get("os.name");
        if (sOSHost.toLowerCase().indexOf("windows") >= 0) {
            Session.sIndraHome = "C:/Program Files/ITS";
        } else {
            Session.sIndraHome = "/tmp/IndraHome";
        }

        Connection dbc = null;
        try {
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // START TESTS *******************************************
            // ===========================================================================
            if (false) // test how long to do 1000 title and parent title and thes expand processes
            {
                dbc = localInitSession("API");
                //int iCorpusid = 1;

                long lstart = System.currentTimeMillis();
                for (int x = 0; x < 100; x++) {
                    int iNodeID = 1402665;
                    int iThesaurusID = 1;

                    api.Log.Log("cycle : " + x);
                    String[] sArr = {"abc", "adrenalin", "aerosol", "africa", "alimentary tract system", "aminophylline", "animal origin", "annual fee", "anticoagulant agent", "anticoagulant citrate phosphate dextrose a", "antilipemic agent", "antioxidant agent", "antitussive agent", "antivitamin k", "appeal", "application letter", "approval package", "approval", "archiving", "assembling procedure", "atpc", "aurothioglucose", "austrian medicines agency", "authorized third party contractors", "automatic reading sticker", "autorizagao de funcionamento", "autorizagao especial", "avk", "azt"};
                    for (int i = 0; i < sArr.length; i++) {
                        String sSQLNodetitle = "select nodetitle from node where nodeid = 1402665";
                        String sTitleNode = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodetitle, dbc);

                        String sSQLParentNodetitle = "select nodetitle from node where nodeid = 11319546";
                        String sTitleNodeParent = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodetitle, dbc);

                        Thesaurus thes = Thesaurus.cacheGetThesaurus(false, iThesaurusID, false, dbc);
                        String s = sArr[i];
                        Vector v = thes.getMultiWordExpansions(s);


                    }
                }
                api.Log.Log("time [" + (System.currentTimeMillis() - lstart) + "]");
                //int iNodeID =
                System.exit(0);

            }

            // ===========================================================================
            if (false) // capture html to file
            {
                dbc = localInitSession("API");
                UrlHtmlAttributes urlHtmlAttributes = new UrlHtmlAttributes("http://web.tiscali.it/no-redirect-tiscali/mmacchieraldo/artmbian.htm");
                UtilHTML.captureHTMLToFile(urlHtmlAttributes, "/temp/temphtml.txt", true);
                System.exit(0);

            }

            // ===========================================================================
            if (false) // 2004 06 04     clob insert and read test
            {
                //clob insert
                dbc = localInitSession("API");
                String s = "test 16718 clob string say 30K bytes long";
                Reader reader = new StringReader(s);
                PreparedStatement pstmt = dbc.prepareStatement("INSERT INTO nodedata (nodeid, nodesource) VALUES (?, ?)");
                pstmt.setBigDecimal(1, new BigDecimal(16718));
                pstmt.setCharacterStream(2, reader, s.length());
                pstmt.execute();
                pstmt.close();

                //clob read
                String sSQL = "	select nodeid, nodesource from nodedata";
                Statement stmt = dbc.createStatement();
                ResultSet rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    int iNodeID = rs.getInt(1);
                    Clob clob = rs.getClob(2);
                    String sNodeSource = clob.getSubString(1, (int) clob.length());
                    System.out.println(iNodeID + ":" + sNodeSource);
                    //String sNodeSource = rs.getClob(2).getAsciiStream();

                }

                rs.close();
                stmt.close();


                System.exit(0);
            }
            // ===========================================================================
            if (false) // test config props behavior
            {
                dbc = localInitSession("API");
                boolean bPropTestGen = Session.cfg.getPropBool("TestGenerator", true, "false");
                System.out.println("bPropTestGen [" + bPropTestGen + "]");
            }
            // ===========================================================================
            if (false) // TSInstallCorpus obsol 2004 10 11 // ingest orthog cnf corpus normal form
            {
                // tsinstall corpus	BIO	test
                // executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TS`orpus&CorpusFolder=C:\\temp\\temp\\biologicalandchemical\\001\\002\\002&CorpusName=test hkon&CorpusActive=-1&skey=1",	dbc	);
                dbc = localInitSession("API");

                if (Session.stopList == null) // really should be done in session as a privatized variabel
                {
                    Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                }

                // deleteSigsinCorpus (7, dbc); // biosafety ...
                deleteSigsinCorpus(2200038, dbc);
                String sURLTSInstallCorpus =
                        //                "http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus" +
                        //                        //"&DoSigGen=true"+
                        //
                        //                        //"&BuildHierarchy=true" + // 1
                        //                        "&CorpusID=2200068"+      // 2
                        //                        //"&CorpusFolder=C:/temp/temp/CNF/excellent 1 test/001" + // 2
                        //                        //"&CorpusFolder=C:/temp/temp/CNF/excellent 3 test/001" + // 2
                        //                        //"&CorpusFolder=C:/temp/temp/temp/Encyclopedia Microbiology/001/001/001/001" + // 2
                        //                        //"&CorpusFolder=C:/temp/temp/CNF/excellent 4 test/001" + // 2
                        //                        //"&CorpusName=zhktest607 "+new java.util.Date().toString() + // 3
                        //                        "&SigGenRangeSize=250" +
                        //                        "&WordFreqMaxForSig=75" +
                        //                        "&NumParentTitleTermAsSigs=2" +
                        //                        "&AllowNumbers=true" +
                        //                        "&PhraseLenMin=2" +
                        //                        "&PhraseLenMax=4" +
                        //                        "&PhraseRepeatMin=2" +
                        //                        "&CorpusActive=1" +
                        //                        "&skey=1";
                        "http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus"
                        + //"&DoSigGen=true"+
                        //"&BuildHierarchy=true" + // 1
                        // "&CorpusID=7"+      // biosafety ...
                        "&CorpusID=2200038" + // 2
                        //"&CorpusFolder=C:/temp/temp/CNF/excellent 1 test/001" + // 2
                        //"&CorpusFolder=C:/temp/temp/CNF/excellent 3 test/001" + // 2
                        //"&CorpusFolder=C:/temp/temp/temp/Encyclopedia Microbiology/001/001/001/001" + // 2
                        //"&CorpusFolder=C:/temp/temp/CNF/excellent 4 test/001" + // 2
                        //"&CorpusName=zhktest607 "+new java.util.Date().toString() + // 3
                        "&NumParentTitleTermAsSigs=3"
                        + //"&SigGenRangeSize=250" +
                        "&PhraseLenMax=4"
                        + "&PhraseLenMin=2"
                        + "&WordFreqMaxForSig=75"
                        + "&includenumbers=false"
                        + //"&PhraseRepeatMin=2" +
                        //"&CorpusActive=1" +
                        "&skey=1";
//                ]] URL [http://localhost/itsapi/ts?fn=tscorpus.TSInstallCorpus&skey=-1459110810&corpusid=7&numparenttitletermassigs=3&phraselenmax
//=4&includenumbers=false&wordfreqmaxforsig=75&phraselenmin=2]
                api.Log.Log("sURLTSInstallCorpus  [" + sURLTSInstallCorpus + "]");

                // tscorpus.TSInstallCorpus


//                String sURLTSInstallCorpus =
//                    //"http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&skey=1220610721&corpusid=2200043&numparenttitletermassigs=3&phraselenmax=4&includenumbers=false&wordfreqmaxforsig=75&phraselenmin=2";
//                    "http://66.134.131.40:8100/servlet/ts?"+
//                        "fn=tscorpus.TSInstallCorpus&"+
//                        "skey=1687487528&"+
//                        "corpusid=2200040&"+
//                        "numparenttitletermassigs=3&"+
//                        "phraselenmax=4&"+
//                        "includenumbers=false&"+
//                        "wordfreqmaxforsig=75&"+
//                        "phraselenmin=2";



                //                        "fn=tscorpus.TSInstallCorpus"+
//                        "&skey=1220610721"+
//                        "&corpusid=2200040"+
//                        "&numparenttitletermassigs=3"+
//                        "&phraselenmax=4"+
//                        "&includenumbers=true"+
//                        "&wordfreqmaxforsig=83"+
//                        "&phraselenmin=2";
                executeAPI(
                        sURLTSInstallCorpus, dbc);
                //"&CorpusFolder=C:/temp/temp/CNF/anarchist 3 full/001" + // 35 or so
                //"&CorpusFolder=C:/temp/temp/CNF/3 test/001" + // 35 or so
                //"&CorpusFolder=C:/Program Files/ITS/CorpusSource/corpusingest/001&CorpusName=test hkon 5&CorpusActive=-1&skey=1" , dbc);
                //"&CorpusFolder=E:/temp/cnf/anarchist/001"+
                //"&CorpusFolder=E:/temp/cnf/dcma/001"+
                //"&CorpusFolder=C:/temp/temp/CNF/secact/Sarbanes-Oxley Act Of 2002/001" + // 35 or so
                //"&CorpusFolder=C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/001/003&CorpusName=test hkon new&CorpusActive=-1&skey=1",	dbc	);
                //"&CorpusFolder=C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/001&CorpusName=test hkon new&CorpusActive=-1&skey=1",	dbc	);
                //                C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/001
                // C:/Documents and Settings/IndraWeb/buildprocess/corpussource save/treaties/00
                //if ( false)
                System.exit(0);
            }
            if (false) // file contents obsoleted 2004 10 11
            { // classify one or a few 2003 12 06// fn: batch false
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                listFileWords(dbc);
                System.exit(0);

            }
            if (false) // 2004 06 18 tsclassifydoc and testTSCLassifyGUIStyle_forQuickDebug20040707
            { // classify one or a few 2003 12 06// fn: batch false
                //update configparams set paramvalue = '5' where paramname = 'NodeCacheCountTarget';
                //update configparams set paramvalue = '10' where paramname = 'NodeCacheCountMax';
                //commit;

                /*
                 * String s = "abcde"; String[] sa = s.split("[ab]");
                 *
                 * UtilSets.sArrPrint(sa );
                 */

                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));

                // test new sig file build file mode
                // medusa
                // perseus

                //Thesaurus t = Thesaurus.cacheGetThesaurus ( false, 100011, true, dbc );
                //Vector v = t.getMultiWordExpansions("paul");
                //Vector v = t.getMultiWordExpansions("edward paul abbey");
                //api.Log.Log ("vector [" + UtilSets.vToStr(v));
                //System.exit(0);
                // UtilFile.deleteFile("C:/Program Files/ITS/CorpusSource/SigWordCountsFile_100011_nonStem.txt");
                Vector v = new Vector();
                v.addElement(null);
                api.Log.Log(UtilSets.vToStr(v));
                //testTSCLassifyGUIStyle( "37784", "100011", null, dbc, 1, 0, null);
                // medusa? testTSCLassifyGUIStyle( "471011", "100011", null, dbc, 1, 0, null);
                // medusa
                // title test frmo html testTSCLassifyGUIStyle( "424133", "100003", "1356357", dbc, 1, 0, null);
                // summary tester
                // parallel test
                // pandora
                api.Log.Log("DELETING FILE SigWordCountsFile_2200031_nonStem.txt");
                api.Log.Log("DELETING FILE SigWordCountsFile_2200031_nonStem.txt");
                api.Log.Log("DELETING FILE SigWordCountsFile_2200031_nonStem.txt");
                api.Log.Log("DELETING FILE SigWordCountsFile_2200031_nonStem.txt");
                api.Log.Log("DELETING FILE SigWordCountsFile_2200031_nonStem.txt");
                UtilFile.deleteFile("/Program Files/ITS/CorpusSource/SigWordCountsFile_2200031_nonStem.txt");
                //testTSCLassifyGUIStyle( "439605", "100011", null, dbc, 1, 0, null);
                testTSCLassifyGUIStyle_forQuickDebug20040707("191", "100045", null, dbc, 1, 0, null);
                System.exit(0);
                testTSCLassifyGUIStyle_forQuickDebug20040707("191", "100045", null, dbc, 1, 0, null);
                testTSCLassifyGUIStyle_forQuickDebug20040707("191", "100045", null, dbc, 1, 0, null);
                testTSCLassifyGUIStyle_forQuickDebug20040707("191", "100045", null, dbc, 1, 0, null);
                //testTSCLassifyGUIStyle( "439605", "2200031", null, dbc, 1, 0, null);
                //testTSCLassifyGUIStyle( "496480", "100011", null, dbc, 1, 0, null);
/*
                 * testTSCLassifyGUIStyle( "496481", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496482", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496483",
                 * "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496484", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496485", "100011", "1402665", dbc, 1, 0,
                 * null); testTSCLassifyGUIStyle( "496486", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496487", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle(
                 * "496488", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496489", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496490", "100011", "1402665", dbc,
                 * 1, 0, null); testTSCLassifyGUIStyle( "496491", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496492", "100011", "1402665", dbc, 1, 0, null);
                 * testTSCLassifyGUIStyle( "496493", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496494", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496495",
                 * "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496496", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496497", "100011", "1402665", dbc, 1, 0,
                 * null); testTSCLassifyGUIStyle( "496498", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496499", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle(
                 * "496500", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496501", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496502", "100011", "1402665", dbc,
                 * 1, 0, null); testTSCLassifyGUIStyle( "496503", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496504", "100011", "1402665", dbc, 1, 0, null);
                 * testTSCLassifyGUIStyle( "496505", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496506", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496507",
                 * "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496508", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496509", "100011", "1402665", dbc, 1, 0,
                 * null); testTSCLassifyGUIStyle( "496510", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496511", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle(
                 * "496512", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496513", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496514", "100011", "1402665", dbc,
                 * 1, 0, null); testTSCLassifyGUIStyle( "496515", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496516", "100011", "1402665", dbc, 1, 0, null);
                 * testTSCLassifyGUIStyle( "496517", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496518", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496519",
                 * "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496520", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496521", "100011", "1402665", dbc, 1, 0,
                 * null); testTSCLassifyGUIStyle( "496522", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496523", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle(
                 * "496524", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496525", "100011", "1402665", dbc, 1, 0, null); testTSCLassifyGUIStyle( "496526", "100011", "1402665", dbc,
                 * 1, 0, null); // 2004 06 18 comment testTSCLassifyGUIStyle( "7290", "100011", "1402665", dbc, 1, 0, null);
                 */
                //Data_WordsToNodes.invalidateNodeSigCache_byCorpus("test", 100011);
                //testTSCLassifyGUIStyle( "41028", "100011", null, dbc, 1, 0, null);
                System.exit(0);
                testTSCLassifyGUIStyle("37784", "100011", null, dbc, 1, 0, null);
                // testTSCLassifyGUIStyle( "38215", "100011", null, dbc, 1, 0, null);
                //testTSCLassifyGUIStyle( "38215", "100011", null, dbc, 2, 2, "1401868");
                //testTSCLassifyGUIStyle( "38215", "100011", null, dbc, 4, 1, true, "1401868");
                System.exit(0);
                testTSCLassifyGUIStyle("37336", "100011", null, dbc, 4, 1, "1401714");
                System.exit(0);
                //      Data_WordsToNodes.invalidateNodeSigCache_byCorpus("hk",100013);
                testTSCLassifyGUIStyle("37284", "100013", null, dbc, 1, 0, "1401714");
                testTSCLassifyGUIStyle("37284", "100046", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "100090", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "100046", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "100090", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "21", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "50", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "18", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "21", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "50", null, dbc, 1, 0, "37336");

                testTSCLassifyGUIStyle("37284", "16,100007,100012,100013", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "100011", null, dbc, 1, 0, "37336");



                testTSCLassifyGUIStyle("37284", "13", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "100002", null, dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "13", "30021", dbc, 1, 0, "37336");
                testTSCLassifyGUIStyle("37284", "13", null, dbc, 1, 0, "37336");

                // 13 10
                testTSCLassifyGUIStyle("37284", "13", "30021", dbc, 1, 0, "37336");
                //Data_WordsToNodes.printCacheContents("from maintest", dbc);
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH2.2a hbk");

                // 100002 30
                testTSCLassifyGUIStyle("37284", "13", "1407969,1407970", dbc, 1, 0, "37336");
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH2.3 hbk");

                // 100011 40
                testTSCLassifyGUIStyle("37284", "100011", "1407969,1407970", dbc, 1, 0, "37336");
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH2.4 hbk");

                // 100014 50
                testTSCLassifyGUIStyle("37284", "100014", "1407969,1407970", dbc, 1, 0, "37336");
                System.exit(0);


                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH3");
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus("test", 100014);
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH4");
                testTSCLassifyGUIStyle("37284", "100014", "1407969,1407970", dbc, 1, 0, "37336");
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH5");
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus("test", 100014);
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH6");
                testTSCLassifyGUIExplainStyle("37284", "1407969", "100014", dbc);
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH7");
                testTSCLassifyGUIExplainStyle("37284", "1407969", "100014", dbc);
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH8");
                testTSCLassifyGUIStyle("37284", "100014", "1407969,1407970", dbc, 1, 0, "37336");
                api.Log.Log("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nHHH9");

                //testTSCLassify(dbc);
                System.exit(0);
            }
            if (false) // login
            { // tslogin
                dbc = localInitSession("API");
                String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?"
                        + "fn=security.TSLogin&UserID=hkon&password=racer9";
                executeAPI(sURLClassify, dbc);
                //testDom4JHtmlRead ();
                System.exit(0);
            }
            if (false) // dom4j read
            { //
                testDom4JHtmlRead();
                System.exit(0);
            }
            if (false) // 2004 04 13 testTSThesaurusExpand
            {
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));

                // test new sig file build file mode
                // medusa
                // perseus
                //UtilFile.deleteFile("C:/Program Files/ITS/CorpusSource/SigWordCountsFile_100011_nonStem.txt");

                //Thesaurus t = Thesaurus.cacheGetThesaurus ( false, 100011, true, dbc );
                //Vector v = t.getMultiWordExpansions("paul");
                //Vector v = t.getMultiWordExpansions("edward paul abbey");
                //api.Log.Log ("vector [" + UtilSets.vToStr(v));
                //System.exit(0);
                Vector v = new Vector();
                v.addElement(null);
                api.Log.Log(UtilSets.vToStr(v));
                testTSThesaurusExpand(dbc, new PrintWriter(System.out));
                System.exit(0);
            }
            if (false) // 2004 04 13  testTSGetNodeSigs
            {
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));

                // test new sig file build file mode
                // medusa
                // perseus
                //UtilFile.deleteFile("C:/Program Files/ITS/CorpusSource/SigWordCountsFile_100011_nonStem.txt");

                //Thesaurus t = Thesaurus.cacheGetThesaurus ( false, 100011, true, dbc );
                //Vector v = t.getMultiWordExpansions("paul");
                //Vector v = t.getMultiWordExpansions("edward paul abbey");
                //api.Log.Log ("vector [" + UtilSets.vToStr(v));
                //System.exit(0);
                Vector v = new Vector();
                v.addElement(null);
                api.Log.Log(UtilSets.vToStr(v));
                testTSGetNodeSigs("1402665", dbc, new PrintWriter(System.out));
                System.exit(0);
            }

            if (false) // dynamic tsclassifydoc
            {
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                testDynamicClassify(dbc);
                System.exit(0);

            }
            if (false) // explain invalidate classify a few tests all run together
            { // classify one or a few 2003 12 06// fn: batch false
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                // extra :
                //api.Log.Log ("HHH1");
                //testTSCLassifyGUIExplainStyle ("37284","1407969", "100014", dbc);

                //testTSCLassify(dbc);
                System.exit(0);
            }
            if (false) // dynamic cache mem tester
            { // classify one or a few 2003 12 06// fn: batch false
                try {
                    dbc = localInitSession("API");
                    MainTestMultiThreadClassifyCacheExerciser.mainTest(dbc);
                    System.exit(0);
                } catch (Throwable e) {
                    api.Log.LogError("top level classify test error", e);
                }
            }
            if (false) //
            {
                dbc = localInitSession("API");
                compareCorpusSigsPostInstallation(dbc);
                System.exit(0);
            }
            if (false) // test and insert clobs 2004 06 15
            {
                dbc = localInitSession("API", true);
                if (Session.stopList == null) // really should be done in session as a privatized variabel
                {
                    Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                }
                testPhraseCollector();
                System.exit(0);
            }
            if (false) // test and insert clobs 2004 06 15
            {
                dbc = localInitSession("API");
                insertClobStringsForNodeData(dbc);
                System.exit(0);
            }

            if (false) // gen oracle counts
            { // classify one or a few 2003 12 06// fn: batch false
                dbc = localInitSession("API");
                //test3DNodeDocNodeVisualization (dbc);
                //test3DNodeDocNodeVisualization_usingOracleForCount (dbc);
                //test3DNodeDocNodeVisualization_usingOracleForCount_allOracleMethod (dbc);
                //test3DNodeDocNodeVisualization_v4_builtFrom1_doAllInMem(dbc);
                //test3DNodeDocNodeVisualization_v5_sortByDocID (dbc);
                test3DNodeDocNodeVisualization_v6_sortByDocID_restrictToNodeAndUnder(dbc);
                System.exit(0);
            }

            if (false) // TSNodeRefreshApply
            {

                dbc = localInitSession("API");

                // 2002	10 14
                // tsrefreshnode
                String SKEY = "-338081444";
                if (false) {

                    String sURLRefreshNode = "http://66.134.131.40:8100/servlet/ts?"
                            + "fn=tsclassify.TSNodeRefreshApply"
                            + "&post=true"
                            + "&MaxNumNodes=100"
                            + "&DeleteProcessedNodes=false"
                            + // "&PropAffinityNodeTitlesOnly=true" + // AFFINITY
                            //"&test=true" +
                            "&MaxDocsPerNode=20"
                            + "&SKEY=" + SKEY
                            //+ "&NODEID=1316793"
                            // + "&NODEID=1311267"
                            + "&NODEID=1377184" // corpus 100006 s
                            // + "&NODEID=1314591"
                            ;
                    // + "&NODEID=1311267";
                    executeAPI(sURLRefreshNode, dbc);
                } /*
                 * else if ( true ) { String sURLRefreshNode = "http://127.0.0.1/itsapi/ts?"+ "fn=tsclassify.TSNodeRefreshApply"+ "&MAXDOCSPERNODE=-1"+ "&REPOSITORYID=-1"+
                 * "&PropAffinityNodeTitlesOnly=true"+ "&SKEY="+SKEY+ "&NODEID=1090464"; // + "&NODEID=1311267"; executeAPI (sURLRefreshNode , dbc);
                 * //MAXDOCSPERNODE=-1&REPOSITORYID=-1&PropAffinityNodeTitlesOnly=true&SKEY=398592822&NODEID=1090464 }
                 */ else if (true) // not a single test - inside noderefreshapply
                {
                    String sURL = "http://66.134.131.35/itsapi/ts?fn=tsclassify.TSClassifyDoc"
                            + "&nodeid=1090189"
                            + "&NodeSet=1090189"
                            + "&propaffinitynodetitlesonly=true"
                            + "&repositoryid=-1"
                            + "&emitxml=true"
                            + "&maxdocspernode=-1"
                            + "&maxnumnodes=100&"
                            + "deleteprocessednodes=false&"
                            + "DocIDSource=676859"
                            + "&post=true"
                            + "&skey=" + SKEY;

                } else {


                    String sURLTSClassify_RefreshNodeStyle_DocIDSource = "http://66.134.131.40:8100/servlet/ts?"
                            + "fn=tsclassify.TSClassifyDoc"
                            + "&post=true"
                            + "&SKEY=" + SKEY
                            + "&nodeset=1377184"
                            + "&emitxml=true"
                            + "&docidsource=635723";
                    //"&docidsource=628005";
                    executeAPI(sURLTSClassify_RefreshNodeStyle_DocIDSource, dbc);
                }
                // + "&NODEID=1311267";

                //IndraXMLargs 0. docidsource:200031
                //IndraXMLargs 1. skey:853827530
                //  IndraXMLargs 2. nodeset:1311267
                //  IndraXMLargs 3. emitxml:true
                //  IndraXMLargs 4. post:true
                //  IndraXMLargs 5. fn:tsclassify.TSClassifyDoc
                //  IndraXMLargs 6. nodeid:1311267


                System.exit(0);
            }

            if (false) // testTSCreateNodeSigsFromDoc  2004 05
            { // classify one or a few 2003 12 01// fn: batch false
                dbc = localInitSession("API");
                //Session.stopList = new clsStemAndStopList (Session.cfg.getProp ("IndraHome") + Session.cfg.getProp ("StopListFile"));

                try {
                    testTSCreateNodeSigsFromDoc(dbc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //testTSCLassify(dbc);
                System.exit(0);
            }
            if (false) { // dom4j
                dbc = localInitSession("API");
                executeAPI("http://localhost:8101/servlet/ts?fn=tscql.TSCql&query="
                        + "SELECT%20<DOCUMENT>%20WHERE%20DOCUMENTID%3D10000&SKEY=234234", dbc);
                //executeAPI ("http://localhost:8101/servlet/ts?fn=tscql.TSCql&query=" +
                //  "SELECT <DOCUMENT> WHERE DOCUMENTID=123&SKEY=234234", dbc);

                System.exit(0);
            }
            if (true) { // classify one or a few 2003 12 01// fn: batch false
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                testTSCLassify2(dbc);
                //testTSCLassify(dbc);
                System.exit(0);
            }
            if (false) { // classify one or a few 2003 12 01// fn: batch false
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                testXMLSeralize(dbc);
                //testTSCLassify(dbc);
                System.exit(0);
            }

            if (false) // 2003 07 30 docforscore instantiate
            {
                try {
                    String sWhichDB = "API";
                    dbc = localInitSession(sWhichDB);
//                    DocForScore docForScore = new DocForScore (
//                            "file:///C:/Program Files/ITS/CorpusSource/classificationtest/43kim0.woti-doc.htm" ,
//                            "file:///C:/Program Files/ITS/CorpusSource/classificationtest/43kim0.woti-doc.htm" ,
//                            true ,
//                            false ,
//                            "hktest" ,
//                            "no summary" ,
//                            true ,
//                            null ,
//                            true ,
//                            false,
//                            com.indraweb.execution.Session.cfg.getPropInt("NumBytesOfWebTopicsToTake"),
//                            1
//                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.exit(0);
            }
            if (false) // classify one or a few 2002 11 17  // fn: batch false
            {
                String sWhichDB = "SVR";
                dbc = localInitSession(sWhichDB);
                int iLoop = 0;
                while (iLoop < 1) {
                    System.out.println("\r\n\r\n\r\n\r\n\r\n********** MAINTEST testTSMetasearchNode iLoop  [" + iLoop + "]");
                    //com.indraweb.util.UtilFile.addLineToFile("c:/t.t", new java.util.Date () + " testTSMetasearchNode iLoop  [" + iLoop + "]\r\n");
                    System.out.flush();
                    testTSMetasearchNode(dbc); // 2003 01 25
                    iLoop++;
                }
                System.exit(0);
            }

            if (false) // test property override 2003
            {
                APIProps apiprops_override = null;
                System.out.println("found /temp/PropertyOverride.txt");
                apiprops_override = new api.APIProps("/temp/PropertyOverride.txt");
                System.out.println("found2 /temp/PropertyOverride.txt");
                Enumeration e = apiprops_override.keys();
                while (e.hasMoreElements()) {
                    String sKey = (String) e.nextElement();
                    String sValue = (String) apiprops_override.get(sKey);
                    System.out.println("setting [" + sKey + "] to [" + sValue + "]");
                }
                System.exit(0);
            }
            /*
             * //String sWhichDB = "SVR"; String sWhichDB = "API"; System.out.println("STATE 2"); dbc = localInitSession ( sWhichDB ); System.out.println("STATE 3");
             */
            if (false) {  // hbk control 2003 06 08 remove for prod. {
                System.out.println("STATE 4");
                loadWebCrawlCache(dbc);
                System.out.println("STATE 5");
            }
            if (false) { // 2003 09 04 TSGetROCData
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                testTSGetROCData(dbc);
                System.exit(0);
            }
            /*
             * if (false) // 2003 12 02 { // test DOM4j //OutputStream out = System.out; FileOutputStream fos = new FileOutputStream("c:/temp/temp.txt");
             * com.iw.ui.explain.serializetest.Dom4jTest.serializetoXML(fos , "dummyhk"); FileInputStream fis = new FileInputStream ("c:/temp/temp.txt"); org.dom4j.Document doc =
             * com.iw.ui.explain.serializetest.Dom4jTest.deserializeFromXMLin(fis); com.iw.ui.explain.serializetest.Dom4JTest3Iterator.iterateTest(doc );
             *
             * System.exit (0); }
             */
            /*
             * if (false) // 2003 12 02 { // test DOM4j tables Dom4jTestTableXML2Way.test (); System.exit (0); } if (false ) // 2003 12 03 { // test DOM4j tables TestXMLEncoder.test (); System.exit
             * (0); }
             */
            if (false) { // test oracle language multiligual 2003 10 21 //
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                // test if oracle can store funny char's  : testOracleMultilingual (dbc);
                if (false) {
                    testCorpusSigDataMigrate(dbc);
                }
                if (true) {
                    testLanguageTranslate(dbc);
                }
                System.exit(0);
            }

            if (false) // test oracle doc conversion 2003 11 19
            {
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                testURLToOracleStreamConvert(dbc);
                //testTSCLassify(dbc);
                System.exit(0);
            }


            if (false) // 2003 10 20 query time test
            { // classify one or a few 2003 10 11 // fn: batch false
                api.Log.Log("entering query time test");
                dbc = localInitSession("API");

                String sClearCache = "ALTER SYSTEM FLUSH SHARED_POOL";
                Statement stmtCAcheClear = dbc.createStatement();
                stmtCAcheClear.executeUpdate(sClearCache);
                dbc.commit();
                stmtCAcheClear.close();
                api.Log.Log("completed cache flush");

                String sSQLnew = "select distinct DOCUMENT.DOCUMENTID, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, DOCUMENT.DOCURL, DOCUMENT.DOCUMENTSUMMARY, DOCUMENT.DATELASTFOUND, DOCUMENT.VISIBLE, DOCUMENT.SHORT, DOCUMENT.AUTHOR, DOCUMENT.SOURCE, DOCUMENT.CITATION, DOCUMENT.SORTDATE, DOCUMENT.SHOWDATE, DOCUMENT.SUBMITTEDBY, DOCUMENT.REVIEWDATE, DOCUMENT.REVIEWEDBY, DOCUMENT.REVIEWSTATUS, DOCUMENT.COMMENTS, DOCUMENT.UPDATEDDATE, DOCUMENT.UPDATEDBY, DOCUMENT.FULLTEXT, contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') from DOCUMENT, DOCUMENTSECURITY where DOCUMENT.DOCUMENTID = DOCUMENTSECURITY.DOCUMENTID and DOCUMENTSECURITY.SECURITYID in( 0 ) and ( contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') > 0) order by 22";
                long lTime2 = testQuerySpeed(sSQLnew, 150, dbc, "new way");

                stmtCAcheClear = dbc.createStatement();
                stmtCAcheClear.executeUpdate(sClearCache);
                dbc.commit();
                stmtCAcheClear.close();
                api.Log.Log("completed cache flush");

                String sSQLOld = "select distinct * from ( select DOCUMENT.DOCUMENTID, DOCUMENT.GENREID, DOCUMENT.DOCTITLE, DOCUMENT.DOCURL, DOCUMENT.DOCUMENTSUMMARY, DOCUMENT.DATELASTFOUND, DOCUMENT.VISIBLE, DOCUMENT.SHORT, DOCUMENT.AUTHOR, DOCUMENT.SOURCE, DOCUMENT.CITATION, DOCUMENT.SORTDATE, DOCUMENT.SHOWDATE, DOCUMENT.SUBMITTEDBY, DOCUMENT.REVIEWDATE, DOCUMENT.REVIEWEDBY, DOCUMENT.REVIEWSTATUS, DOCUMENT.COMMENTS, DOCUMENT.UPDATEDDATE, DOCUMENT.UPDATEDBY, DOCUMENT.FULLTEXT, contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') from DOCUMENT, DOCUMENTSECURITY where DOCUMENT.DOCUMENTID = DOCUMENTSECURITY.DOCUMENTID and DOCUMENTSECURITY.SECURITYID in( 0 ) and ( contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)') > 0)  order by contains (DOCUMENT.FULLTEXT, '(Canada or Russia) and (henry and mike)')    ) ";
                long lTime1 = testQuerySpeed(sSQLOld, 150, dbc, "old way");
                api.Log.Log("query time test result : lTime1 [" + lTime1 + " lTime2 [" + lTime2 + "]");
                //testTSCLassify(dbc);
                System.exit(0);
            }

            if (false)// 2003 10 11
            {
                testDocOpen();
                System.exit(0);
            }

            /*
             * if ( false ) { // TSapplyROC 2003 09 05 dbc = localInitSession ( "API" ); Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") +
             * Session.cfg.getProp("StopListFile")); testApplyROC(dbc); System.exit(0);
            }
             */



            if (false) // hbk control 2002 12 25 remove for prod.
            {
                memNewIntOrStringSameOnesTest();
            }
            if (false) // hbk control 2002 12 25 remove for prod.
            {
                memIArrTest();
            }
            if (false) // hbk control 2002 12 25 remove for prod.
            {
                memRandomStringsArrTest();
            }
            if (false) // corpus file copies with word scramblig
            {
                scrambleCorpusWordsAndNodeIDs();
            }
            if (false) {
                testDataStructLoader(dbc);
            }
            if (false) {
                classifySpeedLoopTestCompareModes(dbc);
            }
            // 2003 03 26
            if (false) // classify one or a few 2002 11 17  // fn: batch false
            {
                testTSThesaurusSynch(dbc);
            }
            if (false) // classify one or a few 2002 11 17  // fn: batch false
            {
                testTSRefreshNode2(dbc); // 2003 01 25
            }            // 2003 03 22
            if (false) {  // 2003 07 28 // explain ?
                dbc = localInitSession("API");
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
                testExplain(dbc);
            }
            if (false) // uses post execute to tsclassify
            {
                testExplainMorePureAsAPICall(dbc);
            }
            if (false) {
                dbc = localInitSession("API");
                com.indraweb.util.Log.log("engine test logger");
                testLogger();
                System.exit(1);
            }


            //System.exit (0);

        } catch (Throwable t) {
            api.Log.Log("maintest throwable [" + t.getMessage() + "]");
            t.printStackTrace();
            api.Log.Log("maintest throwable [" + t.getMessage() + "]");
        }
        try {

            /*
             * if ( false ) // ftp test 2002 11 02 // fn: { try { com.indraweb.classification.BatchClassFileMgr.testFtpConnection2 ("shell.theworld.com", "hkon2", "xxxx"); // prep.ai.mit.edu } catch (
             * Exception e ) { com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e); } System.exit(1); }
             */

            // SET UP ENV SINCE NOT COMING THRU JRUN

//			com.indraweb.util.Log.logClear("testtest\r\n");
            //com.indraweb.util.UtilFile.addLineToFile("c:/temp/temp.txt", new java.util.Date()	+ ": done db init  \r\n");
            // create corpus
            //executeAPI (
            //	"http://207.103.213.118:8100/servlet/ts?fn=tscorpus.TSCreateCorpus&"	+
            //	"CorpusName=(henry and mike)%20Kon%20Test%20Corpus%20Name&CorpusDesc=&CorpusPath=pathunknown&RocF1=1&RocF2=2&RocF3=3&RocC1=1&RocC2=2&RocC3=3");
            // edit	corpus
            //executeAPI ("http://207.103.213.118:8100/servlet/ts?fn=tsdocument.TSGetNodeDocument&CorpusID=1&NodeID=90411");
            //				"http://207.103.213.118:8100/servlet/ts?fn=tscorpus.TSEditCorpusProps&CorpusID=3&" +
            //				"CorpusName=UpdatedCorpName&CorpusPath=updatedCorpusPath&RocF1=10");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&CorpusID=2");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSGetCorpusProps&CorpusID=1");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&UserID=");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/water.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/worldbook/a1_html/000200.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/worldbook/a1_html/000200.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.gulflink.osd.mil/cement_factory_ii/cement_factory_ii_s03.htm#C.%20Cement%20Factory%20Location");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.gulflink.osd.mil/cement_factory_ii/cement_factory_ii_s03.htm#C.%20Cement%20Factory%20Location");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.gulflink.osd.mil/cement_factory_ii/cement_factory_ii_s03.htm#C.%20Cement%20Factory%20Location");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/iraqoil.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&URL=/corpussource/classificationtest/DepletedUranium.html");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&URL=/classifyfiles/westNileVirus.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&URL=/classifyfiles/westNileVirus.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");

            //			executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&url=/corpussource/classificationtest/iraqoil.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&post=true&url=/corpussource/classificationtest/iraqoil.html");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/test2.html&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsnode.TSNodeTitleSearch&Keywords=TIME");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://www.zyvex.com/nano/");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsnode.TSAddNode&NodeID=200021&ParentID=90192&CorpusID=1&NodeTitle=hkon test node&WordsAndFreqs=word1:1;word2:2;word3:3&NodeSize=40&NodeIndexInParent=20");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsnode.TSAddNode&ParentID=90192&CorpusID=1&NodeTitle=hkon test node&WordsAndFreqs=word1:1;word2:2;word3:3&NodeSize=40&NodeIndexInParent=20");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/iraqoil.html&userID=hkontestpageb&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSListCorpora");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&maxnumoutput=20&post=false&url=http://www.rpi.org/news/press_1998_0721b.htm");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=http://www.lassp.cornell.edu/GraduateAdmissions/greene/greene.html&maxnumoutput=5&post=false");


            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/3799.html&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/3799.html&post=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/3799.html&post=true");


            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage");

            // with	name from ARES
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=\\\\66.134.131.38\\public\\corpussource&CorpusName=HKTEST&CorpusActive=1");
            // without name
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=C:\\Documents	and	Settings\\Indraweb\\IndraHome\\corpussource\\merckManualCrawl\\corpusRoot\\001");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome/corpussource/merckManualCrawl/corpusRoot/001/001&CorpusName=HKTEST");

            //			executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusID=1000094&CorpusFolder=C:/Documents and Settings\\Indraweb/IndraHome/corpussource/merckManualCrawl/corpusRoot/001");

            //executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome/corpussource/TechTarget/001&CorpusName=Tech Target&CorpusActive=-1&whichdb=SVR&publisherid=5");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome/corpussource/TechTarget/001&CorpusName=Tech Target&CorpusActive=-1");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage");
            // 9728.html executeAPI	("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://66.134.131.61:8100/patents/9728.html");  // hbk 2002
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://www.zyvex.com/nano/");  // hbk 2002
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&url=http://www.worldviewsoftware.com/ea.html");
            // executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/water.html&userID=hkontestpage");
            //executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusActive=1&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome\\corpussource\\Child Psychology\\Child Psychology\\001");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hbkTest&ExplainScores=true&ExplainScoresShowDocWords=true&NodeToScoreExplain=95634");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=95634&explainscoresshowdocwords=false");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false");
            //	executeAPI ("http://localhost:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings/Indraweb/IndraHome\\corpussource/merckManualCrawl/corpusRoot/001/001&corpusname=HKTEST5&CorpusActive=-1");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=2");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=false&numscorestoexplain=5");


            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn="+
            //			"tsclassify.TSClassifyDoc&URL=http://66.134.131.38/hkon/DepletedUranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1&explainscoresshowdocwords=true&numscorestoexplain=1");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/depleteduranium.html&userID=hkontestpage&explainscores=true&nodetoscoreexplain=-1");

            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium.html");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/for(henry and mike).htm");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/for(henry and mike).htm&explainscores=true");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/for(henry and mike).htm&explainscores=true&nodetoscoreexplain=1129429");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/hoeytest.html&explainscores=true&explainscoresshowdocwords=true&numscorestoexplain=5");
            // hk smallest corpus
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome_default\\corpussource\\chem smallest hk\\001&CorpusName=chem war small	hbk	test&CorpusActive=1", "C:/Documents	and	Settings/IndraWeb/IndraHome_default");

            // tsinstall corpus	merck
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:/Documents and Settings/Indraweb/IndraHome_default/corpussource/merckManualCrawl/corpusRoot/001&CorpusName=merck	hbk	test&CorpusActive=1&skey=1", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // 2002	05 16
            // tsinstall corpus	test warfare
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome_default\\corpussource\\chemical warfare HKTEST\\001&CorpusName=chem war hbk 7&CorpusActive=-1&skey=1",	"C:/Documents and Settings/IndraWeb/IndraHome_default");

            // 2002	05 16
            // tsclassify uranium, post	= false
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium.html&post=true&sorpora=1&SKEY=-220364972",	"C:/Documents and Settings/IndraWeb/IndraHome_default");

            // tsclassify uranium, post	= true,	summary	and	title
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium2.html&post=true&SKEY=-220364972&doctitle=test hbk test	doc	title&docsummary=test hbk test doc summary", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // tsclassify uranium, post	= true,	no summary and title
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium2.html&post=true&SKEY=-220364972&corpora=1", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // classify	uranium, post =	false
            // D:\IndraWeb\SourceSafe_IndrawebRoot\Java2\TS_API_project
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/DepletedUranium.html&post=true&SKEY=-151158519", "C:/Documents	and	Settings/IndraWeb/IndraHome_default");
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tsclassify.TSClassifyDoc&URL=/corpussource/classificationtest/t.html&post=false&SKEY=1892478463", "C:/Documents and Settings/IndraWeb/IndraHome_default");

            // 2002	06 26
            // tsinstall corpus	software_engineering2
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\indrahome\\corpussource\\software_engineering_3\\001&CorpusName=test	hkon&CorpusActive=-1&skey=1", dbc );


            // 2002	07 04
            // tsinstall corpus	test warfare
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome\\corpussource\\chemical warfare HKTEST\\001&CorpusName=test hkon&CorpusActive=-1&skey=1", dbc );

            // 2002	07 22
            // tsinstall corpus	SWE	test
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\Documents and Settings\\Indraweb\\IndraHome\\corpussource\\SWESave\\001\\001&CorpusName=test hkon&CorpusActive=-1&skey=1",	dbc	);

            // 2002	08 09
            // tsinstall corpus	BIO	test
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSInstallCorpus&CorpusFolder=C:\\temp\\temp\\biologicalandchemical\\001\\002\\002&CorpusName=test hkon&CorpusActive=-1&skey=1",	dbc	);



            // 2002	09 18
            /*
             * String s = "file:///C:/Documents and Settings/Indraweb/IndraHome/corpussource/classificationtest/depleteduranium.html"; //UrlHtmlAttributes	uh = new UrlHtmlAttributes (
             * "file:///C:/t.html"); UrlHtmlAttributes uh = new UrlHtmlAttributes ( s);
             *
             * long ltgetATopicFromMinInputs_start	= System.currentTimeMillis();
             *
             * Topic t	= new Topic	// inside	getATopicFromMinInputs	2 ( uh, true, dbc, //	fix	this 20000, null, false, false, Session.cfg.getPropInt ("socketTimeoutMSWebDocs"), true, true );
             * Enumeration	ewords = t.htWordsDocOnly_noDescendants.keys(); while (	ewords.hasMoreElements() ) { String sKey	= (String) ewords.nextElement(); //com.indraweb.util.Log.log	("word [" +
             * sKey + "] count	[" + t.htWordsDocOnly_noDescendants.get	( sKey ) + "]\r\n")	; }
             *
             * System.exit(1);
             */

            // 2002 11 11 test file put for FTP BATCH CLASSIFY
            if (false) // batch classify ftp test
            {
                /*
                 * for ( int i = 0; i < 6; i++ ) { api.tsclassify.BatchClassifyTempFileMgr.addOneDocClassResultToTempFile (8, i + ". batch class test string"); }
                 */
            }

            if (false) //  tsqa.TSQATrec2  v2
            {
                /*
                 * String sURLTrecQA2	= "http://localhost:8105/servlet/ts?fn=tsqa.TSQATrec2&skey=1" + "&NumTopRecs=30" + "&qid=403"+ "" ;
                 *
                 * executeAPI (sURLTrecQA2, dbc ); System.exit(0);
                 */
            }

            if (false) //  tsqa.TSQATrec v.1
            {
                /*
                 * String sURLTrecQA2	= "http://localhost:8105/servlet/ts?fn=tsqa.TSQATrec" + "&skey=1" + //"&outputfile=/temp/temp/temp/test/QaRpt.txt&qid=403,406"; "&outputfile=/temp/hk/QaRpt33.txt"
                 * + "&qid=403"+ //"&REPORTONLY=RECALL" + "&NumTopRecs=10" + //"&REPORTONLY=BOTH"; "" ;
                 *
                 * String sURLTrecQA	= "http://localhost:8105/servlet/ts?fn=tsqa.TSQATrec&outputfile=/temp/hk/QaRpt35.txt&qid=406&skey=19&NumTopRecs=10";
                 *
                 *
                 * executeAPI (sURLTrecQA, dbc );
                 */
            }


            if (false) // classify a few 2002 11 02  // fn: batch true
            {
                /*
                 * try {
                 *
                 * // 2002	07 22 // tsclassify uranium, post	= true //String sURLClassify =
                 * "http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";
                 *
                 * com.indraweb.execution.Session.cfg.setProp("outputToScreen", "true"); String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" + "fn=tsclassify.TSClassifyDoc" +
                 * //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+ //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
                 * //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" + //"&URL=/corpussource/classificationtest/hkmp4.html" +
                 * //"&URL=/corpussource/classificationtest/DepletedUranium.html" + //"&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" + "&REPOSITORYID=11"+
                 * //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" + "&SKEY=-220364972" + "&batch=true" + "&post=true" + //"&URL=/corpussource/classificationtest/FT911-1.HTM" +
                 * //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" + "&URL=/corpussource/classificationtest/FT911-133.HTM" + "&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" +
                 * //"&ExplainScores=true"	+ //"&NodeToScoreExplain=16881" + "" ;
                 *
                 * //com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false"); Data_WordsToNodes.verifyCacheUpToDate(dbc, false); executeAPI (sURLClassify, dbc );
                 * //Data_WordsToNodes.initForceDataStructReload (dbc, true); //executeAPI (sURLClassify, dbc );
                 *
                 * }
                 * catch ( Exception e ) { com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e); }
                 */
            }


            // TEST	FOLDER
            /*
             * String[] sArrFileNames = com.indraweb.util.UtilFileEnumerator.listFilesInFolderNonRecursive_returnFQName ("C:/Documents and
             * Settings/IndraWeb/IndraHome/corpussource/classificationtest/testcase"); testbed	(dbc,	sArrFileNames );
             */


            // 2002	07 08
            // tsrebucket corpus
            //executeAPI ("http://66.134.131.40:8100/servlet/ts?fn=tscorpus.TSRebucket&CorpusID=1&skey=1", dbc );

            // CQL QUERY

            com.indraweb.util.Log.logClear("DONE MAINTEST DONE\r\n");
            System.exit(0);


        } catch (Throwable e) {
            System.out.println("error in main.java:" + e.getMessage());
            com.indraweb.util.Log.FatalError("error in main.java", e);
        }
    }

    private static void testLogger() {
        Logger logger = Logger.getLogger("log");
        String sFileLogCfg = "C:/Program Files/ITS/ITSLogConfig.txt";
        PropertyConfigurator.configure(sFileLogCfg);

        int ix = 0;
        while (true) {
            logger.info(ix + ". in 1 loop");
            try {
                throw new Exception("hk except");
            } catch (Exception e) {
                logger.fatal(ix + ". in loop", e);
            }
            com.indraweb.util.clsUtils.pause_this_many_milliseconds(14000);
            ix++;
            if (ix == 1000) {
                break;
            }
            ;
        }
        logger.info("Exiting application.");
    }

    public static void executeAPI(String surlAPI, Connection dbc)
            throws Throwable {
        executeAPI(surlAPI, dbc, true);
    }

    public static void executeAPI(String surlAPI, Connection dbc, boolean bUseSystemOutIfTrue)
            throws Throwable {

        api.Log.Log("\r\n\r\n\r\n\r\n" + surlAPI);
        String sParmAndValuesOnly = com.indraweb.util.UtilStrings.getAllAfterLastOfThis(surlAPI, "?");

        //split	all	input args into	a vector , so CorpusID=3&PublisherID=1&CorpusShortName=HKTestCorpusShortName
        // gets	split into a vector	with elements :	CorpusID=3,	PublisherID=1, ...
        api.Log.Log("sParmAndValuesOnly [" + sParmAndValuesOnly + "]");
        Vector vParmEqValues = com.indraweb.util.UtilStrings.splitByStrLen1(sParmAndValuesOnly, "&");
        api.APIProps props = new api.APIProps();
        Enumeration e = vParmEqValues.elements();
        while (e.hasMoreElements()) {
            String sParmValue = (String) e.nextElement();  // get e.g.,	"CorpusID=3"
            api.Log.Log("processing parm [" + sParmValue + "]");
            Vector vParmAndValue = com.indraweb.util.UtilStrings.splitByStrLen1(sParmValue, "=");
            if (vParmAndValue.size() != 2) // split into	a vec of  "CorpusID" as	element	0 and "3" as element 1
            {
                throw new Exception("invalid input	parm [" + sParmValue + "]\r\n");
            }

            String sParm = (String) vParmAndValue.elementAt(0);
            String sValue = (String) vParmAndValue.elementAt(1);
            //System.out.println("program adding param [" + sParm + "] value [" + sValue + "]" );
            props.put(sParm, sValue);
        }

        //        PrintStream nps = new PrintStream(new FileOutputStream("NUL:"));
        //        System.setErr(nps);
        //        System.setOut(nps);

        PrintWriter out = null;
        if (bUseSystemOutIfTrue) {
            out = new PrintWriter(System.out);
        } else {
            out = new PrintWriter(System.err);
        }




//        if (bUseSystemOutIfTrue)
//            out = new PrintWriter (System.out);
//        else
//            out = new PrintWriter (System.err);

        // Log.log ("log test");
        // out.println("output test");
        //java.net.URL url = new java.net.URL("http://www.gulflink.osd.mil/declassdocs/cia/19970825/970613_010191_dec_txt_0001.html");
        ////InputStream	is = url.openStream();
        //byte[] barr =	new	byte[5000];
        //is.read(barr);

        try {
            api.APIHandler.doGetMine(null, null, props, out, dbc, true);
        } finally {
            out.flush();
        }
        //out.close();
    }

    // as cut and pasted from ts.java for local	use
    public static Connection initializeDB(String sWhichDB) {
        try {
            Connection dbc = api.statics.DBConnectionJX.getConnection(sWhichDB, "Maintest.initializeDB");
            Hashtable htargs = new Hashtable();

            String sSQL = "	select ParamName, ParamValue from ConfigParams ";
            if (UtilFile.bFileExists("/temp/IndraDebug_UseConfigparams_test.txt")) {
                System.out.println("file present, using db table ConfigParams_test [/temp/IndraDebug_UseConfigparams_test.txt]");
                sSQL = " select ParamName, ParamValue from ConfigParams_test order by ParamName";
            }
            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);

            while (rs.next()) {
                String sK = rs.getString(1);
                String sV = rs.getString(2);

                if (sV == null) {
                    sV = "";
                }
                htargs.put(sK, sV);
            }

            Session.cfg.addToProperties(htargs);
            rs.close();
            stmt.close();
            return dbc;
        } catch (Exception e) {
            Log.LogError("error	getting	DB connection internal stack trace", e);
            return null;
        }
    }

    private static void testbed(Connection dbc, String[] sArrFileNames)
            throws Throwable {

        if (false) { // hbk control test bed
            sArrFileNames = new String[1];
            sArrFileNames[0] = "C:/Documents and Settings/IndraWeb/IndraHome/corpussource/classificationtest/hkmp4.html";
        }

        // *********************************************
        // TEST	BED
        // *********************************************
        String sArrTests_icfg_classification_DocCountMin[] = {"1", "2"};
        String sArrTests_icfg_classification_SigCountMin[] = {"2"};

        int iTestNum = 0;
        for (int i = 0; i < sArrTests_icfg_classification_DocCountMin.length; i++) {
            for (int j = 0; j < sArrTests_icfg_classification_SigCountMin.length; j++) {
                com.indraweb.util.Log.logClearcr("************************************************");
                com.indraweb.util.Log.logClearcr("*********************	START TEST " + (iTestNum++));
                com.indraweb.util.Log.logClearcr("************************************************");
                // now clear that stuff	out
                // delete and verify
                String SQLcountpre = "select count (*) from	document where docurl like '%classificationtest%'";
                long lCntpre = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBLong(SQLcountpre, dbc);
                String SQL1 = "delete from document	where docurl like '%classificationtest%'";
                com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(SQL1, dbc, true, -1);
                String SQLcountverify = "select	count (*) from document	where docurl like '%classificationtest%'";
                long lCntVerify = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBLong(SQLcountverify, dbc);
                com.indraweb.util.Log.logClearcr("DB doc cnt docs like '%classificationtest%' test from	" + lCntpre + "	to " + lCntVerify);

                if (lCntVerify > 0) {
                    throw new Exception("lCntVerify >	0 ");
                }

                Session.cfg.setProp("cfg_classification_DocCountMin",
                        sArrTests_icfg_classification_DocCountMin[i]);
                Session.cfg.setProp("cfg_classification_SigCountMin",
                        sArrTests_icfg_classification_SigCountMin[j]);

                //com.indraweb.util.Log.logClearcr("SIG	WORDS PRE INIT docMin [" +
                //sArrTests_icfg_classification_DocCountMin[i]	+ "] sigmin	[" +
                //sArrTests_icfg_classification_SigCountMin[j]	+ "] " +
                //" ["	+ Data_WordsToNodes.getWordsList()+	"]");
                com.iw.classification.Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));

                //com.indraweb.util.Log.logClearcr("SIG	WORDS POST INIT	[" +
                //sArrTests_icfg_classification_DocCountMin[i]	+ "] sigmin	[" +
                //sArrTests_icfg_classification_SigCountMin[j]	+ "] " +
                //" ["	+ Data_WordsToNodes.getWordsList()+	"]");



                // ***********************************
                // RUN TESTS IN	HERE
                long lStartAPI = System.currentTimeMillis(); //	start file loop
                for (int k = 0; k < sArrFileNames.length; k++) {

                    if (sArrFileNames[k].indexOf("/classificationtest") < 0) {
                        throw new Exception("premised on files	being in or	under 'C:/Documents	and	Settings/IndraWeb/IndraHome/corpussource/classificationtest'");
                    }
                    String sFileTail = "/corpussource/" + UtilStrings.getAllAfterLastOfThis(sArrFileNames[k], "/classificationtest");
                    String sURLClassify = "http://66.134.131.40:8100/servlet/ts?"
                            + "fn=tsclassify.TSClassifyDoc"
                            + "&URL=" + sFileTail
                            + //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                            //"&ExplainScores=true"	+
                            //"&NodeToScoreExplain=2521" +
                            "&dburl=hkon/" + sFileTail
                            + "&SKEY=-220364972"
                            + "&post=true";

                    //com.indraweb.util.Log.logClear ( "sURLclassify:" + sURLClassify +	"\r\n");
                    executeAPI(sURLClassify, dbc);
                }
                com.indraweb.util.Log.logcr("time	(ms) in	executeAPI : " + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStartAPI));
                // RUN TESTS IN	HERE
                // ***********************************
                String SQL = null;
                com.indraweb.util.Log.logClearcr("getting	DB results ...");
                SQL = "select score1, count	(*)	from nodedocument nd, document d where nd.documentid = d.documentid	"
                        + " and d.docurl like	'%classificationtest%' group by	score1 ";

                //com.indraweb.util.Log.logClearcr("starting sql to	get	nodedoc	count for score	= "	+ iArrScoreValues[k] + ")");
                Vector vDBResults = com.indraweb.database.JDBCIndra_Connection.executeDBQueryVecOfVecsReturned(SQL, dbc, -1);
                com.indraweb.util.Log.logClearcr("# docs scored \t" + sArrFileNames.length);
                com.indraweb.util.Log.logClearcr("DocMin\t" + sArrTests_icfg_classification_DocCountMin[i]
                        + "\tSigMin\t" + sArrTests_icfg_classification_SigCountMin[j]);
                if (vDBResults != null && vDBResults.size() > 0) {
                    Vector v1I_Score = (Vector) vDBResults.elementAt(0);
                    Vector v2I_Count = (Vector) vDBResults.elementAt(1);
                    for (int k = 0; k < v2I_Count.size(); k++) {
                        com.indraweb.util.Log.logClearcr("Score\t" + v1I_Score.elementAt(k)
                                + "\tCount\t" + v2I_Count.elementAt(k));

                    }
                } else {
                    com.indraweb.util.Log.logClearcr("db final : no doc NODEDOCUMENT records for these docs");
                }

            }
            //com.indraweb.util.Log.logClear ( "****** START CLASSIFY [" + iz +	" ]************\r\n");
            // doc parm	= 1, sig parm =	1

            //	if (iz == 0	)
            //	{
            //			com.indraweb.util.Log.logClear ( "****** done first	classify - keyread ************* r\n");
            //			com.indraweb.util.UtilKeyReads.read();
            //	}


        }


    }

    static private void memIArrTest() {

        System.gc();
        long lmempre = Runtime.getRuntime().freeMemory();
        String s = "lmempre [" + lmempre + "]";
        log1(s);
        runScopable_memIArrTest();
        clsUtils.waitForKeyboardInput("at hk wait 6");
        clsUtils.waitForKeyboardInput("at hk wait 7");
    }

    static private void runScopable_memIArrTest() {

        System.gc();
        long lmempre = Runtime.getRuntime().freeMemory();
        // make an array of int's to represent word relations ...
        int i_xDimLen = 1000;
        int i_yDimLen = 1000;

        int[][] iArrTest = new int[i_xDimLen][i_yDimLen];

        for (int i = 0; i < i_xDimLen; i++) {
            for (int j = 0; j < i_yDimLen; j++) {
                iArrTest[i][j] = (i * j);
            }
        }

        log1("num arr cells - written [" + (i_xDimLen * i_yDimLen) + "]");
        System.gc();
        long lmempost = Runtime.getRuntime().freeMemory();
        String s = new java.util.Date()
                + " num arr cells - written [" + (i_xDimLen * i_yDimLen) + "] "
                + " mem pre [" + (lmempre) + "]"
                + " mem post [" + (lmempost) + "]"
                + " mem diff [" + (lmempre - lmempost) + "]";
        log1(s);
    }

    private static void memRandomStringsArrTest() throws Exception {
        System.gc();
        long lmempre = Runtime.getRuntime().freeMemory();

        int iNumWords = 1000000;
        int iMinLen = 6;
        int iEveryXReset = 1000000;

        log1("\r\n ********** START " + new java.util.Date()
                + "\r\niNumWords  [" + (iNumWords) + "]"
                + " iMinLen  [" + (iMinLen) + "]"
                + " iEveryXReset  [" + (iEveryXReset) + "]"
                + " lmempre  [" + (lmempre) + "]");

        String[] sArr = new String[iNumWords];

        if (iNumWords % iEveryXReset != 0) {
            throw new Exception("iNumWords % iNumUnique != 0");
        }

        long lmempre_pregc = Runtime.getRuntime().freeMemory();
        System.gc();
        long lmempre_postgc = Runtime.getRuntime().freeMemory();
        Random rand = null;
        for (int kWIdx = 0; kWIdx < iNumWords; kWIdx++) {
            if (kWIdx % 10000 == 0) {
                /*
                 * System.gc(); long lmemmid= Runtime.getRuntime().freeMemory();
                 */
                String s = "kWIdx [" + kWIdx + "] " //                            + " lmemMid [" + lmemmid + "] \r\n"
                        ;
                log1(s);

            }

            if (kWIdx % iEveryXReset == 0) {
                rand = new Random((long) 1000);
                //System.out.println("new rand at kWIdx [" + kWIdx  + "] ");
            }
            int iLen = com.indraweb.util.UtilRandom.getRandomInt_Min0_MaxSizeMinus1(rand, 15);

            if (iLen < iMinLen) {
                iLen = iMinLen;
            }

            char[] carr = new char[iLen];

            for (int j = 0; j < iLen; j++) {
                int ichar = 48 + com.indraweb.util.UtilRandom.getRandomInt_Min0_MaxSizeMinus1(rand, 42);
                char c = (char) ichar;
                carr[j] = c;
            }

            String s2 = new String(carr);
            /*
             * htAll.put ( new Integer ( kWIdx ), s ); htUnique.put ( s, s );
             */
            sArr[kWIdx] = s2;
            if (kWIdx % 10000 == 0) {
                log1(kWIdx + ". " + s2);
            }

            //System.out.println("new rand at kWIdx [" + kWIdx  + "] s [" + s + "]");
        }

        System.gc();
        long lmempost_postgc = Runtime.getRuntime().freeMemory();

        /*
         * clsUtils.waitForKeyboardInput("at testwait 0 " + "\r\nhtunique.size() [" + htUnique.size() + "] " + "\r\nhtAll.size() [" + htAll.size() + "] " + "\r\nlmempre_pregc [" + lmempre_pregc + "] "
         * + "\r\nlmempre_postgc[" + lmempre_postgc+ "] " + "\r\nlmempost_pregc [" + lmempost_pregc + "] " + "\r\nlmempost_postgc [" + lmempost_postgc + "] " + "\r\ngap [" + ( lmempost_postgc -
         * lmempre_postgc ) + "] " );
         */
        /*
         * htUnique = null; htAll = null;
         */
        System.gc();
        long lmempost = Runtime.getRuntime().freeMemory();

        String s =
                " lmempre [" + (lmempre) + "]"
                + " lmempost  [" + (lmempost) + "]"
                + " lmempre - lmempost [" + (lmempre - lmempost) + "]";
        log1(s);

        clsUtils.waitForKeyboardInput("hk wait 10 ");
        clsUtils.waitForKeyboardInput("hk wait 10 ");
        System.exit(1);

    }

    private static void log1(String s) {
        System.out.println(s);
        UtilFile.addLineToFile("/tmp/t.t", s + "\r\n");
    }

    private static void memNewIntOrStringSameOnesTest() {

        if (false) // INT test
        {
            System.gc();
            long lmempre = Runtime.getRuntime().freeMemory();
            String s =
                    " lmempre [" + (lmempre) + "]";
            log1(s);

            int iArrSize = 1000000;
            Integer[] iarr = new Integer[iArrSize];
            for (int i = 1; i < iArrSize; i++) {
                if (i % 10000 == 0) {
                    System.out.println(" i = " + i);
                }

                iarr[i] = new Integer(1);
            }
            System.gc();
            long lmempost = Runtime.getRuntime().freeMemory();
            String s2 =
                    " lmempost[" + (lmempost) + "]";
            log1(s2);
            clsUtils.waitForKeyboardInput("at hkwait 15 "
                    + "\r\ndiff [" + (lmempre - lmempost) + "] ");
        }
        if (true) // String test
        {
            System.gc();
            long lmempre = Runtime.getRuntime().freeMemory();
            String s =
                    "String lmempre [" + (lmempre) + "]";
            log1(s);

            int iArrSize = 1000000;
            String[] sarr = new String[iArrSize];
            for (int i = 1; i < iArrSize; i++) {
                if (i % 10000 == 0) {
                    System.out.println(" i = " + i);
                }

                sarr[i] = new String("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
            }
            System.gc();
            long lmempost = Runtime.getRuntime().freeMemory();
            String s2 =
                    "String lmempost[" + (lmempost) + "]";
            log1(s2);
            clsUtils.waitForKeyboardInput("at hkwait 15 "
                    + "\r\ndiff [" + (lmempre - lmempost) + "] ");
        }

    }

    private static void testDataStructLoader(Connection dbc) throws Throwable {
        try {

            UtilFile.addLineToFileKill("c:/t.t", "AT TOP OF testDataStructLoader \r\n");
            //com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            long lStartTime = System.currentTimeMillis();
            System.gc();
            long lmempreDSbuild = Runtime.getRuntime().freeMemory();

            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));

            System.gc();
            long lmempostDSbuild = Runtime.getRuntime().freeMemory();
            // int iCountWordsDistinct = Data_WordsToNodes.htWordsToVecOfNodeCounts.size();
            // int iCountWordsTotal = Data_WordsToNodes.htIntToSAllWordsRedundant.size();
            int iCountNodes = Data_NodeIDsTo_NodeForScore.getMainHT_htNodeIDToNodeForScore().size();

            // ?print word info at end of test

            // print word info at end of test
            int iCountNumNFSsPrinted = 0;
            Hashtable htNodeIDToNodeForScore = Data_NodeIDsTo_NodeForScore.getMainHT_htNodeIDToNodeForScore();
            Enumeration eNFSs = htNodeIDToNodeForScore.elements();
            while (eNFSs.hasMoreElements()) {
                iCountNumNFSsPrinted++;
                if (iCountNumNFSsPrinted > 2) {
                    break;
                }
                NodeForScore nfs = (NodeForScore) eNFSs.nextElement();
                String[] sarr = nfs.getSarr(false, true, true);
                int[] iarr = nfs.getiarr();
                long lNodeid = nfs.getNodeID();
                if (sarr != null) {
                    for (int i = 0; i < sarr.length; i++) {
                        log1(i + ". nodeid:" + lNodeid + " word [" + sarr[i] + "] cnt [" + iarr[i] + "]");
                    }
                } else {
                    System.out.println("scarr == null");
                }

            }
            System.out.println("completed Data_WordsToNodes.verifyCacheUpToDate ");
//                    System.out.println("iCountWordsDistinct  [" + iCountWordsDistinct + "]");
            //System.out.println("iCountWordsTotal [" + iCountWordsTotal + "]");
            System.out.println("iCountNodes [" + iCountNodes + "]");
            System.out.println("iNumtimesNCConstructorCalled [" + NodeCount.iNumNCConstructors + "]");
            System.out.println("iNumtimesNFSConstructorCalled [" + NodeForScore.iNumNFSConstruct + "]");
            System.out.println("iNumtimesSCConstructorCalled [" + StringAndCount.iNumtimesSCConstructorCalled + "]");

            String sOldWayFile = Session.sIndraHome + "/IndraControl_LoadMemOldWay.txt";
            if (UtilFile.bFileExists(sOldWayFile)) {
                System.out.println("running the old way");
            } else {
                System.out.println("running the new way");
            }

            System.out.println("total time ms [" + (System.currentTimeMillis() - lStartTime) + "]");
            System.out.println(""
                    + "\r\nlmempostDSbuild  [" + lmempostDSbuild + "] "
                    + "\r\ndiff [" + (lmempreDSbuild - lmempostDSbuild) + "] ");
            //Data_WordsToNodes.printDataStructContents_dumpAllNodes(false);
            // Data_WordsToNodes.printCacheContents();
            clsUtils.waitForKeyboardInput("at hkwait 2 ");
            clsUtils.waitForKeyboardInput("at hkwait 3 ");
            // Data_WordsToNodes.initForceDataStructReload  (dbc, false);

            // executeAPI (sURLClassify,  dbc );
//                    executeAPI (sURLClassify,  dbc );
//                    executeAPI (sURLClassify,  dbc );

            com.indraweb.util.Log.log("MAINTEST DONE1 \r\n");
            System.exit(0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testDataStructLoader( Connection dbc)

    private static void testTSCLassify(Connection dbc) throws Throwable {
        try {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

            /*
             * String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" + "fn=tsclassify.TSClassifyDoc" + //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
             * //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" + //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
             * //"&URL=/corpussource/classificationtest/hkmp4.html" + "&URL=/corpussource/classificationtest/DepletedUranium.html" + "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html"
             * + "&REPOSITORYID=11"+ //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" + "&SKEY=-220364972" + "&batch=false" + "&post=true" + //"&post=false" +
             * //"&URL=/corpussource/classificationtest/FT911-1.HTM" + //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" + //"&URL=/corpussource/classificationtest/FT911-133.HTM" + //
             * "&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" + //"&ExplainScores=true"	+ //"&NodeToScoreExplain=16881" + // "&nodeset=421445,421446" //"&nodeset=2521,2523" + "" ;
             */

            //String sFile = "00002821.htm.html";   // 0 bytes
            //String sFile = "82345233.html";   // 0 bytes
            // hbkhbkhbk
            // String sFile = "plat092898.htm";
            //String sFile = "plat092898_preTagRmoved.htm";
            //String sFile = "plat092898_preTagRmoved_mpfilter.html";
            //String sFile = "schiz4.htm";
            //String sFile = "plat092898.htm";



            //String sFile = "schizophrenia.htm";
            //String sFile = "43kim0.woti-doc.htm";
            //String sFile = "temp.html";
            //String sFile = "DepletedUranium.html";
            //String sFile = "background node 207970 corpus 16.html";
            //String sFile = "TreatingSchizophrenia.html";
            //String sFile = "Corpus 16 Node 209038 DOD-FDA discussions.html";
            //String sFile = "Corpus 16 Node 209038 and corpus 17 node 250004.html";
            //String sFile = "node 1386237 ability.html";
            //String sFile = "60867527.htm";
            //String sFile = "temp.txt";
            //String sFile = "347.html";
            String sFile = "Corpus16Node207974_GulfWarIllnesses.html";

            //     DepletedUranium.html" +

            String sURL = "/corpussource/classificationtest/" + sFile;
            api.Log.Log(" Classification sURL [" + sURL + "]");
            String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?"
                    + "fn=tsclassify.TSClassifyDoc"
                    + "&URL=" + sURL
                    + "&dburl=dburl/" + sURL
                    + "&REPOSITORYID=11"
                    + "&SKEY=-220364972"
                    + "&batch=false"
                    + //"&post=true" +
                    "&post=true"
                    + "&docsumm=true"
                    + //"&docsummary=passed4 in docsummary test" +
                    //"&doctitle=passe  d in doctitle test" +
                    //2003 09 22 "&nodetoscoreexplain=209038" +
                    // "&explainscores=true" +
                    "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));
            com.indraweb.util.Log.log("START CLASSIFY 1\r\n");
            executeAPI(sURLClassify, dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            clsUtils.waitForKeyboardInput("at hkwait 4 ");
            com.indraweb.util.Log.log("MAINTEST DONE1 \r\n");
            System.exit(0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void testTSCLassify2(Connection dbc) throws Throwable {
        try {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

            /*
             * String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" + "fn=tsclassify.TSClassifyDoc" + //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
             * //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" + //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
             * //"&URL=/corpussource/classificationtest/hkmp4.html" + "&URL=/corpussource/classificationtest/DepletedUranium.html" + "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html"
             * + "&REPOSITORYID=11"+ //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" + "&SKEY=-220364972" + "&batch=false" + "&post=true" + //"&post=false" +
             * //"&URL=/corpussource/classificationtest/FT911-1.HTM" + //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" + //"&URL=/corpussource/classificationtest/FT911-133.HTM" + //
             * "&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" + //"&ExplainScores=true"	+ //"&NodeToScoreExplain=16881" + // "&nodeset=421445,421446" //"&nodeset=2521,2523" + "" ;
             */

            //String sFile = "00002821.htm.html";   // 0 bytes
            //String sFile = "82345233.html";   // 0 bytes
            // hbkhbkhbk
            // String sFile = "plat092898.htm";
            //String sFile = "plat092898_preTagRmoved.htm";
            //String sFile = "plat092898_preTagRmoved_mpfilter.html";
            //String sFile = "schiz4.htm";
            //String sFile = "plat092898.htm";



            //String sFile = "schizophrenia.htm";
            //String sFile = "43kim0.woti-doc.htm";
            //String sFile = "temp.html";
            //String sFile = "DepletedUranium.html";
            //String sFile = "background node 207970 corpus 16.html";
            //String sFile = "TreatingSchizophrenia.html";
            //String sFile = "Corpus 16 Node 209038 DOD-FDA discussions.html";
            //String sFile = "Corpus 16 Node 209038 and corpus 17 node 250004.html";
            //String sFile = "node 1386237 ability.html";
            //String sFile = "60867527.htm";
            //String sFile = "temp.txt";
            //String sFile = "347.html";
            String sFile = "Corpus16Node207974_GulfWarIllnesses.html";

            //     DepletedUranium.html" +

            String sURL = "/corpussource/classificationtest/" + sFile;
            api.Log.Log(" Classification sURL [" + sURL + "]");
            String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?"
                    + "fn=tsclassify.TSClassifyDoc"
                    + "&dburl=dburl/corpussource/classificationtest/Corpus16Node207974_GulfWarIllnesses.html"
                    + "&URL=/corpussource/classificationtest/Corpus16Node207974_GulfWarIllnesses.html"
                    + //"&docidsource=604046" +
                    "&skey=484464469"
                    + "&explainscores=false"
                    + "&POST=false"
                    + "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));
            com.indraweb.util.Log.log("START CLASSIFY 1\r\n");
            executeAPI(sURLClassify, dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            clsUtils.waitForKeyboardInput("at hkwait 5 ");
            com.indraweb.util.Log.log("MAINTEST DONE1 \r\n");
            System.exit(0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void testTSCLassifyGUIStyle(String sDocIDSource, String sCorpora,
            String sCallerNodeSetOrNull, Connection dbc,
            int iDweight, int iSweight,
            String sNodeToScoreExplainOrNull_) throws Throwable {
        try {
            /*
             * IndraXMLargs 0. skey:90400519 IndraXMLargs 1. docidsource:37283 IndraXMLargs 2. explainscores:false IndraXMLargs 3. post:false IndraXMLargs 4. fn:tsclassify.TSClassifyDoc
             *
             */
            boolean bDynamicNode = false;

            String sNodeSet = "";
            if (sCallerNodeSetOrNull != null) {
                sNodeSet = "&NodeSet=" + sCallerNodeSetOrNull;
            }

            String sParmNodeToScoreExplainOrNull = "";
            String sParmBooleanExplain = "";
            if (sNodeToScoreExplainOrNull_ != null) {
                sParmNodeToScoreExplainOrNull = "&NodeToScoreExplain=" + sNodeToScoreExplainOrNull_;
                sParmBooleanExplain = "&explainscores=" + true;
            }

            String sParm_Corpora = "&Corpora=" + sCorpora;

            String sParm_Sigs = "";
            if (bDynamicNode) {
                api.Log.Log("using dynamic classify");
                api.Log.Log("using dynamic classify");
                api.Log.Log("using dynamic classify");
                api.Log.Log("using dynamic classify");
                api.Log.Log("using dynamic classify");
                api.Log.Log("using dynamic classify");
                api.Log.Log("using dynamic classify");
                String[] sArrWords = {"edward", "edward paul albert", "paul"};
                int[] iArrCounts = new int[sArrWords.length];
                iArrCounts[0] = 3;
                iArrCounts[1] = 5;
                iArrCounts[2] = 7;
                Vector[] vArrUnder = new Vector[sArrWords.length];
                vArrUnder[0] = new Vector();
                vArrUnder[0].addElement("thes11");
                vArrUnder[0].addElement("thes12");
                vArrUnder[2] = new Vector();
                vArrUnder[2].addElement("thes31");

                /*
                 * Vector vInts = new Vector(); vInts.addElement(new Integer (3)); vInts.addElement(new Integer (5)); vInts.addElement(new Integer (7));
                 */
                //String sTermsWithThes = TSClassifyDoc..getvUnder_ArrThesTerms(
                String sTermsWithThes = NodeForScore.getStrCompressedTermsWithThes(
                        sArrWords,
                        iArrCounts,
                        vArrUnder);


                /*
                 * test it via node for score ) getvUnder_ArrThesTerms( "Edward Paul Albert", 100011, 100, dbc, Vector vStrPerNodeDBSigWords_, Vector vIntPerNodeDBSigCounts_, boolean bThesExpand, //10
                 * want thes expansion in general (legacy) boolean bStemmedAlso, // want stemmed also boolean bNodeTitleAlreadyInList sArrWords, iArrCounts)
                 */
                api.Log.Log("sTermsWithThes  [" + sTermsWithThes + "]");
                sParm_Sigs =
                        //"&DynamicNodeSigs=3,term1|||5,term2||thesterm21||thesterm22|||7,term3||thesterm31" +
                        //"&DynamicNodeSigs=" + sTermsWithThes +
                        "&DynamicNodeSigs=33,gerald|||33,ford|||33,president|||13,house|||10,gerald r ford|||10,president ford|||9,michigan|||9,policy|||9,rapids|||8,congress"
                        + "&DCNodeTitle=Jimmy Carter"
                        + "&DCNumTitleWords=1"
                        + "&DCNodeSize=51"
                        + "&DCBuildThesaurus=false";
            }


            String sURLClassify = "http://207.103.213.104:8080/ itsharvester/ts?"
                    + "SKEY=-220364972"
                    + "&fn=tsclassify.TSClassifyDoc"
                    + sNodeSet
                    + //"&NodeSet="+"1407969,1407970" + // testTSCLassifyGUIExplainStyle
                    "&DWeight=" + iDweight
                    + "&SWeight=" + iSweight
                    + "&DocIDSource=" + sDocIDSource + // testTSCLassifyGUIExplainStyle
                    "&DocTitle=conference  objectives    " + // testTSCLassifyGUIExplainStyle
                    sParm_Corpora
                    + sParmBooleanExplain
                    + sParmNodeToScoreExplainOrNull
                    + "&post=false"
                    + sParm_Sigs
                    + "";

            //Data_WordsToNodes.refreshCache (dbc , "5" , false);
            api.Log.Log("START testTSCLassifyGUIStyle \r\n");
            executeAPI(sURLClassify, dbc);
            api.Log.Log("DONE testTSCLassifyGUIStyle \r\n");

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void testTSCLassifyGUIExplainStyle(
            String sDocIDSource,
            String sNodeIDToScoreExplain,
            String sCorpora,
            Connection dbc) throws Throwable {
        try {
            /*
             * IndraXMLargs 0. skey:-1964151412 IndraXMLargs 1. docidsource:37292 IndraXMLargs 2. explainscores:true IndraXMLargs 3. batch:false IndraXMLargs 4. post:false IndraXMLargs 5.
             * fn:tsclassify.TSClassifyDoc IndraXMLargs 6. nodetoscoreexplain:14350
             *
             */
            String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?"
                    + "fn=tsclassify.TSClassifyDoc"
                    + //"&DocIDSource=41293" +
                    //"&DocIDSource=702471" +
                    //"&DocIDSource=41293" + // testTSCLassifyGUIExplainStyle
                    "&DocIDSource=" + sDocIDSource + // testTSCLassifyGUIExplainStyle
                    "&post=true"
                    + "&SKEY=-220364972"
                    + "&explainscores=true"
                    + //"&nodetoscoreexplain=95634" +
                    //"&nodetoscoreexplain=1402331" +
                    "&nodetoscoreexplain=" + sNodeIDToScoreExplain
                    + "&corpora=" + sCorpora
                    + //"&docsummary=passed4 in docsummary test" +
                    //"&doctitle=passe  d in doctitle test" +
                    //2003 09 22 "&nodetoscoreexplain=209038" +
                    // "&explainscores=true" +
                    "";

            //tsclassify.TSClassifyDoc&documen`tid=41293&post=false&SKEY=478186727

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            //Data_WordsToNodes.refreshCache (dbc , "5" , false);
            api.Log.Log("START testTSCLassifyGUIExplainStyle 1\r\n");
            executeAPI(sURLClassify, dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            //clsUtils.waitForKeyboardInput ("at hkwait 6 ");
            api.Log.Log("DONE testTSCLassifyGUIExplainStyle 1\r\n");

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void testTSGetNodeSigs(
            String sNodeID,
            Connection dbc,
            PrintWriter out) throws Throwable {


        int i = 0;
        if (true && i == 0) {
            String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?"
                    + "fn=tsnode.TSGetNodeSigs"
                    + //"&NodeID="+1401714+
                    "&NodeID=" + 11325754
                    + "&SKEY=-220364972"
                    + "";
            executeAPI(sURLClassify, dbc);
            return;

        }


    }

    private static void testTSThesaurusExpand(
            Connection dbc,
            PrintWriter out) throws Throwable {


        String[] sArrWords = {"edward", "edward paul albert", "paul"};
        int[] iArrCounts = new int[sArrWords.length];
        iArrCounts[0] = 3;
        iArrCounts[1] = 5;
        iArrCounts[2] = 7;
        Vector[] vArrUnder = new Vector[sArrWords.length];
        vArrUnder[0] = new Vector();
        vArrUnder[0].addElement("thes11");
        vArrUnder[0].addElement("thes12");
        vArrUnder[2] = new Vector();
        vArrUnder[2].addElement("thes31");

        /*
         * Vector vInts = new Vector(); vInts.addElement(new Integer (3)); vInts.addElement(new Integer (5)); vInts.addElement(new Integer (7));
         */
        //String sTermsWithThes = TSClassifyDoc..getvUnder_ArrThesTerms(
        String sTermsWithThes = NodeForScore.getStrCompressedTermsWithThes(
                sArrWords,
                iArrCounts,
                vArrUnder);

        String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?"
                + "fn=tsthesaurus.TSThesaurusExpand"
                + "&CorpusID=" + 100011
                + "&nodetitle=" + "paul"
                + "&Sigs=" + sTermsWithThes
                + "&SKEY=-220364972"
                + "";
        executeAPI(sURLClassify, dbc);
        return;

    }

    private static void testTSThesaurusSynch(Connection dbc) throws Throwable {
        try {
            String sURLClassify =
                    "http://localhost:8080/servlet/ts?fn=tsthesaurus.thesaurusoracle.TSThesaurusSynch&skey=23434"
                    + "&DBName=client"
                    + "&UserDB=sbooks"
                    + "&PassDB=racer9";

            executeAPI(sURLClassify, dbc);
            clsUtils.waitForKeyboardInput("at hkwait 7 ");
            com.indraweb.util.Log.log("MAINTEST DONE1 \r\n");
            System.exit(0);
        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void testExplainMorePureAsAPICall(Connection dbc) throws Throwable {
        try {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

            /*
             * String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" + "fn=tsclassify.TSClassifyDoc" + //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
             * //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" + //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
             * //"&URL=/corpussource/classificationtest/hkmp4.html" + "&URL=/corpussource/classificationtest/DepletedUranium.html" + "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html"
             * + "&REPOSITORYID=11"+ //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" + "&SKEY=-220364972" + "&batch=false" + "&post=true" + //"&post=false" +
             * //"&URL=/corpussource/classificationtest/FT911-1.HTM" + //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" + //"&URL=/corpussource/classificationtest/FT911-133.HTM" + //
             * "&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" + //"&ExplainScores=true"	+ //"&NodeToScoreExplain=16881" + // "&nodeset=421445,421446" //"&nodeset=2521,2523" + "" ;
             */
            String sURLClassify = "http://66.134.131.40:8100/servlet/ts?"
                    + "fn=tsclassify.TSClassifyDoc"
                    + "&URL=/corpussource/classificationtest/DepletedUranium.html"
                    + "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html"
                    + "&REPOSITORYID=11"
                    + "&SKEY=-220364972"
                    + "&batch=false"
                    + "&post=false"
                    + "&nodetoscoreexplain=208995"
                    + "&explainscores=true"
                    + "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));
            executeAPI(sURLClassify, dbc);
            clsUtils.waitForKeyboardInput("at hkwait 8 ");
            com.indraweb.util.Log.log("MAINTEST DONE1 \r\n");
            System.exit(0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void testTSRefreshNode2(Connection dbc) throws Throwable {
        try {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

            com.indraweb.execution.Session.cfg.setProp("outputToScreen", "true");
            String sURLNodeRefresh = "http://localhost:8080/servlet/ts?"
                    + "fn=tsrefreshnode.TSMetasearchNode"
                    + "&REPOSITORYID=11"
                    + //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                    "&SKEY=-220364972"
                    + //"&batch=false" +
                    "&post=true"
                    + //"&URL=/corpussource/classificationtest/FT911-1.HTM" +
                    //"&dburl=dburl/corpussource/classificationtest/FT911-1.HTM" +
                    //"&URL=/corpussource/classificationtest/FT911-133.HTM" +
                    //						"&dburl=dburl/corpussource/classificationtest/FT911-133.HTM" +
                    //"&ExplainScores=true"	+
                    //"&NodeToScoreExplain=16881" +
                    "&nodeid=2521"
                    + "&whichdb=SVR"
                    + //"&nodeid=2521"   +
                    "";

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));
            executeAPI(sURLNodeRefresh, dbc);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSRefreshNode2

    private static Connection localInitSession(String sWhichDB) {
        return localInitSession(sWhichDB, true);
    }

    private static Connection localInitSession(String sWhichDB, boolean bInitializeDB) {
        if (!com.indraweb.execution.Session.GetbInitedSession()) {
//				com.indraweb.util.UtilFile.addLineToFile("/temp/temp.txt", new java.util.Date() +	": doing MainTest init session  \r\n");
            Hashtable htprops = new Hashtable();
            // Load	environment	from JRUN config application variables first
            String sOSHost = (String) java.lang.System.getProperties().get("os.name");
            if (sOSHost.toLowerCase().indexOf("windows") >= 0) {
                Session.sIndraHome = "C:/Program Files/ITS";
            } else {
                Session.sIndraHome = "/tmp/IndraHome";
            }

            Session.sLogNameDetail = Session.sIndraHome + "/logs/LogDetail.txt";

            htprops.put("ImportDir", "C:/TEMP");
            htprops.put("AuthenticationSystem", "ActiveDirectory");
            htprops.put("MachineName", "indraweb-e5j05c");
            htprops.put("API", "http://localhost:80/itsapi/ts?fn=");
            htprops.put("DBString_API_User", "sbooks");
            htprops.put("DBString_SVR_User", "sbooks");
            htprops.put("DBString_API_Pass", "racer9");
            htprops.put("DBString_SVR_Pass", "indra9");

            System.out.println("using [" + sDB + "]");
            System.out.println("using [" + sDB + "]");
            System.out.println("using [" + sDB + "]");
            System.out.println("using [" + sDB + "]");
            htprops.put("DBString_API_OracleJDBC", sDB);
            System.out.println("DBString_API_OracleJDBC:" + sDB);
            //htprops.put	( "DBString_API_OracleJDBC", "jdbc:oracle:thin:@207.103.213.109:1521:client");
            //htprops.put	( "DBString_SVR_OracleJDBC", "jdbc:oracle:thin:@207.103.213.109:1521:gaea");
            htprops.put("DBString_SVR_OracleJDBC", "jdbc:oracle:thin:@207.103.213.109:1521:gaea");
            htprops.put("IndraHome", Session.sIndraHome);
            htprops.put("IndraFolderHome", Session.sIndraHome);		//}

            htprops.put("BatchClassFTPHost", "shell.theworld.com");		//}
            htprops.put("BatchClassFTPUser", "hkon2");		//}
            htprops.put("BatchClassFTPPass", "xxxx");		//}
            htprops.put("BatchClassFTPRemoteDir", "/usr/tmp");		//}
            htprops.put("outputToFile", "true");
            htprops.put("outputToScreen", "true");

            Session.cfg = new com.indraweb.execution.ConfigProperties(htprops);
            Session.SetbInitedSession(true);
            Session.cfg.setProp("outputToScreen", "true");

            /*
             * System.out.println( " API DB [" + htprops.get ( "DBString_API_OracleJDBC") + "]" + " SVR DB [" + htprops.get ( "DBString_SVR_OracleJDBC") + "]" );
             */

        }

        //com.indraweb.util.UtilFile.addLineToFile("c:/temp/temp.txt", new java.util.Date()	+ ": done chech	or init	session	 \r\n");
        Connection dbc = null;
        if (bInitializeDB) {
            dbc = initializeDB(sWhichDB);
        }
        return dbc;


    }

    private static void scrambleCorpusWordsAndNodeIDs() {
        try {
            for (int i = 20; i < 20; i++) {
                SignatureFile.corpusFileMangler(
                        "C:/Documents and Settings/IndraWeb/IndraHome/corpussource/SigWordCountsFile_300012.txt",
                        "C:/Documents and Settings/IndraWeb/IndraHome/corpussource/SigWordCountsFile_300012" + i + ".txt",
                        i);
                System.out.println("completed i = " + i + "["
                        + "SigWordCountsFile_300012" + i + ".txt" + "]");

            }
        } catch (Throwable t) {
            Log.LogFatal("error 123", t);
            System.out.println("error 123");
            System.exit(1);
        }

        System.out.println("completed ok");
        System.exit(1);
    }

    private static void classifySpeedLoopTestCompareModes(Connection dbc) throws Throwable {
        try {

            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";


            String sURLClassify = "http://66.134.131.40:8100/servlet/ts?"
                    + "fn=tsclassify.TSClassifyDoc"
                    + //"&URL=/corpussource/chemical warfare HKTEST/001/001/001/001/000.html"	+
                    //"&URL=/corpussource/SWEng_noTextAgg_NoFont/001/001/003/000.html" +
                    //"&URL=/corpussource/classificationtest/950925_024pgv_00p.html" +
                    //"&URL=/corpussource/classificationtest/hkmp4.html" +
                    "&URL=/corpussource/classificationtest/DepletedUranium.html"
                    + "&REPOSITORYID=11"
                    + //"&URL=/corpussource/classificationtest/SWE_1_1_2_2_lcase.html" +
                    //"&ExplainScores=true"	+
                    //"&NodeToScoreExplain=2521" +
                    "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html"
                    + "&SKEY=-220364972"
                    + "&batch=true"
                    + "&post=true";

            String sURLClassify_nodocsumm = sURLClassify + "&docsumm=false";

            long lStart1 = System.currentTimeMillis();
            executeAPI(sURLClassify_nodocsumm, dbc);
            long lEnd1 = System.currentTimeMillis();
            com.indraweb.util.Log.logClear("strat 3 : "
                    + (lEnd1 - lStart1 + "\r\n"));
            System.exit(0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    }

    private static void testTSMetasearchNode(Connection dbc) throws Throwable {
        try {
            // 2002	07 22
            // tsclassify uranium, post	= true
            //String sURLClassify =	"http://localhost:8101/servlet/ts?fn=tsclassify.TSClassifyDoc&post=false&URL=http://www.lungusa.org/diseases/lungemphysem.html&SKey=12345678";

            //int iNodeID = ThreadProducer_MSGetNextNode.getNextNode("main test getting node", dbc);
            int iNodeID = 1323509;   //hbk control 203 03 04 which single node local debug a
            com.indraweb.execution.Session.cfg.setProp("outputToScreen", "true");
            com.indraweb.execution.Session.cfg.setProp("cfg_classification_CommaDelimCorporaToUse",
                    "100003");
            com.indraweb.execution.Session.cfg.setProp("proxyhostname", "none");
            com.indraweb.execution.Session.cfg.setProp("proxyhostport", "none");
            com.indraweb.execution.Session.cfg.setProp("useproxyWithHttp", "false");
            com.indraweb.execution.Session.cfg.setProp("useproxyWithSockets", "false");

            String sURLNodeRefresh =
                    "http://localhost:8080/servlet/ts?fn=tsmetasearchnode.TSMetasearchNode"
                    + "&post=true"
                    + "&nodeid=" + iNodeID
                    + "&whichdb=SVR"
                    + "&machthreadinfo=maintestdebug"
                    + "&NUMTHREAD=1";    // hbk numthreads hbk

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));
            executeAPI(sURLNodeRefresh, dbc);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSRefreshNode2

    private static void testExplain(Connection dbc) throws Throwable {
        try {
            /*
             * String sURLClassify	= "http://66.134.131.40:8100/servlet/ts?" + "fn=tsclassify.TSClassifyDoc" + "&URL=/corpussource/classificationtest/DepletedUranium.html" +
             * "&dburl=dburl/corpussource/classificationtest/DepletedUranium.html" + "&REPOSITORYID=11"+ "&SKEY=-220364972" + "&batch=false" + "&post=true" + "" ;
             */

            //String sFileName = "schiz4.htm";   //1386876
            //int iNodeIdToExplain = 1386876;   //

            //String sFileName = "DepletedUranium.html"; // 1386240
            String sFileName = "Corpus11_Node95634_gulfTopic.html"; // 1386240
            int iNodeIdToExplain = 95634;   //
            String sNodeID = "" + iNodeIdToExplain;
            HashTree htArguments = new HashTree();
            htArguments.put("fn", "tsclassify.TSClassifyDoc");
            htArguments.put("URL", "/corpussource/classificationtest/" + sFileName);
            htArguments.put("dburl", "/corpussource/classificationtest/" + sFileName);
            htArguments.put("REPOSITORYID", "11");
            htArguments.put("SKEY", "-220364972");
            htArguments.put("batch", "false");
            htArguments.put("post", "false");
            htArguments.put("nodetoscoreexplain", sNodeID);
            htArguments.put("explainscores", "true");


            if (true) // switch which mode - post or not
            {
                Enumeration et = htArguments.keys();

                // Build URL to call Taxonomy Server
                String sURLExplain = "http://localhost:80/itsapi/ts?";
                int i = 0;
                while (et.hasMoreElements()) {
                    String sKey = (String) et.nextElement();
                    String sValue = (String) htArguments.get(sKey);

                    if (i == 0) {
                        sURLExplain = sURLExplain + sKey + "=" + sValue;
                    } else {
                        sURLExplain = sURLExplain + "&" + sKey + "=" + sValue;
                    }

                    // Remove any apostraphies (because they are illegal) and change
                    // spaces into + signs
                    sURLExplain = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURLExplain, " ", "+"));
                    sURLExplain = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURLExplain, "'", ""));
                    i++;
                }
                System.out.println("SURL explain [" + sURLExplain + "]");

                executeAPI(sURLExplain, dbc);
            } else {


                PrintWriter out = new PrintWriter(System.out);
                Vector vResults = PostExecute(out, "C:/temp/temp/temp/plat092898.htm",
                        "tsclassify.TSClassifyDoc",
                        htArguments, "http://localhost:80/itsapi/ts?fn=");


                if (vResults.size() < 4) {
                    throw new Exception("Sorry, no classification results found "
                            + "for the topic identifier that was specified. ");
                }
                VectorTree vExplain = (VectorTree) vResults.elementAt(2);

                HTMLDocument Document = new HTMLDocument();

                String sNodeTitle = (String) vExplain.elementAt(1);
                if (sNodeTitle.length() > 50) {
                    sNodeTitle = sNodeTitle.substring(0, 47) + "...";
                }

                Document.AddVariable("NODEID", (String) vExplain.elementAt(0));
                Document.AddVariable("NODETITLE", sNodeTitle);
                Document.AddVariable("DOCURL", (String) vExplain.elementAt(2));
                Document.AddVariable("DOCTITLE", (String) vExplain.elementAt(3));

                // retrieve document frequency and coverage
                Vector vClassResults = (VectorTree) vResults.elementAt(4);
                Enumeration eC = vClassResults.elements();
                while (eC.hasMoreElements()) {
                    try {
                        Vector v = (Vector) eC.nextElement();

                        if (sNodeID.equals((String) v.elementAt(1))) {
                            Document.AddVariable("SC1", (String) v.elementAt(3));
                            //Document.AddVariable("SFQ", (String) v.elementAt(8));
                        }
                    } catch (Exception e) {
                    }
                }

                Document.AddVariable("SFQ", "scaled");


                // get frq data
                String sData = (String) vExplain.elementAt((vExplain.size() - 5));
                out.println("<!-- frq: " + sData + " elementAt: " + (vExplain.size() - 4) + "-->");
                //System.out.println("<!-- cov: "+sData+" elementAt: "+(vExplain.size()-4)+"-->");
                int iStart = sData.lastIndexOf("[");
                int iEnd = sData.lastIndexOf("]");
                Document.AddVariable("FRQ", sData.substring(iStart + 1, iEnd));

                // get cov data
                sData = (String) vExplain.elementAt((vExplain.size() - 4));
                out.println("<!-- cov: " + sData + " -->");
                iStart = sData.lastIndexOf("[");
                iEnd = sData.lastIndexOf("]");
                Document.AddVariable("COV", sData.substring(iStart + 1, iEnd));

                Document.AddVariable("Title", "Explain a Document");
                Document.WriteTemplate(out, "header-admin.tpl");
                Document.WriteTemplate(out, "explain/explain-head.tpl");

                // Loop through each explanation .. but filter out things that aren't explanations
                for (int loop = 6; loop < vExplain.size() - 6; loop++) {
                    sData = (String) vExplain.elementAt(loop);

                    // split into readable results..
                    //0. wd- [summary] d1cnt [8] d2cnt [0] incr [0.0]
                    iStart = sData.indexOf("[");
                    iEnd = sData.indexOf("]");
                    Document.AddVariable("SIGNATURE",
                            sData.substring(iStart + 1, iEnd));

                    sData = sData.substring(iEnd + 1);
                    iStart = sData.indexOf("[");
                    iEnd = sData.indexOf("]");
                    Document.AddVariable("SOURCE",
                            sData.substring(iStart + 1, iEnd));

                    sData = sData.substring(iEnd + 1);
                    iStart = sData.indexOf("[");
                    iEnd = sData.indexOf("]");
                    Document.AddVariable("DOC",
                            sData.substring(iStart + 1, iEnd));

                    Document.WriteTemplate(out, "explain/explain-result.tpl");
                }

            } // if as to which mode to run
        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    public static Vector PostExecute(PrintWriter out, String filename,
            String sAPIcall, HashTree htArguments, String sTaxonomyServer) {
        try {
            VectorTree vOriginal = new VectorTree();
            vOriginal.SetParent(null);
            VectorTree vObject = vOriginal;

            // Do a multi-part post
            String boundary = "ThisBoundaryStringCanBeAnythingWhichDoesNotAppearInTheMessage";
            String twoHyphens = "--";
            String lineEnd = "\r\n";

            HttpURLConnection httpURLConn = null;
            DataOutputStream outStream;
            DataInputStream inStream;
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            // create FileInputStream to read from file
            //String sFn = com.indraweb.execution.Session.cfg.getProp("ImportDir")+filename;
            String sFn = filename;
            FileInputStream fileInputStream = new FileInputStream(new File(sFn));


            String sURL = sTaxonomyServer + sAPIcall;
            Enumeration et = htArguments.keys();

            // Build URL to call Taxonomy Server
            while (et.hasMoreElements()) {
                String sKey = (String) et.nextElement();
                String sValue = (String) htArguments.get(sKey);

                sURL = sURL + "&" + sKey + "=" + sValue;

                // Remove any apostraphies (because they are illegal) and change
                // spaces into + signs
                sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL, " ", "+"));
                sURL = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sURL, "'", ""));
            }

            URL theURL = new URL(sURL);
            httpURLConn = (HttpURLConnection) theURL.openConnection();

            httpURLConn.setRequestMethod("POST");
            httpURLConn.setRequestProperty("Connection", "Keep-Alive");
            httpURLConn.setDoOutput(true);
            httpURLConn.setUseCaches(false);
            httpURLConn.setRequestProperty("Accept-Charset", "iso-8859-1,*,utf-8");
            httpURLConn.setRequestProperty("Accept-Language", "en");
            httpURLConn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);

            // Set up POST parameters
            StringBuffer params = new StringBuffer();
            params.setLength(0);
            Enumeration e = htArguments.keys();
            params.append("fn=" + sAPIcall);

            // open output stream to server, POST data and multipart form up to the file data
            outStream = new DataOutputStream(httpURLConn.getOutputStream());

            outStream.writeBytes(twoHyphens + boundary + lineEnd);
            outStream.writeBytes("Content-Disposition: form-data; name=\"import\";" + " filename=\"" + filename + "\"" + lineEnd);
            outStream.writeBytes(lineEnd);

            // create a buffer of maximum size
            // (not really necessary, but might speed up things a bit)
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            //int tot=bufferSize; //debug

            // read file data and write it into form
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);//might change in the last loop
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necessary after file data
            outStream.writeBytes(lineEnd);
            outStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();
            outStream.flush();
            outStream.close();

            // display server response data on console.
            InputStream is = httpURLConn.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                //System.out.println("sData [" + sData+ "]" );

                int iTag1Start = sData.indexOf("<");
                int iTag1End = sData.indexOf(">");
                int iTag2Start = sData.lastIndexOf("</");
                int iTag2End = sData.lastIndexOf(">");

                String sTag1;

                try {
                    sTag1 = sData.substring(iTag1Start + 1, iTag1End);
                } catch (Exception eTag) {
                    sTag1 = "";
                }

                String sTag2 = "";

                if (iTag1End != iTag2End) {
                    sTag2 = sData.substring(iTag2Start + 2, iTag2End);
                }

                // This is how we detect an END tag
                if ((!sTag1.equals("")) && (sTag1.charAt(0) == '/')) {
                    sTag2 = new String(sTag1.substring(1));
                    sTag1 = new String("");
                }

                Integer iVariance = new Integer(0);

                // HACK! HACK! Ignore lines that start with..
                if ((!sTag1.equals("?xml version=\"1.0\" encoding=\"iso-8859-1\" ?"))
                        && (!sTag1.equals("TSRESULT")) && (!sTag2.equals("TSRESULT"))
                        && (!sTag1.equals("APIGETS")) && (!sTag1.equals("CALLCOUNT"))
                        && (!sTag1.equals("CLASSLOAD")) && (!sTag1.equals("TIMEOFCALL_MS"))) {

                    // If s1Tag is populated but s2Tag is not, start the object
                    if ((!sTag1.equals("")) && (sTag2.equals(""))) {

                        if (sTag1.equals("TS_ERROR")) {
                            httpURLConn.disconnect();
                            return vOriginal;
                        }
                        if (sTag1.equals("INTERNALSTACKTRACE")) {
                            httpURLConn.disconnect();
                            return vOriginal;
                        }
                        VectorTree vOldObject = vObject;
                        vObject = new VectorTree();
                        vObject.SetParent(vOldObject);
                        vOldObject.addElement(vObject);

                    }

                    // if s2Tag is populated but s1Tag is not, end the object
                    if ((!sTag2.equals("")) && (sTag1.equals(""))) {
                        // out.println("<BR>End object: "+sTag2);
                        vObject = (VectorTree) vObject.GetParent();
                    }

                    // if both s1Tag and s2Tag are populated, store the field into object started
                    if ((!sTag2.equals("")) && (!sTag1.equals(""))) {
                        String sTag = new String(com.indraweb.util.UtilStrings.getStringBetweenThisAndThat(sData, "<" + sTag1 + ">", "</" + sTag2 + ">"));
                        //out.println("<!-- Add tag: "+sTag1+" value: "+sTag+" -->");
                        // out.println("<BR>Add tag: "+sTag1+" value: "+sTag);

                        // If the variable value contains a "CDATA", eliminate it here
                        if (com.indraweb.util.UtilStrings.getStrContains(sTag, "<![CDATA")) {
                            sTag = new String(sTag.substring(9, (sTag.length() - 3)));
                        }

                        vObject.addElement(sTag);
                    }
                }
            }

            return vObject;
        } catch (Exception except) {
            except.printStackTrace();
            HTML.HTMLDocument Document = new HTML.HTMLDocument();
            Logging.Log.LogError(except, out);
            Document.WriteError(out, except.toString());
            return null;
        }

    } // postExecute

    /*
     * IndraXMLargs 0. url:file:///c:\Documents and Settings\Indraweb\IndraHome/classifyfiles/DepletedUranium.html IndraXMLargs 1. postedfilename:c:\Documents and
     * Settings\Indraweb\IndraHome/classifyfiles/DepletedUranium.html IndraXMLargs 2. showscores:true IndraXMLargs 3. explainscores:true IndraXMLargs 4. nodetoscoreexplain:208995 IndraXMLargs 5.
     * fn:tsclassify.TSClassifyDoc IndraXMLargs 6. skey:1591484286 IndraXMLargs 7. post:false IndraXMLargs 8. content-type:text/plain IndraXMLargs 9. import:DepletedUranium.html
     */
    private static void loadWebCrawlCache(Connection dbc) {
        try {
            String sDirFile = "C:/home/admin/fcrawl/temp16.txt";
            System.out.println("STATE 6");
            Enumeration e = com.indraweb.util.UtilFile.enumFileLines(sDirFile);
            System.out.println("STATE 7");
            int iLineNum1Based = 0;
            int iMaxUrlSize = -1;
            HashSet hsSetURLsDoneAlready = new HashSet();
            int iNumDups = 0;
            while (e.hasMoreElements()) {
                iLineNum1Based++;
                String sLine = (String) e.nextElement();
                Vector v = com.indraweb.util.UtilStrings.splitByStrLen1(sLine, "\t");

                if (v.size() != 2) {
                    throw new Exception("Line " + iLineNum1Based + " size <>2 ");
                }
                String sDocURL = (String) v.elementAt(0);
                if (hsSetURLsDoneAlready.contains(sDocURL.toLowerCase())) {
                    iNumDups++;
                    System.out.print("found (case ins.) dup #" + iNumDups + "line " + iLineNum1Based + " url [" + sDocURL.toLowerCase() + "]");
                    continue;
                }
                hsSetURLsDoneAlready.add(sDocURL.toLowerCase()); //save doc for dup check
                if (sDocURL.length() > iMaxUrlSize) {
                    iMaxUrlSize = sDocURL.length();
                }
                System.out.println("STATE 8.3");
                String sDocFile = (String) v.elementAt(1);

                String sSQL = "insert into WebCrawlCache "
                        + " (REPOSITORYID ,DOCURL , DOCCACHEFILE , STATUS , DATELASTTOUCHED ) "
                        + " values (15, '" + sDocURL + "', '" + sDocFile + "', 1, sysdate )";

                System.out.print("SQL line " + iLineNum1Based + ":" + sSQL);
                com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQL, dbc, false, false, -1);
                if (iLineNum1Based % 100 == 0) {
                    dbc.commit();
                }
                System.out.println("line " + iLineNum1Based + " inserted iMaxUrlSize " + iMaxUrlSize);
            } // while
            System.out.println("STATE 9");
            dbc.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // to add third col and keep file location and file cache info separate
    private static void xpatchWebCrawlCache(Connection dbc) throws Exception {
        String sSQL = "select DOCCACHEFILE from webcrawlcache";
        Statement stmt = dbc.createStatement();
        ResultSet rslt = stmt.executeQuery(sSQL);

        while (rslt.next()) {
            String sDOCCACHEFILE = rslt.getString(1);
            System.out.println("sDOCCACHEFILE [" + sDOCCACHEFILE + "]");
        }

        rslt.close();
        stmt.close();
    }

    private static void testTSGetROCData(Connection dbc) throws Throwable {
        try {
            String sURLgetroc = "http://207.103.213.104:8080/itsharvester/ts?"
                    + "fn=tsclassify.TSGetROCdata"
                    + "&corpusid=100040"
                    + "&DirectoryPath=c:/temp/"
                    + "&SKEY=-220364972"
                    + "";
            executeAPI(sURLgetroc, dbc);
            com.indraweb.util.Log.log("MAINTEST DONE1 \r\n");
            System.exit(0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }


    }

    public static void testDocOpen() throws Throwable {
        String sURL = "file:///C:/Program Files/ITS/CorpusSource/classificationtest/347.html";
        Connection dbc = localInitSession("API");
        //testTSCLassify(dbc);
        UrlHtmlAttributes uh = new UrlHtmlAttributes(sURL);

        IndraURL iURL = new IndraURL(sURL, null, null, null, 1, 0);
        long ltgetATopicFromMinInputs_start = System.currentTimeMillis();

//        Topic t = new Topic// inside	getATopicFromMinInputs	 2
//                (
//                        uh ,
//                        true ,
//                        dbc , //	fix	this
//                        20000 ,
//                        iURL ,
//                        false ,
//                        // phrase scoring false ,
//                        Session.cfg.getPropInt ("socketTimeoutMSWebDocs") ,
//                        true ,
//                        true ,
//                        true ,
//                        true    ,
//                        1 ,
//                        null ,
//                        null,
//                        false,
//                        true
//                );

        //Enumeration ewords = t.htWordsDocOnly_noDescendants.keys ();
//        while (ewords.hasMoreElements ())
//        {
//            String sKey = (String) ewords.nextElement ();
//            com.indraweb.util.Log.log ("doc has word [" + sKey + "] count	[" + t.htWordsDocOnly_noDescendants.get (sKey) + "]\r\n");
//        }

        System.exit(1);

    }
    private static int iCallCount_testQuerySpeed = 1;

    public static long testQuerySpeed(String sSQL, int iNumRecsToPull, Connection dbc, String sComment) throws Throwable {
        long lTimeStart = System.currentTimeMillis();
        api.Log.Log(new java.util.Date() + "start SQL test [" + sComment + "] call number [" + iCallCount_testQuerySpeed + "]");
        long lStart = System.currentTimeMillis();
        Statement stmt = null;
        ;
        ResultSet rs = null;
        int iLoop = 0;
        String DBResult;
        long lStartFetchLoop = -1;
        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            Vector outVecReturnData = new Vector();
            long l = -1;

            ResultSetMetaData rsMD = rs.getMetaData();
            lStartFetchLoop = System.currentTimeMillis();
            while (rs.next()) {
                for (int i = 0; i < rsMD.getColumnCount(); i++) {
                    outVecReturnData.addElement(rs.getObject(i + 1));
                }
                iLoop++;
                if (iLoop == iNumRecsToPull) {
                    break;
                }
            }

        } catch (Throwable e) {
            Log.LogFatal("error throwable [" + sSQL + "]");
            //throw e;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }


        long lDur = com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart);
        long lDurFetchOnly = com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStartFetchLoop);
        api.Log.Log(new java.util.Date() + "done SQL test [" + sComment + "] call number ["
                + iCallCount_testQuerySpeed
                + "] lDur [" + lDur + "] ms "
                + "] lDurFetchOnly [" + lDurFetchOnly + "] ms "
                + "] iNumRecsToPull [" + iNumRecsToPull + "] ms ");
        iCallCount_testQuerySpeed++;
        return lDur;

    }

    /*
     * private static int testApplyROC(Connection dbc) throws Exception { //api.tsclassify.TSApplyROC.doApply("100040", dbc , true); return 0;
     *
     * }
     */
    public static void testLanguageTranslate(Connection dbc) throws Throwable {
        String sOut = TextTranslator_BabelFish.translateText("capabilities", new Integer(2), new Integer(14), true);
        System.exit(1);
        String sURLTranslateCorpus = "http://207.103.213.104:8080/itsharvester/ts?"
                + "fn=multilingual.TSTranslateCorpus"
                + "&CORPUSID=50"
                + "&LANGFROM=2"
                + "&LANGTO=14";

        System.out.println("pre executeapi");
        executeAPI(sURLTranslateCorpus, dbc);
        System.out.println("post executeapi");
        //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
        //executeAPI (sURLClassify,  dbc );
        clsUtils.waitForKeyboardInput("at hkwait 9 ");
        com.indraweb.util.Log.log("MAINTEST DONE1 \r\n");
        System.exit(0);

    }

    public static void testCorpusSigDataMigrate(Connection dbc) throws Throwable {
        //api.multilingual.TSMigrateCorpusLanguageData.doMigrate(100004, dbc);
        api.multilingual.TSMigrateCorpusLanguageData.doMigrate(50, dbc);
    }

    public static void testURLToOracleStreamConvert(Connection dbc) {

        //OutputStream out = System.out;
        //String documentid = "950585";  // just as a test, but any document id will do
        //String documentid = "41293";  // \\66.134.131.60\fdrive\GulfWarDocs\data\4547.html
        //String documentid = "739571";  // http://www.oehha.ca.gov/air/chronic_rels/pdf/sulfuric.pdf
        String documentid = "936894";  // http://ist-socrates.berkeley.edu/~bsp/caucasus/newsletter/2003-03ccan.pdf


        String s = new String();
        CallableStatement cs = null;
        try {
            cs = dbc.prepareCall("begin GETTEXTSTREAM (:1, :2); end;");
            cs.setString(1, documentid);
            cs.registerOutParameter(2, Types.CLOB);
            cs.execute();

            Clob c = cs.getClob(2);
            //System.out.println("<LENGTH>"+c.length()+"</LENGTH>");
            //System.out.println("<PARTIAL>"+c.getSubString(1,2000)+"</PARTIAL>");
            //System.out.println("<PARTIAL>"+c.getSubString(1,2000)+"</PARTIAL>");
        } catch (Exception e) {
            api.Log.LogError(e);

        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (Exception e) {
                }
                cs = null;
            }
        }

    }

    private static void testDom4JHtmlRead() throws Exception {
        // Transmit the request document
        //URL u = new URL ("http://www.google.com");
        URL u = new URL("http://66.134.131.35/itsapi/ts?fn=security.TSLogin&UserID=hkon&password=racer9");
        SAXReader reader = new SAXReader();
        org.dom4j.Document document = reader.read(u);
        //org.dom4j.Element eSUBSCRIBERtest = document.nodeIterator(). element("SUBSCRIBER");
        System.out.println("document.getName() [" + document.getName() + "]");
        org.dom4j.Element elemTSRESULT = document.getRootElement();
        System.out.println("e.getName()  [" + elemTSRESULT.getName() + "]");
        org.dom4j.Element eSUBSCRIBER = elemTSRESULT.element("SUBSCRIBER");
        List listOfSubScirberTags = eSUBSCRIBER.elements();
        Iterator iterSubsciberTags = listOfSubScirberTags.iterator();
        int i = 0;
        while (iterSubsciberTags.hasNext()) {
            org.dom4j.Element elemSub = (Element) iterSubsciberTags.next();
            System.out.println(i + ". elemsub.getName()   [" + elemSub.getName() + "] [" + elemSub.getText() + "]");
        }

    }

    private static void test3DNodeDocNodeVisualization(Connection dbc) throws Exception {
        FileWriter fw = null;
        try {
            String sCorpusID = "300012";

            // collect up nodeids this corpus

            String sSQL1 = "select nodeid, nodetitle from node where corpusid = " + sCorpusID;
            Statement stmt1 = dbc.createStatement();
            ResultSet rs1 = stmt1.executeQuery(sSQL1);
            HashSet hsSNodeIDsThisCorpus = new HashSet();
            Hashtable htSNodeIDToNodeTitle = new Hashtable();
            int iloopnode = -1;
            while (rs1.next()) {
                iloopnode++;
                int iNodeidCurrent = rs1.getInt(1);
                String sNodeTitle = rs1.getString(2);
                hsSNodeIDsThisCorpus.add("" + iNodeidCurrent);
                htSNodeIDToNodeTitle.put("" + iNodeidCurrent, sNodeTitle);
                //if (iloopnode % 1000 == 0)
                //  System.out.println (iloopnode + ". getting node record ");

            }
            rs1.close();
            stmt1.close();


            String sSQL = "select nodeid, documentid from nodedocument";

            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);


            Hashtable htSDocIDToVecOfINodeIDs = new Hashtable();
            Hashtable htNodeNodeCounterAllDocuments = new Hashtable();
            //SortedSet ssAllNodes = new SortedSet();
            int iLoopNodeDoc = -1;
            while (rs.next()) {
                iLoopNodeDoc++;
                //if (iLoopNodeDoc % 10000 == 0)
                //   System.out.println (iLoopNodeDoc + ". getting nodedoc record ");
                //if (iLoopNodeDoc > 500000)
                //  break;

                int iNodeidCurrent = rs.getInt(1);
                if (hsSNodeIDsThisCorpus.contains("" + iNodeidCurrent)) {
                    int iDocid = rs.getInt(2);
                    // for all node node pairs this doc ...
                    Vector vINodeIDsThisDocPre = (Vector) htSDocIDToVecOfINodeIDs.get("" + iDocid);
                    if (vINodeIDsThisDocPre == null) {
                        vINodeIDsThisDocPre = new Vector();
                        htSDocIDToVecOfINodeIDs.put("" + iDocid, vINodeIDsThisDocPre);
                    }
                    //else
                    //System.out.println("found a repeat");

                    MainTest.hash_increment_count_for_string(
                            htNodeNodeCounterAllDocuments, "" + iNodeidCurrent + "\t" + iNodeidCurrent, 1);
                    for (int i = 0; i < vINodeIDsThisDocPre.size(); i++) {
                        Integer INodeIDPre = (Integer) vINodeIDsThisDocPre.elementAt(i);
                        //ssAllNodes.add(INodeID1);
                        if (INodeIDPre.intValue() < iNodeidCurrent) {
                            MainTest.hash_increment_count_for_string(
                                    htNodeNodeCounterAllDocuments, "" + INodeIDPre + "\t" + iNodeidCurrent, 1);
                        } else if (INodeIDPre.intValue() > iNodeidCurrent) {
                            MainTest.hash_increment_count_for_string(
                                    htNodeNodeCounterAllDocuments, iNodeidCurrent + "\t" + INodeIDPre, 1);
                        } else {
                            throw new Exception("why ssame node " + iNodeidCurrent);
                        }
                    }
                    vINodeIDsThisDocPre.addElement(new Integer(iNodeidCurrent));
                }
            }

            rs.close();
            stmt.close();

            System.out.println("done getting nodedoc data");
            Enumeration enumKeys = htNodeNodeCounterAllDocuments.keys();
            int i = 0;
            String sFileName = "c:/temp/test3DNodeDocNodeVisualization_" + sCorpusID + ".txt";
            com.indraweb.util.UtilFile.deleteFile(sFileName);
            fw = new FileWriter(sFileName);
            fw.write("Indraweb Dataset: Node1-Doc-Node2 counts across corpora.");
            int iLoopFile = -1;
            while (enumKeys.hasMoreElements()) {
                iLoopFile++;
                if (iLoopFile % 1000 == 0) {
                    System.out.println("file write line [" + iLoopFile + "]");
                }

                String sNodeNode = (String) enumKeys.nextElement();
                String[] sArrNode = sNodeNode.split("\t");
                String sNodeTitleLeft = (String) htSNodeIDToNodeTitle.get("" + sArrNode[0]);
                String sNodeTitleRight = (String) htSNodeIDToNodeTitle.get("" + sArrNode[1]);
                int iCountCommonDocs = ((Integer) htNodeNodeCounterAllDocuments.get(sNodeNode)).intValue();
                String sOut = "";
                if (iCountCommonDocs > 0) {
                    sOut = i + "\t"
                            + sNodeNode + "\t"
                            + iCountCommonDocs + "\t"
                            + sNodeTitleLeft + "\t"
                            + sNodeTitleRight + "\r\n";

                    fw.write(sOut);
                }
                if (iLoopNodeDoc % 100 == 0) {
                    System.out.print("sOut [" + sOut + "]");
                }
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
        } finally {
            fw.close();
        }
        System.out.println("done");


    }
    /*
     * private static void test3DNodeDocNodeVisualization (Connection dbc) { try { String sSQL = "select n.nodeid, nd.documentid from nodedocument nd, node n " + " where n.nodeid = nd.nodeid and " + "
     * rownum < 10000 and " + "n.corpusid = 100011 " + "";
     *
     * Statement stmt = dbc.createStatement (); ResultSet rs = stmt.executeQuery (sSQL);
     *
     *
     * Hashtable htNodeNodeCounterAllDocuments = new Hashtable (); //SortedSet ssAllNodes = new SortedSet(); int i2 = 0; int iDocIDLast = -1; Vector vNodeIDsThisDoc = new Vector (); while (rs.next ())
     * { int iNodeid = rs.getInt (1); int iDocid = rs.getInt (2); if (iDocid == iDocIDLast && iDocIDLast != -1) { if (i2 % 1000 == 0) System.out.println (i2 + "th row got ");
     *
     *
     * }
     * else // new document - report on this one, dump data continue {
     *
     * // for all node node pairs this doc ... for ( int i = 0 ; i < vNodeIDsThisDoc.size () ; i++ ) { Integer INodeID1 = (Integer) vNodeIDsThisDoc.elementAt (i); //ssAllNodes.add(INodeID1); int
     * iInnerLoop = 0; for ( int j = i ; j < vNodeIDsThisDoc.size () ; j++ ) { Integer INodeID2 = (Integer) vNodeIDsThisDoc.elementAt (j); MainTest.hash_increment_count_for_string (
     * htNodeNodeCounterAllDocuments , "" + INodeID1 + "\t" + INodeID2 , 1); iInnerLoop++; }
     *
     * System.out.print("iInnerLoop [" + iInnerLoop+ "] vNodeIDsThisDoc.size() [" + vNodeIDsThisDoc.size() + "]");
     *
     * vNodeIDsThisDoc = new Vector (); iDocIDLast = iDocid; } i2++; } }
     *
     * rs.close (); stmt.close ();
     *
     * Enumeration enumKeys = htNodeNodeCounterAllDocuments.keys (); int i = 0; while (enumKeys.hasMoreElements ()) { String sNodeNode = (String) enumKeys.nextElement (); System.out.println (i + "\t"
     * + sNodeNode + "\t" + (Integer) htNodeNodeCounterAllDocuments.get (sNodeNode)); i++; } } catch ( Exception e ) { e.printStackTrace (); e.printStackTrace (); } System.out.println ("done");
     *
     *
     * }
     */

    private static void test3DNodeDocNodeVisualization_usingOracleForCount(Connection dbc)
            throws Exception {
        long lStartRoutiune = System.currentTimeMillis();
        FileWriter fw = null;
        try {

            //Vector v = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLGetCorpora, dbc);
            Vector vCorpora = new Vector();
            //vCorpora.addElement("100011");  // tech target ?
            //vCorpora.addElement("100009");  // psych
            //vCorpora.addElement("100045");  // psych
            vCorpora.addElement("100040");  // mitre

            Hashtable htNodeNodeCounterAllDocuments = new Hashtable();
            HashSet hsAllNodes = new HashSet();

            long lStart = System.currentTimeMillis();
            for (int iCorpusIndex = 0; iCorpusIndex < vCorpora.size(); iCorpusIndex++) // for all corpora
            {
                String sSQL = "select nd1.nodeid, nd2.nodeid, count (*), n1.nodetitle, n2.nodetitle "
                        + " from nodedocument nd1, nodedocument nd2, node n1, node n2 "
                        + " where nd1.documentid = nd2.documentid "
                        + " and n1.nodeid = nd1.nodeid "
                        + " and n2.nodeid = nd2.nodeid "
                        + " and n1.corpusid = " + vCorpora.elementAt(iCorpusIndex)
                        + " and n2.corpusid = " + vCorpora.elementAt(iCorpusIndex)
                        + " and n1.nodeid not in (select parentid from node where corpusid = " + vCorpora.elementAt(iCorpusIndex) + ")"
                        + " and n2.nodeid not in (select parentid from node where corpusid = " + vCorpora.elementAt(iCorpusIndex) + ")"
                        + "  group by nd1.nodeid, nd2.nodeid, n1.nodetitle, n2.nodetitle  ";
                System.out.println("sSQL [" + sSQL + "]");
                Statement stmt = dbc.createStatement();
                System.out.println();
                ResultSet rs = stmt.executeQuery(sSQL);

                //SortedSet ssAllNodes = new SortedSet();
                int i2 = 0;
                Vector vNodeIDsThisDoc = new Vector();
                while (rs.next()) {
                    int iNodeID1 = rs.getInt(1);

                    int iNodeID2 = rs.getInt(2);

                    int iCount = rs.getInt(3);
                    String sNode1Title1 = rs.getString(4);
                    String sNode1Title2 = rs.getString(5);


                    MainTest.hash_increment_count_for_string(
                            htNodeNodeCounterAllDocuments, "" + iNodeID1 + "\t" + iNodeID2, iCount);
                    hsAllNodes.add(new Integer(iNodeID1));
                    hsAllNodes.add(new Integer(iNodeID2));
                    i2++;
                    if (i2 % 1000 == 0) {
                        System.out.println(i2 + ". in N-N loop for corpus [" + vCorpora.elementAt(iCorpusIndex) + "]");
                    }
                }

                rs.close();
                stmt.close();

            }

            System.out.println("Indraweb Dataset: Node1-Doc-Node2 counts across corpora.");
            System.out.println("total nodes [" + hsAllNodes.size() + "] this fill phase above took ["
                    + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart)
                    + "] ms for corpus set [" + vToStrLocal(vCorpora) + "] ");
            Enumeration enumKeys = htNodeNodeCounterAllDocuments.keys();
            int i = 0;
            String sFileName = "c:/temp/NDNdata_" + vToStrLocal(vCorpora) + ".txt";
            com.indraweb.util.UtilFile.deleteFile(sFileName);
            fw = new FileWriter(sFileName);
            fw.write("output row counter" + "\t" + "node1" + "\t" + "node2" + "\t"
                    + "shared doc count these two nodes\r\n");
            while (enumKeys.hasMoreElements()) {
                String sNodeNode = (String) enumKeys.nextElement();
                fw.write(i + "\t" + sNodeNode + "\t"
                        + (Integer) htNodeNodeCounterAllDocuments.get(sNodeNode) + "\r\n");
                i++;
                if (i % 50000 == 0) {
                    System.out.println(i + ". file writing line [" + i + "] " + new java.util.Date());
                }
            }
            fw.close();
            fw = null;

        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
        } finally {
            if (fw != null) {
                fw.close();
            }
        }
        System.out.println("done in [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStartRoutiune) + "] ms");

        /*
         * SQL> select corpus.corpusid, count(*), corpus_name from node, corpus where nod rpusid group by corpus.corpus_name,corpus.corpusid order by corpusid
         *
         * SQL> set linesize 1000 SQL> /
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 1 1641 TEST Merck Manual 5 22677 General Knowledge 11 5275 Information
         * Technology 12 20415 Pharmaceuticals: By Type 13 29288 Pharmaceuticals: By Company 16 1400 Gulf War Illness 17 898 Medical Aspects of Biological and Chemical Warfare 18 269 Management of
         * Biological Casualties 21 239 Biosafety in Laboratories 22 365 Technology Underlying WMD 29 310 Reference Texts
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 31 29 Reference Text 32 29 Company/Authority 33 29 Product/Brand 34 29
         * Therap./Disease 35 2182 Company 50 215 WMD DOD Test 51 165415 Gale Encyclopedia of Associations 100001 2456 Management Taxonomy 100002 4043 Business Taxonomy 100003 33508 Medicine 100004
         * 10471 Alternative Medicine
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 100005 9774 Cancer / Oncology 100006 7195 Genetics 100007 1434 Country Topic
         * Overviews 100008 502 Arms Control 100009 717 Psychology 100010 14812 Nursing 100011 1739 Environmental Taxonomy 100012 1096 Foreign Policy 100013 1520 Public Water Systems 100014 4984 EPARS
         * 100027 1154 Software Engineering
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 100039 70 Inside al Qaeda MANUAL ADDITION 100040 81 MITRE Trec 50 Topic Taxonomy
         * 100045 5769 Environmental Microbiology 100046 68 Inside Al Qaeda 100053 1525 TEST EPARS 100057 77 State Departments Patterns of Global Ter 100058 36 Anarchists Cookbook 100059 88 Terrorists
         * Handbook 100060 270 DCMA Policy 100061 92 Delphi: Benefits and Pay 100062 31 Delphi: Career Development
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 100063 59 Delphi: O-C 100064 63 Delphi: Travel 100065 15 Mike Test 1 100066 1
         * Mike Taxonomy 100067 1 Sample Taxonomy 100068 3 Combined Taxonomy 100069 1 Test Taxonomy 100070 2 Testing Taxonomy 200001 47 TEST Child Psychology 200004 45 Chemical Warfare 200008 99 TEST
         * DuPont Ti-Pure IV
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 300012 39014 Chemical Processing Technology 2200001 5139 WHO ATC
         *
         * 57 rows selected.
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- ---------------------------------------------- 1 1934 TEST Merck Manual 5 460486 General Knowledge 11 64791 Information Technology 16
         * 18357 Gulf War Illness 17 17588 Medical Aspects of Biological and Chemical War 18 142 Management of Biological Casualties 21 415 Biosafety in Laboratories 22 486 Technology Underlying WMD
         * 100001 64 Management Taxonomy 100002 761873 Business Taxonomy 100003 15676 Medicine
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- ---------------------------------------------- 100005 2537 Cancer / Oncology 100006 2216 Genetics 100010 17592 Nursing 100011 88407
         * Environmental Taxonomy 100040 40564 MITRE Trec 50 Topic Taxonomy 100045 23716 Environmental Microbiology 100046 16311 Inside Al Qaeda 100058 844 Anarchists Cookbook 100059 7537 Terrorists
         * Handbook 100060 57 DCMA Policy 200004 1659 Chemical Warfare
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- ---------------------------------------------- 300012 3180444 Chemical Processing Technology
         *
         * 23 rows selected.
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         */

    }

    private static void test3DNodeDocNodeVisualization_usingOracleForCount_allOracleMethod(Connection dbc)
            throws Exception {
        long lStartRoutiune = System.currentTimeMillis();
        FileWriter fw = null;
        try {

            //Vector v = com.indraweb.database.JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLGetCorpora, dbc);
            Vector vCorpora = new Vector();
            //vCorpora.addElement("100011");  // tech target ?
            //vCorpora.addElement("100009");  // psych
            //vCorpora.addElement("100045");  // psych
            //vCorpora.addElement ("100040");  // mitre
            vCorpora.addElement("300012");  // kirk

            Hashtable htNodeNodeCounterAllDocuments = new Hashtable();
            HashSet hsAllNodes = new HashSet();

            long lStart = System.currentTimeMillis();
            String sFileName = "c:/temp/NDNdata_OracleOnly_" + vToStrLocal(vCorpora) + ".txt";
            System.out.println("processing file [" + sFileName + "]");
            com.indraweb.util.UtilFile.deleteFile(sFileName);
            fw = new FileWriter(sFileName);
            fw.write("Indraweb Dataset: Node1-Doc-Node2 counts across corpora.");
            fw.write("total nodes [" + hsAllNodes.size() + "] this fill phase above took ["
                    + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart)
                    + "] ms for corpus set [" + vToStrLocal(vCorpora) + "] ");
            fw.write("output row counter" + "\t" + "node1" + "\t" + "node2" + "\t"
                    + "shared doc count these two nodes\r\n");
            for (int iCorpusIndex = 0; iCorpusIndex < vCorpora.size(); iCorpusIndex++) // for all corpora
            {
                String sSQL =
                        /*
                         * "select nd1.nodeid, nd2.nodeid, count (*), n1.nodetitle, n2.nodetitle " + " from nodedocument nd1, nodedocument nd2, node n1, node n2 " + " where nd1.documentid =
                         * nd2.documentid " + " and n1.nodeid = nd1.nodeid " + " and n2.nodeid = nd2.nodeid " + " and n1.nodeid not in (select parentid from node where corpusid = " +
                         * vCorpora.elementAt (iCorpusIndex) + ")" + " and n2.nodeid not in (select parentid from node where corpusid = " + vCorpora.elementAt (iCorpusIndex) + ")" + " and n1.corpusid
                         * = " + vCorpora.elementAt (iCorpusIndex) + " and n2.corpusid = " + vCorpora.elementAt (iCorpusIndex) + " group by nd1.nodeid, nd2.nodeid, n1.nodetitle, n2.nodetitle ";
                         */
                        "select nd1.nodeid, nd2.nodeid, count (*) "
                        + " from nodedocument nd1, nodedocument nd2, node n1, node n2 "
                        + " where nd1.documentid = nd2.documentid "
                        + " and n1.nodeid = nd1.nodeid "
                        + " and n2.nodeid = nd2.nodeid "
                        + " and n1.corpusid = " + vCorpora.elementAt(iCorpusIndex)
                        + " and n2.corpusid = " + vCorpora.elementAt(iCorpusIndex)
                        + "  group by nd1.nodeid, nd2.nodeid ";


                System.out.println("sSQL [" + sSQL + "]");
                Statement stmt = dbc.createStatement();
                ResultSet rs = stmt.executeQuery(sSQL);

                //SortedSet ssAllNodes = new SortedSet();
                int i2 = 0;
                Vector vNodeIDsThisDoc = new Vector();
                while (rs.next()) {
                    int iNodeID1 = rs.getInt(1);

                    int iNodeID2 = rs.getInt(2);

                    int iCount = rs.getInt(3);
                    //String sNode1Title1 = rs.getString (4);
                    //String sNode1Title2 = rs.getString (5);

                    String sWrite = i2 + "\t"
                            + iNodeID1 + "\t"
                            + //sNode1Title1 + "\t" +
                            iNodeID2 + "\t"
                            + //sNode1Title2 + "\t" +
                            iCount
                            + "\r\n";
                    fw.write(sWrite);
                    i2++;
                    if (i2 % 50000 == 0) {
                        System.out.println(i2 + ". file writing line [" + i2 + "] [" + sWrite + "] " + new java.util.Date());
                    }

                    if (i2 > 100000) {
                        break;
                    }
                }

                rs.close();
                stmt.close();

            }

            fw.close();
            fw = null;

        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
        } finally {
            if (fw != null) {
                fw.close();
            }
        }
        System.out.println("done in [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStartRoutiune) + "] ms");

        /*
         * SQL> select corpus.corpusid, count(*), corpus_name from node, corpus where nod rpusid group by corpus.corpus_name,corpus.corpusid order by corpusid
         *
         * SQL> set linesize 1000 SQL> /
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 1 1641 TEST Merck Manual 5 22677 General Knowledge 11 5275 Information
         * Technology 12 20415 Pharmaceuticals: By Type 13 29288 Pharmaceuticals: By Company 16 1400 Gulf War Illness 17 898 Medical Aspects of Biological and Chemical Warfare 18 269 Management of
         * Biological Casualties 21 239 Biosafety in Laboratories 22 365 Technology Underlying WMD 29 310 Reference Texts
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 31 29 Reference Text 32 29 Company/Authority 33 29 Product/Brand 34 29
         * Therap./Disease 35 2182 Company 50 215 WMD DOD Test 51 165415 Gale Encyclopedia of Associations 100001 2456 Management Taxonomy 100002 4043 Business Taxonomy 100003 33508 Medicine 100004
         * 10471 Alternative Medicine
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 100005 9774 Cancer / Oncology 100006 7195 Genetics 100007 1434 Country Topic
         * Overviews 100008 502 Arms Control 100009 717 Psychology 100010 14812 Nursing 100011 1739 Environmental Taxonomy 100012 1096 Foreign Policy 100013 1520 Public Water Systems 100014 4984 EPARS
         * 100027 1154 Software Engineering
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 100039 70 Inside al Qaeda MANUAL ADDITION 100040 81 MITRE Trec 50 Topic Taxonomy
         * 100045 5769 Environmental Microbiology 100046 68 Inside Al Qaeda 100053 1525 TEST EPARS 100057 77 State Departments Patterns of Global Ter 100058 36 Anarchists Cookbook 100059 88 Terrorists
         * Handbook 100060 270 DCMA Policy 100061 92 Delphi: Benefits and Pay 100062 31 Delphi: Career Development
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 100063 59 Delphi: O-C 100064 63 Delphi: Travel 100065 15 Mike Test 1 100066 1
         * Mike Taxonomy 100067 1 Sample Taxonomy 100068 3 Combined Taxonomy 100069 1 Test Taxonomy 100070 2 Testing Taxonomy 200001 47 TEST Child Psychology 200004 45 Chemical Warfare 200008 99 TEST
         * DuPont Ti-Pure IV
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- -------------------------------------------------------- 300012 39014 Chemical Processing Technology 2200001 5139 WHO ATC
         *
         * 57 rows selected.
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- ---------------------------------------------- 1 1934 TEST Merck Manual 5 460486 General Knowledge 11 64791 Information Technology 16
         * 18357 Gulf War Illness 17 17588 Medical Aspects of Biological and Chemical War 18 142 Management of Biological Casualties 21 415 Biosafety in Laboratories 22 486 Technology Underlying WMD
         * 100001 64 Management Taxonomy 100002 761873 Business Taxonomy 100003 15676 Medicine
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- ---------------------------------------------- 100005 2537 Cancer / Oncology 100006 2216 Genetics 100010 17592 Nursing 100011 88407
         * Environmental Taxonomy 100040 40564 MITRE Trec 50 Topic Taxonomy 100045 23716 Environmental Microbiology 100046 16311 Inside Al Qaeda 100058 844 Anarchists Cookbook 100059 7537 Terrorists
         * Handbook 100060 57 DCMA Policy 200004 1659 Chemical Warfare
         *
         * CORPUSID COUNT(*) CORPUS_NAME ---------- ---------- ---------------------------------------------- 300012 3180444 Chemical Processing Technology
         *
         * 23 rows selected.
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         */

    }

    private static void test3DNodeDocNodeVisualization_v4_builtFrom1_doAllInMem(Connection dbc) throws Exception {
        FileWriter fw = null;
        Vector vStrNodeIdColonDocId = new Vector();
        //String sCorpusID = "100001";
        String sCorpusID = "300012";
        int iMinCountInclusive = 2;
        HashSet hsSNodeIDsThisCorpus = new HashSet();
        Hashtable htSNodeIDToNodeTitle = new Hashtable();
        try {
            /*
             * HashSet hsCorporaToDo = new HashSet(); String sCorpusID = "300012";
             *
             * // collect up nodeids this corpus
             *
             * String sSQL1 = "select nodeid, nodetitle, corpusid from node where corpusid = " + sCorpusID; Statement stmt1 = dbc.createStatement (); ResultSet rs1 = stmt1.executeQuery (sSQL1); int
             * iloopnode = -1; while (rs1.next ()) { iloopnode++; int iNodeidCurrent = rs1.getInt (1); String sNodeTitle = rs1.getString (2); int iCorpusID = rs1.getInt (1); hsSNodeIDsThisCorpus.add
             * ("" + iNodeidCurrent); htSNodeIDToNodeTitle.put ("" + iNodeidCurrent , sNodeTitle); if (iloopnode % 1000 == 0) System.out.println (iloopnode + ". getting node record ");
             *
             * }
             * rs1.close (); stmt1.close ();
             *
             *
             */
            String sSQL = "select n.nodeid, documentid from node n, nodedocument nd "
                    + " where n.nodeid = nd.nodeid and "
                    + " n.corpusid = " + sCorpusID;

            Statement stmt = dbc.createStatement();
            long lStart = System.currentTimeMillis();
            System.out.println("sSQL 1 [" + sSQL + "]");
            ResultSet rs = stmt.executeQuery(sSQL);

            Hashtable htSDocIDToVecOfINodeIDs = new Hashtable();
            Hashtable htNodeNodeCounterAllDocuments = new Hashtable();
            //SortedSet ssAllNodes = new SortedSet();
            int iLoopNodeDoc = -1;
            while (rs.next()) {
                iLoopNodeDoc++;
                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". getting nodedoc record ");
                }

                int iNodeidCurrent = rs.getInt(1);
                //if (hsSNodeIDsThisCorpus.contains ("" + iNodeidCurrent))
                {
                    int iDocid = rs.getInt(2);
                    // for all node node pairs this doc ...
                    String sNewRec = iNodeidCurrent + ":" + iDocid;
                    vStrNodeIdColonDocId.addElement(sNewRec);
                    if (iLoopNodeDoc % 1000 == 0) {
                        System.out.println(iLoopNodeDoc + ". sNewRec [" + sNewRec + "]");
                    }

                    Vector vINodeIDsThisDocPre = (Vector) htSDocIDToVecOfINodeIDs.get("" + iDocid);
                }
            }

            rs.close();
            stmt.close();

            System.out.println("sSQL 1 done ms [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart) + "]");
            System.out.println("done getting nodedoc data");


            // now collate
            System.out.println("start collate # node-doc recs : " + vStrNodeIdColonDocId.size());
            Enumeration enumStrNodeIdColonDocId = vStrNodeIdColonDocId.elements();
            iLoopNodeDoc = 0;
            long lLastSetStart = System.currentTimeMillis();
            while (enumStrNodeIdColonDocId.hasMoreElements()) {
                String sNodeIdColonDocId = (String) enumStrNodeIdColonDocId.nextElement();
                String[] sArrNodeDoc = sNodeIdColonDocId.split(":");
                iLoopNodeDoc++;
                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". collating nodedoc record of [" + vStrNodeIdColonDocId.size()
                            + "] ms last set [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lLastSetStart) + "]");
                    lLastSetStart = System.currentTimeMillis();
                }
                String sNodeid = sArrNodeDoc[0];
                String sDocID = sArrNodeDoc[1];

                //if (hsSNodeIDsThisCorpus.contains ("" + iNodeidCurrent))
                {
                    // for all node node pairs this doc ...
                    Vector vINodeIDsThisDocPre = (Vector) htSDocIDToVecOfINodeIDs.get(sDocID);
                    if (vINodeIDsThisDocPre == null) {
                        vINodeIDsThisDocPre = new Vector();
                        htSDocIDToVecOfINodeIDs.put(sDocID, vINodeIDsThisDocPre);
                    }
                    //else
                    //  System.out.println("found a repeat");

                    // self count
                    //MainTest.hash_increment_count_for_string (
                    //        htNodeNodeCounterAllDocuments , sNodeid + "\t" + sNodeid , 1);
                    for (int i = 0; i < vINodeIDsThisDocPre.size(); i++) {

                        Integer INodeIDPre = (Integer) vINodeIDsThisDocPre.elementAt(i);
                        //ssAllNodes.add(INodeID1);
                        int iNodeidCurrent = Integer.parseInt(sNodeid);
                        if (INodeIDPre.intValue() < iNodeidCurrent) {
                            MainTest.hash_increment_count_for_string(
                                    htNodeNodeCounterAllDocuments, "" + INodeIDPre + "\t" + iNodeidCurrent, 1);
                        } else if (INodeIDPre.intValue() > iNodeidCurrent) {
                            MainTest.hash_increment_count_for_string(
                                    htNodeNodeCounterAllDocuments, iNodeidCurrent + "\t" + INodeIDPre, 1);
                        } else {
                            throw new Exception("why ssame node " + iNodeidCurrent);
                        }
                    }
                    vINodeIDsThisDocPre.addElement(new Integer(sNodeid));
                }
            }



            // now emit


            Enumeration enumKeys = htNodeNodeCounterAllDocuments.keys();
            String sFileName = "c:/temp/test3DNodeDocNodeVisualization_" + sCorpusID + ".txt";
            com.indraweb.util.UtilFile.deleteFile(sFileName);
            fw = new FileWriter(sFileName);
            fw.write(new java.util.Date() + " Indraweb Dataset: Node1-Doc-Node2 counts corpus [" + sCorpusID + "].\r\n");
            fw.write(" # records pre min filter [" + htNodeNodeCounterAllDocuments.size() + "] iMinCountInclusive [" + iMinCountInclusive + "]\r\n");
            int iLoopFile = -1;
            while (enumKeys.hasMoreElements()) {
                iLoopFile++;

                String sNodeNode = (String) enumKeys.nextElement();
                String[] sArrNode = sNodeNode.split("\t");
                String sNodeTitleLeft = (String) htSNodeIDToNodeTitle.get("" + sArrNode[0]);
                String sNodeTitleRight = (String) htSNodeIDToNodeTitle.get("" + sArrNode[1]);
                int iCountCommonDocs = ((Integer) htNodeNodeCounterAllDocuments.get(sNodeNode)).intValue();
                String sOut = "";
                if (iCountCommonDocs >= iMinCountInclusive) {
                    sOut = iLoopFile + "\t"
                            + sNodeNode + "\t"
                            + iCountCommonDocs + "\t"
                            //+ sNodeTitleLeft + "\t" +
                            //sNodeTitleRight
                            + "\r\n";

                    fw.write(sOut);
                } //else
                //System.out.println ("common docs <= 0 !!!");
                //if ( iLoopFile % 1000 ==  0 )
                //System.out.println (iLoopFile + ". file write line [" + sOut + "]");

                //if (iLoopNodeDoc % 100 == 0)
                //  System.out.print ("sOut [" + sOut);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
        } finally {
            fw.close();
        }
        System.out.println("done");


    }

    private static void test3DNodeDocNodeVisualization_v5_sortByDocID(Connection dbc) throws Exception {
        FileWriter fw = null;
        String sCorpusID = "300012";
        int iMinCountInclusive = 4;
        //String sCorpusID = "300012";
        try {
            String sSQL = "select n.nodeid, documentid from node n, nodedocument nd "
                    + " where n.nodeid = nd.nodeid and "
                    + " n.corpusid = " + sCorpusID + " order by documentid";

            Statement stmt = dbc.createStatement();
            long lStart = System.currentTimeMillis();
            System.out.println("sSQL 1 [" + sSQL + "]");
            ResultSet rs = stmt.executeQuery(sSQL);

            //SortedSet ssAllNodes = new SortedSet();
            int iLoopNodeDoc = -1;
            int iDodIDCurrentSetForOneDoc = -1;
            Vector vINodeIDThisdoc = new Vector();
            Hashtable htStrNodeNodeToSharedDocCounter = new Hashtable();
            while (rs.next()) {
                iLoopNodeDoc++;

                if (iLoopNodeDoc > 50000) {
                    break;
                }
                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". getting nodedoc record ");
                }

                int iNodeidDBRec = rs.getInt(1);
                //if (hsSNodeIDsThisCorpus.contains ("" + iNodeidCurrent))
                int iDocidDBRec = rs.getInt(2);
                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". nodedoc record doc [" + iDocidDBRec + "] node [" + iNodeidDBRec + "]");
                }
                // for all node node pairs this doc ...
                //String sNewRec = iNodeidDBRec + ":" + iDocidDBRec ;
/*
                 * if (iLoopNodeDoc % 1000 == 0) System.out.println (iLoopNodeDoc + ". sNewRec ");
                 */

                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". htStrNodeNodeToSharedDocCounter.size() : " + htStrNodeNodeToSharedDocCounter.size());
                }
                if (iDocidDBRec != iDodIDCurrentSetForOneDoc) {
                    if (iDodIDCurrentSetForOneDoc > 0) {
                        processTransitionPoint(vINodeIDThisdoc, htStrNodeNodeToSharedDocCounter);
                        iDodIDCurrentSetForOneDoc = iDocidDBRec;
                        vINodeIDThisdoc = new Vector();

                    }
                    iDodIDCurrentSetForOneDoc = iDocidDBRec;

                }
                vINodeIDThisdoc.addElement(new Integer(iNodeidDBRec));

                //System.out.println("outside if");

            }
            // handle end
            System.out.println(iCallCountprocessTransitionPoint + ". TAIL processTransitionPoint [" + iDodIDCurrentSetForOneDoc + "]");
            processTransitionPoint(vINodeIDThisdoc, htStrNodeNodeToSharedDocCounter);

            rs.close();
            stmt.close();

            System.out.println("sSQL 1 done ms [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart) + "]");
            System.out.println("done getting nodedoc data");



            // now emit


            Enumeration enumKeys = htStrNodeNodeToSharedDocCounter.keys();
            String sFileName = "c:/temp/test3DNodeDocNodeVisualization_v5" + sCorpusID + ".txt";
            com.indraweb.util.UtilFile.deleteFile(sFileName);
            fw = new FileWriter(sFileName);
            fw.write(new java.util.Date() + " Indraweb Dataset: Node1-Doc-Node2 counts corpus [" + sCorpusID + "].\r\n");
            fw.write(" # records pre min filter [" + htStrNodeNodeToSharedDocCounter.size() + "] iMinCountInclusive [" + iMinCountInclusive + "]\r\n");
            int iLoopFile = -1;
            int ilineout = 0;
            while (enumKeys.hasMoreElements()) {
                ilineout++;
                iLoopFile++;

                String sNodeColonNode = (String) enumKeys.nextElement();
                String[] sArrNode = sNodeColonNode.split("\t");
                //String sNodeTitleLeft = (String) htSNodeIDToNodeTitle.get ("" + sArrNode[0]);
                //String sNodeTitleRight = (String) htSNodeIDToNodeTitle.get ("" + sArrNode[1]);
                int iCountCommonDocs = ((Integer) htStrNodeNodeToSharedDocCounter.get(sNodeColonNode)).intValue();
                String sOut = "";
                if (iCountCommonDocs >= iMinCountInclusive) {
                    sOut = iLoopFile + "\t"
                            + sNodeColonNode + "\t"
                            + iCountCommonDocs + "\t" //+ sNodeTitleLeft + "\t" +
                            //sNodeTitleRight
                            ;
                    System.out.println(ilineout + ". sOut [" + sOut + "]");

                    fw.write(sOut + "\r\n");
                } //else
                //System.out.println ("common docs <= 0 !!!");
                //if ( iLoopFile % 1000 ==  0 )
                //System.out.println (iLoopFile + ". file write line [" + sOut + "]");

                //if (iLoopNodeDoc % 100 == 0)
                //  System.out.print ("sOut [" + sOut);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
        } finally {
            fw.close();
        }
        System.out.println("done");


    }

    private static void test3DNodeDocNodeVisualization_v6_sortByDocID_restrictToNodeAndUnder(Connection dbc) throws Exception {
        FileWriter fw = null;
        String sNodeID = "1089241";  // dyes and pigments
        //String sNodeID = "1090145";  // under dyes and pigments
        Hashtable htINodeIDToSNodeTitle = new Hashtable();
        int iMinCountInclusive = 2;
        //String sCorpusID = "300012";
        try {
            // get set of nodeid's

            String sSQLGetNodeIDs = "select nodeid, nodetitle  from node where rownum < 1001  start with parentid = " + sNodeID + " "
                    + "connect by prior nodeid = parentid";

            //String sSQLGetNodeIDs = "select nodeid  from node where nodeid in (1090155,1090156,1090147,1090149,1090151,1090153,1090157,1090158,1090160,1090161,1090164,1090148,1090146,1090150,1090152,1090154,1090159,1090162)";
            //String sSQLGetNodeIDs = "select nodeid  from node where nodeid in (1090155,1090156,1090147)";


            System.out.println("sSQL 0[" + sSQLGetNodeIDs + "]");
            Vector vTwoVecsNodeIDsAndNodeTitles = com.indraweb.database.JDBCIndra_Connection.executeDBQueryVecOfVecsReturned(sSQLGetNodeIDs, dbc, -1);
            Vector vNodeIDs = (Vector) vTwoVecsNodeIDsAndNodeTitles.elementAt(0);
            Vector vNodeTitles = (Vector) vTwoVecsNodeIDsAndNodeTitles.elementAt(1);


            // FILL Nodeid to NodeTitle hashtable
            for (int i = 0; i < vNodeIDs.size(); i++) {

                Integer INodeID = (Integer) vNodeIDs.elementAt(i);
                String sNodeTitle = (String) vNodeTitles.elementAt(i);
                htINodeIDToSNodeTitle.put(INodeID, sNodeTitle);
            }


            System.out.println("vNodeIDs.size()  [" + vNodeIDs.size() + "]");

            String sNodeIDsComma = com.indraweb.util.UtilStrings.convertVecToString(vNodeIDs, ",");

            //String sSQL = "select n.nodeid, documentid from node n, nodedocument nd " +
            String sSQL = "select nodeid , documentid from nodedocument "
                    + //" where n.nodeid = nd.nodeid and " +
                    //" n.corpusid = " + sCorpusID + " order by documentid";
                    " where nodeid in (" + sNodeIDsComma + ") order by documentid";

            Statement stmt = dbc.createStatement();
            long lStart = System.currentTimeMillis();
            System.out.println("sSQL 1 [" + sSQL + "]");
            ResultSet rs = stmt.executeQuery(sSQL);

            //SortedSet ssAllNodes = new SortedSet();
            int iLoopNodeDoc = -1;
            int iDodIDCurrentSetForOneDoc = -1;
            Vector vINodeIDThisdoc = new Vector();
            Hashtable htStrNodeNodeToSharedDocCounter = new Hashtable();
            while (rs.next()) {
                iLoopNodeDoc++;

                if (iLoopNodeDoc > 50000) {
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    System.out.println(iLoopNodeDoc + ". STOPPING EARLY !!!!!!!!!! ");
                    break;
                }
                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". getting nodedoc record ");
                }

                int iNodeidDBRec = rs.getInt(1);
                //if (hsSNodeIDsThisCorpus.contains ("" + iNodeidCurrent))
                int iDocidDBRec = rs.getInt(2);
                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". nodedoc record doc [" + iDocidDBRec + "] node [" + iNodeidDBRec + "]");
                }
                // for all node node pairs this doc ...
                //String sNewRec = iNodeidDBRec + ":" + iDocidDBRec ;
/*
                 * if (iLoopNodeDoc % 1000 == 0) System.out.println (iLoopNodeDoc + ". sNewRec ");
                 */

                if (iLoopNodeDoc % 1000 == 0) {
                    System.out.println(iLoopNodeDoc + ". htStrNodeNodeToSharedDocCounter.size() : " + htStrNodeNodeToSharedDocCounter.size());
                }
                if (iDocidDBRec != iDodIDCurrentSetForOneDoc) {
                    if (iDodIDCurrentSetForOneDoc > 0) {
                        processTransitionPoint(vINodeIDThisdoc, htStrNodeNodeToSharedDocCounter);
                        iDodIDCurrentSetForOneDoc = iDocidDBRec;
                        vINodeIDThisdoc = new Vector();

                    }
                    iDodIDCurrentSetForOneDoc = iDocidDBRec;

                }
                vINodeIDThisdoc.addElement(new Integer(iNodeidDBRec));

                //System.out.println("outside if");

            }
            // handle end
            System.out.println(iCallCountprocessTransitionPoint + ". TAIL processTransitionPoint [" + iDodIDCurrentSetForOneDoc + "]");
            processTransitionPoint(vINodeIDThisdoc, htStrNodeNodeToSharedDocCounter);

            rs.close();
            stmt.close();

            System.out.println("sSQL 1 done ms [" + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart) + "]");
            System.out.println("done getting nodedoc data");



            // now emit


            Enumeration enumKeys = htStrNodeNodeToSharedDocCounter.keys();
            String sFileName = "c:/temp/test3DNodeDocNodeVisualization_v6_" + sNodeID + ".txt";
            com.indraweb.util.UtilFile.deleteFile(sFileName);
            fw = new FileWriter(sFileName);
            fw.write(new java.util.Date() + " Indraweb Dataset: Node1-Doc-Node2 counts corpus # nodes[" + vNodeIDs.size() + " nodeid [" + sNodeIDsComma + "].\r\n");
            //fw.write (new java.util.Date () + " Indraweb Dataset: Node1-Doc-Node2 counts corpus [" + sCorpusID + "].\r\n");
            fw.write(" # records pre min filter [" + htStrNodeNodeToSharedDocCounter.size() + "] iMinCountInclusive [" + iMinCountInclusive + "]\r\n");
            int iLoopFile = -1;
            int ilineout = 0;
            while (enumKeys.hasMoreElements()) {
                ilineout++;
                iLoopFile++;

                String sNodeTabNode = (String) enumKeys.nextElement();
                String[] sArrNode = sNodeTabNode.split("\t");
                //String sNodeTitleLeft = (String) htSNodeIDToNodeTitle.get ("" + sArrNode[0]);
                //String sNodeTitleRight = (String) htSNodeIDToNodeTitle.get ("" + sArrNode[1]);
                int iCountCommonDocs = ((Integer) htStrNodeNodeToSharedDocCounter.get(sNodeTabNode)).intValue();
                String sOut = "";
                if (iCountCommonDocs >= iMinCountInclusive) {
                    String[] sArrNodeIDs = sNodeTabNode.split("\t");
                    Integer INodeID1 = new Integer(sArrNodeIDs[0]);
                    Integer INodeID2 = new Integer(sArrNodeIDs[1]);
                    String sNodeTitle1 = (String) htINodeIDToSNodeTitle.get(INodeID1);


                    sOut = //iLoopFile + "\t" +
                            sNodeTabNode + "\t"
                            + iCountCommonDocs + "\t" //+ sNodeTitleLeft + "\t" +
                            //sNodeTitleRight
                            ;
                    System.out.println(ilineout + ". sOut [" + sOut + "]");

                    fw.write(sOut + "\r\n");
                } //else
                //System.out.println ("common docs <= 0 !!!");
                //if ( iLoopFile % 1000 ==  0 )
                //System.out.println (iLoopFile + ". file write line [" + sOut + "]");

                //if (iLoopNodeDoc % 100 == 0)
                //  System.out.print ("sOut [" + sOut);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
        } finally {
            fw.close();
        }
        System.out.println("done");


    }
    private static int iCallCountprocessTransitionPoint = 0;

    private static void processTransitionPoint(Vector vINodeIDThisdoc, Hashtable htStrNodeNodeToSharedDocCounter) {
        //System.out.println("vector nodeids content [" + com.indraweb.util.UtilSets.vToStr(vINodeIDThisdoc) + "]" );
        iCallCountprocessTransitionPoint++;
        if (vINodeIDThisdoc.size() >= 1) {
            int[] iArrNodesThisDoc = com.indraweb.util.UtilSets.convertVectorTo_intArray(vINodeIDThisdoc);

            for (int i = 0; i < iArrNodesThisDoc.length; i++) {
                for (int j = i; j < iArrNodesThisDoc.length; j++) {
                    int iNode1 = iArrNodesThisDoc[i];
                    int iNode2 = iArrNodesThisDoc[j];

                    /*
                     * if (( iNode1 == 11317970 && iNode2 == 11318149 ) || ( iNode1 == 11318149 && iNode2 == 11317970 ) ) System.out.println("iNode1 [" + iNode1 + "] iNode2 [" + iNode2+ "]");
                     */
                    if (iNode1 <= iNode2) {
                        hash_increment_count_for_string(htStrNodeNodeToSharedDocCounter,
                                "" + iNode1 + "\t" + iNode2, 1);
                    } else {
                        hash_increment_count_for_string(htStrNodeNodeToSharedDocCounter,
                                "" + iNode2 + "\t" + iNode1, 1);
                    }
                    /*
                     * if (( iNode1 == 11317970 && iNode2 == 11318149 ) || ( iNode1 == 11318149 && iNode2 == 11317970 ) ) { System.out.println("result [" + htStrNodeNodeToSharedDocCounter.get (iNode2
                     * + "\t" + iNode1 )+ "]"); System.out.println("result [" + htStrNodeNodeToSharedDocCounter.get (iNode1 + "\t" + iNode2 )+ "]"); }
                     */
                }
            }

        }

    }

    // ************
    public static int hash_increment_count_for_string(Hashtable hasht, String s, int iIncrAmt) {
        // ************
        int i, newCount;
        i = hash_get_count_for_string(hasht, s);
        hasht.put(s, new Integer(i + iIncrAmt));
        //newCount = ((Integer) hasht.get (s)).intValue() ;
        //HKonLib.print ("Incremented count for word [" + s + "] to [" + newCount + "]" );
        return i + 1;

    }

    //-----------------------------------------------------------------------------
    public static int hash_get_count_for_string(Hashtable hasht, String s) {
        int i;

        if (hasht == null) {
            int debugbe = hasht.size() + 0;
            ;
        }


        try {
            if (hasht.get(s) == null) {
                return 0;
            } else {
                i = ((Integer) hasht.get(s)).intValue();
            }

            return i;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static String vToStrLocal(Vector v) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < v.size(); i++) {
            sb.append(v.elementAt(i).toString() + "_");
        }
        return sb.toString();

    }

    public static void testPerformance() {
        // test1();

        Object[] oArr = new Object[1000000];
        for (int i = 0; i < oArr.length; i++) {
            if (i % 3 == 0) {
                oArr[i] = new String("");
            }
            if (i % 3 == 1) {
                oArr[i] = new Double(1.0);
            }
            if (i % 3 == 2) {
                oArr[i] = new Integer(5);
            }

        }

        int iCountInt = 0;
        int iCountDbl = 0;
        int iCountStr = 0;

        long lStart = System.currentTimeMillis();
        for (int i = 0; i < oArr.length; i++) {
            if (oArr[i].getClass().getName().equals("java.lang.String")) {
                iCountStr++;
            }
            if (oArr[i].getClass().getName().equals("java.lang.Integer")) {
                iCountInt++;
            }
            if (oArr[i].getClass().getName().equals("java.lanag.Double")) {
                iCountDbl++;
            }
            if (i < 10000 && i % 1000 == 0) {
                System.out.println(
                        i + ". oarr.length [" + oArr.length + "] iCountInt [" + iCountInt + "] "
                        + "iCountDbl [" + iCountDbl + "] "
                        + "iCountStr [" + iCountStr + "] time ["
                        + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart) + "]");
            } else if (i < 100000 && i % 10000 == 0) {
                System.out.println(
                        i + ". oarr.length [" + oArr.length + "] iCountInt [" + iCountInt + "] "
                        + "iCountDbl [" + iCountDbl + "] "
                        + "iCountStr [" + iCountStr + "] time ["
                        + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart) + "]");
            } else if (i > 100000 && i % 100000 == 0) {
                System.out.println(
                        i + ". oarr.length [" + oArr.length + "] iCountInt [" + iCountInt + "] "
                        + "iCountDbl [" + iCountDbl + "] "
                        + "iCountStr [" + iCountStr + "] time ["
                        + com.indraweb.util.UtilProfiling.elapsedTimeMillis(lStart) + "]");
            }
        }


    }

    public static void testXMLSeralize(Connection dbc) throws Throwable {
        String sURLtestXMLSeralize = "http://207.103.213.104:8080/itsharvester/ts?"
                + "fn=tsthesaurus.TSThesaurusNodeReport"
                + //"&BEANXML=TRUE" +
                "&BEANXML=true"
                + "&CORPUSID=5"
                + //"&CORPUSID=100006" +
                "&THESAURUSID=1"
                + "&SKEY=-220364972"
                + "";

        com.indraweb.util.Log.log("START TEXT XML1\r\n");
        executeAPI(sURLtestXMLSeralize, dbc);
        //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
        //executeAPI (sURLClassify,  dbc );
        com.indraweb.util.Log.log("STOP TEXT XML1\r\n");
        System.exit(0);


    }

    private static void testTSCreateNodeSigsFromDoc(Connection dbc) throws Throwable {
        try {

            String sURL_TSCreateNodeSigsFromDoc = "http://207.103.213.104:8080/itsharvester/ts?"
                    + "fn=tsnode.TSCreateSigsFromDoc"
                    + //"&DocIDSource=41293" +
                    //"&DocIDSource=702471" +
                    "&DocIDSource=41293" + // testTSCLassifyGUIExplainStyle
                    "&ParentID=97947" + // testTSCLassifyGUIExplainStyle
                    "&corpusid=11" + // testTSCLassifyGUIExplainStyle
                    "&post=true"
                    + "&SKEY=-220364972"
                    + //"&docsummary=passed4 in docsummary test" +
                    //"&doctitle=passe  d in doctitle test" +
                    //2003 09 22 "&nodetoscoreexplain=209038" +
                    // "&explainscores=true" +
                    "";

            //tsclassify.TSClassifyDoc&documentid=41293&post=false&SKEY=478186727

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            //Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            //com.indraweb.util.Log.log ("START CLASSIFY 1\r\n");
            executeAPI(sURL_TSCreateNodeSigsFromDoc, dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            clsUtils.waitForKeyboardInput("at hkwait 10 ");
            //com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            System.exit(0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    public static void testTSAddNode(Hashtable props, Connection dbc) throws Throwable {
        try {

            StringBuffer sbProps = new StringBuffer();
            Enumeration enumPropKeys = props.keys();
            String sAmpersand = "";
            while (enumPropKeys.hasMoreElements()) {
                String sKey = (String) enumPropKeys.nextElement();
                sbProps.append(sAmpersand + sKey + "=" + props.get(sKey));
                sAmpersand = "&";
            }


            String sURLTSAddNode = "http://207.103.213.104:8080/itsharvester/ts?"
                    + "fn=tsnode.TSAddNode&" + sbProps.toString()
                    + "&SKEY=-220364972"
                    + "";

            //tsclassify.TSClassifyDoc&documentid=41293&post=false&SKEY=478186727

            // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
            // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
            //Data_WordsToNodes.verifyCacheUpToDate (dbc , true);
            //com.indraweb.util.Log.log ("START CLASSIFY 1\r\n");
            System.out.println("sURLTSAddNode [" + sURLTSAddNode + "]");
            executeAPI(sURLTSAddNode, dbc);
            //com.indraweb.util.Log.log("START CLASSIFY 2\r\n");
            //executeAPI (sURLClassify,  dbc );
            //clsUtils.waitForKeyboardInput ("at hkwait 3 ");
            //com.indraweb.util.Log.log ("MAINTEST DONE1 \r\n");
            //System.exit (0);

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void jcharttest() {
    }

    private static void compareCorpusSigsPostInstallation(Connection dbc) throws Exception {
        String sSQLnew = "Select nodeid from node where corpusid = 100233";
        Vector vNodesNew = JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLnew, dbc);
        String sSQLold = "Select nodeid from node where corpusid = 100277";
        Vector vNodesOld = JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLold, dbc);

        for (int i = 0; i < vNodesNew.size(); i++) {
            //if ( i == 83)
            {
                String sNodeIDNew = vNodesNew.elementAt(i).toString();
                String sNodeIDOld = vNodesOld.elementAt(i).toString();

                compareSigData(i, sNodeIDNew, sNodeIDOld, vNodesNew, vNodesOld, dbc);
                compareNodeData(sNodeIDNew, sNodeIDOld, dbc);

            }
        }


    }

    private static void compareNodeData(String sNodeIDNew, String sNodeIDOld, Connection dbc)
            throws Exception {

        // nodepath
        {
            String sSQLNodePathNew = "select path from nodepath where nodeid = " + sNodeIDNew;
            String sSQLNodePathOld = "select path from nodepath where nodeid = " + sNodeIDOld;
            String sNodePathNew = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodePathNew, dbc);
            String sNodePathOld = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodePathOld, dbc);

            if (!sNodePathNew.equals(sNodePathNew)) {
                System.out.println("PATH MISMATCH : new path [" + sNodePathNew + "]\r\n"
                        + "old path [" + sNodePathOld + "]");
            }

        }

        // node title
        {
            String sSQLNodeTitleNew = "select nodetitle from node where nodeid = " + sNodeIDNew;
            String sSQLNodeTitleOld = "select nodetitle from node where nodeid = " + sNodeIDOld;
            String sNodeTitleNew = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodeTitleNew, dbc);
            String sNodeTitleOld = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodeTitleOld, dbc);

            if (!sNodeTitleNew.equals(sNodeTitleOld)) {
                System.out.println("TITLE MISMATCH : new [" + sNodeTitleNew + "]\r\n"
                        + "old [" + sNodeTitleOld + "]");
            }
        }

        // node size
        {
            String sSQLNodeSizeNew = "select nodesize from node where nodeid = " + sNodeIDNew;
            String sSQLNodeSizeOld = "select nodesize from node where nodeid = " + sNodeIDOld;
            String sNodeSizeNew = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodeSizeNew, dbc);
            String sNodeSizeOld = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNodeSizeOld, dbc);

            if (!sNodeSizeNew.equals(sNodeSizeOld)) {
                System.out.println("NODESIZE MISMATCH : new [" + sNodeSizeNew + "]\r\n"
                        + "old [" + sNodeSizeOld + "]");
            }
        }

        // node index WIP
        {
            String sSQLNODEINDEXWITHINPARENTNew = "select NODEINDEXWITHINPARENT from node where nodeid = " + sNodeIDNew;
            String sSQLNODEINDEXWITHINPARENTOld = "select NODEINDEXWITHINPARENT from node where nodeid = " + sNodeIDOld;
            String sNODEINDEXWITHINPARENTNew = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNODEINDEXWITHINPARENTNew, dbc);
            String sNODEINDEXWITHINPARENTOld = JDBCIndra_Connection.executeQueryAgainstDBStr(sSQLNODEINDEXWITHINPARENTOld, dbc);

            if (!sNODEINDEXWITHINPARENTNew.equals(sNODEINDEXWITHINPARENTOld)) {
                System.out.println("NODEINDEXWITHINPARENT MISMATCH : sNODEINDEXWITHINPARENTNew [" + sNODEINDEXWITHINPARENTNew + "]\r\n"
                        + "sNODEINDEXWITHINPARENTOld [" + sNODEINDEXWITHINPARENTOld + "]");
            }

        }

        // node depth
        {
            String sdepthfromrootNew = "select depthfromroot from node where nodeid = " + sNodeIDNew;
            String sdepthfromrootOld = "select depthfromroot from node where nodeid = " + sNodeIDOld;
            String s1depthfromrootNew = JDBCIndra_Connection.executeQueryAgainstDBStr(sdepthfromrootNew, dbc);
            String s1depthfromrootOld = JDBCIndra_Connection.executeQueryAgainstDBStr(sdepthfromrootOld, dbc);

            if (!s1depthfromrootNew.equals(s1depthfromrootOld)) {
                System.out.println("s1depthfromroot MISMATCH : s1depthfromrootNew[" + s1depthfromrootNew + "]\r\n"
                        + "s1depthfromrootOld[" + s1depthfromrootOld + "]");
            }

        }
    }

    private static void compareSigData(int i, String sNodeIDNew, String sNodeIDOld,
            Vector vNodesNew, Vector vNodesOld, Connection dbc)
            throws Exception {
        String sSQLSigSetNew = "select signatureword from signature where nodeid = " + sNodeIDNew;
        String sSQLSigSetOld = "select signatureword from signature where nodeid = " + sNodeIDOld;
        Vector vStrSigSetNew = JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLSigSetNew, dbc);
        Vector vStrSigSetOld = JDBCIndra_Connection.executeQueryAgainstDBVector(sSQLSigSetOld, dbc);

        System.out.println("testing index [" + (i + 1) + "] of [" + vNodesNew.size() + "] sNodeIDNew ["
                + sNodeIDNew + "] sNodeIDOld [" + sNodeIDOld + "] vStrSigSetNew.size() [" + vStrSigSetNew.size() + "]");
        if (vStrSigSetNew.size() != vStrSigSetOld.size()) {
            System.out.println("new ID [" + sNodeIDNew + "]\r\n"
                    + "old ID [" + sNodeIDOld + "]");
            System.out.println(
                    "vStrSigSetNew.size()  [" + vStrSigSetNew.size() + "] "
                    + "vStrSigSetOld.size()  [" + vStrSigSetOld.size() + "] ");
            System.out.println("new style sigs");
            System.out.println(vToStr(vStrSigSetNew, "\r\n"));
            System.out.println("old style sigs");
            System.out.println(vToStr(vStrSigSetOld, "\r\n"));

        }
        HashSet hsNew = new HashSet(vStrSigSetNew);
        HashSet hsOld = new HashSet(vStrSigSetOld);

        hsNew.removeAll(hsOld);
        if (hsNew.size() > 0) {
            System.out.println("ERROR : hsNew.size() > 0");
        }

        hsNew = new HashSet(vStrSigSetNew);
        hsOld = new HashSet(vStrSigSetOld);

        hsOld.removeAll(hsNew);
        if (hsOld.size() > 0) {
            System.out.println("hsOld.size() > 0");
        }


    }

    public static String vToStr(Vector v, String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < v.size(); i++) {
            sb.append(i + "." + v.elementAt(i).toString() + s);
        }
        return sb.toString();

    }

    private static void testDynamicClassify(Connection dbc) throws Throwable {
        /*
         * IndraXMLargs 0. dcbuildthesaurus:false IndraXMLargs 1. dcnodetitle:Jimmy Carter IndraXMLargs 2. explainscores:false IndraXMLargs 3.
         * dynamicnodesigs:3,national|||5,government|||3,georgia|||5,jimmy|||5,jimmy
         * carter|||4,inflation|||3,administration|||3,continuing|||3,soviet|||3,energy|||3,plains|||18,carter|||3,jobs|||6,presidents IndraXMLargs 4. dcnumtitlewords:1 IndraXMLargs 5. dcnodesize:315
         * IndraXMLargs 6. docidsource:1041087 IndraXMLargs 7. dweight:10 IndraXMLargs 8. fn:tsclassify.TSClassifyDoc IndraXMLargs 9. post:false IndraXMLargs 10. skey:-1362801853 IndraXMLargs 11.
         * corpora:100121 IndraXMLargs 12. sweight:10
         */
        String sURLClassify = "http://207.103.213.104:8080/itsharvester/ts?"
                + "fn=tsclassify.TSClassifyDoc"
                + "&dcbuildthesaurus=false"
                + "&dcnodetitle=Jimmy Carter"
                + "&explainscores=false"
                + "&dynamicnodesigs=3,national|||5,government|||3,georgia|||5,jimmy|||5,jimmy carter|||4,inflation|||3,administration|||3,continuing|||3,soviet|||3,energy|||3,plains|||18,carter|||3,jobs|||6,presidents"
                + "&dcnumtitlewords=1"
                + "&dcnodesize=315"
                + "&docidsource=496480"
                + "&dweight=10"
                + "&fn=tsclassify.TSClassifyDoc"
                + "&post=false"
                + "&skey=-1362801853"
                + "&corpora=100121"
                + "&sweight=10" + "";

        //tsclassify.TSClassifyDoc&documen`tid=41293&post=false&SKEY=478186727

        // com.indraweb.execution.Session.cfg.setProp ("AffinityNodeTitlesOnly", "false");
        // boolean bAffinityNodeTitlesOnly = Session.cfg.getPropBool("AffinityNodeTitlesOnly");
        //Data_WordsToNodes.refreshCache (dbc , "5" , false);
        api.Log.Log("START testDynamicClassify 1\r\n");
        executeAPI(sURLClassify, dbc);
        api.Log.Log("END testDynamicClassify 1\r\n");

    }

    private static void insertClobStringsForNodeData(Connection dbc) throws Exception {

        //  12320414

        //long lNodeID = 12320414;
        //String sNewText = "001001Have 001001you 001001ever 001001tried 001001to 001001impress 001001someone 001001by 001001picking 001001one 001001of 001001those 001001Master 001001combination 001001locks 001001and 001001failed? 001001The 001001Master 001001lock 001001company 001001made 001001their 001001older 001001combination 001001locks 001001with 001001a 001001protection 001001scheme. 001001If 001001you 001001pull 001001the 001001handle 001001too 001001hard 001001 001001the 001001knob 001001will 001001not 001001turn. 001001That 001001was 001001their 001001biggest 001001mistake.";
        //processClobStringOneNode ( sNewText, lNodeID, dbc );

        long lNodeID2 = 12320415;
        //String sNewText2 = "001002While 001002the 001002basic 001002themes 001002of 001002lockpicking 001002and 001002uninvited 001002entry 001002have 001002not001002changed 001002much 001002in 001002the 001002last 001002few 001002years 001002some 001002modern 001002devices 001002and001002techniques 001002have 001002appeared 001002on 001002the 001002scene.001002Automobiles:001002Many 001002older 001002automobiles 001002can 001002still 001002be 001002opened 001002with 001002a 001002Slim 001002Jim 001002type 001002of001002opener 001002(these 001002and 001002other 001002auto 001002locksmithing 001002techniques 001002are 001002covered001002fully 001002in 001002the 001002book 001002In 001002the 001002Still 001002of 001002the 001002Night 001002by 001002John 001002RussellIII); 001002however 001002many 001002car 001002manufacturers 001002have 001002built 001002cases 001002over 001002the001002lock 001002mechanism 001002or 001002have 001002moved 001002the 001002lock 001002mechanism 001002so 001002the 001002Slim 001002Jim001002will 001002not 001002work";
        String sNewText2 = "001002While dog cat rat 001002the 001002basic 001002themes 001002of dog cat rat 001002lockpicking 001002and 001002uninvited dog cat rat 001002entry 001002have 001002not001002changed 001002much 001002in 001002the 001002last 001002few 001002years 001002some 001002modern 001002devices 001002and001002techniques 001002have 001002appeared 001002on 001002the 001002scene.001002Automobiles:001002Many 001002older 001002automobiles 001002can 001002still 001002be 001002opened 001002with 001002a 001002Slim 001002modern 001002devices 001002Jim 001002type 001002of001002opener 001002(these 001002and 001002other 001002auto 001002locksmithing 001002techniques 001002are 001002covered001002fully 001002in 001002the 001002book 001002In 001002modern 001002devices 001002the 001002Still 001002of 001002the 001002Night 001002by 001002John 001002RussellIII); 001002however 001002many 001002car 001002manufacturers 001002have 001002built 001002cases 001002over 001002the001002lock 001002mechanism 001002or 001002have 001002moved 001002the 001002lock 001002mechanism 001002so 001002the 001002Slim 001002Jim001002will 001002not 001002work 001002modern 001002devices";
        processClobStringOneNode(sNewText2, lNodeID2, dbc);

//        long lNodeID3 = 12320416;
//        String sNewText3 = "001002001subwordtheartoflockpicking";
//        processClobStringOneNode ( sNewText3, lNodeID3, dbc );

        //long lNodeID4 = 12320412;
        //String sNewText4 = "The 001Anarchists 001Cookbook";
        //String sNewText4 = "";
        //processClobStringOneNode ( sNewText4, lNodeID4, dbc );

    }

    private static void processClobStringOneNode(String sNewText, long lNodeID, Connection dbc) throws Exception {
        //sNewText = "hey hk";
        // delete old recorf\d
        String sClobVerifyPre = UtilClob.getClobStringFromNodeData(lNodeID, dbc);
        api.Log.Log("sClobVerifyPre [" + sClobVerifyPre.length() + "][" + sClobVerifyPre + "]");
        String sSQLDelete = "delete from nodedata where nodeid = " + lNodeID;
        JDBCIndra_Connection.executeUpdateToDB(sSQLDelete, dbc, true, -1);

        // insert new
        //CorpusFileDescriptor.insertClobToNodeData ( sNewText, dbc, lNodeID, null);

        // verify
        String sClobVerifyInsert = UtilClob.getClobStringFromNodeData(lNodeID, dbc);
        //api.Log.Log ("sClobVerifyInsert [" + sClobVerifyInsert.length() + "]["+ sClobVerifyInsert  + "]" );
        if (!sNewText.equals(sClobVerifyInsert)) {
            api.Log.Log("test fail");
        } else {
            api.Log.Log("final node [" + lNodeID + "] sClobTest [" + sClobVerifyInsert.length() + "][" + sClobVerifyInsert + "]");
        }


    }

    private static void testPhraseCollector() {
        Hashtable htResults = new Hashtable();
        String sTest = null;
        // ===================

        //sTest = "symphony circle cat dog symphony circle cat dog symphony circle cat dog";
        sTest = "001symphony 001circle 001cat 001dog 001symphony 001circle 001cat 001dog 001symphony 001circle 001cat 001dog ";
        api.Log.Log("sTest [" + sTest + "]");
        com.indraweb.ir.PhraseExtractor.buildPhrases(sTest.toLowerCase(),
                htResults,
                Session.cfg.getPropInt("PhraseLenMin"),
                Session.cfg.getPropInt("PhraseRepeatMin"),
                ParseParms.getDelimitersAll_Default(),
                Session.cfg.getPropInt("PhraseLenMax"),
                1, // the digit
                true);
        api.Log.Log("phrases [" + UtilSets.htToStr(htResults) + "]");

        api.Log.Log("done");

    }

    private static void testTSCLassifyGUIStyle_forQuickDebug20040707(String sDocIDSource, String sCorpora,
            String sCallerNodeSetOrNull, Connection dbc,
            int iDweight, int iSweight,
            String sNodeToScoreExplainOrNull_) throws Throwable {
        try {
            long lStart = System.currentTimeMillis();
            //api.Log.Log ("DELETING FILE /Program Files/ITS/CorpusSource/SigWordCountsFile_100011_nonStem.txt");
            //UtilFile.deleteFile("/Program Files/ITS/CorpusSource/SigWordCountsFile_100011_nonStem.txt");
            // UtilFile.deleteFile("/Program Files/ITS/CorpusSource/SigWordCountsFile_100011_Stem.txt");


//            String sURLClassify = "http://207.103.213.104:8080/ itsharvester/ts?" +
//                    "fn=tsclassify.TSClassifyDoc&explainscores=false&"+
//                    "doctitle=Screening for cystic fibrosis carrier state.&"+
//                    "docidsource=496620&dweight=3&post=false&skey=1420231769&corpora=2200040&sweight=2&NumParentTitleTermsAsSigs=4";


//            String sURLClassify = "http://localhost/itsapi/ts?"+
//                "fn=tsclassify.TSClassifyDoc&"+
//                "explainscores=false&"+
//                "doctitle=Screening for cystic fibrosis carrier state.&"+
//                "docidsource=500044&"+
//                "dweight=2&"+
//                "sweight=3&"+
//                "numparenttitletermsassigs=3&"+
//                "NumBytesToTake=21001&"+
//                "includenumbers=truex&"+
//                "post=false&"+
//                "skey=-1953731738&"+
//                "corpora=100011";
            String sFile = "MideastTravelWarning.html";
            String sURLClassify = "http://localhost/itsapi/ts?fn=tsclassify.TSClassifyDoc&"
                    + "url=file:///c:/PROGRA~1/ITS/classifyfiles/" + sFile + "&"
                    + "postedfilename=c:/PROGRA~1/ITS/classifyfiles" + sFile + "&"
                    + "explainscores=false&doctitle=None&post=false&skey=-1975543125&"
                    + //"WANTDOCSUMMARY=false&"+
                    //"corpora=5,16,17,18,21,22,100003,100013,100045,200004&content-type=text/plain&"+
                    "corpora=" + sCorpora + "&content-type=text/plain&"
                    + "NumParentTitleTermsAsSigs=0&"
                    + "WANTDOCSUMMARY=false&"
                    + "import=VenezuelaOilOfficialPredictsStability.txt";
//            String sURLClassify = "http://localhost/itsapi/ts?fn=tsclassify.TSClassifyDoc"+
//                "&explainscores=false"+
//                "&doctitle=Screening for cystic fibrosis carrier state."+
//                "&docidsource=496620"+
//                "&dweight=1"+
//                //"&numparenttitletermsassigs=1"+
//                "&post=false"+
//                "&includenumbers=false"+
//                "&skey=-2057470890"+
//                //"&corpora=2200040"+
//                "&corpora=300012"+
//                "&sweight=0";

            // test a bad doc ? error
            //String sURLClassify = "http://localhost/itsapi/ts?fn=tsclassify.TSClassifyDoc&explainscores=false&doctitle=Fractions of calcium in the plant-soil system affected by the application of olive oil wastewater.&docidsource=1991&dweight=1&numparenttitletermsassigs=3&post=false&skey=-364655523&corpora=2200040&sweight=0";
            //String sURLClassify = "http://localhost/itsapi/ts?fn=tsclassify.TSClassifyDoc&explainscores=true&nodeset=1402665&doctitle=Fractions of calcium in the plant-soil system affected by the application of olive oil wastewater.&batch=false&nodetoscoreexplain=1402666&docidsource=1991&dweight=1&numparenttitletermsassigs=3&post=false&skey=342267593&corpora=100011&sweight=0";
            String sURLClassify_explainForGUI = "http://localhost/itsapi/ts?fn=tsclassify.TSClassifyDoc&explainscores=true&nodeset=1402665&doctitle=Screening for cystic fibrosis carrier state.&batch=false&nodetoscoreexplain=1402665&docidsource=500044&dweight=1&numparenttitletermsassigs=3&post=false&skey=-647850549&corpora=100011&sweight=0";
            api.Log.Log("deleting /Program Files/ITS/CorpusSource/SigWordCountsFile_2200040_stem.txt");
            api.Log.Log("deleting /Program Files/ITS/CorpusSource/SigWordCountsFile_2200040_stem.txt");
            api.Log.Log("deleting /Program Files/ITS/CorpusSource/SigWordCountsFile_2200040_stem.txt");
            api.Log.Log("deleting /Program Files/ITS/CorpusSource/SigWordCountsFile_2200040_stem.txt");
            UtilFile.deleteFile("c:/Program Files/ITS/CorpusSource/SigWordCountsFile_2200040_stem.txt");
            //UtilFile.deleteFile("c:/Program Files/ITS/CorpusSource/SigWordCountsFile_2200040_nonstem.txt");
            //UtilFile.deleteFile("c:/Program Files/ITS/CorpusSource/SigWordCountsFile_300012_nonstem.txt");
            //UtilFile.deleteFile("c:/Program Files/ITS/CorpusSource/SigWordCountsFile_100011_nonstem.txt");
            String sTSSignatureExpand = "http://localhost/itsapi/ts?"
                    + "fn=tssignature.TSSignatureExpand&"
                    + "skey=-1997217620&"
                    + "corpusid=2200040&"
                    + "nodeid=100000034&"
                    + "sigs=1,reproductive|||1,fibrosis|||2,cystic&"
                    + "numparenttitletermsassigs=3";


//// tssignatireexpand            sURLClassify = "http://207.103.213.104:8080/ itsharvester/ts?" +
//    sURLClassify = "http://207.103.213.104:8080/ itsharvester/ts?" +
//            "fn=tssignature.TSSignatureExpand&"+
//            "skey=35916856&corpusid=2200040&nodeid=100000034&"+
//            "NumParentTitleTermsAsSigs=6&"+
//                    "sigs=4,fibrosis|||4,cystic|||4,"+
//                    "";

            //String sURLTSInstallCorpus = "http://localhost/servlet/ts?fn=tscorpus.TSInstallCorpus&skey=-1374094013&corpusid=2200040&numparenttitletermassigs=1&phraselenmax=4&includenumbers=false&wordfreqmaxforsig=75&phraselenmin=2";
            String sURLTSInstallCorpus = "http://localhost/itsapi/ts?fn=tscorpus.TSInstallCorpus&skey=-603546757&corpusid=2200040&numparenttitletermassigs=3&phraselenmax=4&includenumbers=true&wordfreqmaxforsig=75&phraselenmin=2";
//            String sURLTSInstallCorpus = "http://localhost/servlet/ts?"+
//                    "fn=tscorpus.TSInstallCorpus"+
//                    "&skey=-1374094013"+
//                    "&corpusid=2200040&"+
//                    "numparenttitletermassigs=1"+
//                    "&phraselenmax=4"+
//                    "&includenumbers=true"+
//                    "&wordfreqmaxforsig=75"+
//                    "&phraselenmin=2";


            String sURL_TSCreateSigsFromDocnodeid = "http://localhost/itsapi/ts?fn=tsnode.TSCreateSigsFromDoc&nodeid=100000034&url=file:///c:\\PROGRA~1\\ITS/classifyfiles/t.t&postedfilename=c:\\PROGRA~1\\ITS/classifyfiles/t.t&phraselenmax=4&wordfreqmaxforsig=75&includenumbers=true&post=true&skey=-1799153815&content-type=text/plain&import=t.t&phraselenmin=2";
            //Data_WordsToNodes.refreshCache (dbc , "5" , false);
            api.Log.Log("START test \r\n");
            //executeAPI ( sTSSignatureExpand, dbc);
            //executeAPI ( sURLClassify, dbc);
            //executeAPI ( sURL_TSCreateSigsFromDocnodeid, dbc);

            //executeAPI ( sURLClassify, dbc);

            Profiler.clear();
            //iLoopCounter = 2;
            lTimeCounter = System.currentTimeMillis();
            System.out.println("\r\n");
            api.Log.Log("lrb pre execute api 2 classify " + com.indraweb.util.UtilProfiling.elapsedTimeMillis(MainTest.lTimeCounter));
            long ltime = System.currentTimeMillis();
            int ix = 1;

            int iLoopCount = 1;
            while (ix > 0 && ix <= iLoopCount) {
                //                timetest1();
                //                timetest2();

                api.Log.Log("START EXECUTE API TEST LOOP [" + (ix) + "] of iLoopCount   [" + iLoopCount + "]");
                //System.out.println("run # [" + ix + "]" );
                //UtilFile.addLineToFile("c:/temp/temp.txt", new java.util.Date() + " run # [" + ix + "]\r\n" );
                executeAPI(sURLClassify, dbc);
                ix++;
                api.Log.Log("DONE profile test [" + ix + "] in " + UtilProfiling.elapsedTimeMillis(ltime) + "]\r\n");
            }

            // Profiler.printReport();
            //executeAPI ( sURLClassify_explainForGUI, dbc); // 2004 07 25
            //executeAPI ( sURLTSInstallCorpus , dbc); // 2004 07 26

        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in classify test nonfatal", e);
        }

    } // private static void testTSCLassifyAPICall

    private static void fileConversion() throws Exception {
        String[] sArrFilesIn = {"E:/Content/classify test files/ap newswire clippings/ArmyDelaysNerveAgentDestruction",
            "E:/Content/classify test files/ap newswire clippings/CalifWestNileCaseJump32Percent",
            "E:/Content/classify test files/ap newswire clippings/DamagesinPipelineLeak",
            "E:/Content/classify test files/ap newswire clippings/EngineeredMarathonMice",
            "E:/Content/classify test files/ap newswire clippings/HomelandSecurityAnnouncesCargoRules",
            "E:/Content/classify test files/ap newswire clippings/IraqIntelPrisonAbuse",
            "E:/Content/classify test files/ap newswire clippings/JamaicatoProvideFreeInternetAccess",
            "E:/Content/classify test files/ap newswire clippings/JusticeDeptInternetCrime",
            "E:/Content/classify test files/ap newswire clippings/LethalGovSecrets",
            "E:/Content/classify test files/ap newswire clippings/MideastTravelWarning",
            "E:/Content/classify test files/ap newswire clippings/PalShotGaza",
            "E:/Content/classify test files/ap newswire clippings/RidgeConventionSecurity",
            "E:/Content/classify test files/ap newswire clippings/RussiaPlaneTerror",
            "E:/Content/classify test files/ap newswire clippings/StudytoExamineEffectsofShipWaste",
            "E:/Content/classify test files/ap newswire clippings/TerrorSuspectsAppearinBritishCourt",
            "E:/Content/classify test files/ap newswire clippings/ThatcherSonCriminal",
            "E:/Content/classify test files/ap newswire clippings/ThreeIndictedinRaisingFundsforHamas",
            "E:/Content/classify test files/ap newswire clippings/UNDeadlineNearsforDisarminginSudan",
            "E:/Content/classify test files/ap newswire clippings/USWaryPublicHealth",
            "E:/Content/classify test files/ap newswire clippings/VenezuelaOilOfficialPredictsStability"};

        for (int i = 0; i < sArrFilesIn.length; i++) {
            String sFileTxt = sArrFilesIn[i] + ".txt";
            String sFileOut = sArrFilesIn[i] + ".html";

            String sFileIn = UtilFile.getFileAsString(sFileTxt);
            FileOutputStream fos = new FileOutputStream(sFileOut);
            fos.write("<HTML>\r\n<BODY>".getBytes());
            fos.write(sFileIn.getBytes());
            fos.write("</BODY>\r\n</HTML>".getBytes());
            fos.close();

        }


    }

    private static void timetest1() {
        for (int i = 1; i < 1000000; i++) {
            if (i % 100000000 == 0) {
                System.out.println("i [" + i + "]");
            }
        }

    }

    private static void timetest2() {
        for (int i = 1; i < 2000000; i++) {
            if (i % 100000000 == 0) {
                System.out.println("i [" + i + "]");
            }
        }

    }

    private static void listFileWords(Connection dbc) throws Throwable {
        //UrlHtmlAttributes uh = new UrlHtmlAttributes ( s);
        UrlHtmlAttributes uh = new UrlHtmlAttributes("file:///C:/temp/temp/temp/abslhoey.html");

        long ltgetATopicFromMinInputs_start = System.currentTimeMillis();

        Topic t = Topic.getATopicFromMinInputs("file:///C:/temp/temp/temp/abslhoey.html",
                // *************************************
                true,// boolean bThisTopicIsForSigGen_DiffScoring_and_FillInTitlesToDB_ ,
                true, //boolean bWantStringBuffForAllTextInOne_ ,
                -1, // int iNumTextBytesToTakeFromDoc_ ,
                dbc,
                true, //boolean bWantPhrases ,
                true, //boolean bTakeFirstLineAsTitleIfNoTitleTag,
                false, // boolean bAddIndexingTextFromTitle,
                true, // boolean bTakeTitleFromStream,
                ParseParms.getDelimitersAll_Default(),
                null // word freq
                );

        Enumeration ewords = t.htWordsDocOnly_noDescendants.keys();
        while (ewords.hasMoreElements()) {
            String sKey = (String) ewords.nextElement();
            int iCount = ((Integer) t.htWordsDocOnly_noDescendants.get(sKey)).intValue();
            if (iCount > 3) {
                api.Log.Log("word [" + sKey + "] count	\t" + iCount);
            }
        }
        api.Log.Log("done");


    }

    private static void deleteSigsinCorpus(int iCorpusid, Connection dbc_) throws Exception {
        String sSQL = "	select node.nodeid from node "
                + " where node.corpusid =  " + iCorpusid;
        //api.Log.Log ("sSQL [" + sSQL + "]" ) ;
        Statement stmt = null;
        ResultSet rs = null;
        Vector vNodes = new Vector();
        long lSeed = 1;
        stmt = dbc_.createStatement();
        rs = stmt.executeQuery(sSQL);

        int iLoop = 0;
        while (rs.next()) {
            iLoop++;
            //Vector vDBResult = JDBCIndra_Connection.executeDBQueryVecOfVecsReturned (sSQL , dbc_ , -1);
            //Vector v3ParentID =(Vector) vDBResult.elementAt(3);

            int iNodeID = rs.getInt(1);
            vNodes.addElement(new Integer(iNodeID));
        }
        rs.close();
        stmt.close();

        for (int i = 0; i < vNodes.size(); i++) {
            String sSQLDelSigs = "delete from signature where nodeid = " + vNodes.elementAt(i);
            // api.Log.Log ("sSQLDelSigs [" + sSQLDelSigs  + "]" );
            JDBCIndra_Connection.executeUpdateToDB(sSQLDelSigs, dbc_, true, false, -1);

        }
    }
}
