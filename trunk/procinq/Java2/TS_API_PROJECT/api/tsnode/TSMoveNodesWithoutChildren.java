package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import com.iw.db.ConnectionFactory;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * move nodes(level 1) without children to one node
 *
 * @author andres
 */
public class TSMoveNodesWithoutChildren {

    private static final Logger log = Logger.getLogger(
            TSMoveNodesWithoutChildren.class);
    private static final int BATCH_UPDATER = 100;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        log.debug("TSMoveNodesWithoutChildren");
        /*
         * obtengo mi propia coneccion.
         * Dad al César lo que es del César, y a Andres lo que es de Andres
         */
        Connection con = null;
        try {
            con = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            NodeUtils nu = new NodeUtils(con);
            //validate AUTHORIZED
            String sKey = (String) props.get("SKEY", true);
            //get nodeid to start with the process
            String nodeid = (String) props.get("nodeid");
            String corpusId = (String) props.get("corpusid");
            String parentId = (String) props.get("parentid");

            if (nodeid == null || corpusId == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nodeid.isEmpty() || corpusId.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            List<String> nodes = null;
            if (parentId != null) {
                if (parentId.equals("-1")) {
                    out.println("<RESULT>Why do you select rootNodeId?</RESULT>");
                } else {
                    nodes = getNodesWithoutChildren(con, nodeid, parentId, corpusId);
                    updateParentId(con, nodes, nodeid);
                    int countNodes = nodes == null ? 0 : nodes.size();
                    out.println("<RESULT>Finished ok!, Moved " + countNodes + " Nodes</RESULT>");
                }
            } else {
                out.println("<RESULT>Error, ParentId null</RESULT>");
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }
    
    /**
     * get nodes without children from any node
     *
     * @param con
     * @param parentId
     * @param corpusId
     * @return
     */
    private static List<String> getNodesWithoutChildren(
            Connection con,
            String excludeId,
            String parentId,
            String corpusId) {

        String sql = "select nodeid from node "
                + "where corpusid = ? and parentid = ? and nodeid != ?"
                + "and nodeid not in (select parentid from node "
                + "where corpusid = ? )";

        List<String> listReturn = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, Long.parseLong(corpusId));
            pstmt.setLong(2, Long.parseLong(parentId));
            pstmt.setLong(3, Long.parseLong(excludeId));
            pstmt.setLong(4, Long.parseLong(corpusId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listReturn.add(rs.getString("nodeid"));
            }
        } catch (SQLException | NumberFormatException e) {
            log.error("Error getNodesWithoutChildren(... ", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException ex) {
                log.error("Error closing resources", ex);
            }
        }
        return listReturn;
    }

    /**
     *
     * @param con
     * @param nodes
     * @param parentId
     * @throws Exception
     */
    private static void updateParentId(Connection con,
            List<String> nodes,
            String parentId) throws Exception {

        if (nodes == null || nodes.isEmpty()) {
            return;
        }

        String sql = "UPDATE SBOOKS.NODE SET PARENTID = ? WHERE NODEID = ?";
        PreparedStatement pstmt = null;
        try {
            con.setAutoCommit(false);
            pstmt = con.prepareStatement(sql);
            int cont = 0;
            for (String nodeId : nodes) {
                cont++;
                pstmt.setLong(1, Long.parseLong(parentId));
                pstmt.setInt(2, Integer.parseInt(nodeId));
                pstmt.addBatch();
                if (cont == BATCH_UPDATER) {
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            if (cont != 0) {
                pstmt.executeBatch();
                pstmt.clearBatch();
            }
            con.commit();
        } catch (SQLException | NumberFormatException e) {
            log.error("updateParentId, Error updating nodes", e);
            try {
                con.rollback();
            } catch (SQLException ex) {
                throw ex;
            }
            throw e;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
