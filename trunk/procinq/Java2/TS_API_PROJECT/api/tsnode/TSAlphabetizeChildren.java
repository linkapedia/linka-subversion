package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.indraweb.database.*;
import api.Log;
import com.iw.db.ConnectionFactory;

/**
 * Organize all nodes underneath the given node alphabetically.  This function is case insensitive, and changes
 *  the NodeIndexWithinParent attribute in the node table.  If the "recursive" flag is set, all node descendents
 *  under the given node are alphabetized recursively.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of this node.
 *  @param  Recursive   If true, all node descendents will be organized.  Default is false.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSAlphabetizeChildren&NodeID=2000&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Nodes alphabetized successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAlphabetizeChildren {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sRecursive = (String) props.get("recursive");
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        
        Connection connection = null;
        try {
            connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            // for authorization purposes, need to know the corpus id of this node
            String sSQL = " select CorpusId from Node where NodeID = " + sNodeID;
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);
            rs.next();

            int iCorpusId = rs.getInt(1);
            rs.close();
            stmt.close();

            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            boolean bRecurse = false;
            if (sRecursive != null) {
                bRecurse = true;
            }

            alphabetizeNode(sNodeID, connection, bRecurse);
            out.println("<SUCCESS>Nodes alphabetized successfully.</SUCCESS>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }finally{
            connection.close();
        }
    }

    public static void alphabetizeNode(String sNodeID, Connection dbc, boolean bRecurse)
            throws Exception {
        String sSQL = "select nodeid from node where parentid = " + sNodeID+" order by lower(nodetitle) asc";
        Statement stmt = null;
        ResultSet rs = null;

        Vector vChildrenToAlphabetize = new Vector();

        try {
			stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);
            int i = 0;

			while (rs.next()) {
                i++; sNodeID = rs.getString(1);
                setNodeIndex(sNodeID, i, dbc);
                if (bRecurse) { vChildrenToAlphabetize.add(sNodeID); }
            }
        } catch (Exception e) { throw e; }
        finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }

        if (bRecurse) {
            for (int i = 0; i < vChildrenToAlphabetize.size(); i++)
                alphabetizeNode((String) vChildrenToAlphabetize.elementAt(i), dbc, true);
        }
    }

    public static void setNodeIndex(String sNodeID, int iIndex, Connection dbc)
            throws Exception {
        String sSQL = "update node set nodeindexwithinparent = " + iIndex + ", dateupdated = sysdate where NodeID = "+sNodeID;
        Statement stmt = null;

        try {
			stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
        } catch (Exception e) { throw e; }
        finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
    }
}
