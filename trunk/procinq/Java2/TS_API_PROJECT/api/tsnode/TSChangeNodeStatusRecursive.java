package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.DecimalFormat;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.classification.Data_WordsToNodes;
import api.Log;

/**
 * Change the status of all nodes at this level and below.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of this node.
 *  @param  MinChar The minimum number of characters (threshold) before the node is joined
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSChangeNodeStatusRecursive&NodeID=2000&MinChar=50&SKEY=9919294812

 *	@return	NODEID identifier of the newly created node
 *  \verbatim
 <?xml version="1.0" encoding="UTF-8" ?>
 <TSRESULT>
    <SUCCESS>32 nodes altered.</SUCCESS>
 </TSRESULT>
 \endverbatim
 */
public class TSChangeNodeStatusRecursive {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sStatus = (String) props.get("Status", true);

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
        }

        int iCorpusID = -1;

        // 1) Get the CORPUSID of the new PARENT node
        String sSQL = " select CorpusId from Node where NodeID = " + sNodeID;
        Statement stmt = null; ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            iCorpusID = rs.getInt(1);
            if (!u.IsAdmin(iCorpusID, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } catch (Exception e) {
            api.Log.LogError("error in node status change, sql: " + sSQL, e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        // 0 status = off, 1 status = on, 2 status = locked
        sSQL = "update node set nodestatus = "+sStatus+" where nodeid = "+sNodeID+" or nodeid in "+
               "(select nodeid from node start with nodeid = "+sNodeID+" "+
               " connect by prior nodeid = parentid) and corpusid = "+iCorpusID;

        stmt = null;
        int results = 0;

        try {
            stmt = dbc.createStatement();
            results = stmt.executeUpdate(sSQL);

        } catch (Exception e) {
            api.Log.LogError("error in tschangenodestatusrecursive, sql: " + sSQL, e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        out.println("<SUCCESS>"+results+" topics altered.</SUCCESS>");
    }
}
