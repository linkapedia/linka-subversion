package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 * Retrieve the properties of a given node within the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of the node whose children are being retrieved.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodeProps&NodeID=1087101&SKEY=9919294812

 *	@return	NODE object
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <NODE>
          <NODEID>1087101</NODEID>
          <CORPUSID>300012</CORPUSID>
          <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
          <PARENTID>1087099</PARENTID>
          <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
          <DEPTHFROMROOT>0</DEPTHFROMROOT>
          <DATESCANNED>null</DATESCANNED>
          <DATEUPDATED>null</DATEUPDATED>
          <NODESTATUS>1</NODESTATUS>
          <NODEDESC><![CDATA[ ]]></NODEDESC>
          <NODESIZE>1</NODESIZE>
          <LINKNODEID>1087101</LINKNODEID>
      </NODE>
 </TSRESULT>
  \endverbatim
 */
public class TSGetNodeProps
{
	private static int iCallCounter = 0;
	private static Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

	// TSGetNodeProps (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		synchronized (ICallCounterSemaphore)
			{ iCallCounter++; }
		out.println ("<CALLCOUNT_FN>" + iCallCounter + "</CALLCOUNT_FN>");
		String sNodeID = (String) props.get ("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			if (sNodeID.equals("-1")) {
				throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);	
			}
			
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();

            // security now handled by the node emitter
            //if (!u.IsAuthorized(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			sSQL = Node.getSQL(dbc)+" where NodeId = " + sNodeID;
			stmt = dbc.createStatement();	
			rs = stmt.executeQuery (sSQL);
 
			int loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.   To do this, hold off
				//  on writing out the initial stream <SUBSCRIBERS> until we have found at 
				//  least one result.   -MAP 10/14/01
				loop = loop + 1;
                Node n = new Node(rs);
                n.emitXML(out, u, true);
			}
		
			rs.close();
		    stmt.close();
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No information found for Node ID: "+sNodeID+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
			}

		}
        catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
