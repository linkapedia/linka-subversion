package api.tsnode.images;

public class WikipediaImageNotFoundException extends RuntimeException {

    private final String wikipediaPage;

    public WikipediaImageNotFoundException(String wikipediaPage) {
        super();
        this.wikipediaPage = wikipediaPage;
    }

    public String getWikipediaPage() {
        return wikipediaPage;
    }
}
