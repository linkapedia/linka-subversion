
package api.tsnode.images;

import api.tsnode.images.bean.ImagePage;
import com.linkapedia.image.ImageService;
import java.io.IOException;


public interface SearchImage {
    public ImagePage searchImageByTerm(NodeSearchTerms nodeSearchTerms, ImageService imageService) throws IOException;
}
