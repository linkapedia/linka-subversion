package api.tsnode.images;

import api.tsnode.images.bean.ImagePage;
import com.linkapedia.image.ImageService;
import java.io.IOException;

public class WikipediaSearchImage implements SearchImage {

    private final SearchImageEngineWikipedia wikipediaEngine;

    public WikipediaSearchImage(SearchImageEngineWikipedia wikipediaEngine) {
        this.wikipediaEngine = wikipediaEngine;
    }

    @Override
    public ImagePage searchImageByTerm(NodeSearchTerms nodeSearchTerms, ImageService imageService) throws IOException 
    {
        return wikipediaEngine.search(nodeSearchTerms, imageService);
    }
}
