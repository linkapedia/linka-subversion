package api.tsnode.images;

import api.tsnode.images.bean.ImagePage;
import api.tsnode.images.interfaces.SearchImageEngine;
import com.linkapedia.image.ImageNotFoundException;
import com.linkapedia.image.ImageService;
import com.linkapedia.procinq.server.bingsearch.BingResponseException;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class DefaultSearchImage implements SearchImage {

    private final List<SearchImageEngine> searchImageEngines;
    private final List<ImagePage> imageTaxonomies;

    public DefaultSearchImage(List<SearchImageEngine> searchImageEngines, List<ImagePage> imageTaxonomies) {
        this.searchImageEngines = searchImageEngines;
        this.imageTaxonomies = imageTaxonomies;
    }

    @Override
    public ImagePage searchImageByTerm(NodeSearchTerms nodeSearchTerms, ImageService imageService) throws IOException {
        for (SearchImageEngine engine : this.searchImageEngines) {
            try {
                return engine.search(nodeSearchTerms, imageService);
            } catch (WikipediaImageNotFoundException | ImageNotFoundException | BingResponseException | IOException ex) {

            }
        }
        return getTaxonomyImage();
    }

    private ImagePage getTaxonomyImage() {
        int indexImage = new Random().nextInt(this.imageTaxonomies.size());
        return this.imageTaxonomies.get(indexImage);
    }
}
