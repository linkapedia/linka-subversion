package api.tsnode.images;

public class NodeSearchTerms {

    private final String id;
    private final String searchTerm;
    private final String nodeTitle;

    public NodeSearchTerms(String id, String searchTerm, String nodeTitle) {
        this.id = id;
        this.searchTerm = searchTerm;
        this.nodeTitle = nodeTitle;
    }

    public String getId() {
        return id;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public String getNodeTitle() {
        return nodeTitle;
    }
}
