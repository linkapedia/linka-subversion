package api.tsnode.images.bean;

import java.io.Serializable;

/**
 *
 * @author andres
 */
public class MessageImages implements Serializable {

    private String requestId;
    private String nodeId;
    private String corpusId;
    private int toLevel;
    private boolean withWikipediaImage;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }

    public int getToLevel() {
        return toLevel;
    }

    public void setToLevel(int toLevel) {
        this.toLevel = toLevel;
    }

    public boolean isWithWikipediaImage() {
        return withWikipediaImage;
    }

    public void setWithWikipediaImage(boolean withWikipediaImage) {
        this.withWikipediaImage = withWikipediaImage;
    }
}
