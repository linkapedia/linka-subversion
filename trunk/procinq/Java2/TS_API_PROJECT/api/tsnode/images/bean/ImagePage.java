package api.tsnode.images.bean;

import com.linkapedia.image.Image;

public class ImagePage {

    private final String pageImageUrl;
    private final Image image;

    public ImagePage(String pageImageUrl, Image image) {
        this.pageImageUrl = pageImageUrl;
        this.image = image;
    }

    public String getPageImageUrl() {
        return pageImageUrl;
    }

    public Image getImage() {
        return image;
    }
}
