package api.tsnode.images.interfaces;

import api.tsnode.images.NodeSearchTerms;
import api.tsnode.images.bean.ImagePage;
import com.linkapedia.image.ImageNotFoundException;
import com.linkapedia.image.ImageService;
import java.io.IOException;

public interface SearchImageEngine {

    public ImagePage search(NodeSearchTerms searchTerm, ImageService imageService) throws IOException, ImageNotFoundException;
}
