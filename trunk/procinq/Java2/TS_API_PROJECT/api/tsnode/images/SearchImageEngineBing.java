package api.tsnode.images;

import api.tsnode.images.bean.ImagePage;
import api.tsnode.images.interfaces.SearchImageEngine;
import api.util.ConfServer;
import com.linkapedia.image.Image;
import com.linkapedia.image.ImageNotFoundException;
import com.linkapedia.image.ImageService;
import com.linkapedia.procinq.server.bingsearch.BingResponseException;
import com.linkapedia.procinq.server.utils.StreamUtil;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

public class SearchImageEngineBing implements SearchImageEngine {

    private static final int CONNECTION_TIMEOUT = 30000;
    private static final int READ_TIMEOUT = 30000;
    private static final int BUFFER_LENGHT = 4096;

    @Override
    public ImagePage search(NodeSearchTerms searchTerm, ImageService imageService) throws IOException, ImageNotFoundException {
        JSONArray bingObject;
        bingObject = getBingResult(searchTerm.getSearchTerm());
        List<String> urlsImages = getUrlsFromBingResult(bingObject);
        Image image = imageService.getMostQualityImageFromUrls(urlsImages);
        return new ImagePage(getImageInfoByUrl(bingObject, image.getUrl()), image);
    }

    private JSONArray getBingResult(String searchTerm) throws IOException, BingResponseException {
        URL url = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        String searchTermEncode = URLEncoder.encode(searchTerm, "UTF-8");

        url = buildUrlByTerm(searchTermEncode);
        connection = getConnection(url);
        try {
            connection.connect();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new BingResponseException("Error to Connect: "
                        + url.toString() + "Status Code: " + connection.getResponseCode());
            }
            inputStream = connection.getInputStream();
            byte[] bytesResultBing = StreamUtil.readStream(inputStream, BUFFER_LENGHT);
            JSONObject bingResult = JSONObject.fromObject(
                    new String(bytesResultBing));
            JSONArray bingResults = (JSONArray) bingResult.getJSONObject("d").
                    getJSONArray("results");
            return bingResults;
        } finally {
            connection.disconnect();
        }
    }

    private HttpURLConnection getConnection(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        String userpassword = ConfServer.getValue("bing.api.user")
                + ":" + ConfServer.getValue("bing.api.pass");
        String encodedAuthorization = new String(
                Base64.encodeBase64(userpassword.getBytes()));
        connection.setRequestProperty("Authorization", "Basic "
                + encodedAuthorization);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);
        connection.setReadTimeout(READ_TIMEOUT);
        return connection;
    }

    private List<String> getUrlsFromBingResult(JSONArray imagesInfoBing) {
        List<String> urls = new ArrayList<>();
        String url;
        for (Object imageBing : imagesInfoBing) {
            JSONObject jsonObj = (JSONObject) imageBing;
            url = jsonObj.getString("MediaUrl");
            urls.add(url);
        }
        return urls;
    }

    private String getImageInfoByUrl(JSONArray bingObject, String url) {
        String currentUrl;
        for (Object bingObj : bingObject) {
            JSONObject bingImageInfo = (JSONObject) bingObj;
            currentUrl = bingImageInfo.getString("MediaUrl");
            if (currentUrl.compareTo(url) == 0) {
                return bingImageInfo.getString("SourceUrl");
            }
        }
        throw new IllegalArgumentException("Url not found in bing results");
    }

    private URL buildUrlByTerm(String termSearch) throws MalformedURLException {
        StringBuilder sb = new StringBuilder();
        String url = ConfServer.getValue("bing.api.url");
        String top = ConfServer.getValue("bing.api.top");
        String skip = ConfServer.getValue("bing.api.skip");
        String ImageFilters = ConfServer.getValue("bing.api.ImageFilters");

        String market = ConfServer.getValue("bing.api.market");
        sb.append(url);
        sb.append("?Query=");
        sb.append("'").append(termSearch).append("'");
        sb.append("&$top=");
        sb.append(top);
        sb.append("&$skip=");
        sb.append(skip);
        sb.append("&$format=json");
        sb.append("&ImageFilters=");
        sb.append("'").append(ImageFilters).append("'");
        if (market != null && !market.equals("")) {
            sb.append("&Market=").append("'").append(market).append("'");
        }
        return new URL(sb.toString());
    }
}
