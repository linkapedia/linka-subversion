package api.tsnode.images;

import api.tsnode.images.bean.ImagePage;
import api.util.ConfServer;
import api.util.S3Storage;
import com.iw.db.ConnectionFactory;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.linkapedia.image.FilterImageName;
import com.linkapedia.image.ImageGeometryValidator;
import com.linkapedia.image.ImageService;
import com.linkapedia.image.utils.ImageDownloader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

public class SearchImageWorker implements Runnable {

    private final SearchImage searchImage;
    private final NotifyStorageAWS notifyStorage;
    List<NodeSearchTerms> terms = new ArrayList<>();
    private static final Logger log = Logger.getLogger(SearchImageWorker.class);
    private static int NOTIFY_EACH = 10;

    public SearchImageWorker(SearchImage searchImage, NotifyStorageAWS notifyStorage) {
        this.searchImage = searchImage;
        this.notifyStorage = notifyStorage;
    }

    @Override
    public void run() {
        ImagePage imagePage;
        int index = 0;
        FilterImageName filterImageName = createFilterName();
        ImageGeometryValidator validator = createFilteGeometryValidator();
        ImageDownloader imageDownloader = createImageDownloader();
        ImageService imageService = new ImageService(filterImageName, validator,
                imageDownloader);
        Connection connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        for (NodeSearchTerms nodeSearchTerm : terms) {
            log.debug("SEARCHING NODEID: " + nodeSearchTerm.getId());
            try {
                imagePage = searchImage.searchImageByTerm(nodeSearchTerm, imageService);
                log.debug("URL" + imagePage.getPageImageUrl());
                notifyStorage.save(imagePage, nodeSearchTerm.getId(), connection);
            } catch (Exception ex) {
                log.debug("CAN'T SAVE IMAGE__________________: "
                        + nodeSearchTerm.getId() + " - " + nodeSearchTerm.getSearchTerm() + ": ", ex);
            } finally {
                if (index == NOTIFY_EACH) {
                    notifyStorage.notifyProgress(index);
                    index = 0;
                }
                index++;
            }
        }
        try {
            connection.close();
        } catch (SQLException ex) {
        }
        if (index != 0) {
            notifyStorage.notifyProgress(index);
        }
        S3Storage.finalizeConnection();
    }

    public void addTerm(NodeSearchTerms nodeSearchTerm) {
        terms.add(nodeSearchTerm);
    }

    private static FilterImageName createFilterName() {
        List<String> namesNotAllowed = Arrays.asList(ConfServer.getValue("images.service.filter.nameNotAllowed").split(","));
        FilterImageName filterImageName = new FilterImageName(namesNotAllowed);
        return filterImageName;
    }

    private static ImageGeometryValidator createFilteGeometryValidator() {
        float maxRatioAllowed = Float.parseFloat(ConfServer.getValue("images.service.maxRatioAllowed"));
        int maxWidth = Integer.parseInt(ConfServer.getValue("images.service.maxWidth"));
        int maxHeight = Integer.parseInt(ConfServer.getValue("images.service.maxHeight"));
        int minWidth = Integer.parseInt(ConfServer.getValue("images.service.minWidth"));
        int minHeight = Integer.parseInt(ConfServer.getValue("images.service.minHeight"));

        ImageGeometryValidator validator = new ImageGeometryValidator(
                maxRatioAllowed,
                maxWidth,
                maxHeight,
                minWidth,
                minHeight);

        return validator;
    }

    private static ImageDownloader createImageDownloader() {
        int maxSizeImageMB = ConfServer.getIntValue("images.service.maxSizeImageMB");
        return new ImageDownloader(maxSizeImageMB * 1024 * 1024);
    }
}
