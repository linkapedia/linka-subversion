package api.tsnode.images;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.db.ConnectionFactory;
import com.iw.system.ImageCrawlReportModel;
import com.iw.system.WrapperListImageCrawlReport;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * get the report to the IMAGECRAWL process
 *
 * @author andres
 */
public class TSImageCrawlStatus {
    
    private static final Logger log = Logger.getLogger(TSImageCrawlStatus.class);
    
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {
        log.debug("TSImageCrawlStatus");
        String rownum = (String) props.get("rownum");
        String requestId = (String) props.get("requestid");
        if (rownum == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (rownum.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        List<ImageCrawlReportModel> report = getImageReport(rownum, requestId);
        WrapperListImageCrawlReport modelReport = new WrapperListImageCrawlReport();
        modelReport.setList(report);
        String data = Transformation.getData(modelReport);
        out.println(data);
    }
    
    private static List<ImageCrawlReportModel> getImageReport(String rowNum,
            String requestId) throws Exception {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("getReportWQaReport(.. "
                    + "Error getting connection from oracle");
        }
        List<ImageCrawlReportModel> listToReturn = new ArrayList<>();
        ImageCrawlReportModel imageModel;
        String sql = "";
        if (requestId == null || requestId.isEmpty()) {
            sql = " SELECT * from (SELECT *   FROM ( "
                    + "SELECT page.*, rownum as row_num FROM ( "
                    + "SELECT * from SBOOKS.IMAGECRAWLREPORT order by requestid DESC "
                    + ") page "
                    + ") "
                    + "WHERE row_num <= ?)";
        } else {
            sql = " SELECT * from (SELECT *   FROM ( "
                    + "SELECT page.*, rownum as row_num FROM ( "
                    + "SELECT * from SBOOKS.IMAGECRAWLREPORT"
                    + " where requestid = ? order by requestid DESC "
                    + ") page "
                    + ") "
                    + "WHERE row_num <= ?)";
        }
        PreparedStatement pstmt = null;
        ResultSet result = null;
        
        try {
            pstmt = conn.prepareStatement(sql);
            if (requestId == null || requestId.isEmpty()) {
                pstmt.setInt(1, Integer.parseInt(rowNum));
            } else {
                pstmt.setInt(1, Integer.parseInt(requestId));
                pstmt.setInt(2, Integer.parseInt(rowNum));
            }
            
            result = pstmt.executeQuery();
            while (result.next()) {
                imageModel = new ImageCrawlReportModel();
                imageModel.setRequestId(String.valueOf(result.getLong("REQUESTID")));
                imageModel.setTimestamp(String.valueOf(result.getLong("TIMESTAMP")));
                imageModel.setCorpus(result.getString("CORPUS"));
                imageModel.setNode(result.getString("NODE"));
                imageModel.setRecursively(String.valueOf(result.getString("RECURSIVELY")));
                imageModel.setStatus(result.getString("STATUS"));
                imageModel.setSummary(result.getString("SUMMARY"));
                listToReturn.add(imageModel);
            }
            return listToReturn;
        } catch (Exception e) {
            log.error("Error in the process status in imagecrawl report", e);
            throw new SQLException("Error in the process status in imagecrawl report", e);
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
