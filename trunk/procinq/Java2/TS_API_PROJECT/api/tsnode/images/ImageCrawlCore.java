package api.tsnode.images;

import api.dao.queue.report.ReportStatusDao;
import api.tsnode.images.bean.ImagePage;
import api.tsnode.images.bean.MessageImages;
import api.tsnode.images.interfaces.SearchImageEngine;
import api.util.ConfServer;
import com.iw.db.ConnectionFactory;
import com.linkapedia.image.Image;
import com.linkapedia.image.ImageNotFoundException;
import com.linkapedia.procinq.server.corpus.CorpusDao;
import com.linkapedia.procinq.server.node.Node;
import com.linkapedia.procinq.server.node.NodeDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ImageCrawlCore {

    private static final String PROCESS_IN_PROCESS = "IN PROCESS";
    private static final String PROCESS_FINISHED = "FINISHED";
    private static final String PROCESS_ERROR = "ERROR";
    private static final String TABLE_STATUS = "IMAGECRAWLREPORT";
    private static final Logger log = Logger.getLogger(ImageCrawlCore.class);
    private static final int THREAD_SIZE = 1;
    private static final String REPORT_WIKIPEDIA_IMAGE_PATH = ConfServer.getValue("com.linkapedia.wikipedia.images.path.status");

    public void start(MessageImages messageImages) throws SQLException {
        ReportStatusDao daoStatus = new ReportStatusDao();
        notifyBeginProcess(messageImages.getRequestId(), daoStatus);

        Connection connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        List<Thread> threads;
        int numberNodeId = Integer.parseInt(messageImages.getNodeId());
        try {
            List<ImagePage> imagesTaxonomy = getTaxonomyImagesById(Integer.parseInt(messageImages.getCorpusId()), connection);
            List<NodeSearchTerms> nodeSearchTerms = createNodeSearchTerms(numberNodeId,
                    messageImages.getToLevel(), connection);

            NotifyStorageAWS notifyStorage = new NotifyStorageAWS(
                    messageImages.getRequestId(),
                    nodeSearchTerms.size());
            List<SearchImageWorker> workers = createWorkers(notifyStorage, messageImages.isWithWikipediaImage(), imagesTaxonomy, messageImages.getRequestId());
            threads = createAndInitWorkers(workers, nodeSearchTerms);
            connection.close();
            connection = null;
            waitingForWorkers(threads);
            connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            notifyProcessSummary(numberNodeId,
                    messageImages.getRequestId(),
                    messageImages.getToLevel(),
                    nodeSearchTerms.size(), connection);
            notifyFinishProcess(messageImages.getRequestId(), daoStatus);
        } catch (NumberFormatException | ImageNotFoundException | SQLException ex) {
            notifyErrorProcess(messageImages.getRequestId(),
                    ex.getMessage(), daoStatus);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    private void notifyProcessSummary(int nodeId, String requestId, int toLevel, int sizeNodes, Connection connection) throws SQLException {
        int number_wikipedia_images;
        if (toLevel == -1) {
            number_wikipedia_images = NodeDao.getCountWikipediaImages(nodeId, connection);
        } else {
            number_wikipedia_images = NodeDao.getCountWikipediaImages(nodeId, toLevel, connection);
        }
        String summary = "# Nodes: " + sizeNodes + ", of Wikipedia: " + number_wikipedia_images;
        updateProcessSummary(requestId, summary, connection);
    }

    private void waitingForWorkers(List<Thread> workers) {
        for (Thread t : workers) {
            try {
                t.join();
            } catch (InterruptedException ex) {
                log.info("ImagesCrawlProcess, InterruptedException", ex);
                continue;
            }
        }
    }

    private List<Thread> createAndInitWorkers(List<SearchImageWorker> workers, List<NodeSearchTerms> nodeSearchTerms) {
        Thread th;
        List<Thread> threads = new ArrayList<>();
        int index = 0;
        for (NodeSearchTerms nodeTerms : nodeSearchTerms) {
            workers.get(index).addTerm(nodeTerms);
            index = ((index + 1) % THREAD_SIZE);
        }
        for (SearchImageWorker worker : workers) {
            th = new Thread(worker);
            threads.add(th);
            th.start();
        }
        return threads;
    }

    private List<SearchImageWorker> createWorkers(NotifyStorageAWS notifyStorageAws, boolean isWithWikipediaImage, List<ImagePage> imagesTaxonomy, String requestId) {
        List<SearchImageWorker> workers = new ArrayList<>();
        SearchImageWorker worker;
        SearchImage searchImage = createSearchImage(isWithWikipediaImage, imagesTaxonomy, requestId);
        for (int i = 1; i <= THREAD_SIZE; i++) {
            worker = new SearchImageWorker(searchImage, notifyStorageAws);
            workers.add(worker);
        }
        return workers;
    }

    private SearchImage createSearchImage(boolean isWithWikipediaImage, List<ImagePage> imagesTaxonomy, String requestId) {
        if (isWithWikipediaImage) {
            return new WikipediaSearchImage(new SearchImageEngineWikipedia(REPORT_WIKIPEDIA_IMAGE_PATH + requestId));
        }
        return new DefaultSearchImage(createSearchEngines(isWithWikipediaImage, requestId), imagesTaxonomy);
    }

    private List<NodeSearchTerms> createNodeSearchTerms(int nodeId, int toLevel, Connection connection) throws SQLException {
        Map<String, String> searchTerms;
        List<Node> nodes;

        if (toLevel == -1) {
            nodes = NodeDao.getNodesByIdUntilLastLevel(nodeId, connection);
            searchTerms = NodeDao.getNodeSearchTermByIdUntilLastLevel(nodeId, connection);
        } else {
            nodes = NodeDao.getNodesByIdUntilLevel(nodeId, toLevel, connection);
            searchTerms = NodeDao.getNodeSearchTermByIdUntilLevel(nodeId, toLevel, connection);
        }

        List<NodeSearchTerms> nodesSearchTerms = new ArrayList<>();
        for (Node node : nodes) {
            NodeSearchTerms nodeTermsSearch = new NodeSearchTerms(
                    String.valueOf(node.getNodeId()),
                    getSearchTermByMap(searchTerms, node),
                    node.getNodeTitle());
            nodesSearchTerms.add(nodeTermsSearch);
        }

        return nodesSearchTerms;

    }

    private void notifyBeginProcess(String requestId, ReportStatusDao dao) {
        updateProcessStatus(requestId,
                TABLE_STATUS,
                PROCESS_IN_PROCESS,
                dao);
    }

    private void notifyFinishProcess(String requestId, ReportStatusDao dao) {
        updateProcessStatus(requestId,
                TABLE_STATUS,
                PROCESS_FINISHED,
                dao);
    }

    private void notifyErrorProcess(String requestId, String message, ReportStatusDao dao) {
        updateProcessStatus(requestId,
                TABLE_STATUS,
                PROCESS_ERROR + " " + message,
                dao);
    }

    private List<ImagePage> getTaxonomyImagesById(int id, Connection connection) throws SQLException {
        List<byte[]> imagesTaxonomy = CorpusDao.getImages(id, connection);
        if (imagesTaxonomy.isEmpty()) {
            throw new ImageNotFoundException("Taxonomy images not found");
        }
        return createImagePageFromTaxonomyImages(imagesTaxonomy);
    }

    private List<SearchImageEngine> createSearchEngines(boolean justSearchOnWikipedia, String requestId) {
        List<SearchImageEngine> searchEngines = new ArrayList<>();
        searchEngines.add(new SearchImageEngineWikipedia(REPORT_WIKIPEDIA_IMAGE_PATH + requestId));
        if (!justSearchOnWikipedia) {
            searchEngines.add(new SearchImageEngineBing());
        }
        return searchEngines;
    }

    private List<ImagePage> createImagePageFromTaxonomyImages(List<byte[]> taxonomyImages) {
        List<ImagePage> imagesPageTaxonomy = new ArrayList<>();
        ImagePage imagePageTaxonomy;
        for (byte[] image : taxonomyImages) {
            imagePageTaxonomy = new ImagePage("", new Image(image, ""));
            imagesPageTaxonomy.add(imagePageTaxonomy);
        }
        return imagesPageTaxonomy;
    }

    private String getSearchTermByMap(Map<String, String> searchTerms, Node node) {
        String searchTermInMap = searchTerms.get(String.valueOf(node.getNodeId()));
        if (searchTermInMap == null) {
            return node.getNodeTitle();
        }
        return searchTermInMap;
    }

    private void updateProcessStatus(String id,
            String table,
            String status,
            ReportStatusDao daoStatus) {
        try {
            //update in oracle with request id in process
            daoStatus.updateProcessStatus(id,
                    table,
                    status);

        } catch (Exception ex) {
            log.error("Error updating the imagecrawl process status by "
                    + status, ex);
        }
    }

    private void updateProcessSummary(String requestId, String summary, Connection connection) throws SQLException {
        String sql = "UPDATE IMAGECRAWLREPORT SET SUMMARY=? WHERE REQUESTID=?";
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, summary);
            pstmt.setLong(2, Long.valueOf(requestId));
            pstmt.executeUpdate();
        } finally {
            pstmt.close();
        }
    }
}
