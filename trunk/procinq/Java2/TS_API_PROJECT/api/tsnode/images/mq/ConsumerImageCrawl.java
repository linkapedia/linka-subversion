package api.tsnode.images.mq;

import api.tsnode.images.bean.MessageImages;

import api.tsnode.images.ImageCrawlCore;
import com.npstrandberg.simplemq.Message;
import com.npstrandberg.simplemq.MessageQueue;

/**
 *
 * @author andres
 */
public class ConsumerImageCrawl implements Runnable {

    private static final org.apache.log4j.Logger log =
            org.apache.log4j.Logger.getLogger(ConsumerImageCrawl.class);
    private int millis = 5000;
    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    @Override
    public void run() {
        ImageCrawlCore mainWorker;
        MessageImages ma = new MessageImages();
        Message m;
        while (true) {
            try {
                m = messageQueue.receiveAndDelete();
                if (m != null) {
                    ma = (MessageImages) m.getObject();
                    //read the objet and start the process
                    mainWorker = new ImageCrawlCore();
                    mainWorker.start(ma);
                }
            } catch (Exception e) {
                //error in the process a message
                log.error("Error procesing data from the queue requestId:"
                        + ma.getRequestId(), e);
            }
            try {
                //TODO: manage the millis with a logic exponencial
                Thread.sleep(millis);
            } catch (InterruptedException ex) {
                log.error("A ImageCrawl CONSUMER IS DEAD, InterruptedException",
                        ex);
            }
        }
    }
}
