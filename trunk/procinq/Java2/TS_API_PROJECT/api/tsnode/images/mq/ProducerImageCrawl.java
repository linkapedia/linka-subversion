package api.tsnode.images.mq;

import api.tsnode.images.bean.MessageImages;
import com.npstrandberg.simplemq.MessageInput;
import com.npstrandberg.simplemq.MessageQueue;

/**
 * Producer for images
 *
 * @author andres
 */
public class ProducerImageCrawl {

    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    public boolean sendMessage(MessageImages message) {
        MessageInput mi = new MessageInput();
        mi.setObject(message);
        return messageQueue.send(mi);
    }
}
