package api.tsnode.images;

import api.tsnode.images.bean.ImagePage;
import api.tsnode.images.interfaces.SearchImageEngine;
import com.linkapedia.image.ImageNotFoundException;
import com.linkapedia.image.ImageService;
import com.linkapedia.image.utils.HtmlPageDowloaderException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SearchImageEngineWikipedia implements SearchImageEngine {

    private String path;

    public SearchImageEngineWikipedia(String path) {
        this.path = path;
    }

    @Override
    public ImagePage search(NodeSearchTerms searchTerm, ImageService imageService) throws IOException, ImageNotFoundException {
        String wikipediaUrl = getWikipediaUrl(searchTerm.getNodeTitle());
        try {
            return new ImagePage(wikipediaUrl, imageService.getMosQualityImageFromWikipediaApi(wikipediaUrl));
        } catch (ImageNotFoundException | HtmlPageDowloaderException | IOException ex) {
            WikipediaImageNotFoundException wikiEx = new WikipediaImageNotFoundException(wikipediaUrl);
            String line = String.format("%s, %s, %s", searchTerm.getId(), searchTerm.getNodeTitle(), wikiEx.getWikipediaPage());
            saveWikipediaImageNotFoundReport(line);
            throw wikiEx;
        }
    }

    private void saveWikipediaImageNotFoundReport(String line) throws IOException {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(
                new FileWriter(path, true)))) {
            out.println(line);
        }
    }

    private String getWikipediaUrl(String searchTerm) {
        String url = "http://en.wikipedia.org/w/api.php?action=query&titles=%s&prop=pageimages&format=json";
        searchTerm = searchTerm.trim().replaceAll("\\s", "_");
        searchTerm = searchTerm.trim().replaceAll("__", ",_");
        return String.format(url, searchTerm);
    }
}
