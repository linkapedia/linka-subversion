package api.tsnode.images;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.tsnode.images.bean.MessageImages;
import api.tsnode.images.mq.ProducerImageCrawl;
import api.util.NodeUtils;

import com.iw.db.ConnectionFactory;
import com.npstrandberg.simplemq.MessageQueue;
import com.npstrandberg.simplemq.MessageQueueService;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * Enqueue the image process
 *
 * look a good image for each node in the context
 *
 * @author andres
 */
public class TSImagesCrawlResource {

    private static final Logger log = Logger.getLogger(TSImagesCrawlResource.class);
    private static final String PROCESS_WAITING = "WAITING";
    private static final String DEFAULT_QUEUE_NAME = "IMAGECRAWLQUEUE";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {
        log.info("TSImagesCrawl called");
        String sKey = (String) props.get("SKEY", true);
        String corpusId = (String) props.get("corpusid");
        String nodeid = (String) props.get("nodeid");
        String toLevel = (String) props.get("tolevel");
        String withWikipediaImage = (String) props.get("withwikipediaimage");

        NodeUtils nu = new NodeUtils(dbc);
        if (nodeid == null || corpusId == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty() || corpusId.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        if (withWikipediaImage == null || withWikipediaImage.isEmpty()) {
            withWikipediaImage = "false";
        }
        //save the process status in oracle
        Map<String, String> parameters = new HashMap<>();
        Date dateTime = new Date();
        parameters.put("CORPUS", corpusId);
        parameters.put("NODE", nodeid);
        parameters.put("RECURSIVELY", toLevel);
        parameters.put("WITHWIKIPEDIAIMAGE", withWikipediaImage);
        parameters.put("TIMESTAMP", dateTime.getTime() + "");

        String requestId = "";
        try {
            requestId = saveProcessStatus(parameters);
        } catch (Exception e) {
            log.error("Error saving process status in WAITING");
            throw e;
        }
        MessageImages message = new MessageImages();
        message.setCorpusId(corpusId);
        message.setNodeId(nodeid);
        message.setToLevel(Integer.parseInt(toLevel));
        message.setRequestId(requestId);
        message.setWithWikipediaImage(Boolean.parseBoolean(withWikipediaImage));

        //send data to the queue
        ProducerImageCrawl producer = new ProducerImageCrawl();
        MessageQueue queue = MessageQueueService.getMessageQueue(DEFAULT_QUEUE_NAME);
        producer.setMessageQueue(queue);//pass the queue
        producer.sendMessage(message);
        out.println("<SUCCESS>" + requestId + "</SUCCESS>");
    }

    /**
     * save initial status
     *
     * @param fields
     * @return
     * @throws Exception
     */
    private static String saveProcessStatus(Map<String, String> fields)
            throws Exception {
        Connection conn = ConnectionFactory.createConnection(
                ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new Exception("saveProcessStatu(.."
                    + " Error getting connection from oracle");
        }
        String sql = "INSERT INTO SBOOKS.IMAGECRAWLREPORT (REQUESTID,TIMESTAMP,"
                + "CORPUS, NODE, RECURSIVELY, STATUS) VALUES "
                + "(SEC_IMAGECRAWL.nextval,?,?,?,?,?)";
        PreparedStatement pstmt = null;
        String key = "";
        ResultSet generatedKeys = null;
        try {
            pstmt = conn.prepareStatement(sql, new String[]{"REQUESTID"});
            pstmt.setLong(1, Long.parseLong(fields.get("TIMESTAMP")));
            pstmt.setString(2, fields.get("CORPUS"));
            pstmt.setString(3, fields.get("NODE"));
            pstmt.setString(4, fields.get("RECURSIVELY"));
            pstmt.setString(5, PROCESS_WAITING);
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new Exception("Creating imagecrawl process failed");
            }
            generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                key = String.valueOf(generatedKeys.getLong(1));
            } else {
                throw new SQLException("Creating imagecrawl process failed,"
                        + " no generated key obtained.");
            }
            return key;
        } catch (Exception e) {
            log.error("Error in the process status in qareport", e);
            throw e;
        } finally {
            try {
                if (generatedKeys != null) {
                    generatedKeys.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
