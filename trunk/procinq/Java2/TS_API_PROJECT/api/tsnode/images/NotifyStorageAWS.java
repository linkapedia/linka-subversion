package api.tsnode.images;

import api.dao.queue.report.ReportStatusDao;
import api.tsnode.images.bean.ImagePage;
import api.util.S3Storage;
import api.util.S3StorageException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.linkapedia.procinq.server.node.NodeDao;
import com.linkapedia.procinq.server.utils.AmazonUtil;
import com.linkapedia.procinq.server.utils.ImageUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.codec.digest.DigestUtils;

public class NotifyStorageAWS {

    private static final org.apache.log4j.Logger log =
            org.apache.log4j.Logger.getLogger(NotifyStorageAWS.class);
    int index = 0;
    int total;
    private String requestId;
    private static final String TABLE_STATUS = "IMAGECRAWLREPORT";
    private final String BUCKET_NAME = "nodeimages";

    public NotifyStorageAWS(String requestId,
            int total) {
        this.requestId = requestId;
        this.total = total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void save(ImagePage imagePage,
            String nodeId, Connection connection) throws S3StorageException,
            IOException, SQLException {
        String dynamoDbNodeId = AmazonUtil.getDynamoDBIdByNodeId(nodeId);
        byte[] image = ImageUtil.getLinkapediaSiteImage(imagePage.getImage().getRawImage());
        byte[] imageMobile = ImageUtil.getLinkapediaMobileImage(image);
        String imageHash = DigestUtils.md5Hex(image);
        String keyNoCrop = AmazonUtil.buildKeyImage(dynamoDbNodeId);
        String keyMobile = AmazonUtil.buildKeyImageMobile(dynamoDbNodeId);
        updateS3(keyNoCrop, image);
        updateS3(keyMobile, imageMobile);
        String imageLinkapediaUrl = AmazonUtil.getImageLinkapediaUrl(dynamoDbNodeId);
        saveImagePage(imagePage, nodeId, imageHash, imageLinkapediaUrl, connection);
        log.debug("UPDATING IMAGE INFO OK!");
    }

    private void saveImagePage(ImagePage imagePage, String nodeId,
            String imageHash, String imageLinkapediaUrl,
            Connection connection) throws SQLException {
        NodeDao.updateImageInfo(Integer.parseInt(nodeId), imagePage.getPageImageUrl(),
                imagePage.getImage().getUrl(),
                imageHash, imageLinkapediaUrl, connection);
    }

    private void updateS3(String key, byte[] image) throws S3StorageException, IOException {
        ObjectMetadata meta = ImageUtil.createMetadataImage(image);
        S3Storage.storageData(BUCKET_NAME, key,
                new ByteArrayInputStream(image), meta);
    }

    synchronized public void notifyProgress(int size) {
        try {
            index += size;
            long percent = (index * 100) / total;
            ReportStatusDao daoStatus = new ReportStatusDao();
            daoStatus.updateProcessStatus(this.requestId,
                    TABLE_STATUS,
                    "IN PROCESS, " + percent + "%");
        } catch (Exception ex) {
            log.error("Error updating notifyProgress", ex);
        }
    }
}
