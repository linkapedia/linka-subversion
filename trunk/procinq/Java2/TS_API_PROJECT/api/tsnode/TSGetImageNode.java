package api.tsnode;

import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.zip.GZIPOutputStream;
import org.apache.log4j.Logger;
import org.xeustechnologies.jtar.TarEntry;
import org.xeustechnologies.jtar.TarOutputStream;

/**
 * Add a Image to node.
 *
 *	@authors Andres Restrepo
 *
 *      @return file.tar.gz 
 *      1->file (images information)
 *      2->metadata image1
 *      image1 byte[]
 *      3->metadata image2
 *      image2 byte[]
 *      ...
 *      n->
 */
public class TSGetImageNode {

    private static final Logger log = Logger.getLogger(TSGetImageNode.class);

    public static void handleTSapiRequest(HttpServletRequest req,
                                          HttpServletResponse res,
                                          api.APIProps props,
                                          PrintWriter out,
                                          Connection dbc
                                          ){

        log.debug("TSGetImageNode");
        String nodeid = (String) props.get("nodeid");
        Statement stmt = null;
        ResultSet rs = null;
        String sSql = "SELECT * FROM NODEIMAGES WHERE NODEID="+nodeid;
        log.debug("Select sql-> "+sSql);
        log.debug("set mime type");
        res.setContentType("application/x-gzip");
        try {
            ByteArrayOutputStream bao=new ByteArrayOutputStream();
            TarOutputStream tao=new TarOutputStream(bao);
            log.info("Get Outputstream body");
            GZIPOutputStream gout=new GZIPOutputStream(res.getOutputStream());
            log.info("Create Statement");
            stmt = dbc.createStatement();
            log.info("Execute Statement");
            rs = stmt.executeQuery(sSql);
            int imageid;
            String Description="";
            String url ="";
            int node;
            StringBuilder lineToFile=new StringBuilder();
            byte[] fileImage=null;
            TarEntry tarEntry=null;
            while (rs.next()) {
                imageid=rs.getInt("IMAGEID");
                Description=rs.getString("DESCRIPTION");
                Description = validateString(Description);
                url =rs.getString("URL");
                url=validateString(url);
                node=rs.getInt("NODEID");
                fileImage=rs.getBytes("IMAGE");
                log.info("Image File Size "+fileImage.length);
                tarEntry=new TarEntry(new File(""), String.valueOf(imageid));
                log.info("File Name "+String.valueOf(imageid));
                tarEntry.setName(String.valueOf(imageid));
                log.info("Set File Size "+fileImage.length);
                tarEntry.setSize(fileImage.length);
                log.info("Put New Entry ");
                tao.putNextEntry(tarEntry);
                log.info("Writer Byte In Tar");
                tao.write(fileImage);
                lineToFile.append(imageid);
                lineToFile.append("||");
                lineToFile.append(Description);
                lineToFile.append("||");
                lineToFile.append(url);
                lineToFile.append("||");
                lineToFile.append(node);
                lineToFile.append("^");
                
                log.debug("\n lineToFile -> "+lineToFile.toString()+"\n");
                //copy line into directory
            }
            String strData=lineToFile.toString();
            if(strData.endsWith("^"))
            {
                strData=strData.substring(0, strData.lastIndexOf("^"));
            }
            tarEntry=new TarEntry(new File(""), "fileMetaData");
            log.info("set File Name Meta Data");
            tarEntry.setName("fileMetaData");
            log.info("set File Size Meta Data");
            tarEntry.setSize(strData.getBytes().length);
            log.info("set Put Entry  Meta Data");
            tao.putNextEntry(tarEntry);
            log.info("write Bytes Meta Data");
            tao.write(strData.getBytes());
            log.info("close Tar");
            tao.close();
            log.info("GZip Output");
            gout.write(bao.toByteArray());
            log.info("GZip Flush");
            gout.flush();
            log.info("GZip Close");
            gout.close();

        } catch (SQLException ex) {
            log.error("SQLException TSGetImageNode: "+ex.getMessage());
        } catch (Exception e) {
            log.error("Exception TSGetImageNode: "+e.getMessage());
        } finally {
            if(rs!=null){
                try {
                    rs.close();
                } catch (SQLException ex) {
                   log.error("error: rs.close() class: TSGetImageNode");
                }
            }
            if(stmt!=null){
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    log.error("error: smtp.close() class: TSGetImageNode");
                }
            }
        }
    }
    private static String validateString(String str){
        str=str.replace("||", "");
        str=str.replace("^", "");
        return str;
    }
}
