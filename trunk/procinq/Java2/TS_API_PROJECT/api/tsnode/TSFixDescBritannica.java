package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import api.util.bean.PackNode;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 * fix description from britannica
 *
 * @author andres
 */
public class TSFixDescBritannica {

    private static final Logger log = Logger.getLogger(TSPopulateSourceWiki.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("TSFixDescBritannica");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        String recursively = (String) props.get("recursively");

        if (recursively == null || recursively.isEmpty()) {
            recursively = "no";
        }

        if ((nodeid == null)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if ((nodeid.isEmpty())) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }

        //logic to fixed the source
        Integer nodeIdL = null;
        try {
            nodeIdL = Integer.parseInt(nodeid);
        } catch (NumberFormatException e) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
        }

        List<PackNode> packNodes = new ArrayList<PackNode>();
        List<String> titlesError = new ArrayList<String>();
        if (recursively.equals("yes")) {
            try {
                packNodes = nu.getTreeNodes(nodeIdL);
            } catch (Exception e) {
                log.error("Error retriving nodes", e);
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR);
            }
        } else {
            PackNode nodeT = nu.getPackNode(nodeIdL);
            if (nodeT != null) {
                packNodes.add(nodeT);
            }
        }

        String description = "";
        int result = 0;
        DecimalFormat twoDigits = new DecimalFormat("0");
        long percent = 0;
        int cont = 0;
        if (packNodes != null) {
            int numNodes = packNodes.size();
            for (PackNode pack : packNodes) {
                try {
                    description = fixDescription(pack.getNodeDescription(), pack.getNodetitle());
                } catch (Exception e) {
                    log.error("Error fixDescription() -> " + pack.getNodeid() + " " + pack.getNodetitle(), e);
                }
                if (description == null || description.isEmpty()) {
                    titlesError.add(pack.getNodeid() + " " + pack.getNodetitle());
                } else {
                    result = updateDescription(dbc, Long.parseLong(pack.getNodeid()), description);
                    if (result == 0) {
                        titlesError.add(pack.getNodeid() + " " + pack.getNodetitle());
                    }
                }
                //show progress
                percent = ((cont + 1) * 100) / numNodes;
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                log.debug("finished one proccess: " + percent + "%");
                out.flush();
                cont++;
            }
        }
        out.println("<RESULT>");
        out.println("<TERMS>");
        for (String t : titlesError) {
            out.println("<TERM>" + t + "</TERM>");
        }
        out.println("</TERMS>");
        out.println("</RESULT>");
    }

    private static String fixDescription(String description, String title) {
        log.debug("fixSource");
        Pattern p = Pattern.compile("^.*(Britannica Student Encyclopedia).*(Britannica Student Encyclopedia).(" + title + ")", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(description);
        if (m.find()) {
            description = m.replaceAll("");
        }
        return description;
    }

    /**
     * save the description
     */
    private static int updateDescription(Connection con, Long nodeid, String desc) throws SQLException {
        log.debug("TSFixDescBritannica: updateDescription");
        String sql = "UPDATE NODE SET NODEDESC = ? where nodeid = ?";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, desc);
            pstmt.setLong(2, nodeid);
            int i = pstmt.executeUpdate();
            return i;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
}
