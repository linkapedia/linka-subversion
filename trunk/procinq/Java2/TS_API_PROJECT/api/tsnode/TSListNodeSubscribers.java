package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * This API call has been deprecated, and should no longer be used.
 * @note    This API call has been deprecated.
 */
public class TSListNodeSubscribers
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sKey = (String) props.get("SKEY", true);
		User ulogin = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // ** DEPRECATED, RETURN
        if (true) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE);

        LDAP_Connection lc = null;
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();
			
			int iCorpusId = rs.getInt(1);
			rs.close(); stmt.close();
			
			if (!ulogin.IsAuthorized(iCorpusId, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			lc = new LDAP_Connection(ulogin.GetDN(), ulogin.GetPassword(), out);
			Vector vUsers = lc.GetAllUsers();
			
			if (vUsers.elementAt(0) == null) { 
				throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); 
			}
		
			int loop = 0;
			Enumeration ev = vUsers.elements();
			while (ev.hasMoreElements()) {
				User u = (User) ev.nextElement();
				Hashtable htNodes = u.GetNodeHash();
				if (htNodes.containsKey(sNodeID)) { 
					loop++; if (loop == 1) { out.println("<SUBSCRIBERS>"); }

					out.println ("   <SUBSCRIBER> ");
					out.println ("      <ID>"+u.GetDN()+","+lc.GetDC()+"</ID> ");
					out.println ("      <EMAIL>"+u.GetEmail()+"</EMAIL>");
					out.println ("      <PASSWORD>"+u.GetPassword()+"</PASSWORD>");
					out.println ("      <NAME>"+u.GetFullname()+"</NAME>");
					out.println ("      <EMAILSTATUS>"+u.GetEmailStatus()+"</EMAILSTATUS>");
					out.println ("      <SCORETHRESHOLD>"+u.GetScoreThreshold()+"</SCORETHRESHOLD>");
					out.println ("      <RESULTSPERPAGE>"+u.GetResultsPerPage()+"</RESULTSPERPAGE>");
					out.println ("      <USERSTATUS>"+u.GetUserStatus()+"</USERSTATUS>");
					out.println ("   </SUBSCRIBER>");
				}
			}
			if (loop > 0) { out.println("</SUBSCRIBERS>"); }
		} catch ( TSException tse )	{ EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
