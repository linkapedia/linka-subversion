package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 * This API call has been deprecated, and should no longer be used.
 * @note    This API call has been deprecated.
 */
public class TSListSubscriberNodes
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // ** DEPRECATED, RETURN
        if (true) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {

			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			if (u.GetNodeHash().size() < 1) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			String sSQL = Node.getSQL(dbc)+" where ";
			
			// Build the WHERE clause of the SQL statement
			Enumeration eHt = u.GetNodeHash().keys();
			int loop = 0;
			while (eHt.hasMoreElements()) {
				String sNodeID = (String) eHt.nextElement();
				String sVal = (String) u.GetNodeHash().get(sNodeID);
				
				if (sVal.equals("1")) {
					loop++; if (loop > 1) { sSQL = sSQL + " OR "; }
					sSQL = sSQL + "NodeID = "+sNodeID;
				}
			}
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			loop = 0;
			while ( rs.next() ) {
				// Since we cannot get the number of results from the Result Object, we
				//  need to count with a loop counter.   If results are 0 we want to return
				//  an exception (error) without printing any streams.   To do this, hold off
				//  on writing out the initial stream <NODES> until we have found at 
				//  least one result.   -MAP 10/14/01
				loop = loop + 1;
				if (loop == 1) { out.println ("<NODES>"); }
                Node n = new Node(rs);
                n.emitXML(out, u, true);
			}
		
			rs.close();
		    stmt.close();

			out.println ("</NODES>");
			
		}
		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { lc.close(); }
	}
}
