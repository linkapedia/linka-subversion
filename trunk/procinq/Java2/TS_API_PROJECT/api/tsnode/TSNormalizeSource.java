package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import api.util.WikiUtil;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSNormalizeSource {

    private static final Logger log = Logger.getLogger(TSNormalizeSource.class);

    //remove bad source in the node recursively
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("TSNormalizeSource");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");
        String recursively = (String) props.get("recursively");

        if (recursively == null || recursively.isEmpty()) {
            recursively = "no";
        }

        if ((nodeid == null)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if ((nodeid.isEmpty())) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }

        //logic to fixed the source
        Long nodeIdL = null;
        try {
            nodeIdL = Long.parseLong(nodeid);
        } catch (NumberFormatException e) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
        }

        List<Long> ids = new ArrayList<Long>();
        List<String> titlesError = new ArrayList<String>();
        if (recursively.equals("yes")) {
            try {
                ids = nu.getTreeNodesNodeid(nodeIdL);
            } catch (Exception e) {
                log.error("Error retriving nodes", e);
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR);
            }
        } else {
            ids.add(nodeIdL);
        }
        WikiUtil wu = new WikiUtil();
        String title;
        String source = "";
        int result = 0;
        DecimalFormat twoDigits = new DecimalFormat("0");
        long percent = 0;
        int cont = 0;
        if (ids != null) {
            int numNodes = ids.size();
            for (long id : ids) {
                title = getNodeTitle(dbc, id);
                try {
                    source = wu.getSingleContent(title);
                } catch (Exception e) {
                    log.error("Error getting source -> " + title, e);
                }
                if (source == null || source.isEmpty()) {
                    titlesError.add(title);
                } else {
                    result = updateSource(dbc, id, source);
                    if (result == 0) {
                        titlesError.add(title);
                    }
                }
                //show progress
                percent = ((cont + 1) * 100) / numNodes;
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                log.debug("finished one proccess: " + percent + "%");
                out.flush();
                cont++;
            }
        }
        out.println("<RESULT>");
        out.println("<TERMS>");
        for (String t : titlesError) {
            out.println("<TERM>" + t + "</TERM>");
        }
        out.println("</TERMS>");
        out.println("</RESULT>");
    }

    /**
     *
     */
    private static int updateSource(Connection con, Long nodeid, String source) throws SQLException {
        log.debug("NodeUtils: getNodesAndSource");
        String sql = "UPDATE NODEDATA SET NODESOURCE = ? where nodeid = ?";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, source);
            pstmt.setLong(2, nodeid);
            int i = pstmt.executeUpdate();
            return i;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    private static String getNodeTitle(Connection con, Long nodeid) throws SQLException {
        log.debug("NodeUtils: getNodesAndSource");
        String sql = "SELECT NODEID, NODETITLE FROM NODE WHERE NODEID = ?";
        ResultSet rs = null;
        String nodeTitle = "";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, nodeid);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                nodeTitle = rs.getString("nodetitle");
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return nodeTitle;
    }
}
