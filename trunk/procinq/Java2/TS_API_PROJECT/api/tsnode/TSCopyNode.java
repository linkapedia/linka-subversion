package api.tsnode;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.DecimalFormat;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.*;

import com.iw.system.User;
import com.indraweb.execution.Session;
import com.iw.classification.Data_WordsToNodes;
import com.iw.db.ConnectionFactory;

/**
 * Copy the given node from one location to another. This function will also
 * copy all descendents of the given node. A node cannot be copied into one of
 * it's descendents.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param NodeID Unique node identifier of this node.
 * @param ParentID Unique node identifier of the parent node to be copied into.
 *
 * @note
 * http://ITSSERVER/itsapi/ts?fn=tsnode.TSCopyNode&NodeID=2000&ParentID=2006&SKEY=9919294812
 *
 * @return	NODEID identifier of the newly created node \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <NODEID>20029</NODEID>
 * </TSRESULT>
 * \endverbatim
 */
public class TSCopyNode {

    private static int Count = 0;
    private static int Total = 0;
    private static String sNodeID = null;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        sNodeID = (String) props.get("NodeID", true);
        String sParentID = (String) props.get("ParentID", true);
        String sShowStatus = (String) props.get("ShowStatus");

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
        }
        try {
            dbc = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);

            int iCorpusID = -1;
            int iDepth = -1;

            // 1) Get the CORPUSID of the new PARENT node
            // LOOP:
            //   2) Get the next available NODEID
            //   3) Insert into NODE select * from NODE, replacing NODEID, PARENTID, and CORPUSID
            //   4) Repeat for all descendents

            // first get the total number of children that we will be copying
            String sSQL = "select count(*) from node start with nodeid = " + sNodeID + " connect by prior nodeid = parentid";
            Statement stmt = null;
            ResultSet rs = null;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                rs.next();

                Total = rs.getInt(1);
            } catch (Exception e) {
                api.Log.LogError("error in tscopynode, sql: " + sSQL, e);
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            // 1) Get the CORPUSID of the new PARENT node
            sSQL = " select CorpusId, DepthFromRoot from Node where NodeID = " + sParentID;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                rs.next();

                iCorpusID = rs.getInt(1);
                iDepth = rs.getInt(2);
                if (!u.IsAdmin(iCorpusID, out)) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                }
            } catch (TSException tse) {
                EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
            } catch (Exception e) {
                api.Log.LogError("error in tscopynode, sql: " + sSQL, e);
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            if (!nodeExists(sNodeID, dbc)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_SUCH_OBJECT);
            }
            synchronized (new Integer(iCorpusID)) {
                if (sShowStatus != null) {
                    sNodeID = copyNodeRecursive(sNodeID, sParentID, iCorpusID + "", out, (iDepth + 1), dbc);
                } else {
                    sNodeID = copyNodeRecursive(sNodeID, sParentID, iCorpusID + "", null, (iDepth + 1), dbc);
                }
            }
            // purge signature cache by corpus
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus("TSCopyNode", iCorpusID);

            out.println("<NODEID>" + sNodeID + "</NODEID>");

            Count = 0;
            Total = 0;
        } catch (Exception e) {
            throw e;
        } finally {
            if (dbc != null) {
                dbc.close();
            }
        }
    }

    public static boolean nodeExists(String NodeID, Connection dbc) throws Exception {
        String sSQL = "select nodeid from node where nodeid = " + NodeID;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            int loop = 0;

            while (rs.next()) {
                loop++;
            }

            if (loop == 0) {
                return false;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        return true;
    }

    public static String copyNodeRecursive(String NodeID, String ParentID, String CorpusID, PrintWriter out,
            int Depth, Connection dbc)
            throws Exception {
        //api.Log.Log("Call into copyNodeRecursive with: NID: "+NodeID+" PID: "+ParentID+" CID: "+CorpusID+" Depth: "+Depth);
        DecimalFormat twoDigits = new DecimalFormat("0");

        boolean bIndraweb = Session.cfg.getPropBool("Indraweb", false, "false");

        // get the highest NIWP among sibling nodes and add 1.  If there are no siblings, use "1".
        String NIWP = "1";
        String sSQL = "select max(nodeindexwithinparent) from node where parentid = " + ParentID;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) {
                int iNIWP = rs.getInt(1);
                NIWP = (iNIWP + 1) + "";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        String sNextID = "" + com.indraweb.utils.oracle.UniqueSpecification.getSubsequentNodeID(dbc, bIndraweb);
        Vector vChildren = new Vector();

        // insert the copied node
        if (sNodeID.equals(NodeID)) {
            sSQL = " INSERT INTO node (nodeid, corpusid, nodetitle, nodesize, parentid, "
                    + " nodeindexwithinparent, depthfromroot, datescanned, dateupdated, nodestatus, "
                    + " nodeltitle, nodelastsync, nodedesc) SELECT " + sNextID + ", " + CorpusID + ", NodeTitle, "
                    + " NodeSize, " + ParentID + ", " + NIWP + ", " + Depth + ", DateScanned, "
                    + " SYSDATE, NodeStatus, NodeLTitle, NodeLastSync, NodeDesc from Node "
                    + " WHERE NodeID = " + NodeID;
        } else {
            sSQL = " INSERT INTO node (nodeid, corpusid, nodetitle, nodesize, parentid, "
                    + " nodeindexwithinparent, depthfromroot, datescanned, dateupdated, nodestatus, "
                    + " nodeltitle, nodelastsync, nodedesc) SELECT " + sNextID + ", " + CorpusID + ", NodeTitle, "
                    + " NodeSize, " + ParentID + ", nodeindexwithinparent, " + Depth + ", DateScanned, "
                    + " SYSDATE, NodeStatus, NodeLTitle, NodeLastSync, NodeDesc from Node "
                    + " WHERE NodeID = " + NodeID;
        }

        stmt = null;
        rs = null;

        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        //api.Log.Log("Node insert successful.");

        // copy relationships as well
        // SIGNATURE
        sSQL = " INSERT INTO signature (nodeid, signatureword, signatureoccurences, lang) SELECT " + sNextID
                + ", SIGNATUREWORD, SIGNATUREOCCURENCES, LANG FROM signature WHERE NodeID = " + NodeID;
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        // MUSTHAVE
        sSQL = " INSERT INTO musthave (nodeid, mustword, lang) SELECT " + sNextID
                + ", MUSTWORD, LANG FROM musthave WHERE NodeID = " + NodeID;
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        //api.Log.Log("Signature transfer successful.");

        // IDRACNODE
        sSQL = " INSERT INTO idracnode (nodeid, nodecountry) SELECT " + sNextID + ", nodecountry "
                + " FROM idracnode WHERE NodeID = " + NodeID;
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        //api.Log.Log("IDRAC Node transfer successful.");

        // NODEDATA
        sSQL = " INSERT INTO nodedata (nodeid, nodesource) SELECT " + sNextID + ", nodesource"
                + " FROM nodedata WHERE NodeID = " + NodeID;
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        // NODEDOCUMENT
        sSQL = " INSERT INTO nodedocument (nodeid, documentid, documenttype, score1, score2, score3, "
                + " score4, score5, score6, score7, score8, score9, score10, score11, score12, docsummary, "
                + " datescored, indexwithinnode) SELECT " + sNextID + ", documentid, documenttype, score1, score2,  "
                + " score3, score4, score5, score6, score7, score8, score9, score10, score11, score12, "
                + " docsummary, datescored, indexwithinnode "
                + " FROM nodedocument WHERE NodeID = " + NodeID;
        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        //api.Log.Log("NODEDOCUMENT transfer successful.");

        long percent = ((Count + 1) * 100) / Total;
        if (out != null) {
            out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
            out.flush();
        }
        Count++;

        // now look for children and insert them, if applicable
        sSQL = "select nodeid from node where parentid = " + NodeID + " and nodeid != " + sNextID;

        //api.Log.Log("Get children: "+sSQL);

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) {
                vChildren.add(rs.getString(1));
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        //api.Log.Log(vChildren.size()+" children found, invoking copyNodeRecursive.");
        for (int i = 0; i < vChildren.size(); i++) {
            copyNodeRecursive((String) vChildren.elementAt(i), sNextID, CorpusID, out, (Depth + 1), dbc);
        }
        return sNextID;
    }
}
