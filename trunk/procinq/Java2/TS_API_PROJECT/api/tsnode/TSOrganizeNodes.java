package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import api.util.SortedComparatorLastWord;
import api.util.SortedComparatorWord1;
import api.util.SortedComparatorWord2;
import api.util.Words;
import com.iw.db.ConnectionFactory;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSOrganizeNodes {

    private static final Logger LOG = Logger.getLogger(TSOrganizeNodes.class);
    private static final int BATCH_UPDATER = 100;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        LOG.debug("TSOrganizeNodes");
        try {
            /*Hack hack, when we have a lot nodes the api connection close the stream
             I am getting our connection now
             also, I need that connection is closed after use
             */
            dbc = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            NodeUtils nu = new NodeUtils(dbc);
            //validate AUTHORIZED
            String sKey = (String) props.get("SKEY", true);
            //get nodeid to start with the process
            String nodeid = (String) props.get("nodeid");
            String wordOrder = (String) props.get("wordOrders");

            if (nodeid == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nodeid.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            int orderBy;
            if (wordOrder == null) {
                orderBy = 1;
            } else {
                orderBy = Integer.parseInt(wordOrder);
                if (orderBy != -1 && orderBy != 1 && orderBy != 2) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR,
                            "orderBy invalid!!!");
                }
            }
            Map<String, List<Words>> map = getPreSortedNodes(
                    getNodes(dbc, nodeid), orderBy);
            //sort each item in the map
            for (Map.Entry<String, List<Words>> entry : map.entrySet()) {
                switch (orderBy) {
                    case 1:
                        Collections.sort(entry.getValue(), new SortedComparatorWord1());
                        break;
                    case 2:
                        Collections.sort(entry.getValue(), new SortedComparatorWord2());
                        break;
                    case -1:
                        Collections.sort(entry.getValue(), new SortedComparatorLastWord());
                        break;
                    default:
                        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR,
                                "orderBy invalid!!!");
                }
            }
            List<String> letterOrder = new ArrayList<>();
            letterOrder.addAll(map.keySet());
            Collections.sort(letterOrder);
            //move nodes to new folder
            String corpusId = nu.getCorpusId(nodeid);
            saveNodes(dbc, nodeid, corpusId, letterOrder, map, orderBy);
            out.println("<RESULT>Finished</RESULT>");
        } catch (Exception e) {
            throw e;
        } finally {
            if (dbc != null) {
                dbc.close();

            }
        }
    }

    private static List<Words> getNodes(
            Connection con,
            String nodeId) throws SQLException {
        LOG.debug("TSOrganizeNodes: getNodes");
        String sql = "SELECT NODE.NODEID, NODE.NODETITLE"
                + " FROM NODE WHERE NODE.PARENTID=?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        List<Words> listResult = new ArrayList<>();
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(nodeId));
            rs = pstmt.executeQuery();
            Words word;
            while (rs.next()) {
                String node = String.valueOf(rs.getInt("nodeid"));
                String nodetitle = rs.getString("nodetitle");
                nodetitle = nodetitle.replaceAll("\\p{Punct}", "").trim();
                word = new Words();
                word.setNodeId(node);
                if (nodetitle != null && !nodetitle.isEmpty()) {
                    String[] array = nodetitle.split("\\s+");
                    word.setWords(Arrays.asList(array));
                }
                listResult.add(word);
            }
            return listResult;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    private static Map<String, List<Words>> getPreSortedNodes(
            List<Words> words, int orderBy) throws IllegalArgumentException {
        if (null == words) {
            throw new IllegalArgumentException("Words can't be null");
        }
        Map<String, List<Words>> mapResult = new CaseInsensitiveMap();
        List<Words> list;
        for (Words word : words) {
            String letter = getFirstLetter(word.getWords(), orderBy);
            if (!letter.matches("[a-zA-Z0-9]")) {
                letter = "OTHERS";
            }
            list = mapResult.get(letter);
            if (list == null) {
                list = new ArrayList<>();
                list.add(word);
                mapResult.put(letter, list);
            } else {
                list.add(word);
            }
        }
        return mapResult;
    }

    private static String getFirstLetter(List<String> words, int orderBy) {
        if (null == words || words.isEmpty()) {
            return "";
        }
        String letter;
        switch (orderBy) {
            case 1:
                letter = String.valueOf(words.get(0).charAt(0));
                break;
            case 2:
                if (words.size() >= 2) {
                    letter = String.valueOf(words.get(1).charAt(0));
                } else {
                    letter = String.valueOf(words.get(0).charAt(0));
                }
                break;
            case -1:
                int size = words.size();
                letter = String.valueOf(words.get(size - 1).charAt(0));
                break;
            default:
                letter = String.valueOf(words.get(0).charAt(0));
        }
        return letter;
    }

    private static String getWordOrder(List<String> words, int orderBy) {
        if (null == words || words.isEmpty()) {
            return "";
        }
        String word;
        switch (orderBy) {
            case 1:
                word = String.valueOf(words.get(0));
                break;
            case 2:
                if (words.size() >= 2) {
                    word = String.valueOf(words.get(1));
                } else {
                    word = String.valueOf(words.get(0));
                }
                break;
            case -1:
                int size = words.size();
                word = String.valueOf(words.get(size - 1));
                break;
            default:
                word = String.valueOf(words.get(0));
        }
        return word;
    }

    /**
     * create a node and put all list in this new node
     *
     * @param nodeId
     * @param words
     */
    private static void saveNodes(
            Connection con,
            String nodeId,
            String corpusId,
            List<String> sortOrderList,
            Map<String, List<Words>> words,
            int orderBy) throws Exception {

        if (con == null) {
            throw new IllegalArgumentException("Connection can't be null");
        }
        if (nodeId == null || nodeId.isEmpty()) {
            throw new IllegalArgumentException("nodeId can't be null");
        }
        if (corpusId == null || corpusId.isEmpty()) {
            throw new IllegalArgumentException("corpusId can't be null");
        }
        if (sortOrderList == null) {
            throw new IllegalArgumentException("sortOrderList can't be null");
        }
        if (words == null) {
            throw new IllegalArgumentException("words can't be null");
        }
        int cont = 0;
        int indexNode = 1;
        PreparedStatement pstmt = null;
        con.setAutoCommit(false);
        try {
            long sProp_NodeID = com.indraweb.utils.oracle.UniqueSpecification.
                    getSubsequentNodeID(con, false);
            //block the method
            com.indraweb.utils.oracle.UniqueSpecification.processAlphaNodes = true;
            for (String letter : sortOrderList) {
                List<Words> currentList = words.get(letter);
                String word1 = getWordOrder(currentList.get(0).getWords(), orderBy);
                String word2 = getWordOrder(currentList.get(currentList.size() - 1).
                        getWords(), orderBy);

                String finalName = word1 + " - " + word2;

                //create the folder
                String sql = "INSERT INTO NODE (NODEID, PARENTID, NODETITLE, "
                        + "NODEINDEXWITHINPARENT,CORPUSID) "
                        + "VALUES (?,?,?,?,?)";
                pstmt = con.prepareStatement(sql);
                pstmt.setLong(1, sProp_NodeID);
                pstmt.setString(2, nodeId);
                pstmt.setString(3, finalName);
                pstmt.setInt(4, indexNode);
                pstmt.setString(5, corpusId);
                pstmt.executeUpdate();

                //save all nodes in the before folder
                sql = "UPDATE NODE SET "
                        + "PARENTID = ?, "
                        + "NODEINDEXWITHINPARENT = ? "
                        + "WHERE NODEID = ?";
                pstmt = con.prepareStatement(sql);
                long indexInternNodes = 1;
                for (Words wordsAux : currentList) {
                    pstmt.setLong(1, sProp_NodeID);
                    pstmt.setLong(2, indexInternNodes);
                    pstmt.setString(3, wordsAux.getNodeId());
                    pstmt.addBatch();
                    cont++;
                    if (cont == BATCH_UPDATER) {
                        //update the batch
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        cont = 0;
                        continue;
                    }
                    indexInternNodes++;
                }
                //the last data
                if (cont != 0) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                }
                indexNode++;
                sProp_NodeID++;
            }
            //commit for save all nodes in the before folder
            con.commit();
        } catch (Exception e) {
            LOG.error("Error updating nodes", e);
            try {
                //rollback all if i found a error
                con.rollback();
            } catch (SQLException ex) {
                throw ex;
            }
            throw e;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            //Unblock the method
            com.indraweb.utils.oracle.UniqueSpecification.processAlphaNodes = false;
        }
    }
}
