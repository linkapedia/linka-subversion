package api.tsnode;

import api.APIProps;
import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.database.SQLGenerators;
import com.indraweb.execution.Session;
import com.iw.classification.Data_WordsToNodes;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Adds a node (topic) relationship to the ITS server. Nodes can be added with their corresponding concept signature set using the WordAndFreqs parameter. If the new node is a link, the location that
 * the link points to should be specified in the LinkNodeID parameter.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param CorpusID Unique corpus identifier indicating this node's taxonomy.
 * @param ParentID Unique node identifier of this node's parent topic. If this is a root node, parentid is -1.
 * @param NodeTitle Title of this node.
 * @param NodeSize Size of the original corpus source for this node. If no corpus source available, use "50".
 * @param NodeDesc (optional) Description for this node, no more than 4000 characters.
 * @param NodeIndexWithinParent (optional) Position of this node within its parent. If not specified, node will be placed at the end.
 * @param LinkNodeID (optional) This parameter is only used if this new node is a link. This column specifies the unique node identifier of the referenced node.
 * @param WordsAndFreqs (optional) A series of signature words and frequencies, divided by colons (:) and separated by commas (,). Default is no signatures.
 *
 * @note http://ITSSERVER/itsapi/ts?fn=tsnode.TSAddNode&NodeID=2000&ParentID=902&CorpusID=1&NodeTitle=hkon&WordsAndFreqs=word1:1;word2:2;word3:3&NodeSize=40&ParentRel=1&NodeIndexInParent=20
 *
 * @return	SUCCESS tag, if successful, along with the new node identifier. \verbatim <?xml version="1.0" encoding="UTF-8" ?> <TSRESULT> <SUCCESS>Node created successfully.</SUCCESS> </TSRESULT>
 * \endverbatim
 */
public class TSAddNode {
    // TSAddNode (sessionid, nodeid, word, frequency)

    private static final Integer ISemaphoreNodeID = new Integer(1);

    public static void handleTSapiRequest(APIProps props, PrintWriter out, Connection dbc) throws Exception {
        synchronized (ISemaphoreNodeID) {
            /*
             * SQL> desc node; Name Null? Type ----------------------------------------- -------- ---------------------------- NODEID NOT NULL NUMBER(12) PARENTID NUMBER(9) CORPUSID NOT NULL NUMBER(9)
             * NODETITLE VARCHAR2(256) NODESIZE NUMBER(9) NODEINDEXWITHINPARENT NUMBER(3) DEPTHFROMROOT NUMBER(3) DATESCANNED DATE DATEUPDATED DATE
             */
            String sProp_CorpusID = (String) props.get("CorpusID", true);
            String sProp_NodeID = (String) props.get("NodeID");
            String sProp_ParentID = (String) props.get("ParentID", true);
            String sProp_NodeTitle = (String) props.get("NodeTitle", true);
            String sProp_NodeDesc = (String) props.get("NodeDesc", "");
            String sProp_WordsAndFreqs = (String) props.get("WordsAndFreqs", ""); // word1:8;word2:7;word3:4
            String sProp_NodeSize = (String) props.get("NodeSize", true);
            String sProp_NODEINDEXWITHINPARENT = (String) props.get("NodeIndexInParent", "end");
            String sProp_LinkNodeID = (String) props.get("LinkNodeID");
            String sKey = (String) props.get("SKEY", true);
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            // populate node description with node title if it was not there
            if (sProp_NodeDesc == null) {
                sProp_NodeDesc = sProp_NodeTitle;
            }

            if ((!sProp_ParentID.equals("-1"))
                    && (!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none"))) {

                // for authorization purposes, need to know the corpus id of this node
                String sSQL = " select CorpusId from Node where NodeID = " + sProp_ParentID;
                Statement stmt = dbc.createStatement();
                ResultSet rs = stmt.executeQuery(sSQL);
                rs.next();

                try {
                    int iCorpusId = rs.getInt(1);
                    if (!u.IsAdmin(iCorpusId, out)) {
                        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                    }
                } catch (TSException tse) {
                    EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
                } catch (Exception e) {
                    api.Log.LogError("error in tsaddnode, sql: " + sSQL, e);
                } finally {
                    if (rs != null) {
                        rs.close();
                        rs = null;
                    }
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                }
            }

            if (sProp_NodeID == null) {
                boolean bIndraweb = Session.cfg.getPropBool("Indraweb", false, "false");
                sProp_NodeID = "" + com.indraweb.utils.oracle.UniqueSpecification.getSubsequentNodeID(dbc, bIndraweb);
                System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------- " + sProp_NodeID);
                System.out.println("---------------------------------- sProp_NodeID " + sProp_NodeID);
                System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------- " + sProp_NodeID);

            }

            int iDEPTHFROMROOT = -1;

            if (!sProp_ParentID.equals("-1")) {
                String sSQLGetParentDepthFromRoot = "select DEPTHFROMROOT from node where nodeid = " + sProp_ParentID;
                iDEPTHFROMROOT = (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLGetParentDepthFromRoot, dbc);
                iDEPTHFROMROOT++;
            } else {
                iDEPTHFROMROOT = 0;
            }

            try {
                if (!u.IsAdmin(sProp_CorpusID, out)) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                }

                if (sProp_NODEINDEXWITHINPARENT.equals("end")) {
                    String sSQLGetMaxNIWP =
                            "select max(NodeIndexWithinParent) from node where parentid = " + sProp_ParentID;
                    sProp_NODEINDEXWITHINPARENT = ""
                            + (1 + (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLGetMaxNIWP, dbc));
                }

                Statement statemnt = null;
                try {
                    String sSQL = " update node set nodeindexwithinparent = nodeindexwithinparent + 1"
                            + " where parentid = " + sProp_ParentID + " and nodeindexwithinparent >= " + sProp_NODEINDEXWITHINPARENT;
                    statemnt = dbc.createStatement();
                    statemnt.executeUpdate(sSQL);
                } catch (Exception e) {
                    throw e;
                } finally {
                    if (statemnt != null) {
                        statemnt.close();
                        statemnt = null;
                    }
                }

                // ***************************
                // insert node
                // **************************
                Vector vFieldNames = new Vector();
                Vector vValues = new Vector();

                // http://ip/ts&

                vFieldNames.addElement("NODEID");
                vFieldNames.addElement("PARENTID");
                vFieldNames.addElement("CORPUSID");
                vFieldNames.addElement("NODETITLE");
                vFieldNames.addElement("NODEDESC");
                vFieldNames.addElement("NODESIZE");
                vFieldNames.addElement("NODEINDEXWITHINPARENT");
                vFieldNames.addElement("DEPTHFROMROOT");
                vFieldNames.addElement("DATESCANNED");
                vFieldNames.addElement("DATEUPDATED");
                vFieldNames.addElement("LINKNODEID");

                vValues.addElement(sProp_NodeID);
                vValues.addElement(sProp_ParentID);
                vValues.addElement(sProp_CorpusID);
                vValues.addElement("'" + sProp_NodeTitle + "'");
                vValues.addElement("'" + sProp_NodeDesc + "'");
                vValues.addElement(sProp_NodeSize);
                vValues.addElement(sProp_NODEINDEXWITHINPARENT);
                vValues.addElement("" + iDEPTHFROMROOT);
                vValues.addElement("sysdate");
                vValues.addElement("sysdate");
                if (sProp_LinkNodeID == null) {
                    sProp_LinkNodeID = sProp_NodeID;
                }
                vValues.addElement(sProp_LinkNodeID);

                // if this node will be a LINK (LinkNodeID != NodeID), we must first check to ensure
                // that there is no circular recursion afoot
                Statement stmt = null;
                ResultSet rs = null;
                if (!sProp_LinkNodeID.equals(sProp_NodeID)) {
                    try {
                        String sSQL2 = "select linknodeid from node START WITH nodeid = " + sProp_LinkNodeID + " connect by prior linknodeid = parentid";
                        stmt = dbc.createStatement();
                        rs = stmt.executeQuery(sSQL2);

                        while (rs.next()) {
                            String sDescendentID = rs.getString(1);
                            if (sDescendentID.equals(sProp_ParentID)) {
                                out.println("<DATAERROR>Creating this link would violate data integrity</DATAERROR>");
                                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_DATA_INTEGRITY_VIOLATION);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    } finally {
                        if (rs != null) {
                            rs.close();
                            rs = null;
                        }
                        if (stmt != null) {
                            stmt.close();
                            stmt = null;
                        }
                    }
                }

                try {
                    String sSQLInsert = SQLGenerators.genSQLInsert(
                            "node",
                            vFieldNames,
                            vValues);

                    // If query statement failed, throw an exception
                    JDBCIndra_Connection.executeInsertToDB(sSQLInsert, dbc, false, false);
                    out.println("<DEBUG> node inserted [" + sProp_NodeID + "] corpus [" + sProp_CorpusID + "]</DEBUG>");

                } catch (Exception e) {
                    Log.LogError("why is nodeid already there ?" + e.getMessage() + ":" + sProp_CorpusID);
                    out.println("<DEBUG>why is nodeid already there ?" + e.getMessage() + ":" + sProp_CorpusID + "</DEBUG>");
                    // assume dup record - keep going
                }

                // *************************************
                // insert or update signatures (within loop)
                // *************************************
                /*
                 * SQL> desc signature; Name Null? Type ----------------------------------------- -------- ---------------------------- NODEID NOT NULL NUMBER(12) SIGNATUREWORD VARCHAR2(30)
                 * SIGNATUREOCCURENCES NUMBER(5)
                 */
                // try an insert first, if exception do update
                {
                    vFieldNames = new Vector();

                    vFieldNames.addElement("NODEID");
                    vFieldNames.addElement("SIGNATUREWORD");
                    vFieldNames.addElement("SIGNATUREOCCURENCES");

                    // GET WORDS AND FREQS
                    if (!sProp_WordsAndFreqs.equals("")) {
                        Vector vWordsAndFreqs = com.indraweb.util.UtilStrings.splitByStrLen1(sProp_WordsAndFreqs, ";");
                        Enumeration eWordFreqConcat = vWordsAndFreqs.elements();

                        int iNumSigWordsInserted = 0;
                        while (eWordFreqConcat.hasMoreElements()) {
                            // try sig udpate first then update
                            String sWordColonFreq = (String) eWordFreqConcat.nextElement();
                            Vector vWordThenFreq_in_VIndexLoc0And1 = com.indraweb.util.UtilStrings.splitByStrLen1(sWordColonFreq, ":");
                            String sWord = (String) vWordThenFreq_in_VIndexLoc0And1.elementAt(0);
                            String sFreq = (String) vWordThenFreq_in_VIndexLoc0And1.elementAt(1);
                            Vector vStrings_FieldNValuesUpdate = new Vector();
                            Vector vStrings_FieldNValuesWhere = new Vector();

                            vStrings_FieldNValuesUpdate.addElement("SIGNATUREOCCURENCES=" + sFreq);

                            vStrings_FieldNValuesWhere.addElement("nodeid=" + sProp_NodeID);
                            vStrings_FieldNValuesWhere.addElement("SIGNATUREWORD='" + sWord + "'");

                            // assuming record already there - do update instead.
                            String sSQLsigupdate = SQLGenerators.genSQLUpdate(
                                    "signature",
                                    vStrings_FieldNValuesUpdate,
                                    vStrings_FieldNValuesWhere);
                            //System.out.println("sSQLsigupdate [" + sSQLsigupdate  + "]" );
                            int iNumUpdates = JDBCIndra_Connection.executeUpdateToDB(sSQLsigupdate, dbc, false, 0);
                            if (iNumUpdates == 0) // update failed, try insert - assumes no such record was error above
                            {
                                vValues = new Vector();

                                vValues.addElement(sProp_NodeID);
                                vValues.addElement("'" + sWord + "'");
                                vValues.addElement(sFreq);

                                String sSQLsigInsert = SQLGenerators.genSQLInsert(
                                        "signature",
                                        vFieldNames,
                                        vValues);
                                //System.out.println("sSQLsigInsert [" + sSQLsigInsert + "]" );
                                JDBCIndra_Connection.executeInsertToDB(sSQLsigInsert, dbc, false);
                                iNumSigWordsInserted++;
                            }
                        }  // while ( eWordFreqConcat.hasMoreElements() )
                        // ************************
                        out.println("<DEBUG>[" + iNumSigWordsInserted + "] sig words inserted</DEBUG>");
                    }
                    // ************************
                    dbc.commit();
                }

                // purge signature cache by corpus
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus("TSAddNode", Integer.parseInt(sProp_CorpusID));

                out.println("<NODEID>" + sProp_NodeID + "</NODEID>");
                out.println("<SUCCESS>Node created successfully.</SUCCESS>");
            } catch (TSException tse) {
                EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
            }

        } // synchronized ( ISemaphoreNodeID )
    }
//	public static void dbInsertOrUpdate ( Vector
}

/*
 * sample leaf nodes 90194 parentid 90192 size 58 corpusid 1 91223 91452 91153 91327 90725 91235 91023 91431 91484
 *
 * NODEID ---------- 91186 90071 90189 90104 90747 91058 90529 90323 90230 91141 90487
 */
