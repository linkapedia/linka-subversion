package api.tsnode.musthavegate.components;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * manage the tree
 *
 * @author andres
 */
public class BuilderTree {

    private List<TreeNode> data;
    //mytree is a root node in the tree
    private TreeNode myTree;
    private static final Logger log = Logger.getLogger(BuilderTree.class);

    public BuilderTree(List<TreeNode> data) {
        this.data = data;
    }

    public TreeNode getMyTree() {
        return this.myTree;
    }

    /**
     * build the tree with the list<TreeNode>
     */
    public void build() {
        log.debug("building tree...");
        //search the root
        List<TreeNode> children = new ArrayList<TreeNode>();
        for (TreeNode treenode : this.data) {
            if (treenode.isRoot()) {
                myTree = treenode;
                children.add(myTree);
                break;
            }
        }
        buildRecursively(children);
        log.debug("Finished building tree!");
    }

    /**
     * intern logic to build the tree from the List<Treenode>
     *
     * @param children
     */
    private void buildRecursively(List<TreeNode> children) {
        List<TreeNode> childrenLocal = null;
        for (TreeNode node : children) {
            childrenLocal = new ArrayList<TreeNode>();
            for (TreeNode treenode : this.data) {
                if (treenode.getNodeInfo().getParentid().equals(node.getNodeInfo().getId())) {
                    childrenLocal.add(treenode);
                }
            }
            buildRecursively(childrenLocal);
            node.setChildren(childrenLocal);
        }
    }

    /**
     * get nodes from specific level inclusive
     *
     * @param fromLevel
     * @param toLevel -1 to infinite
     * @return
     */
    public List<TreeNode> getTreeNodes(int fromLevel, int toLevel) {
        List<TreeNode> selectedNodes = new ArrayList<TreeNode>();
        List<TreeNode> startNode = new ArrayList<TreeNode>();
        startNode.add(myTree);
        getTreeNodes(fromLevel, toLevel, startNode, selectedNodes, 0);
        return selectedNodes;
    }

    /**
     * get nodes from level
     *
     * @param level
     * @return
     */
    public List<TreeNode> getTreeNodes(int level) {
        return getTreeNodes(level, level);
    }

    /**
     * logic for the nodes from specific levels
     *
     * @param fromLevel
     * @param toLevel
     * @param children
     * @param list
     * @param currentLevel
     */
    private void getTreeNodes(int fromLevel, int toLevel, List<TreeNode> children, List<TreeNode> list, int currentLevel) {
        List<TreeNode> childrenLocal = null;
        for (TreeNode node : children) {
            if (currentLevel >= fromLevel) {
                if (currentLevel <= toLevel || toLevel == -1) {
                    list.add(node);
                } else {
                    break;
                }
            }
            childrenLocal = node.getChildren();
            getTreeNodes(fromLevel, toLevel, childrenLocal, list, currentLevel + 1);
        }
    }
}
