package api.tsnode.musthavegate.components;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * validate the parameters
 *
 * @author andres
 */
public class TSMusthaveFirstPassController {

    private static final Logger log = Logger.getLogger(TSMusthaveFirstPassController.class);

    public Map<String, String> parseParameters(api.APIProps props) throws InvalidParameterException {
        log.debug("TSMusthaveFirstPassController : parseParameters()");
        String musthavesType = props.getProperty("musthavestype");
        String musthavesScope = props.getProperty("musthavesscope");
        Map<String, String> mapParameters = new HashMap<String, String>();

        if (musthavesType == null || musthavesType.isEmpty()) {
            throw new InvalidParameterException("musthavestype can't be null or empty");
        }
        if (musthavesScope == null || musthavesScope.isEmpty()) {
            throw new InvalidParameterException("musthavesscope can't be null or empty");
        }
        char option1 = musthavesType.toCharArray()[0];
        char option2 = musthavesScope.toCharArray()[0];
        switch (option1) {
            case '1':
                mapParameters.put("option1", "1");
                break;
            case '2':
                mapParameters.put("option1", "2");
                String level = props.getProperty("level");
                if (level == null || level.isEmpty()) {
                    throw new InvalidParameterException("musthavestype = 2 require lebel can't be null or empty");
                }
                mapParameters.put("level", level);
                break;
            case '3':
                mapParameters.put("option1", "3");
                break;
            case '4':
                mapParameters.put("option1", "4");
                String text = props.getProperty("textbyuser");
                if (text == null || text.isEmpty()) {
                    throw new InvalidParameterException("musthavestype = 4 require textbyuser can't be null or empty");
                }
                mapParameters.put("textbyuser", text);
                break;
            default:
                throw new InvalidParameterException("musthavestype invalid = [1 - 4]");
        }

        switch (option2) {
            case '1':
                mapParameters.put("option2", "1");
                mapParameters.put("fromlevel", "1");
                mapParameters.put("tolevel", "-1");
                break;
            case '2':
                mapParameters.put("option2", "2");
                mapParameters.put("fromlevel", "1");
                mapParameters.put("tolevel", "1");
                break;
            case '3':
                mapParameters.put("option2", "3");
                String fromlevel = props.getProperty("fromlevel");
                String tolevel = props.getProperty("tolevel");
                if (fromlevel == null || fromlevel.isEmpty()) {
                    throw new InvalidParameterException(" musthavesscope = 3 require fromlevel can't be null or empty");
                }
                if (tolevel == null || tolevel.isEmpty()) {
                    throw new InvalidParameterException(" musthavesscope = 3 require tolevel can't be null or empty");
                }

                if (!NumberUtils.isNumber(fromlevel)) {
                    throw new InvalidParameterException(" fromlevel only numbers");
                }

                if (!NumberUtils.isNumber(tolevel)) {
                    throw new InvalidParameterException(" fromlevel only numbers -1 to specific infinite");
                }
                if (Integer.parseInt(tolevel) != -1) {
                    if (Integer.parseInt(fromlevel) > Integer.parseInt(tolevel)) {
                        throw new InvalidParameterException(" fromlevel can't be > that tolevel");
                    }
                }

                mapParameters.put("fromlevel", fromlevel);
                mapParameters.put("tolevel", tolevel);
                break;
            default:
                throw new InvalidParameterException("musthavesscope invalid = [1 - 3]");
        }
        return mapParameters;
    }
}