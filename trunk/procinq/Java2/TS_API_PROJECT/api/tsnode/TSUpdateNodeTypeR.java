package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSUpdateNodeTypeR {

    private static final Logger LOG = Logger.getLogger(TSUpdateNodeTypeR.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        LOG.debug("UpdateNodeTypeR");

        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");

        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        boolean b = false;
        try {
            int type = getNodeType(nodeid, dbc);
            if (type != -1) {
                b = updateChildren(nodeid, dbc, type);
            }
        } catch (Exception e) {
            LOG.error("Error UpdateNodeTypeR ", e);
        }
        out.println("<RESULT>" + b + "</RESULT>");
        LOG.debug("FINISHED");
    }

    private static boolean updateChildren(String nodeId, Connection con, int nodetype) throws Exception {
        LOG.debug("updateChildren(String, Connection, int)");
        LOG.debug("NODEID: " + nodeId);
        LOG.debug("NODE_TYPE: " + nodetype);

        String sql = "UPDATE \"SBOOKS\".\"NODE\" n SET n.NODETYPE = '" + nodetype + "' WHERE "
                + "n.NODEID in (SELECT n.NODEID FROM NODE n START WITH n.NODEID = " + nodeId
                + "CONNECT BY PRIOR n.NODEID = n.PARENTID) and n.NODEID <> " + nodeId;

        Statement smt = null;
        try {
            smt = con.prepareStatement(sql);
            smt.executeUpdate(sql);
        } finally {
            if (smt != null) {
                smt.close();
            }
        }
        return true;
    }

    private static int getNodeType(String node, Connection dbc) throws Exception {
        LOG.debug("getNodeType(String, Connection)");
        int result = -1;
        String sql = "SELECT n.NODEID, n.NODETYPE FROM NODE n WHERE n.NODEID = ?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        int n = Integer.parseInt(node);
        try {
            pstmt = dbc.prepareStatement(sql);
            pstmt.setInt(1, n);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = rs.getInt("NODETYPE");
            }
            return result;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
}
