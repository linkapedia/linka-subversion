package api.tsnode;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.DecimalFormat;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.*;

import com.iw.system.User;

/**
 * Move the given node from one location to another.  This function will move all descendents by default ad
 *  reset their depth from root and corpus (if applicable).
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of this node.
 *  @param  ParentID    Unique node identifier of the parent node to be copied into.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSCopyNode&NodeID=2000&ParentID=2006&SKEY=9919294812

 *	@return	NODE of the displaced node
 *  \verbatim
 <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <NODE>
          <NODEID>1087101</NODEID>
          <CORPUSID>300012</CORPUSID>
          <NODETITLE><![CDATA[ Agrochemicals]]> </NODETITLE>
          <PARENTID>1087099</PARENTID>
          <NODEINDEXWITHINPARENT>1</NODEINDEXWITHINPARENT>
          <DEPTHFROMROOT>0</DEPTHFROMROOT>
          <DATESCANNED>null</DATESCANNED>
          <DATEUPDATED>null</DATEUPDATED>
          <NODESTATUS>1</NODESTATUS>
          <NODEDESC><![CDATA[ ]]></NODEDESC>
          <NODESIZE>1</NODESIZE>
          <LINKNODEID>1087101</LINKNODEID>
      </NODE>
 </TSRESULT>
 \endverbatim
 */
public class TSMoveNode {
    private static int Count = 0;
    private static int Total = 0;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sParentID = (String) props.get("ParentID", true);
        String sShowStatus = (String) props.get("ShowStatus");

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
        }

        int iCorpusID = -1;
        int iDepth = -1;

        // first get the total number of children that we will be moving
        String sSQL = "select count(*) from node start with nodeid = " + sNodeID + " connect by prior nodeid = parentid";
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            Total = rs.getInt(1);
        } catch (Exception e) {
            api.Log.LogError("error in tsmoveynode, sql: " + sSQL, e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        // 1) Get the CORPUSID of the new PARENT node
        sSQL = " select CorpusId, DepthFromRoot from Node where NodeID = " + sParentID;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            iCorpusID = rs.getInt(1);
            iDepth = rs.getInt(2);
            if (!u.IsAdmin(iCorpusID, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } catch (Exception e) {
            api.Log.LogError("error in tsmovenode, sql: " + sSQL, e);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        if (!nodeExists(sNodeID, dbc)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_SUCH_OBJECT);
        }
        synchronized (new Integer(iCorpusID)) {
            if (sShowStatus != null)
                moveNodeRecursive(sNodeID, sParentID, iCorpusID + "", out, (iDepth + 1), dbc);
            else
                moveNodeRecursive(sNodeID, sParentID, iCorpusID + "", null, (iDepth + 1), dbc);
        }

        TSGetNodeProps.handleTSapiRequest(props, out, dbc);

        Count = 0;
        Total = 0;
    }

    public static boolean nodeExists(String NodeID, Connection dbc) throws Exception {
        String sSQL = "select nodeid from node where nodeid = " + NodeID;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            int loop = 0;

            while (rs.next()) {
                loop++;
            }

            if (loop == 0) return false;

        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        return true;
    }

    public static void moveNodeRecursive(String NodeID, String ParentID, String CorpusID, PrintWriter out,
                                           int Depth, Connection dbc)
            throws Exception {
        //api.Log.Log("Call into copyNodeRecursive with: NID: "+NodeID+" PID: "+ParentID+" CID: "+CorpusID+" Depth: "+Depth);
        DecimalFormat twoDigits = new DecimalFormat("0");

        Vector vChildren = new Vector();

        // insert the copied node
        String sSQL = "update node set parentid = "+ParentID+", corpusid = "+CorpusID+", depthfromroot = "+Depth+" "+
                      "where nodeid = "+NodeID;

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        long percent = ((Count + 1) * 100) / Total;
        if (out != null) {
            out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
            out.flush();
        }
        Count++;

        // now look for children and insert them, if applicable
        sSQL = "select nodeid from node where parentid = " + NodeID;

        //api.Log.Log("Get children: "+sSQL);

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            while (rs.next()) {
                vChildren.add(rs.getString(1));
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        //api.Log.Log(vChildren.size()+" children found, invoking copyNodeRecursive.");
        for (int i = 0; i < vChildren.size(); i++) {
            moveNodeRecursive((String) vChildren.elementAt(i), NodeID, CorpusID, out, (Depth+1), dbc);
        }

    }
}
