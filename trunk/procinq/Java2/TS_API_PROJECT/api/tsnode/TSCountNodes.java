package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 *
 * show children number
 */
public class TSCountNodes {

    private static final Logger log = Logger.getLogger(TSCountNodes.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        log.debug("TSCountNodes");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String nodeid = (String) props.get("nodeid");

        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        out.println("<RESULT>");
        try {
            out.println("<COUNT>" + getNumberChildren(nodeid, dbc) + "</COUNT>");
        } catch (SQLException e) {
            out.println("ERROR!");
        }
        out.println("</RESULT>");
    }

    private static String getNumberChildren(String nodeId, Connection dbc) throws SQLException {
        log.debug("getNumberChildren");
        String sql = "select count(*) from node "
                + "start with node.nodeid=? "
                + "connect by prior node.nodeid=node.parentid ";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = dbc.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(nodeId));
            rs = pstmt.executeQuery();
            String nodeCount = "NOT FOUND!";
            if (rs.next()) {
                nodeCount = String.valueOf(rs.getInt(1));
            }
            return nodeCount;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }

    }
}