package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.results.*;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * This API call has been deprecated.  Please use the TSGetNodeProps API call.
 * @note    This API call has been deprecated.
 */
public class TSGetNodeNotificationList {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDate = (String) props.get ("Date", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        // ** DEPRECATED, RETURN
        if (true) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE);

        LDAP_Connection lc = null;
		try {
			Hashtable htNodeHash = new Hashtable(); // contains all changed nodes
			Hashtable htNodeFinalHash = new Hashtable(); // contains all changed and tracked nodes

			// GET ALL MATCHING NODES FROM CLIENT DATABASE *******************
			String sSQL = " select distinct(nodeid) from nodedocument where "+
						  " datescored > to_date('"+sDate+"', 'MM/DD/YYYY HH24:MI:SS') ";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			String sNodeID = null;
			
			while ( rs.next() ) {
				sNodeID = rs.getString(1);
				htNodeHash.put(sNodeID, "1");
			}
			rs.close(); stmt.close();
			
			// GET ALL MATCHING NODES FROM SERVER DATABASE *******************
			HashTree htArguments = new HashTree();
			String sTaxServer = com.indraweb.execution.Session.cfg.getProp("TaxServer")+"/servlet/ts?fn=";
			htArguments.put("whichdb", "SVR");
			htArguments.put("Date", sDate);
			InvokeAPI API = new InvokeAPI ("tsapisvr.TSGetServerNodesChanged", htArguments, sTaxServer);
			HashTree htResults = API.Execute(out);
			
			if (htResults.containsKey("NODES")) {
				HashTree htNodes = (HashTree) htResults.get("NODES");
				Enumeration eNID = htNodes.elements();
				
				while (eNID.hasMoreElements()) {
					sNodeID = (String) eNID.nextElement();
					htNodeHash.put(sNodeID, "1");
				}
			} 

			// GET LIST OF NODE IDs TRACKED BY ANY USER WITHIN THE SYSTEM *****
			String sLdapUsername = com.indraweb.execution.Session.cfg.getProp("LdapUsername");
			String sLdapPassword = com.indraweb.execution.Session.cfg.getProp("LdapPassword");	
			lc = new LDAP_Connection(sLdapUsername, sLdapPassword, out);
			Vector vUsers = vUsers = lc.GetAllUsers(); 
			Enumeration eV = vUsers.elements();

			// for looping through each user ...
			while (eV.hasMoreElements()) {
				User uA = (User) eV.nextElement();
				Hashtable ht = uA.GetNodeHash();

				// for looping through each node tracked by that user ...
				Enumeration eH = ht.keys();
				while (eH.hasMoreElements()) {
					sNodeID = (String) eH.nextElement();
					// if the node tracked has changed, put it into the final bucket
					if (htNodeHash.containsKey(sNodeID)) {
						htNodeFinalHash.put(sNodeID, "1");
					}
				}
			}
			
			// CYCLE THROUGH COMBINED HASH AND PRINT NODE RESULTS *************
			if (htNodeFinalHash.isEmpty()) {
				throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);	
			} else {
				out.println("<NODES>");
				Enumeration eNodes = htNodeFinalHash.keys();
				while (eNodes.hasMoreElements()) {
					sNodeID = (String) eNodes.nextElement();
					out.println("   <NODEID>"+sNodeID+"</NODEID>");
				}
				out.println("</NODES>");
			}
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		} finally { lc.close(); }
	}
}
