package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.system.User;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import org.apache.log4j.Logger;

/**
 * Update the corpus source of a given node within the system.
 * @authors Indraweb Inc, All Rights Reserved.
 * @param	SKEY Session key corresponding to the current user session.
 * @param  NodeID  Unique node identifier of the node whose children are being retrieved.
 * @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodeSource&NodeID=1087101&Source=Test&SKEY=9919294812
 * @return SUCCESS if update was successful
 *  \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 *   <TSRESULT>
 *       <SUCCESS>Node source updated successfully.</SUCCESS>
 *    </TSRESULT>
 * \endverbatim
 */
public class TSUpdateNodeSource {
    private static final Logger log = Logger.getLogger(TSUpdateNodeSource.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        ByteArrayOutputStream bao=((ByteArrayOutputStream) props.get("inputstream"));
        if(bao==null)
        {
            return;
        }
        byte[] bytes = bao.toByteArray();
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        String sSource= new String(bytes,"UTF-8");
        Statement stmt = null;
        try {
            if (sNodeID.equals("-1")) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
            }

            String sSQL = "update node set dateupdated = SYSDATE where nodeid = " + sNodeID;
            try {
                //log.debug("Writing file recived from Client: handlerFile" + sNodeID + ".txt");
                //FileUtils.writeFile("handlerFile" + sNodeID + ".txt", sSource);
                UtilClob.insertClobToNodeData(new Long(sNodeID).longValue(), sSource, dbc);
                stmt = dbc.createStatement();
                stmt.executeUpdate(sSQL);

            } catch (Exception e) {
                api.Log.LogError(e);
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
            } finally {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            out.println("<SUCCESS>Node source updated successfully.</SUCCESS>");
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}
