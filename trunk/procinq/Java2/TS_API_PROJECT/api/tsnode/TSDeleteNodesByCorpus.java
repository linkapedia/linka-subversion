package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Delete all nodes from a given corpus.   These nodes will be deleted immediately and are not recoverable.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID  Unique corpus identifier of the corpus; all nodes will be removed from this corpus
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSDeleteNodesByCorpus&CorpusID=2s&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node deleted successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSDeleteNodesByCorpus
{
	// delete all the nodes in a given corpus
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;

		try {
            // check authorization level before completing this irreversable action
			if ((u == null) || (!u.IsAdmin(sCorpusID, out))) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            String sSQL = "delete from Node where CorpusId = "+sCorpusID;
			stmt = dbc.createStatement();

			stmt.executeUpdate (sSQL);
            out.println("<SUCCESS>Nodes deleted successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { if (stmt != null) { stmt.close(); }}
	}
}
