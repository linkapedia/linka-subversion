package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Remove a single node-document relationship from the server.  This removal is immediate and permanent.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID    Unique document identifier of the node-document relationship being purged.
 *  @param  NodeID  Unique node identifier of the node-document relationship being purged.
 *  @param  permanent (optional)    If this identifier is present, the relationship is permanently purged from the db.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSRemoveNodeDocument&NodeID=2000&DocumentID=15002&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationships deleted successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveNodeDocument
{
	// remove all node relationships for this document
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDocumentID = (String) props.get ("DocumentID", true);
        String sNodeID = (String) props.get ("NodeID", true);
        String permanent = (String) props.get ("permanent");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
		try {
            // check authorization level before completing this irreversable action
			if ((u == null) || (!u.IsMember(out))) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            String sSQL;

            if (permanent == null) {
                sSQL = "update NodeDocument set score1 = -10.0 where NodeID = "+sNodeID+" and DocumentId = "+sDocumentID;
            } else { sSQL = "delete from NodeDocument where NodeID = "+sNodeID+" and DocumentId = "+sDocumentID; }
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Error erasing node document relationships.</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
            out.println("<SUCCESS>Node document relationships deleted successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { if (stmt != null) { stmt.close(); }}
	}
}
