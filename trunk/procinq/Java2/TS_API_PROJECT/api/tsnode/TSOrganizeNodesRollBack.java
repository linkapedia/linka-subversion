package api.tsnode;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import com.iw.db.ConnectionFactory;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSOrganizeNodesRollBack {

    private static final Logger LOG = Logger.getLogger(TSOrganizeNodesRollBack.class);
    private static final int BATCH_UPDATER = 100;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        LOG.debug("TSOrganizeNodesRollBack");
        try {
            /*Hack hack, when we have a lot nodes the api connection close the stream
             I am getting our connection now
             also, I need that connection is closed after use
             */
            dbc = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            NodeUtils nu = new NodeUtils(dbc);
            //validate AUTHORIZED
            String sKey = (String) props.get("SKEY", true);
            //get nodeid to start with the process
            String nodeid = (String) props.get("nodeid");
            if (nodeid == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nodeid.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            //nodeId is me, the current selection on procinq
            List<String> myChildren = getChildren(dbc, nodeid);
            Map<String, List<String>> myGrandChildren = getChildren2(dbc, myChildren);
            applyProcess(dbc, nodeid, myGrandChildren);
            out.println("<RESULT>Finished</RESULT>");
        } catch (Exception e) {
            throw e;
        } finally {
            if (dbc != null) {
                dbc.close();
            }
        }
    }

    private static List<String> getChildren(
            Connection con,
            String nodeId) throws SQLException {
        LOG.debug("TSOrganizeNodesRollBack: getChildren");
        String sql = "SELECT NODE.NODEID"
                + " FROM NODE WHERE NODE.PARENTID=?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        List<String> listResult = new ArrayList<>();
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(nodeId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String node = String.valueOf(rs.getInt("nodeid"));
                listResult.add(node);
            }
            return listResult;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    private static Map<String, List<String>> getChildren2(
            Connection con,
            List<String> list) throws IllegalArgumentException, SQLException {

        if (list == null) {
            throw new IllegalArgumentException("list can't be null");
        }

        LOG.debug("TSOrganizeNodesRollBack: getChildren2");
        String sql = "SELECT NODE.NODEID"
                + " FROM NODE WHERE NODE.PARENTID=?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        List<String> listResult;
        Map<String, List<String>> mapResult = new HashMap<>();
        try {
            pstmt = con.prepareStatement(sql);
            for (String nodeId : list) {
                listResult = new ArrayList<>();
                pstmt.setInt(1, Integer.parseInt(nodeId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    String node = String.valueOf(rs.getInt("nodeid"));
                    listResult.add(node);
                }
                mapResult.put(nodeId, listResult);
            }
            return mapResult;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    /**
     * delete each folder and set the children as my children
     *
     * @param nodeId
     * @param words
     */
    private static void applyProcess(
            Connection con,
            String nodeId,
            Map<String, List<String>> map) throws Exception {

        if (con == null) {
            throw new IllegalArgumentException("Connection can't be null");
        }
        if (nodeId == null || nodeId.isEmpty()) {
            throw new IllegalArgumentException("nodeId can't be null");
        }
        if (map == null) {
            throw new IllegalArgumentException("words can't be null");
        }
        PreparedStatement pstmt = null;
        con.setAutoCommit(false);
        try {
            int cont = 0;
            for (Map.Entry<String, List<String>> row : map.entrySet()) {
                List<String> currentList = row.getValue();
                String currentNodeId = row.getKey();

                //delete the folder
                String sql = "DELETE FROM NODE "
                        + "WHERE NODEID = ?";
                pstmt = con.prepareStatement(sql);
                pstmt.setString(1, currentNodeId);
                pstmt.executeUpdate();

                //save all nodes in the parent folder
                sql = "UPDATE NODE SET "
                        + "PARENTID = ? "
                        + "WHERE NODEID = ?";
                pstmt = con.prepareStatement(sql);
                for (String id : currentList) {
                    pstmt.setString(1, nodeId);
                    pstmt.setString(2, id);
                    pstmt.addBatch();
                    cont++;
                    if (cont == BATCH_UPDATER) {
                        //update the batch
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        cont = 0;
                        continue;
                    }
                }
                //the last data
                if (cont != 0) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                }
            }
            //commit all the changes
            con.commit();
        } catch (Exception e) {
            LOG.error("Error updating nodes", e);
            try {
                //rollback all if I found a error
                con.rollback();
            } catch (SQLException ex) {
                throw ex;
            }
            throw e;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            //Unblock the method
            com.indraweb.utils.oracle.UniqueSpecification.processAlphaNodes = false;
        }
    }
}
