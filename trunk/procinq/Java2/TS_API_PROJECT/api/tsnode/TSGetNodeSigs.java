package api.tsnode;


import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.execution.Session;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 * Retrieve a list of all signatures associated with the given node.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY            Session key corresponding to the current user session.
 *	@param  NodeID          Unique node identifier.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodeSigs&NodeID=999012&SKEY=8919294812

 *	@return	a series of SIGNATURE objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
<TSRESULT>
<CALLCOUNT_FN>1</CALLCOUNT_FN>
<TITLECOUNT>0<TITLECOUNT>
<SIGNATURES>
   <SIGNATURE>
   <WORD><![CDATA[ford]]></WORD>
   <WEIGHT>64</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[president]]></WORD>
   <WEIGHT>33</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[gerald]]></WORD>
   <WEIGHT>19</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[house]]></WORD>
   <WEIGHT>13</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[gerald r ford]]></WORD>
   <WEIGHT>10</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[president ford]]></WORD>
   <WEIGHT>10</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[michigan]]></WORD>
   <WEIGHT>9</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[policy]]></WORD>
   <WEIGHT>9</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[rapids]]></WORD>
   <WEIGHT>9</WEIGHT>
   </SIGNATURE>
   <SIGNATURE>
   <WORD><![CDATA[congress]]></WORD>
   <WEIGHT>8</WEIGHT>
   </SIGNATURE>
</SIGNATURES>
<CLASSLOAD>Wed Apr 14 11:09:20 EDT 2004</CLASSLOAD>
<CALLCOUNT>0</CALLCOUNT>
<TIMEOFCALL_MS>121</TIMEOFCALL_MS>
</TSRESULT>
 \endverbatim
 */
public class TSGetNodeSigs
{
	private static int iCallCounter = 0;
	private static final Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

	// TSGetNodeSigs (sessionid, nodeid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		synchronized (ICallCounterSemaphore)
			{ iCallCounter++; }
		out.println ("<CALLCOUNT_FN>" + iCallCounter + "</CALLCOUNT_FN>");
		String sNodeID = (String) props.get ("NodeID", true);
        String sLang = (String) props.get ("lang", "EN");
		String sKey = (String) props.get("SKEY", true);
		//boolean bProp_ThesaurusExpand = props.getbool("ThesaurusExpand", "false");
		boolean bProp_TitleInsert = props.getbool("TitleInsert", "false");
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusId, nodetitle, nodesize, parentid from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();

			int iCorpusId = -1;
            try { iCorpusId = rs.getInt(1); }
            catch (Exception e) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND); }

			String sNodeTitle = rs.getString(2);
			int iNodeSize = rs.getInt(3);
			int iNodeIDParent = rs.getInt(4);
			rs.close(); stmt.close();

            // debug
			if ((!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) &&
                (!u.IsAuthorized(iCorpusId, out)))
            { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            sSQL = " select SignatureWord, SignatureOccurences from Signature "+
   			       " where NodeId = " + sNodeID + " and lang = '"+sLang+"' order by SignatureOccurences desc";

			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);
 
			/*
		      <SIGNATURES>
		           <SIGNATURE>
		           <WORD>signature word</WORD>
		           <WEIGHT>signature weight</WEIGHT>
		           </SIGNATURE>
		      </SIGNATURES>

			*/

            Vector vStrSigWords = new Vector();
            Vector vIntSigCounts = new Vector();

//            HashSet hsTitleWords = null;
//            String[] sArrNodeTitleCleanedStopped  = null;
//            if ( bProp_TitleInsert )
//            {
//                sArrNodeTitleCleanedStopped =
//                   UtilTitleAndSigStringHandlers.convertTitleToSigStyleSArr ( sNodeTitle , false);
//                hsTitleWords = new HashSet ();
//                UtilSets.addArrayToHS(hsTitleWords, sArrNodeTitleCleanedStopped);
//            }
//
//            int iMaxSigCount = -1;
            while ( rs.next() )
            {

                vStrSigWords.addElement(rs.getString(1));
                Integer ICount = new Integer (rs.getString(2)) ;
                vIntSigCounts.addElement(ICount);
            }
            // add title terms to sig list
            //api.Log.Log ("vStrSigWords post title [" +  UtilSets.vToStr(vStrSigWords)+ "]" );


            rs.close();
		    stmt.close();

            emitTermAndThesSigs (
                    vStrSigWords,
                    vIntSigCounts,
                   // vArrUnderThesWords,
                    sNodeID,
                    out,
                    2
                );
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}

    public static void emitTermAndThesSigs(Vector vStrSigWords,
                                          Vector vIntSigCounts,
                                          //Vector[] vArrUnderThesWords,
                                          String sNodeID,
                                          PrintWriter out,
                                          int iTitleCount)
    {
        out.println ("<TITLECOUNT>"+iTitleCount+"</TITLECOUNT>");
        int loop = 0;
        for ( int iTerm=0; iTerm < vStrSigWords.size(); iTerm++)
        {
            // Since we cannot get the number of results from the Result Object, we
            //  need to count with a loop counter.   If results are 0 we want to return
            //  an exception (error) without printing any streams.   To do this, hold off
            //  on writing out the initial stream <SIGNATURES> until we have found at
            //  least one result.   -MAP 10/15/01
            loop = loop + 1;
            if (loop == 1) { out.println ("<SIGNATURES> "); }

            String sWord = (String) vStrSigWords.elementAt(iTerm);
            Integer IWeight = (Integer) vIntSigCounts.elementAt(iTerm);

            out.println ("   <SIGNATURE> ");
            out.println ("   <WORD><![CDATA["+sWord+"]]></WORD>");
            out.println ("   <WEIGHT>"+IWeight+"</WEIGHT>");
            //            if ( vArrUnderThesWords != null && vArrUnderThesWords[iTerm] != null) {
            //                out.println ("      <THESAURUSTERM> ");
            //                for ( int iThesTerm=0; iThesTerm < vArrUnderThesWords[iTerm].size(); iThesTerm++)
            //                    out.println ("      <WORD><![CDATA["+vArrUnderThesWords[iTerm].elementAt(iThesTerm)+"]]></WORD>");
            //                out.println ("      </THESAURUSTERM> ");
            //            }
            out.println ("   </SIGNATURE>");
        }
        // If no results found, throw an exception
        if ( loop == 0) {
            out.println ( "<DEBUG>No signatures found for Node ID: "+sNodeID+" </DEBUG>");
            //throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_SIGS_THIS_NODE);
        }
        else if ( loop > 0 )
            out.println("</SIGNATURES> ");


    }
}
