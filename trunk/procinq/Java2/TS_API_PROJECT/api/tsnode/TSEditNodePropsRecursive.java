/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: May 20, 2003
 * Time: 3:42:35 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * This API call has been deprecated, and should no longer be used.
 * @note    This API call has been deprecated.
 */
public class TSEditNodePropsRecursive {
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeList = (String) props.get ("NodeList", true);
		String sParentID = (String) props.get ("ParentID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        boolean bAuthorized = false; boolean bSuccess = true;

        // ** DEPRECATED, RETURN
        //if (true) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE);

        Statement stmt = null; ResultSet rs = null;

		try {
            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // Loop through the node list
            int iIndex = 0;
			while (sNodeList.indexOf(",") != -1) {
				String sNodeID = sNodeList.substring(iIndex, sNodeList.indexOf(","));

                if (!bAuthorized && (!CheckAuthorization(dbc,u,sNodeID,out))) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                } else { bAuthorized = true; }

                if (!SetNewParent(dbc, sParentID, sNodeID)) { bSuccess = false; }
                if (!SetAllChildren(dbc, sParentID, sNodeID)) { bSuccess = false; }

                sNodeList = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sNodeList,sNodeID+",",""));
			}
            if (sNodeList.length() != 0) {
                if (!bAuthorized && (!CheckAuthorization(dbc,u,sNodeList,out))) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                } else { bAuthorized = true; }

                if (!SetNewParent(dbc, sParentID, sNodeList)) { bSuccess = false; }
                if (!SetAllChildren(dbc, sParentID, sNodeList)) { bSuccess = false; }
			}

            if (bSuccess) { out.println("<SUCCESS>Nodes moved successfully.</SUCCESS>"); }

 		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}

    // loop through each node, and ensure that the node is in the same corpus as the new or existing parent.  note
    //   this function is recursive.
    private static boolean SetAllChildren (Connection dbc, String sParentID, String sNodeID) throws Exception {
        while (GetCorpusFromNode(dbc, sParentID) != GetCorpusFromNode(dbc, sNodeID)) {
            Statement stmt = null; ResultSet rs = null;
            try {
                SetParentNodeCorpus(dbc, GetCorpusFromNode(dbc, sParentID), sNodeID);

                // check all of this node's children as well
                String sSQL = "select nodeid, corpusid from node start with parentid = "+sNodeID+" connect by prior nodeid = parentid";
                api.Log.Log("SQL(1): "+sSQL);
                stmt = dbc.createStatement(); rs = stmt.executeQuery (sSQL);
                Vector v = new Vector();

                // add node children into a vector.  I want to make sure db cursors are closed before proceeding, though.
                while ( rs.next() ) { v.add((String) rs.getString(1)); }
                rs.close(); stmt.close();

                Enumeration eV = v.elements();
                while (eV.hasMoreElements()) { String s = (String) eV.nextElement(); SetParentNodeCorpus(dbc, GetCorpusFromNode(dbc, sParentID), s); }

                return true;
            } catch (Exception e) { api.Log.LogError(e); return false;
            } finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }
        }
        return true;
    }

    private static boolean SetParentNodeCorpus(Connection dbc, int iCorpusID, String sNodeID) throws Exception {
        return SetParentNodeCorpus(dbc, ""+iCorpusID, sNodeID);
    }
    private static boolean SetParentNodeCorpus(Connection dbc, String sCorpusID, String sNodeID) throws Exception {
        Statement stmt = null;
        try {
            // set the corpus id to be equal to that of the parent
            String sSQL = "update node set corpusid = "+sCorpusID+" where nodeid = "+sNodeID;
            api.Log.Log("SQL(2): "+sSQL);
            stmt = dbc.createStatement();
            if (stmt.executeUpdate (sSQL) == 0) { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND); }
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (stmt != null) { stmt.close(); stmt = null; }}

        return true;
    }

    private static boolean SetNewParent(Connection dbc, String sParentID, String sNodeID) throws Exception {
        Statement stmt = null;
        try {
            // set the corpus id to be equal to that of the parent
            String sSQL = "update node set parentid = "+sParentID+", dateupdated = sysdate where nodeid = "+sNodeID;
            api.Log.Log("SQL(3): "+sSQL);
            stmt = dbc.createStatement();
            if (stmt.executeUpdate (sSQL) == 0) { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND); }
        } catch (Exception e) { api.Log.LogError(e); return false;
        } finally { if (stmt != null) { stmt.close(); stmt = null; }}

        return true;
    }

    private static boolean CheckAuthorization (Connection dbc, User u, String sNodeID, PrintWriter out) throws Exception {
        // for authorization purposes, need to know the corpus id of this node
        int iCorpusId = GetCorpusFromNode(dbc, sNodeID);
        if (!u.IsAdmin(iCorpusId, out)) { return false; }
        else { return true; }
    }

    private static int GetCorpusFromNode (Connection dbc, String sNodeID) throws Exception {
        Statement stmt = null; ResultSet rs = null;
        int iCorpusID = -1;

        try {
            String sSQL = " select CorpusId from Node where NodeID = " +sNodeID;
            api.Log.Log("SQL(4): "+sSQL);
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL); rs.next();
            iCorpusID = rs.getInt(1);

        } catch (Exception e) {
            api.Log.LogError(e);
            return -1;
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
        return iCorpusID;
    }
}
