package api.tsnode;

import java.io.*;
import java.sql.*;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.classification.Data_WordsToNodes;
import com.iw.db.ConnectionFactory;
import com.iw.system.User;
import com.linkapedia.procinq.server.node.NodeDao;

public class TSDeleteNodeBatch {

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        String sKey = (String) props.get("SKEY", true);
        String corpusId = (String) props.get("corpusId");
        String nodeId = (String) props.get("nodeId");
        String fromLevel = (String) props.get("fromLevel");
        if (corpusId == null || nodeId == null || fromLevel == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (corpusId.isEmpty() || nodeId.isEmpty() || fromLevel.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
        if (!u.IsAdmin(corpusId, out)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        Connection con = null;
        try {
            con = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            NodeDao.deleteNodesByIdFromLevel(
                    Integer.parseInt(nodeId),
                    Integer.parseInt(fromLevel), con);
            Data_WordsToNodes.invalidateNodeSigCache_byCorpus("TSDeleteNode", Integer.parseInt(corpusId));
            out.println("<SUCCESS>Process finished ok</SUCCESS>");
        } finally {
            con.close();
        }
    }
}
