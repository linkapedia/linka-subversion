package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.iw.system.Node;
import com.indraweb.database.*;
import api.Log;

/**
 *   (2) separate topic name at commas, making all, except first, as must haves
 *   for example:
 *    original topic = asbestos-cement board, asbestos-cement wallboard, asbestos sheeting
 *    new topic = asbestos-cement board
 *    new must haves = asbestos-cement wallboard & asbestos sheeting
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  Unique node identifier of this node.
 *  @param  NodeTitle The node title currently assigned to this node.
 *  @param  Recursive   If true, all node descendents will be organized.  Default is false.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSStripChildren&NodeID=2000&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Parens stripped successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSCommasToMustHaveRecursive {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sRecursive = (String) props.get("recursive");
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            // for authorization purposes, need to know the corpus id of this node
            String sSQL = " select CorpusId from Node where NodeID = " + sNodeID;
            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);
            rs.next();

            int iCorpusId = rs.getInt(1);
            rs.close();
            stmt.close();

            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            boolean bRecurse = false;
            if (sRecursive != null) {
                bRecurse = true;
            }

            Node n = null;
            try {
                sSQL = Node.getSQL(dbc)+" where NodeId = " + sNodeID;
                stmt = dbc.createStatement();
                rs = stmt.executeQuery (sSQL);

                while ( rs.next() ) {
                    n = new Node(rs);
                }
            } catch (Exception e) { return;
            } finally {
                rs.close(); stmt.close(); rs = null; stmt = null;
            }

            processNode(n, dbc, bRecurse);
            out.println("<SUCCESS>Nodes processed successfully.</SUCCESS>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }

    public static void processNode(Node n, Connection dbc, boolean bRecurse)
            throws Exception {

        setNodeTitle(n, dbc);

        if (bRecurse) {
            String sSQL = Node.getSQL(dbc)+" where n.parentid = " + n.get("NODEID") +" order by lower(nodetitle) asc";
            Statement stmt = null;
            ResultSet rs = null;

            Vector vChildrenToStrip = new Vector();

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery (sSQL);
                int i = 0;

                while (rs.next()) {
                    i++; Node n2 = new Node(rs);
                    if (bRecurse) { vChildrenToStrip.add(n2); }
                }
            } catch (Exception e) { throw e; }
            finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }

            for (int i = 0; i < vChildrenToStrip.size(); i++)
                processNode((Node) vChildrenToStrip.elementAt(i), dbc, true);
        }
    }

    public static void setNodeTitle(Node n, Connection dbc)
            throws Exception {

        String musthave = ""; String lang = "EN";
        String title = n.get("NODETITLE");

        String[] titar = title.split(",");
        if (titar.length > 1) {
            // step 1: create the must have terms
            for (int i = 1; i < titar.length; i++) {
                musthave = titar[i].toLowerCase().trim();
                Statement stmt = null;
                try {
                    String sSQL = "insert into musthave (NodeId, MustWord, Lang) values ("+n.get("NODEID")+", '"+musthave+"', '"+lang+"')";
                    api.Log.Log("SQL: "+sSQL);
                    stmt = dbc.createStatement();

                    // If query statement failed, throw an exception
                    if (stmt.executeUpdate (sSQL) == 0) {
                        System.out.println ( "<DEBUG>Failed to insert: ("+n.get("NODEID")+", '"+musthave+"', '"+lang+"') </DEBUG>");
                    }
                } catch (Exception e) {
                    System.out.println ( "<DEBUG>Failed to insert: ("+n.get("NODEID")+", '"+musthave+"', '"+lang+"') </DEBUG>");
                } finally {
                    if (stmt != null) { stmt.close(); stmt = null; }
                }
            }

            // step 2: alter the node title
            try {
                title = titar[0].trim();
                String sSQL = "update node set nodetitle = '" + title + "', dateupdated = sysdate where NodeID = "+n.get("NODEID");
                Statement stmt = null;

                try {
                    stmt = dbc.createStatement(); stmt.executeUpdate(sSQL);
                } catch (Exception e) { throw e; }
                finally {
                    if (stmt != null) { stmt.close(); stmt = null; }
                }
            } catch (Exception e) {
                e.printStackTrace(System.err);
                throw new Exception("Could not change node title to '"+title+"'.  See error log for more details.");
            }
        }

        return;
    }
}
