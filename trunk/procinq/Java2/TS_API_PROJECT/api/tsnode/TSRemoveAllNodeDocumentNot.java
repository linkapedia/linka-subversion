package api.tsnode;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Remove all node-document relationships where the document ID is X but the node ID is not in the list provided.
 *  This removal is immediate and permanent.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  DocumentID    Unique document identifier of this node-document relationship being purged
 *  @param  NodeList    List of comma-separated node identifiers indicating which nodes should be protected from the delete operation.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSRemoveAllNodeDocumentNot&NodeList=2000,2001&DocumentID=15002&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Node document relationships deleted successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveAllNodeDocumentNot
{
	// remove all node relationships for this document
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDocumentID = (String) props.get ("DocumentID", true);
        String sNodeList = (String) props.get ("NodeList", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        String sSQL = "";
        Statement stmt = null;
		try {
            // check authorization level before completing this irreversable action
			if ((u == null) || (!u.IsMember(out))) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            sSQL = "update NodeDocument set score1 = -10.0 where DocumentId = "+sDocumentID+" and NodeID NOT in ("+sNodeList+")";
			stmt = dbc.createStatement();
            api.Log.Log("query: "+sSQL);

            out.println("<SUCCESS>Node document relationships deleted successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally { if (stmt != null) { stmt.close(); }}
	}
}
