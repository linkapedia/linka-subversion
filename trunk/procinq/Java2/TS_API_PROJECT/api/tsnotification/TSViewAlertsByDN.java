package api.tsnotification;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.iw.system.User;

/**
 * Return a list of alerts for a given user identifier.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  UserID  Unique user identifier of the user owning the alert.
 *
 *  @note   http://ITSSERVER/servlet/ts?fn=tsnode.TSViewAlertsByDN&UserID=...&SKEY=925959622

 *	@return A series of ITS alert objects.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <ALERTS>
            <ALERT>
                <ALERTID>1</ALERTID>
                <ALERTNAME>SELECT <DOCUMENT> WHERE DOCTITLE='anthrax'</ALERTNAME>
                <USERDN>mhoey</USERDN>
                <EMAIL>mhoey@indraweb.com</EMAIL>
                <FROMEMAIL>mhoey@indraweb.com</FROMEMAIL>
                <QUERY><![CDATA[SELECT <DOCUMENT> WHERE DOCTITLE='anthrax']]></QUERY>
                    <NOTES><![CDATA[None]]></NOTES>
                <RUNFREQ>-1</RUNFREQ>
                <LASTRUN>2003-03-10 17:39:31.0</LASTRUN>
                <MODDATE>09-APR-03</MODDATE>
            </ALERT>
 ...
  \endverbatim
 */
public class TSViewAlertsByDN
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        // both a user identifier and e-mail address is required
		String sUserID = (String) props.get ("UserID", true);

        ResultSet rs = null; Statement stmt = null;
        String sEncode = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (u == null) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

			String sSQL = " select AlertID, AlertName, Email, CQL, RunFrequency, LastRunDate, FromEmail,"+
                          " Notes, to_char(createdate, 'DD-MON-YY'), UserDN";
            sSQL = sSQL + ", IDRAClanguage, IDRACregion, IDRACkeywords, IDRACabstract, IDRACbibliography"+
                          ", IDRACtitle, IDRACfulltext";
            sSQL = sSQL + " from Notification";

            if (!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none"))
                sSQL = sSQL + " where lower(UserDN) = lower('"+sUserID+"')";

            sSQL = sSQL + " order by createdate desc";

            api.Log.Log("Debug: "+sSQL);

            //if (true) { out.println("<DEBUG>"+sSQL+"</DEBUG>"); }
			stmt = dbc.createStatement(); rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
                loop++;
                if (loop == 1) { out.println("<ALERTS>"); }

 				out.println (" <ALERT>");
				out.println ("   <ALERTID>"+rs.getString(1)+"</ALERTID>");
                out.println ("   <ALERTNAME><![CDATA["+rs.getString(2)+"]]></ALERTNAME>");
                out.println ("   <USERDN>"+rs.getString(10)+"</USERDN>");
                out.println ("   <EMAIL>"+rs.getString(3)+"</EMAIL>");
                out.println ("   <FROMEMAIL>"+rs.getString(7)+"</FROMEMAIL>");
                if (sEncode != null) {
                    out.println ("   <QUERY><![CDATA["+URLEncoder.encode(rs.getString(4), "UTF-8")+"]]></QUERY>");
                } else {
                    out.println ("   <QUERY><![CDATA["+rs.getString(4)+"]]></QUERY>");
                }
                String sNotes = rs.getString(8);
                out.print ("      <NOTES><![CDATA[");
                if (sNotes == null) { sNotes = "None"; }
				for (int k = 0; k < sNotes.length(); k++) {
					int ichar = (int) sNotes.charAt(k);
					if ((ichar != 13) && (ichar != 10)) { out.print(sNotes.charAt(k)); }
				}
				out.println ("]]></NOTES>");
                out.println ("   <RUNFREQ>"+rs.getString(5)+"</RUNFREQ>");
                out.println ("   <LASTRUN>"+rs.getString(6)+"</LASTRUN>");
                out.println ("   <MODDATE>"+rs.getString(9)+"</MODDATE>");

                // new fields
                if (rs.getString(11) != null) {
                    out.println ("   <LANGUAGE>"+rs.getString(11)+"</LANGUAGE>");
                }
                if (rs.getString(12) != null) {
                    out.println ("   <REGION>"+rs.getString(12)+"</REGION>");
                }
                if (rs.getString(13) != null) {
                    out.println ("   <KEYWORDS><![CDATA["+URLEncoder.encode(rs.getString(13), "UTF-8")+"]]></KEYWORDS>");
                }
                if (rs.getString(14) != null) {
                    out.println ("   <ABSTRACT><![CDATA["+URLEncoder.encode(rs.getString(14), "UTF-8")+"]]></ABSTRACT>");
                }
                if (rs.getString(15) != null) {
                    out.println ("   <BIBLIOGRAPHY><![CDATA["+URLEncoder.encode(rs.getString(15), "UTF-8")+"]]></BIBLIOGRAPHY>");
                }
                if (rs.getString(16) != null) {
                    out.println ("   <TITLE><![CDATA["+URLEncoder.encode(rs.getString(16), "UTF-8")+"]]></TITLE>");
                }
                if (rs.getString(17) != null) {
                    out.println ("   <FULLTEXT><![CDATA["+URLEncoder.encode(rs.getString(17), "UTF-8")+"]]></FULLTEXT>");
                }

				out.println ("</ALERT>");
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No alerts found</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			} else { out.println("</ALERTS>"); }

		} catch ( TSException tse )	 {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
