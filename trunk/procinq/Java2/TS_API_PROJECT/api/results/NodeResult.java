package api.results;

public class NodeResult
{
	private String sNodeID;
	private String sCorpusID;
	private String sParentID;
	private String sNodeTitle = "";
	private String sDocSummary; //optional
	private String sNIWP;
	private String sDepth;
	private String sDateScanned;
	private String sDateUpdated;
	private String sScore1;

	// Accessor methods to private variables
	public String GetNodeID() { return sNodeID; }
	public String GetCorpusID() { return sCorpusID; }
	public String GetParentID() { return sParentID; }
	public String GetNodeTitle() { return sNodeTitle; }
	public String GetDocSummary() { return sDocSummary; }
	public String GetNIWP() { return sNIWP; }
	public String GetDepth() { return sDepth; }
	public String GetDateScanned() { return sDateScanned; }
	public String GetDateUpdated() { return sDateUpdated; }
	public String GetScore1() { return sScore1; }

	// Overriding hashCode function to return a sortable score
	public int hashCode() {
        char c = '\0';
        if (sNodeTitle.length() > 0) { c = sNodeTitle.charAt(0); }

		try {
          int hash = new Integer(GetScore1()).intValue();
          if (sNodeTitle.length() > 0) { hash = (hash*100) - (int) c; }
          return hash;
        }
		catch (Exception e) { return 0; }
	}
	
	// Object constructor
	public NodeResult(String sNodeID, String sCorpusID, String sParentID, String sNodeTitle, 
							  String sNIWP, String sDepth, String sDateScanned, String sDateUpdated,
							  String sScore1) {
		
		this.sNodeID = sNodeID;
		this.sCorpusID = sCorpusID;
		this.sParentID = sParentID;
		this.sNodeTitle = sNodeTitle;
		this.sNIWP = sNIWP;
		this.sDepth = sDepth;
		this.sDateScanned = sDateScanned;
		this.sDateUpdated = sDateUpdated;
		this.sScore1 = sScore1;
	}
	// Constructor WITH DocSummary
	public NodeResult(String sNodeID, String sCorpusID, String sParentID, String sNodeTitle, 
					  String sDocSummary, String sNIWP, String sDepth, String sDateScanned, String sDateUpdated,
					  String sScore1) {
		
		this.sNodeID = sNodeID;
		this.sCorpusID = sCorpusID;
		this.sParentID = sParentID;
		this.sNodeTitle = sNodeTitle;
		this.sDocSummary = sDocSummary;
		this.sNIWP = sNIWP;
		this.sDepth = sDepth;
		this.sDateScanned = sDateScanned;
		this.sDateUpdated = sDateUpdated;
		this.sScore1 = sScore1;
	}
}
