package api;

import com.indraweb.util.UtilStrings;

import java.io.*;
import java.util.*;
// class will mirror content to a file that gets normally just put to an 
// output stream as in unix "tee" program

public class XMLOutRedirector extends java.io.PrintWriter
{
	//java.io.PrintWriter out = null;
	//java.io.FileWriter fw = new java.io.FileWriter (sFileName, true );
	
	java.io.FileWriter fw = null;
	java.io.PrintWriter pwFile = null;
	java.io.PrintWriter pwOriginal = null;

    private StringBuffer sbRetainXMLInMem = null;
    private boolean bRetainXMLInMem = false;

	public XMLOutRedirector ( java.io.PrintWriter pwOriginal_,
							  String sFileName,
							  boolean bAppendFile,
                              boolean bRetainXMLInMem )
	{
        super (pwOriginal_);
        //api.Log.Log ("in XMLOutRedirector sFileName [" + sFileName + "] bRetainXMLInMem [" + bRetainXMLInMem + "]" );
		//java.io.PrintWriter pw2 = new java.io.PrintWriter(pwOriginal_);
		pwOriginal = pwOriginal_;
		this.bRetainXMLInMem = bRetainXMLInMem;
        if ( bRetainXMLInMem )
            sbRetainXMLInMem = new StringBuffer();
		//super (pwOriginal_);
		try 
		{
            if ( sFileName != null )
            {
                fw = new java.io.FileWriter (sFileName, bAppendFile );
                pwFile = new java.io.PrintWriter (fw);
            }
		}
		catch ( Exception e )
		{
			e.printStackTrace();	
		} 
	}
	
	public void close ()
	{
		try 
		{
			if ( pwFile != null )
                pwFile.close();
            if ( fw != null )
                fw.close();
			pwOriginal.close();
			super.close();
		}
		catch ( Exception e )
		{
			e.printStackTrace();	
		} 
	} 
	
	public void print (String s)
	{
		try 
		{
            //api.Log.Log ("in gettext.println [" + s + "] bRetainXMLInMem [" + bRetainXMLInMem + "]" );
            if ( pwFile != null )
                pwFile.print(s);
			pwOriginal.print(s);
            if ( bRetainXMLInMem )
            {
                sbRetainXMLInMem.append(s);
                //api.Log.Log ("in SAVING gettext.println [" + s + "] bRetainXMLInMem [" + bRetainXMLInMem + "]" );

            }
            //super.print(s);
		}
		catch ( Exception e )
		{
			e.printStackTrace();	
		} 
	} 
	
	public void println (String s)
	{
		try 
		{
            //api.Log.Log ("in gettext.println [" + s + "] bRetainXMLInMem [" + bRetainXMLInMem + "]" );
            if ( pwFile != null )
                pwFile.println(s);
			pwOriginal.println(s);
            if ( bRetainXMLInMem )
            {
                sbRetainXMLInMem.append(s);
                //api.Log.Log ("in SAVING gettext.println [" + s + "] bRetainXMLInMem [" + bRetainXMLInMem + "]" );
            }
			//super.println(s);
		}
		catch ( Exception e )
		{
			e.printStackTrace();	
		} 
	} 
	

    public String getText(boolean bStripXMLheader) throws Exception
    {

        if ( !bRetainXMLInMem )
            throw new Exception ("XMLOutRedirector : XML not retained in memory");

        //api.Log.Log ("in xmloutredirector.gettext [" + sbRetainXMLInMem.toString() + "]" );
        if ( bStripXMLheader )
            return UtilStrings.replaceStrInStr(sbRetainXMLInMem.toString(), APIHandler.sXMLHEADER, "");
        else
            return sbRetainXMLInMem.toString();
    }
	//public XMLOutRedirector extends ( PrintWriter out )
	
	
	
	
	
	
	
}
 