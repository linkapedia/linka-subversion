package api.tsrepository;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Retrieve a list of watched folders for a given repository.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique repository identifier of the target repository.
 *  @param  Format (optional)   If 1, format date in form of MM/DD/YY
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsrepository.TSGetWatchedFolders&SKEY=-132981656

 *	@return	a series of WATCHED FOLDER objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <FOLDERS>
        <FOLDER>
            <REPOSITORYID>3</REPOSITORYID>
            <PATH>C:\Documents and Settings\Indraweb\IndraHome\test</PATH>
            <RECURSE>1</RECURSE>
            <DATELASTCAPTURE>11/07/02</DATELASTCAPTURE>
        </FOLDER>
 ...
    </FOLDERS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetWatchedFolders
{
	public static void handleTSapiRequest ( 
										   api.APIProps props, 
										   PrintWriter out, 
										   Connection dbc )
		throws Exception
	{

		String sID = (String) props.get ("RID", true);
		String sFormat = (String) props.get ("FORMAT", "1");
		
		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = " select Path, Recurse, to_char(DateLastCapture, 'MM/DD/YY') "+ 
						  " from WatchFolder where RepositoryID = "+sID;
			if (sFormat.equals("0")) {
				sSQL = " select Path, Recurse, DateLastCapture "+ 
					   " from WatchFolder where RepositoryID = "+sID;		
			}
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				String sPath = rs.getString(1);
				int iRecurse = rs.getInt(2);
				String sDateLastCapture = rs.getString(3);

				loop = loop + 1;
				if (loop == 1) { out.println("<FOLDERS>"); }
				out.println (" <FOLDER>");
				out.println ("   <REPOSITORYID>"+sID+"</REPOSITORYID>");
				out.println ("   <PATH>"+sPath+"</PATH>");
				out.println ("   <RECURSE>"+iRecurse+"</RECURSE>");
				out.println ("   <DATELASTCAPTURE>"+sDateLastCapture+"</DATELASTCAPTURE>");
				out.println ("</FOLDER>");
			}

			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>There are no watched folders.</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_MATCHES_ON_SEARCH);
			}
			out.println("</FOLDERS>");
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
