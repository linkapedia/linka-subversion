package api.tsdiscussion;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.db.ConnectionFactory;
import com.linkapedia.procinq.server.node.Node;
import com.linkapedia.procinq.server.node.NodeDao;
import com.linkapedia.procinq.server.security.Authoritation;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class TSUpdateDiscuss {

    public static void handleTSapiRequest(api.APIProps props,
            PrintWriter out,
            Connection dbc) throws Exception {

        String nodeidStr = (String) props.get("nodeid");
        String withSiblings = (String) props.get("withSiblings", "false");
        String sKey = (String) props.get("SKEY", true);

        Connection connection = null;
        try {
            connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            if (nodeidStr == null || nodeidStr.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            int nodeId = Integer.parseInt(nodeidStr);
            Node node = NodeDao.getNode(nodeId, connection);
            if (!Authoritation.isAuthorizedUser(node.getCorpusId(), sKey, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            List<Node> nodesToUpdate = getNodesToUpdate(
                    node, Boolean.valueOf(withSiblings), connection);
            int cont = 0;
            int numNodes = nodesToUpdate.size();
            for (Node currentNode : nodesToUpdate) {
                List<Node> all_children = NodeDao.getNodesByIdUntilLastLevel(
                        currentNode.getNodeId(), connection);
                NodeDao.updateDiscussionId(all_children, 
                        currentNode.getNodeId(), connection);
                showProgress(cont, numNodes, out);
                cont++;
            }
            out.println("<SUCCESS>Success</SUCCESS>");
        } finally {
            connection.close();
        }
    }

    private static List<Node> getNodesToUpdate(Node node, boolean withSiblings,
            Connection connection) throws SQLException {
        List<Node> nodesToUpdate = new ArrayList<>();
        if (withSiblings && !isRootNode(node)) {
            nodesToUpdate = NodeDao.getNodesByParentId(node.getParentId(), connection);
        } else {
            nodesToUpdate.add(new Node(node.getNodeId()));
        }
        return nodesToUpdate;

    }

    private static boolean isRootNode(Node node) {
        return node.getParentId() == -1;
    }

    private static void showProgress(int cont, int numNodes, PrintWriter out) {
        DecimalFormat twoDigits = new DecimalFormat("0");
        long percent = ((cont + 1) * 100) / numNodes;
        out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
        out.flush();
    }
}
