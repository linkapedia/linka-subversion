package api.security;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * Retrieves configuration values in the system as name value pairs.
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  TYPE (optional) 0 is user parms, 1 is system parms.   Default is all parameters.
 *  @param  PARAMNAME (optional)    only return one parameter value, as specified
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=security.TSGetConfigParams&SKEY=-594900700&TYPE=0

 *	@return	ITS CONFIGPARAMS object containing the system parameters requested.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
<TSRESULT>
   <CONFIGPARAMS>
      <UseNuggets>true</UseNuggets>
      <DC>dc=indraweb</DC>
      <LdapHost>66.134.131.35</LdapHost>
      <LdapPort>389</LdapPort>
      <LdapUsername>cn=Administrator,cn=users,dc=indraweb</LdapUsername>
      <LdapPassword>racerx09</LdapPassword>
      <AdminGroup>CN=IndraAdministrator,CN=Users</AdminGroup>
...
  \endverbatim
 */
public class TSGetConfigParams
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		try {
			
			String sType = (String) props.get("TYPE");
			String sParamName = (String) props.get("PARAMNAME");

            String sSQL = " select ParamName, ParamValue from ConfigParams ";
			if (sType != null) { sSQL = sSQL + "where ParamType = "+sType; }
            if (sParamName != null) { sSQL = sSQL + "where ParamName = '"+sParamName+"'"; }
			Statement stmt = dbc.createStatement();
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				String sK = rs.getString(1);
				String sV = rs.getString(2);
		
				loop = loop + 1;
				if (loop == 1) { out.println ("   <CONFIGPARAMS>"); }
				out.println ("      <"+sK+">"+sV+"</"+sK+">");
			}

			rs.close();
		    stmt.close();

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No parameters found</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_CORPORA_THIS_DB);
			}

			if (loop > 0) {	out.println ("   </CONFIGPARAMS>"); }
		}
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}
