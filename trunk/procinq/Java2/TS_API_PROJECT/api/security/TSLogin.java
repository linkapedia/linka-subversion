package api.security;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.ConfServer;
import com.indraweb.execution.Session;
import com.iw.system.User;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * This API logs the user into the Taxonomy Server.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param UserID The distinguished name that identifies this user in the LDAP
 * server
 * @param Password The password assigned to this user account.
 *
 * @note
 * http://ITSSERVER/servlet/ts?fn=security.TSLogin&UserID=sn=user,ou=users,dc=indraweb,dc=com&Password=mypassword
 *
 * @return An ITS user subscriber object \verbatim <?xml version="1.0"
 * encoding="UTF-8" ?> <TSRESULT> <SUBSCRIBER>
 * <ID>sn=user,ou=users,dc=indraweb,dc=com</ID>
 * <EMAIL>testuser@indraweb.com</EMAIL>
 * <PASSWORD>mypassword</PASSWORD> <NAME>Test User</NAME>
 * <EMAILSTATUS>1</EMAILSTATUS> <SCORETHRESHOLD>50.0</SCORETHRESHOLD>
 * <RESULTSPERPAGE>10</RESULTSPERPAGE> <USERSTATUS>1</USERSTATUS>
 * <KEY>993135977</KEY> </SUBSCRIBER> </TSRESULT> \endverbatim
 */
public class TSLogin {
    // Log the user into the system and return a session key

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sUserDN = (String) props.get("UserID", true);
        String sPassword = (String) props.get("Password", true);

        localLogin(sUserDN, sPassword, out, dbc);
    }

    public static User localLogin(String sUserDN, String sPassword,
            PrintWriter out, Connection dbc) throws Exception {
        User u = null;
        LDAP_Connection lc = null;
        try {
            if (sPassword.equals("")) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_BAD_PASSWORD);
            }

            lc = new LDAP_Connection(sUserDN, sPassword, out);
            if (!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
                u = lc.GetUser(lc.GetLdapUsername());
                if (u == null) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
                }
            }
            
            if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")){
                String userNameProperties = ConfServer.getValue("com.procinq.server.username");
                String passwordProperties = ConfServer.getValue("com.procinq.server.password");
                if (!sUserDN.equals(userNameProperties) || !sPassword.equals(passwordProperties)){
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                }
            }
            
            u = new User();
            String sKey = "";
            while (sKey.equals("") || Session.htUsers.containsKey(sKey)) {
                SecureRandom sr = new SecureRandom();
                int iKey = sr.nextInt();
                sKey = new Integer(iKey).toString();
            }

            u.SetPassword(sPassword);
            u.SetDN(lc.GetLdapUsername());
            u.SetSessionKey(sKey);

            // MODIFIED 12/16: load user variable with group permissions at login time
            lc.Refresh_Connection();
            Hashtable ht = lc.GetGroups(u);
            HashSet hs = GetSecurityPermissions(ht, dbc, lc);
            u.SetGroupPermissions(ht);
            u.SetSecurityPermissions(hs);
            u.SetDocumentSecurityHexmask(api.security.DocumentSecurity.getSecurity(hs));

            Session.htUsers.put(sKey, u);

            if (false) {
                out.println("   <DEBUGINFO>");
                out.println("      <SECURITYHASHSIZE>" + hs.size() + "</SECURITYHASHSIZE>");
                out.println("      <NUMBEROFGROUPS>" + ht.size() + "</NUMBEROFGROUPS>");
                out.println("      <DC>" + lc.GetDC() + "</DC>");
                out.println("   </DEBUGINFO>");
            }

            out.println("   <SUBSCRIBER> ");
            out.println("      <ID>" + lc.GetLdapUsername() + "</ID> ");
            out.println("      <EMAIL>" + u.GetEmail() + "</EMAIL>");
            out.println("      <PASSWORD>" + u.GetPassword() + "</PASSWORD>");
            out.println("      <NAME>" + u.GetFullname() + "</NAME>");
            out.println("      <EMAILSTATUS>" + u.GetEmailStatus() + "</EMAILSTATUS>");
            out.println("      <SCORETHRESHOLD>" + u.GetScoreThreshold() + "</SCORETHRESHOLD>");
            out.println("      <RESULTSPERPAGE>" + u.GetResultsPerPage() + "</RESULTSPERPAGE>");
            out.println("      <USERSTATUS>" + u.GetUserStatus() + "</USERSTATUS>");
            out.println("		<KEY>" + sKey + "</KEY>");
            out.println("      <LICENSE>" + Session.license.getBitmask() + "</LICENSE>");
            out.println("   </SUBSCRIBER>");
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
            return u;
        } finally {
            lc.close();
        }
        return u;

    }

    // given a list of group DNs, return the security IDs
    public static HashSet GetSecurityPermissions(Hashtable ht, Connection dbc, LDAP_Connection lc)
            throws Exception {
        String sGroupList = "(";
        HashSet hs = new HashSet();

        // loop through distinguished names and build an IN list
        int loop = 0;
        Enumeration eH = ht.keys();
        while (eH.hasMoreElements()) {
            loop++;
            if (loop > 1) {
                sGroupList = sGroupList + ", ";
            }
            sGroupList = sGroupList + "'" + (String) eH.nextElement() + "'"; // no longer use DC: ,"+lc.GetDC()+"'";
        }
        if (loop == 0) {
            return new HashSet();
        }

        // make a call to the database
        String sSQL = "select SecurityID from Security where lower(DistinguishedName) IN " + sGroupList.toLowerCase() + ")";
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            while (rs.next()) {
                hs.add(rs.getString(1));
            }
            return hs;
        } catch (Exception e) {
            return hs;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }
}
