package api.tsidrac;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.net.*;
import java.util.Enumeration;
import java.util.zip.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.results.*;
import api.security.*;

import com.iw.system.User;

import com.indraweb.database.*;
import com.indraweb.network.*;
import com.indraweb.html.*;
import com.indraweb.execution.Session;

// TSGetNodeDocument (sessionid, nodeid, genreid)

/**
 * This API call has been deprecated, and should no longer be used.   Please use CQL to retrieve documents.
 * @note    This API call has been deprecated.
 */
public class TSGetNodeDocument
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sCountries = (String) props.get ("Country", true);
		String sGenreID = (String) props.get ("FolderID");
		String sStart = (String) props.get ("Start");
		String sEnd = (String) props.get ("End");
		String sDate = (String) props.get ("Date");
		String sScoreThreshold = (String) props.get ("ScoreThreshold", "45.0");
        String sOutdated = (String) props.get ("Outdated");

		float fScoreThreshold = new Float(sScoreThreshold).floatValue();
		int iScoreThreshold = (int) fScoreThreshold;

		// Optional: User can specify where to stop, start
		if (sStart == null) { sStart = "1"; }
		if (sEnd == null) { sEnd = "5000"; }

		// Format sCountries to conduct SQL query correctly
        Vector vCountries = new Vector();

		if (!sCountries.equals("ALL")) {
            vCountries = com.indraweb.util.UtilStrings.splitByStrLen1(sCountries, ",");
			sCountries = "'"+sCountries+"'";
			sCountries = com.indraweb.util.UtilStrings.replaceStrInStr(sCountries, ",", "','");
		}

		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select CorpusID from Node where NodeID = " +sNodeID;
			Statement stmt = dbc.createStatement();
			ResultSet rs = stmt.executeQuery (sSQL);
			rs.next();

			// Get corpus information along with the cut offs (we'll need them later)
			int iCorpusId = rs.getInt(1);

			rs.close(); stmt.close();

			if ((!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) &&
                (!u.IsAuthorized(iCorpusId, out))) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			// Load the BLACKLIST terms.   Do not display any results that contain any piece
			// of the blacklist terms
			sSQL = "select urlchunk from BlackList where "+
						  "(CorpusID in (select corpusid from Node where NodeID = "+sNodeID+" ) "+
						  "and NodeID is NULL) or (NodeID = "+sNodeID+")";

			ResultSet rsBlack = null;
			Statement stmtBlack = null;

			stmtBlack = dbc.createStatement();
			rsBlack = stmtBlack.executeQuery (sSQL);

			// Load blacklist words into a Vector
			Vector vBlackList = new Vector();

			while ( rsBlack.next() ) {
				String sBlacklistTerm = rsBlack.getString(1);
				vBlackList.addElement(sBlacklistTerm);
			}

			stmtBlack.close();

			sSQL = " select D.DocumentID, D.DocTitle, D.DocURL, D.DocumentSummary, "+
                   " D.Country, D.Bibliography, D.Language, D.IdracNumber, D.AdoptionDate, "+
                   " D.PublicationDate, D.EntryDate, D.RevisionDate, D.SystemDate, D.Outdated, "+
			 	   " N.Score1, N.Score2, N.Score3, N.Score4, N.Score5, N.Score6, "+
				   " to_char(N.DateScored, 'MM/DD/YYYY HH24:MI:SS'), D.GenreID from DocumentCustom D, "+
				   " NodeDocument N where N.Score1 >= "+sScoreThreshold+
				   " and D.DocumentId = N.DocumentID AND N.NodeID = "+sNodeID;
			if (!sCountries.equals("ALL")) {
				sSQL = sSQL + " and ( ";
                for (int i=0; i<vCountries.size(); i++) {
                    if (i!=0) { sSQL = sSQL + "or "; }
                    sSQL = sSQL + "D.Country = '"+(String)vCountries.elementAt(i)+"' ";
                }
                sSQL = sSQL + ")";
			}

			if (sGenreID != null) { sSQL = sSQL + " AND D.GenreID = "+sGenreID; }

            if (sOutdated != null) { sSQL = sSQL + " AND Outdated = 1"; }
			else { sSQL = sSQL + " AND Outdated != 1"; }
			sSQL = sSQL + " order by N.IndexWithinNode asc, N.Score1 desc, D.DocTitle asc";

			rs = null; stmt = null;
			out.println("<SQL>"+sSQL+"</SQL>");

			try {
				stmt = dbc.createStatement();
				rs = stmt.executeQuery (sSQL);
			}
			catch ( SQLException qe) {
				EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
			}

			// loop through each result from the database.  results are sorted by docTITLE but
			//  we need to collect up duplicate docTitles in the display.  therefore, do not
			//  print out information until we know we've hit a docTITLE that is different.
			//  store the temporary data in the vResults vector array.
			int loop = 0; Vector vResults = new Vector(); String sLastDocTitle = "";
			int iNumResults = 0;
			while ( rs.next() ) {
				String iDocID = rs.getString(1);
				String sDocTitle = rs.getString(2);
				String sDocURL = rs.getString(3);
				String sDocSummary = rs.getString(4);
                String sCountry = rs.getString(5);
                String sBib = rs.getString(6);
                String sLang = rs.getString(7);
                String sIdracNum = rs.getString(8);
                String sAdoptDate = rs.getString(9);
                String sPubDate = rs.getString(10);
                String sEntryDate = rs.getString(11);
                String sRevDate = rs.getString(12);
                String sSysDate = rs.getString(13);
                String sIsOutdated = rs.getString(14);
				String fScore1 = rs.getString(15);
				String fScore2 = rs.getString(16);
				String fScore3 = rs.getString(17);
				String fScore4 = rs.getString(18);
				String fScore5 = rs.getString(19);
				String fScore6 = rs.getString(20);
				String sDateScored = rs.getString(21);
				String slGenreID = rs.getString(22);

				// if document is greater than 300 characters, truncate at
				// next available whitespace
				int i = 300; // maximum abstract size
				if (sDocSummary == null) { sDocSummary = "(no abstract found)"; }
				while ((sDocSummary.length() > i) && (sDocSummary.charAt(i) != ' ')) { i++; }
				if (sDocSummary.length() > i) {
					sDocSummary = sDocSummary.substring(0, i)+"...";
				}

				// Check this result against the blacklist.   If no match, instantiate
				//  the new NodeDocumentResult object and put data into HASH table
				boolean bBlackListMatch = false;

				for (int iBlackLoop = 0; iBlackLoop < vBlackList.size(); iBlackLoop++) {
					String sBLterm = (String) vBlackList.elementAt(iBlackLoop);
					if (com.indraweb.util.UtilStrings.getStrContains(sDocURL, sBLterm)) {
						bBlackListMatch = true;
					}
				}

				if (sDate != null) {
					Calendar cNow = Calendar.getInstance();
					Calendar cDoc = Calendar.getInstance();

					// MM/DD/YYYY HH24:MI:SS
					int iMon = new Integer(sDate.substring(0, 2)).intValue();
					int iDay = new Integer(sDate.substring(3, 5)).intValue();
					int iYear = new Integer(sDate.substring(6, 10)).intValue();
					int iHour = new Integer(sDate.substring(11, 13)).intValue();
					int iMin = new Integer(sDate.substring(14, 16)).intValue();
					int iSec = new Integer(sDate.substring(17)).intValue();

					api.Log.Log("Setting reference date: "+iMon+"/"+
								iDay+"/"+iYear+" "+iHour+":"+iMin+":"+iSec);
					cNow.set(iYear, iMon, iDay, iHour, iMin, iSec);

					if (sDateScored.length() == 19) { // MM/DD/YYYY HH24:MI:SS
						iMon = new Integer(sDateScored.substring(0, 2)).intValue();
						iDay = new Integer(sDateScored.substring(3, 5)).intValue();
						iYear = new Integer(sDateScored.substring(6, 10)).intValue();
						iHour = new Integer(sDateScored.substring(11, 13)).intValue();
						iMin = new Integer(sDateScored.substring(14, 16)).intValue();
						iSec = new Integer(sDateScored.substring(17)).intValue();
					} else { // MM/DD/YY HH:MM .. for legacy purposes only
						iMon = new Integer(sDateScored.substring(0, 2)).intValue();
						iDay = new Integer(sDateScored.substring(3, 5)).intValue();
						iYear = new Integer(sDateScored.substring(6, 8)).intValue();
						iHour = new Integer(sDateScored.substring(9, 11)).intValue();
						iMin = new Integer(sDateScored.substring(12)).intValue();
						iSec = 0;

						iYear = iYear + 2000;
					}

					//api.Log.Log("Setting document date: "+iMon+"/"+
					//			iDay+"/"+iYear+" "+iHour+":"+iMin+":"+iSec);
					cDoc.set(iYear, iMon, iDay, iHour, iMin, iSec);

					if (cDoc.before(cNow)) { bBlackListMatch = true; }
				}
				if (loop == 0) { out.println ("<NODEDOCUMENTS> "); } loop++;

				if ((!sDocTitle.equals(sLastDocTitle)) && (!sLastDocTitle.equals(""))) {
					iNumResults++;
				}

				if ((loop >= new Integer(sStart).intValue() ) &&
					(loop <= new Integer(sEnd).intValue() )) {

					if ((!sDocTitle.equals(sLastDocTitle)) && (!sLastDocTitle.equals(""))) {
						Enumeration eV = vResults.elements();
						if (vResults.size() > 0) {
							PrintNodeDocumentResults(vResults, sCountries, out, dbc);
						}
							vResults = new Vector();
					}
					NodeDocumentResult ndr = new NodeDocumentResult(sNodeID, iDocID+"", slGenreID,
																	sDocTitle, sDocURL, sDocSummary,
                                                                    sCountry, sBib, sLang, sIdracNum, sAdoptDate,
                                                                    sPubDate, sEntryDate, sRevDate, sSysDate,
                                                                    sIsOutdated, fScore1,"0.0","0.0","0.0","0.0","0.0",
																	sDateScored);
					if (ndr != null) { vResults.addElement(ndr); }
				}
				sLastDocTitle = sDocTitle;
			}
			rs.close();
			stmt.close();

			if (!vResults.isEmpty()) {
				PrintNodeDocumentResults(vResults, sCountries, out, dbc);
				iNumResults++;
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No documents found for Node Id "+sNodeID+" Genre Id "+sGenreID+" </DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			} else { out.println ("</NODEDOCUMENTS>"); }
			out.println("<NUMRESULTS>"+iNumResults+"</NUMRESULTS>");
		}

		catch ( TSException tse) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}

	public static void PrintNodeDocumentResults (Vector vResults, String sCountries,
												 PrintWriter out, Connection dbc) throws Exception {
		NodeDocumentResult ndr = (NodeDocumentResult) vResults.elementAt(0);

		out.println ("   <NODEDOCUMENT> ");
		out.println ("	    <NODEID>"+ndr.GetNodeID()+"</NODEID>");
		out.println ("	    <DOCUMENTID>"+ndr.GetDocID()+"</DOCUMENTID>");
		out.println ("      <FOLDERID>"+ndr.GetFolderID()+"</FOLDERID>");
		out.println ("      <DOCTITLE><![CDATA["+ndr.GetDocTitle()+"]]></DOCTITLE>");
		out.println ("      <DOCURLS>");

		Enumeration eV = vResults.elements();
		while (eV.hasMoreElements()) {
			NodeDocumentResult n = (NodeDocumentResult) eV.nextElement();
			out.println ("      <DOCURL><![CDATA["+n.GetDocURL()+"]]></DOCURL>");
		}

		out.println ("      </DOCURLS>");
		out.print ("      <DOCSUMMARY><![CDATA[");
		for (int k = 0; k < ndr.GetDocSummary().length(); k++) {
			int ichar = (int) ndr.GetDocSummary().charAt(k);
			if ((ichar != 13) && (ichar != 10)) { out.print(ndr.GetDocSummary().charAt(k)); }
		}
		out.println ("]]></DOCSUMMARY>");
		out.println ("      <SCORE1>"+ndr.GetScore1()+"</SCORE1>");
		out.println ("      <DATESCORED>"+ndr.GetDateScored()+"</DATESCORED>");
		out.println ("		<TYPE>0</TYPE>");

        // IDRAC specific information
		out.println ("		<BIBLIOGRAPHY>"+ndr.GetBib()+"</BIBLIOGRAPHY>");
 		out.println ("		<LANGUAGE>"+ndr.GetLang()+"</LANGUAGE>");
		out.println ("		<IDRACNUMBER>"+ndr.GetIdracNum()+"</IDRACNUMBER>");
		out.println ("		<ADOPTIONDATE>"+ndr.GetAdoptDate()+"</ADOPTIONDATE>");
		out.println ("		<PUBLICATIONDATE>"+ndr.GetPubDate()+"</PUBLICATIONDATE>");
		out.println ("		<ENTRYDATE>"+ndr.GetEntryDate()+"</ENTRYDATE>");
		out.println ("		<REVISIONDATE>"+ndr.GetRevDate()+"</REVISIONDATE>");
		out.println ("		<SYSTEMDATE>"+ndr.GetSysDate()+"</SYSTEMDATE>");
        out.println ("      <OUTDATED>"+ndr.GetIsOutdated()+"</OUTDATED>");

		out.println ("   <COUNTRIES>");
        eV = vResults.elements();
		while (eV.hasMoreElements()) {
			NodeDocumentResult n = (NodeDocumentResult) eV.nextElement();
			out.println ("      <COUNTRY>"+n.GetCountry()+"</COUNTRY>");
		}

		out.println ("   </COUNTRIES>");
		out.println ("   </NODEDOCUMENT>");

		return;
	}
}
