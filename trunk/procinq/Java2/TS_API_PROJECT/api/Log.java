package api;

import org.apache.log4j.Logger;
import com.indraweb.execution.ThreadInfoTestAssertion;
import java.io.PrintWriter;
import java.io.StringWriter;

// This class manages logging to the JRUN logs located in the $JRUN/logs directory
public class Log {

    private static final Logger log = Logger.getLogger(Log.class);

    // Log output to JRUN logs
    public static void Log(String Message) {
        String s = " API: " + Message;
        log.info(s);
    }

    // Log warning or fatal error to JRUN logs
    public static void LogError(String Message) {
        log.error(" API WARNING: " + Message);
        ThreadInfoTestAssertion.captureErrorPut("api.Log.LogError time [" + new java.util.Date()
                + "] [ API WARNING: " + Message + "]");
    }

    public static void LogError(String Message, Throwable e) {
        log.error(" API WARNING: " + Message, e);
        ThreadInfoTestAssertion.captureErrorPut("api.Log.LogError time [" + new java.util.Date()
                + "] [ API WARNING: " + Message + "] [" + stackTraceToString(e) + "]");
    }

    public static void LogError(Throwable e) {
        log.error(" API WARNING: ", e);
        ThreadInfoTestAssertion.captureErrorPut("api.Log.LogError time [" + new java.util.Date()
                + "] [ API WARNING:  [" + stackTraceToString(e) + "]");
    }

    // Fatal error
    public static void LogFatal(String Message) {
        log.fatal(" API FATAL ERROR: " + Message);
        ThreadInfoTestAssertion.captureErrorPut("api.Log.LogFatal time [" + new java.util.Date()
                + "] [ API WARNING: " + Message + "]");
    }

    public static void LogFatal(String Message, Throwable e) {
        log.fatal(" API FATAL ERROR: " + Message);
        ThreadInfoTestAssertion.captureErrorPut("api.Log.LogFatal time [" + new java.util.Date()
                + "] [ API WARNING: " + Message + "] [" + stackTraceToString(e) + "]");
    }

    // Get the stacktrace of an exception into a string
    public static String stackTraceToString(Throwable e) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return "StackTRACE " + sw.toString() + " ";
        } catch (Exception e2) {
            return "exception in stackTraceToString [" + e.getMessage() + "]";
        }
    }
}
