package api.tsqa;
/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 22, 2002
 * Time: 7:55:16 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

import com.indraweb.util.*;
import java.util.*;

public class TrecCompareCounters {

    public int iReportType = -1;
    public int icounttrecHitsthisQ = -1;
    public int lcountIWDistinctDocIDsThisQGt0 = -1;
    public int lcountIWNodeDocIDsThisQGt0 = -1;
    
	public int iNumDistinctFalseOrMissingDocs_noScoreConstraint = -1;
	public int iNumDistinctFalseDocs_GT_zero = -1;
    public int iNumDistinctFalseDocs_GT_fifty = -1;

	public int ivINodeIDssize= -1;
    public String sNodeIDList = null;
	public String sQtitle = null;
	public int iQid = -1;
	public int iNumTopScoringRecordsToConsider = -1;
	public Hashtable htNodeColonDocSemiColonScore_to_dprd = null;
	public Hashtable  htDocsWithOneOrMoreNodeDocScores_GtFifty = new Hashtable();
	public Hashtable  htDocsWithOneOrMoreNodeDocScores_GtZero = new Hashtable();
	public Hashtable  htDocsUniqueAllScores = new Hashtable();

    public TrecCompareCounters  (
        int iQid,
		int iReportType,
		int lcounttrecHitsthisQ,
        int lcountIWDistinctDocIDsThisQGt0,
        int lcountIWNodeDocIDsThisQGt0,
        int ivINodeIDssize,
        String sNodeIDList
           )
    {
		this.iQid = iQid;
		this.iReportType = iReportType;
		this.icounttrecHitsthisQ = lcounttrecHitsthisQ;
        this.lcountIWDistinctDocIDsThisQGt0 = lcountIWDistinctDocIDsThisQGt0;
        this.lcountIWNodeDocIDsThisQGt0 = lcountIWNodeDocIDsThisQGt0;
        this.ivINodeIDssize= ivINodeIDssize;
        this.icounttrecHitsthisQ = icounttrecHitsthisQ ;
        this.sNodeIDList = sNodeIDList ;
    }


    public String toString()
    {
		
		String sPrecOrRecInfo = null;
		if ( iReportType == TSPrecisionRecallReport.iREPORTTYPE_PRECISION ) {
			// if only looking at the top 10 or so 
			if ( iNumTopScoringRecordsToConsider < lcountIWDistinctDocIDsThisQGt0 )
				lcountIWDistinctDocIDsThisQGt0 =  iNumTopScoringRecordsToConsider;
			
			double dPrecision = ((double) lcountIWDistinctDocIDsThisQGt0 -(double) iNumDistinctFalseDocs_GT_zero)/(double) lcountIWDistinctDocIDsThisQGt0;
			sPrecOrRecInfo = 
				"\r\n # *** P\t("+ lcountIWDistinctDocIDsThisQGt0 +"-"+iNumDistinctFalseDocs_GT_zero+")"+"/"+lcountIWDistinctDocIDsThisQGt0 + 
                "\t"+UtilStrings.numFormatDouble ( dPrecision, 2);  

		} else if ( iReportType == TSPrecisionRecallReport.iREPORTTYPE_RECALL ){
			double dRecall = ((double) icounttrecHitsthisQ-(double)iNumDistinctFalseOrMissingDocs_noScoreConstraint)/(double) icounttrecHitsthisQ;			
			sPrecOrRecInfo = 
				"\r\n # *** R\t("+icounttrecHitsthisQ+"-"+iNumDistinctFalseOrMissingDocs_noScoreConstraint+")"+"/"+icounttrecHitsthisQ + 
                "\t"+UtilStrings.numFormatDouble ( dRecall, 2);  
		} else
			sPrecOrRecInfo= " error creating sPrecOrRecInfo";
							   
		if ( iReportType == TSPrecisionRecallReport.iREPORTTYPE_PRECISION )
		{
	        return (">>> " +
					"\r\n # Q Desc\t" + iQid + " : " + sQtitle +
                "\r\n # TREC Docs\t" + icounttrecHitsthisQ +
                "\r\n # IW distinct Docs Gt 0\t" + lcountIWDistinctDocIDsThisQGt0  +
                "\r\n # IW FalsePos NodeDocs Gt 0 \t" +    lcountIWNodeDocIDsThisQGt0  + "\tthis Q " + 
                "\r\n # IW FalsePos Docs \t" + iNumDistinctFalseOrMissingDocs_noScoreConstraint + "\t Distinct (no stars constraint)" +
                "\r\n # IW FalsePos Docs \t" + iNumDistinctFalseDocs_GT_zero + "\t Distinct  (GT 0 star score)" +
                "\r\n # IW FalsePos Docs \t" + iNumDistinctFalseDocs_GT_fifty + "\t Distinct  (GT 50 star score)" +
				sPrecOrRecInfo + 
                "\r\n # MaxCount [" + iNumTopScoringRecordsToConsider  + "]" +
                "\r\n # NodeIDs [" + ivINodeIDssize  + "] NoideIDList [" + sNodeIDList +
                "]\r\n" );
			
		} else if (iReportType == TSPrecisionRecallReport.iREPORTTYPE_RECALL ){
	        return (">>> " +
                "\r\n # TREC Docs\t" + icounttrecHitsthisQ +
                "\r\n # IW distinct docs Gt 0  \t" + lcountIWDistinctDocIDsThisQGt0  +
                "\r\n # IW FalseNeg Docs\t" +  iNumDistinctFalseOrMissingDocs_noScoreConstraint  +
				sPrecOrRecInfo + 
                "\r\n # NodeIDs [" + ivINodeIDssize  + "] NoideIDList [" + sNodeIDList +
                "]\r\n");
			
		} else
			return "error no report type in TrecCompareCounters.java.toString() : " + iReportType;
    }

    public String toStringShortForXML_common()
    {
		String sPrecOrRecInfo = null;
	    return (
            "<TREC_IW_HITS>" + icounttrecHitsthisQ+", " + lcountIWDistinctDocIDsThisQGt0 + "</TREC_IW_HITS>\r\n" +
			"<NODES>" + ivINodeIDssize + ":" + sNodeIDList+ "</NODES>\r\n" +
            ""); 
			
    }

    public String toStringShortForXMLSpecificToReportType()
    {
		
		String sPrecOrRecInfo = null;
		if ( iReportType == TSPrecisionRecallReport.iREPORTTYPE_PRECISION ) {
			double dPrecision = ((double) lcountIWDistinctDocIDsThisQGt0 -(double) iNumDistinctFalseDocs_GT_zero)/(double) lcountIWDistinctDocIDsThisQGt0;
			sPrecOrRecInfo = 
				"<PVALUE>("+ lcountIWDistinctDocIDsThisQGt0 +"-"+iNumDistinctFalseDocs_GT_zero+")"+"/"+lcountIWDistinctDocIDsThisQGt0 + "="+ UtilStrings.numFormatDouble( dPrecision, 2) +"</PVALUE>";  

		} else if ( iReportType == TSPrecisionRecallReport.iREPORTTYPE_RECALL ){
			double dRecall = ((double) icounttrecHitsthisQ-(double)iNumDistinctFalseOrMissingDocs_noScoreConstraint)/(double) icounttrecHitsthisQ;			
			sPrecOrRecInfo = 
				"<RVALUE>("+ icounttrecHitsthisQ+"-"+iNumDistinctFalseOrMissingDocs_noScoreConstraint+")"+"/"+icounttrecHitsthisQ + "=" + UtilStrings.numFormatDouble( dRecall, 2)  + "</RVALUE>" ; 
		} else
			return "error no report type in TrecCompareCounters.java.toString() : " + iReportType;
							   
 		return  sPrecOrRecInfo   ;
			
    }
	
	public void setDPRD( Hashtable htNodeColonDocSemiColonScore_to_dprd	)
	{
		this.htNodeColonDocSemiColonScore_to_dprd = htNodeColonDocSemiColonScore_to_dprd;
	} 
	
	void CalculateAndOutputAggregateNumbers( int			iNumTopRecords,
									java.io.PrintWriter out
	)
	{
		if (iReportType == TSPrecisionRecallReport.iREPORTTYPE_RECALL)
			iNumTopRecords = 900000000;
		else{
			int idebugme = 0;}
			
		// SORT RECORDS INTO A VECTOR 
		Vector v = new Vector();
		Enumeration e = htNodeColonDocSemiColonScore_to_dprd.keys();
		while ( e.hasMoreElements() )		{
			v.addElement(e.nextElement());	
		} 
		
		for ( int i = 0; i < v.size(); i++ ) 		{
			int iMaxLocation = i;
			String s1 = (String) v.elementAt(i);
			double dMaxValue = new Double(UtilStrings.getStrAfterThisToEnd1Based(s1,";",1)).doubleValue();
			
			for ( int j = i+1; j < v.size(); j++ )
			{
				String s2 = (String) v.elementAt(j);
				double d2 = new Double(UtilStrings.getStrAfterThisToEnd1Based(s2,";",1)).doubleValue();
				if ( d2 > dMaxValue )
				{
					dMaxValue = d2;
					iMaxLocation = j;
				} 
			}
			// swap
			if ( iMaxLocation != i ) 
			{
				String sWap = (String) v.elementAt(i);
				v.setElementAt(v.elementAt(iMaxLocation),i);
				v.setElementAt(sWap,iMaxLocation);
			}
		} 
		
		for ( int i = 0; i < iNumTopRecords && i < v.size(); i++ )
		{
			data_PRReportDetail dprd = (data_PRReportDetail) 
										htNodeColonDocSemiColonScore_to_dprd.get (
											(String) v.elementAt(i));
			htDocsUniqueAllScores.put ( new Integer (dprd.iDocumentid2), "dummy" );
			if ( dprd.dScore1 > 0 ) 
				htDocsWithOneOrMoreNodeDocScores_GtZero.put ( new Integer (dprd.iDocumentid2), "dummy" );
			if ( dprd.dScore1 > 50 ) 
				htDocsWithOneOrMoreNodeDocScores_GtFifty.put ( new Integer (dprd.iDocumentid2), "dummy" );
			
			com.indraweb.util.UtilFile.addLineToFile(
				dprd.sFileNameOut,
			    dprd.iQidIdx + "#1#" +
			    dprd.iQid+ "\t" +
			    dprd.sQDesc+ "\t" +
			    dprd.iDocumentid2+ "\t" +
			    dprd.iNodeid + "\t" +
			    dprd.sDoctitle+ "\t" +
			    dprd.sDdocurl+ "\t" +
			    dprd.sNodeTitle+ "\t" +
			    dprd.iCorpusid+ "\t" +
			    dprd.dScore1 +       "\t" +
			    dprd.dScore5 +            "\t" +
			    dprd.dScore6 +                  "\t" +
			    dprd.dScore5X6 + "\r\n"
			);
		} // for ( int i = 0; i < iNumTopRecords && i < v.size(); i++ )
		
		iNumDistinctFalseDocs_GT_fifty = htDocsWithOneOrMoreNodeDocScores_GtFifty.size();
		iNumDistinctFalseDocs_GT_zero = htDocsWithOneOrMoreNodeDocScores_GtZero.size();
		iNumDistinctFalseOrMissingDocs_noScoreConstraint = htDocsUniqueAllScores.size();
		
	}  // void CalculateAggregateNumbers( int iNumTopRecords )

	

}
