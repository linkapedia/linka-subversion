package api.tsuser;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;

import api.TSException;
import api.security.*;

import com.iw.system.User;

import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * Is the current user (indicated by their session key) a system administrator?
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsuser.TSIsAdministrator&SKEY=-132981656

 *	@return	either the AUTHORIZED or NOTAUTHORIZED tag
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <AUTHORIZED>1</AUTHORIZED>
  </TSRESULT>
  \endverbatim
 */
public class TSIsAdministrator
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		if (!u.IsMember(out)) { out.println("<NOTAUTHORIZED>1</NOTAUTHORIZED>"); }
        else { out.println("<AUTHORIZED>1</AUTHORIZED>"); }
    }
}