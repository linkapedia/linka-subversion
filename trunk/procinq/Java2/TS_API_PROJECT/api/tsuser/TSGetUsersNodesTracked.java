package api.tsuser;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;

import api.emitxml.EmitGenXML_ErrorInfo;
import api.security.*;

import com.indraweb.database.*;

/**
 * This API call has been deprecated, and should no longer be used.
 * @note    This API call has been deprecated.
 */
public class TSGetUsersNodesTracked
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE);
    }
}
