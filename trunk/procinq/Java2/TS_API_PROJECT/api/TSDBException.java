package api;

import api.emitxml.EmitGenXML_ErrorInfo;
import api.Log;

public class TSDBException extends TSException
{
	public TSDBException ()
	{
		super ( EmitGenXML_ErrorInfo. ERR_TS_DB_CONNECT_ERROR );
	} 
	public TSDBException (String s )
	{
		super ( EmitGenXML_ErrorInfo. ERR_TS_DB_CONNECT_ERROR, s );
	} 
}
