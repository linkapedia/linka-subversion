package api.tsdomain;

import com.iw.system.WrapperListCategoriesDomain;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author andres
 */
public class Parser {

    public static String parser(WrapperListCategoriesDomain data) throws Exception {
        JAXBContext jc;
        StringWriter writer = new StringWriter();
        try {
            jc = JAXBContext.newInstance(WrapperListCategoriesDomain.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.marshal(data, writer);
        } catch (JAXBException ex) {
            throw new Exception("Error parsing xml data", ex.getCause());
        }
        return writer.toString();
    }
}
