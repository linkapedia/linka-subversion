package api.tsdomain;

import com.iw.system.CategoryDomain;
import com.iw.system.WrapperListCategoriesDomain;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TSGetCategoriesDomain {

    private static final String GET_CATEGORIES_DOMAIN = "select * from categorydomain";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery(GET_CATEGORIES_DOMAIN);
        List<CategoryDomain> categoriesDomain = new ArrayList<>();
        while (rs.next()) {
            String id = rs.getString(1);
            String categoryName = rs.getString(2);
            categoriesDomain.add(new CategoryDomain(id, categoryName));
        }
        WrapperListCategoriesDomain wrapper = new WrapperListCategoriesDomain();
        wrapper.setList(categoriesDomain);
        String data = Parser.parser(wrapper);
        out.println(data);
    }
}
