package api.tsmusthavesgate;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.db.ConnectionFactory;
import com.linkapedia.procinq.server.musthavegate.MusthaveGateDao;
import com.linkapedia.procinq.server.security.Authoritation;
import java.io.PrintWriter;
import java.sql.Connection;

public class TSDeleteMusthavesGate {

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, 
            Connection dbc) throws Exception {
        
        String sKey = (String) props.get("SKEY", true);
        String nodeid = (String) props.get("nodeid");
        String corpusid = (String) props.get("corpusid");
        String depth = (String) props.get("depth");
        if (nodeid == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }

        if (!Authoritation.isAuthorizedUser(Integer.parseInt(corpusid), sKey, out)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }

        int depthInt = Integer.parseInt(depth);
        int nodeidInt = Integer.parseInt(nodeid);
        Connection connection = null;
        try {
            connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            if (depthInt == -1) {
                MusthaveGateDao.deleteMusthaveGateToLastLevel(connection, nodeidInt);
            } else {
                MusthaveGateDao.deleteMusthaveGateToLevel(connection, nodeidInt, depthInt);
            }
        } finally {
            connection.close();
        }
        out.println("<SUCCESS>successfully.</SUCCESS>");
    }
}
