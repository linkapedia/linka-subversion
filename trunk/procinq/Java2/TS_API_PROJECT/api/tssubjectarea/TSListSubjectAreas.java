package api.tssubjectarea;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 *  List all subject areas in the server.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  CorpusID (optional) Use the unique identifier of the corpus (taxonomy) to restrict results by corpus
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSListSubjectAreas&SKEY=993135977

 *	@return	series of subject area objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <SUBJECTAREAS>
        <SUBJECTAREA>
            <SUBJECTAREAID>3</SUBJECTAREAID>
            <SUBJECTNAME>Bioterrorism</SUBJECTNAME>
            <SUBJECTAREASTATUS>1</SUBJECTAREASTATUS>
        </SUBJECTAREA>
 ...
 </TSRESULT>
  \endverbatim
 */
public class TSListSubjectAreas
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// Corpus ID is optional
		String sCorpusID = (String) props.get ("CorpusID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null; ResultSet rs = null;

		try {

			if (sCorpusID != null) {
                if ((!Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) &&
                    (!u.IsAuthorized(sCorpusID, out)))
                { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            }

			String sSQL = " select SubjectAreaID, SubjectName, SubjectAreaStatus from SubjectArea ";
			if (sCorpusID != null) { sSQL = sSQL+" where SubjectAreaID IN (select SubjectAreaID from CorpusSubject "+
											" where CorpusID = "+sCorpusID+")"; }
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				int iID = rs.getInt(1);
				String sName = rs.getString(2);
				int iStatus = rs.getInt(3);

				loop = loop + 1;
				if (loop == 1) { out.println ("<SUBJECTAREAS>"); }
				out.println ("   <SUBJECTAREA> ");
				out.println ("      <SUBJECTAREAID>"+iID+"</SUBJECTAREAID>");
				out.println ("      <SUBJECTNAME>"+sName+"</SUBJECTNAME>");
				out.println ("      <SUBJECTAREASTATUS>"+iStatus+"</SUBJECTAREASTATUS>");
				out.println ("   </SUBJECTAREA>");
			}
			if (loop > 0) {	out.println ("</SUBJECTAREAS>"); }
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
