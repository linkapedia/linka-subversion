package api.tssubjectarea;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Add a corpus relationship to the given subject area.  Corpora that do not exist in any subject areas will
 *   not display in the ITS web client.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  SubjectAreaID   Unique subject area identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSAddCorpusSubject&CorpusID=4&SubjectAreaID=1&SKEY=993135977

 *	@return	SUCCESS tag if relationship was successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Corpus subject area added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddCorpusSubject
{
	// TSAddCorpusFolder (sessionid, corpusid, folderid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sSubjectID = (String) props.get ("SubjectAreaID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;

		try {
			if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = "insert into CorpusSubject (CorpusId, SubjectAreaId) values ("+sCorpusID+","+sSubjectID+")";
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Insert into CorpusSubject table failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_GENRE_FAILURE);
			}
            out.println("<SUCCESS>Corpus subject area added successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
