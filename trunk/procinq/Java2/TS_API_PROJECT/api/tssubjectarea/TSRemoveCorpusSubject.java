package api.tssubjectarea;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove a corpus relationship from the given subject area.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  SubjectAreaID   Unique subject area identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSRemoveCorpusSubject&CorpusID=4&SubjectAreaID=1&SKEY=993135977

 *	@return	SUCCESS tag if relationship was successfully.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Corpus subject area removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveCorpusSubject
{
	// TSRemoveCorpusFolder (sessionid, corpusid, folderid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sSubjectID = (String) props.get ("SubjectAreaID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;

		try {
			if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			if (sCorpusID == null) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
			}

			String sSQL = "delete from CorpusSubject where CorpusID = "+sCorpusID;
			if (sSubjectID != null) { sSQL = sSQL + " and SubjectAreaID = "+sSubjectID; }
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from CorpusSubject table failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_GENRE_DELETE_FAILURE);
			}
            out.println("<SUCCESS>Corpus subject area removed successfully.</SUCCESS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
