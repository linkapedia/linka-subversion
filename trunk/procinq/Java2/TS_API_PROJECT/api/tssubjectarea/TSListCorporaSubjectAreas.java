package api.tssubjectarea;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.iw.system.User;

/**
 *  List all subject areas and their corresponding corpora.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://itsserver/servlet/ts?fn=tssubjectarea.TSListCorporaSubjectAreas&SKEY=993135977

 *	@return	series of subject areas and corpora.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <SUBJECTAREAS>
        <SUBJECTAREA>
            <SUBJECTAREAID>3</SUBJECTAREAID>
            <SUBJECTNAME>Bioterrorism</SUBJECTNAME>
            <SUBJECTAREASTATUS>1</SUBJECTAREASTATUS>
            <CORPORA>
                <CORPUS>
                    <CORPUSID>100058</CORPUSID>
                    <CORPUS_NAME>Anarchists Cookbook</CORPUS_NAME>
                    <CORPUSDESC>None</CORPUSDESC>
                    <ROCF1>0.0</ROCF1>
                    <ROCF2>0.0</ROCF2>
                    <ROCF3>0.0</ROCF3>
                    <ROCC1>0.0</ROCC1>
                    <ROCC2>0.0</ROCC2>
                    <ROCC3>0.0</ROCC3>
                    <CORPUSACTIVE>1</CORPUSACTIVE>
            </CORPUS>
 ...
 </TSRESULT>
  \endverbatim
 */
public class TSListCorporaSubjectAreas
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		// Corpus ID is optional
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null; ResultSet rs = null;

		try {
			String sSQL = " select S.SubjectAreaID, S.SubjectName, S.SubjectAreaStatus, "+
                "C.CorpusID, C.Corpus_Name, C.CorpusDesc, C.ROCF1, C.ROCF2, C.ROCF3, "+
                "C.ROCC1, C.ROCC2, C.ROCC3, C.CorpusActive from SubjectArea S, Corpus C, CorpusSubject SC "+
                "where C.CorpusActive != -1 and S.SubjectAreaID = SC.SubjectAreaID and C.CorpusID = SC.CorpusID "+
                "order by S.SubjectName, C.Corpus_Name asc";
            //out.println("<DEBUG>"+sSQL+"</DEBUG>");
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL);

            Hashtable htAuth = u.GetAuthorizedHash(out);

			int id = 0;
			while ( rs.next() ) {
				int iID = rs.getInt(1);
				String sName = rs.getString(2);
				int iStatus = rs.getInt(3);

                int iCorpusID = rs.getInt(4);
                String sCorpusName = rs.getString(5);
                String sCorpusDesc = rs.getString(6);
                float fRocF1 = rs.getFloat(7);
                float fRocF2 = rs.getFloat(8);
                float fRocF3 = rs.getFloat(9);
                float fRocC1 = rs.getFloat(10);
                float fRocC2 = rs.getFloat(11);
                float fRocC3 = rs.getFloat(12);
                int iCorpusActive = rs.getInt(13);

                if ((Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) ||
                    (htAuth.containsKey(iCorpusID+""))) {
                    if (id == 0) { out.println ("<SUBJECTAREAS>"); }
                    if (iID != id) {
                        if (id != 0) { out.println("      </CORPORA>"); out.println("   </SUBJECTAREA>"); }
                        id = iID;
                        out.println ("   <SUBJECTAREA> ");
                        out.println ("      <SUBJECTAREAID>"+iID+"</SUBJECTAREAID>");
                        out.println ("      <SUBJECTNAME>"+sName+"</SUBJECTNAME>");
                        out.println ("      <SUBJECTAREASTATUS>"+iStatus+"</SUBJECTAREASTATUS>");
                        out.println ("      <CORPORA>");
                    }
					out.println ("         <CORPUS>");
					out.println ("            <CORPUSID>"+iCorpusID+"</CORPUSID>");
					out.println ("            <CORPUS_NAME>"+sCorpusName+"</CORPUS_NAME>");
					out.println ("            <CORPUSDESC>"+sCorpusDesc+"</CORPUSDESC>");
					out.println ("            <ROCF1>"+fRocF1+"</ROCF1>");
					out.println ("            <ROCF2>"+fRocF2+"</ROCF2>");
					out.println ("            <ROCF3>"+fRocF3+"</ROCF3>");
					out.println ("            <ROCC1>"+fRocC1+"</ROCC1>");
					out.println ("            <ROCC2>"+fRocC2+"</ROCC2>");
					out.println ("            <ROCC3>"+fRocC3+"</ROCC3>");
					out.println ("            <CORPUSACTIVE>"+iCorpusActive+"</CORPUSACTIVE>");
					out.println ("         </CORPUS>");
                }
			}
            if (id == 0) { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_CORPORA_THIS_DB); }
            out.println("      </CORPORA>");
            out.println ("   </SUBJECTAREA> ");
			out.println ("</SUBJECTAREAS>");
		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
