package api;

import api.tsnode.images.mq.ConsumerImageCrawl;
import api.wikipedia.mq.Consumer;
import com.linkapedia.procinq.server.musthave.queue.ConsumerMusthaveProcess;
import com.npstrandberg.simplemq.MessageQueue;
import com.npstrandberg.simplemq.MessageQueueService;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ProcinqContextListener implements ServletContextListener {

    private static final String DEFAULT_QUEUE_NAME_WIKIPEDIA = "WIKIPEDIA";
    private static final String DEFAULT_QUEUE_NAME_QAREPORT = "QAREPORT";
    private static final String DEFAULT_QUEUE_NAME_IMAGES = "IMAGECRAWLQUEUE";
    private static final String MUSTHAVE_QUEUE = "MUSTHAVEQUEUE";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        MessageQueue queue = MessageQueueService.getMessageQueue(DEFAULT_QUEUE_NAME_WIKIPEDIA);
        Consumer consumer = Consumer.getInstance();
        consumer.setMessageQueue(queue);
        new Thread(consumer).start();

        MessageQueue queueQaReport = MessageQueueService.getMessageQueue(DEFAULT_QUEUE_NAME_QAREPORT);
        api.qareport.mq.Consumer consumerQaReport = null;
        for (int i = 1; i <= 5; i++) {
            consumerQaReport = new api.qareport.mq.Consumer();
            consumerQaReport.setMessageQueue(queueQaReport);
            new Thread(consumerQaReport).start();
        }
  
        MessageQueue queueImages = MessageQueueService.getMessageQueue(DEFAULT_QUEUE_NAME_IMAGES);
        ConsumerImageCrawl consumerImages = new ConsumerImageCrawl();
        consumerImages.setMessageQueue(queueImages);
        new Thread(consumerImages).start();

        MessageQueue queueMusthave = MessageQueueService.getMessageQueue(MUSTHAVE_QUEUE);
        ConsumerMusthaveProcess consumerMusthave = null;
        for (int i = 1; i <= 5; i++) {
            consumerMusthave = new ConsumerMusthaveProcess();
            consumerMusthave.setMessageQueue(queueMusthave);
            new Thread(consumerMusthave).start();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
