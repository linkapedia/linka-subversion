package api.tests;

import org.dom4j.Element;

import java.util.*;
import java.io.PrintWriter;
import java.sql.ResultSet;


public class TestItem {
    // Combined attributes
    private api.APIProps arguments = new api.APIProps(new Properties());
    private Hashtable htTestInfoFromXML = new Hashtable();
    private Hashtable htNegativeAssertions = new Hashtable();
    private Hashtable htPositiveAssertions = new Hashtable();

    //private StringBuffer sbTestInfo = new StringBuffer();

    // constructor(s)
    public TestItem() { }
    public TestItem(Iterator i) throws Exception {
        while (i.hasNext()) {
            Element e = (Element) i.next();
            String s = e.getName();


            if (s.equals("ARGS")) fillArguments(e);
            else if (s.equals("ASSERTIONS")) fillAssertions(e);
            else htTestInfoFromXML.put (e.getName(), e.getText());
        }
    }

    public String getTestInfoFromXML (String sKey, boolean bRequired)
    {
        String sRet = (String) htTestInfoFromXML.get ( sKey );
        if ( sRet == null && bRequired )
            api.Log.LogError("test info not found [" + sKey + "]", new Exception ("test info not found "));
        return sRet;
    }
    public Hashtable getTestInfoHT ()
    {
        return htTestInfoFromXML;
    }

    public String getFunctionName() { return (String) arguments.get("fn"); }
    public api.APIProps getArguments() { return arguments; }
    public String getTestID() { return (String) htTestInfoFromXML.get("TESTID"); }
    public String getTestSequence() { return (String) htTestInfoFromXML.get("TESTSEQUENCE"); }
    public Hashtable getNegativeAssertions() { return htNegativeAssertions; }
    public Hashtable getPositiveAssertions() { return htPositiveAssertions; }

    private void fillArguments(Element eArguments) {
        Iterator i = eArguments.elements().iterator();
        while (i.hasNext()) {
            Element eArgument = (Element) i.next();
            Element eParam = eArgument.element("PARM");
            Element eValue = eArgument.element("VALUE");

            if (!eParam.getText().toLowerCase().equals("skey")) {
                arguments.put(eParam.getText(), eValue.getText());
            }
        }
        arguments.put("RUNNINGTEST", "TRUE");
    }
    private void fillAssertions(Element eAssertions) throws Exception {
        Iterator i = eAssertions.elements().iterator();
        while (i.hasNext()) {
            Element eAssertion = (Element) i.next();
            Assertion as = new Assertion(eAssertion);

            if ( as.isAPositiveOnIWErrorOrTSError())
                throw new Exception ("as.isAPositiveOnIWErrorOrTSError()");
            if (as.getExists())
                htPositiveAssertions.put(as.getDesc(), as);
            else
                htNegativeAssertions.put(as.getDesc(), as);
        }
    }

    public String getAPICallURL()
    {
        return getAPICallURL ("http://localhost/itsapi/ts?", arguments);
    }
    public static String getAPICallURL(String sHTTPPre, Hashtable arguments)
    {
        StringBuffer sb = new StringBuffer(sHTTPPre);
        Enumeration enumArgKeys = arguments.keys();
        int i = 0;
        boolean bFoundskey = false;
        while ( enumArgKeys.hasMoreElements())
        {
            String sArgKey = (String) enumArgKeys.nextElement();
            String sValue = (String) arguments.get ( sArgKey );

            if ( i > 0 )
                sb.append("&");
            i++;
            if ( sArgKey.equalsIgnoreCase("skey"))
                bFoundskey = true;
            sb.append(sArgKey+"="+sValue);

        }
        if ( i > 0 && !bFoundskey)
            sb.append("&");
        i++;
        sb.append("skey=SKEY___");
        String s1 = sb.toString();
        String s2 = s1.replaceAll("<", "[");
        String s3 = s2.replaceAll(">", "]");

        return s3;
    }

}
