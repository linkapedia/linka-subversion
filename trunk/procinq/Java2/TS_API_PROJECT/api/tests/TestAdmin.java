/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jun 4, 2004
 * Time: 4:14:42 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package api.tests;

import com.indraweb.util.*;
import com.indraweb.html.UtilHTML;
import com.iw.tools.Dom4jHelper;
import api.APIHandler;

import java.util.Enumeration;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.FileInputStream;
import java.io.File;

public class TestAdmin
{
    public static void main (String args [])
    {
        try
        {
            String sDirHome = "/temp/ITStest";
            while (true)
            {

                System.out.println ("Enter menu item: \r\n(1) list tests, \r\n(2) run a test : (q) quit.\r\n:");
                int iResponse = UtilKeyReads.readInt();
                switch ( iResponse )
                {
                    case 1 :
                    {
                        emitFiles();
                        break;
                    }
                    case 2 :
                    {
                        String[] sArrXmlFileNames = emitFiles();
                        System.out.print ("Select a file number from above :");
                        int iResponselocal = UtilKeyReads.readInt();

                        runTestFromXmlTestFile( sArrXmlFileNames[iResponselocal] );
                        break;
                    }

                }
            }
        } catch ( Exception e ) {
            System.out.print ("Top level error in Test Admininistration program " + api.Log.stackTraceToString(e));
        }
    }
    private static int runTestFromXmlTestFile (String sFileName )
    {
        try
        {
            System.out.println("running file [" + APIHandler.sCDMSTESTDIRHome +sFileName + "]" );
            System.out.println("sure you want to run this? 1=y 2=n:");
            int iSure = UtilKeyReads.readInt();
            if ( iSure != 1 )
            {
                System.out.println("aborting file run [" + APIHandler.sCDMSTESTDIRHome+sFileName + "]");
            }
            // back up file
            String sBackFileOnly = System.currentTimeMillis() + "_backup.xml";
            String sBackFile = APIHandler.sCDMSTESTDIRHome + sBackFileOnly;
            System.out.println("attempted : renamed/backed up base file to [" + sBackFileOnly + "]" );
            UtilFile.renameFile ( APIHandler.sCDMSTESTFILE, sBackFile );

           // File f = new File(APIHandler.sCDMSTESTDIRHome + sFileName);
            // copy test xml file to standard test xml file location
            UtilFile.fileCopy( APIHandler.sCDMSTESTDIRHome + sFileName, APIHandler.sCDMSTESTFILE );
            System.out.println("copied [" + sFileName + "] to base file" );

            // run test
            String sURL = "http://localhost/itsapi/ServletTestRunner?suite=IndraTestCase";
            String sFileNameRunOut = APIHandler.sCDMSTESTDIRHome +"Run_"+System.currentTimeMillis()+".txt";
            UtilHTML.captureHTMLToFile(sURL, sFileNameRunOut, true);
            String sXmlRunOutAsString = UtilFile.getFileAsString ( sFileNameRunOut );
            org.dom4j.Document d = Dom4jHelper.getDomFromstring( sXmlRunOutAsString +"<TESTCASES>" ) ;

            // delete copied test xml file
            UtilFile.fileCopy( sFileName, APIHandler.sCDMSTESTFILE );

            System.out.print("delete file ? [" + APIHandler.sCDMSTESTFILE  + "] 1=y,2=n,q=quit :" );
            int iResponse = UtilKeyReads.readLineInt();
            if ( iResponse == 1)
            {
                UtilFile.deleteFile(APIHandler.sCDMSTESTFILE  );
                System.out.println("removed [" + APIHandler.sCDMSTESTFILE  + "]" );
            }

            return 0;
        }   catch ( Exception e )
        {
            e.printStackTrace();
            System.out.println("error in runTestFromXmlTestFile : " + api.Log.stackTraceToString(e) );
            return 1;
        }

    }


    private static String[] emitFiles() throws Exception
    {
        String[] sArrXmlFileNames = UtilFileEnumerator.listFilesInFolderNonRecursive
                ( APIHandler.sCDMSTESTDIRHome, true, ".xml", UtilFileEnumerator.iMODE_CONSTRAIN_NAME_ENDSWITH);
        System.out.print( "\r\n");
        for ( int i = 0; i < sArrXmlFileNames.length; i++)
        {
            String sFileName = (String) sArrXmlFileNames[i];
            System.out.println( i + ". " + sFileName);
        }
        return sArrXmlFileNames;

    }
}
