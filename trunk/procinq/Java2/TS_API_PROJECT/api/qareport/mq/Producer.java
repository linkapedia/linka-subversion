package api.qareport.mq;

import com.npstrandberg.simplemq.MessageInput;
import com.npstrandberg.simplemq.MessageQueue;

/**
 * Producer to qa report
 *
 * @author andres
 */
public class Producer {

    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    public Producer() {
    }

    public boolean sendMessage(MessageQAReport message) {
        MessageInput mi = new MessageInput();
        mi.setObject(message);
        return messageQueue.send(mi);
    }
}
