package api.qareport.mq;

import api.qareport.DynamoDao;
import api.qareport.QaReportWorker;
import com.npstrandberg.simplemq.Message;
import com.npstrandberg.simplemq.MessageQueue;

/**
 *
 * @author andres
 */
public class Consumer implements Runnable {

    private static final org.apache.log4j.Logger log =
            org.apache.log4j.Logger.getLogger(Consumer.class);
    private int millis = 5000;
    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    public Consumer() {
    }

    public void run() {
        DynamoDao dao = new DynamoDao();
        QaReportWorker worker;
        MessageQAReport ma = new MessageQAReport();
        Message m;
        while (true) {
            try {
                m = messageQueue.receiveAndDelete();
                if (m != null) {
                    ma = (MessageQAReport) m.getObject();
                    //read the objet and start the process
                    worker = new QaReportWorker();
                    worker.setSourceDao(dao);
                    worker.start(ma);
                }
            } catch (Exception e) {
                //error in the process a message
                log.error("Error procesing data from the queue requestId:" + ma.getRequestId(), e);
            }
            try {
                //TODO: manage the millis with a logic exponencial
                Thread.sleep(millis);
            } catch (InterruptedException ex) {
                log.error("THE WIKIPEDIA CONSUMER IS DEAD, InterruptedException", ex);
            }
        }
    }
}
