package api.qareport.mq;

import java.io.Serializable;

/**
 *
 * @author andres
 */
public class MessageQAReport implements Serializable {

    private String requestId;
    private String corpusId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }
}
