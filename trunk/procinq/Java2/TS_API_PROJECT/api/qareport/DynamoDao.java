package api.qareport;

import api.util.ConfServer;
import api.util.S3Storage;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public class DynamoDao implements SourceDataDao {

    @Override
    public Map<String, String> getDocumentsNumber(List<String> nodesids) {
        if (nodesids == null) {
            return null;
        }
        BasicAWSCredentials credentials = new BasicAWSCredentials(
                ConfServer.getValue("linkapedia.aws.accessKey"),
                ConfServer.getValue("linkapedia.aws.secretKey"));
        AmazonDynamoDBClient dynamoClient = new AmazonDynamoDBClient(credentials);
        dynamoClient.setEndpoint(ConfServer.getValue("com.linkapedia.api.dynamo.endpoint"));
        Map<String, String> infoDocuments = new HashMap<>();
        for (String i : nodesids) {
            try {
                StringBuilder sb = new StringBuilder(i);
                Condition hashKeyCondition = new Condition()
                        .withComparisonOperator(ComparisonOperator.EQ)
                        .withAttributeValueList(new AttributeValue().withS(sb.reverse().toString()));

                Map<String, Condition> keyConditions = new HashMap<String, Condition>();
                keyConditions.put("nodeID", hashKeyCondition);

                QueryRequest queryRequest = new QueryRequest()
                        .withTableName("NodeDocument")
                        .withKeyConditions(keyConditions).withAttributesToGet("nodeID");

                QueryResult result = dynamoClient.query(queryRequest);
                infoDocuments.put(i, String.valueOf(result.getCount()));
            } catch (Exception e) {
                infoDocuments.put(i, "Error!");
            }
        }
        return infoDocuments;
    }

    @Override
    public Map<String, Boolean> getImagesStatus(List<String> nodesids) {
        if (nodesids == null) {
            return null;
        }
        String key;
        StringBuilder sb;
        Map<String, Boolean> infoImages = new HashMap<>();
        for (String item : nodesids) {
            sb = new StringBuilder(item);
            sb.reverse();
            key = sb.toString() + "/images/" + sb.toString() + "_0_nocrop.jpg";
            boolean result = S3Storage.exits("nodeimages", key);
            if (result) {
                infoImages.put(item, true);
            } else {
                infoImages.put(item, false);

            }
        }
        return infoImages;
    }
}
