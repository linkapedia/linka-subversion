package api.qareport;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.qareport.mq.MessageQAReport;
import api.qareport.mq.Producer;
import api.util.ConfServer;

import com.iw.db.ConnectionFactory;
import com.npstrandberg.simplemq.MessageQueue;
import com.npstrandberg.simplemq.MessageQueueService;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * TODO: this method not have security. get corpusid and get count documents and
 * if nodes have correct images on AWS, save the result on oracle to get queries
 * easier
 *
 * @author andres
 */
public class TSQAReportProcess {

    private static final Logger log = Logger.getLogger(TSQAReportProcess.class);
    private static final String PROCESS_WAITING = "WAITING";
    private static final String DEFAULT_QUEUE_NAME = "QAREPORT";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {
        log.info("TSQAReportProcess called");
        //validate AUTHORIZED
        //String sKey = (String) props.get("SKEY", true);
        String taxonomy = (String) props.get("taxonomy");

        //validate the parameters
        if (taxonomy == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (taxonomy.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        //save the process status in oracle
        Map<String, String> parameters = new HashMap<String, String>();
        Date dateTime = new Date();
        parameters.put("TIMESTAMP", dateTime.getTime() + "");
        parameters.put("CORPUS", taxonomy);
        String requestId = "";
        try {
            requestId = saveProcessStatus(parameters);
        } catch (Exception e) {
            log.error("Error saving process status in WAITING");
            throw e;
        }
        MessageQAReport message = new MessageQAReport();
        message.setRequestId(requestId);
        message.setCorpusId(taxonomy);
        //send data to the queue
        Producer producer = new Producer();
        String queueName = DEFAULT_QUEUE_NAME;
        try {
            queueName = ConfServer.getValue("qareport.crawl.queue.name");
        } catch (Exception e) {
            log.error("Error getting the queueName set default to: "
                    + DEFAULT_QUEUE_NAME, e);
        }
        MessageQueue queue = MessageQueueService.getMessageQueue(queueName);
        producer.setMessageQueue(queue);//pass the queue
        producer.sendMessage(message);
        out.println("<SUCCESS>process added ok for corpus: " + taxonomy + "</SUCCESS>");
    }

    /**
     * save initial status
     *
     * @param fields
     * @return
     * @throws Exception
     */
    private static String saveProcessStatus(Map<String, String> fields) throws Exception {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new Exception("saveProcessStatus: saveProcessStatus(.."
                    + " Error getting connection from oracle");
        }
        String sql = "INSERT INTO SBOOKS.QAREPORT (REQUESTID,TIMESTAMP,"
                + "CORPUS,STATUS) VALUES (SEC_QAREPORT.nextval,?,?,?)";
        PreparedStatement pstmt = null;
        String key = "";
        ResultSet generatedKeys = null;
        try {
            pstmt = conn.prepareStatement(sql, new String[]{"REQUESTID"});
            pstmt.setLong(1, Long.parseLong(fields.get("TIMESTAMP")));
            pstmt.setString(2, fields.get("CORPUS"));
            pstmt.setString(3, PROCESS_WAITING);
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new Exception("Creating qaReportProcess failed");
            }
            generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                key = String.valueOf(generatedKeys.getLong(1));
            } else {
                throw new SQLException("Creating qaReportProcess failed,"
                        + " no generated key obtained.");
            }
            return key;
        } catch (Exception e) {
            log.error("Error in the process status in qareport", e);
            throw e;
        } finally {
            try {
                if (generatedKeys != null) {
                    generatedKeys.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
