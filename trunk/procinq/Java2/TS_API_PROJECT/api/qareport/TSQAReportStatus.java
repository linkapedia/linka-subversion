package api.qareport;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.db.ConnectionFactory;
import com.iw.system.QaReportModel;
import com.iw.system.WrapperListQaReport;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * get the report to the wikipedia process
 *
 * @author andres
 */
public class TSQAReportStatus {

    private static final Logger log = Logger.getLogger(api.report.TSGenerateReport.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {
        log.debug("TSWikipediaReport");
        String rownum = (String) props.get("rownum");
        String taxonomy = (String) props.get("taxonomy");
        if (rownum == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (rownum.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        List<QaReportModel> report = getReportWQaReport(rownum, taxonomy);
        WrapperListQaReport modelReport = new WrapperListQaReport();
        modelReport.setList(report);
        String data = Transformation.getData(modelReport);
        out.println(data);
    }

    private static List<QaReportModel> getReportWQaReport(String rowNum,
            String taxonomy) throws Exception {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("getReportWQaReport(.. "
                    + "Error getting connection from oracle");
        }
        List<QaReportModel> listToReturn = new ArrayList<>();
        QaReportModel aqModel;
        String sql = "";
        if (taxonomy == null || taxonomy.isEmpty()) {
            sql = " SELECT * from (SELECT *   FROM ( "
                    + "SELECT page.*, rownum as row_num FROM ( "
                    + "SELECT * from QAREPORT order by requestid DESC "
                    + ") page "
                    + ") "
                    + "WHERE row_num <= ?)";
        } else {
            sql = " SELECT * from (SELECT *   FROM ( "
                    + "SELECT page.*, rownum as row_num FROM ( "
                    + "SELECT * from QAREPORT where corpus = ? order by requestid DESC "
                    + ") page "
                    + ") "
                    + "WHERE row_num <= ?)";
        }
        PreparedStatement pstmt = null;
        ResultSet result = null;

        try {
            pstmt = conn.prepareStatement(sql);
            if (taxonomy == null || taxonomy.isEmpty()) {
                pstmt.setInt(1, Integer.parseInt(rowNum));
            } else {
                pstmt.setInt(1, Integer.parseInt(taxonomy));
                pstmt.setInt(2, Integer.parseInt(rowNum));
            }

            result = pstmt.executeQuery();
            while (result.next()) {
                aqModel = new QaReportModel();
                aqModel.setRequestId(String.valueOf(result.getLong("REQUESTID")));
                aqModel.setTimestamp(String.valueOf(result.getLong("TIMESTAMP")));
                aqModel.setCorpus(result.getString("CORPUS"));
                aqModel.setStatus(result.getString("STATUS"));
                listToReturn.add(aqModel);
            }
            return listToReturn;
        } catch (Exception e) {
            log.error("Error in the process status in qareport", e);
            throw new SQLException("Error in the process status in qareport", e);
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
