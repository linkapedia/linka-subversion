package api.qareport;

import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public interface SourceDataDao {

    public Map<String, String> getDocumentsNumber(List<String> nodesids);

    public Map<String, Boolean> getImagesStatus(List<String> nodesids);
}
