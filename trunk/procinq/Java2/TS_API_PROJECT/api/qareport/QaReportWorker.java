package api.qareport;

import api.qareport.mq.MessageQAReport;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public class QaReportWorker {

    private SourceDataDao sourceDao;
    private static final int DEFAULT_PAGINATION = 1000;
    private static final org.apache.log4j.Logger log =
            org.apache.log4j.Logger.getLogger(QaReportWorker.class);
    private static final String PROCESS_IN_PROCESS = "IN PROCESS";
    private static final String PROCESS_FINISHED = "FINISHED";

    public QaReportWorker() {
    }

    public SourceDataDao getSourceDao() {
        return sourceDao;
    }

    public void setSourceDao(SourceDataDao sourceDao) {
        this.sourceDao = sourceDao;
    }

    /**
     *
     * @param corpusId
     */
    public void start(MessageQAReport message) {
        int from = 1;
        int to = DEFAULT_PAGINATION;
        List<String> nodes = new ArrayList<>();
        QAReportOracleDao daoTarget = new QAReportOracleDao();
        Map<String, String> infoDocuments;
        Map<String, Boolean> infoImages;

        //update in oracle with request id in process
        try {
            daoTarget.updateProcessStatus(message.getRequestId(), PROCESS_IN_PROCESS);
        } catch (SQLException ex) {
            log.error("Error updating the qareport status by IN PROCESS", ex);
        }


        do {
            try {
                //get nodes from oracle using pagination
                nodes = daoTarget.getNodes(message.getCorpusId(), from, to);
            } catch (SQLException ex) {
                log.error("Error getting nodes from oracle, ", ex);
                break;
            }
            //queries to AWS for this nodes
            infoDocuments = sourceDao.getDocumentsNumber(nodes);
            infoImages = sourceDao.getImagesStatus(nodes);
            //save the AWS info in table node
            try {
                daoTarget.updateDocuments(infoDocuments);
            } catch (SQLException ex) {
                log.error("Error updating documents number in table Node", ex);
            }
            try {
                daoTarget.updateImages(infoImages);
            } catch (SQLException ex) {
                log.error("Error updating images status in table Node", ex);
            }
            // pagination logic
            from = to + 1;
            to = to + DEFAULT_PAGINATION;
        } while (!nodes.isEmpty());

        //update process status
        try {
            daoTarget.updateProcessStatus(message.getRequestId(), PROCESS_FINISHED);
        } catch (SQLException ex) {
            log.error("Error updating the qareport status by FINISHED", ex);
        }
    }
}
