package api.qareport;

import com.iw.db.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class QAReportOracleDao {

    private static final Logger log = Logger.getLogger(QAReportOracleDao.class);
    private static final int BATCH_UPDATER = 100;

    public List<String> getNodes(String corpusId, int from, int to) throws SQLException {
        log.debug("NodeUtils: getTreeNodesNodeid");
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("Error getting connection from oracle");
        }
        List<String> listReturn = new ArrayList<>();
        String sql = "SELECT * from (SELECT * FROM ( "
                + "SELECT page.*, rownum as row_num FROM ( "
                + "select nodeid from node where corpusid = ? "
                + ") page "
                + ") "
                + "WHERE row_num BETWEEN ? AND ?)";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(corpusId));
            pstmt.setInt(2, from);
            pstmt.setInt(3, to);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String nodeid = String.valueOf(rs.getInt("NODEID"));
                listReturn.add(nodeid);
            }
            return listReturn;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void updateDocuments(Map<String, String> documentsInfo) throws SQLException {
        if (documentsInfo == null || documentsInfo.isEmpty()) {
            return;
        }
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("updateDocuments(.. Error getting connection from oracle");
        }
        String sql = "UPDATE SBOOKS.Node SET DOCUMENTSCOUNT=? WHERE NODEID=?";
        PreparedStatement pstmt = null;
        try {
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(sql);
            int cont = 0;
            for (Map.Entry node : documentsInfo.entrySet()) {
                cont++;
                pstmt.setLong(1, Long.parseLong(node.getValue().toString()));
                pstmt.setInt(2, Integer.parseInt(node.getKey().toString()));
                pstmt.addBatch();
                if (cont == BATCH_UPDATER) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            //the last data
            if (cont != 0) {
                //update the batch
                pstmt.executeBatch();
                pstmt.clearBatch();
            }
            conn.commit();
        } catch (Exception e) {
            log.error("Error updating nodes", e);
            try {
                conn.rollback();
            } catch (SQLException ex) {
                throw new SQLException("Error in roolback", ex);
            }
            throw new SQLException("Exception updating nodes", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }

    public void updateImages(Map<String, Boolean> imagesInfo) throws SQLException {
        if (imagesInfo == null || imagesInfo.isEmpty()) {
            return;
        }
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("updateImages(.. Error getting connection from oracle");
        }
        String sql = "UPDATE SBOOKS.Node SET IMAGESTATUS=? WHERE NODEID=?";
        PreparedStatement pstmt = null;
        try {
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(sql);
            int cont = 0;
            for (Map.Entry<String, Boolean> node : imagesInfo.entrySet()) {
                cont++;
                if (node.getValue()) {
                    pstmt.setString(1, "1");
                } else {
                    pstmt.setString(1, "0");
                }
                pstmt.setInt(2, Integer.parseInt(node.getKey().toString()));
                pstmt.addBatch();
                if (cont == BATCH_UPDATER) {
                    //update the batch
                    pstmt.executeBatch();
                    pstmt.clearBatch();
                    cont = 0;
                    continue;
                }
            }
            //the last data
            if (cont != 0) {
                //update the batch
                pstmt.executeBatch();
                pstmt.clearBatch();
            }
            conn.commit();
        } catch (Exception e) {
            log.error("Error updating nodes", e);
            try {
                conn.rollback();
            } catch (SQLException ex) {
                throw new SQLException("Error in roolback", ex);
            }
            throw new SQLException("Exception updating nodes", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }

    public void updateProcessStatus(String requestId, String status) throws SQLException {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("updateProcessStatus(.. Error getting connection from oracle");
        }
        String sql = "UPDATE SBOOKS.QAREPORT SET STATUS=? WHERE REQUESTID=?";
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, status);
            pstmt.setLong(2, Long.valueOf(requestId));
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Updating qarerport failed");
            }
        } catch (Exception e) {
            log.error("Error updating process status in qareport", e);
            throw new SQLException("Error saving process status in qareport", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
