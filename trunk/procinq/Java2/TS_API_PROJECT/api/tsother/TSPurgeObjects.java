package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Purge all objects from the system that were previously marked for deletion.  This purge is permanent and not
 *  recoverable without a full database restoration from Oracle.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TSPurgeObjects&SKEY=-132981656

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>All objects were purged successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSPurgeObjects
{
	// TSPurgeObjects (sessionid)
	// Warning! This func
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{

		// The following objects are applicable to the PURGE operation:
		//    Corpus
		//    Node
		//
		// All other objects in the database are removed real time
		
		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			// Select each Corpus marked for deletion
			String sSQL = "select CorpusId from Corpus where CorpusActive = -1";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			// Delete each corpus individually
			while ( rs.next() ) {
				int iCorpusId = rs.getInt(1);

				sSQL = "delete from Corpus where CorpusID = "+iCorpusId;
				Statement delstmt = dbc.createStatement();	
				if (delstmt.executeUpdate (sSQL) == 0) {
					out.println ( "<DEBUG>Cannot remove corpus "+iCorpusId+"</DEBUG>"); 
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR);
				}
				delstmt.close();
			}

			rs.close();
		    stmt.close();

			// Select each Node marked for deletion
			sSQL = "select NodeId from Node where NodeStatus = -1";
			stmt = dbc.createStatement();	
			rs = stmt.executeQuery (sSQL);

			// Delete each corpus individually
			while ( rs.next() ) {
				int iNodeId = rs.getInt(1);

				sSQL = "delete from Node where NodeID = "+iNodeId;
				Statement delstmt = dbc.createStatement();	
				if (delstmt.executeUpdate (sSQL) == 0) {
					out.println ( "<DEBUG>Cannot remove node "+iNodeId+"</DEBUG>"); 
					throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR);
				}
				delstmt.close();
			}

			rs.close();
		    stmt.close();

            out.println("<SUCCESS>All objects were purged successfully.</SUCCESS>");
		}

		// Catch exception
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
