package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Edit the custom properties of current user in the LDAP system.  User information is derived from the session key.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  ScoreThreshold (optional)  Score threshold at which documents should be filtered.  Tier three is 100.0, tier two is 75.0, and tier one is 50.0.
 *  @param  ResultsPerPage (optional) Number of results to display per page in the user interface.
 *  @param  UserStatus (optional)  User status within the system.   A status of 1 represents a user will normal privledges.
 *  @param  EmailStatus (optional) A user with an e-mail status of 1 is eligible to receive notification e-mails.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TSEditSubscriberProps&SKEY=-132981656&NodeID=48413&Country=US

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Subscriber properties changed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditSubscriberProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sScoreThreshold = (String) props.get ("ScoreThreshold");
		String sResultsPerPage = (String) props.get ("ResultsPerPage");
		String sUserStatus = (String) props.get ("UserStatus");
		String sEmailStatus = (String) props.get ("EmailStatus");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			if (sScoreThreshold != null) {  u.SetScoreThreshold(new Float(sScoreThreshold).floatValue()); }
			if (sResultsPerPage != null) {  u.SetResultsPerPage(new Integer(sResultsPerPage).intValue()); }
			if (sUserStatus != null) {  u.SetUserStatus(new Integer(sUserStatus).intValue()); }
			if (sEmailStatus != null) {  u.SetEmailStatus(new Integer(sEmailStatus).intValue()); }
			
			u.SaveUser(out);
            out.println("<SUCCESS>Subscriber properties changed successfully.</SUCCESS>");

		} catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
 		} catch ( Exception e )	{ 
			EmitGenXML_ErrorInfo.emitException 
				( "TSException", new TSException(EmitGenXML_ErrorInfo.ERR_TS_INVOCATION_TARGET_EXCEPTION), out ); 
		}
	}
}
