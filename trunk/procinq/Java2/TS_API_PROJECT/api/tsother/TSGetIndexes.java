package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.utils.oracle.*;
import com.iw.system.Index;

import com.iw.system.User;
/**
 * Get all full text indexes that exist within the database.  These indexes may then be used to invoke
 *  the TSRebuildIndexes function.  Custom indexes are indicated with a value of '1' in the CUSTOMFIELD struct.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TSGetIndexes&SKEY=-132981656

 *	@return	A series of INDEX objects corresponding to Oracle Text indicies within the system..
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
     <INDEXES>
         <INDEX>
            <INDEXNAME>DOCUMENTDOCTITLE</INDEXNAME>
            <TABLENAME>DOCUMENT</TABLENAME>
            <FIELDNAME>DOCTITLE</FIELDNAME>
            <CUSTOMFIELD>0</CUSTOMFIELD>
        </INDEX>
 ...
  </TSRESULT>
  \endverbatim
 */
public class TSGetIndexes
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

        Hashtable htIndexes = CustomFields.getIndexes(dbc);
        if (htIndexes.size() > 0) {
            out.println("<INDEXES>");
            Enumeration eH = htIndexes.elements();
            while (eH.hasMoreElements()) {
                Index index = (Index) eH.nextElement();
                String custm = "0"; if (index.getName().toUpperCase().startsWith("CS_")) { custm = "1"; }

                out.println("  <INDEX>");
                out.println("     <INDEXNAME>"+index.getName()+"</INDEXNAME>");
                out.println("     <TABLENAME>"+index.getTable()+"</TABLENAME>");
                out.println("     <FIELDNAME>"+index.getField()+"</FIELDNAME>");
                out.println("     <CUSTOMFIELD>"+custm+"</CUSTOMFIELD>");
                out.println("  </INDEX>");
            }
            out.println("</INDEXES>");
        }

    }
}
