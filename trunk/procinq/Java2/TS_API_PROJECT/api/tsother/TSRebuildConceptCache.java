package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.execution.Session;
import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Rebuild the concept alert cache table.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TSRebuildConceptCache&SKEY=-132981656

 *	@return	no return object is applicable.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
...
  </TSRESULT>
  \endverbatim
 */
public class TSRebuildConceptCache
{
    public static final int iMaxRecordCount = Session.cfg.getPropInt("MaxRecordCount");

	public static void handleTSapiRequest (api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception {

        String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

        // purge the current cache
        String sSQL = "TRUNCATE TABLE CONCEPTALERTCACHE";
        Statement stmt = null; ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);

        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }

        // loop through each concept alert and call getSimilarDocuments
        sSQL = "SELECT DOCUMENTID FROM NARRATIVE";
        HashSet hs = new HashSet();

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            while (rs.next()) { hs.add(rs.getString(1)); }

        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }

        // for each concept alert, re-create the cache
        Iterator i = hs.iterator();
        while (i.hasNext()) {
            String documentID = (String) i.next();
            api.tsdocument.TSGetSimilarDocumentsByDocumentID.user = u;
            int count = api.tsdocument.TSGetSimilarDocumentsByDocumentID.getResults
                            (documentID, null, null, iMaxRecordCount, true, dbc, out);
        }
    }
}
