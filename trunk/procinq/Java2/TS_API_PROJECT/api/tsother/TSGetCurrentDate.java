package api.tsother;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Retrieve the current system date in the format MM/DD/YYYY HH24:MI:SS.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Offset (optional)   Allows retrieval of the date from previous days, where offset is the number of days before today.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsother.TGetCurrentDate&SKEY=-132981656

 *	@return	DATE object
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <DATE>01/15/2004 11:41:01</DATE>
  </TSRESULT>
  \endverbatim
 */
public class TSGetCurrentDate
{
	public static void handleTSapiRequest ( 
										   api.APIProps props, 
										   PrintWriter out, 
										   Connection dbc )
		throws Exception
	{
		try {
			String sKey = (String) props.get("SKEY", true);
			String sOff = (String) props.get("Offset", "0");
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = " select to_char(SYSDATE - "+sOff+", 'MM/DD/YYYY HH24:MI:SS') from DUAL ";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			rs.next();

			String Date = rs.getString(1);
			out.println (" <DATE>"+Date+"</DATE>");

			rs.close();
		    stmt.close();
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
