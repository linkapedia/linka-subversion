package api.tsnugget;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;

/**
 * Retrieve every snippet given a node, document combination.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  NodeID  The unique node identifier of the snippet being retrieved.
 *	@param  DocumentID  The unique document identifier of the snippet being retrieved.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnugget.TSGetSnippets&SKEY=-132981656&NodeID=100049&DocumentID=77911

 *	@return	A series of SNIPPET objects, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <SNIPPETS>
        <SNIPPET>
            <SNIPPETID>4</SNIPPETID>
            <NUGGETID>13</NUGGETID>
			<PHRASE>civilian health-care and public health workers</PHRASE>
		</SNIPPET>
 ...
    </SNIPPETS>
   </TSRESULT>
  \endverbatim
 */
public class TSGetSnippets {

	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);
		String sDocumentID = (String) props.get ("DocumentID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			String sSQL = " select S.SnippetId, S.NuggetId, S.Snippet from "+
						  " Snippet S, NodeDocumentSnippet N where "+
						  " S.SnippetId = N.SnippetId and "+
						  " N.NodeID = " +sNodeID+" and N.DocumentID = "+sDocumentID;
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
 
			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				if (loop == 1) { out.println("<SNIPPETS>"); }
				
				String sSnippetID = rs.getString(1);
				String sNuggetID = rs.getString(2);
				String sSnippet = rs.getString(3);	
		
				out.println ("   <SNIPPET> ");
				out.println ("   <SNIPPETID>"+sSnippetID+"</SNIPPETID>");
				out.println ("   <NUGGETID>"+sNuggetID+"</NUGGETID>");
				out.print   ("   <PHRASE>");
					for (int k = 0; k < sSnippet.length(); k++) {
						int ichar = (int) sSnippet.charAt(k);
						if ((ichar != 13) && (ichar != 10)) { out.print(sSnippet.charAt(k)); }
					}
				out.println ("   </PHRASE>");
				out.println ("   </SNIPPET>");
			}

			rs.close();
		    stmt.close();
		
			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No snippets found for Doc ID: "+sDocumentID+" Node ID: "+sNodeID+" </DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
			}
			out.println("</SNIPPETS>");
		}
		
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
