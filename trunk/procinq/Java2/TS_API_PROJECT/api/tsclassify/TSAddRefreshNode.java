package api.tsclassify;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Adds a node id to the refresh queue.  This queues a node to be re-evaluated for classification.
 * All documents in the system at the current time will be sampled for classification into this node.
 * This is a different model for classification then the TSClassify API which takes a document and compares
 * it to all of the available topics.  This takes a topic / node and compares it to all documents. The
 * compare is run in batch at a scheduled time for the ITS server.  This API only schedules the refresh.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  NodeID  Unique node identifier of the topic to be refreshed.
 *
 *  @note   http://ITSERVER/servlet/ts?fn=tsclassify.TSAddRefreshNode&SKEY=801312271&NodeID=280401

 *	@return SUCCESS object if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>1</SUCCESS>
    </TSRESULT>
 \endverbatim
 */
public class TSAddRefreshNode {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sKey = (String) props.get("SKEY", true);
        String sNodeID = (String) props.get("NODEID", true);

        try {
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            String sSQL = "insert into NodeRefresh values (" + sNodeID + ")";
            Statement stmt = null;

            try {
                stmt = dbc.createStatement();
                stmt.executeUpdate(sSQL);
            } catch (SQLException qe) {
                EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
                return;
            } finally {
                stmt.close();
                stmt = null;
            }

            out.println("<SUCCESS>1</SUCCESS>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}