/*
 * 
 * API function for send the corpusid and resources into of the queeqe
 */
package api.tsclassify;

import api.util.SardineUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.googlecode.sardine.DavResource;
import com.googlecode.sardine.SardineFactory;
import com.iw.activemq.beans.ClassificationPackage;
import com.iw.activemq.procinq.QueueProducer;
import com.yuxipacific.documents.beans.DocumentDescriptor;
import org.apache.log4j.Logger;

public class TSGetResource {

    private static final Logger log = Logger.getLogger(TSGetResource.class);
    public static ArrayList<String> uriToSend;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("handleTSapiRequest()");
        List<String> resources = new ArrayList<String>();
        List<String> taxonomies = new ArrayList<String>();
        DocumentDescriptor docDescriptor = null;
        uriToSend = new ArrayList<String>();
        Set<Object> keySet = props.keySet();
        for (Iterator<Object> iterator = keySet.iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            String resource = (String) props.get(key, true);
            if (key.toLowerCase().startsWith("tax")) {
                taxonomies.add(resource);
            } else if (key.toLowerCase().startsWith("uri")) {
                resources.add(resource);
            }
        }
        for (String r : resources) {
            if (r.endsWith("/")) {
                String uriReal = r.substring(0, r.indexOf("/", 7));
                String path = r.substring(r.indexOf("/", 7), r.length());
                SardineUtil.objectSardine = SardineFactory.begin();
                SardineUtil.urlBasePath = uriReal;
                recorrerArbol(path);
            } else {
                uriToSend.add(r);
            }
        }
        log.debug("Resources : " + uriToSend.toString());
        log.info("Sending information to the QUEUE <Tax Count>:" + taxonomies.size() + "<URI Count>: " + uriToSend.size());
        ClassificationPackage pack = null;
        //TODO change the behavior to work with the new DocumentDescriptor object.
        for (String resource : uriToSend) {
            pack = new ClassificationPackage();
            pack.setCorpusIDList(taxonomies);
            //pack.setResourceURI(resource);
            QueueProducer.sendMessage(pack);
        }
    }

    public static void recorrerArbol(String path) throws IOException {
        if (path.contains(" ")) {
            path = path.replace(" ", "%20");
        }
        List<DavResource> lista = SardineUtil.objectSardine.list(SardineUtil.urlBasePath + path);
        if (lista != null || !lista.isEmpty()) {
            if (lista.get(0).isDirectory()) {
                for (int i = 1; i < lista.size(); i++) {
                    if (lista.get(i).isDirectory()) {
                        recorrerArbol(lista.get(i).toString());
                    } else {
                        uriToSend.add(SardineUtil.urlBasePath + lista.get(i).getPath());
                    }
                }
            } else {
                uriToSend.add(SardineUtil.urlBasePath + lista.get(0).getPath());
            }
        }
    }
}