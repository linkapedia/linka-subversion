package api.tsclassify;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Approve documents from the review batch results queue.  See: api::tsclassify::TSReviewBatchResults
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  DocumentIDList  List of document identifiers that have been approved by the editor
 *
 *  @note   http://ITSERVER/servlet/ts?fn=tsclassify.TSApproveReviewedResult&SKEY=801312271&DocumentIDList=2,5,9,14,30

 *	@return A SUCCESS object if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <SUCCESS>Document results approved successfully.</SUCCESS>
  </TSRESULT>
 \endverbatim
 */
public class TSApproveReviewedResult
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sDocumentIDList = (String) props.get ("DocumentIDList", true);  // comma separated list

        Statement stmt = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = "update Document set UpdatedDate = SYSDATE where DocumentID in ("+sDocumentIDList+")";
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Attempt to approve reviewed results failed.</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Document results approved successfully.</SUCCESS>");
		    stmt.close();
		} catch ( TSException tse )	 {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}
}
