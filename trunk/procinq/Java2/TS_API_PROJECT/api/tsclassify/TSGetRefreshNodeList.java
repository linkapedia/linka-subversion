package api.tsclassify;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.system.User;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Returns all of the nodes along with signatures that are scheduled for refresh in the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/servlet/ts?fn=tsclassify.TSGetRefreshNodeList&SKEY=801312271

 *	@return A series of custom ITS Node objects containing signature words and frequencies.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
    <NODES>
        <NODE>
            <NODEID>1311251</NODEID>
            <NODETITLE>Top Node</NODETITLE>
            <SIGNATUREWORD>tony</SIGNATUREWORD>
            <SIGNATUREFREQ>5</SIGNATUREFREQ>
        </NODE>
...
 \endverbatim
 */
public class TSGetRefreshNodeList {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sKey = (String) props.get("SKEY", true);

        try {
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
            if (u == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            String sSQL = "select N.NodeID, N.NodeTitle, S.SignatureWord, S.SignatureOccurences " +
                    "from Node N, Signature S where " +
                    "N.NodeID in (select nodeid from noderefresh) " +
                    "and N.NodeID = S.NodeID order by N.NodeID, S.SignatureOccurences desc";
            ResultSet rs = null;
            Statement stmt = null;
            int loop = 0;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                String sNodeID = "";
                int iaccum = 0;

                while (rs.next()) {
                    loop++;
                    if (loop == 1) {
                        out.println("<NODES>");
                    }
                    String sNID = rs.getString(1);
                    if (!sNID.equals(sNodeID)) {
                        iaccum = 0;
                        sNodeID = sNID;
                    }
                    iaccum++;

                    if (iaccum < 5) {
                        out.println("   <NODE> ");
                        out.println("   <NODEID>" + sNID + "</NODEID>");
                        out.println("   <NODETITLE>" + rs.getString(2) + "</NODETITLE>");
                        out.println("   <SIGNATUREWORD>" + rs.getString(3) + "</SIGNATUREWORD>");
                        out.println("   <SIGNATUREFREQ>" + rs.getString(4) + "</SIGNATUREFREQ>");
                        out.println("   </NODE>");
                    }
                }

            } catch (SQLException qe) {
                EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
                return;
            } finally {
                rs.close();
                stmt.close();
                rs = null;
                stmt = null;
            }

            if (loop == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);
            }
            out.println("</NODES>");

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}