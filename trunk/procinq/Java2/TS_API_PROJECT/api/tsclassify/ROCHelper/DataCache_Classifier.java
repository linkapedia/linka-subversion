/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 31, 2003
 * Time: 10:42:02 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsclassify.ROCHelper;

import api.emitxml.EmitGenXML_ErrorInfo;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Hashtable;

public class DataCache_Classifier {

    private static boolean bCacheStale = true;
    ;
    private static final Object OCacheSynchronizer = new Object();
    private static HashSet hsAllIntegerClassifierIDsExcludeStarCounts = null;
    private static Hashtable htIntegerClassifierIDToSortOrder = null;
    private static Hashtable htIntegerClassifierIDToStrColName = null;
    private static HashSet hsAllStrClassifierNames = null;
    private static Hashtable htStringClassifierNameToStrColName = null;
    private static HashSet hsAllClassifierPaths = null;
    private static Hashtable htStrClassifierPathTohsOfStrClassifierIDs = null;
    private static Hashtable htStrColNameToStringClassifierNames = null;
    private static HashSet hsAllColNames = null;
    private static Hashtable htIClassifierIDFromToStrClassifierName = null;

    public static void invalidateCache() {
        bCacheStale = true;
    }

    public static void fillCache(Connection dbc) throws Exception {
        if (bCacheStale == true) {
            synchronized (OCacheSynchronizer) {
                if (bCacheStale == true) {

                    hsAllIntegerClassifierIDsExcludeStarCounts = new HashSet();
                    htIntegerClassifierIDToSortOrder = new Hashtable();
                    htIntegerClassifierIDToStrColName = new Hashtable();
                    hsAllStrClassifierNames = new HashSet();
                    htStringClassifierNameToStrColName = new Hashtable();
                    hsAllClassifierPaths = new HashSet();
                    htStrClassifierPathTohsOfStrClassifierIDs = new Hashtable();
                    htStrColNameToStringClassifierNames = new Hashtable();
                    hsAllColNames = new HashSet();
                    htIClassifierIDFromToStrClassifierName = new Hashtable();

                    String sSQL = "SELECT CLASSIFIERID, COLNAME, CLASSIFIERNAME, SORTORDER, CLASSPATH FROM CLASSIFIER WHERE ACTIVE = 1 order by CLASSIFIERID";

                    ResultSet rs = null;
                    Statement stmt = null;

                    try {
                        stmt = dbc.createStatement();
                        rs = stmt.executeQuery(sSQL);
                    } catch (SQLException qe) {
                        EmitGenXML_ErrorInfo.getsXML_JavaException("Database exception", qe);
                    }

                    // Need to get total number of results for calculating percentages
                    int iClassifierID = 0;
                    String sColName = null;
                    String sClassifierName = null;
                    String sClassPath = null;
                    int iSortorder = 0;
                    while (rs.next()) {
                        iClassifierID = rs.getInt("CLASSIFIERID");
                        sColName = rs.getString("COLNAME");
                        sClassifierName = rs.getString("CLASSIFIERNAME");
                        iSortorder = rs.getInt("SORTORDER");
                        sClassPath = rs.getString("CLASSPATH");

                        if (sClassifierName.indexOf("StarCount") != 0) {
                            hsAllIntegerClassifierIDsExcludeStarCounts.add(new Integer(iClassifierID));
                        }

                        htIntegerClassifierIDToSortOrder.put(new Integer(iClassifierID), new Integer(iSortorder));
                        htIntegerClassifierIDToStrColName.put(new Integer(iClassifierID), sColName);
                        hsAllStrClassifierNames.add(sClassifierName);
                        htStringClassifierNameToStrColName.put(sClassifierName, sColName);
                        hsAllClassifierPaths.add(sClassPath);

                        HashSet hsOfStrClassifierIDs_ThisClassifierPath = (HashSet) htStrClassifierPathTohsOfStrClassifierIDs.get(sClassPath);
                        if (hsOfStrClassifierIDs_ThisClassifierPath == null) {
                            hsOfStrClassifierIDs_ThisClassifierPath = new HashSet();
                            htStrClassifierPathTohsOfStrClassifierIDs.put(sClassPath, hsOfStrClassifierIDs_ThisClassifierPath);
                        }
                        // not really a pre any more - have instantiated anew if it was null
                        hsOfStrClassifierIDs_ThisClassifierPath.add(sClassPath);
                        htStrColNameToStringClassifierNames.put(sColName, sClassifierName);
                        hsAllColNames.add(sColName);
                        htIClassifierIDFromToStrClassifierName.put(new Integer(iClassifierID), sClassifierName);
                    }
                    rs.close();
                    stmt.close();
                    api.Log.Log("completed CLASSIFIER cache refresh");
                    bCacheStale = false;
                } // race check
            }  // synch
        } // cache stale chack
    }

    public synchronized static HashSet gethsAllIntegerClassifierIDsExcludeStarCounts(Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return (HashSet) hsAllIntegerClassifierIDsExcludeStarCounts;
    }

    public synchronized static Integer getSortOrderForClassifierID(int iClassifierID, Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return (Integer) htIntegerClassifierIDToSortOrder.get(new Integer(iClassifierID));
    }

    public synchronized static String getColNameForClassifierID(int iClassifierID, Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return (String) htIntegerClassifierIDToStrColName.get(new Integer(iClassifierID));
    }

    public synchronized static HashSet gethsAllStrClassifierNames(Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return (HashSet) hsAllStrClassifierNames;
    }

    public synchronized static String getStrColNameFromStrClassifierName(String sClassifierName, Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return (String) htStringClassifierNameToStrColName.get(sClassifierName);
    }

    public synchronized static String getStrClassifierNameFromStrColName(String sColName, Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return (String) htStrColNameToStringClassifierNames.get(sColName);
    }

    public synchronized static String getStrClassifierNameFromIntegerIClassifierID(Integer IClassifierID, Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return (String) htIClassifierIDFromToStrClassifierName.get(IClassifierID);
    }

    public synchronized static HashSet gethsAllStrClassifierPaths(Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return hsAllClassifierPaths;
    }

    public synchronized static HashSet gethsAllColNames(Connection dbc) throws Exception {
        if (bCacheStale == true) {
            fillCache(dbc);
        }
        return hsAllColNames;
    }
}
