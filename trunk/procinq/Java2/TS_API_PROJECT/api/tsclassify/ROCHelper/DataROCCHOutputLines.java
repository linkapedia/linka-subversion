/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 31, 2003
 * Time: 10:40:01 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsclassify.ROCHelper;

import com.indraweb.util.UtilStrings;
import java.util.Vector;
import java.util.Enumeration;

public class DataROCCHOutputLines {
    public String sLine = null;
    public String sSlopeRangeMin = null;
    public String sSlopeRangeMax = null;
    public String sTPRate = null;
    public String sFPRate = null;
    public int iClassifierID = -1;
    public float fScoreCutoff       = -1;
    public String sColName = null;

    public DataROCCHOutputLines (String sLine_
        )
    {

        sLine = sLine_;
        // api.Log.Log ("processing line [" + sLine_ + "]" );
        // [ 0.000, 0.800 ]   1.000 1.000    Cid[3] Sc{3.0} Col<SCORE5>
        // step a get fSlopeRangeMin  and fSlopeRangeMax
        String sBetweenSquareBrackets = UtilStrings.getStringBetweenThisAndThat(sLine, "[", "]" ).trim();
        Vector vsSlopeRangeMinMax = UtilStrings.splitByStrLen1(sBetweenSquareBrackets, ",");
        sSlopeRangeMin = ((String) vsSlopeRangeMinMax.elementAt(0)).trim();
        sSlopeRangeMax = ((String) vsSlopeRangeMinMax.elementAt(1)).trim();

        // step b get fSlopeRangeMin  and fSlopeRangeMax
        String  sAfterFirstRightSqBracket = UtilStrings.getStringBetweenThisAndThat(sLine, "]", "Cid" ).trim();
        Vector vsTPFpRates = UtilStrings.splitByStrLen1(sAfterFirstRightSqBracket, " ");
        sTPRate = ((String) vsTPFpRates.elementAt(0)).trim();
        sFPRate = ((String) vsTPFpRates.elementAt(1)).trim();

        // step c get classifierID, fScoreCutoff, sColName
        String sClassifierID = UtilStrings.getStringBetweenThisAndThat(sLine, "Cid(", ")" );
        iClassifierID = Integer.parseInt(sClassifierID);

        String sScoreCutoff = UtilStrings.getStringBetweenThisAndThat(sLine, "Sc{", "}" );
        fScoreCutoff = Float.parseFloat(sScoreCutoff);

        sColName = UtilStrings.getStringBetweenThisAndThat(sLine, "Col<", ">" );


    }

    public String getDesc()
    {
        return
            sLine + " yields :\r\n"
            +"  sSlopeRangeMin["+sSlopeRangeMin + "]\r\n"
            +"  sSlopeRangeMax["+sSlopeRangeMax + "]\r\n"
            +"  sTPRate["+       sTPRate + "]\r\n"
            +"  sFPRate["+       sFPRate + "]\r\n"
            +"  iClassifierID["+ iClassifierID + "]\r\n"
            +"  fScoreCutoff["+  fScoreCutoff + "]\r\n"
            +"  sColName["+      sColName + "]\r\n" ;
    }

}


