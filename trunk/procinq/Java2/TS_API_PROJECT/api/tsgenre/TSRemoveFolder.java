package api.tsgenre;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Remove a folder from the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  FolderID    Unique identifier corresponding to this folder.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsgenre.TRemoveFolder&FolderID=22&SKEY=993135977

 *	@return Success object if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <SUCCESS>Folder removed successfully.</SUCCESS>
        <CLASSLOAD>Sun Apr 07 18:17:24 EDT 2002</CLASSLOAD>
        <CALLCOUNT>743</CALLCOUNT>
        <TIMEOFCALL_MS>381</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSRemoveFolder
{
	// TSRemoveFolder (sessionid, folderid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sFolderID = (String) props.get ("FolderID");
	
		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			if (sFolderID == null) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
			}
			
			String sSQL = "delete from Genre where GenreID = "+sFolderID;
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from Genre table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_GENRE_DELETE_FAILURE);
			}

			out.println("<SUCCESS>Folder removed successfully.</SUCCESS>");
		    stmt.close();
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
