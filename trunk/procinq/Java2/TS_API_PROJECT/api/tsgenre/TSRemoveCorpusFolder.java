package api.tsgenre;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Removes a corpus folder relationship.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus identifier
 *  @param  FolderID    Unique folder/genre identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsgenre.TSRemoveCorpusFolder&CorpusID=3&FolderID=22&SKEY=993135977

 *	@return An error is returned on failure.
 *  \verbatim
    <TSRESULT>
        <CLASSLOAD>Mon Oct 29 14:07:04 EST 2001</CLASSLOAD>
        <CALLCOUNT>28</CALLCOUNT>
        <TIMEOFCALL_MS>20</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSRemoveCorpusFolder
{
	// TSRemoveCorpusFolder (sessionid, corpusid, folderid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sFolderID = (String) props.get ("FolderID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			if (sCorpusID == null) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
			}
			
			String sSQL = "delete from CorpusGenre where CorpusID = "+sCorpusID;
			if (sFolderID != null) { sSQL = sSQL + " and GenreID = "+sFolderID; }
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Delete from CorpusGenre table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_GENRE_DELETE_FAILURE);
			}

		    stmt.close();
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
