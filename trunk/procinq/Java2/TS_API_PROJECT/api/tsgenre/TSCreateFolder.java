package api.tsgenre;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.indraweb.execution.Session;

/**
 * Creates a folder in the system.  Folder name must be unique.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  FolderName  Name of the newly created folder.
 *  @param  FolderStatus    Status of the folder, 1 is active, 0 is inactive, -1 is marked for deletion.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsgenre.TSCreateFolder&FolderName=Mike&FolderStatus=1&SKEY=993135977

 *	@return An ITS GENRE object, if successful.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <GENRE>
            <GENREID>21</GENREID>
            <GENRENAME>Mike</GENRENAME>
            <GENRESTATUS>1</GENRESTATUS>
        </GENRE>
    <CLASSLOAD>Mon Oct 29 14:07:04 EST 2001</CLASSLOAD>
    <CALLCOUNT>5</CALLCOUNT>
    <TIMEOFCALL_MS>1722</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */
public class TSCreateFolder
{
	// TSCreateFolder (sessionid, genrename, genrestatus)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sGenreName = (String) props.get ("FolderName");
		String sGenreStatus = (String) props.get ("FolderStatus");

		try {
			
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			if ((sGenreName == null) || (sGenreStatus == null)) {
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
			}

			String sSQL = " insert into Genre (GenreName, GenreStatus) values "+
						  " ('"+sGenreName+"',"+sGenreStatus+")";
			Statement istmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (istmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Attempt to add new genre failed</DEBUG>"); 
				api.Log.LogError("SQL failed: "+sSQL);
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CREATE_GENRE_FAILURE);
			}
		    istmt.close();

			sSQL = " select GenreID from Genre where GenreName = '"+sGenreName+"'";
			Statement stmt = dbc.createStatement();
			
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				
				int iGenreID = rs.getInt(1);
				
				out.println ("   <GENRE> ");
				out.println ("      <GENREID>"+iGenreID+"</GENREID>");
				out.println ("      <GENRENAME>"+sGenreName+"</GENRENAME>");
				out.println ("      <GENRESTATUS>"+sGenreStatus+"</GENRESTATUS>");
				out.println ("   </GENRE>");
			}
			rs.close(); 
		    stmt.close();

            if (loop == 0) { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CREATE_GENRE_FAILURE); }

			out.println("<SUCCESS>Folder created successfully.</SUCCESS>");
			
		}
		
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
