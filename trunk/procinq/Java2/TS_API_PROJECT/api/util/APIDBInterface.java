/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Jan 28, 2003
 * Time: 4:12:11 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.util;

import com.indraweb.execution.Session;
import com.indraweb.util.UtilFile;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class APIDBInterface {

    private static final Logger log = Logger.getLogger(APIDBInterface.class);
    private static boolean bSessionCfgsAdded = false;
    private static final Integer ISemaphore = new Integer(0);
    private static String sWhichDB_ = null;
    private static HashSet<String> hsWhichDBsUsed = new HashSet<String>();

    public static String getsWhichDB() {
        return sWhichDB_;
    }
    // Initialize the database and populate the config parameters

    public static Connection freeConnection(Connection dbc) throws Exception {
        //com.indraweb.util.Log.logClearcr("apiDBinterface freeConnection  connection [" + getsWhichDB()+ "]" );
        dbc = api.statics.DBConnectionJX.freeConnection(dbc, getsWhichDB());
        return null;
    }

    public static Iterator<String> getDBsUsed() {
        return hsWhichDBsUsed.iterator();
    }

    /**
     *
     * @param sWhichDB
     * @return
     * @throws Exception
     */
    public static Connection getConnection(String sWhichDB) throws Exception {
        hsWhichDBsUsed.add(sWhichDB);
        if (!bSessionCfgsAdded) {
            synchronized (ISemaphore) {
                if (!bSessionCfgsAdded) // in case two threads had gotten here same time
                {
                    api.APIProps apiprops_override = null;
                    if (new File("/temp/PropertyOverride.txt").exists()) {
                        log.debug("found /temp/PropertyOverride.txt");
                        apiprops_override = new api.APIProps("/temp/PropertyOverride.txt");
                        Enumeration<Object> e = apiprops_override.keys();
                        while (e.hasMoreElements()) {
                            String sKey = (String) e.nextElement();
                            String sValue = (String) apiprops_override.get(sKey);
                            log.debug("override setting [" + sKey + "] to [" + sValue + "]");
                        }
                    } else {
                        log.debug("no /temp/PropertyOverride.txt OK");
                    }

                    try {
                        // give parms to session from DB one time only
                        log.debug("APIDBInterface getConnection using db [" + Session.cfg.getProp("DBString_" + sWhichDB + "_OracleJDBC") + "] to get parms");
                        Connection dbc = api.statics.DBConnectionJX.getConnection(sWhichDB);
                        String sSQL = " select ParamName, ParamValue from ConfigParams order by ParamName";
                        if (UtilFile.bFileExists("/temp/IndraDebug_UseConfigparams_test.txt")) {
                            log.debug("file present, using db table ConfigParams_test [/temp/IndraDebug_UseConfigparams_test.txt]");
                            sSQL = " select ParamName, ParamValue from ConfigParams_test order by ParamName";
                        }

                        Statement stmt = dbc.createStatement();

                        ResultSet rs = stmt.executeQuery(sSQL);

                        log.debug("db params get query done");
                        Hashtable<String, String> htargs = new Hashtable<String, String>();

                        int i = 0;
                        while (rs.next()) {
                            i++;
                            String sK = rs.getString(1);
                            String sV = rs.getString(2);
                            log.debug(i + ". db param [" + sK + "] = [" + sV + "]");

                            String sOverrideValue = null;
                            if (apiprops_override == null
                                    || ((sOverrideValue = (String) apiprops_override.get(sK)) == null)) {
                                if (sV == null) {
                                    sV = "";
                                }
                                htargs.put(sK, sV);
                            } else {
                                log.debug(
                                        "overriding setting for sK [" + sK + "] "
                                        + " from [" + sV + "]"
                                        + " to [" + sOverrideValue.toLowerCase() + "]\r\n");
                                htargs.put(sK, sOverrideValue.toLowerCase());
                            }
                        }
                        rs.close();
                        stmt.close();
                        api.statics.DBConnectionJX.freeConnection(dbc, sWhichDB);

                        Session.cfg.addToProperties(htargs);
                        bSessionCfgsAdded = true;

                    } catch (Exception e) {
                        log.error("error initing session cfgs from DB", e);
                    }
                }
            }
        }

        if (sWhichDB_ != null && !sWhichDB_.equals(sWhichDB)) {
            log.debug("DB switch : sWhichDB_ != null &&  !sWhichDB_.equals(sWhichDB) " + " prior [" + sWhichDB_ + "]  new [" + sWhichDB + "]");
        }
        sWhichDB_ = sWhichDB;
        try {
            Connection dbc = null;
            try {
                dbc = api.statics.DBConnectionJX.getConnection(sWhichDB_);
            } catch (Throwable t) {
                log.error("e from Connection dbc = api.statics.DBConnectionJX.getConnection(sWhichDB);", t);
            }
            return dbc;
        } catch (Exception e) {
            log.error("error getting DB connection internal stack trace", e);
            return null;
        }

    }
}
