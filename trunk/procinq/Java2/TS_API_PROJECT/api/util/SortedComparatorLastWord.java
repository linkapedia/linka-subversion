package api.util;

import java.util.Comparator;

/**
 *
 * @author andres
 */
public class SortedComparatorLastWord implements Comparator<Words> {

    @Override
    public int compare(Words t, Words t1) {
        if (null == t.getWords() || t.getWords().isEmpty()) {
            return 0;
        }
        if (null == t1.getWords() || t1.getWords().isEmpty()) {
            return 0;
        }
        int size = t.getWords().size();
        int size1 = t1.getWords().size();
        return t.getWords().get(size - 1).compareTo(t1.getWords().get(size1 - 1));
    }
}
