package api.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;

public class MultiPartMisc
{
	/**
	 * return specified the number of files uploaded, if a file was uploaded
	 */
	
	public static Vector addPropsUploadfileFromMultiPart 
											( 
												HttpServletRequest request, 
												HttpServletResponse response, 
												MultipartParser mpr,
												api.APIProps apiprops,
												String sPathForFileUploads,
												boolean bKillExistingFiles 
											)
		throws Exception
	{
		Vector vStrings_FilePathsUploaded = new Vector();
		// loop through each part that was sent
		PrintWriter out	= response.getWriter();
		Part p = null;
		while ((p = mpr.readNextPart()) != null) 
		{
			String sParmName = p.getName();
			String sParmValue = new String("");
			if (p.isParam()) { 
				ParamPart pp = (ParamPart) p;
				sParmValue = pp.getStringValue();
				//com.indraweb.util.UtilFile.addLineToFile("c:/t.t", 
				//										 "<got parm>"+ sParmName +":"+ sParmValue + "DEBUG_MPART_FILEUPLOAD>\r\n");
			apiprops.put (sParmName, sParmValue);

			}
			else if (p.isFile()) { 
				FilePart fp = (FilePart) p;
			
				// Write content type and file name into props
				apiprops.put ("Content-type", fp.getContentType());
				sParmValue = fp.getFileName();
					
				// Write file out to temp directory here
						
				if ( !sPathForFileUploads.endsWith ("/"))
					sPathForFileUploads = sPathForFileUploads + "/";
				String sFilename = sPathForFileUploads + fp.getFileName();
				//out.println("<DEBUG_MPART_FILEUPLOAD>"+ sFilename + "</DEBUG_MPART_FILEUPLOAD>");
				//com.indraweb.util.UtilFile.addLineToFile("c:/t.t", 
				//										"<DEBUG_MPART_FILEUPLOAD>"+ sFilename + "</DEBUG_MPART_FILEUPLOAD>");
				vStrings_FilePathsUploaded.addElement ( sFilename );
				long lStartFileUpload = System.currentTimeMillis();
				File fFile = new File(sFilename);
				
				//response.getWriter().println("<STARTFILEUPLOAD>"+sFilename+"</STARTFILEUPLOAD>");
				if (fFile.exists())
				{
					if (bKillExistingFiles)
					{
						fFile.delete();						
					}
					else
					{
						throw new Exception("Sorry, a temp classify file already exists with this name[" + sFilename + "]");
					}
				}
						
				InputStream is = fp.getInputStream();
				FileOutputStream fo = new FileOutputStream(fFile);
				int c;
						
				while ((c = is.read()) != -1) {
					fo.write(c);	
				}
						
				// close streams
				is.close(); 
				fo.close();
				//response.getWriter().println("<DONEFILEUPLOADMS>"+ 
				//							 com.indraweb.util.UtilProfiling.elapsedTimeMillis ( lStartFileUpload )+
				//							 ":"+sFilename+"</DONEFILEUPLOADMS>");
			}
			//out.println("<DEBUG_MPART_PARM>"+ sParmName+ ":" + sParmValue + "</DEBUG_MPART_PARM>");
		}
		return vStrings_FilePathsUploaded;
	} // public static void uploadFileFromRequest ( HttpServletRequest request, 

	
	public static boolean isContentTypeMultiPart (
													HttpServletRequest req 
												)
	{
		// Check the content type to make sure it's "multipart/form-data"
		// Access header two ways to work around WebSphere oddities
		String type = null;
		String type1 = req.getHeader("Content-Type");
		String type2 = req.getContentType();
		// If one value is null, choose the other value
		if (type1 == null && type2 != null) {
		  type = type2;
		}
		else if (type2 == null && type1 != null) {
		  type = type1;
		}
		// If neither value is null, choose the longer value
		else if (type1 != null && type2 != null) {
		  type = (type1.length() > type2.length() ? type1 : type2);
		}

		if (type == null || !type.toLowerCase().startsWith("multipart/form-data")) 
			return false;
		else
			return true;
	} 
}
