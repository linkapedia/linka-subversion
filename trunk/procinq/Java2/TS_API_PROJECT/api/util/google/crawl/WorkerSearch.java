/**
 *
 * @author Andres Felipe Restrepo
 * @version 1.0 @date 20/sep/2011
 */
package api.util.google.crawl;

import api.util.bean.SearchBean;
import api.util.interfaces.ISearchNotify;
import api.util.interfaces.IWorker;
import com.yuxipacific.documents.beans.DocumentDescriptor;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

public class WorkerSearch implements Runnable, IWorker {

    private static final Logger LOG = Logger.getLogger(WorkerSearch.class);
    private Thread th;
    private List<SearchBean> terms;
    private ISearchNotify notify;
    private APIGoogle api = null;
    private final int READ_TIMEOUT = 30000;
    private final int CONNECTION_TIMEOUT = 30000;
    private final int PAGINATION_GOOGLE = 10;
    private Map<String, String> params = null;
    private List<List<String>> arrayCSV = null;
    private int numTermsSuccess = 0;
    private int numTermsErrors = 0;
    private volatile long initWork;

    public WorkerSearch(ISearchNotify notify, Map<String, String> params) {
        this.notify = notify;
        terms = new ArrayList<SearchBean>();
        api = new APIGoogle();
        this.params = params;
        arrayCSV = new ArrayList<List<String>>();
    }

    @Override
    public void run() {
        List<DocumentDescriptor> links = null;
        List<String> aCSV = null;
        String nodeid = null;
        String term = null;
        int numLinks = Integer.parseInt(params.get("numlinks"));
        List<URL> urls = null;
        for (SearchBean sb : terms) {
            try {
                links = new ArrayList<DocumentDescriptor>();
                urls = new ArrayList<URL>();
                //logic to search in google
                initWork = System.currentTimeMillis();
                nodeid = sb.getNodeid();
                term = sb.getTerm();
                int mod = numLinks % PAGINATION_GOOGLE;
                int div = numLinks / PAGINATION_GOOGLE;
                api.setSearchParameter(term);
                api.setCustomSearchID(params.get("customsearch"));
                int inc = 1;
                int cont = 0;
                while (cont < div) {
                    api.setNum(String.valueOf(PAGINATION_GOOGLE));
                    api.setStart(String.valueOf(inc));
                    urls.add(new URL(api.toString()));
                    inc += 10;
                    cont++;
                }
                if (mod != 0) {
                    api.setNum(String.valueOf(mod));
                    api.setStart(String.valueOf(inc));
                    urls.add(new URL(api.toString()));
                }
                //get results
                List<DocumentDescriptor> resultsFromGoogle = new ArrayList<DocumentDescriptor>();
                for (URL url : urls) {
                    resultsFromGoogle = getLinks(url);
                    if (!resultsFromGoogle.isEmpty()) {
                        links.addAll(resultsFromGoogle);
                    } else {
                        break;
                    }
                }
                initWork = 0;
                //build csv to each node to show in the ProcinQ Client           
                aCSV = new ArrayList<String>();
                aCSV.add("\"" + nodeid + "\"");
                aCSV.add("\"" + term + "\"");
                //show first url to search in google
                aCSV.add("\"" + urls.get(0) + "\"");
                if (links.isEmpty()) {
                    numTermsErrors++;
                    aCSV.add("\"Not found result\"");
                    arrayCSV.add(aCSV);
                } else {
                    aCSV.add("\"" + links.size() + "\"");
                    arrayCSV.add(aCSV);
                    //send data into the queue
                    Map<String, String> mapParams = new HashMap<String, String>();
                    mapParams.put("corpusid", params.get("corpusid"));
                    notify.onSearchResult(links, mapParams);
                    numTermsSuccess++;
                }
            } catch (Exception e) {
                LOG.error("Error term: " + sb.getTerm());
                numTermsErrors++;
            } finally {
                //progress
                notify.pr();
            }
        }
    }

    /**
     * return links from google api
     *
     * @param searhTerm
     * @return
     */
    private List<DocumentDescriptor> getLinks(URL url) throws Exception {
        LOG.debug("WorkerSearch getLinks(URL)");
        List<DocumentDescriptor> result = new ArrayList<DocumentDescriptor>();
        DocumentDescriptor _tmpDocument = null;
        String tmpLink = null;
        String tmpTitle = null;
        HttpURLConnection httpConnection = null;
        BufferedReader read = null;
        try {
            LOG.debug("Google Query URL: " + url);
            httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7");
            httpConnection.setConnectTimeout(CONNECTION_TIMEOUT);
            httpConnection.setReadTimeout(READ_TIMEOUT);
            httpConnection.connect();
            //If the result is returned from google successfully.
            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String line;
                StringBuilder responseFromGoogle = new StringBuilder();
                read = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                //Read the results from google
                while ((line = read.readLine()) != null) {
                    responseFromGoogle.append(line);
                }
                JSONObject jsonResponse = JSONObject.fromObject(responseFromGoogle.toString());
                JSONArray items = ((JSONArray) jsonResponse.get("items"));
                if (items != null) {
                    for (Object ob : items) {
                        JSONObject o = (JSONObject.fromObject(ob));
                        tmpLink = o.get("link").toString();
                        tmpTitle = o.get("title").toString();
                        if (tmpLink != null) {
                            _tmpDocument = new DocumentDescriptor();
                            _tmpDocument.setSrcURL(tmpLink);
                            _tmpDocument.setTitle(tmpTitle);
                            result.add(_tmpDocument);
                        }
                    }
                } else {
                    LOG.debug("Result not found - please check your internet connection or your quotas in the Google console");
                }
            }
            return result;
        } finally {
            if (httpConnection != null) {
                httpConnection.disconnect();
            }
            if (read != null) {
                read.close();
            }
        }
    }

    public List<List<String>> getArrayCSV() {
        return arrayCSV;
    }

    public int getNumTermsError() {
        return numTermsErrors;
    }

    public int getNumTermsSuccess() {
        return numTermsSuccess;
    }

    @Override
    public long workTime() {
        return initWork;
    }

    @Override
    public Thread getThread() {
        return th;
    }

    public void setThread(Thread th) {
        this.th = th;
    }

    public void setTerm(SearchBean sb) {
        this.terms.add(sb);
    }
}
