package api.util.google.crawl;

import api.util.interfaces.ISearchNotify;
import com.iw.activemq.beans.ClassificationPackage;
import com.iw.activemq.procinq.QueueProducer;
import com.yuxipacific.documents.beans.DocumentDescriptor;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class NotifySearch implements ISearchNotify<List<DocumentDescriptor>> {

    private static final Logger log = Logger.getLogger(NotifySearch.class);
    private PrintWriter pw;
    private int num;
    private long percent = 0;
    private int i = 0;
    private DecimalFormat twoDigits = new DecimalFormat("0");

    /**
     * constructor set default printwriter
     *
     * @param pw
     */
    public NotifySearch(PrintWriter pw, int num) {
        this.pw = pw;
        this.num = num;
    }

    /**
     * set links into the queue
     *
     * @param result
     * @throws Exception
     */
    @Override
    public void onSearchResult(List<DocumentDescriptor> result, Map<String, String> map) throws Exception {
        log.debug("CorpusID: " + map.get("corpusid"));
        log.debug("Links");
        ClassificationPackage classPkg = null;

        for (DocumentDescriptor docDescriptor : result) {
            try {
                classPkg = new ClassificationPackage();
                classPkg.setCorpusID(map.get("corpusid"));

                //Set the descriptor to send the files to the mq.
                log.debug(docDescriptor);
                classPkg.setDocumentDescriptor(docDescriptor);
                QueueProducer.sendMessage(classPkg);
                log.debug(docDescriptor);
            } catch (Exception e) {
                log.error("An exception has ocurred: ", e);
            }
        }
    }

    /**
     * show progress
     */
    @Override
    public void pr() {
        percent = ((i + 1) * 100) / num;
        pw.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
        log.debug("finished one proccess: " + percent + "%");
        pw.flush();
        i++;
    }
}