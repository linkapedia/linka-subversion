package api.util.search.images;

import api.util.ConfServer;
import api.util.bean.SearchBean;
import api.util.interfaces.ISearchNotify;
import api.util.interfaces.IWorker;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class SearchImageWorker implements Runnable, IWorker {

    private static final int BUFFER_SIZE = 4096;
    private static final int INIT_BAO_SIZE = (1024 * (5 * 1024));
    private static final int CONNECTION_TIMEOUT = 30000;
    private static final int READ_TIMEOUT = 30000;
    private static final Logger log = Logger.getLogger(SearchImageWorker.class);
    private final List<SearchBean> termsToSearch;
    private volatile long initWork;
    private int numTermsSuccess = 0;
    private int numTermsErrors = 0;
    private int index;
    private ISearchNotify searchNotify;
    private byte[] buffer;
    private int readBytes = 0;
    private ByteArrayOutputStream bao;
    private volatile boolean working = false;
    private Thread thread;
    private List<String> listTermsError = new ArrayList<String>();

    /**
     * first constructor to start process
     *
     * @param searchNotify
     */
    public SearchImageWorker(ISearchNotify searchNotify) {
        this.searchNotify = searchNotify;
        this.termsToSearch = new ArrayList<SearchBean>();
        this.buffer = new byte[BUFFER_SIZE];
        this.bao = new ByteArrayOutputStream(INIT_BAO_SIZE);
        this.index = 0;
    }

    /**
     * second constructor to restart the thread
     *
     * @param index
     * @param terms
     * @param searchNotify
     */
    public SearchImageWorker(int index, List<SearchBean> terms, ISearchNotify searchNotify) {
        this.index = index;
        this.termsToSearch = terms;
        this.searchNotify = searchNotify;
    }

    public void run() {
        working = true;
        SearchBean sbcurrent = null;
        List<HashMap<String, Object>> listMap = null;
        while (index < this.termsToSearch.size()) {
            try {
                initWork = System.currentTimeMillis();
                sbcurrent = this.termsToSearch.get(this.index);
                Map<String, String> otherParameters = new HashMap<>();
                otherParameters.put("nodeid", sbcurrent.getNodeid());
                listMap = this.searchTerm(sbcurrent);
                initWork = 0;
                this.searchNotify.onSearchResult(listMap, otherParameters);
                this.numTermsSuccess++;
            } catch (Exception ex) {
                this.numTermsErrors++;
                listTermsError.add(sbcurrent.getTerm());
                log.error("Exception INSERTING IMAGES TO: id:" + sbcurrent.getNodeid() + " - " + sbcurrent.getTerm() + "-> Exception: ", ex);
            } finally {
                this.searchNotify.pr();
                this.index++;
            }
        }
        working = false;
    }

    /**
     *
     * @return information to one term
     * @throws Exception
     */
    private List<HashMap<String, Object>> searchTerm(SearchBean sb) throws Exception {
        List<HashMap<String, Object>> listMap = new ArrayList<HashMap<String, Object>>();
        JSONArray ob = null;
        ob = getResultBing(sb);
        if (ob == null) {
            return listMap;
            //throw new Exception("Not have Result from Bing");
        }
        HashMap<String, Object> mapResult = null;
        int maxImages = ConfServer.getIntValue("bing.api.maxImagesToReturn");
        log.debug("maxImagesToReturn: " + maxImages);
        int i = 0;
        for (Object o : ob) {
            //limit images number
            if (i == maxImages) {
                break;
            }
            //get information to each image
            mapResult = getMapResult((JSONObject) o, sb);
            if (mapResult == null) {
                continue;
            }
            i++;
            listMap.add(mapResult);

        }
        return listMap;
        /*if ((listMap != null) && (!listMap.isEmpty())) {
         return listMap;
         } else {
         throw new Exception("Not have Result from Bing");
         }*/
    }

    /**
     * get json result request bing
     *
     * @param sb
     * @return
     */
    private JSONArray getResultBing(SearchBean sb) {
        log.debug("TERM: " + sb.getTerm());
        bao.reset();
        this.readBytes = 0;
        URL url = null;
        HttpURLConnection con = null;
        String urlEncode = null;
        InputStream is = null;
        try {
            urlEncode = URLEncoder.encode(sb.getTerm(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.debug("UnsupportedEncodingException: " + ex);
            return null;
        }
        try {
            url = getURL(urlEncode);
            log.debug("URL TO SEARCH IMAGES: " + url);
            log.debug("open connection API BING");
            con = (HttpURLConnection) url.openConnection();
            //Auth
            String userpassword = ConfServer.getValue("bing.api.user") + ":" + ConfServer.getValue("bing.api.pass");
            String encodedAuthorization = new String(Base64.encodeBase64(userpassword.getBytes()));
            con.setRequestProperty("Authorization", "Basic "
                    + encodedAuthorization);
            //------
            con.setConnectTimeout(CONNECTION_TIMEOUT);
            con.setReadTimeout(READ_TIMEOUT);
            con.connect();
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                log.debug("Error to Connect: " + url.toString());
                return null;
            }
            is = con.getInputStream();
            log.debug("read JSON API BING");
            while ((readBytes = is.read(buffer, 0, BUFFER_SIZE)) != -1) {
                bao.write(buffer, 0, readBytes);
            }
            JSONObject json = JSONObject.fromObject(new String(bao.toByteArray()));
            JSONArray ob = (JSONArray) json.getJSONObject("d").getJSONArray("results");
            return ob;
        } catch (MalformedURLException mue) {
            log.error("SearchImages: getResultBing(String): MalformedURLException url " + url + " " + mue.getMessage());
            return null;
        } catch (Exception e) {
            log.error("SearchImages: " + e.getMessage());
            return null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    log.error("IOException: " + ex.getMessage());
                }
            }
            if (con != null) {
                con.disconnect();
            }
        }
    }

    /**
     * get Images bytes
     *
     * @param o
     * @param sb
     * @return
     */
    private HashMap<String, Object> getMapResult(JSONObject o, SearchBean sb) {
        bao.reset();
        readBytes = 0;
        HashMap<String, Object> mapResult = new HashMap<String, Object>();
        InputStream is = null;
        HttpURLConnection con = null;
        URL mediaUrl = null;
        JSONObject cm = (JSONObject) o;
        log.debug("nodeid: " + sb.getNodeid());
        mapResult.put("nodeid", sb.getNodeid());
        log.debug("title: " + cm.getString("Title"));
        mapResult.put("title", cm.getString("Title"));
        log.debug("url: " + cm.getString("SourceUrl"));
        mapResult.put("url", cm.getString("SourceUrl"));
        try {
            mediaUrl = new URL(cm.getString("MediaUrl"));
            log.debug("URL TO SEARCH IN BING: " + mediaUrl);
            con = (HttpURLConnection) mediaUrl.openConnection();
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7");
            con.setConnectTimeout(CONNECTION_TIMEOUT);
            con.setReadTimeout(READ_TIMEOUT);
            con.connect();
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                log.error("HttpURLConnection != ok url -> " + mediaUrl);
                return null;
            }
            log.debug("con.getInputStream()");
            is = con.getInputStream();
            log.debug("read bytes and write bao");
            while ((readBytes = is.read(buffer, 0, BUFFER_SIZE)) != -1) {
                bao.write(buffer, 0, readBytes);
            }
            if (bao.toByteArray().length == 0) {
                return null;
            }
            log.debug("put bao into map");
            mapResult.put("image", bao.toByteArray());
            return mapResult;
        } catch (Exception ex) {
            log.debug(ex);
            return null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    log.error("Not close InputStream " + ex.getMessage());
                }
            }
            if (con != null) {
                con.disconnect();
            }
        }
    }

    /**
     * return URL to request bing. I use json to get data
     *
     * @param termSearch
     * @return
     * @throws Exception
     */
    private static URL getURL(String termSearch) throws Exception {
        StringBuilder sb = new StringBuilder();
        String url = ConfServer.getValue("bing.api.url");
        String top = ConfServer.getValue("bing.api.top");
        String skip = ConfServer.getValue("bing.api.skip");
        String ImageFilters = ConfServer.getValue("bing.api.ImageFilters");

        //optional parameter
        String market = ConfServer.getValue("bing.api.market");
        sb.append(url);
        //query
        sb.append("?Query=");
        sb.append("'").append(termSearch).append("'");
        //top
        sb.append("&$top=");
        sb.append(top);
        //skip
        sb.append("&$skip=");
        sb.append(skip);
        //format
        sb.append("&$format=json");
        //image filter
        sb.append("&ImageFilters=");
        sb.append("'").append(ImageFilters).append("'");
        if (market != null && !market.equals("")) {
            sb.append("&Market=").append("'").append(market).append("'");
        }
        log.debug(sb.toString());
        return new URL(sb.toString());
    }

    public List<String> getListTermsError() {
        return listTermsError;
    }

    public ISearchNotify getSearchNotify() {
        return this.searchNotify;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    synchronized public void addSearchTerm(SearchBean infoSearch) {
        this.termsToSearch.add(infoSearch);
    }

    public int getIndex() {
        return this.index;
    }

    public List<SearchBean> getTermList() {
        return this.termsToSearch;
    }

    @Override
    public long workTime() {
        return this.initWork;
    }

    public int getNumTermsErrors() {
        return this.numTermsErrors;
    }

    public int getNumTermsSuccess() {
        return this.numTermsSuccess;
    }

    public boolean isWorking() {
        return this.working;
    }

    @Override
    public Thread getThread() {
        return this.thread;
    }
}
