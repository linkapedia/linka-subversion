package api.util.search.images;

import com.iw.db.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class DaoImages {

    private static final Logger log = Logger.getLogger(DaoImages.class);

    /**
     * get terms to search map:
     * <NodeId, term to search>
     * TODO: pagination in this method
     *
     * @param nodeId
     * @param recursively
     * @return
     * @throws Exception
     */
    public Map<String, String> getDataMustHaveInheritance(String nodeId, boolean recursively) throws Exception {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new Exception("DaoImages: getDataMustHaveInheritance(.. Error getting connection from oracle");
        }
        String sql = "SELECT NODE.NODEID,"
                + "NODE.NODETITLE,"
                + "CORPUS.CORPUSTERMS,"
                + "LINKAPEDIA.GET_MUSTHAVE_WORDS(NODE.NODEID) AS NODE_MUSTWORDS,"
                + "NODE.NODEMUSTHAVEGATEINHERITANCE,"
                + "CASE NODE.NODEMUSTHAVEGATEINHERITANCE"
                + " WHEN NULL THEN (LINKAPEDIA.GET_MUSTHAVE_WORDS(NODE.NODEID))"
                + " WHEN 0    THEN (LINKAPEDIA.GET_MUSTHAVEGATE_WORDS(NODE.NODEID))"
                + " ELSE           (LINKAPEDIA.GET_MUSTHAVE_WORDS((SELECT PARENT_NODE.NODEID"
                + "                                                FROM NODE PARENT_NODE"
                + "                                                WHERE LEVEL = NODE.NODEMUSTHAVEGATEINHERITANCE"
                + "                                                START WITH PARENT_NODE.NODEID = NODE.PARENTID"
                + "                                                CONNECT BY PRIOR PARENT_NODE.PARENTID = PARENT_NODE.NODEID)))"
                + " END AS INHERITANCE_MUSTWORDS"
                + " FROM NODE"
                + " INNER JOIN CORPUS ON CORPUS.CORPUSID = NODE.CORPUSID";
        if (recursively) {
            sql += " START WITH NODE.NODEID = ? CONNECT BY PRIOR NODE.NODEID=NODE.PARENTID";
        } else {
            sql += " WHERE NODE.NODEID = ?";
        }
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Map<String, String> mapTerms = new HashMap<String, String>();
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, Integer.valueOf(nodeId));
            rs = pstmt.executeQuery();
            String musthaveInheritance;
            String curentNodeId;
            String corpusTerm;
            String mustWord;
            String inheritanceMustWord;
            while (rs.next()) {
                musthaveInheritance = rs.getString("NODEMUSTHAVEGATEINHERITANCE") == null ? "-1" : rs.getString("NODEMUSTHAVEGATEINHERITANCE");
                curentNodeId = rs.getString("NODEID");
                mustWord = rs.getString("NODE_MUSTWORDS") == null ? "" : rs.getString("NODE_MUSTWORDS").split("\\|\\|")[0];
                if (mustWord.equals("")) {
                    continue;
                }
                switch (Integer.valueOf(musthaveInheritance)) {
                    case -1:
                        corpusTerm = rs.getString("CORPUSTERMS") == null ? "" : rs.getString("CORPUSTERMS").split(",")[0];
                        if (!corpusTerm.equals("")) {
                            mapTerms.put(curentNodeId, "\"" + mustWord + "\" " + "\"" + corpusTerm + "\"");
                        } else {
                            mapTerms.put(curentNodeId, "\"" + mustWord + "\"");
                        }
                        break;
                    default:
                        //get from musthavegate table or inherit with NODEMUSTHAVEGATEINHERINT
                        inheritanceMustWord = rs.getString("INHERITANCE_MUSTWORDS") == null ? "" : rs.getString("INHERITANCE_MUSTWORDS").split("\\|\\|")[0];
                        if (!inheritanceMustWord.equals("")) {
                            mapTerms.put(curentNodeId, "\"" + mustWord + "\" " + "\"" + inheritanceMustWord + "\"");
                        } else {
                            mapTerms.put(curentNodeId, "\"" + mustWord + "\"");
                        }
                        break;
                }
            }
            return mapTerms;
        } catch (Exception e) {
            log.error("Error in getDataMustHaveInheritance", e);
            throw new Exception("Error in getDataMustHaveInheritance", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }

    }
}
