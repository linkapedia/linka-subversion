package api.util.search.images;

import api.util.interfaces.INotifyAll;
import java.util.List;
import java.util.TimerTask;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HandlerBing extends TimerTask {

    private List<SearchImageWorker> listThreads;
    private static final Log log = LogFactory.getLog(HandlerBing.class);
    private INotifyAll notifyFinish;
    /*
     * private int nrError = 0; private final long KILLTIME = 50000;
     */

    public HandlerBing(List<SearchImageWorker> listThread) {
        this.listThreads = listThread;
    }

    public HandlerBing(List<SearchImageWorker> listThread, INotifyAll notifyFinish) {
        this.listThreads = listThread;
        this.notifyFinish = notifyFinish;
    }

    @Override
    public void run() {
        /*
         * long workTime = 0; long currentTime = 0; long time = 0;
         * List<SearchImageWorker> listWorker = new
         * ArrayList<SearchImageWorker>();
         *
         * for (int i = 0; i < listThreads.size(); i++) { worker =
         * listThreads.get(i); workTime = worker.workTime(); currentTime =
         * System.currentTimeMillis(); time = currentTime - workTime; if (time
         * >= KILLTIME && worker.isWorking()) { nrError++; try {
         * worker.getThread().interrupt(); List<SearchBean> terms =
         * worker.getTermList(); int nextIndex = worker.getIndex() + 1;
         * SearchImageWorker newWorker = new SearchImageWorker(nextIndex, terms,
         * worker.getSearchNotify()); Thread th = new Thread(newWorker);
         * newWorker.setThread(th); th.start(); this.listThreads.remove(worker);
         * listWorker.add(newWorker); } catch (Exception ex) {
         * log.error("Interrupt Thread " + ex.getMessage()); } }
        }
         */
        log.debug("HANDLER IS ALIVE");
        //this.listThreads.addAll(listWorker);
        SearchImageWorker worker = null;
        int cont = 0;
        for (int i = 0; i < listThreads.size(); i++) {
            worker = listThreads.get(i);
            if (!worker.isWorking()) {
                cont++;
            }
        }

        if (cont == listThreads.size()) {
            notifyFinish.onFinish("FINISHED HANDLER");
        }
    }

    public void startThread() {
        for (int i = 0; i < listThreads.size(); i++) {
            this.listThreads.get(i).getThread().start();
        }
    }
}
