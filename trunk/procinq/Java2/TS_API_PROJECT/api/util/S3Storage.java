package api.util;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import java.io.InputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class S3Storage {

    private static final String AWS_ACCESS_KEY;
    private static final String AWS_SECRET_KEY;
    private static final Logger log = Logger.getLogger(S3Storage.class);
    private static final ThreadLocal<AmazonS3Client> threadLocal = new ThreadLocal();

    static {
        AWS_ACCESS_KEY = ConfServer.getValue("linkapedia.aws.accessKey");
        AWS_SECRET_KEY = ConfServer.getValue("linkapedia.aws.secretKey");
        
    }

    public static void storageData(String bucketName, String key, InputStream is, ObjectMetadata meta) throws S3StorageException {
        AmazonS3Client connection = getAmazonS3Connection();
        connection.putObject(bucketName, key, is, meta);
    }

    public static boolean exits(String bucketName, String key) {
        try {
            getAmazonS3Connection().getObjectMetadata(bucketName, key);
        } catch (AmazonServiceException e) {
            return false;
        }
        return true;

    }

    private static AmazonS3Client getAmazonS3Connection() {
        AmazonS3Client amazonS3Client = threadLocal.get();
        BasicAWSCredentials yourAWSCredentials = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);
        if (amazonS3Client == null) {
            amazonS3Client = new AmazonS3Client(yourAWSCredentials);
            threadLocal.set(amazonS3Client);
        }
        return amazonS3Client;
    }

    public static void finalizeConnection() {
        threadLocal.remove();
    }
}
