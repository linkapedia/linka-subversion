package api.util.split;

import api.util.ConfServer;
import java.io.File;

public class UtilSplitArchive {

    public void startSplit() {
        String path = ConfServer.getValue("tsapi.PATH88LEGS");
        try {
            File directory = new File(path);
            File[] numArchives = directory.listFiles();
            if (numArchives.length != 0) {
                for (File f : numArchives) {
                    if (!f.isDirectory()) {
                        UtilThread process = new UtilThread(f);
                        process.start();
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());

        }

    }
}
