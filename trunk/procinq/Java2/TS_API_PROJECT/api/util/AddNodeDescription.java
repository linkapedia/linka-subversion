package api.util;

import java.io.*;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;

/**
 * this function is to build node description from source
 *
 *	@authors Andres Restrepo

 *	@return	SUCCESS tag, if successful, along with the new node identifier.
 *  \verbatim
<?xml version="1.0" encoding="UTF-8" ?>
<TSRESULT>
<SUCCESS>add node description ok</SUCCESS>
</TSRESULT>
\endverbatim
 */
public class AddNodeDescription {

    private static final Logger log = Logger.getLogger(AddNodeDescription.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        String nodeid = (String) props.get("nodeid");
        if (nodeid == null) {
            log.debug("nodeid = null");
            return;
        }
        HashMap<String, String> mapResult = null;
        String description = "";
        DecimalFormat twoDigits = new DecimalFormat("0");
        int i = 0;
        try {
            mapResult = getMapToAnalize(dbc, nodeid);
        } catch (Exception e) {
            log.debug("Exception general AddNodeDescription " + e.getMessage());
            return;
        }
        if (mapResult != null) {
            description = "";
            int size = mapResult.size();
            long percent = 0;
            for (String id : mapResult.keySet()) {
                description = getDescriptionFromSource(mapResult.get(id));
                if (description != null) {
                    log.debug("Description from <<" + id + ">>: " + description);
                    try {
                        insertDescription(dbc, id, description);
                        log.debug("insert ok <<" + id + ">>");
                    } catch (Exception e) {
                        log.error("Exception general insert description " + e.getMessage());
                    }
                }
                percent = ((i + 1) * 100) / size;
                log.debug("PERCENT % " + percent);
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                out.flush();
                i++;
            }
            out.println("<SUCCESS>add nodes description ok</SUCCESS>");
        } else {
            log.error("mapResult=null (nodeid): " + nodeid);
        }
    }

    /**
     * get result to database and return a oranized map
     * @param con
     * @param nodeid
     * @return
     * @throws Exception 
     */
    private static HashMap<String, String> getMapToAnalize(Connection con, String nodeid) throws Exception {
        PreparedStatement pstmt = null;
        HashMap<String, String> mapResult = new HashMap<String, String>();
        ResultSet rs = null;
        String sSQL = "select p.nodeid, nd.nodesource from nodedata nd,(select n.nodeid, n.nodetitle from node n start with n.nodeid = ? connect by prior n.nodeid = n.parentid)  p where nd.nodeid(+)=p.nodeid";
        try {
            pstmt = con.prepareStatement(sSQL);
            pstmt.setInt(1, Integer.parseInt(nodeid));
            rs = pstmt.executeQuery();
            String idnode = "";
            String source = "";
            while (rs.next()) {
                idnode = rs.getString("nodeid");
                source = rs.getString("nodesource");
                //validate (node not contains source)
                if (null != source) {
                    mapResult.put(idnode, source);
                    log.debug("mapResult.put(): " + idnode + " source: " + true);
                } else {
                    log.debug("the nodeid <<" + idnode + ">> not contains source");
                }
            }
        } catch (SQLException se) {
            log.error("SQLException -> " + se.getMessage());
            return null;
        } finally {
            if (null != rs) {
                rs.close();
            }
            if (null != pstmt) {
                pstmt.close();
            }
        }
        if (mapResult.isEmpty()) {
            return null;
        } else {
            return mapResult;
        }
    }

    /**
     *  pass nodeid and setdescription
     * @param nodeid
     * @param description
     * @throws Exception 
     */
    private static void insertDescription(Connection con, String nodeid, String description) throws Exception {
        PreparedStatement pstmt = null;
        String sSQL = "UPDATE node SET nodedesc=? WHERE nodeid=?";
        try {
            pstmt = con.prepareStatement(sSQL);
            pstmt.setString(1, description);
            pstmt.setInt(2, Integer.parseInt(nodeid));
            pstmt.executeUpdate();
        } catch (SQLException se) {
            log.error("SQLException -> " + se.getMessage());
        } finally {
            if (null != pstmt) {
                pstmt.close();
            }
        }
    }

    /**
     * filter source and apply logic to get the description
     * @param source
     * @return 
     */
    private static String getDescriptionFromSource(String source) {
        StringBuilder descToReturn = new StringBuilder("");
        if (!source.isEmpty()) {
            if (source.contains("<LinkedSection>")) {
                source = source.substring(0,
                        source.indexOf("<LinkedSection>"));
            } else {
                source = source.substring(0,
                        source.length() - 1);
            }
            // define rules and test
            StringTokenizer words = new StringTokenizer(
                    source);
            String lastWord = "";
            int i = 1;
            while (words.hasMoreElements()) {
                if (i == 50) {
                    break;
                }
                lastWord = words.nextElement().toString();
                descToReturn.append(lastWord);
                descToReturn.append(" ");
                i++;
            }
            return descToReturn.toString();
        } else {
            return null;
        }
    }
}
