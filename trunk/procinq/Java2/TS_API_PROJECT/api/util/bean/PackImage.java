package api.util.bean;

/**
 *
 * @author andres
 */
public class PackImage {

    private String id;
    private String url;
    private byte[] image;

    public PackImage(String id, String url, byte[] image) {
        this.id = id;
        this.url = url;
        this.image = image;
    }

    public PackImage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
