package api.util;

import java.util.List;

/**
 *
 * @author andres
 */
public class Words implements Comparable<Words> {

    String nodeId;
    List<String> words;

    public Words(String nodeId, List<String> words) {
        this.nodeId = nodeId;
        this.words = words;
    }

    public Words() {
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @Override
    public int compareTo(Words t) {
        if (null == this.getWords() || this.getWords().isEmpty()) {
            return 0;
        }
        if (null == t || t.getWords().isEmpty()) {
            return 0;
        }
        return this.words.get(0).compareTo(t.getWords().get(0));
    }
}
