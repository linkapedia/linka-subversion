package api.util;

import java.util.Comparator;

/**
 *
 * @author andres
 */
public class SortedComparatorWord2 implements Comparator<Words> {

    @Override
    public int compare(Words t, Words t1) {
        if (null == t.getWords() || t.getWords().isEmpty()) {
            return 0;
        }
        if (null == t1.getWords() || t1.getWords().isEmpty()) {
            return 0;
        }
        String word;
        if (t.getWords().size() >= 2) {
            word = t.getWords().get(1);
        } else {
            word = t.getWords().get(0);
        }
        String word1;
        if (t1.getWords().size() >= 2) {
            word1 = t1.getWords().get(1);
        } else {
            word1 = t1.getWords().get(0);

        }
        return word.compareTo(word1);
    }
}
