package api.util;

import java.util.Comparator;

/**
 *
 * @author andres
 */
public class SortedComparatorWord1 implements Comparator<Words> {

    @Override
    public int compare(Words t, Words t1) {
        if (null == t.getWords() || t.getWords().isEmpty()) {
            return 0;
        }
        if (null == t1.getWords() || t1.getWords().isEmpty()) {
            return 0;
        }
        return t.getWords().get(0).compareTo(t1.getWords().get(0));
    }
}
