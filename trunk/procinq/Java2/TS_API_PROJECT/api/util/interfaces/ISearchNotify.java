package api.util.interfaces;

import java.util.Map;

/**
 *
 * @author andres
 */
public interface ISearchNotify<T> {

    
    void onSearchResult(T result,Map<String,String> params) throws Exception;
    void pr();
}
