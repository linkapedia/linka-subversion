package api.util.dynamodb.entities;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import java.io.Serializable;

/*
 * @author andreshinho gaucho
 */
@DynamoDBTable(tableName = "NodeStatic")
public class NodeStaticEntity implements Serializable {

    private String id;
    private String imageURL;

    @DynamoDBHashKey(attributeName = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @DynamoDBAttribute(attributeName = "imageURL")
    public String getImageURL() {
        return imageURL;
    }
}
