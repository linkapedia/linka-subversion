/*
 * get property in config-server
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
 */
package api.util;

import java.util.ResourceBundle;

public class ConfServer {

    private static ResourceBundle bundle = ResourceBundle.getBundle("config-server");

    public static String getValue(String key) {
        return bundle.getString(key);
    }

    public static int getIntValue(String key) {
        return Integer.parseInt(bundle.getString(key));
    }
}