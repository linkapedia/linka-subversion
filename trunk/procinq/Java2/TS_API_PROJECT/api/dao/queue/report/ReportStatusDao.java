package api.dao.queue.report;

import api.wikipedia.DAOException;
import com.iw.db.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 * Save, update process status in tables for report
 *
 * Table should to have a "status" field
 *
 * @author andres
 */
public class ReportStatusDao {

    private static final Logger log = Logger.getLogger(ReportStatusDao.class);

    public void updateProcessStatus(String requestId,
            String tableName,
            String status) throws Exception {
        Connection conn = ConnectionFactory.createConnection(
                ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new SQLException("ReportStatusDao: updateProcessStatus"
                    + "(.. Error getting connection from oracle");
        }
        String sql = "UPDATE " + tableName + " SET STATUS=? WHERE REQUESTID=?";
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, status);
            pstmt.setLong(2, Long.valueOf(requestId));
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Updating table -> "
                        + tableName + " failed");
            }
        } catch (Exception e) {
            throw new Exception("Updating table -> "
                    + tableName + " failed", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
