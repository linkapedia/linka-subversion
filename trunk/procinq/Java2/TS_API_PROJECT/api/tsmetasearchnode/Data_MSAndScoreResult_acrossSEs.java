/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 4, 2003
 * Time: 1:19:24 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsmetasearchnode;

import api.Log;
import com.indraweb.database.SQLGenerators;
import com.indraweb.utils.datasets.Histogram;
import com.indraweb.utils.datasets.HistogramMulti;
import com.indraweb.utils.sets.HashedSets;
import com.indraweb.utils.strings.UtilStrings;
import com.indraweb.webelements.IndraURL;
import com.iw.metasearch.Data_SearchengineCallout;
import java.sql.Connection;
import java.util.*;

public class Data_MSAndScoreResult_acrossSEs {

    // which URLs and which search engines per URL
    private HashedSets hashedSets_sURLToSetOfIntegerSEIDs = new HashedSets();
    // hit counts / search engine / aggergator = redundant to above
    private Histogram histIntegerSEIDToHitCount = new Histogram();
    private Histogram histIntegerSEIDToOpenedOKCount = new Histogram();
    private Histogram histIntegerSEIDToOpenedNotOKCount = new Histogram();
    private HashSet hsURLsAcrossSEsOpenedOK = new HashSet();
    private HashSet hsURLsAcrossSEsOpenedNotOK = new HashSet();
    private int iNumDocToDBByInsert = 0;
    private int iNumDocToDBByUpdate = 0;
    private int iNumDocNotUpdateDueToEditFlag = 0;
    // per SE - score type, score val, count
    Hashtable htsSEToHistMultiOfScoreTypeScoreValCount = new Hashtable();

    public synchronized void xincrScoreTypeAndValueOccurenceCount(
            String sURL,
            String sScoreType,
            String sScoreVal) throws Exception {
        HashSet hs = (HashSet) hashedSets_sURLToSetOfIntegerSEIDs.getSet(sURL);
        if (hs == null) {
            throw new Exception("hs == null");
        }

        xincrScoreTypeAndValueOccurenceCount("AL", sScoreType, sScoreVal);
        Iterator e = hs.iterator();
        while (e.hasNext()) {
            int iSEid = ((Integer) e.next()).intValue();
            String sSEName;
            try {
                sSEName = com.iw.metasearch.Data_SearchengineCallout.getSEShortName(iSEid);
            } catch (Exception e6) {
                Log.LogError("error calling com.iw.metasearch.Data_SearchengineCallout.getSEShortName ( iSEid )", e6);
                sSEName = "ER";
            }

            xincrScoreTypeAndValueOccurenceCount(sSEName, sScoreType, sScoreVal);
        }
    }

    public synchronized void incrDocToDBByInsert() {
        iNumDocToDBByInsert++;
    }

    public synchronized void incrDocToDBByUpdate() {
        iNumDocToDBByUpdate++;
    }

    public synchronized void incrDocNotUpdateDueToEditFlag() {
        iNumDocNotUpdateDueToEditFlag++;
    }

    public synchronized void setDocOpenedStatus(String sURL, boolean bOpenedOK) {
        if (bOpenedOK) {
            hsURLsAcrossSEsOpenedOK.add(sURL);
        } else {
            hsURLsAcrossSEsOpenedNotOK.add(sURL);
        }

        // set counts of openable docs per SE
        HashSet hsISEIDsThisURL = (HashSet) hashedSets_sURLToSetOfIntegerSEIDs.getSet(sURL);
        Iterator e = hsISEIDsThisURL.iterator();
        while (e.hasNext()) {
            Integer ISEid = (Integer) e.next();
            if (bOpenedOK) {
                histIntegerSEIDToOpenedOKCount.incrCounterForKeyValue(ISEid, 1);
            } else {
                histIntegerSEIDToOpenedNotOKCount.incrCounterForKeyValue(ISEid, 1);
            }
        }

    }

    /*
     * public synchronized void addData_resultsOneSEOneSearch ( String sSEShortName, com.iw.metasearch.Data_resultsOneSEOneSearch data_resultsOneSEOneSearch ) {
     * htStringSENameToData_resultsOneSEOneSearch.put (sSEShortName, data_resultsOneSEOneSearch); }
     */

    /*
     * public synchronized void incrScoreTypeAndValueOccurenceCount ( String sScoreType, String sScoreVal, int incrAmt ) { // eg score type : "SCORE1" // eg score value : "100" or "1-10" // eg score
     * count : 12 (occurence counter) data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs.incrScoreTypeAndValueOccurenceCount(sScoreType, sScoreVal, incrAmt ); }
     */

    /*
     * public synchronized Data_resultsOneSEOneSearch getData_resultsOneSEOneSearch ( String sSEName ) { Data_resultsOneSEOneSearch data_resultsOneSEOneSearch = (Data_resultsOneSEOneSearch)
     * htStringSENameToData_resultsOneSEOneSearch.get ( sSEName ); if ( data_resultsOneSEOneSearch == null ) { data_resultsOneSEOneSearch = new Data_resultsOneSEOneSearch(sSEName);
     * htStringSENameToData_resultsOneSEOneSearch.put ( sSEName, data_resultsOneSEOneSearch ); }
     *
     * return data_resultsOneSEOneSearch; }
     *
     */
    /*
     * public synchronized void addData_TSClassifyResultData ( Data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs data_oneTSCLassify ) {
     * vData_resultsScoreTypesAndValueCounts_PerSE_AlsoDocResults_OneTSClassifyOutput.addElement ( data_oneTSCLassify ); }
     */

    /*
     * public void aggrateMaybePrintData ( java.io.PrintWriter pw, int iIndentDepth ) { String sIndentDepth = UtilStrings.createStringFromCharAndNum ( ' ', iIndentDepth ); pw.println( sIndentDepth +
     * "DUMP of Data_MSAndScoreResult_acrossSEs:" );
     *
     * // SEARCH ENGINE RESULTS URLS PER SE pw.println( sIndentDepth + "DUMP SEARCH ENGINE RESULTS PER SE" ); Enumeration e = htStringSENameToData_resultsOneSEOneSearch.keys(); while (
     * e.hasMoreElements() ) { String sKeySEname = (String) e.nextElement(); Data_resultsOneSEOneSearch data_resultsOneSEOneSearch = (Data_resultsOneSEOneSearch)
     * htStringSENameToData_resultsOneSEOneSearch.get ( sKeySEname ); data_resultsOneSEOneSearch.dumpData(pw, iIndentDepth+1); }
     *
     * // SCORING RESULTS PER SE pw.println( sIndentDepth + "DUMP SCORING RESULTS PER SE" ); Enumeration e2 =
     * vData_resultsScoreTypesAndValueCounts_PerSE_AlsoDocResults_OneTSClassifyOutput.elements(); while ( e2.hasMoreElements() ) { Data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs
     * data_resultsScoreTypesAndValueCounts_PerSE_AlsoDocResults_OneTSClassifyOutput = (Data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs) e2.nextElement();
     * data_resultsScoreTypesAndValueCounts_PerSE_AlsoDocResults_OneTSClassifyOutput.dumpData ( pw, iIndentDepth+1, this ); } }
     */
    /*
     *
     * private boolean bAggDataGened = false;
     *
     * class Data_AggregatedWholeRefresh_AcrossMultipleTSClassifies_AcrossSEsInclAll_ACrossScoreTypes_CountsPerScoreValue { Hashtable htIntegerSEID_ToHistogramMultiPerSCoreType = new Hashtable();
     *
     * public void addData ( String sSEName, String sScoreType, String sScoreval, int iIncrAmount) { HistogramMulti histmult = (HistogramMulti) htIntegerSEID_ToHistogramMultiPerSCoreType.get ( sSEName
     * ); if ( histmult == null ) { histmult = new HistogramMulti(); htIntegerSEID_ToHistogramMultiPerSCoreType.put (sSEName, histmult ); } histmult.incrValue (sScoreType, sScoreval, iIncrAmount); }
     *
     * }
     *
     *
     */
    public synchronized void recordMSSearchHits(IndraURL iURL) {
        String sURL = iURL.getURL();
        if (sURL.equals("ZEROFOUNDMARKER")) {
            //hashedSets_sURLToSetOfIntegerSEIDs.addToSet ( iURL.getURL(), ISEID );
            histIntegerSEIDToHitCount.incrCounterForKeyValue(new Integer(iURL.getWhichSE()), 0);
            System.out.println("marking zero found SE [" + iURL.getWhichSE() + "]");
        } else {
            Vector v = iURL.getVecIntegerSEsFindingThisURL();
            java.util.Enumeration e3 = v.elements();
            // collect up which search engines per URL
            // collect up total found overall per SE
            while (e3.hasMoreElements()) {
                //String sSEName = Data_SearchengineCallout.getSEShortName ( ( (Integer) e3.hasMoreElements()).intValue() );
                Integer ISEID = (Integer) e3.nextElement();
                hashedSets_sURLToSetOfIntegerSEIDs.addToSet(iURL.getURL(), ISEID);
                histIntegerSEIDToHitCount.incrCounterForKeyValue(ISEID, 1);
            }
        }
    }

    public void XMLOut(java.io.PrintWriter pw,
            int iIndentDepth,
            int iNodeID,
            Connection dbc) throws Exception {

        /*
         * CREATE TABLE MSRESULTDOCS ( NODEID NUMBER(12) NOT NULL, SEARCHENGINEID NUMBER(4) NOT NULL, DATEOFSEARCH DATE DEFAULT SYSDATE, DOCSBYUPDATE NUMBER(6), DOCSBYINSERT NUMBER(6) DOCSOPENEDOK
         * NUMBER(6), DOCSOPENEDNOTOK NUMBER(6) )
         *
         * CREATE TABLE MSRESULTSCORES ( NODEID NUMBER(12) NOT NULL, SEARCHENGINEID NUMBER(4) NOT NULL, DATEOFSEARCH DATE DEFAULT SYSDATE, SCORETYPE VARCHAR2(10), SCOREVALUE VARCHAR2(10), SCORECOUNT
         * NUMBER(6) )
         *
         */

        /*
         * modeled on demo current basis
         *
         * monitor status invoke actions configure
         *
         * rates
         */

        String sSQLdeletePreDocs = "delete from MSRESULTDOCS where NODEID = " + iNodeID;
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLdeletePreDocs, dbc, false, -1);

        String sSQLdeletePreScores = "delete from MSRESULTSCORES where NODEID = " + iNodeID;
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLdeletePreScores, dbc, false, -1);


        String sIndentDepth = UtilStrings.createStringFromCharAndNum(' ', iIndentDepth);

        pw.println(sIndentDepth + "<TSMETASEARCHNODE>");
        pw.println(sIndentDepth + " <DOCSOPENEDOK>" + hsURLsAcrossSEsOpenedOK.size() + "</DOCSOPENEDOK>");
        pw.println(sIndentDepth + " <DOCSOPENEDNOTOK>" + hsURLsAcrossSEsOpenedNotOK.size() + "</DOCSOPENEDNOTOK>");
        pw.println(sIndentDepth + " <NUMDOCUPDATE>" + iNumDocToDBByUpdate + "</NUMDOCUPDATE>");
        pw.println(sIndentDepth + " <NUMDOCINSERT>" + iNumDocToDBByInsert + "</NUMDOCINSERT>");

//        Hashtable htStringSENameToData_resultsOneSEOneSearch = new Hashtable();

//        Data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs  data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs = new Data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs();

        pw.println(sIndentDepth + " <SEARCHRESULTS>");
        // SEARCH ENGINE RESULTS URLS PER SE
        Enumeration eSEIDs = histIntegerSEIDToHitCount.getData_htObjToIntegerCount().keys();
        while (eSEIDs.hasMoreElements()) {
            pw.println(sIndentDepth + "  <SEARCHENGINERESULT>");
            Integer ISEid = (Integer) eSEIDs.nextElement();
            pw.println(sIndentDepth + "   <ENGINEID>" + ISEid + "</ENGINEID>");
            String sSEName = Data_SearchengineCallout.getSEShortName(ISEid.intValue());
            pw.println(sIndentDepth + "   <ENGINENAME>" + sSEName + "</ENGINENAME>");

            Integer ICountHitsThisSE = (Integer) histIntegerSEIDToHitCount.getData_htObjToIntegerCount().get(ISEid);

            pw.println(sIndentDepth + "   <NUMURLS>" + ICountHitsThisSE + "</NUMURLS>");

            String sNUMURLSOPENOK = "0";
            if (histIntegerSEIDToOpenedOKCount.getData_htObjToIntegerCount().get(ISEid) != null) {
                sNUMURLSOPENOK = histIntegerSEIDToOpenedOKCount.getData_htObjToIntegerCount().get(ISEid).toString();
            }
            String sNUMURLSOPENNOTOK = "0";
            if (histIntegerSEIDToOpenedNotOKCount.getData_htObjToIntegerCount().get(ISEid) != null) {
                sNUMURLSOPENNOTOK = histIntegerSEIDToOpenedNotOKCount.getData_htObjToIntegerCount().get(ISEid).toString();
            }
            pw.println(sIndentDepth + "   <NUMURLSOPENOK>" + sNUMURLSOPENOK + "</NUMURLSOPENOK>");
            pw.println(sIndentDepth + "   <NUMURLSOPENNOTOK>" + sNUMURLSOPENNOTOK + "</NUMURLSOPENNOTOK>");

            dbInsertMSRESULTDOCS(iNodeID, ISEid.intValue(),
                    iNumDocToDBByUpdate,
                    iNumDocToDBByInsert,
                    Integer.parseInt(sNUMURLSOPENOK),
                    Integer.parseInt(sNUMURLSOPENNOTOK),
                    dbc);

            // NOW SCORES THIS SE
            HistogramMulti histmultStringScoreTypeToHistStringScoreValAndIntegerCount_thisSE =
                    (HistogramMulti) htsSEToHistMultiOfScoreTypeScoreValCount.get(sSEName);

            if (histmultStringScoreTypeToHistStringScoreValAndIntegerCount_thisSE != null) {
                //  Across score types this SE
                Enumeration eScoreTypes = histmultStringScoreTypeToHistStringScoreValAndIntegerCount_thisSE.keys();
                while (eScoreTypes.hasMoreElements()) {
                    String sScoreType = (String) eScoreTypes.nextElement();

                    Histogram histScoreValsAndCounts_ThisSEAndScoreType = histmultStringScoreTypeToHistStringScoreValAndIntegerCount_thisSE.getHistogram(sScoreType);
                    Enumeration eScoreVals = histScoreValsAndCounts_ThisSEAndScoreType.getData_htObjToIntegerCount().keys();
                    //  Across score val's this SE this score type
                    while (eScoreVals.hasMoreElements()) {
                        // finally the count
                        String sScoreVal = (String) eScoreVals.nextElement();
                        Integer ICount = (Integer) histScoreValsAndCounts_ThisSEAndScoreType.getData_htObjToIntegerCount().get(sScoreVal);
                        pw.println(sIndentDepth + "   <SCORETYPEVALCOUNT>");
                        pw.println(sIndentDepth + "    <SCORETYPE>" + sScoreType + "</SCORETYPE>");
                        pw.println(sIndentDepth + "    <SCOREVAL>" + sScoreVal + "</SCOREVAL>");
                        pw.println(sIndentDepth + "    <SCOREVALCOUNT>" + ICount + "</SCOREVALCOUNT>");
                        pw.println(sIndentDepth + "   </SCORETYPEVALCOUNT>");

                        dbInsertMSRESULTSCORES(iNodeID, ISEid.intValue(),
                                sScoreType,
                                sScoreVal,
                                ICount.toString(),
                                dbc);


                    }

                }
            }  // if there were any hits

            pw.println(sIndentDepth + "  </SEARCHENGINERESULT>");
        }

        pw.println(sIndentDepth + " </SEARCHRESULTS>");

        // SCORING RESULTS PER SE

        pw.println(sIndentDepth + "</TSMETASEARCHNODE>");
        dbc.commit();
    }  // xmlout

    private static void dbInsertMSRESULTDOCS(int iNODEID,
            int iSEARCHENGINEID,
            int iDOCSBYUPDATE,
            int iDOCSBYINSERT,
            int iDOCSOPENEDOK,
            int iDOCSOPENEDNOTOK,
            Connection dbc) throws Exception {
        Vector vStringsCols = new Vector();
        vStringsCols.addElement("NODEID");
        vStringsCols.addElement("SEARCHENGINEID");
        vStringsCols.addElement("DATEOFSEARCH");
        vStringsCols.addElement("DOCSBYUPDATE");
        vStringsCols.addElement("DOCSBYINSERT");
        vStringsCols.addElement("DOCSOPENEDOK");
        vStringsCols.addElement("DOCSOPENEDNOTOK");

        Vector vStringsValues = new Vector();
        vStringsValues.addElement("" + iNODEID);
        vStringsValues.addElement("" + iSEARCHENGINEID);
        vStringsValues.addElement("SYSDATE");
        vStringsValues.addElement("" + iDOCSBYUPDATE);
        vStringsValues.addElement("" + iDOCSBYINSERT);
        vStringsValues.addElement("" + iDOCSOPENEDOK);
        vStringsValues.addElement("" + iDOCSOPENEDNOTOK);

        String sSQLinsertDocTable = SQLGenerators.genSQLInsert(
                "MSRESULTDOCS",
                vStringsCols,
                vStringsValues);
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLinsertDocTable, dbc, false, -1);
    }

    private static void dbInsertMSRESULTSCORES(int iNODEID,
            int iSEARCHENGINEID,
            String sSCORETYPE,
            String sSCOREVALUE,
            String sSCORECOUNT,
            Connection dbc) throws Exception {
        Vector vStringsCols = new Vector();
        vStringsCols.addElement("NODEID");
        vStringsCols.addElement("SEARCHENGINEID");
        vStringsCols.addElement("DATEOFSEARCH");
        vStringsCols.addElement("SCORETYPE");
        vStringsCols.addElement("SCOREVALUE");
        vStringsCols.addElement("SCORECOUNT");

        Vector vStringsValues = new Vector();
        vStringsValues.addElement("" + iNODEID);
        vStringsValues.addElement("" + iSEARCHENGINEID);
        vStringsValues.addElement("SYSDATE");
        vStringsValues.addElement("" + sSCORETYPE);
        vStringsValues.addElement("" + sSCOREVALUE);
        vStringsValues.addElement("" + sSCORECOUNT);

        String sSQLinsertDocTable = SQLGenerators.genSQLInsert(
                "MSRESULTSCORES",
                vStringsCols,
                vStringsValues);
        com.indraweb.database.JDBCIndra_Connection.executeUpdateToDB(sSQLinsertDocTable, dbc, false, -1);
    }

    /*
     *
     *
     *
     *
     * public class Data_resultsOneSEOneSearch { String sSEName ; boolean bMadeItToEndAllX00 = false; int iNumURLsFound = -1;
     *
     * public Data_resultsOneSEOneSearch (String sSEName_) { sSEName = sSEName_; }
     *
     * public void setbMadeItToEndAllX00 ( boolean b ) { bMadeItToEndAllX00 = b; } public void setNumURLsFound ( int i ) { if (iNumURLsFound != -1) Log.Log ("iNumURLsFound != -1") ; // why being set
     * 2x iNumURLsFound = i; } public int getNumURLsFound () { return iNumURLsFound; }
     *
     * public void XMLout ( java.io.PrintWriter pw, int iIndentDepth ) { String sIndentDepth = UtilStrings.createStringFromCharAndNum ( ' ', iIndentDepth ); pw.println( sIndentDepth +
     * "<SEARCHENGINERESULT>"); pw.println( sIndentDepth + " <ENGINENAME>" + sSEName + "</ENGINENAME>" ); pw.println( sIndentDepth + " <FULLRETURNSET>" + bMadeItToEndAllX00 + "</FULLRETURNSET>" );
     * pw.println( sIndentDepth + " <NUMURLS>" + iNumURLsFound + "</NUMURLS>" ); pw.println( sIndentDepth + "</SEARCHENGINERESULT>"); }
     *
     * }
     *
     *
     *
     *
     */
}
