/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 4, 2003
 * Time: 1:29:34 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsmetasearchnode;

import com.indraweb.webelements.IndraURL;
import com.indraweb.util.Log;
import com.iw.metasearch.Data_SearchengineCallout;

import java.util.Hashtable;

// producer creates IndraURLs and consumer classify/scores them

public class WorkQManager_ForTSMetasearchNode extends com.iw.threadmgr.WorkQManager {

    Data_MSAndScoreResult_acrossSEs data_MSAndScoreResult_acrossSEs = new Data_MSAndScoreResult_acrossSEs ();
    Hashtable htsURLToIndraURL = new Hashtable();
    int iWhichDB_ForSEParams = -1;

    public WorkQManager_ForTSMetasearchNode (
                    int iFullSize_,
                    int iMSTimeoutProducersToComplete_,
                    int iMSTimeoutAllThreadsToComplete_,
                    int iWhichDB_ForSEParams_,
                    int iCapOnTotalUnitsAcceptedByProducers_

                )
    {
        super(iFullSize_, iMSTimeoutProducersToComplete_, iMSTimeoutAllThreadsToComplete_ , iCapOnTotalUnitsAcceptedByProducers_);
        data_MSAndScoreResult_acrossSEs = new Data_MSAndScoreResult_acrossSEs();
        iWhichDB_ForSEParams = iWhichDB_ForSEParams_;
    }


    // about to grab an iURL For scoring ...
    // tell data collector of the url and who found it ...

    Hashtable htStringSENameToHashsetIntegerSEidsWhichFoundThisURL = new Hashtable();
    public Object  getWorkItem ()  throws Exception
    {
        com.indraweb.webelements.IndraURL iURL = ( com.indraweb.webelements.IndraURL ) super.getWorkItem();

        if ( iURL != null )
            data_MSAndScoreResult_acrossSEs.recordMSSearchHits ( iURL );

        return iURL;
    }

    private Object oSynchIURLht = new Object();
    int iAddWorkItemCallCount_imperfect = 0;
    public void addWorkItem ( Object o, String sSEid) throws Exception
    {
        iAddWorkItemCallCount_imperfect++;
        if ( iAddWorkItemCallCount_imperfect % 1000 == 0 )
            System.out.println(new java.util.Date() +  " WorkQManager_ForTSMetasearchNode : iAddWorkItemCallCount_imperfect [" + iAddWorkItemCallCount_imperfect  + "] this one found from SEID [" + sSEid + "]" );
        IndraURL iURL = (IndraURL) o;
        int iSEid = Integer.parseInt(sSEid);

        synchronized ( oSynchIURLht )
        {
            IndraURL iURLPre = (IndraURL) htsURLToIndraURL.get (iURL.getURL());
            if ( iURLPre == null ) {
                htsURLToIndraURL.put ( iURL.getURL(), iURL );
            } else {
                iURLPre.addFoundByThisSE( iSEid );
            }

        }
        super.addWorkItem(o, "" + iSEid );
        //Log.logClearcr("Adding work item URL [" + iURL.sURL  + "]");
    }

    public Data_MSAndScoreResult_acrossSEs getData_MSAndScoreResult()
    {
        return data_MSAndScoreResult_acrossSEs;
    }

    public int getWhichDB_ForSEParams ()
    {
        return iWhichDB_ForSEParams;
    }





}
