/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Jan 22, 2003
 * Time: 11:47:08 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsmetasearchnode;

import api.APIProps;
import com.indraweb.execution.Session;
import com.indraweb.execution.ThreadInfo;
import com.indraweb.ir.clsStemAndStopList;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.webelements.IndraURL;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;
import com.iw.classification.Data_WordsToNodes;
import com.iw.metasearch.Data_SearchengineCallout;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TSMetasearchNode {

    private static int iCallCounter = 0;
    private static final Integer ICallCounterSemaphore = new Integer(0); // dummy number - object to synch call counter on

    // ***********************************************************
    public static void handleTSapiRequest(HttpServletRequest req,
            HttpServletResponse res,
            api.APIProps props,
            PrintWriter out //Connection dbc
            )
            // ***********************************************************
            throws Throwable {
        //System.out.println("in TSMetasearchNode M" );
        // PRE THREADING DB GET


        //com.indraweb.util.UtilFile.deleteFile("/tmp/temphk.txt");


        int iProp_NodeID = Integer.parseInt((String) props.get("NODEID", true));
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        System.out.println("tsmetasearchnode started node [" + iProp_NodeID + "]");

        String sMACHTHREADINFO = (String) props.get("MACHTHREADINFO", true);
        int iNumThreadsExtURLCalls = props.getint("NumThreadsExtURLCalls", true);
        System.out.println("TSMetasearchNode M [" + sMACHTHREADINFO + "] gets node [" + iProp_NodeID + "]");
        long lStartAPIcallTSMetasearch = System.currentTimeMillis();

        Connection dbc = api.statics.DBConnectionJX.getConnection("SVR", "TSMSNode 1 to fill or refill ");
        synchronized (ICallCounterSemaphore) {
            {
                iCallCounter++;
            }
            if (Session.stopList == null) // really should be done in session as a privatized variabel
            {
                Session.stopList = new clsStemAndStopList(Session.cfg.getProp("IndraHome") + Session.cfg.getProp("StopListFile"));
            }

            if (Data_SearchengineCallout.bNeedCacheBeFilled) {
                Data_SearchengineCallout.fillOrRefillCache(dbc);
            }
        }

        //out.println ("<hktestCALLCOUNT_FN>" + iCallCounter + "</hktestCALLCOUNT_FN>");out.flush();
        out.println("<CALLCOUNT_FN>" + iCallCounter + "</CALLCOUNT_FN>");
        out.flush();
        // load the data struct so we can get this node's sig etc info into a metasearch
        Data_WordsToNodes.refreshCache(dbc, new SignatureParms(new APIProps()));
        dbc = api.statics.DBConnectionJX.freeConnection(dbc, "SVR", "TSMSNode 1 to fill or refill ");

        try {
            out.println("<NODEID>" + iProp_NodeID + "</NODEID>");
            out.println("<NUMTHREAD>" + iNumThreadsExtURLCalls + "</NUMTHREAD>");
            out.println("<MACHTHREADINFO>" + sMACHTHREADINFO + "</MACHTHREADINFO>");

            boolean b1 = Session.cfg.getPropBool("useproxyWithHttp");
            com.indraweb.util.UtilNet.setHttpProxyOnOff(b1);
            boolean b2 = Session.cfg.getPropBool("useproxyWithSockets");
            com.indraweb.util.UtilNet.setSocketProxyOnOff(b2);

            //            System.out.println( iCallCounter + ". ****** tsMetaSearchNode processing node [" + iProp_NodeID + "] " +
            //                    "useproxyWithHttp [" + b1 + "] " +
            //                    "useproxyWithSockets [" + b2 + "] "
            //            );

            com.iw.scoring.NodeForScore nfs = Data_NodeIDsTo_NodeForScore.getNodeForScore(iProp_NodeID, false);

            // create thread manager
            int iCapOnTotalUnitsAcceptedByProducers = -1;
            if (false) // hbk control false for production, #= iCapOnTotalUnitsAcceptedByProducers
            {
                iCapOnTotalUnitsAcceptedByProducers = 1;
            }

            WorkQManager_ForTSMetasearchNode workQForMS = new api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode(-1, -1, -1,
                    1,
                    iCapOnTotalUnitsAcceptedByProducers);

            // *************************************
            // PRODUCER - URLS TO SCORE
            // new ThreadProd_API_MSGetURLs
            // *************************************
            // create IndraURL objects into the workQ
            ThreadProd_API_MSGetURLs[] tArrProducers = null;
            if (true) // hbk control true for production false for test URL set
            {
                int[] iArrSE = com.iw.metasearch.Data_SearchengineCallout.getSearchEngineIDs();
                tArrProducers =
                        new ThreadProd_API_MSGetURLs[iArrSE.length];
                for (int i = 0; i < iArrSE.length; i++) {
                    int iSEID = iArrSE[i];
                    String sSECalloutPath = com.iw.metasearch.Data_SearchengineCallout.getClassPath(iSEID);
                    tArrProducers[i] = new ThreadProd_API_MSGetURLs(
                            "constructing MS producer thread [" + sSECalloutPath + "] [" + (i + 1) + "] of [" + iArrSE.length + "] "
                            + " SEID [" + iSEID + "] name [" + Data_SearchengineCallout.getSEShortName(iSEID) + "] ", workQForMS, nfs, iSEID);
                    tArrProducers[i].setName("tProd MS [" + i + "]");
                    //Log.logClearcr("spinning SE thread [" + Data_SearchengineCallout.getSEShortName(iSEID)  + "]");
                    // ThreadInfo.addThreadToRunningList("tMn");  // give this thread an ID in thread manager
                }
            } else // to test/debug a single URL simulated as found (don't run producers)
            {
                String sURLtest = "http://www.xs4all.nl/~stgvisie/visie/dureport/vitale_du.html";
                //Log.logClearcr("testing URL [" + sURLtest + "]");
                IndraURL iURL = new com.indraweb.webelements.IndraURL(
                        sURLtest,
                        "test title",
                        "test date",
                        "test siaSummary",
                        1, // SEID
                        -1);

                workQForMS.addWorkItem(iURL, "1");
                workQForMS.setbProducersDone();
            }

            // *************************************
            // CREATE CONSUMER THREADS - SCORE URLS FROM METASEARCH
            // *************************************
            // to create IndraURL objects into the workQ
            // new ThreadConsumer_ScoreMSResultIURLs
            // constructor consumers to use IndraURL objects to call into
            //     tsclassify to score
            ThreadConsumer_ScoreMSResultIURLs[] tArrConsumerScoreMSResult =
                    new ThreadConsumer_ScoreMSResultIURLs[iNumThreadsExtURLCalls];

            for (int i = 0; i < iNumThreadsExtURLCalls; i++) {
                // hbk control major31 Connection dbcPerThread = com.indraweb.database.DBConnectionJ
                String sTname = "smach [" + sMACHTHREADINFO + "] W/inMti [" + i + "]";
                tArrConsumerScoreMSResult[i] = new ThreadConsumer_ScoreMSResultIURLs(sTname, iProp_NodeID, workQForMS, props, sMACHTHREADINFO, u);
                //Log.logcr("spinning tsmetasearch consumer thread [" + i + "]");
                tArrConsumerScoreMSResult[i].setName(sTname);
                ThreadInfo.addThreadToRunningList(tArrConsumerScoreMSResult[i], sTname);

                // TO DO - have a thread list manager
                // ThreadInfo.addThreadToRunningList("tMn");  // give this thread an ID in thread manager
            }

            // start all running
            long ltimeStart = System.currentTimeMillis();
            boolean bSynch = true;
            // ***********************************
            // ***********************************
            // START THREADS AND WAIT HERE FOR ALL PROD AND CONS THREADS TO COMPLETE
            // ***********************************
            // ***********************************
            //            System.out.println("tArrProducers.length  [" + tArrProducers.length +
            //                    "] tArrConsumerScoreMSResult.length [" + tArrConsumerScoreMSResult.length +
            //                    "] done with node [" + iProp_NodeID  + "]");



            workQForMS.setAndStartThreads(tArrProducers, tArrConsumerScoreMSResult, bSynch); // true = synchronous

            // WAITS HE RE FOR ALL THREADS TO COMPLETE
            Data_MSAndScoreResult_acrossSEs data_MSAndScoreResult = workQForMS.getData_MSAndScoreResult();
            java.io.PrintWriter pw = new java.io.PrintWriter(System.out);
            //data_MSAndScoreResult.XMLOut(pw, 0);

            // POST THREADING DB GET
            dbc = api.statics.DBConnectionJX.getConnection("SVR", "TSMSNode 2 to fill or refill cache");
            try {


                data_MSAndScoreResult.XMLOut(out, 0, iProp_NodeID, dbc);
            } finally {
                dbc = api.statics.DBConnectionJX.freeConnection(dbc, "SVR", "TSMSNode 2 to fill or refill cache");
            }

            pw.flush();
        } catch (Exception e) {
            System.out.println("TSMetasearchNode ERROR M [" + sMACHTHREADINFO + "] in node [" + iProp_NodeID + "]");
            api.Log.LogError("TSMetasearchNode ERROR M [" + sMACHTHREADINFO + "] in node [" + iProp_NodeID + "]", e);
        } finally {
            /*
             * System.out.println("TSMetasearchNode finally M [" + sMACHTHREADINFO + "] done with node [" + iProp_NodeID + "]");
             * api.statics.DBConnectionJX.freeConnection( null, "SVR", "NULL FOR COUNT VERIFY" ) ; // hbk control ( debugging connection counts - this will print the dbc count out
             * Thread.dumpStack();
             */
            //Log.logcr("TSMetaS finally  Done N [" + iProp_NodeID + "] ms [" + (System.currentTimeMillis() -
            //      lStartAPIcallTSMetasearch) + "] M [" + sMACHTHREADINFO + "]");
        }

    }
} // public static void handleTSapiRequest ( HttpServletRequest req, api.APIProps props, PrintWriter out, Connection dbc )

