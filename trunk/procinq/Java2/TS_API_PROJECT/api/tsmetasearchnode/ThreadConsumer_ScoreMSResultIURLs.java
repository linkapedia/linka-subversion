/*
* Created by IntelliJ IDEA.
* User: Henry Kon
* Date: Jan 26, 2003
*/
package api.tsmetasearchnode;

import api.tsclassify.TSClassifyDoc;
import com.iw.system.User;
import com.indraweb.encyclopedia.DocForScore;
import com.indraweb.execution.Session;
import com.indraweb.util.Log;
import com.indraweb.util.UtilProfiling;
import com.indraweb.webelements.IndraURL;
import com.indraweb.utils.datasets.HistogramMulti;
import com.indraweb.ir.ParseParms;
import com.indraweb.signatures.SignatureParms;
import com.iw.threadmgr.ThreadGenericProdCons;
import com.iw.threadmgr.WorkQManager;

import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.sql.Connection;
import java.util.Hashtable;

public class ThreadConsumer_ScoreMSResultIURLs extends ThreadGenericProdCons
{

    api.APIProps props;
    int iNodeID = -1;
    String sMACHTHREADINFO = null;
    boolean bVerbose = false;
    User u = null;

    //private Hashtable htPendingURLs_strToLongStartTime = new Hashtable ();
    public ThreadConsumer_ScoreMSResultIURLs ( String sDesc_,
                                             int iNodeID_,
                                             api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQForMS_,
                                             api.APIProps props_,
                                             String sMACHTHREADINFO_,
                                             User u_

                                             )
    {
        super(sDesc_, workQForMS_);
        props = props_;
        iNodeID = iNodeID_;
        sMACHTHREADINFO = sMACHTHREADINFO_;
        u = u_;

        File f = new File("/temp/IndraDebugMetaSearchVerbose.txt");
        if (f.exists())
        {
            System.out.print("/temp/IndraDebugMetaSearchVerbose.txt present");
            bVerbose = true;
        }
    }


    private api.util.SynchronizedCounter synchCntDocsAccessed = new api.util.SynchronizedCounter();

    //public Hashtable getHtPrndingURLs() {
//        return htPendingURLs_strToLongStartTime;
//    }

    private static int iNumClassifiedCounter_imperfect = 0 ;
    public void run()   // score MS result IndraURL's
    {
        String sWhichDB = "SVR";
        int iLoop = 0;
        try
        {
            while (bStopLooping == false)
            {
                try
                {
                    // Log.logcr("\r\n\r\n" + new java.util.Date () +  " ###### TOP OF LOOP ThreadConsumer_ScoreMSResultIURLs  ");
                    //WorkItemNodeToCrawl workItem = (WorkItemNodeToCrawl) workQ.getWorkItem();
                    // ***************************
                    // GET WORK ITEM - iurl to score
                    // ***************************

                    int iCountNumDocsOpenedThisThread = 0;
                    IndraURL iURL = (IndraURL) super.workQ.getWorkItem();

                    //Log.logcr("ThreadConsumer_ScoreMSResultIURLs got work item iLoop [" + iLoop  + "]");
                    if (iURL == null)
                    {
                        //Log.logcr("iURL == null  in ThreadConsumer_ScoreMSResultIURLs [" + Thread.currentThread().getName() + "] workItem is null - producers should be done ");
                        break;  // should mean only that all producers are done
                    }

                    iCountProducedOrConsumed++;

                    String sURL = iURL.getURL();

                    // TO DO : the consumer's work goes here
                    // get the doc into some kind of form and then
                    // SCORE IT - CALL IN TO TSCLassify

                    int iProp_RepositoryID = -1;

                    String sProp_GenreID = (String) props.get("GenreID", TSClassifyDoc.sGENERID_CLASSIFIER_INSERTS); // optional - default = -1 - 'API Classifier Default'
                    String sProp_DocTitle = (String) props.get("DocTitle", null);
                    String sProp_DocSummary = (String) props.get("DocSummary", "");
                    boolean bProp_DocSumm = props.getbool("DocSumm", "true");
                    String sProp_Corpora = (String) props.get("Corpora", null); // overrides existing doc summary if any
                    // String sProp_NodeSet = (String) props.get("NodeSet", null); // overrides existing doc summary if any
                    String sProp_NodeSet = "" + iNodeID;
                    boolean bStemOn = Session.cfg.getPropBool("StemOnClassifyNodeDocScoring");
                    String sProp_ExplainScoresShowDocWords = (String) props.get("ExplainScoresShowDocWords", "false"); // use callers pref if there
                    boolean bProp_post = props.getbool("post", "false");

                    String sExplainScores = (String) props.get("ExplainScores", "false");
                    boolean bExplainScores = props.getbool("ExplainScores", "false");
                    boolean bWantPhrasing = props.getbool("phrasing", "true");
                    // NodeToScoreExplain
                    String sProp_iNodeToScoreExplain = (String) props.get("NodeToScoreExplain", "-1"); // use callers pref if there
                    int iNodeToScoreExplain = Integer.parseInt(sProp_iNodeToScoreExplain);	// -1 for all
                    // NumScoresToExplain
                    String sProp_iNumScoresToExplain = (String) props.get("NumScoresToExplain", "10"); // use callers pref if there
                    int iNumScoresToExplain = Integer.parseInt(sProp_iNumScoresToExplain);	// -1 for all
                    // ExplainScoresShowDocWords

                    boolean bUseNuggets = com.indraweb.execution.Session.cfg.getPropBool("UseNuggets");

/*
                    bProp_batch
                    if (bProp_batch)
                        sbBatchStringThisClassify = new StringBuffer();
*/

                    int icfg_classification_DocCountMin = 1;
                    int icfg_classification_SigCountMin = 1;

                    com.indraweb.util.IntContainer intContainer_NumNDSDone_OutParmAccounting = new com.indraweb.util.IntContainer();
                    com.indraweb.util.IntContainer intContainer_NumNodes_OutParmAccounting = new com.indraweb.util.IntContainer();

                    //PipedWriter pipeOut = new BufferedWriter( new PipedWriter() );
                    PipedWriter pipeOut = new PipedWriter();
                    PipedReader pipeIn = new PipedReader(pipeOut);
                    java.io.PrintWriter pwPipe = new java.io.PrintWriter(pipeOut);

/* test for pipe size
                    for ( int i = 0; true; i++ )
                    {
                        if ( i%10 == 0)
                            System.out.println("write : " + (10*i));
                        pwPipe.write("aaaaaaaaaa");
                        if (i == 1000000000)
                            break;
                    }

*/
                    // ********************************
                    // call to TSClassify
                    // ********************************
                    //Log.logClearcr( "ThreadConsumer_ScoreMSResultIURLs call loop indicator t.Name [" + Thread.currentThread().getName() + "] call# [" + iLoop + "] (1-base) TsClassifydoc " +
                    //      " url [" + sURL + "] ");

                    Data_MSAndScoreResult_acrossSEs data_MSAndScoreResult_acrossSEs = ((WorkQManager_ForTSMetasearchNode) super.workQ).getData_MSAndScoreResult();
                    DocForScore docForScore = null;
                    // System.out.println("OPEN URL tCons M [" + sMACHTHREADINFO  + "] N [" + iNodeID + "] open doc [" + sURL + "]");
                    try
                    {
                        //Long LStartPre = (Long) htPendingURLs_strToLongStartTime.get( sURL );
                        //if ( LStartPre != null )
                        //  System.out.println("url already being crawled [" + sURL  + "] starting " +
                        //        UtilProfiling.elapsedTimeMillis(LStartPre.longValue())  + "]"  );

                        //if ( sURL.indexOf("xls") > 0 || sURL.indexOf("vsum") > 0 ) // hbk control 2003 03 04 to stop at a particular URL in debug
                        //  {
                        //    int i = 0;
                        //}
                        //htPendingURLs_strToLongStartTime.put(sURL, new Long ( System.currentTimeMillis() ));
                        if (bVerbose)
                            com.indraweb.util.Log.log("pre sURL for DFS [" + sURL + "]\r\n");

                        docForScore = new DocForScore(
                                sURL,
                                sURL,
                                true,
                                bStemOn,
                                sProp_DocTitle,
                                sProp_DocSummary,
                                bWantPhrasing,
                                iURL,
                                true ,
                                bVerbose,
                                1,
                                new ParseParms ( ParseParms.getDelimitersAll_Default(), false, 21000),
                                null
                        );
                        //com.indraweb.util.UtilFile.addLineToFile("/tmp/temphk.txt", "post sURL for DFS [" + sURL + "]\r\n" );

                    } finally
                    {
                        synchCntDocsAccessed.incrVal(1);
                        if (bVerbose)
                            com.indraweb.util.Log.log("OPENd URL [" + sURL + "] [" + synchCntDocsAccessed.getVal() + "] N [" + iNodeID + "] bFileOpenedOk [" + docForScore.bFileOpenedOk + "]" );
                    }

                    if (bVerbose)
                    {
                        if (!docForScore.bFileOpenedOk || !iURL.getdidURLGetReadOK())
                            System.out.println("node [" + iNodeID + "] " + Thread.currentThread().getName() + " : err in file open [" + sURL + "]");
                        else
                            System.out.println("node [" + iNodeID + "] " + Thread.currentThread().getName() + " : file open OK [" + sURL + "]");
                    }


                    data_MSAndScoreResult_acrossSEs.setDocOpenedStatus(iURL.getURL(), iURL.getdidURLGetReadOK());

                    // api.tsclassify.Data_ScoreTypesAndValueCounts_AlsoClassifiedDocStatusAggs
                    {
                        Connection dbcLocal = null;
                        try
                        {
                            dbcLocal = api.statics.DBConnectionJX.getConnection(sWhichDB, "to call TSClassifyDoc.tsClassifydoc");
                            if (dbcLocal == null)
                                throw new Exception("dbcLocal == null for db [" + sWhichDB + "]");

                            long lTimeStart = System.currentTimeMillis();
                            if ( bVerbose )
                                Log.logcr("ThreadConsumer_ScoreMSResultIURLs CALL tsClassifydoc url [" + sURL + "]");
// commented to avoid compile issues every time args change - deprecated
//                            int iClassResult = TSClassifyDoc.tsClassifydoc
//                                    (
//                                            sURL,
//                                            sURL,
//                                            iProp_RepositoryID,
//                                            sProp_GenreID,
//                                            sProp_DocSummary,
//                                            bProp_post,
//                                            pwPipe,
//                                            //new java.io.PrintWriter(System.out) ,
//                                            dbcLocal,
//                                            -1,
//                                            sProp_DocTitle,
//                                            bWantPhrasing,
//                                            bUseNuggets,
//                                            false, // bProp_batch,
//                                            bProp_DocSumm,
//                                            intContainer_NumNDSDone_OutParmAccounting,
//                                            icfg_classification_DocCountMin,
//                                            icfg_classification_SigCountMin,
//                                            intContainer_NumNodes_OutParmAccounting,
//                                            sProp_Corpora,
//                                            sProp_NodeSet,
//                                            false, // bDelClassifiedFile,
//                                            sURL,
//                                            bStemOn,
//                                            bExplainScores,
//                                            sProp_ExplainScoresShowDocWords,
//                                            docForScore,
//                                            true, // bShortOutput mode
//                                            data_MSAndScoreResult_acrossSEs,
//                                            false, // don't delete pre exsting node doc rels - don't bother since this will insert or update
//                                            null,
//                                            null,
//                                            false, // use fulltext column
//                                            u,
//                                            bVerbose,
//                                            false,
//                                            -1,
//                                            false,
//                                            null,
//                                            SignatureParms.getNumParentTitleTermsAsSigs_Default()
//                                    );
                            iNumClassifiedCounter_imperfect++;
                            if ( iNumClassifiedCounter_imperfect % 100 == 0 )
                            {
                                System.out.println("iNumClassifiedCounter_imperfect [" + iNumClassifiedCounter_imperfect+ "] sProp_NodeSet [" + sProp_NodeSet  + "]");
                            }

                            long lDur = System.currentTimeMillis() - lTimeStart;
                            if ( bVerbose )
                                Log.logcr("RTN FROM CALL tsClassifydoc lDur [" + lDur  + "]");
                        } finally
                        {
                            dbcLocal = api.statics.DBConnectionJX.freeConnection(dbcLocal, sWhichDB, "done with classifydoc");
                        }
                    }
/*
                        WorkQManager_ForTSMetasearchNode workQManager_ForRefreshNode = (WorkQManager_ForTSMetasearchNode) super.workQ;
                    workQManager_ForRefreshNode.getData_MSAndScoreResult().addData_TSClassifyResultData( data_FromOneTSClassify );
*/

                    pipeOut.flush();

                    java.io.BufferedReader br = new java.io.BufferedReader(pipeIn);
                    // parse Classify XML results
                    StringBuffer sbXML = new StringBuffer();
                    while (br.ready())
                    {
                        String sLine = br.readLine();
                        sbXML.append(sLine);
                        //Log.logClearcr(Thread.currentThread().getName() + " XML line [" + sLine + "]");
                    }
                    pwPipe.close();
                    br.close();
                    pipeIn.close();
                    pipeOut.close();
                    //System.out.println(sbXML.toString());
                    //workQ.addResultData( 1, (double) iNumShown );

                } catch (Exception e)
                {
                    Log.NonFatalError("err 4 in ThreadConsumer_ScoreMSResultIURLs ", e);
                }
            } // while
            if ( bVerbose )
                Log.logcr("no more URLS - thread ThreadConsumer_ScoreMSResultIURLs [" + sDesc + "] done - ");
        } catch (Exception e)
        {
            Log.NonFatalError("err 5 in ThreadConsumer_ScoreMSResultIURLs ", e);
        }
    }
}



