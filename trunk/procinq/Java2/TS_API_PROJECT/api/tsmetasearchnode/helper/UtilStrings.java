package api.tsmetasearchnode.helper;

import java.util.*;
import java.io.*;

public class UtilStrings
{
	
	/**
	 * trims a string - including the degenerate case of 
	 * only spaces and tabs (returns "") which regular String.trim() does not
	 */

	// **********************************************
	public static String padString ( String s, int len ) 
	// **********************************************
	{
		int delta = len - s.length();
		if ( delta <= 0 )
			return s;
		char[] cArr = new char[ delta ];
		for ( int i = 0; i < delta; i++ )
			cArr [ i ] = ' ';
		
		String pad =  new String ( cArr );
		String rtn = s + pad;
		return rtn;	
	}

	// As per the PERL function, split a string into a vector of strings given a delimiter
	public static Vector Split (String s, String splitterDelimiters ) {
		Vector v = new Vector();
		StringTokenizer st = new StringTokenizer ( s, splitterDelimiters );
		
		String ss = null;
		while ( st.hasMoreElements() )
		{
			ss = (String) st.nextElement();	
			v.addElement( ss );	
		} 
		return v;
	}
	
	// **********************************************
	public static String myTrimFunnySpaces (String s) 
	// **********************************************
	{

/*		Character l;
		l.
		
		
		String test1 = "\t";
		char[] test1c = test1.toCharArray();
		test1c[10].
		int unicodeNumtest1 = Character.get (test1c[0]);
		
		String test2 = " ";
		char[] test2c = test2.toCharArray();
		int unicodeNumtest2 = Character.getNumericValue (test2c[0]);
		
		
*/		
		
		String trimmed = s.trim();
		char [] x = trimmed.toCharArray();

		int startKeep = 0;
		int i;
		for (i = 0; i < x.length; i ++) 
		{ 
			
			int unicodeNum = Character.getNumericValue (x[i]);
			if ( unicodeNum > -1 ) {
			
			//if (unicodeNum != 160 && unicodeNum != '\t' || unicodeNum != ' ') { 
				break;	
			} 
		} 
		startKeep = i;

		int endKeep = x.length	- 1;
		for (i = x.length-1; i >= startKeep; i--) { 
			if (x[i] != 160 && x[i] != '\t' || x[i] != ' ') { 
				break;	
			} 
		} 
		

		char[] keeper = new char[ (endKeep - startKeep) + 1];
		for (i = startKeep; i < endKeep+1; i++){
			char c = x[i];
			keeper[i - startKeep] = c ;
		}
		return new String ( keeper );

	
/* commented 2000 01 29 
	previous whole routine 
		String trimmed = s.trim();
		char [] x = trimmed.toCharArray();
		for (int i = 0; i < x.length; i ++) { 
			if (x[i] != 160 && x[i] != '\t') { 
				return trimmed;	
			} 
		} 
		return "";	
	*/
	}
	
	// **********************************************
	public static String myTrimSpaceCompress  (String s) 
	// **********************************************
	{
		
		s = myTrimFunnySpaces(s); // leading and trailing spaces		
		s = replaceStrInStr (s, "\t", " ");		
		s = replaceStrInStr (s, "  ", " ");		
		s = replaceStrInStr (s, " ,", ",");		
		s = replaceStrInStr (s, ", ", ",");		
		
		return s;
	}

	
	

	// **********************************************
	public static String getStringBetweenThisAndThat (String s, 
	// **********************************************
													  String s1, 
													  String s2 
													  )
	{ 
		return getStringBetweenThisAndThat (s, s1, 1, s2, 1 );
	}
	
	
	// **********************************************
	public static String getStringBetweenThisAndThat (String s, 
	// **********************************************
													  String s1, 
													  int index1, 
													  String s2, 
													  int index2 ) 
	{ 
		String returnStr = null;	

		int startLoc = 0;
		if ( index1 > 0 ) {
			startLoc = getLocationOfNthOfThese (s, s1, index1 ) + 1 ;
			if (startLoc == -1) { 
				//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
				return "";    // EARLY RETURN!!!!!!!!!!!!!
			}
		} else {
			startLoc = -1;
		} 

		if ( index2 > 0 ) { 
			int endLoc = 0;
			int totalLen = startLoc+s1.length();
			if (totalLen == -1) {
				//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
				return "";
			}

			endLoc = getLocationOfNthOfThese (s, s2, index2 ) ;
			if (endLoc == -1) {
				//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
				returnStr = s.substring ( startLoc + s1.length()-1 );			
			}
			else
			{
				try {
					returnStr = s.substring ( startLoc + s1.length()-1, endLoc );			
				} 
				catch ( Exception e ) 
				{
					//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + "");
					returnStr = "";	// EARLY RETURN!!!!!!!!!!!
					
				}
			} 
		} else {
			returnStr = s.substring ( startLoc+1, s1.length());			
		} 
		
		//HKonLib.o (s+","+s1+","+index1+","+s2+","+index2+" ==> " + returnStr);
		
		return returnStr; 	
		/* test cases : 
		// String s = "abcdefghijklmnopqrstuvwxyza23defghijklmnopqrstuvwxyz";
		
		not working : need a non zero len string to do start , 0 
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 1);
		HKonLib.getStringBetweenThisAndThat (s, "e", 2, "n", 1);
		HKonLib.getStringBetweenThisAndThat (s, "a", 0, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "a", 1, "e", 1);
		HKonLib.getStringBetweenThisAndThat (s, "b", 1, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 1, "", 0);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 5, "e", 2);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 5);
		HKonLib.getStringBetweenThisAndThat (s, "b", 2, "e", 0);
		
		test getting a comment field's text - from text= to end ... how 
		*/
		
		
		
		
		
		
	} 
	// **********************************************
	public static String getStrAfterThisToEnd1Based (String s, 
	// **********************************************
									  		   String s1, 
											   int index1 
	)	
	{ 
		String returnStr = null;	

		int startLoc = 0;
		if ( index1 > 0 ) {
			startLoc = getLocationOfNthOfThese ( s, s1, index1 ) + s1.length ();
			if (startLoc == -1) { 
				return "";    // EARLY RETURN!!!!!!!!!!!!!
			}
		} else {
			startLoc = -1;
		} 

		returnStr = s.substring ( startLoc );			
		
		return returnStr; 	
		
		//test getting a comment field's text - from text= to end 
	} 

	// **********************************************
	public static String getStrStartingWithThisToEnd1Based (String s, 
	// **********************************************
									  		   String s1, 
											   int index1 
	)	
	{ 
		String returnStr = null;	

		int startLoc = 0;
		if ( index1 > 0 ) {
			startLoc = getLocationOfNthOfThese ( s, s1, index1 ) + s1.length ();
			if (startLoc == -1) { 
				return "";    // EARLY RETURN!!!!!!!!!!!!!
			}
		} else {
			startLoc = -1;
		} 

		returnStr = s.substring ( startLoc - s1.length() );			
		
		return returnStr; 	
		
	} 


	// **********************************************
	public static String getsUpToNthOfThis_1based (String s, 
	// **********************************************
											String s1, 
											int index1) 
	{ 
		int endLoc = getLocationOfNthOfThese (s, s1, index1 ) ;
		if (endLoc == -1) { 
			return s;    // EARLY RETURN!!!!!!!!!!!!!
		}
		else {
			String retStr =  s.substring (0, endLoc);
			return retStr;
		}
	} 
	
	
	// **********************************************
	public static int getLocationOfNthOfThese (String s, String s1, int index1 ) 
	// **********************************************
	{ 
		int currentPointer = -1;
		int incrementAmount = 0;

		for (int i = 0; i < index1; i++ ) {
			String curString = s.substring( currentPointer + 1 );
			incrementAmount = curString.indexOf ( s1 );
			
			if ( incrementAmount == -1 ) 
				return -1;
			else 
				currentPointer += incrementAmount + 1;
		} 
		
		return currentPointer; 
	
	} 
	
	// **********************************************
	public static boolean getStrContains (String s, String s1 ) 
	// **********************************************
	{ 
			if ( s.indexOf ( s1 ) >= 0) 
			{ 
				return true;	
			} else
				return false;
	} 
	
	// **********************************************
	public static boolean getStrContainsIgnoreCase (String s, String s1 ) 
	// **********************************************
	{ 
			if ( s.toLowerCase().indexOf ( s1.toLowerCase() ) >= 0) 
			{ 
				return true;	
			} else
				return false;
	} 
	
	public static int CONST_CompareEquals = 1;
	public static int CONST_CompareStartsWith = 2;
	public static int CONST_CompareContains = 3;
	
	// **********************************************
	public static boolean myCompare ( String s1, String s2, int compareMode )
	// **********************************************
	{
		if ( compareMode == CONST_CompareEquals ) 
			return ( s1.equals ( s2 ) );
			
		else if ( compareMode == CONST_CompareStartsWith ) 
			return ( s1.startsWith ( s2 ) );
		
		else if ( compareMode == CONST_CompareContains ) 
			return ( UtilStrings.getStrContains ( s1, s2 ) );
		
		return false;
		
		
	} 
   
	// **********************************************
	public static String replaceWipeAllChardInStr ( String s, String repl, char with) 
	// **********************************************
	{
		char[] carr = repl.toCharArray();

		for (int i = 0; i < carr.length; i++)
		{
			s = s.replace (	carr[i], with) ;
		}  	
		return s;
	}	

	// **********************************************
	public static String replaceStrInStr (String s, String replace, String with)
	// **********************************************
	{
		int dontReplaceInsideReplacedStringCounter = 0;
		try 
		{
			while (true) 
			{
				// don't recurse infinitely
				String testStr = s.substring( dontReplaceInsideReplacedStringCounter ) ;
				int replaceableLoc = testStr.indexOf ( replace );
				if ( replaceableLoc >= 0 ) 	
				{	
					
					//if ( replaceableLoc < testStr.length() )
					//{
						String x1 = s.substring ( 0, replaceableLoc + dontReplaceInsideReplacedStringCounter );
						String x2 = s.substring ( dontReplaceInsideReplacedStringCounter + replaceableLoc + replace.length());
						s =  x1 + with + x2;
					//}
					//else
					//{
						//s =  s.substring ( 0, replaceableLoc + dontReplaceInsideReplacedStringCounter ) + 
							//with 
						//;
					//}

					dontReplaceInsideReplacedStringCounter += (replaceableLoc + with.length()  );
					//dontReplaceInsideReplacedStringCounter += (replaceableLoc + replace.length() + 1 );
				} 
				else  
				{
					return s;
				} 
			} 
		} 
		catch ( Exception e ) 
		{
			System.out.println ("replaceStrInStr" + s + "," + replace + "," + with);	
		} 
		return null;
		
	} 
	

	// **********************************************
	public static String replaceStrBetweenThisAndThat (String sFileLine, 
	// **********************************************
													   String sFirstToken, 
													   String sSecondToken, 
													   String sNewValue,
													   boolean fatalIfTokensNotFound) 	
	{
		int iLocFirstToken  = sFileLine.indexOf ( sFirstToken );
		int iLocSecondToken = sFileLine.indexOf ( sSecondToken );
				
		String sPreValue = sFileLine.substring ( 0, iLocFirstToken + 1);
		String sPostValue = sFileLine.substring ( iLocSecondToken );
		String sFinal = sPreValue + sNewValue + sPostValue;
		return sFinal;
	} 
	
	
	
	// **********************************************
	public static String replaceStrInStrSpacesShrinkerToOne (String s )
	// **********************************************
	{
		String replace = "  ";
		String with  = " ";
		do 
		{
			int dontReplaceInsideReplacedStringCounter = 0;
			while (true) 
			{
				// don't recurse infinitely
				String testStr = s.substring( dontReplaceInsideReplacedStringCounter ) ;
				int replaceableLoc = testStr.indexOf ( replace );
				if ( replaceableLoc >= 0 ) 	
				{	
					
					s =  s.substring ( 0, replaceableLoc + dontReplaceInsideReplacedStringCounter ) + 
						 with + 
						 s.substring ( dontReplaceInsideReplacedStringCounter + replaceableLoc + replace.length() 
					);
					dontReplaceInsideReplacedStringCounter += (replaceableLoc + replace.length() + 1);
					if ( dontReplaceInsideReplacedStringCounter > s.length() )
						break;
				} 
				else  
				{
						break;
				} 
			} 
		}
		while ( s.indexOf ( replace ) >= 0 );
		return s;
	} 

	
	// **********************************************
	public static Vector split ( String s, String splitter )
	// **********************************************
	{
		Vector v = new Vector();
		
		while ( true )
		{
			int endLoc = s.indexOf ( splitter ) ;
			if (endLoc == -1) 
			{
				v.addElement( s );	
				break; // last element
			} 
			else
			{
				v.addElement( s.substring ( 0, endLoc ) );	
				s = s.substring ( endLoc + 1  ) ;
			} 
					
		} 
		return v;
	}
	// ************************************************************************
	public static String numFormatInt ( int i, int lenDesired )
	// ************************************************************************
	{
		String sI = Integer.toString ( i ) ;
		String prepend = "";
		if ( sI.length() < lenDesired) 
		{
			for ( int j = 0; j < lenDesired - sI.length(); j++ )	
			{
				 prepend = prepend + "0";
			} 
			
		} 
		return prepend + sI;
	} 

	
	// ************************************************************************
	public static String numFormatLong ( long i, int lenDesired )
	// ************************************************************************
	{
		String sI = Long.toString ( i ) ;
		String prepend = "";
		if ( sI.length() < lenDesired) 
		{
			for ( int j = 0; j < lenDesired - sI.length(); j++ )	
			{
				 prepend = prepend + "0";
			} 
			
		} 
		return prepend + sI;
	} 
	// ************************************************************************
	public static String numFormatDouble	 ( double dnum, int digitsRightOfDecimal )
	// ************************************************************************
	{
		String s = Double.toString ( dnum );
		
		//if ( s.equals ("100.0" ))
		//{
			//int debugme;
		//}
		
		int iDecLoc = UtilStrings.getLocationOfNthOfThese (s, ".", 1);
		
		String s1 = s;
		String decRight = "";

		try 
		{
			s1 = s.substring( 0, iDecLoc );
		
			decRight = null;
			int slen = s.length();
			int s1len = s1.length();
			if (  slen - s1len < digitsRightOfDecimal +1 )
				return s;
			
			decRight = s.substring( iDecLoc ).substring(0, digitsRightOfDecimal+1 );
		} 
		catch ( Exception e ) 
		{
			return "0";
		} 
		return s1 + decRight;
	} 
	
	// **********************************************
	public static boolean isNumericInteger ( String s ) 
	// **********************************************
	{
		if ( s.indexOf ( "." ) > -1 ) 	
		{
			return false;	
		} 
		try 
		{
			int i = Integer.parseInt ( s ) ;	
		} 
		catch ( NumberFormatException e ) 
		{
			return false;	
		} 
		return true;
	}
	
	
	// **********************************************
	public static String stringToOneline ( String s, char filler )
	// **********************************************
	{
		char[] cArr  = s.toCharArray ();
		
		for ( int i = 0; i < cArr.length; i++)
		{
			if ( (int) cArr [ i ] == 13 )
				cArr [ i ] = filler;
			if ( (int) cArr [ i ] == 10 )
				cArr [ i ] = filler;
		}	
		
		String s2 = new String ( cArr );
		s2 = replaceStrInStr ( s2, "  ", " ") ;
		s2 = replaceWipeAllChardInStr ( s2, "\t", '#') ;
		
		return s2;
	} 

	// **********************************************
	public static String cleanAndStripEGTitleTextToLower ( String s, String sCharsToStrip_ )	
	// **********************************************
	{
		String sFinal = UtilStrings.replaceWipeAllChardInStr 
		( s, sCharsToStrip_, ' ').toLowerCase().trim();
		return sFinal;
	} 

	
	// **********************************************
	public static int countNumOccurrencesOfStringInString ( String s, String substr )
	// **********************************************
	{
		int iNumOccur = 0;
		int iNextCutPoint = -1;
		while ( true ) 
		{
			iNextCutPoint = s.indexOf ( substr ); 
			if ( iNextCutPoint >= 0 )
			{
				iNumOccur++;
				s = s.substring ( iNextCutPoint + 1 ) ; //
			}
			else
				break;
		}
		return iNumOccur;
	} 
	
	// **********************************************
	public static String commaReverse ( String s )
	// **********************************************
	{
		int iCountNumCommas = countNumOccurrencesOfStringInString ( s, "," );
		if ( iCountNumCommas == 1 )
		{
			int iCommaLoc = s.indexOf ( "," );
			String sPreComma = s.substring ( 0, iCommaLoc );
			String sPostComma = s.substring ( iCommaLoc + 1 );
			s = sPostComma + " " + sPreComma;
		}
		return s;
	} 
	
	
	// **********************************************
	public static String getStringDelimitedBy_fromStringArray ( String sArr[], String sDelim )
	// **********************************************
	{
		if ( sArr.length == 0 )
			return "";
		
		StringBuffer sb = new 		StringBuffer();
		
		for ( int i = 0; i < sArr.length-1; i++ )
		{
			sb.append ( sArr[i] + sDelim );	
		} 
		sb.append ( sArr[sArr.length-1]  );	
		return sb.toString();
	}
	
	
	// **********************************************
	public static boolean areStrArraysSame ( String[] s1Arr, String[] s2Arr )
	// **********************************************
	{
		if ( s1Arr == null || s2Arr == null )
			return false;
		
		if ( s1Arr.length != s2Arr.length )
		{
			return false;
		}
		else 
		{
			for ( int i = 0 ; i < s1Arr.length; i++ )
			{
				if ( !s1Arr[i].equals ( s2Arr[i] ) )	
					return false;
			} 
		}
		return true;
	}
	
	// **********************************************
	public static boolean isThisStrArrayContained_InThisOrder_WithinThatStrArr ( String[] a1, String[] a2 )
	// **********************************************
	{
		if ( a1 == null || a2 == null )
			return false;
		
		for ( int i = 0; i < a2.length; i++ )
		{
			if ( a1[0].equals (a2[i]) )
			{
				boolean bFoundMatch = isthisArraythe_StartOfThatArray ( a1, 0, a2, i );
				if ( bFoundMatch )
					return true;
			} 
		} 
		return false;
	}
	
	// **********************************************
	public static boolean isthisArraythe_StartOfThatArray ( String[] a1, int iA1offset, String[] a2, int iA2offset )
	// **********************************************
	{
		for ( int i = 0; ; i++ )
		{
			if ( i + iA1offset == a1.length ) 
				return true;
			if ( i + iA1offset >= a1.length || i + iA2offset >=	 a2.length )
				return false;
			if ( !a1 [i + iA1offset].equals ( a2 [i + iA2offset] ) )
				return false;
		} 
	}

	public static String getAllAfterLastOfThis	
		( 
			String s, 
			String s1 
		) 
	{
		int iLocationLast = s.lastIndexOf ( s1 );
		if ( iLocationLast < 0 )
			return "";
		
		String s3 = s.substring ( iLocationLast + 1	);
		return s3;
		
	} 
	
	// **********************************************
	public static String GetPArmFromStructuredString (String sFormattedString, String sParmName, boolean warningNotFound )  
	// **********************************************
	{
		int foundat = sFormattedString.indexOf(sParmName);

		String parmsectionandafter = sFormattedString.substring(foundat);
		String s = null;
		if (true)
			s = parmsectionandafter.substring (parmsectionandafter.indexOf("[")+1, parmsectionandafter.indexOf("]")) ;
		else
			s = "";
		return s;
	}
	
}
