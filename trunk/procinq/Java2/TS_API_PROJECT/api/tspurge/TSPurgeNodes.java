package api.tspurge;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 * Purge node objects from the system that were previously marked for deletion.  This purge is permanent and not
 *  recoverable without a full database restoration from Oracle.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tspurge.TSPurgeNodes&SKEY=-132981656

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>All objects were purged successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSPurgeNodes
{
	// Purge all nodes marked for deletion
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);
	
		try {
			// for authorization purposes, need to know the corpus id of this node
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
			
			String sSQL = "delete from Node where NodeStatus = -1";
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>An error has occured while deleting nodes.</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NODE_NOT_FOUND);
			}

		    stmt.close();
            out.println("<SUCCESS>All objects were purged successfully.</SUCCESS>");
		}
		
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
	}
}
