package api.wikipedia;

/**
 *
 * @author andres
 */
public class WikipediaCategory {

    private String corpusId;
    private String corpusName;
    private String corpusActivate;

    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }

    public String getCorpusName() {
        return corpusName;
    }

    public void setCorpusName(String corpusName) {
        this.corpusName = corpusName;
    }

    public String getCorpusActivate() {
        return corpusActivate;
    }

    public void setCorpusActivate(String corpusActivate) {
        this.corpusActivate = corpusActivate;
    }
}
