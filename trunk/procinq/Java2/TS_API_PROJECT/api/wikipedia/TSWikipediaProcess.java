package api.wikipedia;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.wikipedia.mq.MessageWiki;
import api.wikipedia.mq.Producer;
import com.iw.db.ConnectionFactory;
import com.npstrandberg.simplemq.MessageQueue;
import com.npstrandberg.simplemq.MessageQueueService;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * TODO: this method not have security. get a corpus info to create the
 * structure in oracle with the wikipedia info
 *
 * @author andres
 */
public class TSWikipediaProcess {

    private static final Logger log = Logger.getLogger(TSWikipediaProcess.class);
    private static final String PROCESS_WAITING = "WAITING";
    private static final String DEFAULT_QUEUE_NAME = "WIKIPEDIA";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.info("TSWikipediaProcess called");
        //validate AUTHORIZED
        //String sKey = (String) props.get("SKEY", true);
        String startPoint = (String) props.get("startpoint");
        String taxonomy = (String) props.get("taxonomy");
        String depth = (String) props.get("depth");

        //validate the parameters
        if (startPoint == null || taxonomy == null || depth == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (startPoint.isEmpty() || taxonomy.isEmpty() || depth.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        //save the process status in oracle
        Map<String, String> parameters = new HashMap<String, String>();
        Date dateTime = new Date();
        parameters.put("TIMESTAMP", dateTime.getTime() + "");
        parameters.put("WIKISTARTPOINT", startPoint);
        parameters.put("CORPUS", taxonomy);
        parameters.put("DEPTH", depth);
        String requestId = "";
        try {
            requestId = saveProcessStatus(parameters);
        } catch (Exception e) {
            log.error("Error saving process status in WAITING");
            throw e;
        }
        //send data to the queue
        MessageWiki messageWiki = new MessageWiki();
        messageWiki.setRequestId(requestId);
        messageWiki.setCategoryName(startPoint);
        messageWiki.setCorpusName(taxonomy);
        messageWiki.setDepth(depth);
        Producer producer = Producer.getInstance();
        MessageQueue queue = MessageQueueService.getMessageQueue(DEFAULT_QUEUE_NAME);
        producer.setMessageQueue(queue);//pass the queuse
        producer.sendMessage(messageWiki);
        out.println("<SUCCESS>process added ok</SUCCESS>");
    }

    private static String saveProcessStatus(Map<String, String> fields) throws Exception {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: saveProcessStatus(.. Error getting connection from oracle");
        }
        String sql = "INSERT INTO SBOOKS.WIKIPEDIAREPORT (REQUESTID,TIMESTAMP,WIKISTARTPOINT,"
                + "CORPUS,DEPTH,MESSAGES,STATUS) VALUES (SEC_WIKIPEDIAREPORT.nextval,?,?,?,?,?,?)";
        PreparedStatement pstmt = null;
        String key = "";
        ResultSet generatedKeys = null;
        try {
            pstmt = conn.prepareStatement(sql, new String[]{"REQUESTID"});
            pstmt.setLong(1, Long.parseLong(fields.get("TIMESTAMP")));
            pstmt.setString(2, fields.get("WIKISTARTPOINT"));
            pstmt.setString(3, fields.get("CORPUS"));
            pstmt.setString(4, fields.get("DEPTH"));
            String messages = fields.get("MESSAGES") == null ? "" : fields.get("MESSAGES");
            pstmt.setString(5, messages);
            pstmt.setString(6, PROCESS_WAITING);
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating wikipediaProcess failed");
            }
            generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                key = String.valueOf(generatedKeys.getLong(1));
            } else {
                throw new SQLException("Creating wikipediaProcess failed, no generated key obtained.");
            }
            return key;
        } catch (Exception e) {
            log.error("Error in the process status in wikipedia", e);
            throw new DAOException("Error in the process status in wikipedia", e);
        } finally {
            try {
                if (generatedKeys != null) {
                    generatedKeys.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
