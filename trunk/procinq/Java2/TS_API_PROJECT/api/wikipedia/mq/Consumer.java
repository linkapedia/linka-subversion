package api.wikipedia.mq;

import api.util.ConfServer;
import api.wikipedia.Conector;
import api.wikipedia.WikipediaBot;
import api.wikipedia.WikipediaConector;
import api.wikipedia.WikipediaDao;
import api.wikipedia.WikipediaOracleDAO;
import api.wikipedia.WikipediaWorker;
import com.npstrandberg.simplemq.Message;
import com.npstrandberg.simplemq.MessageQueue;

/**
 *
 * @author andres
 */
public class Consumer implements Runnable {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Consumer.class);
    private static Consumer instance = null;
    private boolean currentWork = false;
    private int millis = 5000;
    private MessageQueue messageQueue;

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    protected Consumer() {
        // singleton
    }

    public static Consumer getInstance() {
        if (instance == null) {
            instance = new Consumer();
        }
        return instance;
    }

    public void run() {
        if (currentWork) {
            throw new IllegalStateException("The app not support multiple consumers right now");
        }
        currentWork = true;
        WikipediaDao dao = new WikipediaOracleDAO();
        Conector con = new WikipediaConector(ConfServer.getValue("wikipedia.crawl.api"));
        WikipediaBot bot = new WikipediaBot("", "", con);
        WikipediaWorker worker;
        MessageWiki ma = new MessageWiki();
        Message m;
        while (true) {
            try {
                m = messageQueue.receiveAndDelete();
                if (m != null) {
                    ma = (MessageWiki) m.getObject();
                    //read the objet and start the process
                    worker = new WikipediaWorker();
                    worker.setBot(bot);
                    worker.setDao(dao);
                    worker.start(ma);
                }
            } catch (Exception e) {
                //error in the process a message
                log.error("Error procecing data from the queue requestId:" + ma.getRequestId(), e);
            }
            try {
                //TODO: manage the millis with a logic exponencial
                Thread.sleep(millis);
            } catch (InterruptedException ex) {
                log.error("THE WIKIPEDIA CONSUMER IS DEAD, InterruptedException", ex);
            }
        }
    }
}
