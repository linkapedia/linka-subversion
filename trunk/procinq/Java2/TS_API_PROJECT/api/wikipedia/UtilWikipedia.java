package api.wikipedia;

import info.bliki.wiki.filter.PlainTextConverter;
import info.bliki.wiki.model.Configuration;
import info.bliki.wiki.model.WikiModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

/**
 * class utility to filter source and another task as normalize titles etc
 *
 * @author andres
 */
public class UtilWikipedia {

    private static final Logger log = Logger.getLogger(UtilWikipedia.class);
    private static final String[] filterCharacters = new String[]{"{", "}"};
    private static final String[] filterReplacement = new String[]{" ", " "};


    /* remove data recursively between openChar and closeChar
     *
     * @param source
     * @param openChar
     * @param closeChar
     * @return
     */
    private static String removeUnUsedCharacter(String source, String openChar, String closeChar) {
        log.debug("remoneUnUsedCharacter");
        StringBuilder sb = new StringBuilder();
        sb.append(source);
        Stack<Integer> stack = new Stack<Integer>();
        int index_open = sb.indexOf(openChar);
        if (index_open == -1) {
            return null;
        }
        int index_close = 0;
        stack.push(index_open);
        index_open = sb.indexOf(openChar, index_open + 1);
        while (!stack.isEmpty()) {
            if (index_open > -1) {
                stack.push(index_open);
                index_open = sb.indexOf(openChar, index_open + 1);
            } else {
                int index = stack.pop();
                index_close = sb.indexOf(closeChar, index);
                if (index_close == -1) {
                    continue;
                }
                sb.delete(index, index_close + 1);
            }
        }
        return sb.toString();
    }

    /**
     *
     * @param source
     * @return
     */
    private static List<String> getLinksFromSource(String source) {
        log.debug("getLinksFromSource");
        List<String> links = new ArrayList<String>();
        Pattern pa;
        Matcher ma;
        String link = "";
        pa = Pattern.compile("(\\[\\[(.*?)]])");
        ma = pa.matcher(source);
        while (ma.find()) {
            link = ma.group(2);
            if (link.contains("|")) {
                link = link.substring(link.lastIndexOf("|") + 1, link.length());
            }
            if ((!link.isEmpty()) && (!link.contains("Category:"))) {
                links.add(link);
            }
        }
        return links;
    }

    /**
     *
     * @param source
     * @return
     */
    public static String getSourceNormalized(String source) {
        if (source == null) {
            return null;
        }
        log.debug("filerSourceWithLinks");
        String sourceFilter = source;
        //get links from the source
        List<String> linksWiki;
        linksWiki = getLinksFromSource(sourceFilter);
        StringBuilder linkedSection = new StringBuilder();
        if (!linksWiki.isEmpty()) {
            linkedSection.append("<LinkedSection>");
            linkedSection.append(StringUtils.join(linksWiki.toArray(), "|"));
            linkedSection.append("</LinkedSection>");
        }
        //parse wiki format to test/plain
        WikiModel wikiModel = new WikiModel(
                Configuration.DEFAULT_CONFIGURATION, Locale.ENGLISH,
                "${image}", "${title}");
        wikiModel.setUp();
        sourceFilter = wikiModel.render(new PlainTextConverter(), sourceFilter);
        sourceFilter = removeUnUsedCharacter(sourceFilter, "{", "}"); //remove data into this open and close characters
        //unescape html encodes
        sourceFilter = StringEscapeUtils.unescapeHtml4(sourceFilter);
        //remove bad characters
        sourceFilter = StringUtils.replaceEach(sourceFilter, filterCharacters, filterReplacement);
        if (sourceFilter != null) {
            sourceFilter = sourceFilter.trim();
            sourceFilter += linkedSection;
            if (sourceFilter.isEmpty()) {
                sourceFilter = null;
            }
        }
        return sourceFilter;
    }

    /**
     *
     * @param title
     * @return
     */
    public static String getTitleNormalized(String title) {
        return title.replace("Category:", "");
    }
}
