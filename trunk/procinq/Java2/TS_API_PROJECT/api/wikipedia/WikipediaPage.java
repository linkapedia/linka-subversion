package api.wikipedia;

/**
 *
 * @author andres
 */
public class WikipediaPage {

    private String pageId;
    private String nodeParentId;
    private String nodeTitle;
    private String nodeDesc;
    private String nodeSize;
    private String depthFromRoot;
    private String corpusId;

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getNodeParentId() {
        return nodeParentId;
    }

    public void setNodeParentId(String nodeParentId) {
        this.nodeParentId = nodeParentId;
    }

    public String getNodeTitle() {
        return nodeTitle;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }

    public String getNodeDesc() {
        return nodeDesc;
    }

    public void setNodeDesc(String nodeDesc) {
        this.nodeDesc = nodeDesc;
    }

    public String getNodeSize() {
        return nodeSize;
    }

    public void setNodeSize(String nodeSize) {
        this.nodeSize = nodeSize;
    }

    public String getDepthFromRoot() {
        return depthFromRoot;
    }

    public void setDepthFromRoot(String depthFromRoot) {
        this.depthFromRoot = depthFromRoot;
    }

    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }
}
