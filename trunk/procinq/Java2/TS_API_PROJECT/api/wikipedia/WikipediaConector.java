package api.wikipedia;

import api.util.ConfServer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class WikipediaConector implements Conector {

    private static final Logger log = Logger.getLogger(WikipediaConector.class);
    private String urlBase;
    private static final String USER_AGENT = "linkapediaNiceCrawl/1.0 (http://www.linkapedia.com/; andres.restrepo@linkapedia.com)";
    private final int READ_TIMEOUT = 30000;
    private final int CONNECTION_TIMEOUT = 30000;
    private Map<String, String> categoryAlready; // to control the infinite loop
    //not include topics with this names
    private static List<String> notTopicNames;
    private static List<String> notTopicsNamesStartAt;

    static {
        try {
            notTopicNames = new ArrayList<String>(
                    Arrays.asList(ConfServer.getValue("wikipedia.crawl.default.configuration.suppress.topics").split(",")));
            notTopicsNamesStartAt = new ArrayList<String>(
                    Arrays.asList(ConfServer.getValue("wikipedia.crawl.default.configuration.suppress.topics.startat").split(",")));
        } catch (Exception e) {
            log.error("Error getting wikipedia.crawl.default.configuration.suppress.topics", e);
            notTopicNames = new ArrayList<String>();
        }
    }

    public WikipediaConector(String urlBase) {
        this.urlBase = urlBase;
        categoryAlready = new HashMap<String, String>();
    }

    public String getUrlBase() {
        return urlBase;
    }

    public void setUrlBase(String urlBase) {
        this.urlBase = urlBase;
    }

    /**
     * get the category members from a category id
     *
     * @param categoryId
     * @param chunk
     * @param max
     * @param type
     * @return
     * @throws ConectorException
     */
    public LinkedHashMap<String, String> getCategoryMembers(String categoryId, int chunk, int max, String type) throws ConectorException {
        log.debug("WikipediaConector: getCategoryMembers(..");
        URL urlConnection;
        //validate parameters
        if (!type.equals("page") && !type.equals("subcat")) {
            throw new InvalidParameterException("WikipediaConector: getCategoryMembers(.. Error type only accept: page or subpage");
        }
        if (chunk < 1) {
            throw new InvalidParameterException("WikipediaConector: getSource(.. chunk can't be < 1");
        }

        //build the map with the parameters
        Map<String, String> map = new HashMap<String, String>();
        map.put("action", "query");
        map.put("cmtype", type);
        map.put("list", "categorymembers");
        map.put("format", "json");
        map.put("cmlimit", String.valueOf(chunk));
        map.put("cmpageid", categoryId);
        try {
            urlConnection = getUrl(map);
        } catch (MalformedURLException ex) {
            throw new ConectorException("WikipediaConector: getCategoryMembers(.. Malformed Url", ex);
        }
        JSONObject retrieveData = null;
        JSONObject queryContinue = null;
        LinkedHashMap<String, String> categoriesMembers = new LinkedHashMap<String, String>();
        while (true) {
            try {
                retrieveData = getObjectFromRequest(getClient(urlConnection));
            } catch (IOException ex) {
                log.error("WikipediaConector: getCategoryMembers(.. Error IOException ", ex);
                throw new ConectorException("WikipediaConector: getCategoryMembers(.. IOException", ex);
            }
            JSONArray queryMembers;
            try {
                queryContinue = retrieveData.getJSONObject("continue");
                queryMembers = retrieveData.getJSONObject("query").getJSONArray("categorymembers");
                if (queryMembers == null) {
                    log.error("Error in the response" + urlConnection);
                    break;
                }
            } catch (Exception e) {
                throw new ConectorException("WikipediaConector: getCategoryMembers(.. Error parsing data", e);
            }
            String title;
            String id;
            for (Object obj : queryMembers) {
                JSONObject objAux = (JSONObject) obj;
                //filter the node
                id = objAux.getString("pageid");
                title = objAux.getString("title");
                boolean filter1 = filterNotTopics(title);
                boolean filter2 = filterNotTopicsStartAt(title);
                if (filter1 && filter2) {
                    if (type.equals("subcat")) {
                        //validate for not get a infinite loop
                        String idAux = categoryAlready.get(id);
                        if (idAux == null) {
                            categoriesMembers.put(id, title);
                            categoryAlready.put(idAux, idAux);
                        }
                    } else {
                        categoriesMembers.put(id, title);
                    }
                }
            }
            if (queryContinue != null && !queryContinue.isEmpty()) {
                //get parameter categorymembers - cmcontinue
                try {
                    map.put("cmcontinue", queryContinue.getString("cmcontinue"));
                    urlConnection = getUrl(map);
                } catch (MalformedURLException ex) {
                    throw new ConectorException("WikipediaConector: getCategoryMembers(.. Malformed Url", ex);
                } catch (Exception e) {
                    log.error("WikipediaConector: getCategoryMembers(.. Error parsing cmcontinue ", e);
                }
            }
            if (categoriesMembers.size() == max || (queryContinue == null || queryContinue.isEmpty())) {
                break;
            }
        }
        return categoriesMembers;
    }

    public Map<String, String> getSource(List<String> pagesId, int chunk) throws ConectorException {
        log.debug("WikipediaConector: getSource(..");
        //validate paramer
        if (chunk < 1) {
            throw new InvalidParameterException("WikipediaConector: getSource(.. chunk can't be < 1");
        }
        URL urlConnection = null;
        Map<String, String> map = new HashMap<String, String>();
        Map<String, String> mapToReturn = new HashMap<String, String>();
        ArrayList<String> pagesToConsult = new ArrayList<String>();
        map.put("action", "query");
        map.put("prop", "revisions");
        map.put("rvprop", "content");
        map.put("format", "json");

        JSONObject retrieveData = null;
        int k = 1;
        for (int i = 0; i < pagesId.size(); i++) {
            pagesToConsult.add(pagesId.get(i));
            if ((k == chunk) || (i + 1 == pagesId.size())) {
                String encodeParameters = StringUtils.join(
                        pagesToConsult.toArray(), "|");
                try {
                    map.put("pageids", URLEncoder.encode(encodeParameters, "UTF-8"));
                    urlConnection = getUrl(map);
                } catch (UnsupportedEncodingException e1) {
                    log.error("WikipediaConector: getSource(.. error encoding parameters", e1);
                } catch (MalformedURLException ex) {
                    throw new ConectorException("WikipediaConector: getSource(.. Malformed Url", ex);
                }
                if (urlConnection == null) {
                    throw new ConectorException("WikipediaConector: getSource(.. error getting the conection");
                }
                //read data
                try {
                    retrieveData = getObjectFromRequest(getClient(urlConnection));
                } catch (IOException ex) {
                    log.error("WikipediaConector: getSource(.. Error IOException ", ex);
                    throw new ConectorException("IOException", ex);
                }
                JSONObject pages = null;
                try {
                    pages = retrieveData.getJSONObject("query").getJSONObject("pages");
                } catch (Exception e) {
                    throw new ConectorException("WikipediaConector: getSource(.. error parsing result", e);
                }
                Iterator<?> keys = pages.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if (pages.get(key) instanceof JSONObject) {
                        String source = "";
                        JSONObject jobj = (JSONObject) pages.get(key);
                        try {
                            source = jobj.getJSONArray("revisions").getJSONObject(0).getString("*");
                        } catch (Exception e) {
                            throw new ConectorException("getSource error parsing result", e);
                        }
                        mapToReturn.put(key, source);
                    }
                }
                k = 0;
                pagesToConsult.clear();
            }
            k++;
        }
        return mapToReturn;
    }

    public String getCategoryId(String categoryTile) throws ConectorException {
        log.debug("WikipediaConector: getCategoryId(..");
        //build the map with the parameters
        URL urlConnection;
        //validate parameters
        Map<String, String> map = new HashMap<String, String>();
        String encodeTitle = categoryTile;
        try {
            encodeTitle = URLEncoder.encode(categoryTile, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.error("WikipediaConector: getCategoryId(.. Error encoding title", ex);
        }
        map.put("action", "query");
        map.put("titles", encodeTitle);
        map.put("format", "json");
        try {
            urlConnection = getUrl(map);
        } catch (MalformedURLException ex) {
            throw new ConectorException("WikipediaConector: getCategoryId(.. Malformed Url", ex);
        }
        JSONObject retrieveData = null;
        try {
            retrieveData = getObjectFromRequest(getClient(urlConnection));
        } catch (IOException ex) {
            throw new ConectorException("WikipediaConector: getCategoryId(.. IOException", ex);
        }
        String key = "";
        try {
            Iterator<?> iter = retrieveData.getJSONObject("query").getJSONObject("pages").keys();
            iter.hasNext();
            key = (String) iter.next();
        } catch (Exception e) {
            log.error("WikipediaConector: getCategoryId(.. error getting id", e);
        }
        return key;
    }

    private URL getUrl(Map<String, String> parameters) throws MalformedURLException {
        Iterator<String> it = parameters.keySet().iterator();
        StringBuilder param = new StringBuilder();
        String sParam;
        param.append(this.urlBase);
        while (it.hasNext()) {
            String e = it.next();
            String value = parameters.get(e);
            param.append(e).append("=").append(value).append("&");
        }
        sParam = param.toString();
        int index = sParam.lastIndexOf("&");
        if (index > -1) {
            sParam = sParam.substring(0, index);
        }
        return new URL(sParam);
    }

    private HttpURLConnection getClient(URL urlConnection) throws IOException {
        HttpURLConnection client;
        try {
            client = (HttpURLConnection) urlConnection.openConnection();
            client.setRequestProperty("User-Agent", USER_AGENT);
            client.setConnectTimeout(CONNECTION_TIMEOUT);
            client.setReadTimeout(READ_TIMEOUT);
        } catch (IOException ex) {
            log.error("Error getCategories, IOException", ex);
            throw ex;
        }
        return client;
    }

    private JSONObject getObjectFromRequest(HttpURLConnection client) throws IOException {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        try {
            client.connect();
            //read data
            br = new BufferedReader(new InputStreamReader(client.getInputStream(), "UTF-8"));
            String line = "";
            sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException ex) {
            log.error("Error connecting to resource getCategories, IOException", ex);
            throw ex;
        } finally {
            if (client != null) {
                client.disconnect();
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    log.error("Error closing BufferedReader in getObjectFromRequest(...", ex);
                }
            }
        }
        JSONObject resultJson = (JSONObject) JSONSerializer.toJSON(sb.toString());
        return resultJson;
    }

    private boolean filterNotTopics(String title) {
        boolean passsFilter = true;
        for (String notTopicNameTemp : notTopicNames) {
            if (StringUtils.containsIgnoreCase(title, notTopicNameTemp)) {
                passsFilter = false;
                break;
            }
        }
        return passsFilter;
    }

    private boolean filterNotTopicsStartAt(String title) {
        boolean passsFilter = true;
        for (String notTopicNameTemp : notTopicsNamesStartAt) {
            if (StringUtils.startsWithIgnoreCase(title, notTopicNameTemp)) {
                passsFilter = false;
                break;
            }
        }
        return passsFilter;
    }
}
