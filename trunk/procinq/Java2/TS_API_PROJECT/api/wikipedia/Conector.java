package api.wikipedia;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author andres
 */
public interface Conector {

    public LinkedHashMap<String, String> getCategoryMembers(String categoryId, int chunk, int max, String type) throws ConectorException;

    public Map<String, String> getSource(List<String> pagesId, int chunk) throws ConectorException;

    public String getCategoryId(String categoryTile) throws ConectorException;
}
