package api.report;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import com.iw.system.ParamsQueryReportOracle;
import java.io.PrintWriter;
import java.sql.Connection;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSGenerateReport {
    
    private static final Logger log = Logger.getLogger(TSGenerateReport.class);
    
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("TSGenerateReport1");
        NodeUtils nu = new NodeUtils(dbc);
        //validate AUTHORIZED
        String sKey = (String) props.get("SKEY", true);
        //get nodeid to start with the process
        String corpusId = (String) props.get("corpusId");
        String reportType = (String) props.get("reportType");
        String queryParams = (String) props.get("paramsquery");
        String nodeId = nu.getRootNode(Integer.parseInt(corpusId));
        if (corpusId == null || queryParams == null || reportType == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (corpusId.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (nu.validateAuthorization(Integer.parseInt(nodeId), sKey, out) == 0) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        
        ReportCore bo = new ReportCore();
        OutputFormat output = new OutputFormatXML();
        
        JSONObject parameters = null;
        parameters = null;
        try {
            parameters = JSONObject.fromObject(queryParams);
            ParamsQueryReportOracle bean = (ParamsQueryReportOracle) JSONObject.toBean(
                    parameters, ParamsQueryReportOracle.class);
            
            if (bean != null) {
                String report = "";
                switch (Integer.parseInt(reportType)) {
                    case 1:
                        report = bo.generateReport1(corpusId, bean, output);
                        break;
                    case 2:
                        report = bo.generateReport2(corpusId, bean, output);
                        break;
                    case 3:
                        report = bo.generateReport3(corpusId, bean, output);
                        break;
                    default:
                        log.error("TSGenerateReport: ReportType is not valid!");
                        throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
                }
                //send data to client
                out.println(report);
            } else {
                log.error("Error parsing data JSON == null");
            }
        } catch (Exception e) {
            log.error("Error parsing data JSON", e);
        }
        log.debug("FINISHED");
    }
}
