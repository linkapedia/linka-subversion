
package api.report;

import com.iw.system.ModelReport;
import com.iw.system.ModelReportDynamo;

/**
 *
 * @author andres
 */
public interface OutputFormat {
    
    public String getData(ModelReport data) throws OutputFormatException;
    
    public String getData(ModelReportDynamo data) throws OutputFormatException;
      
}
