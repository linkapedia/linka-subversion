package api.report.wikipedia;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.wikipedia.DAOException;
import com.iw.db.ConnectionFactory;
import com.iw.system.WikipediaReportModel;
import com.iw.system.WrapperListReport;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;

/**
 * get the report to the wikipedia process
 *
 * @author andres
 */
public class TSWikipediaReport {

    private static final Logger log = Logger.getLogger(api.report.TSGenerateReport.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("TSWikipediaReport");
        String rownum = (String) props.get("rownum");
        if (rownum == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (rownum.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        List<WikipediaReportModel> report = getReportWikipedia(rownum);
        WrapperListReport modelReport = new WrapperListReport();
        modelReport.setList(report);
        String data = Transformation.getData(modelReport);
        out.println(data);
    }

    private static List<WikipediaReportModel> getReportWikipedia(String rowNum) throws Exception {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        if (conn == null) {
            throw new DAOException("WikipediaOracleDAO: saveProcessStatus(.. Error getting connection from oracle");
        }
        List<WikipediaReportModel> listToReturn = new ArrayList<>();
        WikipediaReportModel wikiModel;
        String sql = "SELECT * from wikipediareport order by requestid DESC";
        PreparedStatement pstmt = null;
        ResultSet result = null;
        int iRowNum = Integer.parseInt(rowNum);
        try {
            pstmt = conn.prepareStatement(sql);
            result = pstmt.executeQuery();
            int i = 0;
            while (result.next()) {
                if (i == iRowNum) {
                    break;
                }
                i++;
                wikiModel = new WikipediaReportModel();
                wikiModel.setRequestId(String.valueOf(result.getLong("REQUESTID")));
                wikiModel.setTimestamp(String.valueOf(result.getLong("TIMESTAMP")));
                wikiModel.setWikipediaStartPoint(result.getString("WIKISTARTPOINT"));
                wikiModel.setCorpus(result.getString("CORPUS"));
                wikiModel.setDepth(result.getString("DEPTH"));
                String messages = result.getString("MESSAGES");
                if (messages != null) {
                    JSONArray jsonArray = JSONArray.fromObject(messages);
                    List<String> list = new ArrayList<String>();
                    for (Object obj : jsonArray.toArray()) {
                        list.add(obj.toString());
                    }
                    wikiModel.setMessages(list);
                } else {
                    wikiModel.setMessages(new ArrayList<String>());
                }
                wikiModel.setStatus(result.getString("STATUS"));
                listToReturn.add(wikiModel);
            }
            return listToReturn;
        } catch (Exception e) {
            log.error("Error in the process status in wikipedia", e);
            throw new DAOException("Error in the process status in wikipedia", e);
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.error("Error closing resources for the database", e);
            }
        }
    }
}
