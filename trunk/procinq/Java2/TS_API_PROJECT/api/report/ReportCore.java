package api.report;

import com.iw.system.ModelNodeReport;
import com.iw.system.ModelNodeReportDynamo;
import com.iw.system.ModelReport;
import com.iw.system.ModelReportDynamo;
import com.iw.system.ParamsQueryReportOracle;
import java.util.List;

/**
 *
 * @author andres
 */
public class ReportCore {

    private static final org.apache.log4j.Logger log =
            org.apache.log4j.Logger.getLogger(ReportCore.class);

    /**
     * generate report signatures / musthaves
     *
     * @param corpusId
     * @param parameters
     * @param format
     * @return
     */
    public String generateReport1(String corpusId, ParamsQueryReportOracle parameters, OutputFormat format) {
        ReportDAO dao = new ReportDAO();
        ModelReport modelR = new ModelReport();
        String result = "";
        try {
            List<ModelNodeReport> list = dao.getDataReport1(corpusId, parameters);
            modelR.setNodeCount(String.valueOf(list.size()));
            modelR.setNodes(list);
            result = format.getData(modelR);
        } catch (ReportDAOException ex) {
            log.error("ReportCore: error getting data from oracle. " + ex.getMessage());
        } catch (OutputFormatException ex) {
            log.error("ReportCore: error output format. " + ex.getMessage());
        }
        return result;
    }

    /**
     * generate report Documents
     *
     * @param corpusId
     * @param parameters
     * @param format
     * @return
     */
    public String generateReport2(String corpusId, ParamsQueryReportOracle parameters, OutputFormat format) {
        ReportDAO dao = new ReportDAO();
        ModelReportDynamo modelR = new ModelReportDynamo();
        String result = "";
        try {
            List<ModelNodeReportDynamo> list = dao.getDataReport2(corpusId, parameters);
            modelR.setNodeCount(String.valueOf(list.size()));
            modelR.setNodes(list);
            result = format.getData(modelR);
        } catch (ReportDAOException ex) {
            log.error("ReportCore: error getting data from oracle. " + ex.getMessage());
        } catch (OutputFormatException ex) {
            log.error("ReportCore: error output format. " + ex.getMessage());
        }
        return result;
    }

    /**
     * generate report Images
     *
     * @param corpusId
     * @param parameters
     * @param formate
     * @return
     */
    public String generateReport3(String corpusId, ParamsQueryReportOracle parameters, OutputFormat format) {
        ReportDAO dao = new ReportDAO();
        ModelReportDynamo modelR = new ModelReportDynamo();
        String result = "";
        try {
            List<ModelNodeReportDynamo> list = dao.getDataReport3(corpusId, parameters);
            modelR.setNodeCount(String.valueOf(list.size()));
            modelR.setNodes(list);
            result = format.getData(modelR);
        } catch (ReportDAOException ex) {
            log.error("ReportCore: error getting data from oracle. " + ex.getMessage());
        } catch (OutputFormatException ex) {
            log.error("ReportCore: error output format. " + ex.getMessage());
        }
        return result;
    }
}
