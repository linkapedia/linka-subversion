package api.report;

import com.iw.system.ModelReport;
import com.iw.system.ModelReportDynamo;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author andres
 */
public class OutputFormatXML implements OutputFormat {

    public String getData(ModelReport data) throws OutputFormatException {
        JAXBContext jc;
        StringWriter writer = new StringWriter();
        try {
            jc = JAXBContext.newInstance(ModelReport.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.marshal(data, writer);
        } catch (JAXBException ex) {
            throw new OutputFormatException("Error parsing xml data", ex.getCause());
        }
        return writer.toString();
    }

    @Override
    public String getData(ModelReportDynamo data) throws OutputFormatException {
        JAXBContext jc;
        StringWriter writer = new StringWriter();
        try {
            jc = JAXBContext.newInstance(ModelReportDynamo.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.marshal(data, writer);
        } catch (JAXBException ex) {
            throw new OutputFormatException("Error parsing xml data", ex.getCause());
        }
        return writer.toString();
    }
}
