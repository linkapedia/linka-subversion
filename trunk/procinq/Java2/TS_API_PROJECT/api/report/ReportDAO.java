package api.report;

import com.iw.db.ConnectionFactory;
import com.iw.system.ModelNodeReport;
import com.iw.system.ModelNodeReportDynamo;

import com.iw.system.ParamsQueryReportOracle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class ReportDAO {

    private static final Logger log = Logger.getLogger(ReportDAO.class);

    /**
     * report 1 - signatures / musthaves
     *
     * @param corpusId
     * @param parameters
     * @return
     * @throws ReportDAOException
     */
    public List<ModelNodeReport> getDataReport1(String corpusId,
            ParamsQueryReportOracle parameters) throws ReportDAOException {
        log.debug("ReportDAO: getOracleData(String corpusId)");
        Connection con = ConnectionFactory.createConnection(
                ConnectionFactory.ORACLE);
        if (con == null) {
            throw new ReportDAOException(
                    "ReportDAO: Error getting oracle connection");
        }
        ModelNodeReport modelNode = null;
        List<ModelNodeReport> list = null;

        String sql = "select nodeid, nodetitle, "
                + "linkapedia.GET_MUSTHAVE_COUNT(nodeid) as must,"
                + "linkapedia.GET_SIGNATURE_COUNT(nodeid) as sig "
                + "from node where "
                + "(corpusid = ? and bridgenode = 0) and "
                + "((linkapedia.GET_MUSTHAVE_COUNT(nodeid) = ? or "
                + "linkapedia.GET_MUSTHAVE_COUNT(nodeid) > ?) or "
                + "(linkapedia.GET_SIGNATURE_COUNT(nodeid) < ? or "
                + "linkapedia.GET_SIGNATURE_COUNT(nodeid) > ?)) "
                + "order by nodeid";

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        list = new ArrayList<>();
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(corpusId));
            pstmt.setInt(2, Integer.parseInt(parameters.getLessMusthave()));
            pstmt.setInt(3, Integer.parseInt(parameters.getMoreMusthave()));
            pstmt.setInt(4, Integer.parseInt(parameters.getLessSignature()));
            pstmt.setInt(5, Integer.parseInt(parameters.getMoreSignature()));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String nodeid = String.valueOf(rs.getInt("NODEID"));
                String nodetitle = rs.getString("NODETITLE");
                String inmusthave = String.valueOf(rs.getInt("MUST"));
                String insignature = String.valueOf(rs.getInt("SIG"));
                modelNode = new ModelNodeReport();
                modelNode.setNodeId(nodeid);
                modelNode.setNodeTitle(nodetitle);
                modelNode.setNodeMusthaves(inmusthave);
                modelNode.setNodeSignatures(insignature);
                list.add(modelNode);
            }
            return list;
        } catch (SQLException e) {
            throw new ReportDAOException(
                    "ReportDAO: Error SQL exception exucuting query",
                    e.getCause());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                throw new ReportDAOException(
                        "ReportDAO: Error SQL exception closing resources",
                        e.getCause());
            }
        }

    }

    /**
     * report 2 - Documents
     *
     * @param corpusId
     * @param parameters
     * @return
     * @throws ReportDAOException
     */
    public List<ModelNodeReportDynamo> getDataReport2(String corpusId,
            ParamsQueryReportOracle parameters) throws ReportDAOException {
        log.debug("ReportDAO: getOracleData(String corpusId)");
        Connection con = ConnectionFactory.createConnection(
                ConnectionFactory.ORACLE);
        if (con == null) {
            throw new ReportDAOException(
                    "ReportDAO: Error getting oracle connection");
        }
        ModelNodeReportDynamo modelNode = null;
        List<ModelNodeReportDynamo> list = null;

        String sql = "select nodeid, nodetitle, documentscount from node "
                + "where (corpusid = ? and bridgenode =  0) and "
                + "(documentscount < ? or documentscount > ?) "
                + "order by nodeid";

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        list = new ArrayList<>();

        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(corpusId));
            pstmt.setInt(2, Integer.parseInt(parameters.getLessDocuments()));
            pstmt.setInt(3, Integer.parseInt(parameters.getMoreDocuments()));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String nodeid = String.valueOf(rs.getInt("NODEID"));
                String nodetitle = rs.getString("NODETITLE");
                String documentscount = rs.getString("documentscount");
                modelNode = new ModelNodeReportDynamo();
                modelNode.setNodeId(nodeid);
                modelNode.setNodeTitle(nodetitle);
                modelNode.setDocuments(documentscount);
                list.add(modelNode);
            }
            return list;
        } catch (SQLException e) {
            throw new ReportDAOException(
                    "ReportDAO: Error SQL exception exucuting query",
                    e.getCause());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                throw new ReportDAOException(
                        "ReportDAO: Error SQL exception closing resources",
                        e.getCause());
            }
        }
    }

    /**
     * report 3 - images
     *
     * @param corpusId
     * @param parameters
     * @return
     * @throws ReportDAOException
     */
    public List<ModelNodeReportDynamo> getDataReport3(String corpusId,
            ParamsQueryReportOracle parameters) throws ReportDAOException {
        log.debug("ReportDAO: getOracleData(String corpusId)");
        Connection con = ConnectionFactory.createConnection(
                ConnectionFactory.ORACLE);
        if (con == null) {
            throw new ReportDAOException(
                    "ReportDAO: Error getting oracle connection");
        }
        ModelNodeReportDynamo modelNode = null;
        List<ModelNodeReportDynamo> list = null;

        String sql = "select nodeid, nodetitle, imagestatus from node "
                + "where (corpusid = ? and imagestatus = 0) "
                + "order by nodeid";

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        list = new ArrayList<>();

        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(corpusId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String nodeid = String.valueOf(rs.getInt("NODEID"));
                String nodetitle = rs.getString("NODETITLE");
                String imagestatus = rs.getString("imagestatus");
                modelNode = new ModelNodeReportDynamo();
                modelNode.setNodeId(nodeid);
                modelNode.setNodeTitle(nodetitle);
                modelNode.setImages(imagestatus);
                list.add(modelNode);
            }
            return list;
        } catch (SQLException e) {
            throw new ReportDAOException(
                    "ReportDAO: Error SQL exception exucuting query",
                    e.getCause());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                throw new ReportDAOException(
                        "ReportDAO: Error SQL exception closing resources",
                        e.getCause());
            }
        }
    }
}