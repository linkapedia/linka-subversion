package api.tscorpus;

import api.util.ConfServer;
import com.linkapedia.image.utils.ImageDownloader;
import com.linkapedia.procinq.server.corpus.CorpusDao;
import com.linkapedia.procinq.server.utils.ImageUtil;
import java.io.PrintWriter;
import java.sql.Connection;
import org.apache.log4j.Logger;


public class TSAddImageCorpusURL {

    private static final Logger log = Logger.getLogger(TSAddImageCorpusURL.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        log.debug("TSAddImageCorpusURL");
        String corpusid = (String) props.get("corpusid");
        String url = (String) props.get("url");
        ImageDownloader imageDownloader = createImageDownloader();
        byte[] image = imageDownloader.downloadImageByUrl(url);
        image = ImageUtil.getLinkapediaSiteImage(image);
        String title = getTitleByUrl(url);
        CorpusDao.saveImage(Integer.parseInt(corpusid), title, url, image, dbc);
        out.println("<SUCCESS>images add ok</SUCCESS>");
    }

    private static String getTitleByUrl(String url) {
        return url.substring(url.lastIndexOf("/") + 1, url.length());
    }

    private static ImageDownloader createImageDownloader() {
        int maxSizeImageMB = ConfServer.getIntValue("images.service.maxSizeImageMB");
        return new ImageDownloader(maxSizeImageMB * 1024 * 1024);
    }
}
