package api.tscorpus;

import api.Log;
import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.database.JDBCIndra_Connection;
import com.indraweb.execution.Session;
import com.indraweb.ir.ParseParms;
import com.indraweb.ir.PhraseParms;
import com.indraweb.signatures.SignatureParms;
import com.indraweb.signatures.WordFrequencies;
import com.indraweb.util.UtilFile;
import com.iw.classification.Data_WordsToNodes;
import com.iw.db.ConnectionFactory;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Ingest a new corpus including orthogonal corpus indexing. The information to
 * be ingested is expected to be in <b>CORPUS NORMAL FORM</b>.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param CorpusFolder The path to a directory containing a <b>CORPUS NORMAL
 * FORM</b> directory structure. Only relevant if building from files and not
 * from database.
 * @param CorpusName (optional) Name of the corpus being ingested.
 * @param BuildHierarchy (optional) True to build node hierarchy (eg pre sig
 * gen). Default is false.
 * @param Delimiters (optional) Delimiter characters for article word parsing.
 * Default is . Default is "?!/, &+.:;#*?". To either default or a passed in set
 * the following delimiter set will be included "\u00A0?\"\t\n\r\u001A". // not
 * yet supported
 * @param NumBytesTextPerTopic (optional) Number indicating how many bytes of
 * corpus article text should be incorporated. Default is 21000.
 * @param IncludeNumbers (optional) True to admit numbers as signature terms.
 * Default is false.
 * @param NumParentTitleTermAsSigs (optional) Number of Parent signature terms
 * to include as part of a signature. Default is 3. 0 or less turns off use of
 * parent titles as signatures.
 * @param PhraseCountMultiplier (optional) Integer weight for phrased as opposed
 * to single word signature terms. Default is 1. Includes parent titles.
 * @param PhraseRepeatMin (optional) Minimum number of times a sequence of words
 * must occur to be recognized as a phrase. Default is 3. Includes parent
 * titles.
 * @param PhraseLenMin (optional) Minimum number of words that can constitute a
 * phrase. Default is 2.
 * @param PhraseLenMax (optional) Maximum number of words that can constitute a
 * phrase. Default is 4.
 * @param PhraseWordsCanStartWithNonLetters (optional) False if signature terms
 * will be rejected unless they start with a letter. Default is false.
 *
 * @param MaxSigTermsPerNode (optional) Total number of signature terms per
 * node. Default is 30. Includes parent titles.
 * @param SigGenRangeSize (optional) Number of articles processed at a time.
 * Default is 250.
 * @param WordFreqMaxForSig (optional) Word frequency max to be allowed as a
 * signature. Default is 75.
 * @param NumParentTitleTermAsSigs (optional) Number of parent title words to
 * use in a signature. Default is 3.
 * @param NumParentTitleTermAsSigs (optional) Number of parent title words to
 * use in a signature. Default is 3.
 * @param SigWordsCantStartWith (optional) A string containing the set of
 * characters that a signature term can not start with. Candidates for signature
 * will be eliminated if they start with any of these letter. Default "_-([<>" .
 * @param KeepSigsForParentNodes (optional) Indicates whether signature terms
 * should be inserted into the database for non-leaf nodes. Default is true.
 * @param KeepSigsIfNoSourceText (optional) Indicates whether signature terms
 * should be inserted into the database for nodes that had no source text (e.g.,
 * clob field content is null or whitespace). Default is true.
 *
 * @param CorpusFolder (optional unless BuildHierarchy is true) disk location
 * for top of CNF corpus file hierarchy
 * @param CorpusID (required unless BuildHierarchy is true) corpusid of existing
 * (heirarchy-installed) corpus (eg pre sig gen)
 *
 * @note
 * http://ITSSERVER/servlet/ts?fn=tscorpus.TSInstallCorpusAccess&CorpusName=TestCorpus&CorpusFolder=//hathor/data/001/&SKEY=-161518288
 *
 * @return	TSINSTALLCORPUS_API_CALL_RESULT value 1 or 0. \verbatim <?xml
 * version="1.0" encoding="UTF-8" ?> <TSRESULT>
 * <TSINSTALLCORPUS_API_CALL_RESULT>1</TSINSTALLCORPUS_API_CALL_RESULT>
 * </TSRESULT>
 * \endverbatim
 */
public class TSInstallCorpus {
    // TSInstallCorpus

    private static String sDefaultCorpusName = "unnamed corpus";
    private static boolean bDebugging = false;
    private static final Object ISemaphore = new Object();
    public static boolean bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest = false;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        /*
         * http://207.103.213.118:8100/servlet/ts?fn=tscorpus.TSCreateCorpus& CorpusPath=//machinename/sharename/foldername&CorpusActive=-1&CorpusName=Test Corpus
         *
         * works:	"\\\\66.134.131.38\\public\\classifytest" does not work:	"//66.134.131.38/public/classifytest"
         */
        Connection dbcTest = null;
        try {
            dbcTest = getOracleConnection();
            if (!bDebugging && !com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.INGESTION)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            //String sKey = (String) props.get("SKEY", true);
            //User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            // Ensure user is a member of the admin group
            //if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            String sCorpusName = (String) props.get("CorpusName");
            String corpusInstallOptions = (String) props.get("installoptions", "100");

            if (corpusInstallOptions != null) {
                com.indraweb.execution.Session.corpusInstallOptions = corpusInstallOptions;
            }

            if (sCorpusName == null) {
                props.put("CorpusName", sDefaultCorpusName); // required by tscreatecorpus
            }
            int iCorpusID = props.getint("CorpusID", "-1");
            String sCorpusFolder = (String) props.get("CorpusFolder", null);
            boolean bBuildHierarchyToNodeNodePathNodeData =
                    props.getbool("BuildHierarchy", "false");
            boolean bProp_doSigGen = props.getbool("DoSigGen", "true");

            if (bBuildHierarchyToNodeNodePathNodeData && iCorpusID > 0) {
                throw new Exception("invalid mode - corpusid can't be passed in to build hierarchy which is creating a new corpus");
            }
            if (!bBuildHierarchyToNodeNodePathNodeData && iCorpusID < 0) {
                throw new Exception("invalid mode - corpusid required to build signatures from an existing hierarchy");
            }
            if (!bBuildHierarchyToNodeNodePathNodeData && !bProp_doSigGen) {
                throw new Exception("invalid mode - if not building hierarchy and not doing sig gen then nothing to be done");
            }
            if (bBuildHierarchyToNodeNodePathNodeData && sCorpusFolder == null) {
                throw new Exception("corpus folder (CNF location) required to build hierarchy");
            }
            if (sCorpusName == null && bBuildHierarchyToNodeNodePathNodeData) {
                throw new Exception("corpus name required to build hierarchy");
            }
            if (sCorpusName != null && bBuildHierarchyToNodeNodePathNodeData == false) {
                throw new Exception("corpus name irrelevant if not building hierarchy");
            }
            if (iCorpusID == -1) {
                iCorpusID = TSCreateCorpus.createCorpus(props, out, dbcTest);
            }

            if (iCorpusID < 0) {
                throw new Exception("unable to create corpus, outstream contains stack");
            }

            if (sCorpusName == null) {
                props.remove("CorpusName"); // required by tscreatecorpus
            }
            synchronized (ISemaphore) {
                String sProp_CorpusActive = (String) props.get("CorpusActive", "-1");

                String sProp_CorpusFolder = (String) props.get("CorpusFolder", bBuildHierarchyToNodeNodePathNodeData);

                int iCorpusActive = Integer.parseInt(sProp_CorpusActive);
                String sProp_CorpusName = (String) props.get("CorpusName", null);
                // PARSEPARMS
                String sProp_TopicDelimiters = (String) props.get("DELIMITERS", ParseParms.getDelimitersAll_Default());
                boolean bProp_IncludeNumbers = props.getbool("INCLUDENUMBERS", false);
                ParseParms parseparms = new ParseParms(sProp_TopicDelimiters, bProp_IncludeNumbers, -1); // no limit until NumBytesTextPerTopic supported
                //int iProp_NumBytesTextPerTopic = props.getint("NumBytesTextPerTopic", "21000");

                // SIGNATUREPARMS
                SignatureParms sigparms = new SignatureParms(props); // no limit until NumBytesTextPerTopic supported

                // PHRASEPARMS
                PhraseParms phraseparms = new PhraseParms(props); // no limit until NumBytesTextPerTopic supported

                // VERIFY that 000.html root is present
                String sFolderNAmeForRootFileVerify = null;
                if (bBuildHierarchyToNodeNodePathNodeData) {
                    if (!sProp_CorpusFolder.endsWith("/") && !sProp_CorpusFolder.endsWith("\\")) {
                        sFolderNAmeForRootFileVerify = sProp_CorpusFolder + "/000.html";
                    } else {
                        sFolderNAmeForRootFileVerify = sProp_CorpusFolder + "000.html";
                    }

                    if (!com.indraweb.util.UtilFile.bFileExists(sFolderNAmeForRootFileVerify)) {
                        throw new Exception("Corpus Normal Form violated : file not found : " + sFolderNAmeForRootFileVerify);
                    }
                    // END VERIFY that 000.html root is present
                } // if building hierarhcy also

                //boolean bVerboseSigLogging = com.indraweb.execution.Session.cfg.getPropBool ("verboseSigLogging");
/*
                 * bVerboseSigLogging = true; api.Log.Log ("setting bVerboseSigLogging true");
                 */
                Data_WordsToNodes.invalidateNodeSigCache_byCorpus("TSInstallCorpus", iCorpusID);

                long lTStartSigInstall = System.currentTimeMillis();
                api.Log.Log(" ======= CORPUS INSTALL START "
                        + " iCorpusID [" + iCorpusID + "] "
                        + " sCorpusName [" + sCorpusName + "] "
                        + " bProp_doSigGen [" + bProp_doSigGen + "] "
                        + " bBuildHierarchyToNodeNodePathNodeData [" + bBuildHierarchyToNodeNodePathNodeData + "] "
                        + " sCorpusFolder [" + sCorpusFolder + "] ");
                com.indraweb.signatures.WordFrequencies wordfreqPriors = null;

                // CREATE SIG WORD FREQ FILE to eliminate some words from sig file
                String sWordFreqFile = Session.sIndraHome + "WordFrequencies/" + "SigWordFreqs_IWModel_Corpus5only.txt";
                //Log.log ("sWordFreqFile : " + sWordFreqFile + "\r\n");
                if (!UtilFile.bFileExists(sWordFreqFile)) {
                    api.Log.LogError("WordFrequencies file [" + sWordFreqFile + "] missing ");
                }
                WordFrequencies wordfreq = new com.indraweb.signatures.WordFrequencies(
                        sWordFreqFile, sigparms.iWordFreqMaxForSig, com.indraweb.signatures.WordFrequencies.ICOUNTFILE_MODEL_IW, false);


                com.indraweb.execution.tasks.Task_CorpusInstallHierarchyAndSigs.doIt(
                        iCorpusID,
                        sProp_CorpusFolder,
                        sProp_CorpusName,
                        out,
                        dbcTest,
                        false,
                        bBuildHierarchyToNodeNodePathNodeData,
                        bUseCNFFilesAsSigGenSource_trueMeansOldWayForTest,
                        bProp_doSigGen,
                        phraseparms,
                        parseparms,
                        sigparms,
                        wordfreq);

                String sSQLCountNodes = "select count (*) from node where corpusid = " + iCorpusID;
                int iNodeCount = (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLCountNodes, dbcTest);

                String sSQLCountSigs = "select count (*) from (select n.nodeid from signature s, node n  where n.nodeid = s.nodeid and n.corpusid = " + iCorpusID + ")";
                int iSigCount = (int) JDBCIndra_Connection.executeQueryAgainstDBLong(sSQLCountSigs, dbcTest);

                long lDur = com.indraweb.util.UtilProfiling.elapsedTimeMillis(lTStartSigInstall);
                api.Log.Log(" ========= CORPUS INSTALL COMPLETE id [" + iCorpusID + "] "
                        + "  ms [" + lDur + "] "
                        + "  nodecnt [" + iNodeCount + "] "
                        + "  sigcnt [" + iSigCount + "] "
                        + "  sigcnt / nodecnt [" + (double) iSigCount / (double) iNodeCount + "]");


                out.println("<TSINSTALLCORPUS_API_CALL_RESULT>" + "SUCCESS" + "</TSINSTALLCORPUS_API_CALL_RESULT>");
            } // synchronized
        } catch (Throwable t) {
            com.indraweb.util.Log.NonFatalError("CORPUS INSTALL ERROR", t);
            Log.LogError("CORPUS INSTALL ERROR [" + t.getMessage() + "]\r\n");
            out.println("<TSINSTALLCORPUS_API_CALL_RESULT>" + "FILE" + t.getMessage() + "see logs" + "</TSINSTALLCORPUS_API_CALL_RESULT>");
            throw new Exception("TSInstallCorpus Exception : "
                    + "e.msg [" + t.getMessage() + "] "
                    + " e.stk [" + Log.stackTraceToString(t) + "]");
        } finally {
            if (dbcTest != null) {
                try {
                    dbcTest.close();
                } catch (SQLException e) {
                    Log.LogError("Error closing the connection", e);
                }
            }
        }
    }

    private static Connection getOracleConnection() throws ClassNotFoundException, SQLException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection connection = null;
        connection = DriverManager.getConnection(
                "jdbc:oracle:thin:@link3.chvjczfef6ad.us-east-1.rds.amazonaws.com:1521:link3", "sbooks", "sbooks");
        return connection;
    }
}
