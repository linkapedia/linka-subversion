package api.tscorpus;

import java.io.*;
import java.sql.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.*;

import com.iw.system.User;
import api.Log;

/**
 * Changes the properties of the given corpus. Only a member of the Admin group
 * can alter corpus properties.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param CorpusID Unique corpus identifier
 * @param CorpusName (optional) Corpus display name. This parameter is only
 * provided if altered.
 * @param CorpusDesc (optional) Corpus description. This parameter is only
 * provided if altered.
 * @param CorpusActive (optional) Active state, 1 is active, 0 is inactive. This
 * parameter is only provided if altered.
 *
 * @note
 * http://itsserver/servlet/ts?fn=tscorpus.TSGetCorpusProps&CorpusID=4&SKEY=993135977
 *
 * @return	none \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <CLASSLOAD>Fri Oct 26 17:47:46 EDT 2001</CLASSLOAD>
 * <CALLCOUNT>38</CALLCOUNT>
 * <TIMEOFCALL_MS>10</TIMEOFCALL_MS>
 * </TSRESULT>
 * \endverbatim
 */
public class TSEditCorpusProps {
    // TSEditCorpusProps (sessionid, userid, email, password, firstname, lastname, emailstatus, scorethreshold, resultsperpage, userstatus)
    // Note: All parameters except sessionid and userid are OPTIONAL

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        // NOTE : required fields MUST appear first in the list above.
        String sTableName = "corpus";
        int iNumRequiredFields = 1;
        String[] sArrFields_URLNameSpace = {
            "CorpusID", // required
            "CorpusName",
            "CorpusDesc",
            "RocF1",
            "RocF2",
            "RocF3",
            "RocC1",
            "RocC2",
            "RocC3",
            "CorpusActive",
            "CorpusDomain",
            "CorpusToClassify",
            "CorpusAffinity",
            "CorpusTerms",
            "CorpusRunFrequency",
            "CorpusLastRun",
            "CorpusLastSync",
            "kismetricid",
            "adsenseid",
            "googleaid",
            "tagline",
            "domainName",
            "UseGoogleAds",
            "googleSiteVerificationId",
            "category"
        };

        String[] sArrFields_DBNameSpace = {
            "CorpusID", // required
            "Corpus_Name",
            "CorpusDesc",
            "RocF1",
            "RocF2",
            "RocF3",
            "RocC1",
            "RocC2",
            "RocC3",
            "CorpusActive",
            "CorpusDomain",
            "CorpusToClassify",
            "CorpusAffinity",
            "CorpusTerms",
            "CorpusRunFrequency",
            "CorpusLastRun",
            "CorpusLastSync",
            "KISSMETRICID",
            "ADSENSEID",
            "GOOGLEANALYTICSID",
            "TAGLINE",
            "DOMAINNAME",
            "USEGOOGLEADS",
            "GOOGLESITEVERIFICATIONID",
            "CATEGORYID"
        };

        props.put("CorpusLastSync", "SYSDATE");

        String sCorpusID = (String) props.get("CORPUSID", true);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {

            if (!u.IsAdmin(sCorpusID, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
            String sSQL = api.sqlinteract.SQLModelUpdate.genSQLUpdate(sTableName,
                    props,
                    sArrFields_URLNameSpace,
                    sArrFields_DBNameSpace,
                    iNumRequiredFields);

            out.println("<DEBUG>SQL:" + sSQL + "</DEBUG>");
            out.flush();
            Statement stmt = null;
            try {
                stmt = dbc.createStatement();

                // If query statement failed, throw an exception
                if (stmt.executeUpdate(sSQL) == 0) {
                    out.println("<DEBUG>The specified record does not exist to update</DEBUG>");
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND);
                } else {
                    out.println("<SUCCESS>Corpus altered successfully.</SUCCESS>");
                }
            } catch (Exception e) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION,
                        "e.msg [" + e.getMessage() + "] "
                        + " SQL [" + sSQL.toString() + "] "
                        + " e.stk [" + Log.stackTraceToString(e) + "]");
            } finally {
                out.println(EmitValue.genValue("DEBUG", "edit update operation complete"));
                stmt.close();
            }
        } // Catch exception
        catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}
