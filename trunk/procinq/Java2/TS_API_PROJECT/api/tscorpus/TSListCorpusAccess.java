package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;

import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.iw.system.User;
import com.iw.system.SecurityGroup;

/**
 *  Lists all of the LDAP group permissions with access permission to the given corpus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *
 *  @note  http://ITSSERVER/servlet/ts?fn=tscorpus.TSListCorpusAccess&CorpusID=100007&SKEY=1634835192

 *	@return	a series of corpus access objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <GROUPS>
            <GROUP>
                <GROUPID>cn=Tech Staff,ou=users,dc=indraweb,dc=com</GROUPID>
                <GROUPNAME>Tech Staff</GROUPNAME>
                 <ACCESS>0</ACCESS>
             </GROUP>
            <GROUP>
                <GROUPID>cn=Guests,dc=indraweb,dc=com</GROUPID>
                <GROUPNAME>Guests</GROUPNAME>
                <ACCESS>0</ACCESS>
            </GROUP>
            <GROUP>
                <GROUPID>cn=Administrators,dc=indraweb,dc=com</GROUPID>
                <GROUPNAME>Administrators</GROUPNAME>
                <ACCESS>1</ACCESS>
            </GROUP>
 ...
  \endverbatim
 */
public class TSListCorpusAccess
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = null;
		try {
			if (!u.IsAdmin(sCorpusID, out)) { 
				out.println("<NOTAUTHORIZED>1</NOTAUTHORIZED>");
				throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); 
			}
	
			lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
			Hashtable htGroups = new Hashtable();
			Vector vCorpusGroups = new Vector();
			Vector vAdminGroups = new Vector();
			
			// Get the GROUP LIST from LDAP
			htGroups = lc.GetAllGroups();
			Enumeration eh = htGroups.elements();
			while (eh.hasMoreElements()) {
				Group g = (Group) eh.nextElement();
				if (g.IsAdmin(sCorpusID)) { vAdminGroups.addElement(g); }
				else if (g.IsAuthorized(sCorpusID)) { vCorpusGroups.addElement(g); }
			}
			
			if ((vCorpusGroups.size() == 0) && (vAdminGroups.size() == 0)) { 
				throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); 
			}
			
			out.println("<GROUPS>"); 
			eh = vCorpusGroups.elements();
			while (eh.hasMoreElements()) {
				Group g = (Group) eh.nextElement();
				String s = g.GetDN();

                SecurityGroup sg = api.security.DocumentSecurity.getSecurityByName(s);
                String securityID = "-1";
                if (sg != null) securityID = sg.getID();

				if (!s.endsWith(lc.GetDC())) { s = s+","+lc.GetDC(); }
				
				out.println ("   <GROUP> ");
                out.println ("      <SECURITYID>"+securityID+"</SECURITYID>");
				out.println ("      <GROUPID>"+s+"</GROUPID>");
				out.println ("      <GROUPNAME>"+g.GetGroupName()+"</GROUPNAME>");
                out.println ("      <GROUPLONGNAME>"+g.GetDisplayName()+"</GROUPLONGNAME>");
				out.println ("		<ACCESS>0</ACCESS>");
				out.println ("   </GROUP>");
			}
			eh = vAdminGroups.elements();
			while (eh.hasMoreElements()) {
				Group g = (Group) eh.nextElement();
				String s = g.GetDN();
				
                SecurityGroup sg = api.security.DocumentSecurity.getSecurityByName(s);
                String securityID = "-1";
                if (sg != null) securityID = sg.getID();

				if (!s.endsWith(lc.GetDC())) { s = s+","+lc.GetDC(); }
				
				out.println ("   <GROUP> ");
                out.println ("      <SECURITYID>"+securityID+"</SECURITYID>");
				out.println ("      <GROUPID>"+s+"</GROUPID>");
				out.println ("      <GROUPNAME>"+g.GetGroupName()+"</GROUPNAME>");
                out.println ("      <GROUPLONGNAME>"+g.GetDisplayName()+"</GROUPLONGNAME>");
				out.println ("		<ACCESS>1</ACCESS>");
				out.println ("   </GROUP>");
			}

			out.println("</GROUPS>");
		} catch ( TSException tse )	{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		} finally { lc.close(); }
	}
}
