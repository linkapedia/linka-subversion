package api.tscorpus;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.S3Storage;
import api.util.S3StorageException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.iw.db.ConnectionFactory;
import com.linkapedia.procinq.server.corpus.CorpusDao;
import com.linkapedia.procinq.server.node.NodeDao;
import com.linkapedia.procinq.server.security.Authoritation;
import com.linkapedia.procinq.server.utils.AmazonUtil;
import com.linkapedia.procinq.server.utils.ImageUtil;
import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;
import org.apache.log4j.Logger;


public class TSSaveImagesS3 {

    private static final Logger LOG = Logger.getLogger(TSSaveImagesS3.class);
    private static final String BUCKET_NAME = "taxonomyimages";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        LOG.debug("TSSaveImagesS3 Corpus");
        String sKey = (String) props.get("SKEY", true);
        String corpusIdStr = (String) props.get("corpusid");
        int corpusId = Integer.parseInt(corpusIdStr);

        if (!Authoritation.isAuthorizedUser(corpusId, sKey, out)) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
        }
        Connection connection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);

        int rootNodeId = NodeDao.getRootNode(corpusId, connection);
        List<byte[]> corpusImages = CorpusDao.getImages(corpusId, connection);
        String rootNodeIdDynamo = AmazonUtil.getDynamoDBIdByNodeId(String.valueOf(rootNodeId));
        ObjectMetadata meta;
        int indexImage = 0;
        String keyS3;
        for (byte[] image : corpusImages) {
            byte[] linkapediaSiteImage = ImageUtil.getLinkapediaSiteImage(image);
            meta = ImageUtil.createMetadataImage(linkapediaSiteImage);
            try {
                keyS3 = AmazonUtil.buildKeyImage(rootNodeIdDynamo, indexImage);
                S3Storage.storageData(BUCKET_NAME, keyS3, new ByteArrayInputStream(linkapediaSiteImage), meta);
                indexImage++;
            } catch (S3StorageException e) {
                LOG.error("Error S3StorageException: storageing image ", e);
            }
        }
        out.println("<RESULT>images saved -> " + indexImage + "</RESULT>");
    }
}
