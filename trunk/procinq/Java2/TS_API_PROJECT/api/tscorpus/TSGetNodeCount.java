package api.tscorpus;


import java.io.*;
import java.sql.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.emitxml.*;

import com.iw.system.User;

/**
 * Get the number of topics within a given taxonomy.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID  Unique corpus identifier of this taxonomy.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetNodeCount&CorpusID=5&SKEY=9919294812

 *	@return	COUNT reflecting number of nodes
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <COUNT>200</COUNT>
  </TSRESULT>
  \endverbatim
 */
public class TSGetNodeCount {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        String sCorpusID = (String) props.get("CorpusID", true);

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (u == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED);
        }

        String sSQL = " select count(*) from Node where CorpusID = "+sCorpusID;
        Statement stmt = null; ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);
            rs.next();

            out.println("<COUNT>"+rs.getInt(1)+"</COUNT>");
        } catch (Exception e) { throw e;
        } finally { rs.close(); stmt.close(); rs = null; stmt = null; }
    }
}
