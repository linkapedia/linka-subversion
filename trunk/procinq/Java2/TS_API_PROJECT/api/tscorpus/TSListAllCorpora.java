package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;
import com.indraweb.execution.Session;
import com.iw.system.User;

/**
 *  Returns a list of all corpora, regardless of access permission and state.  Corpus marked for deletion will
 *  be returned with a CorpusActive flag indicating that the corpus was marked for deletion.  This function may
 *  only be invoked by a user with System Administration properties.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note  http://itsserver/servlet/ts?fn=tscorpus.TSListAllCorpora&SKEY=993135977

 *	@return	a series of corpus objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CORPORA>
            <CORPUS>
                <CORPUSID>2</CORPUSID>
                <CORPUS_NAME>Wiley InterScience - Kirk-Othmer Encyclopedia of Chemical Technology</CORPUS_NAME>
                <CORPUSDESC>Updated regularly, Kirk-Othmer Online will stay current with the latest developments in chemical technology and related fields.</CORPUSDESC>
                <ROCF1>0.0</ROCF1>
                <ROCF2>0.0</ROCF2>
                <ROCF3>0.0</ROCF3>
                <ROCC1>0.0</ROCC1>
                <ROCC2>0.0</ROCC2>
                <ROCC3>0.0</ROCC3>
                <CORPUSACTIVE>1</CORPUSACTIVE>
            </CORPUS>
 ...
  \endverbatim
 */
public class TSListAllCorpora
{
	// TSListCorpera (sessionid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{

		try {
			
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			String sSQL = " select CorpusID, Corpus_Name, CorpusDesc, "+
						  " Rocf1, Rocf2, Rocf3, Rocc1, Rocc2, Rocc3, CorpusActive"+			
						  " from Corpus order by lower(Corpus_Name) asc";
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);

			int loop = 0;
			Hashtable htAuth = u.GetAdminAuthorizedHash(out);
			boolean bAdmin = u.IsMember(out);
			while ( rs.next() ) {
				
				int iCorpusID = rs.getInt(1);
				String sCorpusName = rs.getString(2);
				String sCorpusDesc = rs.getString(3);
				float fRocF1 = rs.getFloat(4);
				float fRocF2 = rs.getFloat(5);
				float fRocF3 = rs.getFloat(6);
				float fRocC1 = rs.getFloat(7);
				float fRocC2 = rs.getFloat(8);
				float fRocC3 = rs.getFloat(9);
				int iCorpusActive = rs.getInt(10);
		
				if ((Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) ||
                    (htAuth.containsKey(iCorpusID+"")) || (bAdmin)) {
					// Since we cannot get the number of results from the Result Object, we
					//  need to count with a loop counter.   If results are 0 we want to return
					//  an exception (error) without printing any streams.   To do this, hold off
					//  on writing out the initial stream <CORPERA> until we have found at 
					//  least one result.   -MAP 10/14/01
					loop = loop + 1;
					if (loop == 1) { out.println ("<CORPORA>"); }

					out.println (" <CORPUS>");
					out.println ("   <CORPUSID>"+iCorpusID+"</CORPUSID>");
                    out.println ("   <CORPUS_NAME><![CDATA["+sCorpusName+"]]></CORPUS_NAME>");
                    out.println ("   <CORPUSDESC><![CDATA["+sCorpusDesc+"]]></CORPUSDESC>");
					out.println ("   <ROCF1>"+fRocF1+"</ROCF1>");
					out.println ("   <ROCF2>"+fRocF2+"</ROCF2>");
					out.println ("   <ROCF3>"+fRocF3+"</ROCF3>");
					out.println ("   <ROCC1>"+fRocC1+"</ROCC1>");
					out.println ("   <ROCC2>"+fRocC2+"</ROCC2>");
					out.println ("   <ROCC3>"+fRocC3+"</ROCC3>");
					out.println ("   <CORPUSACTIVE>"+iCorpusActive+"</CORPUSACTIVE>");
					out.println ("</CORPUS>");
				}
			}

			// If no results found, throw an exception
			if ( loop == 0) {
				out.println ( "<DEBUG>No corpora found</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_NO_CORPORA_THIS_DB);
			}

			rs.close();
		    stmt.close();

			if (loop > 0) {	out.println ("</CORPORA>"); }
			
			
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
