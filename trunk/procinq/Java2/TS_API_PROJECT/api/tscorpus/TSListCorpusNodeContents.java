package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import api.results.NodeDocumentResult;

import com.indraweb.database.*;
import com.iw.system.Node;
import com.iw.system.User;

/**
 *  Return all node objects that are descendents of the given node identifier.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  NodeID (optional)   Unique node (topic) identifier to begin search.  Default is root of the given corpus.
 *  @param  MaxDepth(optional)  Maximum number of topics to return.  Default is 1000.
 *
 *  @note   http://ITSERVER/itsapi/ts?fn=tscorpus.TSListCorpusNodeContents&CorpusID=100046&SKEY=1543463905

 *	@return	a series of node objects
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <NODES>
            <NODE>
                <NODEID>1087102</NODEID>
                <CORPUSID>300012</CORPUSID>
                <NODETITLE><![CDATA[ Controlled Release Technology, Agricultural]]> </NODETITLE>
                <PARENTID>1087101</PARENTID>
                <INDEXWITHINPARENT>1</INDEXWITHINPARENT>
                <DEPTHFROMROOT>0</DEPTHFROMROOT>
                <DATESCANNED>null</DATESCANNED>
                <DATEUPDATED>null</DATEUPDATED>
            </NODE>
            <NODE>
                <NODEID>1087110</NODEID>
                <CORPUSID>300012</CORPUSID>
                <NODETITLE><![CDATA[ Agronomic Properties and Nutrient Release Mechanism]]></NODETITLE>
                <PARENTID>1087102</PARENTID>
                <INDEXWITHINPARENT>2</INDEXWITHINPARENT>
                <DEPTHFROMROOT>1</DEPTHFROMROOT>
                <DATESCANNED>null</DATESCANNED>
                <DATEUPDATED>null</DATEUPDATED>
            </NODE>
 ...
  \endverbatim
 */
public class TSListCorpusNodeContents
{
	// TSListCorpusNodesContents
	//  Hierarchy of nodes within a given corpus.
	//  NodeID optional provides ability to start at a given point within the tree.

    public static User u;

	public static void handleTSapiRequest (
										   api.APIProps props,
										   PrintWriter out,
										   Connection dbc )
		throws Exception
	{

		try {
			String sKey = (String) props.get("SKEY", true);
			String sCorpusID = (String) props.get("CorpusID", true);
			String sNodeID = (String) props.get("NodeID");
			String sMaxDepth = (String) props.get("MaxDepth", "1000");
			String sCountries = (String) props.get("Country"); // optional
            u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            // fix countries, if applicable
            if (sCountries != null) {
				// Format sCountries to conduct SQL query correctly
				sCountries = "'"+sCountries+"'";
				sCountries = com.indraweb.util.UtilStrings.replaceStrInStr(sCountries, ",", "','");
            }

			// Hashtable to fill with nodes
			Hashtable htNodes = new Hashtable();
			int iMaxDepth = new Integer(sMaxDepth).intValue();

			// check authorization
			if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            // security now handled by the node emitter
            //if (!u.IsAuthorized(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			// load every node in this corpus into a node class and hash table
			Statement stmt = null; ResultSet rs = null;

            // if node id was not passed, get the top level node identifier
            if (sNodeID == null) {
				try {
					String sSQL = " select NodeId from node where parentid = -1 and corpusid = " + sCorpusID;
					stmt = dbc.createStatement(); rs = stmt.executeQuery (sSQL);

					rs.next(); sNodeID = rs.getString(1);
				} catch (Exception e) { api.Log.LogError(e);
                } finally { rs.close(); stmt.close(); }
			}

            try {
                if (sNodeID == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }

                String sSQL = Node.getSQL(dbc);
                if (sCountries != null) {
                    sSQL = sSQL + " WHERE linknodeid in (select nodeid from idracnode where nodecountry "+
                                  " in ("+sCountries+")) ";
                }
                sSQL = sSQL + " START WITH nodeid = "+sNodeID+" connect by prior linknodeid = parentid ";

                if (false) out.println("<SQL>"+sSQL+"</SQL>");
				stmt = dbc.createStatement();
				rs = stmt.executeQuery (sSQL);

 				int iCorpusID = new Integer(sCorpusID).intValue();

				while (rs.next()) {
                    Node n = new Node(rs);
					htNodes.put(n.get("NODEID"), n);
				}

				Enumeration eT = htNodes.elements();
				while (eT.hasMoreElements()) {
					Node n = (Node) eT.nextElement();
					Node p = (Node) htNodes.get(""+n.get("PARENTID"));

					if (p != null) { p.AddChild(n.get("NODEID")); }
					else {
						if (!n.get("PARENTID").equals("-1")) {
						api.Log.Log("Warning: Taxonomy integrity constraint.  "+
							 	    "Node: "+n.get("NODEID")+" has Parent: "+ n.get("PARENTID")+
								    " which does not exist in the database."); }
					}
				}

			} catch (Exception e) { api.Log.LogError(e);
			} finally {
                if (rs != null) { rs.close(); rs = null; }
                if (stmt != null) { stmt.close(); stmt = null; }
            }

			out.println("<NODES>");
			Node n = (Node) htNodes.get(sNodeID);
            if (n == null) { out.println("<ERROR><![CDATA[Node: "+sNodeID+" not found in hash]]></ERROR>"); }
            else { n.set("DEPTHFROMROOT", "0");
			n.emitXML(out, u, true);

			WriteChildren(out, n.GetChildren(), dbc, htNodes, 0, iMaxDepth, sCountries);  }
			out.println("</NODES>");
		} catch ( TSException tse )	{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}

	public static void WriteChildren(PrintWriter out, Vector vChildren, Connection dbc,
									 Hashtable htNodes, int iDepth, int iMaxDepth, String sCountries)
    throws Exception {
		if (iDepth >= iMaxDepth) { return; }

		vChildren = Sort(vChildren, htNodes);
		Enumeration eV = vChildren.elements();
		iDepth++;
		while (eV.hasMoreElements()) {
			Node n = (Node) eV.nextElement();
			n.set("DEPTHFROMROOT", ""+iDepth);

            if (sCountries != null) {
                out.println("<NODE>");
                n.emitXML(out, u, false);
                WriteDocument(n.get("LINKNODEID"), sCountries, dbc, out);
                out.println("</NODE>");
            }
			else { n.emitXML(out, u, true); }
			WriteChildren(out, n.GetChildren(), dbc, htNodes, iDepth, iMaxDepth, sCountries);
		}
	}

    // only retrieve documents within a specific set of countries
	public static void WriteDocument (String sNodeID, String sCountries, Connection dbc, PrintWriter out)
	throws Exception {
		Vector vResults = new Vector(); String sLastDocTitle = "";
		String sSQL = " select D.DocumentID, D.DocTitle, D.DocURL, N.DocSummary, "+
			 	      " N.Score1, N.Score2, N.Score3, N.Score4, N.Score5, N.Score6, "+
	   	 			  " to_char(N.DateScored, 'MM/DD/YYYY HH24:MI:SS'), D.GenreID from Document D, "+
					  " NodeDocument N where D.DocumentId = N.DocumentID AND N.NodeID = "+sNodeID+
					  " and D.Country in ("+sCountries+") "+
					  " order by N.IndexWithinNode, D.DocTitle asc";
        Statement stmt = null; ResultSet rs = null;

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery (sSQL);

            int loop = 0;
            while (rs.next()) {
                loop++; if (loop == 1) { out.println("<NODEDOCUMENTS>"); }
                String iDocID = rs.getString(1); String sDocTitle = rs.getString(2);
                String sDocURL = rs.getString(3); String sDocSummary = rs.getString(4);
                String fScore1 = rs.getString(5); String fScore2 = rs.getString(6);
                String fScore3 = rs.getString(7); String fScore4 = rs.getString(8);
                String fScore5 = rs.getString(9); String fScore6 = rs.getString(10);
                String sDateScored = rs.getString(11); String sFolderID = rs.getString(12);
                String sType = "0";

                if (sDocSummary == null) { sDocSummary = "(no abstract found)"; }

                if ((!sDocTitle.equals(sLastDocTitle)) && (!sLastDocTitle.equals(""))) {
                    Enumeration eV = vResults.elements();

                    PrintNodeDocumentResults(vResults, sCountries, out, dbc);
                    vResults = new Vector();
                }

                NodeDocumentResult ndr = new NodeDocumentResult(sNodeID, iDocID+"", sFolderID,
                                                                sDocTitle, sDocURL, sDocSummary,
                                                                fScore1,"0.0","0.0","0.0","0.0","0.0",
                                                                sDateScored);
                vResults.addElement(ndr); sLastDocTitle = sDocTitle;
            }
            if (!vResults.isEmpty()) { PrintNodeDocumentResults(vResults, sCountries, out, dbc); }
            if (loop != 0) { out.println("</NODEDOCUMENTS>"); }
        } catch (Exception e) { api.Log.LogError(e); throw e;
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (stmt != null) { stmt.close(); stmt = null; }
        }
	}

	public static void PrintNodeDocumentResults (Vector vResults, String sCountries,
												 PrintWriter out, Connection dbc)
		throws Exception {
		NodeDocumentResult ndr = (NodeDocumentResult) vResults.elementAt(0);

		out.println ("   <NODEDOCUMENT> ");
		out.println ("	    <NODEID>"+ndr.GetNodeID()+"</NODEID>");
		out.println ("	    <DOCUMENTID>"+ndr.GetDocID()+"</DOCUMENTID>");
		out.println ("      <FOLDERID>"+ndr.GetFolderID()+"</FOLDERID>");
		out.println ("      <DOCTITLE><![CDATA["+ndr.GetDocTitle()+"]]></DOCTITLE>");
		out.println ("      <DOCURLS>");

		Enumeration eV = vResults.elements();
		while (eV.hasMoreElements()) {
			NodeDocumentResult n = (NodeDocumentResult) eV.nextElement();
			out.println ("      <DOCURL><![CDATA["+n.GetDocURL()+"]]></DOCURL>");
		}

		out.println ("      </DOCURLS>");
		out.print ("      <DOCSUMMARY><![CDATA[");
		for (int k = 0; k < ndr.GetDocSummary().length(); k++) {
			int ichar = (int) ndr.GetDocSummary().charAt(k);
			if ((ichar != 13) && (ichar != 10)) { out.print(ndr.GetDocSummary().charAt(k)); }
		}
		out.println ("]]></DOCSUMMARY>");
		out.println ("      <SCORE1>"+ndr.GetScore1()+"</SCORE1>");
		out.println ("      <DATESCORED>"+ndr.GetDateScored()+"</DATESCORED>");
		out.println ("		<TYPE>0</TYPE>");
		out.println ("   <COUNTRIES>");

		eV = vResults.elements();
		while (eV.hasMoreElements()) {
			NodeDocumentResult n = (NodeDocumentResult) eV.nextElement();

			// print out associated countries
			String sSQL = " select country from Document where "+
					      " DocumentID = "+n.GetDocID()+" and "+
						  " Country in ("+sCountries+")";
			Statement stmt2 = dbc.createStatement();
			ResultSet rs2 = stmt2.executeQuery (sSQL);
			while ( rs2.next() ) {
				String s = rs2.getString(1);
				out.println("       <COUNTRY>"+s+"</COUNTRY>");
			} rs2.close(); stmt2.close();
		}

		out.println ("   </COUNTRIES>");
		out.println ("   </NODEDOCUMENT>");

		return;
	}

    private static void qs(Vector v) {

	  Vector vl = new Vector();                      // Left and right sides
	  Vector vr = new Vector();
	  Node el;
	  int    key;                                    // key for splitting

	  if (v.size() < 2) return;                      // 0 or 1= sorted

	  Node n = (Node) v.elementAt(0);
	  key = new Integer(n.get("NODEINDEXWITHINPARENT")).intValue();               // 1st element key

      // Start at element 1
      for (int i=1; i < v.size(); i++) {
         el = (Node) v.elementAt(i);
	     if (new Integer(el.get("NODEINDEXWITHINPARENT")).intValue() >= key) vr.addElement(el);// Add to right
	     else vl.addElement(el);                     // Else add to left
	  }

	  qs(vl);                                        // Recursive call left
	  qs(vr);                                        //    "        "  right
	  vl.addElement(v.elementAt(0));

	  addVect(v, vl, vr);
	}

	// Add two vectors together, into a destination Vector
	private static void addVect( Vector dest, Vector left, Vector right ) {

	   int i;
	   dest.removeAllElements();                     // reset destination

	   for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
	   for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
	}

	public static Vector Sort(Vector vChildren, Hashtable ht) {
		Vector v = new Vector();

		Enumeration eV = vChildren.elements();
		while (eV.hasMoreElements()) {
			String sNodeID = (String) eV.nextElement();
			Node n = (Node) ht.get(sNodeID);
			v.addElement(n);
		}
		qs(v);

		return v;
	}
}


