package api.tscorpus;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.execution.Session;
import com.iw.system.User;

/**
 * Returns a list of all corpora that current user (identified by their session)
 * has permission to access.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 *
 * @note http://itsserver/servlet/ts?fn=tscorpus.TSListCorpora&SKEY=993135977
 *
 * @return	a series of corpus objects \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <CORPORA>
 * <CORPUS>
 * <CORPUSID>2</CORPUSID>
 * <CORPUS_NAME>Wiley InterScience - Kirk-Othmer Encyclopedia of Chemical
 * Technology</CORPUS_NAME>
 * <CORPUSDESC>Updated regularly, Kirk-Othmer Online will stay current with the
 * latest developments in chemical technology and related fields.</CORPUSDESC>
 * <ROCF1>0.0</ROCF1>
 * <ROCF2>0.0</ROCF2>
 * <ROCF3>0.0</ROCF3>
 * <ROCC1>0.0</ROCC1>
 * <ROCC2>0.0</ROCC2>
 * <ROCC3>0.0</ROCC3>
 * <CORPUSACTIVE>1</CORPUSACTIVE>
 * </CORPUS>
 * ... \endverbatim
 */
public class TSListCorpora {

    public static void handleTSapiRequest(
            api.APIProps props,
            PrintWriter out,
            Connection dbc)
            throws Exception {

        //boolean bDebugging  = false;
        try {
            String sKey = (String) props.get("SKEY", true);
            User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

            String sSQL = " select CorpusID, Corpus_Name, CorpusDesc, "
                    + " Rocf1, Rocf2, Rocf3, Rocc1, Rocc2, Rocc3,"
                    + " CorpusActive, CorpusDomain, CorpusToClassify,"
                    + " CorpusAffinity, CorpusTerms, KISSMETRICID, ADSENSEID,"
                    + " GOOGLEANALYTICSID,TAGLINE,USEGOOGLEADS,DOMAINNAME, GOOGLESITEVERIFICATIONID, categorydomain.name"
                    + " from Corpus "
                    + " left join categorydomain on corpus.categoryid = categorydomain.id "
                    + " where CorpusActive != -1 order by lower(Corpus_Name) asc";
            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery(sSQL);


            int loop = 0;
            Hashtable htAuth = u.GetAuthorizedHash(out);

            while (rs.next()) {

                int iCorpusID = rs.getInt(1);
                String sCorpusName = rs.getString(2);
                String sCorpusDesc = rs.getString(3);
                float fRocF1 = rs.getFloat(4);
                float fRocF2 = rs.getFloat(5);
                float fRocF3 = rs.getFloat(6);
                float fRocC1 = rs.getFloat(7);
                float fRocC2 = rs.getFloat(8);
                float fRocC3 = rs.getFloat(9);
                int iCorpusActive = rs.getInt(10);
                int iCorpusDomain = rs.getInt(11);
                int iCorpusToClassify = rs.getInt(12);
                int iCorpusAffinity = rs.getInt(13);
                String iCorpusTerms = rs.getString(14);
                String kismetric = rs.getString(15);
                String adsense = rs.getString(16);
                String googleA = rs.getString(17);
                String tagline = rs.getString(18);
                String useGoogleAds = rs.getString(19);
                String domainName = rs.getString(20);
                String googleSiteVerificationId = rs.getString(21);
                String category = rs.getString(22);


                // Only return corpuses that this user is authorized to read.
                //if (bDebugging || ((Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) ||
                if ((Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none"))
                        || (htAuth.containsKey(iCorpusID + "")) || (u.IsMember(out))) {

                    // Since we cannot get the number of results from the Result Object, we
                    //  need to count with a loop counter.   If results are 0 we want to return
                    //  an exception (error) without printing any streams.   To do this, hold off
                    //  on writing out the initial stream <CORPORA> until we have found at
                    //  least one result.   -MAP 10/14/01
                    loop = loop + 1;
                    if (loop == 1) {
                        out.println("<CORPORA>");
                    }

                    out.println(" <CORPUS>");
                    out.println("   <CORPUSID>" + iCorpusID + "</CORPUSID>");
                    out.println("   <CORPUS_NAME><![CDATA[" + sCorpusName + "]]></CORPUS_NAME>");
                    out.println("   <CORPUSDESC><![CDATA[" + sCorpusDesc + "]]></CORPUSDESC>");
                    out.println("   <ROCF1>" + fRocF1 + "</ROCF1>");
                    out.println("   <ROCF2>" + fRocF2 + "</ROCF2>");
                    out.println("   <ROCF3>" + fRocF3 + "</ROCF3>");
                    out.println("   <ROCC1>" + fRocC1 + "</ROCC1>");
                    out.println("   <ROCC2>" + fRocC2 + "</ROCC2>");
                    out.println("   <ROCC3>" + fRocC3 + "</ROCC3>");
                    out.println("   <CORPUSACTIVE>" + iCorpusActive + "</CORPUSACTIVE>");
                    out.println("   <CORPUSDOMAIN>" + iCorpusDomain + "</CORPUSDOMAIN>");
                    out.println("   <CORPUSTOCLASSIFY>" + iCorpusToClassify + "</CORPUSTOCLASSIFY>");
                    out.println("   <CORPUSAFFINITY>" + iCorpusAffinity + "</CORPUSAFFINITY>");
                    out.println("   <CORPUSTERMS>" + iCorpusTerms + "</CORPUSTERMS>");
                    out.println("   <KISSMETRICID>" + kismetric + "</KISSMETRICID>");
                    out.println("   <ADSENSEID>" + adsense + "</ADSENSEID>");
                    out.println("   <GOOGLEANALYTICSID>" + googleA + "</GOOGLEANALYTICSID>");
                    out.println("   <TAGLINE>" + tagline + "</TAGLINE>");
                    out.println("   <USEGOOGLEADS>" + useGoogleAds + "</USEGOOGLEADS>");
                    out.println("   <DOMAINNAME>" + domainName + "</DOMAINNAME>");
                    out.println("   <GOOGLESITEVERIFICATIONID>" + googleSiteVerificationId + "</GOOGLESITEVERIFICATIONID>");
                    out.println("   <CATEGORY>" + category + "</CATEGORY>");
                    out.println("</CORPUS>");
                }
            }

            rs.close();
            stmt.close();


            // If no results found, throw an exception
            if (loop == 0) {
                out.println("<DEBUG>No corpora found</DEBUG>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_CORPORA_THIS_DB);
            }

            if (loop > 0) {
                out.println("</CORPORA>");
            }


        } /*
         <ERRORTERM ID=errorcode>
         <ERRORDESC>Error Description</ERRORDESC>
         </ERRORTERM>
         */ catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}
