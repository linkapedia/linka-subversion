package api.tscorpus;

import api.FileMetadata;
import java.io.*;
import java.sql.*;
import java.util.*;

import com.linkapedia.procinq.server.corpus.CorpusDao;
import com.linkapedia.procinq.server.utils.ImageUtil;
import org.apache.log4j.Logger;

public class TSAddImageCorpus {

    private static final Logger log = Logger.getLogger(TSAddImageCorpus.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        log.debug("TSAddImageCorpus From File");
        List<FileMetadata> listFileMetada = (List<FileMetadata>) props.get("inputstream", null);
        for (FileMetadata file : listFileMetada) {
            CorpusDao.saveImage(Integer.parseInt(file.getNodeId()),
                    file.getDescription(),
                    file.getUri(),
                    ImageUtil.getLinkapediaSiteImage(file.getImage()),
                    dbc);
        }
        out.println("<SUCCESS>images add ok</SUCCESS>");
    }
}
