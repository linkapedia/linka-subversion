package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;

import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 *  Undelete a corpus that was previously marked for deletion.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus identifier
 *
 *  @note   http://ItsServer/servlet/ts?fn=tscorpus.TSUndeleteCorpus&CorpusID=100005&SKEY=-1615182882

 *	@return	an error if unsuccessful
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CLASSLOAD>Fri Oct 26 17:47:46 EDT 2001</CLASSLOAD>
        <CALLCOUNT>38</CALLCOUNT>
        <TIMEOFCALL_MS>10</TIMEOFCALL_MS>
    </TSRESULT>
  \endverbatim
 */

public class TSUndeleteCorpus
{
	// TSUndeleteCorpus (sessionid, corpusid)
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID");
		try {
			String sSQL = "update Corpus set CorpusActive = 1 where CorpusId = "+sCorpusID;
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>The specified corpus does not exist</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_CORPUS_NOT_FOUND);
			}

		    stmt.close();
		}
		
		/*
		<ERRORTERM ID=errorcode>
		   <ERRORDESC>Error Description</ERRORDESC>
		</ERRORTERM>
		*/
		catch ( TSException tse )	
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
