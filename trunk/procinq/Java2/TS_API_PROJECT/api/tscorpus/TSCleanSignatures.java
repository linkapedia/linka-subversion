package api.tscorpus;

import java.io.*;
import java.sql.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import com.iw.db.ConnectionFactory;



import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class TSCleanSignatures {

    private static final Logger log = Logger.getLogger(TSCleanSignatures.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        log.debug("TSCleanSignatures");
        Connection con = null;
        try {
            con = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            NodeUtils nu = new NodeUtils(con);
            //validate AUTHORIZED
            String sKey = (String) props.get("SKEY", true);
            //get nodeid to start with the process
            String corpusId = (String) props.get("corpusid");

            if (corpusId == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (corpusId.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }

            clean_signatures_numbers(con, corpusId);
            clean_signatures_special_characters(con, corpusId);
            clean_signatures_more_two_words(con, corpusId);

            out.println("<SUCCESS>signatures cleaned ok</SUCCESS>");

        } catch (Exception e) {
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }


    }

    private static void clean_signatures_numbers(Connection con, String corpusId)
            throws SQLException {

        String sql = "delete from signature where nodeid in "
                + "(select nodeid from node where corpusid=?) "
                + "and REGEXP_LIKE(SIGNATUREWORD, '[[:digit:]]')";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, corpusId);
            pstmt.executeUpdate();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }

    }

    private static void clean_signatures_special_characters(Connection con, String corpusId)
            throws SQLException {
        String sql = "delete from signature where nodeid in "
                + "(select nodeid from node where corpusid=?) "
                + "and  REGEXP_LIKE (SIGNATUREWORD, '[[:punct:]]')";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, corpusId);
            pstmt.executeUpdate();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    private static void clean_signatures_more_two_words(Connection con, String corpusId)
            throws SQLException {

        String sql = "delete from signature where nodeid in "
                + "(select nodeid from node where corpusid=?) "
                + "and length(SIGNATUREWORD) - length(replace(SIGNATUREWORD, ' ', '')) + 1 > 2";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, corpusId);
            pstmt.executeUpdate();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
}
