package api.tscorpus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;

import api.emitxml.EmitGenXML_ErrorInfo;

import com.indraweb.database.*;

/**
 * This API call has been deprecated.  Security information is now stored inside the LDAP component.
 *
 * @note    This API call has been deprecated.  Security information is now stored inside the LDAP component.
 */
public class TSRemoveCorpusAccess
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		try { throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_OUT_OF_DATE); }
		catch ( TSException tse ) {	EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); } 
	}
}
