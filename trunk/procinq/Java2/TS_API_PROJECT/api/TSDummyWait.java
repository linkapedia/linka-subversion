package api;
/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Mar 8, 2003
 * Time: 4:25:35 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */

import java.util.*;
import java.io.*;
import java.sql.*;
import java.lang.reflect.*;

import javax.servlet.*;
import javax.servlet.http.*;

import api.Log;
import api.TSException;
import api.TSDBException;
import api.TSNoSuchMethodException;
import api.TSNoSuchObjectException;
import api.util.MultiPartMisc;

import com.oreilly.servlet.multipart.*;
import com.indraweb.util.UtilFile;

import api.SerialProps;

import com.indraweb.execution.Session;
import com.iw.classification.Data_WordsToNodes;

import com.indraweb.ir.clsStemAndStopList;

/**
 *	Main servlet class for IndraWeb Taxonomy Server API
 *	All rights reserved.
 *	(c)IndraWeb.com,Inc. 2001-2002
 *
 *	@authors hkon, mpuscar
 *
 *	@param	req	Contains the name-value pairs:
 *						fn=functionname
 *						parm1=parameterValue1
 *						parm2=parameterValue2 ...
 *
 *	@param	res	response is written as XML to the output stream of res

 *	@return	xml via output stream
 */

public class TSDummyWait extends HttpServlet
{
    // This is a "convenience method" so, after overriding it there is no need to call the super()
    // This function takes the place of the initializeSession method in the engine project.


    // *******************************************
    public void doGet(HttpServletRequest req,
                      HttpServletResponse res)
            throws ServletException, IOException
    {
        int i = 1;
        if (i == 1)
        {

            PrintWriter out = res.getWriter();
            String sWaitTime = req.getParameter("WAITTIME");


            int iWaittimeMS = Integer.parseInt( sWaitTime );
            int iTotalTimeRemaining = iWaittimeMS;
            int iWaitThisLoopIteration = -1;
            int iWaitThisLoopIteration_Default_NumMSBetweenUpdates = 60000;  // ms between update
            long lStart = System.currentTimeMillis() ;
            out.println("<FNSTART>" + "" + "</FNSTART>");
            System.out.println("<FNSTART>" + "" + "</FNSTART>");

            int iLoop = 0;
            while (true)
            {
//                res.setHeader("Refresh", "5");
                res.setContentType("text/html");
                iLoop++;

                if (iTotalTimeRemaining > 0)
                {
                    if (iTotalTimeRemaining > iWaitThisLoopIteration_Default_NumMSBetweenUpdates )
                        iWaitThisLoopIteration = iWaitThisLoopIteration_Default_NumMSBetweenUpdates;
                    else
                        iWaitThisLoopIteration = iTotalTimeRemaining;

                    iTotalTimeRemaining -= iWaitThisLoopIteration;
                    out.println("<WAITITERSTART>" + iWaitThisLoopIteration + "</WAITITERSTART>");
                    System.out.println("<WAITITERSTART>" + iWaitThisLoopIteration + "</WAITITERSTART>");
                    com.indraweb.util.clsUtils.pause_this_many_milliseconds(iWaitThisLoopIteration);
                    out.println("<ITERCOMPLETED>" + iWaitThisLoopIteration + "</ITERCOMPLETED>");
                    System.out.println("<ITERCOMPLETED>" + iWaitThisLoopIteration + "</ITERCOMPLETED>");
                } else
                    break;
            } // while true
            long lCLockMS = System.currentTimeMillis() - lStart;
            out.println("<CLOCKMS>" + lCLockMS + "</CLOCKMS>");
            System.out.println("<CLOCKMS>" + lCLockMS + "</CLOCKMS>");
            out.println("<WAITCOMPLETED>" + iWaittimeMS + "</WAITCOMPLETED>\r\n");
            System.out.println("<WAITCOMPLETED>" + iWaittimeMS + "</WAITCOMPLETED>\r\n");

            return;
        }

        // *****************************************************

    }  // doGet
    public void destroy()
    {
    }

}