package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Get all corpora that are associated with the specified thesaurus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusID   Unique thesaurus identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSGetThesaurusCorpora&ThesaurusID=1&SKEY=993135977

 *	@return	a series of corpus objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <CORPORA>
            <CORPUS>
                <CORPUSID>2</CORPUSID>
                <CORPUS_NAME>Wiley InterScience - Kirk-Othmer Encyclopedia of Chemical Technology</CORPUS_NAME>
                <CORPUSDESC>Updated regularly, Kirk-Othmer Online will stay current with the latest developments in chemical technology and related fields.</CORPUSDESC>
                <ROCF1>0.0</ROCF1>
                <ROCF2>0.0</ROCF2>
                <ROCF3>0.0</ROCF3>
                <ROCC1>0.0</ROCC1>
                <ROCC2>0.0</ROCC2>
                <ROCC3>0.0</ROCC3>
                <CORPUSACTIVE>1</CORPUSACTIVE>
            </CORPUS>
 ...
  \endverbatim
 */
public class TSGetThesaurusCorpora
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sTID = (String) props.get ("ThesaurusID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED); }

			String sSQL = " select C.CorpusID, C.Corpus_Name, C.CorpusDesc, "+
						  " C.Rocf1, C.Rocf2, C.Rocf3, C.Rocc1, C.Rocc2, C.Rocc3, C.CorpusActive"+			
						  " from Corpus C, CorpusThesaurus T where T.ThesaurusId = "+sTID+
						  " and C.CorpusID = T.CorpusID";
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				if (loop == 1) { out.println ("<CORPORA>"); }

				int iCorpusID = rs.getInt(1);
				String sCorpusName = rs.getString(2);
				String sCorpusDesc = rs.getString(3);
				float fRocF1 = rs.getFloat(4);
				float fRocF2 = rs.getFloat(5);
				float fRocF3 = rs.getFloat(6);
				float fRocC1 = rs.getFloat(7);
				float fRocC2 = rs.getFloat(8);
				float fRocC3 = rs.getFloat(9);
				int iCorpusActive = rs.getInt(10);
				
				out.println (" <CORPUS>");
				out.println ("   <CORPUSID>"+iCorpusID+"</CORPUSID>");
				out.println ("   <CORPUS_NAME>"+sCorpusName+"</CORPUS_NAME>");
				out.println ("   <CORPUSDESC>"+sCorpusDesc+"</CORPUSDESC>");
				out.println ("   <ROCF1>"+fRocF1+"</ROCF1>");
				out.println ("   <ROCF2>"+fRocF2+"</ROCF2>");
				out.println ("   <ROCF3>"+fRocF3+"</ROCF3>");
				out.println ("   <ROCC1>"+fRocC1+"</ROCC1>");
				out.println ("   <ROCC2>"+fRocC2+"</ROCC2>");
				out.println ("   <ROCC3>"+fRocC3+"</ROCC3>");
				out.println ("   <CORPUSACTIVE>"+iCorpusActive+"</CORPUSACTIVE>");
				out.println ("</CORPUS>");
			}
		
			rs.close();
		    stmt.close();
			
			if (loop == 0) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			out.println ("</CORPORA>");
			
		}
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
