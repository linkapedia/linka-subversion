package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Properties;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Get all of the thesaurus terms in a given thesaurus.  Optional parameters allow selection by starting letter.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusID   Unique thesaurus identifier
 *  @param  Like (optional) Return only thesaurus terms starting with this argument
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSGetWords&ThesaurusID=2&SKEY=993135977

 *	@return	a series of thesaurus term objects.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
    <TSRESULT>
        <WORDS>
            <WORD>
                <WORDID>4900</WORDID>
                <WORDNAME>couch</WORDNAME>
            </WORD>
 ...
  \endverbatim
 */
public class TSGetWords
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sTID = (String) props.get ("ThesaurusID", true);
		String sLike = (String) props.get("Like");
        String sExact = (String) props.get("ExactMatch");
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        LDAP_Connection lc = new LDAP_Connection(u.GetDN(), u.GetPassword(), out);
		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            if (u == null) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_SESSION_EXPIRED); }

			String sSQL = " select distinct(R.wordanchor), W.ThesaurusWord from "+
						  " thesaurusrelations R, thesaurusword W where R.thesaurusid = "+sTID+
						  " and W.wordid = R.wordanchor";

			if (sLike != null) {
                if (sExact != null) { sSQL = sSQL + " and lower(W.ThesaurusWord) = LOWER('"+sLike+"')"; }
				else if (sLike.equals("Other")) {
					sSQL = sSQL+" and (W.ThesaurusWord not like 'a%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'b%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'c%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'd%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'e%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'f%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'g%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'h%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'i%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'j%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'k%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'l%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'm%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'n%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'o%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'p%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'q%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'r%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 's%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 't%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'u%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'v%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'w%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'x%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'y%' ";
					sSQL = sSQL+" and W.ThesaurusWord not like 'z%')";
				} else { sSQL = sSQL+" and W.ThesaurusWord like LOWER('"+sLike+"%')"; }
			}
			
			Statement stmt = dbc.createStatement();	
			ResultSet rs = stmt.executeQuery (sSQL);
			
			int loop = 0;
			while ( rs.next() ) {
				loop = loop + 1;
				if (loop == 1) { out.println ("<WORDS>"); }
				
				int iWordId = rs.getInt(1);
				String sWordName = rs.getString(2);	
		
				out.println ("   <WORD> ");
				out.println ("   <WORDID>"+iWordId+"</WORDID>");
				out.println ("   <WORDNAME>"+sWordName+"</WORDNAME>");
				out.println ("   </WORD>");
			}
		
			rs.close();
		    stmt.close();
			
			if (loop == 0) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND); }
			
			out.println ("</WORDS>");
			
		}
		
		catch ( TSException tse ) { EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); }
        finally { lc.close(); }
	}
}
