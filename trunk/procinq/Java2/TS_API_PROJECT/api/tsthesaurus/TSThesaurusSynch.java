package api.tsthesaurus;

import com.iw.system.User;
import api.TSException;
import api.Log;
import api.emitxml.EmitGenXML_ErrorInfo;

import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;

// TSSynchronize
//
// This synchronized function kicks off the SynchThesaurus stored proc to load a thesaurus from
// indraweb thesaurus tables into Oracle
// given a db name, user, and pass for the stored proc to be able to kick off a shell function
// for thesaurus load from a file

/**
 *  Synchronize all thesauri with the database.  Thesaurus changes are not made to the database real time.
 *   This function may take several minutes and should be run during off peak system time.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSThesaurusSynch&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <<SUCCESS>Thesauri synchronized successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSThesaurusSynch
{
    public static synchronized void handleTSapiRequest(api.APIProps props,
                                                       PrintWriter out,
                                                       Connection dbc)
            throws Exception
    {

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

        if ( !u.IsMember(out) )
        {
            Exception e = new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            Log.LogError("Permissions error in TSThesaurusSynch", e);
            throw e;
        }

        String sDBString = com.indraweb.execution.Session.cfg.getProp("DBString_API_OracleJDBC");
        String s[] = sDBString.split(":");

        String sDBName = (String) props.get("DBName", s[5]);
        String sUserDB = (String) props.get("UserDB", com.indraweb.execution.Session.cfg.getProp("DBString_API_User"));
        String sPassDB = (String) props.get("PassDB", com.indraweb.execution.Session.cfg.getProp("DBString_API_Pass"));

        api.Log.Log("Synchronizing thesauri using: "+sDBName+" "+sUserDB+" "+sPassDB);
        CallableStatement cstmt = null;
        try
        {
            cstmt = dbc.prepareCall("begin SynchThesaurus (:1, :2, :3) ; end;");
            cstmt.setString(1, sDBName);
            cstmt.setString(2, sUserDB);
            cstmt.setString(3, sPassDB);
            cstmt.executeUpdate();
            api.Log.Log("post SynchThesaurus call");
            cstmt.close();

            out.println("<SUCCESS>Thesauri synchronized successfully.</SUCCESS>");
        } catch (Exception e)
        {
            api.Log.LogError("Error executing SynchThesaurus script", e);
            if (cstmt != null)
                cstmt.close();
            throw e;
        }
    }
}


