package api.tsthesaurus;


import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.Log;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;
import com.iw.guiservershared.TableModelXMLBeanSerializable;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;

import sql4j.CQLInterpreter.*;

/**
 *  Remove a relationship between two thesaurus terms.  Note altering the relationship will automatically
 *   purge the signature cache of any corpus using the given thesaurus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID (optional)    Unique corpus identifier that constrains the set of corpora considered.
 *	@param  ThesaurusID (optional)  Unique thesaurus identifier that constrains the set of thesauri considered.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSThesaurusNodeReport&ThesaurusID=1&CorpusID=1&SKEY=993135977

 *	@return	Table model can be in the data struct - as a serialized object
 */
public class TSThesaurusNodeReport
{

    public static void handleTSapiRequest (
            api.APIProps props ,
            PrintWriter pw ,
            Connection dbc)
            throws Exception
    {
        String sKey = (String) props.get ("SKEY" , true);
        User u = (User) com.indraweb.execution.Session.htUsers.get (sKey);
        if (u == null)
        {
            Exception e = new TSException (EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            Log.LogError ("in thesaurus report" , e);
            throw e;
        }

        if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

        String sSQLFinal = null;
        boolean bProp_BeanXML = props.getbool("BEANXML" , true);

        String sProp_CorpusID = (String) props.get ("corpusid" , null);
        String sProp_ThesaurusID = (String) props.get ("thesaurusid" , null);
        String sProp_RowMinIncl = (String) props.get ("RowMinIncl" , null);
        String sProp_RowMaxIncl = (String) props.get ("RowMaxIncl" , null);

        // CORPUSID
        String sConstraintCorpusID = "";
        if (sProp_CorpusID != null && !sProp_CorpusID.trim().toLowerCase().equals("all")) {
            sConstraintCorpusID = " n.corpusid = " + sProp_CorpusID + " and " ;
        }

        // THESAURUSID
        String sConstraintThesaurusID = "";
        if (sProp_ThesaurusID != null && !sProp_ThesaurusID.trim().toLowerCase().equals("all")) {
            sConstraintThesaurusID = " tr.thesaurusid = " + sProp_ThesaurusID + " and " ;
        }
        // MIN ROW
        String sRowMinConstraint = "";
        if ( sProp_RowMinIncl != null )
        {
            sRowMinConstraint = " and  rownum >= " + sProp_RowMinIncl + " ";
        }

        // MAX ROW
        String sRowMaxConstraint = "";
        if ( sProp_RowMaxIncl != null )
        {
            sRowMaxConstraint = " and  rownum <= " + sProp_RowMaxIncl + " ";
        }

        String sCorpusListAllowed = u.GetAuthorizedCorporaAsString (new PrintWriter (System.out));
        //String sCorpusListAllowed = " 5 ";
        sSQLFinal = "select * from ( select distinct corpus_name, thesaurusname, nodetitle, n.nodeid, tw.thesaurusword " +
        //" from node n, thesaurusrelations tr, thesaurusword tw, thesaurus t, corpus c, corpusthesaurus ct" +
        " from node n, thesaurusrelations tr, thesaurusword tw, thesaurus t, corpus c " +
        " where  lower(n.nodetitle) = tw.thesaurusword and " +
        sConstraintCorpusID +
        sConstraintThesaurusID +
        " n.corpusid = c.corpusid " +
        " and  c.corpusid in (" + sCorpusListAllowed + ") " +
        //SECURITY " and  t.thesaurusID in (" + sThesListAllowed + ") " +
        " and  t.thesaurusID = tr.thesaurusid " +
        //" and  ct.corpusid = c.corpusid " +
        //" and  ct.thesaurusID = t.thesaurusID " +
        sRowMinConstraint +
        sRowMaxConstraint +
        " and  tw.wordID = tr.wordanchor ) where rownum < 500 ";

        System.out.println("sSQLFinal in TSThesaurusNodeReport [\r\n" + sSQLFinal + "\r\n]");
        TableModelXMLBeanSerializable tm = new TableModelXMLBeanSerializable();
        // test String[] sArrColNamesPAssedIn = {"hbkcol1","hbkcol2","hbkcol3","hbkcol4"};
        String[] sArrColNamesPAssedIn = {"Corpus","Thesaurus","Node Title","Node ID","Thesaurus Term"};
        if ( !bProp_BeanXML)
        {
            tm.fillDataWithSQLCall ( sSQLFinal,  dbc, sArrColNamesPAssedIn,
                    pw , "THESAURUSREPORT", "REPORTLINE", bProp_BeanXML);
        }
        else // object graph bean xml form
        {
            tm.fillDataWithSQLCall ( sSQLFinal,  dbc, sArrColNamesPAssedIn,
                    null , null, null, bProp_BeanXML);

            //System.out.print("sql query [" + sProp_SQLquery+ "]");
            pw.println("<OBJECT_TABLEMODEL_BEANXML><![CDATA[");
            tm.emitIWXMLData(pw);
            pw.println("]]></OBJECT_TABLEMODEL_BEANXML>");
        }

    }
}
