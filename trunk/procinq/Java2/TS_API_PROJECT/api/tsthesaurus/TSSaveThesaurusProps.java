package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Save updated thesaurus properties to the server.  \
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  ThesaurusID   Unique identifier of the thesaurus to be removed
 *  @param  ThesaurusName New name of the thesaurus
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSSaveThesaurusProps&ThesaurusID=6&ThesaurusName=Test&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Thesaurus updated successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSSaveThesaurusProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sThID = (String) props.get ("ThesaurusID", true);
        String sThNa = (String) props.get ("ThesaurusName", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            // Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = "update Thesaurus set ThesaurusName = '"+sThNa+"' where ThesaurusID = "+sThID;
			Statement stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Update Thesaurus table failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

		    stmt.close();
			out.println("<SUCCESS>Thesaurus updated successfully.</SUCCESS>");
		}
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}
	}
}
