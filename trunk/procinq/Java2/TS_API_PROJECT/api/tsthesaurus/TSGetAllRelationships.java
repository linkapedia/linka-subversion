package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

//import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.*;

import com.iw.system.User;
import com.iw.system.Node;
import com.indraweb.database.*;
import api.Log;

/**
 * Given a thesaurus ID, get all words in the thesaurus and their relationships
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  ThesaurusID  Unique thesaurus identifier of this thesaurus.
 *
 *  @note    http://ITSSERVER/itsapi/ts?fn=tsnode.TSGetAllRelationships&ThesaurusID=1&SKEY=9919294812

 *	@return	SUCCESS tag, if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
     <OBJECTS>
         <OBJECT>
            <WORD>couch</WORD>
            <RELATIONSHIP>1</RELATIONSHIP>
            <PREDICATE>sofa</PREDICATE>
        </OBJECT>
     </OBJECTS>
  </TSRESULT>
  \endverbatim
 */
public class TSGetAllRelationships {
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sThesID = (String) props.get("ThesaurusID", true);
        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            if (u == null) throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            Statement stmt = null; ResultSet rs = null;

            // for authorization purposes, need to know the corpus id of this node
            String sSQL = "SELECT a.thesaurusword, b.relationship, c.thesaurusword FROM "+
               "thesaurusword a, thesaurusrelations b, thesaurusword c WHERE "+
               "a.wordid = b.wordanchor AND c.wordid = b.wordrelated AND b.thesaurusid = "+sThesID;

            try {
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                int loop = 0;

                while (rs.next()) {
                    loop++; if (loop == 1) out.println("<OBJECTS>");
                    out.println("<OBJECT>");
                    out.println("   <WORD><![CDATA["+rs.getString(1)+"]]></WORD>");
                    out.println("   <RELATIONSHIP>"+rs.getString(2)+"</RELATIONSHIP>");
                    out.println("   <PREDICATE><![CDATA["+rs.getString(3)+"]]></PREDICATE>");
                    out.println("</OBJECT>");
                }

                if (loop > 0) out.println("</OBJECTS>");
                else throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NO_ROWS_FOUND);

            } catch (Exception e) { throw e;
            } finally { rs.close(); stmt.close(); rs = null; stmt = null; }

        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        }
    }
}
