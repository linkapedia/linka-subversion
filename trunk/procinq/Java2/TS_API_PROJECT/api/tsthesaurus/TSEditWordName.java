package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Change the name of a thesaurus term.  You may not rename a thesaurus term to a name already used in the system.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  OldName   Current thesaurus term name.
 *  @param  NewName   New thesaurus term name.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSEditWordName&OldName=Test&NewName=Testing&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Thesaurus term renamed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditWordName
{
    // changed: 12/10/03 -- actually edit the name
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sOldName = (String) props.get ("OldName", true);
		String sNewName = (String) props.get("NewName", true);
		String sKey = (String) props.get("SKEY", true);

		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        Statement stmt = null;
		try {
            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

            // Ensure user is a member of the admin group
			if (!u.IsMember(out)) {
                out.println("<NOTAUTHORIZED>You are not authorized to use this function</NOTAUTHORIZED>");
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL="update ThesaurusWord set thesaurusword = lower('"+sNewName+"') where LOWER(thesaurusword)=LOWER('"+sOldName+"')";
			stmt = dbc.createStatement();
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Attempt to edit thesaurus term failed!</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
			out.println("<SUCCESS>Thesaurus term renamed successfully.</SUCCESS>");
		}
		catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		} finally {
            if (stmt != null) { stmt.close(); stmt = null; }
        }

	}
}
