package api.tsthesaurus;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Add a corpus relationship to the given thesaurus.   Note that invoking this function will automatically
 *   invalidate the signature cache of the given corpus.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CorpusID    Unique corpus (taxonomy) identifier.
 *  @param  ThesaurusID   Unique thesaurus identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsthesaurus.TSAddThesaurusCorpus&CorpusID=4&ThesaurusID=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Corpus thesaurus relationship added successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddThesaurusCorpus
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCorpusID = (String) props.get ("CorpusID", true);
		String sThID = (String) props.get ("ThesaurusID", true);
		String sKey = (String) props.get("SKEY", true);
		User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

		try {
			if (!u.IsAdmin(sCorpusID, out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            if (!com.indraweb.execution.Session.license.isAuthorized(com.iw.license.IndraLicense.THESAURUS))
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);

			String sSQL = "insert into CorpusThesaurus (CorpusId, ThesaurusId) values ("+sCorpusID+","+sThID+")";
			Statement stmt = dbc.createStatement();	

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Insert into CorpusThesaurus table failed</DEBUG>"); 
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
			// purge signature cache files
			api.tspurge.TSPurgeSigCache.PurgeSignatures(sCorpusID, out, dbc);

		    stmt.close();

            out.println("<SUCCESS>Corpus thesaurus relationship added successfully.</SUCCESS>");
		}
		catch ( TSException tse )
		{
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out ); 
		}
	}
}
