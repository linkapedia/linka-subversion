package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Remove an ROC Setting object.  This will also remove any associated buckets.  Any corpora that are
 *   using this ROC Setting object will be redirected to the default ROC settings.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID     Unique ROC setting identifier.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSRemoveROCSettings&rid=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>ROC setting removed successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSRemoveROCSettings
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sRID = (String) props.get ("rid", true);
        Statement stmt = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }
            if (sRID.equals("1")) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            dbc.setAutoCommit(false);

            // first reset any corpus pointing to this setting to point to setting 1, instead
            String sSQL = "update corpus set rocSettingID = 1 where rocSettingID = "+sRID;
            stmt = dbc.createStatement();
            // If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
                dbc.rollback();
                out.println ( "<DEBUG>Deletion of rocsetting failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}
            stmt = null;

			sSQL = " delete from ROCsetting where rocSettingID = "+sRID;
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
                dbc.rollback();
                out.println ( "<DEBUG>Deletion of rocsetting failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>ROC setting removed successfully.</SUCCESS>");
		    stmt.close();
            dbc.commit();

		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { dbc.setAutoCommit(true); if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
