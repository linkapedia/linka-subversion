package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Assign an ROC setting to the specified corpus.  A corpus may only have one ROC setting.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  CID     Unique corpus (taxonomy) identifier.
 *  @param  RID     Unique ROC setting identifier
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSAddCorpusROC&CD=4&RID=1&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>Corpus ROC updated successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSAddCorpusROC
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sCID = (String) props.get ("cid", true);
        String sRID = (String) props.get ("rid", true);
        Statement stmt = null; ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " update corpus set rocsettingid = "+sRID+" where corpusid = "+sCID;
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Update corpus ROC failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>Corpus ROC updated successfully.</SUCCESS>");
		    stmt.close();
		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
