package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Edit the properties of an existing ROC setting object.  To modify the buckets for an ROC Setting, use the
 *   api::tsrocsettings::TSEditROCBucketProps function.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *  @param  RID Unique ROC setting identifier of the target to be modified.
 *	@param  Ratio (optional) Cost ratio of true negatives to true positives
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSEditROCSettingsProps&ratio=50&usedefault=true&SKEY=993135977

 *	@return	the new ROCsetting identifier contained in a SUCCESS tag.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>6</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSEditROCSettingsProps
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
        String sRID = (String) props.get ("rid", true);
		String sRatio = (String) props.get ("ratio", true);
        Statement stmt = null; ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " update ROCsetting set ratioTNtoTP = "+sRatio+" where rocSettingID = "+sRID;
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Update rocsetting failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>"+sRID+"</SUCCESS>");
		    stmt.close();
		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
