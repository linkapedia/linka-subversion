package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Create an ROC cutoff bucket.  Each bucket represents a tier of scoring within the server.  Typically three
 *   buckets are used, but more or less may be applied at the administrator's discretion.  Default bucket values
 *   are defined for ROCSetting 1 during system installation.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  ClassifierID     Unique identifier corresponding to the classifier used in this ROC range.
 *  @param  ROCSettingID     Unique ROC setting identifier for this bucket.
 *  @param  StarCount   The tier or layer that this bucket represents (typically 1, 2, or 3).
 *  @param  Cost    The cost ratio of false positives to false negatives.
 *  @param  CutOff  The cut off score for this bucket range (GTE)
 *  @param  Score (optional)    The "score1" representation for this bucket.  Default values are 100 for 3, 75 for 2, and 50 for 1.
 *  @param  TruePos (optional)  True positive percentage in this range.
 *  @param  FalsePos (optional) False positive percentage in this range.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSCreateROCBucket&classifierid=1&rocsettingid=1&starcount=1&cost=0.2&cutoff=0.3&SKEY=993135977

 *	@return	SUCCESS tag if successful.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>ROC Bucket created successfully.</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSCreateROCBucket
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sRID = (String) props.get ("rocsettingid", true);
        String sCID = (String) props.get ("classifierid", true);

        String sStar = (String) props.get ("starcount", true);
        String sCost = (String) props.get ("cost", true);
        String sCut = (String) props.get ("cutoff", true);

        // optional fields
        String sScore = (String) props.get ("score");
        String sTrue = (String) props.get ("truepos");
        String sFalse = (String) props.get ("falsepos");

        Statement stmt = null; ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

			String sSQL = " insert into rocBucketLocalClassifier (rocsettingID, classifierID, starCount, costRatioFPtoFN, cutoffScoreGTE";
            if (sScore != null) { sSQL = sSQL + ", score1Val"; }
            if (sTrue != null) { sSQL = sSQL + ", truePos"; }
            if (sFalse != null) { sSQL = sSQL + ", falsePos"; }
            sSQL = sSQL + ") values ("+sRID+","+sCID+","+sStar+","+sCost+","+sCut;
            if (sScore != null) { sSQL = sSQL + ","+sScore; }
            if (sTrue != null) { sSQL = sSQL + ","+sTrue; }
            if (sFalse != null) { sSQL = sSQL + ","+sFalse; }
            sSQL = sSQL + ")";

			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
            //out.println("<SQL>"+sSQL+"</SQL>");
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Create rocbucket failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}

			out.println("<SUCCESS>ROC Bucket created successfully.</SUCCESS>");
		    stmt.close();
		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
