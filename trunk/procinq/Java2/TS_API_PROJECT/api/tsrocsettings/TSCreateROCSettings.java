package api.tsrocsettings;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.statics.QuerySubmitter;
import api.TSException;
import api.security.*;
import api.emitxml.EmitGenXML_ErrorInfo;

import com.iw.system.User;
import com.indraweb.database.*;

/**
 *  Create an ROC setting object.  Without relationships, ROC setting objects are meaningless.  They are meant
 *   to be created in conjunction with several ROC buckets and associated classifiers to construct an ROC model.
 *
 *	@authors Indraweb Inc, All Rights Reserved.
 *
 *	@param	SKEY    Session key corresponding to the current user session.
 *	@param  Ratio   Cost ratio of true negatives to true positives
 *  @param  UseDefault (optional)   If this parameter is specified, initial ROC buckets will be defined from system defaults.
 *
 *  @note    http://itsserver/servlet/ts?fn=tsrocsettings.TSCreateROCSettings&ratio=50&usedefault=true&SKEY=993135977

 *	@return	the new ROCsetting identifier contained in a SUCCESS tag.
 *  \verbatim
  <?xml version="1.0" encoding="UTF-8" ?>
  <TSRESULT>
      <SUCCESS>6</SUCCESS>
  </TSRESULT>
  \endverbatim
 */
public class TSCreateROCSettings
{
	public static void handleTSapiRequest ( api.APIProps props, PrintWriter out, Connection dbc )
		throws Exception
	{
		String sRatio = (String) props.get ("ratio", true);
        String sDefault = (String) props.get ("usedefault");
        Statement stmt = null; ResultSet rs = null;

		try {
			String sKey = (String) props.get("SKEY", true);
			User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

			// Ensure user is a member of the admin group
			if (!u.IsMember(out)) { throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED); }

            String sSQL = " select rocSetting_seq.nextval from dual ";
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sSQL); rs.next();
            String rocID = rs.getString(1);

            rs.close(); stmt.close(); rs = null; stmt = null;

			sSQL = " insert into ROCsetting (rocsettingID, ratioTNtoTP) values ("+rocID+", "+sRatio+")";
			stmt = dbc.createStatement();

			// If query statement failed, throw an exception
			if (stmt.executeUpdate (sSQL) == 0) {
				out.println ( "<DEBUG>Create rocsetting failed</DEBUG>");
				throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
			}  stmt.close(); stmt = null;

            // if default is true, set defaults
            if (sDefault != null) {
                sSQL = "insert into ROCBUCKETLOCALCLASSIFIER select "+rocID+", starCount, costRatioFPtoFN, "+
                       "cutOffScoreGTE, classifierID, score1Val, truePos, falsePos from ROCBUCKETLOCALCLASSIFIER "+
                       "where rocSettingID = 1";
                stmt = dbc.createStatement();

                // If query statement failed, throw an exception
                if (stmt.executeUpdate (sSQL) == 0) {
                    out.println ( "<DEBUG>Create local classifier rocsettings failed</DEBUG>");
                    throw new TSException (EmitGenXML_ErrorInfo.ERR_TS_ERROR_IN_DB_INTERACTION);
                }
            }

			out.println("<SUCCESS>"+rocID+"</SUCCESS>");
		    stmt.close();
		} 	catch ( TSException tse ) {
			EmitGenXML_ErrorInfo.emitException ( "TSException", tse, out );
		}   finally { if (rs != null) { rs.close(); rs = null; } if (stmt != null) { stmt.close(); stmt = null; }}
	}
}
