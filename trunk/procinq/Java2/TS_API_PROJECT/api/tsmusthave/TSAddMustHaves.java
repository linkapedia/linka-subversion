package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.ir.clsStemAndStopList;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;

/**
 * Given a series of words separated by commas, remove common words and add to
 * the musthave table.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 *
 * @param	SKEY Session key corresponding to the current user session.
 * @param nodeid The unique node identifier
 * @param terms A list of must have terms, separated by commas
 *
 * @note
 * http://ITSSERVER/itsapi/ts?fn=tsnode.TSAddMustHaves&NodeID=2000&Term=Test&SKEY=9919294812
 *
 * @return	SUCCESS tag, if successful. \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <SUCCESS>Must have words added successfully.</SUCCESS>
 * </TSRESULT>
 * \endverbatim
 */
public class TSAddMustHaves {

    private static boolean useCommon = false;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String sTerms = (String) props.get("Terms", true);
        String sLang = (String) props.get("lang", "EN");
        String sCommon = (String) props.get("common");
        String sKey = (String) props.get("SKEY", true);
        String musthaveType = (String) props.get("musthavetype");
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        if (sCommon != null) {
            useCommon = true;
        }
        //String[] terms = sTerms.split(",");

        Statement stmt = null;
        ResultSet rs = null;
        try {
            // for authorization purposes, need to know the corpus id of this node
            String sSQL = "select CorpusId from Node where NodeID = " + sNodeID;
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);
            rs.next();

            int iCorpusId = rs.getInt(1);
            rs.close();
            stmt.close();
            rs = null;
            stmt = null;

            if (!u.IsAdmin(iCorpusId, out)) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }
        } catch (TSException tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        int loop = 0;

        String terms[] = new String[1];
        terms[0] = sTerms;

        for (int i = 0; i < terms.length; i++) {
            // cross reference each term with the common words list.  if it exists, don't add it
            Hashtable ht = clsStemAndStopList.getHTStopWordList();
            if (ht.containsKey(terms[i].toLowerCase()) && useCommon) {
                api.Log.Log("Warning!  Term " + terms[i] + " exists in the commonwords.txt file.  Insert ignored.");
            } else {
                try {
                    String musthaveTableName = "";
                    if (musthaveType == null || musthaveType.equals("1")) {
                        musthaveTableName = "musthave";
                    } else if (musthaveType.equals("2")) {
                        musthaveTableName = "nodemusthavegate";
                    } else if (musthaveType.equals("3")) {
                        musthaveTableName = "canthave";
                    }
                    String sSQL = "insert into " + musthaveTableName + " (NodeId, MustWord, Lang) values (" + sNodeID + ", '" + terms[i] + "', '" + sLang + "')";
                    api.Log.Log("SQL: " + sSQL);
                    stmt = dbc.createStatement();

                    // If query statement failed, throw an exception
                    if (stmt.executeUpdate(sSQL) == 0) {
                        out.println("<DEBUG>Failed to insert: " + sNodeID + " " + terms[i] + " </DEBUG>");
                    } else {
                        loop++;
                    }
                } catch (Exception e) {
                    out.println("<DEBUG>Failed to insert: " + sNodeID + " " + terms[i] + " </DEBUG>");
                } finally {
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                }
            }
        }

        out.println("<SUCCESS>" + loop + " must have words added successfully.</SUCCESS>");

        try {
            String sSQL = "update Node set NodeLastSync = SYSDATE, dateupdated = sysdate where NodeID = " + sNodeID;
            stmt = dbc.createStatement();
            // If query statement failed, throw an exception
            if (stmt.executeUpdate(sSQL) == 0) {
                out.println("<DEBUG>Failed to update node status.</DEBUG>");
            }
        } catch (Exception tse) {
            EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }
}
