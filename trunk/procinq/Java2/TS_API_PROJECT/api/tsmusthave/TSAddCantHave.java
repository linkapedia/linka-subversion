package api.tsmusthave;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import com.iw.db.ConnectionFactory;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;

/**
 * add cantHaves second rules.
 *
 * @authors Andres Restrepo, ningun derecho reservado :).
 *
 * @return	SUCCESS tag, if successful. \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <SUCCESS>images add ok</SUCCESS>
 * </TSRESULT>
 * \endverbatim
 */
public class TSAddCantHave {

    private static final Logger log = Logger.getLogger(TSAddCantHave.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        log.debug("TSAddCantHave");
        /*
         * obtengo mi propia coneccion.
         * Dad al César lo que es del César, y a Andres lo que es de Andres
         */
        Connection con = null;
        try {
            con = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            NodeUtils nu = new NodeUtils(con);
            //validate AUTHORIZED
            String sKey = (String) props.get("SKEY", true);
            //get nodeid to start with the process
            String nodeid = (String) props.get("nodeid");
            String depth = (String) props.get("depth");
            String wordToPut = (String) props.get("wordToPut");

            if (nodeid == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nodeid.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            Map<String, String> nodes = nu.getNodesToLevel(nodeid, Integer.parseInt(depth));

            DecimalFormat twoDigits = new DecimalFormat("0");
            long percent = 0;
            int contPr = 0;
            int numData = nodes.size();

            for (Map.Entry<String, String> node_aux : nodes.entrySet()) {
                String key = node_aux.getKey();
                List<String> must = Arrays.asList(wordToPut);
                nu.insertCanthaves(Integer.valueOf(key), must);
                percent = ((contPr + 1) * 100) / numData;
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                log.debug("finished one proccess: " + percent + "%");
                contPr++;
                out.flush();
            }
            out.println("<SUCCESS>CantHaves inserted ok</SUCCESS>");
        } catch (Exception e) {
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }
}