package api.tsmusthave;

import java.util.List;

/**
 *
 * @author andres
 */
public class RegexHandler {

    private String regex;
    private int group;
    private List<RegexHandler> regexStep2;

    public RegexHandler(String regex, int group) {
        this.regex = regex;
        this.group = group;
    }

    public RegexHandler() {
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public List<RegexHandler> getRegexStep2() {
        return regexStep2;
    }

    public void setRegexStep2(List<RegexHandler> regexStep2) {
        this.regexStep2 = regexStep2;
    }
}
