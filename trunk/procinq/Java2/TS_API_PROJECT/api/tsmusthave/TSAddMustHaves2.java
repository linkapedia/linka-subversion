package api.tsmusthave;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.db.ConnectionFactory;
import com.linkapedia.procinq.server.musthave.MusthaveGeneration;
import com.linkapedia.procinq.server.musthave.MusthaveNextStep;
import com.linkapedia.procinq.server.musthave.MusthaveUseCustom;
import com.linkapedia.procinq.server.musthave.MusthaveUseWords;
import com.linkapedia.procinq.server.musthave.MusthavesUtils;
import com.linkapedia.procinq.server.node.Node;

public class TSAddMustHaves2 {

    private static final int PAGINATION = 1000;

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        Connection sqlConnection = null;
        try {
            sqlConnection = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            /*Class.forName("oracle.jdbc.driver.OracleDriver");
             sqlConnection = DriverManager.getConnection(
             "jdbc:oracle:thin:@link3.chvjczfef6ad.us-east-1.rds.amazonaws.com:1521:link3", "sbooks", "sbooks");*/

            String sKey = (String) props.get("SKEY", true);
            String nodeid = (String) props.get("NODEID");
            String depth = (String) props.get("DEPTH");
            String wordToPut = (String) props.get("WORDTOPUT");
            String customMusthave = (String) props.get("CUSTOMMUSTHAVE");

            if (nodeid == null || nodeid.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            List<MusthaveGeneration> musthavesGeneration = new ArrayList<>();
            if (wordToPut != null) {
                Map<String, Object> filterUseWords = new HashMap<>();
                filterUseWords.put("WORDTOPUT", wordToPut);
                MusthaveGeneration musthaveUseWords = new MusthaveUseWords(filterUseWords);
                musthavesGeneration.add(musthaveUseWords);
            }
            if (customMusthave != null) {
                Map<String, Object> filterCustomHave = new HashMap<>();
                filterCustomHave.put("CUSTOMMUSTHAVE", customMusthave);
                MusthaveGeneration musthaveUseCustom = new MusthaveUseCustom(filterCustomHave);
                musthavesGeneration.add(musthaveUseCustom);
            }

            int from = 1;
            int to = PAGINATION;
            List<Node> nodes;
            MusthaveNextStep musthaveNextStep = new MusthaveNextStep(musthavesGeneration);
            do {
                nodes = MusthavesUtils.getNodes(Integer.parseInt(nodeid),
                        Integer.parseInt(depth), from, to, sqlConnection);
                musthaveNextStep.insertMushaves(nodes, sqlConnection);
                from = to + 1;
                to += PAGINATION;
            } while (!nodes.isEmpty());
            out.println("<SUCCESS>musthaves inserted ok</SUCCESS>");
        } finally {
            sqlConnection.close();
        }
    }

    /*public static void main(String[] args) throws Exception {
     api.APIProps props = new APIProps();
     props.put("NODEID", "17521982");
     props.put("DEPTH", "-1");
     props.put("WORDTOPUT", "-1");
     props.put("CUSTOMMUSTHAVE", "jaleo");
     props.put("SKEY", "765476235276");
     handleTSapiRequest(props, null, null);
     }*/
}