package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.db.ConnectionFactory;
import com.iw.system.MusthaveReportModel;
import com.iw.system.WrapperListMusthaveReport;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TSMusthaveProcessStatus {
   
    
    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {
        
        String rownum = (String) props.get("rownum");
        String requestId = (String) props.get("requestid");
        if (rownum == null) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        if (rownum.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        List<MusthaveReportModel> report = getMusthaveReport(rownum, requestId);
        WrapperListMusthaveReport modelReport = new WrapperListMusthaveReport();
        modelReport.setList(report);
        String data = Transformation.getData(modelReport);
        out.println(data);
    }
    
    private static List<MusthaveReportModel> getMusthaveReport(String rowNum,
            String requestId) throws Exception {
        Connection conn = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
        List<MusthaveReportModel> listToReturn = new ArrayList<>();
        MusthaveReportModel musthaveModel;
        String sql = "";
        if (requestId == null || requestId.isEmpty()) {
            sql = " SELECT * from (SELECT *   FROM ( "
                    + "SELECT page.*, rownum as row_num FROM ( "
                    + "SELECT * from SBOOKS.MUSTUHAVEPROCESSSTATUS order by requestid DESC "
                    + ") page "
                    + ") "
                    + "WHERE row_num <= ?)";
        } else {
            sql = " SELECT * from (SELECT *   FROM ( "
                    + "SELECT page.*, rownum as row_num FROM ( "
                    + "SELECT * from SBOOKS.MUSTUHAVEPROCESSSTATUS"
                    + " where requestid = ? order by requestid DESC "
                    + ") page "
                    + ") "
                    + "WHERE row_num <= ?)";
        }
        PreparedStatement pstmt = null;
        ResultSet result = null;
        
        try {
            pstmt = conn.prepareStatement(sql);
            if (requestId == null || requestId.isEmpty()) {
                pstmt.setInt(1, Integer.parseInt(rowNum));
            } else {
                pstmt.setInt(1, Integer.parseInt(requestId));
                pstmt.setInt(2, Integer.parseInt(rowNum));
            }
            
            result = pstmt.executeQuery();
            while (result.next()) {
                musthaveModel = new MusthaveReportModel();
                musthaveModel.setRequestId(String.valueOf(result.getLong("REQUESTID")));
                musthaveModel.setTimestamp(String.valueOf(result.getLong("TIMESTAMP")));
                musthaveModel.setCorpus(result.getString("CORPUS"));
                musthaveModel.setNode(result.getString("NODE"));
                musthaveModel.setToLevel(String.valueOf(result.getString("TOLEVEL")));
                musthaveModel.setStatus(result.getString("STATUS"));
    
                listToReturn.add(musthaveModel);
            }
            return listToReturn;
        } catch (Exception e) {
            throw new SQLException("Error in the process status in Musthave report", e);
        } finally {
            try {
                if (result != null) {
                    result.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
            }
        }
    }
}
