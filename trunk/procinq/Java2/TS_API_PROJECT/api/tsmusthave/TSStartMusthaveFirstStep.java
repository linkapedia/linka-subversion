package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.iw.db.ConnectionFactory;
import com.linkapedia.procinq.server.corpus.Corpus;
import com.linkapedia.procinq.server.corpus.CorpusDao;
import com.linkapedia.procinq.server.musthave.ProccessStatusDao;
import com.linkapedia.procinq.server.musthave.queue.MessageMusthaveProcess;
import com.linkapedia.procinq.server.musthave.queue.ProducerMusthaveProcess;
import com.linkapedia.procinq.server.node.Node;
import com.linkapedia.procinq.server.node.NodeDao;
import com.npstrandberg.simplemq.MessageQueue;
import com.npstrandberg.simplemq.MessageQueueService;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Date;

public class TSStartMusthaveFirstStep {

    private static final String QUEUE_NAME = "MUSTHAVEQUEUE";
    private static final String INITIAL_STATUS = "WAITING";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out,
            Connection dbc) throws Exception {

        String sKey = (String) props.get("SKEY", true);
        String nodeid = (String) props.get("nodeid");
        String toLevel = (String) props.get("tolevel");
        String useDescription = (String) props.get("UseNodeDescription");
        String useTitleFull = (String) props.get("UseNodeTitleFull");

        if (nodeid == null || nodeid.isEmpty()) {
            throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
        }
        Connection connection = null;
        try {
            connection =
                    ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            Node node = NodeDao.getNode(Integer.parseInt(nodeid), connection);
            Corpus corpus = CorpusDao.getCorpus(node.getCorpusId(), connection);

            long requestId = ProccessStatusDao.saveStatusAndResturnRequestId(
                    new Date().getTime(), corpus.getCorpusId() + "," + corpus.getCorpusName(),
                    node.getNodeId() + ", " + node.getNodeTitle(),
                    Integer.parseInt(toLevel), INITIAL_STATUS, connection);

            MessageMusthaveProcess message = new MessageMusthaveProcess();
            message.setRequestId(requestId);
            message.setNodeId(Long.parseLong(nodeid));
            message.setToLevel(Integer.parseInt(toLevel));
            message.setUseDescription(Boolean.valueOf(useDescription));
            message.setUseTitleFull(Boolean.valueOf(useTitleFull));

            ProducerMusthaveProcess producer = new ProducerMusthaveProcess();
            MessageQueue queue = MessageQueueService.getMessageQueue(QUEUE_NAME);
            producer.setMessageQueue(queue);
            producer.sendMessage(message);

            out.println("<SUCCESS>" + requestId + "</SUCCESS>");
        } finally {
            connection.close();
        }
    }
}
