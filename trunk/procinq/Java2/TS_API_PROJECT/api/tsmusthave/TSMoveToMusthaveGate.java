package api.tsmusthave;

import java.io.*;
import java.sql.*;
import java.util.*;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import api.util.NodeUtils;
import com.iw.db.ConnectionFactory;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;

/**
 * add musthaves second rules.
 *
 * @authors Andres Restrepo, All Rights Reserved.
 *
 * @return	SUCCESS tag, if successful. \verbatim
 * <?xml version="1.0" encoding="UTF-8" ?>
 * <TSRESULT>
 * <SUCCESS>images add ok</SUCCESS>
 * </TSRESULT>
 * \endverbatim
 */
public class TSMoveToMusthaveGate {

    private static final Logger log = Logger.getLogger(TSMoveToMusthaveGate.class);

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc)
            throws Exception {

        log.debug("TSMoveToMusthaveGate");

        Connection con = null;
        try {
            con = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            NodeUtils nu = new NodeUtils(con);
            //validate AUTHORIZED
            String sKey = (String) props.get("SKEY", true);
            //get nodeid to start with the process
            String nodeid = (String) props.get("nodeid");
            String depth = (String) props.get("depth");

            if (nodeid == null) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nodeid.isEmpty()) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_MISSING_PARAMETERS);
            }
            if (nu.validateAuthorization(Integer.parseInt(nodeid), sKey, out) == 0) {
                throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
            }

            Map<String, String> nodes = nu.getNodesToLevel(nodeid, Integer.parseInt(depth));
            Map<String, List<String>> nodeMusthaves = getMusthaves(con, nodes);


            DecimalFormat twoDigits = new DecimalFormat("0");
            long percent = 0;
            int contPr = 0;
            int numData = nodeMusthaves.size();

            for (Map.Entry<String, List<String>> nodeMusthave : nodeMusthaves.entrySet()) {
                String key = nodeMusthave.getKey();
                List<String> value = nodeMusthave.getValue();
                if (value == null || value.isEmpty()) {
                    continue;
                }
                nu.insertMusthavesGate(Integer.valueOf(key), value);
                updateNodeMusthaveInheritance(con, key);

                percent = ((contPr + 1) * 100) / numData;
                out.println("<STATUS>" + twoDigits.format(percent) + "</STATUS>");
                log.debug("finished one proccess: " + percent + "%");
                contPr++;
                out.flush();
            }
            out.println("<SUCCESS>musthavesgate inserted ok</SUCCESS>");
        } catch (Exception e) {
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    private static void updateNodeMusthaveInheritance(Connection con, String nodeId)
            throws SQLException {

        String sql = "UPDATE NODE SET NODEMUSTHAVEGATEINHERITANCE = 0 WHERE NODEID = ?";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, nodeId);
            pstmt.executeUpdate();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    private static Map<String, List<String>> getMusthaves(Connection con, Map<String, String> nodes)
            throws IllegalArgumentException, SQLException {

        if (nodes == null || nodes.isEmpty()) {
            throw new IllegalArgumentException("nodes can't be null");
        }
        Map<String, List<String>> mapResult = new HashMap<>();
        List<String> musthaves = null;
        String sql = "SELECT NODEID, MUSTWORD FROM MUSTHAVE WHERE NODEID=?";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            for (Map.Entry<String, String> node : nodes.entrySet()) {
                musthaves = new ArrayList<>();
                pstmt.setString(1, node.getKey());
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    String mustword = rs.getString("MUSTWORD");
                    musthaves.add(mustword);
                }
                mapResult.put(node.getKey(), musthaves);
            }
        } finally {
            if (rs != null){
                rs.close();
            }
            if (pstmt != null){
                pstmt.close();
            }
        }
        return mapResult;
    }
}