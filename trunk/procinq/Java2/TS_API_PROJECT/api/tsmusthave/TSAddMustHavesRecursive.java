/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Jul 13, 2006
 * Time: 11:05:58 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package api.tsmusthave;

import api.TSException;
import api.emitxml.EmitGenXML_ErrorInfo;
import com.indraweb.utils.oracle.UtilClob;
import com.iw.db.ConnectionFactory;
import com.iw.system.Node;
import com.iw.system.User;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TSAddMustHavesRecursive {

    private static boolean useCommon = false;
    private static String lang = "EN";

    public static void handleTSapiRequest(api.APIProps props, PrintWriter out, Connection dbc) throws Exception {
        String sNodeID = (String) props.get("NodeID", true);
        String depth = (String) props.get("depth");
        String suseNode = (String) props.get("useNode");
        String suseParent = (String) props.get("useParent");
        String susePhrases = (String) props.get("usePhrases");
        String suseRegexTitle = (String) props.get("useRegexTitle");
        String suseRegexDesc = (String) props.get("useRegexDesc");
        String sTerms = (String) props.get("Terms");
        String sRegex = (String) props.get("Regex");
        String sCommon = (String) props.get("common");
        String choiceGroupOption = (String) props.get("choiceGroupOption");
        lang = (String) props.get("lang", "EN");

        String sKey = (String) props.get("SKEY", true);
        User u = (User) com.indraweb.execution.Session.htUsers.get(sKey);

        try {
            /*Hack hack, when we have a lot nodes the api connection close the stream
             I am getting our connection now
             also, I need that connection is closed after use
             */
            dbc = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);
            if (sCommon != null) {
                useCommon = true;
            }

            Statement stmt = null;
            ResultSet rs = null;
            int iCorpusId = -1;
            try {
                // for authorization purposes, need to know the corpus id of this node
                String sSQL = "select CorpusId from Node where NodeID = " + sNodeID;
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);
                rs.next();

                iCorpusId = rs.getInt(1);
                rs.close();
                stmt.close();
                rs = null;
                stmt = null;

                if (!u.IsAdmin(iCorpusId, out)) {
                    throw new TSException(EmitGenXML_ErrorInfo.ERR_TS_NOT_AUTHORIZED);
                }
            } catch (TSException tse) {
                EmitGenXML_ErrorInfo.emitException("TSException", tse, out);
                return;
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
            int loop = 0;

            Vector v = new Vector(); // collect the nodes

            if (depth != null) {
                try {
                    String sSQL = Node.getSQL(dbc) + " where corpusid =  " + iCorpusId + " and depthfromroot = " + depth + " order by lower(nodetitle) asc";
                    stmt = dbc.createStatement();
                    rs = stmt.executeQuery(sSQL);

                    while (rs.next()) {
                        v.add(new Node(rs));
                    }
                } catch (Exception e) {
                    throw e;
                } finally {
                    if (rs != null) {
                        rs.close();
                        rs = null;
                    }
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                }
            } else {
                try {
                    String sSQL = Node.getSQL(dbc) + " start with n.nodeid = " + sNodeID + " connect by prior nodeid = parentid";

                    Connection dbc1 = ConnectionFactory.createConnection(ConnectionFactory.ORACLE);

                    stmt = dbc1.createStatement();
                    rs = stmt.executeQuery(sSQL);

                    while (rs.next()) {
                        v.add(new Node(rs));
                    }
                } catch (Exception e) {
                    throw e;
                } finally {
                    if (rs != null) {
                        rs.close();
                        rs = null;
                    }
                    if (stmt != null) {
                        stmt.close();
                        stmt = null;
                    }
                }
            }

            int total = 0;
            for (int i = 0; i < v.size(); i++) { // for each node, do the deed
                total = total + addMustHave(
                        suseNode,
                        suseParent,
                        susePhrases,
                        suseRegexTitle,
                        suseRegexDesc,
                        sTerms,
                        (Node) v.elementAt(i),
                        sRegex,
                        dbc,
                        choiceGroupOption);
            }

            out.println("<SUCCESS>" + total + " must have words added successfully.</SUCCESS>");
        } catch (Exception e) {
            throw e;
        } finally {
            if (dbc != null) {
                dbc.close();
            }
        }
    }

    private static int addMustHave(
            String useNode,
            String useParent,
            String usePhrases,
            String suseRegexTitle,
            String suseRegexDesc,
            String sTerms,
            Node n,
            String Regex,
            Connection dbc,
            String choiceGroupOption) throws Exception {
        if (n.get("NODESTATUS").equals("2")) {
            api.Log.Log("Warning: Node " + n.get("NODETITLE") + " is locked, skipping.");
            return 0;
        }

        Statement stmt = null;
        ResultSet rs = null;
        int loop = 0;
        String sNodeID = n.get("NODEID");
        if (choiceGroupOption.equals("0")) {
            sTerms = removeWordsInGroupCharacters(sTerms);
        } else if (choiceGroupOption.equals("1")) {
            sTerms = removeGroupCharacters(sTerms);
        }

        // if terms is not null, add terms
        if (sTerms != null && !sTerms.isEmpty()) {
            String[] terms = sTerms.split(",");
            for (int i = 0; i < terms.length; i++) {
                if (insertMustHave(terms[i].toLowerCase(), sNodeID, dbc)) {
                    loop++;
                }
            }
        }

        String titleNode = n.get("NODETITLE");
        if (useNode != null) {
            if (choiceGroupOption.equals("0")) {
                titleNode = removeWordsInGroupCharacters(titleNode);
            } else if (choiceGroupOption.equals("1")) {
                titleNode = removeGroupCharacters(titleNode);
            }
            String[] terms = titleNode.toLowerCase().split(" ");

            for (int i = 0; i < terms.length; i++) {
                if (insertMustHave(terms[i], sNodeID, dbc, false)) {
                    loop++;
                }
            }
        }

        titleNode = n.get("NODETITLE");
        if (usePhrases != null) {
            if (choiceGroupOption.equals("0")) {
                titleNode = removeWordsInGroupCharacters(titleNode);
                if (insertMustHave(titleNode.toLowerCase(), sNodeID, dbc, true)) {
                    loop++;
                }
            } else if (choiceGroupOption.equals("1")) {
                List<String> wordsAux = getCharactersInGroupCharacters(titleNode);
                titleNode = removeWordsInGroupCharacters(titleNode);

                //insert title sobrante
                if (insertMustHave(titleNode.toLowerCase(), sNodeID, dbc, true)) {
                    loop++;
                }
                //insert the other words
                for (String cad : wordsAux) {
                    if (insertMustHave(removeGroupCharacters(cad), sNodeID, dbc, false)) {
                        loop++;
                    }
                }

            }

        }

        if (useParent != null) {
            Node p = null;
            try {
                String sSQL = Node.getSQL(dbc) + " where nodeid = " + n.get("PARENTID");
                stmt = dbc.createStatement();
                rs = stmt.executeQuery(sSQL);

                while (rs.next()) {
                    p = new Node(rs);
                }
            } catch (Exception e) {
                throw e;
            } finally {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }

            if (p != null) {
                String[] terms = p.get("NODETITLE").toLowerCase().split(" ");

                for (int i = 0; i < terms.length; i++) {
                    if (insertMustHave(terms[i], sNodeID, dbc)) {
                        loop++;
                    }
                }

                if (usePhrases != null) {
                    if (insertMustHave(p.get("NODETITLE").toLowerCase(), sNodeID, dbc)) {
                        loop++;
                    }
                }
            }
        }

        // if the regex pattern is not null, use it to extract new must have terms (this code is trash)
        if (Regex != null) {
            String firstletter = n.get("NODETITLE").substring(0, 1);
            Regex = Regex.replaceAll("NODETITLE", "[" + firstletter.toUpperCase()
                    + firstletter.toLowerCase() + "]"
                    + n.get("NODETITLE").substring(1).toLowerCase());

            // first, get this node's source
            String sNodeSource = "";
            try {
                sNodeSource = UtilClob.getClobStringFromNodeData(
                        new Long(sNodeID).longValue(), dbc);
            } catch (Exception e) {
                e.printStackTrace(System.out);
            } // ignore an error here, return empty string

            // then extract all patterns and add as must have terms
            Pattern pat = Pattern.compile(Regex);
            Matcher mat = pat.matcher(sNodeSource);
            while (mat.find()) {
                insertMustHave(mat.group().toLowerCase(), sNodeID, dbc);
                loop++;
            }

            sNodeSource = null;
        }

        //[NEW] use regex nodetitle
        titleNode = n.get("NODETITLE");
        if (suseRegexTitle != null) {
            List<String> terms = regexNodeTitle(titleNode);
            if (choiceGroupOption.equals("0")) {
                for (String s : terms) {
                    s = removeWordsInGroupCharacters(s).trim().toLowerCase();
                    if (!s.isEmpty()) {
                        if (insertMustHave(s, sNodeID, dbc)) {
                            loop++;
                        }
                    }
                }
            } else if (choiceGroupOption.equals("1")) {
                for (String s : terms) {
                    List<String> wordsAux = getCharactersInGroupCharacters(s);
                    s = removeWordsInGroupCharacters(s).trim().toLowerCase();

                    //insert left over String 
                    if (!s.isEmpty()) {
                        if (insertMustHave(s, sNodeID, dbc)) {
                            loop++;
                        }
                    }
                    //insert the other words
                    for (String cad : wordsAux) {
                        cad = removeGroupCharacters(cad).toLowerCase().trim();
                        if (insertMustHave(cad, sNodeID, dbc, false)) {
                            loop++;
                        }
                    }
                }
            }
        }

        //[NEW] use regex description
        if (suseRegexDesc != null) {
            String stopRegex = "(\\band\\b|\\bor\\b|,|\\bin\\b|\\bis\\b|\\.|;|:|!|$|\\(|\\[)";
            String normalizeTitle = n.get("NODETITLE").trim();
            List<RegexHandler> list = new ArrayList<>();
            RegexHandler regexPatternsp = new RegexHandler();
            regexPatternsp.setRegex("\\b" + normalizeTitle + "(\\S|\\s{0,})(,|\\.|\\s{0,})(\\S|\\s{0,})\\((.*?)\\)");
            regexPatternsp.setGroup(4);
            List<RegexHandler> listaux = new ArrayList<>();
            listaux.add(new RegexHandler("\\balso called\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\balso known as\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bcommonly called\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bformerly known as\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bformerly\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\breferred to as\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\breferred as\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bpreviously known as\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bpreviously\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\boften abbreviated to\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\boften abbreviated as\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bshort for\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bacronym\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\boriginally\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("\\bor\\b(.*?)" + stopRegex, 1));
            listaux.add(new RegexHandler("(.*?)" + stopRegex, 1));

            regexPatternsp.setRegexStep2(listaux);
            list.add(regexPatternsp);

            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\balso called\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\balso known as\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bcommonly called\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bformerly known as\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bformerly\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\breferred to as\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\breferred as\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bknown also in .* as(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bpreviously known as\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bpreviously\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\boften abbreviated to\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\boften abbreviated as\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bshort for\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bacronym\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\boriginally\\b(.*?)" + stopRegex,
                    4));
            list.add(new RegexHandler("\\b" + normalizeTitle + "\\b(\\S|\\s{0,})"
                    + "(,|\\s|\\bor\\b|\\band\\b|\\.)(\\S|\\s{0,})\\bor\\b(.*?)" + stopRegex,
                    4));

            String re = regexDesc(n.get("NODEDESC"), list, "").trim().toLowerCase();
            if (!re.isEmpty()) {
                if (insertMustHave(re, sNodeID, dbc)) {
                    loop++;
                }
            }
        }

        try {
            String sSQL = "update Node set NodeLastSync = SYSDATE, dateupdated = sysdate where NodeID = " + sNodeID;
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
        } catch (Exception tse) {
            throw tse;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        return loop;
    }

    private static boolean insertMustHave(String term, String nodeid, Connection dbc) throws Exception {
        return insertMustHave(term, nodeid, dbc, true);
    }

    private static boolean insertMustHave(String term, String nodeid, Connection dbc, boolean ignoreCommon) throws Exception {
        PreparedStatement stmt = null;
        term = term.replaceAll(",", "");
        term = term.replaceAll("&", "");
        if (term.length() < 1) {
            return false;
        }

        // cross reference each term with the common words list.  if it exists, don't add it
        Hashtable ht = com.indraweb.execution.Session.stopList.getHTStopWordList();

        // what's new: if this is a phrase, split it into words and check each for common
        if ((term.indexOf(" ") > -1) && useCommon && !ignoreCommon) {
            String[] words = term.split(" ");
            boolean common = true;

            for (int i = 0; i < words.length; i++) {
                if (!ht.containsKey(words[i].toLowerCase())) {
                    common = false;
                }
            }

            if (common) {
                api.Log.Log("Warning!  All words in phrase " + term + " exists in the commonwords.txt file.  Insert ignored.");
                return false;
            }
        }

        if (ht.containsKey(term.toLowerCase()) && useCommon && !ignoreCommon) {
            api.Log.Log("Warning!  Term " + term + " exists in the commonwords.txt file.  Insert ignored.");
            return false;
        }

        try {
            //String sSQL = "insert into musthave (NodeId, MustWord, Lang) values (" + nodeid + ", '" + term + "', '" + lang + "')";
            String sSQL = "insert into musthave (NodeId, MustWord, Lang) values(?,?,?)";
            stmt = dbc.prepareStatement(sSQL);
            stmt.setString(1, nodeid);
            stmt.setString(2, term);
            stmt.setString(3, lang);

            // If query statement failed, throw an exception
            if (stmt.executeUpdate() == 0) {
                api.Log.Log("{Warning} Failed to insert: " + nodeid + " " + term);
            } else {
                return true;
            }
        } catch (Exception e) {
            api.Log.Log("{Warning} Failed to insert: " + nodeid + " " + term + " " + e.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }

        return false;
    }

    /**
     * example (andres) = ""
     *
     * @param term
     * @return
     */
    private static String removeWordsInGroupCharacters(String term) {
        Pattern pattern = Pattern.compile("([\\(\\[\\{].*?(([\\)\\]\\}])|$))");
        return term.replaceAll(pattern.toString(), "");
    }

    /**
     * example (andres) = andres
     *
     * @param term
     * @return
     */
    private static String removeGroupCharacters(String term) {
        Pattern pattern = Pattern.compile("[\\(\\)\\{\\}\\[\\]]");
        return term.replaceAll(pattern.toString(), "");
    }

    /**
     * example (andres) es muy teso si señor (tipo) = es muy teso si señor |
     * list = andres,tipo
     *
     * @param term
     * @return
     */
    private static List<String> getCharactersInGroupCharacters(String term) {
        Pattern pattern = Pattern.compile("([\\(\\[\\{].*?(([\\)\\]\\}])|$))");
        Matcher mat = pattern.matcher(term);
        List<String> words = new ArrayList<>();
        while (mat.find()) {
            words.add(mat.group());
        }
        return words;
    }

    /**
     * extract REGEX pattern from nodetitle
     *
     * @param title
     * @return
     */
    private static List<String> regexNodeTitle(String title) {
        if (title == null || title.isEmpty()) {
            return new ArrayList<>();
        }
        List<String> termsToReturn = new ArrayList<>();
        // remove THE/the from term
        title = title.toLowerCase().trim();
        if (title.contains("the")) {
            title = title.replaceAll("\\bthe\\b", "");
        }
        //split by separators
        termsToReturn.addAll(Arrays.asList(title.split(
                "(\\bfor\\b|\\band\\b|\\bor\\b|\\bin\\b|\\bon\\b|\\bof a\\b|,|"
                + "\\bare\\b|\\bof the\\b|\\bwith\\b)")));
        return termsToReturn;
    }

    /**
     * get word testing patterns recursively
     *
     * @param a
     * @param regex
     * @param beforeResult
     * @return
     */
    private static String regexDesc(String a, List<RegexHandler> regex, String beforeResult) {
        Pattern pattern;
        String result = beforeResult;
        for (RegexHandler reg : regex) {
            try {
                pattern = Pattern.compile(reg.getRegex(), Pattern.CASE_INSENSITIVE);
                Matcher mat = pattern.matcher(a);
                if (mat.find()) {
                    result = mat.group(reg.getGroup());
                    result = result.trim();
                    // not empty 
                    if (result.isEmpty()) {
                        continue;
                    }
                    if (reg.getRegexStep2() != null) {
                        String auxResult = regexDesc(result, reg.getRegexStep2(), result);
                        if (auxResult.isEmpty()) {
                            continue;
                        } else {
                            return auxResult;
                        }
                    }
                    break;
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        /*end validations
         * use result = "" to continue searching the others patterns
         */
        //not number
        if (result.matches(".*\\d.*")) {
            result = "";
        }
        //max characters
        if (5 < result.split("\\s+").length) {
            result = "";
        }
        return result;
    }
}
