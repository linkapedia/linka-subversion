package com.iw.apishell;

import java.util.*;
import java.io.*;
import java.sql.*;

// HASHTREE.java
//  Written by: Indraweb, Inc.
//  All rights reserved.
//
// HASHTREE objects insert from Hashtables and allow parent child relationships.

public class HashTree extends Hashtable {
	private HashTree htParent;

	// Accessor and set methods for new Hashtable attribute, htParent..
	public HashTree GetParent() { return htParent; }
	public void SetParent(HashTree htParent) {
		this.htParent = htParent;
	}

    // Another constructor
	public HashTree () {}
	public HashTree (String SessionKey) {
		if (SessionKey != null) { this.put("SKEY", SessionKey); }
	}

    public Vector Sort (String Parameter) {
        Vector v = new Vector();

        Enumeration eH = this.keys();
        while (eH.hasMoreElements()) {
            String s = (String) eH.nextElement();
            if (!s.equals("NUMRESULTS")) { v.addElement((HashTree) this.get(s)); }
        }
        qs(v, Parameter);

        return v;
    }

   // Main recursive call
   private void qs(Vector v, String Param) {

      Vector vl = new Vector();                      // Left and right sides
      Vector vr = new Vector();
      String el; String key;                                    // key for splitting

      if (v.size() < 2) return;                      // 0 or 1= sorted

      HashTree ht = (HashTree) v.elementAt(0);       // 1st element key
      key = (String) ht.get(Param); key = key.toLowerCase();

      // Start at element 1
      for (int i=1; i < v.size(); i++) {
         HashTree hte = (HashTree) v.elementAt(i);
         el = (String) hte.get(Param); el = el.toLowerCase();
         if (el.compareTo(key) > 0) vr.addElement(hte);
         else vl.addElement(hte);
      }

      qs(vl, Param);                                        // Recursive call left
      qs(vr, Param);                                        //    "        "  right
      vl.addElement(v.elementAt(0));

      addVect(v, vl, vr);
    }

    // Add two vectors together, into a destination Vector
    private void addVect( Vector dest, Vector left, Vector right ) {

       int i;

       dest.removeAllElements();                     // reset destination

       for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
       for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
    }
}
