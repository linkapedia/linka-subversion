package com.iw.apishell;

import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

/*
 * invoke the synchronization API for off-line batch classification
 *
 * Written By: Michael A. Puscar, Indraweb
 * Date: Sep 18, 2003
 */

public class LoadBatchRun {
	public static void main(String args[]) {

		try {
            if (args.length < 3) {
                System.out.println("Usage: LoadBatchRun.java <username> <password> <api location>");
                throw new Exception("Invalid arguments.");
            }
            String sUserID = (String) args[0];
            String sPassword = (String) args[1];
            String sAPI = (String) args[2];
            String sTablespace = "RDATA";

            if (args.length == 4) { sTablespace = args[3]; }
            sAPI = "http://"+sAPI+"/";

            // LOGIN routine takes a USERID and a PASsWORD
            ITS its = new ITS();
            its.SetAPI(sAPI); //"http://woti.tzo.com:8888/");

            // LOGIN
            User u = its.Login(sUserID, sPassword);

			// Login successful.  Get the session key and put into args for future arguments
			String SKEY = u.getSessionKey();
            its.SetSessionKey(SKEY);

			System.out.println("Login successful.  Your session key is "+SKEY);
            System.out.println("Invoking the synchronization process... ("+sTablespace+")");

            its.synchronizeBatchRun(sTablespace);

		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage());
            e.printStackTrace(System.out);
		}
    }
}
