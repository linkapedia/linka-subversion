package com.iw.apishell;

import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

public class RebuildIndexes {
	public static void main(String args[]) {
        HashTree htArgs = ITS.getArgHash(args);

        String API = (String) htArgs.get("api");
        String Username = (String) htArgs.get("username");
        String Password = (String) htArgs.get("password");
        String Index = (String) htArgs.get("index");
        String Table = (String) htArgs.get("table");
        String Field = (String) htArgs.get("field");
        String Build = (String) htArgs.get("build");

        if (args.length < 6) {
            writeArguments();
            return;
        }

        String SKEY = "";

        if (((Build != null) && (Index != null)) && ((Field == null) || (Table == null))) {
            writeArguments();
            return;
        }

        API = "http://"+API+"/";

        ITS its = new ITS();
        its.SetAPI(API);

		try {
            // LOGIN routine takes a USERID and a PASsWORD
            User u = its.Login(Username, Password);

			// Login successful.  Get the session key and put into args for future arguments
			SKEY = u.getSessionKey();
            its.SetSessionKey(SKEY);

			System.out.println("Login successful.  Your session key is "+SKEY);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("Error: Login failed."); return;
        }

        try {
            long lStart = System.currentTimeMillis();
            if ((Build != null) && (Index != null)) {
                if (!its.rebuildIndexes(Index, Table, Field)) { throw new Exception("Index rebuild failed."); }
            } else if ((Index == null) && (Build != null)) {
                if (!its.rebuildIndexes(true)) { throw new Exception("Index rebuild failed."); }
            } else if (Index != null) {
                if (!its.rebuildIndexes(Index)) { throw new Exception("Index rebuild failed."); }
            } else {
                if (!its.rebuildIndexes(false)) { throw new Exception("Index rebuild failed."); }
            }
            long lEnd = System.currentTimeMillis() - lStart;

            System.out.println("Indexes rebuild completed in "+lEnd+" ms.");
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("Error: Index rebuild failed."); return;
        }
    }

    public static void writeArguments() {
        System.out.println("Usage: RebuildIndexes.jar -username user -password pass -api api");
        System.out.println("  Optional parameters: ");
        System.out.println("  -build true (build index(es) from scratch)");
        System.out.println("  -index index (rebuild a specific index only)");
        System.out.println("  -table table (required when using the BUILD and INDEX argument)");
        System.out.println("  -field field (required when using the BUILD and INDEX argument)");
    }
}
