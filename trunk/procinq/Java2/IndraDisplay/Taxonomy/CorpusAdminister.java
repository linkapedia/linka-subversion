package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class CorpusAdminister
{
	private static void qs(Vector v) {
   
		Vector vl = new Vector();                      // Left and right sides
		Vector vr = new Vector();
		HashTree el;
		String key = new String("");                   // key for splitting
      
		if (v.size() < 2) return;                      // 0 or 1= sorted
      
		HashTree htResult = (HashTree) v.elementAt(0);
		key = (String) htResult.get("CORPUS_NAME");
		key = key.toUpperCase(); 
		key = new String(com.indraweb.util.UtilStrings.replaceStrInStr(key," ",""));
	      
	    // Start at element 1
	    for (int i=1; i < v.size(); i++) {
	       el = (HashTree) v.elementAt(i);
		   String s = (String) el.get("CORPUS_NAME"); s = s.toUpperCase();
		   s = new String(com.indraweb.util.UtilStrings.replaceStrInStr(s," ",""));
		   if (key.compareTo(s) <= 0) vr.addElement(el); // Add to right
	       else vl.addElement(el);                     // Else add to left
	    }
      
	    qs(vl);                                        // Recursive call left
		qs(vr);                                        //    "        "  right
		vl.addElement(v.elementAt(0));
 
		addVect(v, vl, vr);
	}

	// Add two vectors together, into a destination Vector
	private static void addVect( Vector dest, Vector left, Vector right ) {
		
		int i;
		   
		dest.removeAllElements();                     // reset destination
		   
		for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
		for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
		   
	}

	public static Vector Sort (HashTree htHash) {
		Vector vC = new Vector();
		Enumeration e = htHash.elements();
			
		// Fill the vector with all the results
		while (e.hasMoreElements()) {
			HashTree htResult = (HashTree) e.nextElement();
			vC.addElement(htResult);
		}
			
		// Sort this vector
		qs(vC);
		
		return vC;
	}
	
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			// Call: ListCorpora to retrieve Genre data
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tscorpus.TSListAllCorpora", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}

			HTMLDocument Document = new HTML.HTMLDocument();
			Document.AddVariable("Title", "Administer Taxonomies");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-nosearch.tpl");
			Document.WriteTemplate(out, "administer-head.tpl");

			if (htResults.containsKey("CORPORA")) {
				HashTree htCorpora = (HashTree) htResults.get("CORPORA");
				Vector v = Sort(htCorpora);
				Enumeration e3 = v.elements();

				while (e3.hasMoreElements()) {
					
					HashTree htCorpus = (HashTree) e3.nextElement();
					Document.SetHash(htCorpus);
					Document.WriteTemplate(out, "corpus-admin.tpl");
				}
			} else { out.println("The taxonomy server is currently unavailable."); }
			
			Document.WriteTemplate(out, "administer-foot.tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
