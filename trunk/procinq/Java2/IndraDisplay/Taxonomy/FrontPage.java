package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.Math.*;
import java.lang.reflect.Method;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class FrontPage
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
        String sFunction = com.indraweb.execution.Session.cfg.getProp ("FrontPage", false, null);
        if (sFunction != null) {
            Class cProperties = Class.forName ("Logging.APIProps");
			Class cHttpServletRequest = Class.forName ("javax.servlet.http.HttpServletRequest");
			Class cHttpServletResponse = Class.forName ("javax.servlet.http.HttpServletResponse");
			Class cPrintWriter = Class.forName ("java.io.PrintWriter");

			Class[] cArr = { cProperties, cPrintWriter, cHttpServletRequest, cHttpServletResponse };

            Object[] oArr = { props, out, req, res};
			Class cTSapi = Class.forName ( sFunction);
            Method method = cTSapi.getMethod( "handleTSapiRequest", cArr );
            method.invoke ( cTSapi, oArr );

            return;
        }

		// Call: ListCorpora to retrieve Genre data
		HashTree htArguments = new HashTree(props);
		InvokeAPI API = new InvokeAPI ("tssubjectarea.TSListCorporaSubjectAreas", htArguments);
		HashTree htResults = API.Execute(false, false);

		if (!htResults.containsKey("SUBJECTAREAS")) {
			throw new Exception("No subject areas found.");
		}

		HTMLDocument Document = new HTML.HTMLDocument();
		Document.AddVariable("Title", "Available Taxonomies");
		Document.AddVariable("OPTION_CORPUS_LIST", req);
		Document.WriteTemplate(out, "header-admin.tpl");
		Document.WriteTemplate(out, "subject-area-head.tpl");

        HashTree htSubjectAreas = (HashTree) htResults.get("SUBJECTAREAS");
        Vector vSubjectAreas = htSubjectAreas.Sort("SUBJECTNAME");

        if (vSubjectAreas.size() < 1) { throw new Exception("No subject areas found."); }
        Enumeration eV = vSubjectAreas.elements(); int loop = 0;
        while (eV.hasMoreElements()) {
            loop++;

            HashTree ht = (HashTree) eV.nextElement();
            if (ht == null) { ht = new HashTree(); }
            Document.SetHash(ht);

            String sCORP = "<UL> ";

            if (ht.containsKey("CORPORA")) {
                HashTree htCorpora = (HashTree) ht.get("CORPORA");
                Vector vCorpora = htCorpora.Sort("CORPUS_NAME");

                Enumeration eV2 = vCorpora.elements(); int cloop = 0;
                while (eV2.hasMoreElements()) {
                    cloop++;

                    HashTree htCorpus = (HashTree) eV2.nextElement();
                    Document.SetHash(htCorpus);

                    String sCorpusID = (String) htCorpus.get("CORPUSID");
                    String sCorpusName = (String) htCorpus.get("CORPUS_NAME");
					if (sCorpusName.length() > 15) {
						htCorpus.put("CORPUS_NAME", sCorpusName.substring(0, 15)+"..");
					}

                    sCORP = sCORP + "<LI> <a class='corpora' href='/servlet/Main?template=Taxonomy.DisplayResults&CorpusID="+sCorpusID+"'>"+sCorpusName+"</a> <br> &nbsp;";
                }
            }
            sCORP = sCORP+" </UL>";
            Document.AddVariable("CORP", sCORP);
            Document.WriteTemplate(out, "subject-area.tpl");
        }

		Document.WriteTemplate(out, "subject-area-foot.tpl");
		Document.WriteFooter(out);
	}
}
