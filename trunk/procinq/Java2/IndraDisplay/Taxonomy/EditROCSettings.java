package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class EditROCSettings
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
            String sCID = (String) props.get("cid", true);
            String action = (String) props.get("action");

            if (action != null) { // create new ROCsettings for this CID
                HashTree htArguments = new HashTree(props);
                htArguments.put("corpusid", sCID);
                InvokeAPI API = new InvokeAPI ("tsrocsettings.TSGetROCSettingByCorpus", htArguments);
                HashTree htResults = API.Execute(false, false);

                if (!htResults.containsKey("ROCSETTINGID")) { throw new Exception("Select ROC settings: Permission denied."); }
                String sRID = (String) htResults.get("ROCSETTINGID");

                if (sRID.equals("1")) {
                    htArguments = new HashTree(props);
                    htArguments.put("ratio", "50");
                    htArguments.put("usedefault", "true");
                    API = new InvokeAPI ("tsrocsettings.TSCreateROCSettings", htArguments);
                    htResults = API.Execute(false, false);

                    if (!htResults.containsKey("SUCCESS")) { throw new Exception("Create ROC settings: Permission denied."); }
                    sRID = (String) htResults.get("SUCCESS");

                    htArguments = new HashTree(props);
                    htArguments.put("RID", sRID);
                    htArguments.put("CID", sCID);
                    API = new InvokeAPI ("tsrocsettings.TSAddCorpusROC", htArguments);
                    htResults = API.Execute(false, false);
                    if (!htResults.containsKey("SUCCESS")) { throw new Exception("Create ROC settings: Permission denied."); }
                }
            }

            int loop = 0;

			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Edit ROC Settings");
            Document.AddVariable("CID", sCID);

			HashTree htArguments = new HashTree(props);
            htArguments.put("corpusid", sCID);
			InvokeAPI API = new InvokeAPI ("tsrocsettings.TSGetROCSettings", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (!htResults.containsKey("ROCSETTINGS")) {
				throw new Exception ("&nbsp; This corpus has no ROC settings (and is corrupted).  Please see the system administrator for more details.");
			}
            HashTree htROCsettings = (HashTree) htResults.get("ROCSETTINGS");
            String sRID = (String) htROCsettings.get("ROCSETTINGID");
            Document.AddVariable("ROCSETTINGID", sRID);
            Document.AddVariable("RATIOTNTOTP", (String) htROCsettings.get("RATIOTNTOTP"));

            if (sRID.equals("1")) {
                Document.WriteTemplate(out, "header-roc-read-only.tpl");
                Document.WriteTemplate(out, "roc-settings/manage-head-read-only.tpl");
            } else {
                Document.WriteTemplate(out, "header-roc.tpl");
                Document.WriteTemplate(out, "roc-settings/manage-head.tpl");
            }
            Document.WriteTemplate(out, "roc-settings/manage-list-head.tpl");

            if (htROCsettings.containsKey("LOCALCLASSIFIERS")) {
                HashTree htLC = (HashTree) htROCsettings.get("LOCALCLASSIFIERS");
                Enumeration eR = (Enumeration) htLC.elements();

				while (eR.hasMoreElements()) { loop++;
					HashTree htClassifier = (HashTree) eR.nextElement();
					Document.SetHash(htClassifier);
                    if (sRID.equals("1")) { Document.WriteTemplate(out, "roc-settings/manage-element-read-only.tpl"); }
                    else { Document.WriteTemplate(out, "roc-settings/manage-element.tpl"); }
				}
				Document.WriteTemplate(out, "roc-settings/manage-list-foot.tpl");
			}

            if (loop < 3) { Document.AddVariable("ADDLOCALCLASSIFIER",
                    "Click <a href='/servlet/Main?template=Taxonomy.CreateLocalClassifier&RID="+sRID+"'>here to add a new local classifier</a>.");
            }

			Document.WriteTemplate(out, "roc-settings/manage-foot.tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}

	}
}
