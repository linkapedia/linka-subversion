package Taxonomy;

import HTML.HTMLDocument;
import Logging.APIProps;
import com.iw.system.*;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Vector;

import Server.ITS;

public class GetRelatedNodes {
    // Display login page
    public static void handleTSapiRequest(APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
            throws Exception {
        String sDocURL = (String) props.get("DocURL");
        String sDocumentID = (String) props.get("DocumentID");
        String sDocTitle = (String) props.get("DocTitle"); // optional, for display purposes only

        try {
            if ((sDocURL == null) && (sDocumentID == null)) {
                HTMLDocument Document = new HTMLDocument();
                Document.AddVariable("Title", "Related Topics");
                Document.WriteTemplate(out, "header-admin.tpl");
                Document.WriteTemplate(out, "related-nodes.tpl");
                Document.WriteFooter(out);

                return;
            }

            // Read scoring threshold from cookie information
            Cookie Cookies[] = req.getCookies();
            String sScoreThreshold = "75.0"; // default

            if (req.getCookies() != null) {
                int cLen = Cookies.length;

                for (int i = 0; i < cLen; i++) {
                    if (Cookies[i].getName().equals("SCORETHRESHOLD")) {
                        sScoreThreshold = Cookies[i].getValue();
                    }
                }
            }

            ITS its = new ITS((String) props.get("SKEY"));
            Vector vNodes = its.getRelatedNodes(sDocumentID, sDocURL, sScoreThreshold);

            if (vNodes.size() < 2) {
                HTMLDocument doc = new HTMLDocument();
                doc.WriteSuccess(out, "<blockquote>This document does not contain any topic relationships.");
                return;
            }

            HTMLDocument Document = new HTMLDocument();
            Document.AddVariable("Title", "Related Topics");
            Document.WriteTemplate(out, "header-admin.tpl");
            Document.AddVariable("DocURL", sDocURL);
            if (sDocTitle != null) {
                Document.AddVariable("DocTitle", sDocTitle);
            }
            Document.WriteTemplate(out, "related-nodes-doc.tpl");

            int loop = 0;
            for (int i = 0; i < vNodes.size(); i++) {
                loop++;
                Document.AddVariable("LOOPI", loop + "");
                Node n = (Node) vNodes.elementAt(i);

                Document.SetHash(n);
                if (n.htn.containsKey("DOCURL")) Document.AddVariable("EDOCURL", n.getEncodedField("DOCURL"));
                if (n.htn.containsKey("DOCTITLE")) Document.AddVariable("EDOCTITLE", n.getEncodedField("DOCTITLE"));
                if (n.htn.containsKey("DOCUMENTSUMMARY")) Document.AddVariable("EDOCURL", n.getEncodedField("DOCURL"));

                String sStars = DisplayResults.BuildStars(n.get("SCORE1"));
                String sNodeTree = DisplayResults.BuildNodeTree(out, n.get("NODEID"), props);

                Document.AddVariable("NODETREE", sNodeTree);
                Document.AddVariable("STARS", sStars);
                Document.WriteTemplate(out, "related-nodes-results.tpl");
            }
            if (loop != 0) Document.WriteTemplate(out, "search-end.tpl");
            Document.WriteFooter(out);
        } catch (Exception e) {
            out.print("<!-- ");
            e.printStackTrace(out);
            out.println(" -->");
            out.println("<blockquote>Sorry, this document does not exist on our server.  Please check the link for errors.</blockquote>");
        }
    }
}
