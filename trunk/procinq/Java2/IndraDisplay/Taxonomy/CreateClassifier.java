package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class CreateClassifier
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
        String sCID = (String) props.get ("CID");
		String sName = (String) props.get ("Name");
        String sPath = (String) props.get ("Path");
        String sSort = (String) props.get ("Sort");
        String sActive = (String) props.get ("Active");

        if (sName == null) {
			try {
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Create/Edit New Classifier");
				Document.WriteTemplate(out, "header-nosearch.tpl");

                if (sCID != null) {
                    HashTree htArguments = new HashTree(props);
                    htArguments.put("CID", sCID);
                    InvokeAPI API = new InvokeAPI ("tsclassify.TSGetClassifierList", htArguments);
                    HashTree htResults = API.Execute(false, false);

                    if (!htResults.containsKey("CLASSIFIERS")) { throw new Exception("Classifier with ID "+sCID+" was not found."); }
                    HashTree htClassifiers = (HashTree) htResults.get("CLASSIFIERS");
                    HashTree htClassifier = (HashTree) htClassifiers.get("CLASSIFIER");
                    Document.SetHash(htClassifier);
                    Document.AddVariable("HIDECLASS", "<input type='hidden' name='CID' value='"+(String)htClassifier.get("CLASSIFIERID")+"'>\n");

                    // set up multiple select lists
                    if (((String) htClassifier.get("SORTORDER")).equals("ASC")) { Document.AddVariable("SORTA", "selected"); }
                    else { Document.AddVariable("SORTB", "selected"); }

                    if (((String) htClassifier.get("ACTIVE")).equals("Inactive")) { Document.AddVariable("SORTC", "selected"); }
                    else { Document.AddVariable("SORTD", "selected"); }
                } else { Document.AddVariable("COLNAME", "(none assigned)"); }

				Document.WriteTemplate(out, "classifier/edit.tpl");
				Document.WriteFooter(out);
			} catch (Exception e) {
				String sErrorMsg = e.getMessage();
				Log.LogError(e, out);
				return;
			}
		} else { // Create or Edit Edit this classifier now
			// Call: GetClassifierProps
			HashTree htArguments = new HashTree(props);
			if (sName != null) { htArguments.put("Name", sName); }
            String APIcall = "TSCreateClassifier";
            if (sCID != null) { APIcall = "TSEditClassifierProps"; }

			InvokeAPI API = new InvokeAPI ("tsclassify."+APIcall, htArguments);
            if (sCID != null) { htArguments.put("cid", sCID); }
            if (sName != null) { htArguments.put("name", sName); }
            if (sPath != null) { htArguments.put("path", sPath); }
            if (sSort != null) { htArguments.put("sort", sSort); }
            if (sActive != null) { htArguments.put("active", sActive); }

			HashTree htResults = API.Execute(false, false);

			boolean bSuccess = true;
			if (!htResults.containsKey("SUCCESS")) {
				HTMLDocument Document = new HTMLDocument();
				String sFail = "Your classifier ("+sName+") could not be created/modified: Permission denied.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.Classifier'>"+
							  "Click here to return to the classifier menu</a>.";
				Document.WriteSuccess(out, sFail);
			} else {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "Your new classifier ("+sName+") has been created/modified successfully.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Classifier'>"+
								  "Click here to return to the classifier menu</a>.";
				Document.WriteSuccess(out, sSuccess);
			}
		}
	}
}
