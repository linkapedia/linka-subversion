package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class Repository
{
	// Display login page
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("Title", "Manage Repositories");
			Document.WriteTemplate(out, "header-nosearch.tpl");
			Document.WriteTemplate(out, "repository/manage-head.tpl");
			
			// Call: GetRepositoryList
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tsrepository.TSGetRepositoryList", htArguments);
			HashTree htResults = API.Execute(false, false);

			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}
			
			if (!htResults.containsKey("REPOSITORIES")) {
				out.println("&nbsp; There are no repositories on this server.");
			} else {
				HashTree htR = (HashTree) htResults.get("REPOSITORIES");
				Enumeration eR = htR.elements();

				Document.WriteTemplate(out, "repository/manage-list-head.tpl");
				while (eR.hasMoreElements()) {
					HashTree htRepository = (HashTree) eR.nextElement();
					Document.SetHash(htRepository);
					String sRID = (String) htRepository.get("REPOSITORYID");
					if (!sRID.equals("1")) { // RID 1 is reserved.
						Document.WriteTemplate(out, "repository/manage-element.tpl");
					}
				}
				Document.WriteTemplate(out, "repository/manage-list-foot.tpl");
			}
			
			Document.WriteTemplate(out, "repository/manage-foot.tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
