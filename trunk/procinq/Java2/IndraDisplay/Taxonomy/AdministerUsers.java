package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class AdministerUsers
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		try {
			// Call: ListUsers to retrieve Subscriber data
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tsuser.TSListUsers", htArguments);
			HashTree htResults = API.Execute(false, false);
			
			if (htResults.containsKey("SESSIONEXPIRED")) {		
				Server.Login.handleTSapiRequest(props, out, req, res);
				return;
			}

			HTMLDocument Document = new HTML.HTMLDocument();
			Document.AddVariable("Title", "Administer Users");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-admin.tpl");
			Document.WriteTemplate(out, "users/administer-head.tpl");

			if (htResults.containsKey("SUBSCRIBERS")) {
				Document.WriteTemplate(out, "users/users-start.tpl");
				HashTree htSubscribers = (HashTree) htResults.get("SUBSCRIBERS");
				Enumeration e3 = htSubscribers.elements();

				while (e3.hasMoreElements()) {
					HashTree htUser = (HashTree) e3.nextElement();
					Document.SetHash(htUser);
					Document.WriteTemplate(out, "users/user-admin.tpl");
				}
				Document.WriteTemplate(out, "users/users-stop.tpl");
			} else { out.println("There are no users available in this system."); }
			
			Document.WriteTemplate(out, "users/administer-foot.tpl");
			Document.WriteFooter(out);
		}
		catch (Exception e) {
			HTMLDocument Document = new HTMLDocument();
			String sErrorMsg = e.getMessage();
			Document.WriteError(out, "<BLOCKQUOTE>"+sErrorMsg+"</BLOCKQUOTE>");
			Log.LogError(e, out);
		}
	
	}
}
