package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class EditRepository
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sID = (String) props.get ("RID", true);
		String sType = (String) props.get ("Type");
		String sName = (String) props.get ("Name");
		String sLoc = (String) props.get ("Location");
		String sUsername = (String) props.get ("Username");
		String sPassword = (String) props.get ("Password");
		String sFullText = (String) props.get ("Full");
		
		if ((sType == null) && (sName == null) && (sLoc == null) &&
			(sUsername == null) && (sPassword == null) && (sFullText == null)) {
		
			try {
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Manage Repositories");
				Document.WriteTemplate(out, "header-nosearch.tpl");
			
				// Call: GetRepositoryProps
				HashTree htArguments = new HashTree(props);
				htArguments.put("RID", sID);
				InvokeAPI API = new InvokeAPI ("tsrepository.TSGetRepositoryProps", htArguments);
				HashTree htResults = API.Execute(false, false);

				if (htResults.containsKey("SESSIONEXPIRED")) {		
					Server.Login.handleTSapiRequest(props, out, req, res);
					return;
				}
				
				if (!htResults.containsKey("REPOSITORY")) {
					out.println("&nbsp; That repository does not exist on this server.");
				} 
				else { 
					HashTree htR = (HashTree) htResults.get("REPOSITORY");
					Document.SetHash(htR);
					if (htR.get("FULLTEXT").equals("1")) {
						Document.AddVariable("FULL", "<option selected value=1> On<option value=0>Off\n");
					} else { Document.AddVariable("FULL", "<option value=1> On<option selected value=0>Off\n"); }

					// build option type list by getting list of types
					API = new InvokeAPI ("tsrepository.TSGetRepositoryTypeList", htArguments);
					htResults = API.Execute(false, false);
					
					if (!htResults.containsKey("TYPES")) { 
						throw new Exception ("No repository types defined in the database.");
					}
					HashTree htTypes = (HashTree) htResults.get("TYPES");
					Enumeration eTR = htTypes.elements();
					String sOptionTypes = "";
					
					while (eTR.hasMoreElements()) {
						HashTree htT = (HashTree) eTR.nextElement();
						String sKey = (String) htT.get("REPOSITORYTYPENAME");
						String sValue = (String) htT.get("REPOSITORYTYPEID");
						String sCurrValue = (String) htR.get("REPOSITORYTYPE");
						
						if (sValue.equals(sCurrValue)) { sOptionTypes = sOptionTypes+" <option selected value="+sValue+"> "+sKey; }
						else { sOptionTypes = sOptionTypes+" <option value="+sValue+"> "+sKey; }
					}
					
					Document.AddVariable("TEMPLATE", "Taxonomy.EditRepository");
					Document.AddVariable("TYPE", sOptionTypes);
                    Document.AddVariable("RID", sID);
					Document.WriteTemplate(out, "repository/edit.tpl");
					
					// grab watched folders, if any
					API = new InvokeAPI ("tsrepository.TSGetWatchedFolders", htArguments);
					htResults = API.Execute(false, false);

					if (htResults.containsKey("FOLDERS")) {
						String sOptionFolders = "";
						HashTree htFolders = (HashTree) htResults.get("FOLDERS");
						Enumeration eF = htFolders.elements();
						int loop = 0;
						
						while (eF.hasMoreElements()) {
							HashTree htFolder = (HashTree) eF.nextElement();	
							Document.SetHash(htFolder);
							loop++; Document.AddVariable("LOOP", loop+"");
							if (htFolder.get("RECURSE").equals("1")) { 
								Document.AddVariable("RECURSE", "<option value=1 selected>Yes<option value=0>No"); 
								Document.AddVariable("RECURSESCORE", "1");
							} else { 
								Document.AddVariable("RECURSE", "<option value=1>Yes<option selected value=0>No");
								Document.AddVariable("RECURSESCORE", "0");
							}
							
							Document.WriteTemplate(out, "repository/edit-folders.tpl");
						}
					}
				}
				Document.WriteTemplate(out, "repository/edit-foot.tpl");
				Document.WriteFooter(out);
			} catch (Exception e) {
				String sErrorMsg = e.getMessage();
				Log.LogError(e, out);
				return;
			}
		} else { // Edit this repository now
			// Call: GetRepositoryProps
			HashTree htArguments = new HashTree(props);
			htArguments.put("RID", sID);
			if (sType != null) { htArguments.put("Type", sType); }
			if (sName != null) { htArguments.put("Name", sName); }
			if (sLoc != null) { htArguments.put("Location", sLoc); }
			if (sUsername != null) { htArguments.put("Username", sUsername); }
			if (sPassword != null) { htArguments.put("Password", sPassword); }
			if (sFullText != null) { htArguments.put("Full", sFullText); }
			
			boolean bSuccess = true;
			
			// Edit the actual properties
			InvokeAPI API = new InvokeAPI ("tsrepository.TSEditRepositoryProps", htArguments);
			HashTree htResults = API.Execute(false, false);
			if (!htResults.containsKey("SUCCESS")) { bSuccess = false; }

			// Get watched folders from the database
			API = new InvokeAPI ("tsrepository.TSGetWatchedFolders", htArguments);
			htResults = API.Execute(false, false);
			
			if (bSuccess) {
				// Loop through each UI result.  If different from 
				//  previous, edit.
				Enumeration eProps = props.keys();
				while (eProps.hasMoreElements()) {
					String sKey = (String) eProps.nextElement();
					if (sKey.length() > 3) {
						if (sKey.substring(0,4).equals("path")) { 
							// extract the paths
							String sIdentifier = sKey.substring(4,sKey.length());
							String sPath = (String) props.get(sKey);
							String sOldP = (String) props.get("orig"+sKey);
							String sRecurse = (String) props.get("Recurse"+sIdentifier);
							String sOldR = (String) props.get("origRecurse"+sIdentifier); 

							if ((sPath.equals("")) && (!sOldP.equals(""))) {
								RemoveFolder(out, sOldP, sOldR, sID, props);											 
							} else if ((!sPath.equals(sOldP)) || (!sRecurse.equals(sOldR))) {
								EditFolder(out, sOldP, sPath, sRecurse, sID, props);
							}
						}
					}
				}
			}

			// Now that modified folders are added, add any new ones that were specified
			String sNewPath1 = (String) props.get("NewPath1");
			String sNewPath2 = (String) props.get("NewPath2");
			String sNewPath3 = (String) props.get("NewPath3");
			String sRecurse1 = (String) props.get("NewRecurse1");
			String sRecurse2 = (String) props.get("NewRecurse2");
			String sRecurse3 = (String) props.get("NewRecurse3");

			AddFolder(out, sNewPath1, sRecurse1, sID, props);
			AddFolder(out, sNewPath2, sRecurse2, sID, props);
			AddFolder(out, sNewPath3, sRecurse3, sID, props);
			
			if (!bSuccess) {				
				HTMLDocument Document = new HTMLDocument();
				String sFail = "Your repository ("+sName+") could not be altered: Permission denied.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Repository'>"+
								  "Click here to return to the repository menu</a>.";
				Document.WriteSuccess(out, sFail);
			} else {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "Your repository ("+sName+") has been altered successfully.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Repository'>"+
								  "Click here to return to the repository menu</a>.";
				Document.WriteSuccess(out, sSuccess);
			}
		}
	}
	
	public static boolean AddFolder (PrintWriter out, String sPath, String sRecurse, String sID, APIProps props) {
		HashTree htArguments = new HashTree(props);
		
		htArguments.put("RID", sID);
		htArguments.put("Path", sPath);
		htArguments.put("Recurse", sRecurse);

		if (!sPath.equals("")) {
			InvokeAPI API = new InvokeAPI ("tsrepository.TSAddWatchedFolder", htArguments);
            HashTree htResults;
            try { htResults = API.Execute(false, false); }
            catch (Exception e) { api.Log.LogError(e); return false; }
			if (!htResults.containsKey("SUCCESS")) { return false; }				
			return true;
		} else { return true; }
	}
	
	public static boolean EditFolder (PrintWriter out, String sOld, String sPath, String sRecurse, String sID, APIProps props) {
		HashTree htArguments = new HashTree(props);
		
		htArguments.put("RID", sID);
		htArguments.put("OldPath", sOld);
		htArguments.put("Path", sPath);
		htArguments.put("Recurse", sRecurse);

		if (!sPath.equals("")) {
			InvokeAPI API = new InvokeAPI ("tsrepository.TSEditWatchedFolder", htArguments);
            HashTree htResults;
            try { htResults = API.Execute(false, false); }
            catch (Exception e) { api.Log.LogError(e); return false; }
			if (!htResults.containsKey("SUCCESS")) { return false; }				
			return true;
		} else { return true; }
	}	
	
	public static boolean RemoveFolder (PrintWriter out, String sPath, String sRecurse, String sID, APIProps props) {
		HashTree htArguments = new HashTree(props);
		
		htArguments.put("RID", sID);
		htArguments.put("Path", sPath);
		htArguments.put("Recurse", sRecurse);

		if (!sPath.equals("")) {
			InvokeAPI API = new InvokeAPI ("tsrepository.TSRemoveWatchedFolder", htArguments);
            HashTree htResults;
            try { htResults = API.Execute(false, false); }
            catch (Exception e) { api.Log.LogError(e); return false; }
			if (!htResults.containsKey("SUCCESS")) { return false; }				
			return true;
		} else { return true; }
	}
}
