package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;

public class CreateUser
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		HTMLDocument Document = new HTMLDocument();
		Document.WriteError(out, "Sorry, this functionality is not available in this version of the ITS.<p>"+
							"Please see your Indraweb sales representative for more details.");
	}
}
