package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class CreateLocalClassifier
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sRID = (String) props.get ("RID", true);
        String sCID = (String) props.get ("classifierid");

        String sStar = (String) props.get ("star");
        String sCost = (String) props.get ("cost");
        String sCut = (String) props.get ("cutoff");

        String edit = (String) props.get ("edit");

        if (sCID == null) {
            sCID = "";
			try {
				HTMLDocument Document = new HTMLDocument();
				Document.AddVariable("Title", "Create/Edit New Local Classifier");
                Document.AddVariable("rocsettingid", sRID);
				Document.WriteTemplate(out, "header-nosearch.tpl");

                // get the list of available stars for this repository
                HashTree htArguments = new HashTree(props);
                htArguments.put("rid", sRID);
                InvokeAPI API = new InvokeAPI ("tsrocsettings.TSGetAvailableStars", htArguments);
                HashTree htResults = API.Execute(false, false);

                String sStars = ""; String sClassifiers = "";
                try {
                    HashTree htStars = (HashTree) htResults.get("STARS");
                    if (sStar != null) { sStars = sStars + "<option value="+sStar+" selected> "+sStar+" star(s)\n"; }
                    for (int i=3; i >= 1; i--) {
                        if (htStars.containsKey("STAR"+i)) {
                            sStars = sStars + "<option value="+i+"> "+i+" star(s)\n";
                        }
                    }
                } catch (Exception e) { } // exception if no stars available

                Document.AddVariable("STARS", sStars);

                if (sStar != null) {
                    htArguments = new HashTree(props);
                    htArguments.put("rid", sRID);
                    htArguments.put("starcount", sStar);
                    API = new InvokeAPI ("tsrocsettings.TSGetROCBucket", htArguments);
                    htResults = API.Execute(false, false);

                    if (!htResults.containsKey("LOCALCLASSIFIERS")) { throw new Exception("ROC setting with ID "+sRID+" was not found."); }
                    HashTree htClassifiers = (HashTree) htResults.get("LOCALCLASSIFIERS");
                    HashTree htClassifier = (HashTree) htClassifiers.get("LOCALCLASSIFIER");
                    sCID = (String) htClassifier.get("CLASSIFIERID");
                    Document.AddVariable("EDIT", "<input type='hidden' name='edit' value='true'>\n");
                    Document.SetHash(htClassifier);
                } else {
                    Document.AddVariable("CUTOFFSCOREGTE", "11.8");
                    Document.AddVariable("COSTRATIOFPTOFN", "5");
                }

                // get the list of available classifiers
                htArguments = new HashTree(props);
                htArguments.put("rid", sRID);
                API = new InvokeAPI ("tsclassify.TSGetClassifierList", htArguments);
                htResults = API.Execute(false, false);

                if (!htResults.containsKey("CLASSIFIERS")) { throw new Exception("Sorry, there are no classifiers loaded into the system."); }
                HashTree htClassifiers = (HashTree) htResults.get("CLASSIFIERS");
                Enumeration eC = htClassifiers.elements();

                while (eC.hasMoreElements()) {
                    HashTree htClass = (HashTree) eC.nextElement();
                    String active = (String) htClass.get("ACTIVE");
                    String localCID = (String) htClass.get("CLASSIFIERID");
                    if (active.equals("Active")) {
                        String selected = ""; if (sCID.equals(localCID)) { selected = "SELECTED"; }
                        sClassifiers = sClassifiers + "<option value="+(String) htClass.get("CLASSIFIERID")+
                                                      " "+selected+"> "+(String) htClass.get("CLASSIFIERNAME");
                    }
                }
                Document.AddVariable("CLASSIFIERS", sClassifiers);

				Document.WriteTemplate(out, "roc-settings/edit.tpl");
				Document.WriteFooter(out);
			} catch (Exception e) {
				String sErrorMsg = e.getMessage();
				Log.LogError(e, out);
				return;
			}
		} else { // Create or Edit Edit this local classifier now
			HashTree htArguments = new HashTree(props);
            String APIcall = "TSCreateROCBucket";
            if (edit != null) { APIcall = "TSEditROCBucketProps"; }

			InvokeAPI API = new InvokeAPI ("tsrocsettings."+APIcall, htArguments);
            htArguments.put("rocsettingid", sRID);
            if (sCID != null) { htArguments.put("classifierid", sCID); }
            if (sStar != null) { htArguments.put("starcount", sStar); }
            if (sCost != null) { htArguments.put("cost", sCost); }
            if (sCut != null) { htArguments.put("cutoff", sCut); }

            /*  these fields are not currently used by the GUI
            if (sScore != null) { htArguments.put("score", sScore); }
            if (sTrue != null) { htArguments.put("truepos", sTrue); }
            if (sFalse != null) { htArguments.put("falsepos", sFalse); }
            */

			HashTree htResults = API.Execute(false, false);

			boolean bSuccess = true;
			if (!htResults.containsKey("SUCCESS")) {
				HTMLDocument Document = new HTMLDocument();
				String sFail = "Your local classifier could not be created/modified: Permission denied.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.CorpusAdminister'>"+
							  "Click here to return to the corpus administration menu</a>.";
				Document.WriteSuccess(out, sFail);
			} else {
				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "Your new local classifier has been created/modified successfully.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.CorpusAdminister'>"+
								  "Click here to return to the corpus administration menu</a>.";
				Document.WriteSuccess(out, sSuccess);
			}
		}
	}
}
