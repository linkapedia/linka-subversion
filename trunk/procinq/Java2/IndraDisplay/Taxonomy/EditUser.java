package Taxonomy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;
import com.indraweb.execution.Session;

public class EditUser
{
    	private static void EditAlert(APIProps props, PrintWriter out) throws Exception {
			String EID = (String) props.get("EID");
			String RunFreq = (String) props.get("RunFreq");

            HashTree htArguments = new HashTree(props);
            htArguments.put("ID", EID);
			htArguments.put("RunFreq", RunFreq);

			InvokeAPI API = new InvokeAPI ("tsnotification.TSEditAlert", htArguments);
			HashTree htResults = API.Execute(false, false);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Attempt to modify alert ID "+EID+" failed.");
            }
		}

		private static void DeleteAlert (String Email, String UserDN, APIProps props, PrintWriter out) throws Exception {
			String EID = (String) props.get("EID");

            //out.println("<script>alert('EID is: "+EID+"');</script>");

            HashTree htArguments = new HashTree(props);
            htArguments.put("ID", EID);
            htArguments.put("Email", Email);
            htArguments.put("UserID", UserDN);

			InvokeAPI API = new InvokeAPI ("tsnotification.TSRemoveAlert", htArguments);
			HashTree htResults = API.Execute(false, false);

            if (!htResults.containsKey("SUCCESS")) {
                throw new Exception("Attempt to remove alert with ID "+EID+" failed.");
            }
        }

    	public static HashTree ViewAlerts
                (APIProps props, String sEmail, String UserDN, boolean bEncodeQuery, boolean bOrderByDate, boolean bMineOnly, PrintWriter out)
        throws Exception {

			HashTree htArguments = new HashTree(props);
			htArguments.put("Email", sEmail);
			htArguments.put("UserID", UserDN);
			if (bMineOnly) { htArguments.put("MineOnly", "1"); }
			if (bOrderByDate) { htArguments.put("OrderByDate", "1"); }
			if (bEncodeQuery) { htArguments.put("Encode", "1"); }

			// invoke the API
            InvokeAPI API = new InvokeAPI ("tsnotification.TSViewAlerts", htArguments);
			HashTree htResults = API.Execute(false, false);

            if (htResults.containsKey("ALERTS")) { return (HashTree) htResults.get("ALERTS"); }
            else { return new HashTree(); }
		}

	public static Hashtable BuildNodes(String sNodes) {
		Hashtable htNodeHash = new Hashtable();

		if (sNodes.equals("")) { return htNodeHash; }

		// Unfortunately, we must now do some parsing to extract the node list.
		while (sNodes.indexOf(",") != -1) {
			String sNode = sNodes.substring(0, sNodes.indexOf(","));
			htNodeHash.put(sNode, "1");

			sNodes = new String(com.indraweb.util.UtilStrings.replaceStrInStr(sNodes,sNode+",",""));
		}
		if (sNodes.length() != 0) { htNodeHash.put(sNodes, "1"); }

		return htNodeHash;
	}

	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String ID = (String) props.get("ID");
		String sUserID = (String) props.get ("UserID", ID);
		String sScoreThreshold = (String) props.get ("scorethreshold");
		String sResultsPerPage = (String) props.get ("results");
		String sUserStatus = (String) props.get ("userstatus");
		String sEmailStatus = (String) props.get ("emailstatus");
		String sSUBMIT = (String) props.get ("SUBMIT");
		String sHIDDEN = (String) props.get ("nodes_hidden");
		String sPage = (String) props.get("page", "userinfo");

		if (sHIDDEN != null) {
			String sNodes = (String) props.get("nodes_hidden", true);

			// First remove all relationships
			HashTree htArguments = new HashTree(props);
			InvokeAPI API = new InvokeAPI ("tsnode.TSRemoveAllNodesSubscriber", htArguments);
			HashTree htResults = API.Execute(false, false);

			// Second, add only relationships in comma separated list "node_hidden"
			Hashtable htNodes = BuildNodes(sNodes);
			Enumeration eT = htNodes.keys();

			while (eT.hasMoreElements()) {
				String s = (String) eT.nextElement();
				htArguments = new HashTree(props);
				htArguments.put("NodeID", s);
				API = new InvokeAPI ("tsnode.TSAddNodeSubscriber", htArguments);
				htResults = API.Execute(false, false);
			}

			HTMLDocument Document = new HTMLDocument();
			String sSuccess = "Your alert list has been altered successfully.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.EditUser'>"+
							  "Click here to return to the user menu</a>.";

			Document.WriteSuccess(out, sSuccess);

		} else if (sSUBMIT == null) {
			try {
				if (sUserID == null) { throw new Exception("Sorry, this browser is not cookie capable."); }

				// Load user information
				// Call: ListUsers to retrieve Subscriber data
				HashTree htArguments = new HashTree(props);
				htArguments.put("UserID", sUserID);

				InvokeAPI API = new InvokeAPI ("tsuser.TSListUsers", htArguments);
				HashTree htResults = API.Execute(false, false);

				if (htResults.containsKey("SESSIONEXPIRED")) {
					Server.Login.handleTSapiRequest(props, out, req, res);
					return;
				}

				if (!htResults.containsKey("SUBSCRIBERS")) {
					throw new Exception("Sorry, that user does not exist.");
				}

				HashTree htSubscribers = (HashTree) htResults.get("SUBSCRIBERS");
				HashTree htSubscriber = (HashTree) htSubscribers.get("SUBSCRIBER");
				String sScore = (String) htSubscriber.get("SCORETHRESHOLD");
				String sResults = (String) htSubscriber.get("RESULTSPERPAGE");

				HTMLDocument Document = new HTMLDocument();
				Document.SetHash(htSubscriber);
				Document.AddVariable("Title", "Edit user");
				Document.AddVariable("FormPost", "Taxonomy.EditUser");

				// Build some custom forms
				if (new Integer((String) htSubscriber.get("USERSTATUS")).intValue() == 0) {
					Document.AddVariable("USERSTATUS", "<select name='userstatus'> <option value=1> Active <option value=0 selected> InActive </select>");
				} else {
					Document.AddVariable("USERSTATUS", "<select name='userstatus'> <option value=1 selected> Active <option value=0> InActive </select>");
				}

				if (new Integer((String) htSubscriber.get("EMAILSTATUS")).intValue() == 3) {
					Document.AddVariable("EMAILSTATUS", "<select name='emailstatus'> <option selected value=3> Monthly"+
														"<option value=2> Weekly "+
														"<option value=1> Daily "+
														"<option value=0> Off </select>");
				} else if (new Integer((String) htSubscriber.get("EMAILSTATUS")).intValue() == 2) {
					Document.AddVariable("EMAILSTATUS", "<select name='emailstatus'> <option value=3> Monthly"+
														"<option value=2 selected> Weekly "+
														"<option value=1> Daily "+
														"<option value=0> Off </select>");
				} else if (new Integer((String) htSubscriber.get("EMAILSTATUS")).intValue() == 1) {
					Document.AddVariable("EMAILSTATUS", "<select name='emailstatus'> <option value=3> Monthly"+
														"<option value=2> Weekly "+
														"<option value=1 selected> Daily "+
														"<option value=0> Off </select>");
				} else {
					Document.AddVariable("EMAILSTATUS", "<select name='emailstatus'> <option value=3> Monthly"+
														"<option value=2> Weekly "+
														"<option value=1> Daily "+
														"<option value=0 selected> Off </select>");
				}

				if (new Integer(sResults).intValue() == 150) {
					Document.AddVariable("RESULTSPERPAGE", "<select name=\"results\"><option selected> 150<option> 100<option> 50<option> 25<option> 10</select>");
				} else if (new Integer(sResults).intValue() == 25) {
					Document.AddVariable("RESULTSPERPAGE", "<select name=\"results\"><option> 150<option> 100<option> 50<option selected> 25<option> 10</select>");
				} else if (new Integer(sResults).intValue() == 50) {
					Document.AddVariable("RESULTSPERPAGE", "<select name=\"results\"><option> 150<option> 100<option selected> 50<option> 25<option> 10</select>");
				} else if (new Integer(sResults).intValue() == 100) {
					Document.AddVariable("RESULTSPERPAGE", "<select name=\"results\"><option> 150<option selected> 100<option> 50<option> 25<option> 10</select>");
				} else {
					Document.AddVariable("RESULTSPERPAGE", "<select name=\"results\"><option> 150<option> 100<option> 50<option> 25<option selected> 10</select>");
				}

				if (new Float(sScore).floatValue() == 100.0) {
					Document.AddVariable("SCORETHRESHOLD", "<select name=\"scorethreshold\"> <option value='100.0' selected> Editor reviewed results only <option value='75.0'> Three stars or better <option value='50.0'> Two stars or better <option value='25.0'> One star or better <option value='0.0'> Everything</select>");
				} else if (new Float(sScore).floatValue() == 75.0) {
					Document.AddVariable("SCORETHRESHOLD", "<select name=\"scorethreshold\"> <option value='100.0'> Editor reviewed results only <option value='75.0' selected> Three stars or better <option value='50.0'> Two stars or better <option value='25.0'> One star or better <option value='0.0'> Everything</select>");
				} else if (new Float(sScore).floatValue() == 50.0) {
					Document.AddVariable("SCORETHRESHOLD", "<select name=\"scorethreshold\"> <option value='100.0'> Editor reviewed results only <option value='75.0'> Three stars or better <option value='50.0' selected> Two stars or better <option value='25.0'> One star or better <option value='0.0'> Everything</select>");
				} else if (new Float(sScore).floatValue() == 25.0) {
					Document.AddVariable("SCORETHRESHOLD", "<select name=\"scorethreshold\"> <option value='100.0'> Editor reviewed results only <option value='75.0'> Three stars or better <option value='50.0'> Two stars or better <option value='25.0' selected> One star or better <option value='0.0'> Everything</select>");
				} else {
					Document.AddVariable("SCORETHRESHOLD", "<select name=\"scorethreshold\"> <option value='100.0'> Editor reviewed results only <option value='75.0'> Three stars or better <option value='50.0'> Two stars or better <option value='25.0'> One star or better <option value='0.0' selected> Everything</select>");
				}

				Document.AddVariable("SETRESULTS", "<SCRIPT>document.fm.scorethreshold.selectedvalue = "+sScore+"; "+
												   "document.fm.results.selectedvalue = "+sResults+";</SCRIPT>");
				Document.WriteTemplate(out, "users/header-admin-"+sPage+".tpl");

				if (sPage.equals("userinfo")) {
					Document.WriteTemplate(out, "users/create-user.tpl");
					Document.WriteTemplate(out, "users/create-user-submit.tpl");
				} else if (sPage.equals("alerts")) {
                    out.println("<blockquote>");

                    // Read EMAIL, DISTINGUISHED_NAME from cookie
                    Cookie Cookies[] = req.getCookies();
			        String sEmail = "null"; String sUserDN = "null";

                    if (req.getCookies() != null) {
                        int cLen = Cookies.length;

                        // look through and read from cookies
                        for (int i=0; i < cLen; i++) {
                            if (Cookies[i].getName().equals("ID")) { sUserDN = (String) Cookies[i].getValue(); }
                            if (Cookies[i].getName().equals("EMAIL")) { sEmail = (String) Cookies[i].getValue(); }
                       }
                    }

                    if (Session.cfg.getProp("AuthenticationSystem").toLowerCase().equals("none")) {
                        sEmail = "info@intellisophic.com";
                        Document.AddVariable("EMAIL", sEmail);
                    }

                    if (sEmail.equals("null")) {
                        out.println("A valid e-mail address is required to save alerts.  See your system administrator for more details.");
                    } else {
                        if (((String) props.get("edit")) != null) { EditAlert(props, out); }
                        if (((String) props.get("delete")) != null) { DeleteAlert(sEmail, sUserDN, props, out); }

                        HashTree htAlerts = ViewAlerts(props, sEmail, sUserDN, true, false, false, out);
                        if (htAlerts.size() == 0) { Document.WriteTemplate(out, "users/no-alerts-found.tpl"); }
                        else {
                            // write the alerts to screen
                            Document.WriteTemplate(out, "users/alerts-head.tpl");

                            Enumeration eA = htAlerts.elements();
                            while (eA.hasMoreElements()) {
                                Document.ClearHash();

                                HashTree htAlert = (HashTree) eA.nextElement();
                                String sRunFreq = (String) htAlert.get("RUNFREQ");

                                switch (new Integer(sRunFreq).intValue()) {
                                    case -1: Document.AddVariable("SELECTED0", "SELECTED"); break;
                                    case 1: Document.AddVariable("SELECTED1", "SELECTED"); break;
                                    case 7: Document.AddVariable("SELECTED2", "SELECTED"); break;
                                    case 14: Document.AddVariable("SELECTED3", "SELECTED"); break;
                                    case 21: Document.AddVariable("SELECTED4", "SELECTED"); break;
                                    case 28: Document.AddVariable("SELECTED5", "SELECTED"); break;
                                }
                                Document.SetHash(htAlert);
                                Document.WriteTemplate(out, "users/alert-result.tpl");
                            }

                            Document.WriteTemplate(out, "users/alerts-foot.tpl");
                        }
                    }

					out.println("</blockquote>");
				}

				Document.WriteFooter(out);
			}
			catch (Exception e) {
				HTMLDocument Document = new HTMLDocument();
				String sErrorMsg = e.getMessage();
				Document.WriteError(out, "<BLOCKQUOTE><BR>"+sErrorMsg+"</BLOCKQUOTE>");
				Log.LogError(e, out);
			}
			// ELSE: Corpus name, description, and activity were supplied.   Call the
			//       Create corpus function in the Taxonomy Server API
		} else {
			try {
				// Call: Edit User
				HashTree htArguments = new HashTree(props);
				htArguments.put("UserID", sUserID);
				htArguments.put("ScoreThreshold", sScoreThreshold);
				htArguments.put("ResultsPerPage", sResultsPerPage);
				htArguments.put("UserStatus", sUserStatus);
				htArguments.put("EmailStatus", sEmailStatus);

				InvokeAPI API = new InvokeAPI ("tsother.TSEditSubscriberProps", htArguments);
				HashTree htResults = API.Execute(false, false);

				if (htResults.containsKey("SESSIONEXPIRED")) {
					Server.Login.handleTSapiRequest(props, out, req, res);
					return;
				}

				// A success is indicated if there is no TSERROR tag in the return
				if (htResults.containsKey("TS_ERROR")) {
					throw new Exception("Sorry, this user cannot be altered at this time.");
				}

				Cookie cSessionCookie2 = new Cookie("RESULTSPERPAGE", sResultsPerPage);
				cSessionCookie2.setMaxAge(6592000);

				Cookie cSessionCookie4 = new Cookie("SCORETHRESHOLD", sScoreThreshold);
				cSessionCookie4.setMaxAge(6592000);

				res.addCookie(cSessionCookie2);
				res.addCookie(cSessionCookie4);

				HTMLDocument Document = new HTMLDocument();
				String sSuccess = "Your account information has been edited successfully.<P>"+
								  "<a href='/servlet/Main?template=Taxonomy.Administer'>"+
								  "Click here to return to the main menu</a>.";

				Document.WriteSuccess(out, sSuccess);

			} catch (Exception e) {
				HTMLDocument Document = new HTMLDocument();
				String sErrorMsg = e.getMessage();
				Document.WriteError(out, "<BLOCKQUOTE><BR>"+sErrorMsg+"<P>Please press your <b>back</b> button and correct the mistake.</BLOCKQUOTE>");
				Log.LogError(e, out);
			}

		}
	}
}
