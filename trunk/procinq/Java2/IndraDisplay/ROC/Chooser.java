package ROC;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.iw.system.*;

public class Chooser
{
	public static void handleTSapiRequest ( APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		String sNodeID = (String) props.get ("NodeID", true);	
		String sNodeTitle = (String) props.get ("NodeTitle", "Untitled Node");	
		String sCount = (String) props.get ("Count");
		String submit = (String) props.get ("submit");
		
		if (submit == null) {
			HTMLDocument Document = new HTMLDocument();
			Document.AddVariable("NodeID", sNodeID);
			Document.AddVariable("NodeTitle", sNodeTitle);
			Document.AddVariable("Title", "Add Topic to Validation Queue");
			Document.AddVariable("OPTION_CORPUS_LIST", req);
			Document.WriteTemplate(out, "header-nosearch.tpl");
			Document.WriteTemplate(out, "ROC/chooser.tpl");
			Document.WriteFooter(out);				
		} else {
			String sSuccess = "The topic has been successfully added to the validation list.<P>"+
							  "<a href='/servlet/Main?template=Taxonomy.DisplayResults&NodeID="+sNodeID+"'>"+
							  "Click here to return to the topic results</a>.";
			
			// Documents set for analysis must be stored in the client, though they are
			//   currently stored in the server.   The following API function will select 
			//   the Nth document in the server document set and insert it into the client
			//   database.  An error is returned if no documents were found in the server.
			String sAPIcall = "tsdocument.TSPrepareDocumentsForAnalysis";
			if (com.indraweb.execution.Session.cfg.getProp("ClientAnalysis") != null) { 
				sAPIcall = "tsdocument.TSPrepareClientDocumentsForAnalysis"; 
			}
			
			HashTree htArguments = new HashTree(props);
			htArguments.put("NodeID", sNodeID);
			htArguments.put("Count", sCount);
			InvokeAPI API = new InvokeAPI(sAPIcall, htArguments);
			HashTree htResults = API.Execute(false, false);
			
			if (!htResults.containsKey("SUCCESS")) {
				sSuccess = "Sorry, the topic that you selected has no documents for evaluation.";	
			} else {
				API = new InvokeAPI ("tsnode.TSAddNodeValidation", htArguments);
				htResults = API.Execute(false, false);
			
				if (!htResults.containsKey("SUCCESS")) {
					sSuccess = "Sorry, you do not have permission to add topics "+
							   "to the validation list.";	
				}
			}
			
			HTMLDocument Document = new HTMLDocument();
			Document.WriteSuccess(out, sSuccess);			
		}
	}
}
