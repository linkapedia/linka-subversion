package ROC;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.sql.*;
import java.util.*;

import HTML.*;
import Server.*;
import Logging.*;
import com.indraweb.execution.Session;
import com.iw.system.*;

public class Analysis {
    public static void handleTSapiRequest(APIProps props, PrintWriter out, HttpServletRequest req, HttpServletResponse res)
            throws Exception {

        String sCorpusID = (String) props.get("CorpusID", true);
        String submit = (String) props.get("submit");

        HashTree htArguments = new HashTree(props);
        htArguments.put("CorpusID", sCorpusID);
        InvokeAPI API = new InvokeAPI("tsclassify.TSGetROCdata", htArguments);
        try { API.Execute(false, false); } catch (Exception e) { Log.LogError(e); }

        String sResultInfo = "ROC Generate files process completed.";

        HTMLDocument Document = new HTMLDocument();
        Document.WriteSuccess(out, sResultInfo);
    }

}
