package com.iw.fcrawl;

import java.io.*;
import java.util.*;

// Generic Document object (without node relationship)
public class ClassifyException extends Exception
{
    public String TS_ERROR_CODE = "-1";
    public String TS_ERROR_CODE_STRING = "";
    public String TS_ERROR_TYPE = "";
    public String TS_ERROR_DESC = "";
    public String TS_EXCEPTION_GETMESSAGE = "";

	public ClassifyException () {}
    public ClassifyException (String ErrorCode, String ErrorCodeString, String ErrorType,
                              String ErrorDesc, String ErrorMessage) {
        TS_ERROR_CODE = ErrorCode;
        TS_ERROR_CODE_STRING = ErrorCodeString;
        TS_ERROR_TYPE = ErrorType;
        TS_ERROR_DESC = ErrorDesc;
        TS_EXCEPTION_GETMESSAGE = ErrorMessage;
    }
}
