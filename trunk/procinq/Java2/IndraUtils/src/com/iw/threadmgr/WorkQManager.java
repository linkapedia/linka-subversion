/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Jan 14, 2003
 * Time: 10:54:31 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.threadmgr;


import com.indraweb.util.Log;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import java.lang.reflect.Array;

/**
 * Use this one class to implement a managed producer-consumer problem with:
 * 1) full blocking Q - user can specify optional cap on number of to do items to sit in Q
 *    so as not to overrun memory, eat up hi priority work from other machines etc)
 * 2) empty blocking Q - when the consumer thread asks for more work and none is there
 *    it will block until some is there unless producers have stopped producing
 *
 */

// use this class (via subclassing) as the container/ manager for ALL (within JVM only?)
// producer - consumer work problems

// implements a FIFO Queue for multi-threaded producers and consumers to feed
// coordination based on empty and full semaphore objects
// uses empty and full states with couting for full/max state to stop producer threads
// (needed for getnextnode) in which we could be putting
// no busy waits
// can put a cap on max num.
// fully synch - should be no MT corruptions
// two arrays of not yet running instances of threads are sent in - to tell them which work Q to use
//   then start them
// then monitor them
// HAS A MAX TIME TO WAIT FOR PRODUCER TO COMPLETE
// KEEPS STATISTICS FROM RUNS
// IN A SEPARATE PACKAGE
// includes db connection pooler
// includes api machine logins
// goes thru tomcat






public class WorkQManager {

    private int iFullSize = -1; // block producer threads if this is > 0 and Q size is at that #.

    ThreadGenericProdCons[]  arrProducerThreads;
    ThreadGenericProdCons[]  arrConsumerThreads;
    private Vector vQ = new Vector(); // work Q

    //PRODUCER WAITS
    private boolean bProducersDone = false;
    private int iMSTimeoutProducersToComplete = -1;
    private boolean bTimedOutWaitingForProducersToComplete = false;
    private boolean bDidProducersTimeout = false;
    //ALL THREADS WAITS
    private boolean bAllThreadsPandCDone = false;
    private int iMSTimeoutAllThreadsToComplete = -1;
    private boolean bTimedOutWaitingForAllThreadsPandCToComplete = false;
    private boolean bDidAllThreadsTimeout = false;

    private int iCapOnTotalUnitsAcceptedByProducers = -1;
    private boolean bGotFirstConsumerToSayNoWorkAndProducersDone = false;

    private Hashtable htResultData = null;
    Vector vMasterThreadSet = null;

    private WorkQStatistics workStats = new WorkQStatistics();
    //private ThreadGenericProdCons[]  arrProducerThreads = null;
   // private ThreadGenericProdCons[]  arrConsumerThreads = null;

    /**
     * this is a multi thread manager - relieves the using programmer of thread management
     *
     * caller creates a set of
     *
     * TO DO : create a mode where there are only producer thread who do work in a loop say
     * donig a DB update.  really the Node Feeder should run that way - no need to precache
     * NextNode
     *
     * -1 for iFullSize_ means no max
     *
     * -1 for iMaxNumMSToWaitForProducersThreadsToComplete means indefinite
     *  wait for producer threads to complete
     */
    public WorkQManager (
                    int iFullSize_,
                    int iMSTimeoutProducersToComplete_,
                    int iMSTimeoutAllThreadsToComplete_,
                    int iCapOnTotalUnitsAcceptedByProducers_
                )
    {
        iFullSize = iFullSize_;
        iMSTimeoutProducersToComplete = iMSTimeoutProducersToComplete_;
        iMSTimeoutAllThreadsToComplete = iMSTimeoutAllThreadsToComplete_;
        iCapOnTotalUnitsAcceptedByProducers = iCapOnTotalUnitsAcceptedByProducers_;
    }


    public void setAndStartThreads (
            ThreadGenericProdCons[]  arrProducerThreads_,
            ThreadGenericProdCons[]  arrConsumerThreads_,
            boolean bSynchronous
            //Hashtable[] htArrPendingsToMonitor
        ) throws Exception
    {

        arrProducerThreads = arrProducerThreads_;
        arrConsumerThreads = arrConsumerThreads_;

        long lStart = System.currentTimeMillis();
        //System.out.println("work Q manager in setAndStartThreads ");
        if ( bSynchronous )
            vMasterThreadSet = new Vector ();
        // start all producer threads running
        if ( arrProducerThreads_ != null )
        {
            for ( int i = 0; i < arrProducerThreads_.length; i++ )
            {
                arrProducerThreads_[i].start();
                if ( bSynchronous )
                    vMasterThreadSet.addElement ( arrProducerThreads_[i] );   // ****************
            }

            //System.out.println("done starting [" + arrProducerThreads_.length + "] producer threads");

            // join waits until all producer threads are completed to set the "work done" flag
            // bFeedersDone and tell all waiters that no more work will be coming
            AllThreadsDoneCheckThread producersDoneCheckThread = new AllThreadsDoneCheckThread
                    ( arrProducerThreads_,
                      iMSTimeoutProducersToComplete,
                      true,
                      workStats
                    );
            producersDoneCheckThread.setName("producersDoneCheckThread");
            producersDoneCheckThread.start(); //sets a boolean flag to true when
            if ( bSynchronous ) // not needed otherwise
                vMasterThreadSet.addElement ( producersDoneCheckThread );  // ****************
        } else{
            // running in "consumer gets its own work item" mode
        }


        // start all consumer threads running
        if ( arrConsumerThreads_ != null )
        {
            for ( int i = 0; i < arrConsumerThreads_.length; i++ )
            {
                if ( bSynchronous )
                    vMasterThreadSet.addElement ( arrConsumerThreads_[i] );   // ****************
                arrConsumerThreads_[i].start();
            }

            //System.out.println("done starting [" + arrConsumerThreads_.length + "] consumer threads");

/*
            ThreadGenericProdCons[] tArrPandC = concatTArrs (arrProducerThreads_, arrConsumerThreads_);
            AllThreadsDoneCheckThread allThreadsDoneCheckThread = new AllThreadsDoneCheckThread
                    ( tArrPandC ,
                      iMSTimeoutAllThreadsToComplete,
                      false);
            // provides a global timeout watchdog
            allThreadsDoneCheckThread.start(); //sets a boolean flag to true when
            if ( bSynchronous )
                vMasterThreadSet.addElement ( allThreadsDoneCheckThread );
*/
        }
        if ( bSynchronous )
        {
            //System.out.println ("start waiting all threads");
            synchWaitAllThreads ( vMasterThreadSet );
            //System.out.println ("done waiting all threads");
        }

        //System.out.println("work Q manager setAndStartThreads done bSynchronous  [" + bSynchronous + "] in ms [" + (System.currentTimeMillis() - lStart) + "]");
    }

    /**
     * this class is for the sole purpose of having a thread test for
     * when all producer threads have stopped
     * ... test to see if all producer threads are done
     */
    private class AllThreadsDoneCheckThread extends Thread
    {
        // sets bProducersDone to true when all declared producer threads are no longer running.
        ThreadGenericProdCons[]  tArrThreadsToWatch;
        int iMaxNumMSToWaitForProducersThreadsToComplete = 60000;
        boolean bProducersOnly;
        WorkQStatistics workStats ;

        public AllThreadsDoneCheckThread (
                ThreadGenericProdCons[] arrThreadsToWatch_,
                int iMaxNumMSToWaitForProducersThreadsToComplete_,
                boolean bProducersOnly_,
                WorkQStatistics workStats_
           )
        {
            iMaxNumMSToWaitForProducersThreadsToComplete = iMaxNumMSToWaitForProducersThreadsToComplete_;
            tArrThreadsToWatch = arrThreadsToWatch_;
            bProducersOnly = bProducersOnly_;
            workStats = workStats_;
        }


        /**
         * run this thread for the sole purpose of knowing when all threads have stopped
         */
        public void run()
        {
            try
            {
                // wait until all threads have stopped procesing
                int iMSTimeoutThis = -1;
                if ( bProducersOnly )
                    iMSTimeoutThis = iMSTimeoutProducersToComplete;
                else
                    iMSTimeoutThis = iMSTimeoutAllThreadsToComplete;


                long lTimeStartWaiting = System.currentTimeMillis();
                for ( int i = 0; i < tArrThreadsToWatch.length; i++ )
                {
                    if ( iMSTimeoutThis > 0 )
                    {
                        long lTimeElapsed = System.currentTimeMillis() - lTimeStartWaiting;
                        tArrThreadsToWatch[i].join(
                                iMSTimeoutThis - lTimeElapsed );
                    }
                    else
                        tArrThreadsToWatch[i].join(); // no wait max
                }

                // did we time out on producers
                if ( iMSTimeoutThis > 0 &&
                        System.currentTimeMillis() - lTimeStartWaiting >  iMSTimeoutThis )
                {
                    if ( bProducersOnly) {
                        bDidProducersTimeout = true;
                        Log.log ("timed out waiting on producer threads");
                    } else {
                        bDidAllThreadsTimeout = true;
                        Log.log ("timed out waiting on all threads ");
                    }
                }
                tellAllThreadsStopLooping ( tArrThreadsToWatch );
                // to do keep a master thread counter for monitor

                // to do take an accounting Succ.s/Fail etc

            }
            catch ( Exception e )
            {
                Log.log ("error in 'wait until all threads have stopped procesing'", e);
            }
            finally
            {
                bProducersDone = true;
                //if ( bProducersOnly )
                    //Log.logcr("Producer thread group done " +
                      //  "iNumJobsAdded [" + workStats.iNumJobsAdded + "] ");
                //else
                    //Log.logcr("Producer and Consumer thread group done " +
                      //      "iNumJobsRemoved [" + workStats.iNumJobsAdded + "] " +
                        //    "iNumJobsSuccessful [" + workStats.iNumJobsAdded + "] " +
                         //    "iNumJobsFailed [" + workStats.iNumJobsAdded + "] ");

            }
        }
    }

    /**
     * add a to do to the work Q
     */
    public void addWorkItem ( Object o, String sDesc ) throws Exception
    {
        if ( iCapOnTotalUnitsAcceptedByProducers < 0 || workStats.iNumJobsAdded < iCapOnTotalUnitsAcceptedByProducers )
        {
            while ( true )
            {
                boolean bPause = false;
                synchronized ( vQ )
                {
                    if ( iFullSize > 0 && vQ.size() >= iFullSize )
                    {
                        bPause = true;
                    }
                    else
                    {
                        workStats.iNumJobsAdded++;
                        //if ( workStats.iNumJobsAdded % 20 == 0 )
                          //  System.out.println ( "P. addworkitem t [" + Thread.currentThread().getName() + "] added sDesc [" + sDesc + "]." + getQsize() );
                        vQ.addElement( o );
    return;  // EARLY RETURN - CAREFUL HERE THIS GETS OUT OF A WHILE
                        // don't want to loop pause if added
                    }
                }

                if (bPause) // be sure to wait OUTSIDE the synch
                {
                    synchronized (this)                {
                        // int iMSWait  = arrConsumerThreads.length * 1000;    // all threads will be waiting
                        int iMSWait  = 3000;         // if buffer is full how long til I recheck it
                        // BUFFER FULL MESSAGE:
                        //System.out.println(new java.util.Date() + " P. Buffer full t [" + Thread.currentThread().getName() + "] addWork waits desc[" + sDesc + "] qsize:" + getQsize() + " waiting [" +
                        // iMSWait + "]");
                        com.indraweb.util.clsUtils.pause_this_many_milliseconds(iMSWait);
                    }
                } // if pause
            } // while
        } // if not already reached the cap
        else
            Log.logcr ( "hit cap of [" + iCapOnTotalUnitsAcceptedByProducers+ "] units into work Q" );
    }

    /**
     * get a to do to from the work Q and delete the work item from the Q
     */
    private Object oWaitSynch = new Object();
    public Object getWorkItem ()  throws Exception
    {
        Object oReturn = null;
        int iLoop = 0;
        while ( oReturn == null )
        {
            boolean bPause = false;
            synchronized ( vQ )
            {
                if (vQ.size() == 0 && bProducersDone )
                {
                    if ( workStats.iNumJobsAdded == 0 )
                        Log.log ("WARNING: all workQ producers done but no work jobs added\r\n");
                    bGotFirstConsumerToSayNoWorkAndProducersDone = true;
                    //System.out.println("C. done getworkitem NULL t [" + Thread.currentThread().getName() + "]  producers done true" + getQsize());
return null;  // EARLY RETURN
                }
                else if ( vQ.size() == 0 )
                     bPause = true;
                else
                    oReturn = (Object) vQ.remove ( 0 );
            }
            if (bPause && !bGotFirstConsumerToSayNoWorkAndProducersDone ) // be sure to wait OUTSIDE the synch
            {
                int iMSWait  = 1000;
                //if ( iLoop % 20 == 0 )
                    //System.out.println(new java.util.Date() + "loop [" + iLoop + "P. Buffer empty getWork waits desc qsize:" + getQsize() + " waiting [" +
                      //  iMSWait + "] bProducersDone [" + bProducersDone + "]");
                synchronized (oWaitSynch)  // as a gate stops parallel waiting and thus hi thruput
                {
                    com.indraweb.util.clsUtils.pause_this_many_milliseconds(iMSWait);
                }
            } // if pause
        }
        //System.out.println("C. done getworkitem t [" + Thread.currentThread().getName() + "] ." + getQsize() + " bProducersDone [" + bProducersDone + "]" );

        workStats.iNumJobsRemoved++;  // nulls were returned earlier
        return oReturn;
    }

    public int getQsize()  throws Exception
    {
        return vQ.size();
    }


    /**
     * result information from each thread comes back
     */
    public void addResultData ( int iWhichDataSet, double dResultValuePerWorkItem )
    {

        synchronized ( workStats )
        {
            Vector vDoubles = (Vector) workStats.htIntegerDataSet_to_VDoubles_PerCompletedJobResult.get
                (new Integer (iWhichDataSet) );
            if ( vDoubles == null )
            {
                vDoubles = new Vector();
                workStats.htIntegerDataSet_to_VDoubles_PerCompletedJobResult.put (
                        new Integer ( iWhichDataSet ), vDoubles );
            }
            vDoubles.addElement (new Double ( dResultValuePerWorkItem ));
        }
    }

    public Vector getWorkStats (int iDAtaSetID)
    {
        return (Vector) workStats.htIntegerDataSet_to_VDoubles_PerCompletedJobResult.get ( new Integer(iDAtaSetID) );
    }
    /**
     * try to stop running gracefully - tell all threads that the master wants you to stop
     */
    public void tellAllThreadsStopLooping(ThreadGenericProdCons[] tArrThreads)
    {
        // tell all threads to stop looping
        for ( int i = 0; i < tArrThreads.length; i++ )
            tArrThreads[i].stopLooping();

    }

    public static void synchWaitAllThreads(Vector vThreads) throws Exception
    {
        //System.out.println( "synchWaitAllThreads on [" + vThreads.size() + "] threads." );
        Enumeration e = vThreads.elements();
        int i = 1;
        while ( e.hasMoreElements() )
        {
            Thread  t = (Thread) e.nextElement();
            //System.out.println( "start synch waiting on thread [" + t.getName() + "] index (1-base) [" + i + "] of vSize [" + vThreads.size() + "]" );
            t.join();
            //System.out.println( "done synch waiting on thread [" + t.getName() + "]" );
            i++;
        }
        //System.out.println( "done synchWaitAllThreads on all [" + vThreads.size() + "] threads." );

    }

    private ThreadGenericProdCons[] concatTArrs  (
            ThreadGenericProdCons[] tarr1,
            ThreadGenericProdCons[] tarr2)
    {
        int iArr1Size = -1;         int iArr2Size = -1;
        if ( tarr1 == null ) iArr1Size  = 0;
        else iArr1Size = tarr1.length;

        if ( tarr2 == null ) iArr2Size  = 0;
        else iArr2Size = tarr2.length;

        ThreadGenericProdCons[] tArrRet = new ThreadGenericProdCons [iArr1Size + iArr2Size];

        for ( int i = 0; i < iArr1Size; i++ )         {
            tArrRet[i] = tarr1[i];
        }
        for ( int i = 0; i < iArr2Size; i++ )         {
            tArrRet[i+iArr1Size] = tarr2[i];
        }
        return tArrRet;

    }

    public int getNumJobsAdded()
    {
        return workStats.iNumJobsAdded;
    }
    public int getNumJobsRemoved()
    {
        return workStats.iNumJobsRemoved;
    }

    public void setbProducersDone()
    {
        bProducersDone = true;  // created for test mode when just manually inserting one URL in MS and Score thread world
    }


    public class WorkQStatistics
    {
        public int iNumJobsAdded = 0;
        public int iNumJobsRemoved = 0;
        public int iNumJobsSuccessful = 0;
        public int iNumJobsFailed = 0;
        public Hashtable htIntegerDataSet_to_VDoubles_PerCompletedJobResult = new Hashtable();
    }

    public Vector getMasterThreadSet()
    {
        return vMasterThreadSet;
    }


}



