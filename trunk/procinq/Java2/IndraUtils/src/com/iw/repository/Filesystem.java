package com.iw.repository;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

import com.iw.system.*;
import com.iw.tools.*;
import com.iw.fcrawl.*;
import com.iw.fcrawl.Machine;

// This is the repository class for type Filesystem.   This class is assumed to be
//  an NT filesystem.   Please use the UNIXFilesystem class for UNIX based filesystems.
public class Filesystem extends Repository
{
	// Private variable htVectors contains an entry for each corpus containing
	//   documents tagged to be reindexed through Verity
	private Hashtable htVectors = new Hashtable();
	public int iClassifyCounter = 0;
	public int iCrawlCounter = 0;

    private FileFilter hff = new FileFilter();

    public Filesystem () { }

	// Specific filesystem routine to retrieve new objects on the filesystem that have
	//  changed since DateLastCapture.
	public void GetNewObjects(String sLocation, String sDateLastCapture, boolean bRecurse) {
		// First: if recurse boolean is set to true, we need to recurse through every
		//  subdirectory underneath sLocation and concatenate all results (eww!!)
		//
		// Update: 10/15/2002 -- now that we are using threading, grab documents as well
		//  as directories while we recurse.  Put all documents into the vector as they are
		//  found.   They will be processed by waiting classifier threads.
        //System.out.println("\n\rFileSystem: Looking for classification candidates in "+sLocation);

		// initialize directory information
		File fDirectory = new File(sLocation);
		String FilePaths[] = fDirectory.list();

        //System.out.println("Total files found (including directories) directly under "+sLocation+" is "+FilePaths.length);

		// Format DateLastCapture into a Calendar object
		String sLastCapture = sDateLastCapture.substring(0, 19);
		Calendar c = Calendar.getInstance();
		c.set(new Integer(sDateLastCapture.substring(0,4)).intValue(), // Year
			  new Integer(sDateLastCapture.substring(5,7)).intValue()-1, // Month
			  new Integer(sDateLastCapture.substring(8,10)).intValue(), // Day
			  new Integer(sDateLastCapture.substring(11,13)).intValue(), // Hour
			  new Integer(sDateLastCapture.substring(14,16)).intValue(), // Minutes
			  new Integer(sDateLastCapture.substring(17,19)).intValue()); // Seconds

		//System.out.println(FilePaths.length+" files found in directory "+
		//				   fDirectory.getAbsolutePath());

		// loop through each file in this directory
		if (FilePaths != null) {
		for (int i=0; i<FilePaths.length; i++) {
			File f = new File(sLocation+"/"+FilePaths[i]);

			if (!f.isDirectory() && (!f.getName().equals(".file"))) {
				long l = f.lastModified();
				java.util.Date d = new java.util.Date(l);
				java.util.Date dc = c.getTime();

				//System.out.println("Filesystem:Checking file ("+FilePaths[i]+") last modified: "+d.toString()+
				//				   " vs "+dc.toString());
				if (d.after(dc)) {
					System.out.print("."); System.out.flush();
					iCrawlCounter++;
					//System.out.println("File: "+FilePaths[i]+" changed. Last modified: "+d.toString());
					synchronized (this) { AddNewDocument(f.getAbsolutePath()); }
				} else {
                    System.out.print("."); System.out.flush();
                    //System.out.println("File: "+FilePaths[i]+" has not changed. It will not be classified. Last modified: "+d.toString());
                }
			}
            // if this is a directory and recurse is true, continue collecting
			if (f.isDirectory() && bRecurse) {
				GetNewObjects(sLocation+"/"+FilePaths[i],
							  sDateLastCapture, true);
			}
		}}

		System.out.println("");
		System.out.flush();
	}

	// These three functions are repository specific and must be overridden
	public boolean ClassifyDocument (String sPath, com.iw.fcrawl.Machine M) throws com.iw.fcrawl.ClassifyException {
		HashTree htArguments = GetArguments();
        File tempFile = null;

		if (iCrawlCounter == 0) { iCrawlCounter = GetNewDocuments().size(); }

		System.out.println("Classifying: "+sPath);

		try {
            String sPostedPath = sPath;

            if (sPath.toLowerCase().endsWith(".pdf")) {
                tempFile = FcrawlUtils.PDFtoHTML(new File(sPath));
                sPostedPath = tempFile.getAbsolutePath();
            } else if (sPath.toLowerCase().endsWith(".doc")) {
                tempFile = FcrawlUtils.MSWORDtoHTML(new File(sPath));
                sPostedPath = tempFile.getAbsolutePath();
            }

			String api = M.GetAPI();
			String rid = (String) htArguments.get("rid");
			String gid = (String) htArguments.get("gid");
			String batch = (String) htArguments.get("batch");
			String docsumm = (String) htArguments.get("docsumm");
            String corpora = (String) htArguments.get("corpora");

			HashTree htLocalArguments = new HashTree();

			htLocalArguments.put("SKEY", M.GetSKEY());
			htLocalArguments.put("RepositoryID", rid);
            htLocalArguments.put("api", api);
			htLocalArguments.put("ShowScores", "false");
			htLocalArguments.put("DBURL", sPath);
			htLocalArguments.put("post", "true");
			htLocalArguments.put("batch", batch);
			htLocalArguments.put("WANTDOCSUMMARY", "true");
			if (gid != null) { htLocalArguments.put("GenreID", gid); }
            if (corpora != null) { htLocalArguments.put("Corpora", corpora); }

            try {
                InvokeAPI API = new InvokeAPI ("tsclassify.TSClassifyDoc", htLocalArguments);
                System.out.println("Posting file: "+sPostedPath);
                HashTree htResults = API.PostExecute(new File(sPostedPath));
                iClassifyCounter++;

                System.out.println(iClassifyCounter+" of "+iCrawlCounter+" complete.");
                System.out.println("\nFilesystem:Document: "+sPath+" classified successfully! ("+iClassifyCounter+")");
            } catch (com.iw.fcrawl.ClassifyException ce) {
                System.out.println("There was a system error while classifying document: "+sPath);
                System.out.println("Error code: "+ce.TS_ERROR_CODE);
                System.out.println("Error message: "+ce.TS_EXCEPTION_GETMESSAGE);
                System.out.println("Error description: "+ce.TS_ERROR_DESC);
                System.out.println("");

                throw ce;
            } finally { if (tempFile != null) tempFile.delete(); }

		} catch (Exception e) { e.printStackTrace(System.out); return false; }

		return true;
	}
	public boolean UpdateDocumentSecurity (String sPath, Machine M) { return true; }
	public boolean ReindexDocuments () { return true; }

    // override standard repository method and derive date from the filesystem
	public String GetCurrentDate(HashTree htFolder) throws Exception {
		String sPath = (String) htFolder.get("PATH");

        try {
            File f = new File("/temp/.file");
            FileOutputStream out = new FileOutputStream(f);
            out.write(
                    0); out.close();

            // Convert to: 'MM/DD/YYYY HH24:MI:SS'
            java.util.Date d = new java.util.Date(f.lastModified());
            SimpleDateFormat gmtf = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");
            //gmtf.setTimeZone( TimeZone.getTimeZone( "UTC" ) );

            System.out.println("Current date: "+gmtf.format(d));
            f.delete();

            return gmtf.format(d);

        } catch (Exception e) {
            System.out.println("Warning! Could not write a file to the target filesystem.");
            e.printStackTrace(System.out);

            return GetCurrentDate();
        }
    }

	// Lookup this document to find all associated corpora.   Then put the document
	//  path into the vector for each corpus, plus the overall vector.   These vectors
	//  will later be used to reindex documents in the full text verity engine.
	public void AddDocumentIndex(String sPath, Machine M) {

		/* This will be re-done differently, soon.
		HashTree htArguments = GetArguments();

		htArguments.put("Path", sPath);
		InvokeAPI API = new InvokeAPI ("tsdocument.TSGetDocumentCorpora", htArguments);
		HashTree htResults = API.Execute();

		if (!htResults.containsKey("DOCUMENT")) {
			//System.out.println("The document "+sPath+" did not classify into any topics.");
			return;
		} */
		return;
	}

	// constructor: just use the standard constructor
	public Filesystem (HashTree ht, HashTree ht2) {
		SetArguments(ht2);
	}

    // allow any HTML, text, word document or PDF files
	class FileFilter implements FilenameFilter {
	 public boolean accept (File dir, String name) {
	     if ((name.toLowerCase().endsWith ("html")) ||
             (name.toLowerCase().endsWith (".txt")) ||
             (name.toLowerCase().endsWith (".doc")) ||
             (name.toLowerCase().endsWith (".pdf")) ||
			 (name.toLowerCase().endsWith ("htm")))
	          return true;
	     else
	         return false;
	 }
}

}
