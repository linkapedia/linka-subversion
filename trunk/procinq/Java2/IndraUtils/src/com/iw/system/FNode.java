package com.iw.system;

import java.io.*;
import java.util.*;

import com.iw.tools.*;

// Generic Node object
public class FNode
{
	// variables
	public String NodeID;
	public String NodeTitle;
	public Hashtable htSignatures = new Hashtable();
	
	// private variables
	private String CQL;

	public FNode AddSignature (String SigWord, String SigFreq) {
		htSignatures.put(SigWord, SigFreq); return this; 
	}
	
	public String BuildCQL() {
		CQL = "SELECT <DOCUMENT> WHERE FULLTEXT LIKE '("; // beginning CQL

		Vector v = com.iw.fcrawl.UtilStrings.split(NodeTitle, " "); // split into words
		Enumeration eV = v.elements(); int loop = 0;
		while (eV.hasMoreElements()) {
            String sWord = (String) eV.nextElement();
            if (!(sWord.toLowerCase().equals("and")) &&
                !(sWord.toLowerCase().equals("or")) &&
                !(sWord.toLowerCase().equals("not"))) {
                loop++;
                if (loop != 1) { CQL = CQL + " or "; }
                CQL = CQL + "$"+sWord;
            }}
		CQL = CQL + ")";
		  
		if (htSignatures.size() > 0) { CQL = CQL + " AND"; }
		
		Enumeration eH = htSignatures.keys(); loop = 0;
		while (eH.hasMoreElements()) {
			loop++; 
			if (loop == 1) { CQL = CQL + " ("; }
			else { CQL = CQL + " ACCUM "; }
			String sWord = (String) eH.nextElement();
			String sFreq = (String) htSignatures.get(sWord);

            // intermedia does now allow weighting above 10
            int iFreq = new Integer(sFreq).intValue();
            if (iFreq > 10) { sFreq = "10"; }

			CQL = CQL + sWord+"*"+sFreq;
		}
		
		if (htSignatures.size() > 0) { CQL = CQL + ")"; }
	
		CQL = CQL + "'";
		return CQL;
	}
	
	// accessor method for CQL
	public String GetCQL() { return CQL; }
	
	// Object constructor(s)
	public FNode (String NodeID, String NodeTitle) {
		this.NodeID = NodeID; this.NodeTitle = NodeTitle;
	}
}
