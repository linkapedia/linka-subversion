package com.iw.tools;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import xtrim.client.QueryControl;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Vector;
import java.net.HttpURLConnection;
import java.net.URL;
import xtrim.data.Constraint;
import xtrim.data.Domain;
import xtrim.data.DomainConfig;
import xtrim.data.EditableQuery;
import xtrim.data.Result;
import xtrim.data.XtrimEvent;
import xtrim.rmiapi.LoginException;
import xtrim.rmiapi.QueryFactory;
import xtrim.rmiapi.RmiApi;
import xtrim.rmiapi.RmiResultReceiver;
import xtrim.rmiapi.RmiSession;

public class AskOnce extends UnicastRemoteObject implements RmiResultReceiver {

    private RmiSession session = null;
    private RmiApi api = null;

    private String lookUpServerName = "localhost";
    private int lookUpServerHost = 3005;
    private String ecisRmiName = "xtrim.RmiApi";
    private Result results[];

    Result finalres[] = null;

    public AskOnce(String ecisLogin, String ecisPwd, String lookUpServerName, String query) throws Exception {
        super();

        if (lookUpServerName != null) this.lookUpServerName = lookUpServerName;

        String url = "//" + lookUpServerName + ":" + lookUpServerHost + "/" + ecisRmiName;
        api = (RmiApi) Naming.lookup(url);

        // log into the server
        try {
            session = api.login(ecisLogin, ecisPwd, "ProcinQ Connection", false);
            System.out.println("Login successful! Session is: " + session);

            runQuery(query);

            //System.out.println("query completed");
            results = getResults();
        } catch (RemoteException re) {
            re.printStackTrace(System.out);
            System.err.println("Remote exception: Unable to log in.");
            System.exit(1);
        } catch (LoginException le) {
            le.printStackTrace(System.out);
            System.err.println("Log in exception: Unable to log in.");
            System.exit(1);
        }

    }

    public Vector processResults (String fileLocation) {
        Vector v = new Vector();

        // log into the server
        try {
            // (D) for each result, download it and add it to the queue if it ..
            for (int i = 0; i < results.length; i++) {
                Result r = results[i];
                String backend = r.getBackendName();
                File dir = new File(fileLocation+(backend.replaceAll("/", "-")));
                if (!dir.exists()) dir.mkdir();

                // print out the results that would be normally inserted into the ProcinQ database
                System.out.println("TITLE: "+r.getAttribute("title").getValue()+" URL: "+r.getAttribute("URL").getValue());

                // download the file over HTTP
                try { v.add(downloadResult(r, dir)); }
                catch (Exception e) { e.printStackTrace(System.err); }
            }
        } catch (Exception le) {
            le.printStackTrace(System.out);
        }

        return v;
    }

    public File downloadResult(Result r, File dir) throws Exception {
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");

        try {
            URL myURL = new URL(r.getAttribute("URL").getValue());

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestProperty("Content-Encoding", "UTF8");
            httpCon.setRequestProperty("Content-Type", "application/www-form-urlencoding; charset=UTF8");

            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedReader dis = new BufferedReader(new InputStreamReader(is, "UTF8"));

            String record = null;

            while ( (record=dis.readLine()) != null ) { returnString.append(record); }

            File f = new File(dir+"/"+r.getAttribute("record_id").getValue());
            System.out.println("Writing to "+f.getAbsolutePath());
            BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(f));
            OutputStreamWriter out = new OutputStreamWriter(bout, "UTF-8");

            out.write(returnString.toString());
            out.close();

            return f;
        } catch (Exception ex) { throw ex; }
    }

    // accessor function for results
    public Result[] getResults() { return finalres; }

    public static void logSpecial(String lineToadd) {
        File f = new File("E:/test/test.log");

        try {
            // works both for existing and new files
            PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
            file.write(lineToadd);
            file.close();
        } catch (Exception e) {
            return;
        }
    }

    /**
     * My first method to say hello to ECIS (connects, sends a first query, and then prints results to console).
     * @throws Exception if an error occurs.
     */
    public void runQuery(String searchedValue) throws Exception {
        //String lookUpServerName = "localhost";

        try {
            DomainConfig domainConfig = api.getDomainHierarchy();
            Domain rootDomain = domainConfig.getRootDomain();
            Domain domain = rootDomain.getSubDomainFromName("JSP");
            Enumeration backends = domain.getBackends();
            while (backends.hasMoreElements()) {
                Domain backend = (Domain) backends.nextElement();

                // Work with this backend...
                System.out.println(backend.getInternalName());

                // print the domain hierarchy
                EditableQuery query = QueryFactory.createQuery(session.getId(), 2);
                query.setDomain(backend);
                query.addConstraint(QueryFactory.createConstraint("full-text", Constraint.CONTAINS, searchedValue));
                //System.out.println("Read to submit query: " + query);

                // submit request
                api.search(query, this, true, session);
                //System.out.println("Submitted query: " + query + " .. Waiting for events and results.");

                // wait until notified of completion
                synchronized (this) {
                    this.wait();
                }

                //System.out.println("Query is complete!  Hello World!!");
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);

        } finally {
            api.logout(session);
        }
    }

    public void resultsReceived(RmiSession session, long queryId, String source, Result res[])
            throws RemoteException {
        //System.out.println("Received results from query: " + queryId + " from source " + source);
        //for (int i = 0; i < res.length; i++) {
        //    System.out.println("Res " + i + ":" + res[i]);
        //}

        this.finalres = res;
    }

    public void eventReceived(RmiSession session, long queryId, XtrimEvent event)
            throws RemoteException {
        //System.out.println("Received event from query: " + queryId + " " + event);
    }

    public void queryCompleted(RmiSession session, long queryId) throws RemoteException {
        //System.out.println("Received query completion notif for query: " + queryId + "and session: " + session);

        synchronized (this) {
            this.notify();
        }
    }

    public void queryRestored(RmiSession session, QueryControl query) throws RemoteException {
        // ignore
    }

}