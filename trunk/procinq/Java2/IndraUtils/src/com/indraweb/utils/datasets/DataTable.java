/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Nov 1, 2003
 * Time: 2:46:53 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */

package com.indraweb.utils.datasets;

import com.indraweb.util.UtilFile;

import java.sql.*;
import java.util.Vector;
import java.util.Enumeration;

import api.Log;

public class DataTable
{
    public Object[][] oArrArrTableData = null;
    public int iNumCols = -1;
    public int iNumRows = -1;
    public long lCachedTime = -1;
    public String sQueryName = null;

    public DataTable (String sQueryName, String sSQL, int iMaxRecs, Connection dbc ) throws Exception
    {
        oArrArrTableData = callDBDForData ( sSQL,iMaxRecs, dbc );
        lCachedTime = System.currentTimeMillis();
        this.sQueryName = sQueryName;
    }

    public Object[][] getoArrArrData ()
    {
        return oArrArrTableData;
    }

    private Object[][] callDBDForData (String sSQL, int iNumRecordsWanted_100Resolution, Connection dbc) throws Exception
    {
		Statement stmt = null;
		ResultSet rs = null;
        Object[][] oArrArrTableData = null;
		try {
			stmt = dbc.createStatement();
            System.out.println("starting data load sSQL [" + sSQL + "]" );
			rs = stmt.executeQuery (sSQL);

			long l = -1;

			int iNumRowsReturnedThusFar = 0;
			ResultSetMetaData rsMD = rs.getMetaData();
            iNumCols = rsMD.getColumnCount();

            Vector vObjArrRows = new Vector();
            int iRowIndex = -1;
			while ( rs.next() )
			{
                iRowIndex++;
/*
				if ( iRowIndex == 1 )
				{
					for ( int i = 0; i < iColCount; i++ )
					{
							String s5 = rs.getMetaData().getColumnTypeName( i + 1 ) ;
					}
				}
*/

                Object[] oArrRow = new Object[iNumCols];
				for ( int icolIndex = 0; icolIndex < iNumCols; icolIndex++ )
				{
					oArrRow [ icolIndex ] = rs.getObject ( icolIndex + 1 ) ;
                    //System.out.println("dt set value [" + oArrRow [ icolIndex ] + "]" );
				}

                vObjArrRows.addElement(oArrRow);

                if ( iRowIndex % 100 == 0 )
                {
                    if ( iRowIndex % 5000 == 0 )
                        Log.Log ("datatable getting rec #[" + iNumRowsReturnedThusFar + "] on sql [" + sSQL + "]\r\n");

                    if ( iNumRecordsWanted_100Resolution > 0 &&
                            iRowIndex >= iNumRecordsWanted_100Resolution )
                    {
                        Log.Log ("capped num recs in data table on sql [" + sSQL + "]\r\n");
                        break;
                    }
                }
			}

            iNumRows = vObjArrRows.size();
            System.out.println("completed data load iNumRows [" + iNumRows + "]" );
            oArrArrTableData = new Object[iNumRows][iNumCols];
            if ( iNumRows > 0 )
            {
                Enumeration enumRows = vObjArrRows.elements();
                int iEnumRow = -1;
                while (enumRows.hasMoreElements() )
                {
                    iEnumRow++;
                    Object[] oArrRow = (Object[]) enumRows.nextElement();
                    oArrArrTableData [iEnumRow] = oArrRow ;
                }
            }
        }

		finally
		{
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
		}

        return oArrArrTableData ;

    }

    Object getValue(int iRow, int iCol)
    {
        return oArrArrTableData [iRow][iCol];
    }


    // *******************************************************
	public void tablePrint ( )
	// *******************************************************
	{
        System.out.println("TABLE CONTENTS" );
		for ( int i = 0; i < iNumRows; i++ )
		{
            System.out.print("Row " + i + ". " );
			for ( int j = 0; j < iNumCols; j++ )
			{
				System.out.print("\t" + oArrArrTableData[i][j]);
			}
            System.out.println();
		}
        System.out.println("END TABLE CONTENTS" );
	}

    // *******************************************************
	private void tableToFile ( String sFilename, String[][] sArrArrTable )
	// *******************************************************
	{
		for ( int i = 0; i < iNumRows; i++ )
		{
			int iLocalNumCols = sArrArrTable[i].length;
			if ( iLocalNumCols != iNumCols)
				Log.LogError ("bad col count");
			StringBuffer sb = new StringBuffer();
				sb.append ( sArrArrTable [ i ] [ 0 ]  );
			for ( int j = 0; j < iNumCols; j++ )
			{
				sb.append ( "," + sArrArrTable [ i ] [ j ]);
			}
			if ( i == 0 )
				UtilFile.addLineToFileKill (sFilename, sb.toString() + "\r\n");
			else
				UtilFile.addLineToFile (sFilename, sb.toString()  + "\r\n");
		}
	}


}
