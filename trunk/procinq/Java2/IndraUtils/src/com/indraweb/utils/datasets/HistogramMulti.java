/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Feb 4, 2003
 * Time: 6:43:19 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.utils.datasets;

import com.indraweb.utils.strings.UtilStrings;

import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;

import api.tsmetasearchnode.Data_MSAndScoreResult_acrossSEs;

// a set of histograms keyed by an object (e.g., Integer or String)
// used initially as - per score value - give counts per score

public class HistogramMulti {

    Hashtable htObjectToHistogram = new Hashtable();

    public Enumeration keys()
    {
        return htObjectToHistogram.keys();
    }

    public synchronized void incrValue ( Object oKeyDataType, String sKeyValue, int iIncrAmt )
    {
        Histogram hist = ( Histogram )   htObjectToHistogram.get ( oKeyDataType );  // eg get Histogram concerning score 1
        if ( hist == null )
        {
            hist = new Histogram ();
            htObjectToHistogram.put ( oKeyDataType, hist );
        }
        hist.incrCounterForKeyValue ( sKeyValue, iIncrAmt  );
    }

    public synchronized Histogram getHistogram ( Object oKey )
    {
        return (Histogram) htObjectToHistogram.get ( oKey) ;
    }

    public void XMLout( java.io.PrintWriter pw,
                           int iIndentDepth
                           )
    {
        String sIndentDepth = UtilStrings.createStringFromCharAndNum ( ' ', iIndentDepth );
        pw.println( sIndentDepth + "<HISTMULTI>" );

        Enumeration e =   htObjectToHistogram.keys();
        while ( e.hasMoreElements () ) {
            Object oKeyDataType = e.nextElement();

            Histogram histVal = ( Histogram ) htObjectToHistogram.get ( oKeyDataType );
            histVal.XMLout ( pw, iIndentDepth + 1 );
        }
        pw.println( sIndentDepth + "</HISTMULTI>" );
    }
}
