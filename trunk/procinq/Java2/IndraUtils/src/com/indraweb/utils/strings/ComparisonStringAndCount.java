/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Dec 4, 2002
 * Time: 3:58:19 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.utils.strings;

public class ComparisonStringAndCount implements com.ms.util.Comparison

//public static class comparisonClassStringAndCount
{
    public final static int ASC  =  1;
    public final static int DESC = -1;
    int order = -2;

    public ComparisonStringAndCount ( int order_ )
    {
        order = order_;
    }

    public int compare  (Object o1, Object o2) {

        if ( ((StringAndCount) o1).count <  ((StringAndCount) o2).count)
            return -1 * order ;
        else if ( ((StringAndCount) o1).count >  ((StringAndCount) o2).count)
            return 1 * order ;
        else
            return order * ((StringAndCount) o1).word.compareTo ( ((StringAndCount) o1).word );  // hbk 2001 4 27

    }
    public int compare  (StringAndCount o1, StringAndCount o2) {
        if (o1.count < o2.count)
            return -1 * order ;
        else if (o1.count > o2.count)
            return 1 * order ;
        else
            return order * ((StringAndCount) o1).word.compareTo ( ((StringAndCount) o1).word );  // hbk 2001 4 27

    }
}  // public  class comparisonClassStringAndCount
