

package com.indraweb.utils.datasets;

import api.Log;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import java.sql.*;

import com.indraweb.util.UtilFile;


// SEE ALSO METHODS IN "UtilDataTable.java"     , also "jdbcindra_connection.java"
public class DataTables
{
    public static Hashtable htCacheStrQuerynameToDataTable = new Hashtable();
    // so a caller can ask for a table w/o knowing the SQL
    // first caller for a given table must know the SQL of course
    public static Hashtable htCacheStrQuerynameToStrSQL = new Hashtable();


    public static void emptyCache()
    {
        System.out.println ("datatable emptyCache # removed [" + htCacheStrQuerynameToDataTable.size() + "]" );
        htCacheStrQuerynameToDataTable.clear();
        htCacheStrQuerynameToStrSQL.clear();
    }
    public static void emptyCacheOfQueryAndData ( String sQueryName )
    {
        htCacheStrQuerynameToDataTable.remove(sQueryName);
        htCacheStrQuerynameToStrSQL.remove(sQueryName);
    }

    public static synchronized DataTable getDataTable(String sQueryName, String sSQL,
                                                  int iMaxRecs, Connection dbc) throws Exception
    {
        DataTable dtReturn = (DataTable) htCacheStrQuerynameToDataTable.get ( sQueryName );
        if ( dtReturn == null )
        {

            if ( sSQL == null )
            {
                sSQL = (String) htCacheStrQuerynameToStrSQL.get ( sQueryName );
                if ( sSQL == null)
                {
                    throw new Exception ("SQL required passed in for first call to a query name : sQueryName " +
                            "[" + sQueryName + "]");
                }
            }
            dtReturn = new DataTable ( sQueryName, sSQL, iMaxRecs, dbc );
            System.out.println ("new DataTable cached: sQueryName [" + sQueryName + "] sSQL [" + sSQL + "]" );
            htCacheStrQuerynameToDataTable.put ( sQueryName, dtReturn );
        } else
            System.out.println ("cached table found sQueryName [" + sQueryName + "]" );

        return dtReturn;

    }

    public static synchronized DataTable getDataTable(String sQueryName, int iMaxRecs, Connection dbc) throws Exception
    {
        return getDataTable(sQueryName, null, iMaxRecs, dbc);
    }

    public static boolean needsCaching ( String sQueryName, long lTimeRelative, Connection dbc )
    {
        DataTable dt = (DataTable) htCacheStrQuerynameToDataTable.get ( sQueryName );
        if ( dt == null )
           return true;
        else return ( lTimeRelative < dt.lCachedTime ) ;
    }

}
