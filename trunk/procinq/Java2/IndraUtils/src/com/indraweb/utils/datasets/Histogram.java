/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 4, 2003
 * Time: 11:50:37 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.utils.datasets;

import java.util.Hashtable;
import java.util.Enumeration;

import com.indraweb.utils.strings.UtilStrings;

public class Histogram {

    Hashtable htDataObjToInteger = new Hashtable();
    public synchronized void incrCounterForKeyValue ( Object oKeyValue, int iIncrAmt )
    {
        Integer ICountPre = (Integer) htDataObjToInteger.get ( oKeyValue );
        if ( ICountPre == null )
            htDataObjToInteger.put ( oKeyValue, new Integer ( iIncrAmt  ));
        else
        {
            int iCountNew = ICountPre.intValue() +  iIncrAmt ;
            htDataObjToInteger.put ( oKeyValue, new Integer ( iCountNew ) );
        }
    }

    public synchronized Hashtable getData_htObjToIntegerCount ( )
    {
        return htDataObjToInteger;
    }

    public synchronized void XMLout( java.io.PrintWriter pw,
                           int iIndentDepth )
    {
        String sIndentDepth = UtilStrings.createStringFromCharAndNum ( ' ', iIndentDepth );
        pw.println( sIndentDepth + "<HISTOGRAM>" );
        com.indraweb.utils.sets.UtilSets.XMLout( htDataObjToInteger, pw, iIndentDepth + 1 );
        pw.println( sIndentDepth + "</HISTOGRAM>" );
    }

}
