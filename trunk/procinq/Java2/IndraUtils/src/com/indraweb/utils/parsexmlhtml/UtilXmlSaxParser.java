package com.indraweb.utils.parsexmlhtml;

import java.io.InputStream;
import java.net.URL;

public class UtilXmlSaxParser
{

    /** read, parse file, and throw events to the sink passed in */
    public static void parseXML (String sFileName , org.xml.sax.helpers.DefaultHandler handler)
            throws Exception
    {
        org.xml.sax.XMLReader reader = makeXMLReader ();
        reader.setContentHandler (handler);
        org.xml.sax.InputSource insrc = new org.xml.sax.InputSource (sFileName);
        reader.parse (insrc);
    }

    /** read, parse URL, throw events to the sink passed in */
    public static void parseXML (URL url , org.xml.sax.helpers.DefaultHandler handler)
            throws Exception
    {
        //InputStream is = InputStreamsGen.timedSocketConnect (url , sURL, 500000);
        InputStream is = url.openStream ();
        org.xml.sax.XMLReader reader = makeXMLReader ();
        reader.setContentHandler (handler);
        reader.parse (new org.xml.sax.InputSource (is));
    }


    /** create a new XML reader */
    public static org.xml.sax.XMLReader makeXMLReader ()
            throws Exception
    {
        javax.xml.parsers.SAXParserFactory saxParserFactory =
                javax.xml.parsers.SAXParserFactory.newInstance ();
        javax.xml.parsers.SAXParser saxParser = saxParserFactory.newSAXParser ();
        org.xml.sax.XMLReader xmlReader = saxParser.getXMLReader ();
        return xmlReader;
    }

// example sink below :
    /*
// an example sink for content events. It simply prints what it sees.
final class ExampleDefaultHandler
        extends org.xml.sax.helpers.DefaultHandler
        implements org.xml.sax.ContentHandler
{
    final private static void print
            (final String context , final String text)
    {
        System.out.println (context + ": \"" + text + "\".");
    }

    final public void startElement
            (final String namespace , final String localname ,
             final String type , final org.xml.sax.Attributes attributes)
            throws org.xml.sax.SAXException
    {
        print ("startElement" , type);
    }

    final public void endElement
            (final String namespace , final String localname ,
             final String type)
            throws org.xml.sax.SAXException
    {
        print ("endElement  " , type);
    }

    final public void characters
            (final char[] ch , final int start , final int len)
    {
        final String text = new String (ch , start , len);
        final String text1 = text.trim ();
        if (text1.length () > 0) print ("characters  " , text1);
    }
}

    */

}