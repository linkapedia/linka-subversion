/*
 * Created by IntelliJ IDEA.
 * User: Henry Kon
 * Date: Dec 4, 2002
 * Time: 2:05:43 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.utils.strings;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

import com.ms.util.ArraySort;

public class StringAndCount
{

	public String word;
 	public int  count;

	public StringAndCount (String w, int c)
	{
		word = w;
		count = c;
	}

	// ***************************************************
	public static StringAndCount[] convertHTWordCounts_ToSortedStrandCount ( Hashtable htWordCounts,
																boolean bRemoveNumbers
																)
	// ***************************************************
	{
		Vector vSandC = new Vector();
		Enumeration e = htWordCounts.keys();
		while ( e.hasMoreElements () )
		{
			String s = (String) e.nextElement();
			if ( !bRemoveNumbers || !UtilStrings.isNumericInteger ( s ) )
			{
				Integer ICount = (Integer) htWordCounts.get ( s );
				int iCount = ICount.intValue();
				StringAndCount sandc = new StringAndCount (s, iCount);
				vSandC.addElement ( sandc );
			}
		}

		StringAndCount[] sandcArr = new StringAndCount[vSandC.size()] ;
		vSandC.copyInto (sandcArr);
		ArraySort.sort ( sandcArr, new ComparisonStringAndCount ( ComparisonStringAndCount.DESC ) );
		return sandcArr;

	}

// ***************************************************
	public static String convertStringAndCountObjectArray ( Object[] oArr, String sDelim )
	// ***************************************************
	{
		StringBuffer sb = new StringBuffer();
		if ( oArr != null )
		{
			for ( int i = 0; i < oArr.length; i++ )
			{
				if ( i > 0 )
					sb.append ( sDelim );
				StringAndCount sc = (StringAndCount) oArr[i];
				sb.append ( sc.word+"#"+sc.count );
			}
		}
		return sb.toString();
	}

}
