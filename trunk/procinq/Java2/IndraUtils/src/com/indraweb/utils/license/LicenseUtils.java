/*
 These utilities facilitate ITS license creation, encryption and decryption.

 The following is some example code:

 // permissions 0000000000000001011011101
 String bitmap = "0000000000000001011011101";
 System.out.println("bitmap: "+bitmap);

 String hexmap = bitmapToHexmap(bitmap);
 System.out.println("hexmap: "+hexmap);

 String license = hexmapToLicense(hexmap);
 System.out.println("license: "+license);

 System.out.println("--------------------------");

 hexmap = licenseToHexmap(license);
 System.out.println("hexmap: "+hexmap);

 if (isHexmapStable(hexmap)) System.out.println("Hexmap is stable.");
 else System.out.println("Hexmap is UNSTABLE");

 bitmap = hexmapToBitmap(hexmap);
 System.out.println("bitmap: "+bitmap);

 initialize session inits relative to ... see indralicense in guishared
 <context-param>
 <param-name>licenseKey</param-name>
 <param-value>mXlRzUE0RJDbcj+97NguzA==</param-value>
 </context-param>

 */
package com.indraweb.utils.license;

import com.iw.license.IndraLicense;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.math.BigInteger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEParameterSpec;
import org.apache.commons.codec.binary.Base64;

public class LicenseUtils {
    // 8-bytes Salt
    // seed for the generator for encryption - how many bytes encryption ?
    // a few bytes added to front and back

    private static byte[] salt = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
        (byte) 0x56, (byte) 0x34, (byte) 0xE3, (byte) 0x03
    };
    // encryption key
    private static final char[] pass = "AB8391D01EF0192A".toCharArray();

    // decrypt license key into HEXMAP
    public static String licenseToHexmap(String licenseKey) throws Exception {
        PBEParameterSpec ps = new javax.crypto.spec.PBEParameterSpec(salt, 20);
        SecretKeyFactory kf = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey k = kf.generateSecret(new javax.crypto.spec.PBEKeySpec(pass));

        Cipher decryptCipher = Cipher.getInstance("PBEWithMD5AndDES/CBC/PKCS5Padding");
        decryptCipher.init(Cipher.DECRYPT_MODE, k, ps);

        String hexmap = decrypt(licenseKey, decryptCipher);

        //System.out.println("license key: "+licenseKey);
        //System.out.println("hexmap: "+hexmap);

        return hexmap;
    }

    // verify HEXMAP contains acceptable sequence
    public static boolean isHexmapStable(String hexMap) {
        if ((hexMap.startsWith("DEED")) && (hexMap.endsWith("ACE"))) {
            return true;
        }
        return false;
    }

    // convert HEXMAP to bitmap
    public static String hexmapToBitmap(String hexMap) {
        BigInteger bi = new BigInteger(hexMap.substring(4, hexMap.length() - 3), 16);
        return bi.toString(2);
    }

    // create new License object from bitmap
    public static IndraLicense createLicense(String bitMap) {
        return new IndraLicense(bitMap);
    }

    // convert BITMAP to HEXMAP (sequence added)
    public static String bitmapToHexmap(String bitMap) {
        BigInteger bi = new BigInteger(bitMap, 2);
        return ("DEED" + bi.toString(16) + "ACE").toUpperCase();
    }

    // encrypt HEXMAP into license key
    public static String hexmapToLicense(String hexMap) throws Exception {
        PBEParameterSpec ps = new javax.crypto.spec.PBEParameterSpec(salt, 20);
        SecretKeyFactory kf = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey k = kf.generateSecret(new javax.crypto.spec.PBEKeySpec(pass));

        Cipher ecipher = Cipher.getInstance("PBEWithMD5AndDES/CBC/PKCS5Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, k, ps);

        return encrypt(hexMap, ecipher);
    }

    public static String licenseToBitmap(String licenseKey) throws Exception {
        String hexmap = licenseToHexmap(licenseKey);
        //System.out.println("hexmap: "+hexmap);
        if (!isHexmapStable(hexmap)) {
            throw new Exception("License key is not valid!");
        }

        return hexmapToBitmap(hexmap);
    }

    public static String encrypt(String str, Cipher ecipher) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return new Base64().encodeAsString(enc);

        } catch (BadPaddingException e) {
            System.out.println("error BadPaddingException" + e.getMessage());
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        }
        return null;
    }

    public static String decrypt(String str, Cipher dcipher) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new Base64().decode(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");

        } catch (BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        }
        return null;
    }
}
