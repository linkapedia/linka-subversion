/*
 This class is used to retrieve and update data in an Oracle CLOB field.
 */
package com.indraweb.utils.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

/**
 *
 * @author
 */
public class UtilClob {

    private static final Logger log = Logger.getLogger(UtilClob.class);

    // get a clob from the node data table
    public static String getClobStringFromNodeData(long lNodeID, Connection dbc) throws Exception {
        log.debug("select nodesource from nodedata where nodeid = " + lNodeID);
        return getClobString("select nodesource from nodedata where nodeid = " + lNodeID, dbc);
    }

    // get a clob, SQL is passed in
    public static String getClobString(String sSQL, Connection dbc) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        String sTextReturn = "";

        try {
            stmt = dbc.createStatement();
            rs = stmt.executeQuery(sSQL);

            //andres--------------------------
            while (rs.next()) {
                sTextReturn = rs.getString("nodesource");
                log.debug("source: " + sTextReturn);
            }

            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
        return sTextReturn;
    }

    /**
     * Insert into node source table, but remove what is there first yes this code is ugly but this is apparently the only way to effect the change and get beyond the 4k limit.
     *
     * @see <a href="http://otn.oracle.com/sample_code/tech/java/sqlj_jdbc/files/advanced/LOBSample/LOBSample.java.html">LOBSample.java.html</a>
     * @param lNodeID
     * @param sText
     * @param dbc
     * @throws Exception
     */
    public static void insertClobToNodeData(long lNodeID, String sText, Connection dbc) throws Exception {
        Statement stmt = null;
        PreparedStatement pstmt = null;

        try {
            String sSQL = "DELETE FROM NODEDATA WHERE NODEID = " + lNodeID;
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
            stmt.close();
            stmt = null;

            dbc.setAutoCommit(false);
            /*
             * CLOB nodeSourceClob = CLOB.createTemporary(conn, true, CLOB.DURATION_SESSION); Writer clobWriter = nodeSourceClob.setCharacterStream(1); clobWriter.write(sText); clobWriter.flush();
             * clobWriter.close(); log.debug("Length of Clob: " + nodeSourceClob.length() + " for node{" + lNodeID + "}"); log.debug("writting file for debug to disk: " + lNodeID + ".txt");
             * FileUtils.writeFile(lNodeID + ".txt", sText);
             */

            log.debug("source how string: " + sText);
            pstmt = dbc.prepareStatement("INSERT INTO NODEDATA (NODEID, NODESOURCE) VALUES (?,?)");
            pstmt.setLong(1, lNodeID);
            pstmt.setString(2, sText);
            pstmt.executeUpdate();
            pstmt.close();
            pstmt = null;
        } catch (Exception e) {
            log.error("An exception ocurred.", e);
            throw e;
        } finally {
            dbc.commit();
            dbc.setAutoCommit(true);

            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        }
    }
}
