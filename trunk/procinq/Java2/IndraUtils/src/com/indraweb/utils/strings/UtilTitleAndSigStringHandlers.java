/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Dec 5, 2002
 * Time: 4:44:02 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.utils.strings;

import java.util.Hashtable;

public class UtilTitleAndSigStringHandlers {
	public static Hashtable htconvertTitleToSigStyleSArr_stemOn = new Hashtable();
	// ******************************************
	public static String[] convertTitleToSigStyleSArr ( String sNodeTitle, boolean bStemOn )
	// ******************************************
	{
		String[] sArrRet = (String[]) htconvertTitleToSigStyleSArr_stemOn.get (sNodeTitle);
		if ( bStemOn && sArrRet != null )
		{
			return sArrRet;
		}
		else
		{
			sNodeTitle = sNodeTitle.toLowerCase() + " ";
			sArrRet = UtilStrings.turnStringIntoArrayOfStringsDelimitedBy_StopListOption (
				sNodeTitle, "?!/, &+.;:#()\u00A0?\"\n\r[]\u001A>", true, true, true, bStemOn );  // delimiter set classify file build title parse

			if ( bStemOn ) // looks like we're caching only the stemmed version
				htconvertTitleToSigStyleSArr_stemOn.put ( sNodeTitle, sArrRet );

			return  sArrRet;
		}

	}

	// ******************************************
	public static String convertTitleToTitleForTitleMatch ( String sNodeTitle, boolean bStemOn )
	// ******************************************
	{
		String[] sArrNodeTitle = convertTitleToSigStyleSArr ( sNodeTitle, bStemOn );
		StringBuffer sb = new StringBuffer();
		for ( int i = 0; i < sArrNodeTitle.length; i++ )
		{
			sb.append(sArrNodeTitle[i] + " ");
		}
		return  sb.toString();
	}

}
