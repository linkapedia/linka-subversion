/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 18, 2002
 * Time: 5:14:26 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.analysis.trec;

import java.util.*;

public class TQRelContent {

    private static Hashtable htQIDs_To_TrecQs = new Hashtable();

    int iQIdDTrec = -1;
    Vector vTrecHits = new Vector();
    HashSet hsTrecHits = new HashSet();
    Vector vTrecNonHits = new Vector();
    HashSet hsTrecNonHits = new HashSet();

    public TQRelContent  ( int iQIdDTrec )
    {
        this.iQIdDTrec = iQIdDTrec;
    }

    public static void setTrecHitOrNot ( Integer ITrecTopicID, String sUrlPiece, int iNo0Yes1 )
        throws Exception
    {   // get the trec topic if it exists yet
        TQRelContent trectopic = (TQRelContent) htQIDs_To_TrecQs.get ( ITrecTopicID );

        if ( trectopic == null )
            trectopic = new TQRelContent (ITrecTopicID.intValue());

        if (trectopic.hsTrecHits.contains( sUrlPiece ) )             throw new Exception ( "repeat [" + sUrlPiece  + "]");
        if (trectopic.hsTrecNonHits.contains( sUrlPiece ) )             throw new Exception ( "repeat non hit [" + sUrlPiece  + "]");

        if ( iNo0Yes1 == 0 )             {
            trectopic.vTrecNonHits.addElement(sUrlPiece);
            trectopic.hsTrecNonHits.add( sUrlPiece );
        } else {
            trectopic.vTrecHits.addElement(sUrlPiece);
            trectopic.hsTrecHits.add( sUrlPiece );
        }
    }


}
