/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Oct 19, 2002
 * Time: 11:30:25 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.indraweb.analysis.trec;

import com.indraweb.util.UtilFile;
import com.indraweb.util.UtilStrings;
import com.indraweb.database.JDBCIndra_Connection;

import java.util.Vector;
import java.util.HashSet;
import java.sql.Connection;

public class LoadQrelToDB {

// Create table trec_qrel (        qestionid Number(4,0) NOT NULL UNIQUE,  urlpiece Varchar2(50)  NOT NULL,  status Number(1,0)  NOT NULL) ;
//    SQL> desc trec_qrel;
// Name                                      Null?    Type
// ----------------------------------------- -------- ----------------------------
// QESTIONID                                 NOT NULL NUMBER(4)
// URLPIECE                                  NOT NULL VARCHAR2(50)
// STATUS                                    NOT NULL NUMBER(1)public static int loadqreltodb ( String s )
    public static int loadqreltodb ( String sFileName, Connection dbc)
        throws Exception
    {
        Vector vFileStrings = UtilFile.getVecOfFileStrings(sFileName);
        int iLastSize = 0;
        int icountlast = -1;
        System.out.println("vsize " + vFileStrings.size() );
        // LOAD FILE LINES

// restrict which qid's are of interest
HashSet hsIDsToUse = new HashSet();
hsIDsToUse.add ( new Integer(449 ));
hsIDsToUse.add ( new Integer(420 ));
hsIDsToUse.add ( new Integer(434 ));
hsIDsToUse.add ( new Integer(450 ));
hsIDsToUse.add ( new Integer(429 ));
hsIDsToUse.add ( new Integer(441 ));
hsIDsToUse.add ( new Integer(403 ));
hsIDsToUse.add ( new Integer(406 ));
hsIDsToUse.add ( new Integer(435 ));
hsIDsToUse.add ( new Integer(411 ));
hsIDsToUse.add ( new Integer(413 ));
hsIDsToUse.add ( new Integer(447 ));
hsIDsToUse.add ( new Integer(444 ));
hsIDsToUse.add ( new Integer(416 ));
        // walk thru vec of file strings
        int i = -1;
        int iNumInserts = 0;
        for (i = 0; i < vFileStrings.size(); i++ )
        {
            if ( i % 100 == 0 )
                   com.indraweb.util.Log.log(" i [" + i + "] \r\n");
            String sLine =(String)  vFileStrings.elementAt(i);
            // get data from each file
            Vector vLineSplitUp = UtilStrings.splitByStrLen1(sLine, " ");
            Integer IDataQID = new Integer ( (String) vLineSplitUp.elementAt(0));
            String sDataTopicURLSubString = (String) vLineSplitUp.elementAt(2);
            Integer IDataHit01 = new Integer ( (String) vLineSplitUp.elementAt(3));
            // TEST THAT NOTHING BUT
            int iCount = vLineSplitUp.size();
            if ( iCount != icountlast ) {
                // create vec of file strings
                if ( icountlast != -1 )
                    throw new Exception("iCount != icountlast i [" + i + "] iCount  [" + iCount  + "] icountlast  [" + icountlast  + "] " );
                icountlast = iCount;
            }

            dbc.setAutoCommit(false);
            if ( hsIDsToUse.contains(IDataQID))
            {
                String sSqlURLInsert = "insert into trecqrel ( QESTIONID, urlpiece, status) values ( " +

                        "'" + IDataQID  + "'," +
                        "'" + sDataTopicURLSubString + "'," +
                        "'" + IDataHit01+ "')"
                ;
                com.indraweb.util.Log.logClearcr("sSqlURLInsert :"+sSqlURLInsert );
                JDBCIndra_Connection.executeUpdateToDB ( sSqlURLInsert, dbc, false, 1 );
                if ( (i+1) % 50 == 0 ) {
                    dbc.commit();
                }
                iNumInserts++;
            }
        } // for
        dbc.commit();
        return iNumInserts;
    }  // loadqreltodb

}
