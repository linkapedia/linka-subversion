package com.indraweb.analysis.trec;

import com.indraweb.database.JDBCIndra_Connection;

import java.util.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class testclass {

    static HashSet hSSURLPieces = new HashSet ();

    // goal : create a hashset of all docs associated with the Question ID's of interest
    // then check their existence in the docurl field of the doc table.
    // those missing were presumably not classified.

    public static boolean verifyURLPiecesAre1To1WithFullURL  (Connection dbc )
    throws Exception
    {
        // get the urlpieces like "FBIS4-25236" from the DB table trecqrel
        // trecqrel has all data from the file with question ID among the set of questions in
        String sSQL = "select distinct urlpiece from trecqrel where documentid <= 0 or documentid is null";
        Vector v = com.indraweb.database.JDBCIndra_Connection.ExecuteQueryAgainstDBVector(sSQL, dbc);
        for ( int i = 0; i < v.size(); i++ )
        {
            String sURLPiece = (String) v.elementAt(i);
            hSSURLPieces.add(sURLPiece);
        }

        // now sweep thru all docs in there and see how many of these get covered and then how many are left
        // first time around - original file set :
        // String sSQL2 = "select documentid, docurl from document where docurl like '\\\\66.134.131.60\\60gb db drive\\trec\\tars\\%'";
        String sSQL2 = "select documentid, docurl from document where docurl like '\\\\66.134.131.60\\60gb db drive\\extra\\%'";
        // second time around - extra file set MP created for 811 docs :
        Statement stmt = dbc.createStatement();
        ResultSet rs = stmt.executeQuery (sSQL2);
        int iCounter = 0;
        Hashtable htFoundDocURLPiecesDB= new Hashtable();
        while ( rs.next() ) {
            // get FT944-8320 from [\\66.134.131.60\60gb db drive\trec\tars\Ft\FT944\FT944_25\FT944-8320.HTM]
            int iDocid = rs.getInt(1);
            String sURLDB = rs.getString(2);
            int iStartSubIndex = sURLDB.lastIndexOf("\\");
            int iEndSubIndex = sURLDB.lastIndexOf(".");
            if ( iStartSubIndex > 0 && iEndSubIndex > iStartSubIndex ) // if parsable
            {
                // get just the substring
                String sURLPieceDB = sURLDB.substring(iStartSubIndex+1, iEndSubIndex);
                // mark a doc as found by removing it from the hashset from qrel
                int iSizePre = hSSURLPieces.size();
                hSSURLPieces.remove(sURLPieceDB);
                // if seen this doc before on another
                Integer IDocIDPriorForthisDBURLPiece = (Integer) htFoundDocURLPiecesDB.get ( sURLPieceDB );
                Integer IDocID_This = new Integer (iDocid) ;
                if ( IDocIDPriorForthisDBURLPiece != null )
                {
                    if ( IDocIDPriorForthisDBURLPiece != IDocID_This )
                    {
                        com.indraweb.util.Log.logClearcr("ERROR - COLLISION " +                                  " sURLPieceDB [" + iCounter + "] "+                                 " iDocid_url_this  [" + IDocID_This + "] " +                                 " iDocid_prior [" + IDocIDPriorForthisDBURLPiece + "] " +                                  " Prior [" + sURLDB+ "]");
                    }
                }
                int iSizePost = hSSURLPieces.size();
                //com.indraweb.util.Log.logClearcr("precompare::");
                if ( iSizePost  < iSizePre )                     {
                    htFoundDocURLPiecesDB.put (sURLPieceDB, IDocID_This);
                    // now update trecqrel table with doc id from the db
                    String sSQLUpdate = "update trecqrel set documentid = " + IDocID_This + " where URLPIECE = '" + sURLPieceDB + "'";
                    //com.indraweb.util.Log.logClearcr("sSQLUpdate [" + sSQLUpdate  + "]");

                    int iNumUpdated = JDBCIndra_Connection.executeUpdateToDB ( sSQLUpdate, dbc, true, 1 );
                }
            } // if parsable
            else {
                com.indraweb.util.Log.logClearcr("ERROR - URL NOT PARSABLE" +
                    " sURLDB [" + sURLDB + "]");
            }

            if (iCounter % 500 == 0 )
                com.indraweb.util.Log.logClearcr("cnt [" + iCounter + "] "+
                    " hSSURLPieces.size() [" + hSSURLPieces.size() + "] " +
                    " url [" + sURLDB+ "]");
            iCounter++;
        }
        rs.close();
        stmt.close();
        com.indraweb.util.Log.logcr("remaining [" + hSSURLPieces.size() + "]");

        Iterator I = hSSURLPieces.iterator();
        while ( I.hasNext() )
        {
            String sURLLeft = (String) I.next();
            com.indraweb.util.Log.logcr("remaining [" + sURLLeft  + "]");

        }


        return true;
    }


    public static void outputDQQueryResilt ( Connection dbc )
    throws Exception
    {
        int[] iArrQIDs = {403,406,411,413,416,420,429,434,435,441,444,447,449,450};
        String sFileName = "c:/temp/indratrec.txt";
        com.indraweb.util.UtilFile.deleteFile(sFileName   );


        int iNumPrintedTotal = 0;
        for ( int i = 0; i < iArrQIDs.length; i++ )
        {

            //select distinct d.documentid from t   recqnode tqn, nodedocument nd, document d, node n    where  tqn.version = 2    and tqn.nodeid = n.nodeid    and n.corpusid = 100040    and n.nodeid = nd.nodeid    and nd.score1 > 0    and d.documentid = nd.documentid    and questionid = 447   and (score5*score6)  > .000

            // 401	Q0	FT923-1528	0	3.2633	pir9At0
            String sSQL2 = "select d.docurl, score5*score6 " +
            " from trecqnode tqn, nodedocument nd, document d, node n    " +
            " where  tqn.version = 2    " +
            " and tqn.nodeid = n.nodeid    " +
            " and n.corpusid = 100040    " +
            " and n.nodeid = nd.nodeid    " +
            " and nd.score1 > 0    " +
            " and d.documentid = nd.documentid " +
            " and questionid = " + iArrQIDs[i] +
            " order by score5*score6  desc";


            // second time around - extra file set MP created for 811 docs :
            Statement stmt = dbc.createStatement();
            ResultSet rs = stmt.executeQuery (sSQL2);
            int iCounter = 0;
            Hashtable htFoundDocURLPiecesDB = new Hashtable();
            int iNumPrintedThisNode  = 0;
            while ( rs.next()  && iNumPrintedThisNode < 50 ) {
                // get FT944-8320 from [\\66.134.131.60\60gb db drive\trec\tars\Ft\FT944\FT944_25\FT944-8320.HTM]
                String sURLDB = rs.getString(1);
                double dScoreAgg = rs.getDouble(2);

                int iStartSubIndex = sURLDB.lastIndexOf("\\");
                int iEndSubIndex = sURLDB.lastIndexOf(".");
                if ( iStartSubIndex > 0 && iEndSubIndex > iStartSubIndex ) // if parsable
                {
                    String sURLPieceDB = sURLDB.substring(iStartSubIndex+1, iEndSubIndex);
                    String sPReexist = (String) htFoundDocURLPiecesDB.get ( sURLDB );
                    if ( sPReexist == null )
                    {
                        htFoundDocURLPiecesDB.put ( sURLPieceDB , "dummy");
                        com.indraweb.util.UtilFile.addLineToFile(sFileName,
                                iArrQIDs[i] + "\t" +sURLPieceDB + "\t" +iNumPrintedTotal+ "\t" +
                                com.indraweb.util.UtilStrings.numFormatDouble(dScoreAgg, 2) + "\r\n");
                        iNumPrintedThisNode++;
                        iNumPrintedTotal++;
                    }
                } // if parsable
                else {
                    com.indraweb.util.Log.logClearcr("ERROR - URL NOT PARSABLE" +
                        " sURLDB [" + sURLDB + "]");
                }
            }
            rs.close();
            stmt.close();
        }
    }
}
