/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Feb 2, 2003
 * Time: 5:15:11 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.externalsearch;

import api.Log;
import api.tsclassify.TSClassifyDoc;
import api.tsmetasearchnode.helper.UtilStrings;
import com.indraweb.execution.Session;
import com.indraweb.html.HTMLElement;
import com.indraweb.html.HTMLMemDoc;
import com.indraweb.html.UrlHtmlAttributes;
import com.indraweb.html.UtilHTML;
import com.indraweb.util.UtilProfiling;
import com.indraweb.util.Utilemail2;
import com.indraweb.utils.sets.UtilSets;
import com.indraweb.webelements.IndraURL;
import com.iw.metasearch.DBAccessMetaSearch;
import com.iw.metasearch.Data_SearchengineCallout;
import com.iw.scoring.NodeForScore;

import java.sql.Connection;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

// import com.ms.*;


public class SearchIntelliseek {


    /* private static int whichSEAmI = SearchEngineSuperClass.WhichSE_IS;   // SeCfg Search Engine Config
     private static String sWhichSEAmI = SearchEngineSuperClass.getSearchEngineNameFromIntID ( whichSEAmI );
     public static int num_MaxPerSearchThisSE = Session.cfg.getPropInt ( "num"+sWhichSEAmI+"_WebDocsInterestedIn" );
 */
/*
	public static double statlog_numIShits = -1;
	public static double statlog_ISDur = -1;
*/

    public static long export_TimeSpentInParseDBaccess = 0;

    // ***********************************************************************
    public static void doSearch(
            // ***********************************************************************
            Integer ISEID, // SEID for this (google) SE
            NodeForScore nfs,
            //  int iCorpusID_,
            //  int iSigGenRangeID_,
            //   long lDocIDofTopic_,
            //   long lNodeID_,
            api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQForMS_ToPutIndraURLsInto,
            com.indraweb.util.IntContainer intContainerNumIURLsThisSE
            //java.sql.Connection dbc_
            )

/*
	// ***********************************************************************
	public Vector putDBMetaSearch_CandidateURLResultSetThisCorpTopic (
	// ***********************************************************************
											Topic curTopicCorpus_,
											int iCorpusID_,
											int iSigGenRangeID_,
											long lDocIDofTopic_,
											long lNodeID_,
											java.sql.Connection dbc_
											)
*/

            throws Exception {
        long lTimeStart = System.currentTimeMillis();
        Vector vResults = null;
        String[] sURLSearch = null;
        //String sWhichSEAmI = Data_SearchengineCallout.getSEShortName(ISEID.intValue());

        int iSEID = ISEID.intValue();
        int iNum_ResultsOnPage = Data_SearchengineCallout.getNumResultsOnPage(iSEID);
        int iNum_MaxPerSearchThisSE = Data_SearchengineCallout.getMaxPerSearchThisSE(iSEID);

        String sSEShortName = Data_SearchengineCallout.getSEShortName(iSEID);
        StringBuffer sbISIndividualtimes = new StringBuffer();
        long export_lTotalTimeInMemDocCreate = 0;
        long export_lTotalTimeInParseResults = 0;

        try {

            // Log.log ("entering intelliseek putDBMetaSearch_CandidateURLResultSetThisCorpTopic\r\n");
            vResults = new Vector();
            // int individualSearchHitSeekingCurently = 0;
            String[] searchTerms = null;

            // Get search terms from the database
/*
			searchTerms = DBAccess_Central_connection.getSigWords_sArr_withTitle_SortedDesc (false, lNodeID_, dbc_ );
*/
            searchTerms = nfs.getSarr(false, true, false);

            // Build a search string here based on our parameters
            // Search string is an array -- for each set of 40 search engines we need to use
            // send a separate request.

            String sDBName = "API";
            Connection dbc = api.statics.DBConnectionJX.getConnection(sDBName);
            try {
                int iSEid = ISEID.intValue() ;
                sURLSearch = createURLSearchString(
                        searchTerms,
                        nfs,
                        dbc,
                        iSEid);
            } finally {
                dbc = api.statics.DBConnectionJX.freeConnection(dbc, sDBName);
            }

            // Search strings are broken into groups of X (configurable) search engines.   For each group, run:

            for (int loop = 0; loop < sURLSearch.length; loop++) {
                long lTStartLoop = System.currentTimeMillis();
                HTMLMemDoc hm = null;
                UrlHtmlAttributes urlHtml = null;
                urlHtml = new UrlHtmlAttributes(sURLSearch[loop]);
                Log.Log("Executing Intelliseek retrieval: \r\n" + sURLSearch[loop] + "\r\n");

                // Invoke HTTP GET request and bring back our XML
                long lStartTimeMEmDocThisSingleISrequest = System.currentTimeMillis();
                Log.Log("entering intelliseek HTMLMemDoc\r\n");
                try {
                    hm = new HTMLMemDoc(urlHtml, "Intelliseek search sequence", 1,
                            240000); // was from configparams Session.cfg.getPropInt("socketTimeoutMSSearchEngines");
                } catch (Exception e) {
                    Log.LogFatal(
                        " putDBMetaSearchResultSet(): can't get htmlmemdoc for SE [" +
                        sSEShortName + "] search [" + urlHtml.getsURL() + "]", e);
                    // Log.printStabckTrace_mine (62, e);
                    intContainerNumIURLsThisSE.setiVal(-1);
                    return;
                }
                export_lTotalTimeInMemDocCreate += System.currentTimeMillis() - lStartTimeMEmDocThisSingleISrequest;

                sbISIndividualtimes.append("\r\nmemdoc time [" + loop + "] of [" + sURLSearch.length
                        + "] in mins:[" + UtilStrings.numFormatDouble(
                                UtilProfiling.elapsedTimeMinutes(lStartTimeMEmDocThisSingleISrequest), 1) + "]");

                Log.Log("done intelliseek HTMLMemDoc search " + "[" + loop + "] of <max [" + sURLSearch.length
                        + "] in [" + UtilProfiling.elapsedTimeMinutes(lStartTimeMEmDocThisSingleISrequest) + "]\r\n");


                // Push XML through the tokenizer
                int iNumElems = hm.getNumElements();
                // Log.log ("num IS htmlMemDoc elems : " + iNumElems + "\r\n");

                long lStartTimeParseThisSingleISrequest = System.currentTimeMillis();
                // PARSE results
                Vector vTmpResults = null;
                try {
                    vTmpResults = ParseResults(ISEID.intValue(), hm,
                            workQForMS_ToPutIndraURLsInto,
                            Data_SearchengineCallout.getSEShortName(ISEID.intValue()),
                            iNum_MaxPerSearchThisSE );
                } catch (Exception e) {
                    Log.LogFatal(" in htmlmemdoc parse ["
                            + sSEShortName + "] search [" +
                            urlHtml.getsURL() + "]", e);
                    // Log.printStabckTrace_mine (62, e);
                    intContainerNumIURLsThisSE.setiVal(-2);
                    return;
                } finally {
                    export_lTotalTimeInParseResults += System.currentTimeMillis() - lStartTimeParseThisSingleISrequest;
                }

                sbISIndividualtimes.append("\r\nparsetime time [" + loop + "] of [" + sURLSearch.length
                        + "] in mins:[" + UtilStrings.numFormatDouble(
                                UtilProfiling.elapsedTimeMinutes(lStartTimeParseThisSingleISrequest), 1) + "]");
                Log.Log("Intelliseek loop nodeid [" + nfs.getNodeID() +
                        "] loop [" + loop +
                        "] < max [" + sURLSearch.length +
                        "] got #hits [" + vTmpResults.size() + "]" +
                        "] memdoc completed in [" + UtilProfiling.elapsedTimeMinutes(lStartTimeMEmDocThisSingleISrequest) + "]" +
                        "] parse completed in [" + UtilProfiling.elapsedTimeMinutes(lStartTimeParseThisSingleISrequest) + "]" +
                        "] machine [" + Session.cfg.getProp("MachineName") + "\r\n");

                vResults = UtilSets.concatenateVectors(vResults, vTmpResults);
                if (vTmpResults.size() == 0) {
                    String sFilename = Session.cfg.getProp("IndraHome") + "/logs/" +
                            "IndraMemDoc_IS_" + nfs.getNodeID() + "_" + loop + ".txt";
                    hm.writeToFile(sFilename);
                    Utilemail2.sendMail("hkon@indraweb.com",
                            "info@indraweb.com",
                            " IS doc [" + sFilename + "] 0 hits " +
                            "nodeID [" + nfs.getNodeID() + "] ",
                            "url [" + urlHtml + "] loop counter zero based [" +
                            loop + "] < max [" + sURLSearch.length + "]",
                            Session.cfg.getProp("SMTPServer")
                    );
                }
            }
        } finally {
/*
			statlog_numIShits =  (double) vResults.size();
			statlog_ISDur = UtilProfiling.elapsedTimeMinutes ( lTimeStart );
*/

            double rate = (double) 60 * UtilProfiling.rateSincePerSecond(lTimeStart, vResults.size());
            Log.Log("Intelliseek node [" + nfs.getNodeID() +
                    "] dur [" + UtilStrings.numFormatDouble(UtilProfiling.elapsedTimeMinutes(lTimeStart), 1) +
                    "] times [" + sbISIndividualtimes.toString() +
                    "] # hits [" + vResults.size() +
                    "] # hits per min [" + rate +
/*
					"] rpe1 ["  + num_MaxPerSearchThisSE +
*/
                    "] setime [" + Session.cfg.getProp("Intelliseek_setimeParm") +
                    "] machine [" + Session.cfg.getProp("MachineName") + "]\r\n");

            if (
                    (vResults.size() == 0 ||
                    Session.cfg.getPropBool("emailNonZeroIsAlso"))
            ) {
                String surlzero = null;
                if (sURLSearch.length > 0)
                    surlzero = sURLSearch[0];
                Utilemail2.sendMail("hkon@indraweb.com",
                        "info@indraweb.com",
                        "num IS [" + vResults.size() + "] " +
                        "node [" + nfs.getNodeID() + "] " +
                        "dur.min [" + UtilStrings.numFormatDouble(UtilProfiling.elapsedTimeMinutes(lTimeStart), 1) + "] "
                        ,
                        "times [" + sbISIndividualtimes.toString() + "]" +
                        "rate/min [" + UtilStrings.numFormatDouble(rate, 1) + "]" +
/*
										"rpe1 ["  + num_MaxPerSearchThisSE + "] "  +
*/
                        "setime [" + Session.cfg.getProp("Intelliseek_setimeParm") + "] " +
                        "url[0] [" + surlzero + "]",
                        Session.cfg.getProp("SMTPServer")
                );
            }

/*
			Task_RunCrawl.export_lTotalTimeInMemDocCreate = export_lTotalTimeInMemDocCreate;
			Task_RunCrawl.export_lTotalTimeInParseResults = export_lTotalTimeInParseResults;
*/
            intContainerNumIURLsThisSE.setiVal(vResults.size());
        }

    }  // private static void putDBMetaSearchResultSet ( String[] searchTerms )

    // Parse Results -- given an HTMLMemDoc instance, extract the XML from
    //  Intelliseek by the <result> tag and throw results into the Vector Results
    // ***********************************************************************
    private static Vector ParseResults(int iSEid, HTMLMemDoc hm,
                                       api.tsmetasearchnode.WorkQManager_ForTSMetasearchNode workQForMS,
                                       String sWhichSEAmI,
                                        int iNum_MaxPerSearchThisSE ) throws Exception {
        HTMLElement[] htmlElemArr = hm.getElements();
        Vector vReturnResults = new Vector();
        int iNumNewIndraUrls = 0;

        try { // Define stop and end points for the tags we need
            int[] XML_Results = {UtilHTML.XMLTagType_RESULT};
            int[] XML_Engine = {UtilHTML.XMLTagType_ENGINE};

            int[] ResultsDelim = hm.findTypes(XML_Results, 0, -1, false);

            int iEndElem;
            int iStartElem;

            // These variables represent the parameters we need to extract inside the loop
            String Title = null;
            String Summary = null;
            String URL = null;
            Vector vEngines = new Vector();

            // Loop through each result returned by Intelliseek
            for (int j = 0;
                 j < ResultsDelim.length &&
                 (iNum_MaxPerSearchThisSE  < 0 || iNumNewIndraUrls < iNum_MaxPerSearchThisSE);
                 j++) {

                iStartElem = ResultsDelim[j];

                if ((j + 1) >= ResultsDelim.length) {
                    iEndElem = -1;
                } else {
                    iEndElem = ResultsDelim[j + 1];
                }

                int iNextPointer = iStartElem + 2;

                Title = hm.getsHTMLTextOrURL(iNextPointer);
                if (Title == null) {
                    Title = "No Title";
                    iNextPointer = iNextPointer - 1;
                }
                iNextPointer = iNextPointer + 3;
                Summary = hm.getsHTMLTextOrURL(iNextPointer);
                if (Summary == null) {
                    Summary = "No Summary is available.";
                    iNextPointer = iNextPointer - 1;
                }

                iNextPointer = iNextPointer + 3;
                URL = hm.getsHTMLTextOrURL(iNextPointer);

                int[] EngineDelim = hm.findTypes(XML_Engine, iStartElem, iEndElem, false);

                // Before doing the insert, ensure that our variables do not exceed the
                //  maximum size.   If they do, truncate them to their max length
                if (Title.length() > 256) {
                    Title = Title.substring(0, 256);
                }
                if (Summary.length() > 4000) {
                    Summary = Summary.substring(0, 4000);
                }
                if (URL.length() > 256) {
                    URL = URL.substring(0, 256);
                }

                // Because one Intelliseek result returns multiple channels, loop
                // through each of the channel identifiers and create an entry for each
                for (int k = 0;
                     k < EngineDelim.length &&
                        (iNum_MaxPerSearchThisSE  < 0 || iNumNewIndraUrls < iNum_MaxPerSearchThisSE)
                    ; k++) {
                    int iElement = EngineDelim[k];
                    String sEngineId = hm.getsHTMLTextOrURL(iElement + 1);
                    long lTStartDBAccess = System.currentTimeMillis();
                    sEngineId = DBAccessMetaSearch.getChannelfromEngineNameDb(sEngineId);
                    export_TimeSpentInParseDBaccess += System.currentTimeMillis() - lTStartDBAccess;

                    //int iEngineId = Integer.parseInt(sEngineId);

                    IndraURL indraurl = new com.indraweb.webelements.IndraURL(
                            //URL, Title, "", Summary, iEngineId, 1);
                        URL, Title, "", Summary, iSEid, 1);

                    // Log.log(k + ". IS MSrch Q insert[" +URL+"] Title: "+Title+" Channel: "+iEngineId + "\r\n");
                    //Log.log(k + ". Intelliseek MSrch Q insert to size [" + (1 + vReturnResults.size() ) + "] [" +URL+"] Channel: "+iEngineId + "\r\n");
                    vReturnResults.addElement(indraurl);
                    workQForMS.addWorkItem(indraurl, ""+iSEid);
                    iNumNewIndraUrls++;
                }
            } // for (int j = 0; j < ResultsDelim.length; j++ )
        } catch (Exception e) {
            Log.LogFatal("Intelliseek parser error: ", e);
        }
        return vReturnResults;
    }

    // ***********************************************************************
    public static String[] createURLSearchString ( String[] search_terms,
                                                 // ***********************************************************************
                                                 NodeForScore nfs,
                                                 java.sql.Connection dbc_,
                                                 int iSEID
                                                 )
            throws Exception {
        // Get associated Engines from the database
        Vector vEngines = new Vector();
        vEngines = DBAccessMetaSearch.getCategoriesfromDb(nfs.getNodeID(), 3, dbc_);
        DBAccessMetaSearch.initChannelFileFromDB(dbc_);

        // Determine the number of calls we need to make against Intelliseek
        int iIntelliseekBatchSize = 50;
        int StringArrLen = 0;
        if (vEngines.size() != 0) {
            StringArrLen = (vEngines.size() / iIntelliseekBatchSize) + 1;
        }
        String[] UrlString = new String[StringArrLen];

        for (int loop = 0; loop < StringArrLen; loop++) {
            int iStart = loop * iIntelliseekBatchSize;
            int iEnd = (loop + 1) * iIntelliseekBatchSize;
            if (iEnd > vEngines.size()) {
                iEnd = vEngines.size();
            }
            String Engines = "&ce0=";

            for (int i = iStart; i < iEnd; i = i + 1) {
                String sEng = (String) vEngines.elementAt(i);
                if (Engines == "&ce0=") {
                    Engines = Engines + sEng;
                } else {
                    Engines = Engines + "," + sEng;
                }
            }

            StringBuffer sb_TermString = new StringBuffer();
            int iNumSigTermsToUse = UtilSets.min(search_terms.length, 30 ); // was configparams Session.cfg.getPropInt("NumSigTermsUsedInSearch")
            int iNumWordsSoFar = 0;
            Hashtable htWordsAdded = new Hashtable();

            for (int i = 0; i < iNumSigTermsToUse && iNumWordsSoFar < 3; i++) {
                if (search_terms[i] != null && !UtilStrings.isNumericInteger(search_terms[i])) {
                    if (i == 0) {
                        String sTitleClean = null;
                        try {
                            sTitleClean = nfs.getTitle(false);
                        } catch (Exception e) {
                            Log.LogFatal("Error getting dirty title in Intelliseek MS\r\n", e);
                        }

                        StringTokenizer st = new StringTokenizer(sTitleClean);
                        boolean bNeedPlusSignBetween = false;
                        while (st.hasMoreElements()) {
                            if (bNeedPlusSignBetween) sb_TermString.append("+");
                            String wordToAdd = (String) st.nextElement();
                            htWordsAdded.put(wordToAdd, wordToAdd);
                            sb_TermString.append(wordToAdd);
                            iNumWordsSoFar++;
                            bNeedPlusSignBetween = true;
                        }
                    } else {
                        String wordToAdd = search_terms[i];
                        if (htWordsAdded.get(wordToAdd) == null) {
                            sb_TermString.append("+");    // WORD DELIMITER K SET THIS
                            sb_TermString.append(wordToAdd);
                            htWordsAdded.put(wordToAdd, wordToAdd);
                            iNumWordsSoFar++;
                        } else
                            continue;
                    }
                }
            }
            // http://207.103.213.105:9000/search?queryterm=test&option=all&rpe=100&display=all&totalverify=0&ce0=Salon,TheGlobe,The_New_Republic,Washington_Post&auto=all&autonum=4&setime=0&pid=indraweb-xml

//            UrlString[loop] = "http://" +
//                    Session.cfg.getProp("IntelliseekSource") +
//                    "/search?cid=101&cob=indraweb&option=any&display=all&totalverify=0&auto=all&autonum=4&setime="
//                    + Session.cfg.getProp("Intelliseek_setimeParm") +
//                    "&pid=indraweb-xml&queryterm=" + sb_TermString.toString() + Engines + "&rpe=" +
//                    Data_SearchengineCallout.getMaxPerSearchThisSE(iSEID);
            UrlString[loop] = "http://" +
                    "207.103.213.105:9000" + // was from configparams Session.cfg.getProp("IntelliseekSource") +
                    "/search?cid=101&cob=indraweb&option=any&display=all&totalverify=0&auto=all&autonum=4&setime=" +
                    0 + // was from configparams Session.cfg.getProp("Intelliseek_setimeParm")
                    "&pid=indraweb-xml&queryterm=" + sb_TermString.toString() + Engines + "&rpe=" +
                    Data_SearchengineCallout.getMaxPerSearchThisSE(iSEID);
/*
                    num_MaxPerSearchThisSE;
*/
            UrlString[loop] = UtilStrings.replaceStrInStr(UrlString[loop], " ", "%20");
        }
        Log.Log("Intelliseek retrievals planned: " + UrlString.length + " on " + vEngines.size() + " engines\r\n");

        return UrlString;

    }  // createURLSearchStringFromSearchWordsArray
}

