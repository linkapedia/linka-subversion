/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Mar 3, 2004
 * Time: 5:15:40 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.classification;

import org.apache.jcs.utils.locking.ReadWriteLock;

public class ReadWriteLockCache {

    ReadWriteLock rwl = null;
    int iCallCount = 0;
    boolean bVerbose = false;

    public ReadWriteLockCache() {
        rwl = new ReadWriteLock();
    }

    public synchronized void readLock(String sDesc) throws InterruptedException {
        iCallCount++;
        if (bVerbose) {
            api.Log.Log(iCallCount + ".+ ------ start readLock [" + sDesc + "]");
        }
        rwl.readLock();
        if (bVerbose) {
            api.Log.Log(iCallCount + ".- ------ start readLock [" + sDesc + "]");
        }
    }

    public void writeLock(String sDesc) throws InterruptedException {
        iCallCount++;
        if (bVerbose) {
            api.Log.Log(iCallCount + ".+ ------ start writeLock [" + sDesc + "]");
        }
        rwl.writeLock();
        if (bVerbose) {
            api.Log.Log(iCallCount + ".- ------ start writeLock [" + sDesc + "]");
        }
    }

    public synchronized void done(String sDesc) {
        //Thread.dumpStack();
        iCallCount++;
        if (bVerbose) {
            api.Log.Log(iCallCount + ".+ ------ LOCK DONE [" + sDesc + "]");
        }
        rwl.done();
        if (bVerbose) {
            api.Log.Log(iCallCount + ".- ------ LOCK DONE [" + sDesc + "]");
        }
    }
}