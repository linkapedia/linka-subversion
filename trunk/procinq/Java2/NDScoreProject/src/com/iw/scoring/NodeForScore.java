/*
 * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Jan 20, 2003
 * Time: 12:42:41 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.scoring;

import java.util.*;
import java.sql.*;
import java.io.PrintWriter;

import com.indraweb.encyclopedia.StringAndCount;
import com.indraweb.encyclopedia.TextObject;
import com.indraweb.util.*;
import com.indraweb.util.UtilSets;
import com.indraweb.util.stem.Porter;
import com.indraweb.execution.Session;
import com.indraweb.ir.ParseParms;
import com.iw.classification.Data_NodeIDsTo_NodeForScore;
import org.apache.log4j.Logger;

/**
 * NodeForScore contains information pertaining to node (topic) content and metadata. This is used by classifiers in generating node-document scores. <p> Data contained includes Node Title, Node Size,
 * Signatures and weights, Gist, @authors Indraweb Inc, All Rights Reserved.
 */
public class NodeForScore extends TextObject {

    private static final Logger log = Logger.getLogger(NodeForScore.class);
    private long lNodeID = -1;
    private int iNodeSize = -1; // num words
    private long lNodeIDParent = -1; // num words
    private long lParentNodeID = -1;
    private int iCorpusID = -1;
    // title
    private String sNodeTitle = null;
    private String sGist = null;
    private String sNodeTitleStemmed = null;
    private String[] sArrSigsTitleThenNon = null;
    private String[] sArrSigsTitleThenNonStemmed = null;
    private int[] iArrTitleAndNonCounts = null;
    private boolean bArrNodeSigWordsSortedDescIsStemmed = false;
    private boolean bNodeSCArrHasPhrases;
    private int iNumtitleTermsInSCarr = -1;
    private String sDelims_forPhraseAffinityDecomp = null;
    private final String sSynchStringStem = "";
    int iCallCnt_getAffinityTerms = 0;
    int iCallCnt = 0;
    public static int iNumNFSConstruct = -1;

    /**
     * 
     * @return
     * @throws Exception 
     */
    public int getCorpusID() throws Exception {
        if (iCorpusID == -1) {
            throw new Exception("##USERERRMSG##Topic not in server classification set " + lNodeID + "##/USERERRMSG##");
        } else {
            return iCorpusID;
        }
    }

    /**
     *
     * @return
     */
    public String getGist() {
        return sGist;
    }

    /**
     *
     * @param Gist
     * @return
     */
    public String setGist(String Gist) {
        sGist = Gist;
        return Gist;
    }

    /**
     *
     * @param iCorpusID
     * @param lNodeID
     * @param lNodeIDParent
     * @param iNodeSize
     * @param sArrSigTermsFinal
     * @param iArrSigCountsFinal
     * @param bStemmedAlso
     * @param iNumTitleTermsInSigs_
     * @param sNodeTitle
     * @param sDelims_forPhraseAffinityDecomp
     * @throws Exception
     */
    public NodeForScore(
            int iCorpusID,
            long lNodeID,
            long lNodeIDParent,
            int iNodeSize,
            String[] sArrSigTermsFinal,
            int[] iArrSigCountsFinal,
            boolean bStemmedAlso, // want stemmed also
            int iNumTitleTermsInSigs_,
            String sNodeTitle,
            String sDelims_forPhraseAffinityDecomp) throws Exception {


        if (sNodeTitle == null) {
            throw new Exception("node title is null in NFS constructor");
        }

        this.iNumtitleTermsInSCarr = iNumTitleTermsInSigs_;
        this.lNodeIDParent = lNodeIDParent;
        this.lNodeID = lNodeID;
        this.iNodeSize = iNodeSize;
        this.sNodeTitle = sNodeTitle;
        this.iCorpusID = iCorpusID;

        this.iArrTitleAndNonCounts = iArrSigCountsFinal;
        this.sArrSigsTitleThenNon = sArrSigTermsFinal;
        this.sDelims_forPhraseAffinityDecomp = sDelims_forPhraseAffinityDecomp;

        // cache if has any phrases

        for (int i = 0; i < sArrSigTermsFinal.length; i++) {
            if (sArrSigTermsFinal[ i].indexOf(" ") > 0) {
                bNodeSCArrHasPhrases = true;
                break;
            }
        }

        if (bStemmedAlso) {
            if (sArrSigTermsFinal == null) {
                log.debug("sArrSigTermsFinal === null ");
            } else {
                sArrSigsTitleThenNonStemmed = new String[sArrSigTermsFinal.length];
                Porter porter = Porter.getInstance();
                for (int i = 0; i < sArrSigTermsFinal.length; i++) {
                    sArrSigsTitleThenNonStemmed[ i] = porter.stem(sArrSigTermsFinal[ i]);
                }
            }
        }

        iNumNFSConstruct++;
        if (iNumNFSConstruct % 10000 == 0) {

            log.debug("NFS #:" + iNumNFSConstruct + " # nodes in data ["
                    + Data_NodeIDsTo_NodeForScore.getNumNodes()
                    + "] nodeid [" + lNodeID + "] "
                    + "] lNodeIDParent [" + lNodeIDParent + "] "
                    + "] title [" + sNodeTitle + "] ");
        }
    }

    /**
     * Stemmed size is same as non stemmed size since basis in counts does not change
     *
     * @return
     * @throws Exception
     */
    public int getNodeSize() throws Exception {
        if (iNodeSize == -1) {
            log.error("Error - nodesize = -1 for nodeid [" + lNodeID + "]");
        }
        return iNodeSize;
    }

    /**
     *
     * @return
     */
    public long getNodeID() {
        return lNodeID;
    }

    /**
     *
     * @param bStemmed
     * @return
     * @throws Exception
     */
    public String getTitle(boolean bStemmed) throws Exception {
        String sNodeTitleReturn = null;
        if (sNodeTitle == null) {
            throw new Exception("title not filled in nodeid [" + lNodeID + "]");
        }
        // before 2004 06 30 was fillTitleTitleVecWordsAndSize ( dbc );

        sNodeTitleReturn = sNodeTitle;
        if (bStemmed) {
            if (sNodeTitleStemmed == null) {
                synchronized (sSynchStringStem) {
                    if (sNodeTitleStemmed == null) {
                        sNodeTitleStemmed = UtilTitleAndSigStringHandlers.convertTitleToTitleForTitleMatch(sNodeTitle, bStemmed, ParseParms.getDelimitersAll_Default());
                    }
                }
            }
            sNodeTitleReturn = sNodeTitleStemmed;

        }

        return sNodeTitleReturn;
    }

    /**
     *
     * @param dbc
     * @param bStemOn
     * @return
     * @throws Throwable
     */
    public String[] getsArrNodeTitleWords_lowerStemOption(Connection dbc, boolean bStemOn) throws Throwable {

        if (sArrSigsTitleThenNon == null) {
            if (this.sNodeTitle != null && !this.sNodeTitle.trim().equals("")) {
                Vector vTitleWords = new Vector();
                java.util.StringTokenizer str = new java.util.StringTokenizer(this.sNodeTitle);
                while (str.hasMoreElements()) {
                    try {
                        String strWord = str.nextToken("?!/, &+.;:#()\u00A0?\"\t\n\r[]�\u001A");
                        Hashtable htStop = com.indraweb.execution.Session.stopList.getHTStopWordListStemmed();
                        if (htStop.get(strWord) == null) {
                            vTitleWords.addElement(strWord.toLowerCase());
                        }
                    } catch (Exception e) {
                        log.error("exception title processing node [" + lNodeID + "] stringTokenizer having problems with '" + this.sNodeTitle + "'");
                    }
                }
                sArrSigsTitleThenNon = new String[vTitleWords.size()];
                vTitleWords.copyInto(sArrSigsTitleThenNon);
            }
        }

        if (bStemOn) {
            if (sArrSigsTitleThenNonStemmed == null) {

                com.indraweb.util.stem.Porter porter = null;
                if (bStemOn) {
                    porter = com.indraweb.util.stem.Porter.getInstance();
                }

                sArrSigsTitleThenNonStemmed = new String[sArrSigsTitleThenNon.length];
                for (int i = 0; i < sArrSigsTitleThenNonStemmed.length; i++) {
                    sArrSigsTitleThenNonStemmed[i] = porter.stem(sArrSigsTitleThenNon[i]);
                }
            }

        }
        if (!bStemOn) {
            return sArrSigsTitleThenNon;
        } else {
            return sArrSigsTitleThenNonStemmed;
        }
    }

    public int getiNumtitleTermsInSCarr() {
        return iNumtitleTermsInSCarr;
    }

    /**
     *
     * @param bStemmed
     * @param bIncludeTitle
     * @param bIncludeThesaurus
     * @return
     * @throws Exception
     */
    public String[] getSarr(boolean bStemmed, boolean bIncludeTitle, boolean bIncludeThesaurus) throws Exception {
        if (bStemmed) {
            if (bIncludeTitle && bIncludeThesaurus) {
                return sArrSigsTitleThenNonStemmed;
            } else {
                throw new Exception("this bstem case not supported");
            }
        } else {
            if (bIncludeTitle && bIncludeThesaurus) {
                return sArrSigsTitleThenNon;
            } else {
                throw new Exception("this bstem case not supported");
            }
        }
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public int[] getiarr() throws Exception {
        if (iArrTitleAndNonCounts == null) {
            log.warn("iArrTitleAndNonCounts  shouldn't this be set from a file - sure you want to go to db for sigs ? Maybe In Debug mode? - I'll not set it node [" + lNodeID + "]");
        }
        return iArrTitleAndNonCounts;
    }

    /**
     *
     * @return
     */
    public boolean getbNodeSCArrHasPhrases() {
        return bNodeSCArrHasPhrases;
    }

    /**
     *
     * @return
     */
    public long getParentNodeID() {
        return lParentNodeID;
    }

    public void emitMeToSigFile(PrintWriter pw, boolean bStemmed) throws Exception {
        // title terms individually

        String sSigTerm = null;

        for (int i = 0; i < sArrSigsTitleThenNon.length; i++) {
            // if title
            if (!bStemmed) {
                sSigTerm = sArrSigsTitleThenNon[i];
            } else {
                sSigTerm = sArrSigsTitleThenNonStemmed[i];
            }

            if (i < iNumtitleTermsInSCarr) {
                pw.write(sSigTerm + "\t" + iArrTitleAndNonCounts[i] + "\t" + "hy" + "\r\n");
            } else // not title
            {
                pw.write(sSigTerm + "\t" + iArrTitleAndNonCounts[i] + "\t" + "hn" + "\r\n");
            }
        }

        if (sNodeTitle.indexOf("\t") >= 0) {
            throw new Exception("title has tabs [" + lNodeID + "] sNodeTitle [" + sNodeTitle + "]");
        }

        // closing line per node
        pw.write("\t" + lNodeID + "\t" + iNodeSize + "\t" + lNodeIDParent + "\t" + sNodeTitle + "\r\n");

    }

    /**
     *
     * @param bTitlesOnlyForAffinity
     * @param bStem
     * @param sPhraseToWordSplitterDelims_
     * @return
     * @throws Exception
     */
    public HashSet getAffinityTerms(boolean bTitlesOnlyForAffinity, boolean bStem, String sPhraseToWordSplitterDelims_) throws Exception {
        Porter porter = null;
        if (bStem) {
            porter = Porter.getInstance();
        }
        if (sPhraseToWordSplitterDelims_ == null) {
            if (this.sDelims_forPhraseAffinityDecomp == null) {
                throw new Exception("neither caller nor this NFS has phrase delimiters");
            } else {
                sPhraseToWordSplitterDelims_ = this.sDelims_forPhraseAffinityDecomp;
            }
        }
        HashSet hsReturn = new HashSet();
        Hashtable htStopWords = Session.stopList.getHTStopWordList();
        iCallCnt_getAffinityTerms++;
        int iMax = sArrSigsTitleThenNon.length;
        if (bTitlesOnlyForAffinity) {
            iMax = iNumtitleTermsInSCarr;
        }
        for (int i = 0; i < iMax; i++) {
            // thes expand sig terms for affinity purposes
            // debug - line space to separate nodes
            StringTokenizer st = null;
            try {
                st = new StringTokenizer(sArrSigsTitleThenNon[i] + " ", this.sDelims_forPhraseAffinityDecomp);
            } catch (Exception e) {
                log.error("tokenizing exception node [" + lNodeID + "] i [" + i + "] sArrSigsTitleThenNon [" + UtilSets.sArrToStr(sArrSigsTitleThenNon) + "]", e);
            }

            String sWordWithinTermOrAlone = null;
            while (st.hasMoreElements()) {
                try {
                    sWordWithinTermOrAlone = ((String) st.nextElement()).trim();

                    if (bStem) {
                        sWordWithinTermOrAlone = porter.stem(sWordWithinTermOrAlone);
                    }

                    if (htStopWords.get(sWordWithinTermOrAlone) == null) {
                        hsReturn.add(sWordWithinTermOrAlone);
                    }
                } catch (Exception e) {
                    log.error("error in getAffinityTerms ", e); // hack hack
                }
            }
        }
        return hsReturn;
    }

    public static String getStrCompressedTermsWithThes(String[] sArrSigsTitleThenNonToUse, int[] iArrTitleAndNonCounts, Vector[] vArrUnderToUse) {
        StringBuilder sbReturn = new StringBuilder();
        for (int iBaseTerm = 0; iBaseTerm < sArrSigsTitleThenNonToUse.length; iBaseTerm++) {
            if (iBaseTerm > 0) {
                sbReturn.append("|||");
            }

            sbReturn.append(iArrTitleAndNonCounts[iBaseTerm]).append(",").append(sArrSigsTitleThenNonToUse[iBaseTerm]);
            if (vArrUnderToUse != null & vArrUnderToUse[iBaseTerm] != null) {
                for (int iThes = 0; iThes < vArrUnderToUse[iBaseTerm].size(); iThes++) {
                    sbReturn.append("||").append(vArrUnderToUse[iBaseTerm].elementAt(iThes));
                } // for each thes terms this term
            } // if any thes terms this term
        } // for all terms
        return sbReturn.toString();


    }

    /**
     *
     * @param sDescCaller
     * @param bStemOn
     * @throws Exception
     */
    public void printme(String sDescCaller, boolean bStemOn) throws Exception {
        iCallCnt++;
        log.debug("\r\n\r\n" + iCallCnt + ". NFS PRINTME TOSTRING caller [" + sDescCaller + "] bStemOn [" + bStemOn + "]  ========= START NODEFORSCORE ========= ");
        log.debug(" nfs.getNodeID():" + getNodeID()
                + "; nodeSize():" + getNodeSize()
                + "; getiNumtitleTermsInSCarr():" + getiNumtitleTermsInSCarr() //"; title" + getTitle (bStemOn , null)
                );

        String[] sArr = getSarr(bStemOn, true, true);
        if (sArr != null) {
            int[] iArr = getiarr();
            for (int j = 0; j < sArr.length; j++) {
                log.debug("getSarr : " + j + ". node [" + getNodeID() + "] term [" + sArr[j] + "]" + " cnt [" + iArr[j] + "]");
            }
        } else {
            log.debug(iCallCnt + ". nfs.getSarr() sarr is null - not inited");
        }
        log.debug(" --- ");
        log.debug("  lNodeID [" + lNodeID + "]");
        log.debug("  iNodeSize [" + iNodeSize + "]");
        log.debug("  lParentNodeID [" + lParentNodeID + "]");
        log.debug("  iCorpusID [" + iCorpusID + "]");
        log.debug("  sNodeTitle [" + sNodeTitle + "]");
        log.debug("  sGist [" + sGist + "]");
        log.debug("  sNodeTitleStemmed [" + sNodeTitleStemmed + "]");
        log.debug("  sArrSigsTitleThenNon [" + UtilSets.convertObjectArrayToStringWithDelims(sArrSigsTitleThenNon, "||") + "]");
        log.debug("  sArrSigsTitleThenNonStemmed [" + UtilSets.convertObjectArrayToStringWithDelims(sArrSigsTitleThenNonStemmed, "||") + "]");
        if (sArrSigsTitleThenNon != null) {
            log.debug("  sArrNodeSigWordsSortedDescMayBeStemmed [" + UtilSets.convertObjectArrayToStringWithDelims(sArrSigsTitleThenNon, "||") + "]");
        }
        if (sArrSigsTitleThenNonStemmed != null) {
            log.debug("  sArrNodeSigWordsSortedDescMayBeStemmed [" + UtilSets.convertObjectArrayToStringWithDelims(sArrSigsTitleThenNonStemmed, "||") + "]");
        }
        log.debug("  iArrTitleAndNonCounts [" + UtilSets.convertiArrayToStringWithDelims(iArrTitleAndNonCounts, "||") + "]");
        log.debug("  bArrNodeSigWordsSortedDescIsStemmed [" + bArrNodeSigWordsSortedDescIsStemmed + "]");
        log.debug("  bNodeSCArrHasPhrases [" + bNodeSCArrHasPhrases + "]");
        log.debug("  iNumtitleTermsInSCarr [" + iNumtitleTermsInSCarr + "]");
        log.debug("  scarr  [" + StringAndCount.toString(sArrSigsTitleThenNon, iArrTitleAndNonCounts) + "]");
        log.debug("  ========= END NODEFORSCORE ========= \r\n\r\n");

    }
}
