package com.iw.scoring.scoring2;

import java.util.*;
import java.sql.*;

import com.indraweb.util.*;

public class Data_NodeIDToCorpusID
{
	
	// *************************************
	public static void clearNodeToCorpus()
	// *************************************
	{
		htgetCorpusID_FromNodeID = null;
		bSemaphoreDataFillDone = false;
	} 
	
	// *************************************
	public static void clearNodeToCorpus( Integer INodeID )
	// *************************************
	{
		htgetCorpusID_FromNodeID.remove ( INodeID );
	} 
	
	
	private static Hashtable htgetCorpusID_FromNodeID = null;
	private static Integer dummyCorpusResposeForROCTEST = new Integer (1);
	private static Integer ISemaphoreDataFill = new Integer (1);
	private static boolean bSemaphoreDataFillDone = false;
//	// *************************************
//	public static Integer getCorpusID_FromNodeID ( Integer INodeID, Connection dbc )
//	// *************************************
//		throws Exception
//	{
//		Integer ICorpusReturn = null;
//		// should only fail to find a node's corpus if node added - get from DB direct and add to cache
//		if ( bSemaphoreDataFillDone == false )
//		{
//			synchronized ( ISemaphoreDataFill ) // one data struct filler only!
//			{
//				if ( bSemaphoreDataFillDone )  // if someone raced me in here
//				{
//					ICorpusReturn = (Integer) htgetCorpusID_FromNodeID.get ( INodeID );
//					if ( ICorpusReturn == null )
//						return com.indraweb.database.DBAccess_Central_connection.getCorpusID_FromNodeID (INodeID, dbc);
//				}
//				else
//				{
//					htgetCorpusID_FromNodeID = new Hashtable();
//					int[] iArrCorpusIDs = com.iw.classification.Data_WordsToNodes.getintArrCorporaAsPerCfg( dbc, false );
//					fillNodeToCorpusHT ( iArrCorpusIDs, dbc );
//					// set so noone else calls data fill
//					bSemaphoreDataFillDone = true;
//				}
//			}
//		}  // if data struct present
//		ICorpusReturn = (Integer) htgetCorpusID_FromNodeID.get ( INodeID );
//		return ICorpusReturn;
//
//	} // public static Integer getCorpusID_FromNodeID ( Integer INodeID, Connection dbc )

	// ******************************************
	synchronized public static void fillNodeToCorpusHT (int[] iArrCorpusIDs, Connection dbc )
	// ******************************************
		throws Exception
	{
		long lStart = System.currentTimeMillis();
		Log.logClear ("Start fillNodeToCorpusHT\r\n");
		StringBuffer sb = new StringBuffer();
		for ( int i = 0; i < iArrCorpusIDs.length; i++ )
		{
			if ( i > 0 ) 
				sb.append ( " or " );
					
			sb.append ( " corpusid = " + iArrCorpusIDs[i] );
		} 
		
		String sql = "select nodeid, corpusid from node where " + sb.toString();
		Statement stmt = null;;
		ResultSet rs = null;
		try 
		{
			stmt = dbc.createStatement();
			rs = stmt.executeQuery (sql);
	
			Integer INodeID = null;
			Integer ICorpusID = null;
			while (rs.next()) 
			{
				INodeID = new Integer (rs.getInt(1));
				ICorpusID = new Integer (rs.getInt(2));
				// CACHING 
				htgetCorpusID_FromNodeID.put (INodeID, ICorpusID); 
			}
		} 
		catch ( Exception e ) 
		{
			Log.FatalError ("getParentNodeID_FromNodeID() : error getting node ID [" + sql + "]", e);	
		} 
		rs.close();
		stmt.close();
		
		Log.logClear ("done fillNodeToCorpusHT count [" + htgetCorpusID_FromNodeID.size() + 
					  "] in ms [" + UtilProfiling.elapsedTimeMillis ( lStart ) + "]\r\n" );
	} 
}
