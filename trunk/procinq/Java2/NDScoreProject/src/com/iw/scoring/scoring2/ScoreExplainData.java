package com.iw.scoring.scoring2;

import com.iw.scoring.NodeForScore;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstance;
import com.iw.guiservershared.xmlserializejava.XMLBeanSerializeHelper;
import com.indraweb.encyclopedia.DocForScore;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

/**
 * ScoreExplainData is the container object to contain classifier score explanation data.
 *
 * @authors Indraweb Inc, All Rights Reserved.
 */
public class ScoreExplainData {

    private Vector vListScoreIDsInOrderOfFirstInsert = new Vector();
    private Hashtable htKeyIsScoreIDToVectorOFStringsPer = new Hashtable();
    String sNodeIDToExplain = (String) com.indraweb.execution.Session.cfg.getPropDefault("NodeIDToExplain", false, "-1");
    int iNodeIDToExplain = Integer.parseInt(sNodeIDToExplain);
    private ExplainDisplayDataContainer explainDisplayDataContainer = null;

    public synchronized void addExplainDisplayDataInstance(
            ExplainDisplayDataInstance explainDisplayDataInstance) {
        if (explainDisplayDataContainer == null) {
            explainDisplayDataContainer = new ExplainDisplayDataContainer();
        }

        explainDisplayDataContainer.addExplainDisplayDataInstance(explainDisplayDataInstance);
    }

    public synchronized void addExplainDisplayDataInstance(
            ExplainDisplayDataInstance explainDisplayDataInstance, int iTabIndex) {
        if (explainDisplayDataContainer == null) {
            explainDisplayDataContainer = new ExplainDisplayDataContainer();
        }

        explainDisplayDataContainer.addExplainDisplayDataInstance(explainDisplayDataInstance, iTabIndex);
    }

    // ***************************
    public void addExplain(int iScoreMethodID,
            // ***************************
            String sExplanationPiece,
            boolean bScoreExplain,
            int iNodeID) {
        if (!bScoreExplain) {
            return;
        }

        if (iNodeIDToExplain > 0 && iNodeID != iNodeIDToExplain) {
            return;
        }

        Integer IScoreMethodID = new Integer(iScoreMethodID);
        Vector vStringsScoreExpl = (Vector) htKeyIsScoreIDToVectorOFStringsPer.get(IScoreMethodID);
        if (vStringsScoreExpl == null) {
            vStringsScoreExpl = new Vector();
            htKeyIsScoreIDToVectorOFStringsPer.put(IScoreMethodID, vStringsScoreExpl);
            vListScoreIDsInOrderOfFirstInsert.addElement(IScoreMethodID);
        }
        vStringsScoreExpl.addElement(sExplanationPiece);
    }

    // ***************************
    public void emitExplainXML(com.iw.scoring.scoring2.ScoresBetweenTwoTextObjects dsftto,
            java.io.PrintWriter out,
            boolean bStemOn)
            // ***************************
            throws Exception {
        //if ( com.indraweb.util.UtilFile.bFileExists("c:/temp/IndraDebugDontEmitOldScoreExplain.txt") )
        //   return;

        //if ( !com.indraweb.util.UtilFile.bFileExists("c:/temp/IndraDebugDontEmitNewScoreExplain.txt")
        if (explainDisplayDataContainer != null) {
            // out.println ("   <NODETITLE><![CDATA["+sNodeTitle+"]]></NODETITLE>");
            out.println("<EXPLAINDISPLAYSET><![CDATA[");
            String sExplainDisplayDataAsXMLString = XMLBeanSerializeHelper.serializeToString(
                    explainDisplayDataContainer.getSerializableForm());
            out.println(sExplainDisplayDataAsXMLString);
            out.println("]]></EXPLAINDISPLAYSET>");
        }

        NodeForScore nfs = (NodeForScore) dsftto.to1;
        DocForScore dfs = (DocForScore) dsftto.to2;
        String sTitleDoc = dfs.getTitle(bStemOn);
        String sDocURL = dfs.getURL();
        int iNumBytesOfWebTopicsToTake = dfs.getParseParms().getNumBytesToTake();
        long lNodeID = nfs.getNodeID();

        // enum score ID's
        out.println("<SCOREEXPLAINBLOCK_0>");
        out.println("<NODEID>" + lNodeID + "</NODEID>");
        String sTitleNode = nfs.getTitle(bStemOn);
        if (!bStemOn) {
            out.println("<NODETITLE><![CDATA[" + sTitleNode + "]]></NODETITLE>");
        } else {
            String sTitleNodeUnStem = nfs.getTitle(false);
            out.println("<NODETITLE><![CDATA[" + sTitleNodeUnStem + "]]></NODETITLE>");
            out.println("<NODETITLESTEM>" + sTitleNode + "</NODETITLESTEM>");
        }
        out.println("<DOCURL><![CDATA[" + sDocURL + "]]></DOCURL>");
        out.println("<DOCTITLE><![CDATA[" + sTitleDoc + "]]></DOCTITLE>");
        out.println("<NUMBYTESCAP>" + iNumBytesOfWebTopicsToTake + "</NUMBYTESCAP>");


        Enumeration e = vListScoreIDsInOrderOfFirstInsert.elements();
        Vector vStringsScoreExpl_local = null;
        while (e.hasMoreElements()) {
            // enum explain strings
            Integer IScoreMethoID = (Integer) e.nextElement();
            vStringsScoreExpl_local = (Vector) htKeyIsScoreIDToVectorOFStringsPer.get(IScoreMethoID);
            if (vStringsScoreExpl_local == null) {
                throw new Exception("in score explain : vStringsScoreExpl_local == null");
            }

            Enumeration eStrings = vStringsScoreExpl_local.elements();
            String sExplainoneline = null;
            while (eStrings.hasMoreElements()) {
                sExplainoneline = (String) eStrings.nextElement();
                out.println(
                        "  <SCOREEXPLAIN_" + IScoreMethoID + ">"
                        + sExplainoneline
                        + "  </SCOREEXPLAIN_" + IScoreMethoID + ">");


            }
        }
        // print scores
/*
         * if (com.indraweb.execution.Session.cfg.getPropBool("ExplainScoreShowDocWords")) ExplainScores_ShowDocWords(dsftto, out, bStemOn);
         */

        out.println("</SCOREEXPLAINBLOCK_0>");
    }

    // ***************************
    private void ExplainScores_ShowDocWords(com.iw.scoring.scoring2.ScoresBetweenTwoTextObjects dsftto,
            // ***************************
            java.io.PrintWriter out,
            boolean bStemOn)
            throws Exception {
        try {
            com.indraweb.encyclopedia.DocForScore dfs = (com.indraweb.encyclopedia.DocForScore) dsftto.getTextObject2();
            Enumeration enumWords = dfs.getEnumWords_notThesExpanded(bStemOn);
            Vector vDocWords = new Vector();
            while (enumWords.hasMoreElements()) {
                String sWord = (String) enumWords.nextElement();
                vDocWords.addElement(sWord);
            }
            String[] sArrWords = new String[vDocWords.size()];
            vDocWords.copyInto(sArrWords);

            com.indraweb.gfs.GFSSort.sortArrayStrings(sArrWords);

            Hashtable htWordsnCounts = dfs.getHtWordsNCount(bStemOn);
            out.println("    <DOCWORDS>");
            out.println("<![CDATA["); //]]>
            for (int i = 0; i < sArrWords.length; i++) {
                String sCount = null;
                Integer ICount = (Integer) htWordsnCounts.get(sArrWords[i]);
                if (ICount == null) {
                    sCount = null;
                } else {
                    sCount = ICount.toString();
                }

                out.println(i + ". {" + sArrWords[i] + ":" + sCount + "}");
            }
            out.println("]]>");
            out.println("    </DOCWORDS>");
            out.flush();
        } catch (Exception e) {
            com.indraweb.util.Log.NonFatalError("in ExplainScoresShowDocWordsAlso", e);
            throw e;
        }
    }

    // ***************************
    public void emptyScoreExplainer() // ***************************
    {
        vListScoreIDsInOrderOfFirstInsert = new Vector();
        htKeyIsScoreIDToVectorOFStringsPer = new Hashtable();
    }
}
