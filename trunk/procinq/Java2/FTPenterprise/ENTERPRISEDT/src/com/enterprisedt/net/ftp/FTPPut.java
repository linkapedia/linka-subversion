/*                                                         * Created by IntelliJ IDEA.
 * User: henry kon
 * Date: Nov 10, 2002
 * Time: 4:06:54 AM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.enterprisedt.net.ftp;

import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.enterprisedt.net.ftp.FTPConnectMode;
import java.io.IOException;

public class FTPPut {

    /**
     *  Top level call to put a file
     *  See FTPClientTest usage method for parm options
     *
     *    TO DO: expand this!
     *
     *  @author             HBK
        //String host = "ftp.hostname.com";
        //String user = "hkon2";
        //String password = "xxxxx";
        //String slocalfilenameFQ= "c:/temp/temp/temp/test/test.zip";
        //String sremotefilename= "test.zip";
        //String  filename= "test.zip";
        //String sRemoteDir = "/usr/tmp";
        //String mode = "binary";
        //String connMode = "active";     *
     */
    public static void put  (
            String host,
            String user,
            String password,
            String sLocalFileNameFQ,
            String sRemoteDir ,
            String sRemoteFilename,
            String mode,
            String connMode

    ) throws Exception {


        // connect and test supplying port no.
        FTPClient ftp = new FTPClient(host, 21);
        //ftp.login(user, password);
        //ftp.quit();

        // connect again
        ftp = new FTPClient(host);

        // switch on debug of responses
        ftp.debugResponses(true);

        ftp.login(user, password);

        // binary transfer
        if (mode.toUpperCase().equalsIgnoreCase("BINARY")) {
            ftp.setType(FTPTransferType.BINARY);
        }
        else if (mode.toUpperCase().equalsIgnoreCase("ASCII")) {
            ftp.setType(FTPTransferType.ASCII);
        }
        else {
            throw new Exception ("unknown ftp transfer type [" + mode + "] valid are BINARY and ASCII case insensitive." );
        }

        // PASV or active?
        if (connMode.equalsIgnoreCase("PASV")) {
            ftp.setConnectMode(FTPConnectMode.PASV);
        }
        else if (connMode.equalsIgnoreCase("ACTIVE")) {
            ftp.setConnectMode(FTPConnectMode.ACTIVE);
        }
        else {
            throw new Exception ("Unknown connect mode: " + connMode);
        }

        // change dir
        ftp.chdir(sRemoteDir);

        // put a local file to remote host

        ftp.put(sLocalFileNameFQ, sRemoteFilename);

        // get bytes
        byte[] buf = ftp.get(sRemoteFilename);

        // append local file
        ftp.put(sLocalFileNameFQ, sRemoteFilename, true);

        // get bytes again - should be 2 x
        //buf = ftp.get(sRemoteFilename);
        //System.out.println("Got " + buf.length + " bytes");

        // rename
        //ftp.rename(sRemoteFilename, sRemoteFilename+ ".new");

        // get a remote file - the renamed one
        //ftp.get(sRemoteFilename+ ".new", sRemoteFilename+ ".tst");

        // ASCII transfer
        //ftp.setType(FTPTransferType.ASCII);

        // test that list() works
        //String listing = ftp.list(".");
        //System.out.println(listing);

        // test that dir() works in full mode
        //String[] listings = ftp.dir(".", true);
        //for (int i = 0; i < listings.length; i++)
        //    System.out.println(listings[i]);

        // try system()
        //System.out.println(ftp.system());

        // try pwd()
        //System.out.println(ftp.pwd());

        ftp.quit();
    }


    /**
     *  Basic usage statement
     */
    public static void usage() {

        System.out.println("Usage: ");
        System.out.println("com.enterprisedt.net.ftp.FTPClientTest " +
                           "remotehost user password filename directory " +
                           "(ascii|binary) (active|pasv)");
    }

}
















