package com.iw.ui;

import java.awt.*;
import javax.swing.*;

public class WaittingPanel extends JInternalFrame {

    private ITSAdministrator itsframe = null;
    private String title;

    public WaittingPanel(ITSAdministrator frame, String title) {
        this.itsframe = frame;
        this.title = title;
        build();
    }

    private void build() {
        pack();
        setSize(350, 150);
        openAtCenter();
        setTitle(this.title);
        getContentPane().setLayout(new BorderLayout());
        JPanel jpm = new JPanel();
        ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/waiting_80.png");
        JLabel picLabel = new JLabel(iIndraweb);
        jpm.add(picLabel, BorderLayout.NORTH);
        jpm.add(new JLabel("this can take several seconds, please be patient"), BorderLayout.CENTER);
        setFrameIcon(itsframe.iIndraweb);
        getContentPane().add(jpm);
        itsframe.jdp.add(this);

    }

    public void openAtCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 2);
    }
}
