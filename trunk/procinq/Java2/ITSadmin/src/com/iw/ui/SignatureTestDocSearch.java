package com.iw.ui;

import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.tools.JSortTable;
import com.iw.tools.SwingWorker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

public class SignatureTestDocSearch extends DocumentSearch implements ActionListener {
    private Component glass;
    private JInternalFrame MDIframe;
    private JPanel openPanel;
    private User user;
    private ITSTreeNode node;
    private Vector signatures;

    protected JButton testButton;

    public SignatureTestDocSearch (ITSAdministrator frame, ITSTreeNode n, Vector v) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;
        node = n; signatures = v;

        user = ITS.getUser();

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Signature Test", true, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(675, 480);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        searchButton = new JButton("Search");
        searchButton.addActionListener(this);
        cancelButton = new JButton("Close");
        cancelButton.addActionListener(this);
        openButton = new JButton("Open");
        openButton.addActionListener(this);
        testButton = new JButton("Test");
        testButton.addActionListener(this);

        fulltextLabel = new JLabel("Full Text:");
        fulltextLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        fulltextLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        fulltextText = new JTextField();

        titleLabel = new JLabel("Title:");
        titleLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        titleText = new JTextField();

        queryLabel = new JLabel("Abstract:");
        queryLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        abstractText = new JTextField();

        topicLabel = new JLabel("Topics:");
        topicLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        topicText = new JTextField();

        browseButton = new JButton("...");
        browseButton.addActionListener(this);

        resultsTable = new JSortTable();

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestGrid(2, 4);
                            {
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(fulltextLabel);
                                    layout.parent();
                                }
                                layout.add(fulltextText);
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(titleLabel);
                                    layout.parent();
                                }
                                layout.add(titleText);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.addSpace(10);
                                    layout.add(queryLabel);
                                    layout.parent();
                                }
                                layout.add(abstractText);
                                layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                layout.add(topicLabel);
                                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.add(topicText);
                                    layout.add(browseButton);
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                            {
                                layout.add(new JScrollPane(resultsTable));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.addSpace(10);
                        layout.nestBox(OculusLayout.VERTICAL);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(searchButton);
                                        layout.addSpace(10);
                                        layout.add(cancelButton);
                                        layout.addSpace(10);
                                        layout.add(openButton);
                                        layout.addSpace(10);
                                        layout.add(testButton);
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();

            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(searchButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        final DocumentSearch ds = this;

        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {
                    if (e.getSource().equals(cancelButton)) {
                        MDIframe.dispose();
                    } else if (e.getSource().equals(browseButton)) {
                        try {
                            TopicSearch ts = new TopicSearch(ITSframe, ds);
                            ts.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    } else if (e.getSource().equals(searchButton)) {
                        try {
                            loadResults();
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                    } else if (e.getSource().equals(openButton)) {
                        viewDocument();
                    } else if (e.getSource().equals(testButton)) {
                        int[] rows = resultsTable.getSelectedRows();
                        Vector nodeDocuments = new Vector();

                        for (int i = 0; i < rows.length; i++) {
                            Document d = (Document) resultsTable.getModel().getValueAt(rows[i], 0);
                            NodeDocument nd = node.getNodeDocument(d);
                            nodeDocuments.add(nd);
                        }

                        if (nodeDocuments.size() == 0) {
                            JOptionPane.showMessageDialog(ITSframe, "Sorry, you must first select one or more documents.",
                                "Information", JOptionPane.NO_OPTION);
                            return;
                        }

                        // begin classifying documents against this node
                        SignatureTestPanel stp = new SignatureTestPanel(ITSframe, node, nodeDocuments, signatures, true);
                        stp.setVisible(true);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

}
