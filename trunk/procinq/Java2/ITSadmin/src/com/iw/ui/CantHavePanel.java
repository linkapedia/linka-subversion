package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.oculustech.layout.*;
import com.iw.system.*;

public class CantHavePanel extends JPanel implements ActionListener {

    protected JLabel operation;
    protected JTextField txtToLevel;
    protected JTextField txtUserType;
    protected JButton buttonApply;
    protected JButton buttonCancel;
    private JInternalFrame MDIframe;
    private ITSAdministrator itsadmin;
    private ITS its;
    private ITSTreeNode Node;

    public CantHavePanel(ITSAdministrator itsFrame, ITSTreeNode n) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        its = itsFrame.its;
        Node = n;
        itsadmin = itsFrame;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("CantHave Panel", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(450, 140);
        MDIframe.setBackground(Color.lightGray);
        MDIframe.setResizable(false);

        MDIframe.setFrameIcon(itsFrame.iIndraweb);


        JDesktopPane jdesktoppane = itsFrame.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        operation = new JLabel("From current selection To level:");
        operation.setToolTipText("Choose the depth.");

        txtToLevel = new JTextField("2", 5);

        txtUserType = new JTextField(30);

        buttonApply = new JButton("Apply");
        buttonApply.setToolTipText("Apply changes");
        buttonApply.addActionListener(this);

        buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.nestGrid(2, 14);
                    {
                        layout.add(operation);
                        layout.add(txtToLevel);
                        layout.add(new JLabel("User type information"));
                        layout.add(txtUserType);
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(buttonApply);
            layout.addSpace(10);
            layout.add(buttonCancel);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
    }

    public void openAtUpperCenter() {
        Dimension winsize = MDIframe.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        MDIframe.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(buttonCancel)) {
            MDIframe.dispose(); // close the window when the action is taken
            return;
        }

        if (e.getSource().equals(buttonApply)) {
            String depth = txtToLevel.getText();
            String wordToPut = txtUserType.getText();
            PopupProgressBar ppb = new PopupProgressBar(itsadmin,
                    "Adding CantHaves", 0, 100);
            ppb.setVisible(true);
            try {
                String success = its.addCantHave(
                        Node.get("NODEID"),
                        depth,
                        wordToPut,
                        ppb);
                JOptionPane.showMessageDialog(itsadmin, success, "Information",
                        JOptionPane.NO_OPTION);
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
                JOptionPane.showMessageDialog(itsadmin, "Could not add terms: "
                        + ex.getMessage(), "Error", JOptionPane.NO_OPTION);
            } finally {
                ppb.dispose();
            }
        }
        MDIframe.dispose(); // close the window when the action is taken
    }
}
