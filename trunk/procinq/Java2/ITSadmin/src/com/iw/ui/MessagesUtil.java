package com.iw.ui;

import java.util.StringTokenizer;
import javax.swing.JOptionPane;

public class MessagesUtil {

    private static final int DEFAULT_MAX_CHARACTERS_BY_LINE = 50;

    public static void showInfoMessage(String title, String body) {
        body = getParserText(body, title);
        JOptionPane.showMessageDialog(null, body, "Information", JOptionPane.INFORMATION_MESSAGE);
    }

    private static String getParserText(String text, String title) {
        StringTokenizer st = new StringTokenizer(text);
        String currentWord;
        int lenByLine = 0;
        StringBuilder parserText = new StringBuilder("<html><p><h1><u>"+title+"</u></h1><br>");
        while (st.hasMoreTokens()) {
            currentWord = st.nextToken();
            lenByLine += currentWord.length() + 1;
            if (lenByLine >= DEFAULT_MAX_CHARACTERS_BY_LINE) {
                parserText.append("<br>");
                lenByLine = currentWord.length();
            }
            parserText.append(currentWord).append(" ");
        }
        parserText.append("</p></html>");
        return parserText.toString();
    }
}
