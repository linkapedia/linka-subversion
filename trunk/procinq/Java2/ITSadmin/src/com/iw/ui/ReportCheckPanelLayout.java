package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.tools.JSortTable;

public class ReportCheckPanelLayout extends OculusBox {
    protected JLabel jlTaxonomy;
    protected JComboBox jComboBoxTax;
    protected JButton jbSearch;
    protected JLabel jlThesaurus;
    protected JComboBox jComboBoxThes;
    protected JButton jbClose;
    protected JTable jSortTable_OVERRIDE;

    public ReportCheckPanelLayout() {
        initComponents();
    }

    private void initComponents() {
        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        jlTaxonomy = new JLabel("Taxonomy:");
        jlTaxonomy.setFont(Font.decode("SansSerif-BOLD-11"));

        jComboBoxTax = new JComboBox();
        jComboBoxTax.setToolTipText("Select one or all taxonomies to search");

        jbSearch = new JButton("Search");
        jbSearch.setFont(Font.decode("SansSerif-11"));
        jbSearch.setToolTipText("Find topics whose titles are also thesaurus terms");

        jlThesaurus = new JLabel("Thesaurus:");
        jlThesaurus.setFont(Font.decode("SansSerif-BOLD-11"));

        jComboBoxThes = new JComboBox();
        jComboBoxThes.setToolTipText("Select one or all thesauri to search");

        jbClose = new JButton("Close");

        jSortTable_OVERRIDE = new JSortTable();
        jSortTable_OVERRIDE.setToolTipText("Topics from selected corpora having titles matching selected thesauri");

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.addSpace(10);
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestGrid(2, 5);
                                {
                                    layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.addSpace(10);
                                        layout.add(jlTaxonomy);
                                        layout.parent();
                                    }
                                    layout.add(jComboBoxTax);
                                    layout.setGridCellJustification(OculusLayout.RIGHT_JUSTIFY, OculusLayout.CENTER);
                                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                                    {
                                        layout.addSpace(10);
                                        layout.add(jlThesaurus);
                                        layout.parent();
                                    }
                                    layout.add(jComboBoxThes);
                                    layout.addSpace(1);
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(new JScrollPane(jSortTable_OVERRIDE));
                                        layout.addSpace(10);
                                        layout.parent();
                                    }
                                    layout.addSpace(1);
                                    layout.parent();
                                }
                                layout.addSpace(10);
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.nestBox(OculusLayout.VERTICAL);
                                {
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(jbSearch);
                                        layout.addSpace(10);
                                        layout.add(jbClose);
                                        layout.parent();
                                    }
                                    layout.addSpace(10);
                                    layout.parent();
                                }
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);
    }

    public static void main(String[] args) throws Exception {
        // insert your license # below to avoid unlicensed warning popups
        OculusLayout.setLicenseNumber("EVAL");
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ReportCheckPanelLayout pane = new ReportCheckPanelLayout();
        frame.setContentPane(pane);
        frame.pack();
        frame.show();
    }
}
