package com.iw.ui;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.border.*;


import java.awt.Component;
import java.awt.Color;

/**
 * Class for rendering (displaying) individual cells
 * in a JTable.
 * @see JTable
 */
public class DisabledTableRenderer extends DefaultTableCellRenderer {
    private Color unselectedForeground;
    private Color unselectedBackground;
    private Border emptyBorder = BorderFactory.createEmptyBorder();
    private int currentRow = -1;

    /**
     * Overrides <code>JComponent.setForeground</code> to specify
     * the unselected-foreground color using the specified color.
     */

    public void setForeground(Color c) {
        super.setForeground(c);
        unselectedForeground = c;
    }


    public void setCurrentRow(int i) {
        currentRow = i;
    }


    public int getCurrentRow() {
        return currentRow;
    }

    /**
     * Overrides <code>JComponent.setForeground</code> to specify
     * the unselected-background color using the specified color.
     */
    public void setBackground(Color c) {
        super.setBackground(c);
        unselectedBackground = c;
    }


    // implements javax.swing.table.TableCellRenderer
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        if (isSelected) {
            super.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else if (currentRow == row) {
            super.setBackground(table.getSelectionBackground());
        } else {
            super.setForeground((unselectedForeground != null) ?
                    unselectedForeground : table.getForeground());
            super.setBackground((unselectedBackground != null) ?
                    unselectedBackground : table.getBackground());
        }
        if (currentRow >= 0) {
            super.setForeground((Color) UIManager.get("textInactiveText"));
        }
        setFont(table.getFont());
        setBorder(emptyBorder);
        setValue(value);


        return this;
    }


}
