/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Feb 10, 2004
 * Time: 5:30:59 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.system.Genre;

import javax.swing.event.*;
import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.border.LineBorder;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;

public class FolderManipulationFrame extends ManipulationFrame implements ActionListener, CellEditorListener {
    private ITSAdministrator admin = null;
    private CellRenderer _renderer;

    private ManipulationTextField jtf = null;

    public FolderManipulationFrame(ITSAdministrator frame) throws Exception {
        super(frame);

        MDIframe.setTitle("Subject Area Tool");
        admin = frame;

        resultsTable.setRowSelectionAllowed(false);
        resultsTable.setSelectionForeground(Color.BLUE);

        // define the two object types
        jtf = new ManipulationTextField();

        resultsTable.setCellSelectionEnabled(true);

        // add action listener
        addButton.addActionListener(this);
        removeButton.addActionListener(this);
        closeButton.addActionListener(this);
        saveButton.addActionListener(this);

        loadResults(frame.its.getFolders());
    }

    /*
    public class UpdateAnchorAction extends AbstractAction {
        JTable table;

        UpdateAnchorAction(JSortTable table) {
            super("Set Anchor");
            this.table = table;
        }

        // Update the value in the anchor cell whenever the text field changes
        public void actionPerformed(ActionEvent evt) {
            System.out.println("WOO HOO!");
        }
    } */

    public void loadResults(Vector v) throws Exception {
        if (v.size() == 0) {
            return;
        }

        final SortAbstractTableModel dtm = new SortAbstractTableModel();
        final String[] columnNames = {"Name", "Active"};
        dtm.setColumnNames(columnNames);

        Object o[][] = new Object[v.size()][2];
        for (int i = 0; i < v.size(); i++) {
            Genre g = null;
            try {
                g = (Genre) v.elementAt(i);
                o[i][0] = g;
                o[i][1] = new Boolean(g.getActive());
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }
        dtm.setValues(o); dtm.setNumberItems(v.size());

        resultsTable.setModel(dtm);
        TableColumn col = resultsTable.getColumnModel().getColumn(0);
        TableColumn col2 = resultsTable.getColumnModel().getColumn(1);

        col.setPreferredWidth(325);
        col2.setPreferredWidth(10);

        GenreEditor dce = new GenreEditor(jtf);
        col.setCellEditor(dce);

        DefaultCellEditor tce = new DefaultCellEditor(new JCheckBox());
        col2.setCellEditor(tce);
        tce.addCellEditorListener(this);

        dtm.sortColumn(0, false);
    }

    public void saveGenres() throws Exception {
        for (int i = 0; i < resultsTable.getRowCount(); i++) {
            Genre sa = (Genre) resultsTable.getValueAt(i, 0);
            Boolean b = (Boolean) resultsTable.getValueAt(i, 1);

            String sWord = "";

            if (sa.isModified()) {
                System.out.println("SA: "+sa.getName()+" modified? "+sa.isModified()+" active? "+b.booleanValue());

                sa.setActive(b.booleanValue());
                admin.its.saveFolder(sa);
            }
            sa.setModified(false);
            resultsTable.setValueAt(sa, i, 0);
        }

        saveButton.setEnabled(false);
    }

    public void editingStopped(ChangeEvent e) {
        Genre sa = (Genre) resultsTable.getValueAt(resultsTable.getSelectedRow(), 0);
        sa.setModified(true);
        saveButton.setEnabled(true);
    }
    public void editingCanceled(ChangeEvent e) {}

    public void actionPerformed(ActionEvent event) {
        MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            if (event.getSource().equals(closeButton)) {
                MDIframe.dispose();
            } else {
                try {
                SwingWorker aWorker = new SwingWorker(event) {
                    protected void doNonUILogic() {
                        if (e.getSource ().equals(addButton)) {
                            String inputValue =
                                    JOptionPane.showInputDialog("Please enter the name of this new folder.");
                            if (inputValue == null) {
                                return;
                            }
                            SortAbstractTableModel dstm = (SortAbstractTableModel) resultsTable.getModel();
                            Genre g = new Genre(); g.setName(inputValue); g.setActive(true);
                            Vector v = new Vector();
                            try {
                                g = admin.its.addFolder(g);
                                dstm.addRow(new Object[] {g, new Boolean(true)});
                            } catch (Exception e) { e.printStackTrace(System.out); }
                        }
                        if (e.getSource().equals(saveButton)) {
                            if (resultsTable.getEditingRow() != -1) {
                                resultsTable.getCellEditor().stopCellEditing();
                            }

                            try { saveGenres(); }
                            catch (Exception e) { e.printStackTrace(System.out); }
                        }
                        if (e.getSource().equals(removeButton)) {
                            SortAbstractTableModel dstm = (SortAbstractTableModel) resultsTable.getModel();
                            int i = resultsTable.getSelectedRow();

                            Genre g = (Genre) resultsTable.getModel().getValueAt(i, 0);
                            try { admin.its.removeFolder(g); loadResults(admin.its.getFolders()); }
                            catch (Exception e) { e.printStackTrace(System.out); }
                        }
                    }
                    protected void doUIUpdateLogic() {}

                    public void finished() {
                        super.finished();
                        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        getGlassPane().setVisible(false);
                    }
                };
                aWorker.start();

            } catch (Exception e) { e.printStackTrace(System.out); }
        }} catch (Exception e) { e.printStackTrace(System.out); }
        finally { MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); }
    }

    // set the current Genre as modified after check box changed
    public void setRowModified() {
        int row = resultsTable.getSelectedRow();
        setRowModified(row);
    }
    public void setRowModified(int index) {
        Genre g = (Genre) resultsTable.getModel().getValueAt(index, 0);

        if (g != null) {
            g.setModified(true);
            saveButton.setEnabled(true);

            resultsTable.getModel().setValueAt(g, index, 0);
        }
    }

    class GenreEditor extends DefaultCellEditor {
        Genre g = null;

        public GenreEditor(ManipulationTextField b) {
                super(b); //Unfortunately, the constructor
                                        //expects a check box, combo box,
                                        //or text field.
            editorComponent = b;
            setClickCountToStart(2); //This is usually 1 or 2.

            //Must do this so that editing stops when appropriate.
            b.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    fireEditingStopped();
                }
            });
        }

        protected void fireEditingStopped() {
            g.setName(((JTextField) editorComponent).getText());

            super.fireEditingStopped();
        }

        public Object getCellEditorValue() {
            return g;
        }

        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column) {
            ((ManipulationTextField)editorComponent).setText(value.toString());

            g = (Genre)value;
            g.setModified(true);
            g.setName(value.toString());

            saveButton.setEnabled(true);

            return editorComponent;
        }
    }
}
