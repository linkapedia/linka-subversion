package com.iw.ui;

import com.iw.system.ITS;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class LogsWikiImport extends JInternalFrame {
    private JTextPane textPane;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");    

    public LogsWikiImport(ITSAdministrator frame) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);
        setSize(483, 330);
        setTitle("Logs Wiki Import");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;
        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        textPane = new JTextPane();
        textPane.setLocation(0, 0);
        textPane.setSize(479, 303);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(0, 0, 479, 303);
        scrollPane.setViewportView(textPane);

        getContentPane().add(scrollPane);
        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }
    
    public void setText(String text){
        textPane.setText(text);
    }
    
    public String getText(){
        return textPane.getText();
    }
}
