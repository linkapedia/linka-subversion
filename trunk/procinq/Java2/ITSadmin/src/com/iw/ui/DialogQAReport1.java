package com.iw.ui;

import com.iw.system.ITS;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class DialogQAReport1 extends JInternalFrame implements ActionListener {

    private JSpinner spnLessSignature;
    private JTextField txtLessSignature;
    private JSpinner spnMoreSignature;
    private JTextField txtMoreSignatures;
    private JSpinner snpMoreMusthave;
    private JTextField txtMoreMusthave;
    private JButton btnRun;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    private Map<String, String> parameters;
    private SignatureQA parent;

    public DialogQAReport1(ITSAdministrator frame, SignatureQA parent) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);

        this.parent = parent;


        SpinnerModel model =
                new SpinnerNumberModel(1, //initial value
                1, //min
                null, //max
                1);

        SpinnerModel model1 =
                new SpinnerNumberModel(1, //initial value
                1, //min
                null, //max
                1);

        SpinnerModel model2 =
                new SpinnerNumberModel(1, //initial value
                1, //min
                null, //max
                1);

        setSize(384, 200);
        setTitle("Filter: Report - signatures / musthaves");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        JLabel lblLessSignatures = new JLabel("Signatures, less than");
        lblLessSignatures.setBounds(12, 12, 183, 15);
        getContentPane().add(lblLessSignatures);
        spnLessSignature = new JSpinner(model);
        spnLessSignature.setBounds(201, 10, 161, 19);
        txtLessSignature = ((JSpinner.DefaultEditor) spnLessSignature.getEditor()).getTextField();
        txtLessSignature.setText("5");
        txtLessSignature.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    txtLessSignature.setText(txtLessSignature.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(spnLessSignature);

        JLabel lblImportedTaxonomyName = new JLabel("Signatures, more than");
        lblImportedTaxonomyName.setBounds(12, 50, 183, 15);
        getContentPane().add(lblImportedTaxonomyName);
        spnMoreSignature = new JSpinner(model1);
        spnMoreSignature.setBounds(201, 48, 161, 19);
        txtMoreSignatures = ((JSpinner.DefaultEditor) spnMoreSignature.getEditor()).getTextField();
        txtMoreSignatures.setText("40");
        txtMoreSignatures.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    txtMoreSignatures.setText(txtMoreSignatures.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(spnMoreSignature);

        JLabel lblMoreMusthaves = new JLabel("Mutshaves, = 0 or more than");
        lblMoreMusthaves.setBounds(12, 89, 183, 15);
        getContentPane().add(lblMoreMusthaves);

        snpMoreMusthave = new JSpinner(model2);
        snpMoreMusthave.setBounds(201, 87, 161, 20);
        txtMoreMusthave = ((JSpinner.DefaultEditor) snpMoreMusthave.getEditor()).getTextField();
        txtMoreMusthave.setText("5");
        txtMoreMusthave.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    txtMoreMusthave.setText(txtMoreMusthave.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(snpMoreMusthave);


        btnRun = new JButton("Apply");
        btnRun.setBounds(245, 135, 117, 25);
        btnRun.addActionListener(this);
        getContentPane().add(btnRun);

        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnRun) {
            setterParameters();
            this.parent.startReport1();
            this.dispose();
        }
    }

    private void setterParameters() {
        parameters = new HashMap<String, String>();
        parameters.put("LESSSIGNATURES", txtLessSignature.getText());
        parameters.put("MORESIGNATURES", txtMoreSignatures.getText());
        parameters.put("MOREMUSTHAVES", txtMoreMusthave.getText());
    }

    public Map<String, String> getParameters() {
        return parameters;
    }
}
