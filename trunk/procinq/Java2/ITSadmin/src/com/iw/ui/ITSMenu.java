package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Vector;

import javax.help.CSH;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.xml.sax.SAXParseException;

import com.iw.license.IndraLicense;
import com.iw.system.Corpus;
import com.iw.system.HashTree;
import com.iw.system.ITS;
import com.iw.system.Thesaurus;
import com.iw.system.User;
import com.iw.tools.AutonomyOut;
import com.iw.tools.CommaDelimOut;
import com.iw.tools.EndecaOutNameCat;
import com.iw.tools.EndecaOutNameCatFromSigs;
import com.iw.tools.EndecaOutThesaurus;
import com.iw.tools.FilterTitleOut;
import com.iw.tools.FilterTitleSigOut;
import com.iw.tools.FlatTaxonomyParent;
import com.iw.tools.FlattenedTaxonomyOut;
import com.iw.tools.ImageUtils;
import com.iw.tools.IngestThesaurus;
import com.iw.tools.Inxight5Out;
import com.iw.tools.InxightNameCatalogOut;
import com.iw.tools.InxightOut;
import com.iw.tools.InxightOutConteg;
import com.iw.tools.KofaxOut;
import com.iw.tools.MarkLogicOut;
import com.iw.tools.NarrMenuItem;
import com.iw.tools.OTLOut;
import com.iw.tools.PDFimportHandlerPass1;
import com.iw.tools.SicTitleMatchOut;
import com.iw.tools.SurfWaxOut;
import com.iw.tools.SwingWorker;
import com.iw.tools.TefOut;
import com.iw.tools.TranslateCSVOut;
import com.iw.tools.UltraseekOut;
import com.iw.tools.VerityOut;
import com.iw.tools.XtmOut;
import com.iw.tools.mq.core.AdministerClassifiers;
import com.iw.ui.conceptalerts.ConceptAlerts;
import com.iw.ui.conceptalerts.ManageAlerts;
import com.iw.ui.exportgui.ExportCorpusSelect;
import com.oculustech.layout.OculusLayout;

public class ITSMenu extends JPanel implements ActionListener, Runnable {

    private static final long serialVersionUID = 4759160037649892072L;
    public final String hsName = "properties/its.hs";
    public static boolean addMenuItems = false;
    JMenuBar menuBar = null;
    // FILE
    JMenu Filemenu = null;
    JMenuItem TaxSubMenu = null;
    JMenuItem ThsSubMenu = null;
    JMenuItem FolSubMenu = null;
    JMenuItem SASubMenu = null;
    JMenuItem AlSubMenu = null;
    JMenuItem classMenuItem = null;
    JMenuItem importDictionaryMenu = null;
    JMenuItem conceptMenuItem = null;
    JMenuItem exitMenuItem = null;
    // EDIT
    private static JMenu Editmenu = null;
    JMenuItem copyMenuItem = null;
    JMenuItem pasteMenuItem = null;
    JMenuItem cutMenuItem = null;
    JMenuItem prefMenuItem = null;
    // SEARCH
    JMenu Searchmenu = null;
    JMenuItem nodeMenuItem = null;
    JMenuItem docMenuItem = null;
    JMenuItem adTopMenuItem = null;
    JMenuItem adDocMenuItem = null;
    // INGEST
    JMenu Ingestmenu = null;
    JMenuItem importMenuItem = null;
    JMenuItem filesystemMenuItem = null;
    JMenuItem xmlMenuItem = null;
    JMenuItem xmlOReillyMenuItem = null;
    JMenuItem importOxfordMenuItem = null;
    JMenuItem importGaleMenuItem = null;
    JMenuItem cnfMenuItem = null;
    JMenuItem theMenuItem = null;
    JMenuItem csvImportMenuItem = null;
    JMenuItem aqReportGenerate = null;
    JMenuItem createCsvWiki = null;
    JMenuItem csvImportGAAPMenuItem = null;
    JMenuItem pipesvImportMenuItem = null;
    JMenuItem pipesv2ImportMenuItem = null;
    JMenuItem thesFromCsvMenuItem = null;
    JMenu translatemenu = null;
    JMenuItem transFR = null;
    JMenuItem transES = null;
    JMenuItem transAR = null;
    JMenuItem transFS = null;
    JMenuItem transCN = null;
    JMenuItem transRU = null;
    JMenuItem transKR = null;
    JMenuItem transDE = null;
    JMenuItem transSW = null;
    JMenuItem transDU = null;
    JMenuItem transIT = null;
    JMenuItem transJP = null;
    JMenuItem transPG = null;
    // EXPORT
    JMenu Exportmenu = null;
    JMenuItem xtmMenuItem = null;
    JMenuItem owlMenuItem = null;
    JMenuItem inxightMenuItem = null;
    JMenuItem inxightRMenuItem = null;
    JMenuItem inxightSICMenuItem = null;
    JMenuItem SICTitleMenuItem = null;
    JMenuItem inxight5MenuItem = null;
    JMenuItem inxight5ANDMenuItem = null;
    JMenuItem inxightNameCatalogMenuItem = null;
    JMenuItem automMenuItem = null;
    // JMenuItem endecaMenuItem = null; //same as endeca name catalog
    // JMenuItem endecaNoSynMenuItem = null; //same as endeca name catalog but
    // without synonyms
    JMenuItem endecaNameCatalog = null;
    JMenuItem endecaNameCatalogSigsAsSynonyms = null;
    JMenuItem endecaThesaurusFromMustHaves = null;
    JMenuItem verityMenuItem = null;
    JMenuItem verityCengageMenuItem = null;
    JMenuItem ultraseekMenuItem = null;
    JMenuItem tefMenuItem = null;
    JMenuItem tefNoCommasNoParensMenuItem = null;
    JMenuItem FlatMenuItem = null;
    JMenuItem FlatMenuItem2 = null;
    JMenuItem CommaMenuItem = null;
    JMenuItem SurfMenuItem = null;
    JMenuItem FltMenuItem = null;
    JMenuItem FltSigMenuItem = null;
    JMenuItem KofaxOutMenuItem = null;
    JMenuItem MarkLogicMenuItem = null;
    JMenuItem TranslateMenuItem = null;
    // REPORT
    JMenu Reportmenu = null;
    JMenuItem checkMenuItem = null;
    JMenuItem classifyMenuItem = null;
    JMenuItem refreshMenuItem = null;
    JMenuItem nodeReportMenuItem = null;
    JMenuItem childReportMenuItem = null;
    JMenuItem sigReportMenuItem = null;
    JMenuItem qaReportMenuItem = null;
    JMenuItem wikiReportMenuItem = null;
    JMenuItem qaReportGenerateMenuItem = null;
    JMenuItem iamgesProcessMenuItem = null;
    JMenuItem musthaveProcessMenuItem = null;
    // HELP
    JMenu Helpmenu = null;
    JMenuItem helpMenuItem = null;
    JMenuItem aboutMenuItem = null;
    JMenuItem versionMenuItem = null;
    // SELECT/IMPORT
    JMenu selectResource = null;
    JMenuItem managerResource = null;
    JMenuItem splitArchives = null;
    // select classifiers
    JMenu classifiers = null;
    JMenuItem admonClassifiers = null;
    // JMenuItem importLocalMenuItem = null;
    JToolBar toolBar = null;
    JButton taxButton = null;
    JButton theButton = null;
    JButton impButton = null;
    JButton expButton = null;
    JButton hlpButton = null;
    JButton txiButton = null;
    JButton claButton = null;
    ITSAdministrator itsAdminFrame = null;
    Vector narrativeTypes = new Vector();
    private StringBuilder menuXmlTest = new StringBuilder("");

    public ITSMenu(ITSAdministrator itsadminSession) throws Exception // this
    // panel
    // is
    // used
    // by
    // both
    // applet
    // and
    // app -
    // hbk
    {
        super(new BorderLayout());
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        this.itsAdminFrame = itsadminSession;
        this.add(new JLabel("ProcinQ Menu"));

        try {
            narrativeTypes = itsadminSession.its.getNarrativeTypes();
        } catch (Exception e) {
            System.err
                    .println("Error retrieving narrative types from the API layer!");
        }

        com.incors.plaf.alloy.AlloyLookAndFeel.setProperty("alloy.licenseCode",
                "7#Michael_Hoey#gecuh5#6ygjkk");
        try {
            javax.swing.LookAndFeel alloyLnF = new com.incors.plaf.alloy.AlloyLookAndFeel();
            javax.swing.UIManager.setLookAndFeel(alloyLnF);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            // You may handle the exception here
        }

        JDesktopPane jdesktoppane = new JDesktopPane();
        jdesktoppane.setSize(800, 600);

        this.add(jdesktoppane, BorderLayout.CENTER);

        this.setSize(800, 600);

        jdesktoppane.setVisible(true);
        itsadminSession.jdp = jdesktoppane;
        this.setVisible(true);
    }

    public static JMenu getEditMenu() {
        return Editmenu;
    }

    public JMenuBar buildMenu() {
        menuBar = new JMenuBar();

        // security determines which menu items are added
        User u = ITS.getUser();
        IndraLicense license = u.getLicense();

        Filemenu = new JMenu("File");
        Editmenu = new JMenu("Edit");
        Searchmenu = new JMenu("Search");
        Ingestmenu = new JMenu("Ingest");
        Exportmenu = new JMenu("Export");
        Reportmenu = new JMenu("Report");
        Helpmenu = new JMenu("Help");
        selectResource = new JMenu("Resources");
        classifiers = new JMenu("Classifiers");

        Filemenu.setFont(new Font("SansSerif", Font.BOLD, 12));
        Editmenu.setFont(new Font("SansSerif", Font.BOLD, 12));
        Searchmenu.setFont(new Font("SansSerif", Font.BOLD, 12));
        Ingestmenu.setFont(new Font("SansSerif", Font.BOLD, 12));
        Exportmenu.setFont(new Font("SansSerif", Font.BOLD, 12));
        Reportmenu.setFont(new Font("SansSerif", Font.BOLD, 12));
        Helpmenu.setFont(new Font("SansSerif", Font.BOLD, 12));
        selectResource.setFont(new Font("SansSerif", Font.BOLD, 12));
        classifiers.setFont(new Font("SansSerif", Font.BOLD, 12));

        TaxSubMenu = new JMenuItem("Taxonomy");
        ThsSubMenu = new JMenuItem("Thesaurus");
        FolSubMenu = new JMenuItem("Folder");
        SASubMenu = new JMenuItem("Subject Area");
        AlSubMenu = new JMenuItem("Manage Alerts");
        classMenuItem = new JMenuItem("Classify Document");
        importDictionaryMenu = new JMenuItem("Import Dictionary");
        conceptMenuItem = new JMenu("Narrative Reports");
        exitMenuItem = new JMenuItem("Exit");
        nodeMenuItem = new JMenuItem("Topics");
        docMenuItem = new JMenuItem("Documents");
        adTopMenuItem = new JMenuItem("Advanced Topics");
        adDocMenuItem = new JMenuItem("Advanced Documents");
        admonClassifiers = new JMenuItem("Edit");

        // dynamically create the drop down menu for narrative types
        for (int i = 0; i < narrativeTypes.size(); i++) {
            HashTree ht = (HashTree) narrativeTypes.elementAt(i);
            String narrName = (String) ht.get("NARRATIVETYPENAME");
            String narrID = (String) ht.get("NARRATIVETYPEID");

            NarrMenuItem narr = new NarrMenuItem(narrID, narrName);
            conceptMenuItem.add(narr);
            narr.addActionListener(this);
        }

        cutMenuItem = new JMenuItem("Cut");
        copyMenuItem = new JMenuItem("Copy");
        pasteMenuItem = new JMenuItem("Paste");
        prefMenuItem = new JMenuItem("Edit Preferences");

        importMenuItem = new JMenuItem("Topic Map");
        filesystemMenuItem = new JMenuItem("Filesystem");
        xmlMenuItem = new JMenuItem("XML File (xDoc Converter)");
        xmlOReillyMenuItem = new JMenuItem("XML File (OReilly)");
        importOxfordMenuItem = new JMenuItem("XML File (Oxford Dictionary)");
        importGaleMenuItem = new JMenuItem("XML File (Gale)");
        cnfMenuItem = new JMenuItem("Corpus Normal Form");
        theMenuItem = new JMenuItem("ISO Thesaurus");
        csvImportMenuItem = new JMenuItem("Wiki Import");
        createCsvWiki = new JMenuItem("Create CSV from Wikipedia");
        csvImportGAAPMenuItem = new JMenuItem("Taxonomy from GAAP CSV File");
        thesFromCsvMenuItem = new JMenuItem("Thesaurus from CSV File");
        pipesvImportMenuItem = new JMenuItem("Pipe-Delimited File (1 level)");
        pipesv2ImportMenuItem = new JMenuItem("Pipe-Delimited File (2 level)");
        translatemenu = new JMenu("Translated Topic Metadata");
        transFR = new JMenuItem("French");
        transES = new JMenuItem("Spanish");
        transAR = new JMenuItem("Arabic");
        transFS = new JMenuItem("Farsi");
        transCN = new JMenuItem("Chinese");
        transRU = new JMenuItem("Russian");
        transKR = new JMenuItem("Korean");
        transDE = new JMenuItem("German");
        transSW = new JMenuItem("Swedish");
        transDU = new JMenuItem("Dutch");
        transIT = new JMenuItem("Italian");
        transJP = new JMenuItem("Japanese");
        transPG = new JMenuItem("Portuguese");

        translatemenu.add(transAR);
        transAR.addActionListener(this);
        translatemenu.add(transCN);
        transCN.addActionListener(this);
        translatemenu.add(transDU);
        transDU.addActionListener(this);
        translatemenu.add(transFS);
        transFS.addActionListener(this);
        translatemenu.add(transFR);
        transFR.addActionListener(this);
        translatemenu.add(transDE);
        transDE.addActionListener(this);
        translatemenu.add(transIT);
        transIT.addActionListener(this);
        translatemenu.add(transJP);
        transJP.addActionListener(this);
        translatemenu.add(transKR);
        transKR.addActionListener(this);
        translatemenu.add(transPG);
        transPG.addActionListener(this);
        translatemenu.add(transRU);
        transRU.addActionListener(this);
        translatemenu.add(transES);
        transES.addActionListener(this);
        translatemenu.add(transSW);
        transSW.addActionListener(this);

        xtmMenuItem = new JMenuItem("XTM Topic Map");
        owlMenuItem = new JMenuItem("OWL");
        inxightMenuItem = new JMenuItem("Inxight (terms)");
        inxightRMenuItem = new JMenuItem("Inxight (rules)");
        inxightSICMenuItem = new JMenuItem("Inxight (SIC)");
        inxight5MenuItem = new JMenuItem("Inxight (version 5 - loose)");
        inxight5ANDMenuItem = new JMenuItem("Inxight (version 5 - strict)");
        inxightNameCatalogMenuItem = new JMenuItem("Inxight Name Catalog");
        automMenuItem = new JMenuItem("Autonomy");
        SICTitleMenuItem = new JMenuItem("Titles Out (SIC)");
        // endecaMenuItem = new JMenuItem("Endeca");
        // endecaNoSynMenuItem = new JMenuItem("Endeca (No Synonyms)");
        endecaNameCatalog = new JMenuItem("Endeca Name Catalog (Dimensions)");
        endecaNameCatalogSigsAsSynonyms = new JMenuItem(
                "Endeca Name Catalog (Sigs as Synonyms)");
        endecaThesaurusFromMustHaves = new JMenuItem(
                "Endeca Thesaurus (from Must Haves)");
        verityMenuItem = new JMenuItem("Verity OTL");
        verityCengageMenuItem = new JMenuItem("Verity OTL (Cengage)");
        ultraseekMenuItem = new JMenuItem("Verity Ultraseek");
        tefMenuItem = new JMenuItem("Tef");
        tefNoCommasNoParensMenuItem = new JMenuItem(
                "Tef (No Commas, No Parens)");
        FlatMenuItem = new JMenuItem("Flattened List");
        FlatMenuItem2 = new JMenuItem("Flattened List with (Parent)");
        CommaMenuItem = new JMenuItem("Comma Delimeted (Topics Only)");
        SurfMenuItem = new JMenuItem("SurfWax LookAhead");
        FltMenuItem = new JMenuItem("Title Filter");
        FltSigMenuItem = new JMenuItem("Title and Sig Filter");
        KofaxOutMenuItem = new JMenuItem("Kofax Mohomine");
        MarkLogicMenuItem = new JMenuItem("Mark Logic");
        TranslateMenuItem = new JMenuItem("Export for Translation");

        checkMenuItem = new JMenuItem("Thesaurus Report");
        refreshMenuItem = new JMenuItem("Topic Refresh Report");
        nodeReportMenuItem = new JMenuItem("Taxonomy Document Report");
        childReportMenuItem = new JMenuItem("Topic Children Report");
        sigReportMenuItem = new JMenuItem("Topic Signature Report");
        qaReportMenuItem = new JMenuItem("QA Report");
        wikiReportMenuItem = new JMenuItem("Wikipedia Report");
        qaReportGenerateMenuItem = new JMenuItem("QA Report generate");
        iamgesProcessMenuItem = new JMenuItem("Images process, status");
        musthaveProcessMenuItem = new JMenuItem("Musthave process, status");

        helpMenuItem = new JMenuItem("Help");
        aboutMenuItem = new JMenuItem("About");
        versionMenuItem = new JMenuItem("Version");

        managerResource = new JMenuItem("Resource Manager");
        splitArchives = new JMenuItem("Split Archives");

        // importLocalMenuItem = new JMenuItem("Local");

        TaxSubMenu.addActionListener(this);
        ThsSubMenu.addActionListener(this);
        FolSubMenu.addActionListener(this);
        SASubMenu.addActionListener(this);
        AlSubMenu.addActionListener(this);
        classMenuItem.addActionListener(this);
        importDictionaryMenu.addActionListener(this);
        conceptMenuItem.addActionListener(this);
        checkMenuItem.addActionListener(this);
        nodeReportMenuItem.addActionListener(this);
        childReportMenuItem.addActionListener(this);
        sigReportMenuItem.addActionListener(this);
        qaReportMenuItem.addActionListener(this);
        wikiReportMenuItem.addActionListener(this);
        qaReportGenerateMenuItem.addActionListener(this);
        iamgesProcessMenuItem.addActionListener(this);
        musthaveProcessMenuItem.addActionListener(this);

        refreshMenuItem.addActionListener(this);
        nodeMenuItem.addActionListener(this);
        docMenuItem.addActionListener(this);
        adTopMenuItem.addActionListener(this);
        adDocMenuItem.addActionListener(this);
        exitMenuItem.addActionListener(this);
        xtmMenuItem.addActionListener(this);
        // owlMenuItem.addActionListener(this);
        inxightMenuItem.addActionListener(this);
        inxightRMenuItem.addActionListener(this);
        inxightSICMenuItem.addActionListener(this);
        inxight5MenuItem.addActionListener(this);
        inxight5ANDMenuItem.addActionListener(this);
        inxightNameCatalogMenuItem.addActionListener(this);
        automMenuItem.addActionListener(this);
        SICTitleMenuItem.addActionListener(this);
        // endecaMenuItem.addActionListener(this);
        // /endecaNoSynMenuItem.addActionListener(this);
        endecaNameCatalog.addActionListener(this);
        endecaNameCatalogSigsAsSynonyms.addActionListener(this);
        endecaThesaurusFromMustHaves.addActionListener(this);
        verityMenuItem.addActionListener(this);
        verityCengageMenuItem.addActionListener(this);
        ultraseekMenuItem.addActionListener(this);
        tefMenuItem.addActionListener(this);
        tefNoCommasNoParensMenuItem.addActionListener(this);
        FlatMenuItem.addActionListener(this);
        FlatMenuItem2.addActionListener(this);
        CommaMenuItem.addActionListener(this);
        SurfMenuItem.addActionListener(this);
        FltMenuItem.addActionListener(this);
        FltSigMenuItem.addActionListener(this);
        KofaxOutMenuItem.addActionListener(this);
        MarkLogicMenuItem.addActionListener(this);
        TranslateMenuItem.addActionListener(this);

        Filemenu.add(TaxSubMenu);
        Filemenu.add(ThsSubMenu);
        if (!license.isAuthorized(IndraLicense.THESAURUS)) {
            ThsSubMenu.setEnabled(false);
        }
        Filemenu.add(FolSubMenu);
        Filemenu.add(SASubMenu);
        Filemenu.add(AlSubMenu);
        Filemenu.add(importDictionaryMenu);
        Filemenu.add(classMenuItem);
        Filemenu.add(conceptMenuItem);
        if ((!license.isAuthorized(IndraLicense.NARRATIVE))
                || (narrativeTypes.size() == 0)) {
            conceptMenuItem.setEnabled(false);
        }
        Filemenu.addSeparator();
        Filemenu.add(exitMenuItem);

        Editmenu.add(cutMenuItem);
        Editmenu.add(copyMenuItem);
        Editmenu.add(pasteMenuItem);
        Editmenu.addSeparator();
        Editmenu.add(prefMenuItem);

        copyMenuItem
                .addActionListener(new javax.swing.text.DefaultEditorKit.CopyAction());
        cutMenuItem
                .addActionListener(new javax.swing.text.DefaultEditorKit.CutAction());
        pasteMenuItem
                .addActionListener(new javax.swing.text.DefaultEditorKit.PasteAction());
        prefMenuItem.addActionListener(this);

        Searchmenu.add(nodeMenuItem);
        Searchmenu.add(docMenuItem);
        Searchmenu.add(adTopMenuItem);
        Searchmenu.add(adDocMenuItem);

        Ingestmenu.add(importMenuItem);
        Ingestmenu.add(filesystemMenuItem);
        Ingestmenu.add(xmlMenuItem);
        Ingestmenu.add(xmlOReillyMenuItem);
        Ingestmenu.add(importOxfordMenuItem);
        Ingestmenu.add(importGaleMenuItem);
        Ingestmenu.add(cnfMenuItem);
        Ingestmenu.add(theMenuItem);
        Ingestmenu.add(csvImportMenuItem);
        Ingestmenu.add(createCsvWiki);
        Ingestmenu.add(csvImportGAAPMenuItem);
        Ingestmenu.add(thesFromCsvMenuItem);
        Ingestmenu.add(pipesvImportMenuItem);
        Ingestmenu.add(pipesv2ImportMenuItem);
        Ingestmenu.add(translatemenu);
        if (!license.isAuthorized(IndraLicense.INGESTION)) {
            filesystemMenuItem.setEnabled(false);
            xmlMenuItem.setEnabled(false);
            xmlOReillyMenuItem.setEnabled(false);
            importOxfordMenuItem.setEnabled(false);
            importGaleMenuItem.setEnabled(false);
            cnfMenuItem.setEnabled(false);
            theMenuItem.setEnabled(false);
            csvImportMenuItem.setEnabled(false);
            createCsvWiki.setEnabled(false);
            csvImportGAAPMenuItem.setEnabled(false);
            thesFromCsvMenuItem.setEnabled(false);
            pipesvImportMenuItem.setEnabled(false);
            pipesv2ImportMenuItem.setEnabled(false);
            translatemenu.setEnabled(false);
        }

        importMenuItem.addActionListener(this);
        filesystemMenuItem.addActionListener(this);
        xmlMenuItem.addActionListener(this);
        xmlOReillyMenuItem.addActionListener(this);
        importOxfordMenuItem.addActionListener(this);
        importGaleMenuItem.addActionListener(this);
        cnfMenuItem.addActionListener(this);
        theMenuItem.addActionListener(this);
        csvImportMenuItem.addActionListener(this);
        createCsvWiki.addActionListener(this);
        csvImportGAAPMenuItem.addActionListener(this);
        thesFromCsvMenuItem.addActionListener(this);
        pipesvImportMenuItem.addActionListener(this);
        pipesv2ImportMenuItem.addActionListener(this);

        Exportmenu.add(xtmMenuItem);
        // Exportmenu.add(owlMenuItem);
        Exportmenu.add(inxightMenuItem);
        Exportmenu.add(inxight5MenuItem);
        Exportmenu.add(inxight5ANDMenuItem);
        Exportmenu.add(inxightNameCatalogMenuItem);
        Exportmenu.add(inxightRMenuItem);
        Exportmenu.add(inxightSICMenuItem);
        Exportmenu.add(automMenuItem);
        Exportmenu.add(SICTitleMenuItem);
        // Exportmenu.add(endecaMenuItem);
        // Exportmenu.add(endecaNoSynMenuItem);
        Exportmenu.add(endecaNameCatalog);
        Exportmenu.add(endecaNameCatalogSigsAsSynonyms);
        Exportmenu.add(endecaThesaurusFromMustHaves);
        Exportmenu.add(verityMenuItem);
        Exportmenu.add(verityCengageMenuItem);
        Exportmenu.add(ultraseekMenuItem);
        Exportmenu.add(tefMenuItem);
        Exportmenu.add(tefNoCommasNoParensMenuItem);
        Exportmenu.add(FlatMenuItem);
        Exportmenu.add(FlatMenuItem2);
        Exportmenu.add(CommaMenuItem);
        Exportmenu.add(SurfMenuItem);
        Exportmenu.add(FltMenuItem);
        Exportmenu.add(FltSigMenuItem);
        Exportmenu.add(KofaxOutMenuItem);
        Exportmenu.add(MarkLogicMenuItem);
        Exportmenu.add(TranslateMenuItem);

        Reportmenu.add(checkMenuItem);
        Reportmenu.add(nodeReportMenuItem);
        Reportmenu.add(childReportMenuItem);
        Reportmenu.add(sigReportMenuItem);
        Reportmenu.add(qaReportMenuItem);
        Reportmenu.add(wikiReportMenuItem);
        Reportmenu.add(qaReportGenerateMenuItem);
        Reportmenu.add(iamgesProcessMenuItem);
        Reportmenu.add(musthaveProcessMenuItem);
        // Reportmenu.add(refreshMenuItem);

        Helpmenu.add(helpMenuItem);
        // Helpmenu.add(aboutMenuItem);
        Helpmenu.add(versionMenuItem);

        selectResource.add(managerResource);
        selectResource.add(splitArchives);
        // importResourceMenu.add(importLocalMenuItem);

        managerResource.addActionListener(this);
        splitArchives.addActionListener(this);

        classifiers.add(admonClassifiers);
        admonClassifiers.addActionListener(this);
        // importLocalMenuItem.addActionListener(this);

        aboutMenuItem.addActionListener(this);
        versionMenuItem.addActionListener(this);

        menuBar.add(Filemenu);
        menuBar.add(Editmenu);
        menuBar.add(Searchmenu);
        menuBar.add(Ingestmenu);
        System.out.println("Authorized?");
        if (license.isAuthorized(IndraLicense.EXPORT)) {
            menuBar.add(Exportmenu);
        }
        menuBar.add(Reportmenu);
        menuBar.add(selectResource);
        menuBar.add(classifiers);
        menuBar.add(Helpmenu);

        HelpSet hs = null;
        try {
            ClassLoader cl = ITSMenu.class.getClassLoader();
            URL hsURL = HelpSet.findHelpSet(cl, hsName);
            hs = new HelpSet(null, hsURL);
        } catch (Exception ee) {
            System.out.println("HelpSet " + hsName + " not found");
            return menuBar;
        }
        HelpBroker hb = hs.createHelpBroker();
        helpMenuItem.addActionListener(new CSH.DisplayHelpFromSource(hb));

        return menuBar;
    }

    public JToolBar buildToolbar() {
        ImageIcon icoTax = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/taxonomy.gif");
        ImageIcon icoThe = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/thesaurus.gif");
        ImageIcon icoImp = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/search.gif");
        ImageIcon icoExp = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/explain.gif");
        ImageIcon icoTxI = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/import.gif");
        ImageIcon icoCla = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/classify.gif");
        ImageIcon icoHlp = com.iw.tools.ImageUtils.getImage(this,
                "itsimages/help.gif");

        taxButton = new JButton(new toolbarAction(icoTax, "Manage Taxonomy",
                new Integer(KeyEvent.VK_1)));
        theButton = new JButton(new toolbarAction(icoThe, "Manage Thesaurus",
                new Integer(KeyEvent.VK_2)));
        impButton = new JButton(new toolbarAction(icoImp, "Topic Search",
                new Integer(KeyEvent.VK_3)));
        expButton = new JButton(new toolbarAction(icoExp, "Document Search",
                new Integer(KeyEvent.VK_4)));
        txiButton = new JButton(new toolbarAction(icoTxI, "Import Topic Map",
                new Integer(KeyEvent.VK_5)));
        claButton = new JButton(new toolbarAction(icoCla, "Classify Document",
                new Integer(KeyEvent.VK_6)));
        hlpButton = new JButton(new toolbarAction(icoHlp, "Help", new Integer(
                KeyEvent.VK_F1)));

        JToolBar jtb = new JToolBar();

        taxButton.setMaximumSize(new Dimension(30, 30));
        taxButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(taxButton);
        theButton.setMaximumSize(new Dimension(30, 30));
        theButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(theButton);
        impButton.setMaximumSize(new Dimension(30, 30));
        impButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(impButton);
        expButton.setMaximumSize(new Dimension(30, 30));
        expButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(expButton);
        txiButton.setMaximumSize(new Dimension(30, 30));
        txiButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(txiButton);
        claButton.setMaximumSize(new Dimension(30, 30));
        claButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(claButton);
        hlpButton.setMaximumSize(new Dimension(30, 30));
        hlpButton.setMinimumSize(new Dimension(30, 30));
        jtb.add(hlpButton);

        jtb.setOpaque(true);

        HelpSet hs = null;
        try {
            ClassLoader cl = ITSMenu.class.getClassLoader();
            URL hsURL = HelpSet.findHelpSet(cl, hsName);
            hs = new HelpSet(null, hsURL);
        } catch (Exception ee) {
            System.out.println("HelpSet " + hsName + " not found");
            return jtb;
        }
        HelpBroker hb = hs.createHelpBroker();
        hlpButton.addActionListener(new CSH.DisplayHelpFromSource(hb));

        return jtb;
    }

    public class toolbarAction extends AbstractAction {

        private static final long serialVersionUID = 2310938802487832320L;

        public toolbarAction(ImageIcon icon, String desc, Integer mnemonic) {
            super(null, icon);
            putValue(SHORT_DESCRIPTION, desc);
            putValue(MNEMONIC_KEY, mnemonic);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(taxButton)) {
                try {
                    OpenTaxonomy taxBox = new OpenTaxonomy(itsAdminFrame);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (e.getSource().equals(theButton)) {
                try {
                    OpenThesaurus thesBox = new OpenThesaurus(itsAdminFrame);
                    thesBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (e.getSource().equals(expButton)) {
                try {
                    DocumentSearch ds = new DocumentSearch(itsAdminFrame);
                    ds.setVisible(true);
                } catch (Exception e2) {
                    e2.printStackTrace(System.out);
                }
            } else if (e.getSource().equals(impButton)) {
                try {
                    TopicSearch ts = new TopicSearch(itsAdminFrame);
                    ts.setVisible(true);
                } catch (Exception e2) {
                    e2.printStackTrace(System.out);
                }

            } else if (e.getSource().equals(claButton)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(true);
                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File[] sf = chooser.getSelectedFiles();

                    // classify the document
                    SwingWorker aWorker = new SwingWorker(e, sf[0]) {
                        protected void doNonUILogic() {
                            try {
                                itsAdminFrame
                                        .setCursor(Cursor
                                        .getPredefinedCursor(Cursor.WAIT_CURSOR));
                                itsAdminFrame.getGlassPane().setVisible(true);
                                NodeDocView ndv = new NodeDocView(
                                        itsAdminFrame, f);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            } else if (e.getSource().equals(txiButton)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(true);
                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File[] sf = chooser.getSelectedFiles();

                    for (int i = 0; i < sf.length; i++) {
                        try {
                            System.out.println("open import frame for: "
                                    + sf[i].getName());
                            TopicMapImport.importTopicMap(sf[i], itsAdminFrame);
                        } catch (SAXParseException ex) {
                            ex.printStackTrace(System.out);
                            JOptionPane.showMessageDialog(
                                    itsAdminFrame,
                                    "Could not load topic map: "
                                    + sf[i].getName() + " "
                                    + ex.getMessage() + ".", "Error",
                                    JOptionPane.NO_OPTION);
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                            JOptionPane.showMessageDialog(
                                    itsAdminFrame,
                                    "Could not load topic map: "
                                    + sf[i].getName() + " "
                                    + ex.getMessage() + ".", "Error",
                                    JOptionPane.NO_OPTION);
                        }
                    }
                }
            }
        }
    }

    public void importTranslation(final String lang, ActionEvent event)
            throws Exception {
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);

        int option = chooser.showOpenDialog(itsAdminFrame);
        if (option == JFileChooser.APPROVE_OPTION) {
            File sf = chooser.getSelectedFile();

            System.out.println("Reading file " + sf.getAbsolutePath());

            SwingWorker aWorker = new SwingWorker(event, sf) {
                protected void doNonUILogic() {
                    PopupProgressBar ppb = new PopupProgressBar(itsAdminFrame,
                            0, 100);
                    ppb.show();

                    System.out.println("Reading file (2): "
                            + f.getAbsolutePath());

                    try {
                        com.iw.tools.IngestionUtils.importTranslatedMetadata(
                                itsAdminFrame.its, ppb, f, lang);
                    } catch (Exception e) {
                        e.printStackTrace(System.err);
                    }

                    ppb.dispose();

                    JOptionPane.showMessageDialog(itsAdminFrame,
                            "Translated metadata ingested successfully.",
                            "Information", JOptionPane.NO_OPTION);
                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    itsAdminFrame.setCursor(Cursor
                            .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    itsAdminFrame.getGlassPane().setVisible(false);
                }
            };
            aWorker.start();
        }
    }

    public void actionPerformed(ActionEvent event) {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        try {
            // would prefer to do this with a switch statement but, alas, no
            // switch for non-integers in JAVA
            if (event.getSource().equals(exitMenuItem)) {
                System.exit(0);
            } else if (event.getSource().equals(ThsSubMenu)) {
                try {
                    OpenThesaurus thesBox = new OpenThesaurus(itsAdminFrame);
                    thesBox.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(FolSubMenu)) {
                try {
                    FolderManipulationFrame ff = new FolderManipulationFrame(
                            itsAdminFrame);
                    ff.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(SASubMenu)) {
                try {
                    SubjectAreaManipulationFrame ff = new SubjectAreaManipulationFrame(
                            itsAdminFrame);
                    ff.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(AlSubMenu)) {
                try {
                    ManageAlerts ma = new ManageAlerts(itsAdminFrame);
                    ma.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(TaxSubMenu)) {
                try {
                    OpenTaxonomy taxBox = new OpenTaxonomy(itsAdminFrame);
                    taxBox.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }

                // TRANSLATED TAXONOMY METADATA INGESTION
            } else if (event.getSource().equals(transFR)) {
                importTranslation("FR", event);
            } else if (event.getSource().equals(transES)) {
                importTranslation("ES", event);
            } else if (event.getSource().equals(transSW)) {
                importTranslation("SW", event);
            } else if (event.getSource().equals(transDU)) {
                importTranslation("DU", event);
            } else if (event.getSource().equals(transIT)) {
                importTranslation("IT", event);
            } else if (event.getSource().equals(transJP)) {
                importTranslation("JP", event);
            } else if (event.getSource().equals(transPG)) {
                importTranslation("PG", event);
            } else if (event.getSource().equals(transAR)) {
                importTranslation("AR", event);
            } else if (event.getSource().equals(transFS)) {
                importTranslation("FS", event);
            } else if (event.getSource().equals(transCN)) {
                importTranslation("CN", event);
            } else if (event.getSource().equals(transRU)) {
                importTranslation("RU", event);
            } else if (event.getSource().equals(transKR)) {
                importTranslation("KR", event);
            } else if (event.getSource().equals(transDE)) {
                importTranslation("DE", event);

                // END TRANSLATED TAXONOMY METADATA INGESTION
            } else if (event.getSource().equals(importMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(true);
                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File[] sf = chooser.getSelectedFiles();
                    final File[] farr = sf;

                    SwingWorker aWorker = new SwingWorker(event) {
                        protected void doNonUILogic() {
                            for (int i = 0; i < farr.length; i++) {
                                try {
                                    TopicMapImport.importTopicMap(farr[i],
                                            itsAdminFrame);
                                } catch (SAXParseException ex) {
                                    ex.printStackTrace(System.out);
                                    JOptionPane.showMessageDialog(
                                            itsAdminFrame,
                                            "Could not load topic map: "
                                            + farr[i].getName() + " "
                                            + ex.getMessage() + ".",
                                            "Error", JOptionPane.NO_OPTION);

                                } catch (Exception ex) {
                                    ex.printStackTrace(System.out);
                                    JOptionPane.showMessageDialog(
                                            itsAdminFrame,
                                            "Could not load topic map: "
                                            + farr[i].getName() + " "
                                            + ex.getMessage() + ".",
                                            "Error", JOptionPane.NO_OPTION);
                                }
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            } else if (event.getSource().equals(filesystemMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File sf = chooser.getSelectedFile();

                    SwingWorker aWorker = new SwingWorker(event, sf) {
                        protected void doNonUILogic() {
                            itsAdminFrame.jdp.setCursor(Cursor
                                    .getPredefinedCursor(Cursor.WAIT_CURSOR));

                            if (!f.isDirectory()) {
                                JOptionPane.showMessageDialog(itsAdminFrame,
                                        "You must select a folder to ingest.",
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            }

                            try {
                                Corpus c = com.iw.tools.IngestionUtils
                                        .createCorpusFromFilesystem(f,
                                        itsAdminFrame.its);

                                JOptionPane.showMessageDialog(itsAdminFrame,
                                        "Taxonomy created successfully.",
                                        "Information", JOptionPane.NO_OPTION);

                                CorpusManagement DragAndDrop = new CorpusManagement(
                                        itsAdminFrame, c);

                                DragAndDrop.setBounds(50, 50, 850, 550);
                                DragAndDrop.setVisible(true);
                                DragAndDrop
                                        .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                                itsAdminFrame.jdp.add(DragAndDrop);
                                DragAndDrop.moveToFront();

                                itsAdminFrame.jdp.setVisible(true);

                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                                JOptionPane.showMessageDialog(
                                        itsAdminFrame,
                                        "Fatal error: "
                                        + e.getLocalizedMessage(),
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame.jdp
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            } else if (event.getSource().equals(xtmMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new XtmOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(owlMenuItem)) {
                JOptionPane.showMessageDialog(itsAdminFrame,
                        "This feature is still under construction.",
                        "Information", JOptionPane.NO_OPTION);
            } else if (event.getSource().equals(inxightMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new InxightOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(inxightRMenuItem)) {
                try {
                    InxightOut inx = new InxightOut();
                    inx.wordName = "rule";
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, inx);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(inxight5MenuItem)) {
                try {
                    Inxight5Out inx = new Inxight5Out();
                    inx.wordName = "terms";
                    inx.filtertype = "OR";
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, inx);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(inxight5ANDMenuItem)) {
                try {
                    Inxight5Out inx = new Inxight5Out();
                    inx.wordName = "terms";
                    inx.filtertype = "AND";
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, inx);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(inxightNameCatalogMenuItem)) {
                try {
                    InxightNameCatalogOut inx = new InxightNameCatalogOut();
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, inx);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(inxightSICMenuItem)) {
                try {
                    InxightOutConteg inx = new InxightOutConteg();
                    inx.wordName = "terms";
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, inx);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(automMenuItem)) {
                try {
                    AutonomyOut auto = new AutonomyOut();
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, auto);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(SICTitleMenuItem)) {
                try {
                    SicTitleMatchOut inx = new SicTitleMatchOut();
                    inx.wordName = "terms";
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, inx);
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
                // } else if (event.getSource().equals(endecaMenuItem)) {
                // try {
                // ExportCorpusSelect taxBox = new
                // ExportCorpusSelect(itsAdminFrame, new EndecaOut());
                // taxBox.setVisible(true);
                // } catch (Exception ex) {
                // ex.printStackTrace(System.out);
                // }
                // } else if (event.getSource().equals(endecaNoSynMenuItem)) {
                // try {
                // ExportCorpusSelect taxBox = new
                // ExportCorpusSelect(itsAdminFrame, new EndecaNoSynOut());
                // taxBox.setVisible(true);
                // } catch (Exception ex) {
                // ex.printStackTrace(System.out);
                // }
            } else if (event.getSource().equals(endecaNameCatalog)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new EndecaOutNameCat());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource()
                    .equals(endecaNameCatalogSigsAsSynonyms)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new EndecaOutNameCatFromSigs());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(endecaThesaurusFromMustHaves)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new EndecaOutThesaurus());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(verityMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new VerityOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(ultraseekMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new UltraseekOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(tefMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new TefOut(false));
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(tefNoCommasNoParensMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new TefOut(true));
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(FlatMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new FlattenedTaxonomyOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(FlatMenuItem2)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new FlatTaxonomyParent());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(CommaMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new CommaDelimOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(SurfMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new SurfWaxOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(FltMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new FilterTitleOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(FltSigMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new FilterTitleSigOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(verityCengageMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new OTLOut());

                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(MarkLogicMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new MarkLogicOut());

                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(TranslateMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new TranslateCSVOut());

                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(KofaxOutMenuItem)) {
                try {
                    ExportCorpusSelect taxBox = new ExportCorpusSelect(
                            itsAdminFrame, new KofaxOut());
                    taxBox.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(theMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(true);
                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File[] sf = chooser.getSelectedFiles();

                    SwingWorker aWorker = new SwingWorker(event, sf[0]) {
                        protected void doNonUILogic() {
                            try {
                                itsAdminFrame
                                        .setCursor(Cursor
                                        .getPredefinedCursor(Cursor.WAIT_CURSOR));
                                itsAdminFrame.getGlassPane().setVisible(true);
                                Thesaurus t = IngestThesaurus.ingestThesaurus(
                                        f, itsAdminFrame);
                                itsAdminFrame.openThesaurus(itsAdminFrame, t);

                            } catch (IOException ioe) {
                                JOptionPane.showMessageDialog(
                                        itsAdminFrame,
                                        "Error parsing file: "
                                        + f.getAbsolutePath(), "Error",
                                        JOptionPane.NO_OPTION);
                                ioe.printStackTrace(System.err);
                            } catch (Exception e) {
                                JOptionPane
                                        .showMessageDialog(
                                        itsAdminFrame,
                                        "An error was encountered while processing your request.",
                                        "Error", JOptionPane.NO_OPTION);
                                e.printStackTrace(System.err);
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();

                } else {
                }

            } else if (event.getSource().equals(cnfMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File sf = chooser.getSelectedFile();

                    SwingWorker aWorker = new SwingWorker(event, sf) {
                        protected void doNonUILogic() {
                            itsAdminFrame.jdp.setCursor(Cursor
                                    .getPredefinedCursor(Cursor.WAIT_CURSOR));

                            if (!f.isDirectory()) {
                                JOptionPane
                                        .showMessageDialog(
                                        itsAdminFrame,
                                        "You must select a CNF folder to ingest.",
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            }

                            try {
                                Corpus c = com.iw.tools.IngestionUtils
                                        .createCorpusFromCNF(f,
                                        itsAdminFrame.its);

                                JOptionPane
                                        .showMessageDialog(
                                        itsAdminFrame,
                                        "Taxonomy created from CNF successfully.",
                                        "Information",
                                        JOptionPane.NO_OPTION);

                                CorpusManagement DragAndDrop = new CorpusManagement(
                                        itsAdminFrame, c);

                                DragAndDrop.setBounds(50, 50, 850, 550);
                                DragAndDrop.setVisible(true);
                                DragAndDrop
                                        .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                                itsAdminFrame.jdp.add(DragAndDrop);
                                DragAndDrop.moveToFront();

                                itsAdminFrame.jdp.setVisible(true);

                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                                JOptionPane.showMessageDialog(
                                        itsAdminFrame,
                                        "Fatal error: "
                                        + e.getLocalizedMessage(),
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame.jdp
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            } else if (event.getSource().equals(thesFromCsvMenuItem)) {
                SwingWorker aWorker = new SwingWorker(event) {
                    protected void doNonUILogic() {
                        itsAdminFrame.setCursor(Cursor
                                .getPredefinedCursor(Cursor.WAIT_CURSOR));
                        itsAdminFrame.getGlassPane().setVisible(true);

                        String sThesName = JOptionPane
                                .showInputDialog("Please enter the name of this new thesaurus.");
                        if (sThesName == null) {
                            return;
                        }

                        JFileChooser chooser = new JFileChooser();
                        chooser.setMultiSelectionEnabled(false);

                        int option = chooser.showOpenDialog(itsAdminFrame);
                        if (option == JFileChooser.APPROVE_OPTION) {
                            File sf = chooser.getSelectedFile();

                            try {
                                com.iw.tools.IngestionUtils
                                        .createThesaurusFromCSV(sf, sThesName,
                                        itsAdminFrame.its);
                            } catch (FileNotFoundException fnf) {
                                JOptionPane.showMessageDialog(itsAdminFrame,
                                        "Thesaurus file cannot be found.",
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            } catch (UnsupportedEncodingException uee) {
                                JOptionPane
                                        .showMessageDialog(
                                        itsAdminFrame,
                                        "An unexpecting encoding error was found in the CSV file that was provided.",
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            } catch (Exception e) {
                                JOptionPane
                                        .showMessageDialog(
                                        itsAdminFrame,
                                        "An unexpected error was encountered, please see the log file for more details.",
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            }

                            JOptionPane
                                    .showMessageDialog(
                                    itsAdminFrame,
                                    "Thesaurus created from CSV file successfully.",
                                    "Information",
                                    JOptionPane.NO_OPTION);
                        }
                    }

                    protected void doUIUpdateLogic() {
                    }

                    public void finished() {
                        super.finished();
                        itsAdminFrame.setCursor(Cursor
                                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        itsAdminFrame.getGlassPane().setVisible(false);
                    }
                };
                aWorker.start();


            } else if (event.getSource().equals(csvImportMenuItem)) {
                try {
                    WikiImport wikiImport = new WikiImport(itsAdminFrame);
                    wikiImport.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }

            } else if (event.getSource().equals(createCsvWiki)) {
                CreateCsvWikiUI ccw = new CreateCsvWikiUI();
                ccw.showWin(itsAdminFrame);

            } else if (event.getSource().equals(csvImportGAAPMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);

                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File sf = chooser.getSelectedFile();

                    Corpus c = com.iw.tools.IngestionUtils
                            .createCorpusFromGAAPCSV(sf, itsAdminFrame.its);

                    JOptionPane
                            .showMessageDialog(
                            itsAdminFrame,
                            "Taxonomy created from GAAP CSV file successfully.",
                            "Information", JOptionPane.NO_OPTION);

                    CorpusManagement DragAndDrop = new CorpusManagement(
                            itsAdminFrame, c);

                    DragAndDrop.setBounds(50, 50, 850, 550);
                    DragAndDrop.setVisible(true);
                    DragAndDrop
                            .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                    itsAdminFrame.jdp.add(DragAndDrop);
                    DragAndDrop.moveToFront();

                    itsAdminFrame.jdp.setVisible(true);
                }
            } else if (event.getSource().equals(pipesvImportMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);

                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File sf = chooser.getSelectedFile();

                    Corpus c = com.iw.tools.IngestionUtils
                            .createCorpusFromPipeSV(sf, itsAdminFrame.its);

                    JOptionPane
                            .showMessageDialog(
                            itsAdminFrame,
                            "Taxonomy created from pipe-delimited file successfully.",
                            "Information", JOptionPane.NO_OPTION);

                    CorpusManagement DragAndDrop = new CorpusManagement(
                            itsAdminFrame, c);

                    DragAndDrop.setBounds(50, 50, 850, 550);
                    DragAndDrop.setVisible(true);
                    DragAndDrop
                            .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                    itsAdminFrame.jdp.add(DragAndDrop);
                    DragAndDrop.moveToFront();

                    itsAdminFrame.jdp.setVisible(true);
                }
            } else if (event.getSource().equals(pipesv2ImportMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);

                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File sf = chooser.getSelectedFile();

                    Corpus c = com.iw.tools.IngestionUtils
                            .createCorpusFromPipeSV2(sf, itsAdminFrame.its);

                    JOptionPane
                            .showMessageDialog(
                            itsAdminFrame,
                            "Taxonomy created from pipe-delimited file successfully.",
                            "Information", JOptionPane.NO_OPTION);

                    CorpusManagement DragAndDrop = new CorpusManagement(
                            itsAdminFrame, c);

                    DragAndDrop.setBounds(50, 50, 850, 550);
                    DragAndDrop.setVisible(true);
                    DragAndDrop
                            .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                    itsAdminFrame.jdp.add(DragAndDrop);
                    DragAndDrop.moveToFront();

                    itsAdminFrame.jdp.setVisible(true);
                }
            } else if (event.getSource().equals(xmlMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);

                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File sf = chooser.getSelectedFile();

                    SwingWorker aWorker = new SwingWorker(event, sf) {
                        protected void doNonUILogic() {
                            itsAdminFrame.jdp.setCursor(Cursor
                                    .getPredefinedCursor(Cursor.WAIT_CURSOR));

                            try {
                                PDFimportHandlerPass1 pihp1 = com.iw.tools.IngestionUtils
                                        .scanConvertedXML(f);

                                XMLingestionMenu xim = new XMLingestionMenu(
                                        pihp1, itsAdminFrame, f);
                                xim.setVisible(true);

                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                                JOptionPane.showMessageDialog(
                                        itsAdminFrame,
                                        "Fatal error: "
                                        + e.getLocalizedMessage(),
                                        "Error", JOptionPane.NO_OPTION);
                                return;
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame.jdp
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            } else if (event.getSource().equals(xmlOReillyMenuItem)) {
                // need to implement createCorpusFromOreillySource like
                // createCorpusFromOxfordDictionary!
                JOptionPane
                        .showMessageDialog(
                        itsAdminFrame,
                        "This functionality is not really working yet...sorry!",
                        "Error", JOptionPane.NO_OPTION);
                /*
                 * JFileChooser chooser = new JFileChooser();
                 * chooser.setMultiSelectionEnabled(false);
                 * 
                 * int option = chooser.showOpenDialog(itsAdminFrame); if
                 * (option == JFileChooser.APPROVE_OPTION) { File sf =
                 * chooser.getSelectedFile();
                 * 
                 * try { Corpus c =
                 * com.iw.tools.IngestionUtils.createCorpusFromOReillySource(sf,
                 * itsAdminFrame.its);
                 * 
                 * JOptionPane.showMessageDialog(itsAdminFrame,
                 * "Taxonomy created from OReilly file successfully.",
                 * "Information", JOptionPane.NO_OPTION);
                 * 
                 * CorpusManagement DragAndDrop = new
                 * CorpusManagement(itsAdminFrame, c);
                 * 
                 * DragAndDrop.setBounds(50, 50, 850, 550);
                 * DragAndDrop.setVisible(true);
                 * DragAndDrop.setDefaultCloseOperation(
                 * WindowConstants.DISPOSE_ON_CLOSE);
                 * 
                 * itsAdminFrame.jdp.add(DragAndDrop);
                 * DragAndDrop.moveToFront();
                 * 
                 * itsAdminFrame.jdp.setVisible(true); } catch (Exception e) {
                 * e.printStackTrace(System.out); } }
                 */
            } else if (event.getSource().equals(importOxfordMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);

                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File sf = chooser.getSelectedFile();

                    try {
                        Corpus c = com.iw.tools.IngestOxford
                                .createCorpusFromOxfordDictionary(sf,
                                itsAdminFrame.its);

                        JOptionPane
                                .showMessageDialog(
                                itsAdminFrame,
                                "Taxonomy created from Oxford Dictionary file successfully.",
                                "Information", JOptionPane.NO_OPTION);

                        CorpusManagement DragAndDrop = new CorpusManagement(
                                itsAdminFrame, c);

                        DragAndDrop.setBounds(50, 50, 850, 550);
                        DragAndDrop.setVisible(true);
                        DragAndDrop
                                .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                        itsAdminFrame.jdp.add(DragAndDrop);
                        DragAndDrop.moveToFront();

                        itsAdminFrame.jdp.setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                    }
                }
            } else if (event.getSource().equals(importGaleMenuItem)) {
                // ******Hardcoded starting file/folder structure in function
                // for now!!
                // JFileChooser chooser = new JFileChooser();
                // chooser.setMultiSelectionEnabled(false);

                // int option = chooser.showOpenDialog(itsAdminFrame);
                // if (option == JFileChooser.APPROVE_OPTION) {
                // File sf = chooser.getSelectedFile();
                File sf = new File(
                        "c:\\temp\\gale\\aiad_0001\\aiad_0001\\xml\\aiad_0038_0001_1_00001.xml");

                try {
                    Corpus c = com.iw.tools.IngestGale
                            .createCorpusFromGaleDictionary(sf,
                            itsAdminFrame.its);

                    JOptionPane
                            .showMessageDialog(
                            itsAdminFrame,
                            "Taxonomy created from Gale Dictionary file successfully.",
                            "Information", JOptionPane.NO_OPTION);

                    CorpusManagement DragAndDrop = new CorpusManagement(
                            itsAdminFrame, c);

                    DragAndDrop.setBounds(50, 50, 850, 550);
                    DragAndDrop.setVisible(true);
                    DragAndDrop
                            .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                    itsAdminFrame.jdp.add(DragAndDrop);
                    DragAndDrop.moveToFront();

                    itsAdminFrame.jdp.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(importDictionaryMenu)) {
                JFileChooser fileChooser = new JFileChooser();
                File file = null;
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "Files(txt)", "txt");
                fileChooser.setFileFilter(filter);
                int select = fileChooser.showOpenDialog(null);
                if (select == JFileChooser.APPROVE_OPTION) {
                    file = fileChooser.getSelectedFile();
                    SwingWorker aWorker = new SwingWorker(event, file) {
                        protected void doNonUILogic() {
                            itsAdminFrame.jdp.setCursor(Cursor
                                    .getPredefinedCursor(Cursor.WAIT_CURSOR));

                            if (f != null) {
                                PopupProgressBar ppb = new PopupProgressBar(
                                        itsAdminFrame, "Add dictionary", 0, 100);
                                ppb.setVisible(true);
                                try {
                                    itsAdminFrame.its.addDictionary(
                                            f.getName(), f, ppb);
                                } catch (Exception e) {
                                } finally {
                                    ppb.dispose();
                                }
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "not selected file");
                            }

                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame.jdp
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            } else if (event.getSource().equals(classMenuItem)) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(true);
                int option = chooser.showOpenDialog(itsAdminFrame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File[] sf = chooser.getSelectedFiles();

                    // classify the document
                    SwingWorker aWorker = new SwingWorker(event, sf[0]) {
                        protected void doNonUILogic() {
                            try {
                                itsAdminFrame
                                        .setCursor(Cursor
                                        .getPredefinedCursor(Cursor.WAIT_CURSOR));
                                itsAdminFrame.getGlassPane().setVisible(true);
                                NodeDocView ndv = new NodeDocView(
                                        itsAdminFrame, f);
                                ndv.setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace(System.out);
                            }
                        }

                        protected void doUIUpdateLogic() {
                        }

                        public void finished() {
                            super.finished();
                            itsAdminFrame
                                    .setCursor(Cursor
                                    .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            itsAdminFrame.getGlassPane().setVisible(false);
                        }
                    };
                    aWorker.start();
                }
            } else if (event.getSource() instanceof NarrMenuItem) {
                NarrMenuItem nmi = (NarrMenuItem) event.getSource();
                ConceptAlerts p = new ConceptAlerts(itsAdminFrame, nmi);
                p.setVisible(true);
            } else if (event.getSource().equals(checkMenuItem)) {
                try {
                    ReportCheckPanel rcPanel = new ReportCheckPanel(
                            itsAdminFrame);
                    rcPanel.invokeReportCheckPanelLayout();
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(nodeReportMenuItem)) {
                try {
                    TaxonomyReport taxBox = new TaxonomyReport(itsAdminFrame,
                            "Documents");
                    taxBox.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(childReportMenuItem)) {
                try {
                    TaxonomyReport taxBox = new TaxonomyReport(itsAdminFrame,
                            "Children");
                    taxBox.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(sigReportMenuItem)) {
                try {
                    TaxonomyReport taxBox = new TaxonomyReport(itsAdminFrame,
                            "Signatures");
                    taxBox.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource() == qaReportMenuItem) {
                try {
                    SignatureQA reportQA = new SignatureQA(itsAdminFrame);
                    reportQA.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource() == wikiReportMenuItem) {
                try {
                    ReportWikiImport reportWiki = new ReportWikiImport(itsAdminFrame);
                    reportWiki.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource() == qaReportGenerateMenuItem) {
                try {
                    QaReport reportQa = new QaReport(itsAdminFrame);
                    reportQa.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource() == iamgesProcessMenuItem) {
                try {
                    ImageProcessStatus imagesP = new ImageProcessStatus(itsAdminFrame);
                    imagesP.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(musthaveProcessMenuItem)) {
                try {
                    MusthavesProcessStatus musthaveProcess = new MusthavesProcessStatus(itsAdminFrame);
                    musthaveProcess.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(prefMenuItem)) {
                Preferences p = new Preferences(itsAdminFrame);
                p.setVisible(true);
            } else if (event.getSource().equals(refreshMenuItem)) {
                try {
                    NodeRefreshReport ts = new NodeRefreshReport(itsAdminFrame);
                    ts.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(nodeMenuItem)) {
                try {
                    TopicSearch ts = new TopicSearch(itsAdminFrame);
                    ts.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(adTopMenuItem)) {
                try {
                    SimpleTopicSearch ds = new SimpleTopicSearch(itsAdminFrame);
                    ds.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(adDocMenuItem)) {
                try {
                    SimpleDocSearch ds = new SimpleDocSearch(itsAdminFrame);
                    ds.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(docMenuItem)) {
                try {
                    DocumentSearch ds = new DocumentSearch(itsAdminFrame);
                    ds.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (event.getSource().equals(aboutMenuItem)) {
                JInternalFrame MDIframe = new JInternalFrame("About", false,
                        true, false, true);
                JDesktopPane jdesktoppane = itsAdminFrame.jdp;

                MDIframe.setLocation(35, 35);
                MDIframe.setSize(400, 250);

                MDIframe.setFrameIcon(itsAdminFrame.iIndraweb);

                About about = new About();
                MDIframe.getContentPane().add(about);

                jdesktoppane.add(MDIframe);
                MDIframe.show();
                about.setVisible(true);

            }
            if (event.getSource().equals(this.admonClassifiers)) {
                new AdministerClassifiers(this.itsAdminFrame);

            } else if (event.getSource().equals(splitArchives)) {
                new Thread(this).start();

            } else if (event.getSource().equals(managerResource)) {
                new SelectionResource(itsAdminFrame);

                /*
                 * }else if (event.getSource().equals(importLocalMenuItem)){
                 * 
                 * JFileChooser fileImport = new JFileChooser();
                 * fileImport.setMultiSelectionEnabled(false);
                 * fileImport.setFileSelectionMode
                 * (JFileChooser.FILES_AND_DIRECTORIES); int option =
                 * fileImport.showOpenDialog(itsAdminFrame); if (option ==
                 * JFileChooser.APPROVE_OPTION) { File selectedPfile =
                 * fileImport.getSelectedFile();
                 * System.out.println(selectedPfile.getPath()); String[]
                 * ficheros = selectedPfile.list(); if (ficheros == null)
                 * System.
                 * out.println("No hay ficheros en el directorio especificado");
                 * else { for (int x=0;x<ficheros.length;x++)
                 * System.out.println(ficheros[x]); } }
                 */

            } else if (event.getSource().equals(versionMenuItem)) {
                JInternalFrame MDIframe = new JInternalFrame("ProcinQ Version",
                        false, true, false, true);
                JDesktopPane jdesktoppane = itsAdminFrame.jdp;

                MDIframe.setLocation(35, 35);
                MDIframe.setSize(270, 120);

                MDIframe.setFrameIcon(itsAdminFrame.iIndraweb);

                SpringLayout panelLayout = new SpringLayout();
                JPanel panel = new JPanel(panelLayout);

                JLabel img = new JLabel(ImageUtils.getImage(MDIframe,
                        "itsimages/logo.gif"));
                JLabel jlc = new JLabel(itsAdminFrame.its.getClientVersion());
                JLabel jls = new JLabel(itsAdminFrame.its.getServerVersion());

                jlc.setFont(new Font("Arial", Font.BOLD, 12));
                jls.setFont(new Font("Arial", Font.BOLD, 12));

                panel.setBorder(BorderFactory.createEmptyBorder(5, 2, 5, 2));

                // panel.setLayout(new BorderLayout());
                panel.add(img);
                panel.add(jlc);
                panel.add(jls);

                panelLayout.getConstraints(img).setX(Spring.constant(5));
                panelLayout.getConstraints(img).setY(Spring.constant(5));

                panelLayout.getConstraints(jlc).setX(Spring.constant(70));
                panelLayout.getConstraints(jlc).setY(Spring.constant(20));

                panelLayout.getConstraints(jls).setX(Spring.constant(70));
                panelLayout.getConstraints(jls).setY(Spring.constant(40));

                MDIframe.getContentPane().add(panel);

                jdesktoppane.add(MDIframe);
                MDIframe.show();
                panel.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void performAction(ActionEvent event) {
    }

    @Override
    public void run() {
        itsAdminFrame.its.splitArchive();

    }
}
