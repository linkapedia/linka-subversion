package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.oculustech.layout.*;
import com.iw.system.*;
import java.util.HashMap;
import java.util.Map;

public class MusthaveFirstStepDialog extends JPanel implements ActionListener {

    protected JTextField txtToLevel;
    protected JCheckBox useNodeTitleFullChk;
    protected JCheckBox useNodeDescriptionChk;
    protected JButton helpNodeDescriptionBtn;
    protected JButton applyBtn;
    protected JButton cancelBtn;
    private JInternalFrame MDIframe;
    private ITSAdministrator itsadmin;
    private ITS its;
    private ITSTreeNode Node;
    ImageIcon iconHelp = com.iw.tools.ImageUtils.getImage(this, "itsimages/help.png");

    public MusthaveFirstStepDialog(ITSAdministrator itsFrame, ITSTreeNode n) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        its = itsFrame.its;
        Node = n;
        itsadmin = itsFrame;

        MDIframe = new JInternalFrame("Musthave First Step", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(450, 140);
        MDIframe.setBackground(Color.lightGray);
        MDIframe.setResizable(false);

        MDIframe.setFrameIcon(itsFrame.iIndraweb);


        JDesktopPane jdesktoppane = itsFrame.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        JLabel operation = new JLabel("From current selection To level:");
        operation.setToolTipText("Choose the depth.");

        txtToLevel = new JTextField("2", 5);
        
        useNodeTitleFullChk = new JCheckBox("Use Title full");
        useNodeTitleFullChk.setSelected(true);

        useNodeDescriptionChk = new JCheckBox("Use Description");
        useNodeDescriptionChk.setSelected(true);
        helpNodeDescriptionBtn = new JButton(iconHelp);
        helpNodeDescriptionBtn.setBorder(BorderFactory.createEmptyBorder());
        helpNodeDescriptionBtn.setContentAreaFilled(false);
        helpNodeDescriptionBtn.addActionListener(this);


        applyBtn = new JButton("Run");
        applyBtn.addActionListener(this);

        cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.nestGrid(2, 14);
                    {
                        layout.add(operation);
                        layout.add(txtToLevel);
                        layout.add(useNodeTitleFullChk);
                        layout.add(new JLabel());
                        layout.add(useNodeDescriptionChk);
                        layout.add(helpNodeDescriptionBtn);
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(applyBtn);
            layout.addSpace(10);
            layout.add(cancelBtn);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
    }

    public void openAtUpperCenter() {
        Dimension winsize = MDIframe.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        MDIframe.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == applyBtn) {
            Map<String, String> options = new HashMap<String, String>();
            options.put("UseNodeDescription",
                    String.valueOf(useNodeDescriptionChk.isSelected()));
            options.put("UseNodeTitleFull",
                    String.valueOf(useNodeTitleFullChk.isSelected()));

            String toLevel = txtToLevel.getText();
            try {
                String itsApiResult = its.startMusthavesFistStep(Node.get("NODEID"), options, toLevel);
                if (itsApiResult == null) {
                    JOptionPane.showMessageDialog(itsadmin, "Error in the server");
                } else {
                    JOptionPane.showMessageDialog(itsadmin, itsApiResult);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(itsadmin, ex.getMessage());
            }
            MDIframe.dispose();

        }
        if (e.getSource().equals(helpNodeDescriptionBtn)) {
            MessagesUtil.showInfoMessage(
                    "Use Node Description:", "Search patterns like "
                    + "(also know as, referred as, also called...) "
                    + "and save the next word as musthave");
        }
        if (e.getSource().equals(cancelBtn)) {
            MDIframe.dispose();
        }
    }
}
