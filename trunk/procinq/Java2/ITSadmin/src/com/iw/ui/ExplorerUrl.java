/*
 * GUI : Select the URL
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 29, 2011, 14:33 PM
*/

package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ExplorerUrl extends JDialog implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6254578099522595923L;
	private JTextField txtUrl;
	private JButton btnOk;
	private String url;
	public boolean cancelAction;
	
	public ExplorerUrl(){
		cancelAction=true;
		txtUrl = new JTextField("",50);
		btnOk = new JButton("Ok");
		btnOk.addActionListener(this);
		
		getContentPane().add(txtUrl,BorderLayout.EAST);
		getContentPane().add(btnOk,BorderLayout.WEST);
		getRootPane().setDefaultButton(btnOk);
		
		setModal(true);
		setResizable(false);
		setLocation(100, 100);
		setTitle("Select the URL");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
		
	}
	private void setUrl(){
		url= txtUrl.getText();
		
	}
	public String getUrl(){
		return url;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(btnOk)){
			if(txtUrl.getText().length()<8){
				JOptionPane.showMessageDialog(this, "Please select the URL");
				return;
			}
			cancelAction=false;
			setUrl();
			dispose();
		}
		
	}
	
}
