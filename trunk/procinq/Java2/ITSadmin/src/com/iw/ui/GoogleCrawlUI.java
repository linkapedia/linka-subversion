package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.iw.tools.EmitFilterGoogle;

public class GoogleCrawlUI extends JDialog implements ActionListener {

	/**
	 * @author andres
	 */

	private static final Logger LOG = Logger.getLogger(GoogleCrawlUI.class);
	private static final long serialVersionUID = 5641723972246756224L;

	// panels
	private JPanel pnlInformation;
	private JPanel pnlFilter;
	private JPanel pnlButtons;

	// controls;
	private JButton btnSend;
	private JLabel lblMaxNumberLinks;
	private JTextField txtMaxNumberLinks;
	private JRadioButton rbOption1;
	private JRadioButton rbOption2;
	private ButtonGroup bgFilter;
	private boolean cancel;

	private Map<String, String> params;

	public GoogleCrawlUI(ITSAdministrator parent) {
		super(parent, true);
	}
	
	public boolean getResponse(){
		return cancel;
	}

	public void showWin() {
		cancel=true;
		this.setTitle("PARAMETERS GOOGLE CRAWL");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setSize(new Dimension(300, 250));
		this.setResizable(false);
		this.setModal(true);
		this.openAtCenter();

		pnlInformation = new JPanel();
		pnlInformation.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEmptyBorder(10, 10, 10, 10), "Data"));
		pnlInformation.setLayout(new GridLayout(1, 2));
		lblMaxNumberLinks = new JLabel("Max links");
		txtMaxNumberLinks = new JTextField(3);
		txtMaxNumberLinks.setToolTipText("Range 1 - 100");

		pnlInformation.add(lblMaxNumberLinks);
		pnlInformation.add(txtMaxNumberLinks);

		pnlFilter = new JPanel();
		pnlFilter.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(Color.BLACK), "Filter"));
		pnlFilter.setLayout(new GridLayout(2, 1));
		rbOption1 = new JRadioButton(EmitFilterGoogle.getMessage(EmitFilterGoogle.CUSTOM_SEARCH_ORG_EDU));
		rbOption2 = new JRadioButton(EmitFilterGoogle.getMessage(EmitFilterGoogle.CUSTOM_SEARCH_COM));
		bgFilter = new ButtonGroup();
		bgFilter.add(rbOption1);
		bgFilter.add(rbOption2);
		pnlFilter.add(rbOption1);
		pnlFilter.add(rbOption2);

		pnlButtons = new JPanel();
		pnlButtons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		btnSend = new JButton("Send");
		btnSend.addActionListener(this);
		pnlButtons.setLayout(new FlowLayout());
		pnlButtons.add(btnSend);

		// add panel to dialog
		this.getContentPane().add(pnlInformation, BorderLayout.NORTH);
		this.getContentPane().add(pnlFilter, BorderLayout.CENTER);
		this.getContentPane().add(pnlButtons, BorderLayout.SOUTH);

		this.setVisible(true);

	}

	private void openAtCenter() {
		Dimension winsize = this.getSize(), screensize = Toolkit
				.getDefaultToolkit().getScreenSize();
		this.setLocation((screensize.width - winsize.width) / 2,
				(screensize.height - winsize.height) / 2);
	}

	public Map<String, String> getMap() {
		return params;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btnSend)) {
			params = new HashMap<String, String>();
			Integer valueTxt;
			try {
				valueTxt = Integer.parseInt(txtMaxNumberLinks.getText());
			} catch (NumberFormatException nfe) {
				JOptionPane.showMessageDialog(this, "Only numbers");
				return;
			}
			if (txtMaxNumberLinks.getText().isEmpty()) {
				JOptionPane.showMessageDialog(this,
						"Please put number between 1 and 100");
				return;
			}

			if ((valueTxt > 100) || (valueTxt < 1)) {
				JOptionPane.showMessageDialog(this,
						"Please put number between 1 and 100");
				return;
			}
			if ((!rbOption1.isSelected()) && (!rbOption2.isSelected())) {
				JOptionPane.showMessageDialog(this, "Please select one filter");
				return;
			}
			cancel=false;
			params.put("numlinks", txtMaxNumberLinks.getText());
			if (rbOption1.isSelected()) {
				params.put("customsearch", String.valueOf(EmitFilterGoogle.CUSTOM_SEARCH_ORG_EDU));
			} else if (rbOption2.isSelected()) {
				params.put("customsearch", String.valueOf(EmitFilterGoogle.CUSTOM_SEARCH_COM));
			}
			this.dispose();
		}
	}
}
