/*
 * GUI : Display list the taxonomies and list the resources
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
 */

package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.Position;
import javax.swing.tree.TreePath;

import com.iw.system.Corpus;
import com.iw.system.ITS;
import com.iw.tools.webdav.gui.Explorer;

public class SelectionResource extends JPanel implements ActionListener,
		Runnable {

	public ITS its = null;
	public ITSAdministrator ITSframe = null;

	protected JInternalFrame MDIframe;
	private static final long serialVersionUID = 116309844149711179L;

	private JMenuBar menu;

	private JMenu menuTax, // menu of the taxonomies
			menuUri; // menu of the resources

	private JMenuItem menuItemTax, menuItemLocal, menuItemWebdav, menuItemUrl;

	private JList listTax, listUri;

	private JScrollPane panelTax, panelUri;

	private JPanel panelButtons = new JPanel();
	private JPanel panelResources = new JPanel();
	private JPanel panelTaxonomies = new JPanel();
	private JLabel lblTitleTaxonomy = new JLabel("TAXONOMIES");
	private JLabel lblTitleResources = new JLabel("RESOURCES");
	private JButton btnOk;
	private ITSAdministrator frame;

	DefaultListModel modeloUri, modeloTax;

	Explorer exp;// window for select webdav resource
	private ArrayList<String> urlArrayLocal;
	private ArrayList<String> tax;// taxonomiies list to send the server
	private ArrayList<String> res;// resources list to send the server

	public SelectionResource() throws Exception {
	}

	public SelectionResource(ITSAdministrator frame) throws Exception {

		JDesktopPane jdesktoppane = frame.jdp;

		MDIframe = new JInternalFrame("Resource manager", false, true, false,
				true);
		MDIframe.setSize(675, 210);
		MDIframe.setBackground(Color.lightGray);
		its = frame.its;
		ITSframe = frame;

		MDIframe.setFrameIcon(ITSframe.iIndraweb);

		this.frame = frame;

		modeloUri = new DefaultListModel();
		modeloTax = new DefaultListModel();

		listTax = new JList(modeloTax);
		listUri = new JList(modeloUri);
		menu = new JMenuBar();
		menuTax = new JMenu("Taxonomy");
		menuUri = new JMenu("Resource");
		menuItemTax = new JMenuItem("Select Taxonomy");
		menuItemLocal = new JMenuItem("Select local filesystem");
		menuItemWebdav = new JMenuItem("Select webdav resource");
		menuItemUrl = new JMenuItem("Select URL");

		menuItemTax.addActionListener(this);
		menuItemLocal.addActionListener(this);
		menuItemWebdav.addActionListener(this);
		menuItemUrl.addActionListener(this);
		menuTax.add(menuItemTax);
		menuUri.add(menuItemLocal);
		menuUri.add(menuItemWebdav);
		menuUri.add(menuItemUrl);

		menu.add(menuTax);
		menu.add(menuUri);

		MDIframe.setJMenuBar(menu);

		btnOk = new JButton("OK");
		btnOk.addActionListener(this);
		panelTax = new JScrollPane(listTax);
		panelUri = new JScrollPane(listUri);
		panelTaxonomies.setBorder(BorderFactory.createEmptyBorder(20, 20, 20,
				20));
		panelResources.setBorder(BorderFactory
				.createEmptyBorder(10, 20, 10, 20));
		panelButtons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panelButtons.add(btnOk, BorderLayout.CENTER);

		panelTaxonomies.setLayout(new BorderLayout());
		panelResources.setLayout(new BorderLayout());
		panelResources.add(lblTitleResources, BorderLayout.NORTH);
		panelResources.add(panelUri, BorderLayout.CENTER);

		panelTaxonomies.add(lblTitleTaxonomy, BorderLayout.NORTH);
		panelTaxonomies.add(panelTax, BorderLayout.CENTER);

		MDIframe.getRootPane().setDefaultButton(btnOk);
		MDIframe.getContentPane().add(panelTaxonomies, BorderLayout.NORTH);
		MDIframe.getContentPane().add(panelResources, BorderLayout.CENTER);
		MDIframe.getContentPane().add(panelButtons, BorderLayout.SOUTH);
		MDIframe.setSize(600, 475);
		// MDIframe.openAtUpperCenter();
		setVisible(true);

		jdesktoppane.add(MDIframe);
		MDIframe.setVisible(true);

		KeyListener keyListener = new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getSource().equals(listUri)) {
					if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
						if (listUri.getSelectedValues().length >= 0) {
							// modeloUri.removeRange(listUri.getMinSelectionIndex(),listUri.getMaxSelectionIndex());
							Object[] selection = listUri.getSelectedValues();

							for (Object s : selection) {
								for (int k = 0; k < modeloUri.getSize(); k++) {
									if (modeloUri.get(k).equals(s)) {
										modeloUri.remove(k);
										break;
									}
								}
							}
							listUri.revalidate();
						}
					}
				} else if (e.getSource().equals(listTax)) {
					if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
						if (listTax.getSelectedValues().length >= 0) {
							// modeloTax.removeRange(listTax.getMinSelectionIndex(),listTax.getMaxSelectionIndex());
							Object[] selection = listTax.getSelectedValues();

							for (Object s : selection) {
								for (int k = 0; k < modeloTax.getSize(); k++) {
									if (modeloTax.get(k).equals(s)) {
										modeloTax.remove(k);
										break;
									}
								}
							}
							listTax.revalidate();
						}
					}
				}

			}
		};
		listTax.addKeyListener(keyListener);
		listUri.addKeyListener(keyListener);

	}

	/**
	 * build uri webdav convert to a string
	 * 
	 * @param tp
	 *            : path
	 * @return string the path
	 */
	public String buildUriWebdav(TreePath tp) {
		String pathAux = "";
		pathAux = pathAux.concat("" + tp.getPathComponent(1));
		for (int i = 2; i < tp.getPathCount(); i++) {
			pathAux = pathAux.concat(tp.getPathComponent(i) + "/");
		}
		return pathAux;
	}

	/**
	 * local filesystem save in array the path of archive
	 * 
	 * @param f
	 *            : file to return your path
	 */
	private void buildUriLocal(File f) {
		if (f.isDirectory()) {
			File[] a = f.listFiles();

			for (int i = 0; i < a.length; i++) {
				buildUriLocal(a[i]);
			}
		} else {
			urlArrayLocal.add(f.getAbsolutePath());

		}
	}

	/**
	 * return the array with all the uri of archives local
	 * 
	 * @return
	 */
	private ArrayList<String> geturlArrayLocal() {
		return urlArrayLocal;
	}

	/**
	 * function contains
	 * 
	 * @param term
	 * @return
	 */
	private boolean searchTax(String term) {
		if (modeloTax.getSize() > 0) {
			for (Object s : modeloTax.toArray()) {
				if (s.toString().equals(term)) {
					return true;
				}
			}
		} else {
			return false;
		}
		return false;
	}

	/**
	 * function contains
	 * 
	 * @param term
	 * @return
	 */

	private boolean searchUri(String term) {
		if (modeloUri.getSize() > 0) {
			for (Object s : modeloUri.toArray()) {
				if (s.toString().equals(term)) {
					return true;
				}
			}
		} else {
			return false;
		}
		return false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// selected
		// taxonomies-------------------------------------------------------------------------------
		if (e.getSource().equals(menuItemTax)) {
			try {
				ExploreCorpus ec = new ExploreCorpus(frame);
				if (ExploreCorpus.cancelAction) {
					if (ec.getArray().length > 0) {
						Object arrayaux[] = ec.getArray();
						for (int i = 0; i < ec.getArray().length; i++) {
							if (!searchTax(arrayaux[i].toString())) {
								modeloTax.addElement(arrayaux[i]);
							}
						}
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// selected URL foreign
			// site----------------------------------------------------------------------------
		} else if (e.getSource().equals(menuItemUrl)) {
			ExplorerUrl expu = new ExplorerUrl();
			String uri = null;
			if (!expu.cancelAction) {
				uri = expu.getUrl();
				if (uri.endsWith("/")) {
					uri = uri.substring(0, uri.length() - 1);
				}
				if (!searchUri(uri)) {
					this.modeloUri.addElement(uri);
				}
			}
			// selected Resources
			// local----------------------------------------------------------------------------
		} else if (e.getSource().equals(menuItemLocal)) {
			JFileChooser fileImport = new JFileChooser();
			fileImport.setMultiSelectionEnabled(false);
			fileImport.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int option = fileImport.showOpenDialog(this);
			if (option == JFileChooser.APPROVE_OPTION) {
				// TODO select all archives from tje path
				File selectedPfile = fileImport.getSelectedFile();
				urlArrayLocal = null;
				urlArrayLocal = new ArrayList<String>();
				buildUriLocal(selectedPfile);
				ArrayList<String> files = new ArrayList<String>();

				files = geturlArrayLocal();
				for (int i = 0; i < files.size(); i++) {
					if (!searchUri(files.get(i))) {
						modeloUri.addElement(files.get(i));
					}
				}

			}
			// selected Webdav
			// resources----------------------------------------------------------------------------
		} else if (e.getSource().equals(menuItemWebdav)) {
			exp = new Explorer();
			try {
				modeloUri.addElement(buildUriWebdav(exp.getPathSelection()));
			} catch (NullPointerException error) {

			}

			// Send the resources and
			// URIs----------------------------------------------------------------------------
		} else if (e.getSource().equals(btnOk)) {
			tax = new ArrayList<String>();
			res = new ArrayList<String>();
			Corpus c = new Corpus();
			for (int i = 0; i < modeloTax.getSize(); i++) {
				c = (Corpus) modeloTax.get(i);
				tax.add(c.getID());
			}
			for (int k = 0; k < modeloUri.size(); k++) {
				res.add(modeloUri.get(k).toString());
			}
			if (tax.isEmpty()) {
				JOptionPane.showMessageDialog(this, "Select the taxonomies");
				return;
			}
			if (res.isEmpty()) {
				JOptionPane.showMessageDialog(this, "Select Resources");
				return;
			}
			JOptionPane.showMessageDialog(this,
					"Resources and CorpusID were send successfully");

			new Thread().start();
			modeloTax.removeAllElements();
			modeloUri.removeAllElements();

		}
	}

	@Override
	public void run() {
		its.sendResources(tax, res);

	}
}
