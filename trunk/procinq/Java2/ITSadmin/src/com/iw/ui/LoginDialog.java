/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Nov 6, 2003
 * Time: 1:40:38 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.iw.system.*;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.File;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.LogLog;

public class LoginDialog extends JDialog {
    protected JTextField jtfServerName, jtfUsername;
    protected JPasswordField jpwPassword;
    protected JButton jbConnect, jbCancel;

    private Frame itsframe = null;

    public String sServerName;
    public String sUsername;
    public String sPassword;
    public int iButtonPressed;
    public ITS its = null;

    private User loadedUser;

    JLabel jStatusString = new JLabel("Log In to the ProcinQ Editor's Desktop");

    public static final int BTN_CONNECT = 0x01;
    public static final int BTN_CANCEL = 0x02;

    public LoginDialog(Frame frame) throws Exception {
        super(frame, "Connect to server", true);
        itsframe = frame;

        pack();
        this.setSize(350, 220);
        this.openAtCenter();

        loadedUser = null;
        File fp = new File("user.data");
        if (fp.exists()) {
            try { loadedUser = User.deserializeObject("user.data"); }
            catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Note, due to a system change your previous user "+
                    "preferences have been lost.", "Information", JOptionPane.NO_OPTION);
            }
        }

        getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER));
        JPanel jp = new JPanel(new GridLayout(5, 1));

        JPanel jpm = new JPanel(new FlowLayout(FlowLayout.LEFT, 8, 4));
        jpm.add(jStatusString);
        jp.add(jpm);

        JPanel jp1 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 8, 4));
        jp1.add(new JLabel("Server name:"));
        jp1.add(jtfServerName = new JTextField("", 15));
        jp.add(jp1);

        JPanel jp2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 8, 4));
        jp2.add(new JLabel("Username:"));
        jp2.add(jtfUsername = new JTextField("", 15));
        jp.add(jp2);

        JPanel jp3 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 8, 4));
        jp3.add(new JLabel("Password:"));
        jp3.add(jpwPassword = new JPasswordField("", 15));
        jp.add(jp3);

        JPanel jp4 = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 4));
        jbConnect = new JButton("Connect");
        jbConnect.addActionListener(theActionListener);
        jp4.add(jbConnect);

        jbConnect.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) { OuterLogin(); }
                e.consume();
            }
        });

        jbCancel = new JButton("Cancel");
        jbCancel.addActionListener(theActionListener);
        jp4.add(jbCancel);
        jp.add(jp4);

        jbCancel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) { dispose(); }
                e.consume();
            }
        });

        getRootPane().setDefaultButton(jbConnect);

        if (loadedUser != null) {
            jtfServerName.setText(loadedUser.getServer());
            jtfUsername.setText(loadedUser.getUsername());

            // set initial focus
            addWindowListener(new WindowAdapter() {
                public void windowOpened(WindowEvent e) {
                    jpwPassword.requestFocus();
                }
            });
        }

        getContentPane().add(jp);
        this.setResizable(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }

    public void openAtCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 2);
    }

    protected ActionListener theActionListener = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            try {
            // Copy the contents of the servername, username and password fields
            sServerName = jtfServerName.getText();
            sUsername = jtfUsername.getText();
            sPassword = new String(jpwPassword.getPassword());

            // Determine if 'Connect' or 'Cancel' was pressed
            if (e.getSource() == jbConnect) {
                OuterLogin();
            }
            if (e.getSource() == jbCancel) {
                iButtonPressed = BTN_CANCEL;

                System.exit(0);
            }} catch (Exception ex) { ex.printStackTrace(System.out); }
            finally { setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); }
        }
    };

    public void OuterLogin() {
        iButtonPressed = BTN_CONNECT;
        try {
            Login();
        } catch (Exception exception) {
            exception.printStackTrace(System.out);
            jStatusString.setForeground(Color.RED);
            jStatusString.setText(exception.getMessage());
            return;
        }

        jStatusString.setText("Login Successful.");
        //setVisible(false);
        dispose();
    }

    public void Login() throws Exception {
        its = new ITS();
        its.SetAPI("http://" + jtfServerName.getText() + "/");
        User u = null;

        try {
            u = its.Login(jtfUsername.getText(), jpwPassword.getText());
        } catch (ITSConnectionFailure e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.NO_OPTION);
            dispose(); System.exit(0);
        } catch (Exception e) {
            throw e;
        }

        // serialize here
        u.setUsername(jtfUsername.getText());
        u.setServer(jtfServerName.getText());
        if (loadedUser != null) {
            u.setAbstractWeight(loadedUser.getAbstractWeight());
            u.setDocumentWeight(loadedUser.getDocumentWeight());
            u.useDynamicThesaurus(loadedUser.useDynamicThesaurus());
            u.writeToLog(loadedUser.writeToLog());
            u.setCorporaToUse(loadedUser.getCorporaToUse());

            u.setCriticalThreshold(loadedUser.getCriticalThreshold());
            u.setSevereThreshold(loadedUser.getSevereThreshold());
            u.setMinorThreshold(loadedUser.getMinorThreshold());

            u.setIncludeNumbers(loadedUser.getIncludeNumbers() == null ? "false" : loadedUser.getIncludeNumbers().booleanValue()+"");
            u.setNumParentTitleTermAsSigs(loadedUser.getNumParentTitleTermAsSigs() == null ? 3 : loadedUser.getNumParentTitleTermAsSigs().intValue());
            u.setWordFreqMaxForSig(loadedUser.getWordFreqMaxForSig() == null ? 75 : loadedUser.getWordFreqMaxForSig().intValue());
            u.setPhraseLenMin(loadedUser.getPhraseLenMin() == null ? 2 : loadedUser.getPhraseLenMin().intValue());
            u.setPhraseLenMax(loadedUser.getPhraseLenMax() == null ? 4 : loadedUser.getPhraseLenMax().intValue());

            u.setCharacterThreshold(loadedUser.getCharacterThreshold());
            u.setCorpusMTlevel(loadedUser.getCorpusMTLevel());
            u.setPromptDragAndDrop(loadedUser.PromptForDragAndDrop());
            u.setPromptJoinChange(loadedUser.PromptForJoinChange());
            u.setPromptTopicChange(loadedUser.PromptForTopicChange());
            u.setPromptTopicRemoval(loadedUser.PromptForTopicRemoval());
            u.setPathPrefix(loadedUser.getPathPrefix());

            u.setRuleOne(loadedUser.isRuleOneSet());
            u.lockFunctions(loadedUser.lockFunctions());
            u.setSameLineStyles(loadedUser.sameLines());

            u.setHK(loadedUser.isHKSet());
            u.setLP(loadedUser.isLingpipeSet());
            u.setLPC(loadedUser.isCapitalSet());
        }
        u.serializeObject("user.data");

        String SessionKey = u.getSessionKey();
        its.SetSessionKey(SessionKey);

        LogLog.setQuietMode(true);
        //PropertyConfigurator.configure("ITSLogConfig.txt");
    }

    public String toString() {
        String s = "ServerConnectionDialog [Server name='" + sServerName + "', ";
        s += "Username='" + sUsername + "', Password='" + sPassword + "', Button ID=" + iButtonPressed + ".";
        return (s);
    }
}
