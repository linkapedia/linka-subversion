package com.iw.ui;

import com.iw.system.Corpus;
import com.iw.system.ITS;
import com.iw.system.QaReportModel;
import com.iw.system.WrapperListQaReport;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.dom4j.QName;

public class QaReport extends JInternalFrame implements ActionListener {

    private JTable table;
    private JTextField txtRowNum;
    private JButton btnGenerate;
    private JLabel lblRowNum;
    private JButton btnReport;
    private DefaultTableModel modelTable;
    private List<Corpus> listData;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");
    /// combo to generate
    private JComboBox cbxTaxonomies;
    private JTextField txtTaxonomies;
    private Corpus currentTaxonomy;
    private String currentText;
    /// combo to query by corpus
    private JComboBox cbxTaxonomies1;
    private JTextField txtTaxonomies1;
    private Corpus currentTaxonomy1;
    private String currentText1;

    public QaReport(ITSAdministrator frame) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);
        setSize(700, 400);
        setTitle("QA Report");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);


        cbxTaxonomies = new JComboBox();
        cbxTaxonomies.setBounds(75, 12, 235, 25);
        cbxTaxonomies.setToolTipText("");
        cbxTaxonomies.addActionListener(this);
        cbxTaxonomies.setEditable(true);
        cbxTaxonomies.getEditor().getEditorComponent().addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                currentTaxonomy = null;
                txtTaxonomies = (JTextField) cbxTaxonomies.getEditor().getEditorComponent();
                if (!txtTaxonomies.getText().equals(currentText)
                        && !(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    currentText = txtTaxonomies.getText();
                    comboFilter(txtTaxonomies.getText());
                }
            }

            public void keyTyped(KeyEvent e) {
            }
        });


        cbxTaxonomies1 = new JComboBox();
        cbxTaxonomies1.setBounds(300, 335, 235, 25);
        cbxTaxonomies1.setToolTipText("");
        cbxTaxonomies1.addActionListener(this);
        cbxTaxonomies1.setEditable(true);
        cbxTaxonomies1.getEditor().getEditorComponent().addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                currentTaxonomy1 = null;
                txtTaxonomies1 = (JTextField) cbxTaxonomies1.getEditor().getEditorComponent();
                if (!txtTaxonomies1.getText().equals(currentText1)
                        && !(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    currentText1 = txtTaxonomies1.getText();
                    comboFilter1(txtTaxonomies1.getText());
                }
            }

            public void keyTyped(KeyEvent e) {
            }
        });

        JLabel lblCorpusId = new JLabel("CorpusId");
        lblCorpusId.setBounds(8, 12, 117, 25);
        btnGenerate = new JButton("Generate Report");
        btnGenerate.addActionListener(this);
        btnGenerate.setBounds(400, 12, 150, 25);
        getContentPane().add(cbxTaxonomies);
        getContentPane().add(lblCorpusId);
        getContentPane().add(btnGenerate);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(8, 45, 670, 279);
        getContentPane().add(scrollPane);

        table = new JTable();
        modelTable = new DefaultTableModel(
                new Object[][]{},
                new String[]{
            "REQUESTID", "DATE", "CORPUS", "STATUS"
        });
        table.setModel(modelTable);

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
                setCursor(cursor);
                tableMouseClicked(evt);
                cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                setCursor(cursor);
            }
        });

        scrollPane.setViewportView(table);
        lblRowNum = new JLabel("Number rows to get");
        lblRowNum.setBounds(30, 335, 150, 25);
        txtRowNum = new JTextField(30);
        txtRowNum.setBounds(170, 335, 50, 25);
        JLabel lblCorpusQuery = new JLabel("CorpusId");
        lblCorpusQuery.setBounds(230, 335, 117, 25);


        btnReport = new JButton("Run");
        btnReport.setBounds(550, 335, 117, 25);
        btnReport.addActionListener(this);
        getContentPane().add(lblRowNum);
        getContentPane().add(txtRowNum);
        getContentPane().add(lblCorpusQuery);
        getContentPane().add(cbxTaxonomies1);
        getContentPane().add(btnReport);
        //getContentPane().add(scrollPane);

        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);


        loadListTaxonomy();
        loadCbxTaxonomy1();
        loadCbxTaxonomy();

    }

    private void loadData() {
        String corpusId;
        if (cbxTaxonomies1.getSelectedItem() instanceof Corpus) {
            currentTaxonomy1 = (Corpus) cbxTaxonomies1.getSelectedItem();
            currentText1 = currentTaxonomy1.getName();
            corpusId = currentTaxonomy1.getID();
        } else {
            corpusId = "";
        }

        try {
            org.dom4j.Document nodes = ITSframe.its.getQAReport(txtRowNum.getText(),
                    corpusId);
            JAXBContext jc = JAXBContext.newInstance(WrapperListQaReport.class);
            StringReader reader = new StringReader(nodes.getRootElement()
                    .element(new QName("REPORT")).asXML());

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            WrapperListQaReport wikiReport = (WrapperListQaReport) unmarshaller.unmarshal(reader);

            modelTable = new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                "REQUESTID", "DATE", "CORPUS", "STATUS"
            }) {
                boolean[] canEdit = new boolean[]{
                    false, false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };

            table.setModel(modelTable);

            if (wikiReport.getList() != null) {
                Date date;
                for (QaReportModel node : wikiReport.getList()) {
                    date = new Date(Long.parseLong(node.getTimestamp()));
                    modelTable.addRow(new Object[]{node.getRequestId(),
                        date.toString(),
                        node.getCorpus(),
                        node.getStatus()});
                }
            } else {
                JOptionPane.showMessageDialog(this, "Not found process");
            }
        } catch (Exception e) {
            System.out.println("Error reading wikipedia report" + e.getMessage());
        }
    }

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            //String nodeid = null;
            int rowindex = table.getSelectedRow();
            //nodeid = table.getModel().getValueAt(rowindex, 0).toString();
            //editNode(nodeid);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnReport)) {
            loadData();
        } else if (e.getSource().equals(btnGenerate)) {
            runQaReport();
        }
    }

    private void runQaReport() {
        if (cbxTaxonomies.getSelectedItem() instanceof Corpus) {
            currentTaxonomy = (Corpus) cbxTaxonomies.getSelectedItem();
            currentText = currentTaxonomy.getName();
            its.startQAReport(currentTaxonomy.getID());
        } else {
            JOptionPane.showMessageDialog(this, "Select taxonomy");
        }
    }

    public void loadListTaxonomy() {
        try {
            listData = its.getCorporaList();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    private void loadCbxTaxonomy() {
        try {
            for (Corpus item : listData) {
                item.setName(item.getName().trim());
                cbxTaxonomies.addItem(item);
            }
            if (cbxTaxonomies.getSelectedItem() instanceof Corpus) {
                currentTaxonomy = (Corpus) cbxTaxonomies.getSelectedItem();
                currentText = currentTaxonomy.getName();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    private void loadCbxTaxonomy1() {
        try {
            for (Corpus item : listData) {
                item.setName(item.getName().trim());
                cbxTaxonomies1.addItem(item);
            }
            if (cbxTaxonomies1.getSelectedItem() instanceof Corpus) {
                currentTaxonomy1 = (Corpus) cbxTaxonomies1.getSelectedItem();
                currentText1 = currentTaxonomy1.getName();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public void comboFilter(String enteredText) {
        if (enteredText.equals("")) {
            cbxTaxonomies.setModel(new DefaultComboBoxModel(listData.toArray()));
            cbxTaxonomies.setSelectedItem("");
            cbxTaxonomies.showPopup();
            return;
        }

        List<Corpus> newList = new ArrayList<Corpus>();
        int size = listData.size();
        Corpus item = null;
        String name = null;

        for (int i = 0; i < size; i++) {
            item = listData.get(i);
            name = item.getName().toLowerCase().trim();
            if (name.length() >= enteredText.length()) {
                if (name.substring(0, enteredText.length()).equals(enteredText.toLowerCase().trim())) {
                    newList.add(item);
                }
            }
        }

        if (newList.size() > 0) {
            cbxTaxonomies.removeAllItems();
            cbxTaxonomies.setModel(new DefaultComboBoxModel(newList.toArray()));
            cbxTaxonomies.setSelectedItem(enteredText);
            cbxTaxonomies.showPopup();
        } else {
            currentTaxonomy = null;
            cbxTaxonomies.hidePopup();
        }
    }

    public void comboFilter1(String enteredText) {
        if (enteredText.equals("")) {
            cbxTaxonomies1.setModel(new DefaultComboBoxModel(listData.toArray()));
            cbxTaxonomies1.setSelectedItem("");
            cbxTaxonomies1.showPopup();
            return;
        }

        List<Corpus> newList = new ArrayList<Corpus>();
        int size = listData.size();
        Corpus item = null;
        String name = null;

        for (int i = 0; i < size; i++) {
            item = listData.get(i);
            name = item.getName().toLowerCase().trim();
            if (name.length() >= enteredText.length()) {
                if (name.substring(0, enteredText.length()).equals(enteredText.toLowerCase().trim())) {
                    newList.add(item);
                }
            }
        }

        if (newList.size() > 0) {
            cbxTaxonomies1.removeAllItems();
            cbxTaxonomies1.setModel(new DefaultComboBoxModel(newList.toArray()));
            cbxTaxonomies1.setSelectedItem(enteredText);
            cbxTaxonomies1.showPopup();
        } else {
            currentTaxonomy1 = null;
            cbxTaxonomies1.hidePopup();
        }
    }
}
