/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Nov 6, 2003
 * Time: 1:40:38 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.iw.system.*;
import com.oculustech.layout.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import com.iw.tools.ConfClient;
import com.iw.tools.SwingWorker;
import com.iw.tools.TextFilterList;
import com.iw.tools.mq.util.RestMqUtil;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class OpenTaxonomy extends JPanel implements ActionListener,
        ListSelectionListener, KeyListener, Runnable {

    public int iButtonPressed;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    // Instance attributes used in this example
    protected JInternalFrame MDIframe;
    protected JPanel openPanel;
    private JPanel searchPanel;
    protected JList listbox;
    protected Vector listData;
    protected JButton newButton, openButton, cancelButton, copyButton,
            deleteButton, propsButton, buildSite;
    protected JTextField dataField;
    protected TextFilterList txtSearch;
    protected JScrollPane scrollPane;
    private DefaultListModel modelList;
    private static int currentTaxonomiesOpen = 0;

    public OpenTaxonomy() throws Exception {
    }

    public OpenTaxonomy(ITSAdministrator frame) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Open Taxonomy", false, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(710, 250);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        // Create the data model for this example
        modelList = new DefaultListModel();
        listData = frame.its.getCorpora();
        //saveTaxonomyLocalFile("/home/andres/taxonomy.data");
        //listData = getTaxonomyLocalFile("/home/andres/taxonomy.data");
        listbox = new JList(modelList);
        for (Object c : listData) {
            modelList.addElement(c);
        }

        listbox.setFixedCellWidth(540);
        listbox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // listbox.addKeyListener(this);
        // listbox.setSelectedIndex(0);


        newButton = new JButton("New");
        newButton.setMnemonic('n');
        newButton.addActionListener(this);

        this.txtSearch = new TextFilterList(this.modelList);


        openButton = new JButton("Open");
        openButton.setMnemonic('o');
        openButton.addActionListener(this);

        copyButton = new JButton("Copy");
        copyButton.setMnemonic('y');
        copyButton.addActionListener(this);

        propsButton = new JButton("Props");
        propsButton.setMnemonic('p');
        propsButton.addActionListener(this);

        this.buildSite = new JButton("Build Site");
        this.buildSite.setMnemonic('b');
        this.buildSite.addActionListener(this);

        deleteButton = new JButton("Delete");
        deleteButton.setMnemonic('d');
        deleteButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setMnemonic('c');
        cancelButton.addActionListener(this);

        scrollPane = new JScrollPane();
        scrollPane.getViewport().add(listbox);

        this.searchPanel = new JPanel();
        this.searchPanel.setLayout(new BorderLayout());
        this.searchPanel.add(new JLabel("Search:"), BorderLayout.WEST);
        this.searchPanel.add(this.txtSearch, BorderLayout.CENTER);

        layout.addSpace(10);
        layout.addBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(scrollPane, OculusLayout.NO_STRETCH,
                    OculusLayout.WANT_STRETCHED);
            layout.addSpace(15);
            layout.add(this.searchPanel);
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(newButton);
            layout.addSpace(2);
            layout.add(openButton);
            layout.addSpace(2);
            layout.add(propsButton);
            layout.addSpace(2);
            layout.add(buildSite);
            layout.addSpace(2);
            layout.add(copyButton);
            layout.addSpace(2);
            layout.add(deleteButton);
            layout.addSpace(2);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(5);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);
        MDIframe.getRootPane().setDefaultButton(openButton);
        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
        txtSearch.requestFocus();
    }

    public void valueChanged(ListSelectionEvent event) {
        // See if this is a listbox selection and the
        // event stream has settled
		/*
         * if (event.getSource() == listbox && !event.getValueIsAdjusting()) {
         * // Get the current selection and place it in the // edit field Corpus
         * c = (Corpus) listbox.getSelectedValue(); }
         */
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == cancelButton) {
            MDIframe.dispose();
        }
        if (event.getSource() == propsButton) {
            if (listbox.getSelectedValue() == null) {
                return;
            }

            CorpusProperties p = new CorpusProperties(ITSframe,
                    (Corpus) listbox.getSelectedValue());
            // MDIframe.dispose();
        }
        if (event.getSource().equals(this.buildSite)) {

            new Thread(this).start();
        }
        if (event.getSource() == newButton) {
            SwingWorker aWorker = new SwingWorker(event) {
                protected void doNonUILogic() {
                    ITSframe.setCursor(Cursor
                            .getPredefinedCursor(Cursor.WAIT_CURSOR));
                    MDIframe.getGlassPane().setVisible(true);

                    String sCorpusName = JOptionPane
                            .showInputDialog("Please enter the name of this new taxonomy.");
                    if (sCorpusName == null) {
                        return;
                    }

                    Corpus c = null;

                    try {
                        c = its.addCorpus(sCorpusName);
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                        return;
                    }

                    if (c == null) {
                        JOptionPane.showMessageDialog(ITSframe,
                                "Permission denied: Create Taxonomy.  See your system "
                                + "administrator for more details.",
                                "Information", JOptionPane.NO_OPTION);

                        return;
                    }

                    JDesktopPane jdesktoppane = ITSframe.jdp;

                    try {
                        MDIframe.dispose();

                        CorpusManagement DragAndDrop = new CorpusManagement(
                                ITSframe, c);

                        DragAndDrop.setBounds(50, 50, 850, 550);
                        DragAndDrop.setVisible(true);
                        DragAndDrop
                                .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                        jdesktoppane.add(DragAndDrop);
                        DragAndDrop.moveToFront();

                        jdesktoppane.setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                        System.err.println(e.getMessage());
                    }
                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    ITSframe.setCursor(Cursor
                            .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    MDIframe.getGlassPane().setVisible(false);
                }
            };
            aWorker.start();
        }

        if (event.getSource() == copyButton) {
            if (listbox.getSelectedValue() == null) {
                return;
            }

            try {
                CopyTaxPanel taxBox = new CopyTaxPanel(ITSframe,
                        (Corpus) listbox.getSelectedValue());
                taxBox.setVisible(true);
                MDIframe.dispose();
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
            }

        }

        if (event.getSource() == deleteButton) {
            if (listbox.getSelectedValue() == null) {
                return;
            }

            SwingWorker aWorker = new SwingWorker(event) {
                protected void doNonUILogic() {
                    ITSframe.setCursor(Cursor
                            .getPredefinedCursor(Cursor.WAIT_CURSOR));
                    MDIframe.getGlassPane().setVisible(true);

                    if (JOptionPane
                            .showConfirmDialog(
                            MDIframe,
                            "You have elected to delete this taxonomy and all of "
                            + "its topics.  Topics and corpora removed in this way are NOT recoverable.  Are you sure?",
                            "Confirm", JOptionPane.YES_NO_OPTION) != 0) {
                        return;
                    }

                    PopupProgressBar ppb = new PopupProgressBar(ITSframe, 0,
                            100);
                    ppb.show();

                    try {
                        its.deleteCorpus((Corpus) listbox.getSelectedValue(),
                                ppb);
                        // listbox.remove(listbox.getSelectedIndex());
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                        JOptionPane
                                .showMessageDialog(
                                ITSframe,
                                "Failure! An error was encountered during corpus deletion.",
                                "Error", JOptionPane.NO_OPTION);
                        return;
                    } finally {
                        ppb.dispose();
                    }

                    JOptionPane
                            .showMessageDialog(
                            ITSframe,
                            "The taxonomy that you selected have been removed successfully.",
                            "Information", JOptionPane.NO_OPTION);

                    // MDIframe.dispose();

                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    ITSframe.setCursor(Cursor
                            .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    MDIframe.getGlassPane().setVisible(false);
                }
            };
            aWorker.start();
        }

        if (event.getSource() == openButton) {
            if (listbox.getSelectedValue() == null) {
                JOptionPane.showMessageDialog(this, "Select taxonomy");
                return;
            }
            JDesktopPane jdesktoppane = ITSframe.jdp;

            try {
                ITSframe.setCursor(Cursor
                        .getPredefinedCursor(Cursor.WAIT_CURSOR));
                // MDIframe.dispose();

                CorpusManagement DragAndDrop = new CorpusManagement(ITSframe,
                        (Corpus) listbox.getSelectedValue());

                DragAndDrop.setBounds(0, 0, 1000, 600);
                DragAndDrop.setVisible(true);
                DragAndDrop
                        .setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                jdesktoppane.add(DragAndDrop);
                DragAndDrop.moveToFront();
                currentTaxonomiesOpen++;
                jdesktoppane.setVisible(true);

            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.err.println(e.getMessage());
            } finally {
                ITSframe.setCursor(Cursor
                        .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }

    public static int getCurrentTaxonomiesOpen() {
        return currentTaxonomiesOpen;
    }

    public static void setCloseTaxonomiesOpen() {
        currentTaxonomiesOpen -= 1;
    }

    public void keyPressed(KeyEvent evt) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent evt) {
        /*
         * char ch = evt.getKeyChar(); if (!Character.isLetterOrDigit(ch))
         * return;
         * 
         * String m_key = "";
         * 
         * m_key += Character.toLowerCase(ch);
         * 
         * int len = listbox.getModel().getSize();
         * 
         * for (int i = 0; i < len; i++) { String item = new String(
         * listbox.getModel().getElementAt(i).toString().toLowerCase()); if
         * (item.startsWith(m_key)) { listbox.setSelectedIndex(i);
         * listbox.ensureIndexIsVisible(i); break; }
         * 
         * } listbox.ensureIndexIsVisible(listbox.getSelectedIndex());
         */
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize = Toolkit
                .getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    @Override
    public void run() {
        Object tax = this.listbox.getSelectedValue();
        Corpus c = (Corpus) tax;
        StringBuffer sb = new StringBuffer();
        OutputStreamWriter wr = null;
        String data = null;
        URL url = null;;
        HttpURLConnection conn = null;

        if (this.listbox.getSelectedValue() == null) {
            return;
        }
        // logic to restful post
        try {

            sb.append("<BuildRequest>");
            sb.append("<corpusID>");
            sb.append(c.getID());
            sb.append("</corpusID>");
            RestMqUtil.getRestMqUtil().setInfoToClassifier();
            for (String classifier : RestMqUtil.getRestMqUtil()
                    .getClassifiers()) {
                sb.append("<ipAddress>");
                sb.append(classifier);
                sb.append("</ipAddress>");
            }
            sb.append("</BuildRequest>");

            data = sb.toString();

            // Send data
            url = new URL(ConfClient.getValue("BUILDER_URI"));
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setRequestProperty("Accept", "application/xml");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                JOptionPane.showMessageDialog(this, conn.getResponseCode() + ": " + conn.getResponseMessage());

            } else {
                JOptionPane.showMessageDialog(this, "Not conect with the builder");
            }

            /*
             * // Get the response BufferedReader rd = new BufferedReader(new
             * InputStreamReader( conn.getInputStream())); String line; while
             * ((line = rd.readLine()) != null) {
             * System.out.println(line.toString()); }
             */

            // rd.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Not conect with the builder");
            System.err.println("Class: OpenTaxonomy, Method: run() - Runnable");
        } finally {
            if (wr != null) {
                try {
                    wr.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }

    public Vector getTaxonomyLocalFile(String url) {
        try {
            FileInputStream file = new FileInputStream(url);
            ObjectInputStream input = new ObjectInputStream(file);
            return (Vector) input.readObject();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public void saveTaxonomyLocalFile(String uriFile) {
        try {
            FileOutputStream file;
            ObjectOutputStream output;
            file = new FileOutputStream(uriFile);
            output = new ObjectOutputStream(file);
            output.writeObject(listData);
            output.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
}
