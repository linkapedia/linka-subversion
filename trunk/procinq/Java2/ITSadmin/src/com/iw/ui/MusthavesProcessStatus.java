package com.iw.ui;

import com.iw.system.ITS;
import com.iw.system.MusthaveReportModel;
import com.iw.system.WrapperListMusthaveReport;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringReader;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.dom4j.QName;

public class MusthavesProcessStatus extends JInternalFrame implements ActionListener {

    private JTable table;
    private JTextField txtRowNum;
    private JTextField txtRequestId;
    private JLabel lblRowNum;
    private JButton btnReport;
    private DefaultTableModel modelTable;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");

    public MusthavesProcessStatus(ITSAdministrator frame) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);
        setSize(930, 400);
        setTitle("Musthaves Process Status");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(8, 12, 900, 279);
        getContentPane().add(scrollPane);

        table = new JTable();
        modelTable = new DefaultTableModel(
                new Object[][]{},
                new String[]{
            "REQUESTID", "DATE", "CORPUS", "NODE", "TOLEVEL", "STATUS"
        });
        table.setModel(modelTable);

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
                setCursor(cursor);
                cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                setCursor(cursor);
            }
        });

        scrollPane.setViewportView(table);
        lblRowNum = new JLabel("Number rows to get");
        lblRowNum.setBounds(30, 320, 117, 25);
        txtRowNum = new JTextField(30);
        txtRowNum.setBounds(150, 320, 117, 25);

        JLabel lblRequestId = new JLabel("RequestId:");
        lblRequestId.setBounds(290, 320, 117, 25);
        txtRequestId = new JTextField(30);
        txtRequestId.setBounds(370, 320, 117, 25);


        btnReport = new JButton("Run");
        btnReport.setBounds(520, 320, 117, 25);
        btnReport.addActionListener(this);
        getContentPane().add(lblRowNum);
        getContentPane().add(txtRowNum);
        getContentPane().add(lblRequestId);
        getContentPane().add(txtRequestId);
        getContentPane().add(btnReport);
        //getContentPane().add(scrollPane);

        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }

    private void loadData() {
        try {
            org.dom4j.Document nodes = ITSframe.its.getReportMusthaves(
                    txtRowNum.getText(),
                    txtRequestId.getText());
            JAXBContext jc = JAXBContext.newInstance(WrapperListMusthaveReport.class);
            StringReader reader = new StringReader(nodes.getRootElement().
                    element(new QName("REPORT")).asXML());

            Unmarshaller unmarshaller = jc.createUnmarshaller();
            WrapperListMusthaveReport imageReport =
                    (WrapperListMusthaveReport) unmarshaller.unmarshal(reader);

            modelTable = new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                "REQUESTID", "DATE", "CORPUS", "NODE", "TOLEVEL", "STATUS"
            }) {
                boolean[] canEdit = new boolean[]{
                    true, true, true, true, true, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };

            table.setModel(modelTable);

            if (imageReport.getList() != null) {
                Date date;
                for (MusthaveReportModel report : imageReport.getList()) {
                    date = new Date(Long.parseLong(report.getTimestamp()));
                    modelTable.addRow(new Object[]{report.getRequestId(),
                        date.toString(),
                        report.getCorpus(),
                        report.getNode(),
                        report.getToLevel(),
                        report.getStatus()
                    });
                }
            } else {
                JOptionPane.showMessageDialog(this, "Not found process");
            }
        } catch (Exception e) {
            System.out.println("Error reading wikipedia report" + e.getMessage());
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnReport)) {
            loadData();
        }
    }
}
