package com.iw.ui;

import com.iw.system.Corpus;
import com.iw.system.ITS;
import com.iw.system.ITSTreeNode;
import com.iw.system.ModelNodeReport;
import com.iw.system.ModelNodeReportDynamo;
import com.iw.system.ModelReport;
import com.iw.system.ModelReportDynamo;
import com.iw.system.ParamsQueryReportOracle;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.dom4j.QName;

public final class SignatureQA extends JInternalFrame implements ActionListener {

    private JTable table;
    private JButton btnRun;
    private JComboBox cbxTaxonomies;
    private JComboBox cbxReportType;
    private Corpus currentTaxonomy;
    private String currentText;
    private List<Corpus> listData;
    private JTextField txtTaxonomies;
    private DefaultTableModel modelTable;
    private JLabel lblLoading;
    private JLabel lblResultData;
    private Thread worker;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    public ImageIcon iIndraweb = com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif");
    //dialogs
    private DialogQAReport1 dialog1;
    private DialogQAReport2 dialog2;
    String[] reportList = {"Report - signatures / musthaves",
        "Report - Documents",
        "Report - Images"};

    public SignatureQA(ITSAdministrator frame) {

        setResizable(false);
        setClosable(true);
        setIconifiable(true);

        setTitle("QA Report");
        setLocation(60, 60);
        setSize(870, 500);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        cbxTaxonomies = new JComboBox();
        cbxTaxonomies.setBounds(28, 32, 235, 24);
        cbxTaxonomies.setToolTipText("");
        cbxTaxonomies.addActionListener(this);
        cbxTaxonomies.setEditable(true);
        cbxTaxonomies.getEditor().getEditorComponent().addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                currentTaxonomy = null;
                txtTaxonomies = (JTextField) cbxTaxonomies.getEditor().getEditorComponent();
                if (!txtTaxonomies.getText().equals(currentText)
                        && !(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    currentText = txtTaxonomies.getText();
                    comboFilter(txtTaxonomies.getText());
                }
            }

            public void keyTyped(KeyEvent e) {
            }
        });

        getContentPane().add(cbxTaxonomies);

        JLabel lblReportType = new JLabel("Report type:");
        lblReportType.setBounds(285, 33, 103, 30);
        getContentPane().add(lblReportType);

        cbxReportType = new JComboBox(reportList);
        cbxReportType.setSelectedIndex(0);
        cbxReportType.setBounds(370, 35, 235, 24);
        cbxReportType.addActionListener(this);
        getContentPane().add(cbxReportType);

        btnRun = new JButton("Run");
        btnRun.setBounds(717, 35, 100, 24);
        btnRun.addActionListener(this);
        getContentPane().add(btnRun);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(28, 112, 662, 339);
        getContentPane().add(scrollPane);

        table = new JTable();
        /*modelTable = new DefaultTableModel(
         new Object[][]{},
         new String[]{
         "Node Id", "Node Name", "Signatures", "Must Haves"
         });
         table.setModel(modelTable);*/

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
                setCursor(cursor);
                tableMouseClicked(evt);
                cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                setCursor(cursor);
            }
        });

        lblLoading = new JLabel("loading...", com.iw.tools.ImageUtils.getImage(this, "itsimages/loading.gif"), JLabel.CENTER);
        lblLoading.setBounds(717, 170, 117, 38);
        lblLoading.setVisible(false);
        getContentPane().add(lblLoading);

        JLabel lblResult = new JLabel("Count Items:");
        lblResult.setBounds(717, 112, 117, 25);
        lblResultData = new JLabel();
        lblResultData.setBounds(717, 126, 117, 25);
        lblResultData.setText("___");
        getContentPane().add(lblResult);
        getContentPane().add(lblResultData);


        scrollPane.setViewportView(table);
        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);

        loadListTaxonomy();
    }

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            String nodeid = null;
            int rowindex = table.getSelectedRow();
            nodeid = table.getModel().getValueAt(rowindex, 0).toString();
            editNode(nodeid);
        }

    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == btnRun) {
            if (cbxTaxonomies.getSelectedItem() instanceof Corpus) {
                currentTaxonomy = (Corpus) cbxTaxonomies.getSelectedItem();
                currentText = currentTaxonomy.getName();
                int index = cbxReportType.getSelectedIndex();
                switch (index) {
                    case 0:
                        dialog1 = new DialogQAReport1(ITSframe, this);
                        dialog1.setVisible(true);
                        break;
                    case 1:
                        dialog2 = new DialogQAReport2(ITSframe, this);
                        dialog2.setVisible(true);
                        break;
                    case 2:
                        startReport3();
                        break;
                    default:
                        JOptionPane.showMessageDialog(this,
                                "Please select report type");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Select taxonomy");
            }
        }
    }

    public void startReport1() {
        worker = new Thread() {
            @Override
            public void run() {
                loadDataReport1();
            }
        };
        worker.start();
    }

    public void startReport2() {
        worker = new Thread() {
            @Override
            public void run() {
                loadDataReport2();
            }
        };
        worker.start();
    }

    public void startReport3() {
        worker = new Thread() {
            @Override
            public void run() {
                loadDataReport3();
            }
        };
        worker.start();
    }

    public void loadDataReport1() {
        try {
            ParamsQueryReportOracle json = new ParamsQueryReportOracle();
            Map<String, String> parameters = dialog1.getParameters();
            json.setLessSignature(parameters.get("LESSSIGNATURES"));
            json.setMoreSignature(parameters.get("MORESIGNATURES"));
            json.setLessMusthave("0");
            json.setMoreMusthave(parameters.get("MOREMUSTHAVES"));

            lblLoading.setVisible(true);
            org.dom4j.Document nodes = ITSframe.its.getNodesReport(
                    currentTaxonomy.getID(),
                    "1",
                    json);
            JAXBContext jc = JAXBContext.newInstance(ModelReport.class);
            StringReader reader = new StringReader(nodes.getRootElement().
                    element(new QName("REPORT")).asXML());
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            ModelReport nodesReport = (ModelReport) unmarshaller.unmarshal(reader);
            modelTable = new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                "Node Id", "Node Title", "Signatures", "Must Haves"
            }) {
                boolean[] canEdit = new boolean[]{
                    false, false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };

            table.setModel(modelTable);

            lblResultData.setText(nodesReport.getNodeCount());

            if (nodesReport.getNodes()
                    != null) {
                for (ModelNodeReport node : nodesReport.getNodes()) {
                    modelTable.addRow(new Object[]{node.getNodeId(),
                        node.getNodeTitle(),
                        node.getNodeSignatures(),
                        node.getNodeMusthaves()});
                }
            } else {
                JOptionPane.showMessageDialog(this, "Not results found!");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            lblLoading.setVisible(false);
        }
    }

    public void loadDataReport2() {
        try {
            ParamsQueryReportOracle json = new ParamsQueryReportOracle();
            Map<String, String> parameters = dialog2.getParameters();
            json.setLessDocuments(parameters.get("LESSDOCUMENTS"));
            json.setMoreDocuments(parameters.get("MOREDOCUMENTS"));

            lblLoading.setVisible(true);
            org.dom4j.Document nodes = ITSframe.its.getNodesReport(
                    currentTaxonomy.getID(),
                    "2",
                    json);

            JAXBContext jc = JAXBContext.newInstance(ModelReportDynamo.class);
            StringReader reader = new StringReader(nodes.getRootElement().
                    element(new QName("REPORT")).asXML());
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            ModelReportDynamo nodesReport = (ModelReportDynamo) unmarshaller.unmarshal(reader);

            modelTable = new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                "Node Id", "Node Title", "Documents"
            }) {
                boolean[] canEdit = new boolean[]{
                    false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };

            table.setModel(modelTable);
            lblResultData.setText(nodesReport.getNodeCount());

            if (nodesReport.getNodes() != null) {
                for (ModelNodeReportDynamo node : nodesReport.getNodes()) {
                    modelTable.addRow(new Object[]{node.getNodeId(), node.getNodeTitle(), node.getDocuments()});
                }
            } else {
                JOptionPane.showMessageDialog(this, "Not results found!");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            lblLoading.setVisible(false);
        }
    }

    public void loadDataReport3() {
        try {
            ParamsQueryReportOracle json = new ParamsQueryReportOracle();
            lblLoading.setVisible(true);
            org.dom4j.Document nodes = ITSframe.its.getNodesReport(
                    currentTaxonomy.getID(),
                    "3",
                    json);

            JAXBContext jc = JAXBContext.newInstance(ModelReportDynamo.class);
            StringReader reader = new StringReader(nodes.getRootElement().
                    element(new QName("REPORT")).asXML());
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            ModelReportDynamo nodesReport = (ModelReportDynamo) unmarshaller.unmarshal(reader);

            modelTable = new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                "Node Id", "Node Title", "Images"
            }) {
                boolean[] canEdit = new boolean[]{
                    false, false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            };

            table.setModel(modelTable);
            lblResultData.setText(nodesReport.getNodeCount());

            if (nodesReport.getNodes() != null) {
                for (ModelNodeReportDynamo node : nodesReport.getNodes()) {
                    modelTable.addRow(new Object[]{node.getNodeId(), node.getNodeTitle(), node.getImages()});
                }
            } else {
                JOptionPane.showMessageDialog(this, "Not results found!");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            lblLoading.setVisible(false);
        }
    }

    public void loadListTaxonomy() {
        try {
            listData = its.getCorporaList();
            //listData = new ArrayList<Corpus>();

            for (Corpus item : listData) {
                item.setName(item.getName().trim());
                cbxTaxonomies.addItem(item);
            }
            if (cbxTaxonomies.getSelectedItem() instanceof Corpus) {
                currentTaxonomy = (Corpus) cbxTaxonomies.getSelectedItem();
                currentText = currentTaxonomy.getName();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public void comboFilter(String enteredText) {
        if (enteredText.equals("")) {
            cbxTaxonomies.setModel(new DefaultComboBoxModel(listData.toArray()));
            cbxTaxonomies.setSelectedItem("");
            cbxTaxonomies.showPopup();
            return;
        }

        List<Corpus> newList = new ArrayList<Corpus>();
        int size = listData.size();
        Corpus item = null;
        String name = null;

        for (int i = 0; i < size; i++) {
            item = listData.get(i);
            name = item.getName().toLowerCase().trim();
            if (name.length() >= enteredText.length()) {
                if (name.substring(0, enteredText.length()).equals(enteredText.toLowerCase().trim())) {
                    newList.add(item);
                }
            }
        }

        if (newList.size() > 0) {
            cbxTaxonomies.removeAllItems();
            cbxTaxonomies.setModel(new DefaultComboBoxModel(newList.toArray()));
            cbxTaxonomies.setSelectedItem(enteredText);
            cbxTaxonomies.showPopup();
        } else {
            currentTaxonomy = null;
            cbxTaxonomies.hidePopup();
        }
    }

    public List<Object> getAllItemsComboBox(JComboBox combobox) {
        List<Object> temp = new ArrayList<Object>();
        int size = combobox.getItemCount();

        for (int i = 0; i < size; i++) {
            temp.add(combobox.getItemAt(i));
        }
        return temp;
    }

    public void editNode(String nodeid) {
        Vector vNodes = new Vector();
        Hashtable htCorpus = new Hashtable();

        try {
            vNodes = ITSframe.its.topicSearchNode(nodeid);
            ITSTreeNode n = (ITSTreeNode) vNodes.get(0);

            Corpus c = currentTaxonomy;
            JDesktopPane jdesktoppane = ITSframe.jdp;
            CorpusManagement DragAndDrop = null;

            try {
                DragAndDrop = new CorpusManagement(ITSframe, c, n);
            } catch (Exception ex) {
                System.out.println("Could not create corpus management frame.");
                ex.printStackTrace(System.out);
                return;
            }

            DragAndDrop.setBounds(50, 50, 850, 550);
            DragAndDrop.setVisible(true);
            DragAndDrop.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            jdesktoppane.add(DragAndDrop);
            DragAndDrop.moveToFront();

            jdesktoppane.setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
}
