/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Apr 5, 2004
 * Time: 5:52:59 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.ui;

import com.oculustech.layout.OculusBox;
import com.oculustech.layout.OculusLayout;
import com.oculustech.layout.OculusLayoutHelper;
import com.iw.system.Corpus;
import com.iw.tools.SwingWorker;

import javax.swing.event.ListSelectionListener;
import javax.swing.*;
import java.awt.event.KeyListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

public class TaxonomyReport extends OpenTaxonomy implements ActionListener {
    private Component glass;
    private String toolName;

    public TaxonomyReport() throws Exception {
    }

    public TaxonomyReport(ITSAdministrator frame, String Name) throws Exception {
        toolName = Name;

        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Taxonomy Report", false, true, true, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(375, 180);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);
        glass = MDIframe.getGlassPane();

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        // Create the data model for this example
        listData = frame.its.getCorpora();

        listbox = new JList(listData);
        listbox.setFixedCellWidth(240);
        listbox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listbox.addKeyListener(this);
        listbox.setSelectedIndex(0);

        openButton = new JButton("Open");
        openButton.setMnemonic('o');
        openButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setMnemonic('c');
        cancelButton.addActionListener(this);

        scrollPane = new JScrollPane();
        scrollPane.getViewport().add(listbox);

        layout.addSpace(15);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(scrollPane, OculusLayout.NO_STRETCH, OculusLayout.WANT_STRETCHED);
            layout.addSpace(15);
            layout.parent();
        }
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.add(openButton);
            layout.addSpace(2);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(5);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(openButton);
        MDIframe.setContentPane(ob);
        MDIframe.show();
    }

    // Handler for button presses
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == cancelButton) {
            MDIframe.dispose();
        }
        if (event.getSource() == openButton) {
            if (listbox.getSelectedValue() == null) {
                return;
            }

            SwingWorker aWorker = new SwingWorker(event) {
                JDesktopPane jdesktoppane = ITSframe.jdp;

                protected void doNonUILogic() {
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    glass.setVisible(true);

                    try {
                        TaxonomyChart tri =
                                new TaxonomyChart(ITSframe, (Corpus) listbox.getSelectedValue(), toolName);
                        ITSframe.jdp.add(tri);
                        tri.setBounds(10, 30, 900, 580);
                        tri.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        tri.setVisible(true);
                        tri.moveToFront();
                        tri.show();

                        MDIframe.dispose();
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                        System.err.println(e.getMessage());
                    }
                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    glass.setVisible(false);
                }
            };
            aWorker.start();
        }
    }
}