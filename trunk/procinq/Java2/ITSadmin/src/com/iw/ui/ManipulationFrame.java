package com.iw.ui;

import com.iw.system.*;
import com.iw.ui.CorpusManagement;
import com.oculustech.layout.*;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

import com.iw.tools.*;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataContainer;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;

public class ManipulationFrame extends JPanel {
    public ITS its = null;
    public ITSAdministrator ITSframe = null;

    // Instance attributes used in this example
    protected JInternalFrame MDIframe;
    private JPanel openPanel;

    // tools
    public JSortTable resultsTable = new JSortTable();

    protected JButton addButton;
    protected JButton removeButton;
    protected JButton closeButton;
    protected JButton saveButton;

    public ManipulationFrame(ITSAdministrator frame) throws Exception {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        JDesktopPane jdesktoppane = frame.jdp;

        String sAbstractIndicator = "";

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Folder Management Tool", true, true, true, true);
        MDIframe.setLocation(80, 80);
        MDIframe.setSize(675, 380);
        MDIframe.setBackground(Color.lightGray);
        its = frame.its;
        ITSframe = frame;

        MDIframe.setFrameIcon(ITSframe.iIndraweb);

        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        saveButton = new JButton("Save"); saveButton.setEnabled(false);
        addButton = new JButton("Add");
        removeButton = new JButton("Remove");
        closeButton = new JButton("Close");

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                {
                    layout.add(new JScrollPane(resultsTable));
                    layout.nestBox(OculusLayout.VERTICAL);
                    {
                        layout.add(addButton);
                        layout.addSpace(10);
                        layout.add(removeButton);
                        layout.addSpace(10);
                        layout.add(saveButton);
                        layout.addSpace(10);
                        layout.add(closeButton);
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.getRootPane().setDefaultButton(saveButton);
        MDIframe.setContentPane(ob);
        MDIframe.toFront();
        MDIframe.setVisible(true);
    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public class CellRenderer extends JLabel implements TableCellRenderer {

        private LineBorder _selectBorder;
        private EmptyBorder _emptyBorder;
        private Dimension _dim;

        public CellRenderer() {
            super();
            _emptyBorder = new EmptyBorder(1, 2, 1, 2);
            _selectBorder = new LineBorder(Color.BLACK);
            //setOpaque(true);
            setHorizontalAlignment(SwingConstants.CENTER);
        };

        /**
         *
         * Method defining the renderer to be used
         * when drawing the cells.
         *
         */
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row, int column) {

            Object s = value;

            //setFont(_cellFont);
            setText(s.toString());
            setForeground(Color.BLACK);
            setBackground(Color.WHITE);

            if (isSelected) {
                setForeground(Color.BLACK);
                setBackground(Color.BLUE);
                setBorder(_selectBorder);

            } else {
                setBorder(_emptyBorder);
            }

            return this;
        }

    }
}
