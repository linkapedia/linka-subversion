package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.oculustech.layout.*;
import com.iw.system.*;

public class MustHavePanel2 extends JPanel implements ActionListener {

    protected JLabel operation;
    protected JTextField txtToLevel;
    protected JTextField txtSelectedWordInNodeTitle;
    protected JTextField txtPersonalizedMusthaves;
    protected JButton buttonApply;
    protected JButton buttonCancel;
    private JInternalFrame MDIframe;
    private ITSAdministrator itsadmin;
    private ITS its;
    private ITSTreeNode Node;

    public MustHavePanel2(ITSAdministrator itsFrame, ITSTreeNode n) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        its = itsFrame.its;
        Node = n;
        itsadmin = itsFrame;

        MDIframe = new JInternalFrame("Must Have Rules 2", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(450, 140);
        MDIframe.setBackground(Color.lightGray);
        MDIframe.setResizable(false);
        MDIframe.setFrameIcon(itsFrame.iIndraweb);
        JDesktopPane jdesktoppane = itsFrame.jdp;
        setBackground(Color.gray);
        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);
        operation = new JLabel("From current selection To level:");
        operation.setToolTipText("Choose the depth.");
        txtToLevel = new JTextField("2", 5);
        txtSelectedWordInNodeTitle = new JTextField();
        txtSelectedWordInNodeTitle.setToolTipText("1=first word, 2=2do word, ... -1=last word");
        txtPersonalizedMusthaves = new JTextField();
        buttonApply = new JButton("Apply");
        buttonApply.setToolTipText("Apply changes");
        buttonApply.addActionListener(this);
        buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(this);
        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.nestGrid(2, 14);
                    {
                        layout.add(operation);
                        layout.add(txtToLevel);
                        layout.add(new JLabel("Select word to get"));
                        layout.add(txtSelectedWordInNodeTitle);
                        layout.add(new JLabel("Add custom musthave"));
                        layout.add(txtPersonalizedMusthaves);
                        layout.addSpace(10);
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(buttonApply);
            layout.addSpace(10);
            layout.add(buttonCancel);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
    }

    public void openAtUpperCenter() {
        Dimension winsize = MDIframe.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        MDIframe.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(buttonCancel)) {
            MDIframe.dispose();
            return;
        }

        if (e.getSource().equals(buttonApply)) {
            String depth = txtToLevel.getText();
            String wordToPut = txtSelectedWordInNodeTitle.getText();
            String personalizedMusthave = txtPersonalizedMusthaves.getText();

            try {
                MDIframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                its.addMusthaves2(
                        Node.get("NODEID"),
                        depth,
                        wordToPut,
                        personalizedMusthave);
                JOptionPane.showMessageDialog(itsadmin, "Finished ok", "Information",
                        JOptionPane.NO_OPTION);
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
                JOptionPane.showMessageDialog(itsadmin, "Could not add terms: "
                        + ex.getMessage(), "Error", JOptionPane.NO_OPTION);
            }finally{
                MDIframe.setCursor(Cursor.getDefaultCursor());
            }
        }
        MDIframe.dispose();
    }
}
