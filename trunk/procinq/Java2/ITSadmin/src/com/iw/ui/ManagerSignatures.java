/*
 * check the signatures
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Ago 16, 2011, 11:56 AM
*/
package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ManagerSignatures extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8878209945664113217L;
	
	private JList listSignatures;
	private DefaultListModel modelListSignatures;
	private JButton btnAdd,
					btnRemove,
					btnSave;
	private JScrollPane panelList;
	private JPanel panelButtons;
	
	private ArrayList<String> sigList= new ArrayList<String>();
	
	private ArrayList<String> sigToAdd;
	private ArrayList<String> sigToRemove;
	
	private boolean banChange;
	
	/**
	 * Constructor
	 * @param list : array to view into JList
	 */
	public ManagerSignatures(ArrayList<String> list){
		this.sigList=list;
		showWindow();
	
		
		
	}
	/**
	 * load array to JList
	 */
	private void configureWindows(){
		sigToAdd = new ArrayList<String>();
		sigToRemove = new ArrayList<String>();
		for(int i=0;i<sigList.size();i++)
			modelListSignatures.addElement(sigList.get(i));
	}
	
	/**
	 * show window to administre signatures
	 */
	private void showWindow(){
		//Windows
		banChange=false;
		setTitle("Manager Signatures");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				
		//Create controls
		modelListSignatures = new DefaultListModel();
		listSignatures = new JList(modelListSignatures);
		btnAdd = new JButton(com.iw.tools.ImageUtils.getImage(this, "itsimages/plus.gif"));
		btnRemove= new JButton(com.iw.tools.ImageUtils.getImage(this, "itsimages/minus.gif"));
		btnSave= new JButton(com.iw.tools.ImageUtils.getImage(this, "itsimages/saveAction.gif"));
		panelList= new JScrollPane(listSignatures);
		panelButtons= new JPanel();
				
		//configure controls
				
		panelList.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panelButtons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panelButtons.setLayout(new GridLayout(1,4));
		btnAdd.setToolTipText("Add signatures to the dictionary");
		btnRemove.setToolTipText("Remove signatures to the taxonomy");
		btnSave.setToolTipText("Save changes");
		configureWindows();
			
		//events
		
		btnAdd.addActionListener(this);
		btnRemove.addActionListener(this);
		btnSave.addActionListener(this);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {	
				setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				if(banChange){
					
					
					int res= JOptionPane.showConfirmDialog(null, "Save changes?");
					if(res==JOptionPane.OK_OPTION){
								
					}else if(res==JOptionPane.NO_OPTION){
						sigToAdd=null;
						sigToRemove=null;
					}else if(res==JOptionPane.CANCEL_OPTION){
						setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
						sigToAdd=null;
						sigToRemove=null;
						for(int k=0;k<modelListSignatures.getSize();k++)
							modelListSignatures.removeElementAt(k);
						configureWindows();		
					}
					banChange=false;
				}
			}
		});
				
		//add conrols
		panelButtons.add(btnAdd);
		panelButtons.add(btnRemove);
		panelButtons.add(btnSave);
				
		getContentPane().add(panelList,BorderLayout.CENTER);
		getContentPane().add(panelButtons,BorderLayout.NORTH);
				
		setSize(new Dimension(400,400));
		openAtCenter();
		
		setModal(true);
		setResizable(false);
		setVisible(true);
	}
	/**
	 * share is array with other class
	 * @return
	 */
	public ArrayList<String> getSigToAdd(){
		return sigToAdd;
	}
	/**
	 * share is array with other class
	 * @return
	 */
	public ArrayList<String> getSigToRemove(){
		return sigToRemove;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(btnAdd)){
			banChange=true;
			Object[] sig = listSignatures.getSelectedValues();
			for(Object signature:sig){
				sigToAdd.add((String)signature);
			}
            
            for(Object s : sig){
            	for (int k=0;k<modelListSignatures.getSize();k++){
                	if(modelListSignatures.get(k).equals(s)){
                		modelListSignatures.remove(k);
                		break;
                	}
                }
            }
            listSignatures.revalidate();
			
			
		}if(e.getSource().equals(btnRemove)){
			banChange=true;
			Object[] sig = listSignatures.getSelectedValues();
			for(Object signature:sig){
				sigToRemove.add((String)signature);
			}
			
            for(Object s : sig){
            	for (int k=0;k<modelListSignatures.getSize();k++){
                	if(modelListSignatures.get(k).equals(s)){
                		modelListSignatures.remove(k);
                		break;
                	}
                }
            }
            listSignatures.revalidate();
			
		}if(e.getSource().equals(btnSave)){
			banChange=false;
		}
		
	}
    private void openAtCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 2);
    }

	
}
