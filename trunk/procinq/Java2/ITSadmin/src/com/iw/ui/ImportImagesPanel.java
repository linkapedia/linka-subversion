package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.oculustech.layout.*;
import com.iw.system.*;
import org.apache.commons.lang.StringUtils;

public class ImportImagesPanel extends JPanel implements ActionListener {

    protected JLabel operation;
    protected JCheckBox withWikipediaImageChk;
    protected JTextField toLevelTxt;
    protected JButton buttonApply;
    protected JButton buttonCancel;
    private JInternalFrame MDIframe;
    private ITSAdministrator itsadmin;
    private ITS its;
    private ITSTreeNode Node;

    public ImportImagesPanel(ITSAdministrator itsFrame, ITSTreeNode n) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        its = itsFrame.its;
        Node = n;
        itsadmin = itsFrame;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Imasges Import Panel", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(300, 140);
        MDIframe.setBackground(Color.lightGray);
        MDIframe.setResizable(false);

        MDIframe.setFrameIcon(itsFrame.iIndraweb);

        JDesktopPane jdesktoppane = itsFrame.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        withWikipediaImageChk = new JCheckBox("Wikipedia");
        withWikipediaImageChk.setSelected(true);
        withWikipediaImageChk.setToolTipText("Search first in wikipedia article");

        toLevelTxt = new JTextField();
        toLevelTxt.setToolTipText("1=current node, 2=children, ... -1=all recursively");

        buttonApply = new JButton("Run");
        buttonApply.addActionListener(this);

        buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.nestGrid(2, 14);
                    {
                        layout.add(withWikipediaImageChk);
                        layout.addSpace(10);
                        layout.add(new JLabel("To level"));
                        layout.add(toLevelTxt);
                        layout.parent();
                    }
                    layout.addSpace(10);
                    layout.parent();
                }
                layout.parent();
            }
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(buttonApply);
            layout.addSpace(10);
            layout.add(buttonCancel);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.setVisible(true);
    }

    public void openAtUpperCenter() {
        Dimension winsize = MDIframe.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        MDIframe.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(buttonCancel)) {
            MDIframe.dispose(); // close the window when the action is taken
            return;
        }

        if (e.getSource().equals(buttonApply)) {
            String toLevelStr = toLevelTxt.getText();
            boolean withWikipediaImage = withWikipediaImageChk.isSelected();
            if (!IsNumericTolevel(toLevelStr)) {
                JOptionPane.showMessageDialog(itsadmin, "Tolevel should be a number");
                return;
            }
            int toLevel = Integer.parseInt(toLevelStr);
            if (!IsLevelInRange(toLevel)) {
                JOptionPane.showMessageDialog(itsadmin, "Tolevel not allowed, please put a correct value");
                return;
            }
            try {
                String requestid = its.queueImageProcess(
                        Node.get("CORPUSID"), Node.get("NODEID"), toLevel,
                        withWikipediaImage);
                if (requestid != null) {
                    JOptionPane.showMessageDialog(null, "RequestId: "
                            + requestid);
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Error processing request");
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                        "Exceptionh processing request");
            }
        }
        MDIframe.dispose(); // close the window when the action is taken
    }

    private static boolean IsLevelInRange(int tolevel) {
        return (tolevel < -1 || tolevel == 0) ? false : true;
    }

    public static boolean IsNumericTolevel(String str) {
        return str.matches("-?\\d+(.\\d+)?");
    }
}
