package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.ui.ITSAdministrator;
import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.guiservershared.externalexplain.ExplainDisplayDataInstanceTABLE;

public class RefreshNodeProgressBar extends JPanel implements PropertyChangeListener {
    protected JProgressBar refreshProgress;
    protected RowColorJTable resultsTable;

    // window attributes
    private JInternalFrame MDIframe;
    private ITS its; private ITSAdministrator itsadmin;

    // refresh parameters
    private ITSTreeNode Node;
    private boolean titlesOnly;
    private String repositoryID;
    private int maxDocuments;
    private boolean bDone = false;

    public RefreshNodeProgressBar(ITSAdministrator itsFrame, ITSTreeNode n,
                                  boolean bTitlesOnly, String sRepositoryID, int iMaxDocuments) {
        OculusBox ob = new OculusBox();
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        its = itsFrame.its; itsadmin = itsFrame;

        Node = n; titlesOnly = bTitlesOnly; repositoryID = sRepositoryID; maxDocuments = iMaxDocuments;

        // the pop up thesaurus manager is an internal MDI frame
        MDIframe = new JInternalFrame("Refresh Progress", true, true, false, true);
        MDIframe.setLocation(60, 60);
        MDIframe.setSize(575, 455);
        MDIframe.setBackground(Color.lightGray);

        MDIframe.setFrameIcon(itsFrame.iIndraweb);

        JDesktopPane jdesktoppane = itsFrame.jdp;
        setBackground(Color.gray);

        OculusLayoutHelper layout = new OculusLayoutHelper(ob);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        refreshProgress = new JProgressBar();
        refreshProgress.setToolTipText("Completion status of refresh topic.");
        refreshProgress.setBorderPainted (true);
        refreshProgress.setStringPainted(true);

        resultsTable = new RowColorJTable(2, ExplainDisplayDataInstanceTABLE.ICOMPARE_EQUALS.intValue(), (Object) new String(""));
        resultsTable.setToolTipText("Documents are displayed as the topic is refreshed.");

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(refreshProgress,OculusLayout.WANT_STRETCHED,OculusLayout.NO_STRETCH);
            layout.add(new JScrollPane(resultsTable));
            layout.addSpace(10);
            layout.parent();
        }
        layout.addSpace(10);

        openAtUpperCenter();
        jdesktoppane.add(MDIframe);

        MDIframe.setContentPane(ob);
        MDIframe.show();

        itsFrame.addPropertyChangeListener(this);
        itsFrame.setResizable(false);
        itsFrame.removePropertyChangeListener(this);
        itsFrame.setResizable(true);

    }

    public void openAtUpperCenter() {
        Dimension winsize = this.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((screensize.width - winsize.width) / 2,
                (screensize.height - winsize.height) / 3);
    }

    // applyNodeRefresh
    public void propertyChange(PropertyChangeEvent event) {
        SwingWorker aWorker = new SwingWorker(event) {
            private boolean bSuccess = true;
            private String failMessage = "";

            protected void doNonUILogic() {
                try { its.applyNodeRefresh(Node, titlesOnly, maxDocuments, repositoryID); }
                catch (Exception e) {
                    e.printStackTrace(System.out); failMessage = e.getMessage(); bSuccess = false;
                } finally { bDone = true; }
            }
            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                itsadmin.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                itsadmin.getGlassPane().setVisible(false);

                if (bSuccess) {
                    JOptionPane.showMessageDialog(itsadmin, "Topic refreshed successfully.",
                        "Information", JOptionPane.NO_OPTION);
                    MDIframe.dispose();
                } else {
                    JOptionPane.showMessageDialog(itsadmin, "Could not refresh topic: "+failMessage+".",
                        "Error", JOptionPane.NO_OPTION);
                    MDIframe.dispose();
                }
            }
        };
        aWorker.start();

        Thread t = new Thread() {
            public void run() {
                try {
                    int loop = 0;
                    while (!bDone) {
                        System.out.println("Sleeping for 5 seconds..");
                        Thread.currentThread().sleep(5000); loop++;
                        Vector vNodeDocs = its.CQL("SELECT <NODEDOCUMENT> WHERE NODEID = "+Node.get("NODEID")+
                            " AND SCORE1 > 0 ORDER BY SCORE1 DESC");
                        System.out.println("Attempt "+loop+" found "+vNodeDocs.size()+" documents.");

                        loadResults(resultsTable, vNodeDocs);
                        refreshProgress.setValue(vNodeDocs.size());
                    }
                    refreshProgress.setValue(100);
                } catch (Exception e) { e.printStackTrace(System.out); System.out.println("Early abort!"); }
            }
        };
        t.start();
    }

    private void loadResults(JTable table, Vector vDocs) {
        DefaultSortTableModel dtm = new DefaultSortTableModel(vDocs.size(), 3);

        for (int i = 0; i < vDocs.size(); i++) {
            NodeDocument nd = (NodeDocument) vDocs.elementAt(i);
            com.iw.system.Document d = nd.getDocProps();
            dtm.setValueAt(d, i, 0);
            dtm.setValueAt(nd.get("DOCSUMMARY"), i, 1);
            dtm.setValueAt(nd.get("SCORE1"), i, 2);
        }
        Vector v = new Vector();
        v.add("Document Title");
        v.add("Gist");
        v.add("Score");
        dtm.setColumnIdentifiers(v);

        table.setModel(dtm);
    }
}
