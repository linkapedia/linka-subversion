package com.iw.ui;

import com.oculustech.layout.OculusBox;
import com.oculustech.layout.OculusLayout;
import com.iw.system.*;
import com.iw.tools.JSortTable;
import com.iw.tools.DefaultSortTableModel;
import com.iw.tools.SwingWorker;
import com.iw.guiservershared.TableModelXMLBeanSerializable;

import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;

public class ReportCheckPanel extends ReportCheckPanelLayout
        implements ActionListener, MouseListener {
    private JInternalFrame jifMDIFrameThesReport;
    public ITSAdministrator itsAdminFrame = null;
    private Component glass;

    Vector vCorpora = null;

    public ReportCheckPanel(ITSAdministrator itsAdminFrame) throws Exception {
        super();

        this.itsAdminFrame = itsAdminFrame;

        // fill taxonomy drop down
        vCorpora = itsAdminFrame.its.getCorpora();
        Enumeration enumCorp = vCorpora.elements();
        while (enumCorp.hasMoreElements()) {
            jComboBoxTax.addItem(enumCorp.nextElement());
        }
        jComboBoxTax.addItem("All");
        jComboBoxTax.setSelectedItem("All");

        // fill thesaurus drop down
        Vector vThes = itsAdminFrame.its.getThesauri();
        Enumeration enumThes = vThes.elements();
        while (enumThes.hasMoreElements()) {
            jComboBoxThes.addItem(enumThes.nextElement());
        }
        jComboBoxThes.addItem("All");
        jComboBoxThes.setSelectedItem("All");
    }

    public void invokeReportCheckPanelLayout() {
        //OculusBox ob = new OculusBox();
        //OculusLayout.setLicenseNumber("JPE5AQEMAQMP");

        // the pop up thesaurus manager is an internal MDI frame
        jifMDIFrameThesReport = new JInternalFrame("Thesaurus Coverage Report", true, true, true, true);
        jifMDIFrameThesReport.setLocation(60, 60);
        jifMDIFrameThesReport.setSize(675, 500);
        jifMDIFrameThesReport.setBackground(Color.lightGray);

        jifMDIFrameThesReport.setFrameIcon(itsAdminFrame.iIndraweb);
        glass = jifMDIFrameThesReport.getGlassPane();

        //JDesktopPane jdesktoppane = itsAdminFrame.jdp;
        //setBackground(Color.gray);
        show();
        setVisible(true);
        itsAdminFrame.jdp.add(jifMDIFrameThesReport);
        jifMDIFrameThesReport.setVisible(true);
        jifMDIFrameThesReport.show(true);
        jifMDIFrameThesReport.toFront();
        jifMDIFrameThesReport.getContentPane().add(this);
        jifMDIFrameThesReport.getRootPane().setDefaultButton(jbSearch);
        jbSearch.addActionListener(this);
        jbClose.addActionListener(this);
        jSortTable_OVERRIDE.addMouseListener(this);

    }

    // Handler for button presses
    public void actionPerformed(ActionEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {

                    if (e.getSource().equals(jbSearch)) {

                        // CORPUSID
                        String sProp_CorpusID = null;
                        if (jComboBoxTax.getSelectedItem().equals("All")) {
                            //vNodes = its.topicSearch(queryText.getText(), iSearchType);
                        } else {
                            Corpus c = (Corpus) jComboBoxTax.getSelectedItem();
                            sProp_CorpusID = c.getID() + "";
                        }

                        // THESAURUSID
                        String sProp_ThesID = null;
                        if (!jComboBoxThes.getSelectedItem().equals("All")) {
                            Thesaurus t = (Thesaurus) jComboBoxThes.getSelectedItem();
                            sProp_ThesID = t.getID();
                        }

                        try {
                            Hashtable htArgs = new Hashtable();
                            htArgs.put("BEANXML", "true");
                            if (sProp_CorpusID != null)
                                htArgs.put("CorpusID", sProp_CorpusID);
                            if (sProp_ThesID != null)
                                htArgs.put("ThesaurusID", sProp_ThesID);

                            TableModelXMLBeanSerializable tableModelSQLData =
                                    (TableModelXMLBeanSerializable) itsAdminFrame.its.tsThesaurusNodeReport(htArgs);
                            DefaultSortTableModel dstm = new DefaultSortTableModel(
                                    tableModelSQLData.getOArrArrData(), tableModelSQLData.getColNames());
                            dstm.setEditable(false);
                            jSortTable_OVERRIDE.setModel(dstm);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ;

                    } else if (e.getSource().equals(jbClose)) {
                        jifMDIFrameThesReport.dispose();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                try {

                    if (((MouseEvent) e).isShiftDown() ||
                            ((MouseEvent) e).isControlDown() ||
                            ((MouseEvent) e).isAltDown()) {
                        return;
                    }
                    Point p = ((MouseEvent) e).getPoint();
                    JSortTable jSortTableCast = (JSortTable) jSortTable_OVERRIDE;
                    int row = jSortTableCast.rowAtPoint(p);

                    if (((MouseEvent) e).getClickCount() == 2) {
                        Corpus c = null;
                        ITSTreeNode n = null;
                        try {

                            String sCQL = "SELECT <NODE> WHERE NODEID = '" +
                                    jSortTableCast.getModel().getValueAt(row, 3) + "'";
                            Vector vNodes = itsAdminFrame.its.CQL(sCQL);
                            n = (ITSTreeNode) vNodes.elementAt(0);

                            //System.out.println(n.getNodeDesc());

                            String sCorpusName = (String) jSortTableCast.getModel().getValueAt(row, 0);
                            for (int i = 0; i < vCorpora.size(); i++) {
                                Corpus ctest = (Corpus) vCorpora.elementAt(i);
                                String sTestName = ctest.getName();
                                //System.out.println(i+ ". sCorpusName [" + sCorpusName + "] compare to sTestName  [" + sTestName  + "]" );
                                if (sTestName.trim().equals(sCorpusName)) {
                                    c = ctest;
                                    break;
                                }

                            }
                            if (c == null) {
                                JOptionPane.showMessageDialog(itsAdminFrame, "Corpus not found in list [" + sCorpusName + "]", "Information", JOptionPane.NO_OPTION);
                                throw new Exception("Corpus not found in list [" + sCorpusName + "]");
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }

                        JDesktopPane jdesktoppane = itsAdminFrame.jdp;
                        CorpusManagement DragAndDrop = null;
                        try {
                            DragAndDrop = new CorpusManagement(itsAdminFrame, c, n);
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(itsAdminFrame, "Could not create corpus management window corpus [" + c.getID() + "] corpus [" + c.getName() + "] nodetitle [" + n.get("NODETITLE") + "]", "Information", JOptionPane.NO_OPTION);
                            System.out.print("Could not create corpus management window corpus [" + c.getID() + "] corpus [" + c.getName() + "] nodetitle [" + n.get("NODETITLE") + "]");
                            ex.printStackTrace(System.out);
                            return;
                        }

                        DragAndDrop.setBounds(50, 50, 850, 550);
                        DragAndDrop.setVisible(true);
                        DragAndDrop.setDefaultCloseOperation(
                                WindowConstants.DISPOSE_ON_CLOSE);

                        jdesktoppane.add(DragAndDrop);
                        DragAndDrop.moveToFront();

                        jdesktoppane.setVisible(true);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();

    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }
}
