package com.iw.ui;

import com.iw.system.ITSTreeNode;
import com.iw.tools.SwingWorker;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.io.*;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
 
import javax.swing.*;



public class SearchReplace extends JDialog implements ActionListener {
    private JTable table;
    public JFileChooser chooser = new JFileChooser();
    private SpringLayout springLayout;
    private ITSAdministrator ITSframe;
    private ITSTreeNode parent;
    private Component glass;

    final JButton okButton = new JButton();
    final JButton cancelButton = new JButton();
    final JSeparator separator = new JSeparator();
    //final JButton saveButton = new JButton();
    //final JButton loadButton = new JButton();
    final JCheckBox searchAndReplaceCheckBox = new JCheckBox();
    final JCheckBox searchAndReplaceCheckBox_1 = new JCheckBox();

    private PopupProgressBar ppb;
    private CorpusManagement cm;



    /**
     * Create the dialog
     */
    public SearchReplace(ITSTreeNode n, ITSAdministrator itsframe) {
        super();
        ITSframe = itsframe; parent = n;
        setTitle("Search and Replace");
        springLayout = new SpringLayout();
        getContentPane().setLayout(springLayout);
        setBounds(100, 100, 500, 325);
        

        okButton.addActionListener(this);
        getContentPane().add(okButton);
        springLayout.putConstraint(SpringLayout.SOUTH, okButton, 40, SpringLayout.NORTH, getContentPane());
        okButton.setText("    OK   ");

        cancelButton.addActionListener(this);
        cancelButton.setText("Cancel");
        getContentPane().add(cancelButton);
        springLayout.putConstraint(SpringLayout.SOUTH, cancelButton, 70, SpringLayout.NORTH, getContentPane());

        final JScrollPane scrollPane = new JScrollPane();
        getContentPane().add(scrollPane);
        springLayout.putConstraint(SpringLayout.EAST, scrollPane, -127, SpringLayout.EAST, getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, scrollPane, 23, SpringLayout.WEST, getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, scrollPane, -70, SpringLayout.SOUTH, getContentPane());
        springLayout.putConstraint(SpringLayout.NORTH, scrollPane, 0, SpringLayout.NORTH, okButton);

        String[] columnNames = {"Search Text", "Replace Text"};
       
        Object[][] data = { { "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" },{ "", "" }, { "", "" }, { "", "" }, { "", "" }, { "", "" }, { "", "" }, { "", "" }, { "", "" }, { "", "" }, { "", "" } };
        

        table = new JTable(data, columnNames);
        table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.setName("SearchReplaceTable");
      
               
        ExcelAdapter myAd = new ExcelAdapter(table);
        
        
        // mh -- copy paste II
        table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK),
        		TransferHandler.getCopyAction().getValue(Action.NAME));

        
        
        scrollPane.setViewportView(table);

        getContentPane().add(separator);
        springLayout.putConstraint(SpringLayout.WEST, separator, 360, SpringLayout.WEST, getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, separator, -60, SpringLayout.SOUTH, getContentPane());

      //  saveButton.addActionListener(this);
       // saveButton.setText("Save");
        //getContentPane().add(saveButton);
        springLayout.putConstraint(SpringLayout.EAST, okButton, -10, SpringLayout.EAST, getContentPane());
        //springLayout.putConstraint(SpringLayout.WEST, okButton, 0, SpringLayout.WEST, saveButton);
        springLayout.putConstraint(SpringLayout.EAST, cancelButton, -10, SpringLayout.EAST, getContentPane());
        //springLayout.putConstraint(SpringLayout.WEST, cancelButton, 0, SpringLayout.WEST, saveButton);
        //springLayout.putConstraint(SpringLayout.NORTH, saveButton, 80, SpringLayout.NORTH, getContentPane());
        
        
        //final JButton loadButton = new JButton();
        //loadButton.addActionListener(this);
        //loadButton.setText("Load");
        //getContentPane().add(loadButton);
        //springLayout.putConstraint(SpringLayout.EAST, loadButton, -2, SpringLayout.EAST, getContentPane());
        //springLayout.putConstraint(SpringLayout.WEST, loadButton, -82, SpringLayout.EAST, getContentPane());
        //springLayout.putConstraint(SpringLayout.EAST, saveButton, -2, SpringLayout.EAST, getContentPane());
        //springLayout.putConstraint(SpringLayout.WEST, saveButton, 0, SpringLayout.WEST, loadButton);
        //springLayout.putConstraint(SpringLayout.NORTH, loadButton, 115, SpringLayout.NORTH, getContentPane());

        searchAndReplaceCheckBox.setText("Search and Replace through Node Titles");
        getContentPane().add(searchAndReplaceCheckBox);
        springLayout.putConstraint(SpringLayout.WEST, searchAndReplaceCheckBox, 30, SpringLayout.WEST, getContentPane());

        searchAndReplaceCheckBox_1.setText("Search and Replace through Node Source");
        getContentPane().add(searchAndReplaceCheckBox_1);
        springLayout.putConstraint(SpringLayout.SOUTH, searchAndReplaceCheckBox_1, 23, SpringLayout.SOUTH, searchAndReplaceCheckBox);
        springLayout.putConstraint(SpringLayout.NORTH, searchAndReplaceCheckBox_1, 0, SpringLayout.SOUTH, searchAndReplaceCheckBox);
        springLayout.putConstraint(SpringLayout.EAST, searchAndReplaceCheckBox_1, 295, SpringLayout.WEST, searchAndReplaceCheckBox);
        springLayout.putConstraint(SpringLayout.WEST, searchAndReplaceCheckBox_1, 0, SpringLayout.WEST, searchAndReplaceCheckBox);

        final JSeparator separator_1 = new JSeparator();
        getContentPane().add(separator_1);
        springLayout.putConstraint(SpringLayout.NORTH, searchAndReplaceCheckBox, 5, SpringLayout.SOUTH, separator_1);
        springLayout.putConstraint(SpringLayout.EAST, separator_1, -10, SpringLayout.EAST, scrollPane);
        springLayout.putConstraint(SpringLayout.WEST, separator_1, 23, SpringLayout.WEST, getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, separator_1, -60, SpringLayout.SOUTH, getContentPane());
        // springLayout.putConstraint(SpringLayout.NORTH, separator_1, 220, SpringLayout.NORTH, getContentPane());
        //
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(cancelButton)) {
            this.dispose();
        }
        
        else if (e.getSource().equals(okButton)) {
            ppb = new PopupProgressBar(ITSframe, 0, 100);
            ppb.show(); glass = getGlassPane();
            table.getCellEditor().stopCellEditing();
            SwingWorker aWorker = new SwingWorker(e) {

                protected void doNonUILogic() {
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    glass.setVisible(true);

                    try {
                        ITSframe.its.searchAndReplace(parent, getHashFromTable(),
                            searchAndReplaceCheckBox.isSelected(),
                            searchAndReplaceCheckBox_1.isSelected(), ppb);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(ITSframe, "Your search and replace operation could not be completed.",
                            "Error", JOptionPane.NO_OPTION);
                        ex.printStackTrace(System.err);

                        return;
                    }

                    ppb.dispose();

                    JOptionPane.showMessageDialog(ITSframe, "Your search and replace operation has completed.",
                        "Information", JOptionPane.NO_OPTION);

                    dispose();
                }

                protected void doUIUpdateLogic() {
                }

                public void finished() {
                    super.finished();
                    glass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    glass.setVisible(false);
                }
            };
            aWorker.start();

        } else {
            JOptionPane.showMessageDialog(ITSframe, "That fuctionality has not been completed yet.",
                "Information", JOptionPane.NO_OPTION);
        }
    }
    
    public Hashtable getHashFromTable() {
        // table.getCellEditor().stopCellEditing();

        Hashtable ht = new Hashtable();
        for (int i = 0; i < table.getRowCount(); i++) {
            String search = (String) table.getValueAt(i, 0);
            String replace = (String) table.getValueAt(i, 1);

            if ((!search.equals("")) && (!replace.equals("")))
                ht.put((String) table.getValueAt(i, 0),
                       (String) table.getValueAt(i, 1));
        }

        return ht;
    }
}
