package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.util.Log;

import com.oculustech.layout.*;
import com.iw.system.*;
import com.iw.tools.*;
import com.iw.tools.SwingWorker;
import com.iw.tools.image.ui.Manager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CorpusProperties extends OculusBox implements ActionListener,
        FocusListener, ChangeListener, KeyListener {
    // Save/Cancel buttons

    private String strTerms;
    protected JButton saveButton;
    protected JButton cancelButton;
    // General tab
    protected JLabel nameLabel;
    protected JTextField nameText;
    protected JLabel descLabel;
    protected JLabel idLabel;
    protected JTextArea descText;
    protected JLabel taxonomyActive;
    protected JCheckBox activeCheck;
    protected JCheckBox corpusDomain;
    protected JCheckBox corpusToClassify;
    protected JTextField txtKismetricId;
    protected JTextField txtgoogleAnalyiticId;
    protected JTextField txtTagLine;
    protected JTextField txtDomainName;
    protected JTextField txtadSenseId;
    protected JLabel lblAffinity;
    protected JCheckBox chkAffinity;
    protected JCheckBox chkUseGoogleAds;
    protected JTextField txtGoogleSiteVerificationId;
    protected JComboBox comboCategory;
    // Folders tab
    protected JLabel activeFoldersLabel;
    protected SortedList activeFoldersList;
    protected JButton leftArrowButton;
    protected JButton rightArrowButton;
    protected JLabel availableFolderLabel;
    protected SortedList availableFoldersList;
    // Subject areas tab
    protected JLabel activeSubjectsLabel;
    protected SortedList activeSubjectsList;
    protected JButton leftArrowSubjectButton;
    protected JButton rightArrowSubjectButton;
    protected JLabel availableSubjectsLabel;
    protected SortedList availableSubjectsList;
    // Security ADMIN tab
    protected JLabel activeSecurityLabel;
    protected SortedList activeSecurityList;
    protected JButton leftArrowSecurityButton;
    protected JButton rightArrowSecurityButton;
    protected JLabel availableSecurityGroupsLabel;
    protected SortedList availableSecurityList;
    // Security READ tab
    protected JLabel activeReadSecurityLabel;
    protected SortedList activeReadSecurityList;
    protected JButton leftArrowReadSecurityButton;
    protected JButton rightArrowReadSecurityButton;
    protected JLabel availableReadSecurityGroupsLabel;
    protected SortedList availableReadSecurityList;
    // Thesaurus tab
    protected JLabel activeThesaurusLabel;
    protected SortedList activeThesaurusList;
    protected JButton leftArrowThesaurusButton;
    protected JButton rightArrowThesaurusButton;
    protected JLabel availableThesaurusGroupsLabel;
    protected SortedList availableThesaurusList;
    //images tab
    protected Manager panelImages;
    protected JButton uploadImageFileSystem;
    protected JButton uploadImageUrl;
    protected JButton deleteImage;
    protected JButton refreshImage;
    protected JButton btnImagesS3;
    // objects of interest
    protected JLabel lblTerms;
    protected JTextField txtTerms;
    protected DefaultListModel modelList;
    protected JList lstTerms;
    protected JButton btnAdd;
    private Corpus corpus;
    private ITSAdministrator ITSframe;
    private JInternalFrame frame;
    private Component glass;
    // track changes - don't want to save everything, every time
    boolean bGeneralChanged = false;
    boolean bFoldersChanged = false;
    boolean bSubjectsChanged = false;
    boolean bSecurityChanged = false;
    boolean bReadOnlyChanged = false;
    boolean bThesauriChanged = false;
    boolean bAffinityTerms = false;

    private void activaTerms() {
        this.txtTerms.setText("");
        this.txtTerms.setEnabled(true);
        this.lstTerms.setEnabled(true);
        this.btnAdd.setEnabled(true);
    }

    private void desactivaTerms() {
        this.txtTerms.setText("Enable affinity property");
        this.txtTerms.setEnabled(false);
        this.lstTerms.setEnabled(false);
        this.btnAdd.setEnabled(false);
    }

    public CorpusProperties(ITSAdministrator ITSframe, Corpus corpus) {
        this.strTerms = corpus.getTerms();
        this.corpus = corpus;
        this.ITSframe = ITSframe;

        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        frame = new JInternalFrame("Edit Taxonomy Properties (" + corpus.getID() + ")", false, true,
                true, false);
        // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setFrameIcon(ITSframe.iIndraweb);

        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        // GENERAL TAB **************************************
        nameLabel = new JLabel("Taxonomy Name:(" + corpus.getID() + ")");
        nameLabel.setToolTipText("Enter the name of this concept taxonomy.");

        nameText = new JTextField(corpus.getName());
        nameText.setToolTipText("Enter the name of this concept taxonomy.");
        nameText.addFocusListener(this);

        // idLabel = new JLabel("Taxonomy ID");
        // idLabel.setToolTipText("The Corpus ID of the Taxonomy");

        descLabel = new JLabel("Taxonomy Desc: ");
        descLabel
                .setToolTipText("Enter a description of this concept taxonomy.");

        descText = new JTextArea(corpus.getDescription());
        descText.setToolTipText("Enter a description of this concept taxonomy.");
        descText.setLineWrap(true);
        descText.setWrapStyleWord(true);
        descText.setColumns(10);
        descText.setRows(10);
        descText.addFocusListener(this);

        taxonomyActive = new JLabel("Taxonomy Active:");
        taxonomyActive
                .setToolTipText("Check this box if the taxonomy is active in the system.");

        activeCheck = new JCheckBox();

        if (corpus.getActive().equals("1")) {
            this.activeCheck.setSelected(true);
        } else {
            this.activeCheck.setSelected(false);
        }
        activeCheck.setToolTipText("Check this box if the taxonomy is active in the system.");
        activeCheck.addFocusListener(this);

        //checkbox domain
        corpusDomain = new JCheckBox();
        if (corpus.getDomain().equals("1")) {
            this.corpusDomain.setSelected(true);
        } else {
            this.corpusDomain.setSelected(false);
        }
        corpusDomain.setToolTipText("Check this box if the taxonomy is a domain in the system.");
        corpusDomain.addFocusListener(this);

        //checkbox toclassify
        corpusToClassify = new JCheckBox();
        if (corpus.getCorpusToClassify().equals("1")) {
            this.corpusToClassify.setSelected(true);
        } else {
            this.corpusToClassify.setSelected(false);
        }
        corpusToClassify.setToolTipText("Check this box if we need classify this corpus");
        corpusToClassify.addFocusListener(this);


        txtKismetricId = new JTextField(corpus.getKissmetricId());
        txtKismetricId.addFocusListener(this);
        txtadSenseId = new JTextField(corpus.getAdsenseId());
        txtadSenseId.addFocusListener(this);
        txtgoogleAnalyiticId = new JTextField(corpus.getGoogleAnalyticsId());
        txtgoogleAnalyiticId.addFocusListener(this);

        txtGoogleSiteVerificationId = new JTextField(corpus.getGoogleSiteVerificationId());
        txtGoogleSiteVerificationId.addFocusListener(this);

        txtTagLine = new JTextField(corpus.getTagLine());
        txtTagLine.addFocusListener(this);

        txtDomainName = new JTextField(corpus.getDomainName());
        txtDomainName.addFocusListener(this);

        lblAffinity = new JLabel("Affinity");
        lblAffinity.setToolTipText("Check this box to change Affinity status");

        this.chkAffinity = new JCheckBox();
        if (corpus.getAffinity().equals("1")) {
            this.chkAffinity.setSelected(true);
        } else {
            this.chkAffinity.setSelected(false);
        }
        this.chkAffinity
                .setToolTipText("Check this box to change Affinity status");
        this.chkAffinity.addFocusListener(this);
        this.chkAffinity.addChangeListener(this);

        java.util.List<CategoryDomain> categories = new ArrayList<CategoryDomain>();
        try {
            categories = ITSframe.its.getCategoriesDomain();
        } catch (Exception ex) {
            //error
        }

        this.comboCategory = new JComboBox<CategoryDomain>();
        this.comboCategory.addItem(new CategoryDomain("undefined", "undefined"));
        boolean found = false;
        for (CategoryDomain category : categories) {
            this.comboCategory.addItem(category);
            if (corpus.getCategory().equals(category.getName())) {
                this.comboCategory.setSelectedIndex(this.comboCategory.getItemCount() - 1);
                found = true;
            }
        }

        if (!found) {
            this.comboCategory.setSelectedIndex(0);
        }
        //select the current value
        this.comboCategory.addFocusListener(this);

        chkUseGoogleAds = new JCheckBox();
        if (corpus.getUseGoogleAds().equals("1")) {
            this.chkUseGoogleAds.setSelected(true);
        } else {
            this.chkUseGoogleAds.setSelected(false);
        }
        this.chkUseGoogleAds.addFocusListener(this);

        // FOLDERS TAB ***************************************
        activeFoldersLabel = new JLabel("Active Folders");
        activeFoldersLabel.setHorizontalAlignment(SwingConstants.CENTER);
        activeFoldersLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        activeFoldersLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));
        activeFoldersLabel
                .setToolTipText("Folders in this section are currently active for this taxonomy.");

        Vector vActive = new Vector();
        Vector vAvail = new Vector();
        try {
            vActive = ITSframe.its.getFolders(corpus);
            vAvail = preenActive(ITSframe.its.getFolders(), vActive);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        try {
            activeFoldersList = new SortedList(vActive);
            activeFoldersList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        activeFoldersList
                .setToolTipText("Folders in this section are currently active for this taxonomy.");

        leftArrowButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/left.gif"));
        leftArrowButton
                .setToolTipText("Move selected folders into the active state.");
        leftArrowButton.addActionListener(this);

        rightArrowButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/right.gif"));
        rightArrowButton
                .setToolTipText("Remove selected folders into the active state.");
        rightArrowButton.addActionListener(this);

        availableFolderLabel = new JLabel("Available Folders");
        availableFolderLabel.setHorizontalAlignment(SwingConstants.CENTER);
        availableFolderLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        availableFolderLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));
        availableFolderLabel
                .setToolTipText("Folders in this section are not active for this taxonomy.");

        try {
            availableFoldersList = new SortedList(vAvail);
            availableFoldersList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }

        availableFoldersList
                .setToolTipText("Folders in this section are not active for this taxonomy.");

        // SUBJECT AREAS TAB
        // **************************************************************
        vActive = new Vector();
        vAvail = new Vector();
        try {
            vActive = ITSframe.its.getSubjectAreas(corpus);
            vAvail = preenActive(ITSframe.its.getSubjectAreas(), vActive);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        activeSubjectsLabel = new JLabel("Active Subjects");
        activeSubjectsLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));
        activeSubjectsLabel
                .setToolTipText("Subject Areas in this section are currently active for this taxonomy.");

        try {
            activeSubjectsList = new SortedList(vActive);
            activeSubjectsList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        activeSubjectsList
                .setToolTipText("Subject Areas in this section are currently active for this taxonomy.");

        leftArrowSubjectButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/left.gif"));
        leftArrowSubjectButton
                .setToolTipText("Move selected folders into the active state.");
        leftArrowSubjectButton.addActionListener(this);

        rightArrowSubjectButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/right.gif"));
        rightArrowSubjectButton
                .setToolTipText("Remove selected folders into the active state.");
        rightArrowSubjectButton.addActionListener(this);

        availableSubjectsLabel = new JLabel("Available Subjects");
        availableSubjectsLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));
        availableSubjectsLabel
                .setToolTipText("Subject Areas in this section are not active for this taxonomy.");

        try {
            availableSubjectsList = new SortedList(vAvail);
            availableSubjectsList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        availableSubjectsList
                .setToolTipText("Subject Areas in this section are not active for this taxonomy.");

        // SECURITY TAB
        // **************************************************************
        vActive = new Vector();
        vAvail = new Vector();
        Vector vTotal = new Vector();
        try {
            vTotal = ITSframe.its.getSecurityGroups();
            vActive = ITSframe.its.getSecurityGroups(corpus, "1");
            vAvail = preenActive(vTotal, vActive);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        activeSecurityLabel = new JLabel("Granted Admin Access");
        activeSecurityLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        try {
            activeSecurityList = new SortedList(vActive);
            activeSecurityList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        activeSecurityList
                .setToolTipText("Security groups in this section are currently granted administrative access to this taxonomy.");

        leftArrowSecurityButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/left.gif"));
        leftArrowSecurityButton
                .setToolTipText("Move selected groups into the active state.");
        leftArrowSecurityButton.addActionListener(this);

        rightArrowSecurityButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/right.gif"));
        rightArrowSecurityButton
                .setToolTipText("Remove selected groups into the active state.");
        rightArrowSecurityButton.addActionListener(this);

        availableSecurityGroupsLabel = new JLabel("Available Security Groups");
        availableSecurityGroupsLabel.setFont(Font
                .decode("MS Sans Serif-BOLD-11"));
        availableSecurityGroupsLabel
                .setToolTipText("Security groups in this section are not active for this taxonomy.");

        try {
            availableSecurityList = new SortedList(vAvail);
            availableSecurityList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        availableSecurityList
                .setToolTipText("Security groups in this section are not active for this taxonomy.");

        // SECURITY READ ONLY
        // *****************************************************
        vActive = new Vector();
        vAvail = new Vector();
        try {
            vActive = ITSframe.its.getSecurityGroups(corpus, "0");
            vAvail = preenActive(vTotal, vActive);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        activeReadSecurityLabel = new JLabel("Granted Read Access");
        activeReadSecurityLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        try {
            activeReadSecurityList = new SortedList(vActive);
            activeReadSecurityList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        activeReadSecurityList
                .setToolTipText("Security groups in this section are currently granted read access to this taxonomy.");

        leftArrowReadSecurityButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/left.gif"));
        leftArrowReadSecurityButton
                .setToolTipText("Move selected groups into the active state.");
        leftArrowReadSecurityButton.addActionListener(this);

        rightArrowReadSecurityButton = new JButton(ImageUtils.getImage(
                ITSframe, "itsimages/right.gif"));
        rightArrowReadSecurityButton
                .setToolTipText("Remove selected groups into the active state.");
        rightArrowReadSecurityButton.addActionListener(this);

        availableReadSecurityGroupsLabel = new JLabel(
                "Available Security Groups");
        availableReadSecurityGroupsLabel.setFont(Font
                .decode("MS Sans Serif-BOLD-11"));
        availableReadSecurityGroupsLabel
                .setToolTipText("Security groups in this section are not active for this taxonomy.");

        try {
            availableReadSecurityList = new SortedList(vAvail);
            availableReadSecurityList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        availableReadSecurityList
                .setToolTipText("Security groups in this section are not active for this taxonomy.");

        // THESAURUS ONLY *****************************************************

        vActive = new Vector();
        vAvail = new Vector();

        try {
            vActive = ITSframe.its.getThesauri(corpus);
            vAvail = preenActive(ITSframe.its.getThesauri(), vActive);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        activeThesaurusLabel = new JLabel("Active Thesauri");
        activeThesaurusLabel.setFont(Font.decode("MS Sans Serif-BOLD-11"));

        try {
            activeThesaurusList = new SortedList(vActive);
            activeThesaurusList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        activeThesaurusList
                .setToolTipText("Thesauri in this section are currently applied to this taxonomy.");

        leftArrowThesaurusButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/left.gif"));
        leftArrowThesaurusButton
                .setToolTipText("Move selected groups into the active state.");
        leftArrowThesaurusButton.addActionListener(this);

        rightArrowThesaurusButton = new JButton(ImageUtils.getImage(ITSframe,
                "itsimages/right.gif"));
        rightArrowThesaurusButton
                .setToolTipText("Remove selected thesauri into the active state.");
        rightArrowThesaurusButton.addActionListener(this);

        availableThesaurusGroupsLabel = new JLabel("Available Thesauri");
        availableThesaurusGroupsLabel.setFont(Font
                .decode("MS Sans Serif-BOLD-11"));
        availableThesaurusGroupsLabel
                .setToolTipText("Thesauri in this section are not active for this taxonomy.");

        try {
            availableThesaurusList = new SortedList(vAvail);
            availableThesaurusList.sort();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ITSframe, e.getMessage(), "Error",
                    JOptionPane.NO_OPTION);
            e.printStackTrace(System.out);
            return;
        }
        availableThesaurusList
                .setToolTipText("Thesauri in this section are not active for this taxonomy.");

        saveButton = new JButton("Save");
        saveButton.setToolTipText("Save concept taxonomy properties.");
        saveButton.addActionListener(this);

        // tab taxonomy terms affinity **************************
        this.lblTerms = new JLabel("Terms");
        this.txtTerms = new JTextField(20);
        this.btnAdd = new JButton("Add");
        this.btnAdd.addActionListener(this);
        this.modelList = new DefaultListModel();
        this.lstTerms = new JList(this.modelList);

        String terms = corpus.getTerms();
        if ((terms != null) && (terms != "null")) {
            String[] termsSplit = terms.split(",");
            for (String s : termsSplit) {
                if (!s.equals("null")) {
                    this.modelList.addElement(s);
                }
            }

        }

        this.lstTerms.setSelectionMode(2);
        this.lstTerms.addKeyListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.setToolTipText("Cancel changes.");
        cancelButton.addActionListener(this);

        if (this.chkAffinity.isSelected()) {
            activaTerms();
        } else {
            desactivaTerms();
        }

        //images pane
        // tab ImageManager
        panelImages = new Manager();
        panelImages.Build();
        uploadImageFileSystem = new JButton("Upload Images (Local)");
        uploadImageFileSystem.addActionListener(this);

        uploadImageUrl = new JButton("Upload images (URL)");
        uploadImageUrl.addActionListener(this);
        deleteImage = new JButton("Delete Image");
        deleteImage.addActionListener(this);

        refreshImage = new JButton("Refresh");
        refreshImage.addActionListener(this);

        btnImagesS3 = new JButton("Save images in S3");
        btnImagesS3.addActionListener(this);

        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.addSpace(10);
                layout.nestBox(OculusLayout.HORIZONTAL,
                        OculusLayout.TOP_JUSTIFY);
                {
                    layout.addSpace(10);
                    layout.nestTabbedPane();
                    {
                        layout.nestBoxAsTab("General", OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestGrid(2, 14);
                            {
                                layout.add(nameLabel);
                                layout.add(nameText);
                                layout.add(descLabel);
                                layout.add(new JScrollPane(descText));
                                layout.add(taxonomyActive);
                                layout.add(this.activeCheck);
                                layout.add(new JLabel("Domain"));
                                layout.add(this.corpusDomain);
                                layout.add(new JLabel("To classify"));
                                layout.add(this.corpusToClassify);
                                layout.add(this.lblAffinity);
                                layout.add(this.chkAffinity);
                                layout.add(new JLabel("Use GoogleAds"));
                                layout.add(this.chkUseGoogleAds);
                                layout.add(new JLabel("kissmetricId:"));
                                layout.add(txtKismetricId);
                                layout.add(new JLabel("adsenseId:"));
                                layout.add(txtadSenseId);
                                layout.add(new JLabel("googleAnalyiticId:"));
                                layout.add(txtgoogleAnalyiticId);
                                layout.add(new JLabel("tagline"));
                                layout.add(txtTagLine);
                                layout.add(new JLabel("Domain Name"));
                                layout.add(txtDomainName);
                                layout.add(new JLabel("Google site verification id"));
                                layout.add(txtGoogleSiteVerificationId);
                                layout.add(new JLabel("Category:"));
                                layout.add(comboCategory);
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Folders", OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(activeFoldersLabel);
                                layout.add(new JScrollPane(activeFoldersList));
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.addSpace(10);
                                layout.add(leftArrowButton);
                                layout.addSpace(10);
                                layout.add(rightArrowButton);
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(availableFolderLabel);
                                layout.add(new JScrollPane(availableFoldersList));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Subject Areas",
                                OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(activeSubjectsLabel);
                                layout.add(new JScrollPane(activeSubjectsList));
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.addSpace(10);
                                layout.add(leftArrowSubjectButton);
                                layout.addSpace(10);
                                layout.add(rightArrowSubjectButton);
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(availableSubjectsLabel);
                                layout.add(new JScrollPane(
                                        availableSubjectsList));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Admin Security",
                                OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(activeSecurityLabel);
                                layout.add(new JScrollPane(activeSecurityList));
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.addSpace(10);
                                layout.add(leftArrowSecurityButton);
                                layout.addSpace(10);
                                layout.add(rightArrowSecurityButton);
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(availableSecurityGroupsLabel);
                                layout.add(new JScrollPane(
                                        availableSecurityList));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Read Security",
                                OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(activeReadSecurityLabel);
                                layout.add(new JScrollPane(
                                        activeReadSecurityList));
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.addSpace(10);
                                layout.add(leftArrowReadSecurityButton);
                                layout.addSpace(10);
                                layout.add(rightArrowReadSecurityButton);
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(availableReadSecurityGroupsLabel);
                                layout.add(new JScrollPane(
                                        availableReadSecurityList));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Thesaurus",
                                OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(activeThesaurusLabel);
                                layout.add(new JScrollPane(activeThesaurusList));
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.addSpace(10);
                                layout.add(leftArrowThesaurusButton);
                                layout.addSpace(10);
                                layout.add(rightArrowThesaurusButton);
                                layout.parent();
                            }
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.add(availableThesaurusGroupsLabel);
                                layout.add(new JScrollPane(
                                        availableThesaurusList));
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Affinity Terms", 0, 4);
                        {
                            layout.nestBox(1);
                            {
                                layout.add(this.lblTerms);
                                layout.add(this.txtTerms);
                                layout.add(new JScrollPane(this.lstTerms));
                                layout.parent();
                            }
                            layout.nestBox(1);
                            {
                                layout.addSpace(17);
                                layout.add(this.btnAdd);
                                layout.parent();
                            }
                            layout.nestBox(1);
                            {
                                layout.parent();
                            }
                            layout.parent();
                        }
                        layout.nestBoxAsTab("Images", OculusLayout.HORIZONTAL,
                                OculusLayout.TOP_JUSTIFY);
                        {
                            layout.nestBox(OculusLayout.VERTICAL);
                            {
                                layout.addSpace(10);
                                layout.nestBox(OculusLayout.HORIZONTAL,
                                        OculusLayout.TOP_JUSTIFY);
                                {
                                    layout.add(new JScrollPane(panelImages));
                                    layout.nestBox(OculusLayout.VERTICAL);
                                    {
                                        layout.add(uploadImageFileSystem);
                                        layout.addSpace(5);
                                        layout.add(uploadImageUrl);
                                        layout.addSpace(5);
                                        layout.add(deleteImage);
                                        layout.addSpace(5);
                                        layout.add(refreshImage);
                                        layout.addSpace(5);
                                        layout.add(btnImagesS3);

                                    }
                                    layout.parent();
                                }
                                layout.addSpace(10);
                                layout.parent();
                            }
                            layout.addSpace(10);
                            layout.parent();
                        }
                        layout.parent();
                    }
                    layout.parent();
                }
                layout.parent();
            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
            {
                layout.nestBox(OculusLayout.HORIZONTAL);
                {
                    layout.addSpace(10);
                    layout.add(saveButton);
                    layout.addSpace(10);
                    layout.add(cancelButton);
                    layout.parent();
                }
                layout.addSpace(10);
                layout.parent();
            }
            layout.parent();
            layout.addSpace(10);
        }
        frame.setLocation(10, 10);
        glass = frame.getGlassPane();

        frame.setPreferredSize(new Dimension(1000, 600));
        ITSframe.jdp.add(frame);

        frame.getRootPane().setDefaultButton(saveButton);

        frame.setContentPane(this);
        frame.setSize(1000, 600);
        frame.setVisible(true);
        refreshImagesPane();
    }

    public Vector preenActive(Vector v1, Vector v2) {
        Vector v = new Vector();

        for (int i = 0; i < v1.size(); i++) {
            boolean add = true;
            Object o = v1.elementAt(i);

            for (int j = 0; j < v2.size(); j++) {
                Object o2 = v2.elementAt(j);
                if (o.toString().equals(o2.toString())) {
                    add = false;
                }
            }

            if (add) {
                v.add(o);
            }
        }

        return v;
    }

    public void flagChanges(FocusEvent e) {
        // FLAG changes to any objects as requiring a SAVE action
        // GENERAL
        if ((e.getSource().equals(nameText))
                || (e.getSource().equals(descText))
                || (e.getSource().equals(this.activeCheck))
                || (e.getSource().equals(this.corpusDomain))
                || (e.getSource().equals(this.corpusToClassify))
                || (e.getSource().equals(this.txtKismetricId))
                || (e.getSource().equals(this.txtGoogleSiteVerificationId))
                || (e.getSource().equals(this.txtadSenseId))
                || (e.getSource().equals(this.txtgoogleAnalyiticId))
                || (e.getSource().equals(this.txtTagLine))
                || (e.getSource().equals(this.txtDomainName))
                || (e.getSource().equals(this.chkAffinity))
                || (e.getSource().equals(this.chkUseGoogleAds))
                || (e.getSource().equals(this.comboCategory))
                || (e.getSource().equals(this.btnAdd))
                || (e.getSource().equals(this.lstTerms))) {
            this.bGeneralChanged = true;
        }

        // FOLDERS
        if ((e.getSource().equals(activeFoldersList))
                || (e.getSource().equals(leftArrowButton))
                || (e.getSource().equals(rightArrowButton))
                || (e.getSource().equals(availableFoldersList))) {
            bFoldersChanged = true;
        }
        // SUBJECT AREAS
        if ((e.getSource().equals(activeSubjectsList))
                || (e.getSource().equals(leftArrowSubjectButton))
                || (e.getSource().equals(rightArrowSubjectButton))
                || (e.getSource().equals(availableSubjectsList))) {
            bSubjectsChanged = true;
        }
        // ADMIN SECURITY
        if ((e.getSource().equals(activeSecurityList))
                || (e.getSource().equals(leftArrowSecurityButton))
                || (e.getSource().equals(rightArrowSecurityButton))
                || (e.getSource().equals(availableSecurityList))) {
            bSecurityChanged = true;
        }
        // READ SECURITY
        if ((e.getSource().equals(activeReadSecurityList))
                || (e.getSource().equals(leftArrowReadSecurityButton))
                || (e.getSource().equals(rightArrowReadSecurityButton))
                || (e.getSource().equals(availableReadSecurityList))) {
            bReadOnlyChanged = true;
        }
        // THESAURI
        if ((e.getSource().equals(activeThesaurusList))
                || (e.getSource().equals(leftArrowThesaurusButton))
                || (e.getSource().equals(rightArrowThesaurusButton))
                || (e.getSource().equals(availableThesaurusList))) {
            bThesauriChanged = true;
        }

        if ((e.getSource().equals(this.btnAdd))
                || (e.getSource().equals(this.lstTerms))) {
            this.bAffinityTerms = true;
        }

        // System.out.println("general: "+bGeneralChanged+" folders: "+bFoldersChanged+
        // " subjects: "+bSubjectsChanged+" security: "+bSecurityChanged+
        // " read-only: "+bReadOnlyChanged+" thesauri: "+bThesauriChanged);
    }

    public void flagChanges(ActionEvent e) {
        // FLAG changes to any objects as requiring a SAVE action
        // GENERAL
        if ((e.getSource().equals(nameText))
                || (e.getSource().equals(descText))
                || (e.getSource().equals(this.activeCheck))
                || (e.getSource().equals(this.corpusDomain))
                || (e.getSource().equals(this.corpusToClassify))
                || (e.getSource().equals(this.txtKismetricId))
                || (e.getSource().equals(this.txtGoogleSiteVerificationId))
                || (e.getSource().equals(this.txtadSenseId))
                || (e.getSource().equals(this.txtgoogleAnalyiticId))
                || (e.getSource().equals(this.txtTagLine))
                || (e.getSource().equals(this.txtDomainName))
                || (e.getSource().equals(this.chkAffinity))
                || (e.getSource().equals(this.chkUseGoogleAds))
                || (e.getSource().equals(this.comboCategory))
                || (e.getSource().equals(this.btnAdd))
                || (e.getSource().equals(this.lstTerms))) {
            this.bGeneralChanged = true;
        }
        // FOLDERS
        if ((e.getSource().equals(activeFoldersList))
                || (e.getSource().equals(leftArrowButton))
                || (e.getSource().equals(rightArrowButton))
                || (e.getSource().equals(availableFoldersList))) {
            bFoldersChanged = true;
        }
        // SUBJECT AREAS
        if ((e.getSource().equals(activeSubjectsList))
                || (e.getSource().equals(leftArrowSubjectButton))
                || (e.getSource().equals(rightArrowSubjectButton))
                || (e.getSource().equals(availableSubjectsList))) {
            bSubjectsChanged = true;
        }
        // ADMIN SECURITY
        if ((e.getSource().equals(activeSecurityList))
                || (e.getSource().equals(leftArrowSecurityButton))
                || (e.getSource().equals(rightArrowSecurityButton))
                || (e.getSource().equals(availableSecurityList))) {
            bSecurityChanged = true;
        }
        // READ SECURITY
        if ((e.getSource().equals(activeReadSecurityList))
                || (e.getSource().equals(leftArrowReadSecurityButton))
                || (e.getSource().equals(rightArrowReadSecurityButton))
                || (e.getSource().equals(availableReadSecurityList))) {
            bReadOnlyChanged = true;
        }
        // THESAURI
        if ((e.getSource().equals(activeThesaurusList))
                || (e.getSource().equals(leftArrowThesaurusButton))
                || (e.getSource().equals(rightArrowThesaurusButton))
                || (e.getSource().equals(availableThesaurusList))) {
            bThesauriChanged = true;
        }

        // System.out.println("general: "+bGeneralChanged+" folders: "+bFoldersChanged+
        // " subjects: "+bSubjectsChanged+" security: "+bSecurityChanged+
        // " read-only: "+bReadOnlyChanged+" thesauri: "+bThesauriChanged);
    }

    public void saveProperties() {
        // save GENERAL properties if they changed
        if (bGeneralChanged) {
            corpus.setName(nameText.getText());
            corpus.setDescription(descText.getText());

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < this.modelList.getSize(); i++) {
                sb.append(this.modelList.getElementAt(i));
                if (i < this.modelList.getSize() - 1) {
                    sb.append(",");
                }
            }

            String s = sb.toString();
            if (s.isEmpty()) {
                s = "null";
            }
            this.corpus.setTerms(s);

            if (activeCheck.isSelected()) {
                corpus.setActive("1");
            } else {
                corpus.setActive("0");
            }

            if (corpusDomain.isSelected()) {
                corpus.setDomain("1");
            } else {
                corpus.setDomain("0");
            }

            if (corpusToClassify.isSelected()) {
                corpus.setCorpusClassify("1");
            } else {
                corpus.setCorpusClassify("0");
            }

            corpus.setKissmetricId(txtKismetricId.getText());
            corpus.setAdsenseId(txtadSenseId.getText());
            corpus.setGoogleAnalyticsId(txtgoogleAnalyiticId.getText());
            corpus.setGoogleSiteVerificationId(txtGoogleSiteVerificationId.getText());

            corpus.setTagLine(txtTagLine.getText());

            corpus.setDomainName(txtDomainName.getText());

            String affi = this.chkAffinity.isSelected() ? "1" : "0";
            this.corpus.setAffinity(affi);

            String useGoogleAds = this.chkUseGoogleAds.isSelected() ? "1" : "0";
            this.corpus.setUseGoogleAds(useGoogleAds);

            CategoryDomain selectedCategory = null;

            selectedCategory = (CategoryDomain) this.comboCategory.getModel().getSelectedItem();
            if (selectedCategory.getId().equals("undefined")) {
                corpus.setCategory(null);
            } else {
                corpus.setCategory(selectedCategory.getId());
            }

            // ensure length does not exceed maximum
            if (descText.getText().length() > 760) {
                corpus.setDescription(descText.getText().substring(0, 700));
            }

            try {
                ITSframe.its.editCorpusProps(corpus);
                corpus.setCategory(selectedCategory.getName());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                        "Error", JOptionPane.NO_OPTION);
                return;
            }
        }

        // save FOLDER properties if changed
        if (bFoldersChanged) {
            // 1) remove all folders
            try {
                ITSframe.its.removeCorpusFolder(corpus);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                        "Error", JOptionPane.NO_OPTION);
                return;
            }

            // 2) add new folders back one at a time
            DefaultListModel dlm = (DefaultListModel) availableFoldersList
                    .getModel();
            for (int i = 0; i < activeFoldersList.getModel().getSize(); i++) {
                Genre genre = (Genre) activeFoldersList.getModel()
                        .getElementAt(i);

                try {
                    ITSframe.its.addCorpusFolder(corpus, genre);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                            "Error", JOptionPane.NO_OPTION);
                    return;
                }
            }
        }

        // save SUBJECT AREA properties if changed
        if (bSubjectsChanged) {
            // 1) remove all folders
            try {
                ITSframe.its.removeCorpusSubject(corpus);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                        "Error", JOptionPane.NO_OPTION);
                return;
            }

            // 2) add new folders back one at a time
            DefaultListModel dlm = (DefaultListModel) availableSubjectsList
                    .getModel();
            for (int i = 0; i < activeSubjectsList.getModel().getSize(); i++) {
                SubjectArea sa = (SubjectArea) activeSubjectsList.getModel()
                        .getElementAt(i);

                try {
                    ITSframe.its.addCorpusSubject(corpus, sa);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                            "Error", JOptionPane.NO_OPTION);
                    return;
                }
            }
        }

        // save THESAURI properties if changed
        if (bThesauriChanged) {
            // 1) remove all thesauri relationships
            try {
                ITSframe.its.removeCorpusThesaurus(corpus);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                        "Error", JOptionPane.NO_OPTION);
                return;
            }

            // 2) add new folders back one at a time
            DefaultListModel dlm = (DefaultListModel) availableThesaurusList
                    .getModel();
            for (int i = 0; i < activeThesaurusList.getModel().getSize(); i++) {
                Thesaurus t = (Thesaurus) activeThesaurusList.getModel()
                        .getElementAt(i);

                try {
                    ITSframe.its.addCorpusThesaurus(corpus, t);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(ITSframe, e.getMessage(),
                            "Error", JOptionPane.NO_OPTION);
                    return;
                }
            }
        }

        if (bSecurityChanged) {
            // get the list of all current security access from LDAP
            Vector vCurrent = new Vector();
            try {
                vCurrent = ITSframe.its.getSecurityGroups(corpus, "1");
            } catch (Exception e) {
                e.printStackTrace(System.out);
                return;
            }

            // get all items that are not in LDAP but are in the "active" list
            // and vice versa
            Vector vAddList = preenActive(getVector(activeSecurityList),
                    vCurrent);
            Vector vRemoveList = preenActive(vCurrent,
                    getVector(activeSecurityList));

            for (int i = 0; i < vAddList.size(); i++) {
                SecurityGroup sg = (SecurityGroup) vAddList.elementAt(i);
                try {
                    ITSframe.its.setCorpusAdminSecurity(corpus, sg);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }

            for (int i = 0; i < vRemoveList.size(); i++) {
                SecurityGroup sg = (SecurityGroup) vRemoveList.elementAt(i);
                try {
                    ITSframe.its.revokeCorpusSecurity(corpus, sg);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }
        }

        if (bReadOnlyChanged) {
            // get the list of all current security access from LDAP
            Vector vCurrent = new Vector();
            try {
                vCurrent = ITSframe.its.getSecurityGroups(corpus, "0");
            } catch (Exception e) {
                e.printStackTrace(System.out);
                return;
            }

            // get all items that are not in LDAP but are in the "active" list
            // and vice versa
            Vector vAddList = preenActive(getVector(activeReadSecurityList),
                    vCurrent);
            Vector vRemoveList = preenActive(vCurrent,
                    getVector(activeReadSecurityList));

            for (int i = 0; i < vAddList.size(); i++) {
                SecurityGroup sg = (SecurityGroup) vAddList.elementAt(i);
                try {
                    ITSframe.its.setCorpusReadSecurity(corpus, sg);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }

            for (int i = 0; i < vRemoveList.size(); i++) {
                SecurityGroup sg = (SecurityGroup) vRemoveList.elementAt(i);
                try {
                    ITSframe.its.revokeCorpusSecurity(corpus, sg);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }
        }

    }

    public Vector getVector(JList List) {
        Vector v = new Vector();

        DefaultListModel dlm = (DefaultListModel) List.getModel();
        for (int i = 0; i < List.getModel().getSize(); i++) {
            v.add(List.getModel().getElementAt(i));
        }

        return v;
    }

    public void rightArrow(SortedList availableList, SortedList activeList) {
        if (activeList.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(ITSframe, "No items are selected.",
                    "Error", JOptionPane.NO_OPTION);
            return;
        }

        DefaultListModel dlm = (DefaultListModel) availableList.getModel();
        for (int i = 0; i < activeList.getSelectedIndices().length; i++) {
            int index = activeList.getSelectedIndices()[i];

            dlm.addElement(activeList.getModel().getElementAt(index));
        }
        availableList.setModel(dlm);

        int length = activeList.getSelectedIndices().length;
        DefaultListModel dlm2 = (DefaultListModel) activeList.getModel();
        for (int i = length - 1; i >= 0; i--) {
            int index = activeList.getSelectedIndices()[i];

            dlm2.remove(index);
        }
        activeList.setModel(dlm2);
    }

    public void leftArrow(SortedList availableList, SortedList activeList) {
        if (availableList.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(ITSframe, "No items are selected.",
                    "Error", JOptionPane.NO_OPTION);
            return;
        }

        DefaultListModel dlm = (DefaultListModel) activeList.getModel();
        for (int i = 0; i < availableList.getSelectedIndices().length; i++) {
            int index = availableList.getSelectedIndices()[i];

            dlm.addElement(availableList.getModel().getElementAt(index));
        }
        activeList.setModel(dlm);

        int length = availableList.getSelectedIndices().length;
        DefaultListModel dlm2 = (DefaultListModel) availableList.getModel();
        for (int i = length - 1; i >= 0; i--) {
            int index = availableList.getSelectedIndices()[i];

            dlm2.remove(index);
        }
        availableList.setModel(dlm2);

    }

    // event listeners
    public void focusGained(FocusEvent e) {
        // FLAG changes to any objects as requiring a SAVE action
        flagChanges(e);
    }

    public void focusLost(FocusEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {
            public SortedList sl = null;

            protected void doNonUILogic() {
                glass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                glass.setVisible(true);

                // close frame if cancel was selected
                if (e.getSource().equals(cancelButton)) {
                    frame.dispose();
                }

                // arrow button controls :
                // FOLDERS
                if (e.getSource().equals(leftArrowButton)) {
                    leftArrow(availableFoldersList, activeFoldersList);
                }
                if (e.getSource().equals(rightArrowButton)) {
                    rightArrow(availableFoldersList, activeFoldersList);
                }
                // SUBJECT AREAS
                if (e.getSource().equals(leftArrowSubjectButton)) {
                    leftArrow(availableSubjectsList, activeSubjectsList);
                }
                if (e.getSource().equals(rightArrowSubjectButton)) {
                    rightArrow(availableSubjectsList, activeSubjectsList);
                }
                // SECURITY
                if (e.getSource().equals(leftArrowSecurityButton)) {
                    leftArrow(availableSecurityList, activeSecurityList);
                }
                if (e.getSource().equals(rightArrowSecurityButton)) {
                    rightArrow(availableSecurityList, activeSecurityList);
                }
                // READ SECURITY
                if (e.getSource().equals(leftArrowReadSecurityButton)) {
                    leftArrow(availableReadSecurityList, activeReadSecurityList);
                }
                if (e.getSource().equals(rightArrowReadSecurityButton)) {
                    rightArrow(availableReadSecurityList,
                            activeReadSecurityList);
                }
                // THESAURUS
                if (e.getSource().equals(leftArrowThesaurusButton)) {
                    leftArrow(availableThesaurusList, activeThesaurusList);
                }
                if (e.getSource().equals(rightArrowThesaurusButton)) {
                    rightArrow(availableThesaurusList, activeThesaurusList);
                }

                if (this.e.getSource().equals(btnAdd)) {
                    CorpusProperties.this.addTextToList();
                }
                if (e.getSource().equals(uploadImageFileSystem)) {
                    try {
                        addLocalImagesCorpus();
                        refreshImagesPane();
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }
                if (e.getSource().equals(deleteImage)) {
                    deleteImageFromCorpus();
                    refreshImagesPane();
                }

                if (e.getSource().equals(uploadImageUrl)) {
                    addUrlImageCorpus();
                    refreshImagesPane();
                }
                if (e.getSource().equals(refreshImage)) {
                    refreshImagesPane();
                }
                if (e.getSource().equals(btnImagesS3)) {
                    String val = "";
                    //change cursor
                    try {
                        val = ITSframe.its.saveCorpusImagesS3(corpus.getID());
                        JOptionPane.showMessageDialog(ITSframe, val);
                    } catch (Exception e) {
                        Log.error("Error saving images in S3");
                    } finally {
                        //change to default cursor
                    }
                }

                flagChanges((ActionEvent) e);

                if (e.getSource().equals(saveButton)) {
                    saveProperties();
                    JOptionPane.showMessageDialog(ITSframe,
                            "Taxonomy properties were saved successfully.",
                            "Information", JOptionPane.NO_OPTION);
                    frame.dispose();
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                glass.setCursor(Cursor
                        .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                glass.setVisible(false);
            }
        };
        aWorker.start();
    }

    private void addTextToList() {
        this.txtTerms.setText(this.txtTerms.getText().trim());
        if (this.txtTerms.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Empty");
            this.txtTerms.setText("");
            return;
        }
        if (this.txtTerms.getText().length() > 30) {
            JOptionPane.showMessageDialog(this, "Length > 30");
            this.txtTerms.setText("");
            return;
        }
        if (this.txtTerms.getText().contains(",")) {
            JOptionPane.showMessageDialog(this, "No input character\",\"");
            this.txtTerms.setText("");
        } else {
            for (int i = 0; i < this.modelList.getSize(); i++) {
                if (this.modelList.getElementAt(i).equals(
                        this.txtTerms.getText())) {
                    JOptionPane.showMessageDialog(this, "Already added");
                    this.txtTerms.setText("");
                    return;
                }
            }

            this.modelList.addElement(this.txtTerms.getText());
            this.txtTerms.setText("");
        }
    }

    private void deleteImageFromCorpus() {
        if (panelImages.getCurrentImageID() != null) {
            ITSframe.its.deleteImageCorpus(panelImages.getCurrentImageID());
        } else {
            JOptionPane.showMessageDialog(this, "Select a image");
        }
    }

    private void addUrlImageCorpus() {
        ExplorerUrl eUrl = new ExplorerUrl();
        if (!eUrl.cancelAction) {
            ITSframe.its.addImageURLCorpus(corpus, eUrl.getUrl());
        }
    }

    private void addLocalImagesCorpus() throws IOException {
        ArrayList<FileMetadata> listFile = new ArrayList<FileMetadata>();
        StringBuilder buffer = new StringBuilder();
        byte data[] = new byte[1024];
        int b = 0;
        FileMetadata fileMeta = new FileMetadata();
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Images(jpg,gif,png)", "jpg", "gif", "png");
        fileChooser.setFileFilter(filter);
        int select = fileChooser.showOpenDialog(null);
        if (select == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            System.out.println(file.getAbsolutePath());
            FileInputStream fis = new FileInputStream(file);

            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            while ((b = fis.read(data, 0, 1024)) > 0) {
                bao.write(data, 0, b);
            }
            fileMeta.setNodeId(corpus.getID());
            fileMeta.setDescription(filterString(file.getName()));
            fileMeta.setUri(filterString("file://" + file.getPath()));
            fileMeta.setImage(bao.toByteArray());
            //change setId per a value int to send more images
            fileMeta.setId(String.valueOf(1));

            listFile.add(fileMeta);
            buffer.append(fileMeta.getId());
            buffer.append(";");
            buffer.append(fileMeta.getDescription());
            buffer.append(";");
            buffer.append(fileMeta.getNodeId());
            buffer.append(";");
            buffer.append(fileMeta.getUri());
            buffer.append("!");
            bao.close();

            String strBuffer = buffer.toString();
            if (strBuffer.endsWith("!")) {
                int last = strBuffer.lastIndexOf('!');
                strBuffer = strBuffer.substring(0, last);
            }
            System.out.println(strBuffer.toString());
            ITSframe.its.addImageLocalCorpus(listFile, strBuffer.getBytes());
        }
    }

    private String filterString(String term) {
        term = term.replace(";", "");
        term = term.replace("!", "");
        return term;
    }

    public void refreshImagesPane() {
        ArrayList<FileMetadata> images = ITSframe.its.getImagesFromCorpus(corpus.getID());
        panelImages.setFileMetaData(images);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if ((e.getSource().equals(this.lstTerms)) && (e.getKeyCode() == 8)
                && (this.lstTerms.getSelectedValues().length >= 0)) {
            Object[] selection = this.lstTerms.getSelectedValues();

            for (Object s : selection) {
                for (int k = 0; k < this.modelList.getSize(); k++) {
                    if (this.modelList.get(k).equals(s)) {
                        this.modelList.remove(k);
                        break;
                    }
                }
            }
            this.lstTerms.revalidate();
            this.bGeneralChanged = true;
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource().equals(this.chkAffinity)) {
            if (this.chkAffinity.isSelected()) {
                activaTerms();
            } else {
                desactivaTerms();
            }
        }

    }
}
