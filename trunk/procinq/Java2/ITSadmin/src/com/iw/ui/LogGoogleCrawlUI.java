package com.iw.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;

import com.iw.tools.EmitFilterGoogle;
import com.iw.tools.GoogleInfoBean;

public class LogGoogleCrawlUI extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final Logger LOG = Logger.getLogger(LogGoogleCrawlUI.class);
	private static final long serialVersionUID = 6166312377482736122L;

	private GoogleInfoBean gib;

	private JList list;
	private JScrollPane panel;
	private JButton button;
	private JPanel panelButton;
	private JPanel panelInfo;
	private JLabel lblinfoOk;
	private JLabel lblinfoError;
	private JLabel lblinfoCX;
	private JTextField txtcustomSearch;
	private JTextField txtinfoOk;
	private JTextField txtinfoError;

	public LogGoogleCrawlUI(GoogleInfoBean gib, JFrame parent) {
		super(parent, true);
		this.gib = gib;
	}

	public void showWin() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Google Crawl LOG");
		this.setSize(new Dimension(300, 300));
		this.setModal(true);
		this.openAtCenter();
		button = new JButton("Save file");
		button.addActionListener(this);
		panelButton = new JPanel();
		panelButton.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		panelButton.add(button);

		panelInfo = new JPanel();
		panelInfo.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panelInfo.setLayout(new GridLayout(3, 2));
		lblinfoError = new JLabel("Errors");
		lblinfoOk = new JLabel("Success");
		txtinfoOk = new JTextField();
		txtinfoOk.setEnabled(false);
		txtinfoError = new JTextField();
		txtinfoError.setEnabled(false);
		lblinfoCX = new JLabel("Custom Search:");
		txtcustomSearch = new JTextField();
		txtcustomSearch.setEnabled(false);

		panelInfo.add(lblinfoOk);
		panelInfo.add(txtinfoOk);
		panelInfo.add(lblinfoError);
		panelInfo.add(txtinfoError);
		panelInfo.add(lblinfoCX);
		panelInfo.add(txtcustomSearch);

		DefaultListModel listModel = new DefaultListModel();
		list = new JList(listModel);
		panel = new JScrollPane(list);
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.setAutoscrolls(true);

		// set terms
		if (gib.getCSV() != null) {
			for (String csvLine:gib.getCSV()) {
				listModel.addElement(csvLine);
			}
		}
		txtinfoError.setText(String.valueOf(gib.getNumErrors()));
		txtinfoOk.setText(String.valueOf(gib.getNumSuccess()));
		txtcustomSearch.setText(EmitFilterGoogle.getMessage(Integer.parseInt(gib.getFilter())));

		this.getContentPane().add(panelInfo, BorderLayout.NORTH);
		this.getContentPane().add(panel, BorderLayout.CENTER);
		this.getContentPane().add(panelButton, BorderLayout.SOUTH);
		this.setVisible(true);
	}

	public void openAtCenter() {
		Dimension winsize = this.getSize(), screensize = Toolkit
				.getDefaultToolkit().getScreenSize();
		this.setLocation((screensize.width - winsize.width) / 2,
				(screensize.height - winsize.height) / 2);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(button)) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
					"Files *.txt", "txt");
			JFileChooser jfc = new JFileChooser();
			jfc.setFileFilter(filter);
			jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int q = jfc.showSaveDialog(this);
			if (q == JFileChooser.APPROVE_OPTION) {
				File file = jfc.getSelectedFile();
				if (file != null) {
					if (file.exists()) {
						int response = JOptionPane
								.showOptionDialog(
										this,
										"The file already exists, Do you want delete the file?",
										"Question", JOptionPane.YES_NO_OPTION,
										JOptionPane.QUESTION_MESSAGE, null,
										new Object[] { "Yes", "No" }, "Yes");
						if (response != -1) {
							if (response == 0) {
								file.delete();
							} else {
								return;
							}
						}
					}
					OutputStreamWriter writer = null;
					BufferedWriter fbw = null;
					try {
						file.createNewFile();
						writer = new OutputStreamWriter(new FileOutputStream(
								file), "UTF-8");
						fbw = new BufferedWriter(writer);
						fbw.write("REPORT GOOGLE CRAWL");
						fbw.newLine();
						fbw.write("CSV");
						fbw.newLine();
						fbw.write("INFORMATION: NODEID - TERM - FIRST URI - NUM RESULTS");
						if (gib.getCSV() != null) {
							for (String csvLine: gib.getCSV()) {
								fbw.write(csvLine);
							}							
						}
						fbw.write("---------------------------------");
						fbw.newLine();
						fbw.write("Errors: " + txtinfoError.getText());
						fbw.newLine();
						fbw.write("Success: " + txtinfoOk.getText());
						fbw.newLine();
						fbw.write("Custom Search: " + txtcustomSearch.getText());
						fbw.flush();
						LOG.info("Create file ok!");
						JOptionPane.showMessageDialog(this, "Create file ok!");
					} catch (IOException e1) {
						LOG.error("Not create file" + e1.getMessage());
					} finally {
						if (fbw != null) {
							try {
								fbw.close();
							} catch (IOException e1) {
								LOG.error("Error close BufferedWriter"
										+ e1.getMessage());
							}
						}
						if (writer != null) {
							try {
								writer.close();
							} catch (IOException e1) {
								LOG.error("Error close BufferedWriter"
										+ e1.getMessage());
							}
						}

					}

				}

			}
		}

	}

}
