package com.iw.ui;

import com.iw.system.ITS;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class DialogQAReport2 extends JInternalFrame implements ActionListener {

    private JSpinner snpLessDocuments;
    private JTextField txtLessDocuments;
    private JSpinner snpMoreDocuments;
    private JTextField txtMoreDocuments;
    private JButton btnRun;
    public ITS its = null;
    public ITSAdministrator ITSframe = null;
    private Map<String, String> parameters;
    private SignatureQA parent;

    public DialogQAReport2(ITSAdministrator frame, SignatureQA parent) {
        setResizable(false);
        setClosable(true);
        setIconifiable(true);

        this.parent = parent;

        SpinnerModel model =
                new SpinnerNumberModel(1, //initial value
                1, //min
                null, //max
                1);

        SpinnerModel model1 =
                new SpinnerNumberModel(1, //initial value
                1, //min
                null, //max
                1);

        setSize(384, 160);
        setTitle("Filter: Report - documents");
        setLocation(60, 60);
        its = frame.its;
        ITSframe = frame;

        setFrameIcon(ITSframe.iIndraweb);
        getContentPane().setLayout(null);

        JLabel lbllLessDocuments = new JLabel("Documents, less than");
        lbllLessDocuments.setBounds(12, 12, 183, 15);
        getContentPane().add(lbllLessDocuments);
        snpLessDocuments = new JSpinner(model);
        snpLessDocuments.setBounds(201, 10, 161, 19);
        txtLessDocuments = ((JSpinner.DefaultEditor) snpLessDocuments.getEditor()).getTextField();
        txtLessDocuments.setText("1");
        txtLessDocuments.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    txtLessDocuments.setText(txtLessDocuments.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(snpLessDocuments);

        JLabel lblMoreDocuments = new JLabel("Documents, more than");
        lblMoreDocuments.setBounds(12, 50, 183, 15);
        getContentPane().add(lblMoreDocuments);
        snpMoreDocuments = new JSpinner(model1);
        snpMoreDocuments.setBounds(201, 48, 161, 19);
        txtMoreDocuments = ((JSpinner.DefaultEditor) snpMoreDocuments.getEditor()).getTextField();
        txtMoreDocuments.setText("200");
        txtMoreDocuments.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                if (!(e.getKeyCode() >= 37 && e.getKeyCode() <= 40)) {
                    txtMoreDocuments.setText(txtMoreDocuments.getText().replaceAll("\\D", ""));
                }
            }
        });
        getContentPane().add(snpMoreDocuments);

        btnRun = new JButton("Apply");
        btnRun.setBounds(245, 90, 117, 25);
        btnRun.addActionListener(this);
        getContentPane().add(btnRun);

        JDesktopPane jdesktoppane = frame.jdp;
        jdesktoppane.add(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnRun) {
            setterParameters();
            this.parent.startReport2();
            this.dispose();
        }
    }

    private void setterParameters() {
        parameters = new HashMap<String, String>();
        parameters.put("LESSDOCUMENTS", txtLessDocuments.getText());
        parameters.put("MOREDOCUMENTS", txtMoreDocuments.getText());
    }

    public Map<String, String> getParameters() {
        return parameters;
    }
}
