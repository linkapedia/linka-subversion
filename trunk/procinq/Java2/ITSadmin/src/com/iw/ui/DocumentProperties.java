package com.iw.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.oculustech.layout.*;
import com.iw.system.*;

public class DocumentProperties extends OculusBox implements ActionListener {
    protected JLabel documentPropsLabel;
    protected JLabel titleLabel;
    protected JTextField titleText;
    protected JLabel dateFoundLabel;
    protected JTextField dateText;
    protected JLabel docUrlLabel;
    protected JTextField URLText;
    protected JLabel pathLabel;
    protected JTextField pathText;
    protected JLabel genreLabel;
    protected JComboBox genreDropDown;
    protected JLabel repositoryLabel;
    protected JComboBox repDropDown;
    protected JLabel abstractLabel;
    protected JTextArea abstractText;
    protected JButton saveButton;
    protected JButton cancelButton;

    private Document document;
    private JInternalFrame frame;
    private Component glass;
    private ITS its;

    // define label and component vectors in two grids
    private Vector labels1 = new Vector();
    private Vector labels2 = new Vector();
    private Vector labels3 = new Vector();

    private Vector components1 = new Vector();
    private Vector components2 = new Vector();
    private Vector components3 = new Vector();

    public DocumentProperties(ITSAdministrator ITSframe, Document d) {
        this.document = d; this.its = ITSframe.its;

        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        frame = new JInternalFrame("Edit Document Properties", true, true, true, true);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setFrameIcon(ITSframe.iIndraweb);

        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        documentPropsLabel = new JLabel("Document Properties");
        documentPropsLabel.setFont(Font.decode("Trebuchet MS-BOLD-11"));

        // dynamically create labels and fields using api function
        Vector vFields = new Vector();
        try {
            vFields = ITSframe.its.getFields("DOCUMENT");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error: " + e.getMessage(), "Error", JOptionPane.NO_OPTION);
        }

        boolean addOne = true;

        // sort the field results to put varchar fields first
        qs(vFields);

        for (int i = 0; i < vFields.size(); i++) {
            IndraField inf = (IndraField) vFields.elementAt(i);

            // this piece was meant to be generic, but unfortunately there are too many special cases
            // GENRE and REPOSITORY are special cases, trap those
            if (inf.getName().equals("GENREID")) {
                JLabel jl = new JLabel("FOLDER:");
                jl.setName("GENREID");
                jl.setToolTipText("Create or modify the folder associated with this document.");
                if (addOne) labels1.add(jl); else labels2.add(jl);

                Vector genreList = new Vector();
                try {
                    genreList = ITSframe.its.getFolders();
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }

                Genre g = new Genre();
                g.setID("-1");
                g.setName("NONE");
                genreList.add(0, g);

                JComboBox jcb = new JComboBox(genreList);
                setSelectedFolder(jcb, genreList, g);
                jcb.setMaximumSize(new Dimension(250, 50));
                jcb.setPreferredSize(new Dimension(250, 25));
                jcb.setSize(new Dimension(250, 25));

                if (addOne) { components1.add(jcb); addOne = false; }
                else { components2.add(jcb); addOne = true; }

            } else if (inf.getName().equals("REPOSITORYID")) {
                JLabel jl = new JLabel("REPOSITORY:");
                jl.setName("REPOSITORYID");
                jl.setToolTipText("Create or modify the repository associated with this document.");
                if (addOne) labels1.add(jl); else labels2.add(jl);

                Vector repList = new Vector();
                try {
                    repList = ITSframe.its.getRepositories();
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }

                Repository r = new Repository();
                r.setID("-1");
                r.setName("NONE");
                repList.add(0, r);

                JComboBox jcb = new JComboBox(repList);
                setSelectedRepository(jcb, repList, r);
                jcb.setMaximumSize(new Dimension(250, 50));
                jcb.setPreferredSize(new Dimension(250, 25));
                jcb.setSize(new Dimension(250, 25));

                if (addOne) { components1.add(jcb); addOne = false; }
                else { components2.add(jcb); addOne = true; }

            } else {
                JLabel jl = new JLabel(inf.getName() + ":");
                jl.setName(inf.getName().toUpperCase());
                jl.setToolTipText("Create or modify the field '" + inf.getName() + "'.");

                if (isRestricted(inf.getName())) {
                    // restricted fields, do not add
                    //System.out.println("(c) Restricted field: " + inf.getName());
                } else if ((inf.getType().startsWith("VARCHAR")) && (inf.getLength() > 2000)) {
                    //System.out.println("(a) " + inf.getType() + " " + inf.getLength() + " " + inf.getName());

                    labels3.add(jl);

                    JTextArea jta = new JTextArea(document.get(inf.getName()));
                    jta.setBackground(Color.white);
                    jta.setToolTipText("Create or modify the " + inf.getName() + " in the space provided.");
                    jta.setWrapStyleWord(true);
                    jta.setLineWrap(true);

                    components3.add(jta);

                } else if ((inf.getType().startsWith("NUMBER")) && (inf.getLength() == 22) && (!inf.getName().startsWith("DOC"))) {
                    //System.out.println("(b) " + inf.getType() + " " + inf.getLength() + " " + inf.getName() + " " + document.get(inf.getName()));

                    if (addOne) labels1.add(jl); else labels2.add(jl);

                    JComboBox jcb = createBooleanDropdown(document.get(inf.getName()));
                    jcb.setMaximumSize(new Dimension(150, 50));
                    jcb.setPreferredSize(new Dimension(150, 25));
                    jcb.setSize(new Dimension(150, 25));

                    if (addOne) { components1.add(jcb); addOne = false; }
                    else { components2.add(jcb); addOne = true; }
                } else if (inf.getType().startsWith("DATE"))  {
                    //System.out.println("(e) " + inf.getType() + " " + inf.getLength() + " " + inf.getName() + " " + document.get(inf.getName()));

                    if (addOne) labels1.add(jl); else labels2.add(jl);

                    String dateField = document.get(inf.getName());
                    if (dateField.length() > 2) dateField = dateField.substring(0, dateField.length()-2);

                    JTextField jtf = new JTextField(dateField);
                    jtf.setMaximumSize(new Dimension(250, 50));
                    jtf.setPreferredSize(new Dimension(250, 25));
                    jtf.setSize(new Dimension(250, 25));
                    jtf.setToolTipText("Create or modify the date '" + inf.getName() + "' in the format YYYY-MM-DD HH:MI:SS.");

                    if (addOne) { components1.add(jtf); addOne = false; }
                    else { components2.add(jtf); addOne = true; }
                } else {
                    //System.out.println("(d) " + inf.getType() + " " + inf.getLength() + " " + inf.getName());

                    if (addOne) labels1.add(jl); else labels2.add(jl);

                    JTextField jtf = new JTextField(document.get(inf.getName()));
                    jtf.setMaximumSize(new Dimension(250, 50));
                    jtf.setPreferredSize(new Dimension(250, 25));
                    jtf.setSize(new Dimension(250, 25));
                    jtf.setToolTipText("Create or modify the field '" + inf.getName() + "'.");

                    if (addOne) { components1.add(jtf); addOne = false; }
                    else { components2.add(jtf); addOne = true; }
                }
            }
        }

        saveButton = new JButton("Save");
        saveButton.addActionListener(this);

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);

        layout.addSpace(10);
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.nestBox(OculusLayout.VERTICAL);
            {
                layout.addSpace(10);
                layout.nestBox(OculusLayout.VERTICAL);
                {
                    layout.add(documentPropsLabel);
                    layout.nestBox(OculusLayout.HORIZONTAL, OculusLayout.TOP_JUSTIFY);
                    {
                        layout.nestGrid(2, components1.size());
                        {
                            for (int i = 0; i < components1.size(); i++) {
                                layout.add((JLabel) labels1.elementAt(i));
                                layout.add((Component) components1.elementAt(i));
                            }
                            layout.parent();
                        }

                        layout.nestGrid(2, components2.size());
                        {
                            for (int i = 0; i < components2.size(); i++) {
                                layout.add((JLabel) labels2.elementAt(i));
                                layout.add((Component) components2.elementAt(i));
                            }
                            layout.parent();
                        }

                        layout.parent();
                    }
                    layout.addSpace(10);
                    for (int i = 0; i < components3.size(); i++) {
                        layout.add((JLabel) labels3.elementAt(i));
                        layout.add(new JScrollPane((JTextArea) components3.elementAt(i)));
                        layout.addSpace(10);
                    }

                    layout.parent();
                }
                layout.parent();
            }
            layout.addSpace(10);
            layout.parent();
        }
        layout.nestBox(OculusLayout.VERTICAL);
        {
            layout.addSpace(10);
            layout.add(saveButton);
            layout.addSpace(10);
            layout.add(cancelButton);
            layout.parent();
        }
        layout.addSpace(10);

        frame.setLocation(20, 60);
        glass = frame.getGlassPane();

        //frame.setPreferredSize(new Dimension(750, 400));
        ITSframe.jdp.add(frame);

        frame.getRootPane().setDefaultButton(saveButton);

        frame.setContentPane(new JScrollPane(this));
        frame.pack();
        frame.show();
    }

    // set the default genre selection
    public void setSelectedFolder(JComboBox box, Vector genreList, Genre genreDefault) {
        for (int i = 0; i < genreList.size(); i++) {
            Genre g = (Genre) genreList.elementAt(i);
            if (g.getID().equals(document.get("GENREID"))) {
                box.setSelectedItem(g);
                return;
            }
        }

        // no objects found, use NONE
        box.setSelectedItem(genreDefault);
    }

    // set the default genre selection
    public void setSelectedRepository(JComboBox box, Vector repList, Repository repDefault) {
        for (int i = 0; i < repList.size(); i++) {
            Repository r = (Repository) repList.elementAt(i);
            if (r.getID().equals(document.get("REPOSITORYID"))) {
                box.setSelectedItem(r);
                return;
            }
        }

        // no objects found, use NONE
        box.setSelectedItem(repDefault);
    }

    public JComboBox createBooleanDropdown(String defaultValue) {

        Vector items = new Vector();
        for (int i = -1; i < 10; i++) items.add(i + "");

        JComboBox jcb = new JComboBox(items);
        if (defaultValue != null)
            jcb.setSelectedItem(defaultValue);
        else
            jcb.setSelectedItem("1");

        return jcb;
    }

    // these four fields are manually restricted from showing
    public boolean isRestricted(String fieldName) {
        if (fieldName.equals("DOCUMENTID")) return true;
        if (fieldName.equals("DOCUMENTSECURITY")) return true;
        if (fieldName.equals("DOCUMENTSUMMARY2")) return true;
        if (fieldName.equals("DOCUMENTSUMMARY3")) return true;
        if (fieldName.equals("UPDATEDDATE")) return true;

        return false;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(cancelButton)) { frame.dispose(); return; }

        // save
        Hashtable ht = getArguments();
        try { document = its.editDocument(document.get("DOCUMENTID"), ht); frame.dispose(); }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error: " + ex.getMessage(), "Error", JOptionPane.NO_OPTION);
            ex.printStackTrace(System.out);
        }
    }

    private Hashtable getArguments() {
        Hashtable ht = new Hashtable();

        for (int i = 0; i < labels1.size(); i++) {
            JLabel label = (JLabel) labels1.elementAt(i);
            Component c = (Component) components1.elementAt(i);

            String value = "";
            if (c instanceof JComboBox) {
                Object o = ((JComboBox) c).getSelectedItem();
                if (o instanceof Genre) value = ((Genre) o).getID();
                if (o instanceof Repository) value = ((Repository) o).getID();

                if (value.equals("-1")) value = "";
            }
            if (c instanceof JTextField) value = ((JTextField) c).getText();

            //System.out.println("Name: "+label.getName()+" Value: "+value);

            if (!(label.getName().equals("GENREID") && value.equals("")))
                ht.put(label.getName(), value);
        }

        for (int i = 0; i < labels2.size(); i++) {
            JLabel label = (JLabel) labels2.elementAt(i);
            Component c = (Component) components2.elementAt(i);

            String value = "";
            if (c instanceof JComboBox) {
                Object o = ((JComboBox) c).getSelectedItem();
                if (o instanceof Genre) value = ((Genre) o).getID();
                else if (o instanceof Repository) value = ((Repository) o).getID();
                else value = o.toString();

                if (value.equals("-1")) value = "";
            }
            if (c instanceof JTextField) value = ((JTextField) c).getText();

            //System.out.println("Name: "+label.getName()+" Value: "+value);
            ht.put(label.getName(), value);
        }

        for (int i = 0; i < components3.size(); i++) {
            JLabel label = (JLabel) labels3.elementAt(i);
            JTextArea c = (JTextArea) components3.elementAt(i);

            //System.out.println("Name: "+label.getName()+" Value: "+c.getText());
            ht.put(label.getName(), c.getText());
        }

        //ht.put("DOCUMENTID", document.get("DOCUMENTID"));

        return ht;
    }

    // return the string as lower case with the first letter of each word capitalized
    public static String capitalized(String str) {
        String s = "";

        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh))
                s = s + Character.toUpperCase(ch);
            else
                s = s + ch;
            prevCh = ch;  // prevCh for next iteration is ch.
        }
        return s;
    }

    // Main recursive call
    private void qs(Vector v) {

       Vector vl = new Vector();                      // Left and right sides
       Vector vr = new Vector();

       if (v.size() < 2) return;                      // 0 or 1= sorted

       IndraField inf = (IndraField) v.elementAt(0);       // 1st element key

       // Start at element 1
       for (int i=1; i < v.size(); i++) {
          IndraField inf2 = (IndraField) v.elementAt(i);
          if (compare(inf, inf2)) vr.addElement(inf2);
          else vl.addElement(inf2);
       }

       qs(vl);                                        // Recursive call left
       qs(vr);                                        //    "        "  right
       vl.addElement(v.elementAt(0));

       addVect(v, vl, vr);
     }

     // Add two vectors together, into a destination Vector
     private void addVect( Vector dest, Vector left, Vector right ) {

        int i;

        dest.removeAllElements();                     // reset destination

        for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
        for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
     }

    private boolean compare(IndraField inf, IndraField inf2) {
        if (inf2.getType().compareTo(inf.getType()) < 0) return true;
        if (inf2.getType().compareTo(inf.getType()) > 0) return false;

        if (inf2.getName().compareTo(inf.getName()) > 0) return true;
        return false;
    }
}