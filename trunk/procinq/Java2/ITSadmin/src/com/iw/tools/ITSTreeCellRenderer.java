/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Jan 8, 2004
 * Time: 3:24:20 PM
 * To change template for new class use
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.tools;

import com.iw.system.ITSTreeNode;

import javax.swing.tree.*;
import javax.swing.*;
import java.awt.*;
import java.util.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ITSTreeCellRenderer extends DefaultTreeCellRenderer implements ActionListener {

    private JTree tree;
    private boolean isDomain;

    public ITSTreeCellRenderer() {
        super();

    }

    public ITSTreeCellRenderer(boolean isDomain) {
        this.isDomain = isDomain;
    }

    public Icon getLinkIcon() {
        return com.iw.tools.ImageUtils.getImage(this, "itsimages/link.gif");
    }

    private Icon getRootDomainIcon() {
        return com.iw.tools.ImageUtils.getImage(this, "itsimages/domain.png");
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel,
            boolean expanded,
            boolean leaf, int row,
            boolean hasFocus) {
        String stringValue = tree.convertValueToText(value, sel,
                expanded, leaf, row, hasFocus);

        ITSTreeNode n = null;

        try {
            n = (ITSTreeNode) ((DefaultMutableTreeNode) value).getUserObject();
        } catch (Exception e) {
            setComponentOrientation(tree.getComponentOrientation());
            selected = sel;
            setText(((DefaultMutableTreeNode) value).getUserObject() + "");
            return this;
        }

        this.tree = tree;
        this.hasFocus = hasFocus;
        setText(stringValue);
        if (sel) {
            setForeground(getTextSelectionColor());
        } else {
            setForeground(getTextNonSelectionColor());
        }
        // There needs to be a way to specify disabled icons.
        if (!tree.isEnabled()) {
            setEnabled(false);
            if (leaf) {
                setDisabledIcon(getLeafIcon());
            } else if (expanded) {
                setDisabledIcon(getOpenIcon());
            } else {
                setDisabledIcon(getClosedIcon());
            }
        } else {
            setEnabled(true);
            if (!isDomain) {
                if (n.isLink()) {
                    setIcon(getLinkIcon());
                } else if (leaf) {
                    setIcon(getLeafIcon());
                } else if (expanded) {
                    setIcon(getOpenIcon());
                } else {
                    setIcon(getClosedIcon());
                }
            } else {
                if (n.isRoot()) {
                    setIcon(getRootDomainIcon());
                } else if (n.isLink()) {
                    setIcon(getLinkIcon());
                } else if (leaf) {
                    setIcon(getLeafIcon());
                } else if (expanded) {
                    setIcon(getOpenIcon());
                } else {
                    setIcon(getClosedIcon());
                }
            }
        }
        setComponentOrientation(tree.getComponentOrientation());

        selected = sel;

        return this;
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("Action performed! :-)");
    }
}
