package com.iw.tools.image.util;
import java.awt.Image;
import java.util.LinkedHashMap;


public interface IPaint {
	public void onPaint(Image image, LinkedHashMap<String, String> map);
	public LinkedHashMap<String,String> getCurrentInfo();
}
