package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class TefOut extends ExportElement {
	private boolean StripCommasAndParentheses = false;
	
	public TefOut(boolean CommaOption) throws Exception {
		this.StripCommasAndParentheses = CommaOption;
	}

    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public String corpusID = null;
    public String wordName = "terms";

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("commonwords.txt"))));
        corpusID = c.getID();

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { System.err.println("No root node found for corpus "+corpusID); return null; }

        File f = new File(c.getName().replaceAll(" ", "_")+".xml");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2004-2008 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<tef xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://emc.com/SYMBOLIC/TefSchema10/\">\n");
        
        //removed per matt coblenz email of 10/24/06.
        //put back in per B�n�dicte Grizolle email of 5/29/08.
        out.write("<class name=\"Intellisophic\">\n");
        out.write("   <details source=\"\" targetAttribute=\"keywords\" title=\"Intellisophic\">\n");
        //out.write("      <description>Intellisophic, Inc.  The world's largest provider of taxonomic content for use in advanced information intelligence applications.  15 Maple Avenue, Paoli, PA 19301  T: 610.251.1076</description>\n");
        out.write("      <description></description>\n");
        out.write("   </details>\n");
        out.write("   <categoryDefaults>\n");
        out.write("      <impliedKeywordDefaults confidence=\"off\" stem=\"true\" phraseOrderExact=\"false\"/>\n");
        out.write("      <keywordDefaults confidence=\"high\" stem=\"true\" phraseOrderExact=\"true\"/>\n");
        out.write("      <evidencePropagation confidence=\"off\" type=\"off\" />\n");
        out.write("      <categoryEvidenceDefaults confidence=\"off\" />\n");
        out.write("   </categoryDefaults>\n");
        out.write("</class>\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        out.write("<taxonomy name=\""+c.getName()+"\" className=\"Intellisophic\" taxonomyVersion=\"1.0\">\n");
        out.write("<details title=\""+rootNode.get("NODETITLE").trim()+"\">\n");
        // out.write("   <description>Copyright 2004-2008 by Intellisophic, Inc. Corpus "+c.getID()+".  All rights reserved.  "+rootNode.get("NODEDESC")+"</description>\n");
        out.write("   <description>Copyright 2004-2008 by Intellisophic, Inc. Corpus "+c.getID()+".  All rights reserved.  "+ c.getDescription()   +"</description>\n");
        
        out.write("</details>\n");
        out.write("<definition onTargetThreshold=\"70\" candidateThreshold=\"40\">\n");
        out.write("   <evidence>\n");
        out.write("      <evidenceSet />\n");
        out.write("   </evidence>\n");
        out.write("</definition>\n");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, out);

        out.write("</taxonomy>\n");
        out.write("</tef>\n");

        out.close();

        return f;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer out) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() < 1) return;

        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);

            String title = getTefName(n.get("NODETITLE")).trim();
            
            //Removing parentheses from taxonomies for Cities of because we have (( )) surrounding some topics
            //!!!DO NOT build into base product - just use from debug environment!!!
        	//title = title.replaceAll("\\(", " ");
        	//title = title.replaceAll("\\)", " ");
        	//title = title.trim();
        	//MPL - REMOVE ABOVE BEFORE SAVING TO SOURCE SAFE
        	
            //Removed ID per B�n�dicte Grizolle 8/23/10 email.
            //out.write("<category name=\""+title+n.get("NODEID")+"\">\n");
            out.write("<category name=\""+title+"\">\n");
            out.write("<details title=\""+title+"\">\n");
            out.write("   <description>"+title+"</description>\n");
            out.write("</details>\n");
            out.write("<definition onTargetThreshold=\"70\" candidateThreshold=\"40\">\n");
            out.write("   <evidence>\n");
            out.write("      <evidenceSet>\n");

            /*  Whatever the highest term number is in a topic, make it a 75 */
            /*double previousHigh = 0;
            for (int j = 0; j < vSignatures.size(); j++) 
            {
                Signature s = (Signature) vSignatures.elementAt(j);
                if (s.getWeight() > previousHigh) previousHigh = s.getWeight();
            }
            */
            
            /* DO NOT ADD TITLE ANYMORE - per discussion with jacques conan (matt's replacement - april 2008)
            // Add node title terms and make them match the highest sig weight. 

            //if asked to strip out commas and parentheses, need to do that before adding node as evidence
            if ((title.toLowerCase().indexOf("(") > -1 || title.toLowerCase().indexOf(",") > -1) && StripCommasAndParentheses)
            {
               	String strippedTitle = title.replaceAll("\\(", " ");
               	strippedTitle = strippedTitle.replaceAll("\\)", " ");
               	strippedTitle = strippedTitle.replaceAll(",", " ");
                   
                //now need to replace any double spaces with single space, in case we added any!
               	strippedTitle = strippedTitle.replaceAll("  ", " ");
                
                out.write("<keyword name=\""+strippedTitle.toLowerCase()+"\" confidence=\"75\" />\n");
                ht.put(strippedTitle.toLowerCase(), "1");
            }
            else  //write out node title as-is as evidence
            {
                out.write("<keyword name=\""+title.toLowerCase()+"\" confidence=\"75\" />\n");
                ht.put(title.toLowerCase(), "1");
            }
 
          	// Break up node title into parts around parentheses
            // Take parentheses contents and put in as keyword, as well as phrase before it
            // !! Assumption that parentheses are last in phrase !! 
            if (title.toLowerCase().indexOf("(") > -1)
            {
                String[] parenWords = title.toLowerCase().split("\\(");
                 
                //Remove end parentheses from last phrase
                String tempLastString = parenWords[parenWords.length-1].substring(0,(parenWords[parenWords.length-1].length())-1);
                parenWords[parenWords.length-1] = tempLastString;
                
                for (int j = 0; j < parenWords.length; j++) 
                {
                	String trimmedWords = parenWords[j].trim();
                    if ((!ht.containsKey(trimmedWords)) && (!commonWords.containsKey(trimmedWords))) 
                    {
                        if ((trimmedWords.indexOf("(") > -1 || trimmedWords.indexOf(",") > -1) && StripCommasAndParentheses)
                        {
                           	String strippedTitle = trimmedWords.replaceAll("\\(", " ");
                           	strippedTitle = strippedTitle.replaceAll("\\)", " ");
                           	strippedTitle = strippedTitle.replaceAll(",", " ");
                               
                            //now need to replace any double spaces with single space, in case we added any!
                           	strippedTitle = strippedTitle.replaceAll("  ", " ");
                            
                            out.write("<keyword name=\""+strippedTitle+"\" confidence=\"40\" />\n");
                            ht.put(strippedTitle, "1");
                        }
                        else
                        {
                        	out.write("<keyword name=\""+trimmedWords+"\" confidence=\"40\" />\n");
                        	ht.put(trimmedWords, "1");
                        }
                    }
         
                    //break each phrase at commas and put individual words in also
                    String[] words = parenWords[j].split(",");
                    if (words.length > 1)
                    {
    		            for (int k = 0; k < words.length; k++) 
    		            {
    		            	trimmedWords = words[k].trim(); 
    		                if ((!ht.containsKey(trimmedWords)) && (!commonWords.containsKey(trimmedWords))) 
    		                {
    	                        if ((trimmedWords.indexOf("(") > -1 || trimmedWords.indexOf(",") > -1) && StripCommasAndParentheses)
    	                        {
    	                           	String strippedTitle = trimmedWords.replaceAll("\\(", " ");
    	                           	strippedTitle = strippedTitle.replaceAll("\\)", " ");
    	                           	strippedTitle = strippedTitle.replaceAll(",", " ");
    	                               
    	                            //now need to replace any double spaces with single space, in case we added any!
    	                           	strippedTitle = strippedTitle.replaceAll("  ", " ");
    	                            
    	                            out.write("<keyword name=\""+strippedTitle+"\" confidence=\"40\" />\n");
    	                            ht.put(strippedTitle, "1");
    	                        }
    	                        else
    	                        {
    	                        	out.write("<keyword name=\""+getTefName(trimmedWords)+"\" confidence=\"40\" />\n");
    		                    	ht.put(trimmedWords, "1");
    	                        }
    		                }      
    		            }
                    }
                }
            }
            else	
            {
            	//if there is only one comma, also add evidence with word/phrases on either side of comma reversed
            	//for example, "McGillivray, Alexander" is added as "Alexander McGillivray"
            	// and "Little Bighorn, Battle of the" becomes "Battle of the Little Bighorn"
            	String[] commaWords = title.toLowerCase().split(",");
                if (commaWords.length == 2)
                {
                	String reversedTitle = commaWords[1].trim()+ " " + commaWords[0].trim();
                	out.write("<keyword name=\""+reversedTitle+"\" confidence=\"75\" />\n");
                    ht.put(reversedTitle, "1");
                }
            	
                //Break each phrase at commas and put individual words (non-common) in also as keywords
                if (commaWords.length > 1)
                {
		            for (int k = 0; k < commaWords.length; k++) 
		            {
		                if ((!ht.containsKey(commaWords[k].trim())) && (!commonWords.containsKey(commaWords[k].trim()))) 
		                {
		                    out.write("<keyword name=\""+getTefName(commaWords[k]).trim()+"\" confidence=\"40\" />\n");
		                    ht.put(getTefName(commaWords[k]).trim(), "1");
		                }      
		            }
                }
                else //break at any spaces and put individual words (non-common) in also as keywords
                {
                	String[] words = title.toLowerCase().split(" ");
                    if (words.length > 1)
                	for (int k = 0; k < words.length; k++) 
                	{
                          if ((!ht.containsKey(words[k])) && (!commonWords.containsKey(words[k]))) 
                          {
                              out.write("<keyword name=\""+getTefName(words[k]).trim()+"\" confidence=\"40\" />\n");
                              ht.put(getTefName(words[k]).trim(), "1");
                          }
                     }
                }
            }
            */
  
            //to prevent duplicate sigs
            Hashtable ht = new Hashtable();		
           
            //OLD: Add must haves as evidence with weight of 75, same as title - these are synonyms
            //NEW: CHANGED TO -22 AS A MUST HAVE WEIGHT
            //per discussion with jacques conan; matt's replacement
            //they want to handle must haves differently but tef format doesn't have a way to identify
            //so we are making them unique with this value
            //And removing any sigs that are the same as the must haves
            Vector vHave = new Vector();
            vHave = server.getMusthaves(n.get("NODEID"));
            
            if (vHave.size() > 0) {
                for (int j = 0; j < vHave.size(); j++) 
                {
                    String mustHaveTerm = getTefName(vHave.elementAt(j).toString()).toLowerCase().trim();
                    if (!ht.containsKey(mustHaveTerm) && (!commonWords.containsKey(mustHaveTerm)))
                    {
                    	if ((mustHaveTerm.indexOf("(") > -1 || mustHaveTerm.indexOf(",") > -1) && StripCommasAndParentheses)
                        {
                        	String strippedMH = mustHaveTerm.replaceAll("\\(", "");
                        	strippedMH = strippedMH.replaceAll("\\)", "");
                        	strippedMH = strippedMH.replaceAll(",", "");
                              
                            //now need to replace any double spaces with single space, in case we added any!
                        	strippedMH = strippedMH.replaceAll("  ", " ");
                           
                        	if (!ht.containsKey(strippedMH) && (!commonWords.containsKey(strippedMH)))
                        	{
                        		//write out with a special value of -22 so EMC can identify as must have to use as filter
                        		out.write("<keyword name=\""+strippedMH.trim()+"\" confidence=\"-22\" />\n");
                        		//NOT ANYMORE - SKEWS SCORING -- also write out as a signature with weight of 15 to contribute to score
                        		//out.write("<keyword name=\""+strippedMH+"\" confidence=\"15\" />\n");
                        		ht.put(strippedMH, "1");
                        	}
                        }
                        else
                        {
                        	out.write("<keyword name=\""+mustHaveTerm+"\" confidence=\"-22\" />\n");
                        	//out.write("<keyword name=\""+mustHaveTerm+"\" confidence=\"15\" />\n");
                        	ht.put(mustHaveTerm, "1");
                        }
                    }
                }
            }
            else	//there is a bug in the editor's create must haves function so 
            		//if there are no must haves and the topic name contains an apostrophe,
            		//add the topic as a must have
            {
            	String mustHaveTerm = title.toLowerCase();
                if (mustHaveTerm.indexOf("'") > -1)
                {
               		//write out with a special value of -22 so EMC can identify as must have to use as filter
               		out.write("<keyword name=\""+mustHaveTerm+"\" confidence=\"-22\" />\n");
                }
            }

            // OLD: Signatures can be (sig weight of highest)/(sig weight of term)*75
            // April 2008: Signatures are (sig weight)/(total of all sig weights)
            // normalized to 150 because don't want to require as many signatures to 
            // hit EMC thresholds.  they have threshold of 75 to become a definite categorized
            // term.  Normalization of 100 required too many sigs to hit that.
            
            /* Get total of all signatures to normalize with */
            double totalSigsWeight = 0;
            for (int j = 0; j < vSignatures.size(); j++) 
            {
                Signature s = (Signature) vSignatures.elementAt(j);
                String signatureTerm = getTefName(s.getWord()).toLowerCase().trim();
                if (!ht.containsKey(signatureTerm) && (!commonWords.containsKey(signatureTerm)))
                	totalSigsWeight += s.getWeight();
            }
            
            DecimalFormat f = new DecimalFormat("#0");
            for (int j = 0; j < vSignatures.size(); j++) 
            {
                Signature s = (Signature) vSignatures.elementAt(j);
                double weight = (s.getWeight()/totalSigsWeight)*150;
                                                
                if (weight < 1.0) weight = 1.0;
                if (weight > 100.0) weight = 100.0;
                
                String signatureTerm = getTefName(s.getWord()).toLowerCase().trim();
                if (!ht.containsKey(signatureTerm) && (!commonWords.containsKey(signatureTerm)))
                {
                	if ((signatureTerm.indexOf("(") > -1 || signatureTerm.indexOf(",") > -1) && StripCommasAndParentheses)
                    {
                    	String strippedSig = signatureTerm.replaceAll("\\(", " ");
                      	strippedSig = strippedSig.replaceAll("\\)", " ");
                      	strippedSig = strippedSig.replaceAll(",", " ");
                          
                        //now need to replace any double spaces with single space, in case we added any!
                      	strippedSig = strippedSig.replaceAll("  ", " ");
                       
                        if (!ht.containsKey(strippedSig) && (!commonWords.containsKey(strippedSig)))
                        {
                        	out.write("<keyword name=\""+strippedSig+"\" confidence=\""+f.format(weight)+"\" />\n");
                        	ht.put(strippedSig, "1");
                        }
                    }
                    else
                    {
                        out.write("<keyword name=\""+signatureTerm+"\" confidence=\""+f.format(weight)+"\" />\n");
                        ht.put(getTefName(signatureTerm), "1");
                    }
                }
            }
            /*

              Do thesaurus expansion of terms ( do this last if at all )

              <keyword name="accident" confidence="80" />
              <keyword name="grounding #(running aground)" confidence="80" />
              <keyword name="running aground" confidence="80" />
            */
            out.write("      </evidenceSet>\n");
            out.write("   </evidence>\n");
            out.write("</definition>\n");

            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }

            buildChildren(server, n, out);
            out.write("</category>\n");
        }
    }

    public static String getTefName(String s) {
    	
        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        s = s.replaceAll("\"", "&quot;");
        s = s.replaceAll(">", "&gt;");
        s = s.replaceAll("/", "-");
        s = s.replaceAll(":", " -");
        s = s.replaceAll("  ", " ");
                
        //These next three should really be fixed in the taxonomy prior to export or might make funky phrases 
        //s = s.replaceAll("(", "- ");
        //s = s.replaceAll(")", " ");
        //s = s.replaceAll(",", " ");
        
        s = s.replaceAll(" <br> ", "\n");
        
        return s;
    }
}