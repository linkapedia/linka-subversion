package com.iw.tools;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.oculustech.layout.*;
import com.iw.system.ITS;
import com.iw.system.User;
import com.iw.system.NodeDocument;
import com.iw.system.ITSTreeNode;

public class ToolBar extends OculusBox implements FocusListener, MouseListener {
    protected CopyArea classifyArea;

    private static JFrame frame;
    private ITS its;

    public ToolBar() {
        initComponents();
    }

    private void initComponents() {
        try { Login(); }
        catch (Exception e) { e.printStackTrace(System.out); return; }

        com.incors.plaf.alloy.AlloyLookAndFeel.setProperty("alloy.licenseCode", "7#Michael_Hoey#gecuh5#6ygjkk");
        try {
          javax.swing.LookAndFeel alloyLnF = new com.incors.plaf.alloy.AlloyLookAndFeel();
          javax.swing.UIManager.setLookAndFeel(alloyLnF);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
          // You may handle the exception here
        }

        OculusLayoutHelper layout = new OculusLayoutHelper(this);
        layout.setJustification(OculusLayout.TOP_JUSTIFY);

        JLabel labelImage = new JLabel(com.iw.tools.ImageUtils.getImage(this, "itsimages/frameicon.gif"));

        classifyArea = new CopyArea(frame, its);
        classifyArea.setRowSelectionAllowed(false);
        classifyArea.setSelectionForeground(Color.BLUE);
        classifyArea.addFocusListener(this);
        classifyArea.addMouseListener(this);

        layout.add(labelImage);
        layout.add(new JScrollPane(classifyArea));
    }

    public static void openAtBottomRight(JFrame frame) {
        Dimension winsize = frame.getSize(), screensize =
                Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation((screensize.width - 450),
                (screensize.height - 260));
    }

    public void startTimer() {
        classifyArea.startTimer();
    }

    // login routine hardcoded at the moment
    public void Login() throws Exception {
        its = new ITS();
        its.SetAPI("http://66.134.131.57/");
        User u = null;

        try {
            u = its.Login("mhoey", "racerx09");
        } catch (Exception e) {
            throw e;
        }

        String SessionKey = u.getSessionKey();
        its.SetSessionKey(SessionKey);
    }

    public void viewNode(ITSTreeNode n) {
        if ((n.get("NODEDESC") == null) || (n.get("NODEDESC").equals(""))) { return; }
        try {
            String sCommand = "cmd /c explorer \"" + n.get("NODEDESC")+"\"";
            //System.out.println("start view doc with command [" + sCommand + "]");
            java.lang.Runtime.getRuntime().exec(sCommand);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    /*
        ********* EVENT LISTENERS *********
    */
    public void focusGained (FocusEvent e) {
        classifyArea.useCopyBuffer(e);
    }
    public void focusLost (FocusEvent e) { }
    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                frame.getGlassPane().setVisible(true);

                try {
                    if (((MouseEvent) e).isShiftDown() ||
                        ((MouseEvent) e).isControlDown() ||
                        ((MouseEvent) e).isAltDown()) {

                        return;
                    }
                    Point p = ((MouseEvent) e).getPoint();
                    int row = classifyArea.rowAtPoint(p);
                    int col = classifyArea.columnAtPoint(p);

                    if (((MouseEvent) e).getClickCount() == 2) {
                        NodeDocument nd = (NodeDocument) classifyArea.getModel().getValueAt(row, col);
                        ITSTreeNode n = its.getNodeProps(nd.get("NODEID"));

                        viewNode(n);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace(System.out);
                }
            }

            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                frame.getGlassPane().setVisible(false);
            }
        };
        aWorker.start();
    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mousePressed(MouseEvent event) {
    }

    // Main Function
    public static void main(String[] args) throws Exception {
        // insert your license # below to avoid unlicensed warning popups
        OculusLayout.setLicenseNumber("JPE5AQEMAQMP");
        frame = new JFrame("Britannica on Demand");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ToolBar pane = new ToolBar();
        frame.setContentPane(pane);
        //frame.pack();
        frame.setSize(450, 225);
        openAtBottomRight(frame);
        frame.setBackground(Color.lightGray);

        ImageIcon icon = com.iw.tools.ImageUtils.getImage(frame, "itsimages/frameicon.gif");
        frame.setIconImage(icon.getImage());

        frame.show();

        pane.startTimer();
    }
}
