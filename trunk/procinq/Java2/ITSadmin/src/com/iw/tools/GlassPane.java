package com.iw.tools;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.InternalFrameAdapter;

/**
 * This is the glass pane class that intercepts screen interactions during system busy states.
 *
 * @author Yexin Chen
 */
public class GlassPane extends JComponent implements AWTEventListener {
    private Window theWindow;
    private Component activeComponent;

    private static MouseAdapter mouseListen;
    private static KeyAdapter keyListen;
    private static InternalFrameListener frameListen;

    /**
     * GlassPane constructor comment.
     * @param Container a
     */
    protected GlassPane(Component activeComponent) {
        // add adapters that do nothing for keyboard and mouse actions
        mouseListen = new MouseAdapter() {};
        keyListen = new KeyAdapter() {};

        addMouseListener(mouseListen);
        addKeyListener(keyListen);

        setActiveComponent(activeComponent);
    }

    /**
     * Receives all key events in the AWT and processes the ones that originated from the
     * current window with the glass pane.
     *
     * @param event the AWTEvent that was fired
     */
    public void eventDispatched(AWTEvent event) {
        Object source = event.getSource();

        // discard the event if its source is not from the correct type
        boolean sourceIsComponent = (event.getSource() instanceof Component);

        if ((event instanceof KeyEvent) && sourceIsComponent) {
            // If the event originated from the window w/glass pane, consume the event
            if ((SwingUtilities.windowForComponent((Component) source) == theWindow)) {
                ((KeyEvent) event).consume();
            }
        }
    }

    /**
     * Finds the glass pane that is related to the specified component.
     *
     * @param startComponent the component used to start the search for the glass pane
     * @param create a flag whether to create a glass pane if one does not exist
     * @return GlassPane
     */
    public synchronized static Vector mount(Component startComponent, boolean create) {
        Component aComponent = startComponent;
        RootPaneContainer aContainer = getTopAncestor(aComponent);
        Vector vGlassPanes = new Vector();

        if (aContainer != null) {
            // Retrieve an existing GlassPane if old one already exist or create a new one, otherwise return null
            if ((aContainer.getGlassPane() != null) && (aContainer.getGlassPane() instanceof GlassPane)) {
                clearListeners((GlassPane) aContainer.getGlassPane());
                aContainer.getGlassPane().addMouseListener(mouseListen);
                aContainer.getGlassPane().addKeyListener(keyListen);

                vGlassPanes.add((GlassPane) aContainer.getGlassPane());
                return vGlassPanes;
            } else if (create) {
                Component oldGlassPane = aContainer.getGlassPane();

                GlassPane aGlassPane = new GlassPane(startComponent);
                aContainer.setGlassPane(aGlassPane);
                vGlassPanes.add(aGlassPane);
                vGlassPanes.add(oldGlassPane);

                return vGlassPanes;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public synchronized static void unmount(Component startComponent, Component oldGlass) {
        Component aComponent = startComponent;
        RootPaneContainer aContainer = getTopAncestor(aComponent);

        if (aContainer != null && oldGlass != null) {
            aContainer.setGlassPane(oldGlass);
        }
    }

    public synchronized static RootPaneContainer getTopAncestor(Component c) {
        // Climb the component hierarchy until a RootPaneContainer is found or until the very top
        while ((c.getParent() != null) && !(c instanceof RootPaneContainer)) {
            c = (Component) c.getParent();
        }

        // Guard against error conditions if climb search wasn't successful
        if (c instanceof RootPaneContainer) {
            return (RootPaneContainer) c;
        }

        return null;
    }

    public synchronized static void clearListeners(GlassPane gp) {
        gp.removeMouseListener(mouseListen);
        gp.removeKeyListener(keyListen);
    }

    /**
     * Set the component that ordered-up the glass pane.
     *
     * @param aComponent the UI component that asked for the glass pane
     */
    private void setActiveComponent(Component aComponent) {
        activeComponent = aComponent;
    }

    /**
     * Sets the glass pane as visible or invisible. The mouse cursor will be set accordingly.
    */
}
