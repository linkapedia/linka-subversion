package com.iw.tools.image.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.LinkedHashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class PanelViewer extends JPanel implements IPaint {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4738400656696581174L;
	private Image image = null;
	private LinkedHashMap<String, String> map;

	public PanelViewer() {
		this.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK));
		
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(g);
	}
	public void ResetCurrentInfo(){
		image = new ImageIcon("").getImage();
		onPaint(image, new LinkedHashMap<String, String>());
	}

	public void setImage(Image image) {
		this.image = image;
	}
	@Override
	public LinkedHashMap<String, String> getCurrentInfo() {
		return map;
	}

	@Override
	public void onPaint(Image image, LinkedHashMap<String, String> map) {
		this.image = image;
		this.map = map;
		this.repaint();
	}

}
