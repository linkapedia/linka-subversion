package com.iw.tools;

public class EmitFilterGoogle {

	public static final int CUSTOM_SEARCH_ORG_EDU = 1;
	public static final int CUSTOM_SEARCH_COM = 2;

	private static final String MESSAGE1 = "Search by .org and .edu";
	private static final String MESSAGE2 = "Search by .com";

	public static String getMessage(int custom) {
		String msn = "";
		switch (custom) {
		case CUSTOM_SEARCH_ORG_EDU:
			msn = MESSAGE1;
			break;
		case CUSTOM_SEARCH_COM:
			msn = MESSAGE2;
			break;
		default:
			msn = "uncaught";
			break;
		}
		return msn;
	}
}
