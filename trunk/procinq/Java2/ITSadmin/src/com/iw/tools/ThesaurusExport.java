package com.iw.tools;

import com.iw.system.Corpus;
import com.iw.system.ITS;
import com.iw.system.Thesaurus;
import com.iw.system.ThesaurusWord;


import com.iw.system.*;
import com.iw.ui.ITSAdministrator;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class ThesaurusExport {
    public void ThesaurusExport(ITSAdministrator ITSframe, Thesaurus thesaurus) throws Exception { 
    //final Vector listData = new Vector();
    Vector listData = new Vector();
    Vector listSyn = new Vector();
    
    SortedList wordList = new SortedList();
    //private int totalNodes = 1;
    //private int nodesProcessed = 1;
   // private PopupProgressBar ppb = null;

    //private DecimalFormat twoDigits = new DecimalFormat("0");
    
    
        File f = new File(thesaurus.getName().replaceAll(" ", "_")+".ths");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");
       

        // *** WRITE THE HEADER HERE
        //listData = new Vector();
        //listData = ITSframe.its.getThesaurusWords(thesaurus.getID(), "%");
        listData = ITSframe.its.getThesaurusWords(thesaurus.getID(),"%");
        Collections.sort(listData);
       
        // we now have a list of all words in the listData vector.
        // now I need to iterat through the vector and print the stuff out
        //Collections.sort(listData);
        if (listData.size()>0) {
        	
        	// we have a list of terms
        	for (int i=0; i< listData.size();i++){
        		out.write(listData.get(i).toString() + "\r\n");
        		// anchor term written, now check for syn
        		
        		System.out.println("Term Name; "+listData.get(i).toString());
        		
        		//listSyn = ITSframe.its.getSynonyms(thesaurus.getID(),listData.);
        		listSyn = ITSframe.its.getSynonyms(thesaurus.getID(),listData.get(i).toString().trim());
        		
        		
        		System.out.println("SYN Name; "+listSyn.get(0).toString() + "\r\n");
        		Vector vSimi = new Vector();
                Vector vBroa = new Vector();
                Vector vNarr = new Vector();

                for (int j = 0; j < listSyn.size(); j++) {
                	ThesaurusWord tw = (ThesaurusWord) listSyn.elementAt(j);
                    switch (tw.getRelationship()) {
                        case 1:
                            vSimi.add(tw);
                           
                            break;
                        case 2:
                            vBroa.add(tw);
                           
                            break;
                        case 3:
                            vNarr.add(tw);
                           
                            break;
                        default:
                            break;
                    }
                }
                

                // Load the relationship list boxes as appropriate.
                if (!vSimi.isEmpty()) {
                	out.write("\tSYN \r\n");
                	for (int k = 0;k < vSimi.size();k++)
                	{
                		out.write("\t"  + vSimi.get(k).toString() + "\r\n");
                	}
                	
                    //simiList.setListData(vSimi);
                    //simiList.validate();
                } else {
                    //simiList.setListData(new Vector());
                }
                if (!vBroa.isEmpty()) {
                	out.write("\tBROADER TERM \r\n");
                	for (int k=0;k<vBroa.size();k++)
                	{
                		out.write("\t"  + vBroa.get(k).toString() + "\r\n");
                	}
                    //broaList.setListData(vBroa);
                    //broaList.validate();
                } else {
                    //broaList.setListData(new Vector());
                }
                if (!vNarr.isEmpty()) {
                	out.write("\tNARROWER TERM");
                	for (int k=0;k<vNarr.size();k++)
                	{
                		out.write("\t"  + vNarr.get(k).toString()+ "\r\n");
                	}
                    //narrList.setListData(vNarr);
                    //narrList.validate();
                } else {
                    //narrList.setListData(new Vector());
                }
                
        		 		
        		
        		
        		
        	}
        	
        	
        	
        	
        }
       
        
       // wordList = new SortedList(listData);
        //wordList.setListData(listData);
        //wordList.sort();
        //wordList.validate();
        //wordList.
        

        // *** WRITE ROOT NODE INFORMATION HERE


        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        out.close();
      return;
    }




}