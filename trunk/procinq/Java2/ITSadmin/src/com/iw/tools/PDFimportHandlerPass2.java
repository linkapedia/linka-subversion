package com.iw.tools;

import java.io.IOException;
import java.util.*;
import java.text.DecimalFormat;

import org.xml.sax.*;
import org.dom4j.*;
import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

public class PDFimportHandlerPass2
        implements ElementHandler {

    public Hashtable htElements; // all distinct pdf element objects

    private Corpus rootCorpus = null;
    private ITSTreeNode rootNode = null;
    private ITS its = null;

    private String nodeSource = "";
    private String sSource = "";
    private String nodeTitle = "Untitled Topic";
    private int currStatus = -1;

    private float lastY = -1;
    private String lastID = "";

    // variables to update progress bar
    private int size = -1;
    private int nodesInserted = 0;
    private PopupProgressBar ppb = null;
    private DecimalFormat twoDigits = new DecimalFormat("0");

    // node attributes
    private int depthFromRoot = -1;
    private int[] parent = new int[10];

    private Vector vPDFelements = new Vector();

    // constructor
    public PDFimportHandlerPass2 () { super(); }
    public PDFimportHandlerPass2 (Hashtable directives) {
        super(); htElements = directives;
        System.out.println("Begin Pass 2...");

        parent[0] = -1;
    }
    public PDFimportHandlerPass2 (Hashtable directives, Corpus c, ITSTreeNode n, ITS its, int size, PopupProgressBar ppb) {
        super(); htElements = directives; this.ppb = ppb; this.size = size;
        System.out.println("Begin Pass 2...");

        parent[0] = Integer.parseInt(n.get("NODEID"));
        rootCorpus = c; rootNode = n; this.its = its;
    }

    public void closeOut() {
        if (!nodeTitle.equals("")) {
            System.out.println("THE END? Node: " + nodeTitle + " Depth: " + depthFromRoot);

            ITSTreeNode node = its.emptyNode(rootCorpus.getID(), nodeTitle);
            try {
                node.set("PARENTID", parent[depthFromRoot - 1] + "");

                nodesInserted++;
                long perc = (nodesInserted * 100) / size;
                if (ppb != null) ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));

                node = its.addNode(node);
                its.setNodeSource(node, nodeSource);
                parent[depthFromRoot] = Integer.parseInt((String) node.get("NODEID"));
            } catch (Exception e) {
                System.err.println("Could not add node " + nodeTitle);
                e.printStackTrace(System.out);
            }
        }
    }

    public void onStart(ElementPath ep) {
    }

    // Parse each text section sequentially.   As each one is encountered, decide which of the
    //    following actions should be taken.
    //
    // STATUS   ACTION
    //   0      Take no action.  Ignore this tag, and append the text to the previous section.
    //   1      Omit this section.  Same as status 0 except nothing is appended.
    //   2-8    Section belongs to a node of a specific level.  Close off the previous section,
    //          create the node and reset variables to the new section.
    public void onEnd(ElementPath ep) {
        // a paragraph should apply its attributes to all text sections that it encompasses
        if (ep.getPath().endsWith("TEXT")) {
            PDFelement pdf = new PDFelement(ep);

            if (pdf.example[0].length() < 2) return;

            vPDFelements.add(pdf);

        } else if (ep.getPath().endsWith("PARAGRAPH")) {
            /* EXAMPLE:

ID: 4-AdvTimesfalse-false-false-None-None-0-None Text: final proof chapters
ID: 4-AdvTimesfalse-false-false-None-None-0-None Text: CLK9001
ID: 4-AdvP4C4E59false-false-false-None-None-0-None Text: 12th November 2003
ID: 4-AdvP4C4E59false-false-false-None-None-0-None Text: 10:38
ID: 15-AdvTT5360b559.Bfalse-false-false-None-None-0-None Text: Hospital Toxicology
ID: 9-AdvTimes-ifalse-false-false-None-None-0-None Text: D R A Uges
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: Hospital toxicology is concerned with individuals admitted to
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: hospital with suspected poisoning and its prime aim is to assist
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: in the treatment of the patient. The range of substances that may
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: be encountered is huge and ideally the hospital laboratory will
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: have the capability to identify and,if required,quantify pharma-
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: ceutical agents,illicit drugs,gases,solvents,pesticides,toxic metals
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: and a host of other industrial and environmental poisons in
ID: 6-AdvTimesfalse-false-false-None-None-0-None Text: biological fluids. In practice,few laboratories can offer such a

            */
            currStatus = -2;

            ParagraphElement pe = new ParagraphElement(ep);

            for (int i = 0; i < vPDFelements.size(); i++) {
                PDFelement pdf = (PDFelement) vPDFelements.elementAt(i);
                pdf.setParagraph(pe);

                String ID = pdf.getID();
                if (allCaps(pdf.example[0])) ID = ID + "-allcaps";

                if (!ITS.getUser().sameLines()) {
                    if (lastY == pdf.Y) { ID = lastID; }
                    else { lastID = ID; lastY = pdf.Y; }
                }

                // status mappings
                //  "Ignore" - 0
                //  "Omit" - 1
                //  "Level 1" - 2
                //  "Level 2" - 3, etc

                /* RULES:

                A. if (status == 1) return (do nothing)
                B. else if (status == 0) add to the source, reset currstatus
                C. else if (status == currStatus) add to the title, reset currstatus
                D. else if (status != currStatus)
                   (i) if currStatus != -1 write current node
                   (ii) reset source, currstatus and title to current
                */

                int status = ((Integer) htElements.get(ID)).intValue();
                //System.out.println("**DEBUG** status: "+status+" text: "+pdf.example[0]);
                //System.out.println("ID: "+ID+" Status: "+status+" CurrStatus: "+currStatus+" Text: "+pdf.example[0]);

                if (status != 1) { // (a)
                    // if status is 0, ignore the tag and continue to append it (b)
                    if (status == 0) {
                        // if the line ends in a dash, a word was truncated with a hyphen.  Concat these words now.
                        if (nodeSource.endsWith("-")) {
                            nodeSource = nodeSource.substring(0, nodeSource.length()-1) + pdf.example[0].trim();
                        } else {
                            nodeSource = nodeSource + "\n" + pdf.example[0];
                        }
                        currStatus = 0;
                    }
                    // if the status is the same as the previous section, keep adding it (c)
                    else if (currStatus == status) { nodeTitle = nodeTitle + " " + pdf.example[0]; }
                    // if status is greater than 1 and different from the current status, create a node (d)
                    else if (status > 1) {
                        if (currStatus != -1) { // write off previous node HERE:
                            System.out.println("Node: "+nodeTitle+" Depth: "+depthFromRoot);
                            //System.out.println("Source: "+nodeSource);
                            //System.out.println("Depth: "+depthFromRoot);
                            //System.out.println(" ");

                            ITSTreeNode node = its.emptyNode(rootCorpus.getID(), nodeTitle);
                            try {
                                node.set("PARENTID", parent[depthFromRoot-1]+"");

                                nodesInserted++;
                                long perc = (nodesInserted*100) / size;
                                if (ppb != null) ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));

                                node = its.addNode(node);
                                its.setNodeSource(node, nodeSource);
                                parent[depthFromRoot] = Integer.parseInt((String) node.get("NODEID"));
                            } catch (Exception e) {
                                System.err.println("Could not add node "+nodeTitle);
                                e.printStackTrace(System.out);
                            }

                            nodeSource = ""; nodeTitle = "";
                        }
                        nodeTitle = pdf.example[0];
                        depthFromRoot = status - 1;
                        currStatus = status;

                        System.out.println("Forward node: "+nodeTitle+" Forward depth: "+depthFromRoot);
                    }
                }
            }

            vPDFelements.clear();
        }
    }
    private boolean allCaps(String s) {
        char[] cArr = s.toCharArray();

        for (int i = 0; i < cArr.length; i++) {
            int asc = (int) cArr[i];
            if ((asc > 96) && (asc < 123)) { return false; }
        }

        return true;
    }
}

