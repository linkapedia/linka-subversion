package com.iw.tools.image.ui;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.iw.system.FileMetadata;
import com.iw.tools.image.util.PanelMinViewer;
import com.iw.tools.image.util.PanelViewer;

public class Manager extends JPanel {

	/**
	 * @author andres
	 */
	private static final long serialVersionUID = -2031961556002925111L;
	private PanelViewer pv;
	private PanelMinViewer pm;
	private JButton btnNext;
	private JButton btnPrev;
	private JPanel panelNext;
	private JPanel panelPrev;
	private JPanel panelMin;
	private JPanel panelButtons;

	public Manager() {
	}

	public void Build() {
		setLayout(new GridBagLayout());

		panelNext = new JPanel();
		panelPrev = new JPanel();

		panelButtons = new JPanel();
		panelButtons.setLayout(new FlowLayout());
		panelButtons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		btnNext = new JButton(com.iw.tools.ImageUtils.getImage(this,"files/ManagerImages/images/next.png"));
		btnPrev = new JButton(com.iw.tools.ImageUtils.getImage(this,"files/ManagerImages/images/prev.png"));
		panelNext.add(btnNext);
		panelPrev.add(btnPrev);
		panelMin = new JPanel();
		panelMin.setLayout(new GridLayout(1, 7));
		pv = new PanelViewer();
	}

	public void setFileMetaData(ArrayList<FileMetadata> afmd){
		this.removeAll();
		panelMin.removeAll();
		pv.ResetCurrentInfo();
		if(afmd==null)	
			return;
		panelMin.add(panelPrev);
		Image image = null;
		for(int i=0;i<5;i++){
			LinkedHashMap<String, String> mapInfo = new LinkedHashMap<String, String>();
			if(afmd.size()>i){
				image = new ImageIcon(afmd.get(i).getImage()).getImage();
				mapInfo.put("IMAGEID",afmd.get(i).getId());
				mapInfo.put("URI", afmd.get(i).getUri());
				mapInfo.put("DESCRIPTION", afmd.get(i).getDescription());
				pm = new PanelMinViewer(image,mapInfo, pv);
			}else{
				image = new ImageIcon("").getImage();
				pm = new PanelMinViewer(image,new LinkedHashMap<String, String>(), pv);
			}
			panelMin.add(pm);
		}
		panelMin.add(panelNext);
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.gridheight = 2;
		constraints.weighty = 1.0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.fill = GridBagConstraints.BOTH;
		add(pv, constraints);
		constraints.weighty = 0.0;
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.gridwidth = 2;
		constraints.gridheight = 2;
		add(panelMin, constraints);
	}
	public String getCurrentImageID(){
		LinkedHashMap<String, String> map = pv.getCurrentInfo();
		if(map!=null){
			return map.get("IMAGEID");
		}else{
			return null;
		}
		
	}

}
