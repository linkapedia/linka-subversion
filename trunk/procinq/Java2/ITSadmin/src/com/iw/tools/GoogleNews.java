/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Apr 11, 2005
 * Time: 2:23:39 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.tools;

import com.iw.system.*;

import java.util.*;
import java.util.regex.*;
import java.net.*;
import java.io.*;

public class GoogleNews {
    public static Hashtable getLinks(ITSTreeNode n) throws Exception {
        return getLinks(n.get("NODETITLE"));
    }
    public static Hashtable getLinks(String title) throws Exception {
        String url = "http://news.google.com/news?num=100&hl=en&lr=&q="+URLEncoder.encode(title, "UTF-8")+"&c2coff=1&sa=N&tab=wn";
        return fillHash(url, title);
    }

    public static String getData (String URL) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");
        boolean bPDFDocument = false;

        try {
            URL myURL = new URL(URL);

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "other";
            if (httpCon.getContentType().equals("text/html")) { sExt = "html"; }
            if (httpCon.getContentType().equals("plain/text")) { sExt = "txt"; }
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; bPDFDocument = true; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; }

            int len = 0; File tempFile = null; FileOutputStream fos = null;

            byte[] buffer = new byte[512];
            int bytesRead = 0;

            while( (len=bis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
                returnString = returnString.append(new String(buffer));
            } bis.close(); httpCon.disconnect();

            String s = returnString.toString();

            long lEnd = System.currentTimeMillis() - lStart;
            //System.out.println("Item content length is "+s.length()+", retrieved "+URL+" in "+lEnd+" ms.");

            return s;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms attempting to retrieve : "+URL+" : "+e.toString());
            //e.printStackTrace(System.out);
            return "";
        } finally {
            // Check to make sure that we have not exceeded our maximum run time.  If we have,
            //  please dump our memory image out to disk and empty out our processing.

            ///// TO DO /////
        }
    }

    public static File getDataFile (String URL) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");
        boolean bPDFDocument = false;

        try {
            URL myURL = new URL(URL);

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setFollowRedirects(true);
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "other";
            if (httpCon.getContentType().equals("text/html")) { sExt = "html"; }
            if (httpCon.getContentType().equals("plain/text")) { sExt = "txt"; }
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; bPDFDocument = true; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; }

            int len = 0; File tempFile = null; FileOutputStream fos = null;

            byte[] buffer = new byte[512];
            int bytesRead = 0;

            while( (len=bis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
                returnString = returnString.append(new String(buffer));
            } bis.close(); httpCon.disconnect();

            String s = returnString.toString();

            File dir = new File("googlenews");
            if (!dir.exists()) dir.mkdir();

            tempFile = new File(dir.getAbsolutePath()+"/"+clean(URL)+"."+sExt);
            //System.out.println("Writing to temp file: "+tempFile.getAbsolutePath());

            fos = new FileOutputStream(tempFile);
            fos.write(bos.toByteArray());

            bos.flush(); fos.flush();
            bos.close(); fos.close();

            long lEnd = System.currentTimeMillis() - lStart;
            //System.out.println("Item content length is "+s.length()+", retrieved "+URL+" in "+lEnd+" ms.");

            return tempFile;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms attempting to retrieve : "+URL+" : "+e.toString());
            //e.printStackTrace(System.out);
            return null;
        } finally {
            // Check to make sure that we have not exceeded our maximum run time.  If we have,
            //  please dump our memory image out to disk and empty out our processing.

            ///// TO DO /////
        }
    }

    private static Hashtable fillHash(String url, String title) throws Exception {
        Hashtable urls = new Hashtable();

        String page = getData(url).replaceAll("\n", "");
        String total = "-1"; int loop = 0;

        // get numbers
        Pattern p = Pattern.compile("</b> of <b>(.*?)</b> for", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(page); boolean bResult = m.find();

        if (bResult) { total = m.group(1); } // System.out.println("Found "+total+" results for word '"+title+"'."); }

        p = Pattern.compile("</b> of about <b>(.*?)</b> for", Pattern.CASE_INSENSITIVE);
        m = p.matcher(page); bResult = m.find();

        if (bResult) { total = m.group(1); } //System.out.println("Found "+total+" results for word '"+title+"'."); }

        // loop through each result and write the output to a file
        // <a href="http://www.jsonline.com/news/racine/apr05/316294.asp">Vigil will remember victims of crimes</a> <font size=-1 color=#6f6f6f><nobr>Milwaukee Journal Sentinel</nobr></font></font><br>
        p = Pattern.compile("<a href=\"(.*?)\" id=(.*?)>(.*?)</a><br>", Pattern.CASE_INSENSITIVE);
        m = p.matcher(page);
        bResult = m.find();

        while (m.find() && (loop < 100)) {
            String sURL = m.group(1);
            String sTitle = m.group(3);

            sTitle = sTitle.replaceAll("<(.*?)>", "");

            if ((sURL.indexOf("<") == -1) && (sURL.indexOf("google") == -1)) {
                loop++; urls.put(sURL, sTitle);
            }
        }

        System.out.println(loop+" of "+total+" documents extracted.\n");

        return urls;
    }
    public static String clean(String str) {
        String sout = "";

        char ch;       // One of the characters in str.
        int i;         // A position in str, from 0 to str.length()-1.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch))
                sout = sout + Character.toLowerCase(ch);
        }

        //System.out.println("return: "+sout);
        return sout;
    }
}
