package com.iw.tools;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

public class JSortTable extends JTable
        implements MouseListener {
    protected int sortedColumnIndex = -1;
    protected boolean sortedColumnAscending = true;

    public JSortTable() {
        this(new DefaultSortTableModel());
    }

    public JSortTable(int rows, int cols) {
        this(new DefaultSortTableModel(rows, cols));
    }

    public JSortTable(Object[][] data, Object[] names) {
        this(new DefaultSortTableModel(data, names));
    }

    public JSortTable(Vector data, Vector names) {
        this(new DefaultSortTableModel(data, names));
    }

    public JSortTable(SortTableModel model) {
        super(model);
        initSortHeader();
    }

    public JSortTable(SortTableModel model,
                      TableColumnModel colModel) {
        super(model, colModel);
        initSortHeader();
    }

    public JSortTable(SortTableModel model,
                      TableColumnModel colModel,
                      ListSelectionModel selModel) {
        super(model, colModel, selModel);
        initSortHeader();
    }

    protected void initSortHeader() {
        JTableHeader header = getTableHeader();
        header.setDefaultRenderer(new SortHeaderRenderer());
        header.addMouseListener(this);
    }

    public int getSortedColumnIndex() {
        return sortedColumnIndex;
    }

    public boolean isSortedColumnAscending() {
        return sortedColumnAscending;
    }

    /*
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
    {
        Component cell = super.prepareRenderer( renderer, row, column );
        // We do not want to change the row that is marked, so we check for that
        // first.

        System.out.println("is selected? "+row+" "+column);
        if (isCellSelected(row, column)) cell.setBackground(Color.BLUE);
        return cell;
    }


    public boolean editCellAt(int row, int column, EventObject e) {
        if (cellEditor != null && !cellEditor.stopCellEditing()) {
            return false;
        }

        if (row < 0 || row >= getRowCount() ||
                column < 0 || column >= getColumnCount()) {
            return false;
        }

        if (!isCellEditable(row, column))
            return false;

        /*
        if (editorRemover == null) {
            KeyboardFocusManager fm =
                    KeyboardFocusManager.getCurrentKeyboardFocusManager();
            editorRemover = new CellEditorRemover(fm);
            fm.addPropertyChangeListener("permanentFocusOwner", editorRemover);
        }

        TableCellEditor editor = getCellEditor(row, column);
        if (editor != null && editor.isCellEditable(e)) {
            editorComp = prepareEditor(editor, row, column);
            editorComp.setBackground(Color.BLUE);
            if (editorComp == null) {
                removeEditor();
                return false;
            }
            editorComp.setBounds(getCellRect(row, column, false));
            add(editorComp);
            editorComp.validate();

            setCellEditor(editor);
            setEditingRow(row);
            setEditingColumn(column);
            editor.addCellEditorListener(this);

            return true;
        }
        return false;
    }
          */

    public void mouseReleased(MouseEvent event) {

        TableColumnModel colModel = getColumnModel();
        int index = colModel.getColumnIndexAtX(event.getX());
        int modelIndex = colModel.getColumn(index).getModelIndex();

        SortTableModel model = (SortTableModel) getModel();
        if (model.isSortable(modelIndex)) {
            // toggle ascension, if already sorted
            if (sortedColumnIndex == index) {
                sortedColumnAscending = !sortedColumnAscending;
            }
            sortedColumnIndex = index;

            model.sortColumn(modelIndex, sortedColumnAscending);
        }
    }

    public void mousePressed(MouseEvent event) {
    }

    public void mouseClicked(MouseEvent event) {
    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }
}

