package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class InxightOutConteg extends ExportElement {
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable filtercommonWords = new Hashtable();
    public String corpusID = null;
    public String wordName = "terms";

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));
        corpusID = c.getID();

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        DataInputStream disfilter = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\filtercommonwords.txt"))));
        

        String filterrecord = null;
        try {
            while ( (filterrecord=disfilter.readLine()) != null ) { filtercommonWords.put(filterrecord.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file filterwords.txt");
        }

        
        
        
        
        
        
        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { System.err.println("No root node found for corpus "+corpusID); return null; }

        File f = new File(c.getName().replaceAll(" ", "_")+"-ixt.xml");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<inxight-taxonomy xmlns=\"http://www.inxight.com/taxonomy/4.0\" xmlns:st=\"http://www.inxight.com/taxonomy/startree/4.0\" name=\"\">\n");
        out.write("        <st:display-properties layout=\"radial\" clockwise=\"true\" stretchfactor=\"1.0\" style=\"-1\" ");
        out.write("backgroundcolor=\"0xffffff\" selectioncolor=\"0x2cd300\" highlightcolor=\"0x2cd300\" ");
        out.write("textsizemode=\"fittonodearea\" maxchars=\"30\" nchars=\"13\" nodearrangement=\"top\" nodelabelalignment=\"center\">\n");
        out.write("          <st:textfont name=\"dialog\" size=\"12\" bold=\"false\" italic=\"false\"></st:textfont>\n");
        out.write("     </st:display-properties>\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        out.write("        <node>\n");
        out.write("           <label>"+rootNode.get("NODETITLE")+"</label>\n");
        out.write("           <description>"+rootNode.get("NODEDESC")+"</description>\n");
        out.write("           <hierarchy enforce-parent=\"false\" aggregate=\"false\"></hierarchy>\n");
        out.write("           <attributes>\n");
        out.write("              <threshold>0.0</threshold>\n");
        out.write("           </attributes>\n");
        out.write("           <st:node-display-properties nodecolor=\"0xff0033\" textcolor=\"0xffffff\"></st:node-display-properties>\n");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, out, "Root", "RootParent");

        out.write("        </node>\n");
        out.write("</inxight-taxonomy>\n");

        out.close();

        return f;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer out, String SICCODE, String SICParent) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() < 1) return;
        
        
        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);
            String fileName = "";
            if (vSignatures.size() > 0) fileName = writeSample(n, vSignatures);

            //if (p.get("PARENTID").equalsIgnoreCase("-1")){
            	if (n.get("NODETITLE").startsWith("SIC")){
            	       
            	System.out.println("**DEBUG DEPTH**" + p.get("PARENTID"));
            	System.out.println("**DEBUG NODETITLE**" + n.get("NODETITLE"));
            	
            	String LEV2NODE = n.get("NODETITLE");
            	//SICParent = n.get("NODETITLE");
            	SICParent = "RootParent";
            	
            	String[] sSIC = LEV2NODE.split(" ");
            	SICCODE = sSIC[1];
            	// this is taking the second word in the title.
            	System.out.println("**DEBUG SIC**" + SICCODE);
            	
            }
            
            
            String spaces = "           ";
            for (int j = 0; j < Integer.parseInt(n.get("DEPTHFROMROOT")); j++) { spaces = spaces + " "; }

            out.write(spaces+"<link>\n");
            out.write(spaces+"   <st:link-display-properties linkcolor=\"0x808080\"></st:link-display-properties>\n");
            out.write(spaces+"   <node>\n");
          
            out.write(spaces+"      <label>"+SICCODE.trim()+ "|INDUSTRY|"+ getEncodedName(n.get("NODETITLE").trim())+"</label>\n");
            out.write(spaces+"      <description>"+getEncodedName(n.get("NODEDESC"))+"</description>\n");
            out.write(spaces+"      <hierarchy enforce-parent=\"false\" aggregate=\"false\"></hierarchy>\n");
            out.write(spaces+"      <attributes>\n");

            if ((vSignatures.size() == 0) && (wordName.toLowerCase().equals("terms"))) out.write(spaces+"         <filter></filter>\n");
            // else out.write(spaces+"         <filter>"+returnWords(n.get("NODETITLE")).toLowerCase()+" | "+
            
            
            else {
            	// only use parent titles if the parent has SIC in the title ( for filters )
            	if (!SICParent.equalsIgnoreCase("RootParent")) {
            		out.write(spaces+"         <filter>"+returnWordsAmpersand(FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE")) + " " + FixTitle(SICParent)) + "</filter>\n");	
            	}
            	else {
            	out.write(spaces+"         <filter>"+returnWordsAmpersand(FixTitle(n.get("NODETITLE"))) + "</filter>\n");
            	}
            }
            if (wordName.toLowerCase().equals("terms")) out.write(spaces+"         <threshold>0.1</threshold>\n");
            else out.write(spaces+"         <threshold>0.75</threshold>\n");

            if (wordName.toLowerCase().equals("terms")) {
                if (vSignatures.size() == 0) out.write(spaces+"         <"+wordName+"></"+wordName+">\n");
                else 
                	if (!SICParent.equalsIgnoreCase("RootParent")){
                		out.write(spaces+"         <"+wordName+">"+returntermswithtitle(signatures.InxightToString(),FixTitle(p.get("NODETITLE")) + " " + FixTitle(n.get("NODETITLE")) +" " + FixTitle(SICParent) )+"</"+wordName+">\n");	
                	}
                	else{
                		out.write(spaces+"         <"+wordName+">"+returntermswithtitle(signatures.InxightToString(),FixTitle(n.get("NODETITLE")) )+"</"+wordName+">\n");	
                	}
                	
                	
            } else {
            	
            	if (!SICParent.equalsIgnoreCase("RootParent")){
            		out.write(spaces+"         <"+wordName+">"+returnWords(FixTitle(n.get("NODETITLE"))).toLowerCase()+" | "+
                            returnWords(FixTitle(p.get("NODETITLE"))).toLowerCase()+ returnWords(FixTitle(SICParent)).toLowerCase() +"</"+wordName+">\n");	
            	}
            	else {
                out.write(spaces+"         <"+wordName+">"+returnWords(FixTitle(n.get("NODETITLE"))).toLowerCase()+"</"+wordName+">\n");
            	}
            }

            out.write(spaces+"         <xdocs>\n");
            if (vSignatures.size() > 0) {
                out.write(spaces+"            <document>\n");
                out.write(spaces+"               <uri name=\""+fileName+"\" />\n");
                out.write(spaces+"            </document>\n");
            }
            out.write(spaces+"         </xdocs>\n");

            String color = "0x66ff66";
            switch (Integer.parseInt(n.get("DEPTHFROMROOT"))) {
                case 1: color = "0x66ff66"; break;
                case 2: color = "0xFF8080"; break;
                case 3: color = "0x00C0C0"; break;
                case 4: color = "0x4040FF"; break;
                case 5: color = "0xC0C0FF"; break;
                case 6: color = "0xFFFF40"; break;
                case 7: color = "0xC0FFC0"; break;
                case 8: color = "0xFF00FF"; break;
            }

            out.write(spaces+"      </attributes>\n");
            out.write(spaces+"      <st:node-display-properties nodecolor=\""+color+"\" textcolor=\"0x0000ff\"></st:node-display-properties>\n");
            
            if (n.get("NODETITLE").startsWith("SIC")){
            	SICParent = n.get("NODETITLE");
            }
            if (n.get("NODEID").equals(n.get("LINKNODEID"))) buildChildren(server, n, out, SICCODE,SICParent);

            out.write(spaces+"   </node>\n");
            out.write(spaces+"</link>\n");

            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }
        }
    }

    private String writeSample(ITSTreeNode n, Vector signatures) throws Exception {
        if (signatures.size() == 0) throw new Exception("This topic has no signatures.");

        try {
            File f = createOrGetFile("sample");
            f = createOrGetFile("sample/"+corpusID);
            f = createOrGetFile("sample/"+corpusID+"/"+n.get("NODEID"));

            f = new File("sample/"+corpusID+"/"+n.get("NODEID")+"/sample.htm");
            if (f.exists()) f.delete();

            // write header
            FileOutputStream fos = new FileOutputStream(f);
            Writer out = new OutputStreamWriter(fos, "UTF8");

            out.write("<HTML>\r\n<HEAD>\r\n<TITLE>"+n.get("NODETITLE")+"</TITLE>\r\n</HEAD>\r\n<BODY>\r\n");

            // calculate the filler frequency
            double totalwordfreq = 0;
            for (int i = 0; i < signatures.size(); i++) {
                Signature s = (Signature) signatures.elementAt(i);
                totalwordfreq = totalwordfreq + s.getWeight();
            }

            double nodesize = Double.parseDouble(n.get("NODESIZE"));
            double fillweight = nodesize - totalwordfreq;
            if (fillweight < 0) fillweight = 0;

            signatures.add(0, new Signature("fillerword", fillweight));

            double outer = 0; double total = 0;

            //System.out.println("**DEBUG** node: "+n.get("NODETITLE")+" file: "+f.getAbsolutePath()+" nodesize: "+nodesize+" fillweight: "+
            //        fillweight+" totalwordfreq: "+totalwordfreq+" signatures: "+signatures.size());

            // loop through the signatures and write them
            while (total < nodesize) {
                outer++;
                for (int i = 0; i < signatures.size(); i++) {
                    Signature s = (Signature) signatures.elementAt(i);
                    //System.out.println("**DEBUG** signature: "+s.getWord()+" weight: "+s.getWeight()+" outer: "+outer);
                    if (s.getWeight() >= outer) { out.write(s.getWord()+" "); total++; }
                }

                out.write("\r\n");

                if (total < nodesize) {
                    outer++;
                    for (int i = signatures.size()-1; i > -1; i--) {
                        Signature s = (Signature) signatures.elementAt(i);
                        if (s.getWeight() >= outer) { out.write(s.getWord()+" "); total++; }
                    }
                    out.write("\r\n");
                }
            }

            // write footer
            out.write("</BODY>\r\n</HTML>\r\n");
            out.close();

            return "sample/"+corpusID+"/"+n.get("NODEID")+"/sample.htm";
        } catch (Exception e) { e.printStackTrace(System.err); throw e; }
    }

    private File createOrGetFile(String path) throws Exception {
        File f = new File(path);
        if (!f.exists()) f.mkdir();

        return f;
    }


    private String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method

    private String returnWords(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");

        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!commonWords.containsKey(sArr[i].toLowerCase())) {
                if (i > 0) sb.append(" | ");
                sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
            }
        }

        return sb.toString();
    }
    
    private String returntermswithtitle(String Phrase, String Title) {
        StringBuffer sb = new StringBuffer("");
        sb.append(Phrase.toString());
        
        
        Title = Title.replaceAll(",", "");

        String[] sArr = Title.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!commonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.toString().toLowerCase().indexOf("nocase(\""+replaceWildcards(sArr[i].toLowerCase())+"\")" )==-1){
                		sb.append(" | ");
                        String word = replaceWildcards(sArr[i]);
                        word = getEncodedName(word);

                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append("NOCASE(\""+word+"\")");
                        
                       	}
            }
        }
            
        return sb.toString();
    }
    
       
    private String FixTitle(String Phrase){
    	  StringBuffer sb = new StringBuffer("");
          Phrase = Phrase.replaceAll(",", "");
          String[] sArr = Phrase.split(" ");

          for (int i = 0; i < sArr.length; i++) {
          	if (sArr[i].toLowerCase().equals("not")){
          		return getEncodedName(sb.toString());
          	}
          	if (sArr[i].toLowerCase().equals("except")){
          		return getEncodedName(sb.toString());
          	}
          	
          	if (sArr[i].toLowerCase().equals("without")){
          		return getEncodedName(sb.toString());
          	}
          	if (sArr[i].toLowerCase().equals("excluding")){
          		return getEncodedName(sb.toString());
          	}
          		
             sb.append(sArr[i]+" ");
             
          }
              
          return getEncodedName(sb.toString());
    	
    	
    	
    }
    
    
    private String returnWordsAmpersand(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");
        
        boolean FirstFlag = true;
        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
        	if (sArr[i].toLowerCase().equals("not")){
        		return getEncodedName(sb.toString());
        	}
        	if (sArr[i].toLowerCase().equals("except")){
        		return getEncodedName(sb.toString());
        	}
        	if (sArr[i].toLowerCase().equals("excluding")){
        		return getEncodedName(sb.toString());
        	}
        		
            if (!filtercommonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )==-1){
                		if (!FirstFlag) sb.append(" &amp; ");
                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
                        if (FirstFlag)FirstFlag = false;
                       	}
            }
        }
            
        return getEncodedName(sb.toString());
    }
    
    

    private String returnWordsAnd(String Phrase) {
        StringBuffer sb = new StringBuffer("");

        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (i > 0) sb.append(" | ");
            if (!commonWords.containsKey(sArr[i].toLowerCase())) sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
        }

        return sb.toString();
    }
    
    public static String getEncodedName(String s) {
        s = s.replaceAll("&amp;", "&");
        s = s.replaceAll("&", "&amp;");
        //s = s.replaceAll("<=", "&le;");
        //s = s.replaceAll(">=", "&ge;");
        s = s.replaceAll("<", "&lt;");
        // s = s.replaceAll("\"", "&quot;");
        // inxight doesnt want to have these encoded
        s = s.replaceAll(">", "&gt;");

        if (s.indexOf(" ") == -1) {
            s = s.replaceAll("\"", "&quot;");
            s = s.replaceAll("'", "&apos;");
        }

        return s;
    }
    
    
}