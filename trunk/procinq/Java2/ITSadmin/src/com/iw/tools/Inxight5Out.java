package com.iw.tools;

import com.iw.system.*;
import com.iw.system.Corpus;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;
import java.util.regex.*;

public class Inxight5Out extends ExportElement {
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable filtercommonWords = new Hashtable();
    public String corpusID = null;
    public String wordName = "terms";
    public String filtertype = "OR";
    public boolean SICPROC = false;


    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, com.iw.system.Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));
        corpusID = c.getID();
        if (c.getName().equals("American Industries (Full)")){
        	filtertype= "and";
        	SICPROC = true;
        }


        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file commonwords.txt");
        }

        DataInputStream disfilter = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\filtercommonwords.txt"))));


        String filterrecord = null;
        try {
            while ( (filterrecord=disfilter.readLine()) != null ) { filtercommonWords.put(filterrecord.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file filterwords.txt");
        }



        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { System.err.println("No root node found for corpus "+corpusID); return null; }

        File f = new File(c.getName().replaceAll(" ", "").replaceAll("-","")+"IXT5.xml");
        FileOutputStream fos = new FileOutputStream(f);
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<!-- "+c.getName()+" Taxonomy, Copyright 2005 by Intellisophic, Inc. Published by Intellisophic, Inc."+
                  " Corpus "+c.getID()+".  All rights reserved. -->\n");
        out.write("<!-- Source content used by permission of source publisher. Reference Intellisophic Copyright Notice. -->\n");
        out.write("<inxight-taxonomy xmlns=\"http://www.inxight.com/taxonomy/4.3\" name=\"" + c.getName().trim() + "\" author = \"Intellisophic\" language = \"English\">\n");
        //out.write("        <st:display-properties layout=\"radial\" clockwise=\"true\" stretchfactor=\"1.0\" style=\"-1\" ");
        //out.write("backgroundcolor=\"0xffffff\" selectioncolor=\"0x2cd300\" highlightcolor=\"0x2cd300\" ");
        //out.write("textsizemode=\"fittonodearea\" maxchars=\"30\" nchars=\"13\" nodearrangement=\"top\" nodelabelalignment=\"center\">\n");
        //out.write("          <st:textfont name=\"dialog\" size=\"12\" bold=\"false\" italic=\"false\"></st:textfont>\n");
        //out.write("     </st:display-properties>\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        out.write("        <node>\n");
        out.write("           <label>"+getEncodedName(rootNode.get("NODETITLE"))+"</label>\n");
        out.write("           <description>"+getEncodedName(rootNode.get("NODEDESC"))+"</description>\n");
        out.write("           <hierarchy enforce-parent=\"false\" aggregate=\"false\"></hierarchy>\n");
        out.write("           <attributes>\n");
        out.write("              <threshold>0.0</threshold>\n");
        out.write("           </attributes>\n");
        //out.write("           <st:node-display-properties nodecolor=\"0xff0033\" textcolor=\"0xffffff\"></st:node-display-properties>\n");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, out, "root","RootParent");

        out.write("        </node>\n");
        out.write("</inxight-taxonomy>\n");

        out.close();

        return f;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer out, String SICCODE, String SICParent) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() < 1) return;


        for (int i = 0; i < vNodes.size(); i++) {
            ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);

            // going to try this without the export files
            //String fileName = "";
            //if (vSignatures.size() > 0) fileName = writeSample(n,p, vSignatures);

            //if (p.get("PARENTID").equalsIgnoreCase("-1")){
            if (n.get("NODETITLE").startsWith("SIC ")){

            	// System.out.println("**DEBUG DEPTH**" + p.get("PARENTID"));
            	// System.out.println("**DEBUG NODETITLE**" + n.get("NODETITLE"));
            	String LEV2NODE = n.get("NODETITLE");
            	//SICParent = n.get("NODETITLE");
              	SICParent = n.get("NODETITLE");

            	String[] sSIC = LEV2NODE.split(" ");

            	SICCODE = sSIC[1];
            	// this is taking the second word in the title.
            	System.out.println("**DEBUG SIC**" + SICCODE);

            	// String LEV2NODE = n.get("NODETITLE");
            	// String[] sSIC = LEV2NODE.split(" ");
            	//SICCODE = "";
            	// System.out.println("**DEBUG SIC**" + SICCODE);

            }


            String spaces = "           ";
            for (int j = 0; j < Integer.parseInt(n.get("DEPTHFROMROOT")); j++) { spaces = spaces + " "; }

            out.write(spaces+"<link>\n");
            //out.write(spaces+"   <st:link-display-properties linkcolor=\"0x808080\"></st:link-display-properties>\n");
            out.write(spaces+"   <node>\n");

            //out.write(spaces+"      <label>"+n.get("NODETITLE")+"</label>\n");
            if (SICPROC==true) {
            	out.write(spaces+"      <label>"+SICCODE.trim()+ "|INDUSTRY|" + getEncodedName(n.get("NODETITLE"))+" | " + n.get("NODEID")+ "</label>\n");
            } else {
            	out.write(spaces+"      <label>"+getEncodedName(n.get("NODETITLE"))+" | " + n.get("NODEID")+ "</label>\n");
            }

            out.write(spaces+"      <description>"+getEncodedName(n.get("NODEDESC"))+"</description>\n");
            out.write(spaces+"      <hierarchy enforce-parent=\"false\" aggregate=\"false\"></hierarchy>\n");
            out.write(spaces+"      <attributes>\n");

            System.out.println("**DEBUG Number of Sigs:**" + vSignatures.size());
            System.out.println("**DEBUG NodeTitle**" + n.get("NODETITLE"));

            // Get Must Have Terms for this Node
            Vector vHave = new Vector();
            vHave = server.getMusthaves(n.get("NODEID"));



            out.write(spaces+"         <threshold>0.5</threshold>\n");
            if ((vSignatures.size() > 0))
            {
            	//out.write(spaces + "      <rule>MIN(PROB(prior=.335,");
            		if (SICPROC==true & !SICParent.equalsIgnoreCase("RootParent"))
            		{
            			if (UnCommonRequiredTerms(FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE"))+ " " + FixTitle(SICParent).trim()))
            			{
            				// retuns true if I have something -- so print the stuff out
            				out.write(spaces + "         <rule>MIN(PROB(prior=.335,");
            				out.write(InxightToString5(n,vSignatures));
            				out.write(", " + returnWordsAND(FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE"))+ " " + FixTitle(SICParent).trim()) + ")");
            			}
            			else
            			{
            				out.write(spaces + "         <rule>PROB(prior=.335,");
            				out.write(InxightToString5(n,vSignatures));
            				//out.write(")");
            			}

            		}
            		else if (SICPROC ==true & SICParent.equalsIgnoreCase("RootParent"))
            		{
            			if(UnCommonRequiredTerms(FixTitle(n.get("NODETITLE")))){
            				out.write(spaces + "         <rule>MIN(PROB(prior=.335,");
            				out.write(InxightToString5(n,vSignatures));
                    		out.write(", " + returnWordsAND(FixTitle(n.get("NODETITLE")))+ ")");
            			} else {
            				out.write(spaces + "         <rule>PROB(prior=.335,");
            				out.write(InxightToString5(n,vSignatures));
            				//out.write(")");
            			}
            		}
            		else if (SICPROC == false & filtertype.equalsIgnoreCase("OR"))
            		{
            			if (vHave.size()>0) 
            			{	//I have must haves, lets use them
            				//if (UnCommonRequiredTerms(FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE")))){
            				out.write(spaces + "         <rule>MIN(PROB(prior=.335,");
            				//out.write(InxightToString5(n,vSignatures,FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE"))));
            				out.write(InxightToStringMustHave(n,vSignatures,vHave));
                    		out.write(", " + GetMustHaveOR(vHave)+ ")");
            			}
            			else 
            			{
            				out.write(spaces + "         <rule>PROB(prior=.335,");
            				out.write(InxightToString5(n,vSignatures));
            				// out.write(")");
            			}
            		}
            		else if (SICPROC == false & filtertype.equalsIgnoreCase("AND"))
            		{
            			if (vHave.size()>0) 
            			{
            				// I have must haves -- so lets use them
            				//if (UnCommonRequiredTerms(FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE")))){
            				out.write(spaces + "         <rule>MIN(PROB(prior=.335,");
            				//out.write(InxightToString5(n,vSignatures,FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE"))));
            				out.write(InxightToStringMustHave(n,vSignatures,vHave));
                    		out.write(", " + GetMustHaveAND(vHave) + ")");
            			}
            			else 
            			{
            				out.write(spaces + "         <rule>PROB(prior=.335,");
            				//out.write(InxightToString5(n,vSignatures,FixTitle(n.get("NODETITLE")) + " " + FixTitle(p.get("NODETITLE"))));
            				out.write(InxightToString5(n,vSignatures));
            				// out.write(")");
            			}
            		}

            		out.write("\n");
            		out.write(spaces + "         </rule>\n");
            }
            else
            {
        		//if no sigs, just write out the must haves, if they exist, into the rule
            	//almost always have the topic itseld, except with alpha hierarchy nodes (A, B, C, etc.)
           		if (vHave.size() > 0)
           		{
	            	if (filtertype.equalsIgnoreCase("AND"))
	           		{
	    				out.write(spaces + "         <rule>" + GetMustHaveAND(vHave) + "</rule>\n");               			
	           		}
	           		else if (filtertype.equalsIgnoreCase("OR"))
	           		{
	    				out.write(spaces + "         <rule>" + GetMustHaveOR(vHave) + "</rule>\n");               			
	           		}
           		}
            }

            String color = "0x66ff66";
            switch (Integer.parseInt(n.get("DEPTHFROMROOT"))) {
                case 1: color = "0x66ff66"; break;
                case 2: color = "0xFF8080"; break;
                case 3: color = "0x00C0C0"; break;
                case 4: color = "0x4040FF"; break;
                case 5: color = "0xC0C0FF"; break;
                case 6: color = "0xFFFF40"; break;
                case 7: color = "0xC0FFC0"; break;
                case 8: color = "0xFF00FF"; break;
            }

            out.write(spaces+"      </attributes>\n");
            //out.write(spaces+"      <st:node-display-properties nodecolor=\""+color+"\" textcolor=\"0x0000ff\"></st:node-display-properties>\n");

            if (n.get("NODETITLE").startsWith("SIC")){
            	SICParent = n.get("NODETITLE");
            }

            if (n.get("NODEID").equals(n.get("LINKNODEID"))) buildChildren(server, n, out, SICCODE,SICParent);

            out.write(spaces+"   </node>\n");
            out.write(spaces+"</link>\n");

            nodesProcessed++;
            if (ppb != null) {
                long perc = (nodesProcessed*100) / totalNodes;
                ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
            }
        }
    }

    private String writeSample(ITSTreeNode n, ITSTreeNode p, Vector signatures) throws Exception {
        if (signatures.size() == 0) throw new Exception("This topic has no signatures.");

        try {
            File f = createOrGetFile("sample");
            f = createOrGetFile("sample/"+corpusID);
            f = createOrGetFile("sample/"+corpusID+"/"+n.get("NODEID"));

            f = new File("sample/"+corpusID+"/"+n.get("NODEID")+"/sample.htm");
            if (f.exists()) f.delete();

            // write header
            FileOutputStream fos = new FileOutputStream(f);
            Writer out = new OutputStreamWriter(fos, "UTF8");

            out.write("<HTML>\r\n<HEAD>\r\n<TITLE>"+n.get("NODETITLE")+"</TITLE>\r\n</HEAD>\r\n<BODY>\r\n");

            // calculate the filler frequency
            double totalwordfreq = 0;
            double maxweight = 0;
            for (int i = 0; i < signatures.size(); i++) {
                Signature s = (Signature) signatures.elementAt(i);
                totalwordfreq = totalwordfreq + s.getWeight();
                if (maxweight < s.getWeight()) maxweight=s.getWeight();

            }

            double nodesize = Double.parseDouble(n.get("NODESIZE"));
            double fillweight = nodesize - totalwordfreq;
            if (fillweight < 0) fillweight = 0;

            // make sure we have node title and parent title in here.

            signatures.add(0, new Signature("filler", fillweight));
            signatures.add(0, new Signature(n.get("NODETITLE"), maxweight));
            if (n.get("PARENTID").equalsIgnoreCase("-1")){

            }
            else {signatures.add(0, new Signature(p.get("NODETITLE"), maxweight));
            }


            double outer = 0; double total = 0;

            //System.out.println("**DEBUG** node: "+n.get("NODETITLE")+" file: "+f.getAbsolutePath()+" nodesize: "+nodesize+" fillweight: "+
            //        fillweight+" totalwordfreq: "+totalwordfreq+" signatures: "+signatures.size());

            // loop through the signatures and write them
            while (total < nodesize) {
                outer++;
                for (int i = 0; i < signatures.size(); i++) {
                    Signature s = (Signature) signatures.elementAt(i);
                    //System.out.println("**DEBUG** signature: "+s.getWord()+" weight: "+s.getWeight()+" outer: "+outer);
                    if (s.getWeight() >= outer) { out.write(s.getWord()+" "); total++; }
                }

                out.write("\r\n");

                if (total < nodesize) {
                    outer++;
                    for (int i = signatures.size()-1; i > -1; i--) {
                        Signature s = (Signature) signatures.elementAt(i);
                        if (s.getWeight() >= outer) { out.write(s.getWord()+" "); total++; }
                    }
                    out.write("\r\n");
                }
            }

            // write footer
            out.write("</BODY>\r\n</HTML>\r\n");
            out.close();

            return "sample/"+corpusID+"/"+n.get("NODEID")+"/sample.htm";
        } catch (Exception e) { e.printStackTrace(System.err); throw e; }
    }

    private File createOrGetFile(String path) throws Exception {
        File f = new File(path);
        if (!f.exists()) f.mkdir();

        return f;
    }


    private String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method










    private String returnWordsOr(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");

        boolean FirstFlag = true;
        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!filtercommonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.indexOf(InxightEncode(replaceWildcards(sArr[i])))==-1){
                		if (!FirstFlag) sb.append(" | ");
                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append(InxightEncode(replaceWildcards(sArr[i])));
                        if (FirstFlag)FirstFlag = false;
                       	}
            }
        }

        return getEncodedName(sb.toString());
    }
    private String returnWordsAND(String Phrase) {
        StringBuffer sb = new StringBuffer("");
        Phrase = Phrase.replaceAll(",", "");

        boolean FirstFlag = true;
        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (!filtercommonWords.containsKey(sArr[i].toLowerCase())) {
            	    // System.out.println(sb.indexOf("NOCASE(\""+replaceWildcards(sArr[i])+"\")" )!=-1);
                	if (sb.indexOf(InxightEncode(replaceWildcards(sArr[i])))==-1){
                		if (!FirstFlag) sb.append(" , ");
                        //  mh change to not allow duplicate tersm in the filter if they are in both the parent and the node title
                        sb.append(InxightEncode(replaceWildcards(sArr[i])));
                        if (FirstFlag)FirstFlag = false;
                       	}
            }
        }

        return getEncodedName(sb.toString());
    }





    private String InxightToString5 (ITSTreeNode n, Vector vSignatures)
    {
    	double totFrequency;
    	double maxFrequency;

    	Hashtable sighash = new Hashtable();
        StringBuffer sb = new StringBuffer();

        totFrequency = 0;
        maxFrequency = 0;
        for (int i = 0; i < vSignatures.size(); i++) {
        	Signature s = (Signature) vSignatures.elementAt(i);
        		totFrequency = totFrequency + s.getWeight();
        		if (maxFrequency < s.getWeight())maxFrequency = s.getWeight();
        		sighash.put(s.getWord(),"1");
        }


        for (int i = 0; i < vSignatures.size(); i++) {
            if (i > 0) sb.append(",");
            Signature s = (Signature) vSignatures.elementAt(i);

            double sigFrequency = s.getWeight();
            double inxightWeight = sigFrequency/totFrequency;
            java.text.DecimalFormat df = new java.text.DecimalFormat("#.#####");
            if (s.getWord().trim().compareTo("")!=0){

            	if ((s.getWord().trim().indexOf(" ")> -1)|(s.getWord().trim().indexOf("!")> -1) | (s.getWord().trim().indexOf("-")> -1))
            	{
            		//if we have a space or exclamation point or hyphen in it -- so we should double quote it
            		//if (i!=0) sb.append(",");
            		sb.append("\""+InxightEncode(s.getWord())+"\",");
            		sb.append(df.format(inxightWeight));
            	}
            	else
            	{
            		//we just have a single word -- so we don't need double quotes
            		sb.append(InxightEncode(s.getWord())+",");
            		sb.append(df.format(inxightWeight));
            	}
            }
        }

        if (vSignatures.size()>0) sb.append(")");

        return getEncodedName(sb.toString());

}

    private String InxightToStringMustHave (ITSTreeNode n, Vector vSignatures, Vector Phrase)
    {
    	double totFrequency;
    	double maxFrequency;

    	Hashtable sighash = new Hashtable();
        StringBuffer sb = new StringBuffer();

        totFrequency = 0;
        maxFrequency = 0;
        for (int i = 0; i < vSignatures.size(); i++) {
        	Signature s = (Signature) vSignatures.elementAt(i);
        		totFrequency = totFrequency + s.getWeight();
        		if (maxFrequency < s.getWeight())maxFrequency = s.getWeight();
        		sighash.put(s.getWord(),"1");
        }


        for (int i = 0; i < vSignatures.size(); i++) {
            if (i > 0) sb.append(",");
            Signature s = (Signature) vSignatures.elementAt(i);

            double sigFrequency = s.getWeight();
            double inxightWeight = sigFrequency/totFrequency;
            java.text.DecimalFormat df = new java.text.DecimalFormat("#.#####");
            if (s.getWord().trim().compareTo("")!=0){

            	if ((s.getWord().trim().indexOf(" ")> -1)|(s.getWord().trim().indexOf("!")> -1) | (s.getWord().trim().indexOf("-")> -1))
            	{
            		//if we have a space or exclamation point or hyphen in it -- so we should double quote it
            		//if (i!=0) sb.append(",");
            		sb.append("\""+InxightEncode(s.getWord())+"\",");
            		sb.append(df.format(inxightWeight));
            	}
            	else
            	{
            		//we just have a single word -- so we don't need double quotes
            		sb.append(InxightEncode(s.getWord())+",");
            		sb.append(df.format(inxightWeight));
            	}
            }
        }

        //Phrase = Phrase.replaceAll("[(]"," ");
        //Phrase = Phrase.replaceAll("[)]"," ");
        //String[] sArr = Phrase.split(" ");
        for (int j = 0; j < Phrase.size();j++){
          if (Phrase.elementAt(j).toString().trim().compareTo("")!=0){
        	  System.out.println("**DEBUG TERM **" + Phrase.elementAt(j).toString().trim());
        	//if (!filtercommonWords.containsKey(sArr[j].toLowerCase())){
        		if (!sighash.containsKey(Phrase.elementAt(j).toString().trim().toLowerCase())){
        			// make sure its not duped in the title terms

                if (InxightEncode(Phrase.elementAt(j).toString().trim().toLowerCase()).indexOf(" ") > -1)
                    sb.append(" ,\"" + InxightEncode(Phrase.elementAt(j).toString().trim().toLowerCase()) + "\" ,");
                else
                    sb.append(" ," + InxightEncode(Phrase.elementAt(j).toString().trim().toLowerCase()) + " ,");

                java.text.DecimalFormat df = new java.text.DecimalFormat("#.#####");
        		sb.append(df.format(maxFrequency/(totFrequency*Phrase.size())));
        		sighash.put(Phrase.elementAt(j).toString().trim().toLowerCase(),"1");
        		}
        		//}
        	}
        }

        if (vSignatures.size()>0) sb.append(")");

        return getEncodedName(sb.toString());

}



        private String getEncodedName(String s) {
        	s = s.replaceAll("&lt","<");
        	s = s.replaceAll("&gt",">");
            s = s.replaceAll("&amp;", "&");
            s = s.replaceAll("&apos;", "'");
            s = s.replaceAll("&", "&amp;");
            s = s.replaceAll("<", "&lt;");
            s = s.replaceAll(">", "&gt;");

            if (s.indexOf(" ") == -1) {
                s = s.replaceAll("\"", "&quot;");
                s = s.replaceAll("'", "&apos;");
            }

            return s;
        }

        private String InxightEncode (String s){
        	s=s.toLowerCase();
        	s=s.replaceAll("[|]", "");
        	//s=s.replaceAll("[[]", "");
        	//s=s.replaceAll("[]]", "");
        	s=s.replaceAll("[�]", "");
        	s=s.replaceAll("[�]", "");
        	s=s.replaceAll("[*]", "");
        	s=s.replaceAll("[#]", "");
        	s=s.replaceAll("[=]", "");
        	s=s.replaceAll("[?]", "");
        	s=s.replaceAll("[:]", "");
        	s=s.replaceAll("[(]", "");
        	s=s.replaceAll("[)]","");
        	s=s.replaceAll("\"","");

        	if (s.equals("window")){
        		s="'window'";
        	}
        	if (s.equals("count")){
        		s="'count'";
        	}
        	if (s.equals("sentence")){
        		s="'sentence'";
        	}
        	if (s.equals("paragraph")){
        		s="'paragraph'";
        	}
        	if (s.equals("stem")){
        		s="'stem'";
        	}
        	if (s.equals("pos")){
        		s="'pos'";
        	}
        	if (s.equals("min")){
        		s="'min'";
        	}
        	if (s.equals("max")){
        		s="'max'";
        	}
        	if (s.equals("prob")){
        		s="'prob'";
        	}
        	if (s.equals("case")){
        		s="'case'";
        	}
        	if (s.equals("word")){
        		s="'word'";
        	}
        	if (s.equals("sent")){
        		s="'sent'";
        	}
        	if (s.equals("prior")){
        		s="'prior'";
        	}
        	if (s.equals("para")){
        		s="'para'";
        	}



        	return s;
        }
        private String FixTitle(String Phrase){
      	  StringBuffer sb = new StringBuffer("");
            Phrase = Phrase.replaceAll(",", "");
            String[] sArr = Phrase.split(" ");

            for (int i = 0; i < sArr.length; i++) {
            	if (sArr[i].toLowerCase().equals("not")){
            		return getEncodedName(sb.toString());
            	}
            	if (sArr[i].toLowerCase().equals("except")){
            		return getEncodedName(sb.toString());
            	}

            	if (sArr[i].toLowerCase().equals("without")){
            		return getEncodedName(sb.toString());
            	}
            	if (sArr[i].toLowerCase().equals("excluding")){
            		return getEncodedName(sb.toString());
            	}

               sb.append(sArr[i]+" ");

            }

            return getEncodedName(sb.toString());



      }
      private boolean UnCommonRequiredTerms(String Phrase){
          Phrase = Phrase.replaceAll(",", "");

          boolean FirstFlag = false;
          String[] sArr = Phrase.split(" ");

          for (int i = 0; i < sArr.length; i++) {

        	  if (sArr[i].trim().compareTo("")!=0) {
        		  if (!filtercommonWords.containsKey(sArr[i].toLowerCase())) {
               	   FirstFlag = true;
                 	}
              }
          }



    	  return FirstFlag;

      }

      private String GetMustHaveAND(Vector vHave){

      	StringBuffer buffer = new StringBuffer();
          if (vHave.size() >0){
          	for (int i = 0; i < vHave.size(); i++) {
          		if ((vHave.elementAt(i).toString().trim().indexOf(" ")> -1)|(vHave.elementAt(i).toString().trim().indexOf("!")> -1) | (vHave.elementAt(i).toString().trim().indexOf("-")> -1)){
        			buffer.append("\"" + InxightEncode(getEncodedName(replaceWildcards(vHave.elementAt(i).toString().trim()))) + "\"");
        	  	    }
        		else {
        			buffer.append(InxightEncode(getEncodedName(replaceWildcards(vHave.elementAt(i).toString().trim()))));
            	     }
          		if (i == 0 & vHave.size()>1){
          			buffer.append(" , ");
          		}
          		if (i !=0 & i !=vHave.size() -1 ){
          			buffer.append(" , ");
          		}


              }
          }


      return buffer.toString();
      }

      private String GetMustHaveOR(Vector vHave){

        	StringBuffer buffer = new StringBuffer();
            if (vHave.size() >0){
            	for (int i = 0; i < vHave.size(); i++) {
            		if ((vHave.elementAt(i).toString().trim().indexOf(" ")> -1)|(vHave.elementAt(i).toString().trim().indexOf("!")> -1) | (vHave.elementAt(i).toString().trim().indexOf("-")> -1)){
            			buffer.append("\"" + InxightEncode(getEncodedName(replaceWildcards(vHave.elementAt(i).toString().trim()))) + "\"");
            	  	    }
            		else {
            			buffer.append(InxightEncode(getEncodedName(replaceWildcards(vHave.elementAt(i).toString().trim()))));
                	     }
            		if (i == 0 & vHave.size()>1){
            			buffer.append(" | ");
            		}
            		if (i !=0 & i !=vHave.size() -1 ){
            			buffer.append(" | ");
            		}


                }
            }


        return buffer.toString();
        }


}
