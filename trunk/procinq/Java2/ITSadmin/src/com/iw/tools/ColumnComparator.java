package com.iw.tools;

import java.util.*;

public class ColumnComparator
        implements Comparator {
    protected int index;
    protected boolean ascending;

    public ColumnComparator(int index, boolean ascending) {
        this.index = index;
        this.ascending = ascending;
    }

    public int compare(Object one, Object two) {
        Object oOne = null; Object oTwo = null;

        if (one instanceof Object[] &&
            two instanceof Object[]) {

            oOne = ((Object[]) one)[index];
            oTwo = ((Object[]) two)[index];
        }

        if (one instanceof Vector &&
                two instanceof Vector) {
            Vector vOne = (Vector) one;
            Vector vTwo = (Vector) two;
            oOne = vOne.elementAt(index);
            oTwo = vTwo.elementAt(index);
        }

        if (oOne != null && oTwo != null) {
            try {
                float iOne = new Float(oOne.toString()).floatValue();
                float iTwo = new Float(oTwo.toString()).floatValue();

                if (ascending) {
                    if (iOne >= iTwo) { return 1; } else { return 0; }
                } else { if (iOne >= iTwo) { return 0; } else { return 1; } }
            } catch (Exception e) {
                //e.printStackTrace(System.out);
            }

            try {
                if (oOne instanceof Comparable &&
                    oTwo instanceof Comparable) {
                    Comparable cOne = (Comparable) oOne;
                    Comparable cTwo = (Comparable) oTwo;
                    if (ascending) {
                        return cOne.toString().toLowerCase().compareTo(cTwo.toString().toLowerCase());
                    } else {
                        return cTwo.toString().toLowerCase().compareTo(cOne.toString().toLowerCase());
                    }
                }
            } catch (Exception e) {
                System.out.println("Error while sorting columns");
                //e.printStackTrace(System.out);
            }
        }
        return 1;
    }
}

