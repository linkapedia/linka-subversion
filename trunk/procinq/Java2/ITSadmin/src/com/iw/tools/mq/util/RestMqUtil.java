/**
 * this class is to connect restful and get result
 */
package com.iw.tools.mq.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.iw.system.ITS;
import com.iw.tools.ConfClient;
import com.iw.tools.mq.entity.PackageInfo;

public class RestMqUtil{
	private static RestMqUtil restmqutil = null;
	private URL url;
	private String PATH_RESTFUL = "http://" + ITS.getUser().getServer()
			+ "/itsapi/rest/classifiers";
	private ArrayList<PackageInfo> arrayInfoClassifiers;

	/**
	 * singleton pattern
	 * 
	 * @return
	 */
	public static RestMqUtil getRestMqUtil() {
		if (restmqutil == null) {
			restmqutil = new RestMqUtil();
		}
		return restmqutil;
	}

	/**
	 * consult classifiers in the server and setter arrayInfoClassifiers
	 */
	public void setInfoToClassifier() {
		this.arrayInfoClassifiers = new ArrayList<PackageInfo>();
		try {
			this.url = new URL(this.PATH_RESTFUL);
			URLConnection con = this.url.openConnection();

			StringBuilder sb = new StringBuilder();

			BufferedReader read = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String line;
			while ((line = read.readLine()) != null) {
				sb.append(line);
			}
			try {
				Reader reader = new StringReader(sb.toString());
				SAXBuilder builer = new SAXBuilder();

				Document doc = builer.build(reader);

				List children = doc.getRootElement().getChildren();
				for (Iterator i = children.iterator(); i.hasNext();) {
					Element classifier = (Element) i.next();
					PackageInfo pi = new PackageInfo();
					pi.setName(classifier.getChildText("id"));
					pi.setIp(classifier.getChildText("ipAddress"));
					pi.setState(classifier.getChildText("isRunning"));
					pi.setUptime(classifier.getChildText("uptime"));
					pi.setWorksProcessed(classifier
							.getChildText("workprocessed"));
					pi.setWorksFailed(classifier.getChildText("workprocessed"));
					this.arrayInfoClassifiers.add(pi);
				}
			} catch (JDOMException e) {
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param listClassifiers
	 * @return list of classifiers ok
	 */
	public int changeStateClassifiers(HashMap<String, String> mapClassifiers) {
		ArrayList<String> classifiersToOn = new ArrayList<String>();
		ArrayList<String> classifiersToOff = new ArrayList<String>();
		int codResult = 0;
		URL url;
		
		// get classifiers to start or stop
		Iterator<Entry<String, String>> itr = mapClassifiers.entrySet()
				.iterator();
		while (itr.hasNext()) {
			Map.Entry e = (Map.Entry) itr.next();
			if (e.getValue().equals("Running")) {
				classifiersToOff.add(e.getKey().toString());
			} else {
				classifiersToOn.add(e.getKey().toString());
			}
		}
		
		//build xml
		StringBuffer sb = new StringBuffer();
		sb.append("<classifiers>");
		if((classifiersToOn!=null) && (!classifiersToOn.isEmpty())){
			sb.append("<task>");
			sb.append("<action>start</action>");
			for(String cON : classifiersToOn){
				sb.append("<ipAddress>");
				sb.append(cON);
				sb.append("</ipAddress>");
			}
			sb.append("</task>");
		}
		sb.append("<task>");
		if((classifiersToOff!=null) && (!classifiersToOff.isEmpty())){
			sb.append("<action>stop</action>");
			for(String cOFF : classifiersToOff){
				sb.append("<ipAddress>");
				sb.append(cOFF);
				sb.append("</ipAddress>");
			}
			sb.append("</task>");
		}
		sb.append("</classifiers>");
		// Send data
		try {
			url = new URL(PATH_RESTFUL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			codResult=conn.getResponseCode();
			conn.setRequestProperty("Content-Type", "application/xml");
			conn.setRequestProperty("Accept", "application/xml");
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			wr.write(sb.toString());
			wr.flush();
			wr.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.err.println("Class: RestMqUtil, Method: changeStateClassifiers "+e.getMessage());
			return 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Class: RestMqUtil, Method: changeStateClassifiers "+e.getMessage());
			return 0;
		}catch(Exception e){
			System.err.println("Class: RestMqUtil, Method: changeStateClassifiers "+e.getMessage());
			return 0;
		}
		return codResult;

	}

	public String getName(String ip) {
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			if (pi.getIp().equals(ip)) {
				return pi.getName();
			}
		}
		return "null";
	}

	public String getWorksProcessed(String ip) {
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			if (pi.getIp().equals(ip)) {
				return pi.getWorksProcessed();
			}
		}
		return "null";
	}

	public String getWorksFailed(String ip) {
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			if (pi.getIp().equals(ip)) {
				return pi.getWorksFailed();
			}
		}
		return "null";
	}

	public String getUptime(String ip) {
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			if (pi.getIp().equals(ip)) {
				return pi.getUptime();
			}
		}
		return "null";
	}

	public String getState(String ip) {
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			if (pi.getIp().equals(ip)) {
				return pi.getState();
			}
		}
		return "null";
	}

	public ArrayList<String> getClassifiers() {
		ArrayList aclassifiers = new ArrayList();
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			aclassifiers.add(pi.getIp());
		}
		return aclassifiers;
	}

	public int getClassifiersON() {
		int cont = 0;
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			if (pi.getState().equals("true")) {
				cont++;
			}
		}
		return cont;
	}

	public int getClassifiersOFF() {
		int cont = 0;
		for (PackageInfo pi : this.arrayInfoClassifiers) {
			if (pi.getState().equals("false")) {
				cont++;
			}
		}
		return cont;
	}
}