package com.iw.tools.mq.entity;

public class PackageInfo {
	private String name;
	private String ip;
	private String state;
	private String worksProcessed;
	private String worksFailed;
	private String uptime;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getWorksProcessed() {
		return this.worksProcessed;
	}

	public void setWorksProcessed(String worksProcessed) {
		this.worksProcessed = worksProcessed;
	}

	public String getWorksFailed() {
		return this.worksFailed;
	}

	public void setWorksFailed(String worksFailed) {
		this.worksFailed = worksFailed;
	}

	public String getUptime() {
		return this.uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}
}