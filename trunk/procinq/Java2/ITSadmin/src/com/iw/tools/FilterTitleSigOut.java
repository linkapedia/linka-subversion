package com.iw.tools;

import com.iw.system.*;
import com.iw.ui.PopupProgressBar;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;

public class FilterTitleSigOut extends ExportElement {
    public String corpusID = "";
    public ITSTreeNode rootNode = null;
    public Hashtable commonWords = new Hashtable();
    public Hashtable nodes = new Hashtable();
    public Hashtable usedNames = new Hashtable();

    private int totalNodes = 1;
    private int nodesProcessed = 1;
    private PopupProgressBar ppb = null;

    private DecimalFormat twoDigits = new DecimalFormat("0");

    public File exportTaxonomy(ITS server, Corpus c) throws Exception {
        return exportTaxonomy(server, c, null); }
    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppbin) throws Exception {

        corpusID = c.getID();
        if (ppbin != null) this.ppb = ppbin;

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\filtercommon.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("Warning: There was an error while reading the file filtercommon.txt");
        }

        // get the total number of nodes in this corpus
        if (ppbin != null) { totalNodes = server.getNodeCount(c); this.ppb = ppbin; }

        /*
          Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { throw new Exception("No root node found for corpus "+corpusID); }

        // open two files: the .otl file for the topic definitions and a .tax file to create the hierarchy
        File fotl = new File(c.getName().replaceAll(" ", "_")+".fts");
        if (fotl.exists()) { fotl.delete(); }

        FileOutputStream foso = new FileOutputStream(fotl);
        Writer outo = new OutputStreamWriter(foso, "UTF8");

        //outo.write("Start of File \r\n");
        

        // *** WRITE ROOT NODE INFORMATION HERE
        writeNode(rootNode, server, outo);

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, outo);

        // outo.write("</UltraseekTopics>\r\n"); -- end of file

        outo.close();

        return fotl;
    }

    private void writeNode(ITSTreeNode n, ITS server, Writer outo) throws Exception {
        // Vector vSignatures = server.getNodeSignatures(n);

        //n.set("NODETITLE", ((String) n.get("NODETITLE")).replaceAll(" & ", " &amp; "));

        if (((String) n.get("PARENTID")).equals("-1")) n.set("PARENTID", "0");

        String[] sArr = n.get("NODETITLE").split(" ");
      
        for (int i = 0; i < sArr.length; i++) {
        	sArr[i] = sArr[i].replaceAll("[.]","");
        	sArr[i] = sArr[i].replaceAll("[:]","");
        	sArr[i] = sArr[i].replaceAll("[?]","");
        	sArr[i] = sArr[i].replaceAll("[\"]","");
        	
            if (!commonWords.containsKey(sArr[i].toLowerCase())) {
            	if (!nodes.containsKey(sArr[i].toLowerCase())){
            		nodes.put(sArr[i].toLowerCase(),"1");
            	    outo.write(sArr[i].toLowerCase()+"\r\n");
            	}
            }
        }
        
        // now -- do the same with signatures
        Vector vSignatures = server.getNodeSignatures(n);
        if (vSignatures.size() > 0){
        for (int i = 0; i < vSignatures.size(); i++) {
            Signature s = (Signature) vSignatures.elementAt(i);
            sArr = s.getWord().split(" ");
            for (int j = 0; j < sArr.length; j++) {
            	sArr[j] = sArr[j].replaceAll("[.]","");
            	sArr[j] = sArr[j].replaceAll("[:]","");
            	sArr[j] = sArr[j].replaceAll("[?]","");
            	sArr[j] = sArr[j].replaceAll("[\"]","");
            if (!commonWords.containsKey(sArr[j].toLowerCase())) {
            	if (!nodes.containsKey(sArr[j].toLowerCase())){
            		nodes.put(sArr[j].toLowerCase(),"1");
            	    outo.write(sArr[j].toLowerCase()+"\r\n");
            	}
            }
        }
        }
        }
        
        nodesProcessed++;
        if (ppb != null) {
            long perc = (nodesProcessed*100) / totalNodes;
            ppb.setProgress(Integer.parseInt(twoDigits.format(perc)));
        }
    }

    public String buildSigs(Vector v) {
        int words = 0;
        String keywords = "";

        for (int i = 0; i < v.size(); i++) {
            Signature s = (Signature) v.elementAt(i);
            if (!commonWords.containsKey(s.getWord().toLowerCase())) {
                words++; if (words > 1) keywords = keywords + ",";
                keywords = keywords + s;
            }
        }

        return keywords;
    }

    private void buildChildren(ITS server, ITSTreeNode p, Writer outo) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() > 0) {
            for (int i = 0; i < vNodes.size(); i++) {
                ITSTreeNode n = (ITSTreeNode) vNodes.elementAt(i);
                writeNode(n, server, outo);

                buildChildren(server, n, outo);
            }
        }
        //int depth = new Integer(p.get("DEPTHFROMROOT")).intValue();
        //for (int i = 0; i < depth; i++) { outo.write(" "); }
    }

  

    private static String ReplaceXMLChars (String s){
    	 s = s.replaceAll("&amp;", "&");
         s = s.replaceAll("&", "&amp;");
         s = s.replaceAll("<", "&lt;");
         s = s.replaceAll("\"", "&quot;");
         s = s.replaceAll(">", "&gt;");

         return s;
    	
    }
    
    
    
    private static String replaceNonAlpha (String s) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (Character.isLetterOrDigit(chars[i])) buffer.append(chars[i]);
            else buffer.append("-");
        }
        return buffer.toString();
    }
    

    
    
}