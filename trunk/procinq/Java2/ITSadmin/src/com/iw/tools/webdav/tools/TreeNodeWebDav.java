/*
 * class to identify if a resource is director
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 25, 2011, 2:00 AM
*/


package com.iw.tools.webdav.tools;

import javax.swing.tree.DefaultMutableTreeNode;

public class TreeNodeWebDav extends DefaultMutableTreeNode {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6716437133753551153L;
	private boolean directory=false;
	public TreeNodeWebDav(Object  object)
	{
		super(object);
	}
	public void setDirectory(boolean directory)
	{
		this.directory=directory;
	}
	public boolean isDirectory()
	{
		return this.directory;
	}
}
