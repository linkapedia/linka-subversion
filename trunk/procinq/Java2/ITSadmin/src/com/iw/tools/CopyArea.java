package com.iw.tools;

import com.iw.system.ITSTreeNode;
import com.iw.system.ITS;
import com.iw.system.NodeDocument;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.Vector;
import java.util.EventObject;
import java.io.*;

public class CopyArea extends JTable {
    private String buffer = new String();
    private ITS its;
    private CopyArea ca;
    private JFrame frame;

    public CopyArea() { super(); }
    public CopyArea(ITS its) { super(); this.its = its; }
    public CopyArea(JFrame frame, ITS its) { super(); this.its = its; setFrame(frame); }

    public boolean isCellEditable(int row, int column) { return false; }

    public void setFrame(JFrame frame) { this.frame = frame; }

    public void startTimer() {
        ca = this;

        Runnable doConstruct = new Runnable() {
            public void run() {
                try {
                    while (true) {
                        useCopyBuffer(new EventObject(ca));
                        Thread.sleep(3000);
                    }
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                } finally {
                    System.out.println("Done.");
                }
            }
        };
        Thread t = new Thread(doConstruct);
        t.run();
    }

    public void loadTopics(Vector vNodes) {
        DefaultTableModel dtm = new DefaultTableModel((vNodes.size()/2)+1, 2);

        int j = 0;
        for (int i = 0; i < vNodes.size(); i=i+2) {
            NodeDocument n = (NodeDocument) vNodes.elementAt(i);
            dtm.setValueAt(n, j, 0);

            if (vNodes.size() > (i+1)) {
                NodeDocument n2 = (NodeDocument) vNodes.elementAt(i+1);
                dtm.setValueAt(n2, j, 1);
            }
            j++;
        }
        Vector v = new Vector();
        v.add("Topic Result");
        v.add("Topic Result");
        dtm.setColumnIdentifiers(v);

        setModel(dtm);
    }

    public File wrapHTML() throws Exception {
        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";

        File tempFile = new File("file.txt");
        if (tempFile.exists()) { tempFile.delete(); }

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.println("<HTML><HEAD><TITLE>Document</TITLE></HEAD>\r\n<BODY>\r\n");
            out.print(buffer);
            out.println("</BODY>\r\n</HTML>\r\n");
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }

        return tempFile;
    }

    public Vector classifyBuffer() {
        try { return its.classify(wrapHTML(), false); }
        catch (Exception e) { e.printStackTrace(System.out); return new Vector(); }
    }

    public void useCopyBuffer(EventObject e) {
        Clipboard system = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable t = system.getContents(DataFlavor.stringFlavor);

        try {
            if (!buffer.equals((String) t.getTransferData(DataFlavor.stringFlavor))) {
                buffer = (String) t.getTransferData(DataFlavor.stringFlavor);
            } else { return; }
        } catch (Exception ex) { ex.printStackTrace(System.out); }

        SwingWorker aWorker = new SwingWorker(e) {

            protected void doNonUILogic() {
                ca.frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                ca.frame.getGlassPane().setVisible(true);

                loadTopics(classifyBuffer());
            }
            protected void doUIUpdateLogic() {
            }

            public void finished() {
                super.finished();
                ca.frame.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                ca.frame.getGlassPane().setVisible(false);
            }
        };
        aWorker.start();
    }
}
