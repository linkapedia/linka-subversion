package com.iw.tools;

import java.util.*;
import javax.swing.table.*;

public class SortAbstractTableModel
        extends AbstractTableModel
        implements SortTableModel {

    private boolean bEditable = true;

    private String[] columnNames = null;
    private Object[][] data = null;
    private int numberItems = 0;

    public void setColumnNames(Vector names) {
        columnNames = new String[names.size()];
        for (int i = 0; i < names.size(); i++) {
            columnNames[i] = (String) names.elementAt(i);
        }
    }
    public void setColumnNames(String[] s) { columnNames = s; }

    public void setValues (Object[][] o) {
        data = o;
    }

    public void setNumberItems(int i) { numberItems = i; }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public void addRow(Object[] row) {

        Object[][] o = new Object[numberItems+1][columnNames.length];
        for (int j = 0; j < row.length; j++) {
            o[0][j] = row[j];
        }

        for (int i = 0; i < numberItems; i++) {
            for (int j = 0; j < columnNames.length; j++) {
                o[i+1][j] = data[i][j];
            }
        }

        numberItems++;
        data = o;
        fireTableRowsInserted(0, 0);
    }

    public void removeRow(int row) {
        System.out.println("remove row: "+row);

        int offset = 0;
        Object[][] o = new Object[numberItems-1][columnNames.length];
        for (int i = 0; i < numberItems; i++) {
            if (i == row) offset++;
            for (int j = 0; j < columnNames.length; j++) {
                o[i][j] = data[i+offset][j];
            }
        }
        data = o; numberItems--;

        fireTableRowsDeleted(row, row);
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        try {
            return data[row][col];
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("out of bounds: row: "+row+" col: "+col);
            return null;
        }
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public void setEditable(boolean Editable) { bEditable = Editable; }
    public boolean isSortable(int col) {
        return true;
    }
    public boolean isCellEditable(int col) {
        return bEditable;
    }
    public boolean isCellEditable(int col, int row) {
        return bEditable;
    }
    public void sortColumn(int col, boolean ascending) {
        Collections.sort(Arrays.asList(data), new ColumnComparator(col, ascending));
    }

    public void setValueAt(Object value, int row, int col) {
        try {
            int num = Integer.parseInt(value.toString());
            data[row][col] = new Integer(num);
        } catch (NumberFormatException exp) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        } catch (ArrayIndexOutOfBoundsException exp) {
            exp.printStackTrace(System.out);
            return;
        }
    }
}
