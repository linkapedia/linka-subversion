/*
 * util to connect with mediawiki api and return results
 * @authors Andres F. Restrepo A.
 *
 * @version 1.0
 * Date : Dic 13, 2011, 18:53 AM
 */
package com.iw.tools.wiki.util;

import info.bliki.wiki.filter.PlainTextConverter;
import info.bliki.wiki.model.Configuration;
import info.bliki.wiki.model.WikiModel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import com.iw.tools.ConfClient;

public class WikiUtil {

    private String category;
    private HashMap<String, String> mapLoop;
    private boolean fileCreated = false;

    /**
     *
     * @param category
     */
    public WikiUtil(String category) {
        this.category = category;
        mapLoop = new HashMap<String, String>();
    }

    /**
     *
     * @param pages
     * @return the content to all pages array[pageid]
     */
    public HashMap<String, String> getContent(ArrayList<String> pages) {

        ArrayList<String> pagesToConsult = new ArrayList<String>();
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        HashMap<String, String> mapToReturn = new HashMap<String, String>();
        List<Element> itemsRev = new ArrayList<Element>();
        map.put("action", "query");
        map.put("prop", "revisions");
        map.put("rvprop", "content");
        map.put("format", "xml");
        int k = 0;
        for (int i = 0; i < pages.size(); i++) {
            pagesToConsult.add(pages.get(i));
            if ((k == 20) || (i + 1 == pages.size())) {
                try {
                    map.put("pageids",
                            URLEncoder.encode(StringUtils.join(
                            pagesToConsult.toArray(), "|"), "UTF-8"));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                try {
                    itemsRev = getItemsRevision(getURL(map));
                    // put to mapToReturn
                    for (Element e : itemsRev) {
                        mapToReturn.put(e.getAttributeValue("pageid"),
                                filerSourceWithLinks(e.getChild("revisions")
                                .getChildText("rev")));
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                k = 0;
                pagesToConsult.clear();
            }
            k++;
        }
        return mapToReturn;

    }

    /**
     *
     * @param pageid
     * @return the content to one page
     */
    public String getSingleContent(String pageid) {
        List<Element> itemsRev = new ArrayList<Element>();
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        String source = null;
        map.put("action", "query");
        map.put("prop", "revisions");
        map.put("rvprop", "content");
        map.put("format", "xml");
        try {
            map.put("pageids", URLEncoder.encode(pageid, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        try {
            itemsRev = getItemsRevision(getURL(map));
            source = filerSourceWithLinks(itemsRev.get(0).getChild("revisions")
                    .getChildText("rev"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return source;
    }

    /**
     *
     * @param source
     * @return
     */
    private String filerSourceWithLinks(String source) {
        String sourceFilter = source;

        // remove trash
        sourceFilter = removeTrash(sourceFilter);
        ArrayList<String> linksWiki = new ArrayList<String>();

        // delete spaces init and end
        sourceFilter = sourceFilter.trim();
        // remove <ref
        sourceFilter = sourceFilter.replaceAll("(<ref(.*?)</ref>)", "");
        // remove {{*}}
        sourceFilter = sourceFilter.replaceAll("(\\{\\{(.*?)}})", "");
        // remove image link
        sourceFilter = sourceFilter.replaceAll("(\\[\\[(Image:.*?)]])", "");
        // remove File link
        sourceFilter = sourceFilter.replaceAll("(\\[\\[(File:.*?)]])", "");
        // remove commentaries HTML
        sourceFilter = sourceFilter.replaceAll("<!--(.*?)-->", " ");

        linksWiki = getLinksFromSource(sourceFilter);
        StringBuilder linkedSection = new StringBuilder();
        if (!linksWiki.isEmpty()) {
            linkedSection.append("<LinkedSection>");
            linkedSection.append(StringUtils.join(linksWiki.toArray(), "|"));
            linkedSection.append("</LinkedSection>\"");
        }

        WikiModel wikiModel = new WikiModel(
                Configuration.DEFAULT_CONFIGURATION, Locale.ENGLISH,
                "${image}", "${title}");
        wikiModel.setUp();
        sourceFilter = "\""
                + wikiModel.render(new PlainTextConverter(), sourceFilter);

        sourceFilter += linkedSection;

        // remove characters
        sourceFilter = sourceFilter.replaceAll("[=*\"}\'{\\]\\[]", " ");

        // System.out.println(sourceFilter);
        if ((sourceFilter.equals("")) || (sourceFilter.equals(" "))) {
            sourceFilter = null;
        }
        return sourceFilter;
    }

    /**
     *
     * @param source
     * @return
     */
    private String removeTrash(String source) {
        int index = 0;
        String sourceFilter = source;
        sourceFilter = sourceFilter.replaceAll("(== {0,}(See also) {0,}==)",
                "==See also==");
        sourceFilter = sourceFilter.replaceAll("(== {0,}(Notes) {0,}==)",
                "==Notes==");
        sourceFilter = sourceFilter.replaceAll("(== {0,}(References) {0,}==)",
                "==References==");
        sourceFilter = sourceFilter.replaceAll(
                "(== {0,}(External links) {0,}==)", "==External links==");
        sourceFilter = sourceFilter.replaceAll("(== {0,}(Source){0,} ==)",
                "==Source==");

        if (sourceFilter.contains("==See also==")) {
            index = sourceFilter.lastIndexOf("==See also==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==Notes==")) {
            index = sourceFilter.lastIndexOf("==Notes==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==References==")) {
            index = sourceFilter.lastIndexOf("==References==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==External links==")) {
            index = sourceFilter.lastIndexOf("==External links==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        if (sourceFilter.contains("==Source==")) {
            index = sourceFilter.lastIndexOf("==Source==");
            sourceFilter = sourceFilter.substring(0, index);
        }
        return sourceFilter;
    }

    /**
     *
     * @param source
     * @return
     */
    private ArrayList<String> getLinksFromSource(String source) {
        ArrayList<String> links = new ArrayList<String>();
        Pattern pa = null;
        Matcher ma = null;
        String l = "";

        pa = Pattern.compile("(\\[\\[(.*?)]])");
        ma = pa.matcher(source);
        while (ma.find()) {
            l = ma.group(2);
            if (l.contains("|")) {
                l = l.substring(l.lastIndexOf("|") + 1, l.length());
            }
            if ((!l.isEmpty()) && (!l.contains("Category:"))) {
                links.add(l);
            }

        }
        return links;

    }

    /**
     *
     * @param map
     * @return
     * @throws MalformedURLException
     */
    private URL getURL(LinkedHashMap<String, String> map)
            throws MalformedURLException {
        Iterator<String> it = map.keySet().iterator();
        StringBuilder param = new StringBuilder();
        String sParam = "";
        param.append(ConfClient.getValue("wikipedia.crawl.api"));
        while (it.hasNext()) {
            String e = it.next();
            String value = map.get(e);
            param.append(e + "=" + value + "&");
        }
        sParam = param.toString();
        int index = sParam.lastIndexOf("&");
        if (index > -1) {
            sParam = sParam.substring(0, index);
        }
        return new URL(sParam);
    }

    /**
     *
     * @param url
     * @return elements of xml
     */
    @SuppressWarnings("unchecked")
    private List<Element> getItemsRevision(URL url) {
        HttpURLConnection con = null;
        SAXBuilder saxBuilder = null;
        Document doc = null;
        StringReader in = null;
        List<Element> items = null;
        try {
            con = (HttpURLConnection) url.openConnection();
            if ((con != null) && (con.getResponseCode() == 200)) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        con.getInputStream(), "UTF-8"));
                String line = "";
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                saxBuilder = new SAXBuilder();
                in = new StringReader(sb.toString());
                doc = saxBuilder.build(in);

                items = new ArrayList<Element>();
                items = XPath.selectNodes(doc, "/api/query/pages/page");
                return items;
            } else {
                System.out.println("Error getItemsRevision");
                return null;
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     *
     * @param categoryid
     * @param type page|subcat
     * @return map<id,name>
     */
    @SuppressWarnings("unchecked")
    public LinkedHashMap<String, String> getCategoryMembers(String categoryid,
            String type) {

        HttpURLConnection con = null;
        SAXBuilder saxBuilder = null;
        Document doc = null;
        StringReader in = null;
        List<Element> itemsPages = new ArrayList<Element>();
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        LinkedHashMap<String, String> mapToReturn = new LinkedHashMap<String, String>();

        String limit = "";
        if (type.equals("page")) {
            limit = ConfClient.getValue("wikipedia.crawl.maxpages");
        } else if (type.equals("subcat")) {
            limit = ConfClient.getValue("wikipedia.crawl.maxcategory");
        } else {
            return null;
        }
        map.put("action", "query");
        map.put("cmtype", type);
        map.put("list", "categorymembers");
        map.put("format", "xml");
        map.put("cmlimit", limit);
        map.put("cmpageid", categoryid);
        URL url = null;
        try {
            url = getURL(map);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
            return null;
        }

        try {
            con = (HttpURLConnection) url.openConnection();
            if ((con != null) && (con.getResponseCode() == 200)) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        con.getInputStream(), "UTF-8"));
                String line = "";
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                saxBuilder = new SAXBuilder();
                in = new StringReader(sb.toString());
                doc = saxBuilder.build(in);

                itemsPages = XPath.selectNodes(doc,
                        "/api/query/categorymembers/cm");
                String id = "";
                String title = "";
                for (Element e : itemsPages) {
                    id = e.getAttributeValue("pageid");
                    title = e.getAttributeValue("title");
                    if (title.contains("Category:")) {
                        title = title.replace("Category:", "");
                    }
                    if (type.equals("subcat")) {
                        String key = mapLoop.get(id);
                        if (key == null) {
                            mapLoop.put(id, id);
                            mapToReturn.put(id, title);
                        }
                    } else {
                        mapToReturn.put(id, title);
                    }
                }
            } else {
                return null;
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mapToReturn;

    }

    /**
     *
     * @param title
     * @return
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public String getPageIdFromTitle(String title) throws IOException {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        HttpURLConnection con = null;
        SAXBuilder xmlBuilder = null;
        Document xmlDoc = null;
        StringReader in = null;
        List<Element> itemsXp = null;
        map.put("action", "query");
        map.put("prop", "info");
        map.put("titles", URLEncoder.encode(title, "UTF-8"));
        map.put("format", "xml");
        try {
            URL url = getURL(map);
            con = (HttpURLConnection) url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    con.getInputStream(), "UTF-8"));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            xmlBuilder = new SAXBuilder();
            in = new StringReader(sb.toString());
            xmlDoc = xmlBuilder.build(in);
        } catch (IOException e) {
            return "0";
        } catch (JDOMException e) {
            return "0";
        }
        itemsXp = new ArrayList<Element>();
        try {
            itemsXp = XPath.selectNodes(xmlDoc, "/api/query/pages/page");
        } catch (JDOMException e) {
            e.printStackTrace();
        }
        if (!itemsXp.isEmpty()) {
            return itemsXp.get(0).getAttributeValue("pageid");
        }
        return "0";

    }

    /**
     *
     * @param pagesToCsv
     * @return
     */
    public boolean writeCsv(ArrayList<String> pagesToCsv) {
        File file = null;
        OutputStreamWriter writer = null;
        BufferedWriter fbw = null;
        try {
            file = new File(ConfClient.getValue("wikipedia.crawl.pathcsv")
                    + this.category);
            if (!fileCreated) {
                if (file.exists()) {
                    file.delete();
                } else {
                    file.createNewFile();
                }
                fileCreated = true;
            }
            writer = new OutputStreamWriter(new FileOutputStream(file, true),
                    "UTF-8");
            fbw = new BufferedWriter(writer);
            for (String row : pagesToCsv) {
                fbw.write(row.replaceAll("[\n\r]", " "));
                fbw.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fbw) {
                    fbw.close();
                }
                if (null != writer) {
                    writer.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return true;

    }

    /**
     *
     * @param text
     * @return
     * @throws UnsupportedEncodingException
     */
    public String filterNode(String text) {
        text = text.replaceAll("[,\"\\s\\n\\r]", " ");
        return text;
    }

    /**
     *
     * @param text
     * @return
     * @throws UnsupportedEncodingException
     */
    public String filterSource(String text) {
        return text;
    }
}
