package com.iw.tools;

import java.util.ArrayList;
import java.util.List;

public class GoogleInfoBean {
	private String numErrors=" - ";
	private String numSuccess=" - ";
	private List<String> csv = null;
	private String filter=  " - ";
	
	public GoogleInfoBean(){
		csv = new ArrayList<String>();
	}
	
	public String getNumErrors() {
		return numErrors;
	}
	public void setNumErrors(String numErrors) {
		this.numErrors = numErrors;
	}
	public String getNumSuccess() {
		return numSuccess;
	}
	public void setNumSuccess(String numSuccess) {
		this.numSuccess = numSuccess;
	}
	public List<String> getCSV() {
		return csv;
	}
	public void addCSV(String csv) {
		this.csv.add(csv);
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	
}
