/*
 * GUI : Display Jtree, principal frame
 * @authors Andres F. Restrepo A.
 *
 * @version 0.1
 * Date : Jul 24, 2011, 11:56 PM
 */

package com.iw.tools.webdav.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import com.googlecode.sardine.DavResource;
import com.iw.tools.webdav.connection.Webdav;
import com.iw.tools.webdav.tools.TreeNodeWebDav;

public class Explorer extends JDialog implements ActionListener, Runnable {
	/**
	 * 
	 */
	private static final String MESSAGE_ERROR_WEBDAV = "Could not connect to the server "
			+ "webdav look at your settings";

	private static final long serialVersionUID = 994392040357787909L;

	private JTree tree;

	private JToolBar toolBar;

	private JButton btnRefresh, btnSelection, btnAdminServer;

	private JProgressBar pgBar;
	private JScrollPane scrollPane;
	private Webdav sardineInstance;
	private int contBar;
	private TreePath pathSelection;
	private String pathRootPathWebdav = null;// save the path for build the tree
												// with the path root

	/**
	 * Constructor
	 */

	public Explorer() {
		toolBar = new JToolBar();
		btnRefresh = new JButton("Refresh");
		btnSelection = new JButton("Select");
		btnAdminServer = new JButton("Servers");
		btnRefresh.addActionListener(this);
		btnSelection.addActionListener(this);
		btnAdminServer.addActionListener(this);

		pgBar = new JProgressBar();
		pgBar.setStringPainted(true);

		toolBar.add(btnRefresh);
		toolBar.add(btnSelection);
		toolBar.add(btnAdminServer);
		toolBar.add(pgBar);

		// add
		setModal(true);
		getContentPane().add(toolBar, BorderLayout.NORTH);
		scrollPane = new JScrollPane();
		getContentPane().add(scrollPane);
		setTitle("Webdav explorer");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(400, 600);
		setVisible(true);

	}

	/**
	 * call ServerAdmin to administer webdav servers
	 */
	private void initWebdav() {

		ServerAdmin sa = new ServerAdmin();
		if (!ServerAdmin.cancel) {
			String hostName = sa.getHostname();
			String user = sa.getUser();
			String password = sa.getPassword();
			String path = sa.getPath();

			if (hostName.endsWith("/")) {
				hostName = hostName.substring(0, hostName.length() - 1);
			}
			if (!path.startsWith("/")) {
				path = "/" + path;
			}
			if (!path.endsWith("/")) {
				path = path + "/";
			}
			this.pathRootPathWebdav = (hostName + path);

			if ((hostName.equals("")) || (path.equals(""))) {
				JOptionPane.showMessageDialog(this, "Not selecting Directory");
			} else {
				try {
					sardineInstance = new Webdav(hostName, path, user, password);
					setEnabled(false);
					setTitle("LOADING...");

					setJProgressbar();
					Thread hilo = new Thread(this);
					hilo.start();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					JOptionPane.showMessageDialog(this, MESSAGE_ERROR_WEBDAV);
					setEnabled(true);
					setTitle("Webdav explorer");
				}

			}
		}

	}

	/**
	 * configure progressbar to measure progress
	 */
	private void setJProgressbar() {
		List<DavResource> lista = null;
		try {
			lista = sardineInstance.getListResource(sardineInstance
					.getUrlBase() + sardineInstance.getPath());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, MESSAGE_ERROR_WEBDAV);
		}
		int max = lista.size();
		pgBar.setMinimum(0);
		pgBar.setMaximum(max - 2);
	}

	/**
	 * this method build the jtree
	 * 
	 * @param path
	 *            : route to traverse the tree
	 * @param rootNode
	 *            : save the node to build the tree
	 * @param prof
	 *            : to measure the increase of the progressbar
	 */
	private void buildTree(String path, DefaultMutableTreeNode rootNode,
			int prof) {

		if (path.contains(" ")) {
			path = path.replace(" ", "%20");
		}
		List<DavResource> lista = null;
		try {
			lista = sardineInstance.getListResource(sardineInstance
					.getUrlBase() + path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, MESSAGE_ERROR_WEBDAV);
		}
		// System.out.println(lista);//DEBUG
		if (lista != null) {

			for (int i = 1; i < lista.size(); i++) {

				if (lista.get(i).isDirectory()) {
					prof++;
					TreeNodeWebDav nodeDirectory = new TreeNodeWebDav(lista
							.get(i).getName());
					nodeDirectory.setDirectory(true);
					buildTree(lista.get(i).getPath(), nodeDirectory, prof);
					rootNode.add(nodeDirectory);

				} else {

					TreeNodeWebDav node = new TreeNodeWebDav(lista.get(i)
							.getName());
					node.setDirectory(false);
					rootNode.add(node);
					prof++;
				}

				if (prof == 1) {
					pgBar.setValue(contBar++);
					prof = 0;
				}

			}

		}
	}

	public TreePath getPathSelection() {
		return this.pathSelection;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btnRefresh)) {
			if (tree != null) {
				setEnabled(false);
				setTitle("LOADING...");
				contBar = 0;
				Thread hilo1 = new Thread(this);
				hilo1.start();
			}

		} else if (e.getSource().equals(btnSelection)) {
			try {
				TreePath path = tree.getSelectionPath();
				String pathAux = "";

				if (path == null)
					return;
				System.out.println(path.toString());
				pathAux = pathAux.concat(path.getPathComponent(1).toString());
				for (int i = 2; i < path.getPathCount(); i++) {
					pathAux = pathAux.concat(path.getPathComponent(i)
							.toString() + "/");
				}
				this.pathSelection = path;

				dispose();
			} catch (NullPointerException ex) {
				System.out.println(ex.getMessage());
				JOptionPane.showMessageDialog(this, "Error, Nothing selected");
			} catch (Exception exp) {
				System.out.println(exp.getMessage());
				JOptionPane.showMessageDialog(this, "Error, Nothing selected");
			}

		} else if (e.getSource().equals(btnAdminServer)) {
			contBar = 0;
			pgBar.setValue(contBar);
			initWebdav();
		}

	}

	// begin to load JTREE
	@Override
	public void run() {
		if (tree != null) {
			scrollPane.remove(tree);
		}
		setEnabled(false);
		DefaultMutableTreeNode webdavJtree = new TreeNodeWebDav("Webdav");
		DefaultTreeModel modelo = new DefaultTreeModel(webdavJtree);

		tree = new JTree(modelo);
		((DefaultTreeModel) tree.getModel()).nodeChanged(webdavJtree);
		DefaultMutableTreeNode n = new TreeNodeWebDav(this.pathRootPathWebdav);
		((TreeNodeWebDav) n).setDirectory(true);

		modelo.insertNodeInto(n, webdavJtree, 0);

		try {
			buildTree(sardineInstance.getPath(), n, 0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		setEnabled(true);
		setTitle("Webdav explorer");
		scrollPane.setViewportView(tree);
		//System.out.println(modelo.getChild(webdavJtree, 0));
	}
}
