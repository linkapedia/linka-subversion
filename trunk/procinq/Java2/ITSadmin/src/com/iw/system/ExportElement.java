/*
 * Created by IntelliJ IDEA.
 * User: indraweb
 * Date: Mar 29, 2005
 * Time: 10:14:28 AM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.system;

import com.iw.ui.PopupProgressBar;

import java.io.File;

public class ExportElement {
    public ExportElement() { }

    public File exportTaxonomy(ITS server, Corpus c, PopupProgressBar ppb) throws Exception {
        System.err.println("Error: base element called, must be subclassed.");

        throw new Exception("Error: base element called, must be subclassed.");
    }
    public File exportTaxonomy(ITS server, Corpus c) throws Exception {
        System.err.println("Error: base element called, must be subclassed.");

        throw new Exception("Error: base element called, must be subclassed.");
    }
}
