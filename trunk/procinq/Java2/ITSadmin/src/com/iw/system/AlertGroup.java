package com.iw.system;

public class AlertGroup {
    // User attributes
    private String ID;
    private String Name;

    // constructor(s)
    public AlertGroup (String ID, String Name) { this.ID = ID; this.Name = Name; }

    // accessor functions
    public String getID() { return ID; }
    public String getName() { return Name; }

    public String toString() { return Name; }
}
