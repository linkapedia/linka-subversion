/*
 * Created by IntelliJ IDEA.
 * User: Intellisophic
 * Date: Jun 29, 2004
 * Time: 1:55:31 PM
 * To change template for new class use 
 * Code Style | Class Templates options (Tools | IDE Options).
 */
package com.iw.system;

import org.dom4j.*;
import java.util.*;
import java.text.StringCharacterIterator;

// this class represents a single piece of PDF data within an XML corpus ingestion
public class PDFelement {

    // paragraph elements
    private boolean tEmphasis = false;
    private boolean tBold = false;
    private boolean tItalic = false;
    private String tFont = "None";
    private String tFontColor = "None";
    private String tFontSize = "0";
    private String tHeight = "0.0";
    private String tOrigFont = "None";
    private boolean bStartsCapitalEndsPunctuationMostlyWhitespace = false;

    public float Y = -1;

    private ParagraphElement pElement = null;

    // keep three examples for this object
    public String[] example = new String[10];

    private int examples = 0;

    public PDFelement() { }
    public PDFelement(ElementPath ep) {
        fillText(ep);
    }

    // accessor functions
    public boolean IsParagraphEmphasis() { return tEmphasis; }
    public boolean IsParagraphBold() { return tBold; }
    public boolean IsParagraphItalic() { return tItalic; }
    public boolean isRuleOne() { return bStartsCapitalEndsPunctuationMostlyWhitespace; }

    public void setParagraph(ParagraphElement pe) { pElement = pe; }

    public String getFont() { return tFont; }
    public String getFontColor() { return tFontColor; }
    public String getFontSize() { return tFontSize; }
    public String getParagraphHeight() { return tHeight; }
    public String getOrigFont() { return tOrigFont; }
    public String getHeight() { return tHeight; }

    public String getID() {
        String ID = tEmphasis+"-"+tBold+"-"+tItalic+"-"+tFont+"-"+tFontColor+"-"+tFontSize+"-"+tOrigFont+"-"+bStartsCapitalEndsPunctuationMostlyWhitespace;//+"-"+tHeight;
        if (pElement == null) { return ID; }
        else {
            //System.out.println("*** pelement: "+pElement.getID()+" **THISPUSCARID: "+ID+" ***");
            return pElement.getID()+ID; }
    }

    /*
      Attribute: emphasis Value: true
      Attribute: emphasis-bold Value: true
      Attribute: font Value: Times
      Attribute: font-color Value: #000000
      Attribute: font-size Value: 13
      Attribute: height Value: 14.0
      Attribute: number Value: 1
      Attribute: origfont Value: old
      Attribute: width Value: 193.0
      Attribute: x Value: 119.0
      Attribute: y Value: 168.0

    */

    // TEXT emphasis-bold="true" font="Times" font-color="#000000" font-size="15" height="16.0" number="1.1" origfont="old" width="98.0" x="54.0" y="197.0">
    public void fillText(ElementPath ep) {
        if (ep.getPath().endsWith("TEXT")) {
            Element e = ep.getCurrent();

            Attribute emphasis = e.attribute("emphasis");
            if ((emphasis != null) && (emphasis.getText().equals("true"))) tEmphasis = true;

            Attribute bold = e.attribute("emphasis-bold");
            if ((bold != null) && (bold.getText().equals("true"))) tBold = true;

            Attribute italic = e.attribute("emphasis-italic");
            if ((italic != null) && (italic.getText().equals("true"))) tItalic = true;

            Attribute font = e.attribute("font"); if (font != null) tFont = font.getText();
            Attribute fontColor = e.attribute("font-color"); if (fontColor != null) tFontColor = fontColor.getText();
            Attribute fontSize = e.attribute("font-size"); if (fontSize != null) tFontSize = fontSize.getText();
            Attribute height = e.attribute("height"); if (height != null) tHeight = height.getText();
            Attribute origFont = e.attribute("origfont"); if (origFont != null) tOrigFont = origFont.getText();
            Attribute aY = e.attribute("y");
            if (aY != null) Y = Float.parseFloat(aY.getText());

            // new - start generating rules based upon text
            if ((e.getText().trim().length() > 5) && (ITS.getUser().isRuleOneSet())) {
                char[] characters = e.getText().trim().toCharArray();
                char[] charactersU = e.getText().toUpperCase().trim().toCharArray();
                StringCharacterIterator data = new StringCharacterIterator(e.getText());
                int countC = 0; int countS = 0;

                for (char c = data.first(); c != data.DONE; c=data.next()) {
                    if (c == ' ') countS++; else countC++;
                }

                if (((characters[0] >= 0x41) && (characters[0]<= 0x5A)) && (countC < 50) &&
                    (((charactersU[charactersU.length-1] >= 0x41) && (charactersU[charactersU.length-1]<= 0x5A)) ||
                     ((charactersU[charactersU.length-1] >= 0x30) && (charactersU[charactersU.length-1]<= 0x39))))
                    bStartsCapitalEndsPunctuationMostlyWhitespace = true;
            }

            example[0] = ep.getCurrent().getText(); examples++;

            //System.out.println(example[0]+" ::: "+getID());
        }
    }

    public void addExample(PDFelement pdf) {
        if (examples < 10) { example[examples] = pdf.example[0]; examples++; }
        //else { System.out.println("debug: "+getID()+" has more than 8 elements."); }
    }

   public String toString() {
        String s = "";

       /*
        System.out.println("PDFelement:");
        System.out.println("   Paragraph Emphasis: "+tEmphasis);
        System.out.println("   Paragraph Bold: "+tBold);
        System.out.println("   Paragraph Italic: "+tItalic);
        System.out.println("   Paragraph Font: "+tFont);
        System.out.println("   Paragraph Color: "+tFontColor);
        System.out.println("   Paragraph Size: "+tFontSize);
        System.out.println("   Paragraph Height: "+tHeight);
        System.out.println("   Paragraph OrigFont: "+tOrigFont);

        for (int i = 0; i < examples; i++) System.out.println("Example "+(i+1)+": "+example[i]);
        System.out.println(" ");
        */

        return s;
    }
}
