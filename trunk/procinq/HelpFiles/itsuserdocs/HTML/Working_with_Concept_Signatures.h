<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<META NAME="GENERATOR" CONTENT="Solutionsoft HelpBreeze JavaHelp Edition">
<TITLE>Working with Concept Signatures</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF=../javahelp.css>
</HEAD>
<BODY BGCOLOR=#ffffff>
<H1>Working with Concept Signatures</H1>
<P>Concept signatures are used for matching documents with topics.&nbsp; A 
concept signature is a collection of terms ( words or phrases ) that distinguish 
one concept from another.&nbsp;&nbsp; In an Indraweb delivered taxonomy, these 
are included in the topic map and developed through the OCI ( Orthogonal Corpus 
Indexing ) algorithm.&nbsp;&nbsp; In the Editor's Desktop, these signatures can 
be edited, deleted or created for a topic.</P>
<P>&nbsp;</P>
<P><IMG style="WIDTH: 753px; HEIGHT: 517px" height=546 hspace=0 
src="../Images/conceptsig.GIF" width=846 border=0></P>
<P>&nbsp;</P>
<P>In the above picture we can see a topic with its concept signatures.</P>
<P>The first column is the term.&nbsp; A term is a word or a phrase that 
contributes to making a document about this topic.</P>
<P>The second column is the weight.&nbsp; This is the relative importance of 
the term.&nbsp; A higher number denotes more importance for the term.</P>
<P>The weight itself is used as a frequency.&nbsp;&nbsp; It is the percentage of 
terms in the document.&nbsp; To make this a percentage, there is also a document 
size field.&nbsp; In this example it is 45.</P>
<P>&nbsp;</P>
<P>To add a new signature, hit the :</P>
<P><IMG height=34 hspace=0 src="../Images/plus.GIF" width=57 border=0></P>
<P>This will create a new term that you can adjust the weight for</P>
<P>&nbsp;</P>
<P>To remove a signature, hit the:</P>
<P><IMG height=29 hspace=0 src="../Images/minus.GIF" width=58 border=0></P>
<P>When you are finished, you can press save to post your changes to the 
database.</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
<P>&nbsp;</P>
</BODY>
</HTML>