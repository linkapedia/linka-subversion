#!C:\perl\bin\perl.exe

#
# index.cgi
#
# This script will take a properly formatted "book" and convert
# it into corpus normal form 
#
# 1) Accept as input (from an HTML page) a path to the file to be processed,
#    the directory to put the corpus normal form, and finally the font tag(s) 
#    that uniquely identifies the sections
# 2) Process this file line by line (do not load entirely into memory)
#    and identify each of the sections.    
# 3) Print the section titles out as HTML and use radio buttons to allow
#    the user to identify the "type" of each section
# 4) After the user has identified each section and submitted the form, 
#    break the document up into sections and write it out as corpus normal
#    form in the area specified.
# 5) Topics identified with type -1 will be skipped.
# 6) "In progress" saves are saved with the corpus name plus .sav in the destination directory
#
#  
# Written by Michael A. Puscar 6/10/2002 through 6/20/2002
########################################################
#
use CGI;

# set default values
local ($filename) = "";
local ($font) = "";
local ($destination) = "";
local ($corpusname) = "";
local ($submitp) = 0;
local ($includechildren) = 0;

local ($query) = new CGI;
$filename = $query->param('filename') || "";
$font = $query->param('font') || "";
$destination = $query->param('destination') || "";
$corpusname = $query->param('corpusname') || "";

if ($query->param('includechildren') eq "on") { $includechildren = 1; }

if (defined($query->param('submit'))) { $submitp = 1; }

if ($filename eq "") { &write_error ("A filename or location must be specified.", 1); }
if ($destination eq "") { &write_error ("You must specify a destination directory.", 1); }
if ($font eq "") { &write_error ("A unique font code must be specified to identify the sections properly.", 1); }
if ($corpusname eq "") { &write_error ("You must specify a name for this corpus.", 1); }

$font = "(".$font;
if ($query->param('font2') ne "") { $font = $font . "|" . $query->param('font2'); }
if ($query->param('font3') ne "") { $font = $font . "|" . $query->param('font3'); }
if ($query->param('font4') ne "") { $font = $font . "|" . $query->param('font4'); }
if ($query->param('font5') ne "") { $font = $font . "|" . $query->param('font5'); }
$font = $font.")";

# Open the file
open (BOOK, "$filename") || &write_error ("Error: book file not found, $filename\n", 1);
my (@lines) = <BOOK>; close(BOOK);

&write_header();

# If the SAVE or SUBMIT parameters were specified, archive the user's 
#   work for future modification
if (defined($query->param('save')) || $submitp) {
   open (FILE, ">$destination/$corpusname.sav");
   local ($queryloop) = 1;
   
   while (defined($query->param($queryloop))) {
	my ($type) = "type".$queryloop;
      if (defined($query->param($type))) { $type = $query->param($type); }
      else { $type = -2; }
      print FILE $query->param($queryloop)."||".$type."\n";
	$queryloop++;
   }

   close(FILE);
}

if (!$submitp) { 
   &write_template("opentable.tpl"); 

   # if a save file already exists, use it and don't bother parsing the book again
   if (-e "$destination/$corpusname.sav") {
      open (FILE, "$destination/$corpusname.sav"); local (@save) = <FILE>; close(FILE);
      $loop = 0;
      local ($bgcolor) = "";
      foreach $line (@save) {
         $loop++; chomp($line);
         local ($type0, $type1, $type2, $type3, $type4, $type5, $type6, $type7, $type8, $typee);
         my ($section, $type) = split(/\|\|/, $line);

	 if (($loop % 2) == 1) { $bgcolor = "bgcolor='AD9273' "; }
	 else { $bgcolor = "73AD73"; }

         print "<tr $bgcolor><td> &nbsp; <input type=text name='$loop' value='$section' size=75>";
	   if ($type == 0) { $type0 = "checked"; }
	   if ($type == 1) { $type1 = "checked"; }
	   if ($type == 2) { $type2 = "checked"; }
	   if ($type == 3) { $type3 = "checked"; }
	   if ($type == 4) { $type4 = "checked"; }
	   if ($type == 5) { $type5 = "checked"; }
	   if ($type == 6) { $type6 = "checked"; }
	   if ($type == 7) { $type7 = "checked"; }
	   if ($type == 8) { $type8 = "checked"; }
	   if ($type == -1 ) { $typee = "checked"; }
         &write_template("type.tpl");
       }
   &write_template("footer.tpl");
   exit(1);
   }
}
$loop = 0;

# The following variables are defined and used on submit only
local (@buffer_arr); local (@names); local (@cnf_arr); 
local ($section_depth) = -1; local ($write_to_disk) = 1;
local ($currdestination) = $destination."/"."001";

# write the init
if ($submitp) { 
    mkdir ($currdestination) || &write_error("Could not make directory $currdestination\n");
    open (FILE, ">$currdestination/000.html") || &write_error("Could not write to $currdestination/000.html\n"); 
    print FILE "<HTML><HEAD>\n<TITLE>$corpusname</TITLE>\n</HEAD>\n</HTML>\n";
    close (FILE);
}

foreach $line (@lines) {
    chomp($line); 
    if ($line =~ /<FONT size\=2 face\="$font(.*?)\">(.*?)<\/FONT>/i) {
       $loop++;
       my ($section) = $3;

       $section =~ s/<.*?>//gi;
       if ($section =~ /(.*)\./) { $section = $1; }
       $section =~ s/\b(\w+)/\L\u$1/g; # capitalize first letter, lowercase the rest

       # if only writing the template, write the template
       if (!$submitp) {
	  if (($loop % 2) == 1) { $bgcolor = "bgcolor='999999' "; }
	  else { $bgcolor = "bgcolor='ffffff' ";}
	  print "<tr $bgcolor><td> &nbsp; <input type=text name='$loop' value='$section' size=75>";
          &write_template("type.tpl");
       } else {
          # This marks the end of the previous section and the beginning of a new section.
          # If the previous section is not empty, and it is equal to or lower in depth than
          #  the upcoming section, write it out to disk now.
         
          # RULE: At the end of a section--
          #  IF entering into another section of EQUAL LEVEL, 
          #     * print previous section to disk
          #  IF entering into another section of LOWER LEVEL,
          #     * create the directory but do not write
          #  IF entering into another section of HIGHER LEVEL,
          #     * print all sections to disk up to level going to..

          local ($param) = "type".$loop;

          print "<!-- section depth is $section_depth -->\n";
          print "<!-- query param is ".$query->param($param)." --> \n";

	    if ($query->param($param) == -1) {
             # section marked for deletion, set deletion parameter
             $write_to_disk = 0;
          } else {
             $write_to_disk = 1;
             if ($section_depth != -1) {
                $currdestination = &getdestination();
                print "<!-- making directory $currdestination -->\n";
                mkdir ($currdestination) || &write_error("Could not make directory $currdestination\n");
             }

             if ($section_depth >= $query->param($param)) {
                for ($i = $section_depth; $i >= $query->param($param); $i = $i - 1) {
                   my ($tmpdestination) = &gettmpdestination($i);
		       print "<!-- writing to $tmpdestination/000.html ($i, ".$query->param($param).") -->\n";
                   open(FILE, ">$tmpdestination/000.html") || &write_error("$tmpdestination/000.html failed to write.\n");
                   print FILE "<HTML><HEAD>\n<TITLE>".$names[$i]."</TITLE>\n</HEAD>\n";
                   print FILE $buffer_arr[$i];
                   print FILE "</HTML>\n";
                   close (FILE); 
			 $buffer_arr[$i] = "";
                   print "<!-- Buffer written and cleared for section depth $i to $tmpdestination -->\n";
                   if ($i != $query->param($param)) { $cnf_arr[$i] = -1; }
                }
             }

             $section_depth = $query->param($param); 
             $names[$section_depth] = $query->param($loop);
             if (defined($cnf_arr[$section_depth]) && ($cnf_arr[$section_depth] != -1)) {
                $cnf_arr[$section_depth] = $cnf_arr[$section_depth] + 1; 
             } else { $cnf_arr[$section_depth] = 1; }
          }     
       }
    }
    if (($submitp) && ($section_depth >= 0) && ($write_to_disk == 1)) { 
       if ($includechildren == 1) {
          for ($i = 1; $i <= $section_depth; $i++) { 
		print "<!-- Writing line to buffer depth: $i -->\n";
       	if (defined($buffer_arr[$i])) { $buffer_arr[$i] = $buffer_arr[$i] . $line . "\n"; }
            else { $buffer_arr[$i] = "$line\n"; }
          }
       } else {
	    print "<!-- Writing line to buffer depth: $section_depth -->\n";
          if (defined($buffer_arr[$section_depth])) {
  	      $buffer_arr[$section_depth] = $buffer_arr[$section_depth] . $line . "\n"; }
          else { $buffer_arr[$section_depth] = "$line\n"; }
       }
    }
}

if ($submitp) {
    while ($section_depth >= 0) { 
       my ($tmpdestination) = &gettmpdestination($section_depth);
       mkdir ($tmpdestination) || warn "Could not create directory $tmpdestination\n";
       open(FILE, ">$tmpdestination/000.html") || &write_error("Cannot open $tmpdestination/000.html for writing.\n");
       print FILE "<HTML><HEAD>\n<TITLE>".$names[$section_depth]."</TITLE>\n</HEAD>\n";
       print FILE $buffer_arr[$section_depth];
       print FILE "</HTML>\n";
       close (FILE);

       $cnf_arr[$section_depth] = -1;
       $section_depth = $section_depth - 1;
    }
    local ($goto) = $destination."\\"."001";
    &write_template("done.tpl");
} else { &write_template("footer.tpl"); }

sub getdestination {
    $currdestination = $destination."/001";
    my ($arrayindex) = 0;

    while (defined($cnf_arr[$arrayindex])) {
	if ($cnf_arr[$arrayindex] > 0) {
	    $currdestination = $currdestination . "/" . sprintf("%03d", $cnf_arr[$arrayindex]);
	}
	$arrayindex++;
    }

    return $currdestination;
}

sub gettmpdestination {
    my ($section) = $_[0];
    my ($arrayindex) = 0;
    my ($tmp) = $destination . "/001";

    print "<!-- tmpdestination called with $section .. -->\n";
    while (defined($cnf_arr[$arrayindex])) {
	if (($cnf_arr[$arrayindex] > 0) && ($arrayindex <= $section)) {
	    $tmp = $tmp . "/" . sprintf("%03d", $cnf_arr[$arrayindex]);
	}
	$arrayindex++;
    }

    return $tmp;

}

sub parse_section {
    my ($section) = $_[0];

    if ($section =~ /<\/FONT><FONT/i) {
	if ($section !~ /<\/FONT><FONT size=2 face="$font/i) {
              #print "<BR>Before: $section\n";
	      $section =~ s/<FONT(.*)//gi;
              #print "<BR>After: $section\n";
              return $section;
        }
	$section =~ s/<\/FONT><FONT size=2 face="$font,(.*?)>(.*)//gi;
        $section = $section . "FNORD" . $2;
        #print "<BR>Parse section 2: $section\n";
        
        $section = &parse_fnord_section($section);
        #print "<BR>Parse section 3: $section\n";
        return &parse_section($section);
    } else { return $section; }
}

sub parse_fnord_section {
    my ($section) = $_[0];

    if ($section =~ /<\/FONT><FONT/) { 
	$section =~ s/FNORD(.*)<FONT(.*)//gi;
	$section = $section . $1;
        #print "<BR>Parse fnord section 1: $section\n";
    } else { $section =~ s/FNORD//gi; }
    #print "<BR>Parse fnord section 2: $section\n";
    return $section;

}

sub write_header {
    print "Content-type: text/html\n\n";
    open (FILE, "header.tpl"); while (<FILE>) { print $_; } close(FILE);
}

sub write_template {
    my $template = $_[0];
    open (FILE, "$template"); my (@linez) = <FILE>; close(FILE);
    foreach $linex (@linez) {
	$linex =~ s/##(.*)##/${$1}/gi;
	print $linex;
    }
}

sub write_error {
    my ($message) = $_[0];
    my ($header) = $_[1];

    if (defined($header)) { &write_header(); }

    print "<b>Error:</b> $message\n";
    exit(1);
}


