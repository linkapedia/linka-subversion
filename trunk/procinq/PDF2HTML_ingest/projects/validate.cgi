#!C:\perl\bin\perl.exe

#
# validate.cgi
#
# Given a path to a CNF location, walk through each 000.html in the tree
# and validate the following:
#
# 1) Ensure size of the file is over 1k
# 2) Document has a title
# 3) Document title has no non-alphanumeric characters
# 4) Document title is not longer than 150 characters

use CGI;

local ($query) = new CGI;
local ($destination) = $query->param('destination') || "";

if ($destination eq "") { &write_error ("You must specify a destination directory.", 1); }
$destination;

&write_header();
print "<b>Validation results</b>:<BR>\n<UL> \n";

local (@directories_to_search); local ($directory) = "";
push (@directories_to_search, $destination); local (%titles);

while ($directory = pop(@directories_to_search)) {
    &validate_000($directory); # first validate the 000.html in this directory
    opendir(DIR, $directory); # next, find any subdirectories
    my (@allFiles) = readdir(DIR);

    foreach $file (@allFiles) {
       if (($file ne "000.html") && ($file !~ /\./)) {
	  if (-e $directory."\\".$file) {
  	     push (@directories_to_search, $directory."\\".$file);
	     #print "<!-- Found ".$directory."\\".$file." -->\n";
	  }
       }
    }
    undef @allFiles;
}
print "</UL>\n";

&write_template("footer-valid.tpl");

1;

sub validate_000 {
   my ($directory) = $_[0];
   my ($filename) = $directory."\\"."000.html";

   # check file for existence
   if (!-e $filename) { print "<li>Filename: $filename <u>does not exist</u>!!\n"; return; }

   # check file length
   #print "<!-- $filename size: ".(-s $filename)." -->\n";
   if ((-s $filename) < 700) { 
        # only mark as suspiciously short if it's a leaf node, meaning no children
	opendir(DIR, substr($filename, 0, length($filename)-8));
	local (@files) = readdir(DIR); closedir(DIR);    
        if (scalar(@files) > 3) {
	   print "<li>Filename: <a href='$filename'>$filename</a> is suspiciously short (".(-s $filename)." bytes\n";
	} undef @files;
   }

   open (FILE, "$filename"); local ($data) = "";
   while (<FILE>) { $data = $data.$_; } close (FILE);

   $data =~ s/\n//gi;

   if ($data =~ /<TITLE>(.*?)<\/TITLE>/gi) {
	my ($title) = $1; 
        #print "<!-- Title: $title --\n";
  
        if (length($title) > 100) { 
	    print "<li>Filename: <a href='$filename'>$filename</a> has extremely long title ($title)\n"; 
	}

        $ctitle = $title; $ctitle =~ s/\ //gi; $ctitle =~ s/\,//gi; 
	$ctitle =~ s/\(//gi; $ctitle =~ s/\)//gi; $ctitle =~ s/\-//gi;
	$ctitle =~ s/\.//gi; $ctitle =~ s/\://gi; $ctitle =~ s/\///gi;
	if ($ctitle =~ /(\W)/gi) { 
	    print "<li>Filename: <a href='$filename'>$filename</a> has ".
	          "non-alphanumeric characters (title: $title) characters: $1\n";
        }

	# now check and see if this is a duplicate title
	if (defined($titles{$title})) { 
	    if ($filename =~ /(.*)\\(.*)\\000.html/) { 
		my ($path) = $1; 
                if ($path eq $titles{$title}) {
		    print "<li>Filename: <a href='$filename'>$filename</a> has same title ($title) as ".
			  "<a href='".$titles{$title}."'>".$titles{$title}."</a>\n";
	        }
	    }
	} 

	if ($filename =~ /(.*)\\(.*)\\000.html/) {
	   $titles{$title} = $1; 
        }

   } else {
	print "<li>Filename: <a href='$filename'>$filename</a> has no title!\n";
   }
}

sub write_header {
    print "Content-type: text/html\n\n";
    open (FILE, "header.tpl"); while (<FILE>) { print $_; } close(FILE);
}

sub write_template {
    my $template = $_[0];
    open (FILE, "$template"); my (@linez) = <FILE>; close(FILE);
    foreach $linex (@linez) {
	$linex =~ s/##(.*)##/${$1}/gi;
	print $linex;
    }
}

sub write_error {
    my ($message) = $_[0];
    my ($header) = $_[1];

    if (defined($header)) { &write_header(); }

    print "<b>Error:</b> $message\n";
    exit(1);
}

1;
