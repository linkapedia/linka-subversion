#!/usr/bin/perl
#
# GetNode.Cgi
# Written by: Michael A. Puscar
# A simple CGI to create the frameset

use CGI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;

# Were username and password information even sent?
if ((!defined($query->param('hid'))) || 
    (!defined($query->param('clid'))) ||
    (!defined($query->param('clientname'))) ||
    (!defined($query->param('nid'))) ||
    (!defined($query->param('cid')))) {
    print "Location: /seditor/missing-info.html\n\n";
    exit(1);
}

local ($hid) = $query->param('hid');
local ($cid) = $query->param('cid');
local ($nid) = $query->param('nid');
local ($clid) = $query->param('clid');
local ($clientname) = $query->param('clientname');

local ($arguments) = "nid=$nid&cid=$cid&clid=$clid&hid=$hid&clientname=$clientname";

print "Content-type: text/html\n\n";
&print_template("editor-frame.tpl");

1;

