#!/usr/bin/perl
#
# blacklist.cgi
# Written by: Michael A. Puscar
# - Create a global blacklist for the given words

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local (%cookies) = getCookies();

if (!defined($query->param('submit'))) {
    # Connect to the LINUX Oracle database
    local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
    my $q = "SELECT C.CorpusId, C.Corpus_Name FROM Publisher P, Corpus C where P.PublisherId = C.PublisherId and C.PublisherId = ".$cookies{'PUBLISHERID'};
    my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
    $sth->execute() || warn "Fatal database error: $!\n";

    local ($BLACKLISTFORM) = "<form method=get action=\"blacklist.cgi\">";
    $BLACKLISTFORM = $BLACKLISTFORM.
	"<b>Corpus:</b>&nbsp;<select name='corpus'><option value=0> All Corpuses\n";
    while (my ($cid, $cname) = $sth->fetchrow_array()) {
	$BLACKLISTFORM = $BLACKLISTFORM . "<option value=$cid> $cname\n"; }
    $BLACKLISTFORM = $BLACKLISTFORM.
	"</select><br><b>Black List Word: </b>&nbsp; <input type=\"text\" name=\"word\" size=30><input type=\"submit\" name=\"submit\" value=\"Submit\"></form>\n";
    
    $sth->finish;

    print "Content-type: text/html\n\n";
    &print_template("blacklist.tpl");
    exit(1);
}

local ($word) = $query->param('word');
local ($corpus) = $query->param('corpus');
local ($pid) = $cookies{'PUBLISHERID'};
local ($eid) = $cookies{'EDITORID'};

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");

if ($corpus == 0) { &blacklist_all(); } 
else { &blacklist_url ($corpus); }

print "Location: /seditor/blacklist-done.html\n\n";

$dbh->disconnect;

sub blacklist_url {
    my ($corpus) = $_[0];

    my $q="SELECT ClientId FROM Client WHERE CorpusId = $corpus";
    my $cth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
    $cth->execute() || warn "Fatal database error: $!\n";
    while (my $cid = $cth->fetchrow_array) {
	my $q="INSERT INTO BlackList (ClientId, CorpusId, EditorId, URLChunk, DateCreated) ";
	$q=$q."VALUES ($cid, $corpus, $eid, '$word', SYSDATE)";

	#print "Content-type: text/html\n\n $q\n"; exit(1);

	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish;

	$q="INSERT INTO EditorLog (EditorId, ActionCode, Action_String, ActionDate) ";
	$q=$q."VALUES ($eid, 5, 'Blacklisted: $word', SYSDATE)";

	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish;
    } $cth->finish;
}

sub blacklist_all {
    my $q="SELECT Cl.ClientId, C.CorpusId FROM Client Cl, Corpus C WHERE C.CorpusId = Cl.CorpusId AND C.PublisherId = $pid";
    my $pth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
    $pth->execute() || warn "Fatal database error: $!\n";
    while (my ($cid, $corpus) = $pth->fetchrow_array) {
	my $q="INSERT INTO BlackList (ClientId, EditorId, CorpusId, URLChunk, DateCreated) ";
	$q=$q."VALUES ($cid, $eid, $corpus, '$word', SYSDATE)";
	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish;

	$q="INSERT INTO EditorLog (EditorId, ActionCode, Action_String, ActionDate) ";
	$q=$q."VALUES ($eid, 5, 'Blacklisted: $word', SYSDATE)";
	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish;
    } $pth->finish;
}

1;

