#!/usr/bin/perl
#
# topic.cgi
# Written by: Michael A. Puscar

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;

# Were username and password information even sent?
if ((!defined($query->param('hid'))) || 
    (!defined($query->param('clid'))) ||
    (!defined($query->param('cname'))) ||
    (!defined($query->param('search'))) ||
    (!defined($query->param('cid')))) {
    print "Location: /seditor/missing-info.html\n\n";
    exit(1);
}

local ($hid) = $query->param('hid');
local ($cid) = $query->param('cid');
local ($clid) = $query->param('clid');
local ($clientname) = $query->param('cname');
local ($search) = $query->param('search');

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");

my $q = "SELECT N.NodeId, N.NodeTitle from Node N, Hierarchy H where H.NodeId = N.NodeId and H.HierarchyId = $hid and N.CorpusId = $cid and N.NodeTitle like '%$search%'";
my $sth = $dbh->prepare($q);
$sth->execute;

print "Content-type: text/html\n\n";OA
&print_head_restricted($cid);

print "Query results for <b>$search</b>:<P>\n<UL>\n";
while (my ($nid, $nti) = $sth->fetchrow_array) {
    print "<LI> <a href='./GetNode.cgi?nid=$nid&cid=$cid&hid=$hid&clid=$clid&clientname=$clientname'>$nti</a>\n";
}
print "</UL>\n";
if ($sth->rows() == 0) { print "Sorry, there are no topics in this corpus matching your query.  Press your <b>back</b> button and try your query again.\n"; }

&print_foot();
$sth->finish;
$dbh->disconnect();

1;

