#!/usr/bin/perl
#
# main.cgi
# Written by: Michael A. Puscar
# Main Menu

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
local (%cookies) = getCookies();
local ($sth) = &get_corpus($cookies{'PUBLISHERID'});

print "Content-type: text/html\n\n";
if (defined($query->param('submit'))) { 
   &print_head_with_cookies($query->param('corpus'));
} else { &print_head(); }

if (defined($query->param('corpus'))) { $cookies{'CORPUSID'} = $query->param('corpus'); }

print "<SCRIPT LANGUAGE='JavaScript'> if (!GetCookie('USERNAME')) { window.location = 'index.html'; } </SCRIPT>\n";
print "</font><table width=100%><tr><td>\n";
# allow users to select a different corpus
print "<form method=get action='main.cgi'>\n";
print "Change corpus: <select name='corpus'>\n";
while (my ($cid, $cname) = $sth->fetchrow_array) {
    if ($cid == $cookies{'CORPUSID'}) { print "<option selected value=$cid> $cname\n"; }
    else { print "<option value=$cid> $cname\n"; }
}
print "</select><input type='submit' name='submit' value='Change Corpus'></form>\n";
$sth->finish;

local ($cid) = $cookies{'CORPUSID'};
&print_template("main.tpl");

# Which client will the user browse ?
my $q = "select ClientName, ClientId, HierarchyId from Client where CorpusId = $cid";
my $ch = $dbh->prepare($q) || warn "Fatal database error: $!\n";
$ch->execute() || warn "Fatal database error: $!\n";

print "<b>Find a subject by hierarchy:</b> <br><u>Instructions</u>: Type in part or all of the topic title and click the search button.\n<UL>\n";
while (my ($clientname, $clientid, $hid) = $ch->fetchrow_array) {
    print "<form method=get action='topic.cgi'>\n";
    print "<input type=hidden name=clid value=$clientid>\n";
    print "<input type=hidden name=cid value=$cid>\n";
    print "<input type=hidden name=hid value=$hid>\n";
    print "<input type=hidden name=cname value='$clientname'>\n";
    print "<LI> $clientname - <input type=text name=search size=20>\n";
    print "<input type=submit name=submit value='Search'>\n";
}
print "</UL></font>\n";

print "</td></tr></table>\n";
&print_foot();

$ch->finish;
$dbh->disconnect();
1;
