#!/usr/bin/perl
#
# engines.cgi
# Written by: Michael A. Puscar
# - Display hierarchy information and allow users to choose search engines
#
# Need to populate: CORPUS, CLIENT_NAME, NODE_TITLE, RESULTS for template

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local (%cookies) = getCookies();

local ($cid);
local ($nid);
local ($cname);
local ($ntitle);

# Were username and password information even sent?
if ((!defined($query->param('nid'))) || 
    (!defined($query->param('cname'))) ||
    (!defined($query->param('cid')))) {
    #print "Location: /seditor/missing-info.html\n\n";
    #exit(1);

    # Temporary hack -- default to Merck
    $cid = 1; $nid = 90000; $cname = "Merck"; $ntitle = "Merck";
} 

if (defined($query->param('cid'))) { ($cid) = $query->param('cid'); }
if (defined($query->param('nid'))) { ($nid) = $query->param('nid'); }
local ($ntitle) = ""; local ($pid); local ($ptitle);

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:gaea", "sbooks", "racer9");

if ($nid == -1) { $pid = -1; $ntitle = $cname; $ptitle = "None"; } else {
$sth = $dbh->prepare("SELECT nodetitle, parentid from node where nodeid = $nid and corpusid = $cid") || die "Cannot execute node title selection: $!\n";
$sth->execute() || warn "Fatal database error: $!\n";
($ntitle, $pid) = $sth->fetchrow_array();
$sth->finish;

if ($ntitle eq "") { &print_error("Sorry, the topic you selected no longer exists."); exit(0); }

if ($pid == -1) { $ptitle = $cname; } else {
    $sth = $dbh->prepare("SELECT nodetitle from node where nodeid = $pid and corpusid = $cid") || die "Cannot execute node title selection: $!\n";
    $sth->execute() || warn "Fatal database error: $!\n";
    ($ptitle) = $sth->fetchrow_array();
    $sth->finish;
}}

print "Content-type: text/html\n\n";

########### PRINT THE HEADER
&print_head();

########### PRINT THE TOPIC INFORMATION
local ($loop) = 0;
$sth = $dbh->prepare("SELECT nodetitle, NodeId from Node where parentid = $nid and corpusid = $cid") || die "Cannot execute children selection: $!\n";
$sth->execute() || warn "Fatal database error: $!\n";
print "<table width=64% border=0>\n";
print "<tr><td><font size=-1><u>Current Topic</u>:</font></td> <td><font size=-1><b>$ntitle</b></font></td></tr>\n";
print "<tr><td><font size=-1><u>Parent Topic</u>:</font></td> <td><font size=-1><a href='engines.cgi?nid=$pid&cid=$cid&cname=$cname'>$ptitle</a></font></td></tr>\n";
print "<tr><td colspan=2><font size=-1><u>Sub Topics</u>:</font></td></tr></table><P>\n";
print "<center><table width=95%>";

########### PRINT THE SUB TOPIC INFORMATION
while (($child_title, $child_id) = $sth->fetchrow_array()) {
    $loop++; if ($loop == 1) { print "<tr>"; }
    if (length($child_title) > 20) { $child_title = substr($child_title, 0, 20)."..."; }
    print "<td><font size=-1><a href='engines.cgi?nid=$child_id&cid=$cid&cname=$cname'> $child_title</a></font></td>\n";
    if ($loop == 3) { $loop = 0; print "</tr>\n"; }
}
if ($sth->rows == 0) { print "<tr><td>There are no sub topics under $ntitle</td></tr>\n"; }
$sth->finish;
if ($loop != 0) { print "</tr>\n"; } print "</table></center><hr>\n";

# Group / engine selection is based upon inheritence.   First we must determine the
# node id to use for this function
local ($count) = 0; local ($nnid) = $nid;
while (($nnid != -1) & ($count == 0)) {
    $sth = $dbh->prepare("select count(*) from NodeGroup where NodeId = $nnid") || die "Cannot execute group selection: $!\n";
    $sth->execute() || die "Fatal database error: $!\n";
    $count = $sth->fetchrow_array(); $sth->finish;

    if ($count == 0) { 
	$sth = $dbh->prepare("select parentid from node where nodeid = $nnid") || die "Cannot select parent: $!\n";
	$sth->execute() || die "Fatal database error: $!\n";
	$nnid = $sth->fetchrow_array(); $sth->finish;
}}

########### PRINT OUT THE GROUP SELECTION TOOL
print <<EOF;
<script language="javascript">
function Remove() {
   var col = "OldStuff";
   var sl = document.fm[col].selectedIndex;

   // if (confirm("This will delete the selected group.")) {
     if (document.fm[col].options[sl].value!='none') {
         if (document.fm[col].length==1) {
            document.fm[col].options[0].text="";
            document.fm[col].options[0].value=".none";
         } else {
            document.fm[col].options[sl]=null; 
         } 
}} // }

function Add() {
   var o_col = "NewStuff";
   var d_col = "OldStuff";
   o_sl = document.fm[o_col].selectedIndex;
   d_sl = document.fm[d_col].length;

   if (o_sl != -1 && document.fm[o_col].options[o_sl].value > "") {
     oText = document.fm[o_col].options[o_sl].text;
     oValue = document.fm[o_col].options[o_sl].value;
     document.fm[o_col].options[o_sl] = null;
     document.fm[d_col].options[d_sl] = new Option (oText, oValue, false, true);

     if (document.fm[d_col].options[0].value == 'none') {
	 document.fm[d_col].options[0] = null; }
   } else {
     alert("Please select a category first");
   }
}

function About(col) {
  var ww = 350, wh = 350;
  var wPos = 0;
  var lPos = 0;

  var sl = document.fm[col].value;
  var url = "get-info.cgi?group="+sl;

  if(screen) {
	wPos = (screen.width - ww)/2;
	lPos = (screen.height - wh)/2;
  }
  if (!window.SEwin) {
	// Not defined
	SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  } else {
  // defined
  if (SEwin.closed) {
	// closed, open a new one
	SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  }}
  SEwin.focus();
}

function makeList(col) 
{
  val = "";
  for (j=0; j<document.fm[col].length; j++) {
    if (val > "") { val += ","; }
    if (document.fm[col].options[j].value > "") val += document.fm[col].options[j].value;
  } 
  return val;
}

function sub_layout() {
  document.fm["OldStuff_lst"].value = makeList("OldStuff"); 
  document.fm.submit();
}

</script>
<form name="fm" action="modify-node.cgi" method=GET>
<input type="hidden" name="cid" value="$cid">
<input type="hidden" name="cname" value="$cname">
<input type="hidden" name="nid" value="$nid">
<input type="hidden" name="OldStuff_lst" value="">
<input type="button" name="Save" value="Save Changes" onClick="javascript:sub_layout();">&nbsp; 
<input type="submit" name="Revert" value="Revert to Saved"><br>
<table border=0 cellpadding=2 cellspacing=1 width="90%">
<tr><td bgcolor="336666" align=center valign=top width="40%"><table border=0 cellspacing=0 cellpadding=2 width="100%">
<tr><td bgcolor=447777 align=center><font face="arial" size=-1 color="FFFFFF"><b>Available Groups</b></font></td></tr>
<tr><td align=center><table border=0 cellspacing=2 cellpadding=0><tr><td rowspan=2><select name="NewStuff" size=9>
EOF

# Select all available groups not currently used by this node
$sth = $dbh->prepare("select SearchCategoryId, Title from SearchCategory where parentid != -1 and SearchCategoryId not in (select groupid from nodegroup where nodeid = $nnid) order by Title asc") || die "Cannot execute group selection: $!\n";
$sth->execute() || warn "Fatal database error: $!\n";
while (my ($scid, $title) = $sth->fetchrow_array()) {
    print "<option value=$scid> $title\n";
} $sth->finish;
print <<EOF;
</select></td><td valign=top><a href="javascript:About('NewStuff');"><img src="icons/question.gif" border=0></a><br></td></tr><tr><td valign=bottom><br><spacer height=20><br></td></tr></table></td></tr></table></td><td bgcolor="f0f0ff" align=center valign=top width="40%">
<table border=0 cellspacing=0 cellpadding=2 width="100%">
<tr><td bgcolor=447777 align=center><font face="arial" size=-1 color="FFFFFF"><b>Current Groups</b></font></td></tr>
<tr><td align=center bgcolor=336666><table border=0 cellspacing=2 cellpadding=0><tr><td rowspan=2><select name="OldStuff" size=9>
EOF

# Select all available groups not currently used by this node but
#  only if the node id is not -1
if ($nid != -1) { 
    $sth = $dbh->prepare("select G.searchcategoryid, G.Title from SearchCategory G, NodeGroup N where N.nodeid = $nnid and N.GroupId = G.searchcategoryid order by Title asc") || die "Cannot execute group selection: $!\n";
    $sth->execute() || warn "Fatal database error: $!\n";
    while (my ($scid, $title) = $sth->fetchrow_array()) {
	print "<option value=$scid> $title\n";
    } if ($sth->rows == 0) { print "<option value='none'> No groups selected\n"; } $sth->finish;
} else {  print "<option value='none'> No groups selected\n"; }
print <<EOF;
</select></td><td valign=top><a href="javascript:About('OldStuff');"><img src="icons/question.gif" border=0></a><br></td></tr><tr><td valign=bottom><br><spacer height=20><br></td></tr></table></td></tr></table></td></tr><tr>
<td align=center bgcolor=eeeeee><table border=0 cellpadding=2 cellspacing=0><tr><td></td><td><font face=Arial size=2><b>Add Group</b></font></td><td><a href="javascript:Add();"><img src="/seditor/icons/rtc.gif" width=16 height=16 alt="Add Group" border=0></a></td></tr></table></td><td align=center bgcolor=eeeeee><table border=0 cellpadding=2 cellspacing=0><tr><td><a href="javascript:Remove();"><img type="image" src="/seditor/icons/ltc.gif" width=16 height=16 alt="Remove Group" border=0></a></td><td><font face=Arial size=2><b>Remove Group</b></font></td><td></td></tr></table></td></tr>
</table>
</form>
EOF
########### PRINT THE FOOTER
&print_foot();
$dbh->disconnect;

1;

