#!/usr/bin/perl
#
# edit-topic.cgi
# Written by: Michael A. Puscar
# - Display information about this topic to the editor
# - Allow for changes or edits to be made
#
# Need to populate: CORPUS, CLIENT_NAME, NODE_TITLE, RESULTS for template

use CGI;
use DBI;
require "IndraFunc.pm";

$ENV{'ORACLE_HOME'} = '/opt/oracle';

local ($query) = new CGI;
local (%cookies) = getCookies();
local ($results_per_page) = 100;

# Were username and password information even sent?
if ((!defined($query->param('hid'))) || 
    (!defined($query->param('clid'))) ||
    (!defined($query->param('clientname'))) ||
    (!defined($query->param('nid'))) ||
    (!defined($query->param('cid')))) {
    print "Location: /seditor/missing-info.html\n\n";
    exit(1);
}

local ($hid) = $query->param('hid');
local ($cid) = $query->param('cid');
local ($nid) = $query->param('nid');
local ($clid) = $query->param('clid');
local ($clientname) = $query->param('clientname');

# Only display 100 results per page.   If not specified, assume page 1.
local ($page);  if (defined($query->param('page'))) { $page = $query->param('page'); }
else { $page = 1; }

local ($ARGUMENTS) = "hid=$hid&cid=$cid&nid=$nid&clid=$clid&clientname=$clientname&page=$page";

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");

# If defined, then user requested deletion of a few urls
if (defined($query->param('submit'))) { 
    local (@kill_list);
    
    # Get targets for deletion
    for ($i = 1; $i <= $query->param('count'); $i++) {
	my ($id) = 'id'.$i;
	if (defined($query->param($id))) { push (@kill_list, $query->param($id)); }
    }
    
    # KILL targets
    foreach $kill (@kill_list) { 
	my ($q) = "INSERT INTO BlackList (ClientId, CorpusId, EditorId, NodeId, URLChunk, DateCreated) ";
	$q=$q. " VALUES ($clid, $cid, $cookies{'EDITORID'}, $nid, '$kill', SYSDATE)";
	#print "Content-type: text/html\n\n$q\n"; exit(1);
	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish; undef $q; undef $sth;

	my ($q) = "INSERT INTO EditorLog (EditorId, ActionCode, Action_String, NodeId, ActionDate) ";
	$q = $q."values (".$cookies{'EDITORID'}.", 5, 'Added black list word for node $nid', $nid, SYSDATE)";
	my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
	$sth->execute() || warn "Fatal database error: $!\n";
	$sth->finish; undef $q;
    }
}

# Load black list for this node
local (@words);
my ($sth) = &get_blacklist($cid, $clid, $nid);
while (my $word = $sth->fetchrow_array) { push (@words, $word); }
$sth->finish; undef $sth;

# Get the editor's name from cookie and store in NAME
local ($NAME) = $cookies{'FNAME'};
$NAME =~ s/\%20/\ /gi;

# Store the node title in NODE_TITLE
$q = "SELECT NodeTitle FROM Node WHERE NodeId = $nid";
my $sth = $dbh->prepare($q) || warn "Fatal database error: $!\n";
$sth->execute() || warn "Fatal database error: $!\n";
local ($NODE_TITLE) = $sth->fetchrow_array;
$sth->finish;

# Finally, select and build the result list for this node.
# Populate: ##RESULTS## ##START## ##FINISH##
$sth = $dbh->prepare("SELECT d.doctitle, d.docurl, d.documentsummary, nd.documenttype from document d, nodedocument nd WHERE nd.nodeid = $nid and nd.documentid = d.documentid and nd.score1 > 0 order by score1 DESC, score2 DESC, score3 DESC") || die "Cannot execute document selection: $!\n";
$sth->execute() || warn "Fatal database error: $!\n";

$RESULTS = "<form method=get action='edit-topic.cgi'><OL>\n"; local ($loop) = 0;
$RESULTS = $RESULTS . "<input type='submit' name='submit' value='Delete Checked Items'><br>&nbsp;";
while (my ($title, $url, $summary, $type) = $sth->fetchrow_array) {
    if (&url_contain_blacklist($url) == 0) {
	$loop++;
	if ($type == 2) { $RESULTS = $RESULTS . "<LI> <img src='/seditor/icons/reviewed.gif'> "; }
	else { $RESULTS = $RESULTS . "<LI> "; }
	$RESULTS = $RESULTS . "<input type='checkbox' name='id$loop' value='$url'> &nbsp; <a href='$url' target='website'>$title</a> <br>$summary<br>&nbsp;\n";
    }}
$RESULTS = $RESULTS . "<input type='hidden' name='count' value='$loop'>";
$RESULTS = $RESULTS . "<input type='hidden' name='hid' value=$hid>";
$RESULTS = $RESULTS . "<input type='hidden' name='cid' value=$cid>";
$RESULTS = $RESULTS . "<input type='hidden' name='nid' value=$nid>";
$RESULTS = $RESULTS . "<input type='hidden' name='clid' value=$clid>";
$RESULTS = $RESULTS . "<input type='hidden' name='clientname' value='$clientname'>";
$RESULTS = $RESULTS . "<input type='hidden' name='page' value=$page>\n";
$RESULTS = $RESULTS . "</OL>\n";
$RESULTS = $RESULTS . "<input type='submit' name='submit' value='Delete Checked Items'>";

$sth->finish;
$dbh->disconnect;

print "Content-type: text/html\n\n";
&print_template("edit-topic.tpl");

sub url_contain_blacklist {
    my ($url) = @_;

    foreach $word (@words) { if ($url =~ /$word/i) {return 1; }}

    return 0;
}

sub get_blacklist {
   my ($corpus, $client, $nodeid) = @_;
   my $sth = $dbh->prepare("SELECT URLChunk FROM BlackList WHERE ClientId = $client and CorpusId = $corpus and (NodeId = $nodeid or NodeId IS NULL)");
   $sth->execute();
   return $sth;
}

1;

