REM Usage: JARFILE -username user -password pass -api api
REM   Optional parameters:
REM   -build true (build index(es) from scratch)
REM   -index index (rebuild a specific index only)
REM   -table table (required when using the BUILD and INDEX argument)
REM   -field field (required when using the BUILD and INDEX argument)

java -cp APIshell.jar com.iw.apishell.RebuildIndexes -username <username> -password <pass> -api 127.0.0.1
