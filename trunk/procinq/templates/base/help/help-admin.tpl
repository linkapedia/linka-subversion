<blockquote>
  <TABLE cellPadding=2 width="700" border=0>
    <TBODY>
    <TR>
      <TD vAlign=top width="50%">
      <p><b>Administration Help:</b></p>
      <p>This screen is the administration section of the Taxonomy Server.  From the administration screen you
      may change properties of the server. You may also perform system maintenance functions here like purging deleted
      objects, reconfiguring the Receiver Operating Characteristics of taxonomies and classifying individual
      documents.</p>
        </TD>
    </TR>
    </TBODY>
  </TABLE>
</blockquote>