<script>
function EditResult () {
  var ww = 450, wh = 310;
  var wPos = 0;
  var lPos = 0;
  var url = "/servlet/Main?template=Taxonomy.EditResult&NodeID=##NODEID##&Score1=125.0&DocTitle=Untitled+Document&GenreID=##GENREID##&DocumentID=##DOCUMENTID##";

  if(screen) {
  wPos = (screen.width - ww)/2;
  lPos = (screen.height - wh)/2;
  }
  if (!window.SEwin) {
  // Not defined
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  } else {
  // defined
  if (SEwin.closed) {
  // closed, open a new one
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  }}
  SEwin.focus();
}
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
function MM_setTextOfLayer(objName,x,newText) { //v3.0
  if ((obj=MM_findObj(objName))!=null) with (obj)
    if (navigator.appName=='Netscape') {document.write(unescape(newText)); document.close();}
    else innerHTML = unescape(newText);
}
function openSaveSearch(CQL) {
   var sUrl = "/servlet/Main?template=Taxonomy.AddAlert&CQL=";
   if (navigator.appName == 'Netscape') { sUrl = sUrl + CQL; }
   else { sUrl = sUrl + escape(CQL); }
   myWin = window.open( sUrl, "IDRACSaveSearch", "menubar=no,personalbar=no,resizable=yes,scrollbars=no,width=650,height=180" );
   myWin.moveTo(0,0);
   myWin.focus();
}
</script>

<DIV align=center>
</DIV><BR><FONT face="Arial, Helvetica, sans-serif">
&nbsp;&nbsp;
##CATEGORY_HIERARCHY##
</FONT>
<DIV align=left>
<TABLE border=0 width=760 cellspacing=0 cellpadding=0>
<TR bgColor=#333399 borderColor=#996600>
<TD align=left height=23><FONT face="Arial, Helvetica, sans-serif" color=white>
<B>&nbsp;&nbsp; ##NODETITLE##</TD>
<TD width=50>
<a href="/servlet/Main?template=Taxonomy.ShowCorpusHierarchy&submit=true&CorpusID=##CORPUSID##&NodeID=##NODEID##">
<img src="/servlet/images/explore.gif" border=0></a> &nbsp;
<a href="javascript:openSaveSearch('##QUERY##');">
<img src="/servlet/images/save.gif" alt="Save As An Alert" border=0></a>
</td>
<td align=right>
<A class="genrewb" HREF="/servlet/Main?template=Taxonomy.FrontPage">Home</a> |
<A class="genrewb" HREF="/servlet/Main?template=Server.Logout">Logout</a> |
<A class="genrewb" HREF="/servlet/Main?template=Taxonomy.EditUser">Prefs</a> |
<A class="genrewb" HREF="/servlet/Main?template=Taxonomy.Administer">Administer</a> |
<A class="genrewb" HREF="javascript:EditResult();">Add Result</a> |
<A class="genrewb" HREF="/servlet/Main?template=ROC.Chooser&NodeID=##NODEID##&NodeTitle=##NODETITLE##">ROC Options</a> |
<!-- A class="genrewb" HREF="/servlet/Main?template=Taxonomy.AdminBrowse&NodeID=##NODEID##">Edit Topic</a> | -->
<A class="genrewb" HREF="/servlet/Main?template=Taxonomy.Help&helptext=help-home">Help</a>
&nbsp; &nbsp;
</B></FONT></TD></TR></TBODY>
</TABLE>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=700>
  <TR>
    <TD height=60>
<FORM action="/servlet/Main" method=get><FONT face="Arial, Helvetica, sans-serif" size=-1>
<INPUT name=restrict type=hidden> <INPUT name=exclude
      type=hidden>&nbsp;&nbsp;&nbsp; <B>Search Taxonomies: </B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<INPUT NAME="template" TYPE=HIDDEN Value="Taxonomy.Search">
<SELECT name="CorpusID">
<OPTION selected value=0>All Taxonomies
##OPTION_CORPUS_LIST##
</SELECT> </FONT> &nbsp; &nbsp; <INPUT maxLength=100
  name=Keywords size=24>
      &nbsp; <INPUT name=Search type=submit value=Go>
<FONT face="Arial, Helvetica, sans-serif" size=-1>&nbsp;
<a href="/servlet/Main?template=Taxonomy.Search&advanced=true"><b>Advanced Search</b></a></FONT>
<br>
</TD></TR>
</TABLE>

<div id="documents">
</div>
<br>
