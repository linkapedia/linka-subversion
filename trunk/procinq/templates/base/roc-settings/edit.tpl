<SCRIPT>
var myWin;
function EditCW(theURL,winName,features) { //v2.0
  if (myWin && (!myWin.closed))
    myWin.focus();
  else
  {
    myWin = window.open(theURL,winName,features);
    myWin.moveTo(0,0);
  }
}
</SCRIPT>

<blockquote>
<b><u>Edit Local Classifier</u>:</b>
<br>
<form action=/servlet/Main>
<input type="hidden" name="template" value="Taxonomy.CreateLocalClassifier">
<input type="hidden" name="RID" value="##ROCSETTINGID##">
##EDIT##
<table width=70%>
<tr><td>Classifier: </td>
    <td><select name="classifierid">
	##CLASSIFIERS##
	</select>
    </td>
</tr>
<tr><td>Tier Level: </td>
    <td><select name="star">
	##STARS##
	</select>
    </td>
</tr>
<tr><td>Cost Ratio: </td><td><input type="text" name="cost" value="##COSTRATIOFPTOFN##"></td></tr>
<tr><td>Cutoff Score: </td><td><input type-"text" name="cutoff" value="##CUTOFFSCOREGTE##"></td></tr>
<tr><td colspan=2> &nbsp; </td></tr>
<tr><td colspan=2> <input type="submit" name="submit" value="Submit"> </td></tr>
</table>
<p>
</form>