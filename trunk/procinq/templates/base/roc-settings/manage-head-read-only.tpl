<!-- manage-head.tpl -->
<blockquote>
<b>ROC Settings</b><br>
(<i><u>Note</u>: This taxonomy is using the default ROC settings.  If you wish to change
these settings, <a href="/servlet/Main?template=Taxonomy.EditROCSettings&action=new&cid=##CID##">
click here</a></i>).
<p>
<form name="roc" method="post" action="/servlet/Main">
<input type="hidden" name="template" value="Taxonomy.EditROCSettings">
<input type="hidden" name="rocid" value="##ROCSETTINGID##">

<b>True Negatives to True Positives (Ratio)</b>: &nbsp; ##RATIOTNTOTP##
<p>

