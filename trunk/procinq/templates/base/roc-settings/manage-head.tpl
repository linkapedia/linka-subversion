<!-- manage-head.tpl -->
<blockquote>
<b>ROC Settings</b><p>
<form name="roc" method="post" action="/servlet/Main">
<input type="hidden" name="template" value="Taxonomy.EditROCSettings">
<input type="hidden" name="rocid" value="##ROCSETTINGID##">

<SCRIPT>
function EditRatio (ratio) {
   var sUrl = "/servlet/Main?template=Taxonomy.SaveRatio&RID=##ROCSETTINGID##&ratio="+ratio;
   myWin = window.open( sUrl, "EditRatio", "menubar=no,personalbar=no,resizable=yes,scrollbars=no,width=650,height=180" );
   myWin.moveTo(0,0);
   myWin.focus();
}
</SCRIPT>

<b>True Negatives to True Positives (Ratio)</b>: &nbsp;
<input type="text" name="ratio" value="##RATIOTNTOTP##">
<a href="javascript:EditRatio(document.roc.ratio.value);"><img src="/servlet/images/save.gif" border=0></a>
<p>

