</SELECT></td><td valign=center>
<table><tr>
<td><a href="javascript:Up()"><img src="/servlet/images/up.gif" alt="Move subtopic up in display order" border=0></a></td>
<td>&nbsp; Click the UP arrow to move a sub topic forward in display order.</td></tr>
<tr><td><a href="javascript:Down()"><img src="/servlet/images/down.gif" alt="Move subtopic down in display order" border=0></a></td>
<td>&nbsp; Click the DOWN arrow to move a sub topic down in display order.</td></tr>
<tr><td><a href="javascript:Move()"><img src="/servlet/images/move.jpg" alt="Move topic underneath different parents" border=0></a></td>
<td>&nbsp; Click the MOVE icon to move topics underneath different parents.</td></tr>
<tr><td><a href="javascript:Add()"><img src="/servlet/images/add.jpg" alt="Create a new sub topic" border=0></a></td>
<td>&nbsp; Click the ADD icon to create a new sub topic.</td></tr>
<tr><td><a href="javascript:Explore()"><img src="/servlet/images/explore.gif" alt="Explore a sub topic" border=0></a></td>
<td>&nbsp; Click the EXPLORE icon to browse to a child topic.</td></tr>
<tr><td><a href="javascript:Save()"><img src="/servlet/images/save.gif" alt="Save changes" border=0></a></td>
<td>&nbsp; Click the SAVE icon to save your changes to this topic.</td></tr>
<tr><td><a href='javascript:Remove()'><img src="/servlet/images/delete.gif" alt="Delete topic" border=0></a></td>
<td>&nbsp; Click the DELETE icon to delete a sub topic of this topic.</td></tr></table>
</td></tr></table>
</td></tr></table>
</form>
</blockquote>
