<SCRIPT>
function EditResult##DOCUMENTID## () {
  var ww = 450, wh = 310;
  var wPos = 0;
  var lPos = 0;
  var url = "/servlet/Main?template=Taxonomy.EditResult&NodeID=##NODEID##&DocURL=##EDOCURL##&DocTitle=##EDOCTITLE##&GenreID=##GENREID##&Score1=##SCORE1##&DocSummary=##EDOCSUM##&DocumentID=##DOCUMENTID##";

  if(screen) {
  wPos = (screen.width - ww)/2;
  lPos = (screen.height - wh)/2;
  }
  if (!window.SEwin) {
  // Not defined
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  } else {
  // defined
  if (SEwin.closed) {
  // closed, open a new one
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  }}
  SEwin.focus();
}

function DeleteResult##DOCUMENTID## () {
   if (confirm("Deleted results may not be recoverable.   Delete this document?")) {
      window.location = "/servlet/Main?template=Taxonomy.DeleteResult&NodeID=##NODEID##&DocumentID=##DOCUMENTID##";
   }
}

</SCRIPT>

<TABLE width=740 cellspacing=0>
<TR><TD valign=top width=45>&nbsp; &nbsp; &nbsp; <FONT face="Arial, Helvetica" size=-1><strong>##LOOPI##.</strong></FONT></TD>
<TD><FONT face="Arial, Helvetica" size=-1>
<STRONG><DL><DT width=5><A href="##DOCURL##"> ##DOCTITLE##</A></STRONG></FONT></TD>
<TD align=right width=115 valign=middle>&nbsp; <FONT face="Arial, Helvetica" size=-2>  ##STARS## <img src="/servlet/images/bar.gif" alt=""> &nbsp;
<a href="javascript:EditResult##DOCUMENTID##();"><img src="/servlet/images/edit.gif" alt="Edit Result" border=0></a>
<a href="javascript:DeleteResult##DOCUMENTID##();"><img src="/servlet/images/delete2.gif" alt="Delete Result" border=0></a>
<!-- Score1: ##SCORE1## -->
      </NOBR> </FONT></TD></TR>
<TR>
    <TD valign=top>&nbsp; </TD><TD>
      <DL>
        <DT width=5><FONT face="Arial, Helvetica" size=-1>
	##DOCSUMMARY##
	<BR></FONT>
	<A href="##DOCURL##">
	##PDOCURL##</A> (See <a href="/servlet/Main?template=Taxonomy.GetRelatedNodes&Type=##TYPE##&DocumentID=##DOCUMENTID##&DocURL=##EDOCURL##&DocTitle=##EDOCTITLE##">related topics</a>
##NUGGETS##) &nbsp; <BR>
</FONT></DD></DL>
    </TD></TR>
</TABLE>