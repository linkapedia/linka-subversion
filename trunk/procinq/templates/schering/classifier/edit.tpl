<SCRIPT>
var myWin;
function EditCW(theURL,winName,features) { //v2.0
  if (myWin && (!myWin.closed))
    myWin.focus();
  else
  {
    myWin = window.open(theURL,winName,features);
    myWin.moveTo(0,0);
  }
}
</SCRIPT>

<blockquote>
<b><u>Edit Classifier</u>: ##CLASSIFIERNAME##</b>
<br>
<form action=/servlet/Main>
<input type="hidden" name="template" value="Taxonomy.CreateClassifier">
##HIDECLASS##
<table width=70%>
<tr><td>Name: </td><td><input type="text" name="Name" value="##CLAsSIFIERNAME##" size=60 maxlength=20></td></tr>
<tr><td>ClassPath: </td><td><input type="text" name="Path" value="##CLASSPATH##" size=60 maxlength=255></select></td></tr>
<tr><td>DB Column: </td><td>##COLNAME##</td></tr>
<tr><td>Score Sort: </td>
    <td><select name="Sort">
	   <option ##SORTA## value="0"> Ascending
	   <option ##SORTB## value="1"> Descending
	</select>
    </td>
</tr>
<tr><td>Status: </td>
    <td><select name="Active">
	   <option ##SORTD## value="1"> Active
	   <option ##SORTC## value="0"> Inactive
	</select>
    </td>
</tr>
<tr><td colspan=2> &nbsp; </td></tr>
<tr><td colspan=2> <input type="submit" name="submit" value="Submit"> </td></tr>
</table>
<p>
</form>