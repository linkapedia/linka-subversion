<script>
function openSaveSearch(CQL) {
   if (CQL.indexOf("<NODE>") != -1) { alert('Sorry, you may only set alerts on queries that retrieve documents.'); return; }
   var sUrl = "/servlet/Main?template=Taxonomy.AddAlert&CQL=";
   if (navigator.appName == 'Netscape') { sUrl = sUrl + CQL; }
   else { sUrl = sUrl + escape(CQL); }
   myWin = window.open( sUrl, "IDRACSaveSearch", "menubar=no,personalbar=no,resizable=yes,scrollbars=no,width=650,height=180" );
   myWin.moveTo(0,0);
   myWin.focus();
}
function exportResults() {
   window.location = '/servlet/Main?template=Taxonomy.ViewPrintableResults&CQL=##CQLU##';
}
</script>
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
  	<td width="50"><img src="/servlet/images/idrac/s.gif" width=50 height=50></td>
    <td width="50"><img src="/servlet/images/idrac/s.gif" width=50 height=50></td>
  </tr>
  <tr valign="top">
    <td width="50">&nbsp;</td>
    <td width="660" align="left"> <b>CQL</b>: <i>##QUERY##</i> &nbsp;
    <a href="javascript:openSaveSearch('##ESCAPED##');">
    <img src="/servlet/images/save.gif" alt="Save As An Alert" border=0></a> &nbsp;
    <a href="javascript:exportResults();">
    <img src="/servlet/images/schering/export.gif" alt="Export Results" border=0></a> &nbsp; <BR>
	<FONT face="Arial, Helvetica, sans-serif" size=-1><b>Sort Order:</b></font> &nbsp; 
	<select name="sortorder" onChange="window.location = '/servlet/Main?template=Taxonomy.CQL&query=##CQLU##&sort='+this.selectedIndex;">
	##SORDER##
	</select>
<p>