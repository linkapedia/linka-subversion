<SCRIPT>
function Popup(WordID, ThesaurusID) {
  var ww = 575, wh = 385;
  var wPos = 0;
  var lPos = 0;

  var url = "/servlet/Main?template=Taxonomy.GetWordProps&WordID="+WordID+"&ThesaurusID="+ThesaurusID;

  if(screen) {
  wPos = (screen.width - ww)/2;
  lPos = (screen.height - wh)/2;
  }
  if (!window.SEwin) {
  // Not defined
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=1,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  } else {
  // defined
  if (SEwin.closed) {
  // closed, open a new one
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=1,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  }}
  SEwin.focus();
}
</SCRIPT>
<blockquote>
<table width=720><tr><td>
<b><u>Instructions</u></b>: Listed below are each of the anchor terms in this thesaurus.   To view relationships, click on the specific anchor word.  To remove an anchor word from this thesaurus, click on the <i>delete</i> icon preceding the word.   To add a new anchor term, click <a href="javascript:Popup(0, ##THESAURUSID##)">here</a>.
</td></tr></table>
<P>
&nbsp; &nbsp; &nbsp; &nbsp; 
<a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=A">A</a> &nbsp; 
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=B">B</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=C">C</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=D">D</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=E">E</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=F">F</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=G">G</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=H">H</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=I">I</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=J">J</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=K">K</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=L">L</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=M">M</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=N">N</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=O">O</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=P">P</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=Q">Q</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=R">R</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=S">S</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=T">T</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=U">U</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=V">V</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=W">W</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=X">X</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=Y">Y</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=Z">Z</a> &nbsp;
 <a href="/servlet/Main?template=Taxonomy.ThesaurusBrowse&ThesaurusID=##THESAURUSID##&L=Other">Others</a> 
<P>

<SCRIPT>
function Remove(ThesaurusID, AnchorID) {
     if (confirm("You are about to delete this thesaurus term and all of it's relationships, continue?")) {
         window.location = "/servlet/Main?template=Taxonomy.ThesaurusBrowse&L=##LIKE##&ThesaurusID="+ThesaurusID+"&WordID="+AnchorID;
    }
}
</SCRIPT>
<table width=750>
