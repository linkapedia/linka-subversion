<script>
function openSaveSearch(CQL) {
   if (CQL.indexOf("<NODE>") != -1) { alert('Sorry, you may only set alerts on queries that retrieve documents.'); return; }
   var sUrl = "/servlet/Main?template=Taxonomy.AddAlert&CQL=";
   if (navigator.appName == 'Netscape') { sUrl = sUrl + CQL; }
   else { sUrl = sUrl + escape(CQL); }
   myWin = window.open( sUrl, "IDRACSaveSearch", "menubar=no,personalbar=no,resizable=yes,scrollbars=no,width=650,height=180" );
   myWin.moveTo(0,0);
   myWin.focus();
}
</script>
<p>
<blockquote>
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
    <td width="50">&nbsp;</td>
    <td width="660" align="left"> <b>##QUERY##</b> &nbsp;
    <a href="javascript:openSaveSearch('##ESCAPED##');">
    <img src="/servlet/images/save.gif" alt="Save As An Alert" border=0></a>
<p>