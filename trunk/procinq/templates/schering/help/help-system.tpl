<BLOCKQUOTE>
    <TABLE cellPadding=2 width="78%" border=0>
      <TBODY>
      <TR>
        <TD vAlign=top width="50%">
          <P><b>System Parameters Help:</b></P>
          <P>This screen allows you to change system parameters. You will need to restart the server before the parameters take affect.</P>

<style><!--
.Normal
	{font-size:12.0pt;
	font-family:"Times New Roman";}
-->
</style>
</head>
<table border=1 cellspacing=0 cellpadding=0>
  <thead>
  <tr>
    <td width=247 valign=top bgcolor="#FFFFCA" class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>Attribute Name</span></p>
    </td>
    <td width=169 valign=top bgcolor="#FFFFCA" class="Normal">
      <p>Description</p>
    </td>
    <td width=174 valign=top bgcolor="#FFFFCA" class="Normal">
      <p>Recommended Setting</p>
    </td>
  </tr>
  </thead>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>DC:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>The context of the LDAP instance you are using for authentication.  </p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>In the Intellisophic domain, this is dc=Intellisophic,dc=com</p>
    </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapHost:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>The IP address of the LDAP instance you are using for security in the
        system</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapPort</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>The port id of the LDAP server you are using for authentication on the
        server.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>389</p>
    </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapUsername:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>The username of the LDAP user the ITS server is logging in as.  This
        needs to be a user with the ability to change attributes in the LDAP domain.</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>LdapPassword</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>The password for the LDAP administrator.  This is used by the taxonomy
        server for changes to the LDAP database</p>
    </td>
    <td width=174 valign=top class="Normal">&nbsp; </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>AdminGroup</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>The context of the Admin group in the LDAP domain.  Members of this group
        will have admin privledges inside of the taxonomy server.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>cn=Administrators</p>
    </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>NodeCacheCountTarget:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>This number represents the optimal number of topics to be cached in memory at any given time.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>100000</p>
    </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>AffinityNodeTitlesOnly:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>Add topic titles to the list of concept signatures for a given topic.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>true</p>
    </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>NumMSForDocIndexConsideredLong:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>Documents taking longer than this amount of time (in milliseconds) to parse are noted in the logs.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>35000</p>
    </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>StopListFile:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>Indicates the location of the current stop word file for generation of concept signatures.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>/commonwords.txt</p>
    </td>
  </tr>
  <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>NodeCacheCountMax:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>This number represents the maximum amount of topics to keep in cached in memory for classifications.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>120000</p>
    </td>
  </tr>
    <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>OracleBinFolder:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>This is the location of the $ORACLEBIN folder.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>/home/oracle/product/10.1.0/db_2/bin</p>
    </td>
  </tr>
    <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>proxyHost:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>If using an HTTP proxy, specify the IP address of the proxy server.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>none</p>
    </td>
  </tr>
      <tr>
    <td width=247 valign=top class="Normal">
      <p><span style='font-size:8.0pt;font-family:Arial'>proxyPort:</span></p>
    </td>
    <td width=169 valign=top class="Normal">
      <p>If using an HTTP proxy, specify the port number of the proxy server.</p>
    </td>
    <td width=174 valign=top class="Normal">
      <p>none</p>
    </td>
  </tr>
  </table>
          <p></p>
          <p></p>
          <p></p>
          <p>&nbsp;</p>
          </TD>
      </TR>
      <TR>
        <TD vAlign=top width="50%">&nbsp;</TD>
      </TR>
      </TBODY>
    </TABLE>
  </BLOCKQUOTE>
