<BLOCKQUOTE>
    <TABLE cellPadding=2 width="78%" border=0>
      <TBODY>
      <TR>
        <TD vAlign=top width="50%">
          <P><B>Taxonomy Help:</B></P>
          <P>This screen allows you to manipulate the taxonomies that are in the
            system. The screen shows the available taxonomies that are in the system.
            From this screen you may perform the following functions:</P>
          <P><b>Browse a selected taxonomy:</b></P>
          <P>By selecting a taxonomy in the list and hitting browse, you will be
            taken into a screen that shows you the node hierarchy of the taxonomy.
            From here you may edit, add and delete nodes from the system as well
            as manipulate concept sinatures.</P>
          </TD>
      </TR>
      <TR>
        <TD vAlign=top width="50%">&nbsp;</TD>
      </TR>
      </TBODY>
    </TABLE>
  </BLOCKQUOTE>
