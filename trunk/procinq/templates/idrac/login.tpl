
        <TR>
          <TD width=65></TD>
          <TD width=654><IMG height=43 
            alt="IDRAC is the world's leading pharmaceutical regulatory database" 
            src="/images/idrac/idrac_subscriber_button.jpe" width=225 border=0> 
            <P><FONT face=Verdana size=2>Welcome to our new log-in page. <BR>To 
            <B>enter the IDRAC database</B>, log in here:</FONT></P></TD>
          <TD vAlign=top align=left width=258 rowSpan=3>
            <DIV align=center>
            <TABLE height=48 width=183 border=1>
              <TBODY>
              <TR>
                <TD width=167 bgColor=#003060 height=13>
                  <P align=center><FONT 
                  face="Verdana, Arial, Helvetica, sans-serif" color=#ffffff 
                  size=1><B>What's New</B></FONT> </P></TD></TR>
              <TR>
                <TD vAlign=top align=left width=167 bgColor=#d8dcd8 
                  height=86><FONT face="Verdana, Arial, Helvetica, sans-serif" 
                  size=1>Have you heard about the <B>AdComm Bulletin</B>? Our 
                  latest information service, the AdComm Bulletin covers 
                  critical FDA Advisory Committee meetings and consists of a 
                  concise report e-mailed to you merely hours after a meeting 
                  ends. Please e-mail us if you are interested in more details: 
                  <A 
                  href="mailto:adcommbulletin@liquent.com">adcommbulletin@liquent.com</A>.</FONT></TD></TR>
              <TR>
                <TD vAlign=top align=left width=167 bgColor=#003060 height=23>
                  <P align=center><FONT 
                  face="Verdana, Arial, Helvetica, sans-serif" color=#ffffff 
                  size=1><B>Technical Support</B></FONT> </P></TD></TR>
              <TR>
                <TD vAlign=top align=left width=167 bgColor=#d8dcd8 
                  height=86><FONT face="Verdana, Arial, Helvetica, sans-serif" 
                  size=1>Europe/Asia: +33 1 53 06 20 20<BR>(fax): +33 1 53 06 20 
                  10</FONT> 
                  <P><FONT face="Verdana, Arial, Helvetica, sans-serif" 
                  size=1>Americas: +1 866 437 7787 / +1 215 619 6400<BR>(fax): 
                  +1 215 654 7724</FONT></P>
                  <P><FONT face="Verdana, Arial, Helvetica, sans-serif" 
                  size=1><A 
                  href="mailto:idrac-helpdesk@liquent.com">idrac-helpdesk@liquent.com</A></FONT></P>
                  <P><FONT face="Verdana, Arial, Helvetica, sans-serif" 
                  size=1>You can send a question or comment in Japanese to our 
                  special Japan contact:<BR><A 
                  href="mailto:idrac-info.jp@liquent.com">idrac-info.jp@liquent.com</A> 
                  </FONT></P></TD></TR></TBODY></TABLE></DIV></TD></TR>
        <TR>
          <TD width=65></TD>
          <TD width=654></TD></TR>
        <TR>
          <TD width=65></TD>
          <TD width=654>
		<FORM NAME="Login" ACTION="/servlet/Main" METHOD="POST">
		<input type="hidden" name="Page" value="Idrac.FrontPage">
            <TABLE cellSpacing=0 cellPadding=5 width=315 bgColor=#ffffff 
            border=0>
              <TBODY>
              <TR>
                <TD width=303 bgColor=#d8dcd8><B><FONT face=Verdana 
                  size=2>IDRAC user ID</FONT></B></TD></TR>
              <TR>
                <TD width=303 bgColor=#d8dcd8><FONT 
                  face="Times New Roman, Times, serif"><B><INPUT size=12 
                  name="USERID"></B></FONT></TD></TR>
              <TR>
                <TD width=303 bgColor=#d8dcd8><B><FONT face=Verdana 
                  size=2>IDRAC password</FONT></B></TD></TR>
              <TR>
                <TD width=303 bgColor=#d8dcd8><FONT 
                  face="Times New Roman, Times, serif"><INPUT type=password 
                  size=12 name="PASSWORD"></FONT></TD></TR>
              <TR>
                <TD width=303 bgColor=#d8dcd8><FONT 
                  face="Times New Roman, Times, serif"><INPUT type=submit value=" OK " name="button OK"> 
<INPUT type=reset value=Clear name=Clear> 
            </FONT></TD></TR></TBODY></TABLE></FORM></TD></TR>
        <TR>
          <TD width=65></TD>
          <TD width=654></TD></TR>
        <TR>
          <TD width=65></TD>
          <TD width=912 colSpan=2>
            <HR>
          </TD></TR>
        <TR>
          <TD width=65></TD>
          <TD width=654><A 
            href="http://www.liquent.com/regintelligence_overview.asp"><IMG 
            height=43 
            alt="IDRAC is the world's leading pharmaceutical regulatory database" 
            src="/images/idrac/idrac_notsubscribed_button.jpe" width=225 border=0> 
            </A>
            <P><FONT face=Verdana size=2>Learn how the IDRAC database can 
            fulfill all your regulatory information needs...<A 
            href="http://www.liquent.com/regintelligence_overview.asp"><B>click 
            here for details</B></A>.</FONT></P></TD>
          <TD width=258></TD></TR>
        <TR>
          <TD width=65></TD>
          <TD width=912 colSpan=2>
            <HR>
          </TD></TR>
        <TR>
          <TD width=65></TD>
          <TD width=912 colSpan=2>
            <P><FONT face=Verdana size=1><I>Copyright �2001, 2002 <A 
            href="http://www.liquent.com/">Liquent, Inc.</A> An <A 
            href="http://www.informationholdings.com/" target=_blank>Information 
            Holdings Inc.</A> Company. All Rights 
        Reserved</I></FONT></P></TD></TR></TBODY></TABLE>
      <P><BR>
      <P>
      <P><BR></P></TD></TR></TBODY></TABLE></BODY></HTML>