<html>
<head>
<title>Keyword Index</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="/images/idrac/idrac.css" type="text/css">
<script language="JavaScript">
<!--
function NextPage() {
  window.document.permutindex.start.value = 
	##START##;

  var sl = window.opener.document.forme['country'].selectedIndex;
  var sText = window.opener.document.forme['country'].options[sl].text;
  var sValue = window.opener.document.forme['country'].options[sl].value;

  window.document.permutindex.country.value = sValue; // window.opener.document.forme.country.value;
  window.document.permutindex.countrytext.value = sText; // window.opener.document.forme.country.selectedValue;
  window.document.permutindex.NEXT.value = "1";
  window.document.permutindex.submit();
}
function PrevPage() {
  window.document.permutindex.start.value = 
	##START##;

  var sl = window.opener.document.forme['country'].selectedIndex;
  var sText = window.opener.document.forme['country'].options[sl].text;
  var sValue = window.opener.document.forme['country'].options[sl].value;

  window.document.permutindex.country.value = sValue; // window.opener.document.forme.country.value;
  window.document.permutindex.countrytext.value = sText; // window.opener.document.forme.country.selectedValue;
  window.document.permutindex.PREV.value = "1";
  window.document.permutindex.submit();
}
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  if (!window.opener.document.forme) errors += '- IDRAC Search window was closed!';
  else {
    var sl = window.opener.document.forme['country'].selectedIndex;
    var sText = window.opener.document.forme['country'].options[sl].text;
    var sValue = window.opener.document.forme['country'].options[sl].value;

    window.document.permutindex.country.value = sValue; // window.opener.document.forme.country.value;
    window.document.permutindex.countrytext.value = sText; // window.opener.document.forme.country.selectedValue;
  }
  if ((!window.document.permutindex.K0.checked) && (!window.document.permutindex.K1.checked)
    && (!window.document.permutindex.K2.checked) && (!window.document.permutindex.K3.checked)
    && (!window.document.permutindex.K4.checked)) errors+='- Keyword type is required.\n';
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if (nm=="S") nm="Search For"; if (nm=="R") nm="Result per page"; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') {
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (val<min || max<val) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  }
  if (errors) alert('The following error(s) occurred:\n'+errors);
  else window.document.permutindex.B.value="0";
  document.MM_returnValue = (errors == '');
}
function AddKeyword(keyword) {
  var operator, neg;
  operator=""; neg="";
  if (!window.opener.document.forme){
     alert("IDRAC Search window was closed!");
     return;}
  if (window.document.addKeywordForm.opR[0].checked) operator=" AND ";
  if (window.document.addKeywordForm.opR[1].checked) operator=" OR ";
  if (window.document.addKeywordForm.notOp.checked) neg="NOT ";

  if (window.opener.document.forme.keywords.value=="")
	window.opener.document.forme.keywords.value+=neg+"\""+keyword+"\"";
  else
	window.opener.document.forme.keywords.value+=operator+neg+"\"" + keyword + "\"";

return;
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="/images/idrac/kwdIdxBackground.gif">
<table width="610" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#003060">
    <td width="300"><img src="/images/idrac/kwdIdxTitle.gif" width="300" height="40"></td>
    <td width="310" bgcolor="#003060"><img src="/images/idrac/spacer.gif" width="310" height="40"></td>
  </tr>
</table>
<table width="610" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="200" bgcolor="#E6E6E6"><img src="/images/idrac/spacer.gif" width="200" height="5"></td>
    <td width="410"><img src="/images/idrac/spacer.gif" width="410" height="5"></td>
  </tr>
  <tr valign="top"> 
    <td width="200" bgcolor="#E6E6E6"> 
      <table width="200" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5"><img src="/images/idrac/spacer.gif" width="5" height="5"></td>
          <td width="190"> 
            <form name="permutindex" method="get" action="/servlet/Main" onSubmit="MM_validateForm('S','','R','R','','RinRange1:50');return document.MM_returnValue">
	    <input type="hidden" name="template" value="Idrac.KeywordBuilder">
		<input type="hidden" name="NEXT" value="0">
		<input type="hidden" name="PREV" value="0">
              <table width="190" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td><b>Search for</b></td>
                </tr>
                <tr> 
                  <td> 
                    <input type="text" name="S" maxlength="255" size="18" value="##SEARCH##">                  </td>
                </tr>
                <tr> 
                  <td> 
                    <input type="submit" name="Submit" value="Search">
                    &nbsp;&nbsp;&nbsp;<input type="button" name="close" value="Close" onClick="window.close();">
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td><img src="/images/idrac/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr> 
                  <td bgcolor="#000000"><img src="/images/idrac/spacer.gif" width="1" height="1"></td>
                </tr>
                <tr> 
                  <td><img src="/images/idrac/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr> 
                  <td><img src="/images/idrac/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr> 
                  <td><b>Keyword types</b></td>
                </tr>
                <tr> 
                  <td> 
                    <input type="checkbox" name="CON" value="checked" ##CONC##>
                    Concept/Theme</td>
                </tr>
                <tr> 
                  <td> 
                    <input type="checkbox" name="REF" value="checked" ##REFC##>
                    Reference text</td>
                </tr>
                <tr> 
                  <td> 
                    <input type="checkbox" name="COM" value="checked" ##COMC##>
                    Company/Authority</td>
                </tr>
                <tr> 
                  <td> 
                    <input type="checkbox" name="PRO" value="checked" ##PROC##>
                    Product/Brand</td>
                </tr>
                <tr> 
                  <td> 
                    <input type="checkbox" name="THE" value="checked" ##THEC##>
                    Therap. Class/Disease</td>
                </tr>
                <tr> 
                  <td> 
                    <input type="checkbox" name="FDA1" value="checked" ##FDA1C##>
                    Products By Company</td>
                </tr>
                <tr> 
                  <td> 
                    <input type="checkbox" name="FDA2" value="checked" ##FDA2C##>
                    Products By Type</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr>
			<!-- 
                <tr> 
                  <td><b>Search method</b></td>
                </tr>
                <tr> 
                  <td> 
                    <select name="M">
                      <option value="0" selected>Containing</option>
                      <option value="1">Starting with</option>
                      <option value="2">Ending by</option>
                    </select>
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr> 
			 -->
                <tr> 
                  <td><b>Sort order</b></td>
                </tr>
                <tr> 
                  <td> 
                    <select name="O">
                      <option value="ASC" selected>Ascending</option>
                      <option value="DESC">Descending</option>
                    </select>
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td><b>Results per page</b></td>
                </tr>
                <tr> 
                  <td> 
		    <select name="R">
		    <option selected value=##ROWMAX##> ##ROWMAX##
		    <option value=10> 10
		    <option value=20> 20
		    <option value=30> 30
		    <option value=40> 40
		    <option value=50> 50
		    </select>
                    <input type="hidden" name="start" size="4" maxlength="4" value="1">
                    <input type="hidden" name="country" value="">
                    <input type="hidden" name="countrytext" value="">
                    <input type="hidden" name="D" size="5" maxlength="5" value="500">
                  </td>
                </tr>
              </table>
            </form>
          </td>
          <td width="5"><img src="/images/idrac/spacer.gif" width="5" height="5"></td>
        </tr>
      </table>
    </td>
    <td width="410">
      <table width="410" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5"><img src="/images/idrac/spacer.gif" width="5" height="5"></td>
          <td width="400">
            <table width="400" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="200"><img src="/images/idrac/kwdIdxResults.gif" width="200" height="35"></td>
                      <td width="200"><img src="/images/idrac/spacer.gif" width="200" height="35"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td> 
		 