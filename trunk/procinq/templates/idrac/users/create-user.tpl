<link rel="stylesheet" href="/images/espsstyle.css">
<form name="fm" action="/servlet/Main" method=post>
<input type="hidden" name="template" value="Idrac.EditUser">
<TABLE cellSpacing=0 cellPadding=0 width=760 border=0>
  <TBODY>
  <TR vAlign=top>
    <TD width=50>&nbsp;</TD>
    <TD align=left width=660> 
<input type="hidden" name="UserID" value="##ID##">
<p><img height=1 src="/images/idrac/blue.gif" width=660></p>
      <p class="sectionhead">Table of Contents and Reference Text Preferences:</p>
      <p>Select your default regions for the Table of Contents and Reference Page</p>
      <p> 
        <select name="select" size="5">
	  ##R1##
        </select>
        <select name="select2" size="5">
	  ##R2##
        </select>
      </p>
      <p>Tip: If you wish to select just one region, select &quot;None&quot; in 
        the second list box</p>
      <p><img height=1 src="/images/idrac/blue.gif" 
      width=660></p>
      <p class="sectionhead">Simple and Advanced Search Options:</p>
      <p>Number of results per page: 
	##RESULTSPERPAGE##
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        <input type="checkbox" name="return-abstracts" value="checkbox" ##RETRABS##>
        &nbsp;&nbsp;Return Abstracts in Search Results</p>
      <p>Select your default regions. These will be used when you select &quot;My 
        Choice of Regions&quot; in Simple and Advanced Search</p>
      <table width="100%" border="1" cellspacing=0 cellpadding=0 
	 bordercolor="#000000" bordercolordark="#000000" bordercolorlight="#FFFFFF">
        <tr>
          <td>
            <table width="100%" border="0" bordercolor="#000000" bgcolor="#FFFFFF" cellpadding="0" cellspacing=0 class="tabledef">
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox2" value="checkbox">
                  International</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black"> 
                  <input type="checkbox" name="checkbox4" value="checkbox">
                  USA</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox3" value="checkbox">
                  European Union</td>
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox5" value="checkbox">
                  Japan</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black"> 
                  <input type="checkbox" name="checkbox12" value="checkbox">
                  Canada</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox16" value="checkbox">
                  Austria</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox7" value="checkbox">
                  Czech Republic</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox6" value="checkbox">
                  Austrailia</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox17" value="checkbox">
                  Belgium</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox8" value="checkbox">
                  Hungary</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox18" value="checkbox">
                  Denmark</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox9" value="checkbox">
                  Poland</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox19" value="checkbox">
                  Finland</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox10" value="checkbox">
                  Russian Fed.</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox20" value="checkbox">
                  France</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox11" value="checkbox">
                  Slovakia</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox21" value="checkbox">
                  Germany</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox22" value="checkbox">
                  Greece</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF"> 
                  <input type="checkbox" name="checkbox13" value="checkbox">
                  Argentina</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox23" value="checkbox">
                  Ireland</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF"> 
                  <input type="checkbox" name="checkbox14" value="checkbox">
                  Brazil</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox24" value="checkbox">
                  Italy</td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF"> 
                  <input type="checkbox" name="checkbox31" value="checkbox">
                  Hong Kong</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF"> 
                  <input type="checkbox" name="checkbox15" value="checkbox">
                  Mexico</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox25" value="checkbox">
                  Netherlands</td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF"> 
                  <input type="checkbox" name="checkbox32" value="checkbox">
                  Malaysia</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox26" value="checkbox">
                  Portugal</td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF"> 
                  <input type="checkbox" name="checkbox33" value="checkbox">
                  Singapore</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox27" value="checkbox">
                  Spain</td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox28" value="checkbox">
                  Sweden</td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF">&nbsp;</td>
                <td class="black"> 
                  <input type="checkbox" name="checkbox29" value="checkbox">
                  Switzerland</td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF">&nbsp;</td>
                <td class="black">
                  <input type="checkbox" name="checkbox30" value="checkbox">
                  United Kingdom</td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF">&nbsp;</td>
                <td class="black">&nbsp; </td>
                <td class="black">&nbsp;</td>
                <td class="black" bgcolor="#CCFFFF">&nbsp;</td>
                <td class="black">&nbsp;</td>
              </tr>
              <tr> 
                <td class="black" bgcolor="#CCFFFF" bordercolor="#CCFFFF"> 
                  <div align="center"> 
                    <input type="submit" name="Submit" value="Latin America">
                  </div>
                </td>
                <td class="black"> 
                  <div align="center"> 
                    <input type="submit" name="Submit2" value="   Full EU   ">
                  </div>
                </td>
                <td class="black"> 
                  <div align="center"> 
                    <input type="submit" name="Submit22" value="    CEEC    ">
                  </div>
                </td>
                <td class="black" bgcolor="#CCFFFF"> 
                  <div align="center"> 
                    <input type="submit" name="Submit222" value="South East Asia">
                  </div>
                </td>
                <td> 
                  <div align="center"> 
                    <input type="submit" name="Submit2222" value="Clear">
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <p><img height=1 src="/images/idrac/blue.gif" 
      width=660></p>
      <p class="sectionhead">Customer Alerts:</p>
      <p class="sectionhead">Create a new email alert for my topics every 
        <select name="select5">
          <option>day</option>
          <option>week</option>
          <option>month</option>
        </select>
      </p>
      <table width="80%" border="0">
        <tr>
          <td width="24%"> 
            <select name="select4" size="8">
              <option>Alert Number 1</option>
              <option>Alert Number 2</option>
              <option>Alert Number 3</option>
            </select>
          </td>
          <td width="75%"> 
            <p class="sectionhead"><span class="nav"><img src="/images/delete.gif" width="16" height="18">Delete 
              a topic from information alerts<br>
              <br>
              </span><span class="nav"><img src="/images/save.gif" width="17" height="18">Save 
              information alerts</span></p>
            <p>Note: users may add new topics to their profile by navigating to the 
              topic of interest and clicking on "add to my e-mail alerts. </p>
          </td>
          <td width="1%">&nbsp;</td>
        </tr>
      </table>
      <p class="sectionhead"><img height=1 src="/images/idrac/blue.gif" 
      width=660></p>
      <p><a href="mailto">Help us make IDRAC better, click here to send us your 
        questions and suggestions</a></p>
    </TD>
    <TD width=50>&nbsp;</TD></TR></TBODY></TABLE>
