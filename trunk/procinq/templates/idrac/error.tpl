<html>
<head>
<title>Error</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/images/idrac/styles.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINHEIGHT="0" MARGINWIDTH="0" background="/images/idrac/bknd.gif">
<table width="760" border="0" cellspacing="0" cellpadding="0">
<form name="form1" method="get" action="/servlet/Main">
<input type="hidden" name="template" value="Idrac.Search">
  <tr align="left" valign="top"> 
      <td height="95" width="220" background="/images/idrac/mastleft.jpg"><img src="/images/idrac/s.gif" width="220" height="95" usemap="#logo" border="0"></td>
      <td height="95" width="524" background="/images/idrac/mastback.gif" align="right"> 
        <p><img src="/images/idrac/s.gif" width="10" height="10"><br>
          <img src="/images/idrac/searchIDRAC.gif" width="82" height="15">&nbsp; 
          <select size="1" name="Country" class="default">
            <option value="All Regions" selected>All Regions</option>
		<option value="MR">My Regions</option>
		<option value="OR">Other Regions</option>
            <option value="AR">Argentina </option>
            <option value="AT">Austria </option>
            <option value="BE">Belgium </option>
            <option value="BR">Brazil </option>
            <option value="CA">Canada </option>
            <option value="CZ">Czech Republic </option>
            <option value="DK">Denmark </option>
            <option value="EU">European Union </option>
            <option value="FI">Finland </option>
            <option value="FR">France </option>
            <option value="DE">Germany </option>
            <option value="GR">Greece </option>
            <option value="HU">Hungary </option>
            <option value="IR">Ireland </option>
            <option value="IT">Italy </option>
            <option value="JP">Japan </option>
            <option value="NL">Netherlands </option>
            <option value="PL">Poland </option>
            <option value="PT">Portugal </option>
            <option value="RU">Russian Fed. </option>
            <option value="SK">Slovakia </option>
            <option value="SP">Spain </option>
            <option value="SE">Sweden </option>
            <option value="CH">Switzerland </option>
            <option value="UK">United Kingdom </option>
            <option value="US">United States </option>
            <option value="INT">International </option>
          </select>
          &nbsp; 
          <input type="text" name="query" size="18" class="small">
          &nbsp;<input type="image" name="submit" src="/images/idrac/go_mast.gif" width="27" height="28" align="top">&nbsp;<a href="/servlet/Main?template=Idrac.Search&advanced=true" class="white"><span class="small">Advanced 
          Search</span></a><br>
          <img src="/images/idrac/s.gif" width="8" height="8"><br>
          <img src="/images/idrac/nav_none.gif" usemap="#nav" border="0"></p>
	    </form>
      </td>
    <td height="95" width="16" background="/images/idrac/mastback.gif">&nbsp;</td>
  </tr>
</form>  
</table>
<map name="logo"> 
  <area shape="circle" coords="58,48,39" href="/servlet/Main?template=Idrac.FrontPage">
</map>
<map name="nav"> 
  <area shape="rect" coords="417,0,448,14" href="/servlet/Main?template=Taxonomy.Help">
  <area shape="rect" coords="333,0,411,14" href="/servlet/Main?template=Taxonomy.EditUser">
  <area shape="rect" coords="237,0,326,14" href="/servlet/Main?template=Idrac.Browse">
  <area shape="rect" coords="135,0,229,14" href="/servlet/Main?template=Idrac.TableOfContents&CorpusID=29&NodeID=260052&Country=EU">
  <area shape="rect" coords="16,0,126,14" href="/servlet/Main?template=Idrac.TableOfContents&CorpusID=20&Country=EU">
</map>

<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr><td>
<blockquote>
&nbsp; <P>
##ErrorMessage##
</blockquote>
</td></tr>
</table>
      <p class="small" align="center">&copy; 2002 Liquent Inc. All rights reserved<br>
        <a href="whatsnew.html">What's New</a> | <a href="http://www.idrac.com/htm/beta.htm">Enhancements</a> | <a href="http://www.liquent.com/regintelligence_overview.asp">About Us</a> | <a href="mailto:idrac-helpdesk@liquent.com">Contact Us</a> | <a href="deadlink.html"></a><a href="/servlet/Main?template=Taxonomy.Help">Help</a></p>
    </td>
    <td width="50">&nbsp;</td>
  </tr>
</table>
</body>
</html>

