<blockquote>
<b><u>Instructions</u></b>: This function will sychronize your local taxonomies with the 
taxonomies located on Indraweb's home server in Paoli, Pennsylvania.   Any changes that have
been made after the date provided below will be propogated.
<P>
Please enter the synchronization date, in the format MM/DD/YY.  
<form method=post action="/servlet/Main">
<input type="hidden" name="template" value="Server.Synch">
<input type="text" name="Date" size=8 maxlength=8> &nbsp; 
<input type="submit" name="submit" value="Synchronize Now">
</form>
