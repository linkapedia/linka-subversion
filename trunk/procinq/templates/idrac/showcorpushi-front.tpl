<BLOCKQUOTE>
<table width=650><tr><td>
<b>Instructions</b>: This tool will build a topic hierarchy tree for 
a given corpus.  Please select a corpus from the drop down box below, along with
the maximum depth level to be displayed.  <i>Note</i>, increasing the depth 
level beyond the default (3) may increase the time it takes to display the page.
<P>
If you wish to begin at a specific node within the tree, rather than at the corpus root,
you may enter the node identifier for the chosen starting point into the <i>NodeID</i>
field, below.   This field is optional and will default to the corpus root of no parameters
are supplied.
<BR>
<FORM METHOD=post ACTION=/servlet/Main>
<input type="hidden" name="template" value="Taxonomy.ShowCorpusHierarchy">
Corpus: <SELECT NAME="CorpusID">
##OPTION_CORPUS_LIST##
</SELECT>
<BR>Maximum Depth: <input type="text" name="depth" value="3">
<BR>Starting Node ID (optional): <input type="text" name="NodeID" value="">
<p><input type="submit" name="submit" value="Submit">
</form>
</td></tr></table>
</BLOCKQUOTE>
