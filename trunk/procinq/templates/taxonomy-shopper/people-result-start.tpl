<blockquote>
<b>Expert profile:</b> ##NAME## (<a href="/servlet/Main?template=People.Suggest">Suggest expertise</a>)
<P>

##TABLIST##

<SCRIPT>
function help(topic) {
  var ww = 350, wh = 285;
  var wPos = 0;
  var lPos = 0;

  var url = "/servlet/Main?template=Taxonomy.HelpPop&helptext=people-"+topic;

  if(screen) {
  wPos = (screen.width - ww)/2;
  lPos = (screen.height - wh)/2;
  }
  if (!window.SEwin) {
  // Not defined
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  } else {
  // defined
  if (SEwin.closed) {
  // closed, open a new one
  SEwin = window.open(url, "win", 'toolbar=0,directories=0,status=0,scrollbars=0,resizable=0,width='+ww+',height='+wh+',left='+wPos+',top='+lPos+',location=0');
  }}
  SEwin.focus();
}
</SCRIPT>

<form method=post action="/servlet/Main">
<input type="hidden" name="template" value="People.Expert">
<input type="hidden" name="Tab" value="##TAB##">
<input type="hidden" name="UserID" value="##ID##">

<table width=700>
<tr><td width=30%> <b><u>Knowledge Base</u></b> </td>
      <td align=center><b><a href="javascript:help(0);">N/A</a></b></td>
      <td align=center><b><a href="javascript:help(1);">1</a></b></td>
      <td align=center><b><a href="javascript:help(2);">2</a></b></td>
      <td align=center><b><a href="javascript:help(3);">3</a></b></td>
      <td align=center><b><a href="javascript:help(4);">4</a></b></td>
      <td align=center><b><a href="javascript:help(5);">5</a></b></td>
      <td align=center><b><a href="javascript:help(6);">6</a></b></td>
      <td align=center><b><a href="javascript:help(7);">7</a></b></td>
      <td align=center><b><a href="javascript:help(8);">8</a></b></td>
      <td align=center><b><a href="javascript:help(9);">9</a></b></td>
</tr>
