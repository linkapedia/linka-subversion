<blockquote>
<table width=700><tr><td>
<b><u>Instructions</u></b>: 
Please briefly review each of the documents below, and specify whether or not the
document in question should classify into the topic <b>##NODETITLE##</b>.   If it
should, click the check box to the left of the document.
</td></tr>
<tr><td align=right>
<form name="v" method=post action="/servlet/Main">
<input type="hidden" name="template" value="ROC.Validation">
<input type="hidden" name="NodeID" value="##NODEID##">
<input type="hidden" name="CorpusID" value="##CORPUSID##">
<b>Sort using</b>: 
<SELECT NAME="Score" CLASS="select" onChange="this.form.submit()">
<option value="FTIMESC" ##FCD##> Frequency * Coverage
<option value="SCORE4" ##SCORE4D##> Frequency
<option value="SCORE5" ##SCORE5D##> Coverage
</select>
<br>
<hr>
</td></tr></table>
<BR>
