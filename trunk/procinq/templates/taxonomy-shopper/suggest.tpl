<blockquote>
<table width=700><tr><td>
<b><u>Instructions</u></b>: This feature will suggest areas of expertise.  Please copy/paste sample documents that would best characterize the user's knowledge area.  
<P>
<form method="POST" ENCTYPE="multipart/form-data" action="/servlet/Main">
<input type="hidden" name="template" value="People.Suggest">
<input type="hidden" name="users" value="##SELECTUSERS##">
<table width=50%>
<tr><td><b><font face="Arial">Select subject area</font></b>: <select name="subjectarea">
<option value=-1>-- Please select --
##SUBJECTAREA##
</select></font></td></tr>
<tr><td><textarea name="doctext" rows=10 cols=60>Insert a text or HTML document here.</textarea></td></tr>
<tr><td> &nbsp; <BR><input type="submit" name="submit" value="Suggest"> </td></tr>
</table>
</form>
</td></tr></table>
</blockquote>
  