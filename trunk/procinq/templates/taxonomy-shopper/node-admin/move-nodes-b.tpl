<SCRIPT LANGUAGE="JAVASCRIPT"> 
function AddB() {
    window.location="/servlet/Main?template=Taxonomy.AddNode&NodeID=##NODEID##";
}

function UpB() {
   var o_col = "ChildrenB";
   var d_col = "ChildrenA";
   var d_sl = document.fm[d_col].length;

   alert("Your selections have been moved.  They will appear at the bottom of the first list box.");
   for (i=0; i < document.fm[o_col].options.length; i++) {
      if (document.fm[o_col].options[i].selected) {
         oText = document.fm[o_col].options[i].text;
         oValue = document.fm[o_col].options[i].value;
         document.fm[o_col].options[i] = null;
         document.fm[d_col].options[d_sl] = new Option (oText, oValue, false, true);
	 d_sl = d_sl + 1; i = i - 1;
      } 
   }     
}

function ExploreB() {
   var col = "ChildrenB";
   var sl = document.fm[col].selectedIndex;

     if (sl != -1) {
         window.location = "/servlet/Main?template=Taxonomy.MoveNodes&NodeIDA="+document.fm.NodeIDA.value+"&NodeIDB="+document.fm[col].options[sl].value;
    } else { alert("You must first highlight the selection to explore in the drop down list."); }
}

function ExploreLinkB(NodeID) {
   window.location = "/servlet/Main?template=Taxonomy.MoveNodes&NodeIDA="+document.fm.NodeIDA.value+"&NodeIDB="+NodeID;
}

</script>
<input type=hidden name=NodeIDB value=##NODEID##>
<table width=650>
<tr><td><font face="Arial, Helvetica, sans-serif" size=-1><b>Parent Tree:</b></font></td>
                 <td>##NODETREE##</td></tr>
  <tr><td width=100><font face="Arial, Helvetica, sans-serif" size=-1><b>Node Title:</b></font></td>
       <td> <a href="/servlet/Main?template=Taxonomy.AdminBrowse&NodeID=##NODEID##">
	    ##NODETITLE##</a> </td></tr>
</table>