<html>
<head>
<title>Edit signatures</title>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<script language="javascript">
function goDone() {
   var newsigs = "";
   var len = document.fm2.thesignatures.length;
 
   for (i = 0; i < len; i++) {
      if (document.fm2.thesignatures.options[i].value != "none") {
         if (i == 0) { newsigs = document.fm2.thesignatures.options[i].text; }
	 else { newsigs = newsigs + "," + document.fm2.thesignatures.options[i].text; }
      }
   }

   opener.document.fm.Signatures.value = newsigs;
   window.close();
}

function RemoveSig() {
   var sl = document.fm2.thesignatures.selectedIndex;

   if (confirm("Delete this signature from this topic?")) {
      if (document.fm2.thesignatures.options[sl].value != 'none') {
         if (document.fm2.thesignatures.length==1) {
	       document.fm2.thesignatures.options[0].text="##NODETITLE##";
	       document.fm2.thesignatures.options[0].value="none";
         } else {
            document.fm2.thesignatures.options[sl]=null; 
         }
      }
   }
} 

function Add() {
   var word = document.fm2.word.value;
   var freq = document.fm2.freq.value;

   d_sl = document.fm2.thesignatures.length;

   if (word > "" && freq > "") {
     oText = word + " (" + freq + ")";
     oValue = word + ":" + freq;

     document.fm2.word.value = "";
     document.fm2.freq.value = "";

     document.fm2.thesignatures.options[d_sl] = new Option (oText, oValue, false, true);
     document.fm2.thesignatures.selectedIndex = "";

     if (document.fm2.thesignatures.options[0].value == 'none') {
      document.fm2.thesignatures.options[0] = null; }

   } else {
     alert("Sorry, you must type a signature and frequency to add before hitting the add button.");
   }
}
</script>

</head>
<body bgcolor=ffffff topmargin=0 marginheight=0>
<br>
<table border=0 cellpadding=2 cellspacing=0 width=100% bgcolor=ccccff><tr><td height=1>
<table border=0 width=100% cellpadding=4 cellspacing=0 bgcolor=#FFFFFF>
<tr>
<td valign=top align=center>
<table border=0 cellpadding=1 cellspacing=0 width=100%><tr bgcolor=dddddd><td valign=bottom width=18>&nbsp;</td><td width=100%>&nbsp;<font face=arial size=-1 point-size=10 style="font-size:13;">
<b>Edit signatures</b></font></td></tr></table>
<!--this is set for 3 column layout, which is default-->
<form method=GET name=fm2>
<input type=hidden NAME=nodeid VALUE="##NODEID##">
<input type=hidden NAME=nodetitle VALUE="##NODETITLE##">
<tr><td height=5></td></tr>
<tr>
<td valign=top colspan=2 width=250>
<input type=text name=word size="15" maxlength="25">
<input type=text name=freq size="4" maxlength="3">
<font face=arial size=-1 point-size=10 style="font-size:13;">
<input type=button name=add value="Add >>" onclick="Add();">
<br><br>
You can find more information about <a target=new href="#">signatures</a> in our 
<a href="#" target=new>help section</a>.
<p>
</font>
</td>
<td width=15>&nbsp;</td>
<td valign=top>
<table border=0 cellpadding=0 cellspacing=0>
<tr><td>
<font face=arial size=-1 point-size=10 style="font-size:13;">
<select name="thesignatures" size="8" multiple>
<option value="none">
##NODETITLE##</option>
</select></font></td>
<td>&nbsp; <br>
<br>&nbsp;</td>
</tr>
<tr><td colspan=2><font face=arial size=-1 point-size=10 style="font-size:13;"><input type="button" value="Remove" onclick="RemoveSig()">
<br><br></font></td></tr>
</table></td>
</tr>
</table></td></tr>
<tr><td align=center>
</td></tr>
</table>
</td></tr></table>
<!--****END BORDERS FOR CONTENT-->
<table border=0 cellspacing=4 cellpadding=0 width=100%><tr><td align=right>
<font face=arial size=-1 point-size=10 style="font-size:13;">
<INPUT TYPE=button value="Done" NAME="done" onClick="goDone();">
<INPUT TYPE=button value="Cancel" NAME="cancel" onClick="window.close();">
</form>
</font>
</td>
</tr>
<tr><td></form></td></tr></table>
<script language="javascript">
##SIGNATURE_LIST##
document.fm2.thesignatures.selectedIndex = "";

</script>
</body>
</html>
