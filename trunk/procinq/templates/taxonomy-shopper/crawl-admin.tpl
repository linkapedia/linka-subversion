  <BLOCKQUOTE>
    <table width="76%" border="0">
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.CrawlStatus"><b>Monitor</b></a></td>
        <td width="73%" valign="top">Monitor gives you a window into the current run status of the knowledge
	    harvesting system. <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="/servlet/Main?template=Crawl.CorpusPriority"><b>Priorities</b></a></td>
        <td width="73%" valign="top">View and change the priority queue for the knowledge harvesting system.<br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
      <tr> 
        <td width="18%" valign="top"><a href="http://www.surfablebooks.com/seditor/main.cgi"><b>Assign Sources</b></a></td>
        <td width="73%" valign="top">Choose from over one thousand sources and assign to specific corpora for 
	    knowledge harvesting as appropriate. 
          <br>
          <br>
        </td>
        <td width="9%">&nbsp;</td>
      </tr>
    </table>
    </BLOCKQUOTE>