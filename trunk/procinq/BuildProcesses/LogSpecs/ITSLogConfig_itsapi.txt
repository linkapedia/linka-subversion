# IndraWeb Log Configuration

# DEBUG < INFO < WARN < ERROR < FATAL.

#log4j.rootLogger=INFO, ROLLINGLOGFILE
log4j.rootLogger=INFO, CONSOLE, ROLLINGLOGFILE

# APPENDERS

log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
log4j.appender.ROLLINGLOGFILE=org.apache.log4j.DailyRollingFileAppender

log4j.appender.ROLLINGLOGFILE.File=c:/Program Files/ITS/logs/ITSLog.log
#linux log4j.appender.ROLLINGLOGFILE.File=/home/admin/src/ITS/logs/ITSLog.log

log4j.appender.ROLLINGLOGFILE.DatePattern='.'yyyy-MM-dd
log4j.appender.ROLLINGLOGFILE.layout=org.apache.log4j.PatternLayout

# Print the date in ISO 8601 format
log4j.appender.ROLLINGLOGFILE.layout.ConversionPattern=%d [%t] %-5p %c - %m%n

# Print only messages of level WARN or above in the package com.foo.
#log4j.logger.com.foo=WARN


