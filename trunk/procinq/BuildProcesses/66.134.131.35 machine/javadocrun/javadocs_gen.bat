echo off
set JAVADOCINPUT=C:\docume~1\Indraweb\buildprocess\javadocinput
set DOXYGENBIN=C:\PROGRA~1\doxygen\bin
set JAVADOCOUT=C:\docume~1\Indraweb\buildprocess\javadocoutput

rmdir /S /Q    "C:\Documents and Settings\Indraweb\buildprocess\javadocoutput"
if exist "C:\Documents and Settings\Indraweb\buildprocess\javadocoutput" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\javadocoutput"
if exist "C:\Documents and Settings\Indraweb\buildprocess\javadocoutput" pause
mkdir "C:\Documents and Settings\Indraweb\buildprocess\javadocoutput" 

echo step0 images
xcopy "C:\Documents and Settings\Indraweb\buildprocess\javadocinput\images\*.*" "C:\Documents and Settings\Indraweb\buildprocess\javadocoutput\images\*.*"  /s



rem example
rem C:\PROGRA~1\doxygen\bin\doxygen.exe C:\docume~1\Indraweb\buildprocess\javadocinput\modules\tsnugget.cfg





echo step1

%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\people.cfg
echo step2	
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\security.cfg
echo step3
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsclassify.cfg
echo step4
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tscorpus.cfg
echo step5
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tscql.cfg
echo step6
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsdocument.cfg
echo step7
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsgenre.cfg
echo step8
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsnode.cfg
echo step9
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsnotification.cfg
echo step10
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsnugget.cfg
echo step11
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsother.cfg
echo step12
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tspurge.cfg
echo step13
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsrepository.cfg
echo step14
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsrocsettings.cfg
echo step15
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tssubjectarea.cfg
echo step16
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsthesaurus.cfg
echo step17
%DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsuser.cfg
rem echo step18
rem %DOXYGENBIN%\doxygen.exe %JAVADOCINPUT%\modules\tsidrac.cfg
echo step19

echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\people\html\main.html
echo step20
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\security\html\main.html
echo step21
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsclassify\html\main.html
echo step22
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tscorpus\html\main.html
echo step23
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tscql\html\main.html
echo step24
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsdocument\html\main.html
echo step25
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsgenre\html\main.html
echo step26
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsnode\html\main.html
echo step27
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsnotification\html\main.html
echo step28
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsnugget\html\main.html
echo step29
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsother\html\main.html
echo step30
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tspurge\html\main.html
echo step31
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsrepository\html\main.html
echo step32
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsrocsettings\html\main.html
echo step33
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tssubjectarea\html\main.html
echo step34
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsthesaurus\html\main.html
echo step35
echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsuser\html\main.html
echo step36
rem echo ^<script^>window.location='../../index.html';^</script^> > %JAVADOCOUT%\tsidrac\html\main.html


