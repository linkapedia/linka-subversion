

echo LAST CHANCE TO ABORT THIS BUILD - WILL STOP TOMCAT IF YOU CONTINUE
rem pause
echo off

time /T
set ARG1=%1

rem ********* determine ITS home dir for this build

set BUILDHOME=C:\Documents and Settings\Indraweb\buildprocess
set WEBAPPSHOME=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps

if "%ARG1%"=="itsapi"             set SRVLTHM_SVL=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet%ARG1%
if "%ARG1%"=="itsharvesterclient" set SRVLTHM_SVL=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet%ARG1%
if "%ARG1%"=="itsclassifier"      set SRVLTHM_SVL=
if "%ARG1%"=="itsharvester"       set SRVLTHM_SVL=

if "%ARG1%"=="itsapi" set ITSAPPHOME=c:\program files\ITS
if "%ARG1%"=="itsapi" set SRVLTHM_ITS=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsapi
if "%ARG1%"=="itsapi" goto STARTBUILD

if "%ARG1%"=="itsclassifier" 	set ITSAPPHOME=c:\program files\ITSClassifier
if "%ARG1%"=="itsclassifier" 	set SRVLTHM_ITS=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsclassifier
if "%ARG1%"=="itsclassifier" 	goto STARTBUILD

if "%ARG1%"=="itsharvester" 	set ITSAPPHOME=c:\program files\ITSHarvester
if "%ARG1%"=="itsharvester" 	set SRVLTHM_ITS=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsharvester
if "%ARG1%"=="itsharvester" 	goto STARTBUILD

if "%ARG1%"=="itsharvesterclient" 	set ITSAPPHOME=c:\program Files\ITSHarvesterClient
if "%ARG1%"=="itsharvesterclient" 	set SRVLTHM_ITS=C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\itsharvesterclient
if "%ARG1%"=="itsharvesterclient" 	goto STARTBUILD

echo NOTICE: FIRST argument must be (case included) itsapi OR itsclassifier OR itsharvester OR itsharvesterclient Stopping Build
goto ENDPROCERROR

:STARTBUILD

rem goto STEPEIGHTEEN


echo "ARG1 is %ARG1%"
echo "ITSAPPHOME is [%ITSAPPHOME%]"
echo "SRVLTHM_ITS is [%SRVLTHM_ITS%]"
echo "SRVLTHM_SVL is [%SRVLTHM_SVL%]"

rem ************ make sure can manipulate and write required dir's 
rem ************ make sure can manipulate and write required dir's 


echo STEP 1 DELETING OLD SERVLET CONTAINERS DIR 
rmdir /S /Q    "%SRVLTHM_ITS%\WEB-INF\classes"
if exist "%SRVLTHM_ITS%\WEB-INF\classes" ECHO "ERROR removing %SRVLTHM_ITS%\WEB-INF\classes"
if exist "%SRVLTHM_ITS%\WEB-INF\classes" goto ENDPROCERROR
mkdir "%SRVLTHM_ITS%\WEB-INF\classes"
rmdir /S /Q    "%SRVLTHM_ITS%\WEB-INF\lib"
if exist "%SRVLTHM_ITS%\WEB-INF\lib" ECHO "ERROR removing %SRVLTHM_ITS%\WEB-INF\lib"
if exist "%SRVLTHM_ITS%\WEB-INF\lib" goto ENDPROCERROR
mkdir "%SRVLTHM_ITS%\WEB-INF\lib"

if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_NoDel1
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_NoDel1
rmdir /S /Q    "%SRVLTHM_SVL%\WEB-INF\classes"
if exist "%SRVLTHM_SVL%\WEB-INF\classes" ECHO "ERROR removing %SRVLTHM_SVL%\WEB-INF\classes"
if exist "%SRVLTHM_SVL%\WEB-INF\classes" goto ENDPROCERROR
mkdir "%SRVLTHM_SVL%\WEB-INF\classes"
rmdir /S /Q    "%SRVLTHM_SVL%\WEB-INF\lib"
if exist "%SRVLTHM_SVL%\WEB-INF\lib" ECHO "ERROR removing %SRVLTHM_SVL%\WEB-INF\lib"
if exist "%SRVLTHM_SVL%\WEB-INF\lib" goto ENDPROCERROR
mkdir "%SRVLTHM_SVL%\WEB-INF\lib"
:SKIPNOSERVLETUIDIR_NoDel1


echo NOTE: SERVLET CONTAINERS LIKE TOMCAT AND JRUN SHOULD BE SHUT DOWN BEFORE CONTINUING !!!  


cd "%BUILDHOME%"


echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ START BUILD

ECHO STEP 2 BACKUP SIGNATURE FILES 
rmdir  /Q /S "C:\temp\corpussource"
if exist "C:\temp\corpussource" ECHO "ERROR removing C:\temp\corpussource"
if exist "C:\temp\corpussource" goto ENDPROCERROR
mkdir        "C:\temp\corpussource"
copy         "%ITSAPPHOME%\corpussource\sig*.txt" "C:\temp\corpussource"


ECHO STEP 3 BACKUP LOG FILES 
rem save log files for later in build restore to dirs (NOT FOR PRODUCTION BUILD)
rmdir  /Q /S "C:\temp\logs"
if exist "C:\temp\logs" ECHO "ERROR removing C:\temp\logs"
if exist "C:\temp\logs" goto ENDPROCERROR
mkdir        "C:\temp\logs"
copy         "%ITSAPPHOME%\logs\*.*" "C:\temp\logs\"

rem echo "ITSAPPHOME is [%ITSAPPHOME%]"

ECHO STEP 4 CREATE HOME DIR STRUCTURE "%ITSAPPHOME%"
rem ITS HOME (subdirs in alphabetical order)
if exist "%ITSAPPHOME%" rmdir  /Q /S "%ITSAPPHOME%"
if exist "%ITSAPPHOME%" ECHO "ERROR removing %ITSAPPHOME%"
if exist "%ITSAPPHOME%" goto ENDPROCERROR

mkdir        "%ITSAPPHOME%"
mkdir        "%ITSAPPHOME%\bin"
mkdir        "%ITSAPPHOME%\classifyfiles"


ECHO STEP 5 GET NON CLASS FILES (commonwords, ITSLogConfig_%ARG1%.txt) FROM SSAFE THRU BUILDPROCESS
rem **** root files thru build process folder (due to multiple target homes)
if exist "%BUILDHOME%\commonwords.txt" del /F /Q "%BUILDHOME%\commonwords.txt"
if exist "%BUILDHOME%\ITSLogConfig_%ARG1%.txt" del /F /Q "%BUILDHOME%\ITSLogConfig_%ARG1%.txt"
ss get "$/ITSHome/commonwords.txt" -GL"%BUILDHOME%" -R  -W -O- -Yhkon,racer9
ss get "$/ITSHome/ITSLogConfig_%ARG1%.txt" -GL"%BUILDHOME%"  -R  -W -O- -Yhkon,racer9
copy "%BUILDHOME%\commonwords.txt" "%ITSAPPHOME%" /Y
copy "%BUILDHOME%\ITSLogConfig_%ARG1%.txt" "%ITSAPPHOME%\ITSLogConfig.txt" /Y

rem BUILD SPECIFIC - its folder contents

ECHO STEP 5.1 CORPUS-IMPORT thru build process folder
rmdir  /Q /S "%BUILDHOME%\corpus-import"
if exist "%BUILDHOME%\corpus-import" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\corpus-import"
if exist "%BUILDHOME%\corpus-import" goto ENDPROCERROR
mkdir        "%BUILDHOME%\corpus-import"
ss get "$/corpus" -GL"%BUILDHOME%\corpus-import"  -R  -W -O- -Yhkon,racer9
mkdir        "%ITSAPPHOME%\corpus-import"
xcopy "%BUILDHOME%\corpus-import\*.*" "%ITSAPPHOME%\corpus-import" /Y /S /Q

if not "%ARG1%"=="itsapi" goto SKIP_NONITSAPI_1

  ECHO STEP 5.2 DOCUMENTATION thru build process folder
  rmdir  /Q /S "%BUILDHOME%\doc"
  mkdir        "%BUILDHOME%\doc"
  ss get "$/ITS Documentation/*.pdf" -GL"%BUILDHOME%\doc"  -R  -W -O- -Yhkon,racer9
  mkdir        "%ITSAPPHOME%\doc"
  xcopy "%BUILDHOME%\doc\*.*" "%ITSAPPHOME%\doc\" /Y /S /Q

  ECHO STEP 5.3 PERL thru build process folder
  rmdir  /Q /S "%BUILDHOME%\PERL"
  if exist "%BUILDHOME%\PERL" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\PERL"
  if exist "%BUILDHOME%\PERL" goto ENDPROCERROR
  mkdir        "%BUILDHOME%\PERL"
  ss get "$/Perl" -GL"%BUILDHOME%\PERL"  -R  -W  -O- -Yhkon,racer9
  mkdir        "%ITSAPPHOME%\PERL"
  xcopy "%BUILDHOME%\PERL\*.*" "%ITSAPPHOME%\PERL\" /Y /S /Q

  ECHO STEP 5.4 ROCCH thru build process folder
  rem del /F /Q /S "%BUILDHOME%\ROCCH\*.*"
  rmdir  /Q /S "%BUILDHOME%\ROCCH"
  if exist "%BUILDHOME%\ROCCH" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\ROCCH"
  if exist "%BUILDHOME%\ROCCH" goto ENDPROCERROR
  mkdir        "%BUILDHOME%\ROCCH"
  ss get "$/ROCCH/Makefile" -GL"%BUILDHOME%\ROCCH"  -R  -W -O- -Yhkon,racer9
  ss get "$/ROCCH/Readme" -GL"%BUILDHOME%\ROCCH"  -R  -W  -O- -Yhkon,racer9
  ss get "$/ROCCH/ROCCH" -GL"%BUILDHOME%\ROCCH"  -R  -W  -O- -Yhkon,racer9
  mkdir        "%ITSAPPHOME%\ROCCH"
  xcopy "%BUILDHOME%\ROCCH\*.*" "%ITSAPPHOME%\ROCCH" /Y /S /Q

:SKIP_NONITSAPI_1


rem BUILD SPECIFIC - templates
ECHO STEP 6 GET TEMPLATES FROM SSAFE THRU BUILDPROCESS
rem BUILD SPECIFIC - TEMPLATES 
rem **** Templates thru build process folder
rem rmdir  /Q /S "%BUILDHOME%\templates"
rem if exist "%BUILDHOME%\templates" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\templates"
rem if exist "%BUILDHOME%\templates" goto ENDPROCERROR
rem mkdir        "%BUILDHOME%\templates"
rem ss get "$/templates" -GL"%BUILDHOME%\templates"  -R  -W  -O- -Yhkon,racer9
rem mkdir        "%ITSAPPHOME%\templates"

rem **** get Templates/notification to build process folder also
rem mkdir        "%BUILDHOME%\templates\notification"
rem ss get "$/Java2/JAVA Notification Project/*.tpl"  -GL"%BUILDHOME%\templates\notification"  -R  -W -O- -Yhkon,racer9
rem xcopy "%BUILDHOME%\templates\*.*" "%ITSAPPHOME%\templates" /Y /S  /Q


ECHO STEP 6.1 word frequencies thru build process folder
rem rmdir  /Q /S "%BUILDHOME%\WordFrequencies"
rem mkdir        "%BUILDHOME%\WordFrequencies"
rem ss get "$/ITSHome/WordFrequencies" -GL"%BUILDHOME%\WordFrequencies"  -R  -W  -O- -Yhkon,racer9
rem mkdir        "%ITSAPPHOME%\WordFrequencies"
rem xcopy "%BUILDHOME%\WordFrequencies\*.*" "%ITSAPPHOME%\WordFrequencies" /Y   /Q


rem **** create required folders for runtime
mkdir        "%ITSAPPHOME%\CorpusSource"
mkdir        "%ITSAPPHOME%\logs"



rem BUILD SPECIFIC - external jar files 
ECHO STEP 7 GET JAVA ENVIRONMENT JAR FILES FROM SSAFE THRU BUILDPROCESS
rem *********************************** java environment (zip files)
rem rmdir /Q /S "%BUILDHOME%\java environment"
rem if exist "%BUILDHOME%\java environment" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\java environment"
rem if exist "%BUILDHOME%\java environment" goto ENDPROCERROR
rem mkdir       "%BUILDHOME%\java environment"


ECHO STEP 7.1 CREATE FOLDER : SRVLTHM_ITS LIB 
if exist "%SRVLTHM_ITS%\WEB-INF\lib" rmdir /Q /S    "%SRVLTHM_ITS%\WEB-INF\lib"
if exist "%SRVLTHM_ITS%\WEB-INF\lib" ECHO "ERROR removing "%SRVLTHM_ITS%\WEB-INF\lib"
if exist "%SRVLTHM_ITS%\WEB-INF\lib" goto ENDPROCERROR
mkdir          "%SRVLTHM_ITS%\WEB-INF\lib"

if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_noservletlib
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_noservletlib
  ECHO STEP 7.2 CREATE FOLDER : SERVLETHOME-servlet LIB 
  rmdir /Q /S    "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib"
  if exist "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib" ECHO "ERROR removing C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet\WEB-INF\lib"
  if exist "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib" goto ENDPROCERROR
  mkdir          "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\lib"
:SKIPNOSERVLETUIDIR_noservletlib

ECHO STEP 7.3 ************** GET JAR FILES PER BUILD TYPE
rem BUILD SPECIFIC - which jar files needed 


ss get "$/Java2/java environment/gnu-regexp-1.1.4.jar" -GL"%BUILDHOME%\java environment" -R  -W -O- -Yhkon,racer9
ss get "$/Java2/java environment/ms.jar" -GL"%BUILDHOME%\java environment"  -R  -W  -O- -Yhkon,racer9
ss get "$/Java2/java environment/oreilly.jar" -GL"%BUILDHOME%\java environment"  -R  -W -O- -Yhkon,racer9
ss get "$/Java2/java environment/log4j-1.2.8.jar" -GL"%BUILDHOME%\java environment"  -R  -W -O- -Yhkon,racer9
ss get "$/Java2/java environment/runtime12.jar" -GL"%BUILDHOME%\java environment"  -R  -W -O- -Yhkon,racer9
ss get "$/Java2/java environment/DbConnectionBroker1.0.11.jar" -GL"%BUILDHOME%\java environment"  -R  -W -O- -Yhkon,racer9
ss get "$/Java2/java environment/classes12.jar" -GL"%BUILDHOME%\java environment"   -R  -W -O- -Yhkon,racer9

if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_nocopy
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_nocopy
  xcopy "%BUILDHOME%\java environment\*.*" "%SRVLTHM_SVL%\WEB-INF\lib" /Y /Q
:SKIPNOSERVLETUIDIR_nocopy

rem already-copied servlet app needs no db connections or CQL or ftp 
ss get "$/Java2/java environment/FTPEnterprise.jar" -GL"%BUILDHOME%\java environment"  -R  -W -O- -Yhkon,racer9
ss get "$/Java2/java environment/sql4j.jar" -GL"%BUILDHOME%\java environment"  -R  -W -O- -Yhkon,racer9

xcopy "%BUILDHOME%\java environment\*.*" "%SRVLTHM_ITS%\WEB-INF\lib\" /Y /Q


ECHO STEP 8 GET IMAGES DIRECT TO ROOT\images
if "%ARG1%"=="itsharvester" goto SKIPIMAGES
if "%ARG1%"=="itsclassifier" goto SKIPIMAGES
  rem *********************************** images
  rmdir /S /Q    "%WEBAPPSHOME%\ROOT\images"
  if exist "%WEBAPPSHOME%\ROOT\images" ECHO "ERROR removing C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\ROOT\images"
  if exist "%WEBAPPSHOME%\ROOT\images" goto ENDPROCERROR
  mkdir          "%WEBAPPSHOME%\ROOT\images"
  ss get "$/images" -GL"%WEBAPPSHOME%\ROOT\images"  -R  -W  -O- -Yhkon,racer9
:SKIPIMAGES

rem ********************************** CLASSES FOLDERS 

ECHO STEP 9 CREATE FOLDER : SRVLTHM_ITS CLASSES 
rmdir /Q /S    "%SRVLTHM_ITS%\WEB-INF\classes"
if exist "%SRVLTHM_ITS%\WEB-INF\classes" ECHO "ERROR removing "%SRVLTHM_ITS%\WEB-INF\classes"
if exist "%SRVLTHM_ITS%\WEB-INF\classes" goto ENDPROCERROR
mkdir          "%SRVLTHM_ITS%\WEB-INF\classes"


ECHO STEP 10 GET JAVA FROM SSAFE ALL UNDER BUILDPROCESS

rmdir /S /Q    "%BUILDHOME%\java"
if exist "%BUILDHOME%\java" ECHO "ERROR removing "%BUILDHOME%\java"
if exist "%BUILDHOME%\java" goto ENDPROCERROR
mkdir "%BUILDHOME%\java"

ECHO STEP 10.1 GET JAVA SOURCE - CQL
rem if not "%ARG1%"=="itsapi" goto SKIPCQL
rem rmdir /S /Q    "%BUILDHOME%\java\CQL"
if exist "%BUILDHOME%\java\CQL" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\java\CQL"
if exist "%BUILDHOME%\java\CQL" goto ENDPROCERROR
mkdir          "%BUILDHOME%\java\CQL"
ss get "$/Java2/CQL" -GL"%BUILDHOME%\java\CQL"  -R  -W  -O- -Yhkon,racer9
:SKIPCQL

ECHO STEP 10.2 GET JAVA SOURCE - engine
rem rmdir /S /Q    "%BUILDHOME%\java\engine"
if exist "%BUILDHOME%\java\engine" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\java\engine"
if exist "%BUILDHOME%\java\engine" goto ENDPROCERROR
mkdir          "%BUILDHOME%\java\engine"
ss get "$/Java2/engine" -GL"%BUILDHOME%\java\engine"  -R  -W  -O- -Yhkon,racer9

ECHO STEP 10.3 GET JAVA SOURCE - External Classifiers 
rem rmdir /S /Q    "%BUILDHOME%\java\External Classifiers"
mkdir          "%BUILDHOME%\java\External Classifiers"
ss get "$/Java2/External Classifiers" -GL"%BUILDHOME%\java\External Classifiers"  -R  -W  -O- -Yhkon,racer9

rem if "%ARG1%"=="itsclassifier" goto SKIPIndraDisplay
rem if "%ARG1%"=="itsharvester" goto SKIPIndraDisplay
  rem *********************************** GET JAVA SOURCE - IndraDisplay
  rem rmdir /S /Q    "%BUILDHOME%\java\IndraDisplay"
  mkdir          "%BUILDHOME%\java\IndraDisplay"
  ss get "$/Java2/IndraDisplay" -GL"%BUILDHOME%\java\IndraDisplay" -R  -W   -O- -Yhkon,racer9

  rem rmdir /S /Q    "%BUILDHOME%\java\IndraDisplayUtils"
  mkdir          "%BUILDHOME%\java\IndraDisplayUtils"
  ss get "$/Java2/IndraDisplayUtils" -GL"%BUILDHOME%\java\IndraDisplayUtils" -R  -W   -O- -Yhkon,racer9
:SKIPIndraDisplay

ECHO STEP 10.4 GET JAVA SOURCE - IndraUtils
rem rmdir /S /Q    "%BUILDHOME%\java\IndraUtils"
mkdir          "%BUILDHOME%\java\IndraUtils"
ss get "$/Java2/IndraUtils" -GL"%BUILDHOME%\java\IndraUtils" -R  -W   -O- -Yhkon,racer9

rem rmdir /S /Q    "%BUILDHOME%\java\NDMatchProject"
ECHO STEP 10.5 GET JAVA SOURCE - NDMatchProject
mkdir          "%BUILDHOME%\java\NDMatchProject"
ss get "$/Java2/NDMatchProject" -GL"%BUILDHOME%\java\NDMatchProject" -R  -W  -O- -Yhkon,racer9

ECHO STEP 10.6 GET JAVA SOURCE - NDScoreProject
rem rmdir /S /Q    "%BUILDHOME%\java\NDScoreProject"
mkdir          "%BUILDHOME%\java\NDScoreProject"
ss get "$/Java2/NDScoreProject" -GL"%BUILDHOME%\java\NDScoreProject" -R  -W  -O- -Yhkon,racer9

ECHO STEP 10.7 GET JAVA SOURCE - TS_API_PROJECT
rem rmdir /S /Q    "%BUILDHOME%\java\TS_API_PROJECT"
mkdir          "%BUILDHOME%\java\TS_API_PROJECT"
mkdir          "%BUILDHOME%\java\TS_API_PROJECT\api"
ss get "$/Java2/TS_API_PROJECT" -GL"%BUILDHOME%\java\TS_API_PROJECT" -R -W  -O- -Yhkon,racer9

ECHO STEP 10.8.3 delete and recreate %BUILDHOME%\classes dir
rmdir /S /Q    "%BUILDHOME%\classes"
if exist "%BUILDHOME%\classes" ECHO "ERROR removing %BUILDHOME%\classes"
if exist "%BUILDHOME%\classes" goto ENDPROCERROR
mkdir          "%BUILDHOME%\classes"

ECHO STEP 11 !!!!!!!!!!!!! MASTER X00 JAVA FILES BUILD - BUILDPROCESS CLASSES
call ant buildjava 
IF ERRORLEVEL 1 pause

ECHO STEP 12 COPY primary CLASSES from buildprocess to under the SRVLTHM_ITS
xcopy "%BUILDHOME%\classes"  "%SRVLTHM_ITS%\WEB-INF\classes"  /Y /S  /Q

ECHO STEP 13 BUILD ITSHOME/BIN APPLICATIONS 
if not "%ARG1%"=="itsapi" goto SKIPITSAPIBINApplications
rem BUILD ITSHOME/BIN APPLICATIONS 
ECHO STEP 13.1 BUILD ITSHOME/BIN APPLICATIONS - FCRAWL
rem 1) *********************************** fcrawl
mkdir        "%ITSAPPHOME%\bin\fcrawl"
rmdir /S /Q    "%BUILDHOME%\fcrawl"
if exist "%BUILDHOME%\fcrawl" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\fcrawl"
if exist "%BUILDHOME%\fcrawl" goto ENDPROCERROR
mkdir          "%BUILDHOME%\fcrawl\src"
mkdir          "%BUILDHOME%\fcrawl\bin"
ss get "$/Java2/fcrawl/*.java" -GL"%BUILDHOME%\fcrawl\src" -R   -O- -Yhkon,racer9
rem **** get and copy to bin the fcrawl bat file and sample fcrawl cfg file  
ss get "$/Java2/fcrawl/runFcrawl.bat" -GL"%BUILDHOME%\fcrawl" -R  -W -O- -Yhkon,racer9
ss get "$/Java2/fcrawl/fcrawl.cfg" -GL"%BUILDHOME%\fcrawl" -R  -W -O- -Yhkon,racer9
copy "%BUILDHOME%\fcrawl\runFcrawl.bat" "%ITSAPPHOME%\bin\fcrawl"
copy "%BUILDHOME%\fcrawl\fcrawl.cfg" "%ITSAPPHOME%\bin\fcrawl"
rem ************** COMPILE FCRAWL JAVA PROJECT
cd "%BUILDHOME%"
call ant buildfcrawl
IF ERRORLEVEL 1 pause
cd "%BUILDHOME%\fcrawl\bin"
jar -cvf "%ITSAPPHOME%\bin\fcrawl\fcrawl.jar" *.class > c:\temp\temp.txt


ECHO STEP 13.2 BUILD ITSHOME/BIN APPLICATIONS - notification
rem 2) *********************************** notification
mkdir        "%ITSAPPHOME%\bin\notification"
rmdir /S /Q    "%BUILDHOME%\notification"
if exist "%BUILDHOME%\notification" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\notification"
if exist "%BUILDHOME%\notification" goto ENDPROCERROR
mkdir          "%BUILDHOME%\notification\src"
mkdir          "%BUILDHOME%\notification\bin"
ss get "$/Java2/JAVA Notification Project/*.java" -GL"%BUILDHOME%\notification\src" -R   -O- -Yhkon,racer9
rem **** get and copy to bin the notification bat file 
ss get "$/Java2/JAVA Notification Project/runNotification.bat" -GL"%BUILDHOME%\notification" -R  -W   -O- -Yhkon,racer9
copy "%BUILDHOME%\notification\runNotification.bat" "%ITSAPPHOME%\bin\notification"
rem ************** COMPILE Notification JAVA PROJECT
cd "%BUILDHOME%"
call ant buildNotification
IF ERRORLEVEL 1 pause
cd "%BUILDHOME%\notification\bin"
jar -cvf "%ITSAPPHOME%\bin\notification\notification.jar" *.class > c:\temp\temp.txt
rem ********* notification templates 
ss get "$/Java2/JAVA Notification Project/*.tpl" -GL"%BUILDHOME%\notification\src" -R  -W   -O- -Yhkon,racer9


ECHO STEP 13.3 BUILD ITSHOME/BIN APPLICATIONS - RebuildTextIndex
rem 3) *********************************** RebuildTextIndex
mkdir        "%ITSAPPHOME%\bin\RebuildTextIndex"
rem NOTE:  CLASS FILES OF ITSAPI MUST EXIST AT THIS POINT TO ADD HASHREE TO THIS JAR
rmdir /S /Q    "%BUILDHOME%\RebuildTextIndex"
if exist "%BUILDHOME%\RebuildTextIndex" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\RebuildTextIndex"
if exist "%BUILDHOME%\RebuildTextIndex" goto ENDPROCERROR
mkdir          "%BUILDHOME%\RebuildTextIndex\src"
mkdir          "%BUILDHOME%\RebuildTextIndex\bin"
mkdir          "%BUILDHOME%\RebuildTextIndex\bin\api\results"
ss get "$/Java2/InvokeImIndex/src/*.java" -GL"%BUILDHOME%\RebuildTextIndex\src" -R  -O- -Yhkon,racer9
rem **** get and copy to bin the reindex bat file 
ss get "$/Java2/InvokeImIndex/runBuildIndex.bat" -GL"%BUILDHOME%\RebuildTextIndex" -R  -W  -O- -Yhkon,racer9
copy "%BUILDHOME%\RebuildTextIndex\runBuildIndex.bat" "%ITSAPPHOME%\bin\RebuildTextIndex"
jar -cvf "%ITSAPPHOME%\bin\RebuildTextIndex\RebuildIndex.jar" .   > c:\temp\temp.txt



rem ECHO STEP 13.4 ???? BUILD ITSHOME/BIN APPLICATIONS - InvokeIMIndex 
REM HBK WHAT IS GOING ON IN HERE 
rem 4) ************** COMPILE InvokeIMIndex JAVA PROJECT
rem cd "%BUILDHOME%"
rem call ant buildRebuildTextIndex
rem IF ERRORLEVEL 1 pause
rem cd "%BUILDHOME%\RebuildTextIndex\bin"
rem rem **** GET HASHTREE.class for RebuildIndex.jar
rem mkdir "%BUILDHOME%\RebuildTextIndex\bin\api\results"
rem copy "%SRVLTHM_ITS%\WEB-INF\classes\api\results\HashTree.class" "%BUILDHOME%\RebuildTextIndex\bin\api\results"
rem rem **** GET UtilStrings.class for RebuildIndex.jar
rem mkdir "%BUILDHOME%\RebuildTextIndex\bin\com\indraweb\util\"
rem copy "%SRVLTHM_ITS%\WEB-INF\classes\com\indraweb\util\UtilStrings.class" "%BUILDHOME%\RebuildTextIndex\bin\com\indraweb\util"
rem 
rem jar -cvf "%ITSAPPHOME%\bin\fcrawl\fcrawl.jar" *.class 

:SKIPITSAPIBINApplications




if not "%ARG1%"=="itsharvesterclient" goto SKIPITSHarvesterClientBIN
ECHO STEP 14 BUILD ITSHARVESTCLIENT/BIN (NDFEEDER) nodefeeder APPLICATION 
rem 2) *********************************** ndfeeder nodefeeder
if exist "%BUILDHOME%\MetasearchFeeder" rmdir /S /Q    "%BUILDHOME%\MetasearchFeeder"
if exist "%BUILDHOME%\MetasearchFeeder" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\MetasearchFeeder"
if exist "%BUILDHOME%\MetasearchFeeder" goto ENDPROCERROR
mkdir          "%BUILDHOME%\MetasearchFeeder\src"
mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses"
mkdir          "%BUILDHOME%\MetasearchFeeder\bin"
ss get "$/Java2/MetasearchFeeder/src/*.java" -GL"%BUILDHOME%\MetasearchFeeder\src" -R   -O- -Yhkon,racer9
rem **** get and copy to bin the MetasearchFeeder files 
ss get "$/Java2/MetasearchFeeder/run.*" -GL"%BUILDHOME%\MetasearchFeeder" -W   -O- -Yhkon,racer9

rem ************** COMPILE MetasearchFeeder JAVA PROJECT
  cd "%BUILDHOME%"
  call ant buildMetasearchFeeder
  IF ERRORLEVEL 1 pause
  cd "%BUILDHOME%\MetasearchFeeder\bin"
  jar -cvf "%BUILDHOME%\MetasearchFeeder\MetasearchFeeder.jar" .   > c:\temp\temp.txt

  rem ## get missing classes for metasearchfeeder project  
  mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\iw\threadmgr"
  xcopy "%BUILDHOME%\classes\com\iw\threadmgr"  "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\iw\threadmgr"

  mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\database"
  xcopy "%BUILDHOME%\classes\com\indraweb\database"  "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\database"

  mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\execution"
  xcopy "%BUILDHOME%\classes\com\indraweb\execution"  "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\execution"

  mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\util"
  xcopy "%BUILDHOME%\classes\com\indraweb\util"  "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\util"

  mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\utils"
  xcopy "%BUILDHOME%\classes\com\indraweb\utils"  "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\indraweb\utils"  /Y /Q /S

  mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\api"
  xcopy "%BUILDHOME%\classes\api"  "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\api"

  mkdir          "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\api\util"
  xcopy "%BUILDHOME%\classes\api\util"  "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\api\util" /Y /S /Q

  cd "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses"
  jar -uvf "%BUILDHOME%\MetasearchFeeder\MetasearchFeeder.jar" .   > c:\temp\temp.txt
  rem ## buildprocess side metasearchfeeder files created - now copy to itshome/bin/msfeeder
  if exist "%ITSAPPHOME%\bin\MetasearchFeeder" rmdir /S /Q "%ITSAPPHOME%\bin\MetasearchFeeder"
  if exist "%ITSAPPHOME%\bin\MetasearchFeeder" ECHO "ERROR removing %ITSAPPHOME%\bin\MetasearchFeeder"
  if exist "%ITSAPPHOME%\bin\MetasearchFeeder" goto ENDPROCERROR
  mkdir "%ITSAPPHOME%\bin\MetasearchFeeder\"
  mkdir "%ITSAPPHOME%\bin\MetasearchFeeder\com"
  rem ## seems not to want to run main out the jar file 
  xcopy "%BUILDHOME%\MetasearchFeeder\tempadditionalclasses\com\*.*" "%ITSAPPHOME%\bin\MetasearchFeeder\com" /Y /S /Q


  copy "%BUILDHOME%\MetasearchFeeder\run.*" "%ITSAPPHOME%\bin\MetasearchFeeder\" 
  copy "%BUILDHOME%\MetasearchFeeder\*.jar" "%ITSAPPHOME%\bin\MetasearchFeeder\" 
:SKIPITSHarvesterClientBIN




ECHO STEP 15.1 ************** get ITS_X web.xml 
del /F /Q "%SRVLTHM_ITS%\WEB-INF\web.xml"
if exist "%SRVLTHM_ITS%\WEB-INF\web.xml" echo "ERROR DELETING %SRVLTHM_ITS%\WEB-INF\web.xml"
if exist "%SRVLTHM_ITS%\WEB-INF\web.xml" goto ENDPROCERROR
ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/webapps/%ARG1%/WEB-INF/web.xml" -GL"%SRVLTHM_ITS%\WEB-INF" -R -W  -O- -Yhkon,racer9
if not exist "%SRVLTHM_ITS%\WEB-INF\web.xml" echo "ERROR GETTING %SRVLTHM_ITS%\WEB-INF\web.xml"
if not exist "%SRVLTHM_ITS%\WEB-INF\web.xml" goto ENDPROCERROR

del /F /Q "%SRVLTHM_ITS%\WEB-INF\web-linux.xml"
if exist "%SRVLTHM_ITS%\WEB-INF\web-linux.xml" echo "ERROR DELETING %SRVLTHM_ITS%\WEB-INF\web-linux.xml"
if exist "%SRVLTHM_ITS%\WEB-INF\web-linux.xml" goto ENDPROCERROR
ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/webapps/%ARG1%/WEB-INF/web-linux.xml" -GL"%SRVLTHM_ITS%\WEB-INF" -R -W  -O- -Yhkon,racer9


if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_nowebxml
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_nowebxml
  ECHO STEP 15.2 ************** get SERVLET web.xml and retarget web_xxx.xml to web.xml
  if exist "%WEBAPPSHOME%\servlet\WEB-INF\web.xml" del /F /Q "%WEBAPPSHOME%\servlet\WEB-INF\web.xml" 
  ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/UI/web_%ARG1%.xml" -GL"%WEBAPPSHOME%\servlet%ARG1%\WEB-INF" -R -W   -Yhkon,racer9
  rem copy the SRVLTHM_SVL web_xxx.xml file to web.xml - recall that api parm is different in each UI server type
  move "%WEBAPPSHOME%\servlet%ARG1%\WEB-INF\web_%ARG1%.xml" "%SRVLTHM_SVL%\WEB-INF\web.xml"
:SKIPNOSERVLETUIDIR_nowebxml

ECHO STEP 16 ************** get server.xml file to conf dir
del /F /Q "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml"
if exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" echo "ERROR DELETING C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml"
if exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" goto ENDPROCERROR
ss get "$/Java2/SERVLET CONTAINERS/66.134.131.35/server.xml" -GL"C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf" -R -W  -O- -Yhkon,racer9
if not exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" echo "ERROR GETTING C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml"
if not exist "C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\conf\server.xml" goto ENDPROCERROR


ECHO STEP 17 ************** version [%ARG1%] create jar files for four back end servers and two UI servers 

   if exist "%BUILDHOME%\classesthisversion" rmdir /S /Q    "%BUILDHOME%\classesthisversion"
   if exist "%BUILDHOME%\classesthisversion" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\classesthisversion"
   if exist "%BUILDHOME%\classesthisversion" goto ENDPROC
   mkdir "%BUILDHOME%\classesthisversion"

ECHO STEP 17.1 $$$$$$$$ create %ARG1%-svr.jar (back end) version 
rem $$ keep only classes from the master build that are relevant for this version 
rem itsapi - all but metasearcn
rem itsharvester - nothing but login, classify and metasearch and others required, no CQL, 
rem itsclassifier - itsharvester without the metasearch 
rem itsharvesterclient - itsharvester without the metasearch 

   rem *** ITSX-svr INCLUDE
   rem *** ts.class and api 
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-svr"
   rem @@ ts.class
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\ts.class" "%BUILDHOME%\classesthisversion\%ARG1%-svr" /Y /Q 
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-svr\api"
   rem @@ api dir 
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\api" "%BUILDHOME%\classesthisversion\%ARG1%-svr\api" /Y /Q /S

   rem *** com and sql4j 
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-svr\com"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\com" "%BUILDHOME%\classesthisversion\%ARG1%-svr\com" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-svr\sql4j"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\sql4j" "%BUILDHOME%\classesthisversion\%ARG1%-svr\sql4j" /Y /Q /S
   cd "%BUILDHOME%\classesthisversion\%ARG1%-svr"

   rem *** ITSX-svr EXCLUDE
   rem *** delete tsmetasearch content if not itsharvester
   if "%ARG1%"=="itsharvester" goto SKIPDeleteMSClasses
   if "%ARG1%"=="itsapi" goto SKIPDeleteMSClasses
     rem %%%delete tsmetasearch content if not itsharvester
     rem @@ delete api\tsmetasearchnode for all but harvesterclient
     rmdir /S /Q "%BUILDHOME%\classesthisversion\%ARG1%-svr\api\tsmetasearchnode"
   :SKIPDeleteMSClasses

   rem *** do jar for server side 	
   ECHO STEP 17.1.1 start %ARG1%-svr.jar build
   jar -cvf "%SRVLTHM_ITS%\WEB-INF\lib\%ARG1%-svr.jar"  .   > c:\temp\temp.txt


if "%ARG1%"=="itsharvester" echo Conditional - SKIP UI.jar for [%ARG1%] version 
if "%ARG1%"=="itsclassifier" echo Conditional - SKIP UI.jar for [%ARG1%] version
if "%ARG1%"=="itsharvester" goto SKIPNOSERVLETUIDIR_noUIjar
if "%ARG1%"=="itsclassifier" goto SKIPNOSERVLETUIDIR_noUIjar
ECHO STEP 17.2 $$$$$$$$ create %ARG1%-ui (front end) version 

   rem *** ITSX-ui INCLUDE
   rem *** com and sql4j 
   rem $$ jar  itsapi UI jar file 
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\Main.class" "%BUILDHOME%\classesthisversion\%ARG1%-ui" /Y /Q 
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\api"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\api\*.*" "%BUILDHOME%\classesthisversion\%ARG1%-ui\api" /Y /Q 
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\com"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\com" "%BUILDHOME%\classesthisversion\%ARG1%-ui\com" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\Crawl"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\Crawl" "%BUILDHOME%\classesthisversion\%ARG1%-ui\Crawl" /Y /Q  /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\HTML"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\HTML" "%BUILDHOME%\classesthisversion\%ARG1%-ui\HTML" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\Idrac"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\Idrac" "%BUILDHOME%\classesthisversion\%ARG1%-ui\Idrac" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\Logging"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\Logging" "%BUILDHOME%\classesthisversion\%ARG1%-ui\Logging" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\People"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\People" "%BUILDHOME%\classesthisversion\%ARG1%-ui\People" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\ROC"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\ROC" "%BUILDHOME%\classesthisversion\%ARG1%-ui\ROC" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\Server"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\Server" "%BUILDHOME%\classesthisversion\%ARG1%-ui\Server" /Y /Q /S
   mkdir "%BUILDHOME%\classesthisversion\%ARG1%-ui\Taxonomy"
   xcopy "%SRVLTHM_ITS%\WEB-INF\classes\Taxonomy" "%BUILDHOME%\classesthisversion\%ARG1%-ui\Taxonomy" /Y /Q /S

   rem *** ITSX-ui EXCLUDE


   cd "%BUILDHOME%\classesthisversion\%ARG1%-ui"
   ECHO STEP 17.2.1 start %ARG1%-ui.jar build
   jar -cvf "%SRVLTHM_SVL%\WEB-INF\lib\%ARG1%-ui.jar" . > c:\temp\temp.txt

rem **** create the UI-specific copy from the specific to the generic webapp\servlet folder
rmdir /S /Q    "%WEBAPPSHOME%\servlet"
if exist "%WEBAPPSHOME%\servlet" ECHO "ERROR removing C:\Program Files\Apache\jakarta-tomcat-4.1.24-LE-jdk14\webapps\servlet"
if exist "%WEBAPPSHOME%\servlet" goto ENDPROC
mkdir "%WEBAPPSHOME%\servlet"
mkdir "%WEBAPPSHOME%\servlet\classes"
xcopy "%WEBAPPSHOME%\servlet%ARG1%" "%WEBAPPSHOME%\servlet" /Y /Q /S




:SKIPNOSERVLETUIDIR_noUIjar

rem rem $$ jar servlet server jar file 
rem mkdir "%BUILDHOME%\classesthisversion\servlet%ARG1%"
rem jar -cvf "%SRVLTHM_ITS%\WEB-INF\lib\%ARG1%.jar"  "%SRVLTHM_ITS%\WEB-INF\classes  > c:\temp\temp.txt

rem $$$$$$$$ itsharvesterclient version 
rem if exist "%BUILDHOME%\classesthisversion" rmdir /S /Q    "%BUILDHOME%\classesthisversion"
rem if exist "%BUILDHOME%\classesthisversion" ECHO "ERROR removing C:\Documents and Settings\Indraweb\buildprocess\classesthisversion"
rem if exist "%BUILDHOME%\classesthisversion" goto ENDPROC
rem mkdir "%BUILDHOME%\classesthisversion"
rem xcopy "%SRVLTHM_ITS%\WEB-INF\classes\*.class" "%BUILDHOME%\classesthisversion" /Y /Q /S
rem jar -cvf "%SRVLTHM_ITS%\WEB-INF\lib\%ARG1%.jar"  "%SRVLTHM_ITS%\WEB-INF\classes  > c:\temp\temp.txt


cd "%BUILDHOME%\classesthisversion"

rem API SIDE 
rem if exist "%SRVLTHM_ITS%\WEB-INF\lib\%ARG1%.jar" del "%SRVLTHM_ITS%\WEB-INF\lib\%ARG1%.jar" 
rem jar -cvf "%SRVLTHM_ITS%\WEB-INF\lib\%ARG1%.jar"  "%SRVLTHM_ITS%\WEB-INF\classes  > c:\temp\temp.txt
rem rem now delete what not needed from the 
rem rem UI SIDE 
rem if exist "%SRVLTHM_SVL%\WEB-INF\lib\%ARG1%UI.jar" del "%SRVLTHM_SVL%\WEB-INF\lib\%ARG1%UI.jar" 


:STEPEIGHTEEN
ECHO STEP 18 ************** DELETE ITSHOME CLASS FILES SINCE JARRED ALREADY
cd "%BUILDHOME%"
rmdir /Q /S    "%SRVLTHM_ITS%\WEB-INF\classes"
if exist "%SRVLTHM_ITS%\WEB-INF\classes" ECHO ERROR removing "%SRVLTHM_ITS%\WEB-INF\classes"
if exist "%SRVLTHM_ITS%\WEB-INF\classes" goto ENDPROC
mkdir          "%SRVLTHM_ITS%\WEB-INF\classes"


rem ***********************************  (NOT FOR PRODUCTION BUILD)
ECHO STEP 19 ************** RESTORE SIG FILES 
rem ***********************************  restore sig files (NOT FOR PRODUCTION BUILD)
copy         "C:\temp\corpussource\sig*.txt" "%ITSAPPHOME%\corpussource\"

ECHO STEP 20 ************** RESTORE LOG FILES 
rem ***********************************  restore log files (NOT FOR PRODUCTION BUILD)
copy         "C:\temp\logs\*.*" "%ITSAPPHOME%\logs\"


echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BUILD COMPLETED 

goto endproc
:ENDPROCERROR
echo "BUILD ERROR : LOOK JUST ABOVE FOR ERRORS AND WARNINGS"
PAUSE

:ENDPROC
cd "%BUILDHOME%"
echo "BUILD COMPLETE - No apparent errors : but LOOK ABOVE FOR ERRORS AND WARNINGS"
time /T

