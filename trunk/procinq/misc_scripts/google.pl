#!C:\PERL\BIN\PERL

###################
#
# google.pl 
#
# Written by Michael Puscar, 11/17/2004
# Copyright Intellisophic Corporation
#
# This program will retrieve all links in the Google document repository 
# for a given web site.  This is done by breaking queries into finite, daily
# date ranges.  Though undocumented, date range queries are accepted by Google
# and use the Julian calendar.   Results are stored in a tab delimited format.
#
###################

use LWP::UserAgent;
use HTTP::Request;
use Getopt::Long;

# These variables are used to extract the Julian calendar
my @d = qw / 0 31 28 31 30 31 30 31 31 30 31 30 31 /;
my @e = ();
$e[0] = 0;
  for (1..12) {
    $e[$_] = $e[$_-1] + $d[$_-1];       # days in previous months
  }
  @d = qw / Mon Tue Wed Thu Fri Sat Sun /;
  my ($y,$m,$d) = ();

# The %urls hash stores all previously spidered links to ensure that there
# are no duplicate links.
local (%urls);

# command line arguments, only the "site" variable is required
local ($site) = "";
local ($startyear) = 1999;
local ($startmonth) = 1;
local ($startday) = 1;
local ($dedup) = "true";
local ($outfile) = "";
local ($wildcard) = "";

&GetOptions ("site=s" => \$site,
             "dedup=s" => \$dedup,
             "wildcard=s" => \$wildcard,
             "outfile=s" => \$outfile,             
	     "year=i" => \$startyear,
	     "month=i" => \$startmonth,
	     "day=i" => \$startday);

if ($site eq "") { die "Usage: google.pl -site <site url> -dedup true/false -year <start year> -month <start month> -day <start day>\n"; }
if ($outfile eq "") { $outfile = "$site-google.txt"; }

# if dedup is true, load previous crawl and do not grab any new data
if ($dedup eq "true") {
   print "Loading previous crawl data to avoid duplicates ...";
   my ($deduploop) = 0;

   open (FILE, "$outfile"); local (@lines) = <FILE>; close(FILE);
   foreach $line (@lines) {
      my ($fileurl, $filedate, $filetitle) = split(/\t/, $line);
      $urls{$fileurl} = $filetitle; $deduploop++; if (($deduploop % 25) == 0) { print "."; }
   }

   undef @lines;
   print "\nDone.\n\n";
}

# loop through each month from the prespecified start date until January 2005 
# and store the unique URL list in a hash table
for ($year = $startyear; $year < 2005; $year++) {
   for ($month = 1; $month < 13; $month++) {
      if (($month < $startmonth) && ($year == $startyear)) { $month = $startmonth; }
      for ($day = 1; $day < 29; $day = $day + 1) {
         if (($day < $startday) && ($year == $startyear) && ($month == $startmonth)) { $day = $startday; }

         local ($nd) = $day + 1;  local ($nm) = $month; local ($ny) = $year;
         if ($nd > 27) { $nd = 1; $nm++; }
         if ($nm > 12) { $nm = 1; $ny++; }    

         my $julianb = &julian($year, $month, $day);
         my $juliana = &julian($ny, $nm, $nd);

         my ($url) = "http://www.google.com/search?num=100&hl=en&lr=&as_qdr=all&q=daterange%3A$julianb-$juliana+site%3A$site&filter=0";
         if ($wildcard ne "") { $url = "http://www.google.com/search?num=100&hl=en&lr=&as_qdr=all&q=daterange%3A$julianb-$juliana+site%3A$site+$wildcard&filter=0"; }

         &fill_hash($url);
	 sleep(1);
      }
   }
}

# loop through each set of 100 results for this query until we have attained all results
sub fill_hash {
   my $url = $_[0];
   my $loop = 0; my $total = 10000;

   for ($i = 0; $i < $total; $i = $i + 100) {
      my $tmp = $url;
      if ($i != 0) { $tmp = $url . "&start=" . $i; }

      #print $tmp."\n";

      local ($success) = 0; local ($ua, $r, $resp);

      while ($success == 0) {
         $ua = LWP::UserAgent->new(agent => 'Mozilla/4.0 (compatible; MSIE 5.5; Windows 98)');
         $r = HTTP::Request->new("GET", "$tmp");
         $resp = $ua->request($r);

         if ($resp->is_success) { $success = 1; }
         else { warn $resp->error_as_HTML; }
      }

      my ($data) = $resp->content();
      if ($data =~ /<\/b> of <b>(.*?)<\/b> from/i) { $total = $1; }
      if ($data =~ /of about <b>(.*?)<\/b> from/i) { $total = $1; }

      undef $ua; undef $r; undef $resp;
    
      $total =~ s/\,//gi;

      if ($i > $total) { print "Found $loop results from $month-$day-$year to $nm-$nd-$ny (total: $total)\n"; return; }
      if ($total == 10000) { print "No results found from $month-$day-$year to $nm-$nd-$ny\n"; return; }

      if ($total > 1000) { 
         warn "Warning: date range $month-$day-$year to $nm-$nd-$ny has more than 1000 results ($total)."; $total = 1000; 
         $url =~ s/\&filter=0//gi;
      }      

      ### walk through the HTML and extract the links
      $data =~ s/\n//gi; $end = 0;

      while ($end == 0) {
	 # <a href=http://erstest.emea.eu.int/vet/eudbms02.htm onmousedown="return clk(this,'res',101)">Eudra Vigilance</a>
         $data =~ s/\s*href=([^>\" ]+) onmousedown=\"*?.*?>{1}?(.*?)(<\/a>{1}?)//i; 
         #$data =~ s/\s*href=(.*?)onmousedown=\"return clk//i; 
         #$data =~ s/\s*href\s*=\s*\'*?([^>\" ]+)\'*?.*?>{1}?(.*?)(<\/a>{1}?)//i; 
         my $u = $1; my $t = $2;
         if ($u eq "") { $end = 1; }
         else { 
            if ($u =~ /$site/i) { 
               if (!defined($urls{$u})) {
                  $loop++; #print "URL: $u Title: $t Total: $total I: $i Loop: $loop\n"; 
                  $urls{$u} = 1;

                  open (FILE, ">>$outfile"); print FILE "$u\t$nm-$nd-$ny\t$t\n"; close(FILE);
               }               
            }
         }

         undef $u; undef $t;
      }
       
      print "Found $loop results from $month-$day-$year to $nm-$nd-$ny (total: $total)\n";
   }

}

  sub julian {
    my ($y,$m,$d) = @_;
    my $e = 0;
    if ($m < 3) {
      $y--;
      $e = 365;
    }
    my $c = &div($y,100);
    my $g = $y%100;
       $e += $e[$m];
    return 1721060+$d+$e+365*$y+&div($c,4)+24*$c+int($g/4);
  }

  sub div {
    my ($a,$b) = @_;
    my $x = int $a / $b;
    if ($a < 0) {                       # negative correction
      $x -=  $a % $b != 0
    }
    return $x;
  }

1;