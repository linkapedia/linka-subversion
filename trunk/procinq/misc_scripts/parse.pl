#!/usr/bin/perl

use DBI;

open (FILE, "def.xml"); @lines = <FILE>; close(FILE);
local $agent = 0; local $category = 0; local $sing_cat = 0; local $engines = 0;
local ($id) = ""; local ($title) = ""; local ($path) = "";
local (%category_hash); local (%engine_hash);

local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9");

foreach $line (@lines) {
    # Get Agents
    if ($line =~ /<agent>/) { $agent = 1; }
    if (($agent == 1) && ($line =~ /<id>(.*)<\/id>/)) { $id = $1; }
    if (($agent == 1) && ($line =~ /<title>(.*)<\/title>/)) { $title = $1; }
    if (($agent == 1) && ($line =~ /<\/agent>/)) { 
	$agent = 0;
	my ($agentid) = $id+10000;
	$category_hash{$agentid} = $title;
	my $q = "Insert INTO SearchCategory VALUES ($agentid, '$title', -1)";
	print "$q\n";
	my $sth = $dbh->prepare($q) || die "query ($q) failed: $!\n";
	$sth->execute() || die "query ($q) failed: $!\n";      
	print "Agent: $title ID: $id Parent: -1\n";
    }
    
    # Get Categories
    if (($agent == 1) && ($line =~ /<categories>/)) { $category = 1; }
    if (($category == 1) && ($line =~ /<category default=\"(.*)\" sequence=\"(.*)\" id=\"(.*)\" origin=\"(.*)\">(.*)<\/category>/i)) {
	my ($cid) = $3; my ($ctitle) = $5;
	if (!defined($category_hash{$cid})) {
	    $category_hash{$cid} = $ctitle;
	    print "Set category_hash $cid = $ctitle\n";
	    my $q = "Insert INTO SearchCategory VALUES ($cid, '$ctitle', $id)"; print "$q\n";
	    my $sth = $dbh->prepare($q) || die "query ($q) failed: $!\n";
	    $sth->execute() || die "query ($q) failed: $!\n";      
	    print "Category: $ctitle ID: $cid Parent: $id\n";
	}
    }
    if ($line =~ /<\categories>/) { $category = 0; }
    
    # Get Engines
    if (($agent == 0) && ($line =~ /<category>/)) { $sing_cat = 1; }
    if (($agent == 0) && ($line =~ /<\/category>/)) { $sing_cat = 0; }
    if (($sing_cat == 1) && ($line =~ /<id>(.*)<\/id>/)) { $id = $1; }
    if (($sing_cat == 1) && ($line =~ /<engine default=\"(.*)\" sequence=\"(.*)\" id=\"(.*)\">(.*)<\/engine>/)) {
	local ($newid) = $3;
	if ((!defined($engine_hash{$newid})) || ($engine_hash{$newid} eq "")) { $engine_hash{$newid} = $id; }
	else { local (@tmp) = split(/\|\|/, $engine_hash{$newid});
	       local ($loop) = 0; 
	       foreach $tmp (@tmp) { if ($tmp eq $id) { $loop = 1; } }
	       if ($loop == 0) { print "Adding $id to ".$engine_hash{$newid}."\n"; $engine_hash{$newid} = $engine_hash{$newid}."||".$id; }
        }
    }
    
    if ($line =~ /<engine-id>(.*)<\/engine-id>/) { $id = $1; }
    if ($line =~ /<engine-title>(.*)<\/engine-title>/) {
	my ($name) = $1;
	my (@tmp) = split(/\|\|/, $engine_hash{$id});
	foreach $tmp (@tmp) {
	if ($tmp ne "DONE") {
	    my $q = "Insert INTO Channel VALUES ($id, '$name', 1)"; print "$q\n";
	    my $sth = $dbh->prepare($q) || warn "query ($q) failed: $!\n";
	    $sth->execute() || warn "query ($q) failed: $!\n";      
	    if ($category_hash{$tmp} ne "") {
		my $q = "Insert INTO CategoryChannel VALUES ($tmp, $id)"; print "$q\n";
		my $sth = $dbh->prepare($q) || die "query ($q) failed: $!\n";
		$sth->execute() || die "query ($q) failed.  Category: ".$category_hash{$tmp}." $!\n";      
		$engine_hash{$id} = "DONE";
		print "Engine: $name ID: $id Category: ".$category_hash{$tmp}." ($tmp)\n";
	    } else { print "Category not defined: $tmp\n"; }
	}}}
}

$dbh->disconnect;
undef $dbh;
1;

