import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

public class Example {
	public static void main(String args[]) {

		try {
            // LOGIN routine takes a USERID and a PASsWORD
            ITS its = new ITS();
            its.SetAPI("http://68.163.92.229:8888/"); //"http://woti.tzo.com:8888/");

            // LOGIN
            User u = its.Login("sn=cifaadmin,ou=users,dc=cifanet", "racer9");

			// Login successful.  Get the session key and put into args for future arguments
			String SKEY = u.getSessionKey();
            its.SetSessionKey(SKEY);

			System.out.println("Login successful.  Your session key is "+SKEY);

            /*
            // Test each of the API functions out
            System.out.println("Testing getFolders ..");
            Hashtable htF = its.getFolders();
            Enumeration eF = htF.keys();
            while (eF.hasMoreElements()) {
                String sKey = (String) eF.nextElement();
                String sVal = (String) htF.get(sKey);

                System.out.println("GenreID: "+sVal+" GenreName: "+sKey);
            }

            System.out.println("\nTesting getCorpusRoot ..");
            Node n = its.getCorpusRoot("5");
            System.out.println("ID: "+ n.getID() +
                               " Corpus: "+ n.getCorpusID() +
                               " Title: "+ n.getTitle() +
                               " Parent: "+ n.getParent() +
                               " Depth: "+ n.getDepth() +
                               " Index: "+ n.getIndex());

            System.out.println("\nTesting getCorpora ..");
            Vector vC = its.getCorpora();
            Enumeration eVC = vC.elements();
            while (eVC.hasMoreElements()) {
                Corpus c = (Corpus) eVC.nextElement();
                System.out.println("ID: "+c.getID() +
                                   " Name: "+c.getName() +
                                   " Desc: "+c.getDescription() +
                                   " Active: "+c.getActive());
            }

            System.out.println("\nTesting getNodeProps ..");
            n = its.getNodeProps("1315984");
            System.out.println("ID: "+ n.getID() +
                               " Corpus: "+ n.getCorpusID() +
                               " Title: "+ n.getTitle() +
                               " Parent: "+ n.getParent() +
                               " Depth: "+ n.getDepth() +
                               " Index: "+ n.getIndex());

            System.out.println("\nTesting getNodeTree ..");
            Vector v = its.getNodeTree("250577");
            Enumeration eV = v.elements();
            while (eV.hasMoreElements()) {
                Node nT = (Node) eV.nextElement();
                System.out.println("ID: "+ nT.getID() +
                                   " Corpus: "+ nT.getCorpusID() +
                                   " Title: "+ nT.getTitle() +
                                   " Parent: "+ nT.getParent() +
                                   " Depth: "+ nT.getDepth() +
                                   " Index: "+ nT.getIndex());
            }
            */
            System.out.println("\nTesting getRelatedNodes ..");
            Vector vDD = its.getRelatedNodes("2053", "");
            Enumeration eVDD = vDD.elements();
            while (eVDD.hasMoreElements()) {
                NodeDocument nd = (NodeDocument) eVDD.nextElement();
                System.out.println("ID: "+ nd.getID() +
                                   " Corpus: "+ nd.getCorpusID() +
                                   " Summary: "+ nd.getSummary() +
                                   " Title: "+ nd.getTitle() +
                                   " Parent: "+ nd.getParent() +
                                   " Depth: "+ nd.getDepth() +
                                   " Score: "+ nd.getScore() +
                                   " Index: "+ nd.getIndex());
            }
            /*

            System.out.println("\nTesting getDocProps ..");
            Document d = its.getDocProps("1907");
            System.out.println("ID: "+d.getID() +
                               " Genre: "+d.getGenreID() +
                               " Title: "+d.getTitle() +
                               " Abstract: "+d.getAbstract() +
                               " URL: "+d.getDocURL() +
                               " DateLastFound: "+d.getDateLastFound()+
                               " Visible: "+d.getVisible()+
                               " Author: "+d.getAuthor()+
                               " Source: "+d.getSource()+
                               " Citation: "+d.getCitation()+
                               " Sortdate: "+d.getSortDate()+
                               " Showdate: "+d.getShowDate()+
                               " Submitted: "+d.getSubmittedBy()+
                               " Reviewdate: "+d.getReviewDate()+
                               " Reviewed: "+d.getReviewedBy()+
                               " ReviewStatus: "+d.getReviewStatus()+
                               " Comments: "+d.getComments()+
                               " Updateddate: "+d.getUpdatedDate()+
                               " UpdatedBy: "+d.getUpdatedBy());

            System.out.println("\nTesting getDocPropsByURL ..");
            d = its.getDocPropsByURL("/home/dfi/fcrawl/6highdocrepo/11/8c/http:www.cbaci.org:80/PDFCDCFinalReport.woti-doc.htm");
            System.out.println("ID: "+d.getID() +
                               " Genre: "+d.getGenreID() +
                               " Title: "+d.getTitle() +
                               " Abstract: "+d.getAbstract() +
                               " URL: "+d.getDocURL() +
                               " DateLastFound: "+d.getDateLastFound()+
                               " Visible: "+d.getVisible()+
                               " Author: "+d.getAuthor()+
                               " Source: "+d.getSource()+
                               " Citation: "+d.getCitation()+
                               " Sortdate: "+d.getSortDate()+
                               " Showdate: "+d.getShowDate()+
                               " Submitted: "+d.getSubmittedBy()+
                               " Reviewdate: "+d.getReviewDate()+
                               " Reviewed: "+d.getReviewedBy()+
                               " ReviewStatus: "+d.getReviewStatus()+
                               " Comments: "+d.getComments()+
                               " Updateddate: "+d.getUpdatedDate()+
                               " UpdatedBy: "+d.getUpdatedBy());

             System.out.println("\nTesting getDocPropsByURL ..");
            d = its.getDocPropsLikeURL("/home/dfi/fcrawl/%%");
            System.out.println("ID: "+d.getID() +
                               " Genre: "+d.getGenreID() +
                               " Title: "+d.getTitle() +
                               " Abstract: "+d.getAbstract() +
                               " URL: "+d.getDocURL() +
                               " DateLastFound: "+d.getDateLastFound()+
                               " Visible: "+d.getVisible()+
                               " Author: "+d.getAuthor()+
                               " Source: "+d.getSource()+
                               " Citation: "+d.getCitation()+
                               " Sortdate: "+d.getSortDate()+
                               " Showdate: "+d.getShowDate()+
                               " Submitted: "+d.getSubmittedBy()+
                               " Reviewdate: "+d.getReviewDate()+
                               " Reviewed: "+d.getReviewedBy()+
                               " ReviewStatus: "+d.getReviewStatus()+
                               " Comments: "+d.getComments()+
                               " Updateddate: "+d.getUpdatedDate()+
                               " UpdatedBy: "+d.getUpdatedBy());

            System.out.println("\nTesting editNodeDocument ..");
            its.editNodeDocument("1630", "1087139", "/home/dfi/fcrawl/6highdocrepo/78/4/http:hld.sbccom.army.mil:80/faq.woti-doc.htm",
                    "Homeland Defense: Frequently Asked Questionz", "Testing Doc Summary Change", "100");

            System.out.println("\nTesting CQL (node call) ..");
            Vector vED = its.CQL("SELECT <NODE> WHERE NODETITLE LIKE 'Medicine'", 1, 20);
            Enumeration eVD = vED.elements();
            while (eVD.hasMoreElements()) {
                Node nVD = (Node) eVD.nextElement();
                System.out.println("Title: "+nVD.getTitle());
            }

 */
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage());
            e.printStackTrace(System.out);
		}
    }
}
