/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import java.util.Vector;
import java.net.URLEncoder;

public class NodeDocument extends Node {
    // User attributes
    private String DocSummary;
    private String Score;

    // constructor(s)
    public NodeDocument (HashTree ht) {
        setID((String) ht.get("NODEID"));
        setCorpusID((String) ht.get("CORPUSID"));
        setTitle((String) ht.get("NODETITLE"));
        setParent((String) ht.get("PARENTID"));
        setIndex((String) ht.get("INDEXWITHINPARENT"));
        setDepth((String) ht.get("DEPTHFROMROOT"));
        setDateScanned((String) ht.get("DATESCANNED"));
        setDateUpdated((String) ht.get("DATEUPDATED"));

        setSummary((String) ht.get("DOCSUMMARY"));
        setScore((String) ht.get("SCORE"));
    }
    public NodeDocument (Vector v) {
        setID((String) v.elementAt(0));
        setCorpusID ((String) v.elementAt(1));
        setParent((String) v.elementAt(4));
        setTitle((String) v.elementAt(2));
        setSummary((String) v.elementAt(3));
        setIndex((String) v.elementAt(5));
        setDepth((String) v.elementAt(6));

        setDateScanned((String) v.elementAt(7));
        setDateUpdated((String) v.elementAt(8));
        setScore((String) v.elementAt(9));
    }

    // accessor functions
    public String getSummary() { return DocSummary; }
    public String getScore() { return Score; }

    // accessor functions with encoding
    public String getEncodedSummary() { return URLEncoder.encode(DocSummary); }

    public void setSummary(String Summary) { this.DocSummary = Summary; }
    public void setScore(String Score) { this.Score = Score; }

    // the following three static functions are used to sort NodeDocument vectors by score
     public static void qs(Vector v) {

	  Vector vl = new Vector();                      // Left and right sides
	  Vector vr = new Vector();
	  NodeDocument el;
	  int    key;                                    // key for splitting

	  if (v.size() < 2) return;                      // 0 or 1= sorted

	  NodeDocument nd = (NodeDocument) v.elementAt(0);
	  key = new Integer(nd.getScore()).intValue();               // 1st element key

      // Start at element 1
      for (int i=1; i < v.size(); i++) {
         el = (NodeDocument) v.elementAt(i);
	     if (new Integer(el.getScore()).intValue() < key) vr.addElement(el);// Add to right
	     else vl.addElement(el);                     // Else add to left
	  }

	  qs(vl);                                        // Recursive call left
	  qs(vr);                                        //    "        "  right
	  vl.addElement(v.elementAt(0));

	  addVect(v, vl, vr);
	}

	// Add two vectors together, into a destination Vector
	private static void addVect( Vector dest, Vector left, Vector right ) {

	   int i;
	   dest.removeAllElements();                     // reset destination

	   for (i = 0; i < left.size(); i++) dest.addElement(left.elementAt(i));
	   for (i = 0; i < right.size(); i++) dest.addElement(right.elementAt(i));
	}
}
