import java.util.*;
import java.io.*;
import java.sql.*;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

public class VectorTree extends Vector {
	private VectorTree vParent;

	// Accessor and set methods for new Hashtable attribute, htParent..
	public VectorTree GetParent() { return vParent; }
	public void SetParent(VectorTree vParent) {
		this.vParent = vParent;
	}

	// Another constructor
	public VectorTree () {}
}
