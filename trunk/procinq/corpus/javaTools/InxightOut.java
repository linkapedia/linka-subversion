// MainTest

import com.iw.system.*;

import java.util.*;
import java.io.*;
import java.net.*;

public class InxightOut {
    public static int corpusID = 3;
    public static Node rootNode = null;
    public static Hashtable commonWords = new Hashtable();

    public static void main(String args[]) throws Exception {
        //System.out.println("This program will create a topic map in Inxight's expected format.\n");

        // Step 1. Initialize
        ProcinQ server = new ProcinQ();
        server.setAPI("192.168.0.223");

        // Step 2. Log into the server
        //System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        //System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File("C:\\Program Files\\ITS\\commonwords.txt"))));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { commonWords.put(record.toLowerCase(), "1"); }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file commonwords.txt");
            return;
        }

        /*
          Here is the drill: Select the parent node in this taxonomy.   From there, proceed to do a depth-first
          search throughout the taxonomy until all nodes are found.
        */

        // select root node
        try { rootNode = server.getCorpusRoot(corpusID+""); }
        catch (Exception e) { System.err.println("No root node found for corpus "+corpusID); return; }

        FileOutputStream fos = new FileOutputStream(new File("gulf-rand.xml"));
        Writer out = new OutputStreamWriter(fos, "UTF8");

        // *** WRITE THE HEADER HERE
        out.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        out.write("<inxight-taxonomy xmlns=\"http://www.inxight.com/taxonomy/4.0\" xmlns:st=\"http://www.inxight.com/taxonomy/startree/4.0\" name=\"\">\n");
        out.write("        <st:display-properties layout=\"radial\" clockwise=\"true\" stretchfactor=\"1.0\" style=\"-1\" ");
        out.write("backgroundcolor=\"0xffffff\" selectioncolor=\"0x2cd300\" highlightcolor=\"0x2cd300\" ");
        out.write("textsizemode=\"fittonodearea\" maxchars=\"30\" nchars=\"13\" nodearrangement=\"top\" nodelabelalignment=\"center\">\n");
        out.write("          <st:textfont name=\"dialog\" size=\"12\" bold=\"false\" italic=\"false\"></st:textfont>\n");
        out.write("     </st:display-properties>\n");

        // *** WRITE ROOT NODE INFORMATION HERE
        out.write("        <node>\n");
        out.write("           <label>"+rootNode.get("NODETITLE")+"</label>\n");
        out.write("           <description>"+rootNode.get("NODEDESC")+"</description>\n");
        out.write("           <hierarchy enforce-parent=\"false\" aggregate=\"false\"></hierarchy>\n");
        out.write("           <attributes>\n");
        out.write("              <threshold>0.0</threshold>\n");
        out.write("           </attributes>\n");
        out.write("           <st:node-display-properties nodecolor=\"0xff0033\" textcolor=\"0xffffff\"></st:node-display-properties>\n");

        // *** NOW LOOP, FOR EACH CHILD, DRILL DOWN INTO THAT CHILD DEPTH FIRST.  THIS IS A RECURSIVE FUNCTION
        buildChildren(server, rootNode, out);

        out.write("        </node>\n");
        out.write("</inxight-taxonomy>\n");

        out.close();
    }

    private static void buildChildren(ProcinQ server, Node p, Writer out) throws Exception {
        Vector vNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = "+corpusID+" AND PARENTID = "+p.get("NODEID")+" ORDER BY NODEINDEXWITHINPARENT ASC", 1, 5000);
        if (vNodes.size() < 1) return;

        for (int i = 0; i < vNodes.size(); i++) {
            Node n = (Node) vNodes.elementAt(i);
            Vector vSignatures = server.getNodeSignatures(n);
            Signatures signatures = new Signatures(vSignatures);

            String spaces = "           ";
            for (int j = 0; j < Integer.parseInt(n.get("DEPTHFROMROOT")); j++) { spaces = spaces + " "; }

            out.write(spaces+"<link>\n");
            out.write(spaces+"   <st:link-display-properties linkcolor=\"0x808080\"></st:link-display-properties>\n");
            out.write(spaces+"   <node>\n");
            out.write(spaces+"      <label>"+n.get("NODETITLE")+"</label>\n");
            out.write(spaces+"      <description>"+n.get("NODEDESC")+"</description>\n");
            out.write(spaces+"      <hierarchy enforce-parent=\"false\" aggregate=\"false\"></hierarchy>\n");
            out.write(spaces+"      <attributes>\n");

            if (vSignatures.size() == 0) out.write(spaces+"         <terms></terms>\n");
            else out.write(spaces+"         <terms>"+signatures+"</terms>\n");

            out.write(spaces+"         <xdocs></xdocs>\n");

            if (vSignatures.size() == 0) out.write(spaces+"         <filter></filter>\n");
            else out.write(spaces+"         <filter>"+returnWords(n.get("NODETITLE"))+" | "+
                    returnWords(p.get("NODETITLE"))+"</filter>\n");

            out.write(spaces+"         <threshold>0.1</threshold>\n");

            // put the title into the filter frame and the rest as terms
            out.write(spaces+"         <rule></rule>\n");

            String color = "0x66ff66";
            switch (Integer.parseInt(n.get("DEPTHFROMROOT"))) {
                case 1: color = "0x66ff66"; break;
                case 2: color = "0xFF8080"; break;
                case 3: color = "0x00C0C0"; break;
                case 4: color = "0x4040FF"; break;
                case 5: color = "0xC0C0FF"; break;
                case 6: color = "0xFFFF40"; break;
                case 7: color = "0xC0FFC0"; break;
                case 8: color = "0xFF00FF"; break;
            }

            out.write(spaces+"      </attributes>\n");
            out.write(spaces+"      <st:node-display-properties nodecolor=\""+color+"\" textcolor=\"0x0000ff\"></st:node-display-properties>\n");

            buildChildren(server, n, out);

            out.write(spaces+"   </node>\n");
            out.write(spaces+"</link>\n");
        }
    }

    private static String replaceWildcards(String wild) {
        StringBuffer buffer = new StringBuffer();
        char[] chars = wild.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') buffer.append(".*"); else if (chars[i] == '?') buffer.append("."); else buffer.append(chars[i]);
        }
        return buffer.toString();
    }// end replaceWildcards method

    private static String returnWords(String Phrase) {
        StringBuffer sb = new StringBuffer("");

        String[] sArr = Phrase.split(" ");

        for (int i = 0; i < sArr.length; i++) {
            if (i > 0) sb.append(" | ");
            if (!commonWords.containsKey(sArr[i].toLowerCase())) sb.append("NOCASE(\""+replaceWildcards(sArr[i])+"\")");
        }

        return sb.toString();
    }
}