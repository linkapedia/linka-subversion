package com.iw.system;

/*
 * Sample JAVA libraries used to access the ITS API
 *
 * Written By: Michael A. Puscar, Indraweb
 * All rights reserved.
 * Date: Jul 23, 2003
 * Time: 1:35:47 PM
 */

import java.util.*;
import java.net.URLEncoder;
import java.io.PrintWriter;
import java.sql.*;

import com.iw.system.*;
import org.dom4j.Element;

public class Document implements Comparable {
    public Hashtable htd = new Hashtable();

    // constructor(s)
    public Document () {}
    public Document (HashTree ht) {
        htd = new Hashtable();

        Enumeration eN = ht.keys();
        while (eN.hasMoreElements()) {
            String key = (String) eN.nextElement();
            String val = (String) ht.get(key);

            htd.put(key, val);
        }
    }

    public Document (Iterator i) {
        if (htd == null) htd = new Hashtable();
        while (i.hasNext()) {
            Element e = (Element) i.next();
            htd.put(e.getName(), e.getText());
        }
    }

    public Document (ResultSet rs ) throws Exception {
        ResultSetMetaData rsmd = rs.getMetaData();

        for (int i = 1; i <= rsmd.getColumnCount(); i++)
            if (rs.getString(i) != null)
                htd.put(rsmd.getColumnName(i), rs.getString(i));
            else htd.put(rsmd.getColumnName(i), "");
    }

    //YYYY-MM-DD HH24:MI:SS  from MM-DD-YYYY   2004-11-12 15:14:17.0
    public static String FixDate (String Date) {
        if (Date.startsWith("2004")) return Date.substring(0,19);

        //System.out.print("Before: "+Date+" ");
        if (Date.length() == 8) { // 05-19-04
            Date = 20+Date.substring(6)+"-"+Date.substring(0,2)+"-"+Date.substring(3,5)+" 12:00:00";
        } else { // 05-19-2004
            Date = Date.substring(6)+"-"+Date.substring(0,2)+"-"+Date.substring(3,5)+" 12:00:00";
        }
        //System.out.println("After: "+Date);

        return Date;
    }

    // accessor function -- 5/13/2004 only one accessor function required
    public String get(String fieldName) {
        String s = (String) htd.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }
        return s.replaceAll(" <br> ", "\n");
    }

    public void set (String fieldName, String fieldValue) { htd.put(fieldName, fieldValue); }

    // accessor functions with encoding
    public String getEncodedField(String fieldName) throws Exception {
        return URLEncoder.encode(get(fieldName), "UTF-8"); }

    public String toString() {
        String s = get("DOCTITLE"); if (s == null) return super.toString();
        return s;
    }
    public int compareTo(Object o) {
        String s = get("DOCTITLE"); if (s == null) return super.toString().compareTo(o.toString());
        return s.compareTo(o.toString());
    }
}
