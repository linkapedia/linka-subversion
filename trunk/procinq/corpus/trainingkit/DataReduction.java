// DataReduction
//
// Given a directory of files, classify them against the given corpora and write the results to a second directory

import com.iw.system.*;

import java.util.*;
import java.io.*;

public class DataReduction {
    private static String inDir = "C:/examples/exercise1/in/";
    private static String outDir = "C:/examples/exercise1/out/";

    public static void main(String args[]) throws Exception {
        System.out.println("This program demonstrates a data reduction example in the ProcinQ server.\n");

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI("192.168.0.223:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        // Step 3. Read all the files from the directory INDIR
        System.out.println("**** RETRIEVING THE LIST OF FILES TO CLASSIFY ****");
        System.out.println(" ");

        File fDirectory = new File(inDir);
		String FilePaths[] = fDirectory.list();

        System.out.println("**** BEGIN CLASSIFICATION  ****");
        System.out.println(" ");

        for (int i = 0; i < FilePaths.length; i++) {
            File f = new File(inDir+"/"+FilePaths[i]);

            Vector classifyResults = new Vector(); // classification results will be stored here
            StringBuffer sb = new StringBuffer(); // data from the file will be stored here

            // Step 4. Classify each file in the directory
            System.out.println("Classify: "+f.getAbsolutePath());
            DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

            String record = null;
            try {
                while ( (record=dis.readLine()) != null ) { sb.append(record); }
            } catch (IOException e) {
                System.err.println("There was an error while reading the file "+f.getAbsolutePath());
                return;
            }

            // Classify the document.  The server expects a string and a corpusID list.  If more than one taxonomy
            // is specified, the corpusIDs in the second argument should be separated by commas with no spaces.
            try { classifyResults = server.classify(sb.toString(), "58,70,71,72"); }
            catch (SessionExpired se) {
                System.err.println("Session expired."); return; }
            catch (ClassifyError ce) {
                System.err.println("Classification failed.");
                ce.printStackTrace(System.err); return;
            }
            // Step 5. If a document classified into 1 or more topics, write the file to OUTDIR
            if (classifyResults.size() > 0) {
                System.out.println("Success: "+f.getAbsolutePath());
                try {
                    BufferedReader in = new BufferedReader(new FileReader(f));
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outDir+"/"+FilePaths[i])));

                    String line;
                    while ((line = in.readLine()) != null) { out.println(line); }

                    out.flush(); out.close();
                } catch (IOException ioe) {
                    System.err.println("Error: I/O exception writing to: "+outDir+"/"+FilePaths[i]);

                    throw ioe;
                }
            }
        }

        System.out.println("DataReduction complete.\n");
   }
}