import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class Martindale
{
    public static long nodeID = 575201;

    public static Hashtable nodes = new Hashtable();

    // main function
    public static void main(String[] args) {
        // load in the comma separated file
        File f = new File("C:/DOCUME~1/indraweb/projects/perlscripts/martindale/Martindale.txt");

        FileInputStream fis = null;

        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        try {
            Connection connection = DriverManager.getConnection(
                        "jdbc:oracle:thin:@192.168.0.223:1521:content",
                        "sbooks", // ## fill in User here
                        "racer9" // ## fill in Password here
                );

            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            /* Read through the file and look for the following references:

                !100 LEVEL 1
                !103 LEVEL 2
                !301 LEVEL 3
                !321 NODE SOURCE
                !203 LEVEL 4
                !204 LEVEL 5
                !205 LEVEL 6
                !206 LEVEL 7
                !221 NODE SOURCE

                Preface (100)
                   Introduction to Martindale (103)
                      About Martindale (301)
                      What's in Martindale (301)
                      How the information is organised (301)
                         Drug monographs (203)
                            Nomenclature (204)
                               Atomic and molecular weights (205)
                ... etc

            */

            buf.readLine();

            boolean bWritingText = false; String sText = "";
            long parentArr[] = new long[8];
            long niwp[] = new long[8];

            parentArr[0] = 575200;
            niwp[0] = 1; niwp[1] = 1; niwp[2] = 1; niwp[3] = 1;
            niwp[4] = 1; niwp[5] = 1; niwp[6] = 1; niwp[7] = 1;

            while ((sData = buf.readLine()) != null) {
                /***************** TRACK TEXT WRITING HERE ************************/
                if (sData.startsWith("!321")) {
                    bWritingText = true;

                    Pattern r = Pattern.compile("!321(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    if (m.find()) sText = sText + m.group(1).toString()+"\n";
                }

                if (sData.startsWith("!221")) {
                    bWritingText = true;

                    Pattern r = Pattern.compile("!221(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    if (m.find()) sText = sText + m.group(1).toString()+"\n";
                }

                if (sData.startsWith("!117")) {
                    bWritingText = true;

                    Pattern r = Pattern.compile("!117(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    if (m.find()) sText = sText + m.group(1).toString()+"\n";
                }

                if ((!sData.startsWith("!")) && (bWritingText)) {
                    sText = sText + sData + "\n";
                }

                /***************** INSERT NODE LEVELS HERE ************************/
                // ************* LEVEL 1 *************
                if (sData.startsWith("!100")) {
                    Pattern r = Pattern.compile("!100(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    String title = sData;
                    if (m.find()) title = m.group(1).toString();

                    System.out.println(title);

                    if (!nodes.containsKey(title)) {
                        insertNode(nodeID, parentArr[0], 1, niwp[0], title.trim(), connection);
                        nodes.put(title, new Long(nodeID));

                        parentArr[1] = (int) nodeID; nodeID++; niwp[0]++; niwp[1] = 1;
                    } else {
                        parentArr[1] = ((Long) nodes.get(title)).intValue();
                    }
                }

                // ************* LEVEL 2 *************
                if (sData.startsWith("!103")) {
                    if (bWritingText && sData.startsWith("!")) {
                        System.out.println("*** WRITING DATA LENGTH "+sText.length()+" ***");
                        FixBlanks.insertClobToNodeData(nodeID-1, sText, connection);
                        bWritingText = false; sText = "";
                    }

                    Pattern r = Pattern.compile("!103(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    String title = sData;
                    if (m.find()) title = m.group(1).toString();

                    System.out.println("   "+title);

                    insertNode(nodeID, parentArr[1], 2, niwp[1], title.trim(), connection);
                    parentArr[2] = nodeID; nodeID++; niwp[1]++; niwp[2] = 1;
                }

                /* ************* LEVEL 3 *************
                if (sData.startsWith("!301")) {
                    if (bWritingText && sData.startsWith("!")) {
                        System.out.println("*** WRITING DATA LENGTH "+sText.length()+" ***");
                        FixBlanks.insertClobToNodeData(nodeID-1, sText, connection);
                        bWritingText = false;
                    }

                    Pattern r = Pattern.compile("!301(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    String title = sData;
                    if (m.find()) title = m.group(1).toString();

                    System.out.println("      "+title);

                    insertNode(nodeID, parentArr[2], 3, niwp[2], title.trim(), connection);
                    parentArr[3] = nodeID; nodeID++; niwp[2]++; niwp[3] = 1;
                } */

                // ************* LEVEL 4 *************
                if (sData.startsWith("!203")) {
                    if (bWritingText && sData.startsWith("!")) {
                        System.out.println("*** WRITING DATA LENGTH "+sText.length()+" ***");
                        FixBlanks.insertClobToNodeData(nodeID-1, sText, connection);
                        bWritingText = false; sText = "";
                    }

                    Pattern r = Pattern.compile("!203(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    String title = sData;
                    if (m.find()) title = m.group(1).toString();
                    if (title.endsWith(".")) title = title.substring(0, title.length()-1);

                    System.out.println("         "+title);

                    insertNode(nodeID, parentArr[2], 3, niwp[2], title.trim(), connection);
                    parentArr[3] = nodeID; nodeID++; niwp[2]++; niwp[3] = 1;
                }

                // ************* LEVEL 5 *************
                if (sData.startsWith("!204")) {
                    if (bWritingText && sData.startsWith("!")) {
                        System.out.println("*** WRITING DATA LENGTH "+sText.length()+" ***");
                        FixBlanks.insertClobToNodeData(nodeID-1, sText, connection);
                        bWritingText = false; sText = "";
                    }

                    Pattern r = Pattern.compile("!204(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    String title = sData;
                    if (m.find()) title = m.group(1).toString();
                    if (title.endsWith(".")) title = title.substring(0, title.length()-1);

                    System.out.println("            "+title);

                    insertNode(nodeID, parentArr[3], 4, niwp[3], title.trim(), connection);
                    parentArr[4] = nodeID; nodeID++; niwp[3]++; niwp[4] = 1;
                }

                // ************* LEVEL 6 *************
                if (sData.startsWith("!205")) {
                    if (bWritingText && sData.startsWith("!")) {
                        System.out.println("*** WRITING DATA LENGTH "+sText.length()+" ***");
                        FixBlanks.insertClobToNodeData(nodeID-1, sText, connection);
                        bWritingText = false; sText = "";
                    }

                    Pattern r = Pattern.compile("!205(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    String title = sData;
                    if (m.find()) title = m.group(1).toString();
                    if (title.endsWith(".")) title = title.substring(0, title.length()-1);

                    System.out.println("               "+title);

                    insertNode(nodeID, parentArr[4], 5, niwp[4], title.trim(), connection);
                    parentArr[5] = nodeID; nodeID++; niwp[4]++; niwp[5] = 1;
                }

                // ************* LEVEL 7 *************
                if (sData.startsWith("!206")) {
                    if (bWritingText && sData.startsWith("!")) {
                        System.out.println("*** WRITING DATA LENGTH "+sText.length()+" ***");
                        FixBlanks.insertClobToNodeData(nodeID-1, sText, connection);
                        bWritingText = false; sText = "";
                    }

                    Pattern r = Pattern.compile("!206(.*)", Pattern.CASE_INSENSITIVE);
                    Matcher m = r.matcher(sData);

                    String title = sData;
                    if (m.find()) title = m.group(1).toString();
                    if (title.endsWith(".")) title = title.substring(0, title.length()-1);

                    System.out.println("                  "+title);

                    insertNode(nodeID, parentArr[5], 6, niwp[5], title.trim(), connection);
                    parentArr[6] = nodeID; nodeID++; niwp[5]++; niwp[6] = 1;
                }
            }

            fis.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace(System.out); return;
        }

        fis = null;
    }

    private static void insertNode (long nodeid, long parent, long depth, long niwp, String title, Connection connection) throws Exception {
        Statement stmt = null;

        try {
            String query = "insert into node values ("+nodeid+",58,'"+title.replaceAll("'", "''")+"',100,"+
                parent+","+niwp+","+depth+",sysdate,sysdate,1,null,sysdate,'"+title.replaceAll("'", "''")+"',"+
                nodeid+")";

            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally { stmt.close(); stmt = null; }
    }
}
