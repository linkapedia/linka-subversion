package com.iw.system;

import java.util.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.net.URLEncoder;

public class Node {

    // attributes inherited from combining node classes
	private Vector vChildNodes = new Vector();
    private Vector vSignatures = new Vector();

    public Hashtable htn;

    // constructor(s)
    public Node () {}
    public Node (ResultSet rs) throws Exception {
        ResultSetMetaData rsmd = rs.getMetaData();

        if (htn == null) htn = new Hashtable();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            if (rs.getString(i) != null)
                htn.put(rsmd.getColumnName(i), rs.getString(i));
            else htn.put(rsmd.getColumnName(i), "");
        }
    }

    // return true or false if this node is a link
    public boolean isLink() {
        //System.out.println("is link ("+get("NODETITLE")+") NODEID ("+get("NODEID")+") LINKNODEID ("+get("LINKNODEID")+")");
        if (get("LINKNODEID") == null) return false;
        if (get("NODEID").equals(get("LINKNODEID"))) return false;

        return true;
    }

    // accessor functions
    public Vector GetChildren() { return vChildNodes; }
    public Vector getSignatures() { return vSignatures; }

    // Add a child node relationship to this node
	public Vector AddChild(int NodeID) { return AddChild(NodeID+""); }
	public Vector AddChild(String NodeID) {
		vChildNodes.addElement(NodeID);

		return vChildNodes;
	}

	// Remove a child node relationship from this node
	public Vector RemoveChild(int NodeID) { return RemoveChild(NodeID+""); }
	public Vector RemoveChild(String NodeID) {
		vChildNodes.removeElement(NodeID);

		return vChildNodes;
	}

    // accessor function -- 5/13/2004 only one accessor function required
    public String get(String fieldName) {
        String s = (String) htn.get(fieldName);

        if (s == null) { return ""; }
        if (s.equals("null")) { return ""; }
        return s.replaceAll(" <br> ", "\n");
    }
    public void set(String Key, String Value) {
        if (htn == null) htn = new Hashtable();

        if (Key.equals("ID")) { htn.put("NODEID", Value); } // some of these values are required by the SAX parser
        else if (Key.equals("BASENAMESTRING")) { htn.put("NODETITLE", Value); }
        else if (Key.equals("INSTANCEOF")) { htn.put("CORPUSID", Value); }
        else { htn.put(Key, Value); }
    }

    public Hashtable getHash() { return htn; }

    // accessor functions with encoding
    public String getEncodedField(String fieldName) throws Exception {
        return URLEncoder.encode(get(fieldName), "UTF-8"); }

    public String toString() {
        String s = get("NODETITLE"); if (s == null) return super.toString();
        return s;
    }
    public int compareTo(Object o) {
        String s = get("NODETITLE"); if (s == null) return super.toString().compareTo(o.toString());
        return s.compareTo(o.toString());
    }
}
