import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class WBcountries {
    public static int nextNode = 791553;

    // main function
    public static void main(String[] args) {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@66.134.131.62:1521:medusa",
                    "sbooks", // ## fill in User here
                    "racer9" // ## fill in Password here
            );

            System.out.println("Step 1: Load data file.");

            File f = new File("C:/IntelliJ2.5/data/bio.txt");
            FileInputStream fis = null;

            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            buf.readLine();
            while ((sData = buf.readLine()) != null) {
                if (!sData.startsWith("url:")) {
                    String[] topics = sData.split(" -> ");
                    int[] nodes = new int[10];
                    int war = 0;

                    nodes[0] = 533842;

                    for (int i = 1; i < topics.length; i++) {
                        topics[i] = topics[i].trim();
                        topics[i] = topics[i].replaceAll((char) 13 + "", "");
                        topics[i] = topics[i].replaceAll("'", "''");
                        topics[i] = topics[i].replaceAll("&", "&'||'");

                        String query = "select nodeid from node where corpusid = 49 and  " +
                                "lower(nodetitle) = lower('" + topics[i] + "')";
                        if (i != (topics.length - 1)) {
                            query = query + " and depthfromroot = " + i + " and parentid = " + nodes[i - 1];
                        }

                        Statement stmt = null;
                        ResultSet rs = null;

                        try {
                            // execute query
                            stmt = connection.createStatement();
                            rs = stmt.executeQuery(query);

                            int loop = 0;

                            while (rs.next()) {
                                loop++;
                                nodes[i] = rs.getInt(1);

                                if (loop > 1) {
                                    war = 1;
                                    System.err.println("Warning. Topic " + topics[i] + " has more than one result.");
                                }
                            }

                            if (loop == 0) {
                                nextNode++;

                                nodes[i] = nextNode;

                                System.err.print("Warning, topic does not exist: Introduction");
                                for (int j = 1; j <= i; j++) { System.err.print(" -> "+topics[j]); }
                                System.err.println(" ");

                                i = topics.length; war = 1;

                                /*
                                String query2 = "insert into node values ("+nodes[i]+", 49, '"+topics[i]+"', 100, "+nodes[i-1]+", 100, "+
                                     "3, sysdate, sysdate, 1, null, sysdate, '"+topics[i]+"', "+nodes[i]+")";
                                System.out.println(query2);

                                Statement stmt2 = null;

                                try {
                                    // execute query
                                    stmt2 = connection.createStatement();
                                    stmt2.executeUpdate(query2);
                                } catch (Exception e) {
                                    e.printStackTrace(System.out);
                                } finally {
                                    stmt2.close();
                                    stmt2 = null;
                                } */

                            }
                        } catch (java.sql.SQLException e) {
                            throw e;
                        } finally {
                            rs.close();
                            stmt.close();
                        }
                    }

                    if (war == 0) {
                        String query = "update node set depthfromroot = " + (topics.length - 1) + ", parentid = " +
                                nodes[topics.length - 2] + " where nodeid = " + nodes[topics.length - 1];
                        Statement stmt = null;

                        try {
                            stmt = connection.createStatement();
                            stmt.executeUpdate(query);
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        } finally {
                            stmt.close();
                            stmt = null;
                        }

                        System.out.println(query);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.err); return;
        }
    }
}
