import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.net.*;
import java.util.regex.*;

public class Wikipedia
{
    public static int nodeID = 564525;

    // main function
    public static void main(String[] args) {
        // load in the comma separated file
        File f = new File("C:/DOCUME~1/indraweb/projects/perlscripts/wiki/sciences.csv");

        FileInputStream fis = null;

        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        try {
            Connection connection = DriverManager.getConnection(
                        "jdbc:oracle:thin:@192.168.0.223:1521:content",
                        "sbooks", // ## fill in User here
                        "racer9" // ## fill in Password here
                );

            fis = new FileInputStream(f);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            // read through the comma separated file
            // if level 1 or 2, retrieve the content and write it into the database
            // if level 3, retrieve children as well
            int niwp = 0;

            buf.readLine();
            while ((sData = buf.readLine()) != null) {
                niwp++;

                String[] fields = sData.split(",");
                System.out.println("URL "+fields[3]);
                String data = getData(fields[3]);

                insertNode(fields, niwp, connection);
                FixBlanks.insertClobToNodeData(Long.parseLong(fields[0]), stringHTMLtoText(getTop(data)), connection);

                if (fields[2].equals("3")) {
                    Vector v = getSections(data);
                    String[] nodes = new String[5];

                    for (int i = 0; i < v.size(); i++) {
                        String s = (String) v.elementAt(i);
                        String[] section = s.split(",,,");

                        nodes[0] = nodeID+"";
                        nodes[1] = fields[0];
                        nodes[2] = 4+"";
                        nodes[3] = fields[3];
                        nodes[4] = section[0];

                        niwp++; nodeID++;
                        insertNode(nodes, niwp, connection);
                        FixBlanks.insertClobToNodeData(Long.parseLong(nodes[0]), stringHTMLtoText(section[1]), connection);
                    }
                }
            }

            fis.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace(System.out); return;
        }

        fis = null;
    }

    private static String stringHTMLtoText(String s) {
        HTMLtoTEXT parser = new HTMLtoTEXT();

        try { parser.parse(new StringReader(s)); }
        catch (Exception e) {
            //e.printStackTrace(System.out);
            String t = s.replaceAll("<(.*?)>", " ");
            return t;
        }
        return parser.getText();
    }

    private static void insertNode (String[] fields, int niwp, Connection connection) throws Exception {
        Statement stmt = null;

        try {
            System.out.println("Topic: "+fields[4]);
            String query = "insert into node values ("+fields[0]+",53,'"+fields[4].replaceAll("'", "''")+"',100,"+
                fields[1]+","+niwp+","+fields[2]+",sysdate,sysdate,1,null,sysdate,'"+fields[4].replaceAll("'", "''")+"',"+
                fields[0]+")";

            // execute query
            stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally { stmt.close(); stmt = null; }
    }

    // given a URL, retrieve the HTML and return it
    private static String getData (String URL) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");

        try {
            URL myURL = new URL(URL);
            httpCon = (HttpURLConnection) myURL.openConnection();
            //httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error: " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            DataInputStream dis = new DataInputStream(new BufferedInputStream(is));

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "html";

            // NOTE
            String inputLine; String s = "";
            while ((inputLine = dis.readLine()) != null) { s = s + inputLine+" "; }

            long lEnd = System.currentTimeMillis() - lStart;

            return s;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms. : "+e.toString());
            e.printStackTrace(System.out);
            return "";
        }
        finally { httpCon.disconnect(); }
    }

    private static String getTop(String page) {

        if (page.indexOf("<h2>") != -1) {
            Pattern p = Pattern.compile("<h1(.*?)>(.*?)<h2>", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(page); boolean bResult = m.find();

            if (bResult) {
                String s = m.group(2);
                return s;
            }
        } else {
            Pattern p = Pattern.compile("<h1(.*?)>(.*?)printfooter", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(page); boolean bResult = m.find();

            if (bResult) {
                String s = m.group(2);
                return s;
            }
        }
        return "";
    }

    private static Vector getSections(String page) {
        page = page.replaceAll("\\(", " ");
        page = page.replaceAll("\\)", " ");

        String oldTitle = "";

        // Find all <H2> and retrieve sections.  "See Also" does not count
        boolean bResult = true;
        Vector v = new Vector();

        while (bResult) {
            Pattern p = Pattern.compile("<h2>(.*?)</h2>(.*?)<h2>", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(page); bResult = m.find();

            if (bResult) {
                String sTitle = m.group(1);
                String sSection = m.group(2);

                if (oldTitle.equals(sTitle)) return v;
                oldTitle = sTitle;

                page = page.replaceAll("<h2>"+sTitle+"</h2>", " ");

                boolean bLeaveNow = false;
                if (sTitle.indexOf("href") != -1) bLeaveNow = true;

                sTitle = sTitle.replaceAll("<(.*?)>", "");
                sTitle = sTitle.replaceAll("\\(", "");
                sTitle = sTitle.replaceAll("\\)", "");

                System.out.println("  Title: "+sTitle);
                v.add(sTitle+",,,"+sSection);

                if (bLeaveNow) return v;
            }
        }

        return v;
    }
}
