
import java.sql.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

public class FixBlanks {

    public static Hashtable ht = new Hashtable();

    public static void main(String[] args) {

        try {
            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }

        if (args.length == 0) {
            System.err.println("Usage: FixBlanks <corpusid> <location>");
            return;
        }

        File fDirectory = new File(args[1]);
        fillHash(fDirectory);

        System.out.println(" ");
        System.out.println(" ");
        System.out.println("Total qualifying topics: " + ht.size());

        String title = "";
        try {

            // open connection to database
            Connection connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@192.168.0.223:1521:content",
                    "sbooks", // ## fill in User here
                    "racer9" // ## fill in Password here
            );

            Enumeration eH = ht.keys();
            while (eH.hasMoreElements()) {
                title = (String) eH.nextElement();
                String source = (String) ht.get(title);

                title = title.replaceAll("'", "''");

                String query = "SELECT nodeid FROM node WHERE corpusid = "+args[0]+" and lower(trim(nodetitle)) = lower('"+title+"')";

                // execute query
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                int nodeid = -1;

                while (rs.next()) {
                    nodeid = rs.getInt(1);
                    System.out.println("NodeID: "+nodeid);
                }

                if (nodeid == -1) { System.out.println("Could not find nodeid for title: "+title); }

                updateNodeSource(""+nodeid, source, connection);

                rs.close(); statement.close();
            }

            connection.close();

        } catch (java.sql.SQLException e) {
            System.out.println("Failed on : "+title);

            System.err.println(e);
            System.exit(-1);

        }
    }

    public static void fillHash(File fDirectory) {
        String FilePaths[] = fDirectory.list();
        if (FilePaths == null) return;

        for (int i = 0; i < FilePaths.length; i++) {
            File f = new File(fDirectory.getAbsolutePath() + "/" + FilePaths[i]);

            // if this is a directory and recurse is true, continue collecting
            if (f.isDirectory()) {
                fillHash(f);
            }

            if (!f.isDirectory()) {
                String Source = "";
                String thisLine = "";
                String Title = "";

                boolean bTitle = false;

                try {
                    BufferedReader myInput = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
                    while ((thisLine = myInput.readLine()) != null) {
                        if (bTitle) { Title = thisLine.replaceAll("\n", ""); bTitle = false; }
                        if (thisLine.indexOf("<TITLE>") != -1) { bTitle = true; }
                    }
                    myInput.close();
                    Source = getCNFData(f);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    return;
                }

                if ((Source.length() > 0) && (Source.length() < 201)) {
                    ht.put(Title, Source);
                    System.out.print(".");
                } //else { System.out.print("X"); }
            }
        }
    }

    public static void insertClobToNodeData(long lNodeID, String sText, Connection dbc) throws Exception {
        Statement stmt = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sSQL = "delete from NodeData where NodeID = " + lNodeID;
            stmt = dbc.createStatement();
            stmt.executeUpdate(sSQL);
            stmt.close();
            stmt = null;

            dbc.setAutoCommit(false);

            pstmt = dbc.prepareStatement("INSERT INTO NODEDATA (NODEID, NODESOURCE) VALUES (" + lNodeID + ", EMPTY_CLOB())");
            pstmt.execute();

            stmt = dbc.createStatement();
            rs = stmt.executeQuery("SELECT NODESOURCE FROM NODEDATA WHERE NodeID = " + lNodeID);

            if (rs.next()) {
                Clob c = rs.getClob(1);
                java.io.Writer clobWriter = ((oracle.sql.CLOB) c).getCharacterOutputStream();

                // Open the text as a stream for insertion into the Clob column
                StringReader reader = new StringReader(sText); //.replaceAll("'", "''"));

                // Buffer to hold chunks of data to being written to the Clob.
                char[] cbuffer = new char[10 * 4096];

                // Read a chunk of data from the sample input stream, and write the
                // chunk into the Clob column output stream. Repeat till file has been
                // fully read.
                int nread = 0;
                while ((nread = reader.read(cbuffer)) != -1) // Read from stream
                    clobWriter.write(cbuffer, 0, nread);          // Write to Clob

                clobWriter.flush();

                reader.close();
                clobWriter.close(); // Close both streams

                //insertClob(c, dbc);
            }

        } catch (Exception e) {
            e.printStackTrace(System.out); System.out.println("Text: "+sText);
            throw e;
        } finally {
            dbc.commit();
            dbc.setAutoCommit(true);

            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        }
    }

    public static void updateNodeSource(String sNodeID, String sSource, Connection dbc) {
        try {
            insertClobToNodeData(new Long(sNodeID).longValue(), sSource, dbc);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return;
        }
    }

    // get a stream of text from a file.  if this document is not .txt, filter it first.
    public static String getCNFData(File f) throws Exception {
        File tempFile;

        // if the file is of type text, it needs an HTML wrapper before being posted
        try {
            tempFile = HTMLtoTXT(f);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return "";
        }

        FileInputStream fis = null;
        PrintWriter out = null;
        String sOut = "";
        try {
            fis = new FileInputStream(tempFile);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String sData = new String();

            while ((sData = buf.readLine()) != null) {
                sOut = sOut + sData + "\r\n";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (fis != null) {
                fis.close();
                fis.close();
            }
        }

        return sOut;
    }

    public static File HTMLtoTXT(File HTMLfile) throws Exception {
        File tempFile = new File("temp.txt");
        FileReader in = new FileReader(HTMLfile);

        HTMLtoTEXT parser = new HTMLtoTEXT();
        parser.parse(in);
        in.close();

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileOutputStream(tempFile.getAbsolutePath(), false));
            out.print(parser.getText());
        } catch (Exception e) {
            throw e;
        } finally {
            out.close();
        }

        return tempFile;
    }
}