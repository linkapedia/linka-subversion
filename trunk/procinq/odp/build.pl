#!/usr/bin/perl

use LWP::RobotPUA;
use HTTP::Request;

open (FILES, "data.txt"); @FILELIST = <FILES>; close(FILES);

undef $pua; local ($n) = 2;

# Define the PARALLEL USER AGENT, otherwise known as THESWARM
local $pua = new LWP::RobotPUA 'CowboyFinder/0.9', 'michael@cowboysdigest.com';
$pua->remember_failures (1);
$pua->in_order  (0);
$pua->timeout   (30);
$pua->redirect  (1);
$pua->max_req ($n);
$pua->max_hosts ($n);
$pua->delay(1);

local ($count) = 0;

foreach $file (@FILELIST) {
   chop($file); my ($url, $filename) = split(/\|\|/, $file);

   my $request = new HTTP::Request('GET', $url);
   $request->header('url' => $url,
                    'filename' => $filename);
   if ( my $result = $pua->register ($request,\&fly_callback)) {
       my $error = $result->error_as_HTML."\n";
   } else { $count++; print "Registered request: $url ($filename)\n"; }
}

print "Registered $count requests to run with $n being run in parallel.\n";
print "Estimated time to completion: ";
$seconds = $count / ($n*2); $minutes = $seconds / 60; $hours = $minutes / 60;
print "$hours hours ($minutes minutes)\n";

sleep (3);
print "Starting the spider...\n";
$pua->wait(60);

1;

# This callback will be invoked as requests complete!
sub fly_callback {
    my ($content, $response, $protocol, $entry) = @_;
    print "Entering fly_callback..\n"; 

    if (length ($content) ) {
	# store content as it arrives
	$response->add_content($content);
    } elsif ((defined($response->content)) && (length($response->content))) {
	
	# Get stuff back out of header
	my ($url) = $response->request->header('url');
	my ($filename) = $response->request->header('filename');

	print "Received response from $url ($filename)\n";
	open (FILE, ">>$filename"); print FILE $response->content."\n"; close(FILE);
    }
    return undef;
}
