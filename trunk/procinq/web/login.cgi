#!/usr/bin/perl
#
# login.cgi
# Written by: Michael A. Puscar
# Read the corpus information, replace template variables on login page

use CGI;
use DBI;

$ENV{'ORACLE_HOME'} = "/opt/oracle";
local ($query) = new CGI;

# If CORPUS is not defined, flag an error
if (!defined($query->param('CORPUS'))) {
    print "Location: /index.html\n\n"; 
}
local ($dbh) = DBI->connect("dbi:Oracle:schbooks", "system", "manager");
local ($CORPUS) = $query->param('CORPUS');

# For display purposes need: Corpus Desc, Number of Nodes
local ($CORPUS_SHORT, $CORPUS_NAME, $TRIAL, $TRIAL_TEXT, $EXPIR_TEXT, $FEE, $COUNT) = &get_corpus_info($CORPUS);
local ($TRIAL_TEXT_HEAD);

$dbh->disconnect();

# If FEE is 0, user should not need to log in for this service!
if ($FEE == 0) { print "Location: /$CORPUS_SHORT/1.htm\n\n"; }

if (($TRIAL != 356) && ($TRIAL != 0)) { $TRIAL_TEXT_HEAD = $TRIAL_TEXT."S FREE!"; }
$TRIAL_TEXT_HEAD =~ tr/[a-z]/[A-Z]/;

print "Content-type: text/html\n\n";
open (FILE, "templates/login.tpl"); @lines = <FILE>; close(FILE);
foreach $line (@lines) { 
    $line =~ s/##(.*)##/${$1}/gi;
    print $line;
}

sub get_corpus_info {
    my $q = "SELECT CorpusShortName, Corpus_Name, Trial_Days, Trial_Days_Text, Expire_Days_Text, Fee FROM corpus WHERE CorpusId = $CORPUS";
    my $sth = $dbh->prepare($q) or return undef;
    $sth->execute() or return undef;
    my ($CS, $CN, $TD, $TDT, $ED, $FEE) = $sth->fetchrow_array; $sth->finish(); undef $sth;
 
    my $q = "SELECT count(*) FROM Node WHERE CorpusId = $CORPUS";
    my $sth = $dbh->prepare($q) or return undef;
    $sth->execute() or return undef;
    my $CO = $sth->fetchrow_array; $sth->finish(); undef $sth;
    
    return ($CS, $CN, $TD, $TDT, $ED, $FEE, $CO);
}
   
1;
