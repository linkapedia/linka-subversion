<HTML><HEAD>
<TITLE>
Surfable Books Login
</TITLE>
<SCRIPT LANGUAGE="JavaScript" SRC="/javascript/cookies.js"> </SCRIPT>
<SCRIPT> RedirectIfCookie(); </SCRIPT> 
<LINK href="/images/surf.css" rel=stylesheet>
<style type="text/css"><!--a:hover { color:#0000ff; text-decoration:underline; }a:link { color:#0000ff; text-decoration:underline; }a:visited { color:#0000ff; text-decoration:underline; }.getheading { font-family:arial,helvetica,verdana; font-weight:bold; font-size:12px; color:#0000ff; }.gettext { font-family:arial,helvetica,verdana; font-weight:normal; font-size:10px; color:#000000; }.redheading { font-family:arial,helvetica,verdana; font-weight:bold; font-size:16px; color:#cc3300; }.red { color:#cc3300; }.bkheading { font-family:arial,helvetica,verdana; font-weight:bold; font-size:15px; color:#000000; }.text { font-family:arial,helvetica,verdana; font-weight:normal; font-size:13px; color:#000000; }.smtext { font-family:arial,helvetica,verdana; font-weight:normal; font-size:11px; color:#000000; }.monospace { font-family:Courier,monospace; font-size:10px; }.background { background-image:url('./s-default/art/typist.jpg'); background-repeat:no-repeat; }-->
</style>
</HEAD>
<BODY bgColor=#ffffff leftMargin=0 topMargin=0 marginheight="0" marginwidth="0">
<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD width=149><A href="http://www.surfablebooks.com/"><IMG 
      alt="HOMEPAGE - The Surfable Book Project" border=0 height=62 
      src="/images/surfable_header1_small.gif" width=149></A></TD>
    <TD width=573><IMG alt="The Surfable Book Project" height=62 
      src="/images/surfable_header2_small.gif" width=573></TD>
    <TD width="100%"><IMG height=62 
      src="/images/surfable_header3_small.gif" 
width="100%"></TD></TR></TBODY></TABLE>
<P>
<blockquote>
<TABLE border=0 cellPadding=0 cellSpacing=0 width=600>
  <TBODY>
  <TR>
    <TD vAlign=top width=225>
      <TABLE width=215>
        <TBODY>
        <TR>
          <TD align=middle><IMG alt="Here's What You'll Get!" border=0 
            height=20 src="images/login//whatuget.gif" width=177></TD></TR>
        <TR>
          <TD align=left class=gettext> 
            <P><span class=infotitle>Updated Web Links</span><br>
              We scour the web for the most relevant web pages for the subjects 
              covered in your book. We currentlysearch over 1200 different search 
              sites to cover the whole web. We then get rid of all of the irrelevant 
              links so you dont have to sort through them <a 
              href="javascript:go('./s-default/info/magazines.html')">more</a></P>
            <p><span class=infotitle>PDF Files</span><br>
              Over 30% of the information on the web is contained in PDF files. 
              This is informationa that is most valuable to the researcher. We 
              are the only site that will find and catagorize this information 
              in the topic space of your book. <a 
              href="javascript:go('./s-default/info/transcripts.html')">more</a></p>
            <p><span class=infotitle>News Sources</span><br>
              Coming soon - you will be able to see the latest news stories on 
              your areas of interest. We monitor over 50 of the leading news feeds 
              and present the stories to you topically. <a 
              href="javascript:go('./s-default/info/books.html')">more</a></p>
            <p><span class=infotitle>News Alerts</span><br>
              As information becomes available to the world on your selected topics, 
              we will alert you to this via e-mail. Look for this service soon 
              from Surfable Books. <a 
              href="javascript:go('./s-default/info/newspapers.html')">more</a></p>
            <p><span class=infotitle>Ad Free Surfing</span><br>
              Our directory is made from our patent pending technology and your 
              book. We do not corrupt the directory by taking irrelevant placement 
              links or running banner adds. <a 
              href="javascript:go('./s-default/info/maps.html')">more</a></p>
            <p><span class=infotitle>Journal Articles</span><br>
              In addiiton to the public sources of news, we are also catagorizing 
              and indexing some of the leading journals in the world. Look for 
              professional content soon atht he Surfable Books Project! <a 
              href="javascript:go('./s-default/info/images.html')">more</a> </p>
            <P>&nbsp; </P>
            </TD></TR></TBODY></TABLE></TD>
    <TD bgColor=#cccccc width=1><IMG border=0 height=5 
      src="images/login//shim.gif" width=1></TD>
    <TD width=5><IMG border=0 height=5 src="images/login//shim.gif" width=5></TD>
    <TD class=text vAlign=top width=369>
      <P>
      <TABLE align=right border=1 bordercolor=000000 cellPadding=7 cellSpacing=1 width=131>
        <TBODY>
        <TR>
          <TD bgColor=#cccccc class=background>
            <DIV align=center><IMG alt="Subscribers Sign In Here!" border=0 
            height=26 src="images/login/subscribers.gif" width=75> </DIV><!-- BEGIN NOS-CCMEM -->
            <FORM METHOD="POST" ACTION="auth.cgi">
            <SPAN class=smtext> E-Mail Address</SPAN><BR> <INPUT 
            maxLength=20 name=EMAIL size=12> <SPAN 
            class=smtext>Password</SPAN><BR><INPUT maxLength=10 name=PASSWORD
            size=12 type=password>
	    <input type="hidden" name="LOGIN" value="true"> 
	    <input type="hidden" name="CORPUS_NAME" value="##CORPUS_NAME##"> 
	    <input type="hidden" name="CORPUS" value="##CORPUS##"> 
	    <input type="hidden" name="CORPUS_SHORT" value="##CORPUS_SHORT##"> 
            <P> <INPUT alt="Sign In" border=0 height=23 
            name=Submit src="images/login/signin.gif" type=image width=58> </FORM><IMG border=0 height=20 
            src="images/login//shim.gif" width=5> <p></P></TD></TR></TBODY></TABLE>
      <SPAN 
      class=text> <SPAN class=redheading>ACTIVATE MY FREE TRIAL!</SPAN> <BR>
      On the inside cover of the ##CORPUS_NAME## you will find a unique, 10 digit 
      code to activate your subscription. This activation code entitles you to 
      receive an <B>UNLIMITED ##TRIAL_TEXT## FREE</B> trial of the ##CORPUS_NAME## 
      S-Book. 
      <P><a href="register.cgi?corpus=##CORPUS##&type=activate"> Activate my ##TRIAL_TEXT## 
        FREE trial now!</a> 
      <P><SPAN class=bkheading>Purchase a Subscription</SPAN><BR>
        If you purchased the ##CORPUS_NAME## used or did not purchase it at all, 
        you can still get a subscription to the S-Book edition. Click the link 
        below to get started! 
      <P><a href="register.cgi?corpus=##CORPUS##&type=new"> Purchase a ##EXPIR_TEXT## 
        subscription for only $ ##FEE##</a> 
      <P><SPAN class=bkheading>What's An sBook?</SPAN><BR>
      </P>
      An sBook is new way to present the web that uses your favorite reference 
      books. We organize the web and other information based on the same scheme 
      that the editors of your favorite reference book choose to organize it. 
      Every concept that is important, is represented in the sBook directory. 
      The book will still cover authoritative edited content. The sbook will present 
      the community of thought about a subject and related research materials. 
      The sBook is a perfect jumping off point for research and the latest news 
      in the subjects of your book. No scouring through search engines and wading 
      through irrelevant information. All of the available relevant content is 
      presented as a research companion for the book you already own. Try it today 
      and see why we are making the web an open book. 
      <P>&nbsp; </P></blockquote>
      </span></TD>
  </TR></TABLE>

<P align=center><FONT face="Arial, Helvetica, sans-serif"><a href="http://www.indraweb.com">� 
  2001 IndraWeb Ltd. All rights reserved</a><BR>
  <A class=footer
      href="/about_us.htm">About Us</A>&nbsp; |&nbsp; <A
      class=footer href="/contact_us.htm">Contact
      Us</A>&nbsp; |&nbsp; <A class=footer
      href="/jobs.htm">Jobs</A>&nbsp; |&nbsp; <A
      class=footer href="/privacy_policy.htm">Privacy
      Policy</A>&nbsp; |&nbsp; <A class=footer
      href="/terms%20of%20service">Terms of
      Service</A>&nbsp; |&nbsp; <A class=footer
      href="/help">Help</A></FONT></P>
      <P align=center> </P>
</blockquote>
</BODY>
</HTML>
