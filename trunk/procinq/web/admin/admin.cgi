#!/usr/bin/perl
#
# admin.cgi
# Written by: Michael A. Puscar
# Perform administration functions (see comments)

use CGI;
use DBI;

local ($query) = new CGI;
print "Content-type: text/html\n\n";

# Connect to the LINUX Oracle database
local ($dbh) = DBI->connect("dbi:Oracle:athena", "sbooks", "racer9") 
    || &raise_error("<b>Fatal error</b>: cannot connect to database: ".$!."\n");

# Snapshot query: view of which machines are active, nodes being processed, and idle time
if ($query->param('action') =~ /snap/i) {

    # Query: select information from node and updatelog tables where datescanned time and dateupdate time do 
    #        not match AND an entry exists in the log table.
    my $snap = $dbh->prepare("SELECT c.nodeid, n.corpusid, n.nodetitle, to_char(c.checkoutdate, 'mm/dd/yy hh24:mi:ss'), c.hostname FROM node n, checkout c WHERE c.nodeid = n.nodeid order by checkoutdate desc");
    $snap->execute() || &raise_error("<b>Could not execute select statement: ".$!."\n");
    
    &print_head();
    print "<table width=85% border=1 cellpadding=2 cellspacing=2>\n";
    print "<tr><td align=center><b>Node Id</b></td><td align=center><b>Corpus Id</b></td><td align=center><b>Node Title</b></td><td align=center><b>Checked Out</b></td><td align=center><b>Host</b></td></tr>\n";
    while (($nid, $cid, $ntitle, $time, $host) = $snap->fetchrow()) {
	print "<tr><td align=center> $nid </td><td align=center> $cid </td><td align=center> $ntitle </td><td align=center> $time </td><td align=center> $host </td></tr>\n";
    }
    print "</table>\n";
    &print_foot();

# Calculate average time to complete a node over an X day period
} elsif ($query->param('action') =~ /metr/i) {
    my $datein;
    # default time frame is one week (7 days)
    if (defined($query->param('datein'))) { $datein = $query->param('datein'); }
    else { $datein = 7; }  # default is 1 week

    # Query: select all machines that are processing nodes from the database.
    my $mach = $dbh->prepare("SELECT distinct(hostname) from updatelog");
    $mach->execute() || &raise_error("<b>Could not execute select statement: ".$!."\n");

    &print_head();
    
    local (@mach_arr);
    while (my ($machine) = $mach->fetchrow()) { push (@mach_arr, $machine); }

    print "<center>Average processing time for machines during the past $datein days.<P>\n<table width=45% border=1 cellpadding=2 cellspacing=2>\n";
    print "<tr><td align=center><b>Machine Name</b></td><td align=center><b>Avg Time to Process</b></td></tr>\n";

    #  Then: for each machine, calculate the average node processing time per machine during the past X days.
    foreach $machine (@mach_arr) {
	my $metr = $dbh->prepare("select avg(floor((a.updatedate - b.updatedate)*24*60*60*60)/3600) from updatelog a, updatelog b where a.nodeid > 29999 and a.actioncode=1 and b.actioncode=2 and a.nodeid = b.nodeid and a.nodeid in (select nodeid from updatelog where hostname = '$machine' and updatedate > SYSDATE-($datein + 1))");
	$metr->execute() || &raise_error("<b>Could not execute select statement: ".$!."\n");
	my $avg = $metr->fetchrow();
	printf "<tr><td align=center>$machine</td><td align=center>%6.2f minutes</td></tr>\n", abs($avg); undef $avg;	
    }
    print "</table></center>\n";
    &print_foot();

# Set priority for a specific node range
} elsif ($query->param('action') =~ /setnode/i) {
    $low = $query->param('lnode');
    $high = $query->param('hnode');

    if (($low eq "") or ($high = "")) {
	&raise_error("<b>Error:</b> Neither node range may be blank.\n");
    }

    # Update NODE set both date scanned and date updated to SYSDATE-100 for this range
    my $up = $dbh->prepare("update node set DATESCANNED = SYSDATE-100 WHERE nodeid >= $low and nodeid <= $high");
    $up->execute() || &raise_error("<b>Could not execute update statement: ".$!."\n");
    my $up = $dbh->prepare("update node set DATEUPDATED = DATESCANNED WHERE nodeid >= $low and nodeid <= $high");
    $up->execute() || &raise_error("<b>Could not execute update statement: ".$!."\n");

    &print_head();
    print "Nodes $low through $high have been updated successfully.\n";
    &print_foot();

# print out form for corpus selection
} elsif ($query->param('action') =~ /setp/i) {

    # Query: select all corpus hierarchies from the database
    my $ch = $dbh->prepare("select distinct(h.hierarchyid), h.corpusid, c.corpus_name from hierarchy h, corpus c where c.corpusid=h.corpusid order by h.corpusid");
    $ch->execute() || &raise_error("<b>Could not execute select statement: ".$!."\n");

    local (@ch_arr);

    &print_head();
    print "<b>Instructions</b>: \n";
    print "The following table contains a list of corpus hierarchies and their last run time.   If you \n";
    print "wish to set a corpus hierarchy to run immediately, click the checkbox in the left\n";
    print "hand column and press the submit button.\n";
    print "<P>\n";
    print "<b>Note</b>: Without modification, corpus hierarchies will be processed in the order listed below.\n";
    print "<P><form method=get action='admin.cgi'><input type='hidden' name='action' value='psetnow'>\n";
    print "<table width=95% border=0 cellpadding=2 cellspacing=2>\n";
    print "<tr><td colspan=2 align=center><b>Corpus Name</b></td><td align=center><b>Corpus ID</b></td><td align=center><b>Hierarchy</b></td><td align=center><b>Last Updated</b></td></tr>\n";

    while (my ($hid, $cid, $cname) = $ch->fetchrow()) {
	my $setp = $dbh->prepare("select to_char(min(n.dateupdated), 'MM/DD/YY HH24:MI:SS') dat from node n, hierarchy h where n.corpusid = $cid and h.hierarchyid = $hid and n.nodeid = h.nodeid"); $setp->execute() || &raise_error("<b>Could not execute select statement: ".$!."\n"); 
	my ($date) = $setp->fetchrow(); $setp->finish();
	print "<tr><td><input type='radio' name='ch' value='$cid||$hid'><td>$cname</td><td align=center>$cid</td><td align=center>$hid</td><td align=center>$date</td></tr>\n";
    }
    $ch->finish();
    print "</table></center>\n";
    print "<P><input type='submit' name='submit' value='Set Corpus Priority'></form><p>\n";
    &print_foot();

# Set a corpus hierarchy to a set priority (run now, never run)
} elsif ($query->param('action') =~ /psetnow/i) {
    if (!defined($query->param('ch'))) { &raise_error("<b>No corpus selected.</b>\n"); }
    my ($cid, $hid) = split(/\|\|/,$query->param('ch'));

    # Query: update date scanned and date updated for this corpus hierarchy
    my $upo = $dbh->prepare("update node set DATESCANNED = (SYSDATE-500) WHERE nodeid in (select nodeid from hierarchy where corpusid = $cid and hierarchyid = $hid)");
    $upo->execute() || &raise_error("<b>Could not execute select statement: ".$!."\n");
    my $upt = $dbh->prepare("update node set DATEUPDATED = DATESCANNED WHERE nodeid in (select nodeid from hierarchy where corpusid = $cid and hierarchyid = $hid)");
    $upt->execute() || &raise_error("<b>Could not execute select statement: ".$!."\n");

    &print_head();
    print "<b>Hierarchy:</b> $hid, <b>Corpus:</b> $cid were updated successfully.\n";
    &print_foot();
# Show last update for each corpus hierarchy
} elsif ($query->param('action') =~ /stat/i) {

# Execute the front end BUILD script
} elsif ($query->param('action') =~ /buil/i) {

# Execute the front end HTDIG index
} elsif ($query->param('action') =~ /htdi/i) {

# Command unknown (or no command given) -- flag an error
} else {

}

$dbh->disconnect();

sub print_head {
   # Print out the header
    open (FILE, "head.tpl");
    while (<FILE>) { print; }
    close (FILE);
}

sub print_foot {
    # Print out the footer
    open (FILE, "foot.tpl");
    while (<FILE>) { print; }
    close (FILE);
}

sub raise_error {
   my ($message) = $_[0];

   # Print out the error message
   &print_head();
   print $message."\n";
   &print_foot();

   # Exit as gracefully as possible
   exit(1);
}

1;

