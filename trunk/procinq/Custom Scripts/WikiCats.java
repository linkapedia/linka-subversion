// Gale
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.net.*;

import org.xml.sax.InputSource;
import org.dom4j.io.SAXReader;
import org.dom4j.Element;

public class WikiCats {
    public static boolean go = false;

    private static ProcinQ server = new ProcinQ();
    private static Vector Nodes = new Vector();
    private static Hashtable ht = new Hashtable();

    public static void main(String args[]) throws Exception {

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        //File f = new File("C:/wiki/file.html");
        //FileInputStream fis = new FileInputStream(f);
        //String sLast = root.get("NODEID");
        System.out.println("Loading all nodes into hash ...");
        Vector allNodes = server.CQL("SELECT <NODE> WHERE CORPUSID = 108 AND NODEID = LINKNODEID");
        for (int i = 0; i < allNodes.size(); i++) {
            Node n = (Node) allNodes.elementAt(i);
            ht.put(n.get("NODETITLE"), n);
        }

        // loop through each "bad file", get it from the database and try to figure out what's wrong
        File f = new File("wiki.txt");
        BufferedReader input = null;
        try {
            input = new BufferedReader(new FileReader(f));
            String line = null; //not declared within while loop
            while ((line = input.readLine()) != null) {
                System.out.println("Loading all nodes of depth 2 ...");
                Vector Nodes = server.CQL("SELECT <NODE> WHERE CORPUSID = 108 AND NODEID = LINKNODEID AND NODETITLE LIKE '"+line+"'");
                for (int i = 0; i < Nodes.size(); i++) {
                    Thread.sleep(10000);

                    Node n = (Node) Nodes.elementAt(i);
                    String URL = "http://en.wikipedia.org/wiki/Category:" + n.get("NODETITLE").replaceAll(" ", "_");
                    System.out.println("Opening .. " + URL);

                    HttpURLConnection httpCon = null;
                    StringBuffer returnString = new StringBuffer("");
                    boolean bPDFDocument = false;
                    org.dom4j.Document doc = null;
                    boolean read = true;

                    try {
                        URL myURL = new URL(URL);

                        httpCon = (HttpURLConnection) myURL.openConnection();
                        httpCon.setRequestProperty("User-Agent", "Mozilla/5.001 (windows; U; NT4.0; en-us) Gecko/25250101");
                        httpCon.setDoInput(true);
                        httpCon.setDoOutput(true);
                        httpCon.setUseCaches(false);
                        //httpCon.setRequestMethod("POST");
                        httpCon.setDefaultUseCaches(false);

                        if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                            System.err.println("Http error opening " + myURL.getUserInfo() + " : " + httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
                            throw new Exception("Http error opening " + myURL.getUserInfo() + " : " + httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
                        }

                        InputStream is = httpCon.getInputStream();
                        File f2 = new File("C:/wiki/file.html");
                        if (f2.exists()) f2.delete();

                        DataInputStream dis = new DataInputStream(is);
                        StringBuffer sb = new StringBuffer();

                        String record = null; boolean write = true;
                        sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n");
                        try {
                            while ((record = dis.readLine()) != null) {
                                if (record.indexOf("<head>") != -1) write = false;
                                //if (record.indexOf("<html") != -1) record = "<html>
                                if (write) sb.append(record+"\n");
                                if (record.indexOf("</head>") != -1) write = true;
                            }
                        } catch (IOException e) {
                            System.err.println("There was an error while reading the file " + f2.getAbsolutePath());
                            return;
                        }
                        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f2)));
                        out.println(sb.toString());
                        dis.close();
                        out.close();

                        //System.out.println("wrote to file.");

                        SAXReader xmlReader = new SAXReader(false);

                        InputSource ins = new InputSource(new FileInputStream(f2));
                        ins.setEncoding("ISO-8859-1");
                        ins.setSystemId(URL);

                        doc = xmlReader.read(ins);
                    } catch (Exception e) {
                        read = false;
                        e.printStackTrace(System.err);
                        System.out.println("** FLAG FOR LATER: " + URL);
                    } finally {
                        httpCon.disconnect();
                    }

                    if (read == true) {
                        Element eArticle = doc.getRootElement();
                        Element eBody = eArticle.element("body");

                        go = false;
                        recurseElement(eBody, n);
                    }
                    read = true;
                }
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally { input.close(); }
    }

    public static void recurseElement(Element e, Node n) {
        List list = e.elements();
        if (list.size() == 0) return;

        Iterator it2 = list.iterator();

        while (it2.hasNext()) {
            Element e2 = (Element) it2.next();
            //for (int i = 0; i < level; i++) { System.out.print("  "); }
            if (e2.getQualifiedName().equals("ul")) go = true;

            if ((e2.getQualifiedName().equals("a")) && (e2.attribute("title") != null) &&
                    ((e2.attribute("title")).getText().startsWith("Category:"))) {
                if (go) {
                    System.out.println(e2.getText()+" type: "+e2.attribute("title").getText());
                    Node child = null;

                    if (ht.containsKey(e2.getText())) {
                        Node link = (Node) ht.get(e2.getText());
                        try { child = addNode(e2.getText(), n.get("NODEID"), Integer.parseInt(n.get("DEPTHFROMROOT"))+1, link); }
                        catch (Exception ex) { }
                    } else {
                        try {child = addNode(e2.getText(), n.get("NODEID"), Integer.parseInt(n.get("DEPTHFROMROOT"))+1);
                            Nodes.add(child); ht.put(child.get("NODETITLE"), child); }
                        catch (Exception ex) { }
                    }
                } //else { System.out.println("NO! "+e2.getText()+" type: "+e2.attribute("title").getText()); }
            }

            if (e2.getText().startsWith("Articles in category")) go = false;
            recurseElement(e2, n);
        }

    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh) && !(prevCh != (char) 39))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static Node addNode (String sNodeTitle, String sParent, int Depth) throws Exception {
        return addNode(sNodeTitle, sParent, Depth, null); }
    public static Node addNode (String sNodeTitle, String sParent, int Depth, Node link) throws Exception {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", "108");
        n.set("DEPTHFROMROOT", Depth+"");
        if (link != null) n.set("LINKNODEID", link.get("NODEID"));
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try {
            if (link != null) { n = server.addLinkNode(n); }
            else { n = server.addNode(n); }}
        catch (DataIntegrityViolation dive) {
            System.err.println("Data integrity violation adding "+sNodeTitle+" underneath "+sParent+" .."); throw dive; }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); throw e; }

        return n;

    }
}