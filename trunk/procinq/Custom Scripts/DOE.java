import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class DOE {
    public static Hashtable nodes = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        File f = new File("C:/Documents and Settings/indraweb/projects/perlscripts/doe/doe.out");

        String sCorpusName = "DOE";
        System.out.println("Corpus: "+sCorpusName);

        c = server.addCorpus(sCorpusName, sCorpusName);
        Node root = server.getCorpusRoot(c.getID());

        nodes.put("A", addNode("A", "A", root.get("NODEID"), 1));
        nodes.put("B", addNode("B", "B", root.get("NODEID"), 1));
        nodes.put("C", addNode("C", "C", root.get("NODEID"), 1));
        nodes.put("D", addNode("D", "D", root.get("NODEID"), 1));
        nodes.put("E", addNode("E", "E", root.get("NODEID"), 1));
        nodes.put("F", addNode("F", "F", root.get("NODEID"), 1));
        nodes.put("G", addNode("G", "G", root.get("NODEID"), 1));
        nodes.put("H", addNode("H", "H", root.get("NODEID"), 1));
        nodes.put("I", addNode("I", "I", root.get("NODEID"), 1));
        nodes.put("J", addNode("J", "J", root.get("NODEID"), 1));
        nodes.put("K", addNode("K", "K", root.get("NODEID"), 1));
        nodes.put("L", addNode("L", "L", root.get("NODEID"), 1));
        nodes.put("M", addNode("M", "M", root.get("NODEID"), 1));
        nodes.put("N", addNode("N", "N", root.get("NODEID"), 1));
        nodes.put("O", addNode("O", "O", root.get("NODEID"), 1));
        nodes.put("P", addNode("P", "P", root.get("NODEID"), 1));
        nodes.put("Q", addNode("Q", "Q", root.get("NODEID"), 1));
        nodes.put("R", addNode("R", "R", root.get("NODEID"), 1));
        nodes.put("S", addNode("S", "S", root.get("NODEID"), 1));
        nodes.put("T", addNode("T", "T", root.get("NODEID"), 1));
        nodes.put("U", addNode("U", "U", root.get("NODEID"), 1));
        nodes.put("V", addNode("V", "V", root.get("NODEID"), 1));
        nodes.put("W", addNode("W", "W", root.get("NODEID"), 1));
        nodes.put("X", addNode("X", "X", root.get("NODEID"), 1));
        nodes.put("Y", addNode("Y", "Y", root.get("NODEID"), 1));
        nodes.put("Z", addNode("Z", "Z", root.get("NODEID"), 1));

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null; String text = ""; Signatures signatures = new Signatures(); Node n = null; boolean start = false;
        Vector bad = new Vector();
        try {
            while ( (record=dis.readLine()) != null ) {
                String[] values = record.split("\t");
                Node parent = (Node) nodes.get(values[0].substring(0, 1).toUpperCase());
                n = addNode(values[0], values[0], parent.get("NODEID"), 2);
                nodes.put(record, n);

                try { server.setNodeSource(n, values[1]); }
                catch (Exception e) { bad.add(n); }
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }

        System.out.println("Bad nodes: ");
        for (int i = 0; i < bad.size(); i++) { System.out.println("  "+((Node) bad.elementAt(i)).get("NODETITLE")); }
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}