import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class Wiley {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static String corpusID = "1541";
    public static Hashtable ht = new Hashtable();
    public static Hashtable alpha = new Hashtable();
    public static Hashtable source = new Hashtable();

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        /*
        ht.put("&#180;", "'");
        ht.put("&#146;", "'");
        ht.put("&#151;", "-");
        ht.put("&#150;", "-");
        ht.put("&#33;", "!");
        ht.put("&minus;", "-");
        ht.put("&#97;", "-");
        ht.put("&mu;", "-mu-");
        ht.put("&alpha;", "-alpha-");
        ht.put("&beta;", "-beta-");
        ht.put("&Prime;", "-prime-");
        ht.put("&Delta;", "-delta-");
        ht.put("&thinsp;", " ");
        ht.put("&#61;", "=");
        ht.put("&plusmn;", "+-");
        ht.put("&ratio;", "-ratio-");
        ht.put("&middot;", ".");
        ht.put("&bond;", "-bond-");
        ht.put("&lambda;", "-lambda-");
        ht.put("&trade;", "-trade-");
        ht.put("&deg;", " degrees ");
        ht.put("&gt;", ">");
        ht.put("&#160;", " ");
        ht.put("&#98;", "b");
        ht.put("&#119;", "w");
        ht.put("&#145;", "'");
        ht.put("&#103;", "g");
        ht.put("&#100;", "d");
        ht.put("&#162;", " ");
        ht.put("&#34;", "\"");
        ht.put("&#112;", "p");
        */

        alpha.put("A", "11905561");
        alpha.put("B", "11905562");
        alpha.put("C", "11905563");
        alpha.put("D", "11905564");
        alpha.put("E", "11905565");
        alpha.put("F", "11905566");
        alpha.put("G", "11905567");
        alpha.put("H", "11905568");
        alpha.put("I", "11905569");
        alpha.put("J", "11905570");
        alpha.put("K", "11905571");
        alpha.put("L", "11905572");
        alpha.put("M", "11905573");
        alpha.put("N", "11905574");
        alpha.put("O", "11905575");
        alpha.put("P", "11905576");
        alpha.put("Q", "11905577");
        alpha.put("R", "11905578");
        alpha.put("S", "11905579");
        alpha.put("T", "11905580");
        alpha.put("U", "11905581");
        alpha.put("V", "11905582");
        alpha.put("W", "11905583");
        alpha.put("X", "11905584");
        alpha.put("Y", "11905585");
        alpha.put("Z", "11905586");



        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        FileInputStream fstream = new FileInputStream("C:\\Documents and Settings\\indraweb\\projects\\perlscripts\\interscience\\urls.txt");
        DataInputStream in = new DataInputStream(fstream);
        while (in.available() !=0) {
            String s = in.readLine();
            String[] arr = s.split("\t");

            Enumeration eH = ht.keys();
            while (eH.hasMoreElements()) {
                String key = (String) eH.nextElement();
                if (arr[1].indexOf(key) != -1) {
                    arr[1] = arr[1].replaceAll(key, (String) ht.get(key));
                }
            }

            String parentID = arr[0].substring(1, 2).toUpperCase();
            System.out.println("Parent is "+arr[0].substring(1, 2).toUpperCase()+" for "+arr[0]);
            Node n = addNode(arr[1], (String)alpha.get(parentID), 1);

            FileInputStream fstream2 = new FileInputStream("C:\\Documents and Settings\\indraweb\\projects\\perlscripts\\interscience\\"+arr[0]);
            DataInputStream in2 = new DataInputStream(fstream2);

            Node n2 = null; String stuff = "";
            while (in2.available() !=0) {
                String s2 = in2.readLine();

                Enumeration eH2 = ht.keys();
                while (eH2.hasMoreElements()) {
                    String key2 = (String) eH2.nextElement();
                    if (s2.indexOf(key2) != -1) {
                        s2 = s2.replaceAll(key2, (String) ht.get(key2));
                    }
                }

                if (s2.indexOf("SECTION") == -1) { stuff = stuff + s2; }

                // ##SECTION##Acetylation##ENDSECTION##
                if ((s2.indexOf("ENDSECTION") > -1) && (s2.length() > 27) && (s2.length() < 120)) {
                   if ((!stuff.equals("")) && (n2 != null)) {
                       if (n2.get("NODETITLE").equals("Abstract")) server.setNodeSource(n, stuff);
                       else server.setNodeSource(n2, stuff);
                       System.out.println("set source for node "+n2.get("NODETITLE")+" length "+stuff.length());
                       stuff = "";
                   }
                   n2 = addNode(s2.substring(11, s2.length() - 14), n.get("NODEID"), 2);
                }
            }
            if ((!stuff.equals("")) && (n2 != null)) {
                server.setNodeSource(n2, stuff);
                System.out.println("set source for node "+n2.get("NODETITLE")+" length "+stuff.length());
                stuff = "";
            }
            in2.close();
        }

        in.close();

    }

    public static Node addNode(String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", corpusID);
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}