import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class FixWiley {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static String corpusID = "1541";
    public static Hashtable ht = new Hashtable();
    public static Hashtable alpha = new Hashtable();
    public static Hashtable source = new Hashtable();

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("192.168.0.223:81");

        System.out.println("Loading source into memory ...");

        FileInputStream fstreamz = new FileInputStream("C:\\Documents and Settings\\indraweb\\projects\\perlscripts\\interscience\\source.txt");
        DataInputStream inz = new DataInputStream(fstreamz);
        while (inz.available() !=0) {
            String s = inz.readLine();
            String[] srcin = s.split("\t");

            source.put(srcin[1], srcin[0]);
        } inz.close(); fstreamz.close();

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        FileInputStream fstream = new FileInputStream("C:\\Documents and Settings\\indraweb\\projects\\perlscripts\\interscience\\urls.txt");
        DataInputStream in = new DataInputStream(fstream);
        while (in.available() !=0) {
            String s = in.readLine();
            String[] arr = s.split("\t");

            Enumeration eH = ht.keys();
            while (eH.hasMoreElements()) {
                String key = (String) eH.nextElement();
                if (arr[1].indexOf(key) != -1) {
                    arr[1] = arr[1].replaceAll(key, (String) ht.get(key));
                }
            }

            String parentID = arr[0].substring(1, 2).toUpperCase();
            //System.out.println("Parent is "+arr[0].substring(1, 2).toUpperCase()+" for "+arr[0]);
            //Node n = addNode(arr[1], (String)alpha.get(parentID), 1);

            FileInputStream fstream2 = new FileInputStream("C:\\Documents and Settings\\indraweb\\projects\\perlscripts\\interscience\\"+arr[0]);
            DataInputStream in2 = new DataInputStream(fstream2);

            System.out.println("Reading: "+arr[0]);

            Node n2 = null; String stuff = ""; String newstuff = "";
            while (in2.available() !=0) {
                String s2 = in2.readLine();

                Enumeration eH2 = ht.keys();
                while (eH2.hasMoreElements()) {
                    String key2 = (String) eH2.nextElement();
                    if (s2.indexOf(key2) != -1) {
                        s2 = s2.replaceAll(key2, (String) ht.get(key2));
                    }
                }

                if (s2.indexOf("SECTION") == -1) { stuff = stuff + s2; newstuff = newstuff + " " + s2; }

                // ##SECTION##Acetylation##ENDSECTION##
                if ((s2.indexOf("ENDSECTION") > -1) && (s2.length() > 27) && (s2.length() < 120)) {
                   if (!stuff.equals("")) {
                       int length = stuff.length(); if (length > 60) length = 60;
                       if (source.containsKey(stuff.substring(0, length))) {
                           Node n = server.getNodeProps((String) source.get(stuff.substring(0, length)));
                           System.out.println("Matched node id: " + n.get("NODEID") + " title: " + n.get("NODETITLE") +
                                   "file: " + arr[0] + " beginning source: " + stuff.substring(0, length));
                           server.setNodeSource(n, newstuff);
                           //System.out.println("old stuff: "+stuff);
                           //System.out.println("new stuff: "+newstuff);
                       } else {
                           System.err.println("Couldn't find anything for: "+stuff.substring(0, length));
                           //return;
                       }

                       //if (n2.get("NODETITLE").equals("Abstract")) server.setNodeSource(n, stuff);
                       //else server.setNodeSource(n2, stuff);
                       //System.out.println("set source for node "+n2.get("NODETITLE")+" length "+stuff.length());
                       stuff = ""; newstuff = "";
                   }
                   //n2 = addNode(s2.substring(11, s2.length() - 14), n.get("NODEID"), 2);
                }
            }
            if (!stuff.equals("")) {
                int length = stuff.length(); if (length > 60) length = 60;
                if (source.containsKey(stuff.substring(0, length))) {
                    Node n = server.getNodeProps((String) source.get(stuff.substring(0, length)));
                    System.out.println("Matched node id: " + n.get("NODEID") + " title: " + n.get("NODETITLE") +
                            "file: " + arr[0] + " beginning source: " + stuff.substring(0, length));
                    server.setNodeSource(n, newstuff);
                } else {
                    System.err.println("Couldn't find anything for: " + stuff.substring(0, length));
                }              //server.setNodeSource(n2, stuff);
                //System.out.println("set source for node "+n2.get("NODETITLE")+" length "+stuff.length());
                stuff = ""; newstuff = "";
            }
            in2.close();
        }

        in.close();

    }

    public static Node addNode(String sNodeTitle, String sParent, int Depth) {
        if (true) { System.err.println("NO NO NO!!"); return null; }

        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", corpusID);
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}