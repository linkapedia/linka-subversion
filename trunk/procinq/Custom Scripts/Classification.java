// Classification
//
// Classify the file "sample-file.txt" against the ProcinQ server with the given taxonomy(s).

import com.iw.system.*;
import com.iw.tools.*;

import java.util.*;
import java.io.*;

public class Classification {
    public static Hashtable htArguments = new Hashtable(); // hashtable arguments

    public static void main(String args[]) throws Exception {
        System.out.println("This program will classify a document against the ProcinQ server.\n");
        htArguments = com.iw.tools.GetArguments.getArgHash(args);

        if ((!htArguments.containsKey("corpusid")) || (!htArguments.containsKey("server"))) {
            System.err.println("Usage: Login -server <API> -corpusid <comma separated list of ids>"); return;
        }

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArguments.get("server"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        // **** BEGIN EXAMPLE 1 ****
        // Example #1: Classify a document against the Worldbook (49) taxonomy and print the results
        // API calls used in this example: tsclassify.TSClassifyDoc, tsnode.TSGetNodeTree
        // Note: the client is responsible for converting the file into text format
        System.out.println("**** EXAMPLE #1: Classify a document and print the results ****");
        System.out.println(" ");
        File f = new File("sample-file.txt");
        Vector classifyResults = new Vector(); // classification results will be stored here
        StringBuffer sb = new StringBuffer(); // data from the file will be stored here

        System.out.println("Begin classify: "+f.getAbsolutePath());
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) { sb.append(record); }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }

        // Classify the document.  The server expects a string and a corpusID list.  If more than one taxonomy
        // is specified, the corpusIDs in the second argument should be separated by commas with no spaces.
        try { classifyResults = server.classify(sb.toString(), (String) htArguments.get("corpusid")); }
        catch (SessionExpired se) {
            System.err.println("Session expired."); return; }
        catch (ClassifyError ce) {
            System.err.println("Classification failed.");
            ce.printStackTrace(System.err); return;
        }
        if (classifyResults.size() == 0) { System.err.println("Classification returned no results."); return; }
        System.out.println("Classification complete.\n");

        // Loop through the classification results and print detailed concept information
        for (int i = 0; i < classifyResults.size(); i++) {
            NodeDocument nd = (NodeDocument) classifyResults.elementAt(i);

            // Note: the attributes of the ND object are dynamic.  See API for a base set of attributes.
            // Additional attributes can be added simply by adding fields to the database table.   For this
            // example, only the titles of the topic hits are printed.
            System.out.println("Result: "+nd.get("NODETITLE")+" Score: "+nd.get("SCORE1"));

            // finally, this function will give you the context of the concepts that were returned
            Vector nodeTree = server.getNodeTree(nd.get("NODEID"));

            for (int j = nodeTree.size()-1; j > 0; j--) {
                if (j == nodeTree.size()-1) System.out.print("   ");
                else System.out.print(" > ");

                Node n = (Node) nodeTree.elementAt(j);
                System.out.print(n.get("NODETITLE"));
            }

            System.out.println("\n");
        }
        // **** END EXAMPLE 1 ***

   }
}