import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class TageoMore {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Hashtable nodes = new Hashtable();

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        File f = new File("C:/Documents and Settings/indraweb/projects/perlscripts/cities/etc3.out");

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

        String record = null;
        try {
            while ( (record=dis.readLine()) != null ) {
                String recs[] = record.split("\t");
                String URL = recs[0];
                String pURL = recs[1];
                String title = recs[2];
                int depth = 3;

                String parent_title = "None";

                if (!nodes.containsKey(pURL)) {
                    Vector v = server.CQL("SELECT <NODE> WHERE NODEDESC = '"+pURL+"'");
                    if (v.size() > 0) {
                       nodes.put(pURL, (Node) v.elementAt(0));
                    }
                }

                Node parent = (Node) nodes.get(pURL);
                System.out.println("URL: "+URL+" PARENT: "+pURL+" TITLE: "+title+" PARENT: "+parent.get("NODETITLE"));

                addNode(title, URL, parent.get("NODEID"), Integer.parseInt(parent.get("DEPTHFROMROOT"))+1, parent.get("CORPUSID"));
            }
        } catch (IOException e) {
            System.err.println("There was an error while reading the file "+f.getAbsolutePath());
            return;
        }
    }

    public static Node addNode(String sNodeTitle, String sNodeDesc, String sParent, int Depth, String sCorpusID) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeDesc);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", sCorpusID);
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + sNodeTitle + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;

    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}