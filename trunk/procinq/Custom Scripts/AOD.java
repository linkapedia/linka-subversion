import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

public class AOD {
    public static Hashtable htArguments = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Hashtable ht = new Hashtable();
    private static Node rootNode;
    private static Vector later = new Vector();

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("demo", "demo");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        System.out.println("Load parent information into Hashtable ...");

        rootNode = server.getCorpusRoot("922");

        File f = new File("aod.xml");
        FileInputStream fis = new FileInputStream(f);

        System.out.println("File: " + f.getAbsolutePath());

        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);
        Element opr = doc.getRootElement();

        Iterator i = opr.elements("TABLE").iterator();
        String anchor = ""; String relationship = "";  String root = ""; Node myparent = rootNode;
        int depth = 1;

        while (i.hasNext()) {
            Element table = (Element) i.next();

            Iterator i2 = table.elements("TR").iterator();
            while (i2.hasNext()) {
                Element tr = (Element) i2.next();

                // if two elements, it's a new anchor term.  if three elements, it's a new relationship
                List list = tr.elements("TD");
                if (list.size() == 2) {
                    root = getSource((Element) list.get(0)).trim();
                    anchor = getSource((Element) list.get(1)).trim();

                    depth = getDepth(root); myparent = getParent(root);

                    System.out.println("*** ROOT: "+root+" ANCHOR: "+anchor+" ***");
                    Node n = addNode(printCapitalized(anchor), myparent.get("NODEID"), depth);
                    ht.put(root.replaceAll("e", ""), n);

                    myparent = n;
                }
                if (list.size() == 3) {
                    if (!getSource((Element) list.get(1)).trim().equals(""))
                        relationship = getSource((Element) list.get(1));
                    String object = getSource((Element) list.get(2)).trim();

                    if (relationship.equals("SN")) {
                        System.out.println("Description: "+object);
                        try { server.setNodeSource(myparent, object); }
                        catch (Exception e) {
                            System.err.println("Could not set node source, "+myparent.get("NODEID")+" .. "+object); }
                    }

                    if (relationship.equals("NT")) {
                        later.add(object+"-MIKE-"+myparent.get("NODEID")+"-MIKE-"+(depth+1));
                    }
                }
            }

        }

        fis.close();

        for (int j = 0; j < later.size(); j++) {
            String s[] = ((String) later.elementAt(j)).split("-MIKE-");
            String object = s[0]; String nodeid = s[1]; String depth2 = s[2];

            String words[] = object.split(" "); words[0] = words[0].replaceAll("e", "").trim();
            String insert = object;

            if (ht.containsKey(words[0])) {
                insert = "";
                for (int k = 1; k < words.length; k++) { insert = insert + " " + words[k]; }
                insert = insert.trim();

                Node p = (Node) ht.get(words[0]);
                System.out.println("LINKNODE "+nodeid+" Object: "+insert);
                addNode(insert, nodeid, Integer.parseInt(depth2), p.get("NODEID"));
            } else {
                if (words[0].indexOf(".") != -1) {
                    insert = "";
                    for (int k = 1; k < words.length; k++) { insert = insert + " " + words[k]; }
                    insert = insert.trim();
                }

                System.out.println("NODE "+nodeid+" Object: "+insert);
                addNode(insert, nodeid, Integer.parseInt(depth2));
            }
        }

        System.out.println("\nFinis.");
    }

    // AA2.2e
    public static Node getParent (String section) {
        String parent = rootNode.get("NODEID");

        section = section.replaceAll("e", "");

        if (section.length() == 1) return rootNode;
        else if (section.length() == 2) parent = section.substring(0, 1);
        else if (section.indexOf(".") == -1) parent = section.substring(0, 2);
        else {
            parent = section.substring(0, 2);
            String tmp = section.substring(2); boolean insertPeriod = false;

            while (tmp.indexOf(".") != -1) {
                if (insertPeriod) parent = parent + ".";
                else insertPeriod = true;

                parent = parent + tmp.substring(0, tmp.indexOf("."));
                tmp = tmp.substring(tmp.indexOf(".")+1);
            }
        }

        System.out.println("getparent: "+parent);

        if (ht.containsKey(parent)) return (Node) ht.get(parent);
        else return getParent(parent);
    }

    // A, AA, AA2e, AA2.2.4
    public static int getDepth (String section) {
        section = section.replaceAll("e", "");

        if (section.length() < 3) return section.length();
        if (section.indexOf(".") == -1) return 3;

        String s[] = section.substring(2).split(".");

        return 2+s.length;
    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh) && !(prevCh != (char) 39))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout.replaceAll("Aod", "AOD");
    }

    public static String getSource(Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) {
                sb.append(((Text) obj).getText());
            } else if (obj instanceof Element) {
                if (((Element) obj).attribute("char") != null)
                    sb.append(((Element) obj).attribute("char").getText());
                else
                    sb.append(getSource((Element) obj));
            }
        }

        return sb.toString();
    }

    public static Node addNode(String sNodeTitle, String sParent, int Depth, String LinkNodeID) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", printCapitalized(sNodeTitle));
        n.set("NODETITLE", printCapitalized(sNodeTitle));
        n.set("CORPUSID", "922");
        n.set("DEPTHFROMROOT", Depth + "");
        n.set("LINKNODEID", LinkNodeID);
        System.out.println("LINK NODE: " + printCapitalized(sNodeTitle) + " Parent: " + sParent);

        try {
            n = server.addLinkNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;
    }

    public static Node addNode(String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", printCapitalized(sNodeTitle));
        n.set("NODETITLE", printCapitalized(sNodeTitle));
        n.set("CORPUSID", "922");
        n.set("DEPTHFROMROOT", Depth + "");
        System.out.println("New node: " + printCapitalized(sNodeTitle) + " Parent: " + sParent);

        try {
            n = server.addNode(n);
        } catch (Exception e) {
            System.err.println("Could not add node: " + sNodeTitle);
            e.printStackTrace(System.err);
        }

        return n;
    }

    public static Hashtable getArgHash(String[] args) {
        Hashtable htHash = new Hashtable();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String sKey = (String) args[i].substring(1).trim();
                String sVal = new String("");

                if (((i + 1) != args.length) && (!args[i + 1].startsWith("-"))) {
                    sVal = (String) args[i + 1];
                }
                htHash.put(sKey.toLowerCase(), sVal);
            }
        }

        return htHash;
    }
}