import org.xml.sax.InputSource;
import org.dom4j.io.SAXReader;
import org.dom4j.Element;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;

public class FillToCache {

    public static String CacheFile = ""; // list of files that have already been crawled, along with corresponding URL location

    public static Hashtable ht = new Hashtable();

    public static Vector vHyperlinks = new Vector();
    public static int iCrawlCounter = 0;
    public static boolean crawlInProgress = true;

    private static class CrawlerThread extends Thread {
		public CrawlerThread () {
		}

		public void run() {
            while (vHyperlinks.size() > 0) {
                iCrawlCounter++;

                String sURLelement = "";
                synchronized (vHyperlinks) {
                    sURLelement = (String) vHyperlinks.firstElement();
                    vHyperlinks.remove(sURLelement);
                }
                getData(sURLelement, true);
            }

            crawlInProgress = false;
        }
	}

    // constructor: WebCache specific constructor
    public static void main(String args[]) throws Exception {
        ht = getArgHash(args);

        File f = new File("cachefile.txt");
        CacheFile = "cachecrawldata.txt";

        FileInputStream fis = new FileInputStream(f);
        BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
        String sData = new String();

        buf.readLine();
        while ((sData = buf.readLine()) != null) {
            vHyperlinks.add(sData);
        }
        fis.close();

        System.out.println(vHyperlinks.size()+" documents found to download.");

        for (int i = 0; i < vHyperlinks.size(); i++) {
            sData = (String) vHyperlinks.elementAt(i);
            getData(sData, true);
            Thread.sleep(2000);
        }

		System.out.println("Finished.");
		System.out.flush();
    }

    public static String getData (String URLelement, boolean bWriteToFile) {
        String[] elements = URLelement.split("\t");
        String URL = elements[0]; String Title = elements[2];

        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");
        boolean bPDFDocument = false;

        try {
            System.out.println("Processing: "+Title+" ("+URL+") Num: "+iCrawlCounter);
            URL myURL = new URL(URL);

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "txt";
            if (httpCon.getContentType().equals("text/html")) { sExt = "html"; }
            if (httpCon.getContentType().equals("plain/text")) { sExt = "txt"; }
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; bPDFDocument = true; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; }

            int len = 0; File tempFile = null; FileOutputStream fos = null;

            byte[] buffer = new byte[512];
            int bytesRead = 0;

            while( (len=bis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
                returnString = returnString.append(new String(buffer));
            } bis.close(); httpCon.disconnect();

            if (bWriteToFile) {
                tempFile = getTempFile(URL, sExt);
                System.out.println("Writing to temp file: "+tempFile.getAbsolutePath());

                fos = new FileOutputStream(tempFile);
                fos.write(bos.toByteArray());

                bos.flush(); fos.flush();
                bos.close(); fos.close();
            }

            String s = returnString.toString();

            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("Item content length is "+s.length()+", retrieved "+URL+" in "+lEnd+" ms.");

            // Write the "just run" URL out to disk
            if (bWriteToFile) {
                File f = new File(CacheFile);
                synchronized (f) {
                    PrintWriter out = new PrintWriter(new FileOutputStream(f.getAbsolutePath(), true));
                    out.println(URL+"\t"+tempFile.getAbsolutePath()+"\t"+Title);
                    out.close();
                }
            }

            //System.out.println("RETURNING: ### "+s+" ###");
            return s;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms attempting to retrieve : "+URL+" : "+e.toString());
            e.printStackTrace(System.out);
            return "";
        } finally {
            // Check to make sure that we have not exceeded our maximum run time.  If we have,
            //  please dump our memory image out to disk and empty out our processing.

            ///// TO DO /////
        }
    }

    public static File getTempFile(String URL, String sExt) throws Exception {
        Random wheel = new Random() ; // seeded from the clock
        Random wheel2 = new Random() ; // seeded from the clock
        File tempFile = null;

        do {
            // generate random a number 10,000,000 .. 99,999,999
            int unique = ( wheel.nextInt() & Integer. MAX_VALUE ) %90000000 + 10000000 ;
            int unique2 = ( wheel2.nextInt() & Integer. MAX_VALUE ) %40000000 + 5000000 ;

            File dir = new File(unique+""); if (!dir.exists()) { dir.mkdir(); }
            tempFile = new File( dir.getAbsolutePath()+"/"+Integer.toString ( unique2) + "." + sExt );
        } while ( tempFile.exists () );

        // We "finally" found a name not already used. Nearly always the first time.
        // Quickly stake our claim to it by opening/closing it to create it.
        // In theory somebody could have grabbed it in that tiny window since
        // we checked if it exists, but that is highly unlikely.
        synchronized (tempFile) {
            try {
                new FileOutputStream( tempFile ).close ();
                //System.out.println("Temp file successfully created: "+tempFile.getAbsolutePath());
            }
            catch (Exception e) {
                System.out.println("Could not generate temp file.");
                //Runtime r = Runtime.getRuntime (); r.wait(1000); // pause a second
                //return getTempFile(sPath, sExtension);
            }
        }

        return tempFile;
    }

    public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}