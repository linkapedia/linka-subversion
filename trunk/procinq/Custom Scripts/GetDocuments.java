// GetDocuments
//
// Return all documents with the given word in the title.

import com.iw.system.*;
import com.iw.tools.*;

import java.util.*;
import java.io.*;

public class GetDocuments {
    public static Hashtable htArguments = new Hashtable(); // hashtable arguments

    public static void main(String args[]) throws Exception {
        System.out.println("This program will get all documents with a specific word in the title.\n");
        htArguments = com.iw.tools.GetArguments.getArgHash(args);

        if ((!htArguments.containsKey("word")) || (!htArguments.containsKey("server"))) {
            System.err.println("Usage: GetNodes -server <API> -word <WORD>"); return;
        }

        // Step 1. Initialize the server object
        ProcinQ server = new ProcinQ();
        server.setAPI((String) htArguments.get("server"));

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try { server.Login("demo", "demo");  }
        catch (Exception e) { System.err.println("Login authorization failed."); return; }
        System.out.println("Login successful, your session key is "+server.getSessionKey()+".\n");

        System.out.println(" ");

        // Note: Only the first argument to this call (the query) is required.  The second argument is the location
        // to begin and the third argument is the number of results (defaults are 1 and 500, respectively)
        Vector topics = server.CQL("SELECT <DOCUMENT> WHERE DOCTITLE LIKE '"+(String) htArguments.get("word")+"'", 1, 20);
        for (int i = 0; i < topics.size(); i++) {
            Document d2 = (Document) topics.elementAt(i);
            System.out.println("Title: "+d2.get("DOCTITLE")+" ID: "+d2.get("DOCUMENTID"));
            System.out.println("Abstract: "+d2.get("DOCUMENTSUMMARY"));

            System.out.println(" ");
        }

   }
}