import org.dom4j.io.*;
import org.dom4j.*;
import org.xml.sax.InputSource;

import java.io.*;
import java.util.Iterator;
import java.util.List;

public class Factiva {

    public static void main(String args[]) throws Exception {

        double aa = 134;
        double bb = 138;

        System.out.println(aa/bb);

        if (true) return;

        File f = new File("C:/Documents and Settings/indraweb/Desktop/heikki/AFXASI0020040802e0830093t_FT.xml");

        FileInputStream fis = new FileInputStream(f);
        //String sLast = root.get("NODEID");

        SAXReader xmlReader = new SAXReader(false);

        InputSource ins = new InputSource(fis);
        ins.setEncoding("UTF-8");
        ins.setSystemId(f.toURL().toString());

        org.dom4j.Document doc = xmlReader.read(ins);

        Element eDistDoc = doc.getRootElement();
        if (eDistDoc == null) {
            System.err.println("No root element for file: " + f.getAbsolutePath());
            return;
        }

        Element eHead = eDistDoc.element("ReplyItem");
        Element eMeta = eDistDoc.element("MetadataPT");
        Element eBody = eDistDoc.element("ArchiveDoc");

        String title = f.getName();
        int wc = 0;

        if (eHead.element("Title") != null) {
            Element eTitle = eHead.element("Title");
            title = getSource(eTitle.element("Headline"));

            List l = eHead.elements("Num");

            for (int i = 0; i < l.size(); i++) {
                Element e = (Element) l.get(i);
                if (e.attribute("fid") != null) {
                    Attribute a = e.attribute("fid");
                    if (a.getText().trim().equals("wc")) {
                        String swc = e.attribute("value").getText();
                        wc = Integer.parseInt(swc);
                        i = l.size();
                    }}
            }
        }

        System.out.println("File Title: " + title.trim()+" word count: "+wc);

        if (eBody.element("Article") != null) {
            Element eArticle = eBody.element("Article");
            Element e1 = eArticle.element("HandL");
            Element e2 = eArticle.element("TailParas");

            if (e1 != null) System.out.print(getSource(e1.element("LeadPara")));
            if (e2 != null) System.out.print(getSource(e2));
        }

        System.out.println(" ");
        fis.close();
    }

    public static String getSource (Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) { sb.append(((Text) obj).getText()); }
            else if (obj instanceof Element) {
                if (((Element) obj).attribute("char") != null)
                    sb.append(((Element) obj).attribute("char").getText());
                else if (((Element) obj).getQualifiedName().equals("br")) { sb.append(" "); }
                else sb.append(getSource((Element) obj));
            }
        }

        return sb.toString();
    }

}