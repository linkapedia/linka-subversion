import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.net.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.dom4j.Attribute;
import org.xml.sax.InputSource;

/*
GoogleFille.java

 Written by Michael Puscar, 11/17/2004
 Copyright Intellisophic Corporation

 This program will retrieve all links in the Google News document repository for a given set of keywords.

 */

public class GoogleFill {
    public static Hashtable urls = new Hashtable();

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    public static void main(String args[]) throws Exception {
        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("demo", "demo");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        File f = new File("cachefile.txt"); if (f.exists()) f.delete();
        synchronized (f) {
            PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath(), true), "UTF-8"));
            out.println("<?xml version=\"1.0\" ?>");
            out.println("<!DOCTYPE html PUBLIC \"-//Gale//DTD Gale eBook Document DTD 20031113//EN\" \"galeeBkdoc.dtd\">");
            out.println("<RESULTS>");
            out.close();
        }
        System.out.println("File prepared.");

        //String sCQL = "SELECT <NODE> WHERE CORPUSID = 903 OR CORPUSID = 912 OR CORPUSID = 911 OR CORPUSID = 889";
        String sCQL = "SELECT <NODE> WHERE CORPUSID = 903";
        Vector v = server.CQL(sCQL, 4460, 20000);

        for (int i = 0; i < v.size(); i++) {
            Node n = (Node) v.elementAt(i);
            String title = n.get("NODETITLE");
            String url = "http://news.google.com/news?num=100&hl=en&lr=&q="+URLEncoder.encode(title, "UTF-8")+"&c2coff=1&sa=N&tab=wn";

            System.out.println("Processing "+(i+1)+" of "+v.size()+" nodes.");
            fillHash(url, title);

            Thread.sleep(1000);
        }

        f = new File("cachefile.txt");
        synchronized (f) {
            PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath(), true), "UTF-8"));
            out.println("</RESULTS>");
            out.close();
        }
    }

    public static String getData (String URL) {
        long lStart = System.currentTimeMillis();
        HttpURLConnection httpCon = null;
        StringBuffer returnString = new StringBuffer("");
        boolean bPDFDocument = false;

        try {
            URL myURL = new URL(URL);

            httpCon = (HttpURLConnection) myURL.openConnection();
            httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
            httpCon.setDoInput(true); httpCon.setDoOutput(true);  httpCon.setUseCaches(false);
            //httpCon.setRequestMethod("POST");
            httpCon.setDefaultUseCaches(false);

            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Http error opening "+myURL.getUserInfo()+" : " +httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
            }

            InputStream is = httpCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // store the page off in a location specified when the repository was created, and generate a temp file name
            String sExt = "other";
            if (httpCon.getContentType().equals("text/html")) { sExt = "html"; }
            if (httpCon.getContentType().equals("plain/text")) { sExt = "txt"; }
            if (httpCon.getContentType().equals("application/pdf")) { sExt = "pdf"; bPDFDocument = true; }
            if (httpCon.getContentType().indexOf("word") != -1) { sExt = "doc"; }

            int len = 0; File tempFile = null; FileOutputStream fos = null;

            byte[] buffer = new byte[512];
            int bytesRead = 0;

            while( (len=bis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
                returnString = returnString.append(new String(buffer));
            } bis.close(); httpCon.disconnect();

            String s = returnString.toString();

            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("Item content length is "+s.length()+", retrieved "+URL+" in "+lEnd+" ms.");

            return s;

        } catch (Exception e) {
            long lEnd = System.currentTimeMillis() - lStart;
            System.out.println("HTTP/File Write failure after "+lEnd+" ms attempting to retrieve : "+URL+" : "+e.toString());
            e.printStackTrace(System.out);
            return "";
        } finally {
            // Check to make sure that we have not exceeded our maximum run time.  If we have,
            //  please dump our memory image out to disk and empty out our processing.

            ///// TO DO /////
        }
    }

    public static void fillHash(String url, String title) throws Exception {
        String page = getData(url).replaceAll("\n", "");
        String total = "-1"; int loop = 0;

        // get numbers
        Pattern p = Pattern.compile("</b> of <b>(.*?)</b> for", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(page); boolean bResult = m.find();

        if (bResult) { total = m.group(1); System.out.println("Found "+total+" results for word '"+title+"'."); }

        p = Pattern.compile("</b> of about <b>(.*?)</b> for", Pattern.CASE_INSENSITIVE);
        m = p.matcher(page); bResult = m.find();

        if (bResult) { total = m.group(1); System.out.println("Found "+total+" results for word '"+title+"'."); }

        // loop through each result and write the output to a file
        // <a href="http://www.jsonline.com/news/racine/apr05/316294.asp">Vigil will remember victims of crimes</a> <font size=-1 color=#6f6f6f><nobr>Milwaukee Journal Sentinel</nobr></font></font><br>
        p = Pattern.compile("<a href=\"(.*?)\" id=(.*?)>(.*?)</a><br>", Pattern.CASE_INSENSITIVE);
        m = p.matcher(page);
        bResult = m.find();

        while (m.find() && (loop < 50)) {
            String sURL = m.group(1);
            String sTitle = m.group(3);

            sTitle = sTitle.replaceAll("<(.*?)>", "");
            sTitle = sTitle.replaceAll("&#39;", "'");

            //if (sURL.indexOf("\"") != -1) sURL = sURL.substring(0, sURL.indexOf("\""));

            if ((!urls.containsKey(sURL)) && (sURL.indexOf("<") == -1) && (sURL.indexOf("google") == -1)) {
                loop++; urls.put(sURL, sTitle);

                File f = new File("cachefile.txt");
                synchronized (f) {
                    PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath(), true), "UTF-8"));
                    out.println("   <RESULT><URL>"+sURL+"</URL><TITLE>"+sTitle+"</TITLE></RESULT>");
                    out.close();
                }
                //System.out.println("URL: " + sURL + " Title: " + sTitle+" "+loop+" of "+total);
            }
        }

        System.out.println(loop+" of "+total+" documents extracted.\n");
        loop = 0;
    }


}

/*

            $loop++; $urls{$u} = $t;

            open (FILE, ">>$outfile"); print FILE "$u\t$t\n"; close(FILE);
         }
         $u = ""; $id = ""; $t = "";
      }

      undef $u; undef $t; undef $id;
   }

   print "Processed $loop results for word $word (total: $total)\n";
}
*/