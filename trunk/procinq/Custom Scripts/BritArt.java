// BritArt
//
// Read the set of Brittannica articles and ingest them (no relationships)

import com.iw.system.*;
import com.iw.tools.HTMLtoTEXT;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import org.dom4j.io.SAXReader;
import org.dom4j.Element;
import org.dom4j.Text;
import org.xml.sax.InputSource;

public class BritArt {
    public static Hashtable htArguments = new Hashtable();
    public static Hashtable nodes = new Hashtable();
    public static String rootD = "C:/Documents and Settings/indraweb/Desktop/brit_ftp/concise2/xml_articles";

    private static ProcinQ server = new ProcinQ();
    private static Corpus c = null;

    public static void main(String args[]) throws Exception {
        htArguments = getArgHash(args);

        // Step 1. Initialize the server object
        server.setAPI("66.134.131.62:81");

        // Step 2. Log into the server
        System.out.println("Logging into the ProcinQ server..");
        try {
            server.Login("mpuscar", "racer9");
        } catch (Exception e) {
            System.err.println("Login authorization failed.");
            return;
        }
        System.out.println("Login successful, your session key is " + server.getSessionKey() + ".\n");

        File fDirectory = new File(rootD);
        if (!fDirectory.exists()) {
            System.err.println("File: "+rootD+" does not exist.");
            return;
        }

        c = server.getCorpora("2114");
        Node root = server.getCorpusRoot(c.getID());

        String FilePaths[] = fDirectory.list();
        boolean b = false;

        System.out.println("Selecting all Britannica directories.. " + FilePaths.length + " total folders found.");
        for (int i = 0; i < FilePaths.length; i++) {
            File f = new File(rootD+"/"+FilePaths[i]);
            if (FilePaths[i].equals("Geography-cities & towns")) b = true;
            if (b) {
                Node parent = addNode(FilePaths[i], root.get("NODEID"), 1);
                getArticles(parent);
            }
        }
    }

    public static void getArticles (Node parent) throws Exception {
        File fDirectory = new File(rootD+"/"+parent.get("NODETITLE"));
        if (!fDirectory.exists()) {
            System.err.println("File: "+fDirectory.getAbsolutePath()+" does not exist.");
            return;
        }
        String FilePaths[] = fDirectory.list();

        System.out.println("Selecting all Britannica articles under "+fDirectory.getName()+".. " + FilePaths.length + " total articles found.");

        if (FilePaths != null) {
            for (int i = 0; i < FilePaths.length; i++) {
                File f = new File(fDirectory.getAbsolutePath() + "/" + FilePaths[i]);

                FileInputStream fis = new FileInputStream(f);
                //String sLast = root.get("NODEID");

                SAXReader xmlReader = new SAXReader(false);

                InputSource ins = new InputSource(fis);
                ins.setEncoding("UTF-8");
                ins.setSystemId(f.toURL().toString());

                try {
                    org.dom4j.Document doc = xmlReader.read(ins);

                    Element eArticle = doc.getRootElement();
                    if (eArticle == null) {
                        System.err.println("No article tag for file: " + f.getAbsolutePath());
                        return;
                    }

                    Element eTitle = eArticle.element("title");
                    if (eTitle == null) {
                        System.err.println("No title tag for file: " + f.getAbsolutePath());
                        return;
                    }

                    String title = getSource(eTitle);
                    String articleID = eArticle.attribute("artclid").getText();

                    Element eSource = eArticle.element("firstpar");

                    if (title.trim().equals("")) {
                        System.err.println("Empty title tag for file: " + f.getAbsolutePath());
                    } else {
                        System.out.println(" ");
                        System.out.println("Found: articleID " + articleID + " ");
                        System.out.println("File: "+f.getAbsolutePath());
                        System.out.println("Title: " + printCapitalized(title));

                        Node n = addNode(printCapitalized(title), parent.get("NODEID"), 2);

                        if (eSource != null) {
                            String source = getSource(eSource);
                            try { server.setNodeSource(n, source); }
                            catch (Exception e) { System.err.println("Could not set the source for this file."); }
                            System.out.println("Source: "+getSource(eSource));
                        }

                        recurseElement(eArticle, 2, printCapitalized(title), n.get("NODEID"));
                    }
                    fis.close();
                    //String sParent = root.get("NODEID");
                } catch (org.dom4j.DocumentException de) {
                    System.err.println("DocException for: "+f.getAbsolutePath());
                }
            }
        }
    }

    public static String getSource (Element eSource) {
        StringBuffer sb = new StringBuffer();

        List content = eSource.content();
        Iterator it = content.iterator();

        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof Text) { sb.append(((Text) obj).getText()); }
            else if (obj instanceof Element) {
                if (((Element) obj).attribute("char") != null)
                    sb.append(((Element) obj).attribute("char").getText());
                else sb.append(getSource((Element) obj));
            }
        }

        return sb.toString();
    }

    public static void recurseElement (Element e, int loop, String Parent, String ParentID) {
        List list = e.elements("h" + loop);
        if (list.size() == 0) return;

        Iterator it = list.iterator();
        while (it.hasNext()) {
            Element e2 = (Element) it.next();

            if (e2.element("headtitle") != null) {
                System.out.print("Title: "+Parent+" -> ");
                String subtitle = "";

                if ((e2.element("headtitle").element("xref") != null) &&
                    (!e2.element("headtitle").element("xref").getText().trim().equals("")))
                    subtitle = e2.element("headtitle").element("xref").getText();
                else if ((e2.element("headtitle").element("idxref") != null) &&
                    (!e2.element("headtitle").element("idxref").getText().trim().equals("")))
                    subtitle = e2.element("headtitle").element("idxref").getText();
                else subtitle = e2.element("headtitle").getText();

                Node n = addNode(printCapitalized(subtitle.trim()), ParentID, (loop+1));
                System.out.print(printCapitalized(subtitle.trim()));

                // get and set node source
                List lsource = e2.elements("p");

                if (lsource.size() > 0) {
                    StringBuffer sbSource = new StringBuffer();

                    Iterator i3 = lsource.iterator();
                    while (i3.hasNext()) { sbSource.append(getSource((Element) i3.next())); }

                    try {
                        server.setNodeSource(n, sbSource.toString());
                        System.out.println(" source: "+sbSource.toString());
                    } catch (Exception ex) {
                        System.out.println("**error** could not set source for: "+n.get("NODETITLE"));
                        ex.printStackTrace(System.err);
                    }

                } else { System.out.println(" (no source) "); }


                recurseElement(e2, (loop+1), Parent+" -> "+printCapitalized(subtitle.trim()), n.get("NODEID"));
            }
        }
    }

    public static String printCapitalized(String str) {
        String sout = "";

        // Print a copy of str to standard output, with the
        // first letter of each word in upper case.
        char ch;       // One of the characters in str.
        char prevCh;   // The character that comes before ch in the string.
        int i;         // A position in str, from 0 to str.length()-1.
        prevCh = '.';  // Prime the loop with any non-letter character.
        for (i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isLetter(ch) && !Character.isLetter(prevCh))
                sout = sout + Character.toUpperCase(ch);
            else
                sout = sout + ch;
            prevCh = ch;
        }

        //System.out.println("return: "+sout);
        return sout;
    }

    public static Node addNode (String sNodeTitle, String sParent, int Depth) {
        Node n = new Node();
        n.set("PARENTID", sParent);
        n.set("NODESIZE", "50");
        n.set("NODEDESC", sNodeTitle);
        n.set("NODETITLE", sNodeTitle);
        n.set("CORPUSID", c.getID());
        n.set("DEPTHFROMROOT", Depth+"");
        System.out.println("New node: "+sNodeTitle+" Parent: "+sParent);

        try { n = server.addNode(n); }
        catch (Exception e) { System.err.println("Could not add node: "+sNodeTitle); e.printStackTrace(System.err); }

        return n;

    }

    public static Hashtable getArgHash ( String[] args) {
		Hashtable htHash = new Hashtable();

		for ( int i = 0; i < args.length; i++)
		{
			if (args[i].startsWith ("-") ) {
				String sKey = (String) args[i].substring(1).trim();
				String sVal = new String("");

				if (((i+1) != args.length) && (!args[i+1].startsWith("-"))) {
					sVal = (String) args[i+1];
				}
				htHash.put(sKey.toLowerCase(), sVal);
			}
		}

		return htHash;
	}
}