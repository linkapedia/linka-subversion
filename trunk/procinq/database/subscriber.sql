Create table Subscriber (
	UserId Number(9,0) NOT NULL UNIQUE,
	Email Varchar2(50) NOT NULL UNIQUE,
	Password Varchar2(10) NOT NULL,
	FirstName Varchar2(15),
	LastName Varchar2(20),
        Street Varchar2(50),
	City Varchar2(20),
	State Varchar2(2),
	Country Varchar2(20),
	Zipcode Varchar2(12),
	UserStatus Number(2,0) Default 0,
	DefaultCorpusName Varchar2(20),
	UserWantsSpam Number(1,0) Default 0,
Primary Key (UserId)
)  ;

Create table SubscriberCorpus (
	UserId Number(9,0) NOT NULL,
	CorpusId Number(4,0) NOT NULL,
	SubscriptionDate Date,
	ExpirationDate Date,
Primary Key (UserId,CorpusId)
)  ;

Create table CoverIdentifiers (
	CoverId Varchar2(10) NOT NULL UNIQUE,
	CorpusId Number(4,0),
	Status Number(1,0),
Primary Key (CoverId)
)  ;


alter table SubscriberCorpus add foreign key(UserId) references Subscriber (UserId) on delete cascade;
alter table SubscriberCorpus add foreign key(CorpusId) references Corpus (CorpusId) on delete cascade;

create sequence sub_seq start with 1;

create trigger sub_trigger before insert on subscriber for each row when (new.userid is null)
begin select sub_seq.nextval into :new.userid from dual;
end;
/
