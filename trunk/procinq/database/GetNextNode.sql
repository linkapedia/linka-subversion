/* The GetNextNode procedure is invoked by a client machine available */
/* to process (metasearch, cache, score) a node.    The procedure takes */
/* "MachineNameIn" as an input parameter for logging purposes. */
/* 								*/ 
CREATE OR REPLACE PROCEDURE GetNextNode (
        nodeid_out OUT Node.NodeId%TYPE,
        nodetitle_out OUT Node.NodeTitle%TYPE,
        nodesize_out OUT Node.NodeSize%TYPE,
	MachineNameIn IN VARCHAR2, 
	corpusid_in IN NUMBER)
IS 
BEGIN
DECLARE
  BEGIN
  SELECT nodeid INTO nodeid_out FROM
    (SELECT n.nodeid FROM nodecrawlqueue n, corpuspriority p
     WHERE p.corpusid = n.corpusid AND n.status = 1 ORDER BY p.priority, n.nodeid ASC)
    WHERE rownum = 1;
  UPDATE NodeCrawlQueue set STATUS = 2,MachineName = MachineNameIn where NODEID = nodeid_out;
  UPDATE Node set DATESCANNED = SYSDATE where NODEID = nodeid_out;
EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
  GetNextNode(nodeid_out, nodetitle_out, nodesize_out, MachineNameIn, 0);
  END;
COMMIT WORK;
END GetNextNode;
/
