CREATE OR REPLACE PROCEDURE GetTextStream (documentid_in IN Document.DocumentId%TYPE, words OUT CLOB)
IS 
BEGIN
DECLARE
  ri rowid;

  BEGIN
  SELECT rowid INTO ri FROM DOCUMENT WHERE documentid = documentid_in;
  ctx_doc.filter('docfulltext', ri, words, TRUE);
  END;
COMMIT WORK;
END GetTextStream;
/
show errors;
