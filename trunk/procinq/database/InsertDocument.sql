CREATE OR REPLACE PROCEDURE InsertDocument (
	NodeIdIn IN Number,
        DocTitleIn IN Long,
	DocURLIn IN Long,
	DocumentSumIn IN Long,
	ChannelidIn IN Long,
	ScoreOneIn IN Long,
	ScoreTwoIn IN Long,
	ScoreThreeIn IN Long,
	ScoreFourIn IN Long,
	ScoreFiveIn IN Long,
	ScoreSixIn IN Long,
	GenreIdIn IN Long,
	MachineNameIn IN Long,
	DocumentsIn IN Number,
	ObjectionableIn IN Long)
AS
BEGIN 
DECLARE
  v_docid number(9);
  docid number(9);
  DcT Varchar2(256);
  DcU Varchar2(256);
  DcS Varchar2(650);
  SOn Varchar2(10);
  OldSOn Varchar2(10);
  STw Varchar2(10);
  STh Varchar2(10);
  SFo Varchar2(10);
  SFi Varchar2(10);
  SSi Varchar2(10);
  Gid Varchar2(10);
  Obj Varchar2(256);
  Top Number(8);
  Counter Number(9);
BEGIN
  FOR i IN 1..DocumentsIn LOOP
    IF (DocumentsIn=1) THEN
        DcT := DocTitleIn;
        DcU := DocURLIn;
        DcS := DocumentSumIn;
        SOn := ScoreOneIn;
        STw := ScoreTwoIn;
        STh := ScoreThreeIn;
        SFo := ScoreFourIn;
        SFi := ScoreFiveIn;
        SSi := ScoreSixIn;
        Gid := GenreIdIn;
        Obj := ObjectionableIn;
    ELSIF (i=1) THEN
        DcT := SUBSTR(DocTitleIn, 1, (INSTR(DocTItleIn, '||',1,i)-1));
        DcU := SUBSTR(DocURLIn, 1, (INSTR(DocURLIn, '||',1,i)-1));
        DcS := SUBSTR(DocumentSumIn, 1, (INSTR(DocumentSumIn, '||',1,i)-1));
        SOn := SUBSTR(ScoreOneIn, 1, (INSTR(ScoreOneIn, '||',1,i)-1));
        STw := SUBSTR(ScoreTwoIn, 1, (INSTR(ScoreTwoIn, '||',1,i)-1));
        STh := SUBSTR(ScoreThreeIn, 1, (INSTR(ScoreThreeIn, '||',1,i)-1));
        SFo := SUBSTR(ScoreFourIn, 1, (INSTR(ScoreFourIn, '||',1,i)-1));
        SFi := SUBSTR(ScoreFiveIn, 1, (INSTR(ScoreFiveIn, '||',1,i)-1));
        SSi := SUBSTR(ScoreSixIn, 1, (INSTR(ScoreSixIn, '||',1,i)-1));
        Gid := SUBSTR(GenreIdIn, 1, (INSTR(GenreIdIn, '||',1,i)-1));
        Obj := SUBSTR(ObjectionableIn, 1, (INSTR(ObjectionableIn, '||',1,i)-1));
    ELSIF (i=DocumentsIn) THEN
        DcT := SUBSTR(DocTitleIn, (2+(INSTR(DocTitleIn, '||',1,i-1))), length(DocTitleIn));
        DcU := SUBSTR(DocURLIn, (2+(INSTR(DocURLIn, '||',1,i-1))), length(DocURLIn));
        DcS := SUBSTR(DocumentSumIn, (2+(INSTR(DocumentSumIn, '||',1,i-1))), length(DocumentSumIn));
        SOn := SUBSTR(ScoreOneIn, (2+(INSTR(ScoreOneIn, '||',1,i-1))), length(ScoreOneIn));
        STw := SUBSTR(ScoreTwoIn, (2+(INSTR(ScoreTwoIn, '||',1,i-1))), length(ScoreTwoIn));
        STh := SUBSTR(ScoreThreeIn, (2+(INSTR(ScoreThreeIn, '||',1,i-1))), length(ScoreThreeIn));
        SFo := SUBSTR(ScoreFourIn, (2+(INSTR(ScoreFourIn, '||',1,i-1))), length(ScoreFourIn));
        SFi := SUBSTR(ScoreFiveIn, (2+(INSTR(ScoreFiveIn, '||',1,i-1))), length(ScoreFiveIn));
        SSi := SUBSTR(ScoreSixIn, (2+(INSTR(ScoreSixIn, '||',1,i-1))), length(ScoreSixIn));
        Gid := SUBSTR(GenreIdIn, (2+(INSTR(GenreIdIn, '||',1,i-1))), length(GenreIdIn));
        Obj := SUBSTR(ObjectionableIn, (2+(INSTR(ObjectionableIn, '||',1,i-1))), length(ObjectionableIn));
    ELSIF (i>1 AND i<DocumentsIn) THEN
        Top := 2+(INSTR(DocTitleIn, '||',1,i-1));
        DcT := SUBSTR(DocTitleIn, Top, (INSTR(DocTitleIn,'||',1,i))-Top);
        Top := 2+(INSTR(DocURLIn, '||',1,i-1));
        DcU := SUBSTR(DocURLIn, Top, (INSTR(DocURLIn,'||',1,i))-Top);
        Top := 2+(INSTR(DocumentSumIn, '||',1,i-1));
        DcS := SUBSTR(DocumentSumIn, Top, (INSTR(DocumentSumIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreOneIn, '||',1,i-1));
        SOn := SUBSTR(ScoreOneIn, Top, (INSTR(ScoreOneIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreTwoIn, '||',1,i-1));
        STw := SUBSTR(ScoreTwoIn, Top, (INSTR(ScoreTwoIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreThreeIn, '||',1,i-1));
        STh := SUBSTR(ScoreThreeIn, Top, (INSTR(ScoreThreeIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreFourIn, '||',1,i-1));
        STh := SUBSTR(ScoreFourIn, Top, (INSTR(ScoreFourIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreFiveIn, '||',1,i-1));
        STh := SUBSTR(ScoreFiveIn, Top, (INSTR(ScoreFiveIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreSixIn, '||',1,i-1));
        STh := SUBSTR(ScoreSixIn, Top, (INSTR(ScoreSixIn,'||',1,i))-Top);
        Top := 2+(INSTR(GenreIdIn, '||',1,i-1));
        Gid := SUBSTR(GenreIdIn, Top, (INSTR(GenreIdIn,'||',1,i))-Top);
        Top := 2+(INSTR(ObjectionableIn, '||',1,i-1));
        Obj := SUBSTR(ObjectionableIn, Top, (INSTR(ObjectionableIn,'||',1,i))-Top);
    END IF;

    BEGIN 
    SELECT document_seq.nextval INTO docid FROM dual;
    INSERT INTO Document (DocumentId, DocTitle, DocURL, GenreId, DateLastFound, FilterStatus) VALUES (docid, DcT, DcU, Gid, SYSDATE, Obj);
    EXCEPTION 
      WHEN OTHERS THEN
        SELECT distinct(DocumentId) INTO v_docid from Document where DocURL = DcU;
        UPDATE Document SET DateLastFound = SYSDATE where DocURL = DcU;
	-- If Score1 was greater than 100 it was an editor added link
	-- therefore we need an entry in the DocumentChangeLog
	-- SELECT max(Score1) INTO OldSOn FROM NodeDocument WHERE DocumentId = v_docid;
	-- IF (OldSOn > 100.0) THEN
	--   INSERT INTO DocumentChangeLog (DocumentId, NodeId, OldScore, NewScore) VALUES (v_docid, NodeIdIn, OldSOn, Son);
	-- END IF;
        UPDATE NodeDocument SET Score1 = SOn where DocumentId = v_docid and NodeId = NodeIdIn;	
        UPDATE NodeDocument SET Score2 = STw where DocumentId = v_docid and NodeId = NodeIdIn;
        UPDATE NodeDocument SET Score3 = STh where DocumentId = v_docid and NodeId = NodeIdIn;
        UPDATE NodeDocument SET Score4 = SFo where DocumentId = v_docid and NodeId = NodeIdIn;
        UPDATE NodeDocument SET Score5 = SFi where DocumentId = v_docid and NodeId = NodeIdIn;
        UPDATE NodeDocument SET Score6 = SSi where DocumentId = v_docid and NodeId = NodeIdIn;
        UPDATE NodeDocument SET DocSummary = DcS where DocumentId = v_docid and NodeId = NodeIdIn;
        UPDATE Document SET FilterStatus = Obj where DocumentId = v_docid;
        -- Raise_Application_Error(-20004, 'Made it here');
    END;
    END LOOP;
END;

COMMIT WORK;

DECLARE
  v_docid number(9);
  DcS Varchar2(650);
  DcU Varchar2(256);
  Cha Varchar2(4);
  SOn Varchar2(10);
  STw Varchar2(10);
  STh Varchar2(10);
  SFo Varchar2(10);
  SFi Varchar2(10);
  SSi Varchar2(10);
  Top Number(9);
  Dbug Number(9);
BEGIN
  FOR i IN 1..DocumentsIn LOOP
    IF (DocumentsIn=1) THEN 
        DcU := DocURLIn;
        Cha := ChannelidIn;
        DcS := DocumentSumIn;
        SOn := ScoreOneIn;
        STw := ScoreTwoIn;
        STh := ScoreThreeIn;
        SFo := ScoreFourIn;
        SFi := ScoreFiveIn;
        SSi := ScoreSixIn;
    ELSIF (i=1) THEN
        DcU := SUBSTR(DocURLIn, 1, (INSTR(DocURLIn, '||',1,i)-1));
        Cha := SUBSTR(ChannelidIn, 1, (INSTR(ChannelidIn, '||',1,i)-1));
        DcS := SUBSTR(DocumentSumIn, 1, (INSTR(DocumentSumIn, '||',1,i)-1));
        SOn := SUBSTR(ScoreOneIn, 1, (INSTR(ScoreOneIn, '||',1,i)-1));
        STw := SUBSTR(ScoreTwoIn, 1, (INSTR(ScoreTwoIn, '||',1,i)-1));
        STh := SUBSTR(ScoreThreeIn, 1, (INSTR(ScoreThreeIn, '||',1,i)-1));
        SFo := SUBSTR(ScoreFourIn, 1, (INSTR(ScoreFourIn, '||',1,i)-1));
        SFi := SUBSTR(ScoreFiveIn, 1, (INSTR(ScoreFiveIn, '||',1,i)-1));
        SSi := SUBSTR(ScoreSixIn, 1, (INSTR(ScoreSixIn, '||',1,i)-1));
    ELSIF (i=DocumentsIn) THEN
        DcU := SUBSTR(DocURLIn, (2+(INSTR(DocURLIn, '||',1,i-1))), length(DocURLIn));
        Cha := SUBSTR(ChannelidIn, (2+(INSTR(ChannelidIn, '||',1,i-1))), length(ChannelidIn));
        DcS := SUBSTR(DocumentSumIn, (2+(INSTR(DocumentSumIn, '||',1,i-1))), length(DocumentSumIn));
        SOn := SUBSTR(ScoreOneIn, (2+(INSTR(ScoreOneIn, '||',1,i-1))), length(ScoreOneIn));
        STw := SUBSTR(ScoreTwoIn, (2+(INSTR(ScoreTwoIn, '||',1,i-1))), length(ScoreTwoIn));
        STh := SUBSTR(ScoreThreeIn, (2+(INSTR(ScoreThreeIn, '||',1,i-1))), length(ScoreThreeIn));
        SFo := SUBSTR(ScoreFourIn, (2+(INSTR(ScoreFourIn, '||',1,i-1))), length(ScoreFourIn));
        SFi := SUBSTR(ScoreFiveIn, (2+(INSTR(ScoreFiveIn, '||',1,i-1))), length(ScoreFiveIn));
        SSi := SUBSTR(ScoreSixIn, (2+(INSTR(ScoreSixIn, '||',1,i-1))), length(ScoreSixIn));
    ELSIF (i>1 AND i<DocumentsIn) THEN
        Top := 2+(INSTR(DocURLIn, '||',1,i-1));
        DcU := SUBSTR(DocURLIn, Top, (INSTR(DocURLIn,'||',1,i))-Top);
        Top := 2+(INSTR(ChannelidIn, '||',1,i-1));
        Cha := SUBSTR(ChannelidIn, Top, (INSTR(ChannelidIn,'||',1,i))-Top);
        Top := 2+(INSTR(DocumentSumIn, '||',1,i-1));
        DcS := SUBSTR(DocumentSumIn, Top, (INSTR(DocumentSumIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreOneIn, '||',1,i-1));
        SOn := SUBSTR(ScoreOneIn, Top, (INSTR(ScoreOneIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreTwoIn, '||',1,i-1));
        STw := SUBSTR(ScoreTwoIn, Top, (INSTR(ScoreTwoIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreThreeIn, '||',1,i-1));
        STh := SUBSTR(ScoreThreeIn, Top, (INSTR(ScoreThreeIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreFourIn, '||',1,i-1));
        SFo := SUBSTR(ScoreFourIn, Top, (INSTR(ScoreFourIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreFiveIn, '||',1,i-1));
        SFi := SUBSTR(ScoreFiveIn, Top, (INSTR(ScoreFiveIn,'||',1,i))-Top);
        Top := 2+(INSTR(ScoreSixIn, '||',1,i-1));
        SSi := SUBSTR(ScoreSixIn, Top, (INSTR(ScoreSixIn,'||',1,i))-Top);
    END IF;

   Dbug := I;
   SELECT DocumentId INTO v_docid from Document where DocURL = DcU;
   INSERT INTO DocumentChannel values (v_docid, Cha, NodeIdIn);
   DELETE FROM NodeDocument WHERE NodeId = NodeIdIn AND DocumentId = v_docid;
   INSERT INTO NodeDocument (NodeId, DocumentId, DocumentType, DateScored, DocSummary, Score1, Score2, Score3, Score4, Score5, Score6) values (NodeIdIn, v_docid, 1, SYSDATE, DcS, SOn, STw, STh, SFo, SFi, SSi);
   -- Raise_Application_Error(-20004, 'Nid '||NodeIdIn||' SOn '||SOn||' STw '||STw||' STh '||STh||' SFo '||SFo||' SFi '||SFi||' SSi '||SSi);
   END LOOP;
EXCEPTION
    /* No worries if we hit a dup val on index */
    WHEN DUP_VAL_ON_INDEX THEN NULL;
    WHEN INVALID_NUMBER THEN Raise_Application_Error(-20004, 'Fatal Error.   Stack trace: COUNTER '||Dbug||' Nid '||NodeIdIn||' DocId '||v_docid||' Cha '||Cha||' SOn '||SOn||' STw '||STw||' STh '||STh||' SFo '||SFo||' SFi '||SFi||' SSi '||SSi);
    WHEN NO_DATA_FOUND THEN Raise_Application_Error(-20004, 'Fatal Error.  The insert failed when trying to insert the following: Channel:'||Cha||' Score1:'||SOn||' Score2:'||STw||' Score3: '||STh||' Score4: '||SFo||' Score5: '||SFi||' Score6: '||SSi||' SUM: '||DcS||' URL: '||DcU);
END;

COMMIT WORK;

DECLARE
  Top Number(9);
BEGIN
  FOR i IN 1..DocumentsIn LOOP
    INSERT INTO UpdateLog (ActionCode, Hostname, NodeId, UpdateDate) values (3, MachineNameIn, NodeIdIn, SYSDATE);
  END LOOP;
EXCEPTION
    WHEN OTHERS THEN Raise_Application_Error(-20005, 'Error on statement: INSERT INTO UpdateLog (ActionCode, Hostname, NodeId, UpdateDate) values (3, '||MachineNameIn||', '||NodeIdIn||', SYSDATE');
END;
COMMIT WORK;
END InsertDocument;
/
