exec ctx_ddl.create_preference('index_location', 'BASIC_STORAGE');
exec ctx_ddl.set_attribute('index_location', 'I_TABLE_CLAUSE', 'tablespace RDATA');
exec ctx_ddl.create_preference('COMMON_DIR', 'FILE_DATASTORE');

create index NodeNodeTitle on Node(NodeTitle) indextype is ctxsys.context;
create index DocumentDocTitle on Document(DocTitle) indextype is ctxsys.context;
create index DocumentDocumentSummary on Document(DocumentSummary) indextype is ctxsys.context;
create index NodeDocumentDocSummary on NodeDocument(DocSummary) indextype is ctxsys.context;
create index IdracDocumentBibliography on idracdocument(bibliography) indextype is ctxsys.context;

create index DocFullText on Document(FullText)
indextype is ctxsys.context parameters 
('datastore COMMON_DIR storage index_location')
/