create user sbooks identified by its default tablespace DDATA;
grant dba to sbooks;

commit;

call dbms_java.grant_permission( 'SBOOKS','SYS:java.io.FilePermission', 'c:/temp/LogSynchThes.txt', 'write' );
call dbms_Java.grant_permission( 'SBOOKS', 'java.io.FilePermission', '<>', 'read ,write, execute, delete');
exec dbms_java.grant_permission( 'SBOOKS','SYS:java.io.FilePermission', '<<ALL FILES>>', 'read');
exec dbms_java.grant_permission( 'SBOOKS','SYS:java.io.FilePermission', '<<ALL FILES>>', 'write');
EXEC Dbms_Java.grant_permission( 'SBOOKS', 'SYS:java.lang.RuntimePermission', 'writeFileDescriptor', '');
EXEC Dbms_Java.grant_permission( 'SBOOKS', 'SYS:java.lang.RuntimePermission', 'readFileDescriptor', '');

commit;

exec ctx_ddl.create_preference('index_location', 'BASIC_STORAGE');
exec ctx_ddl.set_attribute('index_location', 'I_TABLE_CLAUSE', 'tablespace RDATA');
exec ctx_ddl.create_preference('COMMON_DIR', 'FILE_DATASTORE');

commit;
