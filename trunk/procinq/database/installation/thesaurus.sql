update configparams set paramvalue = '/opt/oracle/ora92/bin' where paramname = 'OracleBinFolder';

commit;

DROP PROCEDURE SynchThesaurus;
CREATE OR REPLACE PROCEDURE SynchThesaurus 
(sDBName VARCHAR2, sUserDB VARCHAR2, 
sPassDB VARCHAR2) AS LANGUAGE JAVA NAME 
'SynchThesaurus.SynchThesaurus 
(java.lang.String, java.lang.String,  java.lang.String)';
/

commit;

