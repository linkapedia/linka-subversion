package com.mycompany.deletescorezeros;

/**
 * Ticket #637. This application runs a process on the production
 * node_document and document_node tables to get rid of any rows where ROC or 
 * Score01=0
 * Author: Sahar Ebadi
 */

import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBWrapperAbs;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.util.List;
import java.util.ResourceBundle;

public class DeleteScoreZeros {
    private static final ResourceBundle config = ResourceBundle.getBundle("config");
    public static void main(String[] args) throws Exception {
        DynamoDBWrapperAbs.env = config.getString("environment");
        int remainToClean =0;  
        // Find the DocumntNode objects where Score01==0 and delete them
        List<NodeDocument> nodeDocs = DataHandlerFacade.FindNodeDocumentsWithScoreOneValueEqualToZero();
        System.out.println("Scan of " + "NodeDocument" + " for nodeDocuments with a score01 equal to zero.");
        if(!nodeDocs.isEmpty()){
            remainToClean = nodeDocs.size();
            System.out.println("num of 0 score nodeDocs:"+nodeDocs.size());
            for (NodeDocument nod : nodeDocs) {
                System.out.println(nod.getDocID()+", NodeID:"+nod.getNodeID()+", remained nodes: "+remainToClean);
                //logger.log(Level.INFO, "{0}, nodeID: {1}", new Object[]{nod.getDocID(), nod.getNodeID()});
                if (nod.getScore01() == 0) {
                    try{
                        DataHandlerFacade.deleteNodeDocument(nod);
                        if(remainToClean>1){
                            remainToClean = remainToClean-1;
                        }
                    }catch (Exception e) {
                        System.out.println("An exception has ocurred while getting Nodes By DocumentId, no of nodes remainToClean: "+remainToClean);
                        System.out.println(e.getMessage());
                    }    
                }
            }
        }
        // Find the DocumntNode objects where Score01==0 and delete them
        System.out.println("#############start scanning DOC NODES#############");
        List<DocumentNode> nodes = DataHandlerFacade.FindDocumentNodesWithScoreOneValueEqualToZero();
        System.out.println("Scan of " + "DocumentNode" + " for documentNodes with a score01 equal to zero.");
        if(!nodes.isEmpty()){
            remainToClean = nodes.size();
            System.out.println("num of 0 score DocNods:"+nodes.size());
            for (DocumentNode nod : nodes) {
                System.out.println(nod.getDocID()+", NodeID:"+nod.getNodeID()+", remained nodes: "+remainToClean);
                //logger.log(Level.INFO, "{0}, nodeID: {1}", new Object[]{nod.getDocID(), nod.getNodeID()});
                if (nod.getScore01() == 0) {
                    try{
                        DataHandlerFacade.deleteDocumentNode(nod);
                        if(remainToClean>1){
                            remainToClean = remainToClean-1;
                        }
                    }catch (Exception e) {
                        System.out.println("An exception has ocurred while getting Nodes By DocumentId, no of nodes remainToClean: "+remainToClean);
                        System.out.println(e.getMessage());
                    }    
                }
            }
        }
        System.out.println("Dynamo has been cleaned successfuly");
    }
}
