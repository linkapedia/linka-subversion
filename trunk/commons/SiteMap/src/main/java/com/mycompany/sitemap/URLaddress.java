package com.mycompany.sitemap;

import java.sql.*;
import java.util.ResourceBundle;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.swing.JOptionPane;


public class URLaddress {

    private static final ResourceBundle jdbcConfig = ResourceBundle.getBundle("database/jdbc");

    public static void main(String[] args) throws Exception {

//        Connection conn = conObj.getConnection(jdbcConfig); 
        Connection conn = getConnection(jdbcConfig);
        String sub = "   ";
        String baseURL = "" ;
        String nodetitle = "";
        int currentNodeid = 0;
        String filePath = "";
        
        String webroot = JOptionPane.showInputDialog(null, "Please enter 1 to create the sitemap for WhyKnows, and 2 for BuddhismPlaces:");
        int webrootInt = Integer.parseInt(webroot);
        if(webrootInt==1){
            baseURL = jdbcConfig.getString("baseUrlWine");
            nodetitle = jdbcConfig.getString("nodeTitleWine");
            currentNodeid = Integer.parseInt(jdbcConfig.getString("nodeIdWine"));
            filePath = jdbcConfig.getString("filePathWine");
        }else if(webrootInt==2){
            baseURL = jdbcConfig.getString("baseUrlbuddhism");
            nodetitle = jdbcConfig.getString("nodeTitlebuddhism");
            currentNodeid = Integer.parseInt(jdbcConfig.getString("nodeIdbuddhism"));
            filePath = jdbcConfig.getString("filePathbuddhism");
        }else{
            JOptionPane.showMessageDialog(null,"Wrong input");
        }
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("urlset");
            // set attribute to  element
            rootElement.setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            doc.appendChild(rootElement);
            // creat the urls address
            try {
                getChildren(conn, currentNodeid, nodetitle, nodetitle, sub, doc, rootElement, baseURL);
            } finally {
                try {
                    conn.close();
                } catch (Exception ignore) {
                }
            }
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filePath));
            transformer.transform(source, result);
            System.out.println("File saved!");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    public static Connection getConnection(ResourceBundle jdbcConfig) throws Exception {
        Connection conn = null;
        if (conn == null) {
            Class.forName(jdbcConfig.getString("jdbc.driver.name"));
            String url = jdbcConfig.getString("jdbc.database.url");
            String username = jdbcConfig.getString("jdbc.database.username");
            String password = jdbcConfig.getString("jdbc.database.password");
            conn = DriverManager.getConnection(url, username, password);
        }
        return conn;
    }

    public static void getChildren(Connection conn, int currentNodeid, String nodetitle, String parentNode, String sub, Document doc,Element rootElement/*, Element url*/, String baseURL) throws Exception {
        //System.out.println(sub + currentNodeid + " " + nodetitle);
        String currentUrl;
        PreparedStatement pstmt = null;
        
        try {
            String sql = "select  nodeid, nodetitle  from node where parentid=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, currentNodeid);
            ResultSet selectResultNode = pstmt.executeQuery();
            try {
                if (selectResultNode.next()) 
                {
                    parentNode = nodetitle;
                    do {
                        currentNodeid = selectResultNode.getInt("nodeid");
                        nodetitle = selectResultNode.getString("nodetitle");
                        getChildren(conn, currentNodeid, nodetitle, parentNode , "   " + sub, doc, rootElement, baseURL);
                        // staff elements
                        Element url = doc.createElement("url");
                        url.setAttribute("title", nodetitle);
                        rootElement.appendChild(url);
                        //firstname elements
                        Element loc = doc.createElement("loc");
                        // reverse node id
                        NodeId nodeIdObj = new NodeId();
                        String reversedNodeId = nodeIdObj.getReversed(currentNodeid);
                        currentUrl = baseURL+parentNode+"/"+nodetitle+"/"+ reversedNodeId;
                        currentUrl = currentUrl.replace(" ", "-");
                        loc.appendChild(doc.createTextNode(currentUrl));
                        url.appendChild(loc);
                        }while(selectResultNode.next());
                }else{
                    
                }
            } finally {
                try {
                    selectResultNode.close();
                } catch (Exception ignore) {
                }
            }
        } finally {
            try {
                pstmt.close();
            } catch (Exception ignore) {
            }
        }
    }


}
class NodeId {
     String getReversed(int currentNodeid) {
        String StrNodeId = new Integer(currentNodeid).toString();
        //Create a StringBuffer from the original string
        StringBuffer bufferNodeId = new StringBuffer(StrNodeId);
        //Reverse the contents of the StringBuffer
        bufferNodeId = bufferNodeId.reverse();
        // convert String back to integer
        String StrReversedNodeId = bufferNodeId.toString();
        return StrReversedNodeId;
    }
}


