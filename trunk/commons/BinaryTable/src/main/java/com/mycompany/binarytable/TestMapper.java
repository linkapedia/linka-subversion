/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.binarytable;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.linkapedia.dynamo.mapper.data.DataUtility;
import com.linkapedia.dynamo.mapper.data.IDataOperations;
import com.linkapedia.dynamo.mapper.format.DocumentNodeToDocNodeMapper;
import com.linkapedia.dynamo.mapper.format.iDynamoMapper;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author andy
 */
public class TestMapper {

    public static void main(String args[]) {
        System.out.println("****************Getting the context****************");
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        iDynamoMapper mapper = (DocumentNodeToDocNodeMapper) context.getBean("docNodeMaper");

        try {
            System.out.println("****************Getting the document nodes****************");
            List<DocumentNode> documentNodes = new ArrayList<DocumentNode>();

            DocumentNode node5 = new DocumentNode();
            node5.setDocID("1");
            node5.setURL("http://www.perrosucio1.com");
            node5.setScore11(1.2f);
            node5.setNodeID("55");

            documentNodes.add(node5);


            DocumentNode node1 = new DocumentNode();
            node1.setDocID("1");
            node1.setURL("http://www.perrosucio1.com");
            node1.setScore11(1.2f);
            node1.setNodeID("11");

            documentNodes.add(node1);

            DocumentNode node2 = new DocumentNode();
            node2.setDocID("2");
            node2.setURL("http://www.perrosucio2.com");
            node2.setScore11(1.5f);
            node2.setNodeID("22");

            documentNodes.add(node2);


            DocumentNode node3 = new DocumentNode();
            node3.setDocID("3");
            node3.setURL("http://www.perrosucio3.com");
            node3.setScore11(1.1f);
            node3.setNodeID("33");

            documentNodes.add(node3);

            DocumentNode node4 = new DocumentNode();
            node4.setDocID("3");
            node4.setURL("http://www.perrosucio3.com");
            node4.setScore11(4.4f);
            node4.setNodeID("44");

            documentNodes.add(node4);





            System.out.println("+++++++++++++Number of Document nodes + " + documentNodes.size());
            System.out.println("****************Mapping the tables****************");
            List<Doc_Node> docsToStore = mapper.getDocNodes(documentNodes);
            System.out.println("****************Saving the data****************");

            System.out.println("****************Done****************");

        } catch (Exception ex) {
            System.out.println("Exception:" + ex);
        }
    }
}
