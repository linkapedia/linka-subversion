package com.mycompany.binarytable;

/**
 * Ticket #654 and 680 convert documentNode table to a Doc_Node table Author:
 * Sahar Ebadi
 */
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBWrapperAbs;
import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

public class App {

    private static final ResourceBundle config = ResourceBundle.getBundle("config");

    public static void main(String[] args) throws IOException, ParseException, Exception {
        DynamoDBWrapperAbs.env = config.getString("environment");
        try {
//            System.out.println("Testing new store...");
//            List<Doc_Node> docNodesToStore = NewStore();
//            System.out.println("Done new store...");
//            InsertNewDoc_Node(docNodesToStore);
//
//            Hashtable DocNodeHashTable = new Hashtable();
//            Doc_Node doc_NodeObj = new Doc_Node();
//            DocNodeHashTable = GenerateHTForNodeList(DocNodeHashTable);
//            InsertToDoc_Node(DocNodeHashTable, doc_NodeObj);
//            RetriveToTest(DocNodeHashTable);
//            DocNodeHashTable.clear();
            //<editor-fold defaultstate="collapsed" desc="Doc_Node functions"> 
            try {
                // initializing the object
                String docId = "0f89cd87e92dcd74472ecd243ee9aeaf";
//                doc_NodeObj.setDocId(docId);
//                ByteBuffer nodeListBytBuff = compressString("897,876,876,0980");
//                doc_NodeObj.setNodeList(nodeListBytBuff);

                //###INSERT: 
//                DataHandlerFacade.saveDoc_Node(doc_NodeObj);
//                System.out.println("item has been inserted!");
//
//            //###RETRIVE:
                System.out.println("Testing the retrieve for doc: " + docId);
                Doc_Node doc = DataHandlerFacade.getDoc_NodesByDocumentId(docId);
//
                System.out.println("item has been RETRIVED as: docId:" + doc.getDocId() + ", nodeList:" + decompressString(doc.getNodeList()));
//
//            //            //###DELETE:
              //  DataHandlerFacade.deleteDoc_Node(doc_NodeObj);
                //System.out.println("item has been DELETED!");

            } catch (Exception e) {
                System.err.println("Error running the binary attribute type example: " + e);
                e.printStackTrace(System.err);
            }
            ////         //</editor-fold>
        } catch (Exception e) {
            System.out.println("Exception :" + e);
        }
    }

    private static List<Doc_Node> NewStore() {
        System.out.println("Going to DocumentNode nodes...");
        System.out.println("Starting...");
        List<DocumentNode> nodes = DataHandlerFacade.ScanDocumentNode();
        System.out.println("Nodes alredy got it");
        List<Doc_Node> docNodes = new ArrayList<Doc_Node>();
        StringBuilder docContent = new StringBuilder();
        boolean ASC = true;
        boolean DESC = false;
        String previousId;
        String url;
        long timeStamp;
        Doc_Node doc = null;
        Map<String, Float> unsortMap = new HashMap<String, Float>();
        if (!nodes.isEmpty()) {
            System.out.println("number of DocumentNode retrived: " + nodes.size());
            previousId = nodes.get(0).getDocID();
            for (int counter = 0; counter < nodes.size(); counter++) {
                DocumentNode node = nodes.get(counter);
                url = node.getURL();
                timeStamp = node.getTimeStamp();
                if (previousId.equalsIgnoreCase(node.getDocID())) {
                    unsortMap.put(node.getNodeID(), node.getScore11());

                    if (nodes.size() - 1 == counter) {
                        //repeat code
                        Map<String, Float> sortedMapAsc = sortByComparator(unsortMap, ASC);
                        doc = new Doc_Node();
                        doc.setDocId(previousId);
                        docContent.append(url).append(",").append(timeStamp).append("{");
                        for (Map.Entry<String, Float> entry : sortedMapAsc.entrySet()) {
                            docContent.append(entry.getKey());
                            docContent.append(",");
                        }
                        docContent.append("}");
                        try {
                            write("DocId: " + doc.getDocId() + "\n" + "Content: " + docContent, doc.getDocId());
                            doc.setNodeList(compressString(docContent.toString()));
                            docContent.setLength(0);
                        } catch (Exception ex) {
                            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        docNodes.add(doc);
                        //End repeat code
                    }
                } else {
                    if (!unsortMap.isEmpty()) {
                        //repeat code
                        Map<String, Float> sortedMapAsc = sortByComparator(unsortMap, ASC);
                        doc = new Doc_Node();
                        doc.setDocId(previousId);
                        docContent.append(url).append(",").append(timeStamp).append("{");
                        for (Map.Entry<String, Float> entry : sortedMapAsc.entrySet()) {
                            docContent.append(entry.getKey());
                            docContent.append(",");
                        }
                        docContent.append("}");
                        try {
                            write("DocId: " + doc.getDocId() + "\n" + "Content: " + docContent, doc.getDocId());
                            doc.setNodeList(compressString(docContent.toString()));
                            docContent.setLength(0);
                        } catch (Exception ex) {
                            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        docNodes.add(doc);
                        //End repeat code

                    }
                    previousId = node.getDocID();
                    unsortMap.clear();
                    if (previousId.equalsIgnoreCase(node.getDocID())) {
                        unsortMap.put(node.getNodeID(), node.getScore11());
                    }
                }
            }
            System.out.println("Number of Docs: " + docNodes.size());
        }
        return docNodes;
    }

    public static void write(String content, String filename) {
        FileWriter fileWriter = null;
        String path = "/home/andres/Documents/test/mapper/";
        try {
            File folder = new File(path);
            if (!folder.exists()) {
                folder.mkdir();
            }

            File newTextFile = new File(path + "/" + filename + ".txt");
            fileWriter = new FileWriter(newTextFile);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
        } finally {
            try {
                fileWriter.close();
            } catch (IOException ex) {
            }
        }
    }

    private static Hashtable GenerateHTForNodeList(Hashtable DocNodeHashTable) throws ParseException {
        //<editor-fold defaultstate="collapsed" desc="scanning DocumentNode table Functions">   
        NodeDocument nodeDocumentObj = new NodeDocument();
        String prevDocId = null;
        List<List<Object>> sameDoc_NodeListArray = new ArrayList<List<Object>>();
        List<Object> shortDocumentNode = null;
        int i = 0;
        boolean firstTime = true;
        Long nodeDocTime = 0L;
        long longDate;
        DateFormat formatter;
        Date date;
        String str_date = "01/15/2013";
        formatter = new SimpleDateFormat("MM/dd/yyyy");
        date = (Date) formatter.parse(str_date);
        longDate = date.getTime();
        // Find the DocumntNode objects where Score01==0 and delete them
        System.out.println("#############start scanning DocumentNode#############");
        List<DocumentNode> nodes = DataHandlerFacade.ScanDocumentNode();
        if (!nodes.isEmpty()) {
            System.out.println("number of DocumentNode retrived: " + nodes.size());
            for (int counter = 0; counter < nodes.size(); counter++) {
                DocumentNode nod = nodes.get(counter);
                shortDocumentNode = new ArrayList<Object>();
                if (firstTime == true) {
                    prevDocId = nod.getDocID();
                    firstTime = false;
                }
                if (nod.getDocID().equalsIgnoreCase(prevDocId)) {
                    try {
                        //put all the node.attr in nodearray Then put all the nodes related with the same docId in an array
                        shortDocumentNode = SetShortDocumentNode(nod, shortDocumentNode, nodeDocumentObj, nodeDocTime, longDate);
                    } catch (Exception ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    sameDoc_NodeListArray.add(i, shortDocumentNode);

                    if (counter == nodes.size() - 1) {
                        sameDoc_NodeListArray = sortByScore11(sameDoc_NodeListArray);
                        //## building the Hash Table
                        if (!DocNodeHashTable.containsKey(nod.getDocID())) {
                            DocNodeHashTable.put(nod.getDocID(), sameDoc_NodeListArray);
                        }
                    }
                    i++;
                } else {
                    try {
                        sameDoc_NodeListArray = sortByScore11(sameDoc_NodeListArray);
                        //## building the Hash Table
                        if (!DocNodeHashTable.containsKey(prevDocId)) {
                            DocNodeHashTable.put(prevDocId, sameDoc_NodeListArray);
                        }
                        prevDocId = nod.getDocID();
                        shortDocumentNode.clear();
                        sameDoc_NodeListArray = new ArrayList<List<Object>>();
                        i = 0;
                        shortDocumentNode = SetShortDocumentNode(nod, shortDocumentNode, nodeDocumentObj, nodeDocTime, longDate);
                        sameDoc_NodeListArray.add(i, shortDocumentNode);
                        i++;
                    } catch (Exception ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return DocNodeHashTable;
    }
//</editor-fold>

    private static void InsertNewDoc_Node(List<Doc_Node> nodesToInsert) {
        System.out.println("Inserting new doc node");
        System.out.println("Size of new nodes: " + nodesToInsert.size());
        int counter = 0;
        for (Doc_Node node : nodesToInsert) {
            try {
                DataHandlerFacade.saveDoc_Node(node);
                counter++;
            } catch (Exception ex) {
                ex.printStackTrace();
                ex.getMessage();
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        System.out.println(counter + " items inserted to Doc_Node! number of keys: " + nodesToInsert.size());
    }

    private static void InsertToDoc_Node(Hashtable DocNodeHashTable, Doc_Node doc_NodeObj) {
        //<editor-fold defaultstate="collapsed" desc="insert to table">
        String HashTableDocId;
        Set keySet = DocNodeHashTable.keySet();
        Iterator itr = keySet.iterator();
        int counter = 0;
        while (itr.hasNext()) {
            try {
                HashTableDocId = itr.next().toString();
//                    System.out.println("insreting: docId:"+HashTableDocId+", nodeList: "+DocNodeHashTable.get(HashTableDocId));
                doc_NodeObj.setDocId(HashTableDocId);
                Object nodeListObj = DocNodeHashTable.get(HashTableDocId);
                String nodeListStr = nodeListObj.toString();
                doc_NodeObj.setNodeList(compressString(nodeListStr));
                //## INSERT to table Doc_Node: doc_NodeObj.setNodeList(compressString(DocNodeHashTable.get(HashTableDocId).toString()));
                DataHandlerFacade.saveDoc_Node(doc_NodeObj);
                counter++;
            } catch (Exception ex) {
                ex.printStackTrace();
                ex.getMessage();
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println(counter + " items inserted to Doc_Node! number of keys: " + DocNodeHashTable.size());
    }
    //</editor-fold>

    private static List<List<Object>> sortByScore11(List<List<Object>> sameDoc_NodeListArray) {
        Object[] temp;
        temp = sameDoc_NodeListArray.toArray();
        Arrays.sort(temp, new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                // Get inner arrays to be compared by type-casting
                ArrayList<Float> temp1 = (ArrayList<Float>) o1;
                ArrayList<Float> temp2 = (ArrayList<Float>) o2;
                // Then compare those inner arrays' indexes// here says which column to compaare
                return temp1.get(0).compareTo(temp2.get(0));
            }
        });
        // Clear the old one and add the ordered list into
        sameDoc_NodeListArray.clear();
        for (Object o : temp) {
            sameDoc_NodeListArray.add((ArrayList<Object>) o);
        }
        return sameDoc_NodeListArray;
    }

    private static Map<String, Float> sortByComparator(Map<String, Float> unsortMap, final boolean order) {

        List<Map.Entry<String, Float>> list = new LinkedList<Map.Entry<String, Float>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Float>>() {
            public int compare(Map.Entry<String, Float> o1,
                    Map.Entry<String, Float> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Float> sortedMap = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    private static List<Object> SetShortDocumentNode(DocumentNode nod, List<Object> shortDocumentNode, NodeDocument nodeDocumentObj, Long nodeDocTime, long longDate) throws Exception {
        shortDocumentNode.clear();
        shortDocumentNode.add(0, nod.getScore11());
        shortDocumentNode.add(1, nod.getNodeID());
        shortDocumentNode.add(2, nod.getURL());
        nodeDocumentObj = DataHandlerFacade.getNodeDocument(nod.getNodeID(), nod.getDocID());
        nodeDocTime = nodeDocumentObj.getTimeStamp();
        if (nodeDocTime == 0) {
            nodeDocTime = longDate;
        }
        shortDocumentNode.add(3, nodeDocTime);
        return shortDocumentNode;
    }

    private static void RetriveToTest(Hashtable DocNodeHashTable) throws Exception {
        Set DocIdList;
        DocIdList = DocNodeHashTable.keySet();
        Iterator itr = DocIdList.iterator();
        while (itr.hasNext()) {
            String DocId = itr.next().toString();
            Doc_Node doc = DataHandlerFacade.getDoc_NodesByDocumentId(DocId);
            System.out.println("#########item has been RETRIVED as: docId:" + doc.getDocId() + ", nodeList:" + decompressString(doc.getNodeList()));
        }
    }

    private static ByteBuffer compressString(String input) throws IOException {
        // Compress the UTF-8 encoded String into a byte[]
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream os = new GZIPOutputStream(baos);
        os.write(input.getBytes("UTF-8"));
        os.finish();
        byte[] compressedBytes = baos.toByteArray();

        // The following code writes the compressed bytes to a ByteBuffer.
        // A simpler way to do this is by simply calling ByteBuffer.wrap(compressedBytes);  
        // However, the longer form below shows the importance of resetting the position of the buffer 
        // back to the beginning of the buffer if you are writing bytes directly to it, since the SDK 
        // will consider only the bytes after the current position when sending data to DynamoDB.  
        // Using the "wrap" method automatically resets the position to zero.
        ByteBuffer buffer = ByteBuffer.allocate(compressedBytes.length);
        buffer.put(compressedBytes, 0, compressedBytes.length);
        buffer.position(0); // Important: reset the position of the ByteBuffer to the beginning
        return buffer;
    }

    private static String decompressString(ByteBuffer input) throws IOException {
        byte[] bytes = input.array();
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPInputStream is = new GZIPInputStream(bais);

        int chunkSize = 1024;
        byte[] buffer = new byte[chunkSize];
        int length = 0;
        while ((length = is.read(buffer, 0, chunkSize)) != -1) {
            baos.write(buffer, 0, length);
        }

        return new String(baos.toByteArray(), "UTF-8");
    }
}
