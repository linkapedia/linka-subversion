/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.exec;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.linkapedia.dynamo.mapper.data.DataUtility;
import com.linkapedia.dynamo.mapper.data.IDataOperations;
import com.linkapedia.dynamo.mapper.format.DocumentNodeToDocNodeMapper;
import com.linkapedia.dynamo.mapper.format.iDynamoMapper;
import java.util.Date;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author andy
 */
public class RunMapperToDocumentNode {

    public static void main(String args[]) {
        System.out.println("****************Getting the context for document node mapping****************");
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        iDynamoMapper mapper = (DocumentNodeToDocNodeMapper) context.getBean("docNodeMaper");
        IDataOperations dataUtil = (DataUtility) context.getBean("dataUtility");
        try {
            System.out.println("****************Getting the node documents****************");
            List<NodeDocument> nodeDocuments = dataUtil.getDataFromNodeDocument();
            System.out.println("+++++++++++++Number of node Documents: " + nodeDocuments.size());
            System.out.println("****************Mapping the tables****************");
            
//            List<DocumentNode> docsToStore = mapper.getDocumentNodes(nodeDocuments);
//            System.out.println("****************Saving the data****************");
//            dataUtil.saveDataDocumentNodes(docsToStore);
           

            DocumentNode documentNode;
            int itemProcessed =0;
            Date dateTime = new Date();
            for (NodeDocument nodeDocument : nodeDocuments) {
                System.out.println("Converting node document: " + nodeDocument.getNodeID() + " - " + nodeDocument.getDocID());
                documentNode = mapper.getDocumentNode(nodeDocument,dateTime.getTime());
                System.out.println("Trying to save document node: "  + documentNode.getDocID() + "-" + documentNode.getNodeID());
                dataUtil.saveDataDocumentNode(documentNode);
                System.out.println("******document node saved******");
                itemProcessed++;
                System.out.println("Items processed: " + itemProcessed + " of:" + nodeDocuments.size());
                
            }
             System.out.println("****************Done****************");

        } catch (Exception ex) {
            System.out.println("Exception:" + ex);
        }
    }
}
