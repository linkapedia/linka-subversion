/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.dynamo.mapper.data;

import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.util.List;

/**
 *
 * @author andres
 */
public class DataUtility implements IDataOperations {

    private String Enviromment;

    public DataUtility(String Enviromment) {
        this.Enviromment = Enviromment;
    }

    public List<DocumentNode> getDataFromDocumentNode() throws Exception {
        try {
            return DataHandlerFacade.ScanDocumentNode();

        } catch (Exception ex) {
            throw ex;
        }

    }

    public void saveDataDoc_Node(List<Doc_Node> nodesToInsert) {
        System.out.println("Inserting new doc node");
        System.out.println("Size of new nodes: " + nodesToInsert.size());
        int counter = 0;
        for (Doc_Node node : nodesToInsert) {
            try {
                DataHandlerFacade.saveDoc_Node(node);
                counter++;
                System.out.println("++++ document: " +  node.getDocId() + " inserted++++");
            } catch (Exception ex) {
                ex.printStackTrace();
                ex.getMessage();
            }
        }
        System.out.println(counter + " items inserted to Doc_Node! number of keys: " + nodesToInsert.size());
    }

    public List<NodeDocument> getDataFromNodeDocument() throws Exception {
        try {
            return DataHandlerFacade.ScanNodeDocument();
        } catch (Exception ex) {
            throw ex;
        }

    }

    public void saveDataDocumentNodes(List<DocumentNode> nodesToInsert) {
        System.out.println("Insering new document node");
        System.out.println("Size of Document nodes: " + nodesToInsert.size());
        int counter = 0;
        for (DocumentNode node : nodesToInsert) {
            try {
                DataHandlerFacade.saveDocumentNode(node);
                counter++;
            } catch (Exception ex) {
                ex.printStackTrace();
                ex.getMessage();
            }
        }
    }

    public void saveDataDocumentNode(DocumentNode nodeToInsert) {
        try {
            DataHandlerFacade.saveDocumentNode(nodeToInsert);
        } catch (Exception ex) {
            ex.printStackTrace();
            ex.getMessage();
        }
    }
}