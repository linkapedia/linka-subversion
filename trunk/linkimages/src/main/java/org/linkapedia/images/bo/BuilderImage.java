package org.linkapedia.images.bo;

import java.awt.image.BufferedImage;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.linkapedia.images.util.ImageCreation;
import org.linkapedia.images.util.ManageResourcesUtil;

/**
 *
 * @author andres
 */
public class BuilderImage {

    private static final Logger log = Logger.getLogger(BuilderImage.class);
    BufferedImage image;
    private static final int DEFAULT_BOXH = 100;
    private static final int DEFAULT_BOXW = 100;
    public static final String SITE_IMAGE = "siteimage";
    public static final String NOCROP_IMAGE = "nocropimage";

    public BuilderImage(BufferedImage image) {
        this.image = image;
    }

    public BufferedImage buildSiteImage() {
        return buildSiteImage(getBoxW(SITE_IMAGE), getBoxH(SITE_IMAGE));
    }

    public BufferedImage buildSiteImage(int boxw, int boxh) {
        log.debug("BuilderImage: buildSiteImage(int,int)");
        BufferedImage background = ImageCreation.scaleImage(image, boxh, boxw,
                false);
        background = ImageCreation.createLayerTransparent(background);
        BufferedImage mini = null;
        if ((this.image.getHeight() <= boxh) && (this.image.getWidth() <= boxw)) {
            mini = this.image;
        } else {
            mini = ImageCreation.scaleImage(image, boxh, boxw, true);
        }
        background = ImageCreation.createCompositeImage(background, mini,
                mini.getHeight(), mini.getWidth(),
                (boxw - mini.getWidth()) / 2, (boxh - mini.getHeight()) / 2);
        try {
            background = ImageCreation
                    .convertToJPG(background, new Float(0.25));
        } catch (IOException e) {
            log.error("Error building jpg image: ", e);

        } catch (Exception e) {
            log.error("Error building jpg image: ", e);
        }
        return background;
    }

    public BufferedImage buildNoCropImage() {
        return buildNoCropImage(getBoxW(NOCROP_IMAGE), getBoxH(NOCROP_IMAGE));
    }

    public BufferedImage buildNoCropImage(int boxw, int boxh) {
        log.debug("BuilderImage: buildNoCropImage(int,int)");
        BufferedImage dest = this.image;
        int width = dest.getWidth();
        int height = dest.getHeight();
        try {
            if (width >= boxw && height >= boxh) {
                if (width * boxh >= height * boxw) {
                    dest = ImageCreation.scaleImage(dest, boxh, width, true);
                } else {
                    dest = ImageCreation.scaleImage(dest, height, boxw, true);
                }
                width = dest.getWidth();
                height = dest.getHeight();
                // crop middle
                dest = dest.getSubimage((width - boxw) / 2, (height - boxh) / 2,
                        boxw, boxh);

                try {
                    dest = ImageCreation
                            .convertToJPG(dest, new Float(0.25));
                } catch (IOException e) {
                    log.error("Error building jpg image: ", e);
                } catch (Exception e) {
                    log.error("Error building jpg image: ", e);
                }

                return dest;
            } else {
                //not scale because the image is very little
                return null;
            }
        } catch (Exception e) {
            log.error("Error creating image nocrop", e);
            return null;
        }
    }

    private Integer getBoxH(String type) {
        Integer boxh = DEFAULT_BOXH;
        try {
            boxh = Integer.parseInt(ManageResourcesUtil.getProperties()
                    .getProperty("org.linkapedia.images." + type + ".boxh",
                    String.valueOf(DEFAULT_BOXH)));
        } catch (NumberFormatException e) {
            log.error("Error getting parameter BOXH from properties ", e);
        }
        return boxh;
    }

    private int getBoxW(String type) {
        Integer boxw = DEFAULT_BOXW;
        try {
            boxw = Integer.parseInt(ManageResourcesUtil.getProperties()
                    .getProperty("org.linkapedia.images." + type + ".boxw",
                    String.valueOf(DEFAULT_BOXW)));
        } catch (NumberFormatException e) {
            log.error("Error getting parameter BOXH from properties ", e);
        }
        return boxw;
    }
}
