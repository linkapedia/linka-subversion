package org.linkapedia.images.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.filters.FilterImages;
import org.linkapedia.images.filters.FilterResources;
import org.linkapedia.images.filters.FilterValidator;
import org.linkapedia.images.filters.config.DefaultFilterImages;
import org.linkapedia.images.util.AppUtil;
import org.linkapedia.images.wrapper.PackImages;

/**
 * @author andres
 * @verion 0.4 BETA
 * 
 *         manage image to html documents
 */

public class ResourceHtml {

	private static final Logger LOG = Logger.getLogger(ResourceHtml.class);
	private static final int TIMEOUT_READ = 30000;
	private static final int TIMEOUT_CONNECTION = 30000;
	private static final int TIME_OUT_JSOUP = 30000;
	private static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.168 Safari/535.19";

	/**
	 * parser InputStream in document
	 * 
	 * @param is
	 * @param charset
	 * @param baseUri
	 * @param pf
	 * @return
	 * @throws Exception
	 */
	public static PackImages getHtmlImages(InputStream is, String charset,
			String baseUri, FilterResources pf) throws Exception {
		LOG.debug("getHtmlImages(InputStream, String ,String, PreFilter)");
		if ((is == null) || (pf == null)) {
			throw new InvalidParameterException("Some parameters are NULL");
		}
		PackImages list = null;
		Document doc = null;
		try {
			doc = Jsoup.parse(is, charset, baseUri);
			list = getPackImagesFromDoc(doc, pf);
		} catch (Exception e) {
			LOG.error("Error parser page from InputStream: " + e.getMessage());
		}
		return list;
	}

	/**
	 * parser file in document
	 * 
	 * @param file
	 * @param charset
	 * @param baseUri
	 * @param pf
	 * @return
	 * @throws Exception
	 */
	public static PackImages getHtmlImages(File file, String charset,
			String baseUri, FilterResources pf) throws Exception {
		LOG.debug("getHtmlImages(File, String ,String, PreFilter)");
		if ((file == null) || (pf == null)) {
			throw new InvalidParameterException("Some parameters are NULL");
		}
		PackImages list = null;
		Document doc = null;
		try {
			doc = Jsoup.parse(file, charset, baseUri);
			list = getPackImagesFromDoc(doc, pf);
		} catch (Exception e) {
			LOG.error("Error parser page from file: " + e.getMessage());
		}
		return list;
	}

	/**
	 * parser url in document
	 * 
	 * @param url
	 * @param pf
	 * @return
	 * @throws Exception
	 */
	public static PackImages getHtmlImages(URL url, FilterResources pf)
			throws Exception {
		LOG.debug("getHtmlImages(URL, String ,String, PreFilter)");
		if ((url == null) || (pf == null)) {
			throw new InvalidParameterException("Some parameters are NULL");
		}
		PackImages list = null;
		Document doc = null;
		try {
			// get elements
			doc = Jsoup.connect(url.toExternalForm()).userAgent("Mozilla")
					.timeout(TIME_OUT_JSOUP).get();
			// if the page need redirect
			Elements elemets = doc.getElementsByTag("META");
			String meta = "";
			if ((elemets != null) && (!elemets.isEmpty())) {
				for (Element el : elemets) {
					meta = el.attr("HTTP-EQUIV");
					if ((meta != null) && (meta.equalsIgnoreCase("REFRESH"))) {
						url = new URL(AppUtil.getRedirectURL(el));
						doc = Jsoup.connect(url.toExternalForm())
								.userAgent("Mozilla").get();
						break;
					}
				}
			}
			list = getPackImagesFromDoc(doc, pf);
		} catch (Exception e) {
			LOG.info("Error get page from url:" + e.getMessage() + " "
					+ url.toExternalForm());
			throw e;
		}
		return list;
	}

	/**
	 * get images from a document already parser
	 * 
	 * @param doc
	 * @param pf
	 * @return
	 */
	private static PackImages getPackImagesFromDoc(Document doc,
			FilterResources pf) throws Exception {
		LOG.debug("getPackImagesFromDoc(Document, PreFilter)");
		PackImages list = new PackImages();
		Elements elements = doc.getElementsByTag("img");
		if (elements.isEmpty()) {
			throw new Exception("The html file not have images");
		}
		int num = pf.getNum();
		Map<String, URL> mapURLS = new LinkedHashMap<String, URL>();
		String urlImage = null;
		int i = 1;
		String elementString = "";
		// get the images from html
		LOG.info("Go to get images from html file");
		for (Element element : elements) {
			try {
				// get absolute path of the image
				urlImage = element.attr("abs:src");
				elementString = element.toString();
				// filter
				if (AppUtil.wordsIn(pf.getWordsIn(), elementString)
						&& (AppUtil.wordsOut(pf.getWordsOut(), elementString))) {
					mapURLS.put(urlImage, new URL(urlImage));
				}
				// get a limit number images (filter)
				if (num != 0) {
					if (i == num)
						break;
				}
				i++;
			} catch (Exception e) {
				LOG.error("Error build url: " + e.getMessage() + " " + urlImage);
			}
		}
		if (mapURLS.isEmpty()) {
			throw new Exception("Not found good images");
		}

		// get type and size of the images
		BufferedImage bi = null;
		// this is to calculate time to get all images
		long n = System.currentTimeMillis();
		ImageInfo ii = null;
		InputStream is = null;
		HttpURLConnection con = null;
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		byte[] b = new byte[1024];
		int length;
		LOG.info("Get images in a BufferedImage");
		FilterImages filter = new DefaultFilterImages();
		FilterValidator fValidator = new FilterValidator(filter);
		for (URL resource : mapURLS.values()) {
			try {
				length = -1;
				bao.reset();
				con = (HttpURLConnection) resource.openConnection();
				con.setReadTimeout(TIMEOUT_READ);
				con.setConnectTimeout(TIMEOUT_CONNECTION);
				// my user agent (use any user agent)
				con.setRequestProperty("User-Agent", USER_AGENT);
				con.connect();
				is = con.getInputStream();
				while ((length = is.read(b)) != -1) {
					bao.write(b, 0, length);
				}
				bi = ImageIO.read(new ByteArrayInputStream(bao.toByteArray()));

				if (bi != null) {
					if (fValidator.filter(bi)) {
						ii = new ImageInfo();
						ii.setImage(bi);
						ii.setId(UUID.randomUUID().toString()
								+ System.nanoTime());
						list.add(ii);
						LOG.info("Get image ok! " + resource.toString());
                                                //if pass the filter select this how the better image
						break;
					}
				}
			} catch (Exception e) {
				LOG.error("Get image error! " + e.getMessage() + " "
						+ resource.toString());
			} finally {
				// close resources
				if (is != null) {
					is.close();
				}
				if (con != null) {
					con.disconnect();
				}
			}
		}
		LOG.info("TIME IN SECONDS (HTML IMAGES): "
				+ (System.currentTimeMillis() - n) / 1000);
		return list;
	}
}