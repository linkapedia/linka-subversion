package org.linkapedia.images.init;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.UUID;
import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.bo.BuilderImage;
import org.linkapedia.images.filters.FilterResources;
import org.linkapedia.images.filters.config.DefaultFilterResources;
import org.linkapedia.images.resources.ResourceHtml;
import org.linkapedia.images.wrapper.PackImages;

/**
 * Test
 *
 */
public class Main {

    private static final Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) throws IOException, Exception {
        URL url = new URL("http://www.niubie.com/");
        PackImages list = null;
        ImageInfo ii = null;
        FilterResources pfWeb = new DefaultFilterResources();

        try {
            list = ResourceHtml.getHtmlImages(url, pfWeb);
        } catch (Exception e1) {
            log.error("Error getting images from url: " + url.toString() + e1.getMessage());
        }

        if (list == null || list.isEmpty()) {
            log.info("Not found good images");
        } else {
            ii = list.get(0);
            BuilderImage builder = new BuilderImage(ii.getImage());
            BufferedImage image = builder.buildSiteImage();
            BufferedImage imageNoCrop = builder.buildNoCropImage();
            testWrite("/home/andres/Desktop", ii.getImage(), "image_original");
            testWrite("/home/andres/Desktop", image, "image_site");
            testWrite("/home/andres/Desktop", imageNoCrop, "image_no_crop");
        }


    }

    private static void testWrite(String path, BufferedImage ii, String name) {
        File f = new File(path + "/"
                + name + ".jpg");
        f.mkdirs();
        try {
            ImageIO.write(ii, "JPG", f);
        } catch (Exception e) {
            log.error("Error wtiting better image on disk" + e);
        }
    }
}