/**
 * @author andres
 * @verion 0.2 BETA
 */
package org.linkapedia.images.io;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.linkapedia.images.beans.ImageInfo;
import org.linkapedia.images.util.ImageCreation;
import org.linkapedia.images.wrapper.PackImages;

public class UtilIO {

	private static final Logger LOG = Logger.getLogger(UtilIO.class);

	/**
	 * write images into file system with correct format
	 * 
	 * @param list
	 * @param file
	 * @param createFolders
	 * @param format
	 * @param quality
	 *            : only to jpg format
	 * @throws Exception
	 */
	public static void writeImages(PackImages list, File file,
			boolean createFolders, String format, Float quality)
			throws Exception {
		LOG.debug("writeImages(PackImages list, File file, boolean, String, Float)");
		if (!file.exists()) {
			if (createFolders) {
				LOG.info("Creating directory to save the image");
				file.mkdirs();
			} else {
				LOG.error("The file(folder) not exits");
				return;
			}

		}
		if (!format.equalsIgnoreCase("PNG")
				&& (!format.equalsIgnoreCase("JPG"))) {
			LOG.error("Not support format only JPG or PNG");
			return;
		}
		BufferedImage image = null;
		OutputStream os = null;
		for (ImageInfo ii : list) {
			try {
				os = new FileOutputStream(file.getAbsolutePath()
						+ File.separator + ii.getId() + "."
						+ format.toLowerCase());
				// save image with the correct format
				if (format.equalsIgnoreCase("JPG")) {
					try {
						image = ImageCreation.convertToJPG(ii.getImage(),
								quality.floatValue());
					} catch (Exception e) {
						image = ImageCreation.convertToJPG(ii.getImage());
					}
					if (image != null) {
						ImageIO.write(image, "jpg", os);
					}
				} else if (format.equalsIgnoreCase("PNG")) {
					image = ImageCreation.convertToPNG(ii.getImage());
					if (image != null) {
						ImageIO.write(image, "png", os);
					}
				}
				LOG.info("save image ok!");
			} catch (Exception e) {
				LOG.error("error save image", e);
			}
		}
	}

	/**
	 * 
	 * @param image
	 * @param file
	 * @param createFolders
	 * @param format
	 * @param quality
	 * @throws Exception
	 */
	public static void writeImages(ImageInfo image, File file,
			boolean createFolders, String format, Float quality)
			throws Exception {
		PackImages list = new PackImages();
		list.add(image);
		writeImages(list, file, createFolders, format, quality);
	}

	/**
	 * 
	 * @param image
	 * @param file
	 * @throws Exception
	 */
	public static void writeImages(ImageInfo image, File file) throws Exception {
		writeImages(image, file, true, "jpg", null);

	}

}