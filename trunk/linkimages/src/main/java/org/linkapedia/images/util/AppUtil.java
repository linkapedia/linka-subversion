package org.linkapedia.images.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;

/**
 * 
 * @author andres methods utils to this apps
 * @version 0.1 BETA
 */
public class AppUtil {

	private static final Logger log = Logger.getLogger(AppUtil.class);

	/**
	 * get url in meta tag to redirect page if is necessary
	 * 
	 * @param el
	 * @return
	 */
	public static String getRedirectURL(Element el) {
		log.debug("AppUtil: getRedirectURL(Element)");
		String content = el.attr("content");
		String url = "";
		Pattern pattern = Pattern.compile("\\d+\\;url\\=(.*)");
		Matcher matcher = pattern.matcher(content);
		if (matcher.find()) {
			url = matcher.group(1);
		}
		return url;
	}

	/**
	 * 
	 * 
	 * @param list
	 * @param resource
	 * @return
	 */
	public static boolean wordsIn(List<String> list, String resource) {
		log.debug("AppUtil: wordsIn(PreFilter, String)");
		if (list == null || list.isEmpty()) {
			return true;
		}
		boolean result = true;
		for (String word : list) {
			if (!StringUtils.containsIgnoreCase(resource, word)) {
				result = false;
				break;
			}
		}
		return result;
	}

	/**
	 * 
	 * 
	 * @param list
	 * @param resource
	 * @return
	 */
	public static boolean wordsOut(List<String> list, String resource) {
		log.debug("AppUtil: wordsOut(PreFilter, String)");
		if (list == null || list.isEmpty()) {
			return true;
		}
		boolean result = true;
		for (String word : list) {
			if (StringUtils.containsIgnoreCase(resource, word)) {
				result = false;
				break;
			}
		}
		return result;
	}
}