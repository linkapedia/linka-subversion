package org.linkapedia.images.init;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.linkapedia.images.bo.BuilderImage;

/**
 *
 * @author andres
 */
public class TestTrasformation {

    public static void main(String[] args) throws IOException {

        File f = new File("/home/andres/Desktop/ancha.jpg");
        BufferedImage image = ImageIO.read(f);
        BuilderImage builder = new BuilderImage(image);

        try {
            image = builder.buildNoCropImage();
            ImageIO.write(image, "JPG", new File("/home/andres/Desktop/result.jpg"));
        } catch (Exception e) {
            System.err.println("Error wtiting better image on disk" + e);
        }


    }
}
