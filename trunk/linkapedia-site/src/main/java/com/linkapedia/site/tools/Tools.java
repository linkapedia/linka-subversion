/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.site.tools;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.linkapedia.site.beans.Domain;
import com.linkapedia.site.beans.Taxonomy;
import com.linkapedia.site.beans.Topic;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author alexander
 */
public class Tools {

    private static String urlRest = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.rest.url");

    public static String getDomain(HttpServletRequest request) {
        try {
            String host = new URL(request.getRequestURL().toString()).getHost();
            return host.replace("www.", "").replace(".com", "").replace("test.", "");
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static boolean existsResourceWeb(String URLName) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        }
    }

    public static JSONObject getDataFromRest(String urlrest) throws JSONException {
        try {
            URL url = new URL(urlrest);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            String json = "";
            while ((output = br.readLine()) != null) {
                json += output;
            }

            conn.disconnect();
            return new JSONObject(json);
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public static String escapeText(String url) {
        if (url != null) {
            url = StringEscapeUtils.unescapeXml(url);
            url = url.toLowerCase();
            url = url.trim();
            url = url.replaceAll("-", "");
            url = url.replaceAll("\\s{2,}", " ");
            url = url.replaceAll(" ", "-");

            String result = new String();
            for (char c : url.toCharArray()) {
                if (Pattern.matches("[a-zA-z0-9-]", String.valueOf(c))) {
                    result = result.concat(String.valueOf(c));
                }
            }
            return result;
        }
        return url;
    }

    public static String getKissmetricsId(String domainId) {
        try {
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId);
            return rest.getString("kissmetricsId");
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static String getGoogleAnalyticsId(String domainId) {
        try {
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId);
            return rest.getString("googleAnalyticsId");
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static List<JSONObject> getTaxonomiesByDomain(String domainId) {
        try {
            List<JSONObject> listTaxonomies = new ArrayList<JSONObject>();
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies");
            JSONArray taxonomies = rest.getJSONArray("taxonomies");
            int size = taxonomies.length();

            for (int i = 0; i < size; i++) {
                JSONObject temp = taxonomies.getJSONObject(i);

                rest = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies/" + temp.getString("id"));
                listTaxonomies.add(rest);
            }

            return listTaxonomies;
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static Topic getTopicInf(String domainId, String taxonomyId, String topicId) {
        try {
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies/" + taxonomyId + "/topics/" + topicId);
            taxonomyId = rest.getJSONObject("taxonomy").getString("id");
            Taxonomy taxonomy = getTaxonomyInf(domainId, taxonomyId);
            Topic topic = new Topic();
            topic.setId(rest.getString("id"));
            topic.setTitle(rest.getString("title"));
            
            if(!rest.isNull("description")){
                String desc = rest.getString("description").trim();
                if(!desc.equals("")){
                   topic.setDescription(desc);
                }else{
                    if(!taxonomy.getDescription().trim().equals("")){
                        topic.setDescription(taxonomy.getDescription());
                    }else{
                        topic.setDescription(topic.getTitle());
                    }                    
                }
            }else{
                topic.setDescription(topic.getTitle());
            }
            
            topic.setTaxonomy(taxonomy);
            topic.setParentId(rest.getString("parentId"));
            
            JSONArray childrensJson = rest.getJSONArray("children");
            List<Topic> childrens = new ArrayList<Topic>();
            int size = childrensJson.length();
            
            for(int i=0; i < size; i++){
                JSONObject childrenJson = childrensJson.getJSONObject(i);
                Topic topicTemp = new Topic();
                topicTemp.setId(childrenJson.getString("id"));
                topicTemp.setTitle(childrenJson.getString("title"));
                topicTemp.setDescription(childrenJson.getString("description"));
                
                childrens.add(topic);
            }
            
            topic.setChildren(childrens);
            topic.setType(rest.getString("type"));
            topic.setAncestors(getAncestors(domainId, taxonomyId, topicId));
            
            return topic;
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static Taxonomy getTaxonomyInf(String domainId, String taxonomyId) {
        try {
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies/" + taxonomyId);
            Taxonomy taxonomy = new Taxonomy();
            taxonomy.setId(rest.getString("id"));
            taxonomy.setName(rest.getString("name"));
            
            if(!rest.isNull("description")){
                taxonomy.setDescription(rest.getString("description"));
            }
            taxonomy.setStatus(rest.getString("status"));
            taxonomy.setActive(rest.getBoolean("active"));
            
            Topic mainTopic = new Topic();
            mainTopic.setId(rest.getJSONObject("mainTopic").getString("id"));
            mainTopic.setTitle(rest.getJSONObject("mainTopic").getString("title"));
            taxonomy.setMainTopic(mainTopic);
            
            Domain domain = new Domain();
            domain.setId(rest.getJSONObject("domain").getString("id"));
            taxonomy.setDomains(domain);
            
            List<Topic> topics = new ArrayList<Topic>();
            JSONArray topicsJson = rest.getJSONArray("topics");
            int size = topicsJson.length();
            
            for(int i=0; i < size; i++){
                JSONObject topicJson = topicsJson.getJSONObject(i);
                Topic topic = new Topic();
                topic.setId(topicJson.getString("id"));
                topic.setTitle(topicJson.getString("title"));
                
                topics.add(topic);
            }
            
            taxonomy.setTopics(topics);            
            
            return taxonomy;
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static List<Topic> getAncestors(String domainId, String taxonomyId, String topicId) {
        try {
            List<Topic> ancestors = new ArrayList<Topic>();
            String parentId;

            do {
                JSONObject topic = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies/" + taxonomyId + "/topics/" + topicId);
                Topic temp = new Topic();
                
                
                if (topic != null) {
                    temp.setId(topic.getString("id"));
                    temp.setTitle(topic.getString("title"));
                    ancestors.add(temp);
                    parentId = topic.getString("parentId");
                } else {
                    break;
                }
            } while (!parentId.equals("-1"));
            Collections.reverse(ancestors);
            return ancestors;
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static JSONArray getDocuments(String domainId, String taxonomyId, String topicId) {
        try {
            String linksLimit = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.links.limit");
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies/" + taxonomyId + "/topics/" + topicId + "/documents?limit=" + linksLimit + "&page=1");
            return rest.getJSONArray("documents");
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public static JSONObject getDocument(String domainId, String taxonomyId, String topicId, String docId) {
        try {
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies/" + taxonomyId + "/topics/" + topicId + "/documents/" + docId);
            return rest;
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }
    
    public static Domain getDomain(String domainId){
        try{
            Domain domain = new Domain();
            JSONObject rest = getDataFromRest(urlRest + "domains/" + domainId);
            List<Taxonomy> taxonomies = new ArrayList<Taxonomy>();
            
            domain.setId(rest.getString("id"));
            domain.setTitle(rest.getString("title"));
            domain.setKissmetricsId(rest.getString("kissmetricsId"));
            domain.setGoogleAnalyiticId(rest.getString("googleAnalyticsId"));
            
            int size = rest.getJSONArray("taxonomies").length();

            for (int i = 0; i < size; i++) {
                JSONObject temp = rest.getJSONArray("taxonomies").getJSONObject(i);

                temp = getDataFromRest(urlRest + "domains/" + domainId + "/taxonomies/" + temp.getString("id"));
                
                Taxonomy taxonomy = new Taxonomy();
                
                taxonomy.setId(temp.getString("id"));
                taxonomy.setName(temp.getString("name"));
                taxonomy.setDescription(temp.getString("description"));
                taxonomy.setStatus(temp.getString("status"));
                taxonomy.setActive(temp.getBoolean("active"));
                
                Topic topic = new Topic();
                
                topic.setId(temp.getJSONObject("mainTopic").getString("id"));
                topic.setTitle(temp.getJSONObject("mainTopic").getString("title"));
                
                taxonomy.setMainTopic(topic);
                taxonomies.add(taxonomy);
            }
            domain.setTaxonomies(taxonomies);
            return domain;
        }catch(JSONException ex){
            ex.printStackTrace(System.out);
        }            
        return null;
    }

    public static boolean isMobileRequest(String userAgent) {
        if (userAgent.contains("Android")
                || userAgent.contains("webOS")
                || userAgent.contains("iPhone")
                || userAgent.contains("iPad")
                || userAgent.contains("iPod")
                || userAgent.contains("BlackBerry")
                || userAgent.contains("Windows Phone")) {
            return true;
        }
        return false;
    }
}
