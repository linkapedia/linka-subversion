/*
 * To change this template, choose VmTools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.site.controllers;

import com.linkapedia.site.tools.ManageResourcesUtils;
import com.linkapedia.site.tools.Tools;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author alexander
 */
@Controller
public class HomeController {
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String createPage(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException{
        String domain = Tools.getDomain(request);     
        String urlMobilSite = "http://m." + domain + ".com/#/";         
        
        
        String userAgent = request.getHeader("User-Agent");
        if(Tools.isMobileRequest(userAgent)){
            response.sendRedirect(urlMobilSite);
        }
        
        return "home";
    }
    
    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String aboutPage(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException{   
        String domain = Tools.getDomain(request);
        String urlMobilSite = "http://m." + domain + ".com/#/";
        String userAgent = request.getHeader("User-Agent");
        if(Tools.isMobileRequest(userAgent)){
            response.sendRedirect(urlMobilSite);
        }
        return "about";
    }
    
    @RequestMapping(value = "/privacy", method = RequestMethod.GET)
    public String privacyPage(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException{   
        String domain = Tools.getDomain(request);
        String urlMobilSite = "http://m." + domain + ".com/#/";       
        String userAgent = request.getHeader("User-Agent");
        if(Tools.isMobileRequest(userAgent)){
            response.sendRedirect(urlMobilSite);
        }
        return "privacy";
    }
}
