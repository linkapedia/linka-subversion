/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.site.controllers;

import com.amazonaws.util.json.JSONException;
import com.linkapedia.site.beans.Domain;
import com.linkapedia.site.tools.ManageResourcesUtils;
import com.linkapedia.site.tools.Tools;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author alexander
 */
@Controller
public class Taxonomies {

    @RequestMapping(value = "/taxonomies", method = RequestMethod.GET)
    public String createPage(ModelMap model, HttpServletRequest request) throws JSONException {
        String domainName = Tools.getDomain(request);             
        String urlMobilSite = "http://m." + domainName + ".com/#/";

        if (domainName.equals("localhost")) {
            domainName = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.domain.search");
        }

        Domain domain = Tools.getDomain(domainName);
        
        model.put("urlMobilSite", urlMobilSite);
        model.put("domaininfo", domain);
        return "taxonomies";
    }
}
