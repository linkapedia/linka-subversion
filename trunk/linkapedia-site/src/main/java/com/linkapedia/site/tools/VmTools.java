/*
 * To change this template, choose VmTools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.site.tools;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author alexander
 */
public class VmTools {    

    public String escapeText(String url) {
        if (url != null) {
            url = StringEscapeUtils.unescapeXml(url);
            url = url.toLowerCase();
            url = url.trim();
            url = url.replaceAll("-", "");
            url = url.replaceAll("\\s{2,}", " ");
            url = url.replaceAll(" ", "-");

            String result = new String();
            for (char c : url.toCharArray()) {
                if (Pattern.matches("[a-zA-z0-9-]", String.valueOf(c))) {
                    result = result.concat(String.valueOf(c));
                }
            }
            return result;
        }
        return url;
    }
    
    public boolean exists(String url) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return false;
        }
    }
}
