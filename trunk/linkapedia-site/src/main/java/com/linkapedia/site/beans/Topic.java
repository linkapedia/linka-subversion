package com.linkapedia.site.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Topic domain object.
 *
 * @author Jose Bravo <jose.bravo at yuxipacific.com>
 */
public class Topic implements Serializable {

    private String id;
    private String title;
    private String description;
    transient private List<String> mustHaves;
    transient private List<String> signatures;
    private Taxonomy taxonomy;
    private String parentId;
    private String path;
    private List<Topic> children;
    transient private Integer childrenIndexWithInParent;
    private String type;
    //TODO: Implement sorted hash map
    private List<Topic> ancestors;

    public Topic() {
    }

    public Topic(final Topic otherTopic) {
        this.id = otherTopic.getId();
        this.title = otherTopic.getTitle();
        this.description = otherTopic.getDescription();
        this.taxonomy = otherTopic.getTaxonomy();
        this.parentId = otherTopic.getParentId();
        this.children = otherTopic.getChildren();
        this.type = otherTopic.getType();
        this.ancestors = otherTopic.getAncestors();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    public Topic withId(String id) {
        this.id = id;
        return this;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    public Topic withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the mustHaves
     */
    public List<String> getMustHaves() {
        return mustHaves;
    }

    /**
     * @param mustHaves the mustHaves to set
     */
    public void setMustHaves(List<String> mustHaves) {
        this.mustHaves = mustHaves;
    }

    /**
     * @return the signatures
     */
    public List<String> getSignatures() {
        return signatures;
    }

    /**
     * @param signatures the signatures to set
     */
    public void setSignatures(List<String> signatures) {
        this.signatures = signatures;
    }

    public Taxonomy getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(Taxonomy taxonomy) {
        this.taxonomy = taxonomy;
    }

    /**
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the children
     */
    public List<Topic> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(List<Topic> children) {
        this.children = children;
    }

    /**
     * @return the childrenIndexWithInParent
     */
    public Integer getChildrenIndexWithInParent() {
        return childrenIndexWithInParent;
    }

    /**
     * @param childrenIndexWithInParent the childrenIndexWithInParent to set
     */
    public void setChildrenIndexWithInParent(Integer childrenIndexWithInParent) {
        this.childrenIndexWithInParent = childrenIndexWithInParent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Topic> getAncestors() {
        return ancestors;
    }

    public void setAncestors(List<Topic> ancestors) {
        this.ancestors = ancestors;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 17 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 17 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 17 * hash + (this.mustHaves != null ? this.mustHaves.hashCode() : 0);
        hash = 17 * hash + (this.signatures != null ? this.signatures.hashCode() : 0);
        hash = 17 * hash + (this.taxonomy != null ? this.taxonomy.hashCode() : 0);
        hash = 17 * hash + (this.parentId != null ? this.parentId.hashCode() : 0);
        hash = 17 * hash + (this.path != null ? this.path.hashCode() : 0);
        hash = 17 * hash + (this.children != null ? this.children.hashCode() : 0);
        hash = 17 * hash + (this.childrenIndexWithInParent != null ? this.childrenIndexWithInParent.hashCode() : 0);
        hash = 17 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 17 * hash + (this.ancestors != null ? this.ancestors.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Topic other = (Topic) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        if (this.mustHaves != other.mustHaves && (this.mustHaves == null || !this.mustHaves.equals(other.mustHaves))) {
            return false;
        }
        if (this.signatures != other.signatures && (this.signatures == null || !this.signatures.equals(other.signatures))) {
            return false;
        }
        if (this.taxonomy != other.taxonomy && (this.taxonomy == null || !this.taxonomy.equals(other.taxonomy))) {
            return false;
        }
        if ((this.parentId == null) ? (other.parentId != null) : !this.parentId.equals(other.parentId)) {
            return false;
        }
        if ((this.path == null) ? (other.path != null) : !this.path.equals(other.path)) {
            return false;
        }
        if (this.children != other.children && (this.children == null || !this.children.equals(other.children))) {
            return false;
        }
        if (this.childrenIndexWithInParent != other.childrenIndexWithInParent && (this.childrenIndexWithInParent == null || !this.childrenIndexWithInParent.equals(other.childrenIndexWithInParent))) {
            return false;
        }
        if ((this.type == null) ? (other.type != null) : !this.type.equals(other.type)) {
            return false;
        }
        if (this.ancestors != other.ancestors && (this.ancestors == null || !this.ancestors.equals(other.ancestors))) {
            return false;
        }
        return true;
    }
}
