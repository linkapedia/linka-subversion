package com.linkapedia.site.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Document domain object.
 *
 * @author Jose Bravo <jose.bravo at yuxipacific.com>
 */
public class Document implements Serializable {

    private String id;
    private String title;
    private String url;
    private String digest;
    private long timestamp;
    private List<Topic> topics;

    public Document() {
    }

    public Document(final Document otherDocument) {
        this.id = otherDocument.getId();
        this.title = otherDocument.getTitle();
        this.url = otherDocument.getUrl();
        this.digest = otherDocument.getDigest();
        this.timestamp = otherDocument.getTimestamp();
        this.topics = otherDocument.getTopics();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the digest
     */
    public String getDigest() {
        return digest;
    }

    /**
     * @param digest the digest to set
     */
    public void setDigest(String digest) {
        this.digest = digest;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the topics
     */
    public List<Topic> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }
}
