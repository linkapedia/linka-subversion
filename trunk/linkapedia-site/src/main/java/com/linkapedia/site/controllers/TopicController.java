package com.linkapedia.site.controllers;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.linkapedia.site.beans.Topic;
import com.linkapedia.site.tools.ManageResourcesUtils;
import com.linkapedia.site.tools.Tools;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author andres
 */
@Controller
public class TopicController {

    @RequestMapping(value = "/topics/{taxonomyName}/{topicTitle}/{topicId}", method = RequestMethod.GET)
    public String createPage(@PathVariable String topicId, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String domainName = Tools.getDomain(request);
        String urlMobilSite = "http://m." + domainName + ".com/#/";

        if (domainName.equals("localhost")) {
            domainName = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.domain.search");
        }

        Topic topic = Tools.getTopicInf(domainName, null, topicId);
        String domainFromTopic = topic.getTaxonomy().getDomain().getId();

        String userAgent = request.getHeader("User-Agent");
        List<JSONObject> listTaxonomies = Tools.getTaxonomiesByDomain(domainName);
        int topicsLimit = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.topics.limit"));

        if (!domainName.equals(domainFromTopic)) {
            if (Tools.isMobileRequest(userAgent)) {
                urlMobilSite = "http://m." + domainFromTopic + ".com/#/";
                urlMobilSite += "page/" + Tools.escapeText(topic.getTaxonomy().getName()) + "/" + Tools.escapeText(topic.getTitle()) + "/" + topicId;
                response.sendRedirect(urlMobilSite);
            } else {
                response.sendRedirect("http://www." + domainFromTopic + ".com/topics/" + Tools.escapeText(topic.getTaxonomy().getName()) + "/" + Tools.escapeText(topic.getTitle()) + "/" + topicId);

            }
        } else {
            if (Tools.isMobileRequest(userAgent)) {
                urlMobilSite += "page/" + Tools.escapeText(topic.getTaxonomy().getName()) + "/" + Tools.escapeText(topic.getTitle()) + "/" + topicId;
                response.sendRedirect(urlMobilSite);
            }
        }

        if (topic.getChildren().isEmpty()) {
            model.put("messageEmpty", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.message.topic.childrens.empty"));
        }

        if (topic.getChildren().size() > topicsLimit) {
            List<Topic> sublist = topic.getChildren().subList(topicsLimit, topic.getChildren().size());
            model.put("arrayJson", new Gson().toJson(sublist));
        } else {
            model.put("arrayJson", "[]");
        }

        model.put("topicsLimit", topicsLimit);
        model.put("taxonomiesInfo", listTaxonomies);

        model.put("taxonomySearch", topic.getTaxonomy().getId());
        model.put("taxonomy_name", topic.getTaxonomy().getName());


        try {
            if (topic.getType().equals("link")) {
                int linksLimit = Integer.parseInt(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.links.limit"));
                JSONArray documents = Tools.getDocuments(domainName, topic.getTaxonomy().getId(), topicId);
                List<JSONObject> finalDocuments = removeEmptyDocuments(documents);

                if (finalDocuments.isEmpty()) {
                    model.put("messageEmpty", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.message.topic.documents.empty"));
                }
                model.put("linksLimit", linksLimit);
                model.put("documents", finalDocuments);
                return "links";
            }
        } catch (JSONException ex) {
            ex.printStackTrace(System.out);
        }
        return "topics";
    }

    private List<JSONObject> removeEmptyDocuments(JSONArray documents) throws JSONException {
        List<JSONObject> newdocs = new ArrayList<JSONObject>();
        int size = documents.length();
        for (int i = 0; i < size; i++) {
            String digest = documents.getJSONObject(i).getString("digest");
            if (!digest.trim().equals("")) {
                newdocs.add(documents.getJSONObject(i));
            }
        }
        return newdocs;
    }
}
