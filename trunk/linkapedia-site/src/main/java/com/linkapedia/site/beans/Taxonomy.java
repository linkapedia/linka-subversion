package com.linkapedia.site.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Taxonomy domain object.
 *
 * @author Jose Bravo <jose.bravo at yuxipacific.com>
 */
public class Taxonomy implements Serializable {

    protected String id;
    protected String title;
    protected String name;
    protected String description;
    protected String status;
    protected Boolean active;
    protected String rootNodeId;    
    transient protected Domain domain;
    protected List<Topic> topics;
    protected Topic mainTopic;

    public Taxonomy() {
    }

    public Taxonomy(final Taxonomy otherTaxonomy) {
        this.id = otherTaxonomy.getId();
        this.title = otherTaxonomy.getTitle();
        this.name = otherTaxonomy.getName();
        this.description = otherTaxonomy.getDescription();
        this.status = otherTaxonomy.getStatus();
        this.active = otherTaxonomy.isActive();        
        this.domain = otherTaxonomy.getDomain();
        this.topics = otherTaxonomy.getTopics();
        this.rootNodeId = otherTaxonomy.getRootNodeId();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    public String getRootNodeId() {
        return rootNodeId;
    }

    public void setRootNodeId(String rootNodeId) {
        this.rootNodeId = rootNodeId;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return if taxonomy is active
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomains(Domain domain) {
        this.domain = domain;
    }

    /**
     * @return the topics
     */
    public List<Topic> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public Topic getMainTopic() {
        return mainTopic;
    }

    public void setMainTopic(Topic mainTopic) {
        this.mainTopic = mainTopic;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 59 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 59 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 59 * hash + (this.status != null ? this.status.hashCode() : 0);
        hash = 59 * hash + (this.active != null ? this.active.hashCode() : 0);        
        hash = 59 * hash + (this.domain != null ? this.domain.hashCode() : 0);
        hash = 59 * hash + (this.topics != null ? this.topics.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Taxonomy other = (Taxonomy) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        if ((this.status == null) ? (other.status != null) : !this.status.equals(other.status)) {
            return false;
        }
        if (this.active != other.active && (this.active == null || !this.active.equals(other.active))) {
            return false;
        }
        if (this.domain != other.domain && (this.domain == null || !this.domain.equals(other.domain))) {
            return false;
        }
        if (this.topics != other.topics && (this.topics == null || !this.topics.equals(other.topics))) {
            return false;
        }
        return true;
    }
}
