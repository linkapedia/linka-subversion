package com.linkapedia.site.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Domain object.
 *
 * @author Jose Bravo <jose.bravo at yuxipacific.com>
 */
public class Domain implements Serializable {

    protected String id;
    protected String title;
    protected String kissmetricsId;
    protected String googleAnalyiticId;
    protected String adSenseId;        
    protected List<Taxonomy> taxonomies;

    public Domain() {
    }

    public Domain(final Domain other) {
        this.id = other.getId();
        this.title = other.getTitle();              
        this.taxonomies = other.getTaxonomies();
        this.kissmetricsId = other.getKissmetricsId();
        this.adSenseId = other.getAdSenseId();
        this.googleAnalyiticId = other.getGoogleAnalyiticId();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getKissmetricsId() {
        return kissmetricsId;
    }

    public void setKissmetricsId(String kissmetricsId) {
        this.kissmetricsId = kissmetricsId;
    }

    public String getGoogleAnalyiticId() {
        return googleAnalyiticId;
    }

    public void setGoogleAnalyiticId(String googleAnalyiticId) {
        this.googleAnalyiticId = googleAnalyiticId;
    }

    public String getAdSenseId() {
        return adSenseId;
    }

    public void setAdSenseId(String adSenseId) {
        this.adSenseId = adSenseId;
    }

    /**
     * @return the taxonomies
     */
    public List<Taxonomy> getTaxonomies() {
        return taxonomies;
    }

    /**
     * @param taxonomies the taxonomies to set
     */
    public void setTaxonomies(List<Taxonomy> taxonomies) {
        this.taxonomies = taxonomies;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 41 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 41 * hash + (this.kissmetricsId != null ? this.kissmetricsId.hashCode() : 0);
        hash = 41 * hash + (this.googleAnalyiticId != null ? this.googleAnalyiticId.hashCode() : 0);
        hash = 41 * hash + (this.adSenseId != null ? this.adSenseId.hashCode() : 0);            
        hash = 41 * hash + (this.taxonomies != null ? this.taxonomies.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Domain other = (Domain) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.kissmetricsId == null) ? (other.kissmetricsId != null) : !this.kissmetricsId.equals(other.kissmetricsId)) {
            return false;
        }
        if ((this.googleAnalyiticId == null) ? (other.googleAnalyiticId != null) : !this.googleAnalyiticId.equals(other.googleAnalyiticId)) {
            return false;
        }
        if ((this.adSenseId == null) ? (other.adSenseId != null) : !this.adSenseId.equals(other.adSenseId)) {
            return false;
        }
        if (this.taxonomies != other.taxonomies && (this.taxonomies == null || !this.taxonomies.equals(other.taxonomies))) {
            return false;
        }
        return true;
    }
}
