/*
 * To change this template, choose VmTools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.site.main;

/**
 *
 * @author alexander
 */
import com.linkapedia.site.tools.ManageResourcesUtils;
import com.linkapedia.site.tools.Tools;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ExecuteTimeInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = Logger.getLogger(ExecuteTimeInterceptor.class);

    //before the actual handler will be executed
    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler)
            throws Exception {

        log.info("Hola al sistema");

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        
        
        
        //Set generic variables in to vm template
        setEnviroments(request, modelAndView);
        System.out.println("Post-handle");
    }
    
    @Override
    public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        System.out.println("After completion handle");
    }
    
    private void setEnviroments(HttpServletRequest request, ModelAndView modelAndView){
        String domainName = Tools.getDomain(request); 
        String domainSearch = domainName;
        
        if(domainSearch.equals("localhost")){
            domainSearch = "linkapedia-drinks";
        }
        String kissmetricsId = Tools.getKissmetricsId(domainSearch);
        String googleAnalyticsId = Tools.getGoogleAnalyticsId(domainSearch);
        
        if(domainName.equals("localhost") || domainName.equals("wine.interestplace.com") || domainName.equals("buddhism.interestplace.com")){
            modelAndView.getModel().put("domainSearch", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.domain.search"));
        }else{
            modelAndView.getModel().put("domainSearch", domainName);
        }
        
        modelAndView.getModel().put("kissmetricsId", kissmetricsId);
        modelAndView.getModel().put("googleAnalyticsId", googleAnalyticsId);
        modelAndView.getModel().put("domain", domainName);
        modelAndView.getModel().put("dnsCloudFront", ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.cloudfront.dns"));                
    }        
}
