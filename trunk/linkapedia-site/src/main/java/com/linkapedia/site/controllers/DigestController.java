/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.site.controllers;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.linkapedia.site.tools.ManageResourcesUtils;
import com.linkapedia.site.tools.Tools;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author alexander
 */
@Controller
public class DigestController {

    @RequestMapping(value = "/digest/{topicName}/{topicId}/{docId}", method = RequestMethod.GET)
    public String createPage(@PathVariable String topicName, @PathVariable String topicId, @PathVariable String docId, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException, JSONException {      
        String domainName = Tools.getDomain(request);
        String urlMobilSite = "http://m." + domainName + ".com/#/";
        
        if(domainName.equals("localhost")){
            domainName = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.site.domain.search");
        }
        
        /**
        JSONObject topicFromRest = Tools.getTopicInf(topicId);      
        String domainFromTopic = topicFromRest.getJSONObject("taxonomy").getJSONObject("domain").getString("id");               
        String userAgent = request.getHeader("User-Agent");
        List<JSONObject> listTaxonomies = Tools.getTaxonomiesByDomain(domainName);        
        JSONObject doc = Tools.getDocument(domainName, topicFromRest.getJSONObject("taxonomy").getString("id"), topicId, docId);
        
         if(!domainName.equals(domainFromTopic)){
            if(Tools.isMobileRequest(userAgent)){
                urlMobilSite = "http://m." + domainFromTopic + ".com/#/";
                urlMobilSite += "digest/" + topicName + "/" + topicId + "/" + docId;
                response.sendRedirect(urlMobilSite);
            }else{
                response.sendRedirect("http://www." + domainFromTopic + ".com/digest/" + topicName + "/" + topicId + "/" + docId);
            }            
        }else{
           if(Tools.isMobileRequest(userAgent)){
                urlMobilSite += "digest/" + Tools.escapeText(doc.getString("title")) + "/" + topicFromRest.getString("id") + "/" + doc.getString("id");
                response.sendRedirect(urlMobilSite);
            }
        }
        
        List<JSONObject> ancestors = Tools.getAncestors(topicId);
        
        model.put("ancestors", ancestors);
        model.put("taxonomiesInfo", listTaxonomies);
        model.put("topic", topicFromRest);
        model.put("taxonomySearch", topicFromRest.getJSONObject("taxonomy").getString("id"));
        model.put("taxonomy_name", topicFromRest.getJSONObject("taxonomy").getString("name"));
        model.put("doc", doc);        
        model.put("urlMobilSite", urlMobilSite);
        **/
        return "digest";
    }
}
