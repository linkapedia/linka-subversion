package com.yuxipacific.services.cache.interfaces;

import java.io.Serializable;

/**
 *
 * @author Xander Kno
 */
public interface Cacheable {

    public Object getObject(Serializable objectKey);

    public void setObject(Serializable objectKey, Object objectValue);

    public void deleteObject(Serializable objectKey);

    public boolean objectExists(Serializable objectKey);
}
