package com.yuxipacific;

import com.yuxipacific.documents.network.DownloadProxy;
import com.yuxipacific.documents.parsers.ParserProxy;
import com.yuxipacific.documents.text.SentenceExtractorProxy;
import com.yuxipacific.documents.utils.FileUtils;
import com.yuxipacific.services.cache.CacheHandler;
import java.io.File;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);
    private static final String TEST_URL = "http://www.meristation.com";

    public static void main(String args[]) {
        log.info("Method: {}", "main");
        try {
            CacheHandler.initCacheHandler(new String[]{"documents", "rejected.documents"});
            String downloadPath = DownloadProxy.downloadFile(new URL(TEST_URL), "D:\\");
            log.info("Download Path: {}", downloadPath);
            String title = ParserProxy.getDocTitleFromFile(new File(downloadPath));
            System.out.println("Titulo: " + title);
            String content = ParserProxy.parse(new URL(TEST_URL));
            System.out.println("Contenido: " + content);
            String sentences = SentenceExtractorProxy.extractFrom(content);
            System.out.println("Oraciones: " + sentences);
            FileUtils.writeFile("Test-File", "D:\\", sentences);
        } catch (Exception e) {
            log.error("An exception ocurred: ", e);
        }
    }
}
