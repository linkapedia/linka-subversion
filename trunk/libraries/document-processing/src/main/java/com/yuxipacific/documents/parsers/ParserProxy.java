package com.yuxipacific.documents.parsers;

import com.yuxipacific.documents.storage.exceptions.parser.EmptyDocumentException;
import com.yuxipacific.documents.storage.exceptions.parser.NoParseableDocumentException;
import com.yuxipacific.documents.utils.FileUtils;
import com.yuxipacific.security.utils.DigestUtils;
import com.yuxipacific.services.cache.CacheHandler;
import com.yuxipacific.services.cache.CacheObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import org.apache.log4j.Logger;
import org.apache.tika.metadata.Metadata;

/**
 *
 * @author Xander Kno
 */
public class ParserProxy {

    private static final Logger log = Logger.getLogger(ParserProxy.class);

    /**
     * Method to handle the parsing of the file represented by the URL
     *
     * @param url Source URL of the file on the Web.
     * @return Parsed text of the file represented by the given URL.
     * @throws EmptyDocumentException
     * @throws FileNotFoundException
     * @throws NoParseableDocumentException
     */
    public synchronized static String parse(URL url) throws EmptyDocumentException, FileNotFoundException, NoParseableDocumentException {
        log.debug("parse(URL)");
        String docContent = null;
        String fileHash;
        String reason = null;
        File fileToParse;
        CacheObject documentCache;
        CacheObject rejectedDocumentCache = null;
        if (url == null) {
            return null;
        }
        fileHash = DigestUtils.getMD5(url.toString());
        try {
            rejectedDocumentCache = CacheHandler.getCachedObjectHandler("rejected.documents");
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                documentCache = CacheHandler.getCachedObjectHandler("documents");
                if (documentCache.objectExists(fileHash)) {
                    fileToParse = new File((String) documentCache.getObject(fileHash));
                    if (fileToParse != null && fileToParse.exists()) {
                        docContent = FileParser.parse(fileToParse);
                        //If doc content is empty or null we reject the file, so we need to remove it from the doc cache.
                        if (docContent == null || docContent.isEmpty()) {
                            reason = "The file {" + url.toString() + "} does not contain relevant text to be classified.";
                            throw new EmptyDocumentException(reason);
                        }
                        //We need to create a file to contain the file title and the content.
                        log.info("DocTitle: " + getDocTitleFromFile(fileToParse));
                    } else {
                        reason = "The file {" + fileToParse.getAbsolutePath() + "} was not found.";
                        throw new FileNotFoundException(reason);
                    }
                }
            } else {
                reason = (String) rejectedDocumentCache.getObject(fileHash);
                throw new EmptyDocumentException(reason);
            }
        } catch (NoParseableDocumentException ex) {
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                rejectedDocumentCache.setObject(fileHash, reason);
            }
            throw new NoParseableDocumentException(ex);
        } catch (EmptyDocumentException ex) {
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                rejectedDocumentCache.setObject(fileHash, reason);
            }
            throw new EmptyDocumentException(reason);
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException(reason);
        }
        return docContent;
    }

    public static String getDocTitleFromFile(File file) {
        log.debug("getFileTitle(File)");
        if (file == null) {
            log.error("The file cannot be null.");
            return null;
        }
        if (!file.isFile() || !file.exists()) {
            log.warn("The file {" + file.getAbsolutePath() + "} does not exist or it's not a file.");
            return null;
        }
        return FileUtils.getMetadata(file).get(Metadata.TITLE);
    }
}
