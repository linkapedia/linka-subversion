package com.yuxipacific.documents.network;

import com.yuxipacific.documents.storage.exceptions.download.DeniedDownloadException;
import com.yuxipacific.documents.storage.exceptions.download.FileSizeExceededException;
import com.yuxipacific.security.utils.DigestUtils;
import com.yuxipacific.services.cache.CacheHandler;
import com.yuxipacific.services.cache.CacheObject;
import java.io.*;
import java.net.*;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;

/**
 * Class to handle the logic for any download, allowing the search of a file in the cache or the Internet if it does not exist.
 * <p/>
 * @author Xander Kno
 * @since 04/24/2012
 */
public class DownloadProxy {

    private static final Logger log = Logger.getLogger(DownloadProxy.class);
    private static final int GENERAL_TIMEOUT = 120000;
    private static final long MAX_FILE_ZILE;
    private static final ResourceBundle systemConfig = ResourceBundle.getBundle("system/config");
    private static int tempFileCounter = 0;

    static {
        MAX_FILE_ZILE = Long.parseLong(systemConfig.getString("file.maxSize"));
    }

    /**
     *
     * @param url
     * @param destination
     * @return Disk path of the downloaded file.
     * @throws FileNotFoundException
     * @throws FileSizeExceededException
     * @throws DeniedDownloadException
     * @throws SocketTimeoutException
     */
    public static String downloadFile(URL url, String destination) throws FileNotFoundException, FileSizeExceededException, DeniedDownloadException, SocketTimeoutException {
        log.debug("downloadFile(URL, String)");
        String fileHash = null;
        String filePath = null;
        InputStream input;
        CacheObject documentCache;
        CacheObject rejectedDocumentCache = null;

        if (url == null || "".equals(url.toString()) || url.toString().isEmpty()) {
            return null;
        }
        ParseContext context = new ParseContext();
        Detector detector = new DefaultDetector();
        Parser parser = new AutoDetectParser(detector);
        context.set(Parser.class, parser);
        Metadata metadata = new Metadata();
        try {
            fileHash = DigestUtils.getMD5(url.toString());
            //Get The cache objects to filter the information.

            rejectedDocumentCache = CacheHandler.getCachedObjectHandler("rejected.documents");

            //Check if the document has not been rejected before.
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                documentCache = CacheHandler.getCachedObjectHandler("documents");
                //If the doc is in the cache and in the disk we can return the path, if not, we have to download the file again.
                if (documentCache.objectExists(fileHash) && (new File((String) documentCache.getObject(fileHash))).exists()) {
                    filePath = (String) documentCache.getObject(fileHash);
                } else {
                    //if the file is not on the cache it means that it needs to be downloaded and parsed.
                    //Validate if we have a valid file to be downloaded.
                    //Search if we need to drop the file
                    input = TikaInputStream.get(url, metadata);
                    MediaType mediaType = detector.detect(input, metadata);
                    String[] ignoredFiles = systemConfig.getString("system.files.ignore.subtypes").split(",");
                    for (String ignore : ignoredFiles) {
                        if (ignore.equals(mediaType.getSubtype())) {
                            throw new DeniedDownloadException();
                        }
                    }

                    //Download the file.
                    File file = getFileFromURI(url.toString(), destination);
                    if (file != null) {
                        filePath = file.getAbsolutePath();
                        documentCache.setObject(fileHash, filePath);
                    } else {
                        log.warn("The file was not downloaded.");
                    }
                }
            } else {
                throw new FileSizeExceededException((String) rejectedDocumentCache.getObject(fileHash));
            }
        } catch (FileNotFoundException ex) {
            String reason = "This file {" + url.toString() + "} was rejected. Reason {File Not Found}";
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                rejectedDocumentCache.setObject(fileHash, reason);
            }
            throw new FileNotFoundException((String) rejectedDocumentCache.getObject(fileHash));
        } catch (SocketTimeoutException ex) {
            String reason = "This file {" + url.toString() + "} was rejected. Reason {Connection Timeout}";
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                rejectedDocumentCache.setObject(fileHash, reason);
            }
            throw new SocketTimeoutException((String) rejectedDocumentCache.getObject(fileHash));
        } catch (IOException ex) {
            String reason = "This file {" + url.toString() + "} was rejected. Reason {File Not Found}";
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                rejectedDocumentCache.setObject(fileHash, reason);
            }
            throw new FileNotFoundException((String) rejectedDocumentCache.getObject(fileHash));
        } catch (FileSizeExceededException ex) {
            String reason = "This file {" + url.toString() + "} was rejected. Reason {File size exceeded}";
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                rejectedDocumentCache.setObject(fileHash, reason);
            }
            throw new FileSizeExceededException((String) rejectedDocumentCache.getObject(fileHash));
        } catch (DeniedDownloadException ex) {
            String reason = "This file {" + url.toString() + "} was rejected. Reason {Forbidden FileType}";
            if (!rejectedDocumentCache.objectExists(fileHash)) {
                rejectedDocumentCache.setObject(fileHash, reason);
            }
            throw new DeniedDownloadException((String) rejectedDocumentCache.getObject(fileHash));
        } catch (Exception ex) {
            log.error("An exception has ocurred while downloading the document.", ex);
        }
        return filePath;
    }

    /**
     *
     * @param sURI
     * @param destination
     * @return
     * @throws FileNotFoundException
     * @throws FileSizeExceededException
     * @throws SocketTimeoutException
     * @throws IOException
     * @throws Exception
     */
    private static File getFileFromURI(String sURI, String destination) throws FileNotFoundException, FileSizeExceededException, SocketTimeoutException, IOException, Exception {
        log.debug("getFileFromURI(String, Long)");
        File _tmpFile;

        URLConnection urlConnection;
        FileOutputStream fos = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        ByteArrayOutputStream bos = null;
        int len;
        String fileHash;
        String extension = "html";

        if (sURI == null || sURI.trim().equals("")) {
            log.error("Error in getFileFromURI, no URI provided.");
            throw new IllegalArgumentException("You must provide a URI.");
        }

        try {
            log.info("Downloading file: " + sURI);
            long startTime = System.currentTimeMillis();
            if (sURI.toLowerCase().startsWith("http") || sURI.toLowerCase().startsWith("ftp")) {
                //Create the url to get the file.
                URL myURL = new URL(sURI);
                urlConnection = myURL.openConnection();
                urlConnection.setConnectTimeout(GENERAL_TIMEOUT);
                urlConnection.setReadTimeout(GENERAL_TIMEOUT);
                //Open the http connection
                if ("http".equals(myURL.getProtocol())) {
                    //Http
                    HttpURLConnection httpCon;
                    httpCon = (HttpURLConnection) urlConnection;
                    httpCon.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.1)");
                    httpCon.setDoInput(true);
                    httpCon.setDoOutput(true);
                    httpCon.setUseCaches(false);
                    httpCon.setDefaultUseCaches(false);
                    log.debug("HttpResponseCode: " + httpCon.getResponseCode());
                    if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        if (httpCon.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
                            log.error("The site <" + sURI + "> cannot be found.");
                            throw new FileNotFoundException("The site <" + sURI + "> cannot be found.");
                        } else if (httpCon.getResponseCode() == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                            log.error("The classifier couldn't access the page <" + sURI + ">. Internal server error");
                            throw new FileNotFoundException("The classifier couldn't access the page <" + sURI + ">. Internal server error");
                        } else if (httpCon.getResponseCode() == HttpURLConnection.HTTP_FORBIDDEN) {
                            log.error("The classifier couldn't access the page <" + sURI + ">. Access Forbidden.");
                            throw new FileNotFoundException("The classifier couldn't access the page <" + sURI + ">. Access Forbidden.");
                        } else if (httpCon.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) {
                            log.error("The classifier couldn't access the page <" + sURI + ">. Moved Temporarily.");
                            throw new FileNotFoundException("The classifier couldn't access the page <" + sURI + ">. Moved Temporarily.");
                        } else {
                            log.error("Http error opening " + myURL.getUserInfo() + " : " + httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
                            throw new Exception("Http error opening " + myURL.getUserInfo() + " : " + httpCon.getResponseCode() + " " + httpCon.getResponseMessage());
                        }
                    }
                    is = httpCon.getInputStream();
                } else {
                    //Ftp
                    is = urlConnection.getInputStream();
                }

                String contentLength = urlConnection.getHeaderField("Content-Length");
                long sizeFile = 0;
                if (contentLength != null && !contentLength.isEmpty()) {
                    sizeFile = Long.parseLong(contentLength);
                }

                String contentType = urlConnection.getContentType();
                if (contentType != null) {
                    extension = (contentType.contains("pdf")) ? "pdf" : (contentType.contains("msword")) ? "doc" : (contentType.contains("text/plain")) ? "txt" : "html";
                }

                if (sizeFile > MAX_FILE_ZILE) {
                    if (!extension.equalsIgnoreCase("txt") && !extension.equalsIgnoreCase("html")) {
                        throw new FileSizeExceededException();
                    }
                }
                bis = new BufferedInputStream(is);
                bos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                long byteRead = 0;
                while (((len = bis.read(buffer)) > 0) && (byteRead < MAX_FILE_ZILE)) {
                    bos.write(buffer, 0, len);
                    byteRead += len;
                }
                //Get the content type of the URL to create a file with the correct extension
                log.debug("Content-Type: " + contentType + "/ extension: " + extension);

                fileHash = DigestUtils.getMD5(sURI);

                String fileDownloadPath = destination + File.separatorChar + extension + File.separatorChar;

                _tmpFile = new File(fileDownloadPath + fileHash);
                while (_tmpFile.exists()) {
                    //If the file already exists, we must create another file.
                    //FIXME: This case should not happen since the hash is unique.
                    fileHash = fileHash + "-" + (tempFileCounter++);
                    _tmpFile = new File(fileDownloadPath + fileHash);
                }
                log.debug("Writting the File to " + _tmpFile.getAbsolutePath());
                //Create the folder structure.
                _tmpFile.getParentFile().mkdirs();
                fos = new FileOutputStream(_tmpFile);
                fos.write(bos.toByteArray());
            } else {
                if (sURI.toLowerCase().startsWith("file")) {
                    sURI = sURI.replace("file://", "");
                }
                _tmpFile = new File(sURI);
            }
            long endTime = System.currentTimeMillis();
            log.info("Download file process total execution time: " + (endTime - startTime) + "ms");
        } catch (FileNotFoundException ex) {
            log.error("File not found for uri: " + ex.getMessage());
            throw ex;
        } catch (UnknownHostException ex) {
            log.error("Cannot found host: " + ex.getMessage());
            throw ex;
        } catch (SocketTimeoutException ex) {
            log.error("Connection time out: " + ex.getMessage());
            throw ex;
        } catch (IOException ex) {
            log.error("IO Error: " + ex.getMessage());
            throw ex;
        } catch (FileSizeExceededException ex) {
            throw ex;
        } catch (Exception ex) {
            log.error("General Error: " + ex.getMessage());
            throw ex;
        } finally {
            try {
                if (bis != null) {
                    bis.close();
                }
                if (bos != null) {
                    bos.flush();
                    bos.close();
                }
                if (fos != null) {
                    fos.flush();
                    fos.close();
                }
                if (is != null) {
                    is.close();
                }
            } catch (Exception e) {
                log.error("General Error in getFileFromURI() closing resources.", e);
            }
        }
        return _tmpFile;
    }
}
