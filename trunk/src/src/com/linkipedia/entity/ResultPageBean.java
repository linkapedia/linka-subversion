package com.linkipedia.entity;

public class ResultPageBean {
    
    private String url;
    private String title;
    private String snippet;
    
    public ResultPageBean(String url, String title, String snippet) {
        super();
        this.url = url;
        this.title = title;
        this.snippet = snippet;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }
    
    

}
