package com.linkipedia.entity;

import java.util.List;
import java.util.Map;

import com.linkipedia.dao.SearchDocumentDao;

public class Concepts {
	
	static SearchDocumentDao searchDao=new SearchDocumentDao();
	
	private int corpusId;
	private int nodeId;
	private String nodeTitle;
	private int nodeSize;
	private int parenentId;
	private int nodeIndexWithInParent;
	private int  depthFromRoot;
	private int crawlStatus;
	
	private List<Signatures> signatures;
	private List<MustHave> musthave;
	private Map<String, List<GoogleCrawlResult>> results;
	
	/**
	 * Ticket #21 Change META DESCRIPTION
	 */
	private List<Source> source;
	
	public List<Source> getSource() {
		return searchDao.getSourceById(nodeId);
	}

	public void setSource(List<Source> source) {
		this.source = source;
	}

	public Concepts(int nodeId) {
		super();
		this.nodeId = nodeId;
	}
	
	public int getCrawlStatus() {
		return crawlStatus;
	}

	public void setCrawlStatus(int crawlStatus) {
		this.crawlStatus = crawlStatus;
	}

	public int getCorpusId() {
		return corpusId;
	}
	public void setCorpusId(int corpusId) {
		this.corpusId = corpusId;
	}
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeTitle() {
		return nodeTitle;
	}
	public void setNodeTitle(String nodeTitle) {
		this.nodeTitle = nodeTitle;
	}
	public int getNodeSize() {
		return nodeSize;
	}
	public void setNodeSize(int nodeSize) {
		this.nodeSize = nodeSize;
	}
	public int getParenentId() {
		return parenentId;
	}
	public void setParenentId(int parenentId) {
		this.parenentId = parenentId;
	}
	public int getNodeIndexWithInParent() {
		return nodeIndexWithInParent;
	}
	public void setNodeIndexWithInParent(int nodeIndexWithInParent) {
		this.nodeIndexWithInParent = nodeIndexWithInParent;
	}
	public int getDepthFromRoot() {
		return depthFromRoot;
	}
	public void setDepthFromRoot(int depthFromRoot) {
		this.depthFromRoot = depthFromRoot;
	}
	public Concepts(int corpusId, int nodeId, String nodeTitle, int nodeSize,
			int parenentId, int nodeIndexWithInParent, int depthFromRoot) {
		this.corpusId = corpusId;
		this.nodeId = nodeId;
		this.nodeTitle = nodeTitle;
		this.nodeSize = nodeSize;
		this.parenentId = parenentId;
		this.nodeIndexWithInParent = nodeIndexWithInParent;
		this.depthFromRoot = depthFromRoot;
	}
	public Concepts() {
	}
	/**
	 * @return the signatures
	 */
	public List<Signatures> getSignatures() {
		return searchDao.getSignaturesById(nodeId);
	}
	/**
	 * @param signatures the signatures to set
	 */
	public void setSignatures(List<Signatures> signatures) {
		this.signatures = signatures;
	}
	/**
	 * @return the musthave
	 */
	public List<MustHave> getMusthave() {
		return searchDao.getMustHavesById(nodeId);
	}
	/**
	 * @param musthave the musthave to set
	 */
	public void setMusthave(List<MustHave> musthave) {
		this.musthave = musthave;
	}
	/**
	 * @return the results
	 */
	public Map<String, List<GoogleCrawlResult>> getResults() {
		return searchDao.getResultGroups(nodeId);
	}
	/**
	 * @param results the results to set
	 */
	public void setResults(Map<String, List<GoogleCrawlResult>> results) {
		this.results = results;
	}
	

}
