package com.linkipedia;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.linkipedia.crawl.ContentParse2GoogleCrawlResult;
import com.linkipedia.crawl.ImageCrawl;
import com.linkipedia.dao.ImageBeanDao;
import com.linkipedia.dao.SearchDocumentDao;
import com.linkipedia.entity.Concepts;
import com.linkipedia.entity.GoogleCrawlResult;
import com.linkipedia.entity.MustHave;

public class StartGoogleSearch {

	private static Properties props = null;

	/**
	 * @param args
	 */
	private static Logger logger = Logger.getLogger(StartGoogleSearch.class);

	public static void main(String[] args) throws Exception {

		SearchDocumentDao dao = new SearchDocumentDao();
		ImageBeanDao imageDao = new ImageBeanDao();
		ImageCrawl imageCrawl = new ImageCrawl();
		// FIXME,just for test!!!!!!!!!!!
		//List<Concepts> list = dao.getConcepts();
		//List<Concepts> list = dao.getConceptsTemp();
		List<Concepts> templist = dao.getAllConcepts();
		
		
		List<Concepts> list = new ArrayList<Concepts>();
		for (Concepts c:templist) {
			if (c.getCrawlStatus()==0) 
				list.add(c);	
		}
		
		

		if (logger.isInfoEnabled())
			logger.info("------------Begin to Google Search!-----------------");

		for (int x = 0; x < list.size(); x++) {
			if (x != 0 && x % 20 == 0) {
				Thread.currentThread().sleep(30000);
			} 
			else if (x != 0 && x % 60 == 0) {
				Thread.currentThread().sleep(80000);
			}
			System.out.println("   ==Totle size is: "+list.size()+" ,the current count is "+x+"  ===");

			Concepts concept = list.get(x);
			
			if (concept.getCrawlStatus() == 0){
				int nodeId = concept.getNodeId();
				if (logger.isInfoEnabled())
					logger.info("Begin to crawl the metadata ,the nodeId is :"
							+ nodeId);
				String nodeTitle = concept.getNodeTitle();

				// link of the image to DB
				// ImageBean imageBean =
				// imageCrawl.parseContentToImageBean(nodeTitle,nodeId);
				// if (imageBean != null)
				// imageDao.insertImageBean(imageBean);

				List<MustHave> mustHaveList = dao.getMustHavesById(nodeId);
				String keywords = "\"" + nodeTitle + "\" ";
				if (mustHaveList != null && mustHaveList.size() > 0) {
					for (int i = 0; i < mustHaveList.size(); i++) {
						MustHave mustHave = mustHaveList.get(i);
						String mustWord = mustHave.getMustWord();
						if (mustWord != null && mustWord.trim().length() > 0) {
							if (!nodeTitle.equalsIgnoreCase(mustWord))
								keywords=keywords+"\""+mustWord+"\" ";
						}
					}
				} 
				System.out.println("    "+keywords + "   ===keywords====");

					ContentParse2GoogleCrawlResult c = new ContentParse2GoogleCrawlResult();
						List<GoogleCrawlResult> videolist = null;
						try {
							videolist = c.parseVedioContentToResult(keywords, 80, "youtube.com", 0, true, nodeId);
							//dao.batchInsertResult(videolist);
						} catch (java.net.MalformedURLException e) {
							e.printStackTrace();
							System.out.println("This url is ignored.");
						} catch (Exception e2){
							e2.printStackTrace();
							System.out.println("Sleeping and will be trying once more.");
							Thread.currentThread().sleep(5*60*1000);
							videolist = c.parseVedioContentToResult(keywords, 80, "youtube.com", 0, true, nodeId);
							//dao.batchInsertResult(videolist);
						}finally{
							if (videolist != null && videolist.size()>0){
								dao.batchInsertResult(videolist);
								}
						}
						

						String[] domain = { ".org", ".edu", ".net", ".gov" };
						for (String str : domain) {
							List<GoogleCrawlResult> list_ = null;
							try {
							list_ = c.parseContentToResult(keywords, 80, str, 0, false, nodeId);
							//dao.batchInsertResult(list_);
							} catch (java.net.MalformedURLException e) {
								e.printStackTrace();
								System.out.println("This url is ignored.");
							}
							catch(Exception e2){
								e2.printStackTrace();
								System.out.println("Sleeping and will be trying once more.");
								Thread.currentThread().sleep(5*60*1000);
								list_ = c.parseContentToResult(keywords, 80, str, 0, false, nodeId);
								//dao.batchInsertResult(list_);
							}finally{
								if (list_ != null && list_.size() > 0){
								dao.batchInsertResult(list_);
								}
							}
						}
					//update the cral status by nodeid and set the status to 1 means had crawl it.
					dao.updatsCrawlStatus(concept.getNodeId(),1);

				if (logger.isInfoEnabled())
					logger.info("End to crawl the metadata ,the nodeId is :"
							+ nodeId);
				
			} else {
				if (logger.isInfoEnabled())
					logger.info(concept.getNodeId()+ " had crawled the metadata,will skip this nodeid.");
			}
	
		}
		if (logger.isInfoEnabled())
			logger.info("------------Google Search Done!-----------------");

	}

}
