package com.linkipedia.dao;

/**
 * Connect to Linkipedia and execute sql script;
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import java.util.List;

import com.linkipedia.constant.Constants;
import com.linkipedia.entity.GoogleCrawlResult;
import com.linkipedia.entity.Link;

public class LinkDao extends Dao {

	public LinkDao() {
		super();

	}

	public List<Link> getChildren(Link link) {
		return this
				.getLinks("select t.nodeid,t.nodetitle,t.depthfromroot from t_metadata t where " + 
						" exists(select * from (select min(t2.nodeid)as nodeid from t_metadata t2 where t2."+Constants.CORPUSID+" group by t2.nodetitle)temp where t.nodeid=temp.nodeid) " + 
						" and exists (select d.nodeid from t_document d where d.nodeid=t.nodeid)"  +
						" and t.parentid=" + link.getNodeid() + " and t."+Constants.CORPUSID+
						" order by t.NODEINDEXWITHINPARENT");
	}

	public List<Link> getLev1Links() {
		return this
				.getLinks("select nodeid,nodetitle,depthfromroot from t_metadata where depthfromroot = 1 and "+Constants.CORPUSID+" order by NODEINDEXWITHINPARENT");

	}

	private List<Link> getLinks(String sql) {

		List<Link> list=null;
		Connection conn = this.getConn();
		try {
			Statement stmt = conn
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.first()) {
				list = new ArrayList<Link>();
				rs.beforeFirst();
			} else
				return null;

			while (rs.next()) {
				int nodeid = (Integer) rs.getInt("nodeid");
				String nodetitle = (String) rs.getString("nodetitle");
				int depthfromroot =  rs.getInt("depthfromroot");
				Link link = new Link(nodeid, nodetitle,depthfromroot);
				list.add(link);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	/**
	 * Ticket #20 Ensure results contain the word "coffee"
	 */
	public void wipeOutLinks(Link link){
		if(link != null){
			Link bottomLink = getBottomLink(link);
			Link parentLink = bottomLink.getParent();
			if(parentLink != null){
				while(parentLink != null){
					wipeOutLink(parentLink);
					parentLink = parentLink.getParent();
				}
			}else{
				link.setContainCoffee(false);
			}
			
		}
	}
	private void wipeOutLink(Link link){
		List<Link> childLinks = link.getChildren();
		if(childLinks != null && childLinks.size() > 0){
			for(Link childLink : childLinks){
				if(childLink.isContainCoffee() == null){
					wipeOutLink(childLink);
				}
			}
			removeLink(link);
		}else{
			setLinkContainCoffee(link);
		}
		
	}
	private void removeLink(Link parentLink){
		List<Link> childLinks = parentLink.getChildren();
		if(childLinks != null){
			removeCurrentLinks(childLinks);
			if(childLinks.size()>0){
				parentLink.setContainCoffee(true);
				return;
			}
		}
		parentLink.setContainCoffee(false);
	}
	public void removeCurrentLinks(List<Link> childLinks){
		if(childLinks != null){
			for(int i = childLinks.size()-1; i >= 0; i--){
				if(!childLinks.get(i).isContainCoffee()){
					childLinks.remove(i);
				}
			}
		}
	}
	private Link getBottomLink(Link link){
		List<Link> links = link.getChildren();
		if(links != null && links.size() > 0){
			return getBottomLink(links.get(0));
		}else{
			return link;
		}
	}
	
	private void setLinkContainCoffee(Link link){
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			connection = getConn();
			st = connection.createStatement();
			rs = st.executeQuery("select doc.TITLE,doc.SNIPPET,doc.DOMAIN_ID,domain.DOMAIN_NAME from t_document doc inner join t_domain domain on doc.DOMAIN_ID=domain.DOMAIN_ID where doc.NODEID="
							+ link.getNodeid() + " order by doc.DOMAIN_ID");
			while (rs.next()) {
				String title = rs.getString("TITLE");
				String snippet = rs.getString("SNIPPET");
				if(containCoffee(title) || containCoffee(snippet)){
					link.setContainCoffee(true);
					return;
				}
			}
			link.setContainCoffee(false);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(rs, st, connection);
		}
	}
	private boolean containCoffee(String source){
		if(source != null){
			String lowwerSource = source.toLowerCase();
			if(lowwerSource.contains(Constants.WORD_COFFEA)||lowwerSource.contains(Constants.WORD_COFFEE)){
				return true;
			}
		}
		return false;
	}
}
