package com.linkipedia.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;

import com.linkipedia.constant.Constants;
import com.linkipedia.util.PropertiestUtils;

public class Dao {

	static String host;
	static String user;
	static String password;
	static Properties prop = null;
	static BasicDataSource dataSource;
	static{
		try {
			prop = PropertiestUtils.loadConfigFile();
			host = prop.getProperty(Constants.JDBC_URL);
			user = prop.getProperty(Constants.JDBC_USER);
			password = prop.getProperty(Constants.JDBC_PASSWORD);
			Properties porps = PropertiestUtils.loadConfigFile();
			
			dataSource = new BasicDataSource();
			dataSource.setDriverClassName(porps.getProperty(Constants.JDBC_DRIVER));
			dataSource.setUrl(host);
			dataSource.setUsername(user);
			dataSource.setPassword(password);
			
			dataSource.setInitialSize(5);
			dataSource.setValidationQuery("select count(*) from t_metadata where nodeid = 1111");
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
	
	public Dao() {
		super();
		this.host = host;
		this.user = user;
		this.password = password;
	}
	
	public Dao(String host, String user, String password) {
		super();
		this.host = host;
		this.user = user;
		this.password = password;
	}


	public Connection getConn() {
		Connection c = null;

		try {
			c=dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}
	


	public Dao close(ResultSet rs, Statement sm, Connection conn) {
		close(rs).close(sm).close(conn);
		return this;
	}

	/**
	 * Close the Connection
	 * 
	 * @param Connection
	 */
	public Dao close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
		return this;
	}

	/**
	 * Close the Statement
	 * 
	 * @param Statement
	 */
	public Dao close(Statement sm) {
		if (sm != null) {
			try {
				sm.close();
			} catch (SQLException e) {
			}
		}
		return this;
	}

	/**
	 * Close the ResultSet
	 * 
	 * @param ResultSet
	 */
	public Dao close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		return this;
	}
}
