package com.linkipedia.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.linkipedia.constant.Constants;

public class PropertiestUtils {
	
	private static Logger logger = Logger.getLogger(PropertiestUtils.class);
	/**
	 * Load the linkpedia.ini
	 * 
	 * @return The properties
	 */
	public static Properties loadConfigFile() throws IOException {
		Properties prop = new Properties();
		FileInputStream fis = null;
		String configFile = System.getProperty("user.dir") + File.separatorChar + "linkipedia.ini";
		try {
			fis = new FileInputStream(configFile);
			prop.load(fis);
			return prop;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/**
	 * Validate the parameters in linkpedia.ini
	 * 
	 * @param prop
	 *            The properties to check.
	 * @return True if all are valid, otherwise return false.
	 */
	private boolean validateConfigParameters(Properties prop) {
		String driverName = prop.getProperty(Constants.JDBC_DRIVER, "");
		if (driverName == null || driverName.length() <= 0) {
			logger.error("DRIVER_NAME in linkpedia.ini is not valid!");
			return false;
		}
		String dbURL = prop.getProperty(Constants.JDBC_URL, "");
		if (dbURL == null || dbURL.length() <= 0) {
			logger.error("DB_URL in linkpedia.ini is not valid!");
			return false;
		}
		String userName = prop.getProperty(Constants.JDBC_USER, "");
		if (userName == null || userName.length() <= 0) {
			logger.error("USER_NAME in linkpedia.ini is not valid!");
			return false;
		}
		String userPwd = prop.getProperty(Constants.JDBC_PASSWORD, "");
		if (userPwd == null || userPwd.length() <= 0) {
			logger.error("USER_PASSWORD in linkpedia.ini is not valid!");
			return false;
		}
		return true;
	}
	

}
