<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${title}</title>
	<link href="css/common.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-min.js" ></script>
	<!--[if IE 6]>
		<script type="text/javascript" src="css/iepng.js" ></script>
		<script type="text/javascript">
		DD_belatedPNG.fix('div, ul, img, li, input');
		</script>
	<![endif]-->
	<meta name="Description" content="${description}" />
	<meta name="Keywords" content="${keywords}" />
	
	<script type="text/javascript">
	
		var morelinktext="More";
		var lesslinktext="5 Records only";
		var moreorless2html="<div style='padding-left:20px'><a class='moreorless2' href='#'>" + morelinktext + "</a></div>";
		var moreorless3html="<a class='moreorless3' href='#'>" + morelinktext + "</a>";
		
		$(function(){
			showTab(1);	
			hideLev2();
			hideLev3();					
			$("p.nav1 img").toggle(function(){$(this).parent().next().hide();$(this).attr("src","images/icon2.gif");},function(){$(this).parent().next().show();$(this).attr("src","images/icon1.gif");});
			$("p.nav2 img").toggle(function(){$(this).parent().nextAll().hide();$(this).attr("src","images/icon4.gif");},function(){$(this).parent().nextAll().show();$(this).attr("src","images/icon3.gif");});
			$("div.nav1 a").click(function(){$("div.nav1 a").removeClass("now");$(this).addClass("now");});
			$("a.moreorless2").toggle(function(){$(this).text(lesslinktext);$(this).parent().siblings().show();},function(){$(this).text(morelinktext);$(this).parent().siblings("div:gt(4)").hide();});
			$("a.moreorless3").toggle(function(){$(this).text(lesslinktext);$(this).siblings().show();},function(){$(this).text(morelinktext);$(this).siblings("a:gt(4)").hide();$(this).siblings("br:gt(4)").hide();});			
			$("p.nav2 img").click();
			$("div.content_left a[href$='" + getFileName() + "']").addClass("now");
		});

		function getFileName(){
			 var url = window.location.href;   
			  fileName = url.split("//")[1].split("/");			
			  //file = fileName[fileName.length-1].split(".")[0]; 
			  return fileName;
			}

		function hideLev2(){
			$("div.nav1 div").each(
					function(){
					if ($(this).children("div").size()>5){
						$(this).children("div:gt(4)").hide();
						$(this).children("div:last").after(moreorless2html);					
					}					
				}
			);
		}
		
		function hideLev3(){
			$("p.nav3").each(
					function(){
					if ($(this).children("a").size()>5){
						$(this).children("a:gt(4)").hide();
						$(this).children("br:gt(4)").hide();
						$(this).children("br:last").after(moreorless3html);
					}
				}
			);
		}

		function showTab(tab){
			var allTabs=$("div#results div.tab");
			allTabs.hide();
			$(".link3 a").removeClass("now");
			var tabdiv=$("div#results div#tab" + tab);
			//if (tabdiv==null)
			//	alert("No result in the domain.");
			tabdiv.show();
			$(".link3 a:eq("+ (tab-1) +")").addClass("now");
		}
		
	</script>

</head>

<body>
<div id="container">
	<div id="header">
		<div id="logo"><a href="http://www.konamountaincoffee.com"><img src="images/logo.png" alt="logo" /></a></div>
		<div id="link_right">
			<div class="link1"><a href="http://www.konamountaincoffee.com">Kona Mountain Coffee</a></div>
			<div class="link2">
				<script type="text/javascript">
					function shs_click(type) {
						u=location.href;
						t=document.title;
						switch (type){
						case "fb":
							url = 'http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t);
							break;
						case "tw":
							url ='http://twitter.com/share?text=Check+out+'+encodeURIComponent(u)+'&t='+encodeURIComponent(t);
							break;
						case "dg":
							url = 'http://digg.com/submit?url='+encodeURIComponent(u)+'&t='+encodeURIComponent(t);
							break;
						} 
						window.open(url,'sharer','toolbar=0,status=0,width=626,height=436');
						return false;
					}
				</script>
				<a onclick="event.cancelBubble=true; return shs_click('fb');" target="_blank" href="http://www.facebook.com/share.php?u=www.linkipedia.com"><img src="images/fackbook.png" alt="facebook" width="32" height="32" /></a>
				<a onclick="event.cancelBubble=true; return shs_click('tw');" target="_blank" href="http://twitter.com/home?status=Check+out+http://www.linkipedia.com"><img src="images/twitter.png" alt="twitter" width="32" height="32" /></a>
				<a onclick="event.cancelBubble=true; return shs_click('dg');" target="_blank" href="http://digg.com/submit?url=http://www.linkipedia.com"><img src="images/digg.png" alt="digg" width="32" height="32" /></a>
			</div>
			<div class="link3">
					<ul>
						<#list domainbeanlist?sort_by("domain_id") as domain>
						<li class="domainbutton"><a href="#" onclick="showTab(${domain.domain_id})">${domain.domain_name}</a></li>
						<li><img src="images/line1.png" /></li>
						</#list>
					</ul>
			</div>
		</div>
	</div>
	<div id="content">
		<div class="location">
		<#--assign count=0-->
		<#if homepage=="true">
			
			<#else>
			<#list link.path as crumb>
				<a href="${crumb.link}">${crumb.nodetitle}</a>
				<#--assign count=count+1--> 
				<#if (crumb_index lt link.path?size-1) > -> </#if>
			</#list>	
		 </#if>
		
		</div>
		<div class="content_left">
			${left_content}
		</div>
		<div class="content_right">
			<h1>${header}</h1>
			<table>
			  <tr>
			    <td width="219"><img src="images/pic1.gif" /></td>
			    <td width="193"><img src="images/pic2.gif" /></td>
			    <td width="43">&nbsp;</td>
			  </tr>
			  <tr>
			    <td align="center">Dark roasted coffee beans</td>
			    <td align="center">Light roasted coffee beans</td>
			    <td>&nbsp;</td>
			  </tr>
			</table>
			
			<div id="results">	
				<#if homepage=="true">
					<p><h3>Welcome to Linkipedia!</h3>Please click the links to view search results.</p>
					<#else>
					<#--assign domain_ids=results?keys?sort-->		
					<#--list domain_ids as domain_id-->	
					
					<#list domainbeanlist?sort_by("domain_id") as domain>
						<div class="tab" id="tab${domain.domain_id}">
							<#if results?keys?seq_contains(domain.domain_id?c)>							
								<#list results[domain.domain_id?c] as snippet>
									<p><h3><a href="${snippet.url}">"${snippet.title}"</a></h3>"${snippet.snippet!''}"</p>
								</#list>
								<#else>
								<p><h3>No result found for this domain.</h3></p>
							</#if>
						</div>			
					</#list>	
				
				</#if>		
			</div>			
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="footer">© 2010 Linkipedia. All rights reserved</div>
</div>
</body>
</html>
