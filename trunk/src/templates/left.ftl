<#list links as biglink>			
	<div class="nav1" >
		<p class="nav1" ><img src="images/icon1.gif" /> <a title="${biglink.nodetitle}" href="${biglink.link}"><#if biglink.nodetitle?length gt 30>${biglink.nodetitle?substring(0,30)}...<#else>${biglink.nodetitle}</#if></a></p>		
		<div>
			<#if biglink.children?exists> 
			<#list biglink.children as smalllink>						
		 		 <div class="nav2" >
		 		 <p class="nav2" ><img src="images/icon3.gif" /><a  title="${smalllink.nodetitle}" href="${smalllink.link}"><#if smalllink.nodetitle?length gt 25>${smalllink.nodetitle?substring(0,25)}...<#else>${smalllink.nodetitle}</#if></a></p>
			 		 <p class="nav3" >
			 		 <#if smalllink.children?exists>
			 		 <#list smalllink.children as littlelink>						 		 
			 		 	<a  title="${littlelink.nodetitle}" href="${littlelink.link}" ><#if littlelink.nodetitle?length gt 30>${littlelink.nodetitle?substring(0,30)}...<#else>${littlelink.nodetitle}</#if></a><br />						    	
			    	</#list>
			    	</#if>
			    	</p>
			    </div>						    
		    </#list>
		    </#if>
	    </div>
    </div>			    				
</#list>